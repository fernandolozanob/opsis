-- clés primaires manquantes
ALTER TABLE ONLY t_doc_lex_droit ADD CONSTRAINT t_doc_lex_droit_pkey PRIMARY KEY (id_doc_lex);
ALTER TABLE ONLY t_doc_publication ADD CONSTRAINT t_doc_publication_pkey PRIMARY KEY (id_doc_publication);
ALTER TABLE ONLY t_etat_usager ADD CONSTRAINT t_etat_usager_pkey PRIMARY KEY (id_etat_usager, id_lang);
ALTER TABLE ONLY t_imageur ADD CONSTRAINT t_imageur_pkey PRIMARY KEY (id_imageur);
ALTER TABLE ONLY t_pers_lex ADD CONSTRAINT t_pers_lex_pkey PRIMARY KEY (id_pers, id_lex);
ALTER TABLE ONLY t_session2_ado ADD CONSTRAINT t_session2_ado_pkey PRIMARY KEY (sesskey);
ALTER TABLE ONLY t_short_url ADD CONSTRAINT t_short_url_pkey PRIMARY KEY (id_short_url);
ALTER TABLE ONLY t_type_exp ADD CONSTRAINT t_type_exp_pkey PRIMARY KEY (id_type_exp);
ALTER TABLE ONLY t_type_import ADD CONSTRAINT t_type_import_pkey PRIMARY KEY (id_type_import, id_lang);
