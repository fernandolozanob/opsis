-- MISE A JOUR DES ETAPES DE TRANSCODAGE IMAGE AVEC GESTION FICHIERS RAW 
delete from t_proc where id_proc in (200,201,202,203,204);
delete from t_proc_etape where id_etape in (200,201,202,203,204);
delete from t_etape where id_etape in (200,201,202,203,204);


INSERT INTO t_proc VALUES (200, 'Taille Originale - JPEG');
INSERT INTO t_proc VALUES (201, 'HD - JPEG');
INSERT INTO t_proc VALUES (202, 'MD - JPEG');
INSERT INTO t_proc VALUES (203, 'SD - JPEG');
INSERT INTO t_proc VALUES (204, 'TIFF');

INSERT INTO t_etape VALUES (200, 'Encodage ORI - JPEG', 25, '.', '.jpg', '<param><ufraw_conf>opsis01.ufraw</ufraw_conf><compression>90</compression><logo_marge>10</logo_marge><logo_position>center</logo_position></param>');
INSERT INTO t_etape VALUES (201, 'Encodage HD - JPEG', 25, '.', '.jpg', '<param><ufraw_conf>opsis01.ufraw</ufraw_conf><compression>80</compression><logo_marge>10</logo_marge><logo_position>center</logo_position><height>2000</height><width>2000</width><kr>1</kr></param>');
INSERT INTO t_etape VALUES (202, 'Encodage MD - JPEG', 25, '.', '.jpg', '<param><ufraw_conf>opsis01.ufraw</ufraw_conf><compression>80</compression><logo_marge>10</logo_marge><logo_position>center</logo_position><height>1000</height><width>1000</width><kr>1</kr></param>');
INSERT INTO t_etape VALUES (203, 'Encodage SD - JPEG', 25, '.', '.jpg', '<param><ufraw_conf>opsis01.ufraw</ufraw_conf><compression>80</compression><logo_marge>10</logo_marge><logo_position>center</logo_position><height>480</height><width>480</width><kr>1</kr></param>');
INSERT INTO t_etape VALUES (204, 'Encodage TIFF', 25, '.', '.tif', '<param><ufraw_conf>opsis01.ufraw</ufraw_conf><logo_marge>10</logo_marge><logo_position>center</logo_position></param>');



INSERT INTO t_proc_etape VALUES (200, 200, 1, 0);
INSERT INTO t_proc_etape VALUES (201, 201, 1, 0);
INSERT INTO t_proc_etape VALUES (202, 202, 1, 0);
INSERT INTO t_proc_etape VALUES (203, 203, 1, 0);
INSERT INTO t_proc_etape VALUES (204, 204, 1, 0);



UPDATE t_etape SET etape_param = '<param><ufraw_conf>opsis01.ufraw</ufraw_conf><compression>80</compression><height>2000</height><width>2000</width><kr>1</kr><logo_marge>10</logo_marge><logo_position>center</logo_position><MAT_TYPE>VISIO</MAT_TYPE></param>' where id_etape = 5;


-- set formats online apres update matinfo avec la nouvelle nomenclature 3.0.6.0 (aussi faisable via admin/tables systemes)
-- UPDATE t_format_mat SET FORMAT_ONLINE = 1  WHERE format_mat like '% - MP3' ; 
-- UPDATE t_format_mat SET format_online=1 WHERE format_mat like 'SD - 4:3 - MP4 - H264' OR format_mat like 'SD - 16:9 - MP4 - H264'; 
/*
SELECT ft.format_mat, count(id_mat) FROM t_format_mat ft
LEFT JOIN t_mat ON format_mat = mat_format 
GROUP BY ft.format_mat 
HAVING count(id_mat) = 0 ; */ 


DELETE FROM t_format_mat where format_mat in (
	SELECT ft.format_mat FROM t_format_mat ft
	LEFT JOIN t_mat ON format_mat = mat_format 
	GROUP BY ft.format_mat 
	HAVING count(id_mat) = 0 
	);
