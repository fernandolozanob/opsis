UPDATE t_val SET val_code='FR' WHERE val_id_type_val='LANG' AND valeur ilike 'français';
UPDATE t_val SET val_code='EN' WHERE val_id_type_val='LANG' AND valeur ilike 'anglais';
UPDATE t_val SET val_code='ES' WHERE val_id_type_val='LANG' AND valeur ilike 'espagnol';
UPDATE t_val SET val_code='DE' WHERE val_id_type_val='LANG' AND valeur ilike 'allemand';
UPDATE t_val SET val_code='IT' WHERE val_id_type_val='LANG' AND valeur ilike 'italien';
UPDATE t_val v1 SET val_code=v2.val_code from t_val v2 WHERE v2.val_id_type_val='LANG' and v2.id_lang='FR' and v1.id_lang!='FR' and v1.id_val=v2.id_val;

INSERT INTO t_module (id_module,module_nom,module_type) VALUES ('32','authot','PAROL');

INSERT INTO t_proc (id_proc,proc_nom) VALUES ('20','Transcription');

INSERT INTO t_etape (id_etape,etape_nom,etape_id_module,etape_in,etape_out,etape_param)
VALUES ('20','Transcription','32','.','%s','<param><url>https://app.xn--autht-9ta.com</url><token>BoIonWDotWBA475TMrA28-bC9w6ocOjpcj0G27Rr9WJB36TUsXSgew</token><function>trans</function><file_vtt>1</file_vtt><MAT_TYPE>AUTHOT</MAT_TYPE><DMAT_INACTIF>1</DMAT_INACTIF></param>');

INSERT INTO t_proc_etape (id_proc,id_etape,pe_ordre,pe_id_etape_prec) VALUES ('20','20','1','0');

INSERT INTO t_engine (id_engine,eng_id_module,eng_nom,eng_ip,eng_max_jobs) VALUES ('32','32','authot_localhost','localhost','4');


