INSERT INTO t_engine (ID_ENGINE,ENG_ID_MODULE,ENG_NOM,ENG_IP,ENG_MAX_JOBS)
VALUES 
(33,33,'audiowaveform_localhost','localhost',4);

INSERT INTO t_module (ID_MODULE,MODULE_NOM,MODULE_TYPE) VALUES (33,'audiowaveform','ENCOD');

DELETE FROM t_etape where id_etape = 21;
INSERT into t_etape VALUES (21,'Extraction AudioWaveForm',33,'.','_awf.dat','<param><MAT_TYPE>AUDIOWAVEFORM</MAT_TYPE></param>');

INSERT into t_proc_etape VALUES (5,21,2,8);

INSERT into t_lieu VALUES ('WAVEFORM','localhost','public/waveform/','%DAT');