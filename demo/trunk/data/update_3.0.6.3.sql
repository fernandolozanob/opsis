-- Etat doc
truncate t_etat_doc;
insert into t_etat_doc values (1,'FR','Media à valider');
insert into t_etat_doc values (2,'FR','À indexer');
insert into t_etat_doc values (3,'FR','Indexation à valider');
insert into t_etat_doc values (4,'FR','Validé');

insert into t_etat_doc values (1,'EN','Media to validate');
insert into t_etat_doc values (2,'EN','To be indexed');
insert into t_etat_doc values (3,'EN','Indexing to validate');
insert into t_etat_doc values (4,'EN','Validated');

insert into t_etat_doc values (1,'DE','Media zu bestätigen');
insert into t_etat_doc values (2,'DE','Zu indexieren');
insert into t_etat_doc values (3,'DE','Indexierung zu bestätigen');
insert into t_etat_doc values (4,'DE','Gültig');

-- Module validation
insert into t_module values (6,'validation','VALID');
INSERT INTO t_etape (ID_ETAPE, ETAPE_NOM, ETAPE_ID_MODULE, ETAPE_IN, ETAPE_OUT, ETAPE_PARAM) VALUES
(10, 'Validation administrateur', 6, '.', '', '<param><mail>info@opsomai.com</mail></param>');
