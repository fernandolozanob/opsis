delete from t_etape where id_etape=90;
insert into t_etape (id_etape,etape_nom,etape_id_module,etape_in,etape_out,etape_param) values (90,'Encodage - Pivot Montage - ProResHQ',3,'.','.mov','<param><flag_etape>montage_pivot</flag_etape><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>3</profile> <aspect_mode>letterbox</aspect_mode> <pix_fmt>yuv422p10le</pix_fmt> <height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');

update t_etape set etape_param='<param><use_embed>1</use_embed><MAT_TYPE>VISIO</MAT_TYPE></param>' where id_etape=5;


update t_etape set etape_param=replace(etape_param,'<aspect_ratio>4:9</aspect_ratio>','<aspect_ratio>4:3</aspect_ratio>') where etape_param like '%<aspect_ratio>4:9</aspect_ratio>%';

-- -------------------------------------
--  Issu de maj_pg_opsis3.0.6
-- -------------------------------------

ALTER TABLE t_mat ALTER COLUMN mat_type_mime TYPE character varying(255);
ALTER TABLE t_mat ADD COLUMN mat_format_env character varying(20) DEFAULT ''::character varying NOT NULL;
ALTER TABLE t_mat ALTER COLUMN mat_format TYPE character varying(100);
ALTER TABLE t_format_mat ALTER COLUMN format_mat TYPE character varying(100);
ALTER TABLE t_mat_track ADD COLUMN mt_codec_long character varying(100);

-- -------------------------------------
-- Opsis3.0.6.2
-- -------------------------------------

-- -------------------------------------
-- Ajout champ à t_doc
-- -------------------------------------
ALTER TABLE t_doc ADD COLUMN doc_date_1  character varying(20);
ALTER TABLE t_doc ADD COLUMN doc_date_2  character varying(20);
ALTER TABLE t_doc ADD COLUMN doc_date_3  character varying(20);
ALTER TABLE t_doc ADD COLUMN doc_date_4  character varying(20);
ALTER TABLE t_doc ADD COLUMN doc_date_5  character varying(20);

-- -------------------------------------
-- Clé primaire t_doc_lex_droit
-- -------------------------------------
ALTER TABLE ONLY t_doc_lex_droit
ADD CONSTRAINT t_doc_lex_droit_pkey PRIMARY KEY (id_doc_lex);

-- ===================

ALTER TABLE t_mat ADD COLUMN mat_bit_rate integer DEFAULT 0 NOT NULL;

-- -------------------------------------
-- Clé primaire t_import
-- -------------------------------------
ALTER TABLE ONLY t_import ADD CONSTRAINT t_import_pkey PRIMARY KEY (id_import);

-- -------------------------------------
-- Opsis3.0.6.31
-- -------------------------------------
-- -------------------------------------
-- Ajout champ à t_mat
-- -------------------------------------
ALTER TABLE t_mat ADD mat_resolution int;

-- -------------------------------------
-- Table t_partage
-- -------------------------------------

CREATE SEQUENCE t_partage_id_partage_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER TABLE public.t_partage_id_partage_seq OWNER TO opsomai;

CREATE TABLE t_partage (
id_partage integer DEFAULT nextval('t_partage_id_partage_seq'::regclass) NOT NULL,
partage_id_lang character varying(2) DEFAULT ''::character varying NOT NULL,
partage_id_gen bigint DEFAULT (0)::bigint NOT NULL,
partage_id_entite integer DEFAULT (0)::integer,
partage_type_entite character varying(40) DEFAULT NULL::character varying,
partage_type character varying(40) DEFAULT NULL::character varying,
partage_code character varying(100) DEFAULT NULL::character varying,
partage_xml text DEFAULT ''::text NOT NULL,
partage_date_crea timestamp without time zone,
partage_date_limit timestamp without time zone,
partage_id_usager_crea integer DEFAULT (0)::integer,
partage_mail_dest character varying(255) DEFAULT ''::character varying
);
ALTER TABLE public.t_partage OWNER TO opsomai;

ALTER TABLE ONLY t_partage
ADD CONSTRAINT t_partage_pkey PRIMARY KEY (id_partage);


