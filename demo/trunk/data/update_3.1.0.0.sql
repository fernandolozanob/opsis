-- Passage lexique NC en hiérachique
update t_type_lex set hierarchique='1' where id_type_lex='NC';

-- Init t_engine
truncate t_engine;
insert into t_engine (id_engine, eng_id_module, eng_nom, eng_ip, eng_max_jobs) select id_module, id_module, concat(module_nom,'_local'), 'localhost',2 from t_module;
