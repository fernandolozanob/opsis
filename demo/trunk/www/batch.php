<?php
    // Lancement en CLI de batch situés dans le répertoire batch du  moteur
    $time_start = microtime(true);
    require_once("conf/conf.inc.php");
    require_once(libDir."session.php");
    require_once(libDir."fonctionsGeneral.php");
    require_once(libDir."class_page.php");

    header("content-type: text/plain");
    
//    if (isset($_GET['action']) && !empty($_GET['action'])){
//    	$batchFile=$_GET['action'].".php";
//    }
    if($argc>1)
    {
        $batchFile=$argv[1];
    }
    
    if (!empty($batchFile)) {
        if (substr($batchFile,-4)!=".php") $batchFile.=".php";
        if(is_file(batchDir.$batchFile)){
			trace('execution batch moteur : '.$batchFile);
            require_once(batchDir.$batchFile);
        }else{
            print "Batch manquant : ".$batchFile."\n";
        }
    }
    else
    {
        print "Syntaxe : batch.php nom_batch\n";
    }
 	$time_end = microtime(true);
	$time=$time_end-$time_start;
	exit;
?>

