<?php
	if ($myUsr->loggedIn()) {
		require_once(modelDir.'model_panier.php');
		$myPanier=new Panier;

		if(defined("gShareSelGrp") && gShareSelGrp && isset($_REQUEST['getGroupes']) && $_GET['getGroupes']== "1"){
			$folders=Panier::getGroupeFolders();
			$myPage->params4XSL['groupeSel']=1;
		}else{
			$folders=Panier::getFolders();
		}
	} else
	{
		require_once(modelDir.'model_panier_session.php');
		$myPanier=new PanierSession;
		$folders=PanierSession::getFolders();
	}
	
	$array_pan_partial_width = array('docListe');
	
	$css_classes=(!isset($_GET['urlaction']) || !in_array($_GET['urlaction'],$array_pan_partial_width))?'full_width':'';
	$css_classes.= ' collapsed';
	
	if (!empty($_POST['stylePanier']) || isset($_GET['showPanier'])){
		$css_classes .=" open_on_load " ;
	}
?>

<div id="panBlock" class=" pres_<?=$_SESSION['USER']['layout_opts']['layout']?>  <?=$css_classes?> ">
	<div id="panGauche" class="">
		<div class="title_bar" onclick="togglePanBlock();event.stopPropagation();"><span class="cart_label"><?=kPaniers;?></span><span id="panCollapseLeft" onclick="togglePanBlock('collapsed');event.stopPropagation();"></span><span id="panExpandFullLeft" onclick="togglePanBlock('full');event.stopPropagation();"></span></div>
		<ul class="panListe"></ul>
	</div>
	<div id="mainPan">
		<div class="title_bar"  onclick="event.stopPropagation();">
			<span id="notice_pan_result">
				<span id="panTitle"><?
				if(isset($_GET['xsl']) && !empty($_GET['xsl']) && $_GET['xsl'] != 'dossier'){ // ajout exception dossier.xsl => permet d'éviter répercussion XSL main page sur panierFooter
					$_REQUEST['xsl'] = $_GET['xsl'];
				}else if( isset($_SESSION['panierListeXSL'])){
					$_REQUEST['xsl'] = $_SESSION['panierListeXSL'];
				}else {
					$_REQUEST['xsl'] = "panier";
				}

				$_REQUEST['nbLignes'] = "all";

				if(isset($_SESSION['USER']['layout_opts']['id_current_panier']) && !empty($_SESSION['USER']['layout_opts']['id_current_panier'])
				&& isset($_SESSION['USER']['layout_opts']['panListeRefToLastOrder']) && !empty($_SESSION['USER']['layout_opts']['panListeRefToLastOrder'])
				&& $_SESSION['USER']['layout_opts']['panListeRefToLastOrder'] == $_SESSION['USER']['layout_opts']['id_current_panier']){
					$_REQUEST['id_panier'] = $_SESSION['USER']['layout_opts']['panListeRefToLastOrder'];
					echo kQuickOrderRefToLastCommande;
				}else if(isset($_SESSION['USER']['layout_opts']['id_current_panier']) && !empty($_SESSION['USER']['layout_opts']['id_current_panier'])){
					foreach($folders as $f){
						if($f['ID_PANIER'] == $_SESSION['USER']['layout_opts']['id_current_panier']){
							if(intval($f['PAN_ID_ETAT']) == 1){
								echo kPanier;
							}else{
								echo $f['PAN_TITRE'];
							}
							$_REQUEST['id_panier'] = $f['ID_PANIER'];
						}
					}
				}else if(count($folders)>0 && $folders[0]['PAN_ID_ETAT'] ==0 && isset($folders[0]['PAN_TITRE']) && !empty($folders[0]['PAN_TITRE'])){
					echo $folders[0]['PAN_TITRE'];
					$_REQUEST['id_panier'] = $folders[0]['ID_PANIER'];
				}else if($folders[0]['PAN_ID_ETAT'] == 1 ){
					echo kPanier;
					$_REQUEST['id_panier'] = $folders[0]['ID_PANIER'];
				}else{
					echo "&#160;";
				}?></span>
				<span id="panNbElems"></span>
			</span>
			<div class='toolbar' onclick="event.stopPropagation();">
				<div class="presentation">
					<span id="panListChooser" class="<?=($_SESSION['USER']['layout_opts']['panier']['affMode']=='liste')?'selected':'';?>" onclick="pres_panier_update({'panier' : {'affMode' : 'liste'}});"> <!-- 'panier2' -->
					<span class="tool_hover_text"><?echo kAffichageListe;?></span></span>
					<span id="panMosChooser"  class="<?=($_SESSION['USER']['layout_opts']['panier']['affMode']=='mos')?'selected':'';?>" onclick="pres_panier_update({'panier' : {'affMode' : 'mos'}});">
					<span class="tool_hover_text"><?echo kAffichageMos;?></span></span>
				</div>
				<div class="toggle_preview tool <?=($_SESSION['USER']['layout_opts']['prev_pop_in'])?'selected':'';?>" onclick="toggleFlagPrevPopin();">
					<span class="tool_hover_text">Prévisualisation</span>
				</div>
				<div class="check_all tool" onclick="smartCheckAll(this,$j('#panFrame'));" >
					<span class="tool_hover_text"><?echo kToutSelectionner;?></span>
				</div>
				<div class="add tool" >
					<span class="tool_hover_text"><?echo kAjouterA;?></span>
					<div class="drop_down" style="display:none;"></div>
				</div>
				<div class="tri tool">
					<span class="tool_hover_text"><?echo kTrierPar;?></span>
					<div class="drop_down" style="display:none;">
						<ul>
							<li onclick="loadPanier(id_current_panier,null,'doc_titre');"><? echo kTitre;?></li>
							<li onclick="loadPanier(id_current_panier,null,'doc_date_prod');"><? echo kDate;?></li>
							<li onclick="loadPanier(id_current_panier,null,'fonds');"><? echo kFonds;?></li>
							<li onclick="loadPanier(id_current_panier,null,'doc_id_type_doc');"><? echo kType;?></li>
						</ul>
					</div>
				</div>
				<div  onclick="triggerPrint('panierdoc')" class=" print tool">
					<span class="tool_hover_text"><?echo kImprimer;?></span>
				</div>
				<div class="menu_actions tool" >
					<span class="tool_hover_text"><?echo kMenuActions;?></span>
					<div class="drop_down" style="display:none;">
						<ul>
							<li onclick="displayMenuActions('export','panierdoc',id_current_panier)" style=""><? echo kExporter;?></li>
							<li onclick="displayMenuActions('partage','panier',id_current_panier)" style=""><? echo kPartager;?></li>
							<? if ($_SESSION['USER']['US_ID_TYPE_USAGER'] > 2){ ?> 
								<li onclick="displayMenuActions('modifLot','panier_doc',id_current_panier);" style=""><? echo kModificationLot;?></li>		                    	
							<? } ?>
						<li onclick="if(check_selected()) displayMenuActions('deplacer');" style=""><? echo kDeplacerVers;?></li>
						<li onclick="if(check_selected()) displayMenuActions('copier');" style=""><? echo kCopierVers;?></li>
						<li onclick="displayMenuActions('transfert','panierdoc',id_current_panier);" style=""><? echo kTransferer;?></li>							
						</ul>
					</div>
				</div>
			</div>
			<span class='montage_back' onclick="retourSelFromMontage();"><?=kQuitterMontage?></span>
		</div>
		<div id="panListe_container">
			<select name="" id="panListe" class="panHeaderFields" onchange="loadPanier(this.value);return false;"></select>
		</div>
		<div id="panierDropZone" ><span id="textDropZone"><?=kDropZoneText?></span></div>
			<div id="panFrame"></div>
	</div>

	<script>
		var id_current_panier = '<?=$_REQUEST['id_panier']?>';
		<?unset($_REQUEST['id_panier']);?>
		var ordre = 0;
		var flag_reload_current_pan = false ;
		var flag_noupdate_current_pan_ref = false ;
		var panier_xsl = '<?=$_REQUEST['xsl']?>';
		refreshDroppables();
		
		refreshFolders();
		$j(document).ready(function(){
			if($j("#panBlock").hasClass('open_on_load')){
				togglePanBlock('full');
				flag_reload_current_pan = true ;
				loadPanier(id_current_panier,$j('#panier'+id_current_panier),null,null,null,function(){
					if($j("#panBlock td.resultsCorps").length >0 ){
						loadPrevDoc($j("#panBlock td.resultsCorps input[name='t_panier_doc[][ID_DOC]']").eq(0).val(),$j("#panBlock tr td.resultsCorps").eq(0).parent());
					}else if ($j("#panBlock div.resultsMos")){
						$j("#panBlock .resultsMos").eq(0).trigger('onclick');
					}
				});
			}else{
				flag_reload_current_pan = true ; 
				loadPanier(id_current_panier,$j('#panier'+id_current_panier));
			}
		});
		
	



	
	</script>

</div>
