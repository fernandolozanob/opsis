<!-- PIED DE PAGE -->
<?
if((!isset($_COOKIE['ops_accept_cookie']) && !isset($_SESSION['cookie_implicit_auth']))){
	$_SESSION['cookie_implicit_auth'] = 1;
	?>
	<div id="msg_info_cookie_box">
		<div id="msg_info_cookie_txt">
			<?=kMsgInfoCookies ?>
			<br/>
			<div class="msg_info_buttons">
			<button class="std_button_style" onclick="$j('#msg_info_cookie_box').hide();"><?=kOk?></button>
			<button class="std_button_style" onclick="showCGU('cookies');"><?=kPlusInformations?></button>
			</div>
		</div>
	</div>
	<?
}else if (isset($_SESSION['cookie_implicit_auth']) && $_SESSION['cookie_implicit_auth'] == 1){
	setcookie('ops_accept_cookie','true',strtotime('+5 years'));
	unset($_SESSION['cookie_implicit_auth']);
}?>
<div id="footer">
<a href="mailto:<?= gMail ?>">Contact</a> | <a href="javascript:showCGU();"><?=kConditionsGenerales?></a> |  <?=" <a href='http://www.opsomai.com' target='_blank'>Opsis Media".(defined('siteTagNumberFromRelease')?' '.siteTagNumberFromRelease:'')." &copy;".kAnneeCopyright." ".kQuiCopyright."</a>";?>
</div>
