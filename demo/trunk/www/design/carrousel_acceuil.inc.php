<div id="carrousel_accueil">
	<div id="carrousel_items_wrapper">
		<?php

		if(defined("kSaveVignetteAsHDForCatIds") && is_string(kSaveVignetteAsHDForCatIds)){
			require_once(modelDir.'model_categorie.php');
			$id_catHD = explode(',',kSaveVignetteAsHDForCatIds);
			$id_catHD = $id_catHD[0]; 
			$catAccueil = new Categorie() ; 
			$catAccueil->t_categorie['ID_CAT'] = $id_catHD ;
			$catAccueil->getCategorie();
			$catAccueil->getDocCat();		
		}

		$cnt = 0 ; 
		foreach($catAccueil->t_doc_cat as $cdoc){
			if($cnt>=10){
				break ; 
			}
			if(!empty($cdoc['CDOC_ID_IMAGE'])){
				$cnt++ ; 
				require_once(modelDir.'model_image.php');
				$img = new Image() ;
				$img->t_image['ID_IMAGE'] = $cdoc['CDOC_ID_IMAGE'];
				$img->getImage();
				$url_image = storyboardChemin.$img->getFilePath();

				echo '<div class="carrousel_item_wrapper">';
				echo '<div class="carrousel_item cdoc_'.$cdoc['ID_DOC'].' '.(isset($cdoc['DOC_ID_MEDIA']) && !empty($cdoc['DOC_ID_MEDIA'])?'media_'.$cdoc['DOC_ID_MEDIA']:'').'" style="background-image:url(\'design/images/degrade_vertical_60px_black_lighter.png\'),url(\''.$url_image.'\');"></div>';
				echo '<div class="carrousel_item_info">';
				switch($cdoc['DOC_ID_MEDIA']){
					case 'V' :
						echo '<img alt="ic&ocirc;ne vid&eacute;o" onclick="window.location=\'index.php?urlaction=doc&id_doc='.$cdoc['ID_DOC'].'\'" src="design/images/100x100-icninfo-video.png" />';
					break;
					case 'P' :
						echo '<img alt="ic&ocirc;ne photo" onclick="window.location=\'index.php?urlaction=doc&id_doc='.$cdoc['ID_DOC'].'\'"  src="design/images/100x100-icninfo-image.png" />';
					break;
					case 'D' :
						echo '<img alt="ic&ocirc;ne document" onclick="window.location=\'index.php?urlaction=doc&id_doc='.$cdoc['ID_DOC'].'\'"  src="design/images/100x100-icninfo-file.png" />';
					break;
					case 'A' :
						echo '<img alt="ic&ocirc;ne audio" onclick="window.location=\'index.php?urlaction=doc&id_doc='.$cdoc['ID_DOC'].'\'"  src="design/images/100x100-icninfo-audio.png" />';
					break;
					case 'R' :
						echo '<img alt="ic&ocirc;ne reportage" onclick="window.location=\'index.php?urlaction=doc&id_doc='.$cdoc['ID_DOC'].'\'"  src="design/images/100x100-icninfo-dossier.png" />';
					break;
				}

				echo '<span class="carrousel_item_titre" onclick="window.location=\'index.php?urlaction=doc&id_doc='.$cdoc['ID_DOC'].'\'">'.$cdoc['DOC_TITRE'].'</span>' ;
				echo '</div>';
				echo '</div>';
			}

		}
		?>

	</div>
	<div id="carrousel_control">
	</div>
</div>
<script type="text/javascript">
var holder, items_wrapper, items, current,timerInterval=9000,control_holder,control_spans,cycle_timeout,detection_resize = false,animating = false;

$j(document).ready(function(){
	initCarrousel();
});

function initCarrousel(){
	holder = $j("#carrousel_accueil");
	items_wrapper = $j("#carrousel_items_wrapper");
	items = $j("#carrousel_items_wrapper .carrousel_item_wrapper");

	items.eq(0).addClass('selected');

	current = 0 ;

	cycle = function(){
		current++;
		goTo(current);
		cycle_timeout = setTimeout(cycle,timerInterval);
	}
	cycle_timeout = setTimeout(cycle,timerInterval);
	initControlCarrousel();

	if ($j(window).width()<tablet_border){
		calcCarrouselLabelWidth();
	}

	$j(window).resize(function(){
		if(animating){
			detection_resize = true ;
		}else{
			goTo(current,false,true)
		}
	});
}

function initControlCarrousel(){
	control_holder = $j("#carrousel_control");
	control_holder.css('visibility','hidden');
	items.each(function(idx,elt){
		control_holder.append('<span class="carrousel_control_span" onclick="goTo('+idx+',true)"></span>');
	});
	control_spans = $j("#carrousel_control .carrousel_control_span");
	control_spans.eq(0).addClass('selected');
	control_holder.css('visibility','visible');

}

function goTo(idx,manualGoTo,instantAnimation){
	if(typeof instantAnimation == 'undefined'){
		instantAnimation = false ;
	}

	if(idx >= items.length){
		idx = 0;
		current = 0;
	}
	if(manualGoTo){
		current = idx;
		clearTimeout(cycle_timeout);
		cycle_timeout = setTimeout(cycle,timerInterval);
	}

	control_spans.removeClass('selected');
	control_spans.eq(idx).addClass('selected');
	animating = true ;
	if(instantAnimation){
		$j(items_wrapper).css('left','-'+idx*holder.width()+"px");
		animating = false ;
	}else{
		duration = 1000 ;
		$j(items_wrapper).animate({
			left : '-'+idx*holder.width()+"px"
		},{
			queue : false,
			duration : duration,
			easing : 'swing',
			complete : function(){
				items.removeClass('selected');
				items.eq(idx).addClass('selected');
				animating = false ;
			},
			step : function (now,tween){
				if(detection_resize && (-(idx*holder.width()) !=  tween.end)){
					tween.end = -(idx*holder.width());
					detection_resize = false;
				}

			}
		});
	}
}
</script>