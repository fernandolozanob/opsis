var elements=[];
//console.log("test")
// console.log(elements);
 // console.log("drag_and_drop.js");


function dragImage(draggable){
    if(document.getElementById("selections")){
      if(document.getElementById("selections").style.display!="block") document.getElementById("selections").style.display = "block";
    }
	if(draggable.element.id.indexOf('doc_') === 0 && $j("#panierDropZone").length>0){
		if(typeof draggable.current_delta != "undefined"){
			current_delta = draggable.current_delta;
			if((Math.abs(draggable.clone_delta[0]-current_delta[0])>20 || Math.abs(draggable.clone_delta[1]-current_delta[1])>20)){
				if($j("#panBlock").hasClass('collapsed')){
					togglePanBlock();
				}
				if(draggable.element.nodeName != 'LI'){ // dans ce cas, les 'LI' ne sont utilisés que pour le montage => à améliorer
					$j("#panierDropZone").addClass("visible");
				}
			}
		}else{
			if($j("#panBlock").hasClass('collapsed')){
				togglePanBlock();
			}
			// console.log(draggable);
			if(draggable.element.nodeName != 'LI'){ // dans ce cas, les 'LI' ne sont utilisés que pour le montage => à améliorer
				$j("#panierDropZone").addClass("visible");
			}
		}
	}
}

function dropItem( dragitem,droparea,finished){
    if(dragitem.parentNode.parentNode.parentNode.id==droparea.id) return;
    split=dragitem.id.split("_");
	typeItem = split[0];
    idItem=split[1];
    if(droparea.id=="link_new_selection"){
      displayNewPanLeft(idItem,droparea);
    }else if (droparea.id.indexOf("classeur") == 0){
      var idCart = droparea.id.substr(8);
      add2folder(idCart,idItem,typeItem);
    }else if (droparea.id.indexOf("panier") == 0){
      var idCart = droparea.id.substr(6);
	  if(idCart == id_current_panier){flag_reload_current_pan = true ; }
      add2folder(idCart,idItem,typeItem);
    }else if (droparea.id.indexOf('panFrame') == 0 && typeof id_current_panier != "undefined"){
		idCart = id_current_panier ;
		add2folder(idCart,idItem,typeItem);
		flag_reload_current_pan = true ;
	}

	if(typeof id_current_panier != "undefined" || typeof idCart !='undefined'){
		if(typeof idCart !='undefined'){
			id_load_panier = idCart;
		}else{
			id_load_panier = id_current_panier;
		}
		$j("#panFrame").addClass('loading')	;
		setTimeout(function(){loadPanier(id_load_panier)},1000);
	}
	if($j("#panierDropZone").length>0){
		$j("#panierDropZone").removeClass("visible");
	}

  }
function endDrag (){
	if($j("#panierDropZone").length>0){
		$j("#panierDropZone").removeClass("visible");
	}

}


//creating Draggable and Droppable objects

function refreshDroppables() {
	if(document.getElementById('link_new_selection')){
		Droppables.remove('link_new_selection');
		Droppables.add( 'link_new_selection', { accept:'conteneur_dragable', hoverclass: 'hoverPanier',onDrop: dropItem, includeScrolls: true } );
	}

	$j("li[id^='panier']").each(function () {
		var idObj = $j(this).attr('id');
		Droppables.remove(idObj);
        Droppables.add(idObj, {  accept:'conteneur_dragable', hoverclass: 'hoverPanier',onDrop: dropItem, includeScrolls: true } );
	});

	if($j("#panFrame").length>0){
		Droppables.add('panFrame',{accept:'conteneur_dragable',hoverclass:'hoverMainFramePanier', onDrop : dropItem, includeScrolls: true} );
	}
}

function updateDraggables(){
	for (_u=0;_u<elements.length;_u++) {
		if(elements[_u] != arr_draggable[_u]){
			new Draggable(elements[_u], {ghosting:false,revert:true,scroll: window,onDrag:dragImage,cloning:true, onEnd : endDrag, scrolls:window,scrollSensitivity: 100});
			arr_draggable[_u] = elements[_u];
		}
	}
}

var arr_draggable = new Array();

window.onload = function() {
	//console.log(elements);
	if(elements){
		for (_u=0;_u<elements.length;_u++) {
			if(document.getElementById(elements[_u]))
				new Draggable(elements[_u], {ghosting:false,revert:true,scroll: window,onDrag:dragImage,cloning:true, onEnd : endDrag, scrollSensitivity: 100});
				arr_draggable[_u] = elements[_u];
		}
	}

	refreshDroppables();
}
