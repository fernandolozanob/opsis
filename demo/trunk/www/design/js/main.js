function displayFacettes(elt)
{
	if ($j(elt).prev().is(':visible')){
		// $j(elt).prev().hide('fast');
		$j(elt).prev().css('display','none');
		$j(elt).parent('.block_facet.open').removeClass("openfull");
		if(typeof str_lang !="undefined" && str_lang.kAfficherTout)
			$j(elt).html(str_lang.kAfficherTout);
	}else{
		$j(elt).parent('.block_facet.open').addClass("openfull");
		$j(elt).prev().show('fast');
		if(typeof str_lang !="undefined" && str_lang.kAfficherTopResults)
			$j(elt).html(str_lang.kAfficherTopResults);
	}
}


// fonction d'affichage du formulaire de nommage rapide d'une nouvelle sélection (menuGauche)
function displayNewPanLeft(id,ctrl,type) {

    myDiv=document.getElementById('newPanLeft');
    myDiv.style.display='block';
    document.formCreaSelection.cartName.focus();
    document.formCreaSelection.cartIdItem.value = id;
    document.formCreaSelection.cartTypeItem.value = type;
    document.getElementById('link_new_selection').style.display='none';
}
function hideNewPanLeft() {

	if (document.getElementById('newPanLeft')!=null){
	    myDiv=document.getElementById('newPanLeft');
	    myDiv.style.display='none';
	    document.formCreaSelection.cartName.value='';
	    document.formCreaSelection.cartIdItem.value = null;
	    document.formCreaSelection.cartTypeItem.value = null;
	    document.getElementById('link_new_selection').style.display='block';
    }
}

// fonction d'affichage du formulaire de nommage rapide d'une nouvelle sélection (menuGauche) en version mobile
function displayNewPanResponsiveForm(id,ctrl,type) {
	var lang = ow_const['langue'].toLowerCase();
	var jsNom = {'en':'Name','fr':'Nom'};
	$j('#panFrame #formPanier').css('display','none');
	$j('#panFrame').append('<div class="resultsMenu"><form name="formCreaSelection" action="#" onsubmit="add2folder(-1,this.cartIdItem.value,this.cartTypeItem.value,this.cartName.value);return false;">' +
		'<input name="cartTypeItem" id="cartTypeItem" type="hidden" value="'+type+'" />'+
		'<input name="cartIdItem" id="cartIdItem" type="hidden" value="'+id+'"/>'+
		'<label>'+jsNom[lang] +'&#160;</label><input name="cartName" type="text" />' +
		'<input type="submit" value="OK"  />' +
		'<input type="button" class="std_button_style" alt="'+str_lang.kAnnuler+'" value="Retour" onclick="removeNewPanResponsiveForm()" />' +
		'</form></div>');
	return false;
}
function removeNewPanResponsiveForm() {
	$j('#panFrame #formPanier').css('display','block');
	$j('#panListe option#panier'+id_current_panier+'').prop('selected', true);
	$j('#panFrame .resultsMenu').remove();
	return false;
}

// Fonction de rafraichissement de l'affichage hierarchique des paniers (menuGauche)
function refreshFolders(id) {
    var lang = ow_const['langue'].toUpperCase();
	if (id) {
		fromlink=document.getElementById('folder_button'+id);
		if (fromlink.className=='collapseFolder') {

			fromlink.className='expandFolder';
			fromlink.innerHTML="<img src='design/images/mini_arrow_right.gif' />";
			action='collapse';
			if (document.getElementById('folderDetail'+id)) document.getElementById('folderDetail'+id).style.display='none';
		} else {

			fromlink.className='collapseFolder';
			fromlink.innerHTML="<img src=''design/images/mini_arrow_down.gif' />";
			action='expand';
		}
	} else {action='';}
	return !sendData('GET','blank.php','urlaction=loadPanier&action='+action+'&id='+id+'&id_lang='+lang+'&display=none','dspFolders');
}


function closethis() {
	$j(this).parent().parent().parent().trigger('touchend');
}

// fonction d'affichage hierarchique des paniers sous forme de folder.
// + créa du formulaire de nommage pour la création d'une nouvelle sélection (menuGauche)
function dspFolders(str) {
	var lang = ow_const['langue'].toLowerCase();
	var jsNom = {'en':'Name','fr':'Nom'};
    var jsCreerSelection = {'en':'New cart', 'fr':'Nouveau panier'};
    var jsPanier = {'en':'Cart', 'fr':'Panier'};

	xml = importXML(str);

	var carts = xml.getElementsByTagName('cart');
	output = '';
	output_2 = '';
	if ($j(window).width()>mobil_border) { // desktop et tablette
		for( i=0; i < carts.length ; i ++){
			try{
				id = carts[i].getElementsByTagName('ID_PANIER')[0].firstChild.nodeValue;
			}catch(e){id='';}
			try{
			etat = carts[i].getElementsByTagName('PAN_ID_ETAT')[0].firstChild.nodeValue;
			}catch(e){etat='';}
			try{
			titre = carts[i].getElementsByTagName('PAN_TITRE')[0].firstChild.nodeValue;
			}catch(e){titre='';}

			if(id!=''){
				output += '<li id="panier'+id+'" onclick="loadPanier('+id+',this)">';
				if(etat=='1' && titre == ''){
					output += jsPanier[lang];
				}else{
					output+=titre;
				}
				output+="</li>";

				output_2 += '<li class="add_panier'+id+'" onclick="addCheckedToCart(this,'+id+');">';
				if(etat=='1' && titre == ''){
					output_2 += jsPanier[lang];
				}else{
					output_2+=titre;
				}
				output_2+="</li>";
			}
		}

	    output += '<li id="link_new_selection" onclick="displayNewPanLeft(0, this)" >'+jsCreerSelection[lang]+'</li>'+
									'<li id="newPanLeft" class="resultsMenu" style="clear:both;display:none;">' +
		 							'<form name="formCreaSelection" onsubmit="add2folder(-1,this.cartIdItem.value,this.cartTypeItem.value,this.cartName.value);return false;">' +
									'<input name="cartTypeItem" id="cartTypeItem" type="hidden" />'+
									'<input name="cartIdItem" id="cartIdItem" type="hidden" />'+
									'<label>'+jsNom[lang] +'&#160;</label><input name="cartName" type="text" style="width:100px" />' +
						 		    '<input type="submit" value="OK"  />' +
						 		    '<input type="button" class="std_button_style" alt="'+str_lang.kAnnuler+'" value="X" onclick="hideNewPanLeft()" />' +
						 		    '</form>' +
						 		    '</li>'

		if(typeof panListeRefToLastOrder != 'undefined' && panListeRefToLastOrder!= 0){
			output += '<li id="panier'+panListeRefToLastOrder+'" class="last_order selected" onclick="loadPanier('+panListeRefToLastOrder+',this,null,null,\'commande\');">'+str_lang.kQuickOrderRefToLastCommande+'</li>';
		}
		if(document.querySelector('ul.panListe') != null ){
			document.querySelector('ul.panListe').innerHTML=output;
		}
		if(typeof id_current_panier != 'undefined' && id_current_panier=='first'){
			id_current_panier = $j("ul.panListe li[id^='panier']").eq(0).attr('id').replace('panier','');
		}
		if(typeof id_current_panier != 'undefined' && id_current_panier != ''){
			loadPanier(id_current_panier,$j('ul.panListe #panier'+id_current_panier));
		}
	} else { // mobile
		for( i=0; i < carts.length ; i ++){
			try{
				id = carts[i].getElementsByTagName('ID_PANIER')[0].firstChild.nodeValue;
			}catch(e){id='';}
			try{
			etat = carts[i].getElementsByTagName('PAN_ID_ETAT')[0].firstChild.nodeValue;
			}catch(e){etat='';}
			try{
			titre = carts[i].getElementsByTagName('PAN_TITRE')[0].firstChild.nodeValue;
			}catch(e){titre='';}
			// console.log("id: "+id+" etat: "+etat+" titre: "+titre);

			if(id!=''){
				output += '<option id="panier'+id+'" value="'+id+'">';
				if(etat=='1' && titre == ''){
					output += jsPanier[lang];
				}else{
					output+=titre;
				}
				output+="</option>";

				output_2 += '<li class="add_panier'+id+'" onclick="addCheckedToCart(this,'+id+');">';
				if(etat=='1' && titre == ''){
					output_2 += jsPanier[lang];
				}else{
					output_2+=titre;
				}
				output_2+="</li>";
			}
		}

/*	    var newsel = '<option id="link_new_selection" onclick="displayNewPanLeft(0, this)" >'+jsCreerSelection[lang]+'</option>'+
									'<option id="newPanLeft" class="resultsMenu" style="clear:both;display:none;">' +
		 							'<form name="formCreaSelection" onsubmit="add2folder(-1,this.cartIdItem.value,this.cartTypeItem.value,this.cartName.value);return false;">' +
									'<input name="cartTypeItem" id="cartTypeItem" type="hidden" />'+
									'<input name="cartIdItem" id="cartIdItem" type="hidden" />'+
									'<label>'+jsNom[lang] +'&#160;</label><input name="cartName" type="text" style="width:100px" />' +
						 		    '<input type="submit" value="OK"  />' +
						 		    '<input type="button" class="std_button_style" alt="'+str_lang.kAnnuler+'" value="X" onclick="hideNewPanLeft()" />' +
						 		    '</form>' +
						 		    '</option>';*/
						 		    //console.log(id_current_panier);
	    //var id_current_pan = $j("#panier<?=$_REQUEST['id_panier']?>");
		output += '<option id="link_new_selection" value="" >'+jsCreerSelection[lang] +'</option>';
		
		if(typeof panListeRefToLastOrder != 'undefined' && panListeRefToLastOrder!= 0){
			output += '<option id="panier'+panListeRefToLastOrder+'" class="last_order selected" onclick="loadPanier('+panListeRefToLastOrder+',this,null,null,\'commande\');">'+str_lang.kQuickOrderRefToLastCommande+'</option>';
		}
		
		document.querySelector('select#panListe').innerHTML=output;

		if(typeof id_current_panier != 'undefined' && id_current_panier=='first'){
			id_current_panier = $j("select#panListe option[id^='panier']").eq(0).attr('id').replace('panier','');
		}
		if(typeof id_current_panier != 'undefined' && id_current_panier != ''){
			loadPanier(id_current_panier,$j('select#panListe #panier'+id_current_panier));
		}
	}

	$j('.toolbar .add .drop_down').html("<ul>"+output_2+"</ul>");
	if (refreshDroppables)
		refreshDroppables();

	if($j(window).width()<tablet_border){
		$j("#conteneur .title_bar .add.tool .drop_down li").each(function(idx,elt){
			elt.addEventListener('touchend', function () {
				$j(this).click();
			    $j(this).parent().parent().parent().trigger('touchend');
			    //$j(this).parent().parent().parent().trigger('touchcancel');
			    //$j(this).parent().parent().parent().trigger('mouseleave');
			    //$j(this).parent().parent().parent().trigger('mouseout');
			},true);
		});
	}
}


function addCheckedToCart(elt,idCart){
	if(typeof idCart == 'undefined' || idCart == null){
		return false ;
	}
	items2add="";
	type = "";

	if($j(elt).parents('#panBlock').length>0){
		type = 'pdoc';
		$j("#panFrame input[type='checkbox']:checked()").each(function(idx,elt){
			items2add += $j(elt).val()+'$';
		});
	}else{
		type = 'doc';
		if(typeof current_id_doc != 'undefined' && current_id_doc != null ){
			items2add+=current_id_doc+'$';
		}
		$j("#resultats input[type='checkbox']:checked()").each(function(idx,elt){
			items2add += $j(elt).val()+'$';
		});
	}
	if(idCart == id_current_panier){
		flag_reload_current_pan = true ;
	}

	if(items2add!='' && type != ''){
		return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&commande=add&items='+items2add+"&typeItem="+type,'updateCarts');
	}
}



function toggleMe(tab_id,panel) {
	if (myVideo && myVideo.UnSetSelection) {
		myVideo.UnSetSelection();
	}

	$j('.panelSelWrapper .panelSel').removeClass('selected');
	$j('.panelSelWrapper #sel_'+tab_id).addClass('selected');


	if (myPanel && myPanel._hasChanged) {ok=confirm(str_lang.kConfirmerAbandon); if (!ok) return;}
	if (myPanel) myPanel._hasChanged=false;

	//Le sidecommande qui comprend rightPanel doit suivre l onglet selectionne.
	sidecommandeObj=document.getElementById('sidecommande').cloneNode(true); //clonage du sidecommande

	//suppression du sidecommande original
	document.getElementById('sidecommande').parentNode.removeChild(document.getElementById('sidecommande'));

	//suppression du icon_bar original
	if(document.getElementById('icon_bar')) document.getElementById('icon_bar').parentNode.removeChild(document.getElementById('icon_bar'));

	//re création du sidecommande dans l onglet sélectionné
	tab=document.getElementById(tab_id);
	tab.appendChild(sidecommandeObj);

	if(panel)
		showPanel(panel);
	else
		hidePanel();

	showTab(tab_id);
	//movePanelIconBar();

	toggleSelection(tab_id)
}

/**************** FONCTION RELATIVE RECHERCHE / AFFICHAGE DE RESULTATS *******************/

// fonction qui permet de 'remplir' la mosaique => évite que la dernière ligne soit incomplète si il existe d'autres documents
function fillMosaic(){
	current_cnt = $j("#main_block .resultsMos").length;

	if(nb_mos_w == null ){
		calculateNbLignes();
	}


	if( typeof current_cnt == "number" && (current_cnt%nb_mos_w) != 0){ // typeof obj_cnt == "number" &&
		// console.log(nb_mos_w+" par ligne,  depasse de : "+(current_cnt%nb_mos_w)+", manque :"+(nb_mos_w - (current_cnt%nb_mos_w)));
		$j.ajax({
			url : "empty.php?urlaction=docListe&where=include&from_offset="+current_cnt+"&nbLignes="+(nb_mos_w - (current_cnt%nb_mos_w))+"&fromAjax=1&cherche=0",
			success: function(data){
				$j("#resultats div.pusher").before(data);
				$j("#quick_search input#nbLignes").val(current_cnt+(current_cnt%nb_mos_w));
				updateDraggables();
			},
			complete : function(){
				if($j('#quick_search input#nbLignes').val() >= totalNbResults ){
					$j("#loadMoreRes").css("display","none");
				}
			}
		});
	}
}


// fonction qui permet de gérer le chargement de resultats supplémentaires.
var f_loading = false ;
var loadMoreRes_multiplier = 4 ; 
var loadMoreRes_counter = 0 ; 
var loadMoreRes_num_autoload = 4 ; 
function loadMoreResults(initMos,callback){
	if(typeof initMos =='undefined'){
		initMos = false ;
	}
	loadMoreRes_counter++;

	if(!f_loading && !$j("#loadMoreResults").hasClass("disabled")){
		// flag de blocage empechant plusieurs loading parallèle,
		f_loading = true ;

		// calculs du nb d'élément à récupérer
		if($j("#resultats").hasClass("mos")){
			current_cnt = $j("#main_block .resultsMos").length;
			if(initMos){
				nbLignes = Math.max(0,(Math.max(nb_mos_total*loadMoreRes_multiplier ,nb_lignes_deft) - current_cnt));
			}else{
				// MS - on peut se baser sur la variable "nb_mos_total" initialisée par calculateNbLignes lorsqu'il est appelé dans fillMosaic, pendant le resize.
				// nbLignes = calculateNbLignes();
				nbLignes = Math.max(nb_mos_total*loadMoreRes_multiplier ,nb_lignes_deft);
			}
		}else if($j("#resultats").hasClass("liste")){
			current_cnt = $j(" #resultats.liste tr:not(.separator, .resultsHead_row)").length;
			nbLignes = Math.max(nb_lignes_deft);
		}

		//feedback utilisateur : chargement & impossible de clicker à nouveau sur le bouton
		if(nbLignes > 0 ){
			$j("#loadMoreRes").addClass("disabled");

			$j.ajax({
				url : "empty.php?urlaction=docListe&where=include&from_offset="+current_cnt+"&nbLignes="+(nbLignes)+"&fromAjax=1&cherche=0",
				success: function(data){
					if($j("#resultats").hasClass("liste")){
						rows = $j(data).find('tr:not(.separator, .resultsHead_row)');
						$j("#resultats.liste table").append(rows);
					}else{
						$j("#resultats div.pusher").before(data);
					}
					// $j("#quick_search input#nbLignes").val(current_cnt+(nbLignes));
					// console.log("current count : "+current_cnt+" nbLignes "+nbLignes+", new val input#nbLignes "+$j("#quick_search input#nbLignes").val()+" total rows : "+totalNbResults);
					updateDraggables();
				},
				complete : function(){
					$j("#loadMoreRes").removeClass("disabled");

					// si pas de résultats, en mobile on ajoute une div d'informations a la liste de résultat
					if ($j('#resultats.mos .resultsMos').length === 0 && $j(window).width() < mobil_border) {
						$j("#resultats div.pusher").before('<div class="noResultCell">Aucun résultat. <br/>Cliquer sur "<a href="index.php?urlaction=docListe&amp;precherche=1">Tous les Documents</a>" pour réinitialiser la recherche.</div>');
					}
					if($j('#resultats.mos .resultsMos').length >= totalNbResults || $j('#resultats.liste tr:not(.separator , .resultsHead_row)').length >= totalNbResults ){
						$j("#loadMoreRes").css("display","none");
					}

					$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").unbind('mouseover');
					$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").unbind('mouseleave');
					$j("#resultats.mos .resultsMos .mos_checkbox input[type='checkbox']").unbind('change');
					$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").hover(callHoverPlayer);
					$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").mouseleave(hideHoverPlayer);
					$j("#main_block #resultats.mos .resultsMos .mos_checkbox input[type='checkbox']").change(function(){
						if($j(this).is(':checked')){
							$j(this).parents('.resultsMos').addClass('checked');
						}else{
							$j(this).parents('.resultsMos').removeClass('checked');
						}
					});
					try{
						if(typeof callback == 'function'){
							callback() ;
						}
					}catch(e){
						// console.log("crash loadMoreRes callback : "+e);
					}
					f_loading = false ;
				}
			});
			updateDraggables();
		}else{
			f_loading = false ;
		}
	}
}

function initAutoLoadMoreResults(){
	autoScrollHandler = function(event){
		if(!f_loading){
			if($j("#main_block").scrollTop()+$j("#main_block").height() >= $j("#main_block").get(0).scrollHeight *80/100 && loadMoreRes_counter%loadMoreRes_num_autoload  != (loadMoreRes_num_autoload - 1) ){
				$j("#loadMoreRes.mod_auto").addClass("auto_hidden");
				loadMoreResults(false,fillMosaic) ; 
				if($j('#resultats.mos .resultsMos').length >= totalNbResults || $j('#resultats.liste tr:not(.separator , .resultsHead_row)').length >= totalNbResults ){
					$j("#main_block").unbind('scroll',autoScrollHandler);
				}
			}else if (loadMoreRes_counter%loadMoreRes_num_autoload == (loadMoreRes_num_autoload - 1) ){
				$j("#loadMoreRes.mod_auto").removeClass("auto_hidden");
			}
		}
	}
	$j("#main_block").scroll(autoScrollHandler);
}

// fonction de centrage de la mosaique dans son container (pour corriger le comportement du float left)
/*function adjustMosaique(){
	$j("#resultats.mos, #panFrame .mosWrapper").css("margin-left","auto");
	w = $j("#main_block").width();
	if(w == null && $j("#panBlock div.mosWrapper").length>0){
		w = $j("#panBlock div.mosWrapper").width();
	}
	// MS - 16.06.15 - on ajoute manuellement la largeur du scroll qui apparait a droite du main_block pour ne pas fausser le calcul
	w = w-18;
	w = w%mos_unit_w;
	w = Math.floor(w/2);
	$j("#resultats.mos, #panFrame .mosWrapper").css("margin-left",w+"px");
	$j("#resultats.mos, #panFrame .mosWrapper").css("visibility","visible");
}	*/
function adjustMosaique(){

	w = $j("#main_block").width();
	w_p = $j("#panFrame").width();
	if(w == null && $j("#panBlock div.mosWrapper").length>0){
		w = $j("#panBlock div.mosWrapper").width();
	}

	// MS - 16.06.15 - on ajoute manuellement la largeur du scroll qui apparait a droite du main_block pour ne pas fausser le calcul
	// NB - 01.03.16 - sur mobile iOS, on enleve cette marge
	if (!navigator.userAgent.match(/(iPad|iPhone|iPod)/g)){
		w = w-18;
	}
	w = w%mos_unit_w;
	w = Math.floor(w/2);
	if (!navigator.userAgent.match(/(iPad|iPhone|iPod)/g)){
		w_p = w_p-18;
	}
	w_p = w_p%mos_unit_w;
	w_p = Math.floor(w_p/2);
	$j("#resultats.mos").css("margin-left",w+"px");
	$j("#panFrame .mosWrapper").css("margin-left",w_p+"px");
	$j("#panFrame .mosWrapper").css("margin-right",w_p+"px");

	if ($j(window).width() < tablet_border) { //pour affichage responsive de la liste de panier
		$j('#mainPan #panListe_container').css("margin-left",w_p+"px");
		$j('#mainPan #panListe_container').css("margin-right",w_p+"px");
	}

	$j("#resultats.mos, #panFrame .mosWrapper").css("visibility","visible");
}



$j(document).ready(function(){
	// fonctions de gestion de la mosaïque
	if($j("#resultats").hasClass("mos")){

		calculateNbLignes();
		if(!$j("#resultats").hasClass('paginee')){
			loadMoreResults(true,fillMosaic);
			// fillMosaic();
		}
		adjustMosaique();
		$j(window).resize(function(){
			calculateNbLignes();
			if(!$j("#resultats").hasClass('paginee')){
				fillMosaic();
			}
			adjustMosaique();
		});
		if($j("#main_block .resultsMos").length>0){
			$j("#main_block #resultats.mos .resultsMos .mos_checkbox input[type='checkbox']").change(function(){
				if($j(this).is(':checked')){
					$j(this).parents('.resultsMos').addClass('checked');
				}else{
					$j(this).parents('.resultsMos').removeClass('checked');

				}
			});
			// on n'appelle pas loadprevDoc si on va ouvrir le panier => on veut plutot loadprevdoc un élément du panier
			if(!$j("#panBlock").hasClass('open_on_load')){
				loadPrevDoc($j("#main_block  .resultsMos").eq(0).attr('id').replace('doc_',''),$j("#main_block .resultsMos").eq(0));
			}
		}
	}else{
		if($j("tr .resultsCorps input[type='checkbox']").length>0){
			// on n'appelle pas loadprevDoc si on va ouvrir le panier => on veut plutot loadprevdoc un élément du panier
			if(!$j("#panBlock").hasClass('open_on_load')){
				loadPrevDoc($j("tr .resultsCorps input[type='checkbox']").eq(0).val().replace('doc_',''),$j("tr .resultsCorps").eq(0).parent());
			}
			$j("#resultats.liste tr").each(function(idx,elt){
				if(!$j(elt).hasClass("separator") && !$j(elt).hasClass("resultsHead_row") ){
					if($j(elt).find(".resultsCorps input[type='checkbox']").length>0){
						$j(elt).click(function(){
							chck = $j(elt).find(".resultsCorps input[type='checkbox']");
							loadPrevDoc(chck.val().replace("doc_",''),elt);
						});
					}
				}
			});
		}
	}
	
	// MS mise en place redirection par doubleclick sur l'image : 
	// Ok pour recherche & categs, 
	// voir panier.xsl pour la gestion du panier
	// mosaique, 
	$j("#resultats.mos div.resultsMos").dblclick(function(evt){
		if($j(evt.target).parents('.resultsMos').find('a.consult').length >0){
			window.location.href = $j(evt.target).parents('.resultsMos').find('a.consult').attr('href');
		}
		
	});
	// liste, 
	$j("#resultats.liste div.resultsMosRow .bgVignetteMos").dblclick(function(evt){
		if($j(evt.target).parents('.resultsMosRow').find('a.consult').length >0){
			window.location.href = $j(evt.target).parents('.resultsMosRow').find('a.consult').attr('href');
		}
	});
	
	// MS on désactive les éléments de la recherche avancée si ils sont cachés
	$j("#adv_search_wrapper.collapsed :input").attr("disabled",true);

	// déploiement de la facette correspondant au dernier refine effectué, si aucun refine on déploie le premier groupe de facette
	if($j("#menuGauche:not(.collapsed) .type_facet").length>0){
		if(typeof obj_refine_solr == 'object' && obj_refine_solr.length>0){
			last_refine = obj_refine_solr[obj_refine_solr.length-1];
			if(typeof last_refine.type != 'undefined'){
				if(last_refine.type.indexOf('hier_') === 0 ){
					$j("#menuGauche .type_facet."+last_refine.type).eq(0).click();
					if(last_refine.value.indexOf('0') === 0 ){
					   $j("#menuGauche .block_facet."+last_refine.type).find(".facet.facet_hier[onclick*='"+last_refine.value.split("'").join("\\\\'")+"']").find(".hier_unfolder").click();
					} else {
						$j("#menuGauche .block_facet."+last_refine.type).find(".facet.facet_hier[onclick*='"+last_refine.value.split("'").join("\\\\'")+"']").parents("div[class^='facet_hier_lvl_']").each(function(idx,elt){
							$j(elt).prev(".facet.facet_hier").find(".hier_unfolder").click();
						});
					}

				}else{
					$j("#menuGauche .type_facet."+last_refine.type).eq(0).click();
				}
			}
		}else{
			setTimeout(function(){$j("#menuGauche .type_facet").eq(0).click();},200);
		}
	}

});

// le changement de la présentation change aussi le nombre de ligne affichée
function presentation_updateLignes(layout_opts) {
	if(layout_opts == 'panier' || layout_opts == 'panier2'){
		$j('#stylePanier').val(layout_opts);
	}
	else
	{
		
		updateLayoutOpts(layout_opts,function(){
			pagerForm.page.value = '1';
			if( !$j("#resultats").hasClass('paginee')){ 
				pagerForm.nbLignes.value = nb_lignes_deft;
			}
			pagerForm.submit();			
		});
		// Si on va vers la mosaique classique (non paginée) ou qu'on en vient,
		// on reset page à 1 & le nombre de ligne au nombre par défaut (devrait etre  50 )

	}
}


function loadPrevDoc(id_doc,elt,rang){
	if($j("#menuDroite").length>0){
		params = {};
		if(typeof elt == "undefined"){
			elt = null ;
			type =null;
		}else{
			$j("#resultats *.selected,#panFrame *.selected").removeClass("selected");
			$j(elt).addClass("selected");

			if($j(elt).parents('#panBlock').length>0){
				type = "pdoc";
			}else{
				type = 'doc';
			}

			if(type == 'pdoc'){
				if($j(elt).hasClass('resultsMos') && $j(elt).attr('id')){
					id_ligne_panier = $j(elt).attr('id').split('_')[1];
				}else{
					id_ligne_panier = $j(elt).find("input[type='checkbox']").val();
				}
				params.id_ligne_panier = id_ligne_panier;
				params.id_panier = id_current_panier;
			}
		}
		
		if((typeof rang == 'undefined' || rang== null ) && typeof $j(elt).find("a[href*='id_doc="+id_doc+"&rang=']").attr('href') == 'string' ){
			rang = $j(elt).find("a[href*='id_doc="+id_doc+"']").attr('href').split('&rang=') ;
			rang = parseInt(rang[1],10);
		}
		params.rang = rang;
		//feedback utilisateur du chargement
		$j("#menuDroite").addClass("loading");
		
		// cleanup de l'éventuel player preview right précédent
		try{
			if(typeof myVideo_prevDoc != 'undefined' && typeof myVideo_prevDoc.destroy == 'function'){
				myVideo_prevDoc.destroy();
				myVideo_prevDoc=null ;
			}else if ($j("#menuDroite video#player_html_simple").length>0){
				$j("menuDroite video#player_html_simple").get(0).pause();
				$j("menuDroite video#player_html_simple").get(0).src = "" ;
				$j("menuDroite video#player_html_simple").get(0).load();
			}
		}catch(e){}
		$j.ajax({
			url : "empty.php?urlaction=loadDoc&id_doc="+id_doc+"&xsl=prevDocRight&params="+JSON.stringify(params),
			success : function(data){
				// console.log("data",data);
				if($j(data).is('#menuDroite')){
					$j("#menuDroite").html($j(data).html());
				}
				
				var show_preview = function(e){
					try{
						if(typeof e != 'undefined' && e != null && typeof e.type !='undefined' && e.type == 'error'){
							$j("#prevMedia img.prev_vignette").css('display','none');
						}
					}catch(err){
						// console.log(err);
					}
					setTimeout(function(){
						$j("#prevMedia").css('min-height',$j('#prevMedia').height()+"px");
						$j("#prevWrapper").css('top',($j("#previewDoc").outerHeight()+26)+"px");
						if(typeof OW_env_user != 'undefined' && (OW_env_user.env_OS == 'Android' || OW_env_user.env_OS == 'iPad' || OW_env_user.env_OS == 'iPhone')){
							$j("#prevMedia img.prev_vignette").click();
						}
						$j("#menuDroite").removeClass("loading");
					}, 10);
				};

				if($j("#prevMedia img.prev_vignette").length > 0){
					$j("#prevMedia img.prev_vignette").on('load',show_preview);
					$j("#prevMedia img.prev_vignette").on('error',show_preview);
				}else{
					show_preview() ;
				}

			}
		});
	}
}

function smartCheckAll(elt_tool,container){
	if(!$j(elt_tool).hasClass('checked')){
		$j(elt_tool).addClass('checked');
		$j(container).find('input[type="checkbox"]').prop('checked',true);
	}else{
		$j(elt_tool).removeClass('checked');
		$j(container).find('input[type="checkbox"]').prop('checked',false);
	}
	$j(container).find('input[type="checkbox"]').trigger('change');
}



/***************************** preview player **************************/


var trigger_preview = null ;
var current_target_preview = null ; 

function callHoverPlayer(event){
	if(sessLayout.prev_pop_in){ //  && !showRightMenu // MS - 09.09.15 - dans cette version, le preview rollhover est indépendant de l'ouverture du menu latéral droit
		// console.log(event);
		id = $j(event.currentTarget).find('input[type="checkbox"]').val();
		current_target_preview = event.currentTarget ; 
		if(!($j("#prevMedia").hasClass('id_'+id))){
			if($j(event.currentTarget).attr('id')){
				type = $j(event.currentTarget).attr('id').replace('_'+id,'');
			}else{
				if($j(event.currentTarget).find('input[name^="t_panier_doc"]').length>0){
					type='pdoc';
				}else{
					type='doc';
				}
			}
			if(trigger_preview !=null ){
				clearTimeout(trigger_preview);
				trigger_preview = null ;
			}

			trigger_preview = setTimeout(function(){
				setPositionPrevPlayer(event);
				loadHoverPlayer(id,'previewHoverWrapper',type);
			},400);
		}
	}
}



function setPositionPrevPlayer(evt){
	if(typeof evt.currentTarget!='undefined'){
		t = evt.currentTarget;
		modifs = {};

		if($j(t).hasClass('resultsMos')){
			o = $j(t).offset();
			if(o.left-410<0){
				modifs.left = o.left + $j(t).width() +10;
			}else{
				modifs.left = o.left-410;
			}
			if(o.top+302>$j(window).height()){
				var class_to_add = "fix_to_bot";
				modifs.top = o.top - ($j(t).height() +25);
			}else{
				modifs.top = o.top;
			}


		}else{
			o = $j(t).offset();
			if((o.top + $j(t).height() + 300 )> $j(window).height()){
				modifs.top = o.top - 300
			}else{
				modifs.top =o.top + $j(t).height() + 20 ;
			}

			modifs.left = o.left;

		}
		$j("#previewHoverWrapper").css(modifs);
		$j("#previewHoverWrapper").get(0).className = "" ;
		if(typeof class_to_add != 'undefined'){
			$j("#previewHoverWrapper").addClass(class_to_add);
		}
	}
}

function loadHoverPlayer(id, target,type){
	if(typeof player_options != 'undefined'){
		delete player_options['image'];
		delete player_options['width'];
		delete player_options['height'];
		delete player_options['url'];
		delete player_options['controls'];
	}
	// console.log("call loadHoverPlayer "+target+"- player_options");
	// console.log((typeof player_options != 'undefined')?player_options:null);


	if(typeof target == 'undefined' || target == null){
		target = 'previewHoverWrapper';
	}
	if((typeof document.hasFocus == 'undefined' || document.hasFocus()) && !$j('#'+target).hasClass("id_"+id)){
		trigger_preview = null ;
		$j.ajax({
			url : 'previewPlayer.php?id='+id+"&type="+type+"&target="+target,
			success : function(data){
				if(target == 'previewHoverWrapper'){
					if($j("#"+$j(current_target_preview).attr('id')+":hover").length>0){
						try{
							$j('#previewHoverWrapper').css('visibility','hidden');
							$j('#previewHoverWrapper').css('display','list-item');
							$j('#previewContainer').html(data);
							$j('#previewHoverWrapper').css('min-height',$j("#previewContainer").get(0).offsetHeight);
							$j('#previewHoverWrapper').css('visibility','visible');

							$j('#previewHoverWrapper').addClass('id_'+id);
						}catch(e){
							//console.log('crash '+e);
						}
					}
				}else if (target == 'prevMedia'){
					try{
						$j('#prevMedia').html(data);
						if($j("#prevMedia").find("#media_space").length>0 && ($j("#prevMedia").find("#media_space").hasClass("video") || $j("#prevMedia").find("#media_space").hasClass("audio"))){
							// cas video /audio
							$j("#prevMedia").css("min-height",(parseInt($j("#prevMedia").css("min-height"),10)+60));
							$j(window).trigger('resize');
						}
						$j('#prevMedia').find('img.prev_vignette').css('display','none');
						$j('#prevMedia').addClass('id_'+id);
					}catch(e){
						//console.log('crash '+e);
					}
				}
			},
			complete : function(data){
				// console.log('complete tst');
				// console.log(data);
			}
		});
	}
}

function hideHoverPlayer(){
	if(sessLayout.prev_pop_in){ //  && !showRightMenu // MS - 09.09.15 - dans cette version, le preview rollhover est indépendant de l'ouverture du menu latéral droit
		if($j("#previewHoverWrapper").length>0){
			$j("#previewHoverWrapper").get(0).className='';
			$j('#previewHoverWrapper').css('display','none');
		}
		if(typeof myVideo != 'undefined' && myVideo!=null){
			if(typeof myVideo.destroy == 'function' ){
				myVideo.destroy() ;
			}
			myVideo = null ;
		}else if (typeof player != 'undefined' && player!=null){
			player = null ;
		}else if (typeof my_player != 'undefined' && my_player!=null){
			if(my_player.thread_affichage){
				my_player.thread_affichage.stop();
			}
			my_player = null ;
		}else if ($j("#previewHoverWrapper").find("#player_html_simple").length>0) {
			try{
				media_elmt = $j("#previewHoverWrapper").find("#player_html_simple").get(0);
				media_elmt.pause();
				media_elmt.src="";
				media_elmt.load();
			}catch(e){
				console.log("crash destroy player simple :"+e);
			}
		}

		$j("#previewContainer").html("");

		if(trigger_preview !=null ){
			clearTimeout(trigger_preview);
			trigger_preview = null ;
		}
	}
}
function checkLayoutRedirect(url){
	if(typeof current_layout!="undefined" && current_layout<= 1 && ($j(window).width() > tablet_border)) {
		window.location.href=url;
	}else{
		return false ;
	}
}



function saveAndGo(url) {
	var form1 = document.getElementById("form1");
	if (!form1) form1 = document.getElementsByName("form1")[0];
	// Le formulaire doit avoir un champ save_action qui détermine le type de sauvegarde
	// à effectuer (ex : doc, sequence, matériel, etc...)
	if (!form1.save_action) commande="SAVE";
	else commande=form1.save_action.value;
    form1.commande.value=commande;
    form1.page.value=url;
    form1.submit();
}

function removeItem(url, form) {
	if (typeof(form)=='undefined') {
		form=document.getElementsByName("form1")[0];
	}
    if (confirm(str_lang.kConfirmerSuppression)) {
    	form.commande.value="SUP";
    	form.page.value=url;
        form.submit();
    }
}

function beginDownload(url){
	$j("#frameDownload").attr('src',url);
}


function displayMenuActions(action,type,id_item,field_ref_xml,context){
	if(!context) context='';
	$j("#popupModal,#frameModal").removeAttr('class');
	if(action == 'export'){
		url_export = 'export.php?';
		// report du type dans l'url
		if(typeof type !='undefined' && type != 'doc'){
			url_export+="type="+type;
		}else{
			type = "doc";
			url_export+="type="+type;
		}

		if(type == 'panierdoc'){
			nb_pan_rows = $j("#panFrame .resultsMos, #panFrame table.tableResults tr:not(.separator,.resultsHead_row)").length;
			url_export+="&nbLignesTotal="+nb_pan_rows;
		}

		if(typeof id_item != 'undefined'){
			// export d'un item en particulier
			url_export+="&id="+id_item+"&exportItem=true";
		}else{
			// export depuis une requete sql
			if(typeof type !='undefined'){
				switch(type){
					case 'doc':
						url_export +="&sql=recherche_DOC_Solr";
						break;
					case 'stat':
						url_export +="&sql=stat_sql";
						break;
					default:
						url_export +="&sql=recherche_"+type.toUpperCase();
						break;
				}
			}
		}

		// passage du chemin du fichier fieldRefXML que l'on veut utiliser (par ex : designDir$/print/xml/fieldRef_doc.xml)
		if(typeof field_ref_xml !='undefined'){
			url_export+="&fieldRefXML="+encodeURIComponent(field_ref_xml);
		}

		// on précise à l'export qu'on vient d'ajax, on a pas besoin de charger du code inutile (doctype, html, body, page.css etc ... )
		url_export+="&fromAjax=1";

		$j("#popupModal #frameModal").addClass("frame_menu_actions");
		//MS - pour être plus résponsif lorsqu'on change le nombre d'éléments de l'export, on recharge quoi qu'il arrive le popup
		//if(true || $j("#frameModal.frame_menu_actions #frameExport").length==0 || $j("#formExport #type").val() != type ){
			$j.ajax({
				url : url_export,
				success : function(data){
					$j("#frameModal.frame_menu_actions").html(data);
					$j("#popupModal").css('display','block');
					$j("#formExport ul.type_export input[type='radio']:checked").trigger("change");
					if(typeof refreshSelectedItemsForExport == 'function'){
						refreshSelectedItemsForExport($j("#formExport #type").val());
					}
				}
			});
		
		
	} else if (action == "partage"){
		if(typeof type == "undefined" || typeof id_item == "undefined"){
			return false ;
		}
		if(type == "panier"){
			xsl_share = "sharePanier";
		}else{
			xsl_share = "shareDoc";
		}
		
		
		url_partage = "empty.php?urlaction=dialog&form=menuPartage&id_item="+id_item+"&type_partage="+type+"&fromAjax=1&xsl="+xsl_share;
		$j("#popupModal #frameModal").addClass("frame_menu_actions");
		
		$j.ajax({
			url : url_partage,
			success : function(data){
				$j("#frameModal.frame_menu_actions").html(data);
				$j("#popupModal").css('display','block');
			}
		});
				
	}else if(action == 'sauver_recherche' ||action== 'add2cart' || action =='deplacer' ||action=='copier' || action=='transfert'){
		$j("#popupModal #frameModal").addClass("frame_menu_actions");
		if(action == 'sauver_recherche')
			url = 'empty.php?urlaction=dialog&form=action_panier&action=save';
		else if (action == 'add2cart')
			url = 'empty.php?urlaction=dialog&form=action_panier&action=add2cart';		
		else if (action == 'deplacer')
			url = 'empty.php?urlaction=dialog&form=action_panier&action=deplacer';		
		else if (action == 'copier')
			url = 'empty.php?urlaction=dialog&form=action_panier&action=copier';
		else if (action == 'transfert')
			url = 'empty.php?urlaction=dialog&form=action_panier&action=transfert';
		
		if(typeof id_current_panier !='undefined'){
			url = url + "&id_current_panier="+id_current_panier;
		}
		$j.ajax({
			url : url,
			success : function(data){
				$j("#frameModal.frame_menu_actions").html(data);
				$j("#popupModal").css('display','block');
			}
		});
	}else if (action == 'download'){
		$j("#popupModal #frameModal").addClass("frame_menu_actions");
		url = 'empty.php?urlaction=dialog&form=action_commande&action=startDownload';
		if(typeof type != 'undefined'){
			// type d'entité
			url+="&type="+type;
		}
		if(typeof id_item != 'undefined'){
			// export d'un item en particulier
			url+="&id="+id_item;
		}
		$j.ajax({
			url : url,
			success : function(data){
				$j("#frameModal.frame_menu_actions").html(data);
				$j("#popupModal").css('display','block');
				refreshSelectedItems('#small_order_form',type);
			}
		});
	}
	else if(action=='modifLot' || action=='saisieLot' ){
		url_modif_lot = 'modifLot.php?action=' + action;
		if (type!= ''){
			url_modif_lot=url_modif_lot + "&entite=" + type;	
		}
		else{
			url_modif_lot=url_modif_lot + "&entite=doc";	
		}

		var ids = '';
		var selector='';

		if( type == 'panier_doc'){
			selector = "form#formPanier";
		} else if( context=='importView' ) {
			selector = "form#import_aff_form";
		}else {
			selector = "#main_block form[name='documentSelection']";
		}
		
		$j(selector+" input[name^='checkbox']:checked").each(function () {
			if (ids != '')
				ids = ids+',';
			ids = ids + $j(this).val();
		});
		if (ids != ''){
			url_modif_lot=	url_modif_lot + "&tIds=" + ids;
		}		
		//id_item = id_panier
		if(typeof id_item != 'undefined'){
			if (type == 'panier_doc')
				url_modif_lot+="&id_panier="+id_item;
			else if( context=='importView' )
				url_modif_lot+="&id_import="+id_item;
		}
		wName='';
		orgName='';

		$j("#popupModal").css('display','block');
		$j("#popupModal #frameModal").addClass("frame_menu_actions");
			$j.ajax({
				url : url_modif_lot,
				success : function(data){
					$j("#frameModal").html(data);
					$j("#popupModal").css('display','block');
					$j("#chooser").css('z-index','300');
					$j("#frameModal").css('width','100%');
					$j('#saisieForm').attr('action',url_modif_lot);
					

				}
			});
	}
}
function refreshSelectedItems(formSelector,type_entite){
		str_selection = str_lang.kSelection;
		nb_selection = 0 ; 
		list_ids = "";
		query_str_checkbox = "";
		if(type_entite == "doc"){
			query_str_checkbox = "#resultats .resultsCorps > input[type='checkbox'],#resultats .resultsMos  .mos_checkbox input[type='checkbox']";
		}else if (type_entite == "panierdoc"){
			query_str_checkbox = "#panFrame #formPanier .resultsCorps > input[type='checkbox'],#panFrame #formPanier .resultsMos  .mos_checkbox input[type='checkbox']";
		}
		$j(query_str_checkbox).each(function(idx,elt){
			if($j(elt).get(0).checked){
				if(list_ids != ''){
					list_ids+=",";
				}
				list_ids += $j(elt).val();
				nb_selection++;
			}
		});

		
		if(nb_selection>0){
			$j(formSelector+" select option.selection").attr('data-nb-items',nb_selection) ;
			$j(formSelector+" select option.selection").html(str_selection+" ("+nb_selection+")") ;
			$j(formSelector+" select option.selection").removeAttr("disabled");
			if($j(formSelector+" select[name='select_cible']").length>0){
				$j(formSelector+" select[name='select_cible']").get(0).selectedIndex=0;
			}
			$j(formSelector+" #selection_ids").val(list_ids);
		}else{
			$j(formSelector+" select option.selection").attr('data-nb-items',nb_selection) ;
			$j(formSelector+" select option.selection").html(str_selection) ;
			$j(formSelector+" select option.selection").attr("disabled","true");
			if($j(formSelector+" select[name='select_cible']").length>0){
				$j(formSelector+" select[name='select_cible']").get(0).selectedIndex=1;
			}
			$j(formSelector+" #selection_ids").val("");

		}
		
		if(list_ids != ''){
			return true ; 
		}else{
			return false ; 
		}
	}
	
	
// fonction qui initialize les animations de gestion de l'espace pour le fil d'ariane
function animateBreadcrumb(){
	// pour chaque lien breadcrumb,
	$j("#header_breadcrumb a").each(function(idx,elt){
		// sur le span qui contient la fleche, lorsqu'on mouseover :
		$j(elt).prev("span.breadcrumb_arrow").on('mouseover', function(){
			// on vérifie que le lien n'est pas déja déplié, et qu'on a pas d'autres animations en cours
			if($j(this).next("a").hasClass('wrapped') && $j("#header_breadcrumb a.wrapping,#header_breadcrumb a.unwrapping").length == 0  ){
				// récupération des tailles des divers éléments impactés par ces animations
				width_accueil = $j("#header_breadcrumb #accueil_link").get(0).offsetWidth;
				width_sep_span = $j("#header_breadcrumb span.breadcrumb_arrow").outerWidth(true) ;
				width_elem_unwrapped = width_accueil+$j("#header_breadcrumb a:not(#accueil_link)").length * width_sep_span;
				width_viewport= $j("#header_breadcrumb").width();

				$j("#header_breadcrumb a:not(#accueil_link,.wrapped,.wrapping) ").each(function(idx,elmt){
					width_elem_unwrapped += Math.min(width_viewport/2,$j(elmt).get(0).scrollWidth);
				});

				width_elem = Math.min(width_viewport/2,$j(this).next("a").get(0).scrollWidth);
				// on déplie la partie du fil d'ariane associée au hovered span
				unwrapBreadcrumb($j(this).next("a"));
				width_elem_unwrapped += width_elem;
				// si on détecte qu'avec l'élément déplié on a trop d'éléments et le fil d'ariane dépasse du viewport,
				while(width_elem_unwrapped  > width_viewport){
					// alors on replie autant d'éléments du fil d'ariane que necessaire pour faire de la place à l'élément qu'on est en train de déplier
					width_elem_unwrapped -= $j("#header_breadcrumb a:not(#accueil_link,.wrapped,.wrapping,.unwrapping)").get(0).scrollWidth;
					wrapBreadcrumb($j("#header_breadcrumb a:not(#accueil_link,.wrapped,.wrapping,.unwrapping)").eq(0));
				}
			}
		});
	});

	// on associe l'évenement resize de la window avec la fonction resizeBreadcrumb
	$j(window).resize(resizeBreadcrumb);
	// on appelle une fois resizeBreadcrumb pour initialiser l'affichage courant
	resizeBreadcrumb();
}

// fonction qui gère les wrap / unwrap des éléments du fil d'ariane lorsqu'on resize la fenetre
function resizeBreadcrumb(){
	// récupération des tailles des divers éléments impactés par ces animations
	width_accueil = $j("#header_breadcrumb #accueil_link").get(0).offsetWidth;
	width_sep_span = $j("#header_breadcrumb span.breadcrumb_arrow").outerWidth(true) ;
	width_elem_unwrapped = width_accueil+$j("#header_breadcrumb a:not(#accueil_link)").length * width_sep_span;
	width_viewport= $j("#header_breadcrumb").width();

	$j("#header_breadcrumb a:not(#accueil_link,.wrapped,.wrapping) ").each(function(idx,elt){
		width_elem_unwrapped += Math.min(width_viewport/2,$j(elt).get(0).scrollWidth);
	});
	// si les éléments déployés dépassent du viewport
	if(width_elem_unwrapped > width_viewport){
		// on appelle wrap sur le premier qu'on trouve qui n'est pas déja plié,
		wrapBreadcrumb($j("#header_breadcrumb a:not(#accueil_link,.wrapped,.wrapping)").eq(0));
		// on rappelle resizeBreadcrumb, avec un timeout pour s'assurer que le prochain appel se réalise bien APRES les animations (pb pour récupérer les tailles réelles des éléments sinon)
		setTimeout(function(){resizeBreadcrumb();},150);
	}else{
	// sinon, on vérifie l'espace disponible à droite du fil d'ariane
		available_space = width_viewport-width_elem_unwrapped;
		if($j("#header_breadcrumb a.wrapped").length>0 && available_space > Math.min(width_viewport/2,$j("#header_breadcrumb a.wrapped").last().get(0).scrollWidth)){
			// si l'espace disponible peut accueillir le dernier élément wrapped du fil d'ariane, alors on le déplie
			unwrapBreadcrumb($j("#header_breadcrumb a.wrapped").last());
			// on rappelle resizeBreadcrumb, avec un timeout pour s'assurer que le prochain appel se réalise bien APRES les animations (pb pour récupérer les tailles réelles des éléments sinon)
			setTimeout(function(){resizeBreadcrumb();},150);
		}
	}
}

// fonction qui gère l'animation de dépliage d'un élément du fil d'ariane
function unwrapBreadcrumb(elt){
	width_viewport= $j("#header_breadcrumb").width();
	$j(elt).css('width','0px');
	$j(elt).removeClass('wrapped').addClass('unwrapping');
	$j(elt).animate({
		// => le Math.min est présent ici pour avoir une limite max à la taille d'1 élément du fil d'ariane
		width : Math.min(width_viewport/2,$j(elt).get(0).scrollWidth)+"px"
	},{
		queue :false,
		duration : 100,
		easing : 'linear',
		complete : function(){
			$j(elt).removeClass('unwrapping');
		}
	});
}
// fonction qui gère l'animation de repli d'un élément du fil d'ariane
function wrapBreadcrumb(elt){
	$j(elt).eq(0).addClass('wrapping');
	$j(elt).eq(0).animate({
		width : "0px",
	},{
		queue :false,
		duration : 100,
		easing : 'linear',
		complete : function(){
			$j(elt).removeClass('wrapping').addClass('wrapped');
		}
	});
}



function triggerPrint(target){
	$j(".target_print").removeClass("target_print");

	switch (target){
		case 'panier':
		case 'panierdoc':
			$j("#panBlock").addClass("target_print");
		break ;
		default :
			$j("div.contenu").addClass("target_print");
		break ;
	}
	window.focus();
	window.print();
	$j("#poster").show();
	$j("#control").show();
	$j("#poster_print").css('visibility', 'hidden');
}


function hideMenuActions(){
	if($j(".frame_menu_actions").attr('id') == 'frameModal' ){
		$j("#frameModal.frame_menu_actions #frameExport").attr('src','');
		$j("#frameModal.frame_menu_actions #frameExport").load();
		// $j("#frameModal.frame_menu_actions").css('display','none');
		$j("#popupModal").css('display','none');
		if(typeof hideCustomExport == 'function'){
			hideCustomExport();
		}
	}else{
		window.close();
	}
}

// fonction pour affichage conditions générales d'utilisation dans un popin (frameModal/popupModal)
function showCGU(anchor){
	if( typeof anchor != 'undefined'){
		anchor = "#"+anchor;
	}else{
		anchor = "";
	}

	$j.ajax({
		url : 'empty.php?html=3'+anchor,
		success : function(data){
			$j("#popupModal,#frameModal").removeAttr('class');
			button_close  = '<span class="html_close" onclick="closePopup(true);"></span>';
			page = button_close+"<div class=\'html_page\'>"+data+"</div>";
			$j("#frameModal").html(page);
			$j("#frameModal").addClass('cgu');
			$j("#popupModal").css('display','block');

		}
	});
}

