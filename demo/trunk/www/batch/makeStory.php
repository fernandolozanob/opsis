﻿<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- Développeurs : Vincent Prost, François Duran, Jérome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
// MISE A JOUR DES SEQUENCES ET SUJETS : DUREE, FONDS, IMAGEUR, IMAGE REPRÉSENTATIVE
/*-----------------------------------------------------------------------------------*/
// VP : 13/12/2005 : Création du fichier


     require_once("../conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php"); 	

	require_once(modelDir.'model_imageur.php');
	require_once(modelDir.'model_materiel.php');
	require_once(modelDir.'model_doc.php');

	global $db;

    print 'DEBUT - MakeStory<br/>';
    print 'DEBUT - MakeStory - check MP4<br/>';
//MP4
    $rows=$db->GetAll("SELECT DISTINCT ID_MAT,MAT_NOM,MAT_RATIO,MAT_CHEMIN FROM t_mat where MAT_NOM like '%.mp4' and (MAT_IMAGEUR='' or MAT_IMAGEUR is null) order by ID_MAT");
	$dimensions=array('','');
    foreach($rows as $row) {
        if (!empty($row['MAT_CHEMIN'])) {
            $filename = kVideosDir.$row['MAT_CHEMIN'].'/'.$row['MAT_NOM'];
        } else {
            $filename = kVideosDir.$row['MAT_NOM'];
        }
	if(is_file($filename)){
			print $row['ID_MAT']."<br/>\n";
			$myImageur=new Imageur;
			$myMat=new Materiel;
			$myDoc=new Doc;
			$myMat->t_mat['ID_MAT']=$row['ID_MAT'];
			$myMat->getMat();
			$myImageur->t_imageur['IMAGEUR']=stripExtension($myMat->t_mat['ID_MAT']);
			if($myImageur->generateStoryboard($myMat,30,$imgFromStoryboard,$dimensions[0],$dimensions[1])){
				$myMat->getDocMat();
				foreach ($myMat->t_doc_mat as $idx=>$dm) {
					$myDoc->t_doc['ID_DOC']=$dm['ID_DOC'];
					$myDoc->saveVignette('doc_id_image',$imgFromStoryboard);
				}
			}else print $myImageur->error_msg."<br/>\n";
			unset($myDoc);
			unset($myMat);
			unset($myImageur);
		}else print $row['ID_MAT']." manquant<br/>\n";
	}


    print 'DEBUT - MakeStory - check JPG<br/>';
//JPG
$rows=$db->GetAll("SELECT DISTINCT ID_MAT,MAT_NOM,MAT_RATIO,MAT_CHEMIN FROM t_mat where MAT_NOM like '%.jpg' and (MAT_IMAGEUR='' or MAT_IMAGEUR is null) order by ID_MAT");
$dimensions=array('800','');
foreach($rows as $row) {
        if (!empty($row['MAT_CHEMIN'])) {
            $filename = kVideosDir.$row['MAT_CHEMIN'].'/'.$row['MAT_NOM'];
        } else {
            $filename = kVideosDir.$row['MAT_NOM'];
        }
	if(is_file($filename)){
		print $row['ID_MAT']."<br/>\n";
		$myImageur=new Imageur;
		$myMat=new Materiel;
		$myDoc=new Doc;
		$myMat->t_mat['ID_MAT']=$row['ID_MAT'];
		$myMat->getMat();
		$myImageur->t_imageur['IMAGEUR']=stripExtension($myMat->t_mat['ID_MAT']);
		if($myImageur->generateThumbnail($myMat,$imgFromStoryboard,$dimensions[0],$dimensions[1])){
			$myMat->getDocMat();
			foreach ($myMat->t_doc_mat as $idx=>$dm) {
				$myDoc->t_doc['ID_DOC']=$dm['ID_DOC'];
				$myDoc->saveVignette('doc_id_image',$imgFromStoryboard);
			}
		}else print $myImageur->error_msg."<br/>\n";
		unset($myDoc);
		unset($myMat);
		unset($myImageur);
	}else print $row['ID_MAT']." manquant<br/>\n";
}


    print 'DEBUT - MakeStory - check PDF<br/>';
//PDF
$rows=$db->GetAll("SELECT DISTINCT ID_MAT,MAT_NOM,MAT_RATIO,MAT_CHEMIN FROM t_mat where MAT_NOM like '%.pdf' and (MAT_IMAGEUR='' or MAT_IMAGEUR is null) order by ID_MAT");
$dimensions=array('800','');
foreach($rows as $row) {
        if (!empty($row['MAT_CHEMIN'])) {
            $filename = kVideosDir.$row['MAT_CHEMIN'].'/'.$row['MAT_NOM'];
        } else {
            $filename = kVideosDir.$row['MAT_NOM'];
        }
	if(is_file($filename)){
		print $row['ID_MAT']."<br/>\n";
		$myImageur=new Imageur;
		$myMat=new Materiel;
		$myDoc=new Doc;
		$myMat->t_mat['ID_MAT']=$row['ID_MAT'];
		$myMat->getMat();
		$myImageur->t_imageur['IMAGEUR']=stripExtension($myMat->t_mat['ID_MAT']);
		if($myImageur->generateStoryBoardFromBureautique($myMat,$imgFromStoryboard,$dimensions[0],$dimensions[1])){
			$myMat->getDocMat();
			foreach ($myMat->t_doc_mat as $idx=>$dm) {
				$myDoc->t_doc['ID_DOC']=$dm['ID_DOC'];
				$myDoc->saveVignette('doc_id_image',$imgFromStoryboard);
			}
		}else print $myImageur->error_msg."<br/>\n";
		unset($myDoc);
		unset($myMat);
		unset($myImageur);
	}else print $row['ID_MAT']." manquant<br/>\n";
}


    print 'FIN - MakeStory<br/>';
?>
