﻿<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- Développeurs : Vincent Prost, François Duran, Jérome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

// majMatLieu.php : Page de mise a jour du champ doc_lex de t_doc
// Appel en ligne de commande : php majDocLex.php

// VP : 16/12/2005 : création de la valeur 'online' dans t_lieu si non existante
// XS : 20/09/2005 : Ajout de la mise à jour du champs DOC_NUM de t_doc
// VP : 4/7/2005 : creation

     require_once("../conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php"); 	

	global $db;

print "Debut du traitement <BR/>";

// I.3.Initialisation du statut DOC_NUM
	$sql="update t_doc set DOC_NUM ='0'";
    $result=$db->Execute($sql);

// II. Mise a jour du statut DOC_NUM
    $dh  = opendir(kVideosDir);
    $k=-2;
    while (false !== ($filename = readdir($dh))) 
    {
        print $filename."<BR/>";
        $k+=1;
        
        // II.1. Mise a jour du statut DOC_NUM
        $sql="update t_doc,t_doc_mat set t_doc.DOC_NUM ='1' where t_doc.ID_DOC=t_doc_mat.ID_DOC and t_doc_mat.ID_MAT='$filename'";
        $result=$db->Execute($sql);
    }
    
print "<br/>Fin du traitement pour ".$k." fichiers<br/>";

?>
