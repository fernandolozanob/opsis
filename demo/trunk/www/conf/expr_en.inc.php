<?
// Expressions spécifiques à la démo
define("kMsgCommandeAchat","Please fill in this order form by indicating the type of material you want to receive.<br/>If you choose online payment, the test page of our partner PAYBOX will be displayed.<br/>
<strong>This order is for demonstration only : no fees or real shipment will be proceded.</strong>");
define("kMsgCommandeAchatRecap","<strong>This order is for demonstration only : no fees or real shipment will be proceded.</strong>");
define("kMsgCommandeTelecharger","The validation of this order gives you access to a download page of the element with the MPEG-2 format.<br/>
On this demonstration, download is fully operational, legal and free.");
define("kMsgCommandeTelechargerRecap","You can download each element independently,
<strong>by clicking on the floppy</strong> of each line.");
define("kInscriptionEntete","Please fill in the subscription below.<br/>Your personal informations will not be stored, because the databased is reset every night.");
define("kMsgEntete","Opsis Media, an Internet software suite <br/>designed for audiovisual archive management, communication and marketing");

// HTML5
define("kMsgTryHTML5","Try our new HTML5 video player");

//Page d'accueil
define("kEveryDocV","Every videos");
define("kEveryDocA","Every sounds");
define("kEveryDocP","Every pictures");
define("kEveryDocD","Every documents");
define("kRetourListeRecherche","Back to search list");

define("kVideos","Videos");
define("kImages","Pictures");
define("kAudios","Audios");
define("kDocuments","Documents");
define("kReportages","Reports");


//formulaire de recherche
define("kDatePublication","Upload date");

// Administration
define("kConfirmViderpanier",'Do you confirm delete ?');

define("kParametreEntete",'Configure header');
define("kParametres",'Settings');

define('kErrorGroupeSauveUsager','Error while adding user to group');
define('kErrorGroupeSauveFonds','Error while adding collection to group');


define("kAjouterDossierDoc","Add to folder ...");

define('kUrl','URL');

define('kPartager','Share');
define('kReseauxSociaux','Social network');
define('kFacebook','Facebook');
define('kTwitter','Twitter');
define('kLinkedIn','Linked in');
define('titre_trop_long_twitter','Title is too long for Twitter');
define('titre_trop_long_linkedin','Title is too long for Linked in');


// site mobile
define('kVoulezVousEtreRedirigeMobile','Do you want to be redirect to mobile website ?');

// Montage
define("kAjouterAuMontage","add to editing");
define("kAtelierMontage","Editing workshop");
define("kVoirDemo","See the demo");
define("kChutier","bin");
define("kGlissezIci","Drop here");
define("kUnPlanDeMesSelections","one shot of « my folders »");
define("kAfficherAutresPlans","display other shots");
define("kCliquezGlissez","drag and drop");
define("kPourChangerOrdrePlans","to change the order of shots");
define("kMontage","editing");
define("kVisionneuse","viewing");
define("kCreerNouveau","create new");
define("kSansTitre","untitled");
define("kJouerClip","play clip");
define("kJouerMontage","play timeline");
define("kSupprimerDuMontage","delete from timeline");
define("kSupprimerDuChutier","delete from bin");
define("kJSMsgSelectClip","Please select a clip before !");
define("kSauvegarderMontage","save editing");
define("kDefinirDebut","mark in");
define("kDefinirFin","mark out");


define('kMentionslegales','Legal notice');

define("kSloganOpsis","Leverage <span>the Value</span> <span>of your Media Assets</span>");
define("kImporter","Import");
define("kRechercher","Search");
define("kOk","OK");
define("kFiltres","Filters");
define("kAfficherTout","Display all");
define("kAfficherTopResults","Display main filters");
define("kLoadMoreResults","Load more results");
define("kPreview","Preview");
define("kVoirFicheDoc","See item");
define("kPaniers","Carts");
define("kDropZoneText","Drop a document here to add it to cart");
define("kQuitterMontage","Quit editing workshop ?");

// Paramètres liés à la vérification anti-bots
// define("kValidHumanTitle","Welcome to the Opsis Media online demo.");
define("kGatePopinTitle","Welcome to the Opsis Media online demo.");



?>