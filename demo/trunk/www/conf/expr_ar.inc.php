<?
// Expressions spécifiques à la démo
define("kMsgCommandeAchat","Please fill in this order form by indicating the type of material you want to receive.<br/>If you choose online payment, the test page of our partner PAYBOX will be displayed.<br/>
<strong>This order is for demonstration only : no fees or real shipment will be proceded.</strong>");
define("kMsgCommandeAchatRecap","<strong>This order is for demonstration only : no fees or real shipment will be proceded.</strong>");
define("kMsgCommandeTelecharger","The validation of this order gives you access to a download page of the element with the MPEG-2 format.<br/>
On this demonstration, download is fully operational, legal and free.");
define("kMsgCommandeTelechargerRecap","You can download each element independently,
<strong>by clicking on the floppy</strong> of each line.");
define("kInscriptionEntete","Please fill in the subscription below.<br/>Your personal informations will not be stored, because the databased is reset every night.");
define("kMsgEntete","Opsis Media, an Internet software suite designed for audiovisual archive management,<br/> communication and marketing");

?>