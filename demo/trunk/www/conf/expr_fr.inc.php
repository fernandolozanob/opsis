<?
// Expressions spécifiques à la démo
define("kMsgCommandeAchat","Merci de remplir ce bon de commande en indiquant le type de support que vous voudriez recevoir.<br/>Si vous choisissez le paiement en ligne, la page de test de notre partenaire PAYBOX de paiement par carte de crédit sera affichée.<br/>
<strong>Ce bon de commande n'est fourni qu'à titre de démonstration et n'entraînera à ce titre ni facturation ni livraison.</strong>");
define("kMsgCommandeAchatRecap","<strong>Ce bon de commande n'est fourni qu'à titre de démonstration et n'entraînera à ce titre ni facturation ni livraison.</strong>");
define("kMsgCommandeTelecharger","La validation de ce bon de commande donne accès à la page de téléchargement des éléments de votre commande.<br/>
Dans cette démonstration, le téléchargement est opérationnel et gratuit.");
define("kMsgCommandeTelechargerRecap","Vous pouvez télécharger chaque élément de votre commande de manière unitaire,
<strong>en cliquant sur la disquette</strong> de la ligne correspondante.");
define("kInscriptionEntete","Merci de remplir le formulaire d'inscription ci-dessous.<br/>Vos données personnelles ne seront pas conservées, car la base de données de cette démonstration est réinitialisée chaque nuit.");
define("kMsgEntete","Opsis Media, une suite logicielle Internet conçue pour la gestion,<br/> la communication et la commercialisation d'archives audiovisuelles");

// HTML5
define("kMsgTryHTML5","Essayez notre nouveau lecteur vidéo HTML5");

//Page d'accueil
define("kEveryDocV","Toutes les vidéos");
define("kEveryDocA","Tous les sons");
define("kEveryDocP","Toutes les images");
define("kEveryDocD","Tous les documents");
define("kRetourListeRecherche","Retour &agrave; la liste de recherche");

define("kVideos","Vidéos");
define("kImages","Images");
define("kAudios","Audios");
define("kDocuments","Documents");
define("kReportages","Reportages");


// Administration
define("kConfirmViderpanier","Confirmez-vous la suppression ?");

define("kParametreEntete",'Paramètres de l\'entête');
define("kParametres",'Paramètres');

define('kErrorGroupeSauveUsager','Erreur lors de l\'ajout de l\'usager au groupe');
define('kErrorGroupeSauveFonds','Erreur lors de l\'ajout du fond au groupe');

//formulaire de recherche
define("kDatePublication","Date de publication");

define("kAjouterDossierDoc","Ajouter dans le dossier thématique ...");

define('kUrl','URL');

define('kPartager','Partager');
define('kReseauxSociaux','Réseaux sociaux');
define('kFacebook','Facebook');
define('kTwitter','Twitter');
define('kLinkedIn','Linked in');
define('titre_trop_long_twitter','Le titre est trop long pour Twitter');
define('titre_trop_long_linkedin','Le titre est trop long pour Linked in');


// site mobile
define('kVoulezVousEtreRedirigeMobile','Voulez-vous etre redirigé vers le site mobile ?');

// Montage
define("kAjouterAuMontage","ajouter au montage");
define("kAtelierMontage","Atelier de montage");
define("kVoirDemo","Voir la démo");
define("kChutier","chutier");
define("kGlissezIci","Glissez ici");
define("kUnPlanDeMesSelections","un plan de « mes sélections »");
define("kAfficherAutresPlans","afficher les autres plans");
define("kCliquezGlissez","cliquez-glissez");
define("kPourChangerOrdrePlans","pour changer l’ordre des plans");
define("kMontage","montage");
define("kVisionneuse","visionneuse");
define("kCreerNouveau","créer nouveau");
define("kSansTitre","sans titre");
define("kJouerClip","jouer le clip");
define("kJouerMontage","jouer le montage");
define("kSupprimerDuMontage","supprimer du montage");
define("kSupprimerDuChutier","supprimer du chutier");
define("kJSMsgSelectClip","Sélectionner d'abord un clip !");
define("kSauvegarderMontage","sauvegarder montage");
define("kDefinirDebut","définir début");
define("kDefinirFin","définir fin");


define('kMentionslegales','Mentions légales');


define("kSloganOpsis","Libérez <span>la puissance</span> <span>de vos médias</span>");
define("kImporter","Importer");
define("kRechercher","Rechercher");
define("kOk","OK");
define("kFiltres","Filtres");
define("kAfficherTout","Afficher Tout");
define("kAfficherTopResults","Afficher les filtres principaux");
define("kLoadMoreResults","Charger plus de résultats");
define("kPreview","Preview");
define("kVoirFicheDoc","Voir la fiche doc");
define("kPaniers","Paniers");
define("kDropZoneText","Déposez un document ici pour l'ajouter au panier");
define("kQuitterMontage","Quitter le mode montage");

// Paramètres liés à la vérification anti-bots
define("kGatePopinTitle","Bienvenue&nbsp;sur la&nbsp;démonstration&nbsp;en&nbsp;ligne du&nbsp;logiciel&nbsp;Opsis&nbsp;Media.");

// Spécifique demo
define("kMsgConnexionRefuseeEtat","Votre compte a été désactivé.<br />Merci de contacter l’administrateur.");


?>
