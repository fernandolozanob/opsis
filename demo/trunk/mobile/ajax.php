<?php

require_once("conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php");
require_once(libDir."class_page.php");

$myFrame=Page::getInstance();

$myFrame->setDesign(templateDir."ajax.html");
$myUsr=User::getInstance();

$html=$myFrame->render();

print($html);

?>