<?php

// site mobile
define('kPageSuivante','Afficher plus de vidéos');
define('kRetourALaListe','Retour à la liste');

define('kVideos','Vidéos');
define('kVideo_s_','Vidéo(s)');
define('kDossier_s_','Dossier(s)');
define('kDVDDisponibleSurLeSiteWeb','DVD disponible sur le site web');
define('kVousAllezEtreRedirigeVersLeSiteWeb','Vous allez être redirigé vers le site web');
?>