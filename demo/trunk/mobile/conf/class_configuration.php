<?php

class configuration
{
    public $config;
    
    public function __construct($filename)
    {
        if (!empty($filename)) {
           $config = $this->parse_ini_file_extended($filename);
            
           defined('APPLICATION_ENV')
                || define('APPLICATION_ENV',
                    (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'production'));
           
           $this->config = $config[APPLICATION_ENV];
        }
    }
    
    private function parse_ini_file_extended($filename) {
        $p_ini = parse_ini_file($filename, true);
        $config = array();
        foreach($p_ini as $namespace => $properties){
            list($name, $extends) = explode(':', $namespace);
            $name = trim($name);
            $extends = trim($extends);
            // create namespace if necessary
            if(!isset($config[$name])) $config[$name] = array();
            // inherit base namespace
            if(isset($config[$extends])){
                foreach($config[$extends] as $prop => $val)
                    $config[$name][$prop] = $val;
            }
            // overwrite / set current namespace values
            foreach($properties as $prop => $val)
            $config[$name][$prop] = $val;
        }
        return $config;
    }

    public function defineConstantList()
    {
       foreach($this->config as $cle => $element) {
            define($cle,$element);
        }
    }
    
    public function defineDependentPath()
    {
        define("kHttp",($_SERVER["HTTPS"]=="on"?"https://":"http://"));
        
        if(defined('kCheminHttp') && defined('kChemin') || (!defined('kCheminHttp') && !defined('kChemin'))) {
            error_log("ERROR - conf.ini - une et une seule des deux constantes kCheminHttp ou kChemin doit être définie !", 0);
        } else if(!defined('kCheminHttp') && defined('kChemin')) {
            define("kCheminHttp",kHttp.$_SERVER['SERVER_NAME'].kChemin);
        } 
        
        if(defined('kCheminHttpMedia') && defined('kCheminMedia') || (!defined('kCheminHttpMedia') && !defined('kCheminMedia'))) {
            error_log("ERROR - conf.ini - une et une seule des deux constantes kCheminHttpMedia ou kCheminMedia doit être définie !", 0);
        } else if(!defined('kCheminHttpMedia') && defined('kCheminMedia')) {
            define("kCheminHttpMedia",kHttp.$_SERVER['SERVER_NAME'].kCheminMedia);
        } 
        
        define('kLivraisonLocaleUrl',kCheminHttp.'/comLivraisonLocale.php');
        define('designUrl',kCheminHttp.'/design/');
        define('imgUrl',kCheminHttp.'/design/images/');
		//upload
		define("kTinymceUploadPath",kCheminLocalMedia."/html");
		define("kTinymceUploadPathRelativ",kCheminHttpMedia."/html");
        
        define('storyboardChemin',kCheminHttpMedia.'/storyboard/');
        define('kVisionnageUrl',kCheminHttpMedia.'/visionnage/');
        define('videosServer',kCheminHttpMedia.'/videos/');
        define('kDocumentUrl',kCheminHttpMedia.'/doc_acc/');
        define("kLivraisonUrl",kCheminHttpMedia."/livraison/");
        define("rapportServer",kCheminHttpMedia."/rapport/");
        define("kFckEditorRessourceDir",kCheminHttpMedia.'/html/');
        
        define('libUrl',coreUrl.'/lib/');
        define('frameUrl',coreUrl.'/frame/');
        define('upload_scriptUrl',coreUrl.'/upload/');
        define('designUrlCore',coreUrl.'/site/design/');
        define('imgUrlCore',coreUrl.'/site/design/images/');
        
        define('baseDir',kCheminLocalSurServeur);
        define('confDir',kCheminLocalSurServeur.'/conf/');
        define('designDir',kCheminLocalSurServeur.'/design/');
        define('imageDir',kCheminLocalSurServeur.'/design/images/');
        define('formDir',kCheminLocalSurServeur.'/design/form/');
        define('listeDir',kCheminLocalSurServeur.'/design/liste/');	
        define('templateDir',kCheminLocalSurServeur.'/design/templates/');
       
        define('kCheminLocalVisionnage',kCheminLocalMedia.'/visionnage/');
        define('klivraisonRepertoire', kCheminLocalMedia.'/livraison/');
        define('kStoryboardDir',kCheminLocalMedia.'/storyboard/');
        define('kDocumentDir',kCheminLocalMedia.'/doc_acc/');
        define('kThumbnailDir',kCheminLocalMedia.'/thumbnails/');
        define("kRapportDir",kCheminLocalMedia."/rapport/");
        define("gFTdumpFile",kCheminLocalMedia."/ftdump.txt");
        define("kFckEditorRessourceAbsolutePath",kCheminLocalMedia.'/html/');
        
        define('kVideosDir',kCheminLocalPrivateMedia.'/videos/');
        define('kImagesDir', kCheminLocalPrivateMedia.'/images/');
        define('FCPWatchFolder',kCheminLocalPrivateMedia.'/import/');
        		
        define('libDir',coreDir.'/lib/');
        define('includeDir',coreDir.'/include/');
        define('frameDir',coreDir.'/frame/');
        define('upload_scriptDir',coreDir.'/upload/');
        define('webserviceDir',coreDir.'/webservice/');
        define('batchDir',coreDir.'/batch/');
        define('siteDirCore',coreDir.'/site/');
        define('confDirCore',coreDir.'/site/conf/');
        define('designDirCore',coreDir.'/site/design/');
        define('imageDirCore',coreDir.'/site/design/images/');
        define('formDirCore',coreDir.'/site/design/form/');
        define('listeDirCore',coreDir.'/site/design/liste/');	
        define('templateDirCore',coreDir.'/site/design/templates/');
        
        define('jobInDir',jobDir.'/in'); // ce repertoire contient les fichiers XML en attente de traitement
        define('jobRunDir',jobDir.'/run'); // ce repertoire contient les fichiers XML en cours de traitement
        define('jobDoneDir',jobDir.'/done');
        define('jobOutDir',jobDir.'/out'); // fichier de retour  par le frontal
        define('kBackupInfoDir',jobDir.'/info'); // "Dossier d’information utilisé par le backup"
        define('jobTmpDir',jobDir.'/tmp'); // repertoire temporaire
        define('jobCancelDir',jobDir.'/cancel'); // XML en attente d'annulation
        define('jobCancellingDir',jobDir.'/cancelling'); // XML en cours d'annulation
        define('jobCanceledDir',jobDir.'/canceled'); // XML des fichiers annulés
        define('jobDeleteDir',jobDir.'/delete'); // XML des traitements en attente de suppression
        define('jobDeletingDir',jobDir.'/deleting'); // XML des traitements en cours de suppression
        define('jobDeletedDir',jobDir.'/deleted'); // XML fichiers XML supprimmés
        define('jobErrorDir',jobDir.'/error'); // XML des erreurs de traitementdefine('jobInDir',jobDir.'/in'); // ce repertoire contient les fichiers XML en attente de traitement
        define('jobSettingDir',jobDir.'/etc/setting'); // fichiers utilisés par les traitements (images,polices ....)
        define('jobImageDir',jobDir.'/etc/images/');
        define('jobMusiqueDir',jobDir.'/etc/musique/');
        define('jobFontDir',jobDir.'/etc/fonts/');
    }
}
?>