<?php
    
    //if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','preproduction');
    //if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','prodPrivate');
    //if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','integration');
    //if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','localGhislain');
    //if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','localMartin');
    //if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','localPierre');
    //if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','localVivien');
    if (!defined('APPLICATION_ENV')) define('APPLICATION_ENV','localVincent');
    
    require_once 'class_configuration.php';
    
    $configuration = new configuration('conf.ini');
    $configuration->defineConstantList();
    $configuration->defineDependentPath();
    
    if(debugMode)
    {
        error_reporting(E_ALL); 
        ini_set('display_errors','On');
    }
    
?>