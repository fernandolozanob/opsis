<?php
require_once('conf/conf.inc.php');
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php");

require_once(libDir.'class_chercheDoc.php');
require_once(libDir.'class_page.php');

$recherche_doc=new RechercheDoc();
$recherche_doc->prepareSQL();

$page=new Page();

if (isset($_POST['page']) && !empty($_POST['page']))
	$num_page=intval($_POST['page']);

$recherche_doc->getSearchFromSession();
$recherche_sql=$recherche_doc->sql;//.$page->addDeftOrder(array('COL'=>array('t1.DOC_DATE_PROD'),'DIRECTION'=>array('DESC')));


$page->initPager($recherche_sql,20,1);
$page->addParamsToXSL(array('ajax'=>'1','page'=>$page->page,'page_suivante'=>kPageSuivante,'nbLigne'=>gNbLignesDocListeDefaut));
$page->afficherListe('doc',designDir.'liste/docListe.xsl');

?>