<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- D�veloppeurs : Loic Desjardins											  ---*/
/*-----------------------------------------------------------------------------------*/

// NOTA : respecter cet ordre !!!
    $time_start = microtime(true);
     require_once("conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php");
     require_once(libDir."class_page.php");

    $myFrame=Page::getInstance();
    
    $myFrame->setDesign(templateDir."empty.html");

	$myFrame->includePath=frameDir;
	if ($_GET['where']) $myFrame->includePath=coreDir."/".$_GET['where']."/";
	$myUsr=User::getInstance();

  	$html=$myFrame->render();

	print($html);
	$time_end = microtime(true);
	$time=$time_end-$time_start;
	exit;
?>

