<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes" indent="yes" />
	<xsl:param name="langue" />
	<xsl:param name="ajax" />
	<xsl:param name="nbRows" />
	<xsl:param name="page" />
	<xsl:param name="nb_pages" />
	<xsl:param name="page_suivante" />
	<xsl:param name="nbLigne" />
	
	<xsl:template match='/select'>
		<xsl:if test="$ajax!=1">
			<xsl:variable name="nb_lignes_total">
				<xsl:if test="$nbLigne&gt;$nbRows">
					<xsl:value-of select="$nbRows" />
				</xsl:if>
				<xsl:if test="$nbLigne&lt;=$nbRows">
					<xsl:value-of select="$nbLigne" />
				</xsl:if>
			</xsl:variable>
			<script type="text/javascript">
			window.onload=function ()
			{
				$('#page_footer p.page_footer_nb_results').html('<xsl:value-of select="$nb_lignes_total" /> / <xsl:value-of select="$nbRows" />&amp;nbsp;<xsl:processing-instruction name="php">echo kVideo_s_;</xsl:processing-instruction>')
			}
			</script>
		</xsl:if>
		
		<xsl:for-each select="doc">
			<xsl:variable name="rang">
				<xsl:value-of select="((($page)-1)*$nbLigne)+@value+1" />
			</xsl:variable>
			<div class="element_liste_doc">
				<table>
					<tr>
						<td rowspan="2" class="vignette_liste_doc">
							<a href="index.php?urlaction=doc&amp;amp;id_doc={ID_DOC}&amp;amp;rang={$rang}">
								<xsl:choose>
									<xsl:when test="IM_CHEMIN='&#160;'">
										<img src="design/images/nopicture.png" alt="vignette" />
									</xsl:when>
									<xsl:otherwise>
										<img src="makeVignette.php?image={concat(IM_CHEMIN,'/',IM_FICHIER)}&amp;amp;type=storyboard&amp;amp;h=54" alt="vignette" />
									</xsl:otherwise>
								</xsl:choose>
							</a>
							<p class="duree">
								<xsl:value-of select="substring(DOC_DUREE,4)" />
							</p>
						</td>
						<td class="titre_doc_liste">
							<a href="index.php?urlaction=doc&amp;amp;id_doc={ID_DOC}&amp;amp;rang={$rang}"><xsl:value-of select="DOC_TITRE" /></a>
						</td>
					</tr>
					<tr>
						<td class="date_doc_liste">
							<a href="index.php?urlaction=doc&amp;amp;id_doc={ID_DOC}&amp;amp;rang={$rang}">
								<xsl:value-of select="DOC_DATE_PROD" />
							</a>
						</td>
					</tr>
				</table>
			</div>
		</xsl:for-each>
		<xsl:if test="$page&lt;$nb_pages">
			<div id="btn_page_suivante">
				<a href="javascript:void(0);" onclick="loadPageAjax();">
					<xsl:if test="$page_suivante!=''">
						<xsl:value-of select="$page_suivante" />
					</xsl:if>
					<xsl:if test="$page_suivante=''">
						<xsl:processing-instruction name="php">echo kPageSuivante;</xsl:processing-instruction>
					</xsl:if>
				</a>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>