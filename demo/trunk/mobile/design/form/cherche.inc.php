<?php

require_once(libDir.'class_chercheDoc.php');
require_once(libDir.'class_formCherche.php');

echo '<div id="form_cherche">';
$form_recherche=new FormCherche();
$form_recherche->chercheObj=$mySearch;
$form_recherche->entity="DOC";
$form_recherche->classLabel="label_champs_form";
$form_recherche->classValue="val_champs_form";
$form_recherche->classHelp="help_champs_form";
$form_recherche->display(file_get_contents(designDir."form/xml/cherche.xml"));
echo '</div>';

if (!(isset($_SESSION['recherche_DOC']['sql']) && !empty($_SESSION['recherche_DOC']['sql'])))
{
	$recherche_doc=new RechercheDoc();
	$recherche_doc->prepareSQL();
	$recherche_doc->AnalyzeFields(array('DOC_ID_TYPE_DOC','MEDIA'),array('4','V'),array('CVLT','CI'),array('AND','AND'));
	$recherche_doc->appliqueDroits();
	$recherche_doc->finaliseRequete();
	$recherche_doc->putSearchInSession();
}
?>