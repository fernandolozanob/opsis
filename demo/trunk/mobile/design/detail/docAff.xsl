<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes" indent="yes" />
	<xsl:param name="langue" />
	<xsl:param name="rang" />
	<xsl:param name="nbDocs" />
	
	<xsl:template name="escapeQuotes">
		<xsl:param name="stream" />
		<xsl:variable name="simple-quote">'</xsl:variable>
		<xsl:choose>
			<xsl:when test="contains($stream,$simple-quote)">
				<xsl:value-of select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template name="escapeQuotes">
					<xsl:with-param name="stream" select="substring-after($stream,$simple-quote)" />
				</xsl:call-template>		
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$stream"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="/EXPORT_OPSIS">
		<xsl:for-each select="t_doc">
			<div class="titre_page_body">
				<p class="text_left">
					<a href="index.php?urlaction=docListe"><xsl:processing-instruction name="php">echo kRetourALaListe;</xsl:processing-instruction></a>
				</p>
				<xsl:if test="$rang!=0">
				</xsl:if>
			</div>
			
			<xsl:if test="$rang!=0">
				<xsl:if test="$rang&gt;1">
					<xsl:variable name="num_page_precedente"><xsl:value-of select="($rang)-1" /></xsl:variable>
					<p class="lien_rang_precedent">
						<a href="index.php?urlaction=doc&amp;amp;rang={$num_page_precedente}"><xsl:processing-instruction name="php"> echo kPrecedent;</xsl:processing-instruction></a>
					</p>
				</xsl:if>
				<xsl:if test="$rang&lt;$nbDocs">
					<xsl:variable name="num_page_suivante"><xsl:value-of select="($rang)+1" /></xsl:variable>
					<p class="lien_rang_suivant">
						<a href="index.php?urlaction=doc&amp;amp;rang={$num_page_suivante}"><xsl:processing-instruction name="php"> echo kSuivant;</xsl:processing-instruction></a>
					</p>
				</xsl:if>
				
				<p class="detail_doc_rang">
					<xsl:value-of select="$rang" />&amp;nbsp;/&amp;nbsp;<xsl:value-of select="$nbDocs" />
				</p>
				
			</xsl:if>
			
			<div id="vignette_detail_doc" onclick="visionne({ID_DOC});">
				<span>
					<img src="design/images/playerMini.png" alt="play" class="btn_play" />
					<xsl:choose>
						<xsl:when test="VIGNETTE=''">
							<img src="design/images/nopicture_big.png" alt="vignette" class="vignette" />
						</xsl:when>
						<xsl:otherwise>
							<img src="makeVignette.php?image={VIGNETTE}&amp;amp;h=169" alt="vignette" class="vignette" />
						</xsl:otherwise>
					</xsl:choose>
				</span>
				<p class="duree"><xsl:value-of select="substring(DOC_DUREE,4)" /></p>
			</div>
			<div id="texte_detail_doc">
				<h1><xsl:value-of select="DOC_TITRE" /></h1>
				
				
				<xsl:variable name="url_serveur">&lt;?php echo gSiteVersionWeb.''; ?&gt;</xsl:variable>
				
				<table class="info_generique">
					<xsl:if test="DOC_COTE!=''">
						<tr>
							<td><xsl:processing-instruction name="php">echo kCote;</xsl:processing-instruction> : </td>
							<td><span class="gras"><xsl:value-of select="DOC_COTE" /></span></td>
						</tr>
					</xsl:if>
					
					<xsl:if test="TYPE_DOC!=''">
						<tr>
							<td><xsl:processing-instruction name="php">echo kTypeDocument;</xsl:processing-instruction> : </td>
							<td><span class="gras"><xsl:value-of select="TYPE_DOC" /></span></td>
						</tr>
					</xsl:if>
					
					<xsl:if test="DOC_DATE_PROD!=''">
						<tr>
							<td><xsl:processing-instruction name="php">echo kDateProduction;</xsl:processing-instruction> : </td>
							<td><span class="gras"><xsl:value-of select="DOC_DATE_PROD" /></span></td>
						</tr>
					</xsl:if>
					
					<xsl:if test="DOC_XML/XML/DOC/FONDS!=''">
						<tr>
							<td><xsl:processing-instruction name="php">echo kFonds;</xsl:processing-instruction> : </td>
							<td><span class="gras"><xsl:value-of select="DOC_XML/XML/DOC/FONDS" /></span></td>
						</tr>
					</xsl:if>
					<xsl:if test="DOC_VAL/XML/THM!=''">
						<tr>
							<td><xsl:processing-instruction name="php">echo kTheme;</xsl:processing-instruction> : </td>
							<td><span class="gras"><xsl:value-of select="DOC_VAL/XML/THM" /></span></td>
						</tr>
					</xsl:if>
					<tr>
						<td colspan="2">
							<xsl:value-of select="DOC_RES" />
						</td>
					</tr>
					<xsl:if test="count(DOC_LEX/XML/LEX[@TYPE='MC'])&gt;0">
						<tr>
							<td>
								<xsl:processing-instruction name="php">echo kMotCle;</xsl:processing-instruction>&amp;nbsp;: 
							</td>
							<td>
								<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='MC']">
									<span class="gras"><xsl:value-of select="TERME" /></span><br />
								</xsl:for-each>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="count(DOC_LEX/XML/LEX[@TYPE='PE'])&gt;0">
						<tr>
							<td>
								<xsl:processing-instruction name="php">echo kPersonnalite;</xsl:processing-instruction>&amp;nbsp;: 
							</td>
							<td>
								<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='PE']">
									<span class="gras"><xsl:value-of select="TERME" /></span><br />
								</xsl:for-each>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="count(DOC_LEX/XML/LEX[@TYPE='LG'])&gt;0">
						<tr>
							<td>
								<xsl:processing-instruction name="php">echo kLieu;</xsl:processing-instruction>&amp;nbsp;: 
							</td>
							<td>
								<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='LG']">
									<span class="gras"><xsl:value-of select="TERME" /></span><br />
								</xsl:for-each>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="count(DOC_LEX/XML/LEX[@TYPE='PRD'])&gt;0">
						<tr>
							<td>
								<xsl:processing-instruction name="php">echo kProducteur;</xsl:processing-instruction>&amp;nbsp;: 
							</td>
							<td>
								<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='PRD']">
									<span class="gras"><xsl:value-of select="TERME" /></span><br />
								</xsl:for-each>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="count(DOC_LEX/XML/LEX[@TYPE='GEN' and @ROLE='REA'])&gt;0">
						<tr>
							<td>
								<xsl:value-of select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='REA']/t_lex/ROLE" />&amp;nbsp;: 
							</td>
							<td>
								<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='GEN' and @ROLE='REA']">
									<span class="gras"><xsl:value-of select="TERME" /></span><br />
								</xsl:for-each>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="count(DOC_LEX/XML/LEX[@TYPE='GEN' and @ROLE='REC'])&gt;0">
						<tr>
							<td>
								<xsl:value-of select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='REC']/t_lex/ROLE" />&amp;nbsp;: 
							</td>
							<td>
								<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='GEN' and @ROLE='REC']">
									<span class="gras"><xsl:value-of select="TERME" /></span><br />
								</xsl:for-each>
							</td>
						</tr>
					</xsl:if>
					<xsl:if test="count(DOC_LEX/XML/LEX[@TYPE='GEN' and @ROLE='ANI'])&gt;0">
						<tr>
							<td>
								<xsl:value-of select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='ANI']/t_lex/ROLE" />&amp;nbsp;: 
							</td>
							<td>
								<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='GEN' and @ROLE='ANI']">
									<span class="gras"><xsl:value-of select="TERME" /></span><br />
								</xsl:for-each>
							</td>
						</tr>
					</xsl:if>
				</table>
				
				<xsl:for-each select="DOC_VAL/XML/SUP">
					<xsl:if test=".='DVD'">
						<p><xsl:processing-instruction name="php">echo kDVDDisponibleSurLeSiteWeb;</xsl:processing-instruction></p>
					</xsl:if>
				</xsl:for-each>
				
				<div class="btn_rs_detail_doc">
					<xsl:variable name="titre_js">
						<xsl:call-template name="escapeQuotes">
							<xsl:with-param name="stream" select="DOC_TITRE" />
						</xsl:call-template>
					</xsl:variable>
					<a href="javascript:void(0);" onclick="ouvreFentreReseaux('{$url_serveur}',{ID_DOC},'{$titre_js}');">
						<img src="design/images/partage_rs_mobile.png" alt="partage reseaux sociaux" />
					</a>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>