var page=1;
var nb_docs=20;
var is_sending_ajax=false;

function loadPageAjax()
{
	if (is_sending_ajax==false)
	{
		is_sending_ajax=true;
		page++;

		$('#btn_page_suivante').html('<img src="design/images/loader_page_mobile.gif" alt="loader" />');
		$.ajax
		({
			url:'ajax.php',
			type:'GET',
			data:
			{
				urlaction:'docListe',
				cherche:'2',
				page:page
			},
			success:function (data, text_status, xhr)
			{
				$('#btn_page_suivante').remove();
				$('#espace_footer').remove();
				$('#page_body').html($('#page_body').html()+data+'<div id="espace_footer">&nbsp;</div>');
				
				var nb_total=$('#page_footer p.page_footer_nb_results').html().split('/');
				$('#page_footer p.page_footer_nb_results').html($('#page_body div.element_liste_doc').length+' /'+nb_total[1]);
				is_sending_ajax=false;
			}
		});
	}
}

function visionne(id_doc)
{
	var amp = String.fromCharCode(38);
	
	$.ajax
	(
		{
			
			url:'empty.php?xmlhttp=1&urlaction=prepareVisu&method=QT&action=visu&pattern=_iphone.mp4&id='+id_doc+'&type=document',
			type:'GET',
			dataType:'xml',
			success: function(data, textStatus, jqXHR)
			{
				var url=$(data).find('mediaurl').text();
				
				if (url!='')
					window.location.href=url;
			}
		}
	)
}

function makeDraggable(elt)
{
}

function ouvreFentreReseaux(host,id_doc,titre_doc)
{
	$('div.fond_modal').remove();
	$('div.modal_contenu').remove();
	
	var url=host+'/index.php?urlaction=doc&id_doc='+id_doc;
	
	var code_modale='';
	
	code_modale+='<div class="modal_contenu">';
	code_modale+='<div class="titre_modale">'+str_lang.partager_sur+'</div>';
	
	code_modale+='<table>';
	code_modale+='<tr><td><img src="design/images/logo_facebook_mobile.png" alt="Facebook" /></td><td><p><a href="http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+titre_doc+'" target="_blank">Facebook</a></p></td></tr>';
	code_modale+='<tr><td><img src="design/images/logo_googleplus_mobile.png" alt="Google +" /></td><td><p><a href="https://plus.google.com/share?url='+encodeURIComponent(url)+'" target="_blank">Google +</a></p></td></tr>';
	code_modale+='<tr><td><img src="design/images/logo_twitter_mobile.png" alt="Twitter" /></td><td><p><a href="javascript:void(0)" onclick="shareTwitter(\''+host+'\',\''+id_doc+'\',\''+titre_doc+'\')">Twitter</a></p></td></tr>';
	code_modale+='<tr><td><img src="design/images/image_mail_mobile.png" alt="Mail" /></td><td><p><a href="mailto:?body='+encodeURIComponent(titre_doc)+"%0A"+encodeURIComponent(host+'/index.php?urlaction=doc&id_doc='+id_doc)+'">Mail</a></p></td></tr>';
	code_modale+='</table>';
	
	code_modale+='<div class="btn_fermer"><a href="javascript:void(0);" onclick="fermeFentreReseaux()">'+str_lang.annuler+'</a></div>';
	code_modale+='</div>';
	code_modale+='<div class="fond_modal"></div>';
	$('body').append(code_modale);
	
	$('div.modal_contenu').css('margin-top','-'+($('div.modal_contenu').height()/2)+'px');
}

function fermeFentreReseaux()
{
	$('div.fond_modal').remove();
	$('div.modal_contenu').remove();
}

function shareTwitter(host,id_doc,titre_doc)
{
	var url_non_encodee=host+'/index.php?urlaction=doc&id_doc='+id_doc
	var url=encodeURIComponent(url_non_encodee);
	var titre_url=encodeURIComponent(titre_doc); // titre encode pour l'url
	
	if ((titre_doc+' '+url_non_encodee).length>140)
		alert(str_lang.titre_trop_long_twitter);
	else
		window.open('http://twitter.com/intent/tweet?url='+url+'&text='+titre_url);
}

