<?php

require_once('conf/conf.inc.php');
require_once(libDir.'session.php');
require_once(libDir.'class_Basis.php');

header('Content-type: text/xml');

$list_db=unserialize(gBasesBasis);

$basis=new Basis();

if (isset($_SESSION['DB']) && !empty($_SESSION['DB']) && in_array($_SESSION['DB'],$list_db))
{
	$basis->setDb($_SESSION['DB']);
}
$view=(empty($_GET['view'])?kVueBasis:$_GET['view']);
$result=$basis->browseIndex($view.'.'.$_GET['type'],'\'%'.addslashes($_GET['debut']).'%\'');

echo '<options>';
foreach ($result as $res)
{
	echo '<option>'.$res['TERM'].'</option>';
}
echo '</options>';

?>