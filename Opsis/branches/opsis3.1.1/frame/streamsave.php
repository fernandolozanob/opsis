<?php

	require_once(modelDir."model_processus.php");
	require_once(modelDir."model_etape.php");
	require_once(modelDir."model_job.php");
	require_once(libDir."class_jobProcess.php");
	
	header("Content-type: text/xml");
	// echo '<?xml version="1.0" encoding="UTF-8" >';
	echo "<xml>";
	if($_GET['action'] == 'start'){
		global $db ; 
		$stream = ($_REQUEST['stream']?$_REQUEST['stream']:'');
		$duration = ($_REQUEST['duration']?$_REQUEST['duration']:'21600');
		$id_doc = ($_REQUEST['id_doc']?$_REQUEST['id_doc']:0);
		$addParams = ($_REQUEST['addParams']?$_REQUEST['addParams']:'');
		
		// debug tests
		// $addParams="";
		// $duration = ($_REQUEST['duration']?$_REQUEST['duration']:'60');
		
		$streamsave_key = removeTrickyChars($stream.'_'.$id_doc.'_'.time().'_'.$duration); 
		$name_file = (isset($_GET['name']) && !empty($_GET['name']))?$_GET['name']:$streamsave_key;
		
		// recuperation id_etape de l'encodage streamsave
		$id_etape = $db->GetOne("select ID_ETAPE from t_etape 
		left join t_module on t_etape.etape_id_module= t_module.id_module
		WHERE t_module.module_nom ='streamsave'");
		
		if(empty($id_etape) || is_int($id_etape)){
			echo "<error>".(kErrorStreamsaveProcNotFound)."</error>";
			return false ; 
		}else if (empty($stream)){
			echo "<error>".kErrorStreamsaveNoStream."</error>";
		}else if (empty($id_doc)){
			echo "<error>".kErrorStreamsaveNoStream."</error>";
		}
		
		// récupération de l'étape & création du job en mode synchrone
		$etape = new Etape($id_etape);
		$etape->flag_runSyncJob = true ; 
		$etape->createJob(0,$name_file,0,0,$job_valid,$job_sync,'<param><stream_server>'.kStreamServerUrl.'</stream_server><duration>'.$duration.'</duration><stream_name>'.$stream.'</stream_name><ID_DOC>'.$id_doc.'</ID_DOC><streamsave_key>'.$streamsave_key.'</streamsave_key>'.$addParams.'</param>');
		
		
		// $job = new Job($id_job_created);
		echo "<id_job>".$job_sync->t_job['ID_JOB']."</id_job>";
		// on set un timeout limit pour stopper l'exec du php ;
		set_time_limit('21600');
		
		// execution synchrone de l'étape d'encodage streamsave
		JobProcess::execJobSync($job_sync);
		
		// paramétrage du lancement d'un processus d'encodage asynchrone en followup de l'encodage streamsave
		if(isset($_REQUEST['trigger_proc']) && !empty($_REQUEST['trigger_proc'])){
			$proc = new Processus($_REQUEST['trigger_proc']);
			$proc->getProcEtape() ; 
			// report du paramètre ID_DOC 
			$proc->createJobs($job_sync->outMat->t_mat['ID_MAT'],$job_sync->outMat->t_mat['MAT_NOM'],"<param><ID_DOC>".$id_doc."</ID_DOC></param>");
		}		
	}else if($_GET['action'] == 'stop'){
		$target = (isset($_GET['target'])?$_GET['target']:'latest');
		
		if($target == 'latest' && isset($_SESSION['syncJobs']) && !empty($_SESSION['syncJobs'])){
			$tmp = array_keys($_SESSION['syncJobs']);
			$id_job = end($tmp);
		}else if(is_numeric($target)  && isset($_SESSION['syncJobs'][$target]) && !empty($_SESSION['syncJobs'][$target])){
			$id_job = $target;
		}
		// trace("id_job ".$id_job);
		if(isset($id_job) && !empty($id_job)){
			$pid = ($_SESSION['syncJobs'][$id_job]['PID']?$_SESSION['syncJobs'][$id_job]['PID']:null ); 
			$ssh = ($_SESSION['syncJobs'][$id_job]['SSH_COMP']?$_SESSION['syncJobs'][$id_job]['SSH_COMP']:null ); 
			JobProcess::killPid($pid,'2',$ssh);
			unset($_SESSION['syncJobs'][$id_job]);
			echo "<id_job>$id_job</id_job>";
			echo "<stop>true</stop>";
		}else{
			echo "<error>".(kErrorStreamsaveJobNotFound)."</error>";
			return false ; 
		}
	}else{
		echo "<error>".(kErrorStreamsaveMissingAction)."</error>";
	}
	
	echo "</xml>";
?>