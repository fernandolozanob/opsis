<?php

require_once(libDir . "class_user.php");
require_once(libDir . "class_page.php");
require_once(modelDir.'model_imageur.php');
require_once(libDir . "class_matInfo.php");
$myPage = Page::getInstance();

$myImageur = new Imageur();

global $db;
$commande = htmlspecialchars(urldecode($_REQUEST['commande']));
$from = $_GET['from'];
$id_lang = $_REQUEST['id_lang'];
if (empty($_REQUEST['id']) && $commande != 'generateStoryboard') {
    return false;
}

//trace(print_r($_REQUEST,true));

switch ($from) {

    case 'materiel':
        include_once(modelDir.'model_materiel.php');
        $myMat = new Materiel();

        $myMat->t_mat['ID_MAT'] = $_REQUEST['id'];
        $arrAddParams = null;
        if (!empty($myMat->t_mat['ID_MAT'])) {
            //Attention obligé de mettre "$arrAddParams[0]" et pas "$arrAddParams" pour que cela soit retrouvé au bon endroit dans l'export xml
            $arrAddParams[0] = array("ID_MAT" => $myMat->t_mat['ID_MAT']);
        }
        $myMat->getMat();
        $myImageur->t_imageur['ID_IMAGEUR'] = $myMat->t_mat['MAT_ID_IMAGEUR'];
        $myImageur->getImageur(null, $id_lang, null, $arrAddParams);
        
//        echo 'debug_affich<pre>';
//        var_dump($myImageur);
//        echo '</pre>';
//        die();
        
        
        $addXML = $myMat->xml_export(0, 0);
        break;

    case 'document':
        include_once(modelDir.'model_doc.php');
        $myDoc = new Doc();
        $myDoc->t_doc['ID_DOC'] = htmlentities($_REQUEST['id']);
        if (!empty($_REQUEST['id_lang'])) {
            $myDoc->t_doc['ID_LANG'] = strtoupper(htmlentities($_REQUEST['id_lang']));
        }
        $myDoc->getDoc();
        //trace(print_r($myDoc,true));
        // VP 3/06/09 : correction bug doublon d'imageur
        if (!empty($myDoc->t_doc['DOC_ID_IMAGEUR'])) {  // NOTE : préférence est donnée à l'imageur directement associé au doc.
            // Ceci est fait stt pour assurer la compatibilité avec certains sites (CNRS)
            $arrImg[$myDoc->t_doc['DOC_ID_IMAGEUR']] = $myDoc->t_doc['DOC_ID_IMAGEUR'];
            $arrLimits[$myDoc->t_doc['DOC_ID_IMAGEUR']] = array("TCIN" => $myDoc->t_doc['DOC_TCIN'], "TCOUT" => $myDoc->t_doc['DOC_TCOUT']); //TC du DOC
            $myImageur->t_imageur['ID_IMAGEUR'] = $myDoc->t_doc['DOC_ID_IMAGEUR'];
        }

        $myDoc->getMats();
        //si on a pas d'imageur document OU BIEN on a le même dans l'imageur matériel, on prend l'imageur matériel
        //car on va pouvoir faire des captures ! Note: le cas DOC_IMAGEUR + MAT_IMAGEUR identique est fréquent chez
        //les anciens clients 

        /*

         * @author B.RAVI 2016-01-26
         * Bug constaté sur CNRS, la cliente capturait des img du storyboard à partir de la vidéo anglaise (quand un notice avait une video anglaise et française
         * ce qui pose pb car la vidéo anglaise a des sous-titres, il faut donc tenir compte de la langue
         */
        $id_mat_visu = $myDoc->getMaterielVisu(true, false, null, $id_lang);


//        echo 'debug_affich $id_mat_visu '.$id_mat_visu;
//        echo '<div style="position:absolute;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_affich $myDoc->t_doc_mat<pre>';
//        var_dump($myDoc->t_doc_mat);
//        echo '</pre></div><br />';


        foreach ($myDoc->t_doc_mat as $idx => $dm) {
            // On prend en priorite :
            // - l'imageur du document
            // - l'imageur du matériel de visionnage
            // - le dernier imageur non vide
            if ($dm['DMAT_INACTIF'] == '1' || empty($dm['MAT']->t_mat['MAT_ID_IMAGEUR'])) {
                continue;
            }
            if (!empty($myDoc->t_doc['DOC_ID_IMAGEUR']) && $myDoc->t_doc['DOC_ID_IMAGEUR'] == $dm['MAT']->t_mat['MAT_ID_IMAGEUR']) {
                $selected_dm = $idx;
                break;
            } elseif (empty($myDoc->t_doc['DOC_ID_IMAGEUR']) && $dm['MAT']->t_mat['ID_MAT'] == $id_mat_visu) {
                $selected_dm = $idx;
            } elseif (!isset($selected_dm)) {
                $selected_dm = $idx;
            }
        }
        if (isset($selected_dm)) {
            $dm = $myDoc->t_doc_mat[$selected_dm];
            $arrImg[$dm['MAT']->t_mat['MAT_ID_IMAGEUR']] = $dm['MAT']->t_mat['MAT_ID_IMAGEUR'];
            $arrLimits[$dm['MAT']->t_mat['MAT_ID_IMAGEUR']] = array("TCIN" => $dm['DMAT_TCIN'], "TCOUT" => $dm['DMAT_TCOUT']); //TC de DOC_MAT

            $arrAddParams[$dm['MAT']->t_mat['MAT_ID_IMAGEUR']] = array("ID_MAT" => $dm['ID_MAT'], 'DMAT_ID_MAT' => isset($_POST['DMAT_ID_MAT']) ? $_POST['DMAT_ID_MAT'] : '');
        }


//        echo 'debug_affich MAT_ID_IMAGEUR '.$dm['MAT']->t_mat['MAT_ID_IMAGEUR'].'<br />';

        $myImageur->selectedPic = $myDoc->t_doc['DOC_ID_IMAGE'];  //affectation de la vignette
//		trace(print_r($arrImg,true));
//		trace(print_r($arrLimits,true));
//		trace(print_r($arrAddParams,true));
//		trace(print_r($arrInactif,true));

        $myImageur->getImageur($arrImg, $id_lang, $arrLimits, $arrAddParams); //récup imageur(s) + images

        $addXML = $myDoc->xml_export(0, 0);
        break;

    case 'imageur' :
        $myImageur->t_imageur['ID_IMAGEUR'] = $_REQUEST['id'];
        $myImageur->getImageur(null, $id_lang);
        break;
}


switch ($commande) {
    case 'add': //ajout
        // pas utilisé	pour le moment
        break;

    case 'view':
        header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");

        if (isset($_GET["page"])) {
            $page = $_GET["page"];
        } else {
            $page = "1";
        }
        $xsl = ($_GET["xsl"] ? $_GET["xsl"] : 'oraoweb_storyboard');

        $myPage->nbLignes = ($_REQUEST['nbLignes'] ? $_REQUEST['nbLignes'] : 9);
        $myPage->page = $_REQUEST['page'];
        if ($myPage->page == '') {
            $myPage->page = $_REQUEST['rang']; //BY LD XXX expérimental
        }
        $myPage->initPagerFromArray($myImageur->t_images);

        $param["nbLigne"] = $myPage->nbLignes;
        $param["page"] = $myPage->page;
        $param["pager_link"] = $myPage->PagerLink;
        $param["offset"] = $myPage->nbLignes * ($myPage->page - 1);
        $param["urlparams"] = $myPage->addUrlParams();
        $param["storyboardChemin"] = storyboardChemin;
        $param["imgurl"] = imgUrl;
        $param["id_lang"] = $id_lang;
        $param["defaultTitle"] = kVisioNomDefautExtrait;


        $myPage->addParamsToXSL($param);
        //trace(print_r($myImageur->xml_export().array2xml($_SESSION['arrLangues'],'t_langues').$addXML,true));
        // En plus de la liste des images, on ajoute au XML la liste des imageurs et la liste des langues
        $myPage->afficherListe('t_images', getSiteFile("listeDir", $xsl . '.xsl'), false, null, $myImageur->xml_export() . array2xml($_SESSION['arrLangues'], 't_langues') . $addXML);

        break;

    case 'grab' :
        //trace(print_r($myMat,true));
        //trace(print_r($myImageur,true));
        // VP 13/12/10 : création imageur par défaut
        /* if(empty($myMat->t_mat['MAT_IMAGEUR'])){
          $imageur=stripExtension($myMat->t_mat['ID_MAT']);
          $myMat->saveImageur($imageur);
          } */


        if (empty($myImageur->t_imageur['IMAGEUR'])) {
            if (!empty($_REQUEST['imageur'])) {
                $myImageur->t_imageur['IMAGEUR'] = $_REQUEST['imageur'];
            } else {
                $myImageur->t_imageur['IMAGEUR'] = stripExtension($myMat->t_mat['MAT_NOM']);
            }
            if (!$myImageur->checkExist()) {
                $myImageur->create();
            }

            $myImageur->getImageur();

            if (empty($myMat->t_mat['MAT_ID_IMAGEUR'])) {
                $myMat->saveImageur($myImageur->t_imageur['ID_IMAGEUR']);
            }
        }

        header("http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
        $objImage = new Image();
        //PC 30/03/11 : On récupère les dim par déft du matériel.
        require_once(modelDir.'model_materiel.php');
        //$infos=Materiel::getFileInfo($myMat->getLieu().$myMat->t_mat['MAT_NOM']);
        $infos = MatInfo::getMatInfo($myMat->getLieu() . $myMat->t_mat['MAT_NOM']);

        if ($myImageur->grabImage($myMat, $_GET['tc'], $id_image, $infos['display_width'], $infos['display_height'], $objImage)) {
            $string = $objImage->xml_export();
        }
        $string.="<msg_out>" . (!empty($myImageur->error_msg) ? $myImageur->error_msg : kSuccesImageurCapture) . "</msg_out>";
        echo "<xml>" . $string . "</xml>";

        break;

    case 'grabAndDl' :
        //trace(print_r($myMat,true));
        //trace(print_r($myImageur,true));

        if (empty($myMat->t_mat['MAT_ID_IMAGEUR'])) {
            //$imageur=stripExtension($myMat->t_mat['MAT_NOM']);
            $imageur = $myImageur->t_imageur['ID_IMAGEUR'];
            $myMat->saveImageur($imageur);
        }
        $objImage = new Image();
        //PC 30/03/11 : On récupère les dim par déft du matériel.
        require_once(modelDir.'model_materiel.php');
        $infos = MatInfo::getMatInfo($myMat->getLieu() . $myMat->t_mat['MAT_NOM']);

        if ($myImageur->downloadImage($myMat, $_GET['tc'], $id_image, $infos['display_width'], $infos['display_height'], $objImage)) {
            $string = $objImage->xml_export();
        }

        if (!empty($myImageur->error_msg)) {
            $string.="<error>" . $myImageur->error_msg . "</error>";
        } else {
            $file = klivraisonRepertoire . $objImage->t_image['IM_CHEMIN'] . "/" . $objImage->t_image['IM_FICHIER'];
            $string.="<msg_out>" . $file . "</msg_out>";
        }
        echo "<xml>" . $string . "</xml>";

        break;

    case 'generateStoryboard':
        echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8 \" /> </head><body>";

        include_once(modelDir.'model_materiel.php');
        require_once(modelDir.'model_doc.php');

        $myMat = new Materiel();
        $myMat->t_mat['ID_MAT'] = $_REQUEST['id_mat'];
        $myMat->getMat();
        // On récupère les dim par déft du matériel.
        require_once(modelDir.'model_materiel.php');
        trace('getMatInfo processStoryboard');
        $infos = MatInfo::getMatInfo($myMat->getLieu() . $myMat->t_mat['MAT_NOM']);
        // VP 30/03/11 : utilisation de display_width et display_height au lieu de video_window
        $dimensions = array($infos['display_width'], $infos['display_height']);
        //$dimensions=split('x',$infos['video_window']);
        //if (empty($dimensions)) $dimensions=array(320,240);
        $story_mode = $_POST['story_mode'];
		if($story_mode == "cutdetection"){
			$rate = null ; 
		}else{ // soit $story_mode == 'rate'
			$rate = $_POST['sb_rate'];
			if (!empty($_POST['sb_number'])) { //si un nb d'image a été renseigné
				$number = $_POST['sb_number'];
				$secs = $infos['duration'];
				if ($number > 0) {
					$rate = round($secs / $number, 3); //intervalle en sec entre images
				}
			}
			$story_mode = 'rate';
		}
        if (!empty($_REQUEST['id'])) {
            $myImageur->t_imageur['ID_IMAGEUR'] = $_REQUEST['id'];
        } else {
            $myImageur->t_imageur['IMAGEUR'] = stripExtension($myMat->t_mat['MAT_NOM']);
        }
        $myImageur->generateStoryboard($myMat, $rate, $_dumb, $dimensions[0], $dimensions[1],$story_mode);

        //pour assigner une vignette lor de la generations des story board si aucune vignette est choisie
        $myMat->getDocMat();
        $docObj = new Doc;
        foreach ($myMat->t_doc_mat as $idx => $dm) {
            //$dm['DOC']->saveImageur($myImageur->t_imageur['IMAGEUR']);
            //if(isset($id_image)) $dm['DOC']->saveVignette('doc_id_image',$id_image);
            // VP 12/10/09 : affectation de l'image en fonction du TC
            $docObj->t_doc['ID_DOC'] = $dm['ID_DOC'];
            $docObj->getDoc();
            if (empty($docObj->vignette)) {
                $docObj->saveVignette('doc_id_image', 0, $myMat->t_mat['MAT_ID_IMAGEUR'], $dm['DMAT_TCIN']);
            }
        }
        unset($docObj);

        $msg = $myImageur->error_msg;
        echo "<div id='output'>";
        if (trim($msg, '<br/>') == '') {
            echo kSuccesImageurSauve;
        } else {
            echo $msg;
        }
        echo "</div>";
        echo "<script>parent.myPanel.showResultInParent(document.getElementById('output'));</script>";
        echo "</body></html>";

        break;

    case 'generateStoryboardByFrontal':
        $mats = array();
        $mats[]['ID_MAT'] = $_REQUEST['id_mat'];
        $myPrms['id_etape'] = $db->GetOne("SELECT id_etape from t_etape e inner join t_module m on e.etape_id_module=m.id_module where module_type='STORY'");
        $myPrms['lancement'] = "proc";
        include(includeDir . "jobListe.php");
        if (!empty($jobObj->t_job["ID_JOB"])) {
            echo "<script>var id_job=" . $jobObj->t_job["ID_JOB"] . ";
								parent.updateProgressJob(id_job);
                                </script>";
        }


        break;

    case 'generateMosaic':
        echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8 \" /> </head><body>";

        include_once(modelDir.'model_materiel.php');
        require_once(modelDir.'model_doc.php');
        $myMat = new Materiel();
        $myMat->t_mat['ID_MAT'] = $_REQUEST['id_mat'];
        $myMat->getMat();
        // On récupère les dim par déft du matériel.
        require_once(modelDir.'model_materiel.php');
        $myImageur->t_imageur['ID_IMAGEUR'] = $_REQUEST['id'];
        $myImageur->generateMosaicFromMat($myMat);


        $msg = $myImageur->error_msg;
        echo "<div id='output'>";
        if (trim($msg, '<br/>') == '') {
            echo kSuccesMosaicSauve;
        } else {
            echo $msg;
        }
        echo "</div>";
        echo "<script>parent.myPanel.showResultInParent(document.getElementById('output'));</script>";
        echo "</body></html>";

        break;

    case 'saveStoryboard' :

        echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8 \" /> </head><body>";

        foreach ($_POST['ID_IMAGE'] as $idx => $id_image) {

            $myImg = new Image();
            $myImg->t_image['ID_IMAGE'] = $id_image;
            $myImg->t_image['ID_LANG'] = $_POST['ID_LANG'][$idx];
            $myImg->t_image['ID_IMAGEUR'] = $_REQUEST['id'];
            //$myImg->t_image['IMAGEUR']=$_POST['imageur'];
            $myImg->t_image['IM_TEXT'] = $_POST['IM_TEXT'][$idx];
            $myImg->t_image['IM_TC'] = $_POST['IM_TC'][$idx];
            $myImg->t_image['IM_CHEMIN'] = $_POST['IM_CHEMIN'][$idx];
            $myImg->t_image['IM_FICHIER'] = $_POST['IM_FICHIER'][$idx];

            if ($_POST['ligne_action'][$idx] == 'edit' && $myImg->t_image['ID_IMAGE']) {
                $myImg->save();
            } elseif ($_POST['ligne_action'][$idx] == 'suppr') {
                //trace(print_r($myImg,true));
                $myImg->deleteImage();
            }
            $msg.=$myImg->error_msg;
            unset($myImg);
        }
        //Sauvegarde vignette
        if ($_POST['id_doc'] && isset($_POST['vignette'])) {
            //update VG 13/03/2012 : ajout de la gestion de la langue
            $id_lang_doc = "";
            if (!empty($_POST['id_lang_doc'])) {
                $id_lang_doc = htmlentities($_POST['id_lang_doc']);
            }
            // VP 6/04/10 : utilisation de saveVignette pour la mise à jour de vignette
            //$db->Execute("UPDATE t_doc SET DOC_ID_IMAGE=".$db->Quote($_POST['vignette'])." WHERE ID_DOC=".$db->Quote($_POST['id_doc']));
            include_once(modelDir.'model_doc.php');
            $myDoc = new Doc();
            $myDoc->t_doc['ID_DOC'] = $_POST['id_doc'];
            $myDoc->saveVignette('DOC_ID_IMAGE', $_POST['vignette'], "", "", $id_lang_doc);
            unset($myDoc);
        }

        //On génère la sortie de la fonction d'abord en encapsulant le message dans un DIV
        //Puis on lance un script dans la frame appelante.
        echo "<div id='output'>";
        if (trim($msg, '<br/>') == '') {
            echo kSuccesImageurSauve;
        } else {
            echo $msg;
        }
        echo "</div>";
        echo "<script>parent.myPanel.showResultInParent(document.getElementById('output'));</script>";
        echo "</body></html>";
        break;
}
?>