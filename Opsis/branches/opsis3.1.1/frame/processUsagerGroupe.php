<?

	global $db;
	require_once(libDir."class_page.php");
	require_once(modelDir.'model_groupe.php');
	require_once(libDir.'/fonctionsXML.php');
	require_once(modelDir.'model_usager.php');


	header('Content-Type: text/html');
	
	$myPage=Page::getInstance();
	
	
	// Param�tres 
	$xsl=((isset($_GET["xsl"]) && $_GET["xsl"])?$_GET["xsl"]:'usagerGroupeListe');
	
	// Si page non pr�cis�e -> premi�re page
	if(isset($_GET["page"])){
	    $page= $_GET["page"];
	} else $page="1";

	// Param de tri
	if(isset($_GET['tri']) && !empty($_GET['tri'])){
	    $tri= strtoupper(substr($_GET["tri"],0,strlen($_GET["tri"])-1));
		if(is_numeric(intval(substr($_GET["tri"],-1)))){
			$ordre = "".substr($_GET["tri"],-1);
		}
	} else{
		$tri="ID_USAGER";
	}
	if(!isset($ordre)){$ordre=1;}

	
	// param d'affinage de la liste
	if(isset($_GET["refine"]) && !empty($_GET['refine'])){
		$refine = $_GET['refine'];
	}
	
	// Si groupe non pr�cis� ->on abandonne
	if(!isset($_GET['id_groupe']) || empty($_GET['id_groupe'])){
		return false ;
	}
	
	// GESTION AJOUT / SUPPRESSION 
	if(isset($_GET['add_item']) && !empty($_GET['add_item']) && is_numeric($_GET['add_item'])){
		$sql = "INSERT INTO t_usager_groupe (ID_USAGER,ID_GROUPE) values (".intval($_GET['add_item']).",".intval($_GET['id_groupe']).")";
		if(!$db->Execute($sql)){echo "Erreur lors de l'ajout ";}
	}
	
	if(isset($_GET['delete_item']) && !empty($_GET['delete_item']) && is_numeric($_GET['delete_item'])){
		$sql = "DELETE from t_usager_groupe WHERE ID_USAGER=".intval($_GET['delete_item'])." AND ID_GROUPE=".intval($_GET['id_groupe']);
		if(!$db->Execute($sql)){echo "Erreur lors de la suppression ";}
	}
	
	
	
	
	// Récupération des usagers en fonction de la page

	// req SQL de récupération des usagers du groupe
	/*$sql="SELECT tug.*,tu.* from t_usager_groupe tug join t_usager tu ON tug.ID_USAGER=tu.ID_USAGER WHERE ID_GROUPE=".intval($_GET['id_groupe']);
	
	// Paramètre qui permet de faire une mini recherche dans les rèsultats
	if(isset($refine) && !empty($refine)){
		$sql.=" AND ".$db->getFullTextSQL(array($refine."*"), array("US_NOM", "US_PRENOM", "US_SOCIETE"));
	}*/
	require_once(libDir.'class_chercheUsager.php');
	$mySearch = new RechercheUsager() ;
	$mySearch->prepareSQL() ; 
	$mySearch->appliqueDroits();
	
	
	$mySearch->tab_recherche[0]['FIELD']="US_NOM,US_PRENOM,US_LOGIN,eu.ETAT_USAGER,tu.TYPE_USAGER";
	$mySearch->tab_recherche[0]['VALEUR']=$mySearch->removeWeirdCharacters($_GET['refine']);
	$mySearch->tab_recherche[0]['OP']='AND';
	$mySearch->tab_recherche[0]['TYPE']='FT';
	$mySearch->makeSQL() ; 
	
	$mySearch->setOrderBy($tri.$ordre);
 	$mySearch->finaliseRequete();
	
	// Affichage :

	// echo $mySearch->sql.$mySearch->getTri();
	$myPage->nbLignes = 20;
	$myPage->page = $page;
	$myPage->tri = $tri;
	$myPage->initPager($mySearch->sql.$mySearch->getTri(), $myPage->nbLignes, $myPage->page); // init splitter object
	$myPage->params4XSL['tri'] = $tri;
	$myPage->params4XSL['ordre'] = $mySearch->triInvert; 

	// Si on passe par processUsagerGroupe, on est normalement dans une gestion ajax de la liste des usager sur groupeSaisie
	$param['listeUsagerAjax'] = "1";
	
	$param["nbLigne"] = $myPage->nbLignes ;
    $param["page"] =$myPage->page;
    $param["entete"] = 1;
	$param['tri']= $tri;
	$param['order'] =$ordre;
    $param["pager_link"]=$myPage->PagerLink;
	$param["offset"] =  $myPage->nbLignes*($myPage->page-1);
    $param["urlparams"]=$myPage->addUrlParams();
    $param["imgurl"]=imgUrl;
    $param["id_lang"]=isset($id_lang)?$id_lang:'';
	$param["defaultTitle"] = kVisioNomDefautExtrait;
	$myPage->addParamsToXSL($param);
	$myPage->afficherListe('t_usager',getSiteFile("listeDir",'usagerGroupeListe.xsl'),false,null);
	
	



?>
