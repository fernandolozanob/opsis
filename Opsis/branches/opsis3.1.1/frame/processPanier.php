<?php
// Pour masquer tous les messages (ex : Deprecated...)
error_reporting(0);
//debug($_REQUEST,'cyan',true);
require_once(libDir."class_user.php");

if (User::getInstance()->loggedIn()) {
	require_once(modelDir.'model_panier.php');
	$myCart=new Panier();
}
else {
require_once(modelDir.'model_panier_session.php');
	$myCart=new PanierSession();
}
global $db;
$id_panier=htmlspecialchars(urldecode($_REQUEST['id_panier']));
if ($id_panier=='' && get_class($myCart)=='Panier') return;
//by LD 06/04/09 : ce bug arrive de façon inexplicable, donc on récupère l'id_panier
if ($id_panier=='_session' & get_class($myCart)=='Panier') $id_panier=Panier::getDefaultPanierId();
$items=urldecode($_REQUEST['items']);
$typeItem=urldecode($_REQUEST['typeItem']);
//@update VG 10/08/10 : ajout du pan_dossier en paramètre
$pan_dossier = !empty($_REQUEST['pan_dossier'])?$_REQUEST['pan_dossier']:'0';
$commande=htmlspecialchars(urldecode($_REQUEST['commande']));
foreach ($_POST["t_panier_doc"] as $k=>$pdoc)
	if (isset($pdoc['ligne_action'])) $_POST['ligne_action'][$k] = $pdoc['ligne_action'];

$myCart->t_panier['ID_PANIER']=$id_panier;

if ($id_panier=='-1' && get_class($myCart)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
	// VP 26/8/09 : possibilité de nommer la sélection
	if($_REQUEST['pan_titre']!='') $pan_titre=$_REQUEST['pan_titre'];
	else {
		$cnt=count(Panier::getFolders(true));
		$pan_titre=kSelection.' '.$cnt;
	}
	$myCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$pan_titre,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID, "PAN_DOSSIER" => $pan_dossier);
	$myCart->createPanier();
	$creationNouvPanier = true ;
}
elseif ($id_panier=='-1')
	$creationNouvPanier = true ;

$myCart->getPanier();
$myCart->getPanierDoc();

$arrItems=explode('$',$items);

switch($commande) { //ajout d'une ligne
case 'add': //ajout
	$errDoublons=0;
	$itemsAdded = 0;
	$arrIdsItemsAdded = array(); 
	// MS 19/04/13 - possibilité d'ajouter des ligne_panier au lieu de document à partir de la page d'un panier => permet de gérer la copie d'extraits
	if(isset($typeItem) && $typeItem == "pdoc"){
		foreach ($arrItems as $id_ligne_pan) {
			if (empty($id_ligne_pan)) break;
			$ligne_panier = Panier::getLignePanier($id_ligne_pan);
			// si on essai d'ajouter au panier d'origine de la ligne, on sort.
			if($ligne_panier['ID_PANIER'] == $myCart->t_panier['ID_PANIER']){
				return ;
			}
			// on modifie la ligne panier d'origine pour créer la copie
			$ligne_panier['ID_PANIER'] = $myCart->t_panier['ID_PANIER'];
			unset($ligne_panier['ID_LIGNE_PANIER']);
			// tentative d'ajout au panier (vérification + insertion en base si OK )
			if (!$myCart->insertPanierDoc($ligne_panier,0)){
				$errDoublons++; //by ld 20/11/08 : appel muet, comptage d'erreurs
			}else{
				$arrIdsItemsAdded['ID_LIGNE_PANIER'][] = $ligne_panier['ID_LIGNE_PANIER'];
				$itemsAdded++;
			}
		}
	}else{
		foreach ($arrItems as $id_doc) {
			if(is_numeric($id_doc)){ 
				if (empty($id_doc)) break;

				if (!$myCart->insertLigne($id_doc,0)){
					$errDoublons++; //by ld 20/11/08 : appel muet, comptage d'erreurs
				}else{ 
					$arrIdsItemsAdded['ID_DOC'][] = $id_doc;
					$itemsAdded++;
				}
			}
		}
	}
	header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
	echo "<xml><msg_out>";
	if ($creationNouvPanier){
		echo kSuccesPanierCreation."\n";
	}
	if ($myCart->t_panier['PAN_ID_ETAT']>0){
		$str_success_single = kSuccesPanierDocumentAjoute;
		$str_success_plural = kSuccesPanierDocumentsAjoutes;
	}else{
		$str_success_single = kSuccesFolderDocumentAjoute ;
		$str_success_plural = kSuccesFolderDocumentsAjoutes ;
	}
	if($itemsAdded > 1 ){
		echo sprintf($str_success_plural."\n",$itemsAdded);
	}else if ($itemsAdded > 0 ){
		echo sprintf($str_success_single."\n",$itemsAdded);
	}
	if($errDoublons>0){
		echo sprintf(kErrorPanierTransfertDoublon,$errDoublons);
	}
	echo "</msg_out>";
	if(defined("kSaveVignetteAsHDForPanIds")){
		$arr_panHD_ids = explode(',',(string)(kSaveVignetteAsHDForPanIds));
		foreach($arr_panHD_ids as $panHD_id){
			if($panHD_id == $myCart->t_panier['ID_PANIER']){
				require_once(modelDir.'model_imageur.php');
				Imageur::refreshImagesPanHD($myCart->t_panier['ID_PANIER'],$arrIdsItemsAdded);
			}
		}
	}

	//PC 15/11/10 : ajout de l'id_panier et titre_panier pour la création de panier à la volée
	if ($id_panier=='-1' && get_class($myCart)=='Panier' ) {
		echo "<pan_id>" . $myCart->t_panier['ID_PANIER'] . "</pan_id><pan_titre>$pan_titre</pan_titre>";
	}
	echo '</xml>';
break;

case 'remove': //suppression d'une ligne
	if(isset($typeItem) && $typeItem == "pdoc"){
		foreach ($arrItems as $id_ligne_pan) {
			if (empty($id_ligne_pan)) break;
			$ligne_panier = Panier::getLignePanier($id_ligne_pan);
			$myCart->deletePanierDoc($ligne_panier);
			unset($ligne_panier);
		}
	}else{
		foreach ($arrItems as $id_doc) {
			if (empty($id_doc)) break;
			$myCart->removeLigne($id_doc);
		}
	}
	header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
	echo "<msg_out>";
	if (!empty($myCart->error_msg)) echo str_replace("<br/>","\n",$myCart->error_msg); else {
		if ($myCart->t_panier['PAN_ID_ETAT']>0) echo kSuccesPanierDocumentRetire; //succes
		else echo kSuccesFolderDocumentRetire; //distinction entre panier et folder (demande clients)
		}
	echo "</msg_out>";
break;

case 'remove_all': //vidage du panier
	$ok=$myCart->deleteToutesLignes();
	if ($ok) echo "<msg_out>".kSuccesPanierVider."</msg_out>";
	else echo "<msg_out>".kErrorPanierVider."</msg_out>";
break;

case 'add_all': //ajout des résultats de recherche dans le panier / dossier sélectionné

	$sql = $_SESSION['recherche_DOC']['sql'];

	// VP (27/3/09) : ajout refine éventuel

	if(isset($_SESSION['recherche_DOC']["refine"])) $sql.= $_SESSION['recherche_DOC']["refine"];

	// VP (6/10/08) : ajout tri éventuel
	if(isset($_SESSION['recherche_DOC']["tri"])) $sql.= $_SESSION['recherche_DOC']["tri"];


	// VP 2/3/09 : prise en compte paramètre useMySQL
	if (defined('useSolr') && useSolr==true)
	{
	   $entete=ob_get_contents(); //récupération entete => contenu du template
		ini_set('max_execution_time','0');
		$result=array();
		$field='id_doc';

		$solr_opts=unserialize(kSolrOptions);

		if (!empty($_SESSION['DB']))
		{
			$solr_opts['path']=$_SESSION['DB'];
		}

		$client_solr=new SolrClient($solr_opts);

		 // $client_solr['timeout']=300;echo 'i';
		$requete_solr=new SolrQuery();
		$nb_doc_groupe=200;
		$end=true;
		$i=0;
		$MaxRowPanier=gRechercheToPanierMaxRows;
		$requete_solr->unserialize($_SESSION['recherche_DOC_Solr']['sql']->serialize());
		foreach ($_SESSION['recherche_DOC_Solr']["tri"] as $champ_tri)
			$requete_solr->addSortField($champ_tri[0],$champ_tri[1]);
		foreach ($requete_solr->getFields() as $fld){
			$requete_solr->removeField ($fld);
		}
		$requete_solr->addField('id_doc');
		$requete_solr->addField('id_lang');

		if(!empty($_SESSION['recherche_DOC_Solr']['refine'])){
			$requete_solr->setQuery($requete_solr->getQuery()." AND ".$_SESSION['recherche_DOC_Solr']['refine']);
		}

		$start = microtime(true);
		while($end && ($i*$nb_doc_groupe) < $MaxRowPanier)
		{
			//trace(($i*$nb_doc_groupe)." < ".$MaxRowPanier."?");
			trace("processPanier : sql : ".$requete_solr);
			$requete_solr->setRows($nb_doc_groupe);
			$requete_solr->setStart($nb_doc_groupe*$i);
						

			$response=$client_solr->query($requete_solr);
			if($i==0)
				$numFound=$response->getResponse()->response->numFound;

			for ($k=0;$k<count($response->getResponse()->response->docs);$k++)
			{
				// $result[$k]=array();
				$result[]['id_doc']=$response->getResponse()->response->docs[$k]->id_doc;
			}
			$i++;
			if($i*$nb_doc_groupe>=$numFound) $end=false;
		}
		trace("temps exec : ".(microtime(true) - $start));
	}
	else if (defined("useSinequa") && useSinequa && $_SESSION["useMySQL"]!="1") {//Lancement de la requête de résultats selon le moteur choisi
   		require_once(libDir."sinequa/fonctions.inc");
		require_once(libDir."sinequa/Intuition.inc");
		// VP 2/9/09 : ajout espace avant SKIP
        $limit=" SKIP 0 COUNT ".gRechercheToPanierMaxRows;
        $iSession=new iSession;
  		// VP (23/10/08) : changement max_answers_count et connexion par méthode connect
		$prms= array ('host' => sinequa_host,
					  'port' => sinequa_port,
					  'read_only' => 1,
					  'charset' => in_UTF8,
					  'page_size' => empty($this->nbLignes)?$this->nbLignes:10,
					  'max_answers_count' => 500000,
					  'default-language' => $_SESSION['langue']
					  );

		//$link = $iSession->in_connect($prms);
		$link = $iSession->connect($prms);
   		$iQuery=$iSession->in_query($sql.$limit);
		//trace($sql.$limit);
   		$rows=$iQuery->the_available_tuples_count;
        for($i=0;$i<$rows;$i++) $result[$i]=$iQuery->in_fetch_array();
        if(isset($iQuery))$iQuery->close();
        if(isset($iSession))$iSession->close();
        $field='id_doc';
	} else {
		// VP 2/9/09 : ajout espace avant LIMIT
   		$limit=" LIMIT ".gRechercheToPanierMaxRows;
     	$result=$db->GetArray($sql.$limit); //exec SQL
     	$field='ID_DOC';
   	}

   	$cnt_ok=0;$cnt_nok=0;//Ajout des lignes issues du résultat de recherche avec compteur des succès et erreurs
   	foreach ($result as $row) {
   		$ok=$myCart->insertLigne($row[$field]);
   		if ($ok) $cnt_ok++; else $cnt_nok++;
   		$msg_err.=$myCart->error_msg;
   	}
   	@ob_clean(); //clean buffer obligatoire car les 2 require sinequa entrainent un pb invisible d'interprétation du xml
   	//vraisemblablement des problèmes de caractère caché ou d'encodage de ces 2 require
	// VP (6/10/08) : modification messages
	header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
    echo "<xml><msg_out>";
    if ($creationNouvPanier){
        echo kSuccesPanierCreation."\n";
    }
    echo $cnt_ok." ".kSuccesPanierDocumentsAjoutes." / ".$cnt_nok." ".kErrorLignesExistentDeja;
    echo "</msg_out>";
    echo '</xml>';

	break;

case 'save':
case 'move':
	$field=strtoupper($_REQUEST['field']);
	$value=htmlspecialchars(urldecode($_REQUEST['value']));
	//Cet ordre est très limité pour le moment puisqu'il ne sauve qu'un champ
	//on va donc éviter d'utiliser un panier->save() qui sauve tous les champs
	//donc on opte pour un simple ordre SQL... A faire évoluer par la suite
	if ($field && $id_panier) $sql="UPDATE t_panier SET ".$field."=".$db->Quote($value)." WHERE ID_PANIER=".intval($id_panier);
	$ok=$db->Execute($sql);
	if ($commande=='save') {
	if ($ok) echo "<msg_out>".kSuccesPanierSauve."</msg_out>";
	else echo "<msg_out>".kErrorPanierSauve."</msg_out>";
	} else { //move
	if ($ok) echo "<msg_out>".kSuccesPanierDeplacer."</msg_out>";
	else echo "<msg_out>".kErrorPanierDeplacer."</msg_out>";
	}
break;

case 'delete' :
	$ok=$myCart->deletePanier();
	if ($ok) echo "<msg_out>".kSuccesPanierSupprimer."</msg_out>";
	else echo "<msg_out>".kErrorPanierSupprimer."</msg_out>";
break;


case 'export':
			header("http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
			echo "<xml>";
			echo $myCart->xml_export();
			echo $myCart->xml_export_panier_doc();
			echo "</xml>";
break;
case 'view':
			header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
			$xml=$myCart->xml_export().$myCart->xml_export_panier_doc();
 			$xml = "<?xml version='1.0' encoding='UTF-8' ?".">\n<select>".$xml."</select>";
 			//trace($xml);
 			$arrProfil['currentIdDoc']=$items;
			//$log = new Logger("tsss.xml");
			//$log->Log($xml);
			$html=TraitementXSLT($xml,getSiteFile("listeDir",$_GET['xsl'].'.xsl'),$arrProfil,0,$xml);
			echo $html;

break;

case 'saveExtraits' : //utilisé pour rightPanel de gestion des extraits (visionneuse)
		// VP 4/10/10 : ajout champ PDOC_ID_IMAGE pour capture d'image
		// VP 14/10/10 : ajout champ PDOC_EXT_COTE
			echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8 \" /> </head><body>";
			foreach ($_POST['ID_LIGNE_PANIER'] as $idx=>$idLP) { //parcours du tableau des extraits
				$myLine=array(	'ID_LIGNE_PANIER'	=> $idLP,
								'ID_PANIER'			=> $id_panier,
								'ID_DOC'			=> $_POST['ID_DOC'][$idx],
								'PDOC_EXT_TITRE'	=> $_POST['PDOC_EXT_TITRE'][$idx],
								'PDOC_EXT_COTE'		=> $_POST['PDOC_EXT_COTE'][$idx],
								'PDOC_EXT_TCIN'		=> $_POST['PDOC_EXT_TCIN'][$idx],
								'PDOC_EXT_TCOUT'	=> $_POST['PDOC_EXT_TCOUT'][$idx],
								'PDOC_EXTRAIT'		=> '1',
								'PDOC_ID_IMAGE'		=> $_POST['PDOC_ID_IMAGE'][$idx],
								'PDOC_EXT_DESC'	=> $_POST['PDOC_EXT_DESC'][$idx],
								'PDOC_EXT_TAGS'	=> $_POST['PDOC_EXT_TAGS'][$idx]
				);
				$ok = true;
				if ($_POST['ligne_action'][$idx]=='edit' && !empty($_POST['PDOC_EXT_TITRE'][$idx]) ) { //mode edit + titre => save
					$ok = $myCart->savePanierDoc($myLine);
				} elseif (!empty($idLP) && $_POST['ligne_action'][$idx]=='suppr')  { //mode suppr + n° ligne => delete
					$ok = $myCart->deletePanierDoc($myLine);
				}
			}
			//On génère la sortie de la fonction d'abord en encapsulant le message dans un DIV
			//Puis on lance un script dans la frame appelante.
		// VP 14/10/10 : message de sauvegarde différent entre sélection et panier
			echo "<div id='output'>";
			if (empty($myCart->error_msg) && $ok) echo ($myCart->t_panier['PAN_ID_ETAT']>0?kSuccesPanierSauve:kSuccesSelectionSauve);
			elseif(empty($myCart->error_msg) && !$ok) echo kErrorPanierSauveExtrait;
			else echo $myCart->error_msg;
			echo "</div>";
			echo "<script>if(parent.myPanel) parent.myPanel.showResultInParent(document.getElementById('output'));
						   if (typeof(main)!='undefined' && main && main.refreshFolders) main.refreshFolders();
						   if (parent.refreshFolders) parent.refreshFolders();
				  </script>";
			echo "</body></html>";
break;

//VP 22/12/08 : ajout cas 'savePanierDoc'
case 'savePanierDoc' : //utilisé pour sauvegarder des lignes du panier
	$myCart->met_a_jour($_POST);
	$k=0; // la numérotation de t_panier_doc est farfellue, on la reconstruit
	echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8 \" /> </head><body>";
	foreach ($myCart->t_panier_doc as $pdoc) { //parcours du tableau des extraits
		if ($_POST['ligne_action'][$k]=='edit') { //mode edit => save
			$myCart->savePanierDoc($pdoc);
		} elseif ($_POST['ligne_action'][$k]=='suppr')  { //mode suppr + n° ligne => delete
			$myCart->deletePanierDoc($pdoc);
		}
		$k++;
	}
		//On génère la sortie de la fonction d'abord en encapsulant le message dans un DIV
		//Puis on lance un script dans la frame appelante.
		// VP 14/10/10 : message de sauvegarde différent entre sélection et panier
		echo "<div id='output'>";
	if (empty($myCart->error_msg)) echo ($myCart->t_panier['PAN_ID_ETAT']>0?kSuccesPanierSauve:kSuccesSelectionSauve); else echo $myCart->error_msg;
	echo "</div>";
	echo "<script>if(parent.myPanel) parent.myPanel.showResultInParent(document.getElementById('output'));
if (typeof(main)!='undefined' && main && main.refreshFolders) main.refreshFolders();
if (parent.refreshFolders) parent.refreshFolders();
</script>";
	echo "</body></html>";
	break;

	//VP 12/06/09 : ajout cas 'copyLines'
	//VP 30/06/09 : mémorisation en session des numéros de lignes seulement
case 'copyLines' : //utilisé pour copier les lignes dans un presse-papier session
				   // On vide le presse-papier session
	if(isset($_SESSION['scrap']['t_panier_doc'])) unset($_SESSION['scrap']['t_panier_doc']);
	// Récupération du tableau de lignes
	/*
	$lignes=json_decode($_GET['lignes']);
	 if(is_array($lignes)){
		 foreach ($myCart->t_panier_doc as $pdoc) { //parcours du tableau des lignes
			 if(in_array($pdoc['ID_LIGNE_PANIER'],$lignes)) $_SESSION['scrap']['t_panier_doc'][]=$pdoc;
		 }
	 }
	 */
	$_SESSION['scrap']['id_panier']=$id_panier;
	$_SESSION['scrap']['lignes']=$_GET['lignes'];
	//trace(print_r($_SESSION['scrap'],true));
	break;

case 'toggleCheckbox' : //PC 27/12/12 : permet de conserver les checkbox cochés lors du changement de page
	$arrId=explode("-", htmlspecialchars(urldecode($_REQUEST['id_ligne_panier'])));
	//$ilp=htmlspecialchars(urldecode($_REQUEST['id_ligne_panier']));
	foreach ($arrId as $ilp) {
		if (!isset($_SESSION["panier$id_panier"]["checked"][$ilp])) $_SESSION["panier$id_panier"]["checked"][$ilp] = $ilp;
		else unset($_SESSION["panier$id_panier"]["checked"][$ilp]);
	}
	break;
}
?>