<?php
	require_once(modelDir.'model_job.php');
	
	global $db ; 
	
	if(isset($_REQUEST['id_job']) && !empty($_REQUEST['id_job'])){
		$job = new Job(); 
		$job->t_job['ID_JOB'] = filter_var($_REQUEST['id_job'],FILTER_SANITIZE_NUMBER_INT);
		$job->getJob() ; 
	}
	
	if(!isset($job)){
		return false ;
	}
	
	$action = $_GET['action'];
	
	
	switch($action){
		
		case 'getProgression' : 
			$res=$db->GetArray('SELECT JOB_ID_ETAT as id_etat,t_etat_job.ETAT_JOB as etat, JOB_PROGRESSION as progress 
			FROM t_job 
			left join t_etat_job on t_job.job_id_etat=t_etat_job.id_etat_job 
			WHERE ID_JOB='.$job->t_job['ID_JOB']);
			
			if($res && !empty($res[0])){
				if(isset($_GET['progressionbar'])){
					echo round($res[0]['JOB_PROGRESSION']).'%';
					if(!empty($_GET['getState']) && $_GET['getState'] == '1') {
						echo ":".$res[0]['JOB_ID_ETAT'].":".$res[0]['ETAT_JOB'];
					}	
				}else{
					echo json_encode($res[0]);
				}
			}else{
				echo "processJob getProgression Error - sql query failed";
			}
		break ; 
		
		case 'execJobSync': 
			// lancement d'un job (et de ses fils) via execution synchrones des traitements jobProcess
			require_once(libDir.'class_jobProcess.php');
			JobProcess::execJobSync($job);
		break ; 
	}
?>