<?php

require_once(libDir."class_user.php");
require_once(modelDir.'model_doc.php');

$myDoc= new Doc();
if (empty($_REQUEST['id'])) return false;
$myDoc->t_doc['ID_DOC']=$_REQUEST['id'];
$commande=htmlspecialchars(urldecode($_REQUEST['commande']));
$myDoc->getDoc(); 
$myDoc->getLexique(); //obligé si on veut transmettre des champs LEX ou VAL aux séquences créées
$myDoc->getPersonnes();
$myDoc->getCategories();
$myDoc->getValeurs();
$myDoc->getSequences();
// VP 24=05/10 : ajout récupération valeurs
$myDoc->getMats(true);

global $db;


switch($commande) {
case 'add': //ajout
		// pas utilisé	pour le moment
break;
case 'export':
			header("http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
			echo "<xml>";
			echo $myDoc->xml_export();
			echo "</xml>";
break;
case 'view':
			header("http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
			$xml=$myDoc->xml_export();
 			$xml = "<?xml version='1.0' encoding='UTF-8' ?".">\n<select>".$xml."</select>";
			$arrParams=null;
			if(isset($_GET['prm_xsl']) && !empty($_GET['prm_xsl'])){
				if(!is_array($_GET['prm_xsl'])){
					$arrParams = (array) $_GET['prm_xsl'];
				}else{
					$arrParams = $_GET['prm_xsl'];
				}
			}
			$html=TraitementXSLT($xml,getSiteFile("listeDir",$_GET['xsl'].'.xsl'),$arrParams,0,$xml);
			echo $html;
break;

case 'saveSequences' :

			//debug($_REQUEST,'green',true);
			//obligatoire sinon IE ne comprend pas l'encodage sans tag META et fait du sale encodage
			header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
			echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8 \" /> </head><body>";
        //debug($_POST,'pink',true);
        // VP 17/07/2012 : Constitution des tableaux doc_lex et doc_val pour les nouvelles séquences
        $FORM=$_POST;
		
        
		// MS Ajout prise en charge des paramètre "inherit_pers_lex", "inherit_doc_lex", "inherit_doc_val" qui permettent d'hériter en bloc
		// les liens personnes, lexiques, valeurs du document parent vers la séquence qu'on est en train de créer;
		// cet héritage est un one-shot, une fois les liens recopiés, modifier les liens du parent n'updatera pas les liens du fils & inversement

		
		if(!empty($FORM['inherit_pers_lex']) && $FORM['inherit_pers_lex']=="1"){
			$FORM['t_doc_lex'] = $myDoc->t_pers_lex;
			foreach($FORM['t_doc_lex'] as &$doc_lex){
				unset($doc_lex['ID_DOC_LEX']);
			}
		}		

		
		if(!empty($FORM['inherit_doc_lex']) && $FORM['inherit_doc_lex']=="1"){			
			if(is_array($FORM['t_doc_lex'])){
				$FORM['t_doc_lex'] = array_merge($FORM['t_doc_lex'],$myDoc->t_doc_lex);
			}else{
				$FORM['t_doc_lex'] = $myDoc->t_doc_lex;
			}
			foreach($FORM['t_doc_lex'] as &$doc_lex){
				unset($doc_lex['ID_DOC_LEX']);
			}
		}
		else if (!empty($FORM['t_doc_lex'])){
            foreach ($FORM['t_doc_lex'] as $idx=>$lex) {
                if (isset($lex['ID_LEX']) || isset($lex['ID_PERS'])) {$lastID=$idx;$FORM['t_doc_lex'][$idx]['ID_DOC']=$mySeq->t_doc['ID_DOC'];}
                if (isset($lex['DLEX_ID_ROLE'])) {$FORM['t_doc_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM['t_doc_lex'][$idx]);}
                if (isset($lex['DLEX_ID_ROLE'])) {$FORM['t_doc_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM['t_doc_lex'][$idx]);}
                if (isset($lex['ID_TYPE_LEX'])) {$FORM['t_doc_lex'][$lastID]['ID_TYPE_LEX']=$lex['ID_TYPE_LEX'];unset($FORM['t_doc_lex'][$idx]);}
                if (isset($lex['DLEX_REVERSEMENT'])) {$FORM['t_doc_lex'][$lastID]['DLEX_REVERSEMENT']=$lex['DLEX_REVERSEMENT'];unset($FORM['t_doc_lex'][$idx]);}
                if (isset($lex['LEX_PRECISION'])) {$FORM['t_doc_lex'][$lastID]['LEX_PRECISION']=$lex['LEX_PRECISION'];unset($FORM['t_doc_lex'][$idx]);}
                if (isset($lex['ID_TYPE_DESC'])) {$FORM['t_doc_lex'][$lastID]['ID_TYPE_DESC']=$lex['ID_TYPE_DESC'];unset($FORM['t_doc_lex'][$idx]);}
            }
        }
		
		if(!empty($FORM['inherit_doc_val']) && $FORM['inherit_doc_val']=="1"){	
		debug( $myDoc->t_doc,'pink',true);
			$FORM['t_doc_val'] = $myDoc->t_doc_val;
		}
        else if (!empty($FORM['t_doc_val'])){
            foreach ($FORM['t_doc_val'] as $idx=>$val) {
                if (isset($val['ID_VAL'])) {$lastID=$idx;$FORM['t_doc_val'][$idx]['ID_DOC']=$mySeq->t_doc['ID_DOC'];}
                if (isset($val['ID_TYPE_VAL'])){
                    if(!isset($FORM['t_doc_val'][$lastID]['ID_TYPE_VAL'])) {$FORM['t_doc_val'][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];}
                    unset($FORM['t_doc_val'][$idx]);
                }
                
            }
        }		
		if(!empty($FORM['inherit_doc_cat']) && $FORM['inherit_doc_cat']=="1"){
			$FORM['t_doc_cat'] = $myDoc->t_doc_cat;
		}
        else if (!empty($FORM['t_doc_cat'])){
            foreach ($FORM['t_doc_cat'] as $idx=>$val) {
                if (isset($val['ID_CAT'])) {$lastID=$idx;$FORM['t_doc_cat'][$idx]['ID_DOC']=$mySeq->t_doc['ID_DOC'];}
                if (isset($val['ID_TYPE_CAT'])){
                    if(!isset($FORM['t_doc_cat'][$lastID]['ID_TYPE_CAT'])) {$FORM['t_doc_val'][$lastID]['ID_TYPE_CAT']=$val['ID_TYPE_CAT'];}
                    unset($FORM['t_doc_cat'][$idx]);
                }
                
            }
        }

        foreach ($_POST['ID_DOC'] as $idx=>$idSeq) { //parcours du tableau des extraits, infos de base
				$myLine=array(	'ID_DOC'			=> $idSeq,
								'DOC_ID_GEN'		=> $myDoc->t_doc['ID_DOC']
								
				);
				foreach ($_POST as $_fld=>$_val) { //ajout des champs présents dans le tableau
					if (is_array($_val) && strpos($_fld,'DOC_')===0) $myLine[$_fld]=$_val[$idx];
				}


				if ($_POST['ligne_action'][$idx]=='edit' && !empty($_POST['DOC_TITRE'][$idx]) ) { //mode edit + titre => save
                    // VP 17/07/2012 : Sauvegarde doc_lex et doc_val pour les nouvelles séquences uniquement
                    if(empty($idSeq)){
                        if (!empty($FORM['t_doc_lex'])) $myLine['t_doc_lex'] =$FORM['t_doc_lex'];
                        if (!empty($FORM['t_doc_val'])) $myLine['t_doc_val'] =$FORM['t_doc_val'];
                        if (!empty($FORM['t_doc_cat'])) $myLine['t_doc_cat'] =$FORM['t_doc_cat'];
                    }
					$docSeq[]=$myLine;

				} elseif (!empty($idSeq) && $_POST['ligne_action'][$idx]=='suppr')  { //mode suppr + n° ligne => delete
					$docSeq2Del[]=$myLine;
				}
			}
			//debug($docSeq,'lightblue',true);
			// debug($docSeq2Del,'pink',true);
			if(isset($FORM["DOC_PARENT"])){
				foreach($FORM["DOC_PARENT"] as $idx=>$val){
					$myDoc->t_doc[$idx]=$val;
				}
				$myDoc->save();
			}
			//trace("docSeq input ".count($docSeq));
			//trace("myDoc->t_doc_seq 1 : ".print_r(count($myDoc->t_doc_seq),1));
			if (!empty($docSeq)) {
				foreach($docSeq as $form_doc_seq){
					// Si edit && on a un id_doc => alors edition d'une séquence existante
					if(isset($form_doc_seq['ID_DOC']) && !empty($form_doc_seq['ID_DOC'])){
						// on cherche la séquence existante correspondante et on la remplace 
						foreach($myDoc->t_doc_seq as $idx=>$doc_seq){
							if($doc_seq['ID_DOC'] == $form_doc_seq['ID_DOC']){
								$myDoc->t_doc_seq[$idx] = $form_doc_seq ; 
								//trace("update ".$doc_seq['ID_DOC']." ".$doc_seq['DOC_TITRE']);
								break ; 
							}
						}
					}else{
						// Sinon edit & on est dans le cas d'une nouvelle séquences, qu'on ajoute. 
						$myDoc->t_doc_seq[] = $form_doc_seq;
					}
				}
				$myDoc->saveSequences();
			}
			//trace("myDoc->t_doc_seq 2 : ".print_r(count($myDoc->t_doc_seq),1));
			//trace("docSeq2Del : ".print_r(count($docSeq2Del),1));
			if (!empty($docSeq2Del)) {$myDoc->t_doc_seq=$docSeq2Del;$myDoc->deleteSequences();}

			//On génère la sortie de la fonction d'abord en encapsulant le message dans un DIV
			//Puis on lance un script dans la frame appelante.
			echo "<div id='output'>";
			if (trim($myDoc->error_msg,'<br/>')=='') echo kSuccesDocSauve; else echo $myDoc->error_msg;
			echo "</div>";
			echo "<script>if(parent.myPanel) parent.myPanel.showResultInParent(document.getElementById('output'));</script>";
			echo "</body></html>";
break;


}






?>