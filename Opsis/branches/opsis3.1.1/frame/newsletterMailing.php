<?php

/*
 * newsletterMailing.php est un script php appelé en ajax qui permet plusieurs choses dont :
 * -mettre à jour le champ US_NEWSLETTER de la table t_usager suivant la bdd du service d'emailing (actuellement mailChimp)
 * car un utilisateur peut effectivement se désinscrire de la newsletter sans passer par le site opsomai (via le lien mailChimp "se désabonner"
 * dans la newsletter-mail qu'il a reçu)
 * 
 */

if (isset($_REQUEST['debug_mode'])) {
	$debug_mode = true;
	error_reporting(E_ALL & ~E_NOTICE);
// error_reporting(E_ALL);
	ini_set('display_errors', 'On');
} else {
	error_reporting(0);
	$debug_mode = false;
}

require_once(libDir . "class_newsletter.php");
global $db;
$email = strtolower(trim($_REQUEST['email']));



if (defined('MailChimpApiKey')) {

	$MailChimpApiKey = trim(constant('MailChimpApiKey'));
	if (!empty($MailChimpApiKey)) {

		$nl = new Opsis\Newsletter(MailChimpApiKey);

		if (isset($_REQUEST['updateSite'])) {

			if (isset($_REQUEST['all'])) {


				$tabSuscribeChange = $nl->getSubscriptionChange();


				if (array_key_exists('KO', $tabSuscribeChange)) {
					echo 'Impossible de synchroniser(désabonnement via le mail) la liste des utilisateurs avec mailChimp';
					echo '<br />';
					echo $nl->tabErrorsToString($tabSuscribeChange['KO']);
				} elseif (array_key_exists('OK', $tabSuscribeChange)) {

					if (count($tabSuscribeChange['OK']['unsubscribed']) > 0) {


						$sql_updateUnsuscribedMembers = 'UPDATE t_usager SET us_newsletter=0 WHERE TRIM(LOWER(us_soc_mail)) in (';

						$n = 1;
						$countUnsuscribedMembers = count($tabSuscribeChange['OK']['unsubscribed']);
						foreach ($tabSuscribeChange['OK']['unsubscribed'] as $value) {


							$sql_updateUnsuscribedMembers.=$db->Quote($value);
							if ($countUnsuscribedMembers > 1 && $n != $countUnsuscribedMembers) {
								$sql_updateUnsuscribedMembers.=',';
							}

							$n++;
						}

						$sql_updateUnsuscribedMembers.=')';

						if ($debug_mode) {
							echo $sql_updateUnsuscribedMembers . '<br />';
						}

//		var_dump($sql_updateUnsuscribedMembers);

						if ($db->Execute($sql_updateUnsuscribedMembers)) {
//			echo '1Le paramètre Newsletter a bien été mis à jour depuis mailChimp';
//			echo '<br />';
						} else {
							echo ('MailChimp synchro(unsuscribe) echec de la requete SQL');
//			echo '1Echec de la requête de la mise à jour du paramètre Newsletter a bien été mis à jour depuis mailChimp';
//			echo '<br />';
						}
					}


					if (count($tabSuscribeChange['OK']['subscribed']) > 0) {


						$sql_updateSuscribedMembers = 'UPDATE t_usager SET us_newsletter=1 WHERE TRIM(LOWER(us_soc_mail)) in (';

						$n = 1;
						$countSuscribedMembers = count($tabSuscribeChange['OK']['subscribed']);
						foreach ($tabSuscribeChange['OK']['subscribed'] as $value) {


							$sql_updateSuscribedMembers.=$db->Quote($value);
							if ($countSuscribedMembers > 1 && $n != $countSuscribedMembers) {
								$sql_updateSuscribedMembers.=',';
							}

							$n++;
						}

						$sql_updateSuscribedMembers.=')';


						if ($debug_mode) {
							echo $sql_updateSuscribedMembers . '<br />';
						}


//		var_dump($sql_updateSuscribedMembers);

						if ($db->Execute($sql_updateSuscribedMembers)) {
//			echo '2Le paramètre Newsletter a bien été mis à jour depuis mailChimp';
//			echo '<br />';
						} else {
							echo('MailChimp synchro(suscribe) echec de la requete SQL' );
//			echo '2Echec de la requête de la mise à jour du paramètre Newsletter a bien été mis à jour depuis mailChimp';
//			echo '<br />';
						}
					}
				}

				$_SESSION['newsletterMailing']['synchro_MailingPlatform_TO_site'] = 'done';
			} else {


				if (empty($email)) {
					echo 'Paramètre email manquant';
					exit;
				}


				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					echo 'Paramètre email non valide';
					exit;
				}


				//si l'user n'existe pas dans mailChimp ou est unsuscribed (retournera false dans les 2 cas), on le désactive
				$isStatusSuscribed = $nl->isStatusSuscribed($email);
				if ($isStatusSuscribed === TRUE) {
					//on l'active seulement si le statut est actif, obsolète
//					$sql = 'UPDATE t_usager SET us_newsletter=1 WHERE us_statut=1 and TRIM(us_soc_mail)=' . $db->Quote($email);
					//en fait la cliente peut laisser un usager inactif abonné, si l'usager veut se réabonner via un lien mailchimp (qui n'existe pas encore, il y a que le désabonnement), on doit lui accorder la possibilité de recevoir la newsletter
					$sql = 'UPDATE t_usager SET us_newsletter=1 WHERE TRIM(us_soc_mail)=' . $db->Quote($email);

					if ($debug_mode) {
						echo $sql . '<br />';
					} else {
						$db->Execute($sql);
					}
				} else if ($isStatusSuscribed === FALSE) {
					$sql = 'UPDATE t_usager SET us_newsletter=0 WHERE TRIM(us_soc_mail)=' . $db->Quote($email);
					if ($debug_mode) {
						echo $sql . '<br />';
					} else {
						$db->Execute($sql);
					}
				} else {
//					var_dump($isStatusSuscribed);
					echo $nl->tabErrorsToString($isStatusSuscribed['KO']);
				}
			}
		}



		if (isset($_REQUEST['getStatusSuscribed_in_mailChimp'])) {
			if (empty($email)) {
				echo 'Paramètre email manquant';
				exit;
			}


			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				echo 'Paramètre email non valide';
				exit;
			}


			//si l'user n'existe pas dans mailChimp ou est unsuscribed (retournera false dans les 2 cas), on le désactive
			$isStatusSuscribed = $nl->isStatusSuscribed($email);
			if ($isStatusSuscribed === TRUE) {
				echo 'true';
			} else {
				echo 'false';
			}


			exit;
		}





		/**
		 * utilisé notamment lors de la suppression d'un utilisateur
		 */
		if (isset($_REQUEST['updatePlatform'])) {

			if (isset($_REQUEST['unsuscribeWithoutCreate'])) {


				if (empty($email)) {

					echo 'Paramètre email manquant';
					exit;
				}


				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

					echo 'Paramètre email non valide';
					exit;
				}


				$return_GetActiveList = $nl->getActiveList();
				if (array_key_exists('OK', $return_GetActiveList)) {
					if ($nl->isSuscriberExists($email, $return_GetActiveList['OK'])) {
						$nl_retour = $nl->save_user($email, array('status' => 'unsubscribed'));
						echo $email . " mis à jour sur la plateform d'emailing avec le statut UNSUBSCRIBED";
					} else {
						echo $email . " non existant dans la plateform d'emailing. Aucune action a été nécessaire";
					}
				} else {
					echo $nl->tabErrorsToString($return_GetActiveList['KO']);
				}
			}
		}


		//si jamais y'a une erreur (mauvaise configuration de liste par exemple) , on doit envoyer 1 flag qui dit stop (il s'agit de la clé KO=>suivi d'un message d'erreur)
		if (isset($_REQUEST['confirmSend'])) {

			$return_GetActiveList = $nl->getActiveList();
			if (array_key_exists('OK', $return_GetActiveList)) {
				$getOneList = $nl->getOneList($return_GetActiveList['OK']);

//				echo '<div style="position:relative;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_affich getOneList<pre>';
//				var_dump($getOneList);
//				echo '</pre></div><br />';

				if (empty($getOneList['name']) || !isset($getOneList['stats']["member_count"])) {
					echo json_encode(array('KO' => 'Indisponibilité temporaire de mailChimp.Veuillez réessayez plus tard'));
				} else {
					echo json_encode(array('OK' => array($getOneList['name'], $getOneList['stats']["member_count"])));
				}
			} else {
				echo json_encode($return_GetActiveList);
			}
		}
	}
}

	
