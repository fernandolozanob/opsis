<?php
// MS - dialog.php => permet d'inclure des fichier venant de formDir en suivant la règle getSiteFile
// ex call  : empty.php?urlaction=dialog&form=<filename>

global $db;
$myUser=User::getInstance();
$myPage=Page::getInstance();

 
if(isset($_GET['form']) && !empty($_GET['form'])){
	// composition nom de fichier
	$form_filename = str_replace(array('/','.'),'',$_GET['form']).".inc.php";
	$form_file = getSiteFile('formDir',$form_filename);
	
	
	if(defined('gDialogWhiteList')){
		$whitelist_site = unserialize(gDialogWhiteList);
	}else{
		$whitelist_site = array();
	}
	
	$whitelist_moteur = array(
		'menuPartage.inc.php',
		'action_panier.inc.php',
		'action_commande.inc.php'
	);
	
	$whitelist = array_merge($whitelist_moteur,$whitelist_site);
	
	// whitelist des fichiers que l'on peut inclure, à alimenter, ou mettre sous forme de constante / fichier / conf ... 
	if(!in_array($form_filename,$whitelist)){
		// message d'erreur si filename pas dans la whitelist
		echo '<div class="title_bar">'.kErreur.'<div id="close_export_menu" title="'.kFermer.'" onclick="hideMenuActions()" style="float:right;"></div></div>';
		echo "L'accès à ce formulaire n'est pas autorisé";
		return false ;
	}
	
	trace("dialog.php include form_filename : ".$form_filename." form_file ".$form_file);
	
	// inclusion effective du fichier
	if(file_exists($form_file)){
		include($form_file);
	}
}

?>