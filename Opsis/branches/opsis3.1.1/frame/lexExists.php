<?php
/*
require_once("../conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php");
*/
global $db;

//header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
header("Content-Type: text/xml");

$mode=$_REQUEST['mode'];
$type=$_REQUEST['lex_id_type_lex'];
$from=$_REQUEST['from'];
$lang=($_REQUEST['id_lang']?$_REQUEST['id_lang']:$_SESSION['langue']);
$idLex=$_REQUEST['id_lex'];
$buffLC=setlocale(LC_CTYPE,0);
setlocale ( LC_CTYPE, 'C' ); //pour gérer le strtolower
$value=$_REQUEST['value'];
//trace(strtolower($_REQUEST['value']));

require_once(modelDir.'model_lexique.php');
	$myLex=new Lexique();
	$myLex->t_lexique['LEX_TERME']=$value;
	$myLex->t_lexique['LEX_ID_TYPE_LEX']=strtoupper($type);
	$myLex->t_lexique['ID_LANG']=strtoupper($lang);
	if ($idLex) $myLex->t_lexique['ID_LEX']=$idLex;

	if (!$myLex->checkExist(false)) $str='<ok/>';
	else
	$str ="<t_lexique>\n\t<ID_LEX>".$myLex->t_lexique['ID_LEX']."</ID_LEX>\n\t<FROM>".$from."</FROM>\n\t<MODE>".$mode."</MODE>\n\t<LEX_TERME>".$myLex->t_lexique['LEX_TERME']."</LEX_TERME>\n\t<LEX_NOTE>".str_replace(array('<','>'),array('&lt;','&gt;'),$myLex->t_lexique['LEX_NOTE'])."</LEX_NOTE><ID_LANG>".$lang."</ID_LANG>\n</t_lexique>";
	echo $str;
	setlocale(LC_CTYPE,$buffLC);
	
	/*
$sql="SELECT * FROM t_lexique where LOWER(LEX_TERME)=".strtolower($db->Quote($value))."
							and LEX_ID_TYPE_LEX=".strtoupper($db->Quote($type))."
							and ID_LANG=".$db->Quote($lang);
if ($idLex) $sql.=" AND ID_LEX<>".$db->Quote($idLex); // auto exclusion de la valeur si passé en param

$idExists=$db->GetRow($sql);
if (!$idExists) $str='<ok/>';
else
$str ="<t_lexique>\n\t<ID_LEX>".$idExists['ID_LEX']."</ID_LEX>\n\t<FROM>".$from."</FROM>\n\t<MODE>".$mode."</MODE>\n\t<LEX_TERME>".$idExists['LEX_TERME']."</LEX_TERME>\n\t<LEX_NOTE>".str_replace(array('<','>'),array('&lt;','&gt;'),$idExists['LEX_NOTE'])."</LEX_NOTE><ID_LANG>".$lang."</ID_LANG>\n</t_lexique>";
echo $str;
setlocale(LC_CTYPE,$buffLC);
	 */
?>