<?
require_once(modelDir.'model_doc.php');
global $db;

//id_doc
$id_dnd=intval($_REQUEST['id_dnd']);
//cat dest
$drop=intval($_REQUEST['drop']);
//cat origine
$drag=intval($_REQUEST['drag']);
// type cat
$type_cat=trim($_REQUEST['id_type_cat']);

$doc = new Doc(); 
$doc->t_doc['ID_DOC'] = $id_dnd;
$doc->t_doc['ID_LANG'] = $_SESSION['langue'];
$doc->getCategories() ;

if(!empty($drop) && !empty($drag) && $drag != $drop){
	foreach($doc->t_doc_cat as $idx=>$doc_cat){
		if($doc_cat['ID_CAT'] == $drag){
			unset($doc->t_doc_cat[$idx]);
			break;
		}
	}
	$doc->t_doc_cat[] = array('ID_CAT'=>$drop,'ID_TYPE_CAT'=>$type_cat);
	// la fonction saveDocCat embarque tous les fonctionnements necessaires (y compris les mises à jour des vignettes)
	$doc->saveDocCat() ; 
	echo $drop;
}

?>