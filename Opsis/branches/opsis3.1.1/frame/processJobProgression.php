<?php 
     require_once("conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php"); 	
     require_once(libDir."class_page.php"); 
	 global $db;
	 
	 
	if(!isset($_REQUEST['id_job']) || empty($_REQUEST['id_job']) || !is_int(intval($_REQUEST['id_job']))){
		echo "processJobProgression Error - id_job undefined";
		exit ; 
	}
	 
	if(isset($_GET['progressionFull'])){
		$res_full=$db->GetArray('SELECT JOB_ID_ETAT as id_etat,t_etat_job.ETAT_JOB as etat, JOB_PROGRESSION as progress 
			FROM t_job 
			left join t_etat_job on t_job.job_id_etat=t_etat_job.id_etat_job 
			WHERE ID_JOB='.intval($_REQUEST['id_job']));
		// echo round($res_moyenne).'%';
		
		if($res_full && !empty($res_full[0])){
			echo json_encode($res_full[0]);
		}else{
			echo "processJobProgression Error - sql query failed";
		}
		
	}
	 
	//barre de progression appell� dans commandeRecap2
	if(isset($_GET['progressionbar'])){
		$rsStatusJob=$db->GetRow('SELECT JOB_PROGRESSION, JOB_ID_ETAT, ETAT_JOB FROM t_job inner join t_etat_job on t_job.JOB_ID_ETAT = t_etat_job.ID_ETAT_JOB WHERE ID_JOB='.intval($_REQUEST['id_job']));
		
		echo round($rsStatusJob['JOB_PROGRESSION']).'%';
		if(!empty($_GET['getState']) && $_GET['getState'] == '1') {
			echo ":".$rsStatusJob['JOB_ID_ETAT'].":".$rsStatusJob['ETAT_JOB'];
		}
	}

?>