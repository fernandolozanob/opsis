<?
require_once(libDir."class_chercheJob.php");
require_once(modelDir.'model_job.php');
global $db;
// MS - 19.07.17 - Utilise pour l'instant encore l'ancienne version de l'execution des recherches qui passe par la classe page, il faudra r�fl�chir au moment du controller_job comment adapter le code qu'on trouve ici. 
$mySearch=new RechercheJob();
if (!isset($myPage) && isset($this)) $myPage = Page::getInstance();
$myUser=User::getInstance();


// VP 4/10/2016 : possibilit� de passer une variable session (cas jobRecordListe)
if(!empty($_POST['sessVar'])){
    $mySearch->sessVar = $_POST['sessVar'];
}

if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	$sql=$_SESSION[$mySearch->sessVar]["sql"];
	$sqlSource=$sql;

	$tabSql = array("" => "WHERE j.JOB_ID_ETAT not in (".jobErreur.",".jobFini.") AND ",
					"fini" => "WHERE j.JOB_ID_ETAT in (".jobFini.") AND ",
					"erreur" => "WHERE j.JOB_ID_ETAT in (".jobErreur.") AND ");
	if (isset($_POST['display'])) {
		$param["urlparams"] = "&display=".$_POST['display'];
		$sql = str_replace("WHERE ", $tabSql[$_POST['display']], $sql);
	}
	else $sql = str_replace("WHERE ", $tabSql[""], $sql);
	
	if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

	//Sauvegarde du nombre de lignes de recherche ? afficher :
	if($_REQUEST['nbLignes'] != '') {
			$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

	// MS - 16.06.15 - ceci corrige le refresh du suivi des traitements qui change de page. 
	// POUR UNE CORRECTION PLUS EN PROFONDEUR, il faudrait refaire une passe sur initPager. 
	// lorsqu'on init la classe myPage, myPage->page est set � 1
	// lorsqu'on passe dans initPager, on appelle getPageFromUrl, qui ne fait rien, car pas de $_REQUEST[page], 
	// => dans initPager, lorsqu'on test la valeur de myPage->page pour savoir si on r�cup�re les "altVar", en l'occurence $_SESSION[$mySearch->sessVar]["page"] (qui a la valeur correcte de la page), 
	// 		myPage->page=1, alors qu'on ne prends les altVar que si myPage->page =='' .... 
	$myPage->page = $_SESSION[$mySearch->sessVar]["page"];

			
	if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;
	$myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object
	
	print("<div class='errormsg'>".$myPage->error_msg."</div>");

	$param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	$param["page"] = $myPage->page;
	$param["titre"] = urlencode(kTraitement);
	$param["jobEnCours"] = jobEnCours;
	$param["jobFini"] = jobFini;
	$param["userID"] = $myUser->UserID;
	$param["profil_lvl_admin"] = kLoggedAdmin;
	if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
	else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
	$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
	$param["xmlfile"]=getSiteFile("listeDir","xml/jobListe.xml");
	
	unset($myPage->params4XSL['scripturl']);
	$param["scripturl"] = $_POST['scripturl'];
	$myPage->addParamsToXSL($param);

	//Ajout du parametre nb_rows pour le retrouver dans la sortie html
	$nbActif = $db->getOne(str_replace("WHERE ", $tabSql[""], "SELECT count(j.JOB_ID_ETAT) " . substr($sqlSource, strpos($sqlSource, "FROM "))));
	$nbFini = $db->getOne(str_replace("WHERE ", $tabSql["fini"], "SELECT count(j.JOB_ID_ETAT) " . substr($sqlSource, strpos($sqlSource, "FROM "))));
	$nbErreur = $db->getOne(str_replace("WHERE ", $tabSql["erreur"], "SELECT count(j.JOB_ID_ETAT) " . substr($sqlSource, strpos($sqlSource, "FROM "))));
	echo "<nb_rows_act>".$nbActif."</nb_rows_act>";
	echo "<nb_rows_fin>".$nbFini."</nb_rows_fin>";
	echo "<nb_rows_err>".$nbErreur."</nb_rows_err>";
	echo "<nb_rows>".$myPage->found_rows."</nb_rows>";
	
    if(!empty($_POST['xsl'])){
        $xsl = $_POST['xsl'];
    }else{
        $xsl = "jobListe.xsl";
    }
	$job_aff = $_POST["job_aff"];
	if ($job_aff > 1) {
        if(!empty($_POST['xsl'])){
            $xsl = $_POST['xsl'];
        }else{
            $xsl = "jobListe$job_aff.xsl";
        }
		//R�cup�ration enfants
		$res = $mySearch->searchChildren();
		$xml = TableauVersXML($res,"t_job_child",4,"select",0);
		$myPage->afficherListe("t_job",getSiteFile("listeDir",$xsl),false,$_SESSION[$mySearch->sessVar]['highlight'], $xml);
	}
	else $myPage->afficherListe("t_job",getSiteFile("listeDir",$xsl),false,$_SESSION[$mySearch->sessVar]['highlight']);
	
	//Ouverture affichage hi�rarchique
	$arrJobOpen = explode(",", $_POST['arr_JobOpen']);
	?><script><?
		foreach ($arrJobOpen as $id=>$job)
			if ($job == "1") {
				echo '$j("#row_job_'.$id.' img.toggleButton").click();';
			}
	?></script><?
}
?>
