<?php
	
	// Si action = update , alors merge des options envoy�es en param�tre 
	if(isset($_GET['action']) && $_GET['action'] == 'update' && !empty($_POST['sessLayout'])){
		$current_layout_opts = $_SESSION['USER']['layout_opts'];
		$new_layout_opts = json_decode($_POST['sessLayout'],true); 
		$_SESSION['USER']['layout_opts'] = merge_array_recurs($current_layout_opts,$new_layout_opts);
		//trace("sessLayout update : ".print_r($_SESSION['USER']['layout_opts'],true));
	}
	
	header('Content-Type: application/javascript');
	
	// si action = buildLocalStorage , alors construction de l'objet JS repr�sentant les options layout
	echo "// definition sessLayout objects : \n";
	echo "try{localStorage.sessLayoutJSON = '".json_encode($_SESSION['USER']['layout_opts'])."';}catch(e){console.log(e);}\n";
	echo "sessLayout = JSON.parse('".json_encode($_SESSION['USER']['layout_opts'])."');\n";
	
	
	
	
?>