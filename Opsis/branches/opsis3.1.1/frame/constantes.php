<? header('Content-Type: text/javascript');
	//require_once(confDir."/conf.inc.php"); //TODO à enlever provoque une fatal error Fatal error: Cannot redeclare class configuration in /var/www/html/dev/babou/cfrt/www/conf/class_configuration.php on line 4, logiquement est toujours appelé via empty.php, donc recupere la conf, a éclaircir
	require_once(confDir."/initialisationGeneral.inc.php");
	require_once(libDir."session.php");


	$str_consts = "kCheminHttp,kCheminHttpMedia,kVisionnageUrl,kLivraisonLocaleUrl,libUrl,kDocumentUrl,designUrl,imgUrl,storyboardChemin,jobAnnule,jobErreur,jobFini,gQuickOrderDownloadSeuilFilesize,gQuickOrderDownloadSeuilNbItems,coreVersion,kAnneeCopyright";

	$constantes = explode(',',$str_consts);

	echo "ow_const = { ";
	$first_const = true;
	foreach($constantes as $c ){
			if($first_const == true){
				$first_const = false ;
				echo "\n";
			}else{
				echo ",\n";
			}
			echo "\t".$c.' : "'.constant($c).'" ';
	}
	echo "\n};\n";

	echo "ow_const['langue'] = \"".$_SESSION['langue']."\" ;\n ";



$str_consts_lang = 'kAbandonnerChangements,
kAfficherTopResults,
kAfficherTout,
kAnnuler,
kConfirmerAbandon,
kConfirmDocSupprimer,
kConfirmerSuppression,
kConfirmJSAllLineSuppression,
kConfirmJSDocDuplication,
kConfirmJSDocSuppression,
kConfirmJSDocSupprVersion,
kConfirmJSFichierSuppression,
kConfirmJSLineSuppression,
kConfirmJSRestoreAll,
kConfirmPanierCreation1,
kConfirmPanierCreation2,
kConfirmPanierDeplacer,
kConfirmPanierDupliquer,
kConfirmPanierMove,
kConfirmPanierRename,
kConfirmPanierSupprimer,
kConfirmPanierVider,
kConfirmPanierTransmettre,
kConfirmPurgeFichier,
kConfirmViderpanier,
kCreerExtrait,
kCritere,
kDOMlimitvalues,
kDOMnodouble,
kElementPanier,
kElementsPanier,
kErreur,
kErreurChampsOblig,
kErreurChampsObligJS,
kErrorImageurCapture,
kErrorImageurCaptureExisteDeja,
kErrorImageurCaptureNoPlayer,
kErrorJSImageurNoRate,
kErrorPanierCreationNoName,
kErrorPanierDeplacerDejaParent,
kErrorPanierDeplacerDossierInterdit,
kErrorPanierMoveSame,
kErrorPanierRenameVide,
kErrorPanierTransmettreNoUsager,
kExportBarTextDemarrage,
kExportBarTextFop,
kExportBarTextReady,
kExportBarTextRunning,
kExtrait,
kFermer,
kFormat,
kImageurWaitGeneration,
kImageWaitCapture,
kJSConfirmExit,
kJSConfirmPanierTransformer,
kJSConfirmPanierTransformerThema,
kJSConfirmSauverModif,
KJSCreerSelection,
kJSErrorDocAccSeq,
kJSErrorOblig,
kJSErrorTitreOblig,
kJSErrorValeurExisteDeja,
kJSErrorValeurOblig,
kJSMontageConfirmSupprClip,
kJSMontageRatioChange,
kJSMsgSelectClip,
kJSReferenceNonTrouvee,
kMediaQualityName_HD,
kMediaQualityName_HQ,
kMediaQualityName_SD,
kMontage,
kMontageSauvegardeConfirm,
kMosaicWaitGeneration,
kMsgNoSelectLignes,
kMsgNoSelectLignes,
kMsgSauvegardeModif,
kNouveauPanier,
kNom,
kPanier,
kQuickOrderMsgDirectDownload,
kQuickOrderMsgDownload,
kQuickOrderMsgError,
kQuickOrderMsgTraitement,
kQuickOrderMsgTranscodage,
kQuickOrderMsgRefCommande,
kQuickOrderRefToLastCommande,
kSansTitre,
kSauverExtrait,
kSelection,
kSousTitrage_ajaxResponseDelayExceeded,
kSousTitrage_alreadyNewLineSubtitle,
kSousTitrage_beforeExportIsModifiedWarning,
kSousTitrage_deleteLineWarning,
kSousTitrage_disabledAlert,
kSousTitrage_enregistrerLigne,
kSousTitrage_errorTc_pioche1,
kSousTitrage_errorTcin_mm,
kSousTitrage_errorTcin_ss,
kSousTitrage_errorTcin1,
kSousTitrage_errorTcin2,
kSousTitrage_errorTcout_mm,
kSousTitrage_errorTcout_ss,
kSousTitrage_errorTcout1,
kSousTitrage_errorTcout2,
kSousTitrage_errorTcout3,
kSousTitrage_errorText,
kSousTitrage_errorText2,
kSousTitrage_generateDiffMatIncrustedWithSubtitleError,
kSousTitrage_generateDiffMatIncrustedWithSubtitleSuccess,
kSousTitrage_publishTechnicalError,
kSousTitrage_publishUnpublishOtherFirst,
kSousTitrage_saveCurrentWarning,
kSousTitrage_saveFileKO,
kSousTitrage_saveFileOK,
kSousTitrage_saveFileWarning,
kSousTitrage_subtitledVersionGenNoMatAfterModif,
kSousTitrage_transcription_confirmAlreadyExist,
kSousTitrage_transcription_processed,
kSousTitrage_transcriptionConfirm,
kSousTitrage_transcriptionLangMissing,
kTCin,
kTCout,
kType,
kUploadFtpFileNotFound,
kUtiliserPositionCommeDebut,
kUtiliserPositionCommeFin,
kVide,
kVisioNomDefautExtrait,
titre_trop_long_linkedin,
titre_trop_long_twitter';



	$const_langues = preg_split("/,\\r?\\n?/",$str_consts_lang);
	if(defined('gAddConstToStrLang')){
		$add_const_to_str_lang = explode(',',gAddConstToStrLang);
		if(is_array($add_const_to_str_lang) && !empty($add_const_to_str_lang)){
			$const_langues = array_merge($const_langues,$add_const_to_str_lang);
		}
	}
	sort($const_langues,SORT_STRING);

	echo "str_lang = { ";
	$first_const = true;
	foreach($const_langues as $c ){
		if(defined($c)){
			if($first_const == true){
				$first_const = false ;
				echo "\n";
			}else{
				echo ",\n";
			}
			echo "\t".$c.' : "'.str_replace('"','\"',constant($c)).'" ';
		}
	}
	echo "\n};\n";



?>



