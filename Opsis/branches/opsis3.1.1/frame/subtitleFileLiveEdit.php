<?php

header("Content-Type:text/vtt;charset=utf-8");
//header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date dans le passé




session_start();
//echo '<pre>';
//var_dump($_SESSION['sousTitrageEditionVolee']);
//echo '</pre>';
//var_dump($_REQUEST['id_doc']);

$id_doc = (int) $_REQUEST['id_doc'];


$id_mat = (int) $_SESSION['sousTitrageEditionVolee']["activeSubtitle"][$id_doc]["id_mat"];


if (!empty($id_mat)) {


    $subtitlesMat = $_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat];

    if (is_array($subtitlesMat) && !empty($subtitlesMat)) {

        ob_start();


        /* Attention ! mettre les \n entre guillemets doubles (et pas guillemet simple '\n') pour que cela soit correctement interpreté !!!! */
        echo "WEBVTT\n";

        $i = 0;
        foreach ($subtitlesMat as $k => $v) {
            echo "\n$i\n";
            echo "$k --> " . $v["tcout"] . "\n";
            echo html_entity_decode($v["text"], ENT_QUOTES, 'UTF-8') . "\n"; //pour éviter ex: L&#39;A54, une des autoroutes
            $i++;
        }


//        echo "0\n";
//        echo "00:00:01.290 --> 00:00:16.110\n";
//        echo "Christina";

        ob_end_flush();
    }
}
