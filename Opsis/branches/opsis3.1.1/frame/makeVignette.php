<?php

	function getVignetteFromPDF($imageSrc) {
		$imageSrcPDF = $imageSrc;
		try {
			$img = new Imagick();
                        /* NB : 2015-04-16 ->readImage() ne fonctionne pas sur tous les pdf */
			$img->readImage($imageSrc);
			//update VG 11/10/2012 : ajout de la gestion du cas où l'extension .pdf est en majuscules : str_replace => str_ireplace
			$imageSrc = str_ireplace('.pdf', '.jpg', $imageSrc);
			$imageSrc = str_replace(kDocumentDir, kThumbnailDir, $imageSrc);

                        /* B.RAVI 2015-04-16 ->writeImage peut renvoyer FALSE et foirer si le repertoire intermediaire n'est pas crée
                         * Exemple concret :
                         * kDocumentDir = /mnt/nas-intranet/media/bycn/public/doc_acc
                         * kThumbnailDir = /mnt/nas-intranet/media/bycn/public/thumbnails/
                         *
                         * $imageSrc = str_replace(kDocumentDir, kThumbnailDir, $imageSrc);
                         *
                         * $imageSrc = /mnt/nas-intranet/media/bycn/public/doc_acc//0000/Classeur1.pdf
                         *
                         * si le repertoire 0000 n'est pas crée préalablement=> ->writeImage() renvoie false
                         */

                        $folderToCreate=str_replace(kThumbnailDir,'', $imageSrc);
                        $folderToCreate=explode('/',$folderToCreate);

                        array_pop($folderToCreate);

                        $stringFolderToCreate=null;
                            foreach ($folderToCreate as $v){
                            if (trim($v)!==''){ //car parfois on a // dans le chemin
                                $stringFolderToCreate.='/'.$v;
                            }
                        }

                        if (!is_dir(str_replace('//','/',kThumbnailDir).$stringFolderToCreate)){
                            if (!mkdir(str_replace('//','/',kThumbnailDir).$stringFolderToCreate, 0777, true)) {
                            die('Echec lors de la création des répertoires intermediaire...');
                            }
                        }


			$img->writeImage($imageSrc);

		} catch (Exception $e) {
			$img->clear();
			$img->destroy();
			trace('PDF endommage : reparation : '.$imageSrcPDF);
			$imageSrcRepaired = stripExtension($imageSrcPDF)."_tmp.".getExtension($imageSrcPDF);
			if(!file_exists($imageSrcRepaired)) {
				$cmd = kPdftk." ".$imageSrcPDF." output ".$imageSrcRepaired;
				exec($cmd);
				trace($cmd);
			}

			try {
				$img = new Imagick();
				$img->readImage($imageSrcRepaired);
				$imageSrc = str_ireplace('.pdf', '.jpg', $imageSrc);
				$imageSrc = str_replace(kDocumentDir, kThumbnailDir, $imageSrc);
				$img->writeImage($imageSrc);
			} catch (Exception $e) {
				if(defined("gMailJobErreur")) {
					$headers="Content-Type: text/html; charset=ISO-8859-1\r\nFrom:".gMail;
					$sujet="Echec creation vignette depuis un PDF";
					$message="PDF : ".$imageSrcPDF;
					$message.="<br/>Message d'erreur imagick : ".$e->getMessage();
					mail(gMailJobErreur,$sujet,$message,$headers);
				}
				trace("Echec creation vignette depuis un PDF : ".$imageSrcPDF);
			}
		}
		$img->clear();
		$img->destroy();

		return $imageSrc;
	}

	function tiff_to_jpeg($file_path) {
	  // transforme un tif en jpg

	  // exec("convert -quality 100 " . $file_path . " " . $file_path . ".jpg");
		$image = new Imagick($file_path);
		$image->setImageFormat('jpg');
		$tif=array('.tiff','.tif');
		$file_path = str_ireplace($tif, '', $file_path);
		$image->writeImage($file_path.'.jpg');
		return $file_path.'.jpg';
    }
    
    
    ob_start(); // au cas où on est un echo quelque part

	if (!is_dir(kThumbnailDir)) $thumbDirOk=mkDir(kThumbnailDir); //crea thumbnails
	else $thumbDirOk=true;

	//@update VG 17/08/2010 : ajout du param "kr" => "keep ratio" : définit si on garde les memes proportions ou non
	$prefix=((isset($_GET['ol']) && $_GET['ol'])?$_GET['ol']."_":"").((isset($_GET['w']) && $_GET['w'])?$_GET['w']."_":"").((isset($_GET['h']) && $_GET['h'])?$_GET['h']."_":"").((isset($_GET['kr']) && $_GET['kr'])?$_GET['kr']."_":"");
	//on préfixe sur la base du fichier Overlay + largeur + hauteur
	//Ceci est fait pour distinguer différentes versions d'une même image de départ
	$thumb=kThumbnailDir.$prefix.((dirname($_GET['image'])!='')?str_replace('/','_',dirname($_GET['image'])):'').basename($_GET['image']);

// VP 16/03/10 : ajout paramètre type pour spécifier le dossier doc_acc ou storyboard
// VP 01/09/10 : ajout cas video
	if (isset($_GET['type']))
	{
		switch($_GET['type']){
			case 'storyboard': $folder=kStoryboardDir;break;
			case 'doc_acc': $folder=kDocumentDir;break;
			case 'video':
				require_once(modelDir.'model_materiel.php');
				/*$folder=Materiel::getLieuByIdMat(basename($_GET['image']));*/
				$myMat = new Materiel();
				$myMat->t_mat['ID_MAT'] = $_GET['image'];
				$myMat->getMat();
				$imageSrc = $myMat->getFilePath();
				if(isset($thumb) && strpos(basename($_GET['image']),'.')===false){
					$thumb.='.jpg';
				}
				// trace('makeVignette ID_MAT : '.$_GET['image'].' '.$imageSrc);
				break;
			case 'doc':
				require_once(modelDir.'model_doc.php');
				/*$folder=Materiel::getLieuByIdMat(basename($_GET['image']));*/
				$myDoc = new Doc();
				$myDoc->t_doc['ID_DOC'] = $_GET['image'];
				$myDoc->getDoc();
				$myDoc->getVignette();
				$imageSrc = kCheminLocalMedia.$myDoc->vignette;
/*				if(isset($thumb) && strpos(basename($_GET['image']),'.')===false){
					$thumb.='.jpg';
				}*/
				// trace('makeVignette ID_DOC : '.$_GET['image'].' '.$imageSrc);
				break;
			default: $folder=kCheminLocalMedia;break;
		}
	}
	elseif (is_file($_GET['image'])) $imageSrc = $_GET['image'];
	else
		$folder=kCheminLocalMedia;
		// error_reporting(E_ALL);
	if(empty($imageSrc)){
		//MS - ajout urldecode => permet de passer certains caractères spéciaux si encodage actif sur l'appel depuis le site,
		// n'a aucun effet sur des chaines non encodées
		$_GET['image'] = urldecode($_GET['image']);
                
                /*
                * @author B.RAVI 2016-01-12
                * Sur le site du CNRS, on se retrouve avec des noms de dossiers avec espaces 
                * ex: makeVignette.php?image=/storyboard/nom dossier/image.jpg
                * or auparavant on ne recherchait que dans nom+dossier/image.jpg (on a besoin de rechercher dans nom dossier/image.jpg AVANT)
                */
				$imageSrc=$folder."/".$_GET['image']; 
                if (!file_exists($imageSrc)){
					$imageSrc=$folder."/".str_replace(' ','+',$_GET['image']); //cas spécial du + dans le nom d'image, expérimental
                }
	}
	// MS - 17.03.16 - makeVignette affiche maintenant par défaut l'image <site>/design/images/nopicture.gif lorsque le fichier n'existe pas / que $GET[image] est vide
	if (!file_exists($imageSrc) || is_dir($imageSrc)) {
		trace('makeVignette.php - vignette no source '.$imageSrc);
		$imageSrc = kCheminLocalSurServeur.'/design/images/nopicture.gif';
		$thumb=kThumbnailDir.$prefix.((dirname($imageSrc)!='')?str_replace('/','_',dirname($imageSrc)):'').basename($imageSrc);
	}
	$mime=getMimeType($imageSrc);
	switch ($mime) {
		case 'image/pdf':
		case 'image/tiff':		
			$thumb .='.jpg';
			break;
	}	

 	//Si param recreate, on efface l'image existante (utile pour du débug et tests)
	if (isset($_GET['recreate']) && $_GET['recreate'])
		@unlink($thumb);

   // VP 18/02/13 : test date fichier pour recréation thumbnail si nécessaire
	$dateImSrc = filemtime($imageSrc);
	if(file_exists($thumb) && !empty($dateImSrc) && $dateImSrc>filemtime($thumb)) @unlink($thumb);

    ob_end_clean();
    
	if ( $thumbDirOk && file_exists($thumb)) {
		//On a déjà un thumbnail ? on le charge
		header("Content-type: image/jpeg");
        header("Content-Length: ".filesize($thumb));
		print file_get_contents($thumb);
		exit; //et on sort
	}
	if (isset($_GET['w']))
		$dst_w=$_GET['w'];
	else
		$dst_w=0;

	if (isset($_GET['h']))
		$dst_h=$_GET['h'];
	else
		$dst_h=0;

	$keepRatio=(empty($_GET['kr'])?false:$_GET['kr']); // (bool)
	if (isset($_GET['ol']) && $_GET['ol']) $overlay=getSiteFile("imageDir",$_GET['ol'].'.png');
	if (isset($overlay) && $overlay && !file_exists($overlay)) {trace('vignette no overlay '.$overlay);$overlay=null;}


	ini_set('gd.jpeg_ignore_warning', 1); //Pour forcer le chargement même qd les images sont corrompues
	ini_set("memory_limit","512M");

	//by LD 13/02/09 => détection mime pour pouvoir faire des vignettes jpeg à partir de tt type
	$mime=getMimeType($imageSrc);
	switch ($mime) {
		case 'image/jpeg':
			$src_im = ImageCreateFromJpeg($imageSrc); break;
		case 'image/gif':
			$src_im = ImageCreateFromGif($imageSrc); break;
		case 'image/png':
			$src_im = ImageCreateFromPng($imageSrc); break;
		case 'image/pdf':
			$imageSrc = getVignetteFromPDF($imageSrc);
			$src_im = ImageCreateFromJpeg($imageSrc);
			break;
		case 'image/tiff':
			$imageSrc =tiff_to_jpeg($imageSrc);
			$src_im =  ImageCreateFromJpeg($imageSrc); break;
			break;

	}



	if ($src_im=='') die ('impossible to open image');
	ImageAlphaBlending($src_im, true);
	$size = GetImageSize($imageSrc);
	$src_w = $size[0];
	$src_h = $size[1];
	//taille de votre image
	// Contraint le rééchantillonage à une largeur fixe
	// Maintient le ratio de l'image
	if ($dst_w && !$dst_h) $dst_h = round(($dst_w / $src_w) * $src_h); //largeur sans hauteur
	if (!$dst_w && $dst_h) $dst_w = round(($dst_h / $src_h) * $src_w); //hauteur sans largeur

	if (!$dst_w && !$dst_h) {$dst_h=$src_h;$dst_w=$src_w;} //pas de size : on garde la taille originale

	//@update VG 17/08/2010
	//On prend les dimensions qui se rapprochent le plus possible de la demande, tout en conservant le ratio
	if($keepRatio == 'cover' && $dst_w && $dst_h){
		$dst_h_temp = round(($dst_w / $src_w) * $src_h);
		$dst_w_temp = round(($dst_h / $src_h) * $src_w);
		
		if($dst_h_temp > $dst_h){
			$offset_h = ($dst_h_temp - $dst_h)/2;
			$offset_w = 0 ; 
			$dst_h = $dst_h_temp;
			
		}elseif($dst_w_temp > $dst_w){
			$offset_w = ($dst_w_temp -$dst_w)/2;
			$offset_h = 0 ; 
			$dst_w = $dst_w_temp;
		}
		// trace("w :".$dst_w." h:".$dst_h." offX:".$offset_w." offY:".$offset_h);
	}else if($keepRatio && $dst_w && $dst_h) {
		$dst_h_temp = round(($dst_w / $src_w) * $src_h);
		$dst_w_temp = round(($dst_h / $src_h) * $src_w);
		
		if($dst_h_temp > $dst_h) $dst_w = $dst_w_temp;
		elseif($dst_w_temp > $dst_w) $dst_h = $dst_h_temp;
	}

	
	//trace('size '.$dst_w.'x'.$dst_h);

	if($keepRatio && $keepRatio == 'cover'){
		
		$resizedSRC = ImageCreateTrueColor(($dst_w-(2*$offset_w)),($dst_h-(2*$offset_h)));
		$resizedSRCTmp = ImageCreateTrueColor($dst_w,$dst_h);
		// $resizedSRC = ImageCrop($resizedSRC,array('x'=>$offset_w,'y'=>$offset_h,'width'=>($dst_w-(2*$offset_w)),'height'=>($dst_h -(2*$offset_h))));
		ImageCopyResampled($resizedSRCTmp,$src_im,0,0,0,0,$dst_w,$dst_h,$src_w,$src_h);		
		$dst_w = $dst_w-(2*$offset_w); 
		$dst_h = $dst_h-(2*$offset_h); 
		ImageCopyResampled($resizedSRC,$resizedSRCTmp,0,0,$offset_w,$offset_h,$dst_w,$dst_h,$dst_w,$dst_h);
		unset($offset_w,$offset_h,$resizedSRCTmp);
	}else{
		$resizedSRC = ImageCreateTrueColor($dst_w,$dst_h);
		ImageCopyResampled($resizedSRC,$src_im,0,0,0,0,$dst_w,$dst_h,$src_w,$src_h);
	}
	if (isset($overlay) && $overlay) { //ajout d'un logo PNG
		//trace('logo');
		$logoImage = ImageCreateFromPNG($overlay);
		ImageAlphaBlending($logoImage, false);
		ImageSaveAlpha($logoImage, true);
		$src_w = ImageSX($logoImage);
		$src_h = ImageSY($logoImage);
		$resizedOL = $logoImage;
		$resizedOL = ImageCreateTrueColor($dst_w,$dst_h);
		
		ImageAlphaBlending($resizedOL, false);
		ImageSaveAlpha($resizedOL, true);
		$trans_colour = imagecolorallocatealpha($resizedOL, 0, 0, 0, 127);
		ImageFill($resizedOL,0,0,$trans_colour);
		// MS 11/09/13 Nouvelle méthode pour l'overlay :
		// 	l'image d'overlay (player.png) par defaut est carrée,
		//  on la scale en fonction de la hauteur de la vidéo,
		//  on la centre horizontalement
		$new_h = $dst_h;
		$new_w = $src_w*$new_h/$src_h;
		$coord_x = ($dst_w-$new_w)/2;
		ImageCopyResampled($resizedOL,$logoImage,$coord_x,0,0,0,$new_w,$new_h,$src_w,$src_h);
		$backup=$resizedSRC;
		$ok=imagecopy($resizedSRC, $resizedOL, 0, 0, 0, 0, $dst_w, $dst_w); //fusion
		if (!$ok)$resizedSRC=$backup;
		//@update VP 18/05/2010
		ImageDestroy($logoImage);
	}

    header("Content-type: image/jpeg");
	
	// MSTEST -
	// 	=> on devrait essayer d'autoriser le cache sur les makevignette => perte de temps de forcer a revalider depuis le server, 
	// => pour l'instant je n'ai testé qu'expire et last-modified mais il faudrait aussi étudier la question du proxy-control : must-revalidate
	 // globalement je pense qu'on peut améliorer la vitesse de chargement des vignettes 
	
    // header("Expires: ".gmdate("D, d M Y H:i:s",strtotime("+1 week"))." GMT",true);
    // header("Last-modified: ".gmdate("D, d M Y H:i:s",strtotime("-1 week"))." GMT",true);

	//trace('out'.$ok);
	//trace(kCheminLocalMedia."/thumbnails/".$prefix.basename($_GET['image']));
	if ($thumbDirOk)  {
		ImageJpeg($resizedSRC,$thumb,80); //Sauve fichier
		//print file_get_contents(kCheminLocalMedia."/thumbnails/".$prefix.basename($_GET['image'])); //et on renvoie l'image
												//car la sauvegarde d'image en fichier ne l'envoie plus en screen
	}
	ImageJpeg($resizedSRC); //screen output sans sauvegarde thumb
	ImageDestroy($src_im);
	ImageDestroy($resizedSRC);
?>
