<?
require_once(libDir."class_chercheFest.php");
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();

$myPage->titre=kFestival;
$myPage->setReferrer(true); //by ld 11 09 08 
$mySearch=new RechercheFest();

	if(isset($_GET["init"])) {$mySearch->initSession(); }// réinit recherche

    if(isset($_POST["F_fest_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
    $deft_field = array("COL" => array("ID_FEST"), "PREFIX" => array("t1"), "DIRECTION" => array("DESC"));
    if (isset($_POST['orderbydeft']) && $_POST['orderbydeft']) {
        $deft_field = $_POST['orderbydeft'];
    }
	$mySort=$myPage->getSort(true,$deft,$deft_field);
	if ($deft==false) $_SESSION[$mySearch->sessVar]["tri"]=$mySort;
	if ($deft==true && $_SESSION[$mySearch->sessVar]["tri"]=='') $_SESSION[$mySearch->sessVar]["tri"]=$mySort;

// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
//include_once(getSiteFile("formDir","chercheFest.inc.php"));
if (file_exists(formDir."chercheFest.inc.php")) require_once(formDir."chercheFest.inc.php");
elseif (file_exists(formDirCore."chercheFest.inc.php")) require_once(formDirCore."chercheFest.inc.php");
else {
	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheFest.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="FEST";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
}


    // III. Suppression d'un imageur, des images li�es et mise � jour de t_doc et t_mat
    if(isset($_POST["commande"])){
        $id_fest=urldecode($_POST["ligne"]);
        if(strcmp($_POST["commande"],"SUP")==0) {
        	require_once(modelDir.'model_festival.php');
            $mon_objet = New Festival();
            $mon_objet->t_fest['ID_FEST']=$id_fest;
            $mon_objet->delete();
        }
    }

	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
		$sql=$_SESSION[$mySearch->sessVar]["sql"];

		if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

		//Sauvegarde du nombre de lignes de recherche � afficher :
		if($_REQUEST['nbLignes'] != '') {
				$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

		if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;

				debug(str_ireplace(array(',','join','WHERE','in '),array(','.chr(10),'join'.chr(10),'WHERE'.chr(10),'in '.chr(10)),$sql),'pink');


		$myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

		print("<div class='errormsg'>".$myPage->error_msg."</div>");
		$param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
		$param["page"] = $myPage->page;
		$param["id_fest"]=$_REQUEST['id_fest'];

		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		// VP 08/04/10 : ajout paramètre xmlfile 
		$param["xmlfile"]=getSiteFile("listeDir","xml/festListe.xml");
		$myPage->addParamsToXSL($param);
		$myPage->afficherListe("festival",getSiteFile("listeDir","festListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	}

 ?>
