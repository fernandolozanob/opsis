<?php

	require_once(libDir."class_chercheJob.php");
	require_once(modelDir.'model_job.php');
	require_once(modelDir.'model_processus.php');
	require_once(modelDir.'model_materiel.php');

	global $db;
	
	$myPage=Page::getInstance();
	$myUser=User::getInstance();
	$myPage->setReferrer(true);
	$myPage->titre=kListeEnregistrements;
	
	$mySearch=new RechercheJob();
    $mySearch->sessVar='recherche_JOBRECORD';
    
	$rec_module=$db->GetOne("select ID_MODULE from t_module where LOWER(MODULE_NOM)='record'");
	if(isset($_GET["init"])) $mySearch->initSession(); // réinit recherche
    if(isset($_POST["commande"])){
		switch($_POST["commande"]){
			case "ADD":
				$myPrms=$_POST;

				// Vérification recouvrement
				// VP 2/11/10 : correction bug sur explode time et duration
				$_date=explode('-',$myPrms['record_date']);
				$_time=explode(':',$myPrms['record_time']);
				$_duration=explode(':',$myPrms['record_duration']);
				$_start=mktime($_time[0],$_time[1],$_time[2],$_date[2],$_date[1],$_date[0]);
				$_end=mktime($_time[0]+$_duration[0],$_time[1]+$_duration[1],$_time[2]+$_duration[2],$_date[2],$_date[1],$_date[0]);
				$sql="select * from t_job where JOB_ID_MODULE=".intval($rec_module)." and JOB_ID_ETAT<".intval(jobFini);
				$rows=$db->GetAll($sql);
				$recouvrement=false;
				foreach($rows as $row){
					$job_param=$row['JOB_PARAM'];
					if(preg_match("|<date>(.*)</date+>|i",$row['JOB_PARAM'],$matches)) $record_date=$matches[1];
					if(preg_match("|<time>(.*)</time+>|i",$row['JOB_PARAM'],$matches)) $record_time=$matches[1];
					if(preg_match("|<duration>(.*)</duration+>|i",$row['JOB_PARAM'],$matches)) $record_duration=$matches[1];
					$_date=explode('-',$record_date);
					$_time=explode(':',$record_time);
					$_duration=explode(':',$record_duration);
					$_rowstart=mktime($_time[0],$_time[1],$_time[2],$_date[2],$_date[1],$_date[0]);
					$_rowend=mktime($_time[0]+$_duration[0],$_time[1]+$_duration[1],$_time[2]+$_duration[2],$_date[2],$_date[1],$_date[0]);
					if(($_start>=$_rowstart && $_start<=$_rowend)||($_end>=$_rowstart && $_end<=$_rowend)) {
						$recouvrement=true;
					}
					
				}
				if($recouvrement) print "<div class='errormsg'>".kMsgRecouvrement."</div>";
				else{
					// Programmation
					$job_param="<param>";
					foreach ($myPrms as $_fld=>$_val) { //Filtrage, on n'envoie que les paramètres liés à l'enregistrement
						if (strpos($_fld,'record_')===0) {
							$_fld=split('_',$_fld,2); //on se débarrasse du préfixe (trans_)
							if(is_array($_val)){
								foreach ($_val as $_fld2=>$_val2){
									$job_param.="<".$_fld[1].">".$_val2."</".$_fld[1].">";
								}
							} else {
								$job_param.="<".$_fld[1].">".$_val."</".$_fld[1].">";
							}
						}
					}
					$job_param.="</param>";
					// VP 20/10/10 : ajout prefix et extension
					if($myPrms['record_format']=="mpeg2") $ext=".mpg";
					else $ext=".mp4";
                    // VP 23/04/12 : passage chaine ne fin du nom de fichier
                    //	$id_mat=$myPrms['record_prefix']."_".str_replace(array("-",":"),"",$myPrms['record_channel']."_".$myPrms['record_date']."_".$myPrms['record_time'].$ext);
					if(!empty($myPrms['record_name'])) {
						$id_mat=$myPrms['record_name'];
					}
					else{
						$id_mat=$myPrms['record_prefix']."_".$myPrms['record_date']."_".$myPrms['record_time']."_".str_replace(array("-",":"),"",$myPrms['record_channel'].$ext);
					}
					if(!empty($myPrms["id_proc"])){
						$procObj = New Processus();
						$procObj->t_proc["ID_PROC"]=$myPrms["id_proc"];
						$procObj->getProc();
						$procObj->getProcEtape();
						$procObj->createJobs('',$id_mat,$job_param,$jobObj);
					}elseif(!empty($myPrms["id_etape"])){
						require_once(modelDir.'model_etape.php');
						$etapeObj = New Etape();
						$etapeObj->t_etape["ID_ETAPE"]=$myPrms["id_etape"];
						$etapeObj->getEtape();
						$id_job_valid=0;
						$etapeObj->createJob('',$id_mat,0,0,$id_job_valid,$jobObj,$job_param,$mat['FILE_NAME_MASK']);
						
					}elseif(!empty($myPrms["id_module"])){
						
						$myJob=new Job();
						$myJob->t_job["JOB_ID_MODULE"]=$myPrms["id_module"];
						$myJob->t_job["JOB_NOM"]=kEnregistrement." ".$id_mat;
						$myJob->t_job["JOB_IN"]=$id_mat;
						$myJob->t_job["JOB_OUT"]=$id_mat;
						$myJob->t_job["JOB_PARAM"]=$job_param;
						$myJob->t_job["JOB_DATE_CREA"]=date("Y-m-d H:i:s");
						$myJob->t_job["JOB_ID_SESSION"]=session_id();
						$myJob->create();
					}
				}
				
				$_POST["F_job_form"]="1";
				break;
				
			case "SUP":
				$myJob = New Job();
				$myJob->t_job['ID_JOB']=$_POST["ligne"];
				$myJob->getJob();
				
				if($myJob->t_job["JOB_ID_ETAT"]>1){
					//$myJob->t_job["JOB_PARAM"]="<param><action>delete</action></param>";
                    $myJob->t_job["JOB_PARAM"] = str_replace("</param>", "<action>delete</action></param>", str_replace("<action>create</action>", "", $myJob->t_job["JOB_PARAM"]));
					$myJob->t_job["JOB_ID_ETAT"]=jobAnnule;
					$myJob->save();
					$myJob->makeXML();
				}else{
					$myJob->delete();
				}
				break;
				
			case "INIT":
				$myJob = New Job();
				$myJob->t_job['ID_JOB']=$_POST["ligne"];
                $myJob->initJob();
//				$myJob->getJob();
//				$myJob->t_job['JOB_DATE_DEBUT']='';
//				$myJob->t_job['JOB_DATE_FIN']='';
//				$myJob->t_job['JOB_MESSAGE']='';
//				$myJob->setStatus(0);
//				$myJob->save();
				break;
        }
    }

    if(isset($_POST["F_job_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL(true);
        
        $mySearch->sqlRecherche = " AND (j.JOB_ID_MODULE=".intval($rec_module)." and j.ID_JOB_GEN=0) OR  (j.JOB_ID_MODULE=0 and j.ID_JOB in (select ID_JOB_GEN from t_job where JOB_ID_MODULE=".intval($rec_module)."))";
		// VP 2/11/10 : requête basée les processus et pas sur les trietemtns unitaires
		/*
         $mySearch->prepareSQL();
         $mySearch->sql= "select distinct j.*, t_module.MODULE_NOM, t_etat_job.ETAT_JOB FROM t_job j
		LEFT JOIN t_module ON j.JOB_ID_MODULE=t_module.ID_MODULE 
		LEFT JOIN t_etat_job ON j.JOB_ID_ETAT=t_etat_job.ID_ETAT_JOB and t_etat_job.ID_LANG='".$_SESSION['langue']."'
        JOIN t_job j2 ON j2.ID_JOB_GEN=j.ID_JOB_GEN and j2.JOB_ID_MODULE=".intval($rec_module)."
		WHERE j.JOB_ID_MODULE!='0'";
         */


		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
        
        $mySearch->finaliseRequeteChildren();
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION[$mySearch->sessVar]["tri"]=$myPage->getSort();


	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheJobRecord.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="JOB";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
	
	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql=$_SESSION[$mySearch->sessVar]["sql"];
		
	    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];
        else $sql.=" order by j.ID_JOB DESC";

	    //Sauvegarde du nombre de lignes de recherche � afficher :
	    if($_REQUEST['nbLignes'] != '') {
	    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

	    if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;
	    $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object
	    print("<div class='errormsg'>".$myPage->error_msg."</div>");

	    $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	    $param["page"] = $myPage->page;
	    $param["titre"] = urlencode(kEnregistrements);
		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		$param["xmlfile"]=getSiteFile("listeDir","xml/jobRecordListe.xml");
	    $myPage->addParamsToXSL($param);

        $res = $mySearch->searchChildren();
        $xml = TableauVersXML($res,"t_job_child",4,"select",0);
	    $myPage->afficherListe("t_job",getSiteFile("listeDir","jobRecordListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight'], $xml);

	}




?>
