<?php

require_once(libDir."class_chercheMessage.php");
require_once(modelDir.'model_message.php');
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();
$myPage->setReferrer(true);
$myPage->titre="";
$mySearch=new RechercheMessage();

if(isset($_GET["init"])) $mySearch->initSession();

if (isset($_POST['commande']) && $_POST['commande'] == "SUP_MSG") {
	foreach ($_POST as $key=>$id_msg) if (strpos($key, "checkbox") === 0) {
		$myMes = New Message();
		$myMes->t_message['ID_MESSAGE']=$id_msg;
		$myMes->delete();
		unset($myMes);
	}
}


if(isset($_POST["F_message_form"]))
{
	$mySearch->prepareSQL();
	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
	$mySearch->finaliseRequete();
}
else {
	$mySearch->prepareSQL();
	$mySearch->finaliseRequete();
}

echo "<div id='pageTitre'>".kListeMessages."</div>";

require_once(libDir."class_formCherche.php");
$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheMessage.xml"));
$myForm=new FormCherche;
$myForm->chercheObj=$mySearch;
$myForm->entity="MES";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->display($xmlform);
	
if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	$sql=$_SESSION[$mySearch->sessVar]["sql"];

	if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

	if($_REQUEST['nbLignes'] != '') {
			$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

	if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;
	$myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]);

	print("<div class='errormsg'>".$myPage->error_msg."</div>");

	$param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	$param["page"] = $myPage->page;
	$param["titre"] = urlencode(kMessage);
	if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
	else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
	$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
	
	$param["xmlfile"]=getSiteFile("listeDir","xml/messageListe.xml");
	$myPage->addParamsToXSL($param);

	$myPage->afficherListe("t_message",getSiteFile("listeDir","messageListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);

}

?>