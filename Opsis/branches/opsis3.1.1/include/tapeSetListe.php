<?
require_once(libDir."class_chercheTapeSet.php");
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();

$myPage->titre=kJeuxCartouches;
$myPage->setReferrer(true); 
$mySearch=new RechercheTapeSet();

	if(isset($_GET["init"])) {$mySearch->initSession(); }

	if(isset($_REQUEST["commande"])){
		if(strcmp($_POST["commande"],"SUP")==0) { // Suppression 
			$id_tape_set=urldecode($_POST["ligne"]);
			require_once(libDir.'class_tape_set.php');
			$mon_objet = New TapeSet();
			$mon_objet->t_tape_set['ID_TAPE_SET']=$id_tape_set;
			$mon_objet->delete();
		}
	}

    if(isset($_POST["F_tape_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION[$mySearch->sessVar]["tri"]=$myPage->getSort();

    include(getSiteFile("designDir","menuBackup.inc.php"));

    if (file_exists(formDir."chercheTapeSet.inc.php")) require_once(formDir."chercheTapeSet.inc.php");
	else {
		require_once(libDir."class_formCherche.php");
		$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheTapeSet.xml"));
		$myForm=new FormCherche;
		$myForm->chercheObj=$mySearch;
		$myForm->entity="TS";
		$myForm->classLabel="label_champs_form";
		$myForm->classValue="val_champs_form";
		$myForm->display($xmlform);
	}



	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
		$sql=$_SESSION[$mySearch->sessVar]["sql"];

		if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

		//Sauvegarde du nombre de lignes de recherche � afficher :
		if($_REQUEST['nbLignes'] != '') {
			$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

		if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;

		$myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

		print("<div class='errormsg'>".$myPage->error_msg."</div>");
		$param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
		$param["page"] = $myPage->page;
		$param["id_tape_set"]=$_REQUEST['id_tape_set'];

		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		$param["xmlfile"]=getSiteFile("listeDir","xml/tapeSetListe.xml");
		$myPage->addParamsToXSL($param);
		$myPage->afficherListe("tape_set",getSiteFile("listeDir","tapeSetListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	}

 ?>
