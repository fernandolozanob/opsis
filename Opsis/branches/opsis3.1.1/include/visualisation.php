<?php
switch ($_GET['method']) {

	case 'ORAO' :
		require_once(libDir.'class_visualisationORAO.php');
		$myVis=new VisualisationORAO($_GET['type']);
	break;

	case 'Flash' :
		require_once(libDir.'class_visualisationFlash.php');
		$myVis=new VisualisationFlash($_GET['type']);
	break;

	case 'FlashVideo' :
		require_once(libDir.'class_visualisationFlashVideo.php');
		$myVis=new VisualisationFlashVideo($_GET['type']);
		break;

	case 'QT' :
		require_once(libDir.'class_visualisationQT.php');
		$myVis=new VisualisationQT($_GET['type']);
	break;

		// VP 6/05/10 : ajout cas Diaporama
	case 'Diaporama' :
		require_once(libDir.'class_visualisationDiaporama.php');
		$myVis=new VisualisationDiaporama($_GET['type']);
		break;
}

	if ($_GET['action']=='save') { //Utile que pour ORAO -> sauvegardeCapsule !
		$myVis->sauvegardeCapsule($_GET);

	} else {	// Tous les autres cas

		$ok=$myVis->prepareVisu($_GET['id'],$_SESSION['langue']);
		
		// VP 6/10/11 : ajout variable session videoUrl
		$_SESSION['videoUrl'] 		= $myVis->mediaUrl;
		$_SESSION['videoPath'] 		= $myVis->mediaPath;
		$_SESSION['videoPlayer']	="opsis";	// authorisation visionnage via Opsis
		
		if ($_GET['method']=='Diaporama')
		{
			//playSlideshow("'.$this->XML.'");
			echo '<div id="slideshow_container"></div>';
			echo '<script type="text/javascript">
			
			window.onload=function()
			{
				var options=
				{
					width:"600px",
					height:"600px",
					xml:"'.$myVis->XML.'",
					loop:true,
					toggle_thumbs:true,
					stepping_frequency:5
				};
				my_player=new OW_slideshow("slideshow_container",options);
			}
			
			</script>';
		}
		else
		{
			if ($ok)
				$myVis->renderComponent();
		}
		
		if (!empty($myVis->error_msg)) {echo "
			<html>
				<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
				<link rel=\"stylesheet\" href='".designUrl."style.css' type='text/css' /></head>
				<body><div class='error'>".$myVis->error_msg."</div></body>
			</html>"; }
	}
?>
