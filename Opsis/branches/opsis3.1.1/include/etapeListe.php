<?
require_once(libDir."class_chercheEtape.php");
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();

$myPage->titre=kEtape;
$myPage->setReferrer(true); //by ld 11 09 08 
$mySearch=new RechercheEtape();

	if(isset($_GET["init"])) {$mySearch->initSession(); }// réinit recherche

    if(isset($_POST["F_etape_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	$mySort=$myPage->getSort(true,$deft);
	if ($deft==false) $_SESSION[$mySearch->sessVar]["tri"]=$mySort;
	if ($deft==true && $_SESSION[$mySearch->sessVar]["tri"]=='') $_SESSION[$mySearch->sessVar]["tri"]=$mySort;



// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
//include_once(getSiteFile("formDir","chercheEtape.inc.php"));
if (file_exists(formDir."chercheEtape.inc.php")) require_once(formDir."chercheEtape.inc.php");
else {
	echo '<div class="title_bar">'.kGestionEtapes.'</div>';
	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheEtape.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="ETAP";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
}


    // III. Suppression d'un imageur, des images li�es et mise � jour de t_doc et t_mat
    if(isset($_POST["commande"])){
        $id_etap=urldecode($_POST["ligne"]);
        if(strcmp($_POST["commande"],"SUP")==0) {
        	require_once(modelDir.'model_etape.php');
            $mon_objet = New Etape();
            $mon_objet->t_etape['ID_ETAPE']=$id_etap;
            $mon_objet->delete();
        }
    }

       if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql=$_SESSION[$mySearch->sessVar]["sql"];

	    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

	    //Sauvegarde du nombre de lignes de recherche � afficher :
	    if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
	    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

	    if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;
	    $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

	    print("<div class='errormsg'>".$myPage->error_msg."</div>");
	    $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	    $param["page"] = $myPage->page;
		
		if (isset($_REQUEST['id_doc']))
			$param["id_doc"]=$_REQUEST['id_doc'];

		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		// VP 08/04/10 : ajout paramètre xmlfile 
		$param["xmlfile"]=getSiteFile("listeDir","xml/etapeListe.xml");
	    $myPage->addParamsToXSL($param);
	    $myPage->afficherListe("etape",getSiteFile("listeDir","etapeListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	}

 ?>
