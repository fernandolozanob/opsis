<?php

include(libDir."class_chercheLexFlatFiles.php");
echo "Generating...\n";
$time_start = microtime(true);
echo $time_start."\n";

global $db;

if (!$tabLangues) $tabLangues=$_SESSION['arrLangues'];
foreach ($tabLangues as $_lang) {
	echo "Processing language : ".$_lang."\n";
	echo "Thesaurus...\n";
	$sql="SELECT distinct ID_LEX,LEX_TERME from t_lexique WHERE LEX_ID_SYN=0 and ID_LANG='".$_lang."' order by lex_terme asc";
	$arrIDs=$db->getAll($sql);
	$str="{utf-8}\n";	
	foreach ($arrIDs as $idx=>$_id) {
		$search=new RechercheLexFlatFiles ();
		$arrCrit=array ('VALEUR'=>$_id['ID_LEX'],'TYPE'=>'LI','FIELD'=>'');
		$search->chercheIdLex($arrCrit,'',true,true,false);
		if (count($search->tabHighlight['LEX_TERME'][0])>1) {
			$_id['LEX_TERME']=str_replace(" ' ","'",$_id['LEX_TERME']);
			$_loc=array_search('"'.$_id['LEX_TERME'].'"',$search->tabHighlight['LEX_TERME'][0]);
			if ($_loc!==false) unset ($search->tabHighlight['LEX_TERME'][0][$_loc]);
			$myResults=implode(";",$search->tabHighlight['LEX_TERME'][0]);
			$myResults=stripslashes($myResults);
			$myResults=str_replace('"','',$myResults);	
		$str.=$_id['LEX_TERME'].";".$myResults."\n";
		unset($search);
		}

	}
	$myFile=kPathSinequaDirectoryThesaurus.'flatFile.'.strtolower($_lang).'.r';
	$Handle = fopen($myFile, 'w') or die("can't open file");
	fwrite($Handle, $str);
	fclose($Handle);	
	echo "Thesaurus done\n
	Synonyms...\n";
	$str="{utf-8}\n";
	$time_end = microtime(true);
	$time=$time_end-$time_start;
	echo $time." sec.\n";
	$time_start = microtime(true);
	
	
	$sql="SELECT distinct ID_LEX,LEX_TERME from t_lexique WHERE LEX_ID_SYN<>0 and ID_LANG='".$_lang."' order by lex_terme asc";
	$arrIDs=$db->getAll($sql);
	foreach ($arrIDs as $idx=>$_id) {
		$search=new RechercheLexFlatFiles ();
		$arrCrit=array ('VALEUR'=>$_id['ID_LEX'],'TYPE'=>'LI','FIELD'=>'');
		$search->chercheIdLex($arrCrit,'',true,false,true);
		if (count($search->tabHighlight['LEX_TERME'][0])>1) {
			$_id['LEX_TERME']=str_replace(" ' ","'",$_id['LEX_TERME']);
			$_loc=array_search('"'.$_id['LEX_TERME'].'"',$search->tabHighlight['LEX_TERME'][0]); //retrait de l'occurence du terme lui-même
			if ($_loc!==false) unset ($search->tabHighlight['LEX_TERME'][0][$_loc]);
			$myResults=implode(";",$search->tabHighlight['LEX_TERME'][0]);
			$myResults=stripslashes($myResults);
			$myResults=str_replace('"','',$myResults);	
		$str.= $myResults.";".$_id['LEX_TERME']."\n";
		unset($search);
		}

	}

	$myFile=kPathSinequaDirectoryThesaurus.'flatFile.'.strtolower($_lang).'.s';
	$Handle = fopen($myFile, 'w') or die("can't open file");
	fwrite($Handle, $str);
	fclose($Handle);
	echo "Synonyms done...\n";
	$time_end = microtime(true);
	echo $time_end."\n";
	$time=$time_end-$time_start;
	echo $time." sec.\n";
	
}
echo "Generation done !";
?>
