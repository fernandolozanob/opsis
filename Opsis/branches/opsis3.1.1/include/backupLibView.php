<?php
require_once(libDir."class_backupJob.php");
require_once(modelDir.'model_tapeSet.php');
require_once(modelDir.'model_librairieElement.php');

global $db;



if (isset($_POST['commande']))
    {
    ini_set('max_execution_time','0');
    switch($_POST['commande'])
    {
    // listing
        case 'listing':
            $myBackupJob=new BackupJob();
            $ok=$myBackupJob->updateTapeStatus();
            break;
            
    // externalisation
        case 'externaliser':

            $arr_elts_lib=LibrairieElement::getElementFromDevice('changer');
            
            // recherche des jeux a externaliser presents dans la librairie
            $list_jeux=$db->Execute('SELECT * FROM t_tape_set WHERE TS_DEVICE=\'changer\' AND TS_EXTERNE=\'1\'')->getRows();
            //var_dump($list_jeux);
            $cartouche_pleines=array();
            
            foreach ($list_jeux as $jeu)
            {
                $obj_jeu=new TapeSet();
                $obj_jeu->t_tape_set['ID_TAPE_SET']=$jeu['ID_TAPE_SET'];
                $obj_jeu->getTapeSet();
                $obj_jeu->getTapes();
                
                // dans le jeu on regarde si il y a des cartouches pleines
                foreach($obj_jeu->t_tape as $tape)
                {
                    //if (($tape['TAPE_UTILISE']/$tape['TAPE_CAPACITE'])>0.9)
                    if ($tape['TAPE_FULL']=='1')
                    {
                        $cartouche_pleines[]=$tape;
                    }
                }
                
            }
            
            if (count($cartouche_pleines)>0)
            {
                // recherche des slot mail libres
                // stocker les mail dans un tableau
                $slot_mail=array();
                $slot_mail_free=array();
                
                foreach ($arr_elts_lib as $element)
                {
                    if ($element->getType()=='mail')
                    {
                        $slot_mail[]=$element;
                        if ($element->getIdTape()=='')
                            $slot_mail_free[]=$element;
                    }
                }
                
                if (count($slot_mail)>0)
                {
                    //deplacer une cartouche (plaine) dans chaque mail vide
                    $indx=0;
                    $cartouches_externalisees=array();
                    
                    foreach ($slot_mail_free as $mail)
                    {
                        if (!isset($cartouche_pleines[$indx]))
                            break;
                        $element_a_deplacer=new LibrairieElement();
                        $element_a_deplacer->getElementFromIdTape($cartouche_pleines[$indx]['ID_TAPE']);
                        
                        // si la cartouche est dans un slot type mail ==> pas de deplacement
                        if ($element_a_deplacer->getType()!='mail')
                            $element_a_deplacer->transfert($mail);
                        
                        $cartouches_externalisees[]=$cartouche_pleines[$indx];
                        $indx++;
                    }
                    
                    // retirer les cartouches dans le tiroir
                    echo sprintf(kTiroirContientCartoucheAExternaliser,count($cartouches_externalisees));
                    echo '<ul>';
                    foreach ($cartouches_externalisees as $cartouche)
                        echo '<li>'.$cartouche['ID_TAPE'].'</li>';
                    echo '</ul>';
                    echo kMerciDeLesRetirerDuTiroir;
                    
                    // cartouches restantes
                    if ((count($cartouche_pleines)-count($cartouches_externalisees))>0)
                        echo '<br />'.sprintf(kIlResteYCartouchesAExternaliser,(count($cartouche_pleines)-count($cartouches_externalisees)));
                    
                    //formulaire avec le bouton OK
                    echo '<form action="index.php?urlaction=backupLibView" method="post">';
                    echo '<input type="submit" value="OK" />';
                    echo '</form>';
                }
                else // pas de slot mail
                {
                    // retirer les cartouche dans la librairie
                    echo sprintf(kXCartoucheAExternaliser.' : <br />',count($cartouche_pleines));
                    echo '<ul>';
                    foreach ($cartouche_pleines as $cartouche)
                        echo '<li>'.$cartouche['ID_TAPE'].'</li>';
                    echo '</ul>';
                    echo kMerciDeRetirerLesCartouches;
                }
                
                // mise a jour de la base de donnees
                $lib_elt=new LibrairieElement();
                $lib_elt->setDevice('changer');
                $lib_elt->updateLibElements();
            }
            else
                echo kPasDeCartoucheAExternaliser;
            
        break;
	
	}
	
}

// recuperation des elements depuis la base de donnees
$arr_elts_lib=LibrairieElement::getElementFromDevice('changer');
$xml='<?xml version="1.0" encoding="UTF-8" ?>'."\n";
$xml.='<select>'."\n";

foreach ($arr_elts_lib as $element)
{
	$xml.="\t<lib_element>\n";
	$xml.="\t\t<device>".$element->getDevice()."</device>\n";
	$xml.="\t\t<type>".$element->getType()."</type>\n";
	$xml.="\t\t<numero>".$element->getNumero()."</numero>\n";
	
	$xml.="\t\t<id_tape>".$element->getIdTape()."</id_tape>\n";
	
	if ($element->getIdTape()!='')
	{
		// taille de la cartouche
		$xml.="\t\t<tape_capacite>".$element->getTapeObject()->t_tape['TAPE_CAPACITE']."</tape_capacite>\n";
		$xml.="\t\t<tape_utilise>".$element->getTapeObject()->t_tape['TAPE_UTILISE']."</tape_utilise>\n";
		$xml.="\t\t<pourcentage_utilise>".($element->getTapeObject()->t_tape['TAPE_UTILISE']/$element->getTapeObject()->t_tape['TAPE_CAPACITE']*100)."</pourcentage_utilise>\n";
		
		$xml.="\t\t<id_tape_set>".$element->getTapeObject()->t_tape_set['ID_TAPE_SET']."</id_tape_set>";
		$xml.="\t\t<ts_externe>".$element->getTapeObject()->t_tape_set['TS_EXTERNE']."</ts_externe>";
		
	}
	
	if ($element->getFromSlot()!='')
		$xml.="\t\t<from_slot>".$element->getFromSlot()."</from_slot>\n";
	$xml.="\t</lib_element>\n";
}
$xml.='</select>'."\n";

$param=array();
if(defined("gPrefixTape")) $param["gPrefixTape"]=gPrefixTape;
else $param["gPrefixTape"]="";
$content=TraitementXSLT($xml,getSiteFile('designDir','liste/backupLibView.xsl'),$param);

include(getSiteFile("designDir","menuBackup.inc.php"));

echo $content;

//var_dump(nl2br(htmlentities($xml)));
?>