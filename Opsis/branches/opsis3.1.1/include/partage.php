<?php
require_once(modelDir."model_partage.php");
require_once(libDir."fonctionsGeneral.php");

$myPartage = new Partage() ; 

if(isset($_POST['action']) && !empty($_POST['action'])){
	switch($_POST['action']){
		case "create" : 
			$myPartage->init();
			$myPartage->updateFromArray($_POST);
			$myPartage->getSharedObject() ; 
			$myPartage->save();
			$myPartage->getPartage();
			
			$myPartage->createSharedChildren();
		
			
			$options = array(
				'sujet'=>$_POST['partage_objet'],
				'corps'=>$_POST['partage_corps'],
				'xslfile'=>getSiteFile("designDir","/print/".$_POST['xsl'].".xsl"),
				'copy_to_sender'=>$_POST['partage_expediteur_cc'],
				'logo_url'=>$_POST['logo_url']
			);
			$ok = $myPartage->triggerShare($options);
			
			if($ok && !empty($myPartage->return_msg)){
				echo $myPartage->return_msg;
			}
			
		break ; 
	}
}



?>
