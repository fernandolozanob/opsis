<?php
	require_once(libDir.'class_etape.php');
	require_once(libDir.'class_facebook.php');
	
	global $db;
	
	$myPage = Page::getInstance();

	$this_url = kCheminHttp.$myPage->getName()."?urlaction=facebookToken";
	
	// récupération paramètres étapes
	if(isset($_GET['id_etape']) && !empty($_GET['id_etape']))  {
		$id_etape =  $_GET['id_etape'];
	} else {
		$id_etape = $db->GetOne("SELECT ID_ETAPE FROM t_etape e INNER JOIN t_module m ON m.ID_MODULE = e.ETAPE_ID_MODULE WHERE MODULE_TYPE = 'DIFF' AND MODULE_NOM = 'facebook'");
	}
	
	if(empty($id_etape)){
		echo kMailFacebookEtapeManquante;
		exit;
	}
	
	$etapeObj = new Etape();
	$etapeObj->t_etape['ID_ETAPE']=$id_etape;
	$etapeObj->getEtape();
	$tabEtapeParam = xml2array($etapeObj->t_etape['ETAPE_PARAM']);
	$tabEtapeParam = $tabEtapeParam['param'];
	
	$access_token = $tabEtapeParam['access_token'];
	$target = $tabEtapeParam['target'];
	if(empty($target))
		$target = "me";
	
	$app_config = array
	(
	 'app_id' => $tabEtapeParam['id_application'],
	 'app_secret' => $tabEtapeParam['api_key'],
	 'default_graph_version' => 'v2.2'
	 );
	
	$fb = new Facebook\Facebook($app_config);
	
	$helper = $fb->getRedirectLoginHelper();
	
	// redirect the user back to the same page if it has "code" GET variable
	if (isset($_GET['code'])) {
		//header('Location: '.$this_url);
		try {
			$access_token = $helper->getAccessToken();
			if($target!='me'){
				$page_request = $fb->get('/'.$target.'?fields=access_token', $access_token);
				$page_array = $page_request->getGraphNode()->asArray();
				$access_token = $page_array['access_token'];
			}
			//var_dump($access_token);
			
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
	}
	
	// Check access token
	try {
		// getting basic info about user
		$profile_request = $fb->get('/'.$target, $access_token);
		$profile = $profile_request->getGraphNode()->asArray();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		trace('Graph returned an error: ' . $e->getCode().' : '.$e->getMessage());
		$permissions = ['email']; // Optional permissions
		$loginUrl = $helper->getLoginUrl($this_url, $permissions);
		echo kMsgFacebookTokenInvalide."<br/>";
		echo $access_token."<br/>";
		echo "<a href='" . $loginUrl . "'>".kMsgFacebookConnect."</a>";
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		trace('Facebook SDK returned an error: ' . $e->getMessage());
		$permissions = ['email']; // Optional permissions
		$loginUrl = $helper->getLoginUrl($this_url, $permissions);
		echo kMsgFacebookTokenInvalide."<br/>";
		echo $access_token."<br/>";
		echo "<a href='" . $loginUrl . "'>".kMsgFacebookConnect."</a>";
		exit;
	}
	
	// The OAuth 2.0 client handler helps us manage access tokens
	$oAuth2Client = $fb->getOAuth2Client();
	
	// Get the access token metadata from /debug_token
	$tokenMetadata = $oAuth2Client->debugToken($access_token);

	try {
		// Exchanges a short-lived access token for a long-lived one
		$access_token = $oAuth2Client->getLongLivedAccessToken($access_token);
	} catch (Facebook\Exceptions\FacebookSDKException $e) {
		echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
		exit;
	}
	// Sauvegarde token en base
	trace("Token Facebook".$access_token);
	$tabEtapeParam['access_token']=$access_token;
	
	$etapeObj->t_etape['ETAPE_PARAM'] = array2xml($tabEtapeParam, "param");
	$ok = $etapeObj->save();
	if($ok) {
		echo kMsgFacebookTokenMAJ;
	} else {
		echo kMsgFacebookErreurBDD;
	}

	unset($fb);
	
	?>

