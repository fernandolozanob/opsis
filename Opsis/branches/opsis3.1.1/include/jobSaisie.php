<?php
	global $db;
	require_once(modelDir.'model_materiel.php');
	
	$myPrms=$_POST; // Par défaut, on récupère les params POST

	//Update VG 13/03/2015  : trace pour le parisien, A supprimer lorsque l'on en aura  plus besoin
	if(!empty($_POST['date_lancement']) && $_POST['id_proc'] == '15') {
		trace("creation du job de diffusion Dailymotion pour id ".$_POST['id_doc'].", programme pour le ".$_POST['date_lancement']);
	}
	
	// VP 30/09/10 : ajout job type record
	if(isset($myPrms["lancement"]) && $myPrms["type"]=="record"){
		include_once(includeDir."jobRecordListe.php");
		
	}elseif(isset($myPrms["lancement"])){

		// Sélection des éléments à traiter
		switch($myPrms["type"]){
			case "mat":
				if(!empty($myPrms["id_mat"])) $mats[0]["ID_MAT"]=$myPrms["id_mat"];
				break;
				
			case "doc":
				if(!empty($myPrms["id_mat"])) $mats[0]["ID_MAT"]=$myPrms["id_mat"];
				// By VP (23/9/08) : remplacement xmlparams par ID_DOC
				if(!empty($myPrms["id_doc"])) $mats[0]["ID_DOC"]=$myPrms["id_doc"];
				//if(!empty($myPrms["xml_params"])) $xml_params=$myPrms["xml_params"];
				break;
				
			case "imageur":
				// Transfert d'image
				if(!empty($myPrms["id_mat"])) $mats[0]["ID_MAT"]=$myPrms["id_mat"];
				break;
				
			case "doc_acc":
				// Import doc_acc
				for($i=0; $i<count($myPrms); $i++) {
					if(isset($myPrms["file_". $i])) {
						$mats[]=array('ID_MAT'=>$myPrms["file_". $i],
									  'FOLDER_IN'=>kDocumentDir,
									  'FOLDER'=>kDocumentDir );
					}
				}
				break;
				
			case "matListe":
				if(isset($_SESSION["recherche_MAT"]["sql"])) $mats=$db->GetAll($_SESSION["recherche_MAT"]["sql"]);
				break;
				
				
			case "num":
			case "mat_doc":
				
				if(!empty($myPrms["id_mat"])) $_list[0]["ID_MAT"]=$myPrms["id_mat"];
				else if(isset($_SESSION["recherche_MAT"]["sql"])) $_list=$db->GetAll($_SESSION["recherche_MAT"]["sql"]);
				
				foreach($_list as $_i => $_mat){
					$matObj= new Materiel;
					$matObj->t_mat['ID_MAT']=$_mat["ID_MAT"];
					$matObj->getMat();
					$matObj->getDocs();
					//$_offset=(empty($matObj->t_mat['MAT_TCIN'])?"00:00:00:00":$matObj->t_mat['MAT_TCIN']);
					foreach($matObj->t_doc_mat as $idx => $dm){
						if(empty($myPrms["file_name_mask"])) $file_name_mask="";
						else $file_name_mask=array($dm['DOC']->t_doc[$myPrms["file_name_mask"]]);
						$mats[]=array('ID_MAT'=>$dm['ID_MAT'],
									  'ID_DOC'=>$dm['ID_DOC'],
									  'TCIN'=>$dm['DMAT_TCIN'],
									  'TCOUT'=>$dm['DMAT_TCOUT'],
									  'FILE_NAME_MASK'=>$file_name_mask,
									  'MAT_COTE_ORI'=>$myPrms['mat_cote_ori'],
									  'id_proc'=>$myPrms['id_proc_'.$dm['ID_DOC']]
									  );
					}
					unset($matObj);
				}
				break;
				
			case "panier":
				require_once(modelDir.'model_panier.php');
				require_once(libDir."class_livraison.php");
				if(!empty($myPrms["id_panier"])) {
					$myPanier=new Panier();
					$myPanier->t_panier['ID_PANIER']=$myPrms["id_panier"];
					$myPanier->getPanier();
					if(!empty($myPrms["folder"])) $nomDossierCourt=$myPrms["folder"];
					else $nomDossierCourt="P".$myPrms["id_panier"];
					$myLivraison=new Livraison();
					$myLivraison->setDir($nomDossierCourt);
					$myLivraison->createDir();
					
					//PC 16/11/12 : Permet de créer des jobs pour les documents n'ayant pas de matériel
					$mats=$myPrms["job_without_mat"];
					
					foreach ($myPanier->t_panier_doc as $idx=>&$doc) {
						
						// VP 9/03/10 : possibilité de spécifier le format de livraison
						if(isset($myPrms['format_'.$doc['ID_LIGNE_PANIER']])) $format=$myPrms['format_'.$doc['ID_LIGNE_PANIER']];
						else $format=$doc['PDOC_SUPPORT_LIV'];

						// VP 15/12/10 : possibilité de spécifier un materiel de livraison [mat_xx]
						$materiels=$myLivraison->getMat2Copy($doc['ID_DOC'],$format,$myPrms['mat_'.$doc['ID_LIGNE_PANIER']]);
						
						foreach ($materiels as $mat) {
							$doc['MAT']=$mat;
							if (!empty($doc['MAT'])) {
								// VP 3/07/09 : prise en compte des TC des extraits
								if($doc['PDOC_EXTRAIT']=='1'){
									$in=$doc['PDOC_EXT_TCIN'];
									$out=$doc['PDOC_EXT_TCOUT'];
								}else{
									$in=$doc['MAT']['DMAT_TCIN'];
									$out=$doc['MAT']['DMAT_TCOUT'];
								}
								$file_name_mask=array(str_replace(":","","-".$in."-".$out));
								// VP 5/10/10 : Inclusion updatePanier dans paramètres jobs
								if($myPrms["updatePanier"]=="1"){
									$mats[]=array('ID_MAT'=>$doc['MAT']['ID_MAT'],
												  'ID_DOC'=>$doc['ID_DOC'],
												  'updatePanier'=>"1",
												  'ID_LIGNE_PANIER'=>$doc['ID_LIGNE_PANIER'],
												  'FOLDER'=>$myLivraison->livraisonDir,
												  'TCIN'=>$in,
												  'TCOUT'=>$out,
												  'FILE_NAME_MASK'=>$file_name_mask,
												  'id_proc'=>$myPrms['id_proc_'.$doc['ID_LIGNE_PANIER']],
												  'proc_critere'=>$myPrms['proc_critere_'.$doc['ID_LIGNE_PANIER']]
												  );
									//Mise à jour infos t_panier_doc
									$doc['PDOC_ID_MAT']=$doc['MAT']['ID_MAT'];
									$doc['PDOC_ID_ETAT']=gPanierEnCours;
								}else{
									$mats[]=array('ID_MAT'=>$doc['MAT']['ID_MAT'],
												  'ID_DOC'=>$doc['ID_DOC'],
												  'ID_LIGNE_PANIER'=>$doc['ID_LIGNE_PANIER'],
												  'FOLDER'=>$myLivraison->livraisonDir,
												  'TCIN'=>$in,
												  'TCOUT'=>$out,
												  'FILE_NAME_MASK'=>$file_name_mask,
												  'id_proc'=>$myPrms['id_proc_'.$doc['ID_LIGNE_PANIER']],
												  'proc_critere'=>$myPrms['proc_critere_'.$doc['ID_LIGNE_PANIER']]
												  );
								}
							}
						}
					}
					
					if($myPrms["updatePanier"]=="1"){
						$myPanier->saveAllPanierDoc();
						$myPanier->updatePanierEtat();
					}
					// Creation fichier XML
					if(!empty($myPrms["xsl"])){
						require_once(modelDir.'model_doc.php');
						$myPanier->getDocsFull(0,0,1,0,0);
						$content=$myPanier->xml_export(0);
						$content.= $myPanier->xml_export_panier_doc(0);
						$content="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$content."\n</EXPORT_OPSIS>";
						$contentExport = TraitementXSLT($content,"design/print/".$myPrms["xsl"].".xsl",array(),0);
						//Création fichier XML
						$fp = fopen($myLivraison->livraisonDir.$myPrms["id_panier"].'.xml', 'w');
						fwrite($fp, $contentExport);
						fclose($fp);
					}

				}
				break;

			case "docListe":
				if(isset($_SESSION["recherche_DOC"]["sql"])) $sql= $_SESSION["recherche_DOC"]["sql"];
				if(isset($_SESSION["recherche_DOC"]["tri"])) $sql.= $_SESSION["recherche_DOC"]["tri"];
				$rsDocs=$db->GetAll($sql);
				require_once(modelDir.'model_doc.php');
				foreach ($rsDocs as $docFound) {
					$_doc=new Doc();
					$_doc->t_doc['ID_DOC']=$docFound['ID_DOC'];
					$_doc->t_doc['ID_LANG']=$docFound['ID_LANG'];
					$_doc->getDoc();$_doc->getDocMat();
					// LD : special INA PUB, pour le fichier créé (FILE_NAME_MASK), on ajoute le n° de programme
					// stocké dans doc_soustitre. C'est pas génial...

					foreach ($_doc->t_doc_mat as $_thisdoc) {
					if (!empty($_thisdoc['DMAT_LIEU']))
					$mats[]=array('ID_MAT'=>$_thisdoc['ID_MAT'],
					'ID_DOC'=>$_thisdoc['ID_DOC'],
					'TCIN'=>$_thisdoc['DMAT_TCIN'],
					'TCOUT'=>$_thisdoc['DMAT_TCOUT'],
					'FILE_NAME_MASK'=>array($_doc->t_doc['DOC_SOUSTITRE'])
					);
				}

				unset($_doc);

				}
				break;

				case "docListeMaster":
				if (defined("useSolr") && useSolr==true) {
					$rsDocs=array();
					$solr_client=new SolrClient(unserialize(kSolrOptions));
					try{
						$query = $_SESSION["recherche_DOC_Solr"]["sql"];
						$query->setStart(0);
						$query->setRows(300);
						$response=$solr_client->query($query);
						if (!empty($response->getResponse()->response->docs))
						{
							foreach($response->getResponse()->response->docs as $doc)
							{
								$arr_doc=array();
								$arr_doc["ID_DOC"]=$doc->offsetGet("id_doc");
								$arr_doc["ID_LANG"]=$doc->offsetGet("id_lang");
								$rsDocs[]=$arr_doc;
							}
						}
					}catch(Exception $e){
						trace("solr query crash : ".$e->getMessage());
					}
				} else {
					if(isset($_SESSION["recherche_DOC"]["sql"])) $sql= $_SESSION["recherche_DOC"]["sql"];
					if(isset($_SESSION["recherche_DOC"]["tri"])) $sql.= $_SESSION["recherche_DOC"]["tri"];
					$rsDocs=$db->GetAll($sql);
				}
				require_once(modelDir.'model_doc.php');
				foreach ($rsDocs as $docFound) {
					$_doc=new Doc();
					$_doc->t_doc['ID_DOC']=$docFound['ID_DOC'];
					$_doc->t_doc['ID_LANG']=$docFound['ID_LANG'];
					$_doc->getDoc();$_doc->getMats();
					
					foreach ($_doc->t_doc_mat as $_thisdoc)
						if (!empty($_thisdoc['MAT']->t_mat['MAT_TYPE']) && $_thisdoc['MAT']->t_mat['MAT_TYPE'] == "MASTER") {
							$mats[]=array('ID_MAT'=>$_thisdoc['ID_MAT'],
								'ID_DOC'=>$_thisdoc['ID_DOC'],
								'TCIN'=>$_thisdoc['DMAT_TCIN'],
								'TCOUT'=>$_thisdoc['DMAT_TCOUT']
							);
							break;
						}
					unset($_doc);
				}
				break;

				default:
					// VP 31/8/09 : cas defaut avec tableau de job construit dans formulaire
					$mats=$myPrms["job"];
					break;

			}
	// Affichage jobs
	include_once(includeDir."jobListe.php");

}elseif(!empty($myPrms["type"])){
	// VP 26/05/11 : ajout traitements hors frontal
	// Traitement hors frontal
	switch($myPrms["type"]){
		case "mat":
		if(!empty($myPrms["id_mat"])) $mats[0]["ID_MAT"]=$myPrms["id_mat"];
		break;
	}

	include(upload_scriptDir."processFiles.php");
}else{
// Affichage du dialogue de configuration
include(getSiteFile("formDir","jobSaisie.inc.php"));
}
?>
