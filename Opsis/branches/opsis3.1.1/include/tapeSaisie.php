<?
 	//trace(print_r($_POST,true));
    require_once(modelDir.'model_tape.php');
    require_once(libDir."class_backupJob.php");
    require_once(modelDir.'model_materiel.php');

    global $db;

	//debug($_POST,'cyan');

	$myUser=User::getInstance();
	$myPage=Page::getInstance();
	$myPage->nomEntite = kCartouche;
	$myTape=New Tape();

    $action=(!empty($_POST["commande"])?$_POST["commande"]:"");
    if(isset($_POST["bOk"]) && empty($action))
        $action="SAVE";
   
    if (isset($_GET["id_tape"]))
    	$myTape->t_tape['ID_TAPE']=$_GET["id_tape"];
    else
    	$myTape->t_tape['ID_TAPE']="";


// II. Traitement des actions
if(!empty($action))
{
    switch($action){
        case "SUP":
            $myTape->delete();
            if(!empty($_POST["page"])){
                $myPage->redirect($_POST["page"]);
            }else $myPage->redirect($myPage->getName()."?urlaction=tapeListe");
            break;
            
        case "SAVE":
        case "SAVE_TAPE":
            if (!empty($myTape->t_tape['ID_TAPE']))
                $myTape->id_tape_ref = $myTape->t_tape['ID_TAPE'];
            $myTape->t_tape['ID_TAPE_SET'] = '';
            $myTape->t_tape['TAPE_LOCALISATION'] = '';
            
            $myTape->updateFromArray($_POST,false);
            $saved=$myTape->save();
            if($saved && $_POST["saveTapeFile"]=="1"){
                $myTape->saveTapeFile(); 
            }
            if($saved && !empty($myTape->t_tape['ID_TAPE_SET']));
			$myTape->updateSet($myTape->t_tape['ID_TAPE_SET']);
            break;

        case "RESTORE_ALL":
            ini_set('max_execution_time','0');

            $myTape->getTape();
            $myTape->getTapeFile();
            $nb_exist=0;
            $nb_restore=0;
            foreach($myTape->t_tape_file as $tf){
                $myMat=new Materiel();
                $myMat->t_mat['ID_MAT']=$tf['TF_ID_MAT'];
                $myMat->getMat();
                if($myMat->hasfile) {
                    echo "Exist : ".$myMat->t_mat['MAT_CHEMIN']."/".$myMat->t_mat['MAT_NOM']."<br/>";
                    $nb_exist++;
                    continue;
                }
                elseif($myMat->t_mat['MAT_TAILLE']==$tf['TF_TAILLE_FICHIER'] && $myMat->t_mat['MAT_DATE_FICHIER']==$tf['TF_DATE_FICHIER']){
                    // Création job restore
                    echo "Restore : ".$myMat->t_mat['MAT_CHEMIN']."/".$myMat->t_mat['MAT_NOM']."<br/>";
                    $nb_restore++;
                   
                    $myBackupJob=new BackupJob();
                    $ok=$myBackupJob->createJob($jobObj, "restore", "", $tf["ID_TAPE_FILE"]);
                    if(!$ok) echo $myBackupJob->error_msg."<br/>";
                }
            }
            echo "--- Total ---<br/>".$nb_exist." fichier(s) existant(s) <br/>".$nb_restore." fichier(s) à restaurer <br/>";
            
            break;
    }
}
if (!empty($myTape->t_tape['ID_TAPE'])) {
	$myTape->getTape();
	$myTape->getTapeFile(); 
}
$myPage->titre = (isset($myTape->t_tape['ID_TAPE']) && !empty($myTape->t_tape['ID_TAPE'])?($myTape->t_tape['ID_TAPE']):'');
$myPage->setReferrer(true);

//debug($myTape,'beige');

require_once(getSiteFile("formDir",'tapeSaisie.inc.php'));
?>