<?php

require_once(libDir."class_chercheLex.php");
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();
$myPage->titre=kGestionLexique;
$myPage->setReferrer(true);
$mySearch=new RechercheLex();

	if(isset($_GET["init"])) $mySearch->initSession(); // réinit recherche

	//@update VG 07/04/2010: gestion du champ 'affichage'
		//@update VG 12/04/2010
	$mySearch->affichage = "LISTE";
	if (isset($_REQUEST['affichage'])) {
		$mySearch->affichage = $_REQUEST['affichage'];
	} elseif (isset($_SESSION[$mySearch->sessVar]['affichage'])) {
		$mySearch->affichage = $_SESSION[$mySearch->sessVar]['affichage'];
	}


    if(isset($_POST["F_lex_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
		$mySearch->appliqueDroits();
 		$mySearch->finaliseRequete(); //fin et mise en session
    }
	

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	$mySort=$myPage->getSort(true,$deft);
	if ($deft==false) $_SESSION[$mySearch->sessVar]["tri"]=$mySort;
	if ($deft==true && (!isset($_SESSION[$mySearch->sessVar]["tri"]) || $_SESSION[$mySearch->sessVar]["tri"]=='')) $_SESSION[$mySearch->sessVar]["tri"]=$mySort;

	// VP 8/02/10 : uniformisation des noms de formulaires en chercheXXX
	if (file_exists(formDir."lexListe.inc.php")) require_once(getSiteFile("formDir","lexListe.inc.php"));
	else require_once(getSiteFile("formDir","chercheLex.inc.php"));


	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
		 $sql=$_SESSION[$mySearch->sessVar]["sql"];
	    // MODE D'AFFICHAGE

	    if ($_SESSION[$mySearch->sessVar]['affichage']=='HIERARCHIQUE') {

//	 		echo "
//			<div class='resultsMenu'><a href='#' onclick=\"popupPrint('palette.php?urlaction=printHLex&type_lex=".$mySearch->getValueForField(array('FIELD'=>'TYPE_LEX','TYPE'=>'CI'))."','main')\">".kImprimer."</a></div>
            echo "
            <div class='resultsMenu'><a href='#' onclick=\"window.location='export.php?fichier=export_thes&export=exportTabLexHier_xls&sql=recherche_LEX&type=lex&nb=all&page=1&mode=hier'\">".kExporter."</a></div>
			<script>
				function dspClock (toggle) {

					if (toggle) {
						document.getElementById('clock').style.display='block';
						if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='none';
						}
					else {
						document.getElementById('clock').style.display='none';
						if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='block';
						}
					return true;
				}
			</script>
			<div id='clock' style='display:none;text-align:center'>".kPatientez."<br/><img src='".imgUrl."wait30trans.gif'></div>
			<link rel='stylesheet' href='".libUrl."webComponents/tafelTree/tree.css' type='text/css' />
			<script type='text/javascript' src='".libUrl."webComponents/prototype/prototype.js'></script>
			<script type='text/javascript' src='".libUrl."webComponents/prototype/scriptaculous.js'></script>
			<script type='text/javascript' src='".libUrl."webComponents/tafelTree/Tree.js'></script>
			<div align='center'  style='width:600px;border:1px solid #999999;' >
			<script>dspClock(true)</script>
			";
            require_once(libDir."class_tafelTree.php");
	        $mySearch->treeParams=array("ID_TYPE_LEX"=>$mySearch->getValueForField(array('FIELD'=>'TYPE_LEX','TYPE'=>'CI')),
	        		"ID_TYPE_DESC"=>'',"ID_ROLE"=>'',"MODE"=>'');
	        $btnEdit=$myPage->getName()."?urlaction=lexSaisie&id_lex=";

			$myAjaxTree=new tafelTree($mySearch,'form1');
			$myAjaxTree->JSReturnFunction=$jsFunction;

			$myAjaxTree->makeRoot();
	        $myAjaxTree->JSlink="window.location.href='".Page::getName()."?urlaction=lexSaisie&id_lex=%3\$s'"; //Le lien sur les terme ouvrira l'édition

	        //Récupération du type de recherche
	        $title=GetRefValue('t_type_lex',$mySearch->getValueForField(array('FIELD'=>'TYPE_LEX','TYPE'=>'CI')),$_SESSION['id_lang']);


	        if ($mySearch->getValueForField(array('FIELD'=>'LEX_TERME','TYPE'=>'FT'))!=''
	        	|| $mySearch->getValueForField(array('FIELD'=>'LEX_TERME','TYPE'=>'C'))!='' ) { //En plus, on a une recherche sur un terme
	        	$rst=$db->GetAll($sql); // Lancement de la requete.
	            if (!empty($rst)) {
					foreach ($rst as $row) { //Pour chaque résultat...
					   if($row['LEX_ID_ETAT_LEX']>=2) { //... on ne garde que les termes valides
						  if ($row['LEX_ID_SYN']==0) {//s'il y a un synonyme préférentiel, c'est lui qu'on retient
							$myAjaxTree->revealNode($row['ID_LEX']);
							 }
						  else $myAjaxTree->revealNode($row['LEX_ID_SYN']);
					   }
					}
				echo "<div class='story'>".count($rst)." ".kResultats."</div>";
	            } else echo "<div class='story'>".kAucunResultat."</div>";
             }
	        if ($_POST['id_asso']) $myAjaxTree->revealNode(intval($_POST['id_asso']));
	        $myAjaxTree->renderOutput('',true,$btnEdit);
	    	echo "<div id='myTree'></div>";
	    	echo "<script>dspClock(false)</script>
			</div>";
	    }
	    else {//Affichage en liste

			debug($sql);
		    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

		    //Sauvegarde du nombre de lignes de recherche � afficher :
		    if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
		    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

			// VP 21/05/10 : ajout paramètre gNbLignesDefaut par défaut
			$nbDeft=(defined("gNbLignesListeDefaut")?gNbLignesListeDefaut:10);
		    if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = $nbDeft;
		    $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

		    print("<div class='errormsg'>".$myPage->error_msg."</div>");

		    $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
		    $param["page"] = $myPage->page;
		    $param["titre"] = urlencode(kLexique);
			if ($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
			else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
			$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
			// VP 08/04/10 : ajout paramètre xmlfile
			$param["xmlfile"]=getSiteFile("listeDir","xml/lexListe.xml");
		    $myPage->addParamsToXSL($param);
		    $myPage->afficherListe("t_lex",getSiteFile("listeDir","lexListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	    }
	}



?>
