<?php

require_once(modelDir.'model_panier.php');

//$myPage=Page::getInstance();

global $db;
    
if(isset($_GET['id_panier'])) $pan_id_gen=intval($_GET['id_panier']);
else $pan_id_gen=0;
if(isset($_GET['tri']) && !empty($_GET['tri'])){
    $tri= strtoupper(substr($_GET["tri"],0,strlen($_GET["tri"])-1));
	if(is_numeric(intval(substr($_GET["tri"],-1)))){
		$ordre = "".substr($_GET["tri"],-1);
	}
} else{
	$tri="pan_titre";
}

$sql = 'SELECT * FROM t_panier WHERE pan_public=1 AND pan_id_gen='.intval($pan_id_gen);
if(isset($tri)){
	$sql.=" ORDER BY t_panier.".$tri;
	if($ordre){
		$sql.=" ASC";
	}else{
		$sql.=" DESC";
	}
}

$paniers_publics=$db->Execute($sql)->getRows();


$xml='<?xml version="1.0" encoding="UTF-8"?>';
$xml.='<EXPORT_OPSIS>';

foreach ($paniers_publics as $pan)
{
	$panier=new Panier();
	$panier->t_panier['ID_PANIER']=$pan['ID_PANIER'];
	$panier->getPanier();
	$panier->getVignette();
	$xml.=$panier->xml_export();
}

$xml.='</EXPORT_OPSIS>';

$content_export=TraitementXSLT($xml,getSiteFile("designDir","liste/panierPublic.xsl"),array("langue"=>$_SESSION['langue'],"profil"=>User::getInstance()->getTypeLog()));

echo $content_export;
?>