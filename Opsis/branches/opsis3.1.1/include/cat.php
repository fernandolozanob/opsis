<?
global $db;
$myUser=User::getInstance();
$myPage=Page::getInstance();
$myPage->nomEntite=kCategorie;

require_once(modelDir.'model_categorie.php');
require_once(libDir."/fonctionsGeneral.php");


// I. Ouverture enregistrement
$myCat= new Categorie();

if(isset($_SESSION['USER']['layout_opts']['layout'])){
	$layout = $_SESSION['USER']['layout_opts']['layout'];
}else{
	$layout = '2';
}

if(isset($_GET["id_cat"]))
{
	$id_lang=strtoupper($_SESSION["langue"]);
	$id_cat=intval($_GET["id_cat"]);
	$myCat= new Categorie($id_cat,$id_lang);

	$myCat->arrDoc = $myCat->getAccessiblesDocs();
	
	$myPage->titre=str_replace(chr(34),'',(isset($my_cat->t_cat['CAT_NOM']) && !empty($my_cat->t_cat['CAT_NOM'])?$my_cat->t_cat['CAT_NOM']:''));
	$myPage->setReferrer(true);
	
	include(getSiteFile("formDir","cat.inc.php"));
	
	$content.= $myCat->xml_export();
	//debug($content);

	$contentnohighlight=$content;

	$content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$content."\n</EXPORT_OPSIS>";
	$contentnohighlight = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$contentnohighlight."\n</EXPORT_OPSIS>";

	if (file_exists(designDir."catAff.xsl")) $xsl=designDir."catAff.xsl";
	else $xsl=getSiteFile("designDir","detail/catAff.xsl");

	$arrPrms = array("profil"=>User::getInstance()->getTypeLog(),
	   "storyboardChemin"=>storyboardChemin
	); 
	
	// transfert des options layout_opts de l'urlaction courante vers les param4XSL
	if(isset($_SESSION['USER']['layout_opts'][$myPage->urlaction]) && !empty($_SESSION['USER']['layout_opts'][$myPage->urlaction]) && is_array($_SESSION['USER']['layout_opts'][$myPage->urlaction])){
		
		foreach($_SESSION['USER']['layout_opts'][$myPage->urlaction] as $layout_opt_key=>$layout_opt_val){
			$arrPrms[$layout_opt_key] = $layout_opt_val;
		}
	}
	
	$contentExport =TraitementXSLT($content,$xsl,$arrPrms,0,$contentnohighlight,null);

	$contentExport = eval("?".chr(62).$contentExport.chr(60)."?");
	echo $contentExport;
}else{
	$myPage->setReferrer(true);
}
?>
