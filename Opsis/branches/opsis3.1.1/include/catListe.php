<?php

global $db;

require_once(libDir."class_chercheCat.php");

$myPage=Page::getInstance();
$myUser=User::getInstance();
$myPage->titre=kGestionCategories;
$myPage->setReferrer(true);

$mySearch=new RechercheCat();
if(isset($_GET["init"])) // réinit recherche
	$mySearch->initSession();
	
$mySearch->affichage = "LISTE";

if (isset($_REQUEST['affichage']))
	$mySearch->affichage = $_REQUEST['affichage'];
elseif (isset($_SESSION[$mySearch->sessVar]['affichage']))
	$mySearch->affichage = $_SESSION[$mySearch->sessVar]['affichage'];

	
if(isset($_POST["F_cat_form"])) //Recherche par formulaire
{
	$mySearch->prepareSQL();
	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs'],$_POST['chNoHist']);
	$mySearch->appliqueDroits();
	$mySearch->finaliseRequete(); //fin et mise en session
}

//var_dump($mySearch);

if (file_exists(formDir."catListe.inc.php")) require_once(formDir."catListe.inc.php");
elseif (file_exists(formDirCore."catListe.inc.php")) require_once(formDirCore."catListe.inc.php");
else {
	echo '<div id="pageTitre" class="title_bar">';
	echo '<div id="backButton"><a class="icoBackSearch" href="index.php?urlaction=admin">'.kRetourAdmin.'</a></div>';
	echo kGestionCategories.'</div>';

    require_once(libDir."class_formCherche.php");
    $xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheCat.xml"));
    $myForm=new FormCherche;
    $myForm->chercheObj=$mySearch;
    $myForm->entity="CAT";
    $myForm->classLabel="label_champs_form";
    $myForm->classValue="val_champs_form";
    $myForm->display($xmlform);
}


if(isset($_SESSION[$mySearch->sessVar]["sql"])) 
{
	$sql=$_SESSION[$mySearch->sessVar]["sql"];
	//echo 'REQUETE SQL : '.$sql;

	debug($sql);

    if ($mySearch->affichage=='HIERARCHIQUE') {
        
        echo "
        <script>
        function dspClock (toggle) {
            
            if (toggle) {
                document.getElementById('clock').style.display='block';
                if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='none';
            }
            else {
                document.getElementById('clock').style.display='none';
                if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='block';
            }
            return true;
        }
        </script>
        <div id='clock' style='display:none;text-align:center'>".kPatientez."<br/><img src='".imgUrl."wait30trans.gif'></div>
        <link rel='stylesheet' href='".libUrl."webComponents/tafelTree/tree.css' type='text/css' />
        <script type='text/javascript' src='".libUrl."webComponents/prototype/prototype.js'></script>
        <script type='text/javascript' src='".libUrl."webComponents/prototype/scriptaculous.js'></script>
        <script type='text/javascript' src='".libUrl."webComponents/tafelTree/Tree.js'></script>
        <div align='center'  style='width:600px;border:1px solid #999999;' >
        <script>dspClock(true)</script>
        ";
        require_once(libDir."class_tafelTree.php");
        $mySearch->treeParams=array("ID_TYPE_CAT"=>$mySearch->getValueForField(array('FIELD'=>'TYPE_CAT','TYPE'=>'CI')));
        $btnEdit=$myPage->getName()."?urlaction=catSaisie&id_cat=";
        
        $myAjaxTree=new tafelTree($mySearch,'form1');
        $myAjaxTree->JSReturnFunction=$jsFunction;
        
        $myAjaxTree->makeRoot();
        $myAjaxTree->JSlink="window.location.href='".Page::getName()."?urlaction=catSaisie&id_cat=%3\$s'"; //Le lien sur les terme ouvrira l'édition
        //Récupération du type de recherche
        $title=GetRefValue('t_type_cat',$mySearch->getValueForField(array('FIELD'=>'TYPE_CAT','TYPE'=>'CI')),$_SESSION['id_lang']);
        
        
        if ($mySearch->getValueForField(array('FIELD'=>'CAT_NOM','TYPE'=>'FT'))!=''
            || $mySearch->getValueForField(array('FIELD'=>'CAT_NOM','TYPE'=>'C'))!='' ) { //En plus, on a une recherche sur un terme
            $rst=$db->GetAll($sql); // Lancement de la requete.
            if (!empty($rst)) {
                foreach ($rst as $row) { //Pour chaque résultat...
                    $myAjaxTree->revealNode($row['ID_CAT']);
                }
                echo "<div class='story'>".count($rst)." ".kResultats."</div>";
            } else echo "<div class='story'>".kAucunResultat."</div>";
        }
        $myAjaxTree->renderOutput('',true,$btnEdit);
        echo "<div id='myTree'></div>";
        echo "<script>dspClock(false)</script>
        </div>";
        
    }
    else
    {
        //Sauvegarde du nombre de lignes de recherche ? afficher :
        if($_REQUEST['nbLignes'] != '')
            $_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];

        // ajout paramètre gNbLignesDefaut par défaut
        $nbDeft=(defined("gNbLignesListeDefaut")?gNbLignesListeDefaut:10);
        
        $my_sort=$myPage->getSort(true,$deft);
        if ($deft==false)
            $_SESSION[$mySearch->sessVar]["tri"]=$my_sort;
        if ($deft==true && $_SESSION[$mySearch->sessVar]["tri"]=='')
            $_SESSION[$mySearch->sessVar]["tri"]=$my_sort;
            
        if(isset($_SESSION[$mySearch->sessVar]["tri"]))
            $sql.= $_SESSION[$mySearch->sessVar]["tri"];

        if(!isset($_SESSION[$mySearch->sessVar]['nbLignes']))
            $_SESSION[$mySearch->sessVar]['nbLignes'] = $nbDeft;
			
			
        $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

        print("<div class='errormsg'>".$myPage->error_msg."</div>");

        $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
        $param["form"] = 'documentSelection';
        $param["page"] = $myPage->page;
        $param["titre"] = urlencode(kLexique);
        if ($_SESSION[$mySearch->sessVar]['nbLignes']=="all") 
            $param["offset"] = "0";
        else 
            $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
            
        $_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
        // VP 08/04/10 : ajout paramètre xmlfile
        $param["xmlfile"]=getSiteFile("listeDir","xml/catListe.xml");
        $myPage->addParamsToXSL($param);
        $myPage->afficherListe("t_categorie",getSiteFile("listeDir","catListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
    }
}

?>
