<?
// **** OBSOLETE : UTILISER CONTROLLER_DOC **** //

try
{
	global $db;
	$myUser=User::getInstance();
	$myPage=Page::getInstance();


	require_once(modelDir.'model_doc.php');
	require_once(libDir.'/class_Basis.php');
	
	if ($myUser->loggedIn()) require_once(modelDir.'model_panier.php');
	else require_once(modelDir.'model_panier_session.php');


	global $db;


	// I. Ouverture enregistrement
	$myDoc= new Doc();

	if(isset($_GET["id_doc"]) || isset($_GET["rang"]))
	{
		if(isset($_GET['selection']) && isset($_SESSION['panier_session'][0]['t_panier_doc'])){
			// Voir le traitement dans include/selection.php pour le fonctionnement complet de la selection en cache (INADL)
			if( !isset($_GET['id_doc']) && isset($_GET['rang'])){
				$data = &$_SESSION['panier_session'][0]['t_panier_doc'][$_GET['rang']-1];
			}else if(isset($_GET['id_doc']) && array_key_exists($_GET['id_doc'],$_SESSION['recherche_DOC']['data'][$_SESSION['DB']])){
				$data = &$_SESSION['recherche_DOC']['data'][$_SESSION['DB']][$_GET['id_doc']];
			}
			
			if(isset($data['DB'])){
				$_SESSION['DB'] = $data['DB'];
			}
			
			$basis_sql=new Basis();
			if (isset($_SESSION['DB']) && !empty($_SESSION['DB']))
				$basis_sql->setDb($_SESSION['DB']);
		
		
		}
		else if(isset($_SESSION["recherche_DOC"]["results"]) && isset($_SESSION["recherche_DOC"]["data"]) 
		&& isset($_SESSION["recherche_DOC"]["page"]) 
		&& isset($_SESSION["recherche_DOC"]["rows"]) 
		&& isset($_SESSION["recherche_DOC"]["nbLignes"]))
		{
			
			// Si rang non défini & id_doc existant mais n'étant pas chargé en cache => on reset le rang sur une valeur de rang qui est chargée
			if (!isset($_GET['rang']) && isset($_GET['id_doc']) && !isset($_SESSION['recherche_DOC']['data'][$_SESSION['DB']][$_GET['id_doc']])){
				$_GET['rang'] = (intval($_SESSION["recherche_DOC"]["page"]-1)*intval($_SESSION["recherche_DOC"]["nbLignes"]))+1;
			}
			
			// Si rang défini => détermine l'id_doc à appeler dans la variable session[recherche_DOC][data]
			if(isset($_GET['rang'])){
				$rang_sess = intval($_GET['rang'] - ((intval($_SESSION["recherche_DOC"]["page"])-1)*(intval($_SESSION["recherche_DOC"]["nbLignes"])))-1);
				$_GET['id_doc'] = $_SESSION["recherche_DOC"]["results"][$rang_sess];
			}
			
			// id_doc défini permet de récupérer le document complet dans la variable session data
			if(isset($_GET['id_doc'])){
				$data = &$_SESSION['recherche_DOC']['data'][$_SESSION['DB']][$_GET['id_doc']];
				
				// Si le rang n'est pas set (=> on est arrivé par une référence directe à l'ID_DOC), alors on va initialiser le rang si il correspond à un des resultats en cache
				if(!isset($_GET['rang']) && array_search($_GET['id_doc'],$_SESSION['recherche_DOC']['results'])!==false){
					$_GET['rang'] = (intval($_SESSION["recherche_DOC"]["page"]-1)*intval($_SESSION["recherche_DOC"]["nbLignes"]))+array_search($_GET['id_doc'],$_SESSION['recherche_DOC']['results'])+1;	
				}
			}
			
			$basis_sql=new Basis();
			if (isset($_SESSION['DB']) && !empty($_SESSION['DB']))
				$basis_sql->setDb($_SESSION['DB']);
		
		}else {
		
			if (!empty($_GET['rang']))
				$rang=intval($_GET['rang']);
			
			$limite='';
			
			
			if (!empty($_GET['id_doc']))
			{
				$id_doc=addslashes($_GET["id_doc"]);
				$sql='SELECT * FROM '.kVueBasis.' WHERE NO=\''.$id_doc.'\'';
				$nb_lignes=10;
				$page=1;
				$highlight=false;
			}
			else
			{
				$sql=$_SESSION["recherche_DOC"]["sql"];
				// VP 12/11/2012 : modif requête pour ramener tous les champs
				$data_sql=explode(' WHERE ',$sql);
				$sql='SELECT * FROM '.kVueBasis.' WHERE '.$data_sql[1];
				if(isset($_SESSION["recherche_DOC"]["tri"])) $sql.= $_SESSION["recherche_DOC"]["tri"];
				$nb_lignes=1;
				$page=$rang;
				$highlight=true;
			}
			

			$basis_sql=new Basis();
			
			if (isset($_SESSION['DB']) && !empty($_SESSION['DB']))
				$basis_sql->setDb($_SESSION['DB']);
			
			try
			{
				$data=$basis_sql->Execute($sql,$nb_lignes,$page,$highlight);
			}
			catch(BasisException $e)
			{
				echo 'Erreur requete : '.$e->getMessage();
				echo 'Erreur requete : '.$e->getLine();
			}
			
			
			$data=xml2tab($data);
			$data=$data['DATAS'][0]['DATA'][0];
			$data=$data['ROW'][0];
		}
			
		logAction("DOC",array("ID_DOC"=>$data['NO'],'ACT_REQ'=>$data['TI'],"ACT_SQL"=>$data['NO'],"ACT_BASE"=>$_SESSION['DB']));
		

		$myDoc->t_doc=$data;
		
				
		include (getSiteFile("designDir","menuDoc.inc.php"));
		
		
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$xml.= "<EXPORT_OPSIS>\n";
		$xml.= "\t<t_doc id_doc=\"0\">\n";
		
		foreach ($myDoc->t_doc as $nom_champ=>$champ)
		{
			$xml.= "\t\t<".$nom_champ.">".str_replace(('&'),('&amp;'),$champ)."</".$nom_champ.">\n";
		}
		
		
		$xml.= "\t</t_doc>\n";
		$xml.= "</EXPORT_OPSIS>\n";
	
	
		
		if (file_exists(designDir."docAffBasis.xsl"))
			$xsl=designDir."docAffBasis.xsl";
		else
			$xsl=getSiteFile("designDir","detail/docAffBasis.xsl");
			
		if (!isset($xmlfile) || empty($xmlfile))
			$xmlfile = "docAffBasis.xml";
		

		
		$contentExport =TraitementXSLT($xml,$xsl,array("profil"=>User::getInstance()->getTypeLog(),
																				"gestPers"=>gGestPers,
																				"id_priv_us_doc"=>$id_priv,
																				"loggedIn"=>loggedIn(),
																				"gPlayerVideo"=>gPlayerVideo,
																				"storyboardChemin"=>storyboardChemin,
																				"tab"=>$_GET['tab'],
																				"docAccChemin"=>kDocumentUrl,
																				"rang"=>$rang,
																				"nbDocs"=>$_SESSION['recherche_DOC']['doc_rows'],
																				"xmlfile"=>getSiteFile("designDir","detail/xml/$xmlfile"),
																				"id_panier"=>$_GET['id_panier']
																				),0);//,$contentnohighlight,$_SESSION['recherche_DOC']['highlight']);


	   		
			$contentExport = str_replace(array('&lt;','&gt;'),array("<",">"),$contentExport);
			$contentExport = eval("?".chr(62).$contentExport.chr(60)."?");
	   		echo $contentExport;		
	}
	
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?>