<?php

//liste des rapport mensuels

// on recupere les donnees concernant les rapport mensuels
$liste_rapports=list_directory(kRapportDir);
$donnees=array();

$xml='<?xml version="1.0" encoding="utf-8"?>';
$xml.='<select>';
foreach ($liste_rapports as $i=>$data)
{
	$donnees[$i]=array();
	$donnees[$i]['nom_fichier']=$data;
	sscanf(basename($data),'RPOPS%2d%2d.pdf',$donnees[$i]['annee'],$donnees[$i]['mois']);
	$donnees[$i]['size']=filesize($donnees[$i]['nom_fichier']);
	$xml.='<rapport>';
	
	$xml.='<annee>'.($donnees[$i]['annee']+2000).'</annee>';
	$xml.='<mois>'.$donnees[$i]['mois'].'</mois>';
	$xml.='<nom_fichier>'.basename($donnees[$i]['nom_fichier']).'</nom_fichier>';
	$xml.='<chemin_fichier>'.rapportServer.basename($donnees[$i]['nom_fichier']).'</chemin_fichier>';	
	
	$xml.='</rapport>';
}
$xml.='</select>';

$html=TraitementXSLT($xml,getSiteFile('listeDir','listeRapport.xsl'),null);
//$xml, $nomFichierXsl, $arrProfil,$print=1,$securexml="",$arrHighlight=null,$htmlOutput=true

echo $html
?>