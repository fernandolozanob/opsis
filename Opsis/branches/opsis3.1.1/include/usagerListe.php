<?php

require_once(libDir."class_chercheUsager.php");
require_once(modelDir.'model_usager.php');
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();
$myPage->titre=kGestionUtilisateurs;
$myPage->setReferrer(true);
$mySearch=new RechercheUsager();


	if(isset($_GET["init"])) $mySearch->initSession(); // réinit recherche

	 if(isset($_GET["defaultSearch"])){
			$mySearch->prepareSQL();
            if(isset($_GET["statut"])) { $mySearch->addStatutSQL($_GET["statut"]); }
			$mySearch->finaliseRequete(); //fin et mise en session
	}
    
	
    if(isset($_POST["commande"])){

        if(strcmp($_POST["commande"],"SUP")==0) {
            $myUsr = New Usager();
            $myUsr->t_usager['ID_USAGER']=$_POST["ligne"];
            $myUsr->delete();
        }
    }


    if(isset($_POST["F_usager_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
		$mySearch->appliqueDroits();
 		$mySearch->finaliseRequete(); //fin et mise en session
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION[$mySearch->sessVar]["tri"]=$myPage->getSort();



// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
//require_once(getSiteFile("formDir","usagerListe.inc.php"));
// MS normalisation du nom du fichier modifiable, usagerListe.inc.php => chercheUsager.inc.php 
// on laisse l'ancienne option pour rétrocompatibilité 
if (file_exists(formDir."chercheUsager.inc.php")) require_once(formDir."chercheUsager.inc.php");
else if (file_exists(formDirCore."chercheUsager.inc.php")) require_once(formDirCore."chercheUsager.inc.php");
else if (file_exists(formDir."usagerListe.inc.php")) require_once(formDir."usagerListe.inc.php");
else if (file_exists(formDirCore."usagerListe.inc.php")) require_once(formDirCore."usagerListe.inc.php");
else {

	echo "<div id='pageTitre'>". kListeUsagers ."</div>";

	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheUsager.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="USAGER";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
}
	
	
	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql=$_SESSION[$mySearch->sessVar]["sql"];

		//debug($sql);

	    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

	    //Sauvegarde du nombre de lignes de recherche � afficher :
	    if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
	    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

        // VP 24/08/16 : ajout paramètre gNbLignesDefaut par défaut
        $nbDeft=(defined("gNbLignesListeDefaut")?gNbLignesListeDefaut:10);
	    if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = $nbDeft;
	    $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

	    print("<div class='errormsg'>".$myPage->error_msg."</div>");

	    $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	    $param["page"] = $myPage->page;
	    $param["titre"] = urlencode(kUsager);
		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		// VP 08/04/10 : ajout paramètre xmlfile 
		$param["xmlfile"]=getSiteFile("listeDir","xml/usagerListe.xml");
	    $myPage->addParamsToXSL($param);

	    $myPage->afficherListe("t_usager",getSiteFile("listeDir","usagerListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);

	}


?>
