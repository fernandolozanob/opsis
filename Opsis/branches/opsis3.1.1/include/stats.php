<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- Développeurs : Vincent Prost, François Duran, Jérome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
// STATISTIQUES
// Nombre de connexions : en pages ou en hits, provenance
// Méthodes d’interrogation : dossiers thématiques, recherche simple ou avancée
// Utilisateurs (nombre et type) : sur usagers authentifiés et selon des catégories (associatif, enseignants chercheurs, professionnel AV)
/*-----------------------------------------------------------------------------------*/

// VP : 12/12/2005 : création


	$myUsr=User::getInstance();
	$myPage=Page::getInstance();
	global $db;
 	//mise en session des paramètres
 	if (!empty($_POST['bOK'])) {
 		$_SESSION["stats"]=$_POST;
 		foreach($_POST as $var=>$val) {
 			$$var=str_replace('"', '', $val);
 		}
 	}
 	elseif (isset($_SESSION["stats"]))
 	{
 		foreach($_SESSION["stats"] as $var=>$val) $$var=$val;
 	}
	if(isset($_GET["init"])) { 
		unset($stat_num);
		unset($stat_date_deb);
		unset($stat_date_fin);
		unset($stat_usager);
		unset($stat_type_usager);
		
	}	
 	if ($myPage->getPageFromURL()!="") $_SESSION["stat_page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION["stat_tri"]=$myPage->getSort();

?>

<div id="pageTitre"> <?=kStatistiques?></div>
<div class="feature">
<? include_once(getSiteFile("formDir",'stats.inc.php'));

	if (isset($stat_num) && !empty($tStat[$stat_num])) {
		// Construction requête
		$sql=$tStat[$stat_num];

		if(strpos(strtoupper($sql)," ORDER BY ")>0){
			$sqlOrder = substr($sql,strpos(strtoupper($sql)," ORDER BY "));
			$sql = substr($sql,0,strpos(strtoupper($sql)," ORDER BY "));
		}
		if(strpos(strtoupper($sql)," GROUP BY ")>0){
			$sqlGroupe = substr($sql,strpos(strtoupper($sql)," GROUP BY "));
			$sql = substr($sql,0,strpos(strtoupper($sql)," GROUP BY "));
		}
		if(strpos(strtoupper($sql)," WHERE ")>0){
			$sqlWhere = substr($sql,strpos(strtoupper($sql)," WHERE "));
			$sql = substr($sql,0,strpos(strtoupper($sql)," WHERE "));
		}

		
		if(defined('kDbType') and (strcasecmp(kDbType,'postgres') == 0 || strcasecmp(kDbType,'postgres7') == 0)) {
			$funcDate="to_char";
			$dateY="YYYY";
			$dateM="mm";
			$dateD="dd";
		} else {
			$funcDate="DATE_FORMAT";
			$dateY="%Y";
			$dateM="%m";
			$dateD="%d";
		}

		if($stat_date_deb!=''){
			if(isset($sqlWhere)) $sqlWhere.=" and ".$funcDate."(t_action.ACT_DATE,'".$dateY."-".$dateM."-".$dateD."')>='".fDate($stat_date_deb)."' ";
			else $sqlWhere=" where ".$funcDate."(t_action.ACT_DATE,'".$dateY."-".$dateM."-".$dateD."')>='".fDate($stat_date_deb)."' ";
		}
		if($stat_date_fin!=''){
			if(isset($sqlWhere)) $sqlWhere.=" and ".$funcDate."(t_action.ACT_DATE,'".$dateY."-".$dateM."-".$dateD."')<='".fDate($stat_date_fin)."' ";
			else $sqlWhere=" where ".$funcDate."(t_action.ACT_DATE,'".$dateY."-".$dateM."-".$dateD."')<='".fDate($stat_date_fin)."' ";
		}
		//PC 08/03/11 : "2" remplacé par kLoggedDoc si il existe
		// MS 20/04/15 - réécriture de la gestion stat_type_usager + optimisation si la requete originale contient déja une jointure vers t_usager...
		if(isset($stat_type_usager) && $stat_type_usager!=''){
			if(isset($sqlWhere)){
				if(strpos($sql ,'join t_usager')!==false){
					$sqlWhere.=" and t_usager.US_ID_TYPE_USAGER<".(defined("kLoggedDoc")?kLoggedDoc:2)." ";
				}else{
					$sqlWhere.=" and t_action.ID_USAGER not in(select ID_USAGER from t_usager where US_ID_TYPE_USAGER>=".(defined("kLoggedDoc")?kLoggedDoc:2).") ";
				}
			}else{
				$sqlWhere=" where t_action.ID_USAGER not in(select ID_USAGER from t_usager where US_ID_TYPE_USAGER>=".(defined("kLoggedDoc")?kLoggedDoc:2).")";
			}
		}
		
		//@update VG 10/06/2010
		//@update VG 25/06/2010
		if(isset($stat_usager) && $stat_usager!=''){
			$aStatsUsager = explode (',', $stat_usager);
			$allUsager = '';
			foreach ($aStatsUsager as $usager) $allUsagerSQL .= "'".trim($usager)."',";
			$allUsagerSQL = substr($allUsagerSQL, 0, -1);
			if(isset($sqlWhere)){
				if(strpos($sql ,'join t_usager')!==false){
					$sqlWhere.=" and  trim(".$db->Concat('t_usager.US_NOM',"' '",'t_usager.US_PRENOM').") in (".$allUsagerSQL.")";
				}else{	
					$sqlWhere.=" and t_action.ID_USAGER in(select ID_USAGER from t_usager where trim(".$db->Concat('US_NOM',"' '",'US_PRENOM').") in (".$allUsagerSQL."))";
				}
			}else{
				$sqlWhere=" where t_action.ID_USAGER in(select ID_USAGER from t_usager where trim(".$db->Concat('US_NOM',"' '",'US_PRENOM').") in ( ".$allUsagerSQL."))";
			}
		}
		
		if (!isset($_SESSION['stat_tri']))
			$_SESSION['stat_tri']=null;
		
		$tmp_tri = explode(" ",trim($_SESSION['stat_tri']));
		if(isset($_SESSION['stat_tri']) && preg_match("/(.| )".$tmp_tri[2]."( |,)/",$sql,$matches)==0 ){
			$_SESSION['stat_tri'] = $sqlOrder;
		}
		
		if(isset($sqlWhere)) $sql.= $sqlWhere;
		if(isset($sqlGroupe)) $sql.= $sqlGroupe;
		if(isset($sqlOrder) && ! isset($_SESSION["stat_tri"])) $sql.= $sqlOrder;
		 if(isset($_SESSION["stat_tri"])) $sql.= $_SESSION["stat_tri"];

		//Sauvegarde du nombre de lignes de recherche � afficher :
		if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
				$_SESSION['stat_nbLignes'] = $_REQUEST['nbLignes'];}

		if(!isset($_SESSION['stat_nbLignes'])) $_SESSION['stat_nbLignes'] = 10;

		if (strpos(strtoupper($sql)," ORDER ")===false && strpos(strtoupper($sql),"ACT_DATE") > 0) $sql.= " ORDER BY ACT_DATE desc ";

	
		// Sauvegarde requête pour export et impression
		unset($_SESSION["stat_sql"]);
                
//                var_dump($sql);
		$_SESSION["stat_sql"]["sql"]=$sql;

		$tmp_tri = explode(" ",trim($_SESSION['stat_tri']));

		if (!isset($tmp_tri[2]))
			$tmp_tri[2]=null;
		
		if (!isset($tmp_tri[3]))
			$tmp_tri[3]=null;
		
		$myPage->initPager($sql,$_SESSION['stat_nbLignes'],$_SESSION['stat_page']);
	    $param["nbLigne"] = $_SESSION['stat_nbLignes'];
	    $param["page"] = $myPage->page;
	    $param["tri"] = $tmp_tri[2].(($tmp_tri[3] == "DESC" )?"1":"0");
	    $param["titre"] = urlencode(kStatistiques);
		if($_SESSION['stat_nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION['stat_nbLignes']*($myPage->page-1);

	    $myPage->addParamsToXSL($param);
		// Affichage résultat
		$myPage->afficherListe("stat",getSiteFile("listeDir","stats.xsl"));
	}
?>
</div>