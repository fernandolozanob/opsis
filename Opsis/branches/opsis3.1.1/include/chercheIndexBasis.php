<?
    global $db;
  debug($_REQUEST);
  include (libDir.'class_pageBasis.php');
    $myFrame=PageBasis::getInstance();
    $myFrame->getSort();


    //by ld 03/10/08 => pas de setReferrer si on appelle cette palette depuis une palette
    //c'est le cas en saisie personne ou lexique en palette qui peut appeler une autre palette (sel de valeur,...)
    //on ne fait pas le setReferrer car sinon on va revenir sur la liste des valeurs et non sur la liste d'origine (pers, lex, doc)
    //by ld 10/12 => test changé de index=true a palette=false car on peut appeler ceci depuis une page que index
    // (par ex: indexPopup pour gaumont)
    if (strpos($_SERVER['HTTP_REFERER'],'palette.php')===false) $myFrame->setReferrer(true);

    $myUsr=User::getInstance();

    $titre_index =""; // Titre de la page

    $id_champ_appelant=""; // Type de recherche (L, V_DIF, etc...)
    $id_input_result=""; //Champ dans lequel on affiche la réponse (retour de clic)

    $champ=""; // Type de recherche ? OBSOLETE
    $valeur=""; //Valeur recherchée

    $id_type_desc=""; // pas passé en param, extrait de id_champ_appelant
    $dlex_id_role=""; // semble pas utilisé

	$searchParamsForUrl = array(); //@update VG 29/06/2010 : initialisation  du tableau de paramètre à passer par l'url
  //$affich_nb="1";
  $lex_aff=0;
  $type_lex="";
    //debug($_REQUEST);

    if(isset($_REQUEST['titre_index'])) $titre_index = urldecode(trim($_REQUEST['titre_index']));
    if(isset($_REQUEST['id_input_result'])) $id_input_result = urldecode(trim($_REQUEST['id_input_result']));
    if(isset($_REQUEST['champ'])) $id_champ_appelant = trim($_REQUEST['champ']);
	if(isset($_REQUEST['autreChamps'])) $autreChamps = $_REQUEST['autreChamps'];
    if(isset($_POST['valeur'])) $_GET['valeur']=$_POST['valeur'];

	if(isset($_REQUEST['page']))
		$myFrame->page=$_REQUEST['page'];
	
    $valeur = trim($_GET['valeur']);

    if(isset($_REQUEST["id_lang"])) $id_lang = strtoupper($_REQUEST["id_lang"]);
  else $id_lang=$_SESSION['langue']; //Langue de recherche, mis par déft avec la session par LD le 14/09/07

    if (isset($_REQUEST["affich_nb"])) $affich_nb=$_REQUEST["affich_nb"];

    if (isset($_REQUEST["rtn"])) $jsFunction=$_REQUEST['rtn']; else $jsFunction='addValue';


// VP 28/06/11 : ajout paramètre tri pour les arbres hiérarchiques
	if (isset($_REQUEST["tri"])) $tri=$_REQUEST['tri'];

  // ID courante envoyée par la page : cette ID sert surtout à être filtrée de la recherche.
  // Ex : on ne veut lier un doc à lui-même ou un lex à lui-même.
  if (isset($_REQUEST["currentId"])) $currentId=$_REQUEST["currentId"];

  // VP 10/3/10 : Correction bug si xsl vide
  if (isset($_REQUEST['xsl'])) $xsl=$_REQUEST['xsl'];
  else $xsl='paletteSimpleBasis';

  $blnPrivilegeDoc=false; // Pas de filtrage sur l'affichage des documents


  //debug($sql);
?>
<script>
  function dspClock (toggle) {

    if (toggle) {
      document.getElementById('clock').style.display='block';
      if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='none';
      }
    else {
      document.getElementById('clock').style.display='none';
      if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='block';
      }
    return true;
  }
</script>
 <form align='center' name="form1" id="form1" onSubmit="dspClock(true)" method="post" action="<?=$myFrame->getName()."?urlaction=chercheIndexBasis".$myFrame->addUrlParams()?>" >
  <fieldset >
  <legend><?= $titre_index ?></legend>
    <input name="ligne" type="hidden" value="" />
    <input name="id_type_val" type="hidden" value="<?= $id_type_val ?>" />
    <input name="affich_nb" type="hidden" value="<?= $affich_nb?>" />
    <input name="id_doc" type="hidden" value="<?= $id_doc ?>" />
    <input name="titre_index" type="hidden" value="<?= urlencode($titre_index) ?>" />
    <input name="id_input_result" type="hidden" value="<?= $id_input_result ?>" />
    <input name="champ" type="hidden" value="<?= $id_champ_appelant ?>" />
    <!--input name="valeur" type="hidden" value="<?= str_replace('"','&quot;',$valeur) ?>" /-->
  <!-- ' -->
    <input name="id_lang" type="hidden" value="<?= $id_lang ?>" />
    <input name="type_lex" type="hidden" value="<?= $type_lex ?>" />
  <input name="val_id_gen" type="hidden" value="<?= $val_id_gen ?>" />
  <input name="page" type="hidden" value="<?=$myFrame->page?>" /> <!-- juste utilisé pour la input box -->


        <?

    //Vérif autorisation visionnage hiérarchique
    // VP 17/03/09 : ajout LFC à types hiérarchiques
    if (in_array($desc_champs_appelant[0],array('LF','L','LC','LFC'))) { //rech lexique
      $hier=$db->GetOne('SELECT HIERARCHIQUE FROM t_type_lex WHERE id_type_lex='.$db->Quote($type_lex));
      if ($hier!='1') {$chooseDisplayMode=false;$lex_aff=1;} //forçage du mode alpha seul si pas hiérarchique
    }
    require(getSiteFile("formDir","formPalette.php"));

        ?>
  </fieldset>
  </form>
<div id='clock' style='display:none;text-align:center'><?=kPatientez?><br/><img src="<?=imgUrl?>wait30trans.gif"></div>
<?
	global $db;

    // VP 27/09/2012 : initialisation base
    if(!empty($_SESSION['DB'])) $myFrame->setDatabase($_SESSION['DB']);
    
    // I. Affichage Alphabétique
	$champs=explode(',',$_GET['champ']);
	$champ=$champs[0];
	
	$sql='BROWSE INDEX OF '.kVueBasis.'.'.$champ;
	
	if (!empty($_REQUEST['valeur']))
	{
		$sql.=' FROM \'%'.str_replace('\'','\\\'',$_REQUEST['valeur']).'%\'';
	}
	
	//echo $sql;
	
	debug($sql, "gold", true);
		
	$nb2show=(defined("gPaletteNbParDefaut")?gPaletteNbParDefaut:15);
	debug($sql, "gold", true);
	$param["affich_nb"] = $affich_nb;
	$param["titre_index"]=$titre_index;
	$param["btn_edit"]=$btnEdit;
	$param["jsFunction"]=$jsFunction;
	$param["id_input_result"]=$id_input_result;
	$param["page"]=$myFrame->page;
	$myFrame->initPager($sql,$nb2show,null,null,60,false);
	
	if (!empty($w))
		$param["w"]='&w='.$w;
	if (!empty($h))
		$param["h"]='&h='.$h;
	
	unset($_GET['lex_aff']); //experimental
	$param["get_params"]=$myFrame->addUrlParams($searchParamsForUrl, true); //@update VG 29/06/2010 : ajout du tableau de paramètres de la recherche(rempli ou vide) pour l'url

	$myFrame->addParamsToXSL($param);
	$myFrame->afficherListe('val',getSiteFile("listeDir",$xsl.'.xsl'),$blnPrivilegeDoc);
      ?>
