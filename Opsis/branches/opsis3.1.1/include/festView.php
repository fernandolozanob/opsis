<?
    require_once(modelDir.'model_festival.php');
    global $db;
    
	$myUsr=User::getInstance();
	$myPage=Page::getInstance();
	$myPage->nomEntite=kFestival;
// I. Initialisation
    $myFest = New Festival();
    // I.1. Liste des langues
	$t_lang=array_ucase($_SESSION["arrLangues"]);
    
    
    if(isset($_GET["id_lang"])) $myFest->t_fest['ID_LANG']=$_GET["id_lang"];
    else $myFest->t_fest['ID_LANG']= $_SESSION["langue"];
    
    // I.2.Recup Id
    $myFest->t_fest['ID_FEST']=$_REQUEST["id_fest"];
    // III.2. Chargement des informations
    $myFest->getFest();
    $myFest->getSections(true); //on récupère aussi les documents
    $myFest->getDocs();
    $myFest->getLexique();
    $myFest->getDocAcc();
    $myFest->getValeurs();
    $myFest->getPersonnes();

		
	$myPage->titre = (isset($myFest->t_fest['FEST_LIBELLE']) && !empty($myFest->t_fest['FEST_LIBELLE'])?$myFest->t_fest['FEST_LIBELLE']:'');
	$myPage->setReferrer(true);
	
//debug($myFest,'pink');

?>
        <div id="pageName">
            <? include(getSiteFile("designDir","menuFest.inc.php")); ?>
        </div>
<?
	$content.= $myFest->xml_export();
	//debug($content,'beige');

  	$contentnohighlight=$content;

	$content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$content."\n</EXPORT_OPSIS>";
    $contentnohighlight = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$contentnohighlight."\n</EXPORT_OPSIS>";

    $contentExport =TraitementXSLT($content,getSiteFile("designDir","festAff.xsl"),array("profil"=>User::getInstance()->getTypeLog(),
				"loggedIn"=>loggedIn(),
				"tab"=>$_GET['tab'],																		
				"docAccChemin"=>kDocumentUrl,
				"rang"=>$rang,
				"nbFest"=>$_SESSION['recherche_FEST']['rows'],
				"xmlfile"=>getSiteFile("designDir","detail/xml/festAff.xml"),),0,$contentnohighlight,$_SESSION['recherche_FEST']['highlight']);

	$contentExport = eval("?".chr(62).$contentExport.chr(60)."?");
	echo $contentExport;
?>