<?php

require_once(modelDir.'model_usager.php');

global $db;

$myPage=Page::getInstance();

if(!isset($_GET['id']) && isset($_GET['loginValidate'])){
	$_GET['id'] = $_GET['loginValidate'];
}

if (!empty($_GET['id']))
{
	// ondecrypte les informations
	// les valeurs sont 0=>ID_USAGER,1=>US_LOGIN
	// $valeurs=unserialize(password_decode(kCleCryptage,$_GET['id']));
	
	require_once(libDir."class_RSA.php");
	$rsa = new RSA(tokenRSAModulus,tokenRSAExponentPrivate);
	$loginValidate_token = $rsa->decrypt(base64_decode($_REQUEST['id']));
	$loginValidate_token = explode("|",$loginValidate_token) ;
	$loginValidate_id_user = $loginValidate_token[1];
	$loginValidate_login = $loginValidate_token[2];
	$loginValidate_time = $loginValidate_token[0];!
	
	$loginValidate_time = (microtime(true) - $loginValidate_time );
	$loginValidate_time = ($loginValidate_time / 3600);
	
	if (!empty($loginValidate_id_user) && !empty($loginValidate_login) && !empty($loginValidate_time))
	{
		if($loginValidate_time < 24*7 ){    // pour l'instant, le temps de validité d'un token d'inscription émis par opsis est donc de 7 jours
			//obligé de mettre AS COMPTE (en majuscule) sinon cela passe pas sur mysql
			$result=$db->Execute('SELECT COUNT(*) AS COMPTE FROM t_usager WHERE ID_USAGER='.intval($loginValidate_id_user)." AND US_LOGIN='".$loginValidate_login."'")->GetRows();
			if (intval($result[0]['COMPTE'])==1)
			{
				$my_usager = new Usager();
				$my_usager->t_usager['ID_USAGER']=$loginValidate_id_user;
				$my_usager->getUsager();
				//$my_usager->getUsager($loginValidate_id_user);
				//$my_usager->t_usager=$my_usager->t_usager[0];
				if($my_usager->t_usager['US_ID_ETAT_USAGER'] == Usager::getIdEtatUsrFromCode('INACTIF')){
					$loginValidateState =( defined("gLoginValidateUserToStatus")?gLoginValidateUserToStatus:'ACTIF' ); 
					$my_usager->t_usager['US_ID_ETAT_USAGER']=Usager::getIdEtatUsrFromCode($loginValidateState); // pour l'instant j'ai transposé US_STATUT= 1 à US_ID_ETAT_USAGER = [id du code 'ACTIF'], 																		 // mais devrait éventuellement être à MAIL_VALID (voir rétrocompat avec disney..)
					$my_usager->save();
					// trace("new etat usager : ".(Usager::getIdEtatUsrFromCode($loginValidateState)));
					if($loginValidateState == 'ACTIF'){
						// dans ce cas on sait qu'on est passé de 'INACTIF' à 'ACTIF', on va donc logger automatiquement l'utilisateur
						require_once(libDir."class_user.php");
						$myUsr= new User() ; 
						$myUsr->Login($my_usager->t_usager['US_LOGIN'],$my_usager->t_usager['US_PASSWORD'],'',false,true);
					}
					$user_state_updated  = true ; 
				}else{
					$user_state_updated = false ; 
				}
			}
		}
	}
}

include(getSiteFile("designDir","loginValidate.inc.php"));
?>
