<?
require_once(libDir."class_chercheTape.php");
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();

$myPage->titre=kCartouches;
$myPage->setReferrer(true); 
$mySearch=new RechercheTape();


	if(isset($_GET["init"])) {$mySearch->initSession(); }// réinit recherche

	if(isset($_REQUEST["commande"])){
        switch($_REQUEST["commande"]){
            case "listing": // Listing 
                ini_set('max_execution_time','0');
                 // Création Job Backup
                require_once(libDir."class_backupJob.php");
                $myBackupJob=new BackupJob();
                // Listing cartouches 
                $ok=$myBackupJob->updateTapeStatus();
                
                /*
                //PC 01/10/12 : mise à jour de l'état archivage pour tous les fichiers archivés
                require_once(modelDir.'model_materiel.php');
                // VP 19/10/12 : changement requête : ceux qui sont à l'état 3 (archivé en librairie) et ceux qui sont sur une cartouche online
                //$arrArch = $db->getAll("SELECT ID_MAT FROM t_mat WHERE MAT_ID_ETAT_ARCH in (3,4);");
                $arrArch = $db->getAll("SELECT distinct ID_MAT FROM t_mat WHERE MAT_ID_ETAT_ARCH = 3 or
                 ID_MAT in (select TF_ID_MAT from t_tape_file tf INNER JOIN t_tape t ON tf.ID_TAPE=t.ID_TAPE where t.TAPE_STATUS=1)");
                foreach ($arrArch as $arch){
                    $mat = new Materiel();
                    $mat->t_mat['ID_MAT'] = $arch["ID_MAT"];
                    $mat->getMat();
                    $mat->updateEtatArch();
                    unset($mat);
                }
                */
                break;
            case "SUP": // Suppression 
                $id_tape=urldecode($_POST["ligne"]);
                require_once(modelDir.'model_tape.php');
                $mon_objet = New Tape();
                $mon_objet->t_tape['ID_TAPE']=$id_tape;
                $mon_objet->delete();
                break;
        }
	}

    if(isset($_POST["F_tape_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION[$mySearch->sessVar]["tri"]=$myPage->getSort();


    include(getSiteFile("designDir","menuBackup.inc.php"));

	// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
	//include_once(getSiteFile("formDir","chercheTape.inc.php"));
	if (file_exists(formDir."chercheTape.inc.php")) require_once(formDir."chercheTape.inc.php");
	else {
		require_once(libDir."class_formCherche.php");
		$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheTape.xml"));
		$myForm=new FormCherche;
		$myForm->chercheObj=$mySearch;
		$myForm->entity="TAPE";
		$myForm->classLabel="label_champs_form";
		$myForm->classValue="val_champs_form";
		$myForm->display($xmlform);
		
		$param["tapeSetList"] = listOptions("Select distinct ID_TAPE_SET from t_tape_set",Array("ID_TAPE_SET","ID_TAPE_SET"), 0, 0);
		
		if(isset($_REQUEST["commande"]) && strcmp($_REQUEST["commande"],"UpdateSet")==0 ){
            //&& !empty($_POST["id_tape_set"])
			require_once(modelDir.'model_tape.php');
			$id_jeu_1 = $_POST["id_tape_set"];
			$id_jeu_2 = $db->getOne("SELECT TS_ID_TAPE_SET_MIROIR FROM t_tape_set WHERE ID_TAPE_SET = ".intval($id_jeu_1));
					
			foreach($_POST["checkboxTape"] as $id_tape){
				$mon_objet = New Tape();
				$mon_objet->t_tape['ID_TAPE']=$id_tape;
				if (!isset($_POST["repart_mirror"]))
					$mon_objet->updateSet($_POST["id_tape_set"]);
				else {
					$num_tape = intVal(ereg_replace("[^0-9]","",$id_tape));
					if ($num_tape % 2 == 1)
						$mon_objet->updateSet($id_jeu_2);
					else
						$mon_objet->updateSet($id_jeu_1);
				}
			}
		}
	}



	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
		$sql=$_SESSION[$mySearch->sessVar]["sql"];

		if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

		//Sauvegarde du nombre de lignes de recherche � afficher :
		if($_REQUEST['nbLignes'] != '') {
			$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

		if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;

		$myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

		print("<div class='errormsg'>".$myPage->error_msg."</div>");
		$param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
		$param["page"] = $myPage->page;
		$param["id_tape"]=$_REQUEST['id_tape'];

		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		// VP 08/04/10 : ajout paramètre xmlfile 
		$param["xmlfile"]=getSiteFile("listeDir","xml/tapeListe.xml");
		$myPage->addParamsToXSL($param);
		$myPage->afficherListe("tape",getSiteFile("listeDir","tapeListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	}

 ?>
