<?
require_once(modelDir.'model_tapeSet.php');
global $db;

$myUser=User::getInstance();
$myPage=Page::getInstance();
$myTapeSet=New TapeSet();

$action=$_POST["commande"];

if(isset($_POST["bOk"]) || !empty($action))
{
    if(strcmp($action,"SUP")==0) { 
    	$myTapeSet->t_tape_set['ID_TAPE_SET']=$_REQUEST["id_tape_set"];
        $myTapeSet->delete();
        if(!empty($_POST["page"])){
            $myPage->redirect($_POST["page"]);
        }else
        	$myPage->redirect($myPage->getName()."?urlaction=tapeSetListe");
    }
    else if(isset($_POST["bOk"]) || $action=="SAVE" )
    {
		$myTapeSet->updateFromArray($_POST,false);
        $saved=$myTapeSet->save();
    }
}
elseif (isset($_REQUEST["id_tape_set"])) {
	$myTapeSet->t_tape_set['ID_TAPE_SET']=$_REQUEST["id_tape_set"];
	$myTapeSet->getTapeSet();
	$myTapeSet->getTapes(); 
}

if ($_REQUEST['form'] && is_file(getSiteFile("formDir",$_REQUEST['form'].'.inc.php')))
	require_once(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'));
else
	require_once(getSiteFile("formDir",'tapeSetSaisie.inc.php'));
?>