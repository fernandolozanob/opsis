<?php
$user=User::getInstance();
$myPage=Page::getInstance();

require_once(modelDir.'model_panier.php');
$myPanier = New Panier();
$listSelections=Panier::getFolders(true);

global $db;

$myPanier->t_panier['ID_PANIER']=intval($_REQUEST['id_panier']);
$myPanier->sqlOrder=$myPage->getSort(false);

if(isset($_REQUEST["id_panier"])) $id_panier=intval(trim($_REQUEST["id_panier"]));
else $id_panier="";
if (isset($_REQUEST["tri"])) $arrOrders=tri2array($_REQUEST["tri"]); else $arrOrders=null;
if (!empty($myPanier->sqlOrder)) $_SESSION['tri_panier_doc']=$myPanier->sqlOrder;
else unset($_SESSION['tri_panier_doc']);

if(!isset($_POST["commande"]) || empty($_POST["commande"])) {	// Pas de cmd, lecture d'un panier par défaut ou chgt page ou tri, RAZ des statuts précédemment renseignés
	$myPanier->getPanier();
} else {
	$action=$_POST["commande"];
	$ligne=trim($_POST["ligne"]);
	$id_ligne_panier=$_POST["id_ligne_panier".$ligne];
	$id_panier=intval($_REQUEST["id_panier"]);

	$myPanier->t_panier['ID_PANIER']=intval($_REQUEST["id_panier"]);
	$myPanier->currentLine=(integer)trim($_POST["ligne"])-1;
	
    switch($action){
        case 'SUP':
            if(strcmp($ligne,"")==0) {
                $myPanier->deletePanier();
                if (!empty($_POST["page"])) $myPage->redirect($_POST["page"]);
                elseif (!empty($_POST["refer"])) $myPage->redirect($_POST["refer"]);
                else $myPage->redirect($myPage->getReferrer());
            } else {
                $myPanier->getPanier();
                $myPanier->met_a_jour($_POST);
                
                foreach ($myPanier->t_panier_doc as $pdoc)
                    if($_POST["ligne"]==$pdoc['ID_LIGNE_PANIER']) $myPanier->deletePanierDoc($pdoc);
                $myPanier->getPanierDoc();
                $myPanier->dropError(kSuccesPanierDocumentRetire);
            }
            break;
        case "SUP_LIGNES" :

			$myPanier->getPanier();
			$myPanier->met_a_jour($_POST);
	        foreach ($myPanier->t_panier_doc as $pdoc){
			// On ne commande que les lignes qui sont cochées
			//	VP 10/9/09 : changement test sur checkbox
			//	if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1') {
				if(!empty($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
					$myPanier->deletePanierDoc($pdoc);
				}
			}
			$myPanier->getPanier();
 			if ($myPanier->t_panier['PAN_ID_ETAT']>0) $myPanier->dropError(kSuccesPanierDocumentRetire); //succes
			else $myPanier->dropError(kSuccesFolderDocumentRetire); //distinction entre panier et folder (demande clients)
            break;
		
		case 'DUP' :
		case 'DUPLIQUER' :
			$myPanier->getPanier();
			$myPanier->met_a_jour($_POST,1,false);
			if (get_class($myPanier)=='Panier' ){
				$newCart=$myPanier->duplicate($myPanier->t_panier['PAN_ID_GEN']);
			}
			$newCart->getPanier(); //on reload et on passe sur le nouveau panier
			$myPanier=$newCart;

			if ($err==0) $myPanier->dropError(kSuccesPanierDupliquer);
			echo '<script>refreshFolders();</script>';
			break;
            
        case 'SAVE':
            $myPanier->getPanier();	
            $myPanier->met_a_jour($_POST);
            $myPanier->getVignette();
            
			if (empty($myPanier->t_panier["ID_PANIER"]))
				$ok=$myPanier->createPanier();
			else
				$ok=$myPanier->save();
            $ok2=$myPanier->saveAllPanierDoc();
            $myPanier->getPanierDoc();
            if ($ok && $ok2){
                $myPanier->dropError(kSuccesCommandeSauve);
				
				if (isset($_POST['send_mail']) && !empty($_POST['send_mail']) && $_POST['send_mail']=='1')
					$myPanier->send_mail_usager();
            }
			else $myPanier->dropError(kErreurCommandeSauve);
            break;
            
        case 'DUP':
            $myPanier->getPanier();
            $myPanier->met_a_jour($_POST,1,false);

            if (get_class($myPanier)=='Panier' ) {
                $myPanier=$myPanier->duplicate();
            }
            break;
        
		case 'TRANSFERT_PANIER' :

			$id_panier = $myPanier->t_panier['ID_PANIER'];
			$myPanier->getPanier();
			$myPanier->met_a_jour($_POST);

			if(isset($_POST['action_panier']) && ($_POST['action_panier']=="deplacer_selection")){
				if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) {
					$newCart=new Panier();
					
					if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
					else {$name= kSelection.' '.count($listSelections);}
					if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
					if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
					$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen']);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) {
					$newCart=new Panier();
					$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}
				$errDoublons=0;
				foreach ($myPanier->t_panier_doc as $pdoc){
					if(!empty($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
						$pdoc['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++;
					}
				}
				if ($errDoublons>0) {
					$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
				} else {
					$myPanier->dropError(kSuccesPanierTransfert);
					if($_POST['dspDest']=="1"){
						$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
					}
					$param["popup"] =1;	
					$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
				}
				if($_POST['Dest']=="1"){
					$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
				}
			}
			if(isset($_POST['action_panier']) && ($_POST['action_panier']=="deplacer_selection_all")){
				if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) {
					$newCart=new Panier();
					
					if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
					else {$name= kSelection.' '.count($listSelections);}
					if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
					if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
					$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen']);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) { //Création à  la volée d'une nouvelle commande sauf si panier session !
					$newCart=new Panier();
					$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}
				$errDoublons=0;
				foreach ($myPanier->t_panier_doc as $pdoc){
						$pdoc['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on transfère avec check de doublons
					
				}
				if ($errDoublons>0) {
					$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
				} else {
					$myPanier->dropError(kSuccesPanierTransfert);
					if($_POST['dspDest']=="1"){
						$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
					}
					$param["popup"] =1;	
					$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
				}
				if($_POST['Dest']=="1"){
						$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
					}
			}
			if(isset($_POST['action_panier']) && ($_POST['action_panier']=="cp_selection_all")){

				if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) { //Création à  la volée d'un nouveau panier sauf si panier session !
					$newCart=new Panier();
					
					if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
					else {$name= kSelection.' '.count($listSelections);}
					if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
					if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
					$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen']);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) { //Création à  la volée d'une nouvelle commande sauf si panier session !
					$newCart=new Panier();
					$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}
				$errDoublons=0;
				foreach ($myPanier->t_panier_doc as $pdoc){
						$pdoc['ID_LIGNE_PANIER']="";
						 $myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on transfère avec check de doublons
					
				}
				if ($errDoublons>0) {
					$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
				} else {
					$myPanier->dropError(kSuccesPanierTransfert);
					if($_POST['dspDest']=="1"){
						$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
					}
					$param["popup"] =1;	
					$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
				}
				if($_POST['Dest']=="1"){
					$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
				}
			}
		
			//copie selection	
			if(isset($_POST['action_panier']) && ($_POST['action_panier']=="cp_selection")){
				if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) { //Création à  la volée d'un nouveau panier sauf si panier session !
					$newCart=new Panier();
					
					if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
					else {$name= kSelection.' '.count($listSelections);}
					if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
					if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
					$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen']);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) { //Création à  la volée d'une nouvelle commande sauf si panier session !
					$newCart=new Panier();
					$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
					$newCart->createPanier();
					$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}
				$errDoublons=0;
				foreach ($myPanier->t_panier_doc as $pdoc){
					if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1' ) {
						$pdoc['ID_LIGNE_PANIER']="";
						$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++;
					}
				}
				if ($errDoublons>0) {
					$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
				} else {
					$myPanier->dropError(kSuccesPanierTransfert);
					if($_POST['dspDest']=="1"){
						$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
					}	
					$param["popup"] =1;	
					$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
				}
				if($_POST['Dest']=="1"){
					$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
				}
			}
			$myPanier->t_panier['ID_PANIER'] = $id_panier;
			$myPanier->getPanier();
		break;
		
		case 'DUPLIQUER_LIGNES' :

			$myPanier->getPanier();
			$myPanier->met_a_jour($_POST);

			
			if (get_class($myPanier)=='Panier' ) { //Création à  la volée d'un nouveau panier sauf si panier session !
				$newCart=new Panier();
				$newCart->t_panier=array(  //transfert des paramètres et copie des valeur du panier d'origine
										   "PAN_ID_ETAT"=>($myPanier->t_panier['PAN_ID_ETAT']=="1"?"0":$myPanier->t_panier['PAN_ID_ETAT']),
										   "PAN_ID_GEN"=>$myPanier->t_panier['PAN_ID_GEN'],
										   "PAN_DOSSIER"=>$myPanier->t_panier['PAN_DOSSIER'],
										   "PAN_FRAIS_HT"=>$myPanier->t_panier['PAN_FRAIS_HT'],
										   "PAN_TOTAL_HT"=>$myPanier->t_panier['PAN_TOTAL_HT'],
										   "PAN_TVA"=>$myPanier->t_panier['PAN_TVA'],
										   "PAN_OBJET"=>$myPanier->t_panier['PAN_OBJET'],
										   "PAN_XML"=>$myPanier->t_panier['PAN_XML'],
										   "PAN_TITRE"=>$myPanier->t_panier['PAN_TITRE'],
										   "PAN_ID_TYPE_COMMANDE"=>$myPanier->t_panier['PAN_ID_TYPE_COMMANDE'],
										   "PAN_ID_USAGER"=>User::getInstance()->UserID);
				$ok=$newCart->createPanier();
				
				$_REQUEST['id_panier'] = $newCart->t_panier['ID_PANIER'];
				if($myPanier->t_panier['PAN_ID_ETAT']=="0") $myPanier->t_panier['CARTLIST']=$newCart->t_panier['ID_PANIER'];
			}
			$err=0;
			foreach ($myPanier->t_panier_doc as $pdoc){
				// On ne duplique que les lignes qui sont cochées
				//	VP 10/9/09 : changement test sur checkbox
				//if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1') {
				if(!empty($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
					$pdoc['ID_LIGNE_PANIER']="";
					$pdoc['ID_PANIER']=$newCart->t_panier['ID_PANIER'];
					if (!$newCart->savePanierDoc($pdoc)) $err++;
				}
			}
			$newCart->getPanier(); //on reload et on passe sur le nouveau panier
			$myPanier=$newCart;
			if ($err==0) $myPanier->dropError(kSuccesPanierDupliquerLignes);
			//Récup des paniers pour faire le compte des progs dedans
			echo '<script>refreshFolders();</script>';
		break;
		
        case 'LIVRAISON_JOB':	
            // Validation de la commande avec creation d'un job
            if(empty($myPanier->t_panier['ID_PANIER'])) {
                $myPanier->updateFromArray($_POST);
                $myPanier->t_panier['PAN_ID_TYPE_COMMANDE']=$_REQUEST['pan_id_type_commande'];
				$myPanier->met_a_jour_lignes('PDOC_ID_ETAT',gPanierTraitement); // MS 10.07.14 - update de l'état des pdoc ici plutot que après l'initialisation des jobs
                $myPanier->saveAllPanierDoc();
				$myPanier->getCommande();
                $idNewCom = $myPanier->t_panier['ID_PANIER'];
            } else {
                $myPanier->getPanier();
				
				// si l'utilisateur rafraichit la page les job sont relancés et les mail son renvoyés ($_POST['pan_id_etat']<gPanierTraitement).
				// si le panier courant est déjà  en cours de livraison ou déja livré on en met pas a jour le job_id_etat
				// la condition $myPanier->t_panier['PAN_ID_ETAT'] < gPanierTraitement n'est plus vraie
				if($myPanier->t_panier['PAN_ID_ETAT']>=gPanierTraitement)
					unset($_POST['pan_id_etat']);
				
                $myPanier->updateFromArray($_POST);
				$myPanier->met_a_jour_lignes('PDOC_ID_ETAT',gPanierTraitement); // MS 10.07.14 - update de l'état des pdoc ici plutot que après l'initialisation des jobs
                $myPanier->saveAllPanierDoc();
            }		
            
            $myPanier->t_panier['PAN_DATE_COM']=date("Y-m-d H:i:s");
            $myPanier->getDocs();
            $myPanier->getTarifs();
            $myPanier->getPanierDoc();

            //On remet l'ID_PANIER qui peut avoir été mis à jour à vide si le panier a été créé ici
            if(!empty($idNewCom)) {
                $myPanier->t_panier['ID_PANIER'] = $idNewCom;
            }

            // pour afficher le nom et le tcin et le tcout dans le contenu de la commande
            $myPanier->getPanierDoc();
			
			if(isset($_POST['livraison_ftp'])  && !empty($_POST['livraison_ftp'])){
				if(isset($myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN']) && !empty($myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'])){
					$_POST['ftp_login'] = $myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'];
				}else{
					$_POST['ftp_login'] = 'tmp_'.kDatabaseName.'_'.$user->UserID.'_'.time();
					$myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'] = $_POST['ftp_login'];
				}
				if(isset($myPanier->t_panier['XML']['commande']['PANXML_FTP_PW']) && !empty($myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'])){
					$_POST['ftp_pw'] = $myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'];
				}else{
					require_once(libDir."fonctionsGeneral.php");
					$_POST['ftp_pw'] = random_alphanum_string(array('fullrandom_length'=>8));
					$myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'] = $_POST['ftp_pw'];
				}
				$myPanier->t_panier['XML']['commande']['PANXML_FTP_HOST'] = kUrlFtp;
				$myPanier->t_panier['PAN_XML'] = array2xml($myPanier->t_panier['XML']['commande'],'commande');
			}
			
			
			// ajout de ID_PROC a la ligne du panier
			foreach ($myPanier->t_panier_doc as $idx=>$pdoc)
			{
				foreach ($_POST['t_panier_doc'] as $ligne)
				{
					if ($pdoc['ID_DOC']==$ligne['ID_DOC'])
					{
						$myPanier->t_panier_doc[$idx]['ID_PROC']=$ligne['ID_PROC'];
					}
				}
			}
			
            if($myPanier->t_panier['PAN_ID_ETAT'] < gPanierTraitement ){
            
                require_once(libDir.'class_livraison.php');
                
                $nomDossierCourt='P'.$myPanier->t_panier['ID_PANIER'].'_'.$myPanier->t_panier['PAN_ID_USAGER'];
                $livraison=new Livraison();
				
				if (isset($_POST['repertoire_livraison']) && !empty($_POST['repertoire_livraison']) && defined($_POST['repertoire_livraison']))
				{
					$livraison->setDir($nomDossierCourt,constant($_POST['repertoire_livraison']));
				}
				else
					$livraison->setDir($nomDossierCourt);
                $livraison->createDir();
				
                //OLD:$livraison->arrDocs=$myPanier->t_panier_doc;
				//Si un document est un reportage, on lance le job sur tous ses fils
				require_once(modelDir.'model_docReportage.php');
				foreach ($myPanier->t_panier_doc as $pdoc){
					if (Reportage::isReportageEnabled() && ($pdoc['DOC_ID_TYPE_DOC'] == gReportagePicturesActiv)) {
						$tmp_doc=new Doc();
						$tmp_doc->t_doc['ID_DOC']=$pdoc['ID_DOC'];
						$tmp_doc->t_doc['ID_LANG']=$pdoc['ID_LANG'];
						$tmp_doc->getDocLiesDST();
						foreach ($tmp_doc->arrDocLiesDST as $docD) {
							foreach($docD as $f=>$v) if (isset($pdoc[$f])) $pdoc[$f] = $v;
							$livraison->arrDocs[] = $pdoc;
						}
					}
					else $livraison->arrDocs[] = $pdoc;
				}
				
                $id_job_liv=$livraison->prepareLivraison($_POST,$_POST['crit_mat'],$_POST['dynamic_param']);
                
				if (Reportage::isReportageEnabled()) {
					foreach ($myPanier->t_panier_doc as $pdoc){
						foreach ($livraison->arrDocs as $ldoc) if($ldoc['ID_LIGNE_PANIER'] == $pdoc['ID_LIGNE_PANIER']) $pdoc['PDOC_ID_JOB'] = $ldoc['PDOC_ID_JOB'];
						$arrDocs[] = $pdoc;
					}
					$livraison->arrDocs=$arrDocs;
				}
				
                if($id_job_liv){
                    // aprs la livraison on update et save les valeurs de folder out & id_job (dossier cible de livraison, pour récupération ultérieure des fichiers, download, vérification d'existence)
                    $tabxml = xml2array($myPanier->t_panier['PAN_XML']);
                    
                    $tabxml['commande']["PANXML_FOLDER_OUT"] = $livraison->livraisonDir;
                    $tabxml['commande']["PANXML_ID_JOB"] = $id_job_liv;
                    
                    $myPanier->t_panier['PAN_XML'] = array2xml($tabxml['commande'],'commande');
                    unset($tabxml);
                    
                    // récupération des arrDocs (PDOC_ID_JOB renseigné au cours de la livraison => à sauver en base)
                    $myPanier->t_panier_doc = $livraison->arrDocs;
                    
                    // On initialise l'état panier & de chaque panier_doc à gPanierTraitement 
                    // o gPanierTraitement>1 (définition d'un pan de commande) et gPanierTraitement < gPanierFini
                    // Permet de bien identifier un panier en cours de traitement 
                    
					// MS - 10.07.14 - le passage à  gPanierTraitement des panier_doc est déplacé plus haut. dans certains conditions, on overwritais l'état gPanierFini de certaines lignes par gPanierTraitement
					// on passe donc les panier_doc à  gPanierTraitement avant l'initialisation des jobs, et on les unset ici pour ne pas perturber les panier_doc eventuellement déja terminés
					// $myPanier->met_a_jour_lignes('PDOC_ID_ETAT',gPanierTraitement);
					foreach($myPanier->t_panier_doc as $idx=>$pdoc){
						unset($myPanier->t_panier_doc[$idx]['PDOC_ID_ETAT']);
					}                    $myPanier->t_panier['PAN_ID_ETAT'] = gPanierTraitement;
                    $myPanier->saveAllPanierDoc();
                    $myPanier->save();
                    $myPanier->getPanier();
                }
				
				if (isset($_POST['send_mail']) && !empty($_POST['send_mail']) && $_POST['send_mail']=='1' && empty($_POST['no_send_mail_en_cours']))
				{
					if(isset($_POST['noSendMailAdmin']) || isset($_POST['noSendMailUser'])){
						$myPanier->send_mail_usager((isset($_POST['noSendMailAdmin'])?0:1),(isset($_POST['noSendMailUser'])?0:1), '', $mailCommande);
					}else{
						$myPanier->send_mail_usager(1, 1, '', $mailCommande);
					}
				}
				
            }										
            break;
		case 'LIVRAISON_MONTAGE' : 		
        	if(empty($myPanier->t_panier['ID_PANIER'])) {
        		$myPanier->met_a_jour($_POST);
        		$myPanier->t_panier['PAN_ID_TYPE_COMMANDE']=$_REQUEST['pan_id_type_commande'];
        		$myPanier->getCommande();
        		$idNewCom = $myPanier->t_panier['ID_PANIER'];
        	} else {
				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST);
        	}
			$myPanier->t_panier['PAN_ID_ETAT']=gPanierTraitement;
			$myPanier->etat_panier=GetRefValue("t_etat_pan",$myPanier->t_panier["PAN_ID_ETAT"],$_SESSION['langue']);
			
			$myPanier->t_panier['PAN_DATE_COM']=date("Y-m-d H:i:s");
			$myPanier->getDocs();
			$myPanier->getTarifs();
			$myPanier->save();
			$myPanier->saveAllPanierDoc();			
			//On remet l'ID_PANIER qui peut avoir été mis à  jour à  vide si le panier a été créé ici
			if(!empty($idNewCom)) {
				$myPanier->t_panier['ID_PANIER'] = $idNewCom;
			}

			// pour afficher le nom et le tcin et le tcout dans le contenu de la commande
			$myPanier->getPanierDoc();
			
			$xslFile='commandeRecap';
			require_once(libDir.'class_livraison.php');
			$nomDossierCourt='P'.$myPanier->t_panier['ID_PANIER'].'_'.$user->UserID;
			$livraison=new Livraison();
            if (isset($_POST['repertoire_livraison']) && !empty($_POST['repertoire_livraison']) && defined($_POST['repertoire_livraison'])) {
                $livraison->setDir($nomDossierCourt,constant($_POST['repertoire_livraison']));
            } else
                $livraison->setDir($nomDossierCourt);
			$livraison->createDir();
			$livraison->arrDocs=$myPanier->t_panier_doc;
			
			if (isset($_POST['send_mail']) && !empty($_POST['send_mail']) && $_POST['send_mail']=='1')
				$send_mail_usager=true;
			else
				$send_mail_usager=false;
			
			$params_job_montage = array(
				'ratio_liv'=>(isset($_POST['PANXML_MONTAGE_RATIO'])?$_POST['PANXML_MONTAGE_RATIO']:$_POST['panxml_montage_ratio']),
				'montage_xml'=>(isset($_POST['PANXML_MONTAGE'])?$_POST['PANXML_MONTAGE']:$_POST['panxml_montage']),
				'PANXML_ID_PROC'=>(isset($_POST['PANXML_ID_PROC'])?intval($_POST['PANXML_ID_PROC']):intval($_POST['panxml_id_proc'])),
				'send_mail'=>$send_mail_usager);
				
			// idéalement on devrait avoir une option pour set la source (id_mat) souhaitée pour l'encodage directement dans le xml du montage, mais il n'est pr l'instant pas évident d'updater le xml de montage via commandeSaisie.inc.php. *
			//Pour l'instant on passe donc juste par des parametres PANXML_MONTAGE_MATSRC_{idx_clip}, qu'on ajoute aux params_job_montage
			foreach($myPanier->t_panier['XML']['commande']['PANXML_MONTAGE']['track']['clipitem'] as $idx=>$clip){
				if(isset($myPanier->t_panier['XML']['commande']['PANXML_MONTAGE_MATSRC_'.$idx]) && !empty($myPanier->t_panier['XML']['commande']['PANXML_MONTAGE_MATSRC_'.$idx])){
					$params_job_montage['PANXML_MONTAGE_MATSRC_'.$idx] = $myPanier->t_panier['XML']['commande']['PANXML_MONTAGE_MATSRC_'.$idx];
				}
			}
				
			// trace("commande.php - LIVRAISON_MONTAGE - params_job_montage ".print_r($params_job_montage,true));
				
			if(isset($_POST['livraison_ftp']) && !empty($_POST['livraison_ftp'])){
				$params_job_montage['livraison_ftp'] =$_POST['livraison_ftp'];
				if(isset($myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN']) && !empty($myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'])){
					$params_job_montage['ftp_login'] = $myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'];
				}else{
					$params_job_montage['ftp_login'] = 'tmp_'.kDatabaseName.'_'.$user->UserID.'_'.time();
					$myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'] = $params_job_montage['ftp_login'];
				}
				if(isset($myPanier->t_panier['XML']['commande']['PANXML_FTP_PW']) && !empty($myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'])){
					$params_job_montage['ftp_pw'] = $myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'];
				}else{
					require_once(libDir."fonctionsGeneral.php");
					$params_job_montage['ftp_pw'] = random_alphanum_string(array('fullrandom_length'=>8));
					$myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'] = $params_job_montage['ftp_pw'];
				}
				$myPanier->t_panier['XML']['commande']['PANXML_FTP_HOST'] = kUrlFtp;
				$myPanier->t_panier['PAN_XML'] = array2xml($myPanier->t_panier['XML']['commande'],'commande');
			}

			$id_job_montage=$livraison->prepareLivraison_Montage($params_job_montage,array('MAT_TYPE'=>'MASTER'));
			
			// après la livraison on update et save les valeurs de folder out & id_job (dossier cible de livraison, pour récupération ultérieure des fichiers, download, vérification d'existence)
			$tabxml = xml2array($myPanier->t_panier['PAN_XML']);
			
			$tabxml['commande']["PANXML_FOLDER_OUT"] = $livraison->livraisonDir;
			$tabxml['commande']["COMMANDE_MONTAGE"] = true;
			
			
			$name_file_out = getItem("t_job","JOB_OUT",array("ID_JOB_GEN"=>$id_job_montage),array("ID_JOB"=>"DESC"));
			if(!empty($name_file_out) && is_array($name_file_out) && isset($name_file_out['JOB_OUT'])){
				$tabxml['commande']["PANXML_FILE_OUT"] = $name_file_out["JOB_OUT"];
			}

			
			$tabxml['commande']["PANXML_ID_JOB"] = $id_job_montage;
			
			$myPanier->t_panier['PAN_XML'] = array2xml($tabxml['commande'],'commande');
			unset($tabxml);
			
			$myPanier->save();

			if (isset($_POST['send_mail']) && !empty($_POST['send_mail']) && $_POST['send_mail']=='1')
			{
				if(isset($_POST['noSendMailAdmin']) || isset($_POST['noSendMailUser'])){
					$myPanier->send_mail_usager((isset($_POST['noSendMailAdmin'])?0:1),(isset($_POST['noSendMailUser'])?0:1), '', $mailCommande);
				}else{
					$myPanier->send_mail_usager(1, 1, '', $mailCommande);
				}
			}
        break ; 

		  
        case 'LIVRAISON':
            require_once(libDir.'class_livraison.php');

            $myPanier->getPanier();
            $myPanier->getPanierDoc();
            $myPanier->met_a_jour($_POST);
            $nomDossierCourt=preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime());
            $myLivraison=new Livraison();
            $myLivraison->setDir($nomDossierCourt);
            $myLivraison->arrDocs=$myPanier->t_panier_doc;

            if(!empty($_POST['zipFile'])) $myLivraison->zipFile=$_POST['zipFile'];
            $myLivraison->doLivraison();
            //$myPanier->t_panier_doc=$myLivraison->arrDocs; //pour contrer un bug bizarre qui fait disparaitre la dernière ligne

            echo $myLivraison->error_msg;
            $myPanier->getPanierDoc();
            break;
            
        case 'LIVRAISON_LIGNE':
            require_once(libDir.'class_livraison.php');

            $myPanier->getPanier();
            $myPanier->met_a_jour($_POST);

            foreach ($myPanier->t_panier_doc as $pdoc){
                if($ligne==$pdoc['ID_LIGNE_PANIER']) {
                    $nomDossierCourt=preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()); //nom de dossier défini par microtime
                    $myLivraison=new Livraison();
                    $myLivraison->setDir($nomDossierCourt);
                    $myLivraison->arrDocs[0]=$pdoc;

                    if(!empty($_POST['zipFile'])) $myLivraison->zipFile=$_POST['zipFile'];
                    $myLivraison->doLivraison();
                    $pdoc=$myLivraison->arrDocs[0];
                }
            }
            echo $myLivraison->error_msg;
            $myPanier->getPanierDoc();
            break;
		
		case 'LIVRAISON_DVD':		
			// Validation de la commande avec creation d'un job DVD
        	if(empty($myPanier->t_panier['ID_PANIER'])) {
        		$myPanier->met_a_jour($_POST);
        		$myPanier->t_panier['PAN_ID_TYPE_COMMANDE']=$_REQUEST['pan_id_type_commande'];
        		$myPanier->getCommande();
        		$idNewCom = $myPanier->t_panier['ID_PANIER'];
        	} else {
				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST);
        	}
			
			$myPanier->t_panier['PAN_ID_ETAT']=gPanierTraitement;
			$myPanier->met_a_jour_lignes('PDOC_ID_ETAT',gPanierTraitement);
			$myPanier->t_panier['PAN_DATE_COM']=date("Y-m-d H:i:s");
			$myPanier->getDocs();
			$myPanier->getTarifs();
			
			$myPanier->save();
			$myPanier->saveAllPanierDoc();
			$myPanier->getPanierDoc(); //on récupère les lignes restantes, obligé pour avoir le champ ID_LIGNE_PANIER_ORIG

			$myPanier->met_a_jour($_POST);
			//On remet l'ID_PANIER qui peut avoir été mis à  jour à  vide si le panier a été créé ici
			if(!empty($idNewCom)) {
				$myPanier->t_panier['ID_PANIER'] = $idNewCom;
			}

			foreach ($myPanier->t_panier_doc as $key=>$doc) {
				if(!empty($doc['ID_LIGNE_PANIER_ORIG']))
					$myPanier->deletePanierDoc(array('ID_LIGNE_PANIER'=>$doc['ID_LIGNE_PANIER_ORIG'])); //Retrait de la ligne du panier par deft
			}
			
			// pour afficher le nom et le tcin et le tcout dans le contenu de la commande
			$myPanier->getPanierDoc();
			
			$xslFile='commandeRecap';
			
			require_once(libDir.'class_livraison.php');
			
			$nomDossierCourt='P'.$myPanier->t_panier['ID_PANIER'].'_'.$myPanier->t_panier['PAN_ID_USAGER'];
			$livraison=new Livraison();
			if (isset($_POST['repertoire_livraison']) && !empty($_POST['repertoire_livraison']) && defined($_POST['repertoire_livraison'])) {
				$livraison->setDir($nomDossierCourt,constant($_POST['repertoire_livraison']));
			} else
					$livraison->setDir($nomDossierCourt);
			$livraison->createDir();
			$livraison->arrDocs=$myPanier->t_panier_doc;
			
			if (isset($_POST['send_mail']) && !empty($_POST['send_mail']) && $_POST['send_mail']=='1')
				$send_mail_usager=true;
			else
				$send_mail_usager=false;
			
			$params_job_dvd = array(
				'enveloppe_liv'=>(isset($_POST['PANXML_ENVELOPPE_LIV'])?$_POST['PANXML_ENVELOPPE_LIV']:$_POST['panxml_enveloppe_liv']),
				'standard_liv'=>(isset($_POST['PANXML_STANDARD_LIV'])?$_POST['PANXML_STANDARD_LIV']:$_POST['panxml_standard_liv']),
				'ratio_liv'=>(isset($_POST['PANXML_RATIO_LIV'])?$_POST['PANXML_RATIO_LIV']:$_POST['panxml_ratio_liv']),
				'PANXML_ID_PROC'=>(isset($_POST['PANXML_ID_PROC'])?$_POST['PANXML_ID_PROC']:$_POST['panxml_id_proc']),
				'send_mail'=>$send_mail_usager);
				
			if(isset($_POST['livraison_ftp'])  && !empty($_POST['livraison_ftp'])){
				$params_job_dvd['livraison_ftp'] =$_POST['livraison_ftp'];
				if(isset($myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN']) && !empty($myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'])){
					$params_job_dvd['ftp_login'] = $myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'];
				}else{
					$params_job_dvd['ftp_login'] = 'tmp_'.kDatabaseName.'_'.$user->UserID.'_'.time();
					$myPanier->t_panier['XML']['commande']['PANXML_FTP_LOGIN'] = $params_job_dvd['ftp_login'];
				}
				if(isset($myPanier->t_panier['XML']['commande']['PANXML_FTP_PW']) && !empty($myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'])){
					$params_job_dvd['ftp_pw'] = $myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'];
				}else{
					require_once(libDir."fonctionsGeneral.php");
					$params_job_dvd['ftp_pw'] = random_alphanum_string(array('fullrandom_length'=>8));
					$myPanier->t_panier['XML']['commande']['PANXML_FTP_PW'] = $params_job_dvd['ftp_pw'];
				}
				$myPanier->t_panier['XML']['commande']['PANXML_FTP_HOST'] = kUrlFtp;
				$myPanier->t_panier['PAN_XML'] = array2xml($myPanier->t_panier['XML']['commande'],'commande');
			}

			$id_job_dvd=$livraison->prepareLivraison_DVD($params_job_dvd,array('MAT_TYPE'=>'MASTER'));			
			
			
			// après la livraison on update et save les valeurs de folder out & id_job (dossier cible de livraison, pour récupération ultérieure des fichiers, download, vérification d'existence)
			$tabxml = xml2array($myPanier->t_panier['PAN_XML']);
			
			$tabxml['commande']["PANXML_FOLDER_OUT"] = $livraison->livraisonDir;
			
			$name_file_out = getItem("t_job","JOB_OUT",array("ID_JOB_GEN"=>$id_job_dvd),array("JOB_OUT"=>"DESC"));
			if(!empty($name_file_out) && is_array($name_file_out) && isset($name_file_out['JOB_OUT'])){
				$tabxml['commande']["PANXML_FILE_OUT"] = $name_file_out["JOB_OUT"];
			}
			
			$tabxml['commande']["PANXML_ID_JOB"] = $id_job_dvd;
			
			$myPanier->t_panier['PAN_XML'] = array2xml($tabxml['commande'],'commande');
			unset($tabxml);
			
			$myPanier->save();
			$myPanier->saveAllPanierDoc();
			
			if (isset($_POST['send_mail']) && !empty($_POST['send_mail']) && $_POST['send_mail']=='1')
			{
				if(isset($_POST['noSendMailAdmin']) || isset($_POST['noSendMailUser'])){
					$myPanier->send_mail_usager((isset($_POST['noSendMailAdmin'])?0:1),(isset($_POST['noSendMailUser'])?0:1), '', $mailCommande);
				}else{
					$myPanier->send_mail_usager(1, 1, '', $mailCommande);
				}
			}
			
			break;
	}
}

require_once(modelDir.'model_doc.php');
$myPanier->getProprioPanier();
foreach ($myPanier->t_panier_doc as &$pdoc) {
	$doc = new Doc();
	$doc->t_doc['ID_DOC'] = $pdoc['ID_DOC'];
	$doc->getDoc();
	$doc->getValeurs();
	$doc->getLexique();
	$doc->getPersonnes();
	$pdoc['DOC'] = $doc;
}

include(getSiteFile("formDir","commandeSaisie.inc.php"));
?>
