<?php
require_once(modelDir.'model_categorie.php');
require_once(modelDir.'model_docAcc.php');

global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();


if (!empty($_GET['id_cat']))
	$id_cat=intval($_GET['id_cat']);
else
	$id_cat='';

$my_cat=new Categorie($id_cat);

if (!empty($_POST['commande']))
{
	switch ($_POST['commande'])
	{
		case "SAVE":  // Sauvegarde apr�s modif infos
			if (!empty($_POST['id_cat'])){ // si on enregistre ou on cr��
				$my_cat->t_categorie['ID_CAT'] = $_POST['id_cat'];
				$my_cat->getCategorie();
			}
			
			if (isset($_POST['cat_nom']))
				$my_cat->set('CAT_NOM',$_POST['cat_nom']);
			
			$my_cat->set('ID_LANG',$_SESSION['langue']);
			
			if (isset($_POST['cat_desc']))
				$my_cat->set('CAT_DESC',$_POST['cat_desc']);
			
			if (isset($_POST['cat_intro']))
				$my_cat->set('CAT_INTRO',$_POST['cat_intro']);
			
			if (isset($_POST['cat_code']))
				$my_cat->set('CAT_CODE',$_POST['cat_code']);
			
			if (isset($_POST['cat_date_debut']))
				$my_cat->set('CAT_DATE_DEBUT',$_POST['cat_date_debut']);
			
			if (isset($_POST['cat_date_fin']))
				$my_cat->set('CAT_DATE_FIN',$_POST['cat_date_fin']);
			
			if (isset($_POST['CAT_ID_GEN']))
				$my_cat->set('CAT_ID_GEN',$_POST['cat_id_gen']);
			
			if (isset($_POST['cat_id_type_cat']))
				$my_cat->set('CAT_ID_TYPE_CAT',$_POST['cat_id_type_cat']);
			
			if (isset($_POST['cat_id_doc_image']) && !empty($_POST['cat_id_doc_image']) && (!isset($_POST['CAT_IMAGE']) || empty($_POST['CAT_IMAGE']))){
				if(!empty($my_cat->t_categorie['CAT_ID_DOC_ACC'])){
					require_once(modelDir.'model_docAcc.php');
					$my_da = new DocAcc() ; 
					$my_da->t_doc_acc['ID_DOC_ACC'] = $my_cat->t_categorie['CAT_ID_DOC_ACC'];
					$my_da->delete() ; 
					$my_cat->set('CAT_ID_DOC_ACC',0);
				}
				$my_cat->set('CAT_ID_DOC_IMAGE',$_POST['cat_id_doc_image']);
			}
			if (isset($_POST['CAT_IMAGE']) && $_POST['CAT_IMAGE']=='0')
				$my_cat->set('CAT_IMAGE',$_POST['CAT_IMAGE']);
			
			
			
			
			if (isset($_FILES['cat_image']) && !empty($_FILES['cat_image']) 
			&& !(empty($_FILES['cat_image']['name']) && $_FILES['cat_image']['error']==4)){
				$my_cat->set('CAT_IMAGE',$_FILES['cat_image']);
				$my_cat->set('CAT_ID_DOC_IMAGE',0);
			}
			
			if (isset($_POST['id_doc_lie']))
				$my_cat->addDoc($_POST['id_doc_lie']);

			
			if (isset($_POST['doc_cat_suppr']))
				$my_cat->removeDoc($_POST['doc_cat_suppr']);
				
			 // MS - 17.03.16 - mise en place update sommaire de t_doc_cat ; 
			if (!empty($_POST['t_doc_cat'])){
				foreach ($_POST['t_doc_cat'] as $idx=>$doc_cat) {
					if (isset($doc_cat['ID_DOC']) || isset($doc_cat['ID_DOC'])) {
						$lastID=$idx;
						$_POST['t_doc_cat'][$idx]['ID_DOC']=$doc_cat['ID_DOC'];
					}
					if (isset($doc_cat['CDOC_ORDRE'])) {$_POST['t_doc_cat'][$lastID]['CDOC_ORDRE']=$doc_cat['CDOC_ORDRE'];unset($_POST['t_doc_cat'][$idx]);}
					if (isset($doc_cat['ID_TYPE_CAT'])) {$_POST['t_doc_cat'][$lastID]['ID_TYPE_CAT']=$doc_cat['ID_TYPE_CAT'];unset($_POST['t_doc_cat'][$idx]);}
					if (isset($doc_cat['CDOC_ID_IMAGE'])) {$_POST['t_doc_cat'][$lastID]['CDOC_ID_IMAGE']=$doc_cat['CDOC_ID_IMAGE'];unset($_POST['t_doc_cat'][$idx]);}
				}
			}

		
			// MS - 27.04.16 - afin d'�viter d'avoir � red�velopper un nouveau cas "multi", j'ai ajouter l'array arrFather aux objets cat�gories (cf docs, personnes)
			// cependant comme dans ce fichier (catSaisie.php) on utilise pas updateFromArray, on doit r�percuter la gestion des input post arrFather ici.
			// ce n'est pas terrible, � revoir eventuellement (passer toutes les modifs du cas save via la fonction Categorie::updateFromArray serait un bon d�but ...)
			if ($_POST['arrFather']){
				$my_cat->t_categorie['CAT_ID_GEN']=$_POST['arrFather'][0]['ID_CAT'];
			}else{
				$my_cat->arrFather = array() ; 
			}
					
			$my_cat->save();
            $my_cat->saveVersions($_POST['version']);

			//VP 30/6/18 : d�placement appel updateDocChampsXML depuis m�thode save
			$my_cat->updateDocChampsXML(true);
			
			$my_cat->getCategorie();//on reprends les infos de la base
			$my_cat->getVignette();
			if(! empty($_POST['t_doc_cat'])){
				$my_cat->updateDocCats($_POST['t_doc_cat']);
			}
			$my_cat->saveAllDocCat() ; 
			
			// g�n�ration des images HD si ID_CAT est dans kSaveVignetteAsHDForCatIds
			if(defined("kSaveVignetteAsHDForCatIds")){
				$arr_catHD_ids = explode(',',(string)(kSaveVignetteAsHDForCatIds));
				foreach($arr_catHD_ids as $catHD_id){
					if($catHD_id == $my_cat->t_categorie['ID_CAT']){
						$docs_a_traiter =array() ; 
						// MS - dans le cas de catSaisie, on n'a besoin d'updater les images HD que pour les docs qui n'en ont pas => pas de notion de modification de la vignette doc depuis catSaisie
						foreach($my_cat->t_doc_cat as $doccat){
							if(empty($doccat['CDOC_ID_IMAGE'])){
								$docs_a_traiter['ID_DOC'][]  = $doccat['ID_DOC'];
							}
						}
						// trace(print_r($docs_a_traiter,true));
						if(!empty($docs_a_traiter)){
							require_once(modelDir.'model_imageur.php');
							Imageur::refreshImagesCatHD($my_cat->t_categorie['ID_CAT'],$docs_a_traiter);
						}
					}
				}
			}
			
			break;

		case "DUP" : 
			$newCat = $my_cat->duplicateCat() ; 
			$myPage->redirect($myPage->getName()."?urlaction=catSaisie&id_cat=".$newCat->t_categorie['ID_CAT']."&id_lang=".$newCat->t_categorie['ID_LANG']."&duplicated=1");
			break ; 
			
		case "SUP": // Supression
			if (!empty($_POST['id_cat'])) // si on enregistre ou on cr��
				$my_cat = new Categorie($_POST['id_cat']);			
				
            $my_cat->delete();
			
            if ($myPage->hasReferrer())
                $myPage->redirect($myPage->getReferrer());
			break;
	}
}
$myPage->nomEntite=kCategorie;
$myPage->titre=str_replace(chr(34),'',(isset($my_cat->t_categorie['CAT_NOM']) && !empty($my_cat->t_categorie['CAT_NOM'])?$my_cat->t_categorie['CAT_NOM']:''));
$myPage->setReferrer(true,'',true);

// R�cup�ration des versions
$my_cat->getVersions();


echo "<div class='error'>".$my_cat->error_msg."</div>";

include(getSiteFile("formDir","catSaisie.inc.php"));


?>
