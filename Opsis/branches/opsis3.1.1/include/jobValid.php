<?php
	require_once(modelDir.'model_job.php');
	require_once(modelDir.'model_materiel.php');

	$myPage=Page::getInstance();
	$myUser=User::getInstance();

	$myPage->titre=kValidationEtape;

    $myJob= new Job();
	if(isset($_REQUEST['id_job'])){
	   $myJob->t_job['ID_JOB']=$_REQUEST['id_job'];
	   $myJob->getJob();
		// JOB_IN
		if($myJob->t_job['JOB_IN']!=''){
			if (isset($myJob->t_job['JOB_ID_MAT']) && !empty($myJob->t_job['JOB_ID_MAT']))
			{
				$matInObj=new Materiel();
				$matInObj->t_mat['ID_MAT']=$myJob->t_job['JOB_ID_MAT'];
				$matInObj->getMat();
				$matInObj->getDocs();
			}
			else
			{
				$matInObj=Materiel::getMatByMatNom($myJob->t_job['JOB_IN']);
				$matInObj->getDocs();
			}
			
		}
		// JOB_OUT
		if($myJob->t_job['JOB_OUT']!=''){//VG 10/09/10 : correction : $this->t_job['JOB_OUT'] => $myJob->t_job['JOB_OUT']
			/*$matOutObj=new Materiel();
			$matOutObj->t_mat['ID_MAT']=$myJob->t_job['JOB_OUT'];
			$matOutObj->getMat();
			$matOutObj->getDocs();*/
			$matInObj=Materiel::getMatByMatNom($myJob->t_job['JOB_IN']);
		}

	}
	if($_POST){
		// VP 2/12/09 : mise à jour JOB_DATE_FIN
		switch ($_POST['commande']) {
			case "VALIDE":
				$myJob->t_job['JOB_ID_ETAT']=jobFini;
				$myJob->t_job['JOB_DATE_FIN']=date("Y-m-d H:i:s");
				$myJob->save();
				// VP 7/05/10 : ajout paramètre bypass_jobs pour supprimer des jobs
				if(!empty($_POST['bypass_jobs'])){
					$arrId=explode(",",$_POST['bypass_jobs']);
					$dumb=new Job;
					$arrJob=$dumb->getJob($arrId);
					foreach ($arrJob as $idx=>$jobObj) {$jobObj->delete();}
					unset($arrJob);
					unset($arrId);
					unset($dumb);
				}
				break;

			case "REFUSE":
				$myJob->t_job['JOB_ID_ETAT']=jobErreur;
				$myJob->t_job['JOB_DATE_FIN']=date("Y-m-d H:i:s");
				$myJob->t_job['JOB_MESSAGE']=kErreurJobNonValide;
				$myJob->save();
				break;
		}
	}
	switch($myJob->t_job['JOB_ID_ETAT']){
		case jobFini:
			printf(kMsgValidationJob, $myJob->t_job['JOB_NOM']);
			break;
		case jobErreur:
			printf(kMsgValidationJobRefusee, $myJob->t_job['JOB_NOM']);
			break;
		default:
			// Affichage du dialogue de validation
			include(getSiteFile("formDir","jobValid.inc.php"));
			break;
	}


?>
