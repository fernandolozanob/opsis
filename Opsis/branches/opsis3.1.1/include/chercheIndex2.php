<?
	global $db;
  debug($_REQUEST);
	$myFrame=Page::getInstance();
	$myFrame->getSort();


    //by ld 03/10/08 => pas de setReferrer si on appelle cette palette depuis une palette
    //c'est le cas en saisie personne ou lexique en palette qui peut appeler une autre palette (sel de valeur,...)
    //on ne fait pas le setReferrer car sinon on va revenir sur la liste des valeurs et non sur la liste d'origine (pers, lex, doc)
    //by ld 10/12 => test changé de index=true a palette=false car on peut appeler ceci depuis une page que index
    // (par ex: indexPopup pour gaumont)
	// MS - 12.04.16 - Si le referer est un formulaire de saisie dans une palette (lexSaisie ou valSaisie, à alimenter au besoin)
	// alors on est dans "une palette dans une palette"(docSaisie => open palette => nouveau/modifier => open palette sur le formulaire)
	// Dans ce cas on empeche les appels au setReferer car setReferer ne peut pas faire la distinction entre les diverses origines d'appels des palettes, 
	// ce qui causait des mauvaises redirections (la palette de 'premier niveau' retournait vers l'url de la palette imbriquée)
	if (strpos($_SERVER['HTTP_REFERER'],'palette.php')===false){
		$myFrame->setReferrer(true);
	}elseif (strpos($_SERVER['HTTP_REFERER'],'palette.php?urlaction=lexSaisie') === false && strpos($_SERVER['HTTP_REFERER'],'palette.php?urlaction=valSaisie') === false ){
		$myFrame->setReferrer();
	}
	$myUsr=User::getInstance();

	$titre_index =""; // Titre de la page

	$id_champ_appelant=""; // Type de recherche (L, V_DIF, etc...)
	$id_input_result=""; //Champ dans lequel on affiche la réponse (retour de clic)

	$champ=""; // Type de recherche ? OBSOLETE
	$valeur=""; //Valeur recherchée

	$id_type_desc=""; // pas passé en param, extrait de id_champ_appelant
	$dlex_id_role=""; // semble pas utilisé

	$searchParamsForUrl = array(); //@update VG 29/06/2010 : initialisation  du tableau de paramètre à passer par l'url
  //$affich_nb="1";
  $lex_aff=0;
  $type_lex="";

	if(isset($_REQUEST['titre_index'])) $titre_index = urldecode(trim($_REQUEST['titre_index']));
	if(isset($_REQUEST['id_input_result'])) $id_input_result = urldecode(trim($_REQUEST['id_input_result']));
	if(isset($_REQUEST['champ'])) $id_champ_appelant = trim($_REQUEST['champ']);
	if(isset($_REQUEST['autreChamps'])) $autreChamps = $_REQUEST['autreChamps'];
	if(isset($_REQUEST['type_priv'])) $type_priv = $_REQUEST['type_priv'];
	if(isset($_REQUEST['applydroit'])) $apply_droit = $_REQUEST['applydroit'];
	//B.RAVI 2016-10-06 besoin sur scnf modifLot, de pouvoir filtrer dans la palette avec id_type_doc_min, on ne peut pas utiliser &valeur (car la valeur du champ 'valeur' peut etre réécraser lors d'1 recherche dans la palette)
	if(isset($_REQUEST['index_filter'])) $index_filter = $_REQUEST['index_filter'];
	if(isset($_POST['valeur'])) $_GET['valeur']=$_POST['valeur'];
	$valeur = trim($_GET['valeur']);

	if(isset($_REQUEST["id_lang"])) $id_lang = strtoupper($_REQUEST["id_lang"]);
  else $id_lang=$_SESSION['langue']; //Langue de recherche, mis par déft avec la session par LD le 14/09/07

	if (isset($_REQUEST["affich_nb"])) $affich_nb=$_REQUEST["affich_nb"];

   
    
    if (trim($_REQUEST["rtn"])!='') $jsFunction=$_REQUEST['rtn']; else $jsFunction='addValue';
    

// VP 28/06/11 : ajout paramètre tri pour les arbres hiérarchiques
	if (isset($_REQUEST["tri"])) $tri=$_REQUEST['tri'];

  // ID courante envoyée par la page : cette ID sert surtout à être filtrée de la recherche.
  // Ex : on ne veut lier un doc à lui-même ou un lex à lui-même.
  if (isset($_REQUEST["currentId"])) $currentId=$_REQUEST["currentId"];

  if(isset($_POST['id_asso'])) $id_asso=intval($_POST['id_asso']);

  // VP 10/3/10 : Correction bug si xsl vide
  if (isset($_REQUEST['xsl'])) $xsl=$_REQUEST['xsl'];
  else $xsl='paletteSimple';

  $blnPrivilegeDoc=false; // Pas de filtrage sur l'affichage des documents

	if(isset($apply_droit)){
		$applyDroit = $apply_droit;
	}else{
	  $applyDroit = true;	//Application des droits
	  if (strpos($_SERVER['HTTP_REFERER'], "urlaction=docSaisie") > 0 ) $applyDroit = false;
	}

	// I.Affichage des Index
	if($id_champ_appelant!=""){
		
		   // I.1. Gestion des droits (cr�ation du morceau de requete SQL de filtre)
			/*
			$sql_droits="";
			$liste_id="";
			foreach($_SESSION["USER"]["US_GROUPES"] as $value){
				if ($value["ID_PRIV"]!=0){ $liste_id.="'".$value["ID_FONDS"]."', "; }
			}
			$liste_id = substr($liste_id,0,-2);
			if(strlen($liste_id)>0) {
				$sql_droits= " and t_doc.DOC_ID_FONDS in (".$liste_id.")";
			}*/

			// I.2. Choix de l'index � afficher
			$desc_champs_appelant = explode("_",$id_champ_appelant);
			switch ($desc_champs_appelant[0]){

		case "C" : //Recherche de documents avec champ DOC (doc_titre,doc_titre_col, etc) et valeur
		case "FT" :
		case "FTE" :
		case "DOC" :
			include(libDir."class_chercheDoc.php");
			$champ=substr($id_champ_appelant,(strlen($desc_champs_appelant[0])+1));  //@update VG 05/08/2010 : on calcule d'anord le champ avant toute modification de $desc_champs_appelant
			if (empty($desc_champs_appelant[1])) $champ="DOC_TITRE"; //appel sans champ (ex: DOC parent)
					$mySearch=new RechercheDoc();
			$mySearch->useSession=false; //pas de mise en session
			$mySearch->setPrefix(null);
			$mySearch->prepareSQL();
			// VP 13/06/2012 : optimisation requête ave cgroup by que pour $affich_nb
			if ($affich_nb) {
				//PC 01/07/14 : Remplacement de ID_DOC par 0 pour éviter les doublons
				if (!empty($autreChamps))
				{
					$mySearch->sql="SELECT $champ as VALEUR, $autreChamps as VALEUR2, 0 as ID_VAL, count(ID_DOC) as NB from t_doc";
					$mySearch->sqlSuffixe=" order by VALEUR,VALEUR2,ID_VAL";
				}
				else
				{
					$mySearch->sql="SELECT $champ as VALEUR, 0 as ID_VAL, count(ID_DOC) as NB from t_doc";
					$mySearch->sqlSuffixe=" order by VALEUR,ID_VAL";
				}
					
			}elseif ($desc_champs_appelant[0]=="DOC"){
				if (!empty($autreChamps))
				{
					$mySearch->sql="SELECT $champ as VALEUR, $autreChamps as VALEUR2, ID_DOC as ID_VAL from t_doc";
					$mySearch->sqlSuffixe=" order by VALEUR,VALEUR2";
				}
				else
				{				
					$mySearch->sql="SELECT $champ as VALEUR, ID_DOC as ID_VAL from t_doc";
					$mySearch->sqlSuffixe=" order by VALEUR";
				}
			}else{
				if (!empty($autreChamps))
				{
					$mySearch->sql="SELECT distinct $champ as VALEUR, $autreChamps as VALEUR2, ID_DOC as ID_VAL from t_doc";
					$mySearch->sqlSuffixe=" order by VALEUR,VALEUR2";
				}
				else
				{				
					$mySearch->sql="SELECT distinct $champ as VALEUR, ID_DOC as ID_VAL from t_doc";
					$mySearch->sqlSuffixe=" order by VALEUR";
				}
			}
   
                        
                        
			$mySearch->sqlWhere.=" and $champ is not null";
                        
                        //B.RAVI 2016-01-04 si $champ = numérique en BDD (smallint,bigint..etc),  le morçeau de code    $champ != ' ' and $champ != ''     provoque postgres7 error: [-1: ERREUR:  syntaxe en entree invalide pour l'entier (pas d'erreur sur MYSQL)
                        $rs_type_bdd_field = $db->execute("select $champ from t_doc");
                        $type_bdd_field=$rs_type_bdd_field->FetchField();
//                      var_dump('debug_affich '.$db->MetaType($type_bdd_field->type));
                            
                        if (!in_array($db->MetaType($type_bdd_field->type),array('N','I'))){
			$mySearch->sqlWhere.="  and $champ != ' ' and $champ != ''";
                        }
                        
                        

			if ($desc_champs_appelant[0]=="DOC") $desc_champs_appelant[0]="FT"; //rétrocompatibilité

			$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
			
			
			$mySearch->tab_recherche[]=array('FIELD'=>'ID_LANG',
					  'VALEUR'=>$_SESSION['langue'],
					  'TYPE'=>'AND',
					'OP'=>'AND');

			if (!empty($valeur)) {
			$mySearch->tab_recherche[]=
				  array('FIELD'=>$champ,
					  'VALEUR'=>$valeur.($desc_champs_appelant[0]=='C'?'%':''),
					  'TYPE'=>$desc_champs_appelant[0],
					'OP'=>'AND');
			}
			if (!empty($_GET['type_doc'])) {

		  	$mySearch->tab_recherche[]=
				  array('FIELD'=>'DOC_ID_TYPE_DOC',
					  'VALEUR'=>$_GET['type_doc'],
					  'TYPE'=>'C',
					'OP'=>'AND');
			}
		
			if (!empty($_GET['id_media'])) {
				$mySearch->tab_recherche[]=
					array('FIELD'=>'DOC_ID_MEDIA',
						  'VALEUR'=>$_GET['id_media'],
						  'TYPE'=>'C',
						'OP'=>'AND');
			}
		
			if ($affich_nb)
				$mySearch->sqlSuffixe=" GROUP BY VALEUR,ID_VAL".$mySearch->sqlSuffixe;
			//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
			if (((defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits)) 
			||(!defined('gSeuilAppliqueDroits') && $myUsr->Type < kLoggedDoc ))&& $applyDroit){
				$mySearch->appliqueDroits();
			}
			$mySearch->makeSQL();
			$mySearch->finaliseRequete();
			$sql=$mySearch->sql;

			break;
 
		case "MDOC" :  //Recherche de documents avec champ DOC et d'autres champs
			//if ($desc_champs_appelant[0]=="MDOC") $desc_champs_appelant[0]="C"; //rétrocompatibilité
			
			include(libDir."class_chercheDoc.php");
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			if (empty($desc_champs_appelant[1])) $champ="DOC_TITRE"; //appel sans champ (ex: DOC parent)
			$mySearch=new RechercheDoc();
			$mySearch->useSession=false; //pas de mise en session
			
			$mySearch->setPrefix(null);
			$mySearch->prepareSQL();
			
			// VP 13/06/2012 : optimisation requête (suppression count et group by)
			if (!empty($autreChamps)) {
				//$mySearch->sql="SELECT $champ as VALEUR, $autreChamps as VALEUR2, ID_DOC as ID_VAL, count(ID_DOC) as NB from t_doc";
				$mySearch->sql="SELECT distinct $champ as VALEUR, $autreChamps as VALEUR2, ID_DOC as ID_VAL from t_doc";
			}else{
				$mySearch->sql="SELECT distinct $champ as VALEUR, ID_DOC as ID_VAL from t_doc";
			}
			//$mySearch->sqlSuffixe=" group by $champ";
			$mySearch->sqlSuffixe=" order by $champ";
			
			
			$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
			
			if (!empty($valeur)) {
				$mySearch->tab_recherche[]=
				array('FIELD'=>$champ,
					  'VALEUR'=>$valeur.'%',
					  'TYPE'=>'C',
					  'OP'=>'AND');
			}
			if (!empty($_POST['autreValeur'])) {
				$mySearch->tab_recherche[]=
				array('FIELD'=>$autreChamps,
					  'VALEUR'=>'%'.$_POST['autreValeur'].'%',
					  'TYPE'=>'C',
					  'OP'=>'AND');
			}
			//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
			if (((defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits) ) 
			||(!defined('gSeuilAppliqueDroits') && $myUsr->Type<kLoggedDoc )) && $applyDroit){
				$mySearch->appliqueDroits();
			}
			$mySearch->makeSQL();
			$mySearch->finaliseRequete();
			$sql=$mySearch->sql;
			
			$param['autre_libelle'] = $_GET['autre_libelle'];
			$param['libelle'] = $_GET['libelle'];
			
			break;
			
		case "EXP" :
			
			include(libDir."class_chercheExp.php");
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			if (empty($desc_champs_appelant[1])) $champ="EXP_ARTICLE";
			$mySearch = new RechercheExp();
			$mySearch->useSession = false;
			
			$mySearch->prepareSQL();
			
			$champ1 = $_GET['champ1'];
			$champ2 = $_GET['champ2'];
			$champ3 = $_GET['champ3'];
			
			// VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et distinct / order by sinon
			$mySearch->sql = "SELECT distinct $champ as VALEUR, t_exploitation.ID_EXP as ID_VAL ";
			if (isset($_GET['champ1']))
				$mySearch->sql .= ", $champ1 as VALEUR1 ";
			if (isset($_GET['champ2']))
				$mySearch->sql .= ", $champ2 as VALEUR2 ";
			if (isset($_GET['champ3']))
				$mySearch->sql .= ", $champ3 as VALEUR3 ";
			$mySearch->sql .= "FROM t_exploitation
			LEFT OUTER JOIN t_type_exp on t_type_exp.id_type_exp = t_exploitation.EXP_ID_TYPE_EXP
			WHERE $champ ".$db->like('%'.$valeur.'%');
			
			//$mySearch->sqlSuffixe = " group by $champ";
			$mySearch->sqlSuffixe = " order by $champ";
			
			
			$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
			
			$sql=$mySearch->sql;
			
			break;

		case "BOOL" :
			
			include(libDir."class_chercheDoc.php");

			$mySearch = new RechercheDoc();
			$mySearch->useSession = false;
			
			$mySearch->prepareSQL();
			

			
			// VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et distinct / order by sinon
			$mySearch->sql = "SELECT DISTINCT * FROM (VALUES(1,1),(0,0)) AS tbl (VALEUR, ID_VAL)";

			$sql=$mySearch->sql;
			
			break;
					/*

				// I.2.b Index issu de T_DOC et d'une table li�e :
				case "CI":
					$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);

					$sql ="SELECT t_".strtolower($champ).".ID_$champ as ID, t_".strtolower($champ).".$champ as VALEUR, count(distinct t_doc.ID_DOC) as NB from t_doc, t_".strtolower($champ);
					$sql.=" WHERE t_doc.DOC_ID_".$champ."=t_".strtolower($champ).".ID_$champ";
					$sql.=" AND t_doc.ID_LANG='".simpleQuote($id_lang)."'";
					if(strtolower($champ)!="fonds"){
						$sql.=" AND t_".strtolower($champ).".ID_LANG='".simpleQuote($id_lang)."'";
					}
					if($valeur!="") $sql.=" AND t_".strtolower($champ).".$champ LIKE '".simpleQuote($valeur)."%'";
					$sql.=" ".$sql_droits." GROUP BY $champ";
				  break;
*/
		// I.2.c Index issu de T_DOC et d'une table li�e :
		case "CT":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);

			$table="t_".strtolower($champ);
			$champ_id="ID_".$champ;
			$champ_doc="DOC_ID_".$champ;
			if($champ=="ETAT_DOC") $champ_doc="DOC_ID_ETAT_DOC";
			else if(($champ=="USAGER_CREA")||($champ=="USAGER_MODIF")) {
			$champ="US_NOM";
			$champ_id="ID_USAGER";
			$table="t_usager";
			}
			//PC 16/02/11 : ajout de l'ID_VAL
			// VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et order by sinon
			if ($affich_nb) {
				$sql ="SELECT $table.$champ as VALEUR, $table.$champ_id as ID_VAL, count(distinct t_doc.ID_DOC) as NB from ".$table;
			}else{
				$sql ="SELECT distinct $table.$champ as VALEUR, $table.$champ_id as ID_VAL from ".$table;
			}
			
			$sql.=" LEFT OUTER JOIN t_doc ON t_doc.".$champ_doc." = ".$table.".".$champ_id;
			if(($table!="t_usager")&&($table!="t_media")){
				$sql.=" AND $table.ID_LANG=t_doc.ID_LANG ";
				$sql.=" WHERE $table.ID_LANG=".$db->Quote($id_lang);
			}else{
				$sql.=" WHERE t_doc.ID_LANG=".$db->Quote($id_lang);
			}
			
			//B.RAVI 2016-10-06 besoin sur scnf modifLot, de pouvoir filtrer dans la palette avec id_type_doc_min
			if( preg_match('/^id_min([0-9]{1,2})$/', $index_filter,$matches)
				&& 	$table='t_type_doc'
					) {
				$sql.=" AND $table.ID_$champ >".$db->Quote($matches[1]);
			}
			
			if($valeur!="") {
				$sql.=" AND $table.$champ ".$db->like($valeur.'%');
			}
			$sql.=" ".$sql_droits;
			if ($affich_nb) {
				$sql.=" GROUP BY VALEUR,ID_VAL";
			}else{
				$sql.=" ORDER BY VALEUR";
			}
			break;


				// I.2.d Index issu de T_DOC_LEX :
				case "LF": // Fulltext
				case "LFC": //Fulltext avec extension %, by LD 04/06/08
				case "LFCS": //Fulltext avec extension % et gestion synonymes, by PC 18/12/12
				case "LFCP": //Fulltext avec extension % et gestion terme préférentiel
				case "L": // Commence par la saisie VAL%
				case "LC": //Saisie dans la chaine %VAL%
				  //SYNTAXE DU CHAMP APPELANT
				  //TYPE_TYPEDESC_TYPELEX_MODE_ROLE  ex: L_GEN_PP_ASSO_AUT,
				  //les champs peuvent être omis mais il faut conserver les "_" , ex: L_GEN___AUT
					$chooseDisplayMode=true;
					$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
					$id_type_desc=$desc_champs_appelant[1];
					//@update VG 25/08/2010 : ajout de la possibilité du passage de lex_aff dans le nom du champ
					if (isset($_REQUEST["lex_aff"])) $lex_aff=$_REQUEST["lex_aff"];
					else if(!empty($desc_champs_appelant[5])) $lex_aff = $desc_champs_appelant[5];
					if (isset($_REQUEST["type_lex"])) $type_lex=$_REQUEST["type_lex"];
					if(!empty($desc_champs_appelant[2])) $type_lex=$desc_champs_appelant[2];
					$dlex_id_role=$desc_champs_appelant[4]; //réhabilité le 6/11/07 mais placé en 4ème position
					$mode=$desc_champs_appelant[3];
					if (isset($_REQUEST["mode"])) $mode=$_REQUEST["mode"];
					// VP 13/04/10 : ajout paramètre dspAsso
					if (isset($_REQUEST["dspAsso"])) $dspAsso=$_REQUEST["dspAsso"];

					if ($desc_champs_appelant[0]=='LF' || $desc_champs_appelant[0]=='LFC' || $desc_champs_appelant[0]=='LFCS' || $desc_champs_appelant[0]=='LFCP') {
						$typeRech='FT';
						if ($desc_champs_appelant[0]=='LFC' || $desc_champs_appelant[0]=='LFCS' || $desc_champs_appelant[0]=='LFCP') {
						  $valeur=preg_replace("/[%\*]$/","",$valeur);
						  $extension="*";
						  }
					} else {
						$typeRech='C';
						$valeur=preg_replace("/[%\*]$/","",$valeur); //suppression du dernier char si % ou *
						$extension="%";
					}

					include(libDir."class_chercheLex.php");
					$mySearch=new RechercheLex();
					$mySearch->useSession=false; //pas de mise en session
					$mySearch->prepareSQL();
					$mySearch->setPrefix(null);
					$mySearch->sqlSuffixe="";
					 $mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);

			//PC 12/01/2011 : Ajout des droits pour le lexique
			//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
			if (((defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits) ) 
			||(!defined('gSeuilAppliqueDroits') && $myUsr->Type<kLoggedDoc )) && $applyDroit){
            	require_once(libDir."class_chercheDoc.php");
			    $mySearchDoc=new RechercheDoc();
			    $mySearchDoc->appliqueDroits();
			    $sql_droit =  $mySearchDoc->sqlRecherche;
			    unset($mySearchDoc);
            }
          
            // VP 8/07/09 : ajout critère ID_LEX!=0 pour optimisation
            // VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et order by sinon
            // PC 05/02/2013 : ",t_doc_lex.ID_TYPE_DESC, t_doc_lex.DLEX_ID_ROLE" retiré des requêtes SQL pour éviter les doublons
            // VP 14/02/13 : suppression rapatriement t_doc_lex.ID_TYPE_DESC, t_doc_lex.DLEX_ID_ROLE
            
                    if($affich_nb){
						if ($desc_champs_appelant[0] == "LFCS") {
							$mySearch->sql = "select distinct t_lexique.ID_LEX as ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_COOR, t_lexique.LEX_TERME as VALEUR, syn.LEX_TERME as SYNO, syn.ID_LEX as ID_PREF,  count(distinct t_doc_lex.ID_DOC) as NB
								from t_lexique
                                left join t_doc_lex ON (t_doc_lex.ID_LEX=t_lexique.ID_LEX)
								left join t_lexique syn ON syn.ID_LEX = t_lexique.LEX_ID_SYN
								";
							 $mySearch->sqlSuffixe = "  GROUP BY ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN,t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_TERME, t_lexique.LEX_COOR, SYNO, ID_PREF ORDER BY VALEUR";
						} elseif ($desc_champs_appelant[0] == "LFCP") {
							$mySearch->sql = "select distinct t_lexique.ID_LEX as ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_COOR, t_lexique.LEX_TERME as VALEUR, count(distinct t_doc_lex.ID_DOC) as NB
								from t_lexique
                                left join t_doc_lex ON (t_doc_lex.ID_LEX=t_lexique.ID_LEX)
								left join t_lexique syn ON syn.LEX_ID_SYN = t_lexique.ID_LEX
								";
							 $mySearch->sqlSuffixe = "  GROUP BY ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN,t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_TERME, t_lexique.LEX_COOR ORDER BY VALEUR";
						} else {
							$mySearch->sql = "select t_lexique.ID_LEX as ID_VAL, LEX_ID_SYN, LEX_ID_GEN,t_lexique.LEX_COOR,
							LEX_ID_TYPE_LEX, LEX_ID_ETAT_LEX, LEX_TERME as VALEUR,  count(distinct t_doc_lex.ID_DOC) as NB
							from t_lexique left join t_doc_lex ON (t_doc_lex.ID_LEX=t_lexique.ID_LEX and t_doc_lex.ID_LEX!=0)
							";
							$mySearch->sqlSuffixe = "  GROUP BY ID_VAL, t_lexique.LEX_COOR,LEX_ID_SYN, LEX_ID_GEN,LEX_ID_TYPE_LEX, LEX_ID_ETAT_LEX, LEX_TERME ORDER BY VALEUR";
						}
                    }else{
						if($id_type_desc || $dlex_id_role || !empty($sql_droit)){
							if ($desc_champs_appelant[0] == "LFCS"){
								$mySearch->sql = "select distinct t_lexique.ID_LEX as ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN,t_lexique.LEX_COOR, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_COOR, t_lexique.LEX_TERME as VALEUR, syn.LEX_TERME as SYNO
									from t_lexique 
									left join t_lexique syn ON syn.ID_LEX = t_lexique.LEX_ID_SYN and syn.ID_LANG=t_lexique.ID_LANG
									";
							}elseif ($desc_champs_appelant[0] == "LFCP") {
								$mySearch->sql = "select distinct t_lexique.ID_LEX as ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN,t_lexique.LEX_COOR, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_TERME as VALEUR
									from t_lexique 
									left join t_lexique syn ON syn.LEX_ID_SYN = t_lexique.ID_LEX and syn.ID_LANG=t_lexique.ID_LANG
									";
								$mySearch->sqlSuffixe = "  GROUP BY ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN,t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_TERME, t_lexique.LEX_COOR ORDER BY VALEUR";
							} else{
								$mySearch->sql = "select distinct t_lexique.ID_LEX as ID_VAL, t_lexique.LEX_COOR,LEX_ID_SYN, LEX_ID_GEN, LEX_ID_TYPE_LEX, LEX_ID_ETAT_LEX, LEX_TERME as VALEUR
									from t_lexique";
							}
							$mySearch->sql .=" left join t_doc_lex ON (t_doc_lex.ID_LEX=t_lexique.ID_LEX) ";
						} else {
							if ($desc_champs_appelant[0] == "LFCS"){
								$mySearch->sql = "select t_lexique.ID_LEX as ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_COOR, t_lexique.LEX_TERME as VALEUR, syn.LEX_TERME as SYNO
								from t_lexique
								left join t_lexique syn ON syn.ID_LEX = t_lexique.LEX_ID_SYN and syn.ID_LANG=t_lexique.ID_LANG
								";
							}elseif ($desc_champs_appelant[0] == "LFCP"){
								$mySearch->sql = "select t_lexique.ID_LEX as ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_COOR, t_lexique.LEX_TERME as VALEUR
								from t_lexique
								left join t_lexique syn ON syn.LEX_ID_SYN = t_lexique.ID_LEX and syn.ID_LANG=t_lexique.ID_LANG
								";
								$mySearch->sqlSuffixe = "  GROUP BY ID_VAL, t_lexique.LEX_ID_SYN, t_lexique.LEX_ID_GEN,t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_ID_ETAT_LEX, t_lexique.LEX_TERME, t_lexique.LEX_COOR ORDER BY VALEUR";
							}else{
								$mySearch->sql = "select t_lexique.ID_LEX as ID_VAL, LEX_ID_SYN, LEX_ID_GEN, LEX_ID_TYPE_LEX, LEX_ID_ETAT_LEX, LEX_COOR, LEX_TERME as VALEUR
								from t_lexique";
							}

						}
							
						$mySearch->sqlSuffixe = "  ORDER BY VALEUR";
                    }
//                    $mySearch->sql .=" left join t_doc t1 ON (t1.ID_DOC=t_doc_lex.ID_DOC";
//                    if(!empty($sql_droit) && (!isset($type_priv) || $type_priv == 0)) $mySearch->sql .=" $sql_droit ";
//                    $mySearch->sql .=") where t_lexique.ID_LANG=".$db->Quote($id_lang);
                    if(!empty($sql_droit) && (!isset($type_priv) || $type_priv == 0) || $_REQUEST['limTypeDoc'])
                    {
                        $mySearch->sql .=" left join t_doc t1 ON (t1.ID_DOC=t_doc_lex.ID_DOC ".$sql_droit." )";  
                    }
                    $mySearch->sql .=" where t_lexique.ID_LANG=".$db->Quote($id_lang);
                    if ($_REQUEST['limTypeDoc']){
                        $tab_limTypeDoc=array_map('intval', explode(',',$_REQUEST['limTypeDoc']));//intval pour éviter injection sql
                        $mySearch->sql .=" AND t1.doc_id_type_doc in (".implode(',',$tab_limTypeDoc).") ";
                    }
                  
                  if (!empty($valeur)) $mySearch->tab_recherche[]=
                                array('FIELD'=>'t_lexique.LEX_TERME',
                              'VALEUR'=>($desc_champs_appelant[0]=='LC'?'%':'').$valeur.$extension,
                              'TYPE'=>$typeRech,
                            'OP'=>'AND');

			if ($type_lex) $mySearch->tab_recherche[]=
						array('FIELD'=>'t_lexique.LEX_ID_TYPE_LEX',
							  'VALEUR'=>$type_lex,
							  'TYPE'=>'CE',
							'OP'=>'AND');

			if ($id_type_desc) $mySearch->tab_recherche[]=
						array('FIELD'=>'ID_TYPE_DESC',
							'VALEUR'=>$id_type_desc,
							'TYPE'=>'CE',
							'OP'=>'AND');
			if ($dlex_id_role) $mySearch->tab_recherche[]=
						array('FIELD'=>'DLEX_ID_ROLE',
							'VALEUR'=>$dlex_id_role,
							'TYPE'=>'CE',
							'OP'=>'AND');
			// VP 9/01/18 : ajout paramètre id_fonds
			if ($_REQUEST['id_fonds']) {
				$mySearch->tab_recherche[]=
				array('FIELD'=>'VAL_ID_FONDS',
					  'VALEUR'=>$_REQUEST['id_fonds'],
					  'TYPE'=>'CE',
					  'OP'=>'AND');
			}
					
		  switch ($mode) { // Ajout de critères en plus (recherche de parents possibles ou syno possibles)
			case "SYNO":
			  $mySearch->tab_recherche[]=
				array('FIELD'=>'t_lexique.LEX_ID_SYN',
					'VALEUR'=>0,
					'TYPE'=>'CE',
					'OP'=>'AND');
			  /*$mySearch->tab_recherche[]=
				array('FIELD'=>'t_lexique.LEX_ID_GEN',
					'VALEUR'=>0,
					'TYPE'=>'C',
					'OP'=>'AND');*/
			  $chooseDisplayMode=true;
			break;
			case "ASSO":
			case "GENE":
			case "PREF":
			  $mySearch->tab_recherche[]=
				array('FIELD'=>'t_lexique.LEX_ID_SYN',
					'VALEUR'=>0,
					'TYPE'=>'CE',
					'OP'=>'AND');
			  $chooseDisplayMode=true;
			break;
			case "CHILD":
				//update VG 27/06/12 : désactivation recherche LEX_ID_GEN (~BPI bug redmine 1431) 
				//(MS - 05/07/12 réactivation fonctionnalité pour le Parisien, recherche sur les fils nécessaire)
			  $mySearch->tab_recherche[]=
				array('FIELD'=>'t_lexique.LEX_ID_GEN',
					'VALEUR'=>intval($currentId),
					'TYPE'=>'CE',
					'OP'=>'AND');
				$mySearch->tab_recherche[]=
				array('FIELD'=>'t_lexique.LEX_ID_SYN',
					'VALEUR'=>0,
					'TYPE'=>'CE',
					'OP'=>'AND');
			$chooseDisplayMode=true;
			break;
			case "VALID": //recherche que sur les termes valides (ex: cnrs)
			  $mySearch->tab_recherche[]=
				array('FIELD'=>'t_lexique.LEX_ID_ETAT_LEX',
					'VALEUR'=>2,
					'TYPE'=>'CE',
					'OP'=>'AND');
			break;
			case "LAST":
				if(empty($lex_aff) || $lex_aff == "1") {
					$mySearch->sql .= " AND t_lexique.ID_LEX NOT IN (SELECT distinct LEX_ID_GEN FROM t_lexique) "; 
				}
			break;
			}

		  if ($currentId)  { //si un id est passé, on l'exclut des résultats
			$mySearch->tab_recherche[]=
			  array('FIELD'=>'t_lexique.ID_LEX',
				  'VALEUR'=>$currentId,
				  'TYPE'=>'C',
				  'OP'=>'AND NOT');
		  }
			if ($desc_champs_appelant[0] == "LFCP") {
				if (!empty($valeur)) $mySearch->tab_recherche[]=
                                array('FIELD'=>'syn.LEX_TERME',
                              'VALEUR'=>($desc_champs_appelant[0]=='LC'?'%':'').$valeur.$extension,
                              'TYPE'=>$typeRech,
                            'OP'=>'OR');
			}


			$mySearch->appliqueDroits();
				$mySearch->makeSQL();
				$mySearch->finaliseRequete();
				$sql=$mySearch->sql;
				//					$sql=$mySearch->sql." GROUP BY ID_VAL ORDER BY VALEUR" ;
				$btnEdit=$myFrame->getName()."?urlaction=lexSaisie&type_lex=".$type_lex."&currentId=".$currentId."&id_lex=";
				$w=600;
				$h=550;

				$mySearch->treeParams=array(
					"ID_TYPE_LEX"=>$type_lex,
					"ID_TYPE_DESC"=>$id_type_desc,
					"ID_ROLE"=>$dlex_id_role,
					"MODE"=>$mode,
					"tri"=>$tri,
			 "dspAsso"=>$dspAsso);

				  break;



                // I.2.e Index issu de T_DOC_VAL, T_DOC_MAT_VAL  OU T_MAT_VAL:
                case "V":
                case "VF":
                case "VFC":
                case "VC":
                    $champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
                    $id_type_val = $desc_champs_appelant[1];
                    if (isset($_REQUEST["val_id_gen"])) $val_id_gen=$_REQUEST["val_id_gen"];

                    if (isset($_REQUEST["lex_aff"])){
                        $lex_aff=$_REQUEST["lex_aff"];
                        $chooseDisplayMode=true;
                    }

                    include(libDir."class_chercheVal.php");

                    if ($desc_champs_appelant[0]=='VF' || $desc_champs_appelant[0]=='VFC') {
                        $typeRech='FT';
                        if ($desc_champs_appelant[0]=='VFC') {
                          $valeur=preg_replace("/[%\*]$/","",$valeur);
                          $extension="*";
                          }
                    } else {
                        $typeRech='C';
                        $valeur=preg_replace("/[%\*]$/","",$valeur); //suppression du dernier char si % ou *
                        $extension="%";
                    }

                    $mySearch=new RechercheVal();
                    $mySearch->useSession=false;
                    $mySearch->prepareSQL();
                    $mySearch->setPrefix(null);
                    //PC 12/01/2011 : Ajout des droits pour les valeurs
                    //MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
                    if (((defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits) ) 
                    ||(!defined('gSeuilAppliqueDroits') && $myUsr->Type<kLoggedDoc )) && $applyDroit){
                        require_once(libDir."class_chercheDoc.php");
                        $mySearchDoc=new RechercheDoc();
                        $mySearchDoc->appliqueDroits();
                        $sql_droit =  $mySearchDoc->sqlRecherche;
                        unset($mySearchDoc);
                    }
                    $mySearch->sqlSuffixe = " ORDER BY VALEUR";
                    
                    // VP 16/04/2014 : optimisation requête (suppression distinct)
//                    $mySearch->sql = "SELECT distinct v.ID_VAL as ID_VAL,VALEUR as VALEUR 
//                        FROM t_val v LEFT JOIN t_doc_val on t_doc_val.ID_VAL = v.ID_VAL
//						LEFT JOIN t_doc t1 ON (t1.ID_DOC=t_doc_val.ID_DOC)
//						WHERE v.ID_LANG=".$db->Quote($id_lang);
//					if(!empty($sql_droit)  && (!isset($type_priv) || $type_priv == 0)) $mySearch->sql .=" $sql_droit ";

					$mySearch->sql = "SELECT v.ID_VAL as ID_VAL,VALEUR as VALEUR FROM t_val v WHERE v.ID_LANG=".$db->Quote($id_lang);
					
					if(!empty($sql_droit)  && (!isset($type_priv) || $type_priv == 0)) {
						$mySearch->sql .="and exists (select ID_VAL from t_doc_val,t_doc t1 where t1.ID_DOC=t_doc_val.ID_DOC $sql_droit )";
					}

					$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);

                    if (!empty($valeur)) $mySearch->tab_recherche[]=array('FIELD'=>'VALEUR',
                              'VALEUR'=>($desc_champs_appelant[0]=='VC'?'%':'').$valeur.$extension,
                              'TYPE'=>$typeRech,
                            'OP'=>'AND');


                    $mySearch->tab_recherche[]=array('FIELD'=>'VAL_ID_TYPE_VAL',
                            'VALEUR'=>$id_type_val,
                             'TYPE'=>'C',
                             'OP'=>'AND');

                    $mySearch->treeParams=array("ID_TYPE_VAL"=>$id_type_val);
                    
                    // VP 21/01/10 : ajout paramètre val_id_gen
                    if ($val_id_gen) {

                        $mySearch->sql = str_replace('FROM',',VAL_ID_GEN as PARENT FROM',$mySearch->sql);

                        $mySearch->tab_recherche[]=
                        array('FIELD'=>'VAL_ID_GEN',
                            'VALEUR'=>$_REQUEST['val_id_gen'],
                            'TYPE'=>'C',
                            'OP'=>'AND');
                    }
					// VP 9/01/18 : ajout paramètre id_fonds
					if ($_REQUEST['id_fonds']) {
						$mySearch->tab_recherche[]=
						array('FIELD'=>'VAL_ID_FONDS',
							  'VALEUR'=>$_REQUEST['id_fonds'],
							  'TYPE'=>'CE',
							  'OP'=>'AND');
					}
					$mySearch->appliqueDroits();
                    $mySearch->makeSQL();
                    $mySearch->finaliseRequete();
                    $sql=$mySearch->sql ;
                    //$sql=$mySearch->sql." GROUP BY v.ID_VAL ORDER BY VALEUR" ;
                    $affich_nb=0;
                    $urlparams=",".$db->Quote($id_type_val);

					$btnEdit=$myFrame->getName()."?urlaction=valSaisie&val_id_type_val=".$id_type_val."&id_val=";
                    $size=400;
                    $w=400;
                    $h=300;

                 break;

				case "P" : // recherche de personne
				case "PF" :
				case "PFC" :
				case "PC" :

		  // VP 24/07/09 : ajout cas PF, PFC et PC
		  // VG 05/05/11 : utilisation de concat_ws à la place de concat
		  // VG 21/06/11 : ajout de la gestion de $chooseDisplayMode
		  // VG 22/06/11 : modif de la gestion du type_lex et forçage du type d'affichage en alphabétique si pas de type_lex
					// VP 2/11/17 : paramètres remontés en début de cas pour modificaiotn éventuelle requête sql
					$id_type_desc=$desc_champs_appelant[2];
					$dlex_id_role=$desc_champs_appelant[3];
					$mode = $desc_champs_appelant[4];
					
					include(libDir."class_cherchePers.php");

					if ($desc_champs_appelant[0]=='PF' || $desc_champs_appelant[0]=='PFC') {
						$typeRech='FT';
						if ($desc_champs_appelant[0]=='PFC') {
						  $valeur=preg_replace("/[%\*]$/","",$valeur);
						  $extension="*";
						}
					} else {
						$typeRech='C';
						$valeur=preg_replace("/[%\*]$/","",$valeur); //suppression du dernier char si % ou *
						$extension="%";
					}

					$mySearch=new RecherchePersonne();
					$mySearch->useSession=false; //pas de mise en session
					$mySearch->setPrefix(null);
					$mySearch->prepareSQL();
					$mySearch->sqlSuffixe='';
					// VP 8/07/09 : ajout critère ID_PERS!=0 pour optimisation
					// VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et order by sinon
					
					if (defined("gFieldsForPersTerme"))
						$pers_terme = "trim( trailing ' ' from concat_ws(' ', ".trim(strtoupper(implode(",", unserialize(gFieldsForPersTerme)))).") )";
					else
						$pers_terme = "trim( trailing ' ' from concat_ws(' ', PERS_NOM, PERS_PRENOM) )";
					
					if($affich_nb){
						$mySearch->sql="select distinct t_personne.ID_PERS as ID_VAL, $pers_terme as VALEUR, PERS_ID_ETAT_LEX , count(ID_DOC) as NB
						from t_personne left join t_doc_lex on (t_personne.ID_PERS=t_doc_lex.ID_PERS and t_doc_lex.ID_PERS!=0)";
						$mySearch->sqlSuffixe = "  GROUP BY t_personne.ID_PERS, t_personne.ID_LANG ORDER BY VALEUR";
					}else{
						if($id_type_desc || $dlex_id_role){
							$mySearch->sql = "select distinct t_personne.ID_PERS as ID_VAL, $pers_terme as VALEUR, PERS_ID_ETAT_LEX
							from t_personne left join t_doc_lex on (t_personne.ID_PERS=t_doc_lex.ID_PERS and t_doc_lex.ID_PERS!=0) ";
						}else{
							$mySearch->sql="select t_personne.ID_PERS as ID_VAL, $pers_terme as VALEUR, PERS_ID_ETAT_LEX from t_personne";
						}
						$mySearch->sqlSuffixe = "  ORDER BY VALEUR";
					}

					$mySearch->sql .=" where t_personne.ID_LANG=".$db->Quote($id_lang);
					
					$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);

					  if (isset($_REQUEST["type_lex"])) $type_lex=$_REQUEST["type_lex"];
					  else $type_lex=$desc_champs_appelant[1]; //type lex
					  
					  $postedTypeLex = $mySearch->getValueForField("PERS_ID_TYPE_LEX");
					  if(!empty($postedTypeLex)) $type_lex=$postedTypeLex;
					  
					  if(empty($type_lex)) {
						$lex_aff = '1';
						$chooseDisplayMode = false;
					  } else {
						  $chooseDisplayMode = true;
							 if (isset($_REQUEST["lex_aff"])) {
								$lex_aff=$_REQUEST["lex_aff"];
							 } else if(!empty($desc_champs_appelant[5])) {
								$lex_aff = $desc_champs_appelant[5];
							 } else {
								$lex_aff = '1';
							 }
					  }					 
						 // VP 10/09/09 : ajout test sur valeur
					if (!empty($valeur)) $mySearch->tab_recherche[]=array('FIELD'=>'PERS_NOM,PERS_PRENOM',
							   'VALEUR'=>($desc_champs_appelant[0]=='PC'?'%':'').$valeur.$extension,
								  'TYPE'=>$typeRech,
								'OP'=>'AND');
	
	
					if (!empty($type_lex)) {
						$mySearch->tab_recherche[]=
							array('FIELD'=>'PERS_ID_TYPE_LEX',
								'VALEUR'=>$type_lex,
								'TYPE'=>'CE',
								'OP'=>'AND');
					  }

					if ($id_type_desc) { //type desc
						$mySearch->tab_recherche[]=
							array('FIELD'=>'ID_TYPE_DESC',
								  'VALEUR'=>$id_type_desc,
								  'TYPE'=>'CE',
								  'OP'=>'AND');
					}
					if ($dlex_id_role) { //role
						$mySearch->tab_recherche[]=
							array('FIELD'=>'DLEX_ID_ROLE',
								'VALEUR'=>$dlex_id_role,
								'TYPE'=>'CE',
								'OP'=>'AND');
					}
					if (isset($_GET['pers_organisation']) && !empty($_GET['pers_organisation'])){
						$mySearch->tab_recherche[]=array('FIELD'=>'PERS_ORGANISATION',
										  'VALEUR'=>$_GET['pers_organisation'],
										  'TYPE'=>'C',
										'OP'=>'AND');
					}
				  
				//update VG 19/04/11 : ajout du mode (ident. à type "L")
				if ($mode) {
					switch ($mode) {
						case "SYNO":
						    $mySearch->tab_recherche[]=
								array('FIELD'=>'t_personne.PERS_ID_SYN',
								'VALEUR'=>0,
								'TYPE'=>'CE',
								'OP'=>'AND');
							//$chooseDisplayMode=true;
						break;
						case "CHILD":
								$mySearch->tab_recherche[]=
								array('FIELD'=>'PERS_ID_GEN',
									'VALEUR'=>$currentId,
									'TYPE'=>'CE',
									'OP'=>'AND'
								);
						break;
						case "LAST":
							if(empty($lex_aff) || $lex_aff == "1") {
								$mySearch->sql .= " AND t_personne.ID_PERS NOT IN (SELECT distinct PERS_ID_GEN FROM t_personne) "; 
							}
						break;
						/*case "GENE":
						case "PREF":
						  $mySearch->tab_recherche[]=
							array('FIELD'=>'t_lexique.LEX_ID_SYN',
								'VALEUR'=>0,
								'TYPE'=>'CE',
								'OP'=>'AND');
						  $chooseDisplayMode=true;*/
						break;
					}
				}
				if ($currentId)  { //si un id est passé, on l'exclut des résultats
					$mySearch->tab_recherche[]=
						array('FIELD'=>'t_personne.ID_PERS',
							'VALEUR'=>$currentId,
							'TYPE'=>'C',
							'OP'=>'AND NOT');
				}

				$mySearch->appliqueDroits();
				$mySearch->makeSQL();
				$mySearch->finaliseRequete();
				$sql=$mySearch->sql ;
				//$sql=$mySearch->sql." GROUP BY VALEUR" ;
				  $btnEdit=$myFrame->getName()."?urlaction=personneSaisie&id_pers=";
				  $w=600;
				  $h=650;
	
					$mySearch->treeParams=array(
												"ID_TYPE_LEX"=>$type_lex,
												"ID_TYPE_DESC"=>$id_type_desc,
												"ID_ROLE"=>$dlex_id_role,
												"MODE"=>$mode,
												"tri"=>$tri,
												"dspAsso"=>$dspAsso);
					break;


				case "M" : // recherche de matériels
		  // VP 9/07/09 : optimisation affichage matériel si affich_nb=0
				  include(libDir."class_chercheMat.php");
				  $mySearch=new RechercheMat();
				  $mySearch->useSession=false;//pas de mise en session
				  $mySearch->setPrefix(null);
				  $mySearch->prepareSQL();

				  $type=$desc_champs_appelant[1];
				   switch ($type) {
					 case "GRP" : // Recherche par groupe (mat_groupe)
						   // VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et order by sinon
						   if ($affich_nb) {
							   $mySearch->sql="SELECT distinct MAT_GROUPE as ID_VAL, MAT_GROUPE as VALEUR, count(ID_DOC) as NB
							   from t_mat left join t_doc_mat on t_doc_mat.ID_MAT=t_mat.ID_MAT where trim(t_mat.MAT_GROUPE)<>'' ";
							   $mySearch->sqlSuffixe = "  GROUP BY MAT_GROUPE";
						   }else{
							   $mySearch->sql="SELECT distinct MAT_GROUPE as ID_VAL, MAT_GROUPE as VALEUR from t_mat where trim(t_mat.MAT_GROUPE)<>'' ";
							   $mySearch->sqlSuffixe = "  ORDER BY MAT_GROUPE";
						   }
			  
						   $mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
						   if (!empty($valeur)) {
							  $mySearch->tab_recherche[]=array('FIELD'=>'ID_MAT',
									  'VALEUR'=>$valeur,
									  'TYPE'=>'CE',
									  'OP'=>'AND');
						   }
						   $mySearch->appliqueDroits();
						   $mySearch->makeSQL();
						   $mySearch->finaliseRequete();
						   //$sql=$mySearch->sql." GROUP BY MAT_GROUPE";
						   $sql=$mySearch->sql;
					 break;

			// case "ELT" : // Recherche ID_MAT et MAT_GROUPE
			case "ELT" :
				   $sqlAdd = (empty($autreChamps))?'':', '.$autreChamps.' AS COL';
				   // VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et order by sinon
				  if ($affich_nb) {
					   $mySearch->sql="SELECT distinct t_mat.ID_MAT as ID_VAL, t_mat.MAT_GROUPE as VALEUR, count(ID_DOC) as NB
					   ".$sqlAdd." from t_mat left join t_doc_mat on (t_doc_mat.ID_MAT=t_mat.ID_MAT) where 1=1 ";
					   $mySearch->sqlSuffixe = "  GROUP BY t_mat.ID_MAT, VALEUR";
				   }else{
					   $mySearch->sql="SELECT distinct t_mat.ID_MAT as ID_VAL, t_mat.MAT_GROUPE as VALEUR ".$sqlAdd." from t_mat where 1=1 ";
					   $mySearch->sqlSuffixe = "  ORDER BY t_mat.ID_MAT";
				   }
				  if(empty($_POST['chFields'])) $mySearch->setTabRechercheFromUrl();
				  else {
					$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
					$searchParamsForUrl = $mySearch->getTabRechercheForUrl();
				  }
				  if (!empty($valeur)) {
					$mySearch->tab_recherche[]=array('FIELD'=>'ID_MAT',
									 'VALEUR'=>$valeur,
									 'TYPE'=>'CE',
									 'OP'=>'AND');
				  }
						   
					$mySearch->appliqueDroits();
					$mySearch->makeSQL();
					$mySearch->finaliseRequete();
					$sql=$mySearch->sql;
					//$sql=$mySearch->sql." GROUP BY t_mat.ID_MAT";

				   break;
						   
					default : // Liste de matériels classique : recherche exacte (pas d'analyse, pas de FT !!!)
						//VP 4/12/08 : ajout possibilité index sur champ de t_mat
						$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
						if(empty($champ))
							$champ="MAT_NOM";
						// VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et order by sinon
						if ($affich_nb) {
							$mySearch->sql="SELECT distinct t_mat.ID_MAT as ID_VAL, t_mat.$champ as VALEUR, count(ID_DOC) as NB
							from t_mat left join t_doc_mat on (t_doc_mat.ID_MAT=t_mat.ID_MAT) where 1=1";
							$mySearch->sqlSuffixe = "  GROUP BY t_mat.ID_MAT, t_mat.$champ";
						}else{
							$mySearch->sql="SELECT distinct t_mat.ID_MAT as ID_VAL, t_mat.$champ as VALEUR from t_mat where 1=1";
							$mySearch->sqlSuffixe = "  ORDER BY t_mat.$champ";
						}
						
						$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
				if (!empty($valeur))
				  // VP 18/05/10 : correction bug si champ!=ID_MAT
				  $valeur=preg_replace("/[%\*]$/","",$valeur); //suppression du dernier char si % ou *
				  $extension="%";
				  $mySearch->tab_recherche[]=array('FIELD'=>'t_mat.'.$champ,
							  'VALEUR'=>$valeur.$extension,
							  'TYPE'=>'CE',
							'OP'=>'AND')
							;
							
							//limitation par extensions
							if (!empty($_GET['mat_ext'])) {
								$extSearch=new RechercheMat();
								$extSearch->useSession=false;//pas de mise en session
								$extSearch->setPrefix(null);
								$extSearch->prepareSQL();
				  
								$exts = explode(",", $_GET['mat_ext']);
								foreach ($exts as $ext) {
									$extSearch->tab_recherche[]=array('FIELD'=>'t_mat.MAT_NOM',
																		'VALEUR'=>"%.$ext",
																		'TYPE'=>'CE',
																		'OP'=>"OR");
								}
								$extSearch->makeSQL();
								$extSql = substr($extSearch->sqlRecherche, 3);
							}
							
							//limitation par format
							if (!empty($_GET['mat_format'])) {
								$fmtSearch=new RechercheMat();
								$fmtSearch->useSession=false;//pas de mise en session
								$fmtSearch->setPrefix(null);
								$fmtSearch->prepareSQL();
				  
								$fmts = explode(",", $_GET['mat_format']);
								foreach ($fmts as $fmt) {
									$fmtSearch->tab_recherche[]=array('FIELD'=>'t_mat.MAT_FORMAT',
																		'VALEUR'=> $fmt,
																		'TYPE'=>'CE',
																		'OP'=>"OR");
								}
								$fmtSearch->makeSQL();
								$fmtSql = substr($fmtSearch->sqlRecherche, 3);
							}
						   // VP 9/01/18 : ajout paramètre id_fonds
						   if ($_REQUEST['id_fonds']) {
							   $mySearch->tab_recherche[]=
							   array('FIELD'=>'MAT_ID_FONDS',
									 'VALEUR'=>$_REQUEST['id_fonds'],
									 'TYPE'=>'CE',
									 'OP'=>'AND');
						   }

						   $mySearch->appliqueDroits();
						   $mySearch->makeSQL();
						   $mySearch->finaliseRequete();
						   $sql=$mySearch->sql;
						   if (!empty($extSql)) $sql = str_replace("where 1=1", "where 1=1 AND ($extSql)", $sql);
						   if (!empty($fmtSql)) $sql = str_replace("where 1=1", "where 1=1 AND ($fmtSql)", $sql);
						   //$sql=$mySearch->sql." GROUP BY t_mat.$champ";
					 break;

				   }
				  $btnEdit=$myFrame->getName()."?urlaction=matSaisie&id_mat=";
				  $w=600;
				  $h=650;

				break;

				case "REF" : // récupération de valeurs depuis une table de référence;
				//@update VG 25/08/2010 : possibilité de passer le paramètre "tbl" dans $id_champ_appelant
					$tbl=(!empty($_REQUEST['tbl']))?$_REQUEST['tbl']:substr($id_champ_appelant,(strlen($desc_champs_appelant[0])+1));
				  	$cols=$db->MetaColumns($tbl);
				  	$cols=array_keys($cols);
		  			$affich_nb=0;
				  	if ($cols[0]) $chmp1=$cols[0];
				  	if ($cols[1]) $chmp2=$cols[1]; else $chmp2=$cols[0];
				  	if (strtolower($tbl)=='t_diffuseur') {
				  		$chmp1=$chmp2; //Cas spécial : cette table n'a pas d'ID !
				  	}
				  	//update VG 02/09/2011 : certaines tables n'ont pas d'ID_LANG, or le param de langue est mis systématiquement sur les boutons "Index"
				  	if(!in_array("ID_LANG", $cols) && !in_array("id_lang", $cols) )
				  		$id_lang = '';
				  

				  $sql="SELECT ".$chmp1." as ID_VAL, ".$chmp2." as VALEUR FROM ".$tbl;
		  if (!empty($id_lang)) $sql.=" WHERE ID_LANG=".$db->Quote($id_lang);

		  break;

				  case "IMG" : // récupération des storyboard
					$sql="SELECT t_imageur.ID_IMAGEUR as ID_VAL, t_imageur.IMAGEUR as VALEUR, count(ID_IMAGE) as NB
					  from t_imageur left join t_image ON (t_image.ID_IMAGEUR=t_imageur.ID_IMAGEUR) ";
					if (!empty($valeur)) $sql.=" WHERE t_imageur.IMAGEUR like '".$db->Quote($valeur)."%' ";
					$sql.="AND t_image.ID_LANG=".$db->Quote($id_lang)." GROUP BY t_imageur.ID_IMAGEUR, t_imageur.IMAGEUR";

				  break;

				  case "PRECISION" : //Liste des précisions
					$sql="SELECT LEX_PRECISION as ID_VAL, LEX_PRECISION as VALEUR, count(ID_DOC) as NB
					 from t_doc_lex_precision dlp left join t_doc_lex dl on (dlp.id_doc_lex=dl.id_doc_lex)";
					if (!empty($valeur)) $sql.=" WHERE LEX_PRECISION ".$db->like('%'.$valeur.'%');
					else $sql.=" WHERE LEX_PRECISION!='' ";
					if (!empty($autreChamps)) $sql.=" AND dl.ID_TYPE_DESC = ".$db->Quote($autreChamps)." ";
				  $sql.="GROUP BY LEX_PRECISION ORDER BY LEX_PRECISION ASC";
				  $affich_nb=1;
				  break;

				case "USR" :
				  include(libDir."class_chercheUsager.php");
				  $mySearch=new RechercheUsager();
				  $mySearch->useSession=false; //pas de mise en session
				  $mySearch->setPrefix(null);
				  $mySearch->sqlSuffixe='';
				  $mySearch->prepareSQL();
				  $filter=str_replace('§',',',(isset($_REQUEST['filter']))?$_REQUEST['filter']:'');
				  $filterTypeList=str_replace('§',',',(isset($_REQUEST['filterTypeList']))?$_REQUEST['filterTypeList']:'');
					
					/**
					 * @author B.RAVI 31/03/17 17:28
					 * Pour pouvoir rechercher par truncature sur le champ de recherche Societé
					 * On ajoute * si seulement le type est FTS
					 */
					if (is_array($_POST['chTypes'])){	
						$tabKeyFT=array_keys($_POST['chTypes'],'FT');
						if (is_array($tabKeyFT)){	
							foreach ($tabKeyFT as $v){
								if (!empty($_POST['chValues'][$v])){
									$_POST['chValues'][$v]=$_POST['chValues'][$v].'*';
								}
							}
						}
					}

				  $mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);

				  $mySearch->sql="select ID_USAGER as ID_VAL, ".$db->Concat('US_NOM',"' '",'US_PRENOM')." as VALEUR";
                  if (isset($_GET['champ1']))
                        $mySearch->sql .= ", ".$_GET['champ1']." as VALEUR1 ";
				  $mySearch->sql .= " from t_usager left join t_type_usager
				  on t_usager.US_ID_TYPE_USAGER=t_type_usager.ID_TYPE_USAGER WHERE 1=1";
				
		
				  //* pour pouvoir rechercher par troncature
				  $mySearch->tab_recherche[]=array('FIELD'=>'US_NOM,US_PRENOM',
							  'VALEUR'=>$valeur.'*',
							  'TYPE'=>'FT',
							'OP'=>'AND');
				if(!empty($filter)){	
					$mySearch->tab_recherche[]=array('FIELD'=>'ID_USAGER',
							  'VALEUR'=>$filter,
							  'TYPE'=>'C',
							'OP'=>'AND NOT');
				}
												
					if (isset($_GET['typeLogLimit'])){
						switch($_GET['typeLogLimit']){
							case "eq": 
								$mySearch->tab_recherche[]=array('FIELD'=>'US_ID_TYPE_USAGER',
								  'VALEUR'=>$myUsr->getTypeLog(),
								  'TYPE'=>'C',
									'OP'=>'AND');
								break ; 
							case "lt":
								$mySearch->tab_recherche[]=array('FIELD'=>'US_ID_TYPE_USAGER',
								  'VALEUR'=>$myUsr->getTypeLog(),
								  'TYPE'=>'CVLT',
									'OP'=>'AND');
								break ;
								case "lte":
								$mySearch->tab_recherche[]=array('FIELD'=>'US_ID_TYPE_USAGER',
								  'VALEUR'=>$myUsr->getTypeLog(),
								  'TYPE'=>'CVLTE',
									'OP'=>'AND');
								break ; 
							case "gt":
								$mySearch->tab_recherche[]=array('FIELD'=>'US_ID_TYPE_USAGER',
								  'VALEUR'=>$myUsr->getTypeLog(),
								  'TYPE'=>'CVGT',
									'OP'=>'AND');
								break ; 
							case "gte":
								$mySearch->tab_recherche[]=array('FIELD'=>'US_ID_TYPE_USAGER',
								  'VALEUR'=>$myUsr->getTypeLog(),
								  'TYPE'=>'CVGTE',
									'OP'=>'AND');
								break ; 
							}
								
					}
					if (isset($_GET['filterTypeList']) && !empty($filterTypeList)){
						$mySearch->tab_recherche[]=array('FIELD'=>'US_ID_TYPE_USAGER',
							  'VALEUR'=>$filterTypeList,
							  'TYPE'=>'C',
							'OP'=>'AND NOT');
					}
					if (isset($_GET['type_us_min'])){
						$mySearch->tab_recherche[]=array('FIELD'=>'US_ID_TYPE_USAGER',
							  'VALEUR'=>$_GET['type_us_min'],
							  'TYPE'=>'CVGTE',
							'OP'=>'AND');
					}

				if(isset($_REQUEST['groupFilter']) && !empty($_REQUEST['groupFilter'])){
					$mySearch->tab_recherche[]=array('FIELD'=>'ID_GROUPE',
							  'VALEUR'=>"".$_REQUEST['groupFilter'],
							  'TYPE'=>'UGR',
							  'OP'=>(isset($_REQUEST['groupFilterOP']) && !empty($_REQUEST['groupFilterOP']) && in_array($_REQUEST['groupFilterOP'],array('AND','OR','AND NOT')))?$_REQUEST['groupFilterOP']:'AND NOT');
				}
				$mySearch->appliqueDroits();
				$mySearch->makeSQL();
				$mySearch->finaliseRequete();
				$sql=$mySearch->sql." ORDER BY VALEUR";
				$affich_nb=0;
					
				$btnEdit=$myFrame->getName()."?urlaction=usagerSaisie&id_usager=";
				$w=600;
				$h=550;

				break;

		   case "FON" :
		   case "FONC" :
				include(libDir."class_chercheFonds.php");
				$mySearch=new RechercheFonds();
				$mySearch->useSession=false; //pas de mise en session
				$mySearch->setPrefix(null);
				$mySearch->sqlSuffixe='';
				$mySearch->prepareSQL();
				$filter=str_replace('§',',',isset($_REQUEST['filter'])?$_REQUEST['filter']:'');
			  
				if (isset($_REQUEST["lex_aff"])) $lex_aff=$_REQUEST["lex_aff"];
				 
				// L'affichage hierarchique est reservé au utilisateurs admins ayant accès à tout les fonds, affichage standard sinon 
				if($lex_aff == 2 && ($myUsr->getTypeLog()< kLoggedDoc)){
					$lex_aff = 1;
				}
				
				$extension="";
				if ($desc_champs_appelant[0]=='FONC') {
					$valeur=preg_replace("/[%\*]$/","",$valeur);
					$extension="*";
				}
				 

				// VP 2/03/09 : ajout possibilité index sur champ de t_fonds
				$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
				if(empty($champ)) $champ="FONDS";

				$mySearch->sql="select ID_FONDS as ID_VAL, $champ as VALEUR , ID_FONDS_GEN as PARENT
					  from t_fonds f
					  where 1=1";

					  $mySearch->tab_recherche=array(1=>array('FIELD'=>$champ,
								  'VALEUR'=>$valeur.$extension,
								  'TYPE'=>'FT',
								'OP'=>'AND'),
							   2=>array('FIELD'=>'ID_LANG',
								  'VALEUR'=>$id_lang,
								  'TYPE'=>'C',
								'OP'=>'AND'));
								
						
						if(isset($filter) && !empty($filter)){
							$mySearch->tab_recherche[]=array('FIELD'=>'ID_FONDS',
								  'VALEUR'=>$filter,
								  'TYPE'=>'C',
								'OP'=>'AND NOT');
								//NOTA : tjs mettre les AND NOT à la fin !
						}
				// MS on se sert de la valeur contenue dans "type_priv" pour set le privilège minimum requis pour afficher les fonds
				// Si vide => la limite sera 0 (défini dans appliqueDroits)
			
				if(isset($type_priv) && is_numeric($type_priv)){
					$privilege_mini = $type_priv;
				}
				
				//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
				if (((defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits) ) 
				||(!defined('gSeuilAppliqueDroits') && $myUsr->Type<kLoggedDoc )) && $applyDroit){
					$mySearch->appliqueDroits($privilege_mini);
				}
				
				$mySearch->makeSQL();
				$mySearch->finaliseRequete();
				$sql=$mySearch->sql." ORDER BY VALEUR";
				$affich_nb=0;
				
				
				$mySearch->treeParams=array(
						"ID_FONDS"=>$type_lex,
						"MODE"=>(isset($mode))?$mode:'',
						"tri"=>(isset($tri))?$tri:'',
						"dspAsso"=>(isset($dspAsso))?$dspAsso:null);
		

				break;

                 case "CAT" :
                  include(libDir."class_chercheCat.php");
                  $mySearch=new RechercheCat();
                  $mySearch->useSession=false; //pas de mise en session
                  $mySearch->setPrefix(null);
                  $mySearch->sqlSuffixe='';
                  $mySearch->prepareSQL();
				  if(isset($desc_champs_appelant[1])){
					$type=$desc_champs_appelant[1];
				  }
				if (isset($_REQUEST["type_cat"])) $type=$_REQUEST["type_cat"];
				
				$filter=str_replace('§',',',$_REQUEST['filter']);
				if (isset($_REQUEST["lex_aff"])) $lex_aff=$_REQUEST["lex_aff"];

                $btnEdit=$myFrame->getName()."?urlaction=catSaisie&fromPalette=1&type_cat=".$type_cat."&currentId=".$currentId."&id_cat=";
				$w=600;
				$h=550;

				
				$mySearch->treeParams=array();
                  $mySearch->sql="select ID_CAT as ID_VAL, CAT_NOM as VALEUR
                  from t_categorie as CAT ";
                  $mySearch->tab_recherche=array(1=>array('FIELD'=>'CAT_NOM',
                              'VALEUR'=>$valeur,
                              'TYPE'=>'FT',
                            'OP'=>'AND'));
							if ($type){
								$mySearch->tab_recherche[]=
								array('FIELD'=>'CAT.CAT_ID_TYPE_CAT',
								  'VALEUR'=>$type,
								  'TYPE'=>'C',
								'OP'=>'AND');
								$mySearch->treeParams['ID_TYPE_CAT']  = $type;
							}
							$mySearch->tab_recherche[] = array('FIELD'=>'ID_CAT',
							  'VALEUR'=>$filter,
							  'TYPE'=>'C',
							'OP'=>'AND NOT');
						 
						
					$mySearch->appliqueDroits();
					$mySearch->makeSQL();
				  	$mySearch->finaliseRequete();
					$sql=$mySearch->sql." ORDER BY VALEUR";
					$affich_nb=0;

			break;
				
				
			case "GRP" :
				  include(libDir."class_chercheGroupe.php");
				  $mySearch=new RechercheGroupe();
				  $mySearch->useSession=false; //pas de mise en session
				  $mySearch->setPrefix(null);
				  $mySearch->sqlSuffixe='';
				  $mySearch->prepareSQL();
		  			$filter=str_replace('§',',',$_REQUEST['filter']);

				  $mySearch->sql="select ID_GROUPE as ID_VAL, GROUPE as VALEUR
				  from t_groupe g where 1=1";
				  $mySearch->tab_recherche=array(1=>array('FIELD'=>'GROUPE',
							  'VALEUR'=>$valeur,
							  'TYPE'=>'FT',
							'OP'=>'AND'),
						   2=>array('FIELD'=>'ID_GROUPE',
							  'VALEUR'=>intval($filter),
							  'TYPE'=>'C',
							'OP'=>'AND NOT')
						   );
					$mySearch->appliqueDroits();
					$mySearch->makeSQL();
				  	$mySearch->finaliseRequete();
					$sql=$mySearch->sql." ORDER BY VALEUR";
					$affich_nb=0;

				break;

				case "PAN" :
				  	include(libDir."class_cherchePanier.php");
					 if (isset($_REQUEST["lex_aff"])) $lex_aff=$_REQUEST["lex_aff"];
							$chooseDisplayMode=true;
						  $mySearch=new RecherchePanier();
		
						  if ($_REQUEST['onlyDossier'])
						  $mySearch->onlyDossier=true; //on n'affique des dossiers
				  else $mySearch->onlyDossier=false; //on n'affique des dossiers
		
						  if ($_REQUEST['includeRoot'])
						  $mySearch->includeRoot=true; //on n'affique des dossiers
				  else $mySearch->includeRoot=false; //on n'affique des dossiers
		
						  if ($_REQUEST['includePublic'])
						  $mySearch->includePublic=$_REQUEST['includePublic']; //on n'affique des dossiers publics
				  else $mySearch->includePublic=false; //on n'affique des dossiers	publics
		
				  // VP 27/8/09 : nouveaux paramètres affichage hiérarchique des paniers (excludePanier et excludeId)
				  if ($_REQUEST['excludePanier'])
					$mySearch->excludePanier=$_REQUEST['excludePanier']; //on n'affiche pas le panier
				  else $mySearch->excludePanier=0; //on n'affiche panier et sélections
		
				  if ($_REQUEST['excludeId']) $mySearch->excludeId=$_REQUEST['excludeId']; //Id Panier à exclure

		  			$mySearch->useCookies=false; //on ne garde pas l'état aff. hiér. en cookie
					$mySearch->useSession=false; //pas de mise en session
				  $mySearch->setPrefix(null);
				  $mySearch->sqlSuffixe='';
				  $mySearch->prepareSQL();

		
					$filter=str_replace('$',',',$_REQUEST['filter']);
		
					$mySearch->sql="select ID_PANIER as ID_VAL, PAN_TITRE as VALEUR
						from t_panier where PAN_ID_USAGER=".$db->Quote($myUsr->UserID);
					$mySearch->tab_recherche[]=array('FIELD'=>'PAN_TITRE',
							  	'VALEUR'=>$valeur,
								'TYPE'=>'FT',
								'OP'=>'AND');
		
					if ($filter!=='')
				  		$mySearch->tab_recherche[]=array('FIELD'=>'ID_PANIER',
										  'VALEUR'=>$filter,
										  'TYPE'=>'C',
										'OP'=>'AND NOT');
		
		
					if ($mySearch->includePublic =='2')
						$mySearch->tab_recherche[]=array('FIELD'=>'PAN_PUBLIC',
										  'VALEUR'=>'1',
										  'TYPE'=>'C',
										'OP'=>'AND');
			
					$mySearch->makeSQL();
				  	$mySearch->finaliseRequete();
					$sql=$mySearch->sql." ORDER BY VALEUR";
					$affich_nb=0;
					$btnEdit=$myFrame->getName()."?urlaction=panierSaisie";
				   	$w=500;
					$h=400;


				break;

				case "RDIF" :
					$champ=strtolower(substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1));
                                    
                                        $rs_type_bdd_field = $db->execute("select $champ from t_doc_redif");
                                        $type_bdd_field=$rs_type_bdd_field->FetchField();
                                        
                                        
                                        
                                        $where_rdif=$champ." like ".$db->Quote($valeur.'%');
                                        //B.RAVI 20015-09-17 nous ne pouvons pas faire de like sur un type date en bdd, l'utilisateur va chercher par ex : "2012-05-12","2012-05","2012"
                                        if ($db->MetaType($type_bdd_field->type)=='D'){//si champ date
                                         
                                         if (trim($valeur)!=''){
                                         $tab_redif_date=explode('-',$valeur);   
                                        
                                        
                                        $where_rdif=null;
                                        foreach($tab_redif_date as $k=>$v){
                                           if ($k==3) {break;}
                                           if ($k) {$where_rdif.='AND ';}
                                           if (!$k) { $where_rdif.="EXTRACT(YEAR FROM ".$champ.")=".$db->Quote($v);} 
                                           if ($k==1) { $where_rdif.="EXTRACT(MONTH FROM ".$champ.")=".$db->Quote($v);}  
                                           if ($k==2) { $where_rdif.="EXTRACT(DAY FROM ".$champ.")=".$db->Quote($v);}  
                                           
                                           
                                           
                                        }
                                        }else{
                                            $where_rdif="1=1";
                                        }
                                        }
                                        
					$sql="SELECT distinct $champ as ID_VAL, $champ as VALEUR from t_doc_redif WHERE ".$where_rdif." ORDER BY ".$champ;
//                                        var_dump($sql);
                                     
				break;
				
				case "FES" :
				case "FEST" :
					include(libDir."class_chercheFest.php");
					$mySearch=new RechercheFest();
					$mySearch->useSession=false;
					$mySearch->setPrefix(null);
					$mySearch->sqlSuffixe='';
					$mySearch->prepareSQL();

					$mySearch->sql="select ID_FEST as ID_VAL, FEST_LIBELLE as VALEUR
					from t_festival as FES 
					where FEST_LIBELLE != '' AND ID_LANG = ".$db->Quote($_SESSION['langue']);
					
					$valeur=preg_replace("/[%\*]$/","",$valeur);
					$extension="*";
					
					$mySearch->tab_recherche=array(1=>array('FIELD'=>'FEST_LIBELLE', 'VALEUR'=>$valeur.$extension, 'TYPE'=>'FT', 'OP'=>'AND'));
								 
								
					$mySearch->makeSQL();
					$mySearch->finaliseRequete();
					$sql=$mySearch->sql." ORDER BY VALEUR";
					$affich_nb=0;
					
					$btnEdit=$myFrame->getName()."?urlaction=festSaisie&id_fest=";
				    $w=600;
				    $h=550;

				break;
				
				case "FORMAT-MAT" :
					$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
					if(empty($champ)) {
						$champ = "format_mat";
					}
					$table="t_format_mat";
					$champ_id="format_mat";
						// VP 13/06/2012 : optimisation requête avec group by réservé à $affich_nb=1 et order by sinon
					if ($affich_nb) {
						$sql ="SELECT $table.$champ as VALEUR, $table.$champ_id as ID_VAL, count(distinct format_mat) as NB from ".$table;
					}else{
						$sql ="SELECT distinct $table.$champ as VALEUR, $table.$champ_id as ID_VAL from ".$table;
					}
					
				 	$sql .= " WHERE 1=1 ";
					if($valeur!="") $sql.=" AND $table.$champ ".$db->like($valeur.'%');
					if ($affich_nb) {
						$sql.=" GROUP BY VALEUR,ID_VAL";
					}else{
						$sql.=" ORDER BY VALEUR";
					}
				break;
			} 
	}
	else
		exit;
  //debug($sql);
  //trace($sql);
  //echo 'debug_affich sql '.$sql.'<br />';
?>
<script>
  function dspClock (toggle) {

	if (toggle) {
	  document.getElementById('clock').style.display='block';
	  if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='none';
	  }
	else {
	  document.getElementById('clock').style.display='none';
	  if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='block';
	  }
	return true;
  }
</script>
 <form align='center' name="form1" id="form1" onSubmit="dspClock(true)" method="post" action="<?=$myFrame->getName()."?urlaction=chercheIndex2".$myFrame->addUrlParams()?>" >
  <fieldset >
  <legend><?= $titre_index ?></legend>
	<input name="ligne" type="hidden" value="" />
	<input name="id_type_val" type="hidden" value="<?= isset($id_type_val)?$id_type_val:'' ?>" />
	<input name="affich_nb" type="hidden" value="<?= $affich_nb?>" />
	<input name="id_doc" type="hidden" value="<?= isset($id_doc)?$id_doc:'' ?>" />
	<input name="titre_index" type="hidden" value="<?= urlencode($titre_index) ?>" />
	<input name="id_input_result" type="hidden" value="<?= $id_input_result ?>" />
	<input name="index_filter" type="hidden" value="<?= $index_filter ?>" />
	<input name="champ" type="hidden" value="<?= $id_champ_appelant ?>" />
	<!--input name="valeur" type="hidden" value="<?= str_replace('"','&quot;',$valeur) ?>" /-->
  <!-- ' -->
	<input name="id_lang" type="hidden" value="<?= $id_lang ?>" />
	<input name="type_lex" type="hidden" value="<?= $type_lex ?>" />
  <input name="val_id_gen" type="hidden" value="<?= isset($val_id_gen)?$val_id_gen:'' ?>" />
  <input name="page" type="hidden" value="<?=$myFrame->page?>" /> <!-- juste utilisé pour la input box -->
  <input name="id_asso" type="hidden" value="<?= isset($id_asso)?$id_asso:'' ?>" />


		<?

	//Vérif autorisation visionnage hiérarchique
	// VP 17/03/09 : ajout LFC à types hiérarchiques
	// PC 18/12/12 : ajout LFCS à types hiérarchiques
	if (in_array($desc_champs_appelant[0],array('LF','L','LC','LFC','LFCS','LFCP','P','PF','PFC','PC'))) { //rech lexique
		// VP 8/11/12 : changement requête t_type_lex en "in" 
		//		$hier=$db->GetOne('SELECT HIERARCHIQUE FROM t_type_lex WHERE id_type_lex='.$db->Quote($type_lex));
		$hier=$db->GetOne("SELECT HIERARCHIQUE FROM t_type_lex WHERE id_type_lex in ('".str_replace(",","','",$type_lex)."')");
		if ($hier!='1') {$chooseDisplayMode=false;$lex_aff=1;} //forçage du mode alpha seul si pas hiérarchique
	}
	
	if(in_array($desc_champs_appelant[0],array('FON')) && $lex_aff==2){
		$hier='1';
	}
	
	require(getSiteFile("formDir","formPalette.php"));

		?>
  </fieldset>
  </form>
<div id='clock' style='display:none;text-align:center'><?=kPatientez?><br/><img src="<?=imgUrl?>wait30trans.gif"></div>
<?
		global $db;
		// I. Affichage Alphabétique
		debug($sql, "gold", true);
		if ($lex_aff!=2){
	  $nb2show=(defined("gPaletteNbParDefaut")?gPaletteNbParDefaut:15);
	  debug($sql, "gold", true);
	  $myFrame->initPager($sql,$nb2show,null,null,60);
			$param["affich_nb"] = $affich_nb;
			$param["titre_index"]=$titre_index;
			$param["btn_edit"]=isset($btnEdit)?$btnEdit:'';
			$param["jsFunction"]=$jsFunction;
			$param["id_input_result"]=$id_input_result;
			$param["page"]=$myFrame->page;
			if (!empty($w)) $param["w"]='&w='.$w;
			if (!empty($h)) $param["h"]='&h='.$h;
			unset($_GET['lex_aff']); //experimental
			$param["get_params"]=$myFrame->addUrlParams($searchParamsForUrl, true); //@update VG 29/06/2010 : ajout du tableau de paramètres de la recherche(rempli ou vide) pour l'url


            $myFrame->addParamsToXSL($param);
          $myFrame->afficherListe('val',getSiteFile("listeDir",$xsl.'.xsl'),$blnPrivilegeDoc);

		}
		// II. Affichage hiérarchique
		else
		{

		   // require_once(libDir."class_dtree.php");
			require_once(libDir."class_tafelTree.php");

			echo "<script>dspClock(true)</script>";
		  if (strpos($xsl,'Edit')===false) $btnEdit=''; //Mode simple, on réinit le btnEdit sinon on le transmet à l'arbre
							  // et chaque noeud devient éditable
		  else { // Ajout du redimensionnement
			$_str=split('&',$btnEdit); //Mais on ne peut pas l'ajouter en fin de chaine (réservé pour l'ID)
			$_str[0].="&w=".$w."&h=".$h; //donc on le colle après le premier argument
			$btnEdit=implode('&',$_str); //et on recolle la chaine... mini-usine à gaz donc
		  }
		  $title=GetRefValue('t_type_lex',$id_type_lex,$_SESSION['id_lang']);
			
		  $myAjaxTree=new tafelTree($mySearch,'form1');
		  $filter_values=str_replace('§',',',(isset($_REQUEST['filter']))?$_REQUEST['filter']:'');
		  $mySearch->treeParams['filter_values'] = explode(',',$filter_values);
		  $myAjaxTree->JSlink="window.parent.".$jsFunction."(\"%s\",\"%s\",%s)";
		   if($mySearch->noAjax) $myAjaxTree->noAjax=$mySearch->noAjax;
		   if($mySearch->useCookies) $myAjaxTree->useCookies=$mySearch->useCookies;
		   if($mySearch->imgBase) $myAjaxTree->imgBase=$mySearch->imgBase;
			if($mySearch->includeRoot)	$myAjaxTree->includeRoot=$mySearch->includeRoot;
	
		  //update VG 19/05/2011 : ajout constante
		  if(defined('gTafelTree_ImgBaseUrl')) $myAjaxTree->imgBaseUrl = gTafelTree_ImgBaseUrl;
		  // VP 27/01/10 : ajout paramètre imgBaseUrl
		  if ($_REQUEST['imgBaseUrl']) $myAjaxTree->imgBaseUrl=$_REQUEST['imgBaseUrl'];
		  $myAjaxTree->JSReturnFunction=$jsFunction;
		  $myAjaxTree->makeRoot();

            debug($myAjaxTree, "red", true);
		   if ($valeur!='') { //En plus, on a une recherche sur un terme
			  	$rst=$db->GetAll($sql); // Lancement de la requete.
				  if (!empty($rst)) {
					  foreach ($rst as $row) { //Pour chaque résultat...
						 switch ($desc_champs_appelant[0]) {
						   case 'LF':
						   case 'L':
						   case 'LC' :
						   case 'LFC':
						   case 'LFCS':
			                 if($row['LEX_ID_ETAT_LEX']>=2) { //... on ne garde que les termes valides
			                  if ($row['LEX_ID_SYN']==0) {//s'il y a un synonyme préférentiel, c'est lui qu'on retient
			                  $myAjaxTree->revealNode($row['ID_VAL']);
			                   }
			                  else $myAjaxTree->revealNode($row['LEX_ID_SYN']);
			                 }
			               break;
                        
                            default:
								$myAjaxTree->revealNode($row['ID_VAL']);
						   break;
			
						 }
			
					  }
					echo "<div class='story'>".count($rst)." ".kResultats."</div>";
			  } else {
			  		echo "<div class='story'>".kAucunResultat."</div>";
			  }
			}
			if (!empty($id_asso)) $myAjaxTree->revealNode($id_asso,'rebond');
	  //debug($btnEdit);
			$myAjaxTree->renderOutput($id_input_result,true,$btnEdit);
	  echo "<script>dspClock(false)</script>";
	  //debug($myAjaxTree,'gold');
		}

	  ?>
