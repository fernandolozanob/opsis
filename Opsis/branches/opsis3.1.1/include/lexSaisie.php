<?php
require_once(modelDir.'model_lexique.php');
global $db;
//debug($_POST,'gold',true);
$myPage=Page::getInstance();
$myPage->nomEntite = kLexique;
$myPage->setReferrer(true);
//@update VG 27/05/10 : pour l'affichage de la liste des documents liés
$docLiesUseMysql = '1';

$myLex=new Lexique();

if (!isset($_REQUEST['commande']))
	$_REQUEST['commande']='';

switch ($_REQUEST['commande']) {
	case "SAVE":  // Sauvegarde après modif infos
		$myLex->updateFromArray($_POST);
		$ok=$myLex->save(); // renvoie 'crea' si on crée, rien si on update
		if ($ok) $myLex->saveVersions($_POST['version']);
		$myLex->saveSyno();
		$myLex->saveAsso();
		$myLex->saveChildren();
		//VP 30/6/18 : déplacement appel updateDocChampsXML depuis méthode save
		$myLex->updateDocChampsXML(true);

		$myLex->getLexique();
		$myLex->getChildren();
		$myLex->getVersions();
		$myLex->getDocs();
		$myLex->getSyno();
		$myLex->getAsso();
		$myLex->getSynoPref();
		//PC 18/12/12 : remplacement des synonymes non préférentiels
		if ($_REQUEST['replaceNonPrefSyno']) $myLex->replaceNonPrefSyno();
		
		// VP 18/03/09 : retour à la liste après sauvegarde

		if ($myPage->hasReferrer() && $ok) {
			$url = $myPage->getReferrer();
			//by LD 30/06/08 pour ajouter le terme à la redirection => du coup le retour vers la liste filtre automatiquement sur le terme sauvé
			if ($_REQUEST['fldToPassToRedirection'])
				$url .= (strpos($url, "?") === false ? "?" : "&") . $_REQUEST['fldToPassToRedirection']."=".urlencode($myLex->t_lexique["LEX_TERME"]);
			$myPage->redirect($url);
		}
	break;

	case "SUP": // Supression
	case "SUPPR": // Retrocompatibilité
		// VP 10/3/10 : Suppression par commande SUP au lieu de SUPPR
		$myLex->updateFromArray($_POST);
		// VP 6/07/2018 : appel getDocs puis updateDocChampsXML pour mettre à jour t_doc et Solr
		$myLex->getDocs(false);
		if ($myLex->delete() && $myLex->detachChildren()){
			$myLex->updateDocChampsXML(true);
			if ($myPage->hasReferrer())
				$myPage->redirect($myPage->getReferrer());
		}
	break;

	case "DETACH_FILS": //Détachage des lex filles attachées
		$myLex->updateFromArray($_POST);
		$myLex->detachChildren();
		$myLex->getLexique();
		$myLex->getDocs();
		$myLex->getVersions();
		$myLex->getSyno();
		$myLex->getAsso();
		$myLex->getSynoPref();
	break;

	case "DETACH_DOC" : //Rupture du lien avec doc, pers et mat
	case "DETACH_PERS" :
	case "DETACH_MAT":
		$myLex->updateFromArray($_POST);
		$entity=split('_',$_REQUEST['commande']);
		$myLex->detachLink($entity[1]);
		//update VG 12/10/11 : màj du champ DOC_VAL
		$myLex->getDocs();
		$myLex->detachLink($entity[1]);
		if($entity['1'] == "DOC") {
			$myLex->updateDocChampsXML();
		}
		$myLex->getChildren();
		$myLex->getDocs();
		$myLex->getVersions();
		$myLex->getSyno();
		$myLex->getAsso();
		$myLex->getSynoPref();
		break;

	case "FUSION" :
		$myLex->updateFromArray($_POST);
		//if (empty($myLex->t_lexique['ID_LEX'])) $myLex->create(true);
		//debug("FUSION :".$_POST['fusionLex'],'orange');
		$myLex->fusion($_POST['fusionLex']); // fusion : ce terme remplace l'ancien (note : fonction inverse / v1)
		$myLex->getLexique();
		$myLex->getDocs();
		$myLex->getVersions();
		$myLex->getSyno();
		$myLex->getAsso();
		$myLex->getChildren();
		$myLex->getSynoPref();
	break;

	case ""; // pas d'action, on préinitialise le type avec celui issu du get (cas de création d'une nv lex)
		$myLex->updateFromArray($_POST);
		$myLex->getVersions();
	break;
}


if (isset($_GET['id_lex']) && !empty($_GET['id_lex'])) { //Premier chargement de la page à partir de la liste.
	$myLex->t_lexique['ID_LEX']=$_REQUEST['id_lex'];
	$myLex->getLexique();
	$myLex->getVersions();
	$myLex->getDocs();
	$myLex->getSyno();
	$myLex->getAsso();
	$myLex->getChildren();
	$myLex->getSynoPref();
}

// $myPage->setReferrer(true,(isset($myLex->t_lexique['LEX_TERME']) && !empty($myLex->t_lexique['LEX_TERME'])?$myLex->t_lexique['LEX_TERME']:''));
$myPage->titre = (isset($myLex->t_lexique['LEX_TERME']) && !empty($myLex->t_lexique['LEX_TERME'])?$myLex->t_lexique['LEX_TERME']:'');
$myPage->setReferrer(true);

//debug($myLex,'gold',true);
echo "<div class='error'>".$myLex->error_msg."</div>";

include(getSiteFile("formDir","lexSaisie.inc.php"));

$myPage->renderReferrer();

?>



