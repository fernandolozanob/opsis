<?
 	//trace(print_r($_POST,true));
    require_once(modelDir.'model_etape.php');
	global $db;

	debug($_POST,'cyan');

	$myUser=User::getInstance();
	$myPage=Page::getInstance();
	$myPage->nomEntite = kEtape;
	$myEtape=New Etape();

    $action=$_POST["commande"];



// II. Traitement des actions
if(isset($_POST["bOk"]) || !empty($action))
{
    // II.1. Suppression enregistrement
    if(strcmp($action,"SUP")==0) { 
    	$myEtape->t_etape['ID_ETAPE']=$_REQUEST["id_etape"];
        $myEtape->delete();
        if(!empty($_POST["page"])){
              $myPage->redirect($_POST["page"]);
        }else $myPage->redirect($myPage->getName()."?urlaction=etapeListe");
    }
    // II.2. Sauvegarde enregistrement
    else if(isset($_POST["bOk"]) || $action=="SAVE" || $action=="DUP_ETAPE")
    {
		$myEtape->updateFromArray($_POST);
		//debug($myEtape,'pink');
        $saved=$myEtape->save();
        
        // II.2.C. Duplication de l'enregistrement dans la langue courante
        if (strcmp($action,"DUP_ETAPE")==0){
            $newEtape=$myEtape->duplicate();
            $myEtape=$newEtape;
        }

    }
} else $myEtape->t_etape['ID_ETAPE']=$_REQUEST["id_etape"];

$myEtape->getEtape();

debug($myEtape,'beige');

$myPage->titre =(isset($myEtape->t_etape['ETAPE_NOM']) && !empty($myEtape->t_etape['ETAPE_NOM'])?$myEtape->t_etape['ETAPE_NOM']:'');
$myPage->setReferrer(true);

require_once(getSiteFile("formDir",'etapeSaisie.inc.php'));
?>
