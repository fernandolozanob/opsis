<?php
	global $db;
	// Vérification des paramètres
	foreach($this->args as $key => $val) {
		
		switch ($key) { 
			case 'verb':
				break;

			case 'from':
				if (!isset($from)) {
					$from = $val;
				} else {
					$this->dropError('badArgument', $key, $val);
				}
				break;
				
			case 'until':
				if (!isset($until)) {
					$until = $val; 
				} else {
					$this->dropError('badArgument', $key, $val);
				}
				break;
				
			case 'set':
				if (!isset($set)) {
					$set = $val;
				} else {
					$this->dropError('badArgument', $key, $val);
				}
				break;      
				
			case 'metadataPrefix':
				if (!isset($metadataPrefix)) {
					if($val != "oai_dc"){
						$this->dropError ('cannotDisseminateFormat', $key, $val);
					} else $metadataPrefix=$val;
				} else {
					$this->dropError('badArgument', $key, $val);
				}
				break;
				
			case 'resumptionToken':
				if (!isset($resumptionToken)) {
					$resumptionToken = $val;
				} else {
					$this->dropError('badArgument', $key, $val);
				}
				break;
				
			default:
				$this->dropError('badArgument', $key, $val);
		}
	}
	// Création dossier token si non existant
	if(!is_dir(tokenDir)) mkdir(tokenDir);
	
	// VP 24/11/10 : initialisaiton entite
	$entite=(empty($set)?"doc":$set);
	
	// Resume previous session?
	if (isset($this->args['resumptionToken'])) {            
		if (count($this->args) > 3) {
			// overwrite all other errors
			$this->error_msg='';
			$this->dropError('exclusiveArgument');
		} else {
			$tokenFile=tokenDir."id-$resumptionToken";
			if (is_file($tokenFile)) {
				$fp = fopen($tokenFile, 'r');
				$filetext = fgets($fp, 4096);
				$textparts = explode('#', $filetext);
				$deliveredrecords = (int)$textparts[0];
				$sql = $textparts[1];
				$metadataPrefix = $textparts[2];
                $sql_delete = $textparts[3];
				fclose($fp); 
				unlink ($tokenFile);
			} else {
				$this->dropError('badResumptionToken', '', $resumptionToken);
			}
		}
	} else {
		
		// Paramètres obligatoires
		if (!isset($this->args['metadataPrefix'])) {
			$this->dropError('missingArgument', 'metadataPrefix');
		} 
		
		$deliveredrecords=0;

		// VP 10/02/11 : gestion OAI festival et personne
		switch($entite){
			case "doc":
				require_once(libDir."class_chercheDoc.php");
				$mySearch=new RechercheDoc();
				$mySearch->sqlRecherche="select t1.ID_DOC as ID,t1.ID_LANG,t1.DOC_DATE_MOD as DATE_MOD from t_doc t1 left join t_doc_val t2 on t1.ID_DOC = t2.ID_DOC where t1.DOC_ACCES='1'";
				if($from) $mySearch->sqlRecherche.=" and t1.DOC_DATE_MOD >=".$db->Quote($from);
				if($until) $mySearch->sqlRecherche.=" and t1.DOC_DATE_MOD <=".$db->Quote($until);
                if(isset($_SESSION['langue'])) $mySearch->sqlRecherche.=" and t1.ID_LANG =".$db->Quote($_SESSION['langue']);
				$mySearch->appliqueDroits();
				if( stripos( $mySearch->sqlRecherche, 'group by' ) === false ) {
					$mySearch->sqlRecherche .= " group by t1.ID_DOC,t1.ID_LANG ";
				}
				$sql =$mySearch->sqlRecherche." order by 1,2";

				unset($mySearch);
                
                $sql_delete="select ID_DOC as ID,ACT_DATE from t_action where ACT_TYPE='DEL' and ID_DOC!=0";
				if($from) $sql_delete.=" and ACT_DATE >=".$db->Quote($from);
				if($until) $sql_delete.=" and ACT_DATE <=".$db->Quote($until);
				break;
			case "fest":
				$sql="select ID_FEST as ID,ID_LANG,FEST_DATE_MOD as DATE_MOD from t_festival where FEST_ID_GEN=0";
				if($from) $sql.=" and FEST_DATE_MOD >=".$db->Quote($from);
				if($until) $sql.=" and FEST_DATE_MOD <=".$db->Quote($until);
				$sql.=" order by 1,2";
				break;
			case "pers":
				$sql="select ID_PERS as ID,ID_LANG,PERS_DATE_MOD as DATE_MOD from t_personne where PERS_STATUT=1";
				if($from) $sql.=" and PERS_DATE_MOD >=".$db->Quote($from);
				if($until) $sql.=" and PERS_DATE_MOD <=".$db->Quote($until);
                if(isset($_SESSION['langue'])) $sql.=" and ID_LANG =".$db->Quote($_SESSION['langue']);
				$sql.=" order by 1,2";
				break;
		}
	}
	if ($this->error_msg != '') return;
    trace($sql);
	
	// Lancement requête
	$rows=$db->GetAll($sql);
	$num_rows=count($rows);
    
    $num_delete=0;
    if(isset($sql_delete)){
        $rows_delete=$db->GetAll($sql_delete);
        $num_delete=count($rows_delete);
    }
       
	$maxCount=100;
    
	// Test nécessité d'un token
	if ($num_rows + $num_delete - $deliveredrecords > $maxCount) {
		$token = $this->get_token(); 
		$fp = fopen (tokenDir."id-$token", 'w');
		$thendeliveredrecords = (int)$deliveredrecords + $maxCount;
		fputs($fp, "$thendeliveredrecords#"); 
		fputs($fp, "$sql#"); 
		fputs($fp, "$metadataPrefix#");
        if(!empty($sql_delete)) fputs($fp, $sql_delete); 
		fclose($fp); 
		$restoken = 
		'  <resumptionToken expirationDate="'.$expirationdatetime.'"
		completeListSize="'.($num_rows + $num_delete).'"
		cursor="'.$deliveredrecords.'">'.$token."</resumptionToken>\n";
	}
	// Dernière livraison retour ResumptionToken vide
	else{
		$restoken = 
		'  <resumptionToken completeListSize="'.($num_rows + $num_delete).'"
		cursor="'.$deliveredrecords.'"></resumptionToken>'."\n";
	}
	
	$maxrec = min($num_rows , $deliveredrecords+$maxCount);

    $nbrec=0;
	if($num_rows==0 && $num_delete==0) $this->dropError('noRecordsMatch');
	elseif($this->args['verb']=="ListIdentifiers"){
		$output = " <ListIdentifiers>\n";
        // Enregistrements actifs
        if($num_rows>$deliveredrecords){
            for($i=$deliveredrecords;$i<$maxrec;$i++){
                $nbrec++;
                $row=$rows[$i];
                $identifier="oai:".gRepositoryId.":".$entite."-".$row['ID'];
                if($row['ID_LANG']) $identifier.="-".strtolower($row['ID_LANG']);
                $output .="
                <header>  
                <identifier>".$identifier."</identifier>
                <datestamp>".date("c",strtotime($row['DATE_MOD']))."</datestamp>
                <setSpec>".$entite."</setSpec>
                </header>";
            }
        }
        // Enregistrements supprimés
        if($nbrec<$maxCount && $num_delete>0){
            $minrec=$deliveredrecords+$nbrec-$num_rows;
            $maxrec = min($num_delete, $deliveredrecords+$maxCount-$num_rows);
            for($i=$minrec;$i<$maxrec;$i++){
                $nbrec++;
                $row=$rows_delete[$i];
                $identifier="oai:".gRepositoryId.":".$entite."-".$row['ID']."-".strtolower($_SESSION['langue']);
                $output .="
                <header status=\"deleted\">  
                <identifier>".$identifier."</identifier>
                <datestamp>".date("c",strtotime($row['ACT_DATE']))."</datestamp>
                <setSpec>".$entite."</setSpec>
                </header>";
            }
        }
		$output .= $restoken." </ListIdentifiers>\n";
		echo $output;
	}
	elseif($this->args['verb']=="ListRecords"){
		$output = " <ListRecords>\n";
        // S'il reste des enregistrements
        if($num_rows>$deliveredrecords){
            for($i=$deliveredrecords;$i<$maxrec;$i++){
                $nbrec++;
                $row=$rows[$i];
                $identifier="oai:".gRepositoryId.":".$entite."-".$row['ID'];
                if(isset($row['ID_LANG'])) $identifier.="-".strtolower($row['ID_LANG']);
                $output .=$this->getRecord($identifier,$metadataPrefix);
            }
        }
        //trace("deliveredrecords=$deliveredrecords nbrec=$nbrec maxCount=$maxCount num_delete=$num_delete");
        // Enregistrements supprimés
        if($nbrec<$maxCount && $num_delete>0){
            $minrec=$deliveredrecords+$nbrec-$num_rows;
            $maxrec = min($num_delete, $deliveredrecords+$maxCount-$num_rows);
            for($i=$minrec;$i<$maxrec;$i++){
                $nbrec++;
                $row=$rows_delete[$i];
                $identifier="oai:".gRepositoryId.":".$entite."-".$row['ID']."-".strtolower($_SESSION['langue']);
                $output .="<record>
                <header status=\"deleted\">  
                <identifier>".$identifier."</identifier>
                <datestamp>".date("c",strtotime($row['ACT_DATE']))."</datestamp>
                <setSpec>".$entite."</setSpec>
                </header>
                </record>\n";
            }
        }
		$output .= $restoken." </ListRecords>\n";
		echo $output;
	}
	

?>
