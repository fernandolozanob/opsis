<?php

	// Vérification des paramètres
	foreach($this->args as $key => $val) {
		
		switch ($key) { 
			case 'verb':
				break;
			case 'identifier':
				$identifier=$val;
				break;
				
			case 'metadataPrefix':
				if($val != "oai_dc"){
					$this->dropError ('cannotDisseminateFormat', $key, $val);
				}
				else $metadataPrefix=$val;
				break;
				
			default:
				$this->dropError ('badArgument', $key, $val);
	}
}
	// Paramètres obligatoires
	if (!isset($this->args['identifier'])) {
		$this->dropError('missingArgument', 'identifier');
	}
	if (!isset($this->args['metadataPrefix'])) {
		$this->dropError('missingArgument', 'metadataPrefix');
	} 
	
	$content=$this->getRecord($identifier,$metadataPrefix);
	
	if (empty($this->error_msg)) echo "  
	<GetRecord>
	".$content."
	</GetRecord>\n";

?>
