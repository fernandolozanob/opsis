<?php
	// Vérification des paramètres
	foreach($this->args as $key => $val) {
		
		switch ($key) { 
			case 'verb':
				break;
				
			default:
				$this->dropError ('badArgument', $key, $val);
		}
	}
	
	if (!empty($this->error_msg)) return;

echo "<ListMetadataFormats>
<metadataFormat>
<metadataPrefix>oai_dc</metadataPrefix>
<schema>http://www.openarchives.org/OAI/2.0/oai_dc.xsd</schema>
<metadataNamespace>http://www.openarchives.org/OAI/2.0/oai_dc/</metadataNamespace>
</metadataFormat>
</ListMetadataFormats>";

?>
