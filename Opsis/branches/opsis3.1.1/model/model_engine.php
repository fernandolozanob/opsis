<?php
require_once(modelDir.'model.php');

class Engine extends Model
{
	private $id_engine;
	private $id_module;
	private $nom_serveur;
	private $ip_serveur;
	private $nb_max_jobs;
	private $nom_module;
	
	function __construct($id_engine=null)
	{
		parent::__construct('t_engine',$id_engine,'');
		$this->id_engine=0;
		$this->id_module=0;
		$this->nom_serveur='';
		$this->ip_serveur='localhost';
		$this->nb_max_jobs=0;
		if ($id_engine!=null)
			$this->getEngineFromId($id_engine);
	}
	
	// MS - TODO refonte getData (eventuellement, semble pas forcément necessaire ici
	public function getEngineFromId($id_engine)
	{
		global $db;
		
		$id_engine=intval($id_engine);
		
		$result=$db->Execute('SELECT * FROM t_engine WHERE ID_ENGINE='.$id_engine.'')->getRows();
		
		$this->id_engine=intval($result[0]['ID_ENGINE']);
		$this->id_module=intval($result[0]['ENG_ID_MODULE']);
		$this->nom_serveur=$result[0]['ENG_NOM'];
		$this->ip_serveur=$result[0]['ENG_IP'];
		$this->nb_max_jobs=intval($result[0]['ENG_MAX_JOBS']);
	}
	
	public function getEngineFromModule($module)
	{
		global $db;
		
		$id_engine=intval($id_engine);
		
		if(is_int($module)){
			$result=$db->Execute('SELECT * FROM t_engine WHERE ENG_ID_MODULE='.intval($module).'')->getRows();
		}else if( is_string($module)){
			$result=$db->Execute('SELECT * FROM t_engine LEFT JOIN t_module ON ENG_ID_MODULE=ID_MODULE WHERE MODULE_NOM='.$db->Quote($module).'')->getRows();
		}	
		
		$this->id_engine=intval($result[0]['ID_ENGINE']);
		$this->id_module=intval($result[0]['ENG_ID_MODULE']);
		$this->nom_serveur=$result[0]['ENG_NOM'];
		$this->ip_serveur=$result[0]['ENG_IP'];
		$this->nb_max_jobs=intval($result[0]['ENG_MAX_JOBS']);
	}
	
	public function getIdEngine()
	{
		return $this->id_engine;
	}
	
	public function getIdModule()
	{
		return $this->id_module;
	}
	
	public function getModuleName()
	{
		if (!isset($this->nom_module) || empty($this->nom_module))
		{
			global $db;
			
			$result=$db->Execute('SELECT MODULE_NOM FROM t_module WHERE ID_MODULE='.$this->id_module.'')->getRows();
			$this->nom_module=$result[0]['MODULE_NOM'];
		}
		
		return $this->nom_module;
	}
	
	public function getNomServeur()
	{
		return $this->nom_serveur;
	}
	
	public function getIpAdress()
	{
		return $this->ip_serveur;
	}
	
	public function getNbMaxJobs()
	{
		return $this->nb_max_jobs;
	}
	
	public static function getAllModuleEngines($module)
	{
		global $db;
		$engines=array();
		if(is_numeric($module)){
			$result=$db->Execute('SELECT ID_ENGINE FROM t_engine WHERE ENG_ID_MODULE='.intval($module).'')->getRows();
		}else if( is_string($module)){
			$result=$db->Execute('SELECT ID_ENGINE FROM t_engine LEFT JOIN t_module ON ENG_ID_MODULE=ID_MODULE WHERE MODULE_NOM='.$db->Quote($module).'')->getRows();
		}
		
		foreach ($result as $res)
		{
			$eng=new Engine($res['ID_ENGINE']);
			$engines[]=$eng;
		}
		
		return $engines;
	}
}


?>