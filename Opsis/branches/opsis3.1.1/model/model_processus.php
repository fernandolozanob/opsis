<?
//************************************************** PROCESSUS ***************************************************************

// VP : 27/11/2007 : Création du fichier, définition de la classe Processus


//************************************************** PROCESSUS ***************************************************************
require_once(modelDir.'model.php');
class Processus extends Model
{

	var $t_proc;
	var $t_proc_etape;
	var $error_msg;


	function __construct($id=null,$version='') {
		parent::__construct('t_proc',$id,$version);
	}


	function getProc($arrProc=array()){
		return $this->getData($arrProc); 
	}


	function getProcEtape() {
		require_once(modelDir.'model_etape.php');
		global $db;
		$sql="SELECT * from t_proc_etape WHERE ID_PROC=".intval($this->t_proc['ID_PROC'])." order by PE_ORDRE";

		$this->t_proc_etape=$db->GetAll($sql);
		foreach ($this->t_proc_etape as $idx=>&$etp) {
			$objEtp=new Etape;
			if(isset($this->flag_runSyncJob) && !empty($this->flag_runSyncJob) && $this->flag_runSyncJob){
				$objEtp->flag_runSyncJob = $this->flag_runSyncJob;
			}
			$objEtp->t_etape['ID_ETAPE']=$etp['ID_ETAPE'];
			$objEtp->getEtape();
			$etp['ETAPE']=$objEtp;
			unset($objEtp);
		}
	}


	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {

	 	if (empty($this->t_proc)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_proc[strtoupper($fld)])) $this->t_proc[strtoupper($fld)]=$val;
		}

	}

	function checkExist() {
		global $db;
		return $db->GetOne("SELECT ID_PROC from t_proc
							WHERE LOWER(TRIM(PROC_NOM))=".$db->Quote(trim(strtolower($this->t_proc['PROC_NOM'])))
							.(!empty($this->t_proc['ID_PROC'])?" AND ID_PROC<>".intval($this->t_proc['ID_PROC']):"") );
	}

    function duplicate(){
    global $db;

		if (empty($this->t_proc_etape)) $this->getProcEtapes(); // Récup des infos si pas encore loadées


        $dupProc=new Processus();
        $dupProc=$this; // Recopie des infos
        unset($dupProc->t_proc['ID_PROC']); // pour éviter les effets de bord
        $ok=$dupProc->create(false) && $dupProc->saveEtapes(); // Création avec désactivation du checkExist.
        if ($ok) $dupProc->dropError(kSuccesProcessusDupliquer);
        return $dupProc;
    }


	function save() {
		global $db;
		if (empty($this->t_proc['ID_PROC'])) {return $this->create();}
		if ($this->checkExist()) {$this->dropError(kErrorProcessusExisteDeja);return false;}

		$ok=$db->Execute('UPDATE t_proc SET PROC_NOM='.$db->Quote($this->t_proc['PROC_NOM']).' WHERE ID_PROC='.intval($this->t_proc['ID_PROC']));

		if (!$ok) {$this->dropError(kErrorProcessusSauve);return false;}
		return true;
	}

	function create($chkDoublons=true) {
		global $db;
		if (!empty($this->t_proc['ID_PROC'])) {return $this->save();}
		if ($chkDoublons && $this->checkExist()) {$this->dropError(kErrorProcessusExisteDeja);return false;}

		$ok=$db->Execute('INSERT INTO t_proc (ID_PROC,PROC_NOM) values ('.intval($this->t_proc['ID_PROC']).','.$db->Quote($this->t_proc['PROC_NOM']).')');

		if (!$ok) {$this->dropError(kErrorProcessusCreation);return false;}
		$this->t_proc['ID_PROC']=$db->getOne('SELECT max(ID_PROC) FROM t_proc WHERE PROC_NOM='.$db->Quote($this->t_proc['PROC_NOM']));
		return true;
	}


	function saveEtapes() {
		global $db;

		if (empty($this->t_proc['ID_PROC'])) {$this->dropError(kErrorProcessusNoID);return false;}
		$db->StartTrans();
		$db->Execute('DELETE from t_proc_etape WHERE ID_PROC='.intval($this->t_proc['ID_PROC']));

		foreach ($this->t_proc_etape as $pe) {
			$db->Execute('INSERT INTO t_proc_etape (ID_PROC,ID_ETAPE,PE_ORDRE,PE_ID_ETAPE_PREC)
			 values ('.intval($this->t_proc['ID_PROC']).','.intval($pe['ID_ETAPE']).','.intval($pe['PE_ORDRE']).','.intval($pe['PE_ID_ETAPE_PREC']).')');
		}
		$ok=$db->CompleteTrans();

		if (!$ok) {$this->dropError(kErrorProcessusSauveEtapes);return false;}
		return true;

	}

	function delete() {
		// VP 6/05/10 : suppression par deleteSQLFromArray
		deleteSQLFromArray(array("t_etape","t_proc_etape"),"ID_PROC",$this->t_proc['ID_PROC']);
		/*
		global $db;
		$db->StartTrans();
		$db->Execute("DELETE from t_proc_etape WHERE ID_PROC=".$db->Quote($this->t_proc['ID_PROC']));
		$db->Execute("DELETE from t_proc WHERE ID_PROC=".$db->Quote($this->t_proc['ID_PROC']));
		$ok=$db->CompleteTrans();
		if (!$ok) {$this->dropError(kErrorProcessusSuppr);return false;}
		 */
		return true;
	}

	//by ld => dynamic_args, array de valeurs insérées dynamiquement dans le nom du fichier out
	// VP 16/10/08 : ajout paramètre $id_superJob pour créer un job parent
	// VP 23/04/10 : ajout paramètre $bypass pour sauter des étapes
    // VP 10/05/12 : ajout paramètre plateforme pour gérer plusieurs frontaux
    // VP 5/06/14 : ajout paramètre $id_job_prec pour permettre au premier job lancé d'avoir un prédecesseur
	function createJobs($id_mat,$mat_nom,$param="",&$id_job,$dynamic_args=null,$id_superJob=0,$id_job_prec=0,$bypass=array(),$priority=2, $plateforme="",$job_date_lancement='') {
		global $db;
		require_once(modelDir.'model_etape.php');
		require_once(modelDir.'model_job.php');
		// trace("fonction createjobs"); 
		// trace(print_r($param,true));
		// Création Job pour le processus
		$jobObj=new Job;
		if(!empty($this->flag_runSyncJob) && $this->flag_runSyncJob){
			$jobObj->flag_runSyncJob = $this->flag_runSyncJob;
			if(!preg_match('/<run_sync_job>true<\/run_sync_job>/',$param)){
				//trace("Processus : add run_sync_job");
				if(empty($param)){
					$param = "<param><run_sync_job>true</run_sync_job></param>" ;
				}else{
					$param = str_replace("</param>",'<run_sync_job>true</run_sync_job></param>',$param) ;
				}
			}
		}
		$jobObj->t_job["JOB_ID_PROC"]=$this->t_proc["ID_PROC"];
		$jobObj->t_job["JOB_NOM"]=$this->t_proc["PROC_NOM"]." ".$mat_nom;
		$jobObj->t_job["JOB_IN"]=$mat_nom;
		$jobObj->t_job["JOB_ID_MAT"]=$id_mat;
		$jobObj->t_job["JOB_PARAM"]=$param;
		$jobObj->t_job["ID_JOB_GEN"]=intval($id_superJob);
        $jobObj->t_job["ID_JOB_PREC"]=intval($id_job_prec);
		$jobObj->t_job["JOB_DATE_CREA"]=date("Y-m-d H:i:s");
		$jobObj->t_job["JOB_DATE_DEBUT"]=$jobObj->t_job["JOB_DATE_CREA"];
		$jobObj->t_job["JOB_ID_SESSION"]=session_id();
		$jobObj->t_job["JOB_PRIORITE"]=$priority;
        if(defined("kJobPlateforme")){
            $job_plateforme=(empty($plateforme)?kJobPlateforme:$plateforme);
        }else $job_plateforme="";
        $jobObj->t_job["JOB_PLATEFORME"]=$job_plateforme;
		
		if (isset($job_date_lancement) && !empty($job_date_lancement))
			$jobObj->t_job["JOB_DATE_LANCEMENT"]=$job_date_lancement;
		
		$jobObj->create();
		$id_job=$jobObj->t_job["ID_JOB"];
        // Job précédent le processus
        if(!empty($id_job_prec)){
            $jobPrecObj=new Job;
            $jobPrecObj->t_job["ID_JOB"]=$id_job_prec;
            $jobPrecObj->getJob();
        }else{
            $jobPrecObj=null;
        }
		// Création Jobs pour les étapes
		$id_job_valid=0;
		foreach ($this->t_proc_etape as $idx=>$etp){
			// VP 23/04/10 : traitement étapes à sauter
			if(in_array($etp["ID_ETAPE"],$bypass)) continue;
			if(in_array($etp["PE_ID_ETAPE_PREC"],$bypass)) $etp["PE_ID_ETAPE_PREC"]="0";
			
			if($etp["PE_ID_ETAPE_PREC"]!="0"){
				$mat_nom_etp=$job[$etp["PE_ID_ETAPE_PREC"]]->t_job["JOB_OUT"];
				$etp_id_job_prec=$job[$etp["PE_ID_ETAPE_PREC"]]->t_job["ID_JOB"];
				$tmp_id_mat='';
            } elseif(!empty($jobPrecObj)){
                $etp_id_job_prec=$jobPrecObj->t_job["ID_JOB"];
                $mat_nom_etp=$jobPrecObj->t_job["JOB_OUT"];
                $tmp_id_mat='';
			} else{
				$etape_in = $db->getOne("SELECT etape_in FROM t_etape WHERE id_etape =".intval($etp["ID_ETAPE"]));
				if (defined("gListePatternVisio")) $baseName = str_replace(unserialize(gListePatternVisio), "", stripExtension($mat_nom));
				else $baseName = $baseName = str_replace("_vis", "", stripExtension($mat_nom));
				$new_id_mat = $db->getOne("SELECT id_mat FROM t_mat WHERE mat_nom like ".$db->Quote($baseName.$etape_in));
				
				if (!empty($etape_in) && strlen($etape_in) > 1 && $new_id_mat) {
					$mat_nom_etp=$baseName.$etape_in;
					$etp_id_job_prec="0";
					$tmp_id_mat=$new_id_mat;
				} else {
					$mat_nom_etp=$mat_nom;
					$etp_id_job_prec="0";
					$tmp_id_mat=$id_mat;
				}
			}
			
			
			// VP (9/10/08) : ajout paramètre gProcParamFirstOnly
			if ($idx>0) {
				$newparam=''; // Limitation de la propagation des paramètres entre les étapes d'un processus, 
				// Pour l'INA on ne doit PAS passer les TC_IN / TC_OUT d'une étape à l'autre,
				// possibilité de propager d'autres paramètres si necessaire : 
				// Dans le cas des paniers, on propage quand même certaines infos : ID_LIGNE_PANIER et FOLDER
				// VP 21/11/08 : correction bug transmission FOLDER
				// VP 5/10/10 : changement FOLDER en FOLDER_OUT
                // VP 31/05/12 : propagation ftp_folder
				// MS 10/07/12 : ajout propagation ID_DOC => nécessaire pour plusieurs diffusion (récup data via bdd et interpretation xsl)
				// XB 03/03/15 : ajout champs génrique inherit_VALEUR
				// PC 13/07/15 : ajout propagation paramêtres diffusion
				// MS 26/02/18 : fix des champs INHERIT_(nom tag), ceux ci seront recopiés dans tous les fils .. à surveiller & adapter en fonction des besoins
				if(preg_match("|<ID_LIGNE_PANIER>(.*)</ID_LIGNE_PANIER+>|i",$param,$matches)) $newparam.="<ID_LIGNE_PANIER>".$matches[1]."</ID_LIGNE_PANIER>";
				if(preg_match("|<ID_PANIER>(.*)</ID_PANIER+>|i",$param,$matches)) $newparam.="<ID_PANIER>".$matches[1]."</ID_PANIER>";
				if(preg_match("|<FOLDER_OUT>(.*)</FOLDER_OUT+>|i",$param,$matches)) $newparam.="<FOLDER_OUT>".$matches[1]."</FOLDER_OUT>";
                if(preg_match("|<ftp_folder>(.*)</ftp_folder+>|i",$param,$matches)) $newparam.="<ftp_folder>".$matches[1]."</ftp_folder>";
                if(preg_match("|<entity>(.*)</entity+>|i",$param,$matches)) $newparam.="<entity>".$matches[1]."</entity>";
                if(preg_match("|<ID_DOC>(.*)</ID_DOC+>|i",$param,$matches)) $newparam.="<ID_DOC>".$matches[1]."</ID_DOC>";
                if(preg_match("|<ID_LANG>(.*)</ID_LANG+>|i",$param,$matches)) $newparam.="<ID_LANG>".$matches[1]."</ID_LANG>";
                if(preg_match_all("|<INHERIT_(.*?)>(.*?)</INHERIT_(.*?)>|i",$param,$matches)){
					if(!empty($matches) && !empty($matches[0]) && !empty($matches[0][0])){
						$newparam.=implode('',$matches[0]);
					}
					//trace("inherit_newparam : ".print_r($newparam,1));
				}
                if(preg_match("|<video_star>(.*)</video_star+>|i",$param,$matches)) $newparam.="<video_star>".$matches[1]."</video_star>";
				if($newparam!='') $param="<param>".$newparam."</param>";
				else $param="";
				
				$dynamic_args = null ; 
			}
			// VP 27/11/09 : ajout paramètre $id_job_valid initialisé à 0 au départ
            // VP 27/06/12 : ajout paramètre job_plateforme
			$etp["ETAPE"]->createJob($tmp_id_mat,$mat_nom_etp,$id_job,$etp_id_job_prec,$id_job_valid,$etpJobObj,$param,$dynamic_args,$priority,$job_plateforme,$job_date_lancement); //'' remplace par $param
			$job[$etp["ID_ETAPE"]]=$etpJobObj;
                         
		}
		// VP 20/10/09 : mise à jour "JOB_OUT"
		if($etpJobObj) {
			$jobObj->t_job["JOB_OUT"]=$etpJobObj->t_job["JOB_OUT"];
			$jobObj->save();
		}
		return true;
	}



 	/** Export XML de l'objet usager
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {


        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_proc>\n";
		// USAGER
		foreach ($this->t_proc as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}


       if (!empty($this->t_proc_etape)) {
        // I.4. Propri�t�s relatives � t_doc_mat
        $content.="\t<t_proc_etape nb=\"".count($this->t_proc_etape)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_proc_etape as $name => $value) {
                $content .= "\t\t<PROC_ETAPE ID_ETAPE=\"".$this->t_proc_etape[$name]["ID_ETAPE"]."\">\n";
                // Pour chaque valeur
                foreach ($this->t_proc_etape[$name] as $val_name => $val_value) {
                    if (is_object($val_value)) {$content.=$val_value->xml_export();}
                    else {
                    	$content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                    }
                }
                $content .= "\t\t</PROC_ETAPE>\n";
            }
        $content.="\t</t_proc_etape>\n";
       }

		$content.=$indent."</t_proc>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
}

?>