<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- D�veloppeurs : Vincent Prost, Fran�ois Duran, J�rome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

// XS : 28/09/2005 : Cr�ation du fichier, d�finition de la classe FONDS

//************************************************** FONDS ***************************************************************
require_once(modelDir."/model.php");

class Fonds extends Model
{
	var $t_fonds;
	var $t_groupe_fonds;
	var $error_msg;
	const delimiter_genealogie = '.';

	function __construct($id= null ,$version=""){
		parent::__construct('t_fonds',$id,$version);
	}
	
	function getFonds($arrFnd=array(),$version="") {
		return $this->getData($arrFnd,$version);
	}
	
	//Remplit l'objet fonds à partir de la BDD, sans calcul
	function getData($arrData=array(),$version="") {
		global $db;
		$arrFonds = parent::getData($arrData,$version) ; 
		if (empty($arrFonds) && !empty($this->t_fonds) && !$arrData) {  // Un seul résultat : on affecte le résultat à la valeur en cours
			if (!empty($this->t_fonds['ID_FONDS_GEN'])) {
				$this->t_fonds['FONDS_GEN']=$db->GetOne("SELECT FONDS FROM t_fonds WHERE ID_FONDS=".intval($this->t_fonds['ID_FONDS_GEN'])." AND ID_LANG=".$db->Quote($this->t_fonds['ID_LANG']));
			}
		} else {
			foreach ($arrFonds as $idx=>$Fnd) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				if (!empty($arrFonds[$idx]->t_fonds['ID_FONDS_GEN'])) {
					$arrFonds[$idx]->t_fonds['FONDS_GEN']=$db->GetOne("SELECT FONDS FROM t_fonds WHERE ID_FONDS=".intval($arrFonds[$idx]->t_fonds['ID_FONDS_GEN'])." AND ID_LANG=".$db->Quote($arrFonds[$idx]->t_fonds['ID_LANG']));
				}
			}
			unset($this); // plus besoin
			return $arrFonds;
		}
	}


	function getChildren($id="") {
		global $db;
        
		if(!empty($this->t_fonds["ID_FONDS"]))
			$id = $this->t_fonds["ID_FONDS"];
		else
			$this->t_fonds["ID_FONDS"] = $id;
        
		if (empty($this->t_fonds['ID_LANG'])) $idLang=$_SESSION['langue'];
		else $idLang=$this->t_fonds['ID_LANG'];
        
		$sql = "SELECT * FROM t_fonds WHERE ID_FONDS_GEN=".intval($id)." AND ID_LANG=".$db->Quote($idLang)." ORDER BY fonds";
		$res = $db->GetAll($sql);
        
		foreach ($res as $i => $lFonds) {
			$fondsTemp = new Fonds();
			$fondsTemp->updateFromArray($lFonds);
            
			$this->arrChildren[$i] = $fondsTemp;
			unset($fondsTemp);
		}
	}

	/** Export XML de l'objet usager
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {


        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');


		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_fonds>\n";
		// Fonds
		foreach ($this->t_fonds as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}

		if(!empty($this->arrChildren)) {
	 		$content .= "\t<t_fonds_children nb=\"".count($this->t_groupe_fonds)."\">\n";
		 	foreach ($this->arrChildren as $oFondsChild)
            $content.=$oFondsChild->xml_export(0,$encodage,"\t\t");
            $content.= "\t</t_fonds_children>\n";
		}

       if (!empty($this->t_groupe_fonds)) {
        // t_groupe_fonds
        $content.="\t<t_groupe_fonds nb=\"".count($this->t_groupe_fonds)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_groupe_fonds as $name => $value) {
                $content .= "\t\t<GROUPE_FONDS ID_FONDS=\"".$this->t_groupe_fonds[$name]["ID_FONDS"]."\">\n";
                // Pour chaque valeur
                foreach ($this->t_groupe_fonds[$name] as $val_name => $val_value) {
                    if (is_object($val_value)) {$content.=$val_value->xml_export();}
                    else {
                    	$content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                    }
                }
                $content .= "\t\t</GROUPE_FONDS>\n";
            }
        $content.="\t</t_groupe_fonds>\n";
       }


		$content.=$indent."</t_fonds>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}

	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){
		global $db;
		
		if($mode == null){
			$mode=array();
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		$limit="";
		if ($nb!="all"){
			$limit= $db->limit($nb_offset,$nb);
		}
	
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		$xml_file_path = kServerTempDir."/export_opsis_fonds_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
	
		$sql_fon = "SELECT f.*, fp.FONDS as PARENT from t_fonds f
		LEFT JOIN t_fonds fp ON fp.ID_FONDS = f.ID_FONDS_GEN AND fp.ID_LANG = f.ID_LANG ".$critere.$limit;
		
		$results = $db->Execute($sql_fon);
		$ids = implode(',',$db->GetCol($sql_fon));
		if($ids == ''){
			fwrite($xml_file,"</EXPORT_OPSIS>\n");
			return false;
		}
		
		foreach ($results as $usr){
			$xml.="\t<t_fonds>";
			foreach($usr as $fld=>$val){
					 $xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
				}
			$xml.="\t</t_fonds>";
			fwrite($xml_file,$xml);
			$xml = "";
		}
		
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
		flock($xml_file, LOCK_UN);
		fclose($xml_file);
		return $xml_file_path;
	}


	function getGroupeFonds() {
		require_once(modelDir.'model_groupe.php');
		global $db;
		$sql="SELECT * from t_groupe_fonds WHERE ID_FONDS=".intval($this->t_fonds['ID_FONDS']);

		$this->t_groupe_fonds=$db->GetAll($sql);

		foreach ($this->t_groupe_fonds as $idx=>&$fnd) {
			$arrId[]=$fnd['ID_GROUPE'];
			$fnd['PRIV']=$db->GetOne("SELECT PRIV from t_privilege WHERE ID_PRIV=".intval($fnd['ID_PRIV'])." AND ID_LANG=".$db->Quote($_SESSION['langue']));
		}

		$dumb=new Groupe;
		$arrFnd=$dumb->getGroupe(isset($arrId)?$arrId:null);
		unset($dumb);
		
		if ($arrFnd)
			foreach ($arrFnd as $idx=>$grpObj) $this->t_groupe_fonds[$idx]['GROUPE']=$grpObj;

	}


	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {

	 	if (empty($this->t_fonds)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_fonds[strtoupper($fld)])) $this->t_fonds[strtoupper($fld)]=$val;
		}
		if (empty($this->t_fonds['ID_LANG'])) $this->t_fonds['ID_LANG']=$_SESSION['langue'];

	}

	//@update VG 31/05/2010
	//@update VG 09/06/2010 : FONDS prioritaire pour les calculs, et donc pour les modifs de fonds
	//Remplit l'objet fonds avec les données de l'objet ou passées en paramètres et les données calculables
	function setFonds($tab_valeurs = array(),$withFondColSaisie=false) {
		global $db;
		$this->updateFromArray($tab_valeurs);

		if ($this->t_fonds['ID_FONDS_GEN'] == '') $idFondsGen = 0;
		else $idFondsGen = $this->t_fonds['ID_FONDS_GEN'];

		//Si on a un FONDS, on recalcule FONDS_COL. FONDS est prioritaire sur FONDS_COL
		if(!empty($this->t_fonds['FONDS'])) {
			//ID_FONDS_GEN à 0 : ne possède pas de fonds parent => fonds_col = fonds
			if($idFondsGen == '0') {
				$this->t_fonds['FONDS_COL'] = $this->t_fonds['FONDS'];
				$this->t_fonds['FONDS_GEN'] = '';

			//Possède un fonds parent : fonds_col calculé à partir de fonds et fonds_gen (fonds parent)
			}else {
				//On n'a que l'id du parent : on récupère alors son nom
				$this->t_fonds['FONDS_GEN']=(empty($this->t_fonds['ID_FONDS_GEN']))?'':$db->GetOne("SELECT FONDS FROM t_fonds WHERE ID_FONDS=".intval($idFondsGen)." AND ID_LANG=".$db->Quote($this->t_fonds['ID_LANG']));

				if(!empty($this->t_fonds['FONDS_GEN'])) {
					if($withFondColSaisie == false){
						$this->t_fonds['FONDS_COL'] = $this->getFondsPath();
					}
				}
			}
		}
		//Si on n'a pas de fonds et que FONDS_COL est connu : on extrait les données de fonds_col
		elseif (!empty($this->t_fonds['FONDS_COL'])) {
			$aFondsCol = explode(self::delimiter_genealogie, $this->t_fonds['FONDS_COL']);
			//Dans le cas où on a bien 2 parties : fonds_parent et fonds
			if(count($aFondsCol) > 1) {
				$this->t_fonds['FONDS'] = empty($aFondsCol[count($aFondsCol)-2])?$aFondsCol[count($aFondsCol)-1]:$aFondsCol[count($aFondsCol)-2];
				$this->t_fonds['FONDS_GEN'] = empty($aFondsCol[count($aFondsCol)-1])?'':$aFondsCol[count($aFondsCol)-1];

			//Sinon c'est un fonds sans parents, de premier niveau
			} else {
				$this->t_fonds['FONDS'] = $this->t_fonds['FONDS_COL'];
				$this->t_fonds['FONDS_GEN'] = '';
				$this->t_fonds['ID_FONDS_GEN'] = '0';
			}
		}

	}

	function checkExist() {
		global $db;
							
		return $db->GetOne("SELECT id_fonds from t_fonds
							WHERE 1=1"
							.(!empty($this->t_fonds['FONDS'])?" AND LOWER(TRIM(FONDS))=".$db->Quote(trim(strtolower($this->t_fonds['FONDS']))):"")
							.(!empty($this->t_fonds['ID_FONDS'])?" AND ID_FONDS<>".intval($this->t_fonds['ID_FONDS']):"")
							//@update VG 28/05/2010
							.(!empty($this->t_fonds['FONDS_COL'])?" AND LOWER(TRIM(FONDS_COL))=".$db->Quote(trim(strtolower($this->t_fonds['FONDS_COL']))):"")
							//XB
							.(!empty($this->t_fonds['ID_LANG'])?" AND LOWER(TRIM(ID_LANG))=".$db->Quote(trim(strtolower($this->t_fonds['ID_LANG']))):"")  
							 );
	}


	function save($withFondColSaisie=false) {
		global $db;
		$this->setFonds($tab_valeurs = array(),$withFondColSaisie);//Permet de setter les champs calculables
		if (empty($this->t_fonds['ID_FONDS'])) {return $this->create();}
		if ($this->checkExist()) {$this->dropError(kErrorFondsExisteDeja);return false;}

		$this->createFondsParentIfNotExist();
		$rs = $db->Execute("SELECT * FROM t_fonds WHERE ID_FONDS=".intval($this->t_fonds['ID_FONDS'])." AND ID_LANG=".$db->Quote($this->t_fonds['ID_LANG']));
		$sql = $db->GetUpdateSQL($rs, $this->t_fonds);

		if (!empty($sql)) $ok = $db->Execute($sql);
				
		if (!$ok && !empty($sql)) {$this->dropError(kErrorFondsSauve);return false;}
		return true;
	}


	function create($createParent = false) {
		global $db;
		if ($this->checkExist()) {$this->dropError(kErrorFondsExisteDeja);return false;}

		//@update VG 31/05/2010
		if ($createParent && !empty($this->t_fonds['FONDS_GEN'])) {
			$ok = $this->createFondsParentIfNotExist();
			if(!$ok) return false;
		}

		if (isset($this->t_fonds['ID_FONDS']) && intval($this->t_fonds['ID_FONDS']) == 0){
			unset($this->t_fonds['ID_FONDS']);
		}

		$ok =$db->insertBase("t_fonds","id_fonds",$this->t_fonds);
		if (!$ok) {$this->dropError(kErrorFondsCreation);return false;}
		$this->t_fonds['ID_FONDS']=$ok;
		return true;
	}


	/**
	 * @update VG 01/06/2010
	 * @update VG 09/06/2010 : ajout du paramètre $createFondsParent
	 * @update VG 15/06/2010 : ajout de paramètres
	 * Créé une nouvelle occurence si la valeur n'existe pas, avec ses versions, et retourne l'idVal
	 */
	function createFromArray($tab_valeurs, $create = true, $update = true, $saveVersion = true, $createFondsParent = true) {
		$this->updateFromArray($tab_valeurs);
		//Si la valeur existe déjà, on retourne juste son id
		$exist = $this->checkExist();
		if ($exist) {
			$this->t_fonds['ID_FONDS']=$exist;
			return $exist;
		}

		//Sinon on la créé ou on la sauvegarde
		$existId = $this->checkExistId();
		if($existId) {
			if(!$update) return false;
			$ok = $this->save();
		} else {
			if (!$create) return false;
			$this->setFonds();
			$ok = $this->create($createFondsParent);
		}
		if(!$ok) return false;

		//On duplique alors aussi pours les versions en langues étrangères
		if($saveVersion) {
			$aLang = getOtherLanguages();
			$fondsVersions = array();
			foreach ($aLang as $lang) {
				$fondsVersions[$lang] = array(
											'FONDS' =>	$this->t_fonds['FONDS'],
											'FONDS_COL'	=>	$this->t_fonds['FONDS_COL'],
										);
			}
			$ok = $this->saveVersions($fondsVersions);
		}

		if ($ok) return $this->t_fonds['ID_FONDS'];
		else return false;
	}


	function saveGroupe() {
		global $db;

		if (empty($this->t_fonds['ID_FONDS'])) {$this->dropError(kErrorFondsNoID);return false;}
		$db->StartTrans();
		$db->Execute('DELETE from t_groupe_fonds WHERE ID_FONDS='.intval($this->t_fonds['ID_FONDS']));
		if(!empty($this->t_groupe_fonds)){
			foreach ($this->t_groupe_fonds as $gf) {
				if (empty($gf['ID_FONDS'])) $gf['ID_FONDS'] = $this->t_fonds['ID_FONDS'];
				$db->Execute('INSERT INTO t_groupe_fonds (ID_FONDS,ID_PRIV,ID_GROUPE) values ('.intval($gf['ID_FONDS']).','.intval($gf['ID_PRIV']).','.intval($gf['ID_GROUPE']).')');
			}
		}
		$ok=$db->CompleteTrans();

		if (!$ok) {$this->dropError(kErrorFondsSauveGroupe);return false;}
		return true;

	}


	function delete() {

		global $db;
		$db->StartTrans();

		$db->Execute("DELETE from t_groupe_fonds WHERE ID_FONDS=".intval($this->t_fonds['ID_FONDS']));
		$db->Execute("DELETE from t_fonds WHERE ID_FONDS=".intval($this->t_fonds['ID_FONDS']));
		$ok=$db->CompleteTrans();
		if (!$ok) {$this->dropError(kErrorFondsSuppr);return false;}
		return true;

	}


	function getVersions () {
		global $db;
		$tabLg=isset($_SESSION['tab_langue'])?$_SESSION['tab_langue']:array();
		unset($tabLg[$_SESSION['langue']]); // retrait de la langue en cours.
		if (!empty($this->t_fonds['ID_FONDS'])) {
		$sql="SELECT t_lang.ID_LANG, FONDS, FONDS_COL, LANG, ID_FONDS FROM t_fonds
				right join t_lang on (t_lang.ID_LANG=t_fonds.ID_LANG)
				WHERE
				t_fonds.ID_LANG<>".$db->Quote($this->t_fonds['ID_LANG'])." AND ID_FONDS=".intval($this->t_fonds['ID_FONDS']);
		$this->arrVersions=$db->GetAll($sql);
		}

		if (empty($this->arrVersions)){ //Pas d'ID OU Pas de version enregistrée, on charge la liste des langages
		$sql="SELECT *,'' as FONDS, '' as FONDS_COL, null as ID_FONDS FROM t_lang
				WHERE
				ID_LANG<>".$db->Quote($this->t_fonds['ID_LANG']);
		$this->arrVersions=$db->GetAll($sql);

		}
	}

	/** Sauve les différentes versions
	 *  IN : tableau des versions, MODE = retour de la fonction save appelée avant. Si retour=1=>création
	 * 	OUT : màj base
	 */
	 //MS 05-04-13 ajout sauvegarde de l' ID_FONDS_GEN du document dont on sauve les versions
	function saveVersions($tabVersions='') {
		global $db;
		foreach ($tabVersions as $lang=>$valeur) {
			if (!empty($valeur['FONDS'])) {
			$version= new Fonds;
			$version->t_fonds= $this->t_fonds;
			$version->t_fonds['ID_LANG']=$lang;
			$version->t_fonds['FONDS']=$valeur['FONDS'];
			if(isset($valeur['FONDS_COL']) && !empty($valeur['FONDS_COL'])){
				$version->t_fonds['FONDS_COL']=$valeur['FONDS_COL'];
			}else{
				$version->t_fonds['FONDS_COL']=$version->getFondsPath();
			}
			if(!empty($this->t_fonds['ID_FONDS_GEN'])){
				$version->t_fonds['ID_FONDS_GEN']=$this->t_fonds['ID_FONDS_GEN'];
			}
			//XB
			if ($version->checkExistId()) {
				$ok=$version->save();
				
			} else {
				$version->setFonds();//Permet de setter les champs calculables
				$ok=$version->create();
				
			}

			if ($ok==false && $version->error_msg) {$this->dropError(kErrorFondsVersionSauve.' : '.$version->error_msg);return false;}
			unset($version);
			}
		}
		return true;
	}


	//@update VG 28/06/2010 : ajout du test sur la valeur de ID_FONDS
	function checkExistId() {
		if (empty($this->t_fonds['ID_FONDS'])) {
			return false;
		}

		global $db;
		$sql="SELECT ID_FONDS FROM t_fonds WHERE ID_FONDS=".intval($this->t_fonds['ID_FONDS']);
		$sql.=" AND ID_LANG=".$db->Quote($this->t_fonds['ID_LANG']);
		$rs=$db->GetOne($sql);

		return ($db->GetOne($sql));
	}


	/**
	 * @update VG 28/05/10
	 */
/*	function setFondsCol() {
		if (empty($this->t_fonds['FONDS'])) {
			return false;
		}

		if(empty($this->t_fonds['FONDS_GEN']) || empty($this->t_fonds['ID_FONDS_GEN'])) $this->t_fonds['FONDS_COL'] = $this->t_fonds['FONDS'];
		else $this->t_fonds['FONDS_COL'] = $this->t_fonds['FONDS_GEN'].self::delimiter_genealogie.$this->t_fonds['FONDS'];

		return true;
	}*/

	/**
	 * @update VG 28/05/10
	 */
/*	function setFondsGenFromFondsCol() {
		if (empty($this->t_fonds['FONDS_COL'])) {
			return false;
		}

		$aFondsCol = explode(self::delimiter_genealogie, $this->t_fonds['FONDS_COL']);
		if(strcasecmp($aFondsCol[0],$this->t_fonds['FONDS_COL']) != '0') {
			$this->t_fonds['FONDS_GEN'] = $aFondsCol[0];
		}
		return true;
	}*/

	/**
	 * @update VG 28/05/10
	 */
/*	function setFondsFromFondsCol() {
		if (empty($this->t_fonds['FONDS_COL'])) {
			return false;
		}

		$aFondsCol = explode(self::delimiter_genealogie, $this->t_fonds['FONDS_COL']);
		if($aFondsCol[0] !== $this->t_fonds['FONDS_COL']) {
			if (empty($aFondsCol[1])) {
				return false;
			}

			$this->t_fonds['FONDS'] = $aFondsCol[1];

		} else {
			$this->t_fonds['FONDS'] = $this->t_fonds['FONDS_COL'];
		}

		return true;
	}*/

	/**
	 * @update VG 28/05/10
	 * @update VG 31/05/10
	 * @update VG 09/06/10
	 * @update MS 29/03/13 - Si on a un ID_FONDS_GEN, on teste dans un premier temps l'existence du parent via l'ID_FONDS_GEN
	 */
	function createFondsParentIfNotExist() {
		if(!empty($this->t_fonds['ID_FONDS_GEN'])){
			global $db;
			$sql="SELECT ID_FONDS FROM t_fonds WHERE ID_FONDS=".intval($this->t_fonds['ID_FONDS_GEN']);
			$sql.=" AND ID_LANG=".$db->Quote($this->t_fonds['ID_LANG']);
			$rs=$db->GetOne($sql);
			if(!empty($rs)){
				return false;
			}
		}
		
		if(empty($this->t_fonds['FONDS_GEN']))
			return false;

		$datas = array (
						'FONDS' =>	$this->t_fonds['FONDS_GEN'],
						'FONDS_COL' => $this->t_fonds['FONDS_GEN'],
						'ID_LANG'	=>	$this->t_fonds['ID_LANG'],
						'ID_FONDS_GEN'	=>	'0'
				);
		
		$oFondsGen = new Fonds();
		$oFondsGen->updateFromArray($datas);

		$idFondsGen = $oFondsGen->checkExist();
		if(!$idFondsGen) {
			$ok = $oFondsGen->create();
			if(!$ok) return false;
			$idFondsGen = $oFondsGen->t_fonds['ID_FONDS'];

			//On sauve les autres versions
			$aLang = getOtherLanguages();
			$fondsVersions = array();
			foreach ($aLang as $lang) {
				$fondsVersions[$lang] = array(
											'FONDS' =>	$oFondsGen->t_fonds['FONDS'],
											'FONDS_COL'	=>	$oFondsGen->t_fonds['FONDS_COL'],
										);
			}
			$oFondsGen->saveVersions($fondsVersions);
		}

		$this->t_fonds['ID_FONDS_GEN'] = $idFondsGen;

		return true;
	}
	
	// fonction de construction de FONDS_PATH (sur le modele de LEX_PATH ou CAT_PATH, mais ne sera pas stocké en base SQL, uniquement à destination de solr)
	// MS - 09.02.18 - modification de la fonction & de ses paramètres pr la rendre plus flexible et pour empêcher les boucles infinies
	function getFondsPath($include_depth=false,$delim=self::delimiter_genealogie){
		if(isset($this->t_fonds['FONDS'])){
			$fonds = $this->t_fonds ; 
			$lang = $fonds['ID_LANG']; 
			$arr_path = array($fonds['FONDS']); 
			while($fonds['ID_FONDS_GEN'] != 0){
				$fonds = getItem('t_fonds',array('ID_FONDS','FONDS','ID_FONDS_GEN'),array('ID_FONDS'=>$fonds['ID_FONDS_GEN'],'ID_LANG'=>$lang),array('ID_FONDS'=>'ASC'));
				if($delim == '/'){
					$tmp_fonds = str_replace('/','%SLASH%',$fonds['FONDS']);
				}else{
					$tmp_fonds = $fonds['FONDS'];
				}
				if(in_array($tmp_fonds,$arr_path)){
					// ajout d'un cas d'erreur pour éviter les boucles infinies 
					trace("Fonds::getFondsPath ERROR fonds déjà présent dans la hierarchie : ".$tmp_fonds.' > '.print_r(implode('.',array_reverse($arr_path)),1));
					return false ; 
				}
				$arr_path[] = $tmp_fonds;
			}
			
			$arr_path = array_reverse($arr_path);
			$path = implode($delim,$arr_path);
			if($include_depth){
				$path = intval(count($arr_path) - 1 ).$delim.$path;
			}
			return $path ; 
		}else{
			return false ; 
		}
	}
}
?>