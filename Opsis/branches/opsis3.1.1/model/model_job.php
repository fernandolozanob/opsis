<?
//************************************************** JOB ***************************************************************

// VP : 27/11/2007 : Création du fichier, définition de la classe Job

//************************************************** JOB ***************************************************************
require_once(modelDir.'model.php');

class Job extends Model
{

	var $t_job;
	var $t_module;
	var $error_msg;
	
	var $logFile;
	var $xmlFile;
    var $xmlRunFile;
	var $full_xml_job_process;
	var $logXmlFile;
	var $flag_runSyncJob ;


	 //*** Constructeur
    function __construct($id=null,$version=""){
		parent::__construct('t_job',$id,$version);
	}
	
	function getJob($arrJob=array()){
		return $this->getData($arrJob) ; 
	}
	
	
	function getData($arrData=array(),$version=""){
		global $db;
		$arrJob=parent::getData($arrData,$version); 
		
		if (empty($arrJob) && !empty($this->t_job) && !$arrData) { 
			$this->getModule();
			if(preg_match('/<run_sync_job>true<\/run_sync_job>/',$this->t_job['JOB_PARAM'])){
				$this->flag_runSyncJob = true ; 
			}
		}else{
			$arrJobSorted = array() ; 
			foreach ($arrJob as $idx=>$Job) { 
				$arrJobSorted[$Job->t_job['ID_JOB']] = $Job ; 
				$arrJobSorted[$Job->t_job["ID_JOB"]]->getModule();
				if(preg_match('/<run_sync_job>true<\/run_sync_job>/',$Job->t_job['JOB_PARAM'])){
					$arrJobSorted[$Job->t_job["ID_JOB"]]->flag_runSyncJob = true ; 
				}
			}
			return $arrJobSorted;
		}
	}
	
	/*function getJob($arrJob=array()) {
		global $db;
		if (!$arrJob) $arrCrit['ID_JOB']=array($this->t_job['ID_JOB']); else $arrCrit['ID_JOB']=(array)$arrJob;
		
		$this->t_job=getItem("t_job",null,$arrCrit,null,0); //AJOUTER GESTION DES ORDRES

		if (count($this->t_job)==1 && !$arrJob) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->t_job=$this->t_job[0];
				$this->getModule();
				if(preg_match('/<run_sync_job>true<\/run_sync_job>/',$this->t_job['JOB_PARAM'])){
					$this->flag_runSyncJob = true ; 
				}
		}
		else {
            $arrVals=array();
			foreach ($this->t_job as $idx=>$Job) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$Job["ID_JOB"]]= new Job();
				//$arrVals[$idx]->arrJob=$Job;
				$arrVals[$Job["ID_JOB"]]->t_job=$Job;
				$arrVals[$Job["ID_JOB"]]->getModule();
				if(preg_match('/<run_sync_job>true<\/run_sync_job>/',$Job['JOB_PARAM'])){
					$arrVals[$Job["ID_JOB"]]->flag_runSyncJob = true ; 
				}
			}
			unset($this); // plus besoin
			return $arrVals;
		}
	}*/
	
	function getChildrenJobs($include_this = false, $exclude_structural_jobs = false){
		global $db ; 
		
		$arrJobs = array() ; 
		if($include_this && (!$exclude_structural_jobs || $this->t_job['JOB_ID_MODULE'] != 0)){
			$arrJobs[$this->t_job['ID_JOB']] = $this;
		}
		$ids = array($this->t_job['ID_JOB']);
		$i_backup = 0 ; 
		do {
			$ids_tmp = $db->GetCol('SELECT ID_JOB from t_job WHERE ID_JOB_GEN in ('.implode(',',$ids).")");
			$ids = array();
			if(!empty($ids_tmp)){
				foreach($ids_tmp as $id){
					$jobTmp = new Job();
					$jobTmp->t_job['ID_JOB'] = $id ;
					$jobTmp->getJob();
					if(!$exclude_structural_jobs || $jobTmp->t_job['JOB_ID_MODULE'] != 0 ){
						$arrJobs[$jobTmp->t_job['ID_JOB']] = $jobTmp ;
					}
					$ids[] = $jobTmp->t_job['ID_JOB'] ;
				}
			}
			$i_backup++;
		}while ($i_backup < 10 && !empty($ids)); // i_backup permet d'éviter tout gag de boucle infinie, 10 de profondeur devrait être largement suffisant pr la plupart des usages
		
		return $arrJobs;
	}
	
	function getEtatJob(){
		global $db;
		if(!empty($this->t_job)){
			return $this->t_job['ETAT_JOB']=$db->GetOne("select t_etat_job.ETAT_JOB from t_etat_job where ID_ETAT_JOB=".intval($this->t_job['JOB_ID_ETAT'])." AND ID_LANG=".$db->Quote($_SESSION['langue']));
		}
	}

	function setLogFile(){
		$this->logFile=jobOutDir.'/'.$this->t_module['MODULE_NOM'].'/P'.$this->t_job["JOB_PRIORITE"].'_'.str_replace(array('-',' ',':'),'',$this->t_job["JOB_DATE_CREA"]).'_'.kDatabaseName."_".sprintf('%08d',$this->t_job["ID_JOB"]).".log";
	}
// VP 22/04/10 : création fonction setLogXmlFile()
	function setLogXmlFile(){
		$this->logXmlFile=jobOutDir.'/'.kDatabaseName."_".sprintf('%08d',$this->t_job["ID_JOB"]).".xml";
	}
	
	function setXmlFile(){
		// VP 15/01/09 : ajout module dans nom dossier in
		$moduleDir=jobInDir.'/'.$this->t_module['MODULE_NOM'];
		if(!is_dir($moduleDir)) mkdir($moduleDir);
		//modification du nom du fichier xml de pour le frontal pour prendre en compte les priorites
		$this->xmlFile=$moduleDir."/P".$this->t_job["JOB_PRIORITE"]."_".str_replace(array('-',' ',':'),'',$this->t_job["JOB_DATE_CREA"])."_".kDatabaseName."_".sprintf('%08d',$this->t_job["ID_JOB"]).".xml";
		//VP 17/01/11 : ficher xml pending
		if(defined("jobPendingDir")){
            $moduleDir=jobPendingDir."/".$this->t_module['MODULE_NOM'];
            $this->xmlPendingFile=$moduleDir."/".basename($this->xmlFile);
		}
        if(defined("jobRunDir")){
            $moduleDir=jobRunDir."/".$this->t_module['MODULE_NOM'];
            $fichiers_run=list_directory($moduleDir, array('xml'));
            if(count($fichiers_run)>0){
                foreach($fichiers_run as $f){
                    if(basename($f)==basename($this->xmlFile)){
                        $this->xmlRunFile = $f;
                    }
                }
            }
        }
	}

	function getModule(){
		global $db;
		if(!empty($this->t_job['JOB_ID_MODULE'])) $this->t_module=$db->GetRow("select * from t_module where ID_MODULE=".intval($this->t_job['JOB_ID_MODULE']));
	}

// VP 7/01/09 : ajout méthode setModule()
	function setModule($arrCrit){
		global $db;
		$sql="select * from t_module where 1=1 ";
		foreach ($arrCrit as $fld=>$val) {
			if (strpos($fld,'MODULE_')===0) $sql.= " and ".strtoupper($fld)."=".$db->Quote($val);
		}
		$this->t_module=$db->GetRow($sql);
		$this->t_job['JOB_ID_MODULE']=$this->t_module['ID_MODULE'];
	}
	
	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {

	 	if (empty($this->t_job)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_job[strtoupper($fld)])) $this->t_job[strtoupper($fld)]=$val;
		}

	}

	function save() {
		global $db;
		if (empty($this->t_job['ID_JOB'])) {return $this->create();}

		$rs = $db->Execute("SELECT * FROM t_job WHERE ID_JOB=".intval($this->t_job['ID_JOB']));
		$sql = $db->GetUpdateSQL($rs, $this->t_job);
		if (!empty($sql)) $ok = $db->Execute($sql);
		// trace("save job ".$sql);

		if (!$ok && !empty($sql)) {$this->dropError(kErrorJobSauve);return false;}
		return true;
	}

	function create() {
		global $db;
		if (!empty($this->t_job['ID_JOB'])) {return $this->save();}

		$this->t_job['JOB_ID_USAGER']=intval($_SESSION["USER"]['ID_USAGER']);
		
		$ok = $db->insertBase("t_job","id_job",$this->t_job);
		if (!$ok) {$this->dropError(kErrorJobCreation);return false;}
		$this->t_job['ID_JOB']=$ok;
		if ($this->t_job['JOB_ID_ETAT']==1){
            if(defined("kJobPlateforme") && $this->t_job['JOB_PLATEFORME']!= kJobPlateforme){
                $this->t_job['JOB_ID_ETAT']==0;
            } elseif(!$this->makeXML()){
                $this->t_job['JOB_ID_ETAT']=jobErreur;
                $this->t_job['JOB_MESSAGE']=$this->error_msg;
            }
            $this->save();
        }

		return true;
	}


	function initJob() {
        if(!isset($this->t_job['JOB_ID_ETAT'])) $this->getJob();
//        // Annulation si job en cours
//        if($this->t_job['JOB_ID_ETAT']==jobEnCours){
//            $this->makeCancelXml();
//        }
        // Initialisation ensuite si le job précédent est fini
        $etat=1;
		if (!empty($this->t_job['ID_JOB_PREC'])){
            $oJobPrec=new Job();
            $oJobPrec->t_job['ID_JOB']=$this->t_job['ID_JOB_PREC'];
            $oJobPrec->getJob();
            if($oJobPrec->t_job['JOB_ID_ETAT'] != jobFini){
                $etat=0;
            }
            unset($oJobPrec);
        }
        $this->t_job['JOB_ID_ETAT']=$etat;
        $this->t_job['JOB_PROGRESSION']=0;
        $this->t_job['JOB_DATE_DEBUT']='';
        $this->t_job['JOB_DATE_FIN']='';
        $this->t_job['JOB_MESSAGE']='';
        
		if ($this->t_job['JOB_ID_ETAT']==1){
            if(defined("kJobPlateforme") && $this->t_job['JOB_PLATEFORME']!= kJobPlateforme){
                $this->t_job['JOB_ID_ETAT']==0;
            } elseif(!$this->makeXML()){
                $this->t_job['JOB_ID_ETAT']=jobErreur;
                $this->t_job['JOB_MESSAGE']=$this->error_msg;
            }
        }
        $this->save();
	}
    
	function setStatus($etat) {
		$this->t_job["JOB_ID_ETAT"]=$etat;
		if($etat=="0"){
			// Suppression fichier XML
			$this->deleteXmlFile();
			// Suppression fichier log frontal
			$this->deleteLog();
		}
	}

	function updateProcStatus() {
		global $db;
		if(!empty($this->t_job["ID_JOB_GEN"])){
			$procStatus=$db->GetOne("select min(JOB_ID_ETAT) from t_job where ID_JOB_GEN=".intval($this->t_job["ID_JOB_GEN"]));
            if(empty($procStatus)) $procStatus=jobEnCours; // Statut en cours par défaut
			$res_moyenne=$db->GetOne('SELECT AVG(JOB_PROGRESSION) FROM t_job WHERE ID_JOB_GEN='.intval($this->t_job["ID_JOB_GEN"]));
			$db->Execute("UPDATE t_job SET JOB_ID_ETAT=".intval($procStatus).",JOB_PROGRESSION=".floatval($res_moyenne)." WHERE ID_JOB=".intval($this->t_job["ID_JOB_GEN"]));
		}
	}
	
	function delete() {
		global $db;
        if(!empty($this->t_job['ID_JOB'])){
            $db->StartTrans();
            // VP 15/10/10 : suppression jobs fils
            //$db->Execute("DELETE from t_job WHERE ID_JOB=".$db->Quote($this->t_job['ID_JOB']));
            $db->Execute("DELETE from t_job WHERE ID_JOB=".intval($this->t_job['ID_JOB'])." or ID_JOB_GEN=".intval($this->t_job['ID_JOB']));
            // VP 24/06/10 : mise à jour des jobs impactés
            $db->Execute("UPDATE t_job set ID_JOB_GEN=0 where ID_JOB_GEN=".intval($this->t_job['ID_JOB']));
            $db->Execute("UPDATE t_job set ID_JOB_PREC=0 where ID_JOB_PREC=".intval($this->t_job['ID_JOB']));
            $db->Execute("UPDATE t_job set ID_JOB_VALID=0 where ID_JOB_VALID=".intval($this->t_job['ID_JOB']));
            $ok=$db->CompleteTrans();
            if (!$ok) {$this->dropError(kErrorJobSuppr);return false;}
            // Suppression fichier XML consigne
            $this->deleteXmlFile();
            // Suppression fichier log frontal
            $this->deleteLog();
            return true;
        }
	}

    // VP (16/10/08) : ajout méthode deleteLog()
    // Suppression fichier log frontal
    function deleteLog() {
        $this->setLogFile();
        if(file_exists($this->logFile)) unlink($this->logFile);
        return true;
    }

    // VP 22/02/15 : ajout méthode deleteXmlFile()
    // Suppression des fichiers xml
    function deleteXmlFile() {
        $this->setXmlFile();
        // Suppression fichier xml in
        if(file_exists($this->xmlFile)) unlink($this->xmlFile);
        // Suppression ficher xml pending
        if(isset($this->xmlPendingFile) && file_exists($this->xmlPendingFile)) unlink($this->xmlPendingFile);
        // Suppression ficher xml run
        if(isset($this->xmlRunFile) && file_exists($this->xmlRunFile)) unlink($this->xmlRunFile);
        return true;
    }

	// Paramétrage du nom de fichier en fonction de pattern $in et $out
	// $in contient une liste de séparateurs séparés par des /
	// LD : dynamic_args contient un tableau de valeurs à insérer dynamiquement dans le nom de fichier
	function setOut($fileIn,$in,$out,$dynamic_args=null) {
//		$t=explode("/",$in);
//		$file=str_replace($t,"/",$fileIn);
//		$t=explode("/",$file,2);
//		$_jobout=$t[0];
		
		if($dynamic_args){
			if(is_array($dynamic_args) && !isset($dynamic_args['suffixe']) && !isset($dynamic_args['rename'])){
				$dynamic_args['suffixe'] = $dynamic_args;
			}else if(!is_array($dynamic_args)){
				$dynamic_args['suffixe'] = array($dynamic_args);
			}
		}

        $t=explode("/",$in);
        foreach ($t as $value) {
            if(strpos($fileIn, $value)) {
                $needle = $value;
                break;
            }
        }
        // VP 3/01/13 : traitement cas needle vide
        if(empty($needle)) $_jobout =$fileIn;
		else if (isset($dynamic_args['rename']) && !empty($dynamic_args['rename'])) $_jobout = $dynamic_args['rename'] ; 
        else $_jobout = substr($fileIn, 0, strrpos($fileIn, $needle));

		// VP (31/8/08) : possibilité de passer dynamic_args en chaine plutôt que tableau
		if($dynamic_args['suffixe'] && !is_array($dynamic_args['suffixe'])) $dynamic_args['suffixe']=array($dynamic_args['suffixe']);
		if (is_array($dynamic_args['suffixe'])) $_jobout.=vsprintf($out,$dynamic_args['suffixe']); else $_jobout.=$out;
		// By VP (17/9/08) : filtrage des caractères interdits
		$this->t_job["JOB_OUT"]=getFileFromPath($_jobout,true);
		// MS 26/07/2012 pour les modules de diffusion (et surtout multi-diffusion)
		// on veut pouvoir conserver le meme fichier en entrée / sortie 
		if(($this->t_module["MODULE_TYPE"]=="DIFF")&&(getExtension($fileIn)!='')){
			$this->t_job["JOB_OUT"]= $fileIn;
		}
	}

	function readLog(&$log, $partial=false) {
		$this->setLogFile();
		// récupération du fichier de log du job en utilisant un pattern : permet de continuer de lire un job en cours meme si on a modifié sa priorité pendant l'execution. 
		$logFilePattern = preg_replace('/(P[0-9])/','P?',$this->logFile);
		$foundLogFile = glob($logFilePattern);
		if(is_array($foundLogFile)){
			$foundLogFile = reset($foundLogFile);
		}
		if($foundLogFile && file_exists($foundLogFile)){
            $length = 1048576; // 1Mo
            if($partial && filesize($foundLogFile)>2*$length){
                $fp = fopen($foundLogFile, 'r');
                $log = fread($fp, $length); // début fichier
                fseek($fp, filesize($foundLogFile) - $length);
                $log .= fread($fp, $length); // fin fichier
                fclose($fp);
            } else {
                $log = file_get_contents($foundLogFile);
            }
			//$log = file_get_contents($this->logFile);
			return true;
		}else if (isset($this->local_out_log) && !empty($this->local_out_log)){
			$log = $this->local_out_log ; 
			return true ; 
		} else return false;
	}

	// VP 22/04/10 : création fonction readLogXml()
	function readLogXml(&$logXml) {
		$this->setLogXmlFile();
		if(file_exists($this->logXmlFile)){
			$logXml = file_get_contents($this->logXmlFile);
			return true;
		}else return false;
	}

    
	function makeXML() {
		global $db;
		// VP 17/11/09 : traitement cas validation
		// Dans le cas d'une étape de validation, on ne crée pas de XML et on envoie un mail
		if($this->t_module["MODULE_TYPE"]=="VALID"){
			if(preg_match("|<mail>(.*)</mail+>|i",$this->t_job["JOB_PARAM"],$matches)) $dest=$matches[1];
			$sujet=kMailValidation.$this->t_job["JOB_NOM"];
			$ok=$this->send_mail($dest,$sujet,"print/jobValidMail.xsl");
			if(!$ok) $this->dropError(kErrorEnvoiMailValidation);
			else $this->t_job["JOB_DATE_DEBUT"]=date("Y-m-d H:i:s");
			return $ok;
		}
		// Nom du fichier : base_NNNNN.xml
		// Le XML n'est pas écrit pour les processus
		require_once(modelDir.'model_materiel.php');
		if(empty($this->t_job["JOB_ID_PROC"])){
			$search = array('&','<','>');
			$replace = array('&#38;','&#60;','&#62;');

			// VP (19/11/08) : Révision mécanisme des folders
			// VP (9/10/08) : Cablage entrée sur sortie du job précédent (Attention : STORY et FTP non traité)
			// VP (20/10/09) : Prise en compte lieu mat du traitement précédent 
			if($this->t_job["ID_JOB_PREC"]!=0){
				$jobPrec=new Job();
				$jobPrec->t_job["ID_JOB"]=$this->t_job["ID_JOB_PREC"];
				$jobPrec->getJob();
				// VP 5/10/10 : changement FOLDER en FOLDER_OUT
				if(preg_match("|<FOLDER_OUT>(.*)</FOLDER_OUT+>|",$jobPrec->t_job["JOB_PARAM"],$matches)) {
					$inFolder=$matches[1];
					$in=$inFolder.$jobPrec->t_job["JOB_OUT"];
				}elseif($jobPrec->t_module["MODULE_TYPE"]=="STORY"){
					$imgObj=Imageur::getImageurByName($jobPrec->t_job["JOB_OUT"]);
					if($imgObj)
						$in=kStoryboardDir.str_replace($search,$replace,$imgObj->getImageurPath());
				}else if(preg_match('/<entity>doc_acc<\/entity>/',$jobPrec->t_job['JOB_PARAM'])){
						require_once(modelDir.'model_docAcc.php');
						// JOB_ID_MAT devrait être remplacé par "JOB_ID_ENTITE", et se baser sur le fait que sauf spécifié, l'entité est "mat"
						$doc_acc_out = getItem('t_doc_acc',array('ID_DOC_ACC'),array('DA_FICHIER'=>$jobPrec->t_job['JOB_OUT'],'ID_LANG'=>$_SESSION['langue']),array('ID_DOC_ACC'=>'ASC'));
						if(!empty($doc_acc_out)){
							$da_obj = new DocAcc($doc_acc_out['ID_DOC_ACC']);
							$in=str_replace($search,$replace,$da_obj->getFilePath());
						}
				}else{
					//Récupération du chemin
					if (!empty($jobPrec->t_job["JOB_ID_MAT"])) {
						$matPrec = new Materiel;
						$matPrec->t_mat['ID_MAT'] = $jobPrec->t_job["JOB_ID_MAT"];
						$matPrec->getMat();
						$matObj=Materiel::getMatByMatNom($jobPrec->t_job["JOB_OUT"], $matPrec->t_mat['MAT_CHEMIN']);
					}else
						$matObj=Materiel::getMatByMatNom($jobPrec->t_job["JOB_OUT"]);
                    //$in=Materiel::getLieuByIdMat($jobPrec->t_job["JOB_OUT"]).$jobPrec->t_job["JOB_OUT"];
                    if($matObj) $in=str_replace($search,$replace,$matObj->getFilePath());
                }
				unset($jobPrec);
			}
			$param=$this->t_job["JOB_PARAM"]."\n";
			// VP (9/10/08) : Prise en compte paramètre FOLDER éventuel
			// VP 5/10/10 : changement FOLDER en FOLDER_OUT
			if(preg_match("|<FOLDER_OUT>(.*)</FOLDER_OUT+>|",$param,$matches)) $outFolder=$matches[1];
			elseif(stripos($this->t_module["MODULE_NOM"], 'brightcove') !== false ) {
				$chemin=klivraisonRepertoire.preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()).'/';
				mkdir($chemin);
				if(!empty($chemin)) {
					$param=str_replace("</param>","<FOLDER_OUT>".$chemin."</FOLDER_OUT></param>",$param);
					$this->t_job["JOB_PARAM"]=$param;
					$this->save();
					$outFolder = $chemin;
				}
			}
			elseif(isset($inFolder))  $outFolder=$inFolder;
			else
			{
				$donnees_lieu_mat=Materiel::setLieuByIdMat($this->t_job["JOB_OUT"]);
				$outFolder=$donnees_lieu_mat['LIEU_PATH'];
			}
			
			// VP 3/12/09 : Prise en compte paramètre FOLDER_IN éventuel
			if(preg_match("|<FOLDER_IN>(.*)</FOLDER_IN+>|",$param,$matches)) {
				$in=$matches[1].$this->t_job["JOB_IN"];
			}

            // VP 4/07/13 : ajout entity doc_acc
            $entity="mat";
            if(preg_match("|<entity>(.*)</entity+>|i",$param,$matches)) {
                $entity=$matches[1];
            }
            if($entity=="doc_acc"){
				if((!isset($in) || empty($in)) && !empty($this->t_job["JOB_ID_MAT"])){
					// Fichier in
					require_once(modelDir.'model_docAcc.php');
					$myDA= new DocAcc;
					$myDA->t_doc_acc['ID_DOC_ACC']=$this->t_job["JOB_ID_MAT"];
					$myDA->getDocAcc();
					$in=str_replace($search,$replace,$myDA->getFilePath());
				 }
                unset($myDA);
                // Fichier out
                $outFolder=kDocumentDir;
                $out=str_replace($search,$replace,$outFolder.$this->t_job["JOB_OUT"]);
                
            }
			if(!empty($this->flag_runSyncJob) && $this->flag_runSyncJob){
				if(!preg_match('/<run_sync_job>true<\/run_sync_job>/',$this->t_job['JOB_PARAM'])){
					if(empty($this->t_job['JOB_PARAM'])){
						$this->t_job['JOB_PARAM'] = "<param><run_sync_job>true</run_sync_job></param>" ;
					}else{
						$this->t_job['JOB_PARAM']= str_replace("</param>",'<run_sync_job>true</run_sync_job></param>',$this->t_job['JOB_PARAM']) ;
					}
				}
			}
                
            // VP 7/01/09 : ajout cas backup (BACK)
            switch($this->t_module["MODULE_TYPE"]){
                case "ENCOD": //XXX => GET
				 if(preg_match("|<ID_MAT_SUBTITLE>(.*)</ID_MAT_SUBTITLE+>|i",$param,$matches)) {
					$id_mat_sub=$matches[1];
					if($id_mat_sub){
						$matSub = new Materiel;
						$matSub->t_mat['ID_MAT'] =$id_mat_sub;
						$matSub->getMat();					
						$path=$matSub->getLieuByIdMat($id_mat_sub);
						$path_matSub= $path.$matSub->t_mat['MAT_NOM'];
						 if(!empty($path_matSub)) {
							$param=str_replace("</param>","<path_sub>".$path_matSub."</path_sub></param>",$param);                     
                            $this->t_job["JOB_PARAM"]=$param;
                            $this->save();
						 }
					}
				}

                    if($entity!="doc_acc"){
                        $matObj=$this->getMatObj();
                        if(!isset($in)) {
                            if($matObj) $in=str_replace($search,$replace,$matObj->getFilePath());
                            //$in=str_replace($search,$replace,Materiel::getLieuByIdMat($this->t_job["JOB_IN"]).$this->t_job["JOB_IN"]);
                            //$this->t_job["JOB_IN"]
                        }
                        $out=str_replace($search,$replace,$outFolder.$this->t_job["JOB_OUT"]);
                        // VP (13/10/08) : ajout TC offset éventuel
                        // VP 13/07/09 : test MAT_TCFORCE pour MPEG seulement
                        // VP 9/04/10 : envoi balise TcOffset si tci=offset
                        if(preg_match("|<tci>(.*)</tci+>|i",$this->t_job["JOB_PARAM"],$matches)) {
                            $tci=$matches[1];
                            // VP 9/09/10 : correction bug sur test format
                            //if(($tci=="offset")&&($matObj->t_mat['MAT_TCFORCE']=='1' || ($matObj->t_mat['MAT_FORMAT']!= "MPEG2" && $matObj->t_mat['MAT_FORMAT']!= "MPEG1"))) $TcOffset=$matObj->t_mat['MAT_TCIN'];
                            // VP 27/01/11 : affectation TcOffset systématique si TCI
                            $TcOffset=$matObj->t_mat['MAT_TCIN'];
                        }
                        if(strpos($param,'tc_source')>0) {
                            $tcin=str_replace(':','\\:',$matObj->t_mat['MAT_TCIN']);
                            $param=str_replace('tc_source',$tcin,$param);
                        }
                        //unset($matObj);
                    }
                    break;
					
					
                case "NUM":
                    if(!isset($in)) $in=str_replace($search,$replace,$this->t_job["JOB_IN"]);
                    $out=str_replace($search,$replace,$outFolder.$this->t_job["JOB_OUT"]);
                    break;
                    
                case "STORY":
                    if($entity == 'doc_acc'){
						//dans le cas doc_acc on ne crée pas d'imageur, on met donc les images dans un sous dossier au nom de JOB_OUT qui deviendra le DA_CHEMIN dans updateJobs
						  $out=str_replace($this->t_job["JOB_OUT"],$this->t_job["JOB_OUT"].'/'.$this->t_job["JOB_OUT"],$out);
					}else{
						$imageur=str_replace($search,$replace,$this->t_job["JOB_OUT"]); //XXX => GET
						
						$matObj=$this->getMatObj();
						if($matObj){
							require_once(modelDir.'model_imageur.php');
							$myImageur=new Imageur;
						   
							if (isset($matObj->t_mat['MAT_ID_IMAGEUR']) && !empty($matObj->t_mat['MAT_ID_IMAGEUR']))
							{
								$myImageur->t_imageur['ID_IMAGEUR']=$matObj->t_mat['MAT_ID_IMAGEUR'];
								$myImageur->getImageur();
							}
							else
							{
								$myImageur->t_imageur['IMAGEUR']=$imageur;
								$myImageur->save();
							}
							
							$matObj->saveImageur($myImageur->t_imageur['ID_IMAGEUR']);
							$matObj->save();
							//trace(print_r($matObj,true));
							
							if(!isset($in)) {
								$in=str_replace($search,$replace,$matObj->getFilePath());
								//$in=str_replace($search,$replace,Materiel::getLieuByIdMat($this->t_job["JOB_IN"]).$this->t_job["JOB_IN"]);
							}

							$out=kStoryboardDir.$myImageur->getImageurPath()."/".$imageur."_";
							// VP 11/02/10 : création dossier storyboard
							if (!is_dir(kStoryboardDir.$myImageur->getImageurPath())) {
								$myImageur->createImageurDir();
								chmod(kStoryboardDir.$myImageur->getImageurPath(),0775); //on remet un chmod car le mode dans mkdir est mal géré
							}
							
							$myImageur->deleteAllImages(true);
							unset($myImageur);
						}
                    }
                    break;
                    
                case "FTPIM":
                    $imageur=substr($this->t_job["JOB_IN"],0,strlen($this->t_job["JOB_IN"])-13);
                    $in=str_replace($search,$replace,kStoryboardDir.$imageur."/".$this->t_job["JOB_IN"]);
                    $out=str_replace($search,$replace,$this->t_job["JOB_OUT"]);
                    break;
                    
                case "FTP":
                    // VP 27/10/10 : traitement cas FTP
                    if(!isset($in)) {
                        $matObj=$this->getMatObj();
                        if($matObj) $in=str_replace($search,$replace,$matObj->getFilePath());
                        //$in=str_replace($search,$replace,Materiel::getLieuByIdMat($this->t_job["JOB_IN"]).$this->t_job["JOB_IN"]);
                    }
                    $out=str_replace($search,$replace,$this->t_job["JOB_OUT"]);
                    break;

                case "BACK":
                    require_once(libDir."class_backupJob.php");
                    $myBackupJob=new BackupJob();
                    if(preg_match("|<action>(.*)</action+>|i",$this->t_job["JOB_PARAM"],$matches)) $action=$matches[1];
                    if(preg_match("|<media>(.*)</media+>|i",$this->t_job["JOB_PARAM"],$matches)) $media=$matches[1];
                    if(preg_match("|<set>(.*)</set+>|i",$this->t_job["JOB_PARAM"],$matches)) $set=$matches[1];
                    $finfo=BackupJob::getFinfo($this->t_job["JOB_IN"], $media, $this->t_job["JOB_ID_MAT"]);
                    switch($action){
                        case "backup":
                            $in=str_replace($search,$replace,$finfo['path']);
                            $out=str_replace($search,$replace,$this->t_job["JOB_OUT"]);
                            if(!is_file($finfo['path'])){
                                // VP 3/09/10 : vérification si fichier présent et déblocage job suivant
                                $this->dropError(kMaterielFichierManquant);
                                //$db->Execute("UPDATE t_job set ID_JOB_PREC=0 where ID_JOB_PREC=".$db->Quote($this->t_job['ID_JOB']));
                                $this->launchNextJobs();
                                return false;
                            }elseif($myBackupJob->checkBackupExists($finfo,$set)){
                                // vérification si fichier non sauvé
                                // VP 4/03/09 : envoi erreur si fichier déja sauvé
                                $this->dropError(kErrorFichierDejaSauve);
                                $this->launchNextJobs();
                                return false;
                            }else{
                                // recherche de la cassette
                                $id_tape=$myBackupJob->getCurrentTape($finfo,$set);
                                // Ajout balise <id_tape>
                                // VP 4/03/09 : envoi erreur si pas de tape
                                if(!empty($id_tape)) $param=str_replace("</param>","<tape>".$id_tape."</tape></param>",$param);
                                else {
                                    if(!empty($myBackupJob->error_msg)) $this->dropError($myBackupJob->error_msg);
                                    else $this->dropError(kErrorTapeNoId);
                                    return false;
                                }
                            }
                            if($entity!="doc_acc"){
                                // VP 18/10/12 : ajout chemin fichier pour conservation dans tar de sauvegarde
                                $matObj=$this->getMatObj();
                                if($matObj) $chemin=str_replace($search,$replace,$matObj->t_mat['MAT_CHEMIN']);
                                if(!empty($chemin)) $param=str_replace("</param>","<chemin>".$chemin."</chemin></param>",$param);
                            }
                            
                            $this->t_job["JOB_PARAM"]=$param;
                            $this->save();
                            break;
                            
                        case "restore":
                        case "restoreOffline":
                            $in=str_replace($search,$replace,$this->t_job["JOB_IN"]);
                            if(preg_match("|<FOLDER_OUT>(.*)</FOLDER_OUT+>|",$param,$matches)){
                                $outFolder=$matches[1];
                                $out=str_replace($search,$replace,$outFolder.$this->t_job["JOB_OUT"]);
                            }
                            else $out=str_replace($search,$replace,$finfo['path']);
                            if($entity!="doc_acc"){
                                // VP 19/10/12 : ajout chemin fichier pour restoration du tar de sauvegarde
                                $matObj=$this->getMatObj();
                                if($matObj) $chemin=str_replace($search,$replace,$matObj->t_mat['MAT_CHEMIN']);
                                if(!empty($chemin)) $param=str_replace("</param>","<chemin>".$chemin."</chemin></param>",$param);
                            }
                            $this->t_job["JOB_PARAM"]=$param;
                            $this->save();
                            break;
                        default:
                            $in=str_replace($search,$replace,$this->t_job["JOB_IN"]);
                            $out=str_replace($search,$replace,$this->t_job["JOB_OUT"]);
                            break;
                    }
                    unset($myBackupJob);
                    break;

                default: // XXX => GET
                    if(!isset($in)) {
                        $matObj=$this->getMatObj();
                        if($matObj) $in=str_replace($search,$replace,$matObj->getFilePath());
                        //$in=str_replace($search,$replace,Materiel::getLieuByIdMat($this->t_job["JOB_IN"]).$this->t_job["JOB_IN"]);
                    }
                    // VP 21/01/10 : ajout paramètre $outFolder
                    //$out=str_replace($search,$replace,$this->t_job["JOB_OUT"]);
                    $out=str_replace($search,$replace,$outFolder.$this->t_job["JOB_OUT"]);
                    break;
            }
            // VP (23/9/08) : Traitement xsl éventuel
            // VP (23/11/09) : choix entre export doc et export job
            if(preg_match("|<doc_xsl>(.*)</doc_xsl+>|i",$param,$matches)){
                $doc_xsl=$matches[1];
                if(!empty($doc_xsl)){
                    // VP 1/06/2012 : intégration export <ID_LIGNE_PANIER> et <ID_DOC> dans xml_export()
                    $xml=$this->xml_export();
                    
                    // VP 24/08/10 : suppression remplacement &, traiter en CDATA les données si nécessaire
                    //$xml=str_replace("&","&amp;",$xml);
                    $xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";
                    $prm["today"] = date("Y-m-d H:i:s");
                    $prm["pubdate"]= date("r");
                    // VP 7/02/11 : ajout htmlOuput=false à TraitementXSLT
                    $xml_params = TraitementXSLT($xml,getSiteFile("designDir","print/".$doc_xsl),$prm,0,"",null,false);
                    // VP 11/9/09 : Changement expression dans eval (nb : les X représentent des chevrons) 
                    //eval("?X". str_replace(array("&","<br/>"),array("&amp;"," "),$xml_params). "X?");
                    ob_start();
                    eval("?>". str_replace(array("<br/>","&lt;?","?&gt;"),array("\n","<?","?>"),$xml_params). "<?");
                    $xml_params=ob_get_contents();
                    ob_end_clean();
                    // VP 22/01/10 : inclusion xml_param dans <param>
                    //$param.=$xml_params;
                    $param=str_replace("</param>",$xml_params."</param>",$param);
                    // VP 27/01/10 : cas où <in> et <out> sont vides
                    if(preg_match("|<in>(.*)</in+>|i",$xml_params,$matches)) {
                        $in=$matches[1];
                    } elseif(preg_match("|<in/>|i",$xml_params,$matches)) {
                        $in="";
                    }
                    // VP 10/12/08 : Remplacement éventuel du fichier de sortie
                    if(preg_match("|<out>(.*)</out+>|i",$xml_params,$matches)) {
                        $out=$matches[1];
                    } elseif(preg_match("|<out/>|i",$xml_params,$matches)) {
                        $out="";
                    }
                }
            }

			// VP (23/9/08) : Constitution fichier XML
			// VP (21/1/09) : ajout test sur $in et $out
			// VP 1/12/09 : ajout encoding
			$content="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			$content.="<data>\n";
			if(!empty($in)) $content.="<in>".$in."</in>\n";
			// VP 8/04/10 suppression balise TcOffset
			// VP 9/09/10 : réintroduction balise TcOffset
			if(isset($TcOffset)) $content.="<TcOffset>".$TcOffset."</TcOffset>\n";
			if(!empty($out)) $content.="<out>".$out."</out>\n";		
			if (isset($this->t_job["JOB_DATE_LANCEMENT"]) && !empty($this->t_job["JOB_DATE_LANCEMENT"]))
				$content.="<date_lancement>".$this->t_job["JOB_DATE_LANCEMENT"]."</date_lancement>\n";
			
			$content.=$param;
			$content.="</data>\n";
			
			$this->full_xml_job_process = $content ; 
			
			// VP 4/03/09 : ouverture fichier XML juste avant écriture
			if(empty($this->flag_runSyncJob) || !$this->flag_runSyncJob){
				$this->setXmlFile();
				$handle=fopen($this->xmlFile,'w');
				if (!$handle) {
					$this->dropError(kErrorVisioCreationXML);
					return false;
				}
				fwrite($handle,$content);
				fclose($handle);
			}
		}
		return true;
	}
	
	function makeCancelXml()
	{
		$xml_directory=jobCancelDir.'/'.$this->t_module['MODULE_NOM'];
		if(!is_dir($xml_directory))
			mkdir($xml_directory);
		//modification du nom du fichier xml de pour le frontal pour prendre en compte les priorites
		$xml_cancel_file=$xml_directory."/P".$this->t_job["JOB_PRIORITE"]."_".str_replace(array('-',' ',':'),'',$this->t_job["JOB_DATE_CREA"])."_".kDatabaseName."_".sprintf('%08d',$this->t_job["ID_JOB"]).".xml";
		
		file_put_contents($xml_cancel_file,'');
	}
	
	function makeDeleteXml(){
		$xml_directory=jobDeleteDir.'/'.$this->t_module['MODULE_NOM'];
		if(!is_dir($xml_directory))
			mkdir($xml_directory);
		//modification du nom du fichier xml de pour le frontal pour prendre en compte les priorites
		$xml_cancel_file=$xml_directory."/P".$this->t_job["JOB_PRIORITE"]."_".str_replace(array('-',' ',':'),'',$this->t_job["JOB_DATE_CREA"])."_".kDatabaseName."_".sprintf('%08d',$this->t_job["ID_JOB"]).".xml";
		
		file_put_contents($xml_cancel_file,'');
	}

	// VP (7/01/09) : ajout méthode waitForJob()
	/** Attend la fin d'un job
			/!\ NE PASSE PAR UPDATEJOBS
	* 	IN : $timeout en secondes (option)
	* 	OUT : objet màj
	*/
	function waitForJob($timeout=600){
		$time_start = time();
		if(empty($timeout)) $timeout=3600; // 1 heure max
		$time_end=$time_start + $timeout;
		$_finJob=false;

		while(!$_finJob && time()<$time_end){
			// Mise à jour objet (au cas où updateJobs est déjà passé)
			$this->getJob();
			if($this->t_job["JOB_ID_ETAT"]< jobErreur){
			// Lecture log
				if($this->readLog($log)){
					if(strpos($log,"Erreur")>0){
						$_finJob=true;
					}else if(strpos($log,"Fin")>0){
						$_finJob=true;
					}
				}
			}else{
				$_finJob=true;
			}
		}
		if($_finJob) return true;
		else{
			if(time()>=$time_end) {
				$this->error_msg = kErrorTimeout;
			}
			return false;
		}
	}

	/** Attend la fin d'un job
     /!\ NE PASSE PAR UPDATEJOBS
     * 	IN : $timeout en secondes (option)
     * 	OUT : objet màj
     */
    function getMatObj() {
        if (isset($this->t_job["JOB_ID_MAT"]) && !empty($this->t_job["JOB_ID_MAT"]))
        {
            $matObj=new Materiel();
            $matObj->t_mat['ID_MAT']=$this->t_job["JOB_ID_MAT"];
            $matObj->getMat();
        }
        else
        {
            if($this->t_job["ID_JOB_PREC"]!=0){
				$jobPrec=new Job();
				$jobPrec->t_job["ID_JOB"]=$this->t_job["ID_JOB_PREC"];
				$jobPrec->getJob();
				if (!empty($jobPrec->t_job["JOB_ID_MAT"])) {
					$matPrec = new Materiel;
					$matPrec->t_mat['ID_MAT'] = $jobPrec->t_job["JOB_ID_MAT"];
					$matPrec->getMat();
					$matObj=Materiel::getMatByMatNom($jobPrec->t_job["JOB_OUT"], $matPrec->t_mat['MAT_CHEMIN']);
					return $matObj;
				}
			}
			$matObj=Materiel::getMatByMatNom($this->t_job["JOB_IN"]);
        }
        return $matObj;
    }

	// VP (10/10/08) : ajout fonction get_mail_user() pour chercher le mail de l'usager qui a lancé le job
	function get_mail_user(){
		global $db;
        // VP 10/10/12 : rappatriement mail usager basé sur JOB_ID_USAGER
		//return $db->GetOne("select US_SOC_MAIL from t_usager,t_session2_ado tsa where t_usager.ID_USAGER=tsa.EXPIREREF and tsa.SESSKEY='".$this->t_job["JOB_ID_SESSION"]."'");
		return $db->GetOne("select US_SOC_MAIL from t_usager where t_usager.ID_USAGER=".intval($this->t_job["JOB_ID_USAGER"]));
	}

	/**
	 * lancement des jobs suivants
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function launchNextJobs(){
		global $db;
        if(!empty($this->t_job['ID_JOB'])){
            $id_jobs_suivants=$db->GetAll('SELECT ID_JOB FROM t_job WHERE ID_JOB_PREC='.intval($this->t_job['ID_JOB']));
            
            if (!empty($id_jobs_suivants))
            {
                foreach ($id_jobs_suivants as $id_job_suivant)
                {
                    $job_suivant=new Job();
                    $job_suivant->t_job['ID_JOB']=$id_job_suivant['ID_JOB'];
					if($this->flag_runSyncJob){
						$job_suivant->flag_runSyncJob = $this->flag_runSyncJob;
					}
                    $job_suivant->getJob();
                    if(defined("kJobPlateforme") && $this->t_job['JOB_PLATEFORME']!= kJobPlateforme){
                        $this->t_job['JOB_ID_ETAT']==0;
                    } elseif($job_suivant->makeXML()){
                        $job_suivant->t_job['JOB_ID_ETAT']=jobAttente;
                    }else{
                        $job_suivant->t_job['JOB_ID_ETAT']=jobErreur;
                        $job_suivant->t_job['JOB_MESSAGE']=$job_suivant->error_msg;
                    }
                    $job_suivant->save();
                }
            }
        }
	}

	// VP 17/11/09 : ajout paramètres destinataire, sujet et xsl à send_mail
	function send_mail($dest="",$sujet="",$xsl="print/jobMail.xsl",$withChilds=0){
		trace("job : send_mail xsl=$xsl");
       	$xml=$this->xml_export(0,0,'',$withChilds);
       
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";

       	$frontiere = "_----------=_parties_".md5(uniqid (rand()));
        $param = array("boundary"=>$frontiere);
      	$message= TraitementXSLT($xml,getSiteFile("designDir",$xsl),$param,1);
        
		ob_start();
        eval("?>". str_replace(array('||'," ' "),array(chr(10),"'"),$message)."<?");
        $message=ob_get_contents();
		ob_end_clean();

		// VP (10/10/08) : ajout appel fonction get_mail_user();
		if(empty($dest)) $dest=gMailJob.', '.$this->get_mail_user();
		
		//PC 31/10/12 : encode les caracteres pour les mail de type "NOM <mail@host.com>"
		$dest = str_replace("&amp;", "&", $dest);
		$dest = str_replace("&lt;", "<", $dest);
		$dest = str_replace("&gt;", ">", $dest);
		
		if(empty($sujet)) $sujet=kMailFinTraitement.$this->t_job["JOB_NOM"];
        //NOTE : conversion en ISO-8859-1 pour compatibilité maximum avec les clients mail (notamment EUDORA & HOTMAIL)
		//update VG 09/12/2011
		//$headers="Content-Type: text/plain; charset=ISO-8859-1\r\nFrom:".gMail;
		$headers="From:".gMail."\nMIME-Version: 1.0\nContent-Type: multipart/alternative; boundary=\"".$frontiere."\"";
        // VP 22/06/12 : remplacement ’ par '
        //$message=str_replace("‘","'",$message); // Ne fonctionne pas (utf-8)
        $message=str_replace(chr(226).chr(128).chr(153),"'",$message);
		//update VG 16/10/2012 : on passe désormais par mb_send_mail (et ses fonctions associées) fait pour spécifier les encodages de caractères, pour plus de fiabilité.
		//$message=utf8_decode($message);
		//$sujet=utf8_decode($sujet);
		mb_language("en");
		$message = mb_convert_encoding($message, "ISO-8859-1","UTF-8");
//		$sujet = mb_convert_encoding($sujet, "ISO-8859-1","UTF-8"); //B.RAVI 2016-11-25 y'a déja un utf8_decode dans $msg->send_mail
		
		include_once(modelDir.'model_message.php');
		$msg = new Message();
		return $msg->send_mail($dest, gMail, $sujet, $message, "multipart/alternative", $frontiere);
	}

 	/** Export XML de l'objet JOB
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="",$withChilds=0) {


        $content="";
        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_job>\n";
		// JOB
		foreach ($this->t_job as $fld=>$val) {
			// VP 23/04/10 : inclusion balise <xml> dans JOB_PARAM
			if(strtoupper($fld)=='JOB_PARAM')
				$content.=$indent."\t<".strtoupper($fld)."><xml>".$val."</xml></".strtoupper($fld).">\n";
			else 
				$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}
		$content.=$indent."</t_job>\n";

		// VP 23/11/09  export mat ou doc
		require_once(modelDir.'model_materiel.php');
		require_once(modelDir.'model_doc.php');
		$matObj=new Materiel();
		$docObj=new Doc();
		
		if($withChilds > 0 ){
			$res = getItem('t_job','ID_JOB',array('ID_JOB_GEN'=>$this->t_job['ID_JOB']),array('ID_JOB'=>'ASC'),0);
			$xml_childs = ""; 
			if($res && is_array($res)){
				foreach($res as $row){
					if(isset($row['ID_JOB'])){
						$child_job = new Job() ; 
						$child_job->t_job['ID_JOB'] = $row['ID_JOB'];
						$child_job->getJob() ; 
						if(isset($child_job->t_job['JOB_NOM'])){
							$xml_childs .="<JOB_CHILD>".$child_job->xml_export($entete,$encodage,$indent."\t",($withChilds-1))."</JOB_CHILD>";
						}
						unset($child_job);
					}
				}
				if(!empty($xml_childs)){
					$content.= $prefix."<JOB_CHILDS>\n";
					$content.= $prefix."\t".$xml_childs;
					$content.= $prefix."</JOB_CHILDS>\n";
				}
			}
		}
		
		// JOB_IN
		if($this->t_job['JOB_IN']!=''){
			if (isset($this->t_job['JOB_ID_MAT']) && !empty($this->t_job['JOB_ID_MAT']))
			{
				$matObj->t_mat['ID_MAT']=$this->t_job['JOB_ID_MAT'];
				$matObj->getMat();
			}
			else
				$matObj=Materiel::getMatByMatNom($this->t_job['JOB_IN']);
				//$matObj->t_mat['MAT_NOM']=$this->t_job['JOB_IN'];
				
			if($matObj!=null && $matObj->checkExist()){
				$matObj->getMat();
				$matObj->getDocs(true);
				$xmlin=$matObj->xml_export(0,0);
				$content.= $prefix."<JOB_IN>\n";
				$content.= $prefix."\t".$xmlin;
				$content.= $prefix."</JOB_IN>\n";
			}else{
				//VP 27/11/09 : vérification existance par cote
				$docObj->t_doc['DOC_COTE']=$this->t_job['JOB_IN'];
				$id_doc=$docObj->checkExistCote();
				if($id_doc){
					$docObj->t_doc['ID_DOC']=$id_doc;
					$docObj->getDocFull(1,1,1,0,0,0,0,0,0,1);
					$xmlin=$docObj->xml_export(0,0);
					$content.= $prefix."<JOB_IN>\n";
					$content.= $prefix."\t".$xmlin;
					$content.= $prefix."</JOB_IN>\n";
				}
			}
		}
		// JOB_OUT
		if($this->t_job['JOB_OUT']!=''){
			//$matObj->t_mat['ID_MAT']=$this->t_job['JOB_OUT'];
			$matObj=Materiel::getMatByMatNom($this->t_job['JOB_OUT']);
			if($matObj!=null && $matObj->checkExist()){
				$matObj->getMat();
				$matObj->getDocs(true);
				$xmlout=$matObj->xml_export(0,0);
				$content.= $prefix."<JOB_OUT>\n";
				$content.= $prefix."\t".$xmlout;
				$content.= $prefix."</JOB_OUT>\n";
			}else{
				$docObj->t_doc['DOC_COTE']=$this->t_job['JOB_OUT'];
				$id_doc=$docObj->checkExistCote();
				if($id_doc){
					$docObj->t_doc['ID_DOC']=$id_doc;
					$docObj->getDocFull(1,1,1,0,0,0,0,0,0,1);
					$xmlout=$docObj->xml_export(0,0);
					$content.= $prefix."<JOB_OUT>\n";
					$content.= $prefix."\t".$xmlout;
					$content.= $prefix."</JOB_OUT>\n";
				}
			}
		}
		unset($matObj);
		unset($docObj);
        // VP 1/06/12 : ajout export panier et doc
        if(preg_match("|<ID_LIGNE_PANIER>(.*)</ID_LIGNE_PANIER+>|",$this->t_job["JOB_PARAM"],$matches)) {
            $id_ligne_panier=$matches[1];
            // Export panier 
            require_once(modelDir.'model_panier.php');
            $objPanier = new Panier();
            $objPanier->getPanierDoc(false,$id_ligne_panier);
            $objDoc = new Doc();
            $objDoc->t_doc['ID_DOC']=$objPanier->t_panier_doc[0]['ID_DOC'];		
            if(preg_match("|<ID_LANG>(.*)</ID_LANG+>|",$this->t_job["JOB_PARAM"],$matches)) {
                $objDoc->t_doc['ID_LANG']=$matches[1];
            }   	
            $objDoc->getDocFull(1,1,1,1,1,1,0,0,0,1);
			$objDoc->getDocAcc();
            $content.=$objDoc->xml_export();
            $content.=$objPanier->xml_export_panier_doc();
            unset($objDoc);
            unset($objPanier);
        }elseif(preg_match("|<ID_DOC>(.*)</ID_DOC+>|",$this->t_job["JOB_PARAM"],$matches)) {
            $id_doc=$matches[1];
            // Export doc 
            require_once(modelDir.'model_doc.php');
            $objDoc = new Doc();
            $objDoc->t_doc['ID_DOC']=$id_doc;
            if(preg_match("|<ID_LANG>(.*)</ID_LANG+>|",$this->t_job["JOB_PARAM"],$matches)) {
                $objDoc->t_doc['ID_LANG']=$matches[1];
            }   
            $objDoc->getDocFull(1,1,1,1,1,1,0,0,0,1);
			$objDoc->getDocAcc();
            $content.=$objDoc->xml_export();
            unset($objDoc);
        }
        if(preg_match("|<ID_PANIER>(.*)</ID_PANIER+>|",$this->t_job["JOB_PARAM"],$matches)) {
            $id_panier=$matches[1];
            // Export panier 
            require_once(modelDir.'model_panier.php');
            $objPanier = new Panier();
            $objPanier->t_panier['ID_PANIER']=$id_panier;
            $objPanier->getPanier(null, false);
            $content.=$objPanier->xml_export();
            unset($objPanier);
        }
		$log = new Logger("job.xml");
		$log->Log($content);

		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
	
	
	function updateJob(&$arrJob = array()){
		global $db ; 
		
		$_hasChanged=false;
		// VP 2/07/13 : test entity (mat/doc_acc)
		$entity="mat";
		if(preg_match("|<entity>(.*)</entity+>|i",$this->t_job["JOB_PARAM"],$matches)) {
			$entity=$matches[1];
		}
		// trace("Job:updateJobs ".$this->t_job['ID_JOB']);
		switch($this->t_job["JOB_ID_ETAT"]){
			case jobAttente:
			case jobEnCours:
				// Lecture log et stockage du contenu dans $log
				//echo "[en cours] pre read log : ".microtime(true)."\n";
				if($this->readLog($log, true)){
					require_once(modelDir.'model_materiel.php');
					$donnees_lieu_mat=Materiel::setLieuByIdMat($this->t_job["JOB_OUT"]);
					$out_folder=$donnees_lieu_mat['LIEU_PATH'];

					// trace("Job:updateJob readLog log : ".$this->t_job['ID_JOB']." ".$log);
					//echo "[en cours] post read log : ".microtime(true)."\n";
					$data=explode("\n",$log);
					// Traitement du nouveau statut
					if(strpos($log,"Erreur")>0){
						trace('Erreur : '.$this->t_job["JOB_NOM"]);
						// Mise à jour statut
						// VP (19/1/09) : mise à jour date en cas d'erreur
						$this->t_job["JOB_DATE_FIN"]=date("Y-m-d H:i:s");
						if($this->t_job["JOB_DATE_DEBUT"]=="0000-00-00 00:00:00"){
							$this->t_job["JOB_DATE_DEBUT"]=substr($data[0],0,19);
						}
						$this->t_job["JOB_MESSAGE"]=substr($log,strpos($log,"Erreur"));
						$this->t_job["JOB_ID_ETAT"]=jobErreur;
						$_hasChanged=true;
					}else if(strpos($log,"Fin traitement")>0){
						$this->t_job["JOB_MESSAGE"]="";
						$this->t_job["JOB_DATE_FIN"]=date("Y-m-d H:i:s");
						if($this->t_job["JOB_DATE_DEBUT"]=="0000-00-00 00:00:00"){
							$this->t_job["JOB_DATE_DEBUT"]=substr($data[0],0,19);
						}
						trace("Fin traitement : ".$this->t_job["JOB_NOM"]);
						switch($this->t_module["MODULE_TYPE"]){
							case "ENCOD":
							case "NUM":
								require_once(modelDir.'model_materiel.php');
								
								$donnees_lieu_mat=Materiel::setLieuByIdMat($this->t_job["JOB_OUT"]);
								$out_folder=$donnees_lieu_mat['LIEU_PATH'];
								trace('$out_folder :'.$out_folder);
								// VP 5/10/10 : changement FOLDER en FOLDER_OUT
									if(preg_match("|<FOLDER_OUT>(.*)</FOLDER_OUT+>|",$this->t_job["JOB_PARAM"],$matches)){
										// Le fichier de sortie est dans le FOLDER et pas dans kVideosDir
										$folder=$matches[1];	
										// MS 04/04/14 - cas de multiples images DVD => un zip est préparé, et on update donc le JOB_OUT
										if(strpos($log,"DVD zipped")>0 && file_exists($folder.(str_replace('.iso','.zip',$this->t_job["JOB_OUT"])))){
											$this->t_job["JOB_ID_ETAT"]=jobFini;
											$this->t_job["JOB_OUT"] = (str_replace('.iso','.zip',$this->t_job["JOB_OUT"]));
											$_hasChanged=true;
										}else if(file_exists($folder.$this->t_job["JOB_OUT"])){
											$this->t_job["JOB_ID_ETAT"]=jobFini;
											$_hasChanged=true;
										}else{
											$this->t_job["JOB_MESSAGE"]=kMaterielFichierManquant." ".$folder.$this->t_job["JOB_OUT"];
											$this->t_job["JOB_ID_ETAT"]=jobErreur;
											$_hasChanged=true;
										}
                                    }elseif($entity=="doc_acc" && file_exists(kDocumentDir.$this->t_job["JOB_OUT"])){
                                        // VP 2/07/13 : cas doc_acc

                                        require_once(modelDir.'model_docAcc.php');
                                        // Création OUT par duplication IN
                                        $myDA=new DocAcc;
                                        $myDA->t_doc_acc['ID_DOC_ACC']=$this->t_job["JOB_ID_MAT"];
                                        $myDA->getDocAcc();
                                        $myDA->t_doc_acc['ID_DOC_ACC']=NULL;
                                        $myDA->t_doc_acc['DA_FICHIER']=getFileFromPath($this->t_job["JOB_OUT"]);
                                        $myDA->t_doc_acc['DA_TITRE']=$this->t_job["JOB_OUT"];
										
										try{
											$xml_params = xml2array($this->t_job['JOB_PARAM']);
											if(isset($xml_params['param']))
												$xml_params = $xml_params['param'];
											$myDA->mapFields($xml_params);
										}catch(Exception $e){
											trace("updateJobs - doc_acc - mapFields xml_params crash - ".$e->getMessage());
										}
										
                                        foreach ($_SESSION['arrLangues'] as $lg) {
                                            $myDA->t_doc_acc['ID_LANG']=$lg;
                                            $ok=$myDA->create(false);
                                        }
										 if($ok){
											// VP 10/07/13 : Sauvegarde éventuelle des valeurs
											$myDA->saveDocAccVal();
										}
										
                                        // Extraction texte pour pdf
                                        $fileSrc=$myDA->getFilePath();
                                        if(getExtension($fileSrc)=='pdf') $myDA->extractText($fileSrc);

                                        if ($ok) {
                                            $this->t_job["JOB_ID_ETAT"]=jobFini;
                                            $_hasChanged=true;
                                        } else {
                                            $this->t_job["JOB_MESSAGE"]=$myDA->error_msg;
                                            $this->t_job["JOB_ID_ETAT"]=jobErreur;
                                            $_hasChanged=true;						
                                        }
										
										// MS 03/10/13 Si le job courant précède un autre job, alors on update le JOB_ID_MAT du job suivant avec l'ID_MAT du matériel créé.
										$item_job_suiv = getItem("t_job",array('ID_JOB','JOB_ID_MAT'),array("ID_JOB_PREC"=>$this->t_job["ID_JOB"]),array('ID_JOB'=>"DESC"));
										if(!empty($item_job_suiv) && $item_job_suiv['JOB_ID_MAT']==0){
											$jobSuiv = new Job();
											$jobSuiv->t_job['ID_JOB'] = $item_job_suiv['ID_JOB'];
											$jobSuiv->getJob();
											$jobSuiv->t_job['JOB_ID_MAT'] = $myDA->t_doc_acc['ID_DOC_ACC'];
											$jobSuiv->save();
										}
                                        unset($myDA);
									}elseif(file_exists($out_folder.$this->t_job["JOB_OUT"])){
										// Intégration du fichier en base pour les traitements transcodage
										require_once(modelDir.'model_doc.php');
										
										// MS 28.02.18 - fix pour prise en compte des paramètres passés en mode "INHERIT_", 
										// => dans class_jobProcess, les paramètres sont interprétés en retirant le "INHERIT_", donc idem ici, pour l'instant uniquement sur certaines infos DOC & MATs passés par les params job,
										// à généraliser au besoin. 
										$job_params = str_replace(array('<INHERIT_','</INHERIT_'),array('<','</'),$this->t_job['JOB_PARAM']);
										
										// Récupération objet mat in
                                        $matInObj=$this->getMatObj();
                                        if(!empty($matInObj->t_mat['MAT_NOM']))
											$matInObj->id_mat_ref=$matInObj->t_mat['MAT_NOM'];
										unset($id_doc);
										if(preg_match("|<ID_DOC>(.*)</ID_DOC+>|",$job_params,$matches)) $id_doc=$matches[1];
										
										if (isset($matInObj) && $matInObj!=null) // dans le cas d'un job de diffusion => pas de job in matInObj null
										{									
											$matInObj->getDocMat();
											$saveDocMat=$matInObj->t_doc_mat;
											//echo "ID_DOC >>> ".$id_doc;
											// Sauvegarde valeur avant initialisation nouveau mat
											$mat=array();
											if(preg_match("|<MAT_COTE_ORI>(.*)</MAT_COTE_ORI+>|",$job_params,$matches))
												$mat['MAT_COTE_ORI']=$matches[1];
											else
												$mat['MAT_COTE_ORI']=$matInObj->t_mat['MAT_NOM'];
                                            $mat['MAT_ID_ORI']=$matInObj->t_mat['ID_MAT']; // identifiant matériel d'origine
											$mat['MAT_TCIN']=$matInObj->t_mat['MAT_TCIN'];
											$mat['MAT_TCOUT']=$matInObj->t_mat['MAT_TCOUT'];
											$mat['MAT_TCFORCE']=$matInObj->t_mat['MAT_TCFORCE'];
											$mat['MAT_CHEMIN']=$matInObj->t_mat['MAT_CHEMIN'];
											// ajout transfert du mat_chemin d'origine aux fichiers transcodés
											$mat['MAT_CHEMIN_ORI']=$matInObj->t_mat['MAT_CHEMIN_ORI'];
											$mat['MAT_ID_GROUPE_MAT']=$matInObj->t_mat['MAT_ID_GROUPE_MAT'];
											$mat['MAT_ID_FONDS']=$matInObj->t_mat['MAT_ID_FONDS'];
											// Mise à jour flag MAT_NUM
											if($this->t_module["MODULE_TYPE"]=="NUM"){
												$matInObj->t_mat['MAT_NUM']="1";
												$matInObj->save();
											}
																						
										}
										// MS - possibilité de forcer le MAT_LIEU si il se trouve dans les params du job générant le mat
										if(preg_match("|<MAT_LIEU>(.*)</MAT_LIEU+>|",$job_params,$matches)){
											$mat['MAT_LIEU']=$matches[1];
										}
                      
                                        // Création nouveau matériel
                                        $matObj=new Materiel();
                                        $ok=$matObj->ingest($out_folder.$this->t_job["JOB_OUT"],$mat,"",false, $infos);
                     
                                        if ($ok) {
											$matObj->id_mat_ref=$matObj->t_mat['MAT_NOM'];// Sinon il ne se sauve pas
                                            //Mise à jour des TC
                                            // VP (29/9/08) : Offset éventuel, si MAT_TCFORCE de l'origine est à 1 ou si tcin est à 0
                                            if(($mat['MAT_TCFORCE']=='1')||($infos['tcin']=="00:00:00:00")){
											   // MS 13/12/2012 - récupération de l'offset à partir du TCIN du matériel d'origine
											   $_offset=$mat['MAT_TCIN'];

                                                // On regarde si le module Cut n'a pas renvoyé un TC ou s'il n'est pas dans Param
                                                if(strpos($log,"Retour Cut OK debut:")>0) {
                                                    $_in=substr($log,strpos($log,"Retour Cut OK debut:")+21,11);
                                                }elseif(preg_match("|<TcIn>(.*)</TcIn+>|i",$this->t_job["JOB_PARAM"],$matches)) $_in=$matches[1];
                                                else $_in=$infos['tcin'];

                                                $matObj->t_mat['MAT_TCIN']=frameToTC(tcToFrame($_in) + tcToFrame($_offset));
                                                // On regarde si TC est dans param sinon on le calcule à partir de tcin
                                                // en effet la durée retournée par ffmpeg et infomedia est fausse si découpage sliceOfMov
                                                if(preg_match("|<TcOut>(.*)</TcOut+>|i",$this->t_job["JOB_PARAM"],$matches))$matObj->t_mat['MAT_TCOUT']=frameToTC(tcToFrame($matches[1]) + tcToFrame($_offset));
                                                else $matObj->t_mat['MAT_TCOUT']=secDecToTc(tcToSecDec($_in) + $infos['duration'] + tcToSecDec($_offset));
                                            } elseif(!empty($infos['tcout'])) {
                                                $matObj->t_mat['MAT_TCIN']=$infos['tcin'];
                                                $matObj->t_mat['MAT_TCOUT']=$infos['tcout'];
                                            }
											// GT : définition du mat_type du nouveau matériel
											if(preg_match("|<MAT_TYPE>(.*)</MAT_TYPE+>|",$job_params,$matches))
												$matObj->t_mat['MAT_TYPE']=$matches[1];
												
											$matObj->id_mat_ref = $matObj->t_mat['MAT_NOM'];
                                            $matObj->save();
											
											
											if($matObj->t_mat['MAT_ID_MEDIA'] == 'D' && strpos($matObj->t_mat['MAT_NOM'],'.pdf')!== false && strpos($matInObj->t_mat['MAT_NOM'],'.pdf')===false){
												// MS 27-08-14
												// On est dans le cas d'un document Office que l'on transforme en PDF,
												// pour l'instant nous ne pouvons pas compter le nombre de page directement depuis l'original, 
												// on update donc l'info MAT_NB_PAGES du master à partir du MAT_NB_PAGES du pdf visio
												$matInObj->t_mat['MAT_NB_PAGES'] = $matObj->t_mat['MAT_NB_PAGES'];
												$ok2 =$matInObj->save();
											}
											
											if(isset($id_doc)  && preg_match("|<DOC_ID_TYPE_DOC>(.*)</DOC_ID_TYPE_DOC+>|",$this->t_job["JOB_PARAM"],$matches)){
												$doc = new Doc($id_doc);
												$doc->t_doc['DOC_ID_TYPE_DOC'] = $matches[1];
												//trace("doc_id_type_doc updated : ".$doc->t_doc['DOC_ID_TYPE_DOC']);
												$doc->save() ; 
											}
											
											
											if(isset($id_doc) && preg_match("|<DESACTIVE_IN>(.*)</DESACTIVE_IN+>|",$this->t_job["JOB_PARAM"],$matches)){
												$doc = new Doc();
												$doc->t_doc['ID_DOC'] = $id_doc;
												$doc->getDocMat();
												if($matches[1]){
													foreach($doc->t_doc_mat as $idx=>&$dmat){
														if($dmat['ID_MAT'] == $matInObj->t_mat['ID_MAT']){
															$dmat['DMAT_INACTIF'] = 1;
															$id_ref = $idx;
															break ; 
														}
													}
												}

												$doc->saveDocMat();
												unset($doc);
											}
											$matObj->t_doc_mat=$saveDocMat;
                                            
                                            if (empty($matObj->t_doc_mat) && !empty($id_doc)) {
                                                $matObj->t_doc_mat[0]['ID_DOC']=$id_doc;
												if(isset($matObj->t_mat['MAT_TCIN']) && isset($matObj->t_mat['MAT_TCOUT'])){
													$matObj->t_doc_mat[0]['DMAT_TCIN'] = $matObj->t_mat['MAT_TCIN'];
													$matObj->t_doc_mat[0]['DMAT_TCOUT'] = $matObj->t_mat['MAT_TCOUT'];
												}
                                            }
                                            else
                                            {
                                                // VP 22/03/2013 ajout & pour mettre à jour matObj->t_doc_mat
                                                foreach ($matObj->t_doc_mat as $idx=>&$docMat) {
                                                    //si on a passé un ID_DOC alors on ne crée qu'un tableau concernant cet ID_DOC
                                                    if ($id_doc && $docMat['ID_DOC']!=$id_doc) {
                                                        unset($matObj->t_doc_mat[$idx]);
                                                    }
                                                    else {
                                                        $docMat["ID_MAT"]=$matObj->t_mat['ID_MAT'];
                                                        $docMat["ID_DOC_MAT"]='';
														if(preg_match("|<DESACTIVE_IN>(.*)</DESACTIVE_IN+>|",$this->t_job["JOB_PARAM"],$matches)){
															$docMat["DMAT_INACTIF"]=0;
														}
                                                        $docMat["DMAT_LIEU"]='';// Moche, on vide DMAT_LIEU pour garder distinction Original / copie (INA)
                                                    }
                                                }
                                            }

                                            $db->Execute("DELETE from t_doc_mat WHERE ID_MAT=".intval($matObj->t_mat['ID_MAT']));
											$matObj->saveDocMat();
                                            // VP 9/10/12 : Mise à jour Etat archivage
                                            $matObj->updateEtatArch();
                                            // Mise à jour flag doc num
											require_once(modelDir.'model_doc.php');
											$matObj->getDocs();
											foreach ($matObj->t_doc_mat as $idx=>$dm) {
												$dm['DOC']->updateDocNum();
											}
											$this->t_job["JOB_ID_ETAT"]=jobFini;
											$_hasChanged=true;
											
											// MS 03/10/13 Si le job courant précède un autre job, alors on update le JOB_ID_MAT du job suivant avec l'ID_MAT du matériel créé.
											$item_job_suiv = getItem("t_job",array('ID_JOB','JOB_ID_MAT'),array("ID_JOB_PREC"=>$this->t_job["ID_JOB"]),array('ID_JOB'=>"DESC"));
											if(!empty($item_job_suiv) && $item_job_suiv['JOB_ID_MAT']==0){
												$jobSuiv = new Job();
												$jobSuiv->t_job['ID_JOB'] = $item_job_suiv['ID_JOB'];
												$jobSuiv->getJob();
												$jobSuiv->t_job['JOB_ID_MAT'] = $matObj->t_mat['ID_MAT'];
												$jobSuiv->save();
											}
											
										} else {
											$this->t_job["JOB_MESSAGE"]=$matObj->error_msg;
											$this->t_job["JOB_ID_ETAT"]=jobErreur;
											$_hasChanged=true;						
										}
										// MS - 28.03.18 - Pour les cas d'exec job par frontal synchrone necessitant des traitements async lancé en followup, on conserve une référence vers le mat généré. 
										if(isset($this->flag_runSyncJob) && $this->flag_runSyncJob){
											$this->outMat = $matObj ;
										}
										
										//echo("MAT ".$matObj->t_mat['ID_MAT']." >>> ".$matObj->error_msg);
										unset($matObj);
										unset($matInObj);
									} else {
										// Mise à jour statut
										$this->t_job["JOB_MESSAGE"]=kMaterielFichierManquant.$out_folder.$this->t_job["JOB_OUT"];
										$this->t_job["JOB_ID_ETAT"]=jobErreur;
										$_hasChanged=true;
									}
								break;
								
							case "STORY":
								require_once(modelDir.'model_imageur.php');
								require_once(modelDir.'model_materiel.php');
                               // trace("updateJob Story ".$entity);
								
								if($entity=="doc_acc" && file_exists(kDocumentDir.$this->t_job["JOB_OUT"])){
									if ($arr_files = scandir(kDocumentDir.$this->t_job["JOB_OUT"])){
										foreach($arr_files as $file) {
											if(strpos($file,$this->t_job["JOB_OUT"]) === 0 ){
												// MS 28.02.18 - fix pour prise en compte des paramètres passés en mode "INHERIT_", 
												// => dans class_jobProcess, les paramètres sont interprétés en retirant le "INHERIT_", donc idem ici, pour l'instant uniquement sur certaines infos DOC & MATs passés par les params job,
												// à généraliser au besoin. 
												$job_params = str_replace(array('<INHERIT_','</INHERIT_'),array('<','</'),$this->t_job['JOB_PARAM']);
												
												// Création OUT par duplication IN
												require_once(modelDir.'model_docAcc.php');
												$myDA=new DocAcc;
												$myDA->t_doc_acc['ID_DOC_ACC']=$this->t_job["JOB_ID_MAT"];
												$myDA->getDocAcc();
												$myDA->t_doc_acc['ID_DOC_ACC']=NULL;
												$myDA->t_doc_acc['DA_FICHIER']=getFileFromPath($file);
												$myDA->t_doc_acc['DA_CHEMIN']=$this->t_job["JOB_OUT"];
												$myDA->t_doc_acc['DA_TITRE']=$file;

												try{
													$xml_params = xml2array($job_params);
													if(isset($xml_params['param'])){
														$xml_params = $xml_params['param'];
													}
													// workaround pour gérer le cas ou on a à la fois "VI_TYPJ" et "INHERIT_VI_TYPJ" par exemple (vient des règles de propagations de Processus:createJobs qui sont un peu aberrantes dans certains cas ) et ne pas créer de doublon qui seront ensuite rejeter par saveDocAccVal
													foreach($xml_params as $idx=>$param){
														if(is_array($param)){
															$xml_params[$idx] = array_unique($param);
															if(count($xml_params[$idx]) == 1){
																$xml_params[$idx] = reset($param);
															}
														}														
													}
													$myDA->mapFields($xml_params);
												}catch(Exception $e){
													trace("updateJobs - doc_acc - mapFields xml_params crash - ".$e->getMessage());
												}												
												
												foreach ($_SESSION['arrLangues'] as $lg) {
													$myDA->t_doc_acc['ID_LANG']=$lg;
													$ok=$myDA->create(false);
												}
												if($ok){
													// VP 10/07/13 : Sauvegarde éventuelle des valeurs
													$myDA->saveDocAccVal();
												}
												// Extraction texte pour pdf
												$fileSrc=$myDA->getFilePath();
												if(getExtension($fileSrc)=='pdf') $myDA->extractText($fileSrc);

												if ($ok) {
													$this->t_job["JOB_ID_ETAT"]=jobFini;
													$_hasChanged=true;
												} else {
													$this->t_job["JOB_MESSAGE"]=$myDA->error_msg;
													$this->t_job["JOB_ID_ETAT"]=jobErreur;
													$_hasChanged=true;						
												}
												unset($myDA);
											}
										}
									}
								}else{
									$matObj=$this->getMatObj();
									$matObj->id_mat_ref=$matObj->t_mat['MAT_NOM'];// Sinon il ne se sauve pas
									//$matObj->t_mat['MAT_ID_IMAGEUR']=$myImageur->t_imageur['ID_IMAGEUR'];
									$matObj->save();
									//trace(print_r($matObj,true));
									$myImageur=new Imageur;
									$myImageur->t_imageur['ID_IMAGEUR']=$matObj->t_mat['MAT_ID_IMAGEUR'];
									$myImageur->getImageur();
									// VP 8/04/10 : Suppression images -> fait dans makeXML
									//$myImageur->deleteAllImages(true);

									if (!$myImageur->checkExist()) $myImageur->create(); //cr�ation �ventuelle imageur
									// Association imageur au materiel
									//$matObj= new Materiel;
									// R�cup�ration objet mat in
									//$matObj->t_mat['ID_MAT']= $this->t_job["JOB_IN"];
									//$matObj->getMat();
									
									// Flag pdf
									$ispdf=(getExtension($matObj->t_mat['MAT_NOM'])=="pdf");
									// By VP (28/8/08) : ajout calcul offset story
									$offset_story=(empty($matObj->t_mat['MAT_TCIN'])?0:tcToFrame($matObj->t_mat['MAT_TCIN']));
									//Parcours r�pertoire
									$cnt=0;
									if(preg_match("|<iteration>(.*)</iteration+>|i",$this->t_job["JOB_PARAM"],$matches)) $rate=$matches[1];
									else $rate=gStoryboardIntervalleDefaut; // valeur par d�faut
									// if ($handle = opendir(kStoryboardDir.$myImageur->t_imageur['IMAGEUR'])) {
										// while (false !== ($file = readdir($handle))) {
									// MS - scandir à la place de readdir => permet de trier par nom de dossier, et non par date de création
									if ($arr_files = scandir(kStoryboardDir.$myImageur->getImageurPath())){
										foreach($arr_files as $file) {
											if (strpos($file,$myImageur->t_imageur['IMAGEUR'])===0) { //Ce filtre est destin� � ne pas traiter des images ajout�es manuellement
												// On essaie de g�n�rer un timecode � partir du nom du fichier (8 chiffres apr�s _)
												$_file_=explode("_",stripExtension($file)); //explode _
												if($ispdf){
													$TC="";
												}elseif (strlen($_file_[count($_file_)-1])==8 && is_numeric($_file_[count($_file_)-1])) { //ok, on a bien 8 chiffres
													$TC=substr($_file_[count($_file_)-1],0,2).":".substr($_file_[count($_file_)-1],2,2).":".substr($_file_[count($_file_)-1],4,2).":".substr($_file_[count($_file_)-1],6,2);
												} else { // pas d'extraction de timecode, il faut en calculer un sur la base des it�rations.
													$sec=$cnt*$rate;
													// MS - FFMPEG plus utilisé, la 2e image est bien à $cnt*$rate secondes.
													//if ($cnt==1) $sec=1; //BUG FFMPEG => LA 2EME IMAGE EST TJS A 1s DU DEBUT (QQ SOIT le RATE)
													$TC=secToTime($sec).":00";
												}
								
												$myImg=new Image;
												//$myImg->t_image['IM_TC']=$TC;
												$myImg->t_image['IM_TC']=frameToTC(tcToFrame($TC)+$offset_story);
												$myImg->t_image['ID_IMAGEUR']=$myImageur->t_imageur['ID_IMAGEUR'];
												$myImg->t_image['ID_LANG']=$_SESSION['langue'];
												$myImg->t_image['IM_FICHIER']=$file;
												$myImg->t_image['IM_CHEMIN']=$myImageur->getImageurPath();
												$ok=$myImg->save();
												$myImg->saveVersions($_SESSION['arrLangues'],$ok);
												//update VP/VG 22/01/13 : sauvegarde de la 1ere image su storyboard
												if($ispdf){
													if($cnt==0){$id_image=$myImg->t_image['ID_IMAGE'];}
												}
												elseif ($cnt<2) {$id_image=$myImg->t_image['ID_IMAGE'];} // Image 2 par défaut
												//echo $myImg->error_msg;
												unset($myImg);
												$cnt++;
											}
										}
										// Mise � jour vignette
										require_once(modelDir.'model_doc.php');
										//$matObj->getDocs();
										$matObj->getDocMat();
										
										foreach ($matObj->t_doc_mat as $idx=>$dm) {
											//$dm['DOC']->saveImageur($myImageur->t_imageur['IMAGEUR']);
											//if(isset($id_image)) $dm['DOC']->saveVignette('doc_id_image',$id_image);
											// VP 12/10/09 : affectation de l'image en fonction du TC
											// VP 28/06/11 : affectation de l'image en fonction de la langue
											$docObj=new Doc;
											$docObj->t_doc['ID_DOC']=$dm['ID_DOC'];
											// VG 16/09/11 : on a désormais besoin de connaitre le media afin de déterminer la règle à appliquer
											$docObj->t_doc['DOC_ID_MEDIA']=$matObj->id_media;
											$docObj->getDoc() ; 
											if(empty($docObj->t_doc['DOC_ID_DOC_ACC'])){
												$docObj->saveVignette('doc_id_image',0,$matObj->t_mat['MAT_ID_IMAGEUR'],$dm['DMAT_TCIN'],$dm['DMAT_ID_LANG']);
											}
											unset($docObj);
										}
										$this->t_job["JOB_ID_ETAT"]=jobFini;
										$_hasChanged=true;
									} else {
										$this->t_job["JOB_MESSAGE"]=kErrorImageurCreationRepertoire;
										$this->t_job["JOB_ID_ETAT"]=jobErreur;
										$_hasChanged=true;
									}
									unset($matObj);
									unset($myImageur);
								}
								break;
								
							case "PAROL":
								require_once(modelDir.'model_materiel.php');
								if(preg_match("|<ID_DOC>(.*)</ID_DOC+>|",$this->t_job["JOB_PARAM"],$matches)) $id_doc=$matches[1];
								if(preg_match("|<FOLDER_OUT>(.*)</FOLDER_OUT+>|",$this->t_job["JOB_PARAM"],$matches)) $custom_folder_out=$matches[1];
								$this->t_job["JOB_ID_ETAT"]=jobFini;
								$cpt_file= 0;
								$extension=array();
								if(preg_match("|<file_xml>1</file_xml+>|i",$this->t_job["JOB_PARAM"]) || preg_match("|<file_full_xml>1</file_full_xml+>|i",$this->t_job["JOB_PARAM"])){
									if(file_exists(kVideosDir.$this->t_job["JOB_OUT"].'.xml')
										|| isset($custom_folder_out) && file_exists($custom_folder_out.$this->t_job["JOB_OUT"].'.xml')){
										$file_xml=1;$cpt_file++;$extension[]='xml';
									}
								}
								if(preg_match("|<file_srt>1</file_srt+>|i",$this->t_job["JOB_PARAM"])){
									if(file_exists(kVideosDir.$this->t_job["JOB_OUT"].'.srt')
										|| isset($custom_folder_out) && file_exists($custom_folder_out.$this->t_job["JOB_OUT"].'.srt')){
										$file_srt=1;$cpt_file++;	$extension[]='srt';	
									}										
								}
								if(preg_match("|<file_txt>1</file_txt+>|i",$this->t_job["JOB_PARAM"])){
									if(file_exists(kVideosDir.$this->t_job["JOB_OUT"].'.txt')
										|| isset($custom_folder_out) && file_exists($custom_folder_out.$this->t_job["JOB_OUT"].'.txt')){
										$file_txt=1;$cpt_file++;	$extension[]='txt';
									}
								}
								if(preg_match("|<file_vtt>1</file_vtt+>|i",$this->t_job["JOB_PARAM"])){
									if(file_exists(kVideosDir.$this->t_job["JOB_OUT"].'.vtt')
										|| isset($custom_folder_out) && file_exists($custom_folder_out.$this->t_job["JOB_OUT"].'.vtt')){
										$file_vtt=1;$cpt_file++;	$extension[]='vtt';
									}
								}
								// $this->t_job["JOB_OUT"] =$this->t_job["JOB_OUT"].'.txt';
								$_hasChanged=true;


								// if(file_exists(kVideosDir.$this->t_job["JOB_OUT"])){
								if($cpt_file > 0 ){
								if((isset($file_xml) && $file_xml == 1 )|| (isset($file_vtt) && $file_vtt == 1 )){
									// mise du contenu dans champ_parol
									if(preg_match("|<champ_parol>(.*)</champ_parol+>|i",$this->t_job["JOB_PARAM"],$matches)) {
										$champ_parol=$matches[1];
										$data = file_get_contents(kVideosDir.$this->t_job["JOB_OUT"].'.xml');
										// $data = utf8_encode(strstr($data, "<Trans"));
										require_once(modelDir.'model_doc.php');
										$docObj=new Doc;
										$docObj->t_doc["ID_DOC"]=$id_doc;
										$docObj->getDoc();
										$docObj->t_doc[$champ_parol]=$data;
										$docObj->save();
										unset($docObj);
										
									}
								}
										 $matInObj=$this->getMatObj();
										$matInObj->id_mat_ref=$matInObj->t_mat['MAT_NOM'];
										unset($id_doc);
										if (isset($matInObj) && $matInObj!=null) // dans le cas d'un job de diffusion => pas de job in matInObj null
										{
											$matInObj->getDocMat();
											$saveDocMat=$matInObj->t_doc_mat;

										// trace("saveDocmat");
										// trace(print_r($saveDocMat,true));
											// Sauvegarde valeur avant initialisation nouveau mat
											$mat=array();
											if(preg_match("|<MAT_COTE_ORI>(.*)</MAT_COTE_ORI+>|",$this->t_job["JOB_PARAM"],$matches))
												$mat['MAT_COTE_ORI']=$matches[1];
											else
												$mat['MAT_COTE_ORI']=$matInObj->t_mat['MAT_NOM'];
											$mat['MAT_TCIN']=$matInObj->t_mat['MAT_TCIN'];
											$mat['MAT_TCOUT']=$matInObj->t_mat['MAT_TCOUT'];
											$mat['MAT_TCFORCE']=$matInObj->t_mat['MAT_TCFORCE'];
											$mat['MAT_CHEMIN']=$matInObj->t_mat['MAT_CHEMIN'];
											// ajout transfert du mat_chemin d'origine aux fichiers transcodés
											$mat['MAT_CHEMIN_ORI']=$matInObj->t_mat['MAT_CHEMIN_ORI'];
											$mat['MAT_ID_GROUPE_MAT']=$matInObj->t_mat['MAT_ID_GROUPE_MAT'];

																						
										}
										if(preg_match("|<MAT_TYPE>(.*)</MAT_TYPE+>|",$this->t_job["JOB_PARAM"],$matches))
											$mat['MAT_TYPE']=$matches[1];
										
										if(preg_match("|<MAT_LIEU>(.*)</MAT_LIEU+>|",$this->t_job["JOB_PARAM"],$matches))
											$mat['MAT_LIEU']=$matches[1];
										foreach ($extension as $idx=>$ext){
											// Création nouveau matériel
											$matObj=new Materiel();
											$ok=$matObj->ingest(kVideosDir.$this->t_job["JOB_OUT"].'.'.$ext,$mat,"",false, $infos);
											$matObj->t_doc_mat=$saveDocMat;
											//$db->Execute("DELETE from t_doc_mat WHERE ID_MAT=".intval($matObj->t_mat['ID_MAT']));
										
											foreach ($matObj->t_doc_mat as $idx=>&$docMat) {
												$docMat["ID_MAT"]=$matObj->t_mat['ID_MAT'];
												$docMat["ID_DOC_MAT"]='';
												//spécifique colas
												if(preg_match("|<DMAT_INACTIF>(.*)</DMAT_INACTIF+>|",$this->t_job["JOB_PARAM"],$matches))
													$docMat["DMAT_INACTIF"]='1';
											}

											
											$matObj->saveDocMat();
										}

										}
										if ($ok) {
											$this->t_job["JOB_ID_ETAT"]=jobFini;
											$_hasChanged=true;
											
											// MS 03/10/13 Si le job courant précède un autre job, alors on update le JOB_ID_MAT du job suivant avec l'ID_MAT du matériel créé.
											$item_job_suiv = getItem("t_job",array('ID_JOB','JOB_ID_MAT'),array("ID_JOB_PREC"=>$this->t_job["ID_JOB"]),array('ID_JOB'=>"DESC"));
											if(!empty($item_job_suiv) && $item_job_suiv['JOB_ID_MAT']==0){
												$jobSuiv = new Job();
												$jobSuiv->t_job['ID_JOB'] = $item_job_suiv['ID_JOB'];
												$jobSuiv->getJob();
												$jobSuiv->t_job['JOB_ID_MAT'] = $matObj->t_mat['ID_MAT'];
												$jobSuiv->save();
											}

										}
											
										
								break;

                         case "TEXTE":
                             // Module OCR
                             if($entity=="doc_acc"){
                                 $out_file=kDocumentDir.$this->t_job["JOB_OUT"];
                                 if(file_exists($out_file)){
                                     require_once(modelDir.'model_docAcc.php');
                                     $myDA=new DocAcc;
                                     $myDA->t_doc_acc['ID_DOC_ACC']=$this->t_job["JOB_ID_MAT"];
                                     $myDA->getDocAcc();
                                     $xml=file_get_contents($out_file);
                                     // Suppression entête
                                     if(strpos($xml,'<?xml')!==false && strpos($xml,'>')!==false){
                                        //$xml=strstr($xml,'\n');
                                        $xml=substr($xml,strpos($xml,'>')+1);
                                     }
                                     $myDA->t_doc_acc['DA_TEXTE']=$xml;
                                     $ok=$myDA->save();
                                     if ($ok) {
                                        $this->t_job["JOB_ID_ETAT"]=jobFini;
                                        $_hasChanged=true;
                                     } else {
                                        $this->t_job["JOB_MESSAGE"]=$myDA->error_msg;
                                        $this->t_job["JOB_ID_ETAT"]=jobErreur;
                                        $_hasChanged=true;						
                                     }
                                     unset($myDA);
                                     unlink($out_file);
                                 }else{
                                    $this->t_job["JOB_MESSAGE"]=kMaterielFichierManquant." ".$out_file;
                                    $this->t_job["JOB_ID_ETAT"]=jobErreur;
                                 }
                             }else{
								require_once(modelDir.'model_materiel.php');
                                 $donnees_lieu_mat=Materiel::setLieuByIdMat($this->t_job["JOB_OUT"]);
                                 $out_file=$donnees_lieu_mat['LIEU_PATH'].$this->t_job["JOB_OUT"];
                                 if(file_exists($out_file)){
                                     $matInObj=$this->getMatObj();
                                     if (isset($matInObj) && $matInObj!=null){
                                         $xml=file_get_contents($out_file);
                                         // Suppression entête 
                                         if(strpos($xml,'<?xml')!==false){
                                            $xml=substr(strstr($xml,"\n"),1);
                                         }
										 $matInObj->id_mat_ref = $matInObj->t_mat['MAT_NOM'];
                                         $matInObj->t_mat['MAT_TEXTE']=$xml;
                                         $ok=$matInObj->save();
										 $matInObj->getDocs();
										 foreach($matInObj->t_doc_mat as $dm){
											$dm['DOC']->t_doc['DOC_TRANSCRIPT'] = $xml;
											$ok = $ok && ($dm['DOC']->save());
										 }
										 
                                         if ($ok) {
                                             $this->t_job["JOB_ID_ETAT"]=jobFini;
                                             $_hasChanged=true;
                                         } else {
                                             $this->t_job["JOB_MESSAGE"]=$matInObj->error_msg;
                                             $this->t_job["JOB_ID_ETAT"]=jobErreur;
                                             $_hasChanged=true;						
                                         }
                                         unset($matInObj);
                                     }else{
                                        $this->t_job["JOB_MESSAGE"]=kMaterielFichierManquant;
                                        $this->t_job["JOB_ID_ETAT"]=jobErreur;
                                     }
                                     unlink($out_file);
                                 }else{
                                    $this->t_job["JOB_MESSAGE"]=kMaterielFichierManquant." ".$out_file;
                                    $this->t_job["JOB_ID_ETAT"]=jobErreur;
                                 }
                             
                             }
                             break;

                         case "SFTP":
								// Mise � jour champ_youtube
								if(preg_match("|<id_doc>(.*)</id_doc+>|i",$this->t_job["JOB_PARAM"],$matches)) $id_doc=$matches[1];
								if(preg_match("|<champ_youtube>(.*)</champ_youtube+>|i",$this->t_job["JOB_PARAM"],$matches)) $champ_youtube=$matches[1];
								if($this->readLogXml($logXml)){
									if(preg_match("|<id type=\"video_id\">(.*)</id+>|i",$logXml,$matches)) $id_youtube=$matches[1];
								}
								if(!isset($id_doc)){
									require_once(modelDir.'model_materiel.php');
									//$matObj= new Materiel;
									//$matObj->t_mat['ID_MAT']=$this->t_job['JOB_IN'];
									
                                    $matObj=$this->getMatObj();
									
									if($matObj->checkExist()){
										$matObj->getDocMat();
										$id_doc=$matObj->t_doc_mat[0]['ID_DOC'];
									}
									unset($matObj);
								}
								print "ID_DOC=".$id_doc." champ=".$champ_youtube." ID_youtube=".$id_youtube;
								trace("ID_DOC=".$id_doc." champ=".$champ_youtube." ID_youtube=".$id_youtube);
								if(isset($id_doc) && isset($champ_youtube) && isset($id_youtube)){
									require_once(modelDir.'model_doc.php');
									$docObj=new Doc;
									$docObj->t_doc["ID_DOC"]=$id_doc;
									$docObj->getDoc();
									$docObj->t_doc[$champ_youtube]=$id_youtube;
									$docObj->save();
									unset($docObj);
									// Mise � jour statut
									$this->t_job["JOB_ID_ETAT"]=jobFini;
									$_hasChanged=true;
								} else {
									// Mise � jour statut
									$this->t_job["JOB_MESSAGE"]=kErrorRetourYoutube;
									$this->t_job["JOB_ID_ETAT"]=jobErreur;
									$_hasChanged=true;
								}

							break;

						case "HTTP":
							if(preg_match("|<action>(.*)</action+>|i",$this->t_job["JOB_PARAM"],$matches)) $action=$matches[1];
							switch($action){
								case "getChannels":
									// Mise à jour liste des chaînes
									if(preg_match("|<type_val>(.*)</type_val+>|i",$this->t_job["JOB_PARAM"],$matches)) $type_val=$matches[1];
									else $type_val="CHKW";
									$infoFile=kBackupInfoDir.'/'.$this->t_job["JOB_OUT"];
									if(file_exists($infoFile)){
										$val_gen=stripExtension($this->t_job["JOB_OUT"]);
										$xml = file_get_contents($infoFile);
										$tab=xml2tab($xml);
										$channel=array();
										// VP 7/04/10 : modification récupération chaîne
										//foreach($tab['KEWEGO_RESPONSE'][0]['MESSAGE'][0]['CHANNEL'] as $ch){
										foreach($tab['MESSAGE'][0]['CHANNEL'] as $ch){
											// CSIG-> VAL_CODE et NAME->VALEUR
											$channel[$ch['CSIG']]=$ch['NAME'];
										}
										// Creation valeur générique
										require_once(modelDir."model_val.php");		
										$myVal=new Valeur();
										$myVal->t_val['VAL_ID_TYPE_VAL']=$type_val;
										$myVal->t_val['VALEUR']=$val_gen;
										$myVal->t_val['ID_LANG']=$_SESSION['langue'];
										$exist=$myVal->checkExist(false);
										if($exist) $myVal->t_val['ID_VAL']=$exist;
										else {
											$myVal->create(); //création dans la langue
											foreach ($_SESSION['arrLangues'] as $lg) {
												if (strtoupper($lg)!=strtoupper($myVal->t_doc['ID_LANG'])) $arrVersions[$lg]=$val;
											}
											$myVal->saveVersions($arrVersions); //sauve dans autres langues
											unset($arrVersions);
										}
										$id_gen=$myVal->t_val['ID_VAL'];
										//Suppression et mise à jour valeurs 
										if(!empty($id_gen)){
											$rows=$db->GetAll("select distinct ID_VAL,VAL_CODE from t_val where VAL_ID_TYPE_VAL='CHKW' and VAL_ID_GEN=".intval($id_gen));
											foreach($rows as $row){
												if(array_key_exists($row["VAL_CODE"],$channel)){
													//mise à jour
													$db->Execute("update t_val set VALEUR=".$db->Quote(str_replace("\'","'",$channel[$row["VAL_CODE"]]))." where VAL_ID_TYPE_VAL='CHKW' and VAL_CODE=".$db->Quote($row["VAL_CODE"]));
												}else{
													// Suppression
													$myVal->t_val['ID_VAL']=$row["ID_VAL"];
													$myVal->delete();
												}
												unset($channel[$row["VAL_CODE"]]);
											}
										}
										// Creation nouvelles valeurs
										foreach($channel as $csig=>$name){
											$myVal=new Valeur();
											$myVal->t_val['VAL_ID_TYPE_VAL']=$type_val;
											$myVal->t_val['VAL_ID_GEN']=$id_gen;
											$myVal->t_val['VALEUR']=$name;
											$myVal->t_val['VAL_CODE']=$csig;
											$myVal->t_val['ID_LANG']=$_SESSION['langue'];
											$myVal->create(); //création dans la langue
											foreach ($_SESSION['arrLangues'] as $lg) {
												if (strtoupper($lg)!=strtoupper($myVal->t_doc['ID_LANG'])) $arrVersions[$lg]=$val;
											}
											$myVal->saveVersions($arrVersions); //sauve dans autres langues
											unset($arrVersions);
										}
										
										// Mise � jour statut
										$this->t_job["JOB_ID_ETAT"]=jobFini;
										$_hasChanged=true;
									}else{
										// Mise � jour statut
										$this->t_job["JOB_MESSAGE"]=kErrorRetourKewego;
										$this->t_job["JOB_ID_ETAT"]=jobErreur;
										$_hasChanged=true;
									}
									
									break;
									
								case "uploadThumbnails":
									// Changement vignette
									// Mise � jour statut
									$this->t_job["JOB_ID_ETAT"]=jobFini;
									$_hasChanged=true;
									break;

								default:
								case "setDetails":
								case "envoi":
									// 14/06/10 : le tag action pour setDetails et envoi n'est plus dans JOB_PARAM, on est donc dans le cas default
									// Mise � jour champ_kewego
									if(preg_match("|<id_doc>(.*)</id_doc+>|i",$this->t_job["JOB_PARAM"],$matches)) $id_doc=$matches[1];
									elseif(preg_match("|<ID_DOC>(.*)</ID_DOC+>|",$this->t_job["JOB_PARAM"],$matches)) $id_doc=$matches[1];
									if(preg_match("|<champ_kewego>(.*)</champ_kewego+>|i",$this->t_job["JOB_PARAM"],$matches)) $champ_kewego=$matches[1];
									if(preg_match("|Kewego (.*)\\n|",$log,$matches)) $id_kewego=$matches[1];
									// VP 22/04/10 : Lecture log xml
									if($this->readLogXml($logXml)){
										if(preg_match("|<sig>(.*)</sig+>|i",$logXml,$matches)) $id_kewego=$matches[1];
									}
									if(!isset($id_doc)){
										require_once(modelDir.'model_materiel.php');
										//$matObj= new Materiel;
										//$matObj->t_mat['ID_MAT']=$this->t_job['JOB_IN'];
										
										$matObj=$this->getMatObj();
										
										if($matObj->checkExist()){
											$matObj->getDocMat();
											$id_doc=$matObj->t_doc_mat[0]['ID_DOC'];
										}
										unset($matObj);
									}
									trace("ID_DOC=".$id_doc." champ=".$champ_kewego." ID_Kewego=".$id_kewego);
									
									if(isset($id_doc) && isset($champ_kewego)){
										// Mise � jour statut
										$this->t_job["JOB_ID_ETAT"]=jobFini;
										$_hasChanged=true;

										require_once(modelDir.'model_doc.php');
										$docObj=new Doc;
										$docObj->t_doc["ID_DOC"]=$id_doc;
										$docObj->getDoc();
										if(empty($docObj->t_doc[$champ_kewego])){
											if(isset($id_kewego)){
												$docObj->t_doc[$champ_kewego]=$id_kewego;
												$docObj->save();
											}else{
												// Mise � jour statut (pas d'ID kewego)
												$this->t_job["JOB_MESSAGE"]=kErrorRetourKewego;
												$this->t_job["JOB_ID_ETAT"]=jobErreur;
											}
										}
										unset($docObj);
									} else {
										// Mise � jour statut
										$this->t_job["JOB_MESSAGE"]=kErrorRetourKewego;
										$this->t_job["JOB_ID_ETAT"]=jobErreur;
										$_hasChanged=true;
									}
									break;
							}
							break;

						case "BACK":
							require_once(modelDir.'model_materiel.php');
							// Mise à jour statut
							$this->t_job["JOB_ID_ETAT"]=jobFini;
							$_hasChanged=true;
							// Mise à jour BDD
							// VP 13/09/13 : initialisation des variables
							$action="";
							$media="";
							$id_tape="";
							$set="";
							$nb_block=0;
							$n_file="";
							if(preg_match("|<action>(.*)</action+>|i",$this->t_job["JOB_PARAM"],$matches)) $action=$matches[1];
							if($action=="backup"){
								if(preg_match("|<media>(.*)</media+>|i",$this->t_job["JOB_PARAM"],$matches)) $media=$matches[1];
								if(preg_match("|<tape>([^<]*)</tape+>|i",$this->t_job["JOB_PARAM"],$matches)) $id_tape=$matches[1];
								if(preg_match("|<set>(.*)</set+>|i",$this->t_job["JOB_PARAM"],$matches)) $set=$matches[1];
								$backLogOK=false;
								$_lignes=explode("\n",$log);
								foreach($_lignes as $_ligne){
									if(strpos($_ligne,"Block")>0){
										$mots=explode(' ',strstr(str_replace("  "," ",$_ligne), 'Block'));
										if(count($mots)>2){
											$backLogOK=true;
											$block=$mots[1] +0;
											$blockfin=$mots[3] +0;
											$nb_block=$blockfin - $block;
										}
									}else if(strpos($_ligne,"File")>0){
										$mots=explode(' ',strstr($_ligne, 'File'));
										if(count($mots)>1){
											 $backLogOK=true;
											 $n_file=$mots[1] +0;
										}
									}
									// VP 13/06/12 : prise en compte changement de cartouche par LtoBckFile.sh
									if(strpos($_ligne,"Tape")>0){
										$mots=explode(' ',strstr($_ligne, 'Tape'));
										if(count($mots)>1){
											$id_tape=$mots[1];
										}
									}
								}
								
								// VP 3/03/09 : ajout test sur nb_block et envoi erreur
								if(!empty($media) && !empty($id_tape) && $backLogOK){
									require_once(libDir."class_backupJob.php");
									$finfo=BackupJob::getFinfo($this->t_job["JOB_IN"], $media, $this->t_job["JOB_ID_MAT"]);
									// Insertion t_tape_file
									require_once(modelDir.'model_tape.php');
									$myTape=new Tape();
									$myTape->t_tape['ID_TAPE']=$id_tape;
									$myTape->getTape();
									$myTape->t_tape_file[]=array('ID_TAPE'=>$myTape->t_tape['ID_TAPE'],
																'ID_TAPE_SET'=>$set,
																'TF_MEDIA'=>$media,
																'TF_FICHIER'=>$finfo['name'],
																'TF_TAILLE_FICHIER'=>$finfo['size'],
																'TF_DATE_FICHIER'=>$finfo['mdate'],
																'TF_DATE_COPIE'=>$this->t_job["JOB_DATE_FIN"],
																'TF_ID_MAT'=>$this->t_job['JOB_ID_MAT'],
																'TF_ID_JOB'=>$this->t_job['ID_JOB'],
																'TF_BLOCK'=>$block,
																'TF_TAILLE_BLOCK'=>gTailleBlock,
																'TF_NB_BLOCKS'=>$nb_block,
																'TF_N_FILE'=>$n_file
																 );
									$myTape->saveTapeFile(false);
									trace("insertion t_tape_file TAPE=".$id_tape." N_FILE=".$n_file." FICHIER=".$finfo['name']);
									// VP 11/02/09 : calcul taille utilisée basée sur block de fin
									//$myTape->t_tape['TAPE_UTILISE']=$myTape->t_tape['TAPE_UTILISE']+$finfo['size'];
									// VP 26/08/10 : calcul taille utilisée basée sur taille fichier si pas de gestion de block 
									if(empty($nb_block)){
										$nb_block=floor($finfo['size']/gTailleBlock) + 1;
										$myTape->t_tape['TAPE_UTILISE']=$myTape->t_tape['TAPE_UTILISE']+gTailleBlock*$nb_block;
									}else{
										$myTape->t_tape['TAPE_UTILISE']=$blockfin * gTailleBlock;
									}
									$myTape->save();
									unset($myTape);
									
									//PC 01/10/12 : Mise à jour de l'état archivage
									if (!empty($this->t_job["JOB_ID_MAT"])){
									  $matObj=$this->getMatObj();
									  $matObj->updateEtatArch();
									}
								}else{
									$this->t_job["JOB_ID_ETAT"]=jobErreur;
									$this->t_job["JOB_MESSAGE"]=kErrorVariableVide." ID_TAPE=".$id_tape." NB_BLOCKS=".$nb_block." NO_FILE=".$n_file; 
								}
							}
							elseif($action=="restore"){
								//PC 02/10/12 : Mise à jour de l'état archivage
								if (!empty($this->t_job["JOB_ID_MAT"])){
									$matObj=$this->getMatObj();
									if(isset($matObj->t_mat['ID_MAT'])){
										$matObj->updateEtatArch();
									} elseif(is_file(kVideosDir.$this->t_job["JOB_OUT"])) {
										// VP  3/03/2016 : Création mat si non existant
										$ok=$matObj->ingest(kVideosDir.$this->t_job["JOB_OUT"],null,"",false, $infos);
									}
								}
							}
							break;
								
								case 'DIFF':
									require_once(modelDir.'model_doc.php');
									require_once(modelDir.'model_panier.php');
									// recuperation du job parent
									$job_parent=new Job();
									$job_parent->t_job['ID_JOB']=$this->t_job['ID_JOB_GEN'];
									$job_parent->getJob();
									$id_doc=$id_video=$private_id='';
									// on recupere l'id du document
									if(preg_match("|<ID_DOC>(.*)</ID_DOC+>|",$job_parent->t_job["JOB_PARAM"],$matches))
										$id_doc=trim($matches[1]);
									//XB									
									if(preg_match("|<ID_LIGNE_PANIER>(.*)</ID_LIGNE_PANIER+>|",$job_parent->t_job["JOB_PARAM"],$matches)){
										$id_ligne_panier=trim($matches[1]);
										trace('id ligne panier'.$id_ligne_panier);
									}
									// si l'id du document est vide on va chercher dans le job courant
									if (empty($id_doc))
									{
										if(preg_match("|<ID_DOC>(.*)</ID_DOC+>|",$this->t_job["JOB_PARAM"],$matches))
											$id_doc=trim($matches[1]);
									}

									// VP 19/07/13 : Traitement delete
									if(preg_match("|delete:<id>(.*)</id>|",$log,$matches)){
										$id_video="";
									}else{
										// on recupere l'id de la video dans le log
										if(preg_match("|video_id:<id>(.*)</id>|",$log,$matches))
										{
											$id_video=trim($matches[1]);
											trace('id de la video '.$id_video);
										}

										if(preg_match("|private_id:<id>(.*)</id>|",$log,$matches))
										{
											$private_id=trim($matches[1]);
											trace('id de la video '.$private_id);
										}
										
										if (empty($id_video))
										{
											$this->t_job["JOB_ID_ETAT"]=jobErreur;
											$this->t_job["JOB_MESSAGE"]='id de la video manquant';
											break;
										}
									}
									//recuperation du nom du champ
									if(preg_match("|<champ_retour>(.*)</champ_retour+>|i",$this->t_job["JOB_PARAM"],$matches))
										$nom_champ_id_video=$matches[1];
									
									// on stocke l'id dans 
									if (!empty($nom_champ_id_video) && !empty($id_doc))
									{
										trace('Sauvegarde ID video dans '.$id_doc.' => '.$nom_champ_id_video.' = '.$id_video);
										$my_doc=new Doc();
										$my_doc->t_doc['ID_DOC']=intval($id_doc);
										$my_doc->getDoc();
										$my_doc->t_doc[$nom_champ_id_video]=$id_video;
										$my_doc->save();
										// MS - 03.02.17 - update des docs dans les autres langues => permet d'éviter le cas où on ne recopie pas le champ de retour si il appartient aux champs docs dépendants de l'id_lang
										foreach($_SESSION['arrLangues'] as $lg){
											if($my_doc->t_doc['ID_LANG'] != $lg){
												$otherDoc = new Doc();
												$otherDoc->t_doc['ID_DOC'] = $my_doc->t_doc['ID_DOC'];
												$otherDoc->t_doc['ID_LANG'] = $lg;
												$otherDoc->getDoc();
												$otherDoc->t_doc[$nom_champ_id_video]=$id_video;
												$otherDoc->save(); 
											}
										}
										if (defined('useSolr') && useSolr==true)
											$my_doc->saveSolr();
									}
									//on stock l'id dans t_doc_publication
									if (!empty($id_doc))
									{
										trace('Sauvegarde ID video dans t_doc_publication de '.$id_doc.' => '.$id_video);
										$my_doc=new Doc();
										$my_doc->t_doc['ID_DOC']=intval($id_doc);
										$my_doc->createFromArrayDocPublication($id_video,'',$this->t_job["JOB_ID_ETAPE"],$this->t_module["MODULE_NOM"],$private_id, $this->t_job ['ID_JOB']);
									
									}
									//on stock l'id dans t_doc_publication
									if (!empty($id_ligne_panier) && empty($id_doc))
									{
										$id_doc=$db->GetOne("SELECT id_doc FROM t_panier_doc WHERE id_ligne_panier=".$id_ligne_panier);
										trace('Sauvegarde ID video dans t_doc_publication a partir du panier de '.$id_doc.' => '.$id_video);
										$my_doc=new Doc();
										$my_doc->t_doc['ID_DOC']=intval($id_doc);
										$my_doc->createFromArrayDocPublication($id_video,$id_ligne_panier,$this->t_job["JOB_ID_ETAPE"],$this->t_module["MODULE_NOM"],'', $this->t_job ['ID_JOB']);
									}
									
									$this->t_job["JOB_ID_ETAT"]=jobFini;
									$_hasChanged=true;
									break;
									
								default: 
								// Autres modules
								// Mise à jour statut
								$this->t_job["JOB_ID_ETAT"]=jobFini;
								$_hasChanged=true;
								break;
							}
							
							if ($this->t_job["JOB_ID_ETAT"]==jobFini)
							{
								// Le job est fini on met sa progression a 100%
								$this->t_job["JOB_PROGRESSION"]=100;
								// deplacement du fichier out dans le repertoire done
								rename($this->logFile,jobDoneDir.'/'.$this->t_module['MODULE_NOM'].'/'.basename($this->logFile));
								// Le job est fini --> on lance les jobs qui dependent de ce job.
								trace("updateJobs ".$this->t_job['ID_JOB']);
								$this->launchNextJobs();
							}
							
						} else if(strpos($log,"Debut traitement")>0){
							preg_match_all('/(.*) Progression (.*)%[ - (.*)]{0,3}/',$log,$matches);
							$this->t_job['JOB_PROGRESSION']=$matches[2][count($matches[2])-1];
							// mise a jour de la progression
							if($this->t_job["JOB_ID_ETAT"] != jobEnCours) $this->t_job["JOB_DATE_DEBUT"]=date("Y-m-d H:i:s");
							//trace("update to en cours ".$this->t_job['ID_JOB']." ".$this->t_job['JOB_PROGRESSION']);
							$this->t_job["JOB_ID_ETAT"]=jobEnCours;
							$_hasChanged=true;
						}

						//relance de job si en erreur et attempt défini
						if($this->t_job["JOB_ID_ETAT"]==jobErreur && (preg_match("|<attempt>(.*)</attempt+>|",$this->t_job["JOB_PARAM"],$attempt)) && (preg_match("|<time_attempt>(.*)</time_attempt+>|",$this->t_job["JOB_PARAM"],$time_attempt))){
							//relance le attempt
							$essai=$attempt[1];
							if($essai > 0) {
								$essai=$essai-1;
								$date = $time_attempt[1];
								$date=explode(":",$date);
								$sec= ($date[0]*3600) +($date[1]*60)+$date[2];
								$my_date_time=time("Y-m-d H:i:s");
								$my_new_date_time=$my_date_time+$sec;
								$job_date_lancement=date("Y-m-d H:i:s",$my_new_date_time);
								$this->t_job["JOB_PARAM"] = preg_replace("/<attempt>.*?<\/attempt>/", "<attempt>".$essai."</attempt>",$this->t_job["JOB_PARAM"]);
								$this->t_job["JOB_DATE_LANCEMENT"]=$job_date_lancement;
								$this->initJob();
							}
						}
						// Envoi mail si erreur
						if($this->t_job["JOB_ID_ETAT"]==jobErreur){
							// deplacement du fichier out dans le repertoire error
							rename($this->logFile,jobErrorDir.'/'.$this->t_module['MODULE_NOM'].'/'.basename($this->logFile));
							// Si erreur, envoi mail admin
							// VP 1/06/2012 : prise en compte paramètres <mailJobErreur> et <mailJobErreurXsl>
							$sujet="Erreur traitement : ".$this->t_job["JOB_NOM"]." (".kDatabaseName." ".(defined("APPLICATION_ENV")?APPLICATION_ENV:"")." ID=".$this->t_job["ID_JOB"].")";
							$dest=(defined("gMailJobErreur")?gMailJobErreur:"");
							if(preg_match("|<mailJobErreur>(.*)</mailJobErreur+>|i",$this->t_job["JOB_PARAM"],$matches)){
								$dest=gMailJobErreur.', '.$matches[1];
							}
							 if(preg_match("|<send_mail_usager>(.*)</send_mail_usager+>|",$this->t_job["JOB_PARAM"],$matches)){
								if(!empty($dest))
									$dest.=', '.$this->get_mail_user();
								else
									$dest=$this->get_mail_user();
							}
							if(!empty($dest)){
								if(preg_match("|<mailJobErreurXsl>(.*)</mailJobErreurXsl+>|i",$this->t_job["JOB_PARAM"],$matches)){
									$xsl=$matches[1];
									if(preg_match("|<send_mail_admin>(.*)</send_mail_admin+>|",$this->t_job["JOB_PARAM"],$matches) && defined("gMailJobErreur")){
									 include_once(modelDir.'model_message.php');
									 $msg = new Message();
									 $msg->send_mail(gMailJobErreur,gMail,$sujet,$this->t_job["JOB_MESSAGE"], "text/html");
							}
									$this->send_mail($dest,$sujet,"print/".$xsl);
								}else{
									include_once(modelDir.'model_message.php');
									$msg = new Message();
									$msg->send_mail($dest, gMail, $sujet, $this->t_job["JOB_MESSAGE"], "text/html");
								}
							}
						}
						// Sauvegarde job
						$this->save();
					}
					break;

	
				case 0:
					// VP (22/10/08) : Recup statut job précédent
					if(!empty($this->t_job["ID_JOB_PREC"])){
						if(!isset($arrJob[$this->t_job["ID_JOB_PREC"]])){
							$jobPrecObj=new Job();
							$jobPrecObj->t_job["ID_JOB"]=$this->t_job["ID_JOB_PREC"];
							$jobPrecObj->getJob();
							$arrJob[$this->t_job["ID_JOB_PREC"]]=$jobPrecObj;
							unset($jobPrecObj);
						}
						$finPrec=($arrJob[$this->t_job["ID_JOB_PREC"]]->t_job["JOB_ID_ETAT"]==jobFini);
					}else $finPrec=true;
					// VP (27/11/09) : Recup statut job validation
					if(!empty($this->t_job["ID_JOB_VALID"])){
					   if(!isset($arrJob[$this->t_job["ID_JOB_VALID"]])){
							$jobValidObj=new Job();
							$jobValidObj->t_job["ID_JOB"]=$this->t_job["ID_JOB_VALID"];
							$jobValidObj->getJob();
							$arrJob[$this->t_job["ID_JOB_VALID"]]=$jobValidObj;
						   unset($jobValidObj);
						}
						$finValid=($arrJob[$this->t_job["ID_JOB_VALID"]]->t_job["JOB_ID_ETAT"]==jobFini);
					}else $finValid=true;
					// Si traitement précédent et validation terminé, lancement du traitement
					if($finPrec && $finValid){
						// Ecriture fichier xml
						if($this->makeXML()){
							// Mise � jour statut
							// VP 15/10/10 : pas de mise à jour de statut si cela a été fait dans makeXML
							if(empty($this->t_job["JOB_ID_ETAT"])) $this->t_job["JOB_ID_ETAT"]=jobAttente;
							$_hasChanged=true;
						} else {
							$this->t_job["JOB_ID_ETAT"]=jobErreur;
							$_hasChanged=true;
							$this->t_job["JOB_MESSAGE"]=$this->error_msg;
						}
						// Sauvegarde job
						$this->save();
					}
					break;
			}
		if(function_exists("appliqueJobCustom")){
		   appliqueJobCustom($this);
		}	
		
		
		// Supression log si etat fini ou erreur
		if($_hasChanged && $this->t_job["JOB_ID_ETAT"] >=jobErreur){
			$this->deleteLog();
		}
		// Mise � jour statut processus
		if($_hasChanged && $this->t_job["ID_JOB_GEN"]!="0"){
			
			if(!function_exists("callUpdatePanierEtat")){
				function callUpdatePanierEtat ($job,$updatePanier,$matches,&$myPanier) {
					if($updatePanier=="1" && preg_match("|<ID_LIGNE_PANIER>(.*)</ID_LIGNE_PANIER+>|",$job->t_job["JOB_PARAM"],$matches)) {
						// Mise à jour ligne panier
						$id_ligne_panier=$matches[1];
						require_once(modelDir.'model_panier.php');
						$myPanierDoc=Panier::getLignePanier($id_ligne_panier);
						$myPanierDoc['PDOC_ID_ETAT']=gPanierFini;
						if(empty($myPanierDoc['PDOC_ID_JOB']) || $myPanierDoc['PDOC_ID_JOB'] == 0){
							$myPanierDoc['PDOC_ID_JOB']=$job->t_job['ID_JOB'];
						}
						if(Panier::savePanierDoc($myPanierDoc)){
							// Mise à jour statut panier (UNIQUEMENT SI PARAMETRE)
							$myPanier=new Panier();
							$myPanier->t_panier["ID_PANIER"]=$myPanierDoc['ID_PANIER'];
							return $myPanier->updatePanierEtat();
						}
					}else if($updatePanier=="1" && preg_match("|<ID_PANIER>(.*)</ID_PANIER+>|",$job->t_job["JOB_PARAM"],$matches)) {
						require_once(modelDir.'model_panier.php');
						$id_panier=$matches[1];
						$myPanier = New Panier();
						$myPanier->t_panier['ID_PANIER'] = $id_panier;
						$myPanier->getPanier();
						$myPanier->met_a_jour_lignes('PDOC_ID_ETAT',gPanierFini);
						$myPanier->saveAllPanierDoc();
						return $myPanier->updatePanierEtat();
					}
				}
			}
			// initialisation du job temporaire au job courant
			$job_tmp = $this;
			$max_depth = 20 ; 
			$secure_depth = 0 ;
			do{
				// récupération du job courant si pas dans arrJob
				if(!isset($arrJob[$job_tmp->t_job["ID_JOB_GEN"]])){
					$jobParentObj=new Job();
					$jobParentObj->t_job["ID_JOB"]=$job_tmp->t_job["ID_JOB_GEN"];
					$jobParentObj->getJob();
					$arrJob[$job_tmp->t_job["ID_JOB_GEN"]]=$jobParentObj;
				}
				// update de la progression & de l'état du job parent  
				if(isset($arrJob[$job_tmp->t_job["ID_JOB_GEN"]])){
					$jobParentObj=$arrJob[$job_tmp->t_job["ID_JOB_GEN"]];
					$lastProcStatus=$jobParentObj->t_job["JOB_ID_ETAT"];
					$job_tmp->updateProcStatus();
					$jobParentObj->getJob();
					if(($jobParentObj->t_job["JOB_ID_ETAT"]==jobFini) && ($lastProcStatus != jobFini)){
						// VP 11/10/13 : Lancement les jobs qui dependent de ce processus (ex : dvd ou post-traitement panier)
						$jobParentObj->launchNextJobs();
						// update des états ligne_panier / panier en rapport avec les jobs terminés. 
						if(preg_match("|<updatePanier>(.*)</updatePanier+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)) {
							$updatePanier=$matches[1];
							$etat_pan = callUpdatePanierEtat($jobParentObj,$updatePanier,$matches,$myPanier);
						}
					}
				}
				$job_tmp = $jobParentObj ;
				
				// MS - sécurité max_depth à cause de la nature recursive de ce while;
				$secure_depth++ ; 
				if($secure_depth >= $max_depth){
					trace("updateJobs WARNING max_depth $max_depth atteinte pour recursion updateProcStatus");
					break ; 
				}	
			}while(!empty($job_tmp->t_job['ID_JOB_GEN']));
			
			
			if(($jobParentObj->t_job["JOB_ID_ETAT"]==jobFini) && ($lastProcStatus != jobFini)){
				//creation du compte FTP
				if(preg_match("|<livraison_ftp>(.*)</livraison_ftp+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)){
					if(preg_match("|<ftp_login>(.*)</ftp_login+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)) $ftp_login=$matches[1];
					if(preg_match("|<ftp_pw>(.*)</ftp_pw+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)) $ftp_pw=$matches[1];
					if(preg_match("|<FOLDER_OUT>(.*)</FOLDER_OUT+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)) $folder_out=$matches[1];
					if(!empty($ftp_login) && !empty($ftp_pw) && !empty($folder_out)){
						require_once(libDir.'class_ftp.php');
						$ftp=new Ftp();
						$ftp->setLogin($ftp_login);
						$ftp->setPassword($ftp_pw);
						$ftp->setHome($folder_out);
						$ftp->createCompte();
					}
				}
				
				// Livraison signiant
				if(preg_match("|<livraison_signiant>(.*)</livraison_signiant+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)){
					if (isset($myPanier) && isset($etat_pan) && $etat_pan == gPanierFini){
						$myPanier->getPanier();
						$myPanier->createSigniantPackage();
					}
				}
				
				if(preg_match("|<send_mail>(.*)</send_mail+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)){
					if (isset($myPanier) && isset($etat_pan) && $etat_pan == gPanierFini){
						$myPanier->getPanier();
						if(preg_match("|<mails>(.*)</mails+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)){
							$jobParentObj->send_mail($matches[1]);
						}
						if (!preg_match("|<no_send_mail_usager>1</no_send_mail_usager+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)) {
							$myPanier->send_mail_usager(1);
						}
					}else{
						$adddest = "" ; 
						if(!preg_match("|<no_send_mail_admin>1</no_send_mail_admin+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)){
							$adddest.=", ".gMailJob;
						}
						if(!preg_match("|<no_send_mail_usager>1</no_send_mail_usager+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)){
							$adddest.=", ".$jobParentObj->get_mail_user();
						}
						$sujet = "" ; 
						if(preg_match("|<mail_fin_traitement_sujet>(.*)</mail_fin_traitement_sujet+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)){
							$sujet.=$matches[1];
						}
						
															
						// MS - 12.02.15 - modification de l'appel à send_mail en fonction de si le job a un JOB_ID_PROC renseigné ou non.
						// en théorie, si pas de job_id_proc, alors on est dans le cas d'un job d'import (lié à un objet class_import)
						// on modifie donc l'appel à send_mail pour indiquer que le xml exporté puis traité dans jobMail doit inclure les informations des fils de ce job 
						if($jobParentObj->t_job['JOB_ID_PROC'] == 0 ){
							if(preg_match("|<mails>(.*)</mails+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)) $jobParentObj->send_mail($matches[1].$adddest,$sujet,'print/jobMail.xsl',1);
							else $jobParentObj->send_mail("",'','print/jobMail.xsl',1);
						}else{
							if(preg_match("|<mails>(.*)</mails+>|i",$jobParentObj->t_job["JOB_PARAM"],$matches)) $jobParentObj->send_mail($matches[1].$adddest,$sujet);
							else $jobParentObj->send_mail();
						}
					}
				}
				
			}
			if(isset($myPanier)) 
				unset($myPanier);
		}
	}
}

?>
