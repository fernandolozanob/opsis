<?
/*  class pour fournir les url courtes */
require_once(modelDir.'model.php');
class ShortUrl extends Model{

	protected static $chars = "123456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
    protected static $checkUrlExists = true;
    protected $timestamp;
	
	function __construct($id=null,$version='') {
		parent::__construct('t_short_url',$id,$version);
		$this->timestamp = $_SERVER["REQUEST_TIME"];
		$this->error_msg="";
	}

	public function urlToShortCode($url) {
	trace('url debut '.$url);
        if (empty($url)) {
			$this->error_msg="L'URL est vide";
            throw new Exception("L'URL est vide");
        }
 
   //      if ($this->validateUrlFormat($url) == false) {
			// $this->error_msg="L'URL est mal formée";
   //           throw new Exception("L'URL est mal formée");
			
   //      }
 
        if (self::$checkUrlExists) {
            if (!$this->verifyUrlExists($url)) {
				$this->error_msg="URL n'existe pas.";
                 throw new Exception("URL n'existe pas.");			
			}
        }
 
        $shortCode = $this->urlExistsInDb($url);
        if ($shortCode == false) {
            $shortCode = $this->createShortCode($url);
        }
 
        return $shortCode;
    }
	
	 protected function validateUrlFormat($url) {
        return filter_var($url, FILTER_VALIDATE_URL,FILTER_FLAG_HOST_REQUIRED);
    }
 
	//appel curl pour vérifier que la page existe
    protected function verifyUrlExists($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
 
        return (!empty($response) && $response != 404);
    }
 
	//verifie si l'url longue à déja une short url 
    protected function urlExistsInDb($url) {
		global $db;
		$result=$db->GetOne("SELECT SU_CODE FROM t_short_url WHERE SU_URL='".$url."'");
        return (empty($result)) ? false : $result;
    }
	
	 protected function createShortCode($url) {
        $id = $this->insertUrlInDb($url);
        $shortCode = $this->convertIntToShortCode($id);
        $this->insertShortCodeInDb($id, $shortCode);
        return $shortCode;
    }
	
	 protected function insertUrlInDb($url) {
		global $db;
		$record = array("SU_URL" => $url,"SU_DATE_CREA" => $this->timestamp);
		$ok = $db->insertBase("t_short_url","id_short_url",$record);
		return $ok;
    }
	
	//convertit le timestamp en short code
	 protected function convertIntToShortCode($id) {
		global $db;
        $id = intval($id);
		$tp=$db->GetOne("select SU_DATE_CREA from t_short_url WHERE id_short_url=".$id);
        if ($tp < 1) {
			 $this->error_msg="ID non valide";
             throw new Exception(
                 "ID non valide");
				
        }
 
        $length = strlen(self::$chars);
		//longueur min >>> A VOIR
        if ($length < 10) {
			$this->error_msg="longeur minimum de 10 caractère";
            throw new Exception("longeur minimum de 10 caractère ");		
        }
 
        $code = "";
        while ($tp > $length - 1) {
            $idx=$tp % $length;
            $code = self::$chars[$idx].$code;   
            $tp = floor($tp / $length);
            $tp = (int)$tp;
        }

        $code = self::$chars[$tp] . $code;
        return $code;
    }

	   protected function insertShortCodeInDb($id, $code) {
	   global $db;
        if ($id == null || $code == null) {
  			$this->error_msg="nombre de paramètres invalides";       
			throw new Exception("nombre de paramètres invalides.");			
        }
		$rs = $db->Execute("SELECT * FROM t_short_url WHERE ID_SHORT_URL=".intval($id));
		$sql = $db->GetUpdateSQL($rs, array("SU_CODE" => $code));
		if (!empty($sql)) $ok = $db->Execute($sql);
 
        return true;
    }
	
	//renvoi de l'url associée au code  paramètre increment pour lancer le compteur
	 public function shortCodeToUrl($code, $increment = true) {
        if (empty($code)) {
			$this->error_msg="Aucun code n'est soumis";			
            throw new Exception("Aucun code n'est soumis");

        }
 
        if ($this->validateShortCode($code) == false) {
			$this->error_msg="L'URL courte a un format incorrect.";		
             throw new Exception(
                "L'URL courte a un format incorrect.");
         }

 
        $urlRow = $this->getUrlFromDb($code);
        if (empty($urlRow)) {
			$this->error_msg="Cette url courte n'existe pas.";
            throw new Exception(
                 "Cette url courte n'existe pas.");
         }
 
        if ($increment == true) {
            $this->incrementCounter($urlRow["ID_SHORT_URL"]);
        }
 
        return $urlRow["SU_URL"];
    }
	
	//que des lettre et des chiffres
    protected function validateShortCode($code) {
        return preg_match("|[" . self::$chars . "]+|", $code);
    }
	
	//recherche du code dans t_short_url
	 protected function getUrlFromDb($code) {
		global $db;
		$result=$db->GetRow("SELECT ID_SHORT_URL,SU_URL FROM t_short_url WHERE SU_CODE='".$code."'");
        return (empty($result)) ? false : $result;

    }
 
	//compteur
    protected function incrementCounter($id) {
		global $db;
		$rs = $db->Execute("SELECT * FROM t_short_url WHERE ID_SHORT_URL=".intval($id));
		$sql = $db->GetUpdateSQL($rs, array("SU_COUNT" => "SU_COUNT+1"));
		if (!empty($sql)) $ok = $db->Execute($sql);
    }
 
}
?>