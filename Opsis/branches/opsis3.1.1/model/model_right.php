<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2012                                                                        ---*/
/*--- Développeur : Pierre Champon												  ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
require_once(modelDir.'model.php');

class Right extends Model
{
	var $t_right = array();
	var $t_right_val = array();
	var $t_usager = array();

	var $error_msg;


	function __construct($id=null,$version='') {
		parent::__construct('t_right',$id,$version);
		$this->init();
	}


	function getRights(){
		return $this->getData() ; 
	}
	
	
	/*function getRights() {
		global $db;
		
		$arrCrit=array();
		if (!empty($this->t_right['ID_RIGHT']))
			$arrCrit['ID_RIGHT'] = $this->t_right['ID_RIGHT'];
		$this->t_right=getItem("t_right",null,$arrCrit,null,0);
		
		if (count($this->t_right)>=1) {
			$this->t_right=$this->t_right[0];
			return $this;
		}
	}*/
	
	function getRightsVal() {
		global $db;
		
		$this->t_right_val = $db->getAll("SELECT * 
											FROM t_right_val rv 
											INNER JOIN t_val v ON v.ID_VAL = rv.ID_VAL and v.ID_LANG like ".$db->Quote($_SESSION['langue'])."
											WHERE ID_RIGHT = ".intval($this->t_right['ID_RIGHT']));
	}
	
	function getUsager() {
		global $db;
		
		if (!empty($this->t_right['ID_PANIER'])){
			$res = $db->getAll("SELECT u.* 
											FROM t_panier p 
											INNER JOIN t_usager u ON u.ID_USAGER = p.PAN_ID_USAGER 
											WHERE p.ID_PANIER = ".intval($this->t_right['ID_PANIER']));
			if (count($res) > 0)
				$this->t_usager = $res[0];
		}
	}
	
	function getRightsByPanier($id_panier) {
		global $db;
		
		$arrCrit=array();
		if (!empty($id_panier))
			$arrCrit['ID_PANIER'] = $id_panier;
		$this->t_right=getItem("t_right",null,$arrCrit,null,0);
		
		if (count($this->t_right)>=1) {
			$this->t_right=$this->t_right[0];
			return $this;
		}
	}
	
	function getRightsByUser($id_usager) {
		getRights("", $id_usager);
	}
	
	function getRightsByDoc($id_doc) {
		getRights($id_doc, "");
	}
	
	function checkExist() {
		global $db;
		return $db->GetOne("SELECT ID_RIGHT from t_right WHERE ID_RIGHT=".intval($this->t_right['ID_RIGHT']));
	}

	function addRights($id_panier, $date_debut_droit, $date_fin_droit) {
		if (empty($id_panier) || empty($date_debut_droit) || empty($date_fin_droit)){
			$this->dropError(kErrorDroitCreation);
			return false;
		}
		else {
			$this->t_right['ID_PANIER'] = $id_panier;
			$this->t_right['RIGHT_BEGIN_DT'] = $date_debut_droit;
			$this->t_right['RIGHT_END_DT'] = $date_fin_droit;
			return create();
		}
	}
	
	function setRights($t_right = array()) {
		foreach ($t_right as $fld=>$val)
			if (isset($this->t_right[$fld]))
				$this->t_right[$fld] = $val;
		return $this->save();
	}

	function create() {
		global $db;
		if (!empty($this->t_right['ID_RIGHT'])) {return $this->save();}
		if ($this->checkExist()) {return $this->save();}

		// $rs = $db->Execute("SELECT * FROM t_right WHERE id_right=-1");
		// $sql = $db->GetInsertSQL($rs, $this->t_right);
		// $ok = $db->Execute($sql);
		$ok = $db->insertBase("t_right","id_right",$this->t_right);
		if (!$ok) {$this->dropError(kErrorDroitCreation);return false;}
		$this->t_right['ID_RIGHT']=$ok;

		return true;
	}

	function save() {
		global $db;
		if (empty($this->t_right['ID_RIGHT'])) {return $this->create();}

		$rs = $db->Execute("SELECT * FROM t_right WHERE ID_RIGHT=" . intval($this->t_right['ID_RIGHT']));
		$sql = $db->GetUpdateSQL($rs, $this->t_right);
		if (!empty($sql)) $ok = $db->Execute($sql);

		if (!$ok) {$this->dropError(kErrorDroitSauve);return false;}
		return true;
	}
	
	function saveVal() {
		global $db;

		if (empty($this->t_right['ID_RIGHT'])) {$this->dropError(kErrorDroitNoID);return false;}
		$db->StartTrans();
		$db->Execute('DELETE from t_right_val WHERE ID_RIGHT='.intval($this->t_right['ID_RIGHT']));

		foreach ($this->t_right_val as $rv) {
			$db->Execute('INSERT INTO t_right_val (ID_RIGHT,ID_VAL,ID_TYPE_VAL) 
							values ('.intval($this->t_right['ID_RIGHT']).','.intval($rv['ID_VAL']).','.$db->Quote($rv['ID_TYPE_VAL']).')');
		}
		$ok=$db->CompleteTrans();

		if (!$ok) {$this->dropError(kErrorDroitSauveVal);return false;}
		return true;
	}

	function deleteRights($id_right) {
		$this->t_right['ID_RIGHT'] = $id_right;
		delete();
	}

	function delete() {
		global $db;
		$db->StartTrans();
		
		$db->Execute("DELETE from t_right WHERE ID_RIGHT=".intval($this->t_right['ID_RIGHT']));
		$db->Execute("DELETE from t_right_val WHERE ID_RIGHT=".intval($this->t_right['ID_RIGHT']));
		
		$ok=$db->CompleteTrans();
		
		if (!$ok) {$this->dropError(kErrorRightSuppr);return false;}
		return true;

	}
}

?>