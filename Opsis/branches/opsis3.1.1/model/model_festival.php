<?
/**
 * Classe festival, fortement inspirée de la classe doc
 */
require_once(modelDir.'model.php');
class Festival extends Model {



    var $t_fest_lex = array();
    var $t_fest_val = array();
    var $t_section =array();

    var $error_msg;

  
    var $relation;

 	var $t_festival = array();
 	var $vignette; // Chemin de la vignette associée au festival


	var $arrFather= array();

	var $arrVersions=array();
	var $arrDocAcc=array();

 	var $uniqueFields; // tableau (optionnel) contenant les champs sur lesquels lors d'une sauvegarde le test d'unicité est basé


 	private $arrFieldsToSaveWithIdLang = array(

 	      "FEST_DESC" ,"FEST_LIBELLE", "FEST_PALMARES" , "FEST_XML", "FEST_NOTES");


 	private $arrFieldsToSaveWithoutIdLang= array(

 	     "FEST_ID_GEN","FEST_ANNEE","FEST_ID_IMAGE","FEST_DATE_MOD","FEST_ID_USAGER_MOD", "FEST_ADRESSE", "FEST_CODE", "FEST_DATE_DEBUT", "FEST_DATE_FIN");


    private $arrTextFields=array("FEST_DESC","FEST_PALMARES","FEST_LIBELLE","FEST_PALMARES"); // liste des champs de type texte dont il faut traiter les apostrophes

    // Constructeur

    function __construct($id=null,$version=''){
		if (defined('gFestUniqueFields')) $this->uniqueFields=(array)unserialize(gFestUniqueFields);
    	parent::__construct("t_festival",$id,$version);
    }

	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */

 	function updateFromArray ($tab_valeurs,$allFields=true) {
	 	if (empty($this->t_festival)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_festival[strtoupper($fld)])) $this->t_festival[strtoupper($fld)]=stripControlCharacters($val);
		}
		
		 //Si on n'utilise que des champs dans tab_valeurs (ex: sequences, sauv fest)
		//on vire les champs non présents dans tab_valeurs, qui ne sont pas des clés primaires.
		if (!$allFields) {
			//préparation d'un tableau contenant juste les clés uppercased de tab_valeurs
			$arrUpperKeys=array_keys(array_change_key_case($tab_valeurs,CASE_UPPER)); 
			foreach ($this->t_festival as $fld=>$_v) { //07/04/08 ai viré le test && empty($_v) car une valeur vide EST significative !
				if (!in_array($fld,$arrUpperKeys) && !in_array($fld,$this->arrPrimaryKeys)) {
					unset($this->t_festival[$fld]);
				}
			}
		}
		if ($tab_valeurs['arrFather']) $this->t_festival['FEST_ID_GEN']=$tab_valeurs['arrFather'][0]['ID_FEST']; // Récup du parent depuis le tableau
		$this->t_section=$tab_valeurs['t_section'];
		$this->arrFather=$tab_valeurs['arrFather'];
	}


	/**
	 * Récupère les infos sur un ou plusieurs festivals.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets fest, sinon màj de l'objet en cours
	 */

	function getFest($arrFest=array(),$version=""){
		return $this->getData($arrFest,$version);
	}
	 
	function getData($arrData=array(),$version=""){
		global $db;
		$arrFest = parent::getData($arrData,$version);
		if (empty($arrFest) && !empty($this->t_festival) && !$arrData) { 
				$this->spaceQuotes(array(" ' "," ’ "),array("'","’"));
				$this->getVignette();
				if (!empty($this->t_festival['FEST_ID_GEN'])) {
						$this->arrFather=$db->GetAll("SELECT * FROM t_festival WHERE ID_FEST=".intval($this->t_festival['FEST_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_festival['ID_LANG']));
					}
				}
		else {
            foreach ($arrFest as $idx=>$Fest) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrFest[$idx]->spaceQuotes(array(" ' "," ’ "),array("'","’"));
				$arrFest[$idx]->getVignette();
			}
			unset($this); // plus besoin
			return $arrFest;
		}
	}



	/**
	 * Récupère la vignette de la notice
	 * IN : objet FEST
	 * OUT : var de classe vignette mise à jour.
	 */
	function getVignette() {
		global $db;

		if (!empty($this->t_festival['FEST_ID_IMAGE'])) {
			$vignetteChemin=$db->GetOne('SELECT REPLACE( '.$db->Concat('DA_CHEMIN',"'/'",'DA_FICHIER').' , \'//\', \'/\' ) FROM t_doc_acc WHERE ID_DOC_ACC='.intval($this->t_festival['FEST_ID_IMAGE']));
			$relativePathFromMedia=str_replace(kCheminHttpMedia,'',kDocumentUrl);
			if ($vignetteChemin) $this->vignette= (strpos($vignetteChemin,'http://')===0?$vignetteChemin:$relativePathFromMedia.$vignetteChemin);
		}
	}


	/** Récupère les libellés pour toutes les langues de l'appli.
	 *  IN : rien (objet)
	 * 	OUT : tableau de classe arrVersions
	 *  NOTE : si le lexique existe déjà en base, on ramènes ttes les langues même si le lib n'existe pas encore (right join)
	 * 		   si le lexique  n'existe pas (création), on ramène toutes les langues.
	 */
	function getVersions () {
		global $db;

		if (!empty($this->t_festival['ID_FEST'])) {
			$sql="SELECT t_lang.ID_LANG, FEST_DESC, ID_FEST FROM t_festival
					 INNER join t_lang on (t_lang.ID_LANG=t_festival.ID_LANG)
					WHERE
					t_festival.ID_LANG<>".$db->Quote($this->t_festival['ID_LANG'])." AND ID_FEST=".intval($this->t_festival['ID_FEST']);
			$this->arrVersions=$db->GetAll($sql);
		}
	}

	/**
	 * Récupère les documents d'accompagnement, toutes versions confondues.
	 */

	function getDocAcc() {
		global $db;
		require_once(modelDir.'model_docAcc.php');
		unset($this->t_doc_acc);
		$sql="SELECT ID_DOC_ACC,ID_LANG FROM t_doc_acc WHERE ID_FEST=".intval($this->t_festival['ID_FEST'])." 
				AND ID_LANG=".$db->Quote($this->t_festival['ID_LANG'])." ORDER BY ID_DOC_ACC,ID_LANG";//by ld 250808 restriction langue
		$rs=$db->GetAll($sql);
		foreach ($rs as $idx=>$fld) {
			$docAcc= new DocAcc();
			$docAcc->t_doc_acc['ID_DOC_ACC']=$fld['ID_DOC_ACC'];
			$docAcc->t_doc_acc['ID_LANG']=$fld['ID_LANG'];
			$docAcc->getDocAcc();
			$docAcc->getValeurs();
			$this->t_doc_acc[]=$docAcc;
		}
		//debug($this->t_doc_acc,'yellow');
	}

	/**
	 * Récupère les ID des sections liées à ce festival
	 * IN : rien
	 * OUT : tab de classe t_section
	 */

	function getChildren() {
		global $db;
		$sql="SELECT * from t_festival WHERE FEST_ID_GEN=".intval($this->t_festival['ID_FEST'])."  AND ID_LANG=".$db->Quote($this->t_festival['ID_LANG']);
		$this->t_section=$db->GetAll($sql);
	}

	/**
	 * Récupère la liste des rôles possibles pour un ID_TYPE_DESC
	 * IN : ID_TYPE_DESC
	 * OUT : tableau de valeurs de type ID/LIBELLE
	 * NOTE : cette fonction peut paraître hors sujet mais cette relation ID_TYPE_DESC / ROLES ne se pose que lors de la
	 * saisie des fest
	 */

	 function getRoles($id_type_desc,$add_blank=false) {
	 	global $db;
	 	if (!$id_type_desc) return false;
	 	if ($add_blank) $arrRoles[]='---';
	 	$rs=$db->Execute("SELECT ID_ROLE,ROLE  FROM t_role WHERE ID_TYPE_DESC=".$db->Quote($id_type_desc)." AND ID_LANG=".$db->Quote($this->t_festival['ID_LANG'])." ORDER BY ROLE" );

	 	while ($row=$rs->FetchRow()) {
	 		$arrRoles[$row['ID_ROLE']]=$row['ROLE'];
	 	}
	 	$rs->Close();
	 	return $arrRoles;
	 }


	/** MAJ dans la base des champs de type XML : doc_val, doc_xml, doc_lex
 	 *  Cette fonction doit être appelée APRES la sauvegarde complète de la fiche
 	 *  IN : objet doc, type (champ à mettre à jour)
 	 *  OUT : ce champs est mis à jour dans t_doc
 	 *  Note : ces champs sont utilisés en cas de recherche FULLTEXT sur tous les champs texte d'un document
 	 */

    function updateChampsXML($type,$arrLangues=null){
        global $db;
			if ($arrLangues===null) $arrLangues=$_SESSION['arrLangues'];
            foreach($arrLangues as $value)
            {
                
                //Section (= festival fils)
                $sql = "SELECT T2.FEST_LIBELLE, T2.ID_FEST, T2.FEST_ANNEE
					FROM t_festival T1 
					LEFT OUTER JOIN t_festival T2 ON (T2.FEST_ID_GEN=T1.ID_FEST AND T1.ID_LANG=T2.ID_LANG) 	
					WHERE T1.ID_FEST=".intval($this->t_festival['ID_FEST'])." and T1.ID_LANG='".$value."'";
               
                $result=$db->Execute($sql);
                $fest_xml="";
                while($list=$result->FetchRow()) {
                    $fest_xml.="<SECTION><FEST_LIBELLE>".$list["FEST_LIBELLE"]."</FEST_LIBELLE>";
                    $fest_xml.="<ID_FEST>".$list["ID_FEST"]."</ID_FEST>";
                    $fest_xml.="<FEST_ANNEE>".$list["FEST_ANNEE"]."</FEST_ANNEE></SECTION>";
                }
                $result->Close;
                
                //Doc acc
                $sql = "select distinct da.DA_TITRE,da.DA_FICHIER,da.DA_CHEMIN,da.ID_LANG,dav.ID_TYPE_VAL,v.VALEUR,v.ID_VAL
						from t_doc_acc da left OUTER join
						t_doc_acc_val dav on (dav.id_doc_acc=da.id_doc_acc) 
						left OUTER join  t_val v on (dav.id_val=v.id_val and da.id_lang=v.id_lang)
						where da.id_fest=".intval($this->t_festival['ID_FEST'])." and da.id_lang=".$db->Quote($value);
                $dacs=$db->GetAll($sql);
                foreach ($dacs as $dac) {
                	$fest_xml.="<DOC_ACC>";
                	$fest_xml.="<DA_TITRE>".$dac['DA_TITRE']."</DA_TITRE>";
                	$fest_xml.="<DA_FICHIER>".$dac['DA_FICHIER']."</DA_FICHIER>";
					$fest_xml.="<DA_CHEMIN>".$dac['DA_CHEMIN']."</DA_CHEMIN>";
                	$fest_xml.="<ID_LANG>".$dac['ID_LANG']."</ID_LANG>";
                	$fest_xml.="<ID_TYPE_VAL>".$dac['ID_TYPE_VAL']."</ID_TYPE_VAL>";
                 	$fest_xml.="<VALEUR>".$dac['VALEUR']."</VALEUR>";
                	$fest_xml.="<ID_VAL>".$dac['ID_VAL']."</ID_VAL>";
                	$fest_xml.="</DOC_ACC>";               	
                }
                
                
                $fest_xml=str_replace("'"," ' ",$fest_xml);
				if (!empty($fest_xml)) $fest_xml="<XML>".$fest_xml."</XML>";
                //$sql="UPDATE t_festival SET FEST_XML=".$db->Quote($fest_xml)." WHERE ID_FEST=".intval($this->t_festival['ID_FEST'])." AND ID_LANG=".$db->Quote($value);
               	//debug($sql,'beige');
                //$ok=$db->Execute($sql);
				$rs = $db->Execute("SELECT * FROM t_festival WHERE ID_FEST=".intval($this->t_festival['ID_FEST'])." AND ID_LANG=".$db->Quote($value));
                $sql = $db->GetUpdateSQL($rs, array("FEST_XML" => $fest_xml));
                if (!empty($sql)) {
                    $ok = $db->Execute($sql);
                    if (!$ok) echo "Pb SQL Maj XML >>> ".$sql;
                }
               
            }
    }
	

    /**
     * Création en base d'un nouveau festival avec les infos de base (initialisées à la créa objet)
     * IN : tab class t_festival
     * OUT : nv festival créé en base, var de classe ID_FEST renseignée avec nv ID
     *
     */

    function initFest($id_fest=0){
		global $db;
		$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
		if ($id_fest) $record["ID_FEST"] = $id_fest;
		$record["ID_LANG"] = $this->t_festival['ID_LANG'];
		$ok = $db->insertBase("t_festival","id_fest",$record);
        if (!$ok) {$this->dropError(kErrorCreationFest);return false;}
		
        $this->t_festival['ID_FEST']=($id_fest==0?$ok:$id_fest);
        return true;
    }



    /**
     * Duplication du festival
     * IN : this
     * OUT : nouveau festival dupliqué.
     */
    function duplicateFest(){
    global $db;

		if (empty($this->arrVersions)) $this->getVersions(); // Récup des infos si pas encore loadées
		if (empty($this->t_fest_lex)) $this->getLexique();
		if (empty($this->t_fest_val)) $this->getValeurs();

        $dupFest=new Festival();
        $dupFest=$this; // Recopie des infos
        unset($dupFest->t_festival['ID_FEST']); // pour éviter les effets de bord
        $dupFest->fest_trad=1; // pour générer les autres versions
        $dupFest->initFest(); // récupération du nouvel ID
        $dupFest->save(1); // Sauvegarde avec désactivation du checkExist.

        $dupFest->saveFestLex();
        $dupFest->saveFestVal();
        return $dupFest;
    }

	/**
		* Sélecteur d'entités à sauvegarder
	 * IN : tableau des entités à sauver
	 * OUT : sauvegardes
	 * NOTE : utilisé pour les modifs par lots
	 */
	function multiSave($arr2save=array()) {
		debug($arr2save);
		$this->save();
		if (array_search('VAL',$arr2save)!==false) {$this->saveFestVal(false);}//$this->updateChampsXML('DOC_VAL');}
		if ($arr2save['LEX']) $this->saveFestLex();
		if ($arr2save['SEQ']) $this->saveSection();

		return true;
	}
	
	function checkExist() {
		global $db;
		$sql="SELECT ID_FEST FROM t_festival WHERE 1=1 ";
					foreach ($this->uniqueFields as $fld) {
						$sql.=" AND ".strtoupper($fld)."=".$db->Quote($this->t_festival[strtoupper($fld)]);
					}
					if ($this->t_festival['ID_FEST']) $sql.=" AND ID_FEST!=".intval($this->t_festival['ID_FEST']);
		            debug($sql,'pink');
		            $existDeja=$db->GetOne($sql);
		return $existDeja;
	
	}
	
    /**
     * Sauve le festival en base
     * 1. Si créa d'un nv festival, vérifie si le festival existe  et abandonne si oui
     * 2. Sinon, crée un nv festival provisoire et récupère l'ID.
     * 3. QQ soit le mode, les valeurs des champs sont updatées en base...
     * 4. ... en deux phases : update pour la langue demandée, puis update pr ttes les langues dispo
     *
     * IN : langue (version à sauver), flag dupliquer O/N,
     * OUT : infos sauvées en base ou retour à false en cas d'erreur.
     */
    function save($dupliquer=0,$failIfExists=true){
        global $db;

		if ($dupliquer==0) {
			$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
		}
        $db->StartTrans();
        
		if (empty($this->t_festival['ID_LANG']))
			$this->t_festival['ID_LANG'] = $_SESSION['langue'];
		
        if (empty($this->t_festival['ID_FEST'])) {
        	if ($dupliquer==0) {
	           if (!empty($this->uniqueFields)) { //Le test d'unicité se base sur le param config uniqueFields qui contient les champs constituant la clé unique
					
		            $existDeja=$this->checkExist();
					debug('FESTIVAL>>'.$existDeja,'red');
		            if (!$existDeja){
		                $this->initFest();
		            }
		            else {
		            	$this->t_festival['ID_FEST']=$existDeja;
		                if ($failIfExists) {
		                	$this->dropError(kErrorFestivalExistant);
			                $db->CompleteTrans();
			                return FALSE;
		                }
	            	}
	           } else $this->initFest(); //pas de test d'unicité => on crée systématiquement un nv ID_FEST
        	}
        } else { //on a un ID spécifié <= import
        	$exist=$this->checkExistId();
        	if (!$exist) $this->initFest($exist);
        }
        
		$this->t_festival['FEST_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_festival['FEST_ID_USAGER_MODIF']=$_SESSION['USER']['ID_USAGER'];
		
		//Si c juste une année, on ajoute un mois-jour
		if (preg_match('@^[0-9]{4}$@i',$this->t_festival['FEST_ANNEE'])==1) $this->t_festival['FEST_ANNEE'].="-01-01"; 
		
		$record = array();
		foreach ($this->t_festival as $fld=>$val) {
			if (strpos($fld,'FEST_')===0 && in_array($fld,$this->arrFieldsToSaveWithIdLang))
				$record[$fld] = $val;
		}
		if (isset($record)) { //il y a des champs ? on execute l'ordre
			$rs = $db->Execute("SELECT * FROM t_festival where ID_FEST=".intval($this->t_festival['ID_FEST'])." AND ID_LANG=".$db->Quote($this->t_festival['ID_LANG']));
			$sql = $db->GetUpdateSQL($rs, $record);
			if (!empty($sql)) $ok = $db->Execute($sql);
		}
        unset($record);

		$record = array();
		foreach ($this->t_festival as $fld=>$val) {

		if (strpos($fld,'FEST_')===0 && in_array($fld,$this->arrFieldsToSaveWithoutIdLang))
				$record[$fld] = $val;
		}
		if (isset($record)) { //il y a des champs ? on execute l'ordre
			$rs = $db->Execute("SELECT * FROM t_festival where ID_FEST=".intval($this->t_festival['ID_FEST']));
			$sql = $db->GetUpdateSQL($rs, $record);
			if (!empty($sql)) $ok = $db->Execute($sql);
		}

        // I.5. G�n�ration des versions traduites. On ne g�n�re pas de version traduite pour les festivals dupliqu�s
        if($dupliquer==0){ 
        		$this->saveVersions();
        }

       if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveFest);trace($sql); return false;}


    return true;
    }

/**
 * Sauve les documents associés à un festival et les valeurs associées au lien DOC_FEST_VAL
 * IN : tableau t_doc_fest
 * OUT : base mise à jour
 */
	function saveDocFest() {
		global $db;
		$db->Execute("delete from t_doc_fest where id_fest=".intval($this->t_festival['ID_FEST']));
		foreach ($this->t_doc_fest as $idx=>$doc) {
			$ok = $db->insertBase("t_doc_fest","id_doc_fest",array("ID_FEST" => intval($this->t_festival['ID_FEST']), "ID_DOC" => intval($doc['ID_DOC'])));
			$iddf=$ok;
		
			if ($doc['DOC_FEST_VAL'] && is_array($doc['DOC_FEST_VAL'])) {
				foreach ($doc['DOC_FEST_VAL'] as $val) {
					$db->Execute("DELETE FROM t_doc_fest_val WHERE ID_DOC_FEST = ".intval($iddf)." AND ID_VAL = ".intval($val['ID_VAL']));
					$ok = $db->insertBase("t_doc_fest_val","id_doc_fest",array("ID_DOC_FEST" => intval($iddf), "ID_VAL" => intval($val['ID_VAL']), "ID_TYPE_VAL" => $val['ID_TYPE_VAL']));
				}
			}
			
		}	
	}
	
	function checkExistId() {
		global $db;
		$sql="SELECT ID_FEST FROM t_festival WHERE ID_FEST=".intval($this->t_festival['ID_FEST']);
		return ($db->GetOne($sql));
	}
	


	/** Sauve les informations des FEST_LEX
	 *  IN : tableau FEST_LEX
	 *  OUT : base mise à jour
	 */
	function saveFestLex($purge=true) {
		global $db;
		 if (empty($this->t_festival['ID_FEST'])) {$this->dropError(kErrorDeleteFestNoId);return false;}
		 if (empty($this->t_fest_lex)) return false;
		$db->StartTrans();
		// Purge générale ? Ok pour le mode édition fest, mais pas pour l'import
		if ($purge) $ok=$db->Execute('DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX from t_doc_lex WHERE ID_FEST='.intval($this->t_festival['ID_FEST']).')');
		if (!$ok) trace($db->ErrorMsg());
		if ($purge) $ok=$db->Execute('DELETE FROM t_doc_lex WHERE ID_FEST='.intval($this->t_festival['ID_FEST']));
		if (!$ok) trace($db->ErrorMsg());
		trace(print_r($this->t_fest_lex,1));
		foreach($this->t_fest_lex as $idx=>$fLex) {

			if ($fLex['LEXIQUE']) unset($fLex['LEXIQUE']); //On ôte l'objet encapsulé s'il existe

			// On notera que ID_FEST est celui en cours et pas celui contenu dans le tableau.
			// Effectivement, en cas de duplication, on récupère le tableau avec l'ID_FEST de l'original
			


			if ($fLex['ID_LEX'] || $fLex['ID_PERS']) {
				//Pas de purge générale, donc on micro-purge la valeur. Ajouter TYPE_LEX ?
				if (!$purge) $db->Execute('DELETE FROM t_doc_lex WHERE ID_FEST='.intval($this->t_festival['ID_FEST'])
														.' AND ID_LEX='.intval($fLex['ID_LEX']).' AND ID_PERS='.intval($fLex['ID_PERS']));
														
				$ok = $db->insertBase("t_doc_lex","id_doc_lex", array("ID_FEST" => intval($this->t_festival['ID_FEST']),"ID_LEX" => intval($fLex['ID_LEX']),"ID_PERS" => intval($fLex['ID_PERS']),"ID_TYPE_DESC" => $fLex['ID_TYPE_DESC'],"DLEX_ID_ROLE" => "".$fLex['DLEX_ID_ROLE'],"DLEX_REVERSEMENT" => "".$fLex['DLEX_REVERSEMENT']));
				if (!$ok) trace($db->ErrorMsg());
				$idDocLex=$ok;
	
				if (!empty($fLex['LEX_PRECISION'])) {
				$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX=".intval($idDocLex)." AND ID_LANG=".$db->Quote($this->t_festival['ID_LANG']));
				$ok = $db->insertBase("t_doc_lex_precision","id_doc_lex", array("ID_DOC_LEX" => intval($idDocLex), "ID_LANG" => $this->t_festival['ID_LANG'], "LEX_PRECISION" => $fLex['LEX_PRECISION']));
				if (!$ok) trace($db->ErrorMsg());
				}
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveFestLex);trace($sql); return false;} else return true;
	}

	/**
	 * Sauve les informations d'un lien FEST / VALEUR
	 * IN : tableau t_fest_val, $purge=>vidage t_fest_val avant full insert
	 * OUT : base mise à jour
	 * NOTE : si on a des champs VALEUR et TYPE_VAL mais pas d'ID_VAL (par exemple pour les imports)
	 * 	on va tenter de le récupérer
	 */
	function saveFestVal($purge=true) {
		global $db;
		 if (empty($this->t_festival['ID_FEST'])) {$this->dropError(kErrorDeleteFestNoId);return false;}
		 if (empty($this->t_fest_val)) return false;
		$db->StartTrans();
		// Purge générale ? Utile si on crée une fiche en mode édition. Pas en mode import
		if ($purge) $db->Execute('DELETE FROM t_fest_val WHERE ID_FEST='.intval($this->t_festival['ID_FEST']));
		foreach ($this->t_fest_val as $idx=>$fVal) {
			
			if ($fVal['ID_VAL']=='' && $fVal['VALEUR']!='') //Récup d'ID, par exemple, en cas d'import, où pas d'ID
				$fVal['ID_VAL']=$db->GetOne('SELECT ID_VAL from t_val WHERE VALEUR LIKE '.$db->Quote($fVal['VALEUR']).
											  ' AND VAL_ID_TYPE_VAL='.$db->Quote($fVal['ID_TYPE_VAL']));
			
			if ($fVal['ID_VAL'] && $fVal['ID_VAL']!='') { //vérif existence id_val=> peut-être vide si l'on choisit une valeur parmi une dropliste qui
										 // qui contient une valeur vide.
				//Précaution en cas de duplication : on spécifie bien que l'ID_FEST est celui en cours.
				// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_FEST originel dedans.
				$fVal['ID_FEST']=$this->t_festival['ID_FEST'];
				
				
				//Pas de purge générale, donc on micro-purge la valeur. Ajouter TYPE_VAL ?
				if (!$purge) $db->Execute('DELETE FROM t_fest_val WHERE ID_FEST='.intval($fVal['ID_FEST'])
													." AND ID_VAL=".intval($fVal['ID_VAL'])
													." AND ID_TYPE_VAL=".$db->Quote($fVal['ID_TYPE_VAL']));
				
				$ok = $db->insertBase("t_fest_val","id_fest",array("ID_FEST" => intval($fVal['ID_FEST']), "ID_VAL" => $fVal['ID_VAL'], "ID_TYPE_VAL" => "".$fVal['ID_TYPE_VAL']));
				if (!$ok) trace($db->ErrorMsg());
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveFestVal);trace($sql); return false;} else return true;
	}

	


	/** Sauve un festival dans les autres langues disponibles dans OPSIS
	 *  IN : tableau de classe t_festival, $tabLangues (opt) = tableau des langues que l'on veut sauver
	 *  OUT : bdd màj avec insertion t_festival des nv langues avec contrôle d'existence
	 * 		  + recopie des t_doc_lex_precision pour ces nouvelles versions
	 *  NOTES : pas d'update, juste insertion (contrôle si version existe)
	 * 			se base sur le tableau des champs dans t_festival -> on a retiré l'insertion de ts les champs (15/02/08)
	 */
	function saveVersions($tabLangues='') {
	        global $db;
			//si on ne passe pas de tableau, on utilise le tableau de ttes les langues en session
			if ($tabLangues=='') $tabLangues=$_SESSION['arrLangues']; 
	        if (empty($this->t_festival['ID_FEST'])) {$this->dropError(kErrorDeleteFestNoId);return false;}
			$ok=true;
	        
	            foreach($tabLangues as $lg){
	            if (strtoupper($lg)==strtoupper($_SESSION['langue'])) continue;
                // I.5.a. On s'assure que la version � cr�er n'existe pas d�j�
                $existVersion=$db->Execute("SELECT ID_FEST FROM t_festival WHERE ID_FEST=".intval($this->t_festival['ID_FEST'])." AND ID_LANG=".$db->Quote($lg));
                 if($existVersion->RecordCount()==0){
                    
					$record = $this->t_festival;
					$record['ID_LANG'] = $lg;
					if (empty($record['FEST_ID_USAGER_MODIF'])) $record['FEST_ID_USAGER_MODIF'] = 0;
				
					$ok = $db->insertBase("t_festival","id_fest",$record);
                    if (!$ok) {$this->dropError(kErrorSauveAutreVersion." (".$lg.")");trace($sql);}

                   // I.5.b Insertion des pr�cisions des versions traduites dans t_doc_lex_precision
                    $db->Execute("INSERT INTO t_doc_lex_precision SELECT t_doc_lex_precision.ID_DOC_LEX, '".($lg)."', t_doc_lex_precision.LEX_PRECISION FROM t_doc_lex_precision, t_doc_lex WHERE t_doc_lex.ID_DOC_LEX=t_doc_lex_precision.ID_DOC_LEX AND t_doc_lex.ID_FEST='".$this->t_festival['ID_FEST']."'");
                }
            }
    if (!$ok) return false;
	return true;
	}



    /**
     * Récupère les infos sur le festival + d'autre types d'infos selon les params
     * IN : bool pour charger LEX, VAL, MAT, IMG, SEQ, REDIF
     * OUT : tableaux de classe fest et autres tableaux selon les extensions.
     * NOTE : ramène aussi les libellés utilisateurs
     */
    function getFestFull($charger_lex=0,$charger_val=0,$charger_seq=0,$charger_doc=0){
	   global $db;
	   $this->getFest();
	   $this->user_modif=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_festival['FEST_ID_USAGER_MODIF']));

       if ($charger_lex!=0)  {$this->getLexique();$this->getPersonnes();}
       if ($charger_val!=0)  $this->getValeurs();
	   if ($charger_seq!=0) $this->getSections();
	   if ($charger_doc!=0) $this->getDocs();
    }


	/**

	 * Récupère les informations sur un festival (t_festival) et les stocke dans autant de variables
	 * de classe. On retire l'espace autour des apostrophes pour les champs texte
	 * IN : $id_fest, $id_lang
	 * OUT : variables de classe
	 */

	function getFestExtended($id_fest,$id_lang) {
	    global $db;
        $sql="SELECT T1.*, T2.FEST_LIBELLE as FEST_LIBELLE_PARENT, T3.US_NOM as US_NOM_MODIF,
				t_image.IM_CHEMIN, t_image.IM_FICHIER";
        $sql.=" FROM t_festival T1";
		$sql.=" LEFT OUTER JOIN t_image ON t_image.ID_IMAGE=T1.FEST_ID_IMAGE";
        $sql.=" LEFT OUTER JOIN t_festival T2 ON (T1.FEST_ID_GEN=T2.ID_FEST AND T2.ID_LANG=".$db->Quote($id_lang).")";
        $sql.=" LEFT OUTER JOIN t_usager T3 ON (T1.FEST_ID_USAGER_MODIF=T3.ID_USAGER)";
        $sql.=" WHERE T1.ID_FEST=".intval($id_fest)." and T1.ID_LANG=".$db->Quote($id_lang);
        $result=$db->Execute($sql);

        if ($result->RecordCount()>0){
            $list = $result->FetchRow();
            foreach ($list as $key => $value) {
                $champ = strtolower($key);
                $this->$champ = $value;
            }
			$this->spaceQuotes(array(" ' "," ’ "),array("'","’"));
       }
	}


	/**
	 * Récupère les termes de lexique + précisions + rôles associés à un festival
	 * IN : ID_FEST et ID_LANG
	 * OUT : tableau t_fest_lex (classe)
	 */

	function getLexique () {
		    global $db;

		    $sql = "SELECT t_doc_lex.*, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_TERME, t_lexique.LEX_NOTE, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE";
            $sql.= " FROM t_doc_lex INNER JOIN t_lexique ON t_doc_lex.ID_LEX=t_lexique.ID_LEX ";
            $sql.= " LEFT OUTER JOIN t_doc_lex_precision ON (t_doc_lex_precision.ID_DOC_LEX=t_doc_lex.ID_DOC_LEX AND t_doc_lex_precision.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']).") ";
            $sql.= " LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']).") ";
            $sql.= " WHERE t_doc_lex.ID_PERS=0 AND t_doc_lex.ID_FEST=".intval($this->t_festival['ID_FEST'])." AND t_lexique.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']);

            $this->t_fest_lex=$db->GetAll($sql);
	}

	/**
	 * Récupération des personnes liées au festival via la table t_doc_lex
	 * IN : id_fest, id_lang (var de classe)
	 * OUT : tableau t_pers_lex.
	 * NOTE : ATTENTION ce tableau s'appelle bien pour commodité t_pers_lex mais n'a RIEN à voir avec la table t_pers_lex
	 */
	function getPersonnes() {
			global $db;
		    $sql = "SELECT t_doc_lex.*, t_personne.*, t_doc_acc.DA_FICHIER, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE";
            $sql.= " FROM t_doc_lex INNER JOIN t_personne ON t_doc_lex.ID_PERS=t_personne.ID_PERS ";
            $sql.= " LEFT JOIN t_doc_acc ON (t_personne.PERS_PHOTO=t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']).") ";
            $sql.= " LEFT JOIN t_doc_lex_precision ON (t_doc_lex_precision.ID_DOC_LEX=t_doc_lex.ID_DOC_LEX AND t_doc_lex_precision.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']).") ";
            $sql.= " LEFT JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']).") ";
            $sql.= " WHERE t_doc_lex.ID_LEX=0 AND t_doc_lex.ID_FEST=".intval($this->t_festival['ID_FEST'])." 
					AND t_personne.ID_LANG=".$db->Quote($this->t_festival['ID_LANG'])." ORDER BY t_personne.PERS_NOM";

            $this->t_fest_pers=$db->GetAll($sql);
	}


	/**
	 * Récupère les valeurs liées à un festival
	 * IN : ID_FEST et ID_LANG
	 * OUT : tableau tab_val (classe)
	 */
	function getValeurs() {
			global $db;
		    $sql = "SELECT * FROM t_fest_val
					INNER JOIN t_val ON t_fest_val.ID_VAL=t_val.ID_VAL
					WHERE t_fest_val.ID_FEST=".intval($this->t_festival['ID_FEST'])." 
					AND t_val.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']);
            $this->t_fest_val=$db->GetAll($sql);
            trace($sql);
	}


	/**
	 * Récupère la liste des sections associées à un festival (ie => sous-festival)
	 * IN : tableau classe fest
	 * OUT : tableau tab_seq (classe)
	 */
	function getSections($extended=false) {
		global $db;
        $sql="select distinct T1.*,
				 ".$db->Concat('t_doc_acc.DA_CHEMIN',"'/'",'DA_FICHIER ')." as DA_FICHIER
				from t_festival T1
				INNER join t_festival T2 ON T1.FEST_ID_GEN=T2.ID_FEST
				left OUTER join t_doc_acc on (T1.FEST_ID_IMAGE=t_doc_acc.ID_DOC_ACC)
				where T2.ID_FEST=".intval($this->t_festival['ID_FEST'])." and T1.ID_LANG=".$db->Quote($this->t_festival['ID_LANG'])." order by T1.FEST_LIBELLE"; 
		$this->t_section=$db->GetAll($sql);
		if ($extended) {
			foreach ($this->t_section as $idx=>$sct) {
				$dumb=new Festival();
				$dumb->t_festival['ID_FEST']=$sct['ID_FEST'];
				$dumb->t_festival['ID_LANG']=$this->t_festival['ID_LANG'];
				$dumb->getDocs();
				$this->t_section[$idx]['FESTIVAL']=$dumb;
				unset($dumb);			
			}
		}
	}

/**
 * Récupère la liste des documents liés à un festival et des valeurs associées à ce lien
 */
	function getDocs() {
		global $db;
		//NOTE : à faire évoluer en tableau à la demande
		$sql="SELECT d.ID_DOC, d.DOC_TITREORI, d.DOC_SOUSTITRE, d.DOC_TITRE, d.DOC_TITRE_COL,
				d.DOC_COTE_ANC, d.DOC_AUTRE_TITRE, d.DOC_XML, d.DOC_LEX, d.DOC_VAL, df.ID_DOC_FEST,d.DOC_DATE_PROD,d.DOC_ACCES
				from t_doc d
				INNER JOIN t_doc_fest df ON df.id_doc=d.id_doc 
				WHERE ID_FEST=".intval($this->t_festival['ID_FEST'])." AND d.ID_LANG=".$db->Quote($this->t_festival['ID_LANG']); 
		$this->t_fest_doc=$db->GetAll($sql);
		foreach ($this->t_fest_doc as $i=>$df) {
			$sql="SELECT * from t_doc_fest_val dfv
					INNER JOIN t_val v ON dfv.ID_VAL=v.ID_VAL 
					where v.ID_LANG=".$db->Quote($this->t_festival['ID_LANG'])." and dfv.ID_DOC_FEST=".intval($df['ID_DOC_FEST']);
			$_d=$db->GetAll($sql);
			if (!empty($_d)) $this->t_fest_doc[$i]['DOC_FEST_VAL']=$_d;
			unset ($_d);
		}
	}


    /**
     * Supprime un festival de la base ou bien juste une version (si id_lang est passé)
     * En cas de suppression totale du festival, celui-ci est effacé + liens au lexique, matériel, valeurs et panier
     * IN : id_lang (opt, pour supprimer juste une version)
     * OUT : update BDD
     */

    function deleteFest($id_lang=""){
        global $db;

        if (empty($this->t_festival['ID_FEST'])) {$this->dropError(kErrorDeleteFestNoId);return false;}

        if (empty($id_lang)) {
	        // II.1.C. Suppression des enregistrements li�s
	        deleteSQLFromArray(array("t_festival","t_doc_lex","t_fest_val"),"ID_FEST",$this->t_festival['ID_FEST']);
			$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_FEST= ".intval($this->t_festival['ID_FEST']).")");
        } else {

	        deleteSQLFromArray(array("t_festival"),array("ID_FEST","ID_LANG"),array($this->t_festival['ID_FEST'],$id_lang));
			$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_FEST= ".intval($this->t_festival['ID_FEST']).") and ID_LANG='".($id_lang)."'");
        }
        return true;
    }

	/** Supprime toutes les sections ainsi que les liens de ces séquences : matériel, valeur, lex, etc
	 * 	IN : array t_section de l'objet (chargé automatiquement si vide)
	 *  OUT : update BDD puis vidage tableau t_section
	 */
	function deleteSections($keepArray=true) {
		global $db;

		if (empty($this->t_section)) $this->getSections();
		foreach ($this->t_section as $seq) {
	        if (!empty($seq['ID_FEST'])) {
		        deleteSQLFromArray(array("t_festival","t_doc_lex","t_fest_val"),"ID_FEST",$seq['ID_FEST']);
				$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_FEST = ".intval($seq['ID_FEST']).")");
	        }
		}
		if (!$keepArray) unset ($this->t_section);
				
	}
	
	/** Supprime une section
	 * 	IN : array fest 
	 *  OUT : update BDD 
	 */
	function deleteSection($fest) {
		global $db;

	        if (!empty($fest['ID_FEST'])) {
		        deleteSQLFromArray(array("t_festival","t_doc_lex","t_fest_val"),"ID_FEST",$fest['ID_FEST']);
				$ok=$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_FEST = '".($fest['ID_FEST'])."')");
	        }
	}	

    //*** XML
    function xml_export($entete=0,$encodage=0,$prefix=""){
        $content="";
        $search = array(" ' ",'&','<','>');
        $replace = array("'",'&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
        $content.=$prefix."<t_festival id_fest=\"".$this->t_festival['ID_FEST']."\">\n";

            // D'abord l'objet
                $list_class_vars = get_class_vars(get_class($this));
                foreach ($this->t_festival as $name => $value) {
					if(substr($value,0,5)=="<XML>") $content .= $prefix."\t<".strtoupper($name).">".str_replace(array(" ' ","&"),array("'","&amp;"),$value)."</".strtoupper($name).">\n";
					else //$content .= $prefix."\t<".strtoupper($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".strtoupper($name).">\n";
                    	$content .= $prefix."\t<".strtoupper($name).">".str_replace($search,$replace, $value)."</".strtoupper($name).">\n";
                    }

                //$content .=$prefix. "\t<LIBELLE_PARENT>".str_replace($search,$replace, ereg_replace("\n|\r","|",$this->arrFather[0]['FEST_LIBELLE']))."</LIBELLE_PARENT>\n";
                $content .=$prefix. "\t<LIBELLE_PARENT>".str_replace($search,$replace, $this->arrFather[0]['FEST_LIBELLE'])."</LIBELLE_PARENT>\n";
                //by ld 17/07/08 : retrait du chemin serveur
                $content .=$prefix. "\t<VIGNETTE>".$this->vignette."</VIGNETTE>\n";
                $content .=$prefix. "\t<US_MODIF>".$this->user_modif."</US_MODIF>\n";


            if (!empty($this->t_fest_lex)) {
			// I.2. Propri�t�s relatives � t_fest_lex
            $content.=$prefix."\t<t_fest_lex nb=\"".count($this->t_fest_lex)."\">\n";

            	// Constitution du tableau hiérarchique TYPE_DESC => ROLE => lexique
                foreach ($this->t_fest_lex as $idx => $lex) {
                        $arrRoles[$lex["ID_TYPE_DESC"]][$lex["DLEX_ID_ROLE"]][]=$lex;
                }
                   foreach ($arrRoles as $type_desc => $rol) {
                    $content .=$prefix. "\t\t<TYPE_DESC ID_TYPE_DESC=\"".$type_desc."\" >\n";
                        foreach ($rol as $role=>$lexs) {
                        $content .=$prefix. "\t\t\t<t_rol DLEX_ID_ROLE=\"".$role."\" >\n";
	                     foreach ($lexs as $idx=>$lex)	{
	                        $content .=$prefix. "\t\t\t\t<t_lex ID_DOC_LEX=\"".$lex['ID_LEX']."\" >\n";
	                        foreach ($lex as $fld=>$value) {
	                                if(substr($value,0,5)=="<XML>") $content .=$prefix. "\t\t\t\t\t<".$fld.">".str_replace(" ' ","'",$value)."</".$fld.">\n";
	                                else $content .=$prefix. "\t\t\t\t\t<".$fld.">".str_replace($search,$replace, $value)."</".$fld.">\n";
	                        }
	                        $content .=$prefix. "\t\t\t\t</t_lex>\n";
	                        }
	                       	$content .=$prefix."\t\t\t</t_rol>\n";
                        }
                     $content .= $prefix."\t\t</TYPE_DESC>\n";
                    }
            $content.=$prefix."\t</t_fest_lex>\n";
            }
            unset($arrRoles); //reset tableau
            
            
            if (!empty($this->t_fest_pers)) {

            $content.=$prefix."\t<t_fest_pers nb=\"".count($this->t_fest_pers)."\">\n";

            	// Constitution du tableau hiérarchique TYPE_DESC => ROLE => lexique
                foreach ($this->t_fest_pers as $idx => $lex) {
                        $arrRoles[$lex["ID_TYPE_DESC"]][$lex["DLEX_ID_ROLE"]][]=$lex;
                }
                  //echo "<PRE>";print_r($arrRoles);echo "</PRE>";
	                   foreach ($arrRoles as $type_desc => $rol) {
                    $content .= $prefix."\t\t<TYPE_DESC ID_TYPE_DESC=\"".$type_desc."\" >\n";
                        foreach ($rol as $role=>$lexs) {
                        $content .=$prefix. "\t\t\t<t_rol DLEX_ID_ROLE=\"".$role."\" >\n";
	                     foreach ($lexs as $idx=>$lex)	{
	                        $content .=$prefix. "\t\t\t\t<t_personne ID_PERS=\"".$lex['ID_PERS']."\" >\n";
	                        // Pour chaque propri�t� du terme

	                        foreach ($lex as $fld=>$value) {
	                                $content .=$prefix. "\t\t\t\t\t<".$fld.">".str_replace($search,$replace, $value)."</".$fld.">\n";
	                        }
	                        $content .=$prefix. "\t\t\t\t</t_personne>\n";
                        }
	                         $content .=$prefix."\t\t\t</t_rol>\n";
                        }

                     $content .=$prefix. "\t\t</TYPE_DESC>\n";
                    }
            $content.=$prefix."\t</t_fest_pers>\n";
            }

            if (!empty($this->t_fest_val)) {
            // I.3. Propri�t�s relatives � t_doc_val
            $content.=$prefix."\t<t_fest_val nb=\"".count($this->t_fest_val)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_fest_val as $name => $value) {
                    $content .=$prefix. "\t\t<TYPE_VAL ID_TYPE_VAL=\"".$value['VAL_ID_TYPE_VAL']."\" >";
                    // Pour chaque valeur

                        $content .=$prefix. "\t\t<t_val ID_VAL=\"".$value['ID_VAL']."\">\n";
                        $content .=$prefix. "\t\t\t<ID_VAL>".$value['ID_VAL']."</ID_VAL>\n";
                        $content .=$prefix. "\t\t\t<VALEUR>".str_replace($search,$replace, $value['VALEUR'])."</VALEUR>\n";
                        $content .=$prefix. "\t\t</t_val>\n";

                    $content .=$prefix. "</TYPE_VAL>\n";
                }
            $content.=$prefix."\t</t_fest_val>\n";
            }



           if (!empty($this->t_section)) {
            $content.=$prefix."\t<t_section nb=\"".count($this->t_section)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_section as $name => $value) {
                    $content .= $prefix."\t\t<FEST ID_FEST=\"".$this->t_section[$name]["ID_FEST"]."\">";
                    // Pour chaque valeur
                    foreach ($this->t_section[$name] as $val_name => $val_value) {
						//pas d'encodage des chevrons pour les champs contenant du XML
						if (is_object($val_value)) { $content.="\t\t\t<".strtoupper($val_name).">".$val_value->xml_export(0,0,"\t\t\t\t")."</".strtoupper($val_name).">";} 
						elseif(substr($val_value,0,5)=="<XML>") $content .= $prefix."\t\t\t<".strtoupper($val_name).">".str_replace(array(" ' ","&"),array("'","&amp;"),$val_value)."</".strtoupper($val_name).">\n";
						else $content .= $prefix."\t\t\t<".strtoupper($val_name).">".str_replace($search,$replace,$val_value)."</".strtoupper($val_name).">\n";
                    }
                    $content .=$prefix. "\t\t</FEST>\n";
                }
            $content.=$prefix."\t</t_section>\n";
          }

          if (!empty($this->t_doc_acc)) {
            $content.=$prefix."\t<t_doc_acc nb=\"".count($this->t_doc_acc)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_doc_acc as $name => $value) {

					$content.=$prefix.$value->xml_export(0,0,chr(9).chr(9));
                }
            $content.=$prefix."\t</t_doc_acc>\n";
          }
          
          if (!empty($this->t_fest_doc)) {
            $content.=$prefix."\t<t_fest_doc nb=\"".count($this->t_fest_doc)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_fest_doc as $name => $doc) {
					$content.=$prefix."\t\t<DOC>";
					foreach ($doc as $name => $v) {
                    	if (is_array($v)) 
                    	 $content .= "\t\t\t".array2xml($v,$name)."\n";
                    	elseif(substr($v,0,5)=="<XML>") $content .= $prefix."\t\t\t<".strtoupper($name).">".str_replace(array(" ' ","&"),array("'","&amp;"),$v)."</".strtoupper($name).">\n";
                    	 else 
                   		 $content .= "\t\t\t<".strtoupper($name).">".str_replace($search,$replace, $v)."</".strtoupper($name).">\n";
                    	
                    }
                    $content.=$prefix."\t\t</DOC>";
                }
            $content.=$prefix."\t</t_fest_doc>\n";
          }          


        $content.=$prefix."</t_festival>\n";

        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }


	/**
	 * Insère une séquence d'un document en base 
	 * IN : TC IN, TC OUT, DUREE, TITRE SEQUENCE, LANGUE
	 * OUT : Insertion en base
	 */
    function insertSection($fest) {
    	global $db;
    	$mySeq=new Festival();
    	$mySeq->updateFromArray($fest,false);
		//debug($fest,'silver');
		
		//héritage automatique du festival vers la section
    	$mySeq->t_festival['FEST_ID_GEN']=$this->t_festival['ID_FEST'];    	
    	$mySeq->t_festival['FEST_ANNEE']=$this->t_festival['FEST_ANNEE'];      	
      	$mySeq->t_festival['ID_LANG']=$this->t_festival['ID_LANG'];
      	
		//Source import (mapFields) ? on la prend, sinon on prend la source formulaire
		if (!empty($fest['t_fest_lex']))	$mySeq->t_fest_lex=$fest['t_fest_lex'];
		if (!empty($fest['t_fest_val']))  $mySeq->t_fest_val=$fest['t_fest_val']; 
		if (!empty($fest['t_doc_fest']))  $mySeq->t_doc_fest=$fest['t_doc_fest']; 

    	
    	//trace(print_r($mySeq,true));    	
    	$rtn=$mySeq->save(0,false);
    	if (!$rtn) {$this->dropError(kErrorCreationFestSection." : ".$mySeq->error_msg);return false;}
    	
    	$idSection=$mySeq->t_festival['ID_FEST'];

    	$mySeq->saveFestLex();
    	$mySeq->saveFestVal();
    	$mySeq->saveDocFest();
    	//$mySeq->saveVersions(); pas utile car on crée déjà les versions au moment du SAVE
    	$mySeq->updateChampsXML('FEST_XML');
		$this->dropError($mySeq->error_msg); //transmission 

		unset ($mySeq); 
		return $idSection;
    }


	/**
	 * Sauvegarde des séquences par appel de insertSection() ou updateSection() suivant le cas
	 * IN :
	 * OUT : Insertion en base
	 */
    function saveSections() {
        if (empty($this->t_festival['ID_FEST'])) {$this->dropError(kErrorDeleteFestNoId);return false;}
    	global $db;
    	$db->StartTrans();
    	
    	//$this->deleteSections();
		foreach ($this->t_section as $idx=>$fest) {
				if ($fest['commande']=='SUP') $this->deleteSection($fest);
				else
				$this->insertSection($fest);
		}

    	$this->updateChampsXML('FEST_XML');
		
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveFestSection); return false;} else return true;

    }



    /**
     * Mise à jour base + objet de la vignette représentative de la notice.
     * IN : ID (id_doc_acc)
     * OUT : base + objet t_fest mis à jour
     * NOTE : 2 champs peuvent héberger une image représentative, aussi pour éviter toute confusion, l'autre est vidé
     * 		  lorsque l'un est mis à jour si une image est donnée en paramètre
	 *		CF code pour une explication des cas possibles
	 * NOTE2 : le paramètre type ne sert à rien MAIS DOIT ETRE GARDE pour la compatibilité entre ts les saveVignette
     */
	function saveVignette($type,$id) {
		//if (empty($id)) return false; Viré car il faut pouvoir déselectionner une image
		global $db;
		$sql="UPDATE t_festival SET FEST_ID_IMAGE=".intval($id)." WHERE ID_FEST=".intval($this->t_festival['ID_FEST']);
		$db->Execute($sql);
		$this->t_festival['FEST_ID_IMAGE']=$id;
	}

	function mapFields($arr) {
		global $db;
		require_once(modelDir.'model_personne.php');
		require_once(modelDir.'model_lexique.php');
		require_once(modelDir."model_val.php");
		require_once(modelDir.'model_materiel.php');
		
		foreach ($arr as $idx=>$val) {
			
			$myField=$idx;
			// analyse du champ
			if ($myField=='ID_FEST') $this->t_festival['ID_FEST']=$val;
			$arrCrit=explode("_",$myField);
			switch ($arrCrit[0]) {
				case 'FEST': //champ table FEST
					$val=trim($val);
					$flds=explode(",",$myField);
					if ($val==='0' || !empty($val)) foreach ($flds as $_fld) $this->t_festival[$_fld]=$val;
					break;
				
				case 'V': //valeur
					if (empty($val)) break;
					$myVal=new Valeur();
					$myVal->t_val['VALEUR']=$val;
					$myVal->t_val['VAL_ID_TYPE_VAL']=$arrCrit[1];
					$myVal->t_val['ID_LANG']=$_SESSION['langue'];
					$_val=$myVal->checkExist();
					if ($_val) {
						$myVal->t_val['ID_VAL']=$_val;
						$myVal->save(); }
					else {
						$myVal->create(); //création dans la langue
						foreach ($_SESSION['arrLangues'] as $lg) {
							if ($lg!=$_SESSION['langue']) $arrVersions[$lg]=$val;
						}
						$myVal->saveVersions($arrVersions); //sauve dans autres langues
						unset($arrVersions);
					}
					$this->t_fest_val[]=array('ID_TYPE_VAL'=>$arrCrit[1],'ID_VAL'=>$myVal->t_val['ID_VAL']);
					break;
					
				case 'L': //lexique
					if ($val!=='') {
						if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;}

						foreach($val as $thisVal) {
							$myLex=new Lexique();
							if (!$thisVal['LEX_TERME']) $thisVal['LEX_TERME']=$thisVal['VALEUR'];
							if (!$thisVal['LEX_ID_TYPE_LEX']) $thisVal['LEX_ID_TYPE_LEX']=$arrCrit[3]; //par déft, extrait du champ

							$thisVal['ID_LANG']=$this->t_festival['ID_LANG'];

							$myLex->createFromArray($thisVal, true, true);

							//Lien doc_lex
							$this->t_fest_lex[]=array('DLEX_ID_ROLE'=>($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]),
													 'ID_TYPE_DESC'=>($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]),
													 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
													 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
													 'ID_LEX'=>$myLex->t_lexique['ID_LEX']);
						}
					}
					break;
				case 'P': //personne
					if ($val!=='') {
						if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
						foreach($val as $thisVal) {
							
							$myPers=new Personne();
							if (!$thisVal['PERS_NOM'])
							$thisVal['PERS_NOM']=$thisVal['VALEUR'];
							if(!$thisVal['PERS_ID_TYPE_LEX'])
							$thisVal['PERS_ID_TYPE_LEX']=$arrCrit[3]; //par déft, extrait du champ
							
							$thisVal['ID_LANG']=$this->t_festival['ID_LANG'];
							
							$myPers->createFromArray($thisVal, true, true, true);
							
							$tmp_fest_lex=array('DLEX_ID_ROLE'=>($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]),
													 'ID_TYPE_DESC'=>($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]),
													 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
													 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
													 'PERS_ID_TYPE_LEX'=>$thisVal['PERS_ID_TYPE_LEX'],
													 'PERS_NOM'=>$thisVal['PERS_NOM'],
													 'ID_PERS'=>$myPers->t_personne['ID_PERS']);
							
							if($thisVal['PERS_PRENOM']){
								$tmp_fest_lex['PERS_PRENOM'] = $thisVal['PERS_PRENOM'];
							}
							
							$this->t_fest_lex[]= $tmp_fest_lex;
						}
					}
					break;
			}
		}
	}
	
	
	
	
} // fin classes
?>