<?
//************************************************** MODEL ***************************************************************

// VP : 15/03/2016 : Création de la classe Model

//************************************************** MODEL ***************************************************************
require_once(modelDir."model.php");
class Partage extends Model
{
	var $code_length  ; 
	var $shared_object ; 
	var $shared_children ;
	var $return_msg ; 
	var $params ; 


	function __construct($id=null, $version=""){
		parent::__construct("t_partage",$id,$version);
		$this->code_length = 10 ; 
		$this->shared_object = null ; 
		$this->shared_children = null ; 
	}
	
	
	function init() {
        global $db;
        $cols=$db->MetaColumns($this->table);
        foreach ($cols as $i=>$col) $this->{$this->table}[$i]='';
		
		// création d'un nouveau code random 
		$this->createNewCode();
		// alimentation de la date de création du partage
		$this->t_partage['PARTAGE_DATE_CREA'] = str_replace("'","",$db->DBTimeStamp(time()));
		// alimentation de l'usager à l'origine du partage
		if (isset($_SESSION['USER']['ID_USAGER']) && !empty($_SESSION['USER']['ID_USAGER']) && empty($this->t_partage['PARTAGE_ID_USAGER_CREA'])){			
			$this->t_partage['PARTAGE_ID_USAGER_CREA'] = $_SESSION['USER']['ID_USAGER'];
		}
		// alimentation de la langue destination du partage 
		if(isset($_SESSION['langue']) && !empty($_SESSION['langue']) && empty($this->t_partage['PARTAGE_ID_LANG'])){
			$this->t_partage['PARTAGE_ID_LANG'] = $_SESSION['langue'];
		}
    }
	
	// fonction de création d'un nouveau code random & unique de longueur {this->code_length} chars
	private function createNewCode(){
		global $db;
		require_once(libDir."fonctionsGeneral.php");
		
		$sql_verif_unique = "SELECT id_partage from t_partage where partage_code=";
		$i=0 ; 
		do{
			$this->t_partage['PARTAGE_CODE'] = random_alphanum_string(array('fullrandom_length'=>$this->code_length));
			$i++;
		}while($db->GetOne($sql_verif_unique.$db->Quote($this->t_partage['PARTAGE_CODE'])) !== null);
		//trace("PARTAGE - createNewCode : ".$this->t_partage['PARTAGE_CODE'].", ".$i." passe(s)");
	}
	
	function getPartage($withObject = true){
		return $this->getData(null,null,$withObject);
	}
	function getData($arrData=array(),$version="",$withObject = true){
		parent::getData() ; 
		if(!empty($this->t_partage['PARTAGE_XML'])){
			require_once(libDir."fonctionsXML.php");
			$this->t_partage['XML'] = xml2array($this->t_partage['PARTAGE_XML']);
		}
		if($withObject){
			$this->getSharedObject() ; 
		}
	}
	
	function getPartageFromCode($code,$withObject = true){
		if(!isset($code)||empty($code)){
			$this->dropError("getPartageFromCode - ".kErrorPartageCodeEmpty);
			return false ;
		}
		global $db;
		$id_partage = $db->GetOne('SELECT ID_PARTAGE FROM t_partage WHERE PARTAGE_CODE='.$db->Quote($code));
		
		if(!empty($id_partage)){
			$this->t_partage['ID_PARTAGE'] = $id_partage;
			$this->getPartage() ; 
			return true;
		}		
	}
	
	
	function updateFromArray ($tab_valeurs) {
		global $db ; 
		if(array_key_exists('PARTAGE_DATE_LIMIT',$tab_valeurs)){
			$tab_valeurs['PARTAGE_DATE_LIMIT'] = str_replace("'","",$db->DBTimeStamp($tab_valeurs['PARTAGE_DATE_LIMIT'] ));
		}else if(array_key_exists('partage_date_limit',$tab_valeurs)){
			$tab_valeurs['partage_date_limit'] = str_replace("'","",$db->DBTimeStamp($tab_valeurs['partage_date_limit'] ));
		}
		if(array_key_exists('partage_params',$tab_valeurs)){
			$this->params = $tab_valeurs['partage_params'];
		}
		//traiter le post et envoyer dans variable de la classe, unset du post
		return parent::updateFromArray($tab_valeurs);
	}
	
	
	function isValid(){
		// Si l'objet n'est pas initialisé => KO
		if(!isset($this->t_partage['ID_PARTAGE']) || empty($this->t_partage['ID_PARTAGE'])){
			$this->dropError("isValid - ".kErrorPartageObjetNonInitialise);
			return false ;
		}
		// Si l'objet a une  limite  => on teste
		if(isset($this->t_partage['PARTAGE_DATE_LIMIT']) && ! empty($this->t_partage['PARTAGE_DATE_LIMIT'])){
			// Si la date actuelle est < à PARTAGE_DATE_LIMIT => OK
			if(strtotime($this->t_partage['PARTAGE_DATE_LIMIT']) >= time() ){
				return true ; 
			}else {
			// Sinon KO 
				return false ;
			}
		// Si l'objet n'a pas de limite date définie => OK 
		}else{
			return true ; 
		}
	}
	
	// En fonction de partage_type_entite & partage_id_entite, cette fonction devra retourner l'entitée qui est l'objet du partage
	function getSharedObject(){
		if(!isset($this->t_partage['PARTAGE_TYPE_ENTITE']) || empty($this->t_partage['PARTAGE_TYPE_ENTITE'])  || !isset($this->t_partage['PARTAGE_ID_ENTITE']) || empty($this->t_partage['PARTAGE_ID_ENTITE'])
		){
			$this->dropError("getSharedObject - ".kErrorPartageMissingInfos);
			return false ; 
		}
		// récupération de l'entitée partagée en fonction du type_entite renseigné
		switch($this->t_partage['PARTAGE_TYPE_ENTITE']){
			case "doc" : 
				require_once(modelDir.'model_doc.php');
				$myDoc = new Doc() ;
				$myDoc->t_doc['ID_DOC'] = $this->t_partage['PARTAGE_ID_ENTITE'];
				$myDoc->t_doc['ID_LANG'] = $this->t_partage['PARTAGE_ID_LANG'];
				$myDoc->getDoc() ; 
				
				$this->shared_object = $myDoc;
			break ;
			case "panier" : 
				require_once(modelDir.'model_panier.php');
				$myPanier = new Panier();  
				$myPanier->t_panier['ID_PANIER'] = $this->t_partage['PARTAGE_ID_ENTITE'];
				$myPanier->t_panier['ID_LANG'] = $this->t_partage['PARTAGE_ID_LANG'];
				$myPanier->getPanier() ; 
			
				$this->shared_object = $myPanier;
			break ; 
			case "import" : 
				require_once(modelDir.'model_import.php');
				$myImp = new Import();  
				$myImp->t_import['ID_IMPORT'] = $this->t_partage['PARTAGE_ID_ENTITE'];
				//$myImp->t_import['ID_LANG'] = $this->t_partage['PARTAGE_ID_LANG'];
				$myImp->getImport() ; 
				$this->shared_object = $myImp;
			break ; 
		}
		if(!is_null($this->shared_object )){
			return $this->shared_object ; 
		}
		
	}
	
	
	// Cette fonction doit créer les sous-partages si necessaire : 
	// Pour l'instant lorsqu'on crée un partage d'un panier on va devoir créer les "sous partages" correspondant aux panierdocs 
	// => création d'un partage par document 
	function createSharedChildren(){
		if(!isset($this->t_partage['PARTAGE_TYPE_ENTITE']) || empty($this->t_partage['PARTAGE_TYPE_ENTITE'])  || !isset($this->t_partage['PARTAGE_ID_ENTITE']) || empty($this->t_partage['PARTAGE_ID_ENTITE'])
		){
			$this->dropError("createSharedChildren - ".kErrorPartageMissingInfos);
			return false ; 
		}
		require_once(libDir."fonctionsXML.php");
		
		$arr_fld_inherit = array('PARTAGE_ID_LANG','PARTAGE_TYPE','PARTAGE_DATE_CREA','PARTAGE_DATE_LIMIT','PARTAGE_ID_USAGER_CREA','PARTAGE_MAIL_DEST');
		
		switch ($this->t_partage['PARTAGE_TYPE_ENTITE']){
			case "panier" : 
				if(empty($this->shared_object)){
					$panier = $this->getSharedObject() ; 
				}else{
					$panier = $this->shared_object;  
				}
				$panier->getPanierDoc() ; 
							
				foreach($panier->t_panier_doc as $pdoc){
					$partage_fils = new Partage() ; 
					$partage_fils->init();
					foreach($arr_fld_inherit as $fld){
						$partage_fils->t_partage[$fld] = $this->t_partage[$fld];
					}
					$partage_fils->t_partage['PARTAGE_TYPE_ENTITE'] = "doc";
					$partage_fils->t_partage['PARTAGE_ID_ENTITE'] = $pdoc['ID_DOC'];
					$partage_fils->t_partage['PARTAGE_ID_GEN'] = $this->t_partage['ID_PARTAGE'];
					$partage_fils->getSharedObject() ;
					
					// on stock les infos panier_doc, qui pourraient etre perdues, dans le champ PARTAGE_XML qui permet de stocker diverses informations
					// $partage_fils->t_partage['XML'] = xml2array($partage_fils->t_partage['PARTAGE_XML']);
					foreach($pdoc as $field=>$value){
						if(!is_array($value) && !is_object($value)
						&& (strpos($field,'PDOC') === 0 || in_array($field,array('ID_LIGNE_PANIER','VIGNETTE','EXT_DUREE'))) ){
							$partage_fils->t_partage['XML']['t_panier_doc'][$field] = $value ; 
						}
					}
					$partage_fils->t_partage['PARTAGE_XML'] = array2xml($partage_fils->t_partage['XML']['t_panier_doc'],"t_panier_doc");
					if(!empty($this->params)){
						$partage_fils->params=$this->params;
					}
					$partage_fils->save() ;
					$this->shared_children[] = $partage_fils;
				}
			break ;			

			case "import" : 

				if(empty($this->shared_object)){
					$import = $this->getSharedObject() ; 
				}else{
					$import = $this->shared_object;  
				}
				 
							
				foreach($import->t_imp_docs as $imp_doc){
					$partage_fils = new Partage() ; 
					$partage_fils->init();
					foreach($arr_fld_inherit as $fld){
						$partage_fils->t_partage[$fld] = $this->t_partage[$fld];
					}
					$partage_fils->t_partage['PARTAGE_TYPE_ENTITE'] = "doc";
					$partage_fils->t_partage['PARTAGE_ID_ENTITE'] = $imp_doc['ID_DOC'];
					$partage_fils->t_partage['PARTAGE_ID_GEN'] = $this->t_partage['ID_PARTAGE'];
					$partage_fils->getSharedObject() ;

					// on stock les infos panier_doc, qui pourraient etre perdues, dans le champ PARTAGE_XML qui permet de stocker diverses informations
					// $partage_fils->t_partage['XML'] = xml2array($partage_fils->t_partage['PARTAGE_XML']);
					foreach($imp_doc as $field=>$value){
						if(!is_array($value) && !is_object($value) ){
							$partage_fils->t_partage['XML']['t_imp_docs'][$field] = $value ; 
						}
					}
					//$partage_fils->t_partage['PARTAGE_XML'] = array2xml($partage_fils->t_partage['XML']['t_panier_doc'],"t_panier_doc");
					if(!empty($this->params)){
						$partage_fils->params=$this->params;
					}
					$partage_fils->save() ;
					$this->shared_children[] = $partage_fils;
				}
			break ;
		}
		
		
		return true; 
	}
	
	// pas utilisé pour l'instant - correspond à une implémentation ou on stocke l'ensemble de l'entité dans le partage
	function copyObjectInXML(){
		if(isset($this->shared_object) && !empty($this->shared_object)){
			try{
				$xml = $this->shared_object->xml_export();
				if(!empty($xml) && $xml){
					$this->t_partage['PARTAGE_XML'] = trim($xml);
					return true ; 
				}
			}catch(Exception $e){
				$this->dropError("partage crash copyObjectInXML - ".$e->getMessage());
				trace("partage crash copyObjectInXML - ".$e->getMessage());
				return false;
			}
		}
	}
	
	
	
	function getSharedChildren(){
		global $db ;
		if(!isset($this->t_partage['ID_PARTAGE']) || empty($this->t_partage['ID_PARTAGE'])){
			$this->dropError("getSharedChildren - ".kErrorPartageObjetNonInitialise);
			return false ;
		}
		
		$children_ids = $db->GetCol("SELECT ID_PARTAGE FROM t_partage WHERE PARTAGE_ID_GEN=".$this->t_partage['ID_PARTAGE']." ORDER BY ID_PARTAGE ASC");
		
		$shared_children = array() ; 
		foreach($children_ids as $id){
			$partage_fils = new Partage(); 
			$partage_fils->t_partage['ID_PARTAGE'];
			$partage_fils->getPartage() ; 
			$shared_children[] = $partage_fils ; 
		}
		$this->shared_children = $shared_children ; 
	}
	
	
	
	
	
	
	function triggerShare($opts=array()){
		// TODO : 
		// Cette fonction devra, suivant le partage_type, réaliser les actions de partages : 
		// le premier cas à implémenter est l'envoi mail (via class_message)
		// A voir si on utilise directement la xsl panier ou si on crée une xsl qui se base sur un xml_export de la table partage ... 
		
		switch($this->t_partage['PARTAGE_TYPE']){
			case "MAIL" : 
				$xml_export = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>\n".$this->xml_export(0,'UTF-8')."</EXPORT_OPSIS>";
				
				require_once(libDir."fonctionsXML.php");
				require_once(modelDir.'model_message.php');
				
				
				if(isset($opts['sujet']) && !empty($opts['sujet'])){
					$sujet_msg = $opts['sujet'] ; 
				}else{
					$sujet_msg = kPartage.((isset($this->t_partage['PARTAGE_TITRE']) && !empty($this->t_partage['PARTAGE_TITRE']))?" ".$this->t_partage['PARTAGE_TITRE']:"");
				}
				// trace("xml_export : ".$xml_export);
				
				$param = array(); 
				if(isset($opts['corps']) && !empty($opts['corps'])){
					$param['corps'] = $opts['corps'];
				}
				$param['date_limite'] = $this->t_partage['PARTAGE_DATE_LIMIT'];
				
				$param['chemin_http'] = kCheminHttp;
				
				if(isset($opts['logo_url']) && !empty($opts['logo_url'])){
					$param['logo_url'] = $opts['logo_url'];
				}
				$corps_msg = TraitementXSLT($xml_export,$opts['xslfile'],$param);
				
				// trace("triggerShare MAIL - sujet_msg".$sujet_msg."\ncorps_msg".$corps_msg);
				
				$message = new Message() ;
				if(isset($this->t_partage['PARTAGE_ID_USAGER_CREA']) && !empty($this->t_partage['PARTAGE_ID_USAGER_CREA'])){
					require_once(modelDir.'model_usager.php');
					$sender = new Usager() ;
					$sender->t_usager['ID_USAGER'] = $this->t_partage['PARTAGE_ID_USAGER_CREA'];
					$sender->getUsager() ;
					if(!empty($sender->t_usager['US_SOC_MAIL'])){
						if(isset($opts['copy_to_sender']) && !empty($opts['copy_to_sender'])){
							$message->cc_to = $sender->t_usager['US_SOC_MAIL'];
						}
						$message->reply_to = $sender->t_usager['US_SOC_MAIL'];
					}
				}
				$ok = $message->send_mail($this->t_partage['PARTAGE_MAIL_DEST'],gMail,$sujet_msg,$corps_msg,"text/html");
				if($ok){
					$this->return_msg = sprintf(kPartageMailEnvoye,$this->t_partage['PARTAGE_MAIL_DEST']);
				}
				
				return true ; 
			break ; 
		}
		
		
	}
	
	
	/** Export XML de l'objet
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {
		if(!$this->table || !$this->key)
            return false;

        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<".$this->table.">\n";
		foreach ($this->{$this->table} as $fld=>$val) {
			if($fld == 'PARTAGE_XML'){
				$content.=$indent."\t<".strtoupper($fld).">".$val."</".strtoupper($fld).">\n";	
			}else{
				$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
			}
		}
		
		
		if(isset($this->shared_object) && !empty($this->shared_object) && is_object($this->shared_object)){
			$content.=$indent."\t<t_partage_objet>\n";
			$content.=$indent.$this->shared_object->xml_export(0,'UTF-8',$indent."\t\t") ;
			$content.=$indent."\t</t_partage_objet>\n";
		}
		
		
		if(isset($this->shared_children) && !empty($this->shared_children)){
			$content.="\t<t_partage_fils>\n";
			foreach($this->shared_children as $shared_child){
				$content.=$shared_child->xml_export(0,'UTF-8',$indent."\t\t");
			}
			$content.="\t</t_partage_fils>\n";
		}

		$content.=$indent."</".$this->table.">\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
function save() {
		global $db;
		if(!empty($this->params)){
       		$this->t_partage['PARTAGE_XML'] .= array2xml($this->params);
       	}
       	if(!empty($this->t_partage['PARTAGE_XML'])){
		$this->t_partage['PARTAGE_XML'] = '<params>'.$this->t_partage['PARTAGE_XML'].'</params>';
		}
		return parent::save();
	}
}

?>
