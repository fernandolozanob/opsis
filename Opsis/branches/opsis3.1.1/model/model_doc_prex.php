<?
//************************************************** ETAPE ***************************************************************

// VP : 27/11/2007 : Création du fichier, définition de la classe DroitPrex

//************************************************** ETAPE ***************************************************************
require_once(modelDir."/model.php");
// Attention par rapport à la classe générale model, ici on stocke dans t_prex au lieu de t_doc_prex ..
class DroitPrex extends Model
{

	var $t_prex = array();
	var $t_dp_pers = array();
	var $t_dp_val = array();
	var $error_msg;


	function __construct($id=null,$version='') {
		$this->error_msg='';
		parent::__construct('t_doc_prex',$id, $version); 
	}
	
	
	// MS - TODO refonte getData
	function getPrex($arrDPrex = array()) {
		global $db;
		$sql = "SELECT dp.*, dp.ID_DOC_PREX, dpp.ID_PERS, dpp.DPP_PRIX, dpp.DPP_DEVISE, dpp.DPP_COMMENT, ".$db->Concat('PERS_PRENOM',"' '","PERS_NOM")." as PERS_NOM
	    			FROM t_doc_prex dp
	    			LEFT OUTER JOIN t_dp_pers dpp on dpp.ID_DOC_PREX = dp.ID_DOC_PREX
	    			LEFT OUTER JOIN t_personne p on p.ID_PERS = dpp.ID_PERS
	    			WHERE dp.ID_DOC_PREX=" . intval($this->t_prex['ID_DOC_PREX']). " LIMIT 1";
					//. " GROUP BY dpp.ID_DOC_PREX, dp.ID_DOC_PREX";
        $this->t_prex = $db->GetAll($sql);

		if (count($this->t_prex)==1 && !$arrDPrex) { // Un seul résultat : on affecte le résultat à la valeur en cours
			$this->t_prex = $this->t_prex[0];
		}
		else {
			foreach ($this->t_prex as $idx=>$DPrex) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx] = new DroitPrex();
				$arrVals[$idx]->t_prex = $DPrex;
			}
			unset($this);
			return $arrVals;
		}
	}

	/**
	 * Récupère la personne liée au droit préexistant
	 * OUT : tableau t_dp_pers
	 */
	function getPersonne() {
			global $db;
		    $sql = "SELECT p.*, dpp.ID_DOC_PREX, dpp.DPP_PRIX, dpp.DPP_DEVISE, dpp.DPP_COMMENT
		    			FROM t_dp_pers dpp
		    			INNER JOIN t_personne p on p.ID_PERS = dpp.ID_PERS
		    			WHERE dpp.ID_DOC_PREX=" . intval($this->t_prex['ID_DOC_PREX']);
            $this->t_dp_pers = $db->GetAll($sql);
	}

	/**
	 * Récupère les valeurs liées au droit préexistant
	 * OUT : tableau t_dp_pers
	 */
	function getValeurs() {
			global $db;
		    $sql = "SELECT t_dp_val.*, t_val.VAL_ID_TYPE_VAL, VALEUR
		    			FROM t_dp_val
		    			INNER JOIN t_val on t_val.ID_VAL = t_dp_val.ID_VAL
		    			WHERE ID_DOC_PREX=" . intval($this->t_prex['ID_DOC_PREX']);
            $this->t_dp_val = $db->GetAll($sql);
	}

	/**
	 * Récupère toutes les données liées au droit préexistant
	 * OUT : tableau t_dp_pers
	 */
	function getPrexFull($with_pers = true, $with_val = true) {
		$this->getPrex();
		if ($with_pers)
			$this->getPersonne();
		if ($with_val)
			$this->getValeurs();
	}

	/**
	 * Ajoute un message d'erreur pour la sortie
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function dropError($errLib) {
		$this->error_msg .= $errLib."<br/>";
	}


	function init() {
		global $db;
		$cols = $db->MetaColumns('t_doc_prex');
		foreach ($cols as $i=>$col)
			$this->t_prex[$i] = '';
	}



	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {

	 	if (empty($this->t_prex))
	 		$this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_prex[strtoupper($fld)]))
				$this->t_prex[strtoupper($fld)] = $val;
		}

	}

	function checkExist() {
		global $db;
		return $db->GetOne("SELECT ID_DOC_PREX from t_doc_prex WHERE ID_DOC_PREX=".intval($this->t_prex['ID_DOC_PREX']));
	}

    function duplicate(){
   		global $db;
        $dupPrex = new DroitPrex();
        $dupPrex = $this;
        unset($dupPrex->t_prex['ID_DOC_PREX']);
        $ok = $dupPrex->create(false);
        if ($ok)
        	$dupPrex->dropError(kSuccesPrexDupliquer);
        return $dupPrex;
    }
	
    function mapFields($arr) {
		global $db;
		require_once(modelDir."model_val.php");
		$lang = $_SESSION["langue"];

		foreach ($arr as $idx=>$val) {
			$myField = $idx;
			if ($myField=='DP_TYPE'){
				
			}
			
			$arrCrit=split("_",$myField);
			switch ($arrCrit[0]) {
				case 'DP':
					$val=trim($val);
					$flds=split(",",$myField);
					if (!empty($val))
						foreach ($flds as $_fld)
							$this->t_prex[$_fld]=$val;
					break;
				//PC 25/11/10 Modification pour intergrer valeur multiples (séparé par des virgules)
				case 'VAL':
					foreach ($val as $k=>$v)
						foreach ($v as $fi=>$va){
							if ($fi == 'VALEUR'){
								foreach (explode(',', $va) as $vid=>$va){
									$va = str_replace('_', ' ', $va);
									$res = $db->GetAll("SELECT ID_VAL, VAL_ID_TYPE_VAL
														FROM t_val v
														WHERE VALEUR like '$va' and ID_LANG like '$lang';");
									if ($res && count($res) == 1){
										$va = $res[0]['ID_VAL'];
										$fi = 'ID_VAL';
										$this->t_dp_val[$k * 1000 + $vid]['ID_TYPE_VAL'] = $res[0]['VAL_ID_TYPE_VAL'];
										$this->t_dp_val[$k * 1000 + $vid]['ID_VAL'] = $res[0]['ID_VAL'];
									}
								}
							}
							if ($fi == 'DPV_NB')
								$this->t_dp_val[$k * 1000][$fi] = $va;
						}
					break;
				case 'PERS':
					require_once(modelDir.'model_personne.php');
					foreach ($val as $k=>$v){
						// VP 25/01/11 : création personne si non existe
						//						foreach ($v as $fi=>$va){
						//							if ($fi == 'PERS_NOM'){
						//								$va = str_replace('_', ' ', $va);
						//								$res = $db->GetAll("SELECT ID_PERS
						//													FROM t_personne
						//													WHERE PERS_NOM like '$va';");
						//								if ($res && count($res) == 1){
						//									$this->t_dp_pers[$k]['ID_PERS'] = $res[0]['ID_PERS'];
						//								}
						//							}
						//					}
						$myPers=new Personne();
						$myPers->updateFromArray($v, true);
						if (!empty($myPers->t_personne['ID_PERS'])) $exists=$myPers->checkExistId();
						else $exists=$myPers->checkExist(false);
						// Création si non existe
						if ($exists) $myPers->t_personne['ID_PERS']=$exists;
						else $myPers->initPersonne($myPers->t_personne['ID_PERS']);
						if(!empty($myPers->t_personne['ID_PERS'])){
							$this->t_dp_pers[$k]['ID_PERS'] = $myPers->t_personne['ID_PERS'];
							$this->t_dp_pers[$k]['DPP_PRIX'] = $v['DPP_PRIX'];
							$this->t_dp_pers[$k]['DPP_DEVISE'] = $v['DPP_DEVISE'];
						}
						unset($myPers);
					}
					break;
			}
		}
    }
    
	function save() {
		global $db;
		if (empty($this->t_prex['ID_DOC_PREX'])) {return $this->create();}

		$rs = $db->Execute("SELECT * FROM t_doc_prex WHERE ID_DOC_PREX=" . intval($this->t_prex['ID_DOC_PREX']));
		$sql = $db->GetUpdateSQL($rs, $this->t_prex);
		if (!empty($sql)) $ok = $db->Execute($sql);

		if (!$ok) {
			$this->dropError(kErrorDocPrexSauve);
			return false;
		}
		return true;
	}

	function create($chkDoublons=true) {
		global $db;
		if (!empty($this->t_prex['ID_DOC_PREX']))
			return $this->save();
		if ($chkDoublons && $this->checkExist()) {
			$this->dropError(kErrorDocPrexExisteDeja);
			return false;
		}

        if (intval($this->t_prex['ID_DOC_PREX']) == 0){
            unset($this->t_prex['ID_DOC_PREX']);
        }
		$ok = $db->insertBase("t_doc_prex","id_doc_prex",$this->t_prex);
		if (!$ok) {$this->dropError(kErrorDocPrexCreation);return false;}
		$this->t_prex['ID_DOC_PREX'] = $ok;

		return true;
	}

	function savePersonne() {
		global $db;
		if (empty($this->t_prex['ID_DOC_PREX'])) {$this->dropError(kErrorDocPrexNoId);return false;}
		if (empty($this->t_dp_pers)){
			$db->Execute('DELETE from t_dp_pers where ID_DOC_PREX='.intval($this->t_prex['ID_DOC_PREX']).'');
			return false;
		}
		$db->StartTrans();

		$tab=$db->GetAll("select ID_PERS from t_dp_pers where ID_DOC_PREX=".intval($this->t_prex['ID_DOC_PREX']));
		foreach($tab as $row) {$arrIDs[$row['ID_PERS']]=$row['ID_PERS'];}

		foreach($this->t_dp_pers as $idx=>&$pers) {

			// Vérification existence
			$sqlone='SELECT ID_PERS FROM t_dp_pers WHERE ID_DOC_PREX='.intval($this->t_prex['ID_DOC_PREX']);
			$idPers=$db->GetOne($sqlone);

			if(empty($idPers)){
				// INSERT t_dp_pers
				if(!empty($this->t_prex['ID_DOC_PREX']))
					$pers['ID_DOC_PREX']=$this->t_prex['ID_DOC_PREX'];
				$ok = $db->insertBase("t_dp_pers","id_doc_prex",$pers);
				if (!$ok) trace($db->ErrorMsg());
				unset($arrIDs[$pers['ID_PERS']]);
			}else{
				// UPDATE t_dp_pers
				if (!isset($pers['ID_DOC_PREX']))
					$pers['ID_DOC_PREX'] = $this->t_prex['ID_DOC_PREX'];
				$sql = "";
				$rs = $db->Execute("SELECT * FROM t_dp_pers WHERE ID_DOC_PREX = " . intval($this->t_prex['ID_DOC_PREX']). " AND ID_PERS =".intval($idPers));
				$sql = $db->GetUpdateSQL($rs, $pers);
				if (!empty($sql)) $ok = $db->Execute($sql);
		
				if (!$ok) trace($db->ErrorMsg());
				unset($arrIDs[$idPers]);
			}
		}
		// Purge éventuelle
		if(count($arrIDs)>0){
			$db->Execute('DELETE from t_dp_pers where ID_PERS in ('.implode(",",$arrIDs).') AND ID_DOC_PREX='.intval($this->t_prex['ID_DOC_PREX']));
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauvePrexPersonne);trace($sql); return false;} else return true;
	}

	function saveValeurs($arrType = array("EXPL", "LANG", "PF")) {
		global $db;

		if (!is_array($arrType))
			$arrType = array($arrType);
		foreach ($arrType as &$row)
			$row = "'$row'";
		if (empty($this->t_prex['ID_DOC_PREX'])) {$this->dropError(kErrorDocPrexNoId);return false;}
		if (empty($this->t_dp_val)){
			$db->Execute('DELETE from t_dp_val where ID_DOC_PREX='.intval($this->t_prex['ID_DOC_PREX']).'
							 AND ID_TYPE_VAL in (' . implode(",", $arrType) . ')');
			return false;
		}
		$db->StartTrans();

		$tab = $db->GetAll("select ID_VAL from t_dp_val where ID_DOC_PREX=".intval($this->t_prex['ID_DOC_PREX']).'
							 AND ID_TYPE_VAL in (' . implode(",", $arrType) . ')');
		foreach($tab as $row) {$arrIDs[$row['ID_VAL']]=$row['ID_VAL'];}

		foreach($this->t_dp_val as $idx=>&$val) {

			// Vérification existence
			$sqlone = 'SELECT ID_VAL FROM t_dp_val WHERE ID_DOC_PREX='.intval($this->t_prex['ID_DOC_PREX']) .
						' AND ID_VAL='.$db->Quote($val['ID_VAL']);
			$idVal = $db->GetOne($sqlone);

			if(empty($idVal)){
				$val['ID_DOC_PREX'] = $this->t_prex['ID_DOC_PREX'];
				$ok = $db->insertBase("t_dp_val","id_doc_prex",$val);
				if (!$ok) trace($db->ErrorMsg());
				unset($arrIDs[$val['ID_VAL']]);
			//PC 16/11/10 : Update du champs DPV_NB si valeur deja existente
			}elseif (isset($val['DPV_NB'])){
				$sql = "";
				$rs = $db->Execute("SELECT * FROM t_dp_val WHERE ID_DOC_PREX = " . intval($this->t_prex['ID_DOC_PREX']) . " AND ID_VAL = " .intval($val['ID_VAL']));
				$sql = $db->GetUpdateSQL($rs, array("DPV_NB" => intval($val['DPV_NB'])));
				if (!empty($sql)) $ok = $db->Execute($sql);
				if (!$ok && !empty($sql)) trace($db->ErrorMsg());
				unset($arrIDs[$idVal]);
			}
			else
				unset($arrIDs[$idVal]);
		}
		// Purge éventuelle
		if(count($arrIDs)>0){
			$db->Execute('DELETE from t_dp_val where ID_VAL in ('.implode(",",$arrIDs).') AND ID_DOC_PREX='.intval($this->t_prex['ID_DOC_PREX']).'');
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauvePrexVal);trace($sql); return false;} else return true;
	}

	function delete() {
		return deleteSQLFromArray(array("t_doc_prex","t_dp_pers","t_dp_val"), "ID_DOC_PREX", $this->t_prex['ID_DOC_PREX']);
	}


 	/** Export XML de l'objet usager
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {
        $content="";
        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0)+
			$content .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

		$content .= $indent."<t_doc_prex ID_DOC_PREX=\"".$this->t_prex['ID_DOC_PREX']."\">\n";
		// USAGER
		foreach ($this->t_prex as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}

        if (!empty($this->t_dp_val)) {
        // I.3. Propri�t�s relatives � t_doc_val
        $content.=$indent."\t<t_dp_val nb=\"".count($this->t_dp_val)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_dp_val as $name => $value) {
            	//by ld 11/12/08 => on utilise ID_TYPE_VAL (type de l'asso) plutôt que VAL_ID_TYPE_VAL (type de la valeur).
            	//ceci pour afficher le cas où la même valeur de base sert dans plusieurs types d'asso
            	//par exemple CDR : LANG => langue dialogue (LANG) + langue soustitre (SUB)
                $content .=$indent. "\t\t<TYPE_VAL ID_TYPE_VAL=\"".$value['ID_TYPE_VAL']."\" >";
                // Pour chaque valeur
            	// VP 23/11/09 : ajout VAL_CODE dans export
            	// VP 20/08/10 : ajout VAL_ID_GEN dans export

                    $content .=$indent. "\t\t<t_val ID_VAL=\"".$value['ID_VAL']."\">\n";
                    $content .=$indent. "\t\t\t<ID_VAL>".$value['ID_VAL']."</ID_VAL>\n";
                    $content .=$indent. "\t\t\t<VALEUR>".str_replace($search,$replace, $value['VALEUR'])."</VALEUR>\n";
					$content .=$indent. "\t\t\t<VAL_CODE>".str_replace($search,$replace, $value['VAL_CODE'])."</VAL_CODE>\n";
					$content .=$indent. "\t\t\t<VAL_ID_GEN>".str_replace($search,$replace, $value['VAL_ID_GEN'])."</VAL_ID_GEN>\n";
					$content .=$indent. "\t\t\t<DPV_NB>".str_replace($search,$replace, $value['DPV_NB'])."</DPV_NB>\n";
                    $content .=$indent. "\t\t</t_val>\n";

                $content .=$indent. "\t\t</TYPE_VAL>\n";
            }
        $content.=$indent."\t</t_dp_val>\n";
        }

		if (!empty($this->t_dp_pers)) {
			$content.=$indent."\t<t_dp_pers nb=\"".count($this->t_dp_pers)."\">\n";
			 foreach ($this->t_dp_pers as $name => $pers) {
			 	 $content .=$indent. "\t\t<DP_PERS ID_PERS=\"".$pers['ID_PERS']."\">\n";
			 	 foreach($pers as $fld => $val)
			 		$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
			 	 $content .=$indent. "\t\t</DP_PERS>\n";
			 }
			 $content .=$indent. "\t</t_dp_pers>\n";
		}

		$content.=$indent."</t_doc_prex>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
}

?>