<?php
require_once(modelDir.'model.php');
class Import extends Model{

	var $t_import;// tableau contenant les valeurs en base 
	var $t_imp_doc;
	var $t_imp_mat;
	var $sqlOrderDoc;
	var $sqlOrderMat;

    //*** Constructeur
    function __construct($id=null,$version=""){
		$this->entity = "t_import";
		parent::__construct('t_import',$id,$version);
	}
	
	public function getTypeImport(){
		global $db;

		$sql = "SELECT type_import from t_type_import where id_type_import=".$db->Quote($this->t_import['IMP_TYPE'])." AND ID_LANG=".$db->Quote($_SESSION['langue']);
		$type_imp = $db->GetRow($sql);
		if(!empty($type_imp) && !empty($type_imp['TYPE_IMPORT'])){
			$this->t_import['type_import'] = $type_imp['TYPE_IMPORT'];
		}else{
			trace("getTypeImport - impossible de recuperer type import, id:".$this->t_import['ID_IMPORT']);
		}
	}
	
	
	public function getImportUsager(){
		global $db ; 
	
		if(isset($this->t_import['IMP_ID_USAGER']) && !empty($this->t_import['IMP_ID_USAGER'])){
			$sql = "SELECT US_NOM || ' ' || US_PRENOM as IMP_USAGER 
			FROM  t_usager where id_usager=".$db->Quote($this->t_import['IMP_ID_USAGER']);
			$imp_usager = $db->getRow($sql);
			if(!empty($imp_usager) && !empty($imp_usager['IMP_USAGER'])){
				$this->t_import['IMP_USAGER'] = $imp_usager['IMP_USAGER'];
				return true ; 
			}else{
				trace("getImportUsager - impossible de recuperer imp_usager, id:".$this->t_import['ID_IMPORT']);
				return false ; 
			}
		}else{
			trace("getImportUsager - pas d'imp_id_usager, id:".$this->t_import['ID_IMPORT']);
			return false;
		}
	}
	
	
	/**
	 * R�cup�re les infos sur un ou plusieurs imports.
	 * IN : array de ID (opt).
	 * OUT : si array sp�cifi� -> tableau d'objets imports, sinon m�j de l'objet en cours
	 */
	public function getImport($arrImp=array()){
		return $this->getData($arrImp);
	}
	 
	public function getData($arrData=array(),$version=""){
		$arrImp = parent::getData($arrData);
		trace("getData : ".print_r($arrImp,true));
		if (empty($arrImp) && !empty($this->t_import) && !$arrData){  // Un seul r�sultat : on affecte le r�sultat � l'import en cours 
				trace("getdata1");
				$this->getTypeImport();
				$this->getImportUsager();
				$this->getImpDocs();
				$this->getImpMats();
		}else{
			trace("getdata2");
			foreach ($arrImp as $idx=>$import) { // Plusieurs r�sultats : on renvoie un tableau d'objet contenant les r�sultats
				$arrImp[$idx]->getImpDocs();
				$arrImp[$idx]->getImpMats();
				$arrImp[$idx]->getTypeImport();
				$arrImp[$idx]->getImportUsager();
			}
			unset($this); // plus besoin
			return $arrImp;
		}
	}
	
	// r�cup�re la liste des documents cr��s par l'import
	public function getImpDocs(){
		global $db;
	
		$sql = "SELECT t_doc.ID_DOC,t_job.ID_JOB as IMP_DOC_ID_JOB, t_job.JOB_PROGRESSION as IMP_DOC_JOB_PROG,t_job.JOB_ID_ETAT as IMP_DOC_JOB_ID_ETAT, t_etat_job.ETAT_JOB as IMP_DOC_JOB_ETAT , t_fonds.FONDS , t_type_doc.TYPE_DOC, t_mat.MAT_TYPE, t_mat.MAT_WIDTH,t_mat.MAT_HEIGHT,t_mat.MAT_NB_PAGES from t_doc 
		left join t_fonds on t_doc.doc_id_fonds=t_fonds.id_fonds and t_doc.id_lang=t_fonds.id_lang
		left join t_type_doc on t_doc.doc_id_type_doc=t_type_doc.id_type_doc and t_doc.id_lang=t_type_doc.id_lang
		left join t_doc_mat on t_doc.id_doc=t_doc_mat.id_doc
		join t_mat on t_doc_mat.id_mat=t_mat.id_mat and mat_id_import=".$db->Quote($this->t_import['ID_IMPORT'])."
		left join t_job on t_doc_mat.id_mat=t_job.job_id_mat AND t_job.job_id_proc<>0 and t_job.id_job_gen=".intval($this->t_import['IMP_ID_JOB'])."
		left join t_etat_job on t_job.JOB_ID_ETAT=t_etat_job.ID_ETAT_JOB and t_etat_job.ID_LANG=".$db->Quote($_SESSION['langue'])."
		WHERE DOC_ID_IMPORT=".$db->Quote($this->t_import['ID_IMPORT'])." 
		AND t_doc.ID_LANG=".$db->Quote($_SESSION['langue']);
		
		
		if(!empty($this->mediaFilter)){
			$sql.=" AND DOC_ID_MEDIA =".$db->Quote($this->mediaFilter);
		}		
		if(!empty($this->type_doc)){
			$sql.=" AND DOC_ID_TYPE_DOC =".$db->Quote($this->type_doc);
		}
		
		if (!empty($this->sqlOrderDoc))  $sql.=$this->sqlOrderDoc; 
		else $sql.=" ORDER BY t_doc.DOC_TITRE ASC"; 
		
		$imp_docs = $db->GetAll($sql);
		require_once(modelDir.'model_doc.php');
		foreach($imp_docs as $doc){
			$tmp_doc = new Doc();
			$tmp_doc->t_doc['ID_DOC']= $doc['ID_DOC'];
			$tmp_doc->getDoc();
			trace("imp_docs : ".print_r($doc,true));
			$tmp_doc->t_doc['IMP_DOC_ID_JOB'] = $doc['IMP_DOC_ID_JOB'];
			$tmp_doc->t_doc['IMP_DOC_JOB_PROG'] = $doc['IMP_DOC_JOB_PROG'];
			$tmp_doc->t_doc['IMP_DOC_JOB_ID_ETAT'] = $doc['IMP_DOC_JOB_ID_ETAT'];
			$tmp_doc->t_doc['IMP_DOC_JOB_ETAT'] = $doc['IMP_DOC_JOB_ETAT'];
			// trace("getImpDocs ".$tmp_doc->t_doc['ID_DOC'].", vignette: ".$tmp_doc->vignette);
			$tmp_doc->t_doc['VIGNETTE'] = $tmp_doc->vignette;
			$tmp_doc->t_doc['FONDS'] = $doc['FONDS'];
			$tmp_doc->t_doc['TYPE_DOC'] = $doc['TYPE_DOC'];
			// MS - 15.06.15 - ajout des infos mats pour affichage dans le r�sum� de l'import les dimensions / nb de pages en plus de la dur�e
			$tmp_doc->t_doc['MAT_TYPE'] = $doc['MAT_TYPE'];
			$tmp_doc->t_doc['MAT_WIDTH'] = $doc['MAT_WIDTH'];
			$tmp_doc->t_doc['MAT_HEIGHT'] = $doc['MAT_HEIGHT'];
			$tmp_doc->t_doc['MAT_NB_PAGES'] = $doc['MAT_NB_PAGES'];
			
			
			$this->t_imp_docs[] = $tmp_doc->t_doc;
		}
		$this->t_import['NB_DOCS'] = count($this->t_imp_docs);
		
		
		return true ; 	
	}
	
	// r�cup�re la liste des mat�riels cr��s par l'import
	public function getImpMats(){
		global $db;
		$sql = "SELECT *,t_job.ID_JOB as IMP_DOC_ID_JOB, t_job.JOB_PROGRESSION as IMP_MAT_JOB_PROG, t_job.JOB_ID_ETAT as IMP_MAT_JOB_ID_ETAT , t_etat_job.ETAT_JOB as IMP_MAT_JOB_ETAT 
			from t_mat 
			LEFT JOIN t_job ON t_mat.ID_MAT = t_job.JOB_ID_MAT AND t_job.job_id_module=0 
			left join t_etat_job on t_job.JOB_ID_ETAT=t_etat_job.ID_ETAT_JOB and t_etat_job.ID_LANG=".$db->Quote($_SESSION['langue'])."
			WHERE MAT_ID_IMPORT=".$db->Quote($this->t_import['ID_IMPORT']);
		
	
		if(!empty($this->mediaFilter)){
			$sql.=" AND MAT_ID_MEDIA =".$db->Quote($this->mediaFilter);
		}
	
		if (!empty($this->sqlOrderMat))  $sql.=$this->sqlOrderMat; 
		else $sql.=" ORDER BY t_mat.MAT_NOM ASC"; 
		
		// echo $sql;
		//trace("sql ".$sql);
		$imp_mats = $db->GetAll($sql);
		
		if(!empty($imp_mats)){
			$this->t_imp_mats = $imp_mats;
		}else if($imp_mats === false ){
			trace("erreur getImpMats ".$this->t_import['ID_IMPORT']);
			return false ; 
		}
		$this->t_import['NB_MATS'] = count($this->t_imp_mats);
		
		return true ;
	}
	
	
	// Fonction create de l'import
	//VG : id_usager_impWF : pas de session user qd on importe par watchfolder ou FTP. => possibilité de passer un id_usager
	public function create ($checkDoublon=true, $id_usager_impWF='') {
		global $db;

		$this->t_import['IMP_DATE']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_import['IMP_ID_USAGER']=$_SESSION['USER']['ID_USAGER'];
		if(empty($_SESSION['USER']['ID_USAGER'])) {
			$this->t_import['IMP_ID_USAGER'] = intval($id_usager_impWF);
		}
		// langue ? 
		// if (empty($this->t_import['ID_LANG'])) $this->t_import['ID_LANG']=$_SESSION['langue'];
		
		if(!isset($this->t_import['IMP_NOM']) || empty($this->t_import['IMP_NOM'])){
			$this->t_import['IMP_NOM'] = "Import ".date("d/m/Y H:i");
		}else if(strlen($this->t_import['IMP_NOM'])>250){
			$this->t_import['IMP_NOM'] = substr($this->t_import['IMP_NOM'],0,250);
		}
		
		
		if(!isset($this->t_import['IMP_TYPE']) || empty($this->t_import['IMP_TYPE'])){
			trace(kErrorImpNoType);
		}
		
		if (isset($this->t_import['ID_IMPORT']) && intval($this->t_import['ID_IMPORT']) == 0)
			unset($this->t_import['ID_IMPORT']);

		
		$ok = $db->insertBase("t_import","id_import",$this->t_import);
		if (!$ok) {
			$this->dropError(kErrorImpCreation);trace(kErrorImpCreation.$sql);
			return false;
		}
		$this->t_import['ID_IMPORT']=$ok;
		return true;
	}

    
    public function save(){
		global $db;
		
		if(!isset($this->t_import['ID_IMPORT']) || empty($this->t_import['ID_IMPORT'])){
			trace(kErrorImpSaveNoId);
			return false; 
		}
		
		$rs = $db->Execute("SELECT * FROM t_import WHERE ID_IMPORT=".intval($this->t_import['ID_IMPORT']));
		$sql = $db->GetUpdateSQL($rs, $this->t_import);
		if (!empty($sql)) $ok = $db->Execute($sql);
		
		if (!empty($sql) && !$ok) 
		{
			$this->dropError(kErrorImpSave);
			trace($sql);
			return false;
		}
		return $ok;
	}
	
	
	function createJobImport($params){
		require_once(modelDir.'model_job.php');
		
		$job_imp=new Job();
		if(!isset($this->t_import['type_import']) && !empty($this->t_import['IMP_TYPE'])){
			$this->getTypeImport();
		}
		
		if(isset($this->t_import['IMP_ID_JOB']) && !empty($this->t_import['IMP_ID_JOB'])){
			$job_imp->t_job['ID_JOB'] = $this->t_import['IMP_ID_JOB'];
		}
		
		$job_imp->t_job['JOB_NOM']= (isset($this->t_import['IMP_NOM'])?$this->t_import['IMP_NOM']:'').' '.(isset($this->t_import['type_import'])?$this->t_import['type_import']:'');
				
		$job_imp->t_job['JOB_ID_MAT']=0;
		$job_imp->t_job['JOB_IN']='';
		$job_imp->t_job['JOB_OUT']='';
		$job_imp->t_job['JOB_ID_MODULE']=0;
		if(isset($params) && !empty($params)){
			$job_imp->t_job['JOB_PARAM']='<param>'.$params.'</param>';
		}
		$job_imp->t_job['JOB_ID_SESSION']=session_id();
		$job_imp->t_job['JOB_DATE_CREA']=date('Y-m-d H:i:s');
		$job_imp->save();
		
		if(empty($this->t_import['IMP_ID_JOB'])){
			$this->t_import['IMP_ID_JOB'] = $job_imp->t_job['ID_JOB'];
		}
		
		$this->save();
	}
	
	
	// fonction d'export xml
	function xml_export($entete="",$encodage="",$indent=""){ 
		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
		$content.=$indent."<t_import>\n";
		// IMPORTS
		foreach ($this->t_import as $fld=>$imp) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($imp)."</".strtoupper($fld).">\n";
		}
		
		if(isset($this->t_imp_docs) && !empty($this->t_imp_docs)){
			$content.="\t<t_imp_docs>\n";
			foreach ($this->t_imp_docs as $imp_doc){
				$content.=$indent."\t\t<t_doc>\n";
				foreach ($imp_doc as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
						
				$content.=$indent."\t\t</t_doc>\n";
			}
			$content.="\t</t_imp_docs>\n";
		}
		if(isset($this->t_imp_mats) && !empty($this->t_imp_mats)){
			$content.="\t<t_imp_mats>\n";
			foreach ($this->t_imp_mats as $imp_mat){
				$content.=$indent."\t\t<t_mat>\n";
				foreach ($imp_mat as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
						
				$content.=$indent."\t\t</t_mat>\n";
			}
			$content.="\t</t_imp_mats>\n";
		}
		
		$content.=$indent."</t_import>\n";



		// Encodage si demand�
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}

	
	

}





?>

