<?
//************************************************** ETAPE ***************************************************************

// VP : 27/11/2007 : Création du fichier, définition de la classe Etape

//************************************************** ETAPE ***************************************************************
require_once(modelDir.'model.php');
class Etape extends Model 
{

	var $t_etape;
	var $error_msg;
	var $flag_runSyncJob;


	function __construct($id=null,$version='') {
		parent::__construct("t_etape",$id,$version);
	}

	function getEtape($arrEtape=array()){
		return $this->getData($arrEtape);
	}
	
	/*function getEtape($arrEtape=array()) {
		global $db;
		if (!$arrEtape) $arrVals=array($this->t_etape['ID_ETAPE']); else $arrVals=(array)$arrEtape;
		$arrCrit['ID_ETAPE']=$arrVals;

		$this->t_etape=getItem("t_etape",null,$arrCrit,null,0); //AJOUTER GESTION DES ORDRES

		if (count($this->t_etape)==1 && !$arrEtape) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->t_etape=$this->t_etape[0];
		}
		else {
			//VP 21/01/10 : correction getEtape par tableau
            $arrVals=array();
			foreach ($this->t_etape as $idx=>$Etape) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx]= new Etape();
				$arrVals[$idx]->t_etape=$Etape;
			}
			unset($this); // plus besoin
			return $arrVals;
		}
	}*/


	
	static function getEtapeFromParam($str_recherche)
	{
		global $db;
		
		if (!empty($str_recherche))
		{
			$result=$db->Execute("SELECT ID_ETAPE FROM t_etape WHERE ETAPE_PARAM LIKE '%".$str_recherche."%'")->GetRows();
			
			if (count($result)==1)
			{
				$result=$result[0]['ID_ETAPE'];
				$etape=new Etape();
				$etape->t_etape['ID_ETAPE']=$result;
				$etape->getEtape();
			
				return $etape;
			}
			else if (count($result)>=1)
			{
				$etapes=array();
				
				foreach($result as $res)
				{
					$etape=new Etape();
					$etape->t_etape['ID_ETAPE']=$res['ID_ETAPE'];
					$etape->getEtape();
					$etapes[]=$etape;
				}
				
				return $etapes;
			}
			else
				return null;
		}
		else
			return null;
		
		return null;
	}



	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {

	 	if (empty($this->t_etape)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_etape[strtoupper($fld)])) $this->t_etape[strtoupper($fld)]=$val;
		}

	}

	function checkExist() {
		global $db;
		return $db->GetOne("SELECT ID_ETAPE from t_etape
							WHERE LOWER(TRIM(ETAPE_NOM))=".$db->Quote(trim(strtolower($this->t_etape['ETAPE_NOM'])))
							.(!empty($this->t_etape['ID_ETAPE'])?" AND ID_ETAPE<>".intval($this->t_etape['ID_ETAPE']):"") );
	}

    function duplicate(){
    global $db;

		
        $dupEtape=new Etape();
        $dupEtape=$this; // Recopie des infos
        unset($dupEtape->t_etape['ID_ETAPE']); // pour éviter les effets de bord
        $ok=$dupEtape->create(false); // Création avec désactivation du checkExist.
        if ($ok) $dupEtape->dropError(kSuccesEtapeDupliquer);
        return $dupEtape;
    }

	function save() {
		global $db;
		if (empty($this->t_etape['ID_ETAPE'])) {return $this->create();}
		if ($this->checkExist()) {$this->dropError(kErrorEtapeExisteDeja);return false;}

		//$ok=$db->Execute('UPDATE t_etape SET ETAPE_NOM='.$db->Quote($this->t_etape['ETAPE_NOM']).' WHERE ID_ETAPE='.$db->Quote($this->t_etape['ID_ETAPE']));
		//by LD 05 05 09  : avant on ne sauvait que le nom
		$rs = $db->Execute("SELECT * FROM t_etape WHERE ID_ETAPE=".intval($this->t_etape['ID_ETAPE']));
		$sql = $db->GetUpdateSQL($rs, $this->t_etape);
		if (!empty($sql)) $ok = $db->Execute($sql);

		if (!$ok && !empty($sql)) {$this->dropError(kErrorEtapeSauve);return false;}
		return true;
	}

	function create($chkDoublons=true) {
		global $db;
		if (!empty($this->t_etape['ID_ETAPE'])) {return $this->save();}
		if ($chkDoublons && $this->checkExist()) {$this->dropError(kErrorEtapeExisteDeja);return false;}
		
		$ok = $db->insertBase("t_etape","id_etape",$this->t_etape);
		if (!$ok) {$this->dropError(kErrorEtapeCreation);return false;}
		$this->t_etape['ID_ETAPE']=$ok;

		return true;
	}


	function delete() {
		// VP 6/05/10 : suppression dans table liée (t_proc_etape)
		deleteSQLFromArray(array("t_etape","t_proc_etape"),"ID_ETAPE",$this->t_etape['ID_ETAPE']);
		/*
		global $db;
		$db->StartTrans();
		$db->Execute("DELETE from t_etape WHERE ID_ETAPE=".$db->Quote($this->t_etape['ID_ETAPE']));
		$ok=$db->CompleteTrans();
		if (!$ok) {$this->dropError(kErrorEtapeSuppr);return false;}
		*/
		return true;

	}

    // VP 10/05/12 : ajout paramètre plateforme pour gérer plusieurs frontaux
	function createJob($id_mat,$mat_nom,$id_job_gen=0,$id_job_prec=0,&$id_job_valid=0,&$jobObj,$xml_params="",$dynamic_args=null,$priority=2,$plateforme="",$job_date_lancement='') {
		// VP 27/11/09 : ajout paramètre $id_job_valid
		global $db;
		require_once(modelDir.'model_job.php');
		$jobObj=new Job;
		if(!empty($this->flag_runSyncJob) && $this->flag_runSyncJob){
			$jobObj->flag_runSyncJob = $this->flag_runSyncJob;
			if(!preg_match('/<run_sync_job>true<\/run_sync_job>/',$xml_params)){
				//trace("Etape : add run_sync_job");
				if(empty($xml_params)){
					$xml_params = "<param><run_sync_job>true</run_sync_job></param>" ;
				}else{
					$xml_params = str_replace("</param>",'<run_sync_job>true</run_sync_job></param>',$xml_params) ;
				}
			}
			//trace("Etape:createJob - set flag_runSyncJob for new job : intval : ".intval($this->flag_runSyncJob));
		}
		$jobObj->t_job["JOB_ID_ETAPE"]=$this->t_etape["ID_ETAPE"];
		$jobObj->t_job["JOB_ID_MODULE"]=$this->t_etape["ETAPE_ID_MODULE"];
		$jobObj->getModule();
		$jobObj->t_job["JOB_NOM"]=$this->t_etape["ETAPE_NOM"]." ".$mat_nom;
		$jobObj->t_job["ID_JOB_PREC"]=$id_job_prec;
		$jobObj->t_job["ID_JOB_VALID"]=$id_job_valid;
		$jobObj->t_job["ID_JOB_GEN"]=$id_job_gen;
		$jobObj->t_job["JOB_IN"]=$mat_nom;
		$jobObj->t_job["JOB_ID_MAT"]=$id_mat;
		$jobObj->t_job["JOB_PRIORITE"]=$priority;
        if(defined("kJobPlateforme")){
            $jobObj->t_job["JOB_PLATEFORME"]=(empty($plateforme)?kJobPlateforme:$plateforme);
        }
		
		if (isset($job_date_lancement) && !empty($job_date_lancement))
			$jobObj->t_job["JOB_DATE_LANCEMENT"]=$job_date_lancement;
		
		//$jobObj->t_job["JOB_OUT"]=str_replace($this->t_etape["ETAPE_IN"],$this->t_etape["ETAPE_OUT"],$id_mat);
		$jobObj->setOut($mat_nom,$this->t_etape["ETAPE_IN"],$this->t_etape["ETAPE_OUT"],$dynamic_args);

        // VP (27/10/2015 : modif merge avec priorité à paramètres hérités
        //$myPrms=$xml_params.$this->t_etape["ETAPE_PARAM"];
        //$myPrms=str_replace("</param><param>","",$myPrms); //fait pour virer les balises params au milieu si on a des params des 2 cotés
        $tabxml1 = xml2tab($xml_params, false, false);
        $tabxml2 = xml2tab($this->t_etape["ETAPE_PARAM"], false, false);

  		$tabxml1['param'] = $tabxml1['param'][0];
  		$tabxml2['param'] = $tabxml2['param'][0];
  		
        if(isset($tabxml1['param']) && isset($tabxml2['param']))
            $tabxml = array_merge ( $tabxml2['param'], $tabxml1['param']);
        elseif(isset($tabxml2['param']))
			$tabxml = $tabxml2['param'];
        elseif(isset($tabxml1['param']))
			$tabxml = $tabxml1['param'];

        $myPrms = array2xml($tabxml,'param');
        //VP 19/11/2015 : correction bug des doubles <param>
        $myPrms='<param>'.str_replace(array('<param>','</param>'),'',$myPrms).'</param>';
        
		if (!(isset($jobObj->t_job["ID_JOB_PREC"]) && !empty($jobObj->t_job["ID_JOB_PREC"])) || $jobObj->t_job["ID_JOB_PREC"]==0)
			$jobObj->t_job["JOB_ID_ETAT"]=1;
		
		$jobObj->t_job["JOB_PARAM"]=$myPrms; 
		$jobObj->t_job["JOB_DATE_CREA"]=date("Y-m-d H:i:s");
		$jobObj->t_job["JOB_ID_SESSION"]=session_id();
		$jobObj->create();
		if($jobObj->t_module['MODULE_TYPE']=='VALID') $id_job_valid=$jobObj->t_job["ID_JOB"];
		//debug($jobObj,'silver');
		return true;
	}




 	/** Export XML de l'objet usager
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {

        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_etape>\n";
		
		// USAGER
		foreach ($this->t_etape as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}


		$content.=$indent."</t_etape>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
}

?>