<?
require_once(modelDir.'model.php');
class DocAcc extends Model
{

	var $t_doc_acc;
	var $t_doc_acc_val; //Tableau des valeurs associées
	var $arrVersions; // Tableau des autres versions
	var $fileExists; // Fichier existe physiquement sur le disque, 1:oui, 0:non
	var $origFile; // Nom original du fichier. (Utilisé en cas de renommage)

	var $error_msg;

	function __construct($id=null,$version='') {
		parent::__construct('t_doc_acc',$id, $version); 
	}

	/**
	 * Récupère les infos sur un ou plusieurs doc_acc.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets lexique, sinon màj de l'objet en cours
	 */
	function getDocAcc($arrDA=array(),$version="") {
		return $this->getData($arrDA,$version)  ; 
	}
	
	
	function getData($arrData=array(),$version=""){
		global $db;
		$arrDocAcc=parent::getData($arrData,$version); 
		
		if (empty($arrDocAcc) && !empty($this->t_doc_acc) && !$arrData) { 
			$this->fileExists=$this->checkFileExists();
			$this->origFile=$this->t_doc_acc['DA_FICHIER'];			
		}else{
			foreach ($arrDocAcc as $idx=>$DocAcc) { 
				$arrDocAcc[$idx]->fileExists=$arrDocAcc[$idx]->checkFileExists();
				$arrDocAcc[$idx]->origFile=$arrDocAcc[$idx]->t_doc_acc['DA_FICHIER'];
			}
			return $arrDocAcc;
		}
	}
	
	/**
	 * Récupères les valeurs liées à un document d'accompagnement
	 * IN : ID_DOC et ID_LANG
	 * OUT : tableau t_doc_acc_val (classe)
	 */
	function getValeurs() {
			global $db;
			require_once(modelDir."model_val.php");
		    $sql = "SELECT t_doc_acc_val.* FROM t_doc_acc_val where t_doc_acc_val.ID_DOC_ACC=".intval($this->t_doc_acc['ID_DOC_ACC']);
            $this->t_doc_acc_val=$db->GetAll($sql);
            foreach ($this->t_doc_acc_val as $idx=>&$dav) {
            	  $val=new Valeur();
            	  $val->t_val['ID_VAL']=$dav['ID_VAL'];
            	  $val->t_val['ID_LANG']=$this->t_doc_acc['ID_LANG'];
            	  $val->getVal();

            	  $dav['VAL']=$val;
            	  unset($val);
            }
	}


	/** Récupère les libellés pour toutes les langues de l'appli.
	 *  IN : rien (objet)
	 * 	OUT : tableau de classe arrVersions
	 *  NOTE : si le doc acc existe déjà en base, on ramènes ttes les langues même si le lib n'existe pas encore (right join)
	 * 		   si le doc acc n'existe pas (création), on ramène toutes les langues.
	 */
	function getVersions () {
		global $db;
		$tabLg=$_SESSION['tab_langue'];
		unset($tabLg[$_SESSION['langue']]); // retrait de la langue en cours.
		if (!empty($this->t_doc_acc['ID_DOC_ACC'])) {
		$sql="SELECT t_lang.ID_LANG, DA_TITRE, DA_FICHIER, DA_CHEMIN, DA_TEXTE, ID_DOC_ACC, ID_DOC, ID_PERS FROM t_doc_acc
				right join t_lang on (t_lang.ID_LANG=t_doc_acc.ID_LANG)
				WHERE
				t_doc_acc.ID_LANG<>".$db->Quote($this->t_doc_acc['ID_LANG'])." AND ID_DOC_ACC=".intval($this->t_doc_acc['ID_DOC_ACC']);
		$this->arrVersions=$db->GetAll($sql);
		}

		if (empty($this->arrVersions)){ //Pas d'ID OU Pas de version enregistrée, on charge la liste des langages
		$sql="SELECT *,'' as DA_TITRE, '' as DA_FICHIER, '' as DA_CHEMIN,'' as DA_TEXTE,  null as ID_DOC_ACC, null as ID_DOC, null as ID_PERS FROM t_lang
				WHERE
				ID_LANG<>".$db->Quote($this->t_doc_acc['ID_LANG']);
		$this->arrVersions=$db->GetAll($sql);
		}
	}


	/** Vérifie si un DocAcc existe déjà en base
	 * 	Critères d'unicité sur le fichier
	 *  IN : objet doc acc
	 *  OUT : TRUE si existe, FALSE si libre
	 */
	function checkExist() {
		global $db;
		$sql="SELECT ID_DOC_ACC FROM t_doc_acc WHERE TRIM(LOWER(DA_FICHIER))=".$db->Quote(trim(strtolower($this->t_doc_acc['DA_FICHIER'])))
			 ." AND ID_LANG=".$db->Quote($this->t_doc_acc['ID_LANG']);
			//si le doc acc existe déjà en base, on l'exclue de la recherche de doublon
		if (!empty($this->t_doc_acc['ID_DOC_ACC'])) $sql.=" AND ID_DOC_ACC<>".$this->t_doc_acc['ID_DOC_ACC'];
		$rs=$db->GetRow($sql);
		if (count($rs)==0) return false; else return $rs['ID_DOC_ACC']; //note : on utilise GetAll qui discarde le rs
	}

	function checkExistId() {
		global $db;
		$sql="SELECT ID_DOC_ACC FROM t_doc_acc WHERE ID_DOC_ACC=".intval($this->t_doc_acc['ID_DOC_ACC']);
		$sql.=" AND ID_LANG=".$db->Quote($this->t_doc_acc['ID_LANG']);
		return ($db->GetOne($sql));
	}

	/** CheckFileExists (doc_acc)
	 * IN : t_doc_acc['DA_FICHIER'], const. gDocAccAlsoCheckUrl
	 * OUT : 1(fichier / url existe ou pas de vérif) / 0 (fichier absent ou mauvaise URL)
	 * NOTE : selon le type de 'DA_FICHIER' (fichier local ou url) le comportement diffère
	 * 		URL => si gDocAccAlsoCheckUrl=true, on vérifie l'url vraiment sinon on renvoie 1 (vrai)
	 * 			ATTENTION ! Cette vérif d'url demande BCP de temps ET n'est pas infaillible ! (ex: NOOS qui affiche une page qd url fausse)
	 * 		FILE => on vérifie si le fichier existe sur le serveur
	 */
	function checkFileExists() {
		if (strpos($this->t_doc_acc['DA_FICHIER'], "http") ===0) {
			if(!defined('gDocAccAlsoCheckUrl') || gDocAccAlsoCheckUrl==false) return 1; else return $this->url_exists($this->t_doc_acc['DA_FICHIER']);
		}
		//LD 06/11 : détection OS => si Win, décodage utf8 pour gérer les fichiers accentués
		if (stripos(PHP_OS,'WIN')!==false) $str=utf8_decode($this->t_doc_acc['DA_FICHIER']); else $str=$this->t_doc_acc['DA_FICHIER'];
		if (!empty($this->t_doc_acc['DA_CHEMIN'])) $str = $this->t_doc_acc['DA_CHEMIN']."/".$str;
		if (file_exists(kDocumentDir.$str)) return 1; else return 0;
	}


	function existence($fichier) { //permet de tester l'existence d'un fichier

		if (@fclose(@fopen($fichier, 'r')))
			return TRUE;
		else
			return FALSE;
	}

	function url_exists($url) {
	     if ((strpos($url, "http")) === false) $url = "http://" . $url;
	     if (is_array(@get_headers($url)))
	          return 1;
	     else
	          return 0;
	}
	
	function getFilePath()
	{
		if (strpos($this->t_doc_acc['DA_FICHIER'],'http://')===0) return $this->t_doc_acc['DA_FICHIER'];
	
		if (empty($this->t_doc_acc['DA_CHEMIN'])) $path = kDocumentDir;
		else $path = kDocumentDir.str_replace("//", "/", $this->t_doc_acc['DA_CHEMIN']."/");
		
		return $path.$this->t_doc_acc['DA_FICHIER'];
	}
	
	function getFileUrl($relative_to_media = false)
	{
		if (strpos($this->t_doc_acc['DA_FICHIER'],'http://')===0) return $this->t_doc_acc['DA_FICHIER'];
		
		if($relative_to_media){
			if (empty($this->t_doc_acc['DA_CHEMIN'])) $path = str_replace(kCheminHttpMedia,'',kDocumentUrl);
			else $path = str_replace(kCheminHttpMedia,'',kDocumentUrl).str_replace("//", "/", $this->t_doc_acc['DA_CHEMIN']."/");
		}else{
			if (empty($this->t_doc_acc['DA_CHEMIN'])) $path = kDocumentUrl;
			else $path = kDocumentUrl.str_replace("//", "/", $this->t_doc_acc['DA_CHEMIN']."/");
		}
		return $path.$this->t_doc_acc['DA_FICHIER'];
	}

	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 *
	 */
 	function updateFromArray ($tab_valeurs) {
	 	if (empty($this->t_doc_acc)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_doc_acc[strtoupper($fld)])) $this->t_doc_acc[strtoupper($fld)]=$val;
		}
	}


	/**
	 * Crée un doc acc en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj et objet màj avec l'ID doc acc créé
	 */
	function create ($checkDoublon=true,$genereFolder=true) {
		global $db;
		if (empty($this->t_doc_acc['DA_FICHIER'])) {$this->dropError(kErrorDocAccSourceVide);return false;}
		$existingId=$this->checkExist();
		if ($checkDoublon && $existingId) {
			$this->dropError(kErrorDocAccExisteDeja);$this->t_doc_acc['ID_DOC_ACC']=$existingId;$this->save();return false;
		}

		//Si le document d'accompagnement existe déjà et a un sous-dossier on garde le même
		if($existingId && empty($this->t_doc_acc['DA_CHEMIN'])) {
			$oDocAcc = new DocAcc();
			$oDocAcc->t_doc_acc["ID_DOC_ACC"] = $existingId;
			$oDocAcc->getDocAcc();
			$this->t_doc_acc['DA_CHEMIN'] = $oDocAcc->t_doc_acc['DA_CHEMIN'];
		}
		
		// Appel matinfo pour DA_TAILLE et DA_DATE_FICHIER
		if(is_file($this->getFilePath())){
			require_once(libDir."class_matInfo.php");
			$this->t_doc_acc['DA_TAILLE'] = getFileSize($this->getFilePath());
			$this->t_doc_acc['DA_DATE_FICHIER'] = MatInfo::getDateTimeFromFile($this->getFilePath());
		}
		// j'enlève le test "isset &&" empty car on unset pas ID_DOC_ACC == NULL sinon, ce qui résulte en une insertion en base avec ID_DOC_ACC = 0 et génère donc des erreurs. 
		if (empty($this->t_doc_acc["ID_DOC_ACC"])) unset($this->t_doc_acc["ID_DOC_ACC"]) ;
		$ok = $db->insertBase("t_doc_acc","id_doc_acc",$this->t_doc_acc);
		if (!$ok) {$this->dropError(kErrorDocAccCreation);trace($sql);return false;}
		$this->t_doc_acc['ID_DOC_ACC']=$ok;

		//PC 27/12/12 : Déplacement du document d'accompagnement dans un dossier
		if ($genereFolder && !empty($this->t_doc_acc['ID_DOC_ACC']) && is_file(kDocumentDir.$this->t_doc_acc['DA_FICHIER'])) {
			if(empty($this->t_doc_acc['DA_CHEMIN'])) {
				$folder = $this->getFolderName($this->t_doc_acc['ID_DOC_ACC']);
			} else {
				$folder = $this->t_doc_acc['DA_CHEMIN'];
			}
			
			if (rename(kDocumentDir.$this->t_doc_acc['DA_FICHIER'], kDocumentDir.$folder.$this->t_doc_acc['DA_FICHIER'])) {
				$db->execute("UPDATE t_doc_acc SET DA_CHEMIN = '$folder' WHERE DA_FICHIER = ".$db->Quote($this->t_doc_acc['DA_FICHIER']));
				$this->t_doc_acc['DA_CHEMIN'] = $folder;
			}
		}

		return true;
	}
	
	public static function getFolderName($id) {
		$num = "".floor($id / 1000);
		while (strlen($num) < 4) $num = "0".$num;
		if (!is_dir(kDocumentDir.$num)) mkdir(kDocumentDir.$num);
		return $num."/";
	}


	/**
	 * Sauve en base un doc acc (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save () {
		global $db;

		//if (empty($this->t_doc_acc['ID_DOC_ACC'])) {return 'crea';} //WARNING : j'ai retiré la redirection vers la créa car ça faisait des doublons !
		//if ($this->checkExist()) {$this->dropError(kErrorDocAccExisteDeja);return false;}

		$rs = $db->Execute("SELECT * FROM t_doc_acc WHERE ID_DOC_ACC=".intval($this->t_doc_acc['ID_DOC_ACC'])." AND ID_LANG=".$db->Quote($this->t_doc_acc['ID_LANG']));
		$sql = $db->GetUpdateSQL($rs, $this->t_doc_acc);
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok) {$this->dropError(kErrorDocAccSauve);trace($sql);return false;}
		/*
		if ($this->origFile!=$this->t_doc_acc['DA_FICHIER'] && !empty($this->origFile)) {
			$ok=rename(kDocumentDir.$this->origFile,kDocumentDir.$this->t_doc_acc['DA_FICHIER']);
			if (!$ok) {$this->dropError(kErrorDocAccRenommageFichier);return false;}
		}
		*/
		return true;
	}


	/**
	 * Sauve les informations d'un lien EVT / VALEUR
	 * IN : tableau EVT_VAL
	 * OUT : base mise à jour
	 */
	function saveDocAccVal() {
		global $db;

		$db->Execute('DELETE FROM t_doc_acc_val WHERE ID_DOC_ACC='.intval($this->t_doc_acc['ID_DOC_ACC']));
		if (!$this->t_doc_acc_val) return false;

		$db->StartTrans();
		foreach ($this->t_doc_acc_val as $idx=>$docVal) {

			//Précaution en cas de duplication : on spécifie bien que l'ID_DOC est celui en cours.
			// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_VAL originel dedans.
			$docVal['ID_DOC_ACC']=$this->t_doc_acc['ID_DOC_ACC'];
			if (isset($docVal['VAL'])) unset ($docVal['VAL']); // on retire l'objet encapsulé

			$ok = $db->insertBase("t_doc_acc_val","id_doc_acc",$docVal);
			if (!$ok) trace($db->ErrorMsg());
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocAccVal);trace($sql); return false;} else return true;
	}


	/** Sauve les différentes versions
	 *  IN : tableau des versions
	 * 	OUT : màj base
	 */
	function saveVersions($tabVersions='') {
		global $db;
		foreach ($tabVersions as $lang=>$valeur) {
			if (!empty($valeur['DA_TITRE'])) {
			$version= new DocAcc;
			$version->t_doc_acc= $this->t_doc_acc;
			$version->t_doc_acc['DA_TITRE']=$valeur['DA_TITRE'];
			$version->t_doc_acc['DA_FICHIER']=$valeur['DA_FICHIER'];
			$version->t_doc_acc['DA_CHEMIN']=$valeur['DA_CHEMIN'];
            $version->t_doc_acc['DA_TEXTE']=$valeur['DA_TEXTE'];
			$version->t_doc_acc['ID_LANG']=$lang;

			if ($version->checkExistId()) $ok=$version->save(); else $ok=$version->create();

			if ($ok==false && $version->error_msg) {$this->dropError(kErrorDocAccVersionSauve.' : '.$version->error_msg);return false;}
			unset($version);
			}
		}
		return true;
	}


	/** Récupération des paramètres de classe
	 * pour l'upload Flash **/
    static function getUploadParams() {
		// VP 14/01/10 : recup paramètre maxTotalSize de php.ini
    	$arrParams["strFilesFormat"]=".*";
		if($_GET['nb'])
			$arrParams["maxFileCount"]=$_GET['nb'];
		else
			$arrParams["maxFileCount"]='10';
		$arrParams["maxTotalSize"]=ini_get("upload_max_filesize"); // récup du param PHP.ini
		$arrParams["maxFileSize"]=ini_get("upload_max_filesize"); // récup du param PHP.ini
 		$arrParams["destinationDir"]=kDocumentDir;

 		$colors["backgroundColor"]='#c1f8ff';
		$colors["buttonBackgroundColor"]='#9ac0c4';
		$colors["listBackgroundColor"]='#c4fbff';
		$caption["labelText"]='Select&nbsp;material&nbsp;files';

		return array("arrParams"=>$arrParams,"colors"=>$colors,"caption"=>$caption);   // le paramétrage par déft convient très bien, on retourne false

    }



    //*** Upload d'un fichier $input_file envoy� par l'utilisateur
    // NOTE LD : obsolète avec l'upload Flash mais gardé qd même !
    function upload_file($tab_files,$input_file){ // $tab_files correspond au $_FILES du formulaire

        if (isset($tab_files[$input_file])){
            if ($tab_files[$input_file]['name'])
            {
                // I. Initialisation de l'upload
                $new_upload = new Upload($tab_files);
                $new_upload ->permission = unserialize(gListeTypesDocAcc);

                // II. Sauvegarde du fichier (On utilise la classe UPLOAD pour g�rer le fichier)
                if ($new_upload->save (kDocumentDir,$input_file,0)){
                //if ($new_upload->save ($filename,kDocumentDir,$input_file,1)){
                }else{
                    $this->dropError("<span class=\"errormsg\">".kErreurUpload."<br/>".$new_upload->errors."</span>");
                }

                // III. Mise � jour de l'objet
                //$this->da_fichier = $tab_files[$input_file]['name'];
                $this->da_fichier = $new_upload ->file_name;
            }else{
                $this->dropError("<span class=\"errormsg\">".kErreurChampsOblig."</span>");
            }
        }else{
            $this->dropError("<span class=\"errormsg\">".kErreurChampsOblig."</span>");
        }
    }


    //*** Cr�ation du repertoire physique des docs
    function cree_repertoire($dir){


        // On cr�e le repertoire
        if (!is_dir($dir)){
            if(!mkdir($dir)){
                $this->erreur++;
                return "<span class=\"errormsg\">".kErreurCreerRepertoire." ".$dir."</span>";
            }else
                return;
        }
        else return;
    }


    /** Fonction de transcodage de fichier en PDF
     *  IN :  $self=> transcodage du fichier DA (true) ou création d'un autre (false)
     */
	function transcodeToPdf(&$myTransPDF) {
		
		if (!$this->checkFileExists()) {$this->dropError(kUploadTranscodageErreurNoSource);return false;}
		
		$fileSrc=$this->getFilePath();
		$ext="pdf";
		if(getExtension($fileSrc)!=$ext){
			$filePdf=kDocumentDir.stripExtension(basename($fileSrc)).".".$ext;
			$ok = $this->convertToPdf($fileSrc,$filePdf);
			
			if ($ok) {  //Création du nouveau doc_acc
				$myTransPDF=new DocAcc;
				$myTransPDF->t_doc_acc=$this->t_doc_acc; //copie des infos
				$myTransPDF->t_doc_acc['ID_DOC_ACC']= NULL;
				$myTransPDF->t_doc_acc['DA_FICHIER']=basename($filePdf);
				$myTransPDF->t_doc_acc['DA_TITRE']=basename($filePdf);
				foreach ($_SESSION['arrLangues'] as $lg) {
					$myTransPDF->t_doc_acc['ID_LANG']=$lg;
					$ok=$myTransPDF->create(false);
				}
				$this->dropError(kUploadTranscodageBilan." ".$filename.".".$ext." : ".str_replace('<br/>','',$myTransPDF->error_msg)); //Récupération des msg de sauvegarde du mat transcodé
                // Extraction texte
				$myTransPDF->extractText($myTransPDF->getFilePath());
			}
		}
		
		return $ok;
	}
	
    /** Fonction de conversion texte
     *  IN : arrParams => tableau des params du fichier à générer, $self=> transcodage du fichier matériel (true) ou création d'un autre (false)
     */
	function convertToPdf($fileSrc,$fileOut) {
		global $db;
		if(!file_exists($fileSrc)) {
			$this->dropError(kUploadTranscodageErreurNoSource);
			return false;
		}
	 	if (!defined("kBinUnoconv")) {$this->dropError(kErrorConvertPdfNoTool);return false; }
		
		$ok = true;
		$cmd = kBinUnoconv." -f pdf -o \"".$fileOut."\" \"".$fileSrc."\"";
		trace($cmd);
		exec($cmd);
		$ok = is_file($fileout);
		if (!$ok) {$this->dropError(kErreurConversionPdf);return false;}
		return $ok;
	}
	
    /** Fonction de conversion texte
     *  IN : arrParams => tableau des params du fichier à générer, $self=> transcodage du fichier matériel (true) ou création d'un autre (false)
     */
	function extractText($fileSrc) {
		global $db;
		if(!file_exists($fileSrc)) {
			$this->dropError(kUploadTranscodageErreurNoSource);
			return false;
		}
	 	if (!defined("kBinConvertText")) {$this->dropError(kErrorConvertTextNoTool);return false; }
		
		$ok = true;
		$filenameTxt = stripExtension($fileSrc).".txt";
		//$cmd = "/usr/local/bin/pdftotext -enc UTF-8 ".$fileSrc." ".$filenameTxt;
		$cmd = kBinConvertText." ".$fileSrc." ".$filenameTxt;
		trace($cmd);
		exec($cmd);
		$contenu = stripControlCharacters(file_get_contents($filenameTxt));
		unlink($filenameTxt);
		
		if(empty($contenu)) {
			$this->dropError(kErreurConversionTexte);
			trace(kErreurConversionTexte." (".$fileSrc.")");
			$ok = false;
		} else {
			$this->dropError(kSuccesRecuperationTexte);
			//echo $contenu;
			$sql="UPDATE t_doc_acc SET DA_TEXTE=".$db->Quote($contenu)." WHERE ID_DOC_ACC=".intval($this->t_doc_acc['ID_DOC_ACC']);
			$db->Execute($sql);
		}
		
		return $ok;
	}
	


    //*** Insertion de l'objet ($tab_lang correspond � l'ensemble des langues du syst�me)
    /*
    function saveDocAcc($tab_lang){
        global $db;

        // I. UPDATE
        if ($this->id_doc_acc!=NULL){
            // I.A. On v�rifie qu'un doc_acc ne porte pas ce titre
            $existDeja=$db->Execute("SELECT DA_FICHIER FROM t_doc_acc WHERE DA_FICHIER=".$db->Quote($this->da_fichier)." AND ID_DOC_ACC!=".$db->Quote($this->id_doc_acc));
            if ($existDeja->RecordCount()==0){

                // I.A.1. Mise � jour des champs g�n�riques
                $db->Execute("UPDATE t_doc_acc SET ID_DOC='".$this->id_doc."', DA_FICHIER=".$db->Quote($this->da_fichier)." WHERE ID_DOC_ACC='".$this->id_doc_acc."'");

                // I.A.2. Mise � jour des champs d�pendants de la langue
                foreach($this->tab_lang_champs as $indice => $value){
                    $sql = "UPDATE t_doc_acc SET DA_TITRE =".$db->Quote($this->tab_lang_champs[$indice])." ";
                    $sql.= " WHERE ID_DOC_ACC=".$db->Quote($this->id_doc_acc)." AND ID_LANG='".$indice."'";
                    $db->Execute($sql);
                }

                return TRUE;
            }else{
                $this->erreur++;
                return FALSE;
            }
        }
        // II. INSERT
        else {
            // II.A. On v�rifie qu'un id_doc_acc ne porte pas ce nom
            $existDeja=$db->Execute("SELECT DA_FICHIER FROM t_doc_acc WHERE DA_FICHIER=".$db->Quote($this->da_fichier));
            if ($existDeja->RecordCount()==0){
                // II.A.1. Insertion des champs g�n�riques
                foreach($tab_lang as $indice){
                    $sql = "INSERT INTO t_doc_acc (ID_DOC_ACC, ID_LANG, ID_DOC, ID_PERS, DA_FICHIER) ";
                    $sql.= " VALUES (".$db->Quote($this->id_doc_acc).", ".$db->Quote(strtoupper($indice)).", ".$db->Quote($this->id_doc).", ".$db->Quote($this->id_pers).", ".$db->Quote($this->da_fichier).")";
                    $db->Execute($sql);
                    // I.A.1.a. Si c'est la premi�re fois qu'on fait l'insert du document d'accompagnement, on recup�re son ID pour que les prochains aient le m�me ID.
                    if($this->id_doc_acc==NULL){
                        $this->id_doc_acc = $db->Insert_ID("t_doc_acc");
                    }
                }

                // II.A.2. Mise � jour des champs d�pendants de la langue
                foreach($this->tab_lang_champs as $indice => $value){
                    $sql = "UPDATE t_doc_acc SET DA_TITRE =".$db->Quote($value)." ";
                    $sql.= " WHERE ID_DOC_ACC=".$db->Quote($this->id_doc_acc)." AND ID_LANG=".$db->Quote(strtoupper($indice));
                    $db->Execute($sql);
                }

                // On cr�e le repertoire des documents
                $this->cree_repertoire(kDocumentDir);

                $this->id_doc_acc=$db->Insert_ID("t_doc_acc");
                return TRUE;
            }else{
                $this->erreur++;
                return FALSE;
            }
        }
    }
    */


    //*** Suppression
    function delete($supprimerFichier=0){
        global $db;

        if (empty($this->t_doc_acc['ID_DOC_ACC']) && $this->t_doc_acc['ID_DOC_ACC'] != "0") {$this->dropError(kErrorDocAccNoId);return false;}

        // VP 4/07/13 : Suppression de toutes les versions à la fois
        if(defined("gDocAccDeleteAllVersions") && gDocAccDeleteAllVersions==true){
            unset($this->t_doc_acc['ID_LANG']);
        }

        if ($supprimerFichier || gDocAccDeleteFiles==true) { // Vérification de l'existence de liens : autre versions, doc, personne
            $sql="SELECT DISTINCT ID_PERS,ID_DOC FROM t_doc_acc WHERE
						ID_DOC_ACC=".intval($this->t_doc_acc['ID_DOC_ACC']);
			if (!empty($this->t_doc_acc['ID_DOC'])) $sql.=" AND ID_DOC<>".intval($this->t_doc_acc['ID_DOC']);
			if (!empty($this->t_doc_acc['ID_PERS'])) $sql.=" AND ID_PERS<>".intval($this->t_doc_acc['ID_PERS']);
			if (!empty($this->t_doc_acc['ID_FEST'])) $sql.=" AND ID_FEST<>".intval($this->t_doc_acc['ID_FEST']);
			if (!empty($this->t_doc_acc['ID_PANIER'])) $sql.=" AND ID_PANIER<>".intval($this->t_doc_acc['ID_PANIER']);

			if (!empty($this->t_doc_acc['ID_LANG'])) $sql.=" AND ID_LANG<>".$db->Quote($this->t_doc_acc['ID_LANG']);

			$result=$db->GetAll($sql);
			if (count($result)!=0) {$this->dropError(kWarningDocAccFichierUtilise);}
       		 else {
                // rien d'autre n'est plus lié à ce fichier : ni autre version, ni autre doc, ni autre personne
                if(is_file($this->getFilePath())){
                    unlink($this->getFilePath());
          			}
          		// le document est "orphelin", on en profite pour supprimer les liens aux valeurs
          		deleteSQLFromArrayOfCrit("t_doc_acc_val",array("ID_DOC_ACC"=>$this->t_doc_acc['ID_DOC_ACC']));
       		 }
       	}

       	$arrCrit["ID_DOC_ACC"]=$this->t_doc_acc['ID_DOC_ACC'];
       	if ($this->t_doc_acc['ID_LANG']) $arrCrit["ID_LANG"]=$this->t_doc_acc['ID_LANG'];  //si on delete juste une version
    	deleteSQLFromArrayOfCrit("t_doc_acc",$arrCrit); //suppression en base du document
    }


	/** Effectue le mappage entre un tableau et des champs de doc_acc
	 *
	 */
	// VP 15/02/10 : création fonction mapFields
	function mapFields($arr) {

		require_once(modelDir."model_val.php");

		foreach ($arr as $idx=>$val) {

			$myField=$idx;
			// analyse du champ
			if ($myField=='ID_DOC_ACC') $this->t_doc_acc['ID_DOC_ACC']=$val;
			if ($myField=='ID_LANG') $this->t_doc_acc['ID_LANG']=$val;
			if ($myField=='ID_DOC') $this->t_doc_acc['ID_DOC']=$val;
			if ($myField=='ID_CAT') $this->t_doc_acc['ID_CAT']=$val;
			if ($myField=='ID_PERS') $this->t_doc_acc['ID_PERS']=$val;
			if ($myField=='ID_FEST') $this->t_doc_acc['ID_FEST']=$val;
			if ($myField=='ID_PANIER') $this->t_doc_acc['ID_PANIER']=$val;
			$arrCrit=split("_",$myField);
			switch ($arrCrit[0]) {
				case 'DA': //champ table DOC_ACC
					$val=trim($val);
					$flds=split(",",$myField);
					if (!empty($val)) foreach ($flds as $_fld) $this->t_doc_acc[$_fld]=$val;
					break;

				case 'V': //valeur
					if (empty($val)) break;
					$myVal=new Valeur();
					$myVal->t_val['VALEUR']=$val;
					$myVal->t_val['VAL_ID_TYPE_VAL']=$arrCrit[1];
					$myVal->t_val['ID_LANG']=$_SESSION['langue'];
					$_val=$myVal->checkExist();
					if ($_val) {
						$myVal->t_val['ID_VAL']=$_val;
						$myVal->save(); }
					else {
						$myVal->create(); //création dans la langue
						foreach ($_SESSION['arrLangues'] as $lg) {
							if ($lg!=$_SESSION['langue']) $arrVersions[$lg]=$val;
						}
						$myVal->saveVersions($arrVersions); //sauve dans autres langues
						unset($arrVersions);
					}
					$this->t_doc_acc_val[]=array('ID_TYPE_VAL'=>$arrCrit[1],'ID_VAL'=>$myVal->t_val['ID_VAL']);
					break;

                case 'VI': //id valeur
                    if (!is_array($val)) {$_x[0]['ID_VAL']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
                    
                    foreach($val as $thisVal) {
                        $myVal=new Valeur();
                        //NOTE : pour le ID_TYPE_VAL, on peut forcer le type via arrCrit pour ne pas avoir le même que VAL_ID_TYPE_VAL
                        //Cette différence entre id_type_val et val_id_type_val existe chez qq sites (CDR)
                        $this->t_doc_acc_val[]=array('ID_TYPE_VAL'=>$arrCrit[1]!=''?$arrCrit[1]:$myVal->t_val['VAL_ID_TYPE_VAL'],'ID_VAL'=>$thisVal['ID_VAL']);
                    }
					break;

			}
		}
	}


    //*** XML
	function xml_export($entete="",$encodage="",$indent="") {

        $search = array(" ' ",'&','<','>');
        $replace = array("'",'&#38;','&#60;','&#62;');
		$content='';

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_doc_acc>\n";
		// PLACE
		foreach ($this->t_doc_acc as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}

        if (!empty($this->t_doc_acc_val)) {
        // I.3. Propri�t�s relatives � t_doc_val
        $content.=$indent."\t<t_doc_acc_val nb=\"".count($this->t_doc_acc_val)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_doc_acc_val as $name => $value) {
//				$content .=$indent. "\t\t<ID_TYPE_VAL>".$value['ID_TYPE_VAL']."</ID_TYPE_VAL>";
//                $content .=$indent. "\t\t<ID_VAL>".$value['ID_VAL']."</ID_VAL>\n";
//                $content .=$indent. "\t\t<ID_DOC_ACC>".$value['ID_DOC_ACC']."</ID_DOC_ACC>\n";
//                $content .=$indent. "\t\t<t_val>".$value['VAL']->xml_export(0,0,$indent.chr(9).chr(9))."</t_val>\n";
				// VP 15/02/10 : modification export t_doc_acc
				$content .=$indent."\t\t<TYPE_VAL ID_TYPE_VAL=\"".$value['ID_TYPE_VAL']."\" >";
                $content .=$indent. "\t\t".$value['VAL']->xml_export(0,0,$indent.chr(9).chr(9))."\n";
				$content .=$indent."</TYPE_VAL>\n";
            }
        $content.=$indent."\t</t_doc_acc_val>\n";
        }

		$content.=$indent."</t_doc_acc>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
}

?>
