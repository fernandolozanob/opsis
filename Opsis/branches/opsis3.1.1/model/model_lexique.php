<?php
require_once(modelDir.'model.php');
class Lexique extends Model {

var $t_lexique;

var $type_lex; // Type lexique, en toutes lettres

var $arrFather; // tableau ID  père (unique)
var $arrChildren; // tableau ID enfants
var $arrSyno; //tableau ID synonymes
var $arrSynoPref; // synonymes pref
var $arrAsso; // tableau des termes associés
var $arrDoc; // tableau ID docs
var $arrDocLex; // tableau docs
var $arrVersions; //tableau des versions
var $error_msg;
var $uniqueFields;

private $arrTextFields=array("lex_terme"); // liste des champs de type texte dont il faut traiter les apostrophes


    //*** Constructeur
    function __construct($id=null,$version=""){
		parent::__construct('t_lexique',$id,$version);

		if (defined("gLexUniqueFields")) $this->uniqueFields=(array)unserialize(gLexUniqueFields);
		else $this->uniqueFields=array("LEX_ID_TYPE_LEX", "LEX_TERME");
	}


	/**
	 * Récupère les infos sur un ou plusieurs lexique.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets lexique, sinon màj de l'objet en cours
	 */
	 
	function getData($arrData=array(),$version="") {
		global $db;
		$arrLexs=parent::getData($arrData,$version); 
		
		if (empty($arrLexs) && !empty($this->t_lexique) && !$arrData) { 
			$this->type_lex=GetRefValue("t_type_lex",$this->t_lexique["LEX_ID_TYPE_LEX"],$this->t_lexique['ID_LANG']);
			if (!empty($this->t_lexique['LEX_ID_GEN'])) {
					$this->arrFather=$db->GetAll("SELECT * FROM t_lexique WHERE ID_LEX=".intval($this->t_lexique['LEX_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']));
			}
		}else{
			foreach ($arrLexs as $idx=>$Lexique){ // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrLexs[$idx]->type_lex=GetRefValue("t_type_lex",$arrLexs[$idx]->t_lexique["LEX_ID_TYPE_LEX"],$arrLexs[$idx]->t_lexique['ID_LANG']);
			}
			unset($this); // plus besoin
			return $arrLexs;
		}
	}
	
	function getLexique($arrLex=array(),$version=""){
		return $this->getData($arrLex,$version);
	}
	
	function getSyno() {
		global $db;

		$sql="SELECT * FROM t_lexique WHERE LEX_ID_SYN=".intval($this->t_lexique['ID_LEX'])." AND ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']);
		$this->arrSyno=$db->GetAll($sql);

	}

	function getSynoPref() {
		global $db;

		$sql="SELECT * FROM t_lexique WHERE ID_LEX=".intval($this->t_lexique['LEX_ID_SYN'])." AND ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']);
		$this->arrSynoPref=$db->GetAll($sql);

	}

	/**
	 * Récupération des termes associés (jointure lexique_asso & lexique).
	 * IN : var classe t_lexique
	 * OUT : tab class arrAsso
	 * NOTE : comme relation bijective, on autoexclue le lexique en cours
	 */
	function getAsso() {
		global $db;
		$sql="SELECT distinct l.* FROM t_lexique l
				INNER JOIN t_lexique_asso la ON la.id_lex_asso=l.id_lex
				WHERE l.ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']);
		$sql.=" AND la.ID_LEX=".intval($this->t_lexique['ID_LEX']);
		$this->arrAsso=$db->GetAll($sql);
	}


	/** Export XML de l'objet LEX
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_lexique>\n";
		// PLACE
		foreach ($this->t_lexique as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}
		$content.=$indent."\t<TYPE_LEX>".GetRefValue("t_type_lex",$this->t_lexique["LEX_ID_TYPE_LEX"],$_SESSION['langue'])."</TYPE_LEX>\n";

		if (!empty($this->arrFather)) {
			$content.=$indent."\t<t_lexique_father count='".count($this->arrFather)."'>";
			foreach ($this->arrFather as $idx=>$_lex) {
				$content.=$indent."\t\t<t_lexique>\n";
				foreach ($_lex as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_lexique>\n";
			}
			$content.=$indent."\t</t_lexique_father>";
		}

		if (!empty($this->arrChildren)) {
			$content.=$indent."\t<t_lexique_children count='".count($this->arrChildren)."'>";
			foreach ($this->arrChildren as $idx=>$_lex) {
				$content.=$indent."\t\t<t_lexique>\n";
				foreach ($_lex as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_lexique>\n";
			}
			$content.=$indent."\t</t_lexique_children>";
		}

		if (!empty($this->arrSyno)) {
			$content.=$indent."\t<t_lexique_syno count='".count($this->arrSyno)."'>";
			foreach ($this->arrSyno as $idx=>$_lex) {
				$content.=$indent."\t\t<t_lexique>\n";
				foreach ($_lex as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_lexique>\n";
			}
			$content.=$indent."\t</t_lexique_syno>";
		}

		if (!empty($this->arrSynoPref)) {
			$content.=$indent."\t<t_lexique_syno_pref count='".count($this->arrSynoPref)."'>";
			foreach ($this->arrSynoPref as $idx=>$_lex) {
				$content.=$indent."\t\t<t_lexique>\n";
				foreach ($_lex as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_lexique>\n";
			}
			$content.=$indent."\t</t_lexique_syno_pref>";
		}

		if (!empty($this->arrAsso)) {
			$content.=$indent."\t<t_lexique_asso count='".count($this->arrAsso)."'>";
			foreach ($this->arrAsso as $idx=>$_lex) {
				$content.=$indent."\t\t<t_lexique>\n";
				foreach ($_lex as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_lexique>\n";
			}
			$content.=$indent."\t</t_lexique_asso>";
		}

		if (!empty($this->arrVersions)) {
			$content.=$indent."\t<t_version count='".count($this->arrVersions)."'>";
			foreach ($this->arrVersions as $idx=>$_lex) {
				$content.=$indent."\t\t<t_lexique>\n";
				foreach ($_lex as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_lexique>\n";
			}
			$content.=$indent."\t</t_version>";
		}


		$content.=$indent."</t_lexique>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
	
	/** Export XML optimisé 
	 *  IN : requete SQL, page, nombre de documents, modes d'export, valeurs à rechercher, valeurs de remplacement
	 *  OUT : chemin fichier XML au format UTF8.
	 */
	// function getXMLList($sql,$page=1,$nb="all",$mode=array(),$search_sup=array(),$replace_sup=array()){
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=array(),$search_sup=array(),$replace_sup=array()){
		global $db;

		if($mode == null){
			$mode=array();
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		if(strpos(strtolower($critere),'order by') === false){
			$critere.=" order by id_lex asc ";
		}
        
        if($mode['hier']==1){
            $critere=substr ( $critere, 0, strpos(strtolower($critere),'order by') )." order by concat(L.lex_path, L.lex_terme) asc ";
        }

		$limit="";
		if ($nb!="all"){
			$limit = $db->limit($nb_offset,$nb);
		}
	
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		$xml_file_path = kServerTempDir."/export_opsis_thesaurus_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
		
		$sql_lex = "SELECT L.* , TL.TYPE_LEX, T2.LEX_TERME as LEX_FATHER, T3.LEX_TERME AS SYNO_PREF  
		FROM t_lexique L
		LEFT OUTER JOIN t_type_lex TL ON L.LEX_ID_TYPE_LEX = TL.ID_TYPE_LEX AND L.ID_LANG=TL.ID_LANG 
		LEFT OUTER JOIN t_lexique T2 ON L.ID_LANG=T2.ID_LANG AND L.LEX_ID_GEN=T2.ID_LEX 
		LEFT OUTER JOIN t_lexique T3 ON L.ID_LANG=T3.ID_LANG AND L.LEX_ID_SYN=T3.ID_LEX ".$critere.$limit;
	    
		$results_lex = $db->Execute($sql_lex);
		$ids = implode(',',$db->GetCol($sql_lex));
		if($ids == ''){
			return false;
		}
		
		
		
		$sql_asso="SELECT distinct l.*, la.ID_LEX AS ID_LEX_ORI FROM t_lexique l
					INNER JOIN t_lexique_asso la ON la.id_lex_asso=l.id_lex
					WHERE l.ID_LANG=".$db->Quote($_SESSION['langue'])." AND la.ID_LEX in ($ids) ORDER BY la.ID_LEX ASC";
		$results_asso = $db->Execute($sql_asso);	 
		unset($sql_asso);
		
		$sql_children="SELECT * from t_lexique WHERE LEX_ID_GEN in ($ids)  AND ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY LEX_ID_GEN ASC";
		$results_children = $db->Execute($sql_children);	 
		unset($sql_children);
		
		$sql_syno="SELECT * FROM t_lexique WHERE LEX_ID_SYN in ($ids) AND ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY LEX_ID_SYN ASC";
		$results_syno = $db->Execute($sql_syno);	 
		unset($sql_syno);

		$sql_versions="SELECT t_lang.ID_LANG, LEX_TERME, LEX_NOTE, LANG, ID_LEX FROM t_lexique
				right join t_lang on (t_lang.ID_LANG=t_lexique.ID_LANG)
				WHERE
				t_lexique.ID_LANG<>".$db->Quote($_SESSION['langue'])." AND ID_LEX in ($ids) ORDER BY ID_LEX ASC";
		$results_versions = $db->Execute($sql_versions);	 
		unset($sql_versions);
		
		while($asso = $results_asso->FetchRow()){
			$results_asso_ref[$asso['ID_LEX_ORI']][]=$results_asso->CurrentRow()-1;
		}

		while($child = $results_children->FetchRow()){
			$results_children_ref[$child['LEX_ID_GEN']][]=$results_children->CurrentRow()-1;
		}

		while($syno = $results_syno->FetchRow()){
			$results_syno_ref[$syno['LEX_ID_SYN']][]=$results_syno->CurrentRow()-1;
		}
		while($version = $results_versions->FetchRow()){
			$results_versions_ref[$version['LEX_ID_SYN']][]=$results_versions->CurrentRow()-1;
		}

		foreach($results_lex as $lex){
			$xml .= "\t<t_lexique>\n";
			foreach($lex as $fld=>$val){
				$xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
			}
			
			
			//==========GESTION LEX CHILDREN==========
			if(isset($results_children_ref[$lex['ID_LEX']][0])){
				$results_children->Move($results_children_ref[$lex['ID_LEX']][0]);
				$array_children = $results_children->GetRows(count($results_children_ref[$lex['ID_LEX']]));
			}else{
				$array_children=array();
			}

			if(count($array_children)>0){
				$xml.="\t\t<t_lexique_children nb=\"".count($array_children)."\" >\n";

				foreach($array_children as $child){
					$xml.="\t\t\t<t_lexique >\n";
					foreach($child as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_lexique>\n";
				}
				$xml.="\t\t</t_lexique_children>\n";
			}
			
			//==========GESTION LEX SYNO==========
			if(isset($results_syno_ref[$lex['ID_LEX']][0])){
				$results_syno->Move($results_syno_ref[$lex['ID_LEX']][0]);
				$array_syno = $results_syno->GetRows(count($results_syno_ref[$lex['ID_LEX']]));
			}else{
				$array_syno=array();
			}
			if(count($array_syno)>0){
				$xml.="\t\t<t_lexique_syno nb=\"".count($array_syno)."\" >\n";

				foreach($array_syno as $syno){
					$xml.="\t\t\t<t_lexique >\n";
					foreach($syno as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_lexique>\n";
				}
				$xml.="\t\t</t_lexique_syno>\n";
			}
			
			//==========GESTION LEX ASSO==========
			if(isset($results_asso_ref[$lex['ID_LEX']][0])){
				$results_asso->Move($results_asso_ref[$lex['ID_LEX']][0]);
				$array_asso = $results_asso->GetRows(count($results_asso_ref[$lex['ID_LEX']]));
			}else{
				$array_asso=array();
			}

			if(count($array_asso)>0){
				$xml.="\t\t<t_lexique_asso nb=\"".count($array_asso)."\" >\n";

				foreach($array_asso as $asso){
					$xml.="\t\t\t<t_lexique >\n";
					foreach($asso as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_lexique>\n";
				}
				$xml.="\t\t</t_lexique_asso>\n";
			}
			
			//==========GESTION LEX VERSION==========
			if(isset($results_versions_ref[$lex['ID_LEX']][0])){
				$results_versions->Move($results_versions_ref[$lex['ID_LEX']][0]);
				$array_version = $results_versions->GetRows(count($results_versions_ref[$lex['ID_LEX']]));
			}else{
				$array_version=array();
			}

			if(count($array_version)>0){
				$xml.="\t\t<t_version nb=\"".count($array_version)."\" >\n";

				foreach($array_version as $version){
					$xml.="\t\t\t<t_lexique >\n";
					foreach($version as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_lexique>\n";
				}
				$xml.="\t\t</t_version>\n";
			}
			
			
			$xml .= "\t</t_lexique>\n";
			fwrite($xml_file,$xml);
			$xml = "";
			
		}
		
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
		flock($xml_file, LOCK_UN);
		fclose($xml_file);
	
		return $xml_file_path;
	}

	
	/** Vérifie si un Lexique existe déjà en base
	 * 	Critères d'unicité sur le nom & le type (car un même nom peut désigner un état, lga ou autre)
	 *  IN : objet lexique, exact=true/false (recherche par = ou par Like)
	 *  OUT : TRUE si existe, FALSE si libre
	 */
	 //@update VG 07/07/2010 : ajout du param checkOnId, qui indique si on vérifie en prenant en compte l'ID_LEX
	function checkExist($exact=true,$fusion=false, $checkOnId = true) {
		global $db;
		//LD 10 06 09 : suppression du LOWER / strtolower qui fait sauter des accents
		$sql="SELECT ID_LEX FROM t_lexique WHERE ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']);
		// VP 9/11/2017 : utilsation de $uniqueFields
		foreach ($this->uniqueFields as $fld) {
			if($fld == "LEX_ID_GEN" || $fld == "LEX_ID_FONDS"){
				$sql.=" AND ".strtoupper($fld)."=".intval($this->t_lexique[strtoupper($fld)]);
			}elseif($fld == "LEX_TERME"){
				// VP 10/01/14 : utilisation de $db->like
				// MS 19/05/14 : utilisation de $db->getNormalizedLike qui permet d'ajouter les unaccent à la requete en version postgres (pour être similaire avec fonctionnement mysql)
				if($exact==true){
					$sql.=" AND (TRIM(LEX_TERME) =".$db->Quote(trim($this->t_lexique['LEX_TERME']))."
					OR TRIM(LEX_TERME) =".$db->Quote(trim(str_replace("'"," ' ",$this->t_lexique['LEX_TERME']))).")";
				}else{
					$sql.=" AND (".$db->getNormalizedLike("TRIM(LEX_TERME)",trim($this->t_lexique['LEX_TERME']))."
					OR ".$db->getNormalizedLike("TRIM(LEX_TERME)",trim(str_replace("'"," ' ",$this->t_lexique['LEX_TERME']))).")";
				}
			}else{
				$sql.=" AND ".strtoupper($fld)."=".$db->Quote($this->t_lexique[strtoupper($fld)]);
			}
			
		}
		if (!empty($this->t_lexique['ID_LEX']) && $checkOnId) $sql.=" AND ID_LEX<>".intval($this->t_lexique['ID_LEX']);
		$rs=$db->GetAll($sql); //note : on utilise GetAll qui discarde le rs

		if ($rs==false) return false; elseif($fusion==false) {
			$this->t_lexique['ID_LEX']=$rs[0]['ID_LEX']; // 21/08/2007 : expérimental, ajouté pour l'import
			//22/12/08 => ld, sauf si fusion (on ne change PAS l'id en cours !)
			return true;
		}
	}


	function checkExistId() {
		global $db;
		$sql="SELECT ID_LEX FROM t_lexique WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']);
		$sql.=" AND ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']);
		return ($db->GetOne($sql));
	}

	/**
	 * Sauve en base un lexique (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save ($fusion=false) {
		global $db;
		if (empty($this->t_lexique['ID_LEX'])) {$this->create($fusion);return 'crea';} //Protection : redirection de sauvegarde
		if ($this->checkExist(false,$fusion)&& !$fusion) {$this->dropError(kErrorLexiqueExisteDeja);return false;}
		$this->t_lexique['LEX_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_lexique['LEX_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		if (empty($this->t_lexique['LEX_TERME'])) return false;
	    //update VG 13/07/11
        if($this->t_lexique['LEX_ID_GEN'] == $this->t_lexique['ID_LEX']) {
        	$this->t_lexique['LEX_ID_GEN'] = '0';
        }
        
		// VP 9/09/2015 : mise à jour lex_path
		// MS 29.01.18 - toujours appelé.
        $this->setLexPath();
		
		//VP 29/10/09 : ajout spaceQuotes
		$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
		$rs = $db->Execute("SELECT * FROM t_lexique WHERE ID_LEX=".intval($this->t_lexique['ID_LEX'])." AND ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']));
		$sql = $db->GetUpdateSQL($rs, $this->t_lexique);
		if (!empty($sql)){
			$ok = $db->Execute($sql);
			if (!$ok) {$this->dropError(kErrorLexiqueSauve);trace($sql);return false;}
		}
		
		//VP 30/6/18 : déplacement appel updateDocChampsXML dans lexSaisie
		//$this->updateDocChampsXML(true);

		return true;
	}


	/**
	 * Crée un lexique en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj et objet màj avec l'ID lex créé
	 */
	function create ($fusion=false) {
		global $db;
		//En cas de fusion, on ne vérifie pas l'existence du terme puisqu'à cette étape, l'ancien terme existe en base
		if (!$fusion) if ($this->checkExist()) {$this->dropError(kErrorLexiqueExisteDeja);return false;}

		//par déft, statut candidat
		if(empty($this->t_lexique['LEX_ID_ETAT_LEX'])) $this->t_lexique['LEX_ID_ETAT_LEX']=1;
		$this->t_lexique['LEX_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_lexique['LEX_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_lexique['LEX_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		//unset($this->t_place['ID_PLACE']); //Pour ne pas faire apparaitre la colonne dans le SQL ET générer un ID
		//VP 12/03/10 : ajout spaceQuotes
		$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
		//update VG : l'identifiant est à la fois l'id ET la langue. ici, pour un même lexique on provoquait le changement d'id sur le changement de langue....
		if (isset($this->t_lexique['ID_LEX']) && (intval($this->t_lexique['ID_LEX']) == 0)){
			unset($this->t_lexique['ID_LEX']);
		}
		$ok = $db->insertBase("t_lexique","id_lex",$this->t_lexique);
		if (!$ok) {$this->dropError(kErrorLexiqueCreation);trace($sql);return false;}
		$this->t_lexique['ID_LEX']=$ok;
		return true;
	}


	/**
	 * @update VG 01/06/2010
	 * @update VG 15/06/2010
	 * @update VG 07/07/2010 : le checkExist se fait maintenant sans vérification sur l'id
	 * @update VG 08/07/2010 : placement du getLexique en tout premier lieu
	 * Créé une nouvelle occurence si la valeur n'existe pas, avec ses versions, et retourne l'idVal . Met à jour si la valeur existe déjà
	 *
	 * ATTENTION ! Cette méthode ne fusionne ni ne doublonne !!!
	 */
	function createFromArray($tab_valeurs, $create = true, $update = true, $saveVersion = true) {
		// VP 11/07/2017 : possibilité de passer un paramètre FULLPATH qui contient la hiérachie du terme
		if(isset($tab_valeurs['FULLPATH']) && !empty($tab_valeurs['FULLPATH'])){
			// Séparateur tab par défaut et / sinon
			$fullpath = $tab_valeurs['FULLPATH'];
			if(strpos($fullpath, chr(9))===false){
				$fullpath=str_replace('/',chr(9),$fullpath);
			}
			$fullpath=trim($fullpath);
			$_chunks=explode(chr(9), $fullpath);
			$tab_valeurs2=$tab_valeurs;
			unset($tab_valeurs2['FULLPATH']);
			$id_lex=intval($tab_valeurs2['LEX_ID_GEN']);
			foreach($_chunks as $_chunk){
				$temp_lex= new Lexique;
				$tab_valeurs2['LEX_TERME']=$_chunk;
				$tab_valeurs2['LEX_ID_GEN']=$id_lex;
				$id_lex = $temp_lex->createFromArray($tab_valeurs2, $update, $create, $saveVersion);
			}
			$this->t_lexique=$temp_lex->t_lexique;
			return $this->t_lexique['ID_LEX'];
		} else {
		
			if(isset($tab_valeurs['ID_LEX']) && !empty($tab_valeurs['ID_LEX'])){
				$this->t_lexique['ID_LEX'] = $tab_valeurs['ID_LEX'];
			}
			if(!empty($this->t_lexique['ID_LEX'])) $this->getLexique();
			$this->updateFromArray($tab_valeurs);

			//Si la valeur existe déjÃ , on retourne juste son id
			$exist = $this->checkExist(false, false, false);
			//Sinon on la créé ou on la sauvegarde
			$existId = $this->checkExistId();
			
			if ($exist || $existId) {
				//$this->t_lexique['ID_LEX'] = $exist;
				if(!$update) return $this->t_lexique['ID_LEX'];
				$ok = $this->save();
			} else {
				if (!$create) return false;
				$ok = $this->create(false);
			}

			if (!$ok) return false;

			if($saveVersion) {
				$aLang = getOtherLanguages($this->t_lexique['ID_LANG']);
				$lexVersions = array();
				foreach ($aLang as $lang) {
					$lexVersions[$lang] = array(
												'LEX_TERME' =>	(!empty($tab_valeurs['LEX_TERME_'.$lang]))?$tab_valeurs['LEX_TERME_'.$lang]:$this->t_lexique['LEX_TERME'],
												'LEX_NOTE'	=>	(!empty($tab_valeurs['LEX_NOTE_'.$lang]))?$tab_valeurs['LEX_NOTE_'.$lang]:$this->t_lexique['LEX_NOTE']
											);
				}
			}
			$this->saveVersions($lexVersions);

			if ($ok) return $this->t_lexique['ID_LEX'];
			else return false;
		}
	}


	/** Sauve les différentes versions
	 *  IN : tableau des versions, MODE = retour de la fonction save appelée avant. Si retour=1=>création
	 * 	OUT : màj base
	 */
	function saveVersions($tabVersions='',$fusion=false) {
		global $db;

		foreach ($tabVersions as $lang=>$valeur) {
			if (!empty($valeur['LEX_TERME'])) {
			$version= new Lexique;
			$version->t_lexique= $this->t_lexique; //recopie de ts les champs par déft
			$version->updateFromArray($valeur);
			$version->t_lexique['ID_LANG']=$lang; //update langue

			if ($version->checkExistId()) $ok=$version->save($fusion); else $ok=$version->create($fusion);

			if ($ok==false && $version->error_msg) {$this->dropError(kErrorLexiqueVersionSauve.' : '.$version->error_msg);return false;}
			unset($version);
			}
		}
		return true;
	}

	/**
	 * Sauve les synonymes du terme lexique
	 * IN : tableau classe arrSyno
	 * OUT : DB màj
	 *
	 */
	function saveSyno( $purge=true, $create = true, $update = false) {
		global $db;
		$rs = $db->Execute("SELECT * FROM t_lexique WHERE LEX_ID_SYN=".intval($this->t_lexique['ID_LEX']));
		$sql = $db->GetUpdateSQL($rs, array("LEX_ID_SYN" => 0));
		if (!empty($sql)) $db->Execute($sql);
		
		//TODO : VG : faire cette purge ( passée en paramètre histoire d'avoir la même signature que saveAsso, et recopiée depuis saveAsso )
		if ($purge) {
// 			$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']));
// 			$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX_ASSO=".intval($this->t_lexique['ID_LEX']));
		}
		foreach ($this->arrSyno as $idx=>$syno) { // Puis on rattache.
			// VP 19/10/09 : possibilité de créer plusieurs synonymes
			//if ($idx==='new' && !empty($syno)) { // Création à la volée d'un nv syno ! ATTENTION === car new est mot réservé
			if (empty($syno['ID_LEX'])) { // Création à la volée d'un nv syno !
				if(!empty($syno['LEX_TERME'])) $terme=trim($syno['LEX_TERME']);
				else $terme= trim($syno);
				if(!empty($terme)) {
					$newSyno=new Lexique();
					$newSyno->t_lexique['LEX_TERME']=$terme;
					$newSyno->t_lexique['LEX_ID_SYN']=$this->t_lexique['ID_LEX'];
					
					if(empty($syno['LEX_ID_ETAT_LEX']))
						$newSyno->t_lexique['LEX_ID_ETAT_LEX']=1;
					else
						$newSyno->t_lexique['LEX_ID_ETAT_LEX']=$syno['LEX_ID_ETAT_LEX'];
					
					$newSyno->t_lexique['LEX_ID_TYPE_LEX']=$this->t_lexique['LEX_ID_TYPE_LEX'];
					$newSyno->t_lexique['ID_LANG']=$this->t_lexique['ID_LANG'];
					
					//Si la valeur existe déjà , on retourne juste son id
					$exist = $newSyno->checkExist(false, false, false);
					//Sinon on la créé ou on la sauvegarde
					$existId = $newSyno->checkExistId();
					
					if ($exist || $existId) {
						//$this->t_lexique['ID_LEX'] = $exist;
						if(!$update) return $newSyno->t_lexique['ID_LEX'];
						$ok = $newSyno->save();
					} else {
						if (!$create) return false;
						$ok = $newSyno->create(false);
					}

					// VP 19/10/09 : sauvegarde de versions différentes arrSyno[XX]['LEX_TERME']
					foreach ($_SESSION['arrLangues'] as $lang) {
						if($lang != strtoupper($this->t_lexique['ID_LANG'])){
							$lexVersions[$lang] = array(
								'LEX_TERME' =>	(!empty($syno['LEX_TERME_'.$lang]))?$syno['LEX_TERME_'.$lang]:$newSyno->t_lexique['LEX_TERME'],
								'LEX_NOTE'	=>	(!empty($syno['LEX_NOTE_'.$lang]))?$syno['LEX_NOTE_'.$lang]:$newSyno->t_lexique['LEX_NOTE']
							);
						}
					}
					$newSyno->saveVersions($lexVersions);
					unset($lexVersions);
					if (!$ok) {$this->dropError(kErrorLexiqueCreationSyno.": ".$newSyno->error_msg);}
					unset($newSyno);
				}
			} else {

				$rs = $db->Execute("SELECT * FROM t_lexique WHERE ID_LEX=".intval($syno['ID_LEX']));
				$sql = $db->GetUpdateSQL($rs, array("LEX_ID_SYN" => intval($this->t_lexique['ID_LEX'])));
				if (!empty($sql)) $ok=$db->Execute($sql);
				if (!$ok) {$this->dropError(kErrorLexiqueSauveSyno);return false;}
			}
		}
	}
	
	function replaceNonPrefSyno() {
		if (empty($this->arrSynoPref) && count($this->arrSyno > 0)) {
			global $db;
			$id_lex = $this->t_lexique['ID_LEX'];
			
			foreach ($this->arrSyno as $syno) {
				$id_syn = $syno['ID_LEX'];
				$rs = $db->Execute("SELECT * FROM t_doc_lex WHERE ID_LEX=".intval($id_syn));
				$sql = $db->GetUpdateSQL($rs, array("ID_LEX" => intval($id_lex)));
				if (!empty($sql)) $ok=$db->Execute($sql);
				//controler syno deja présent
			}
		}
	}

	function saveChildren() {

		global $db;
		$db->StartTrans();
		$rs = $db->Execute("SELECT * FROM t_lexique WHERE LEX_ID_GEN=".intval($this->t_lexique['ID_LEX']));
		$sql = $db->GetUpdateSQL($rs, array("LEX_ID_GEN" => 0));
		if (!empty($sql)) $db->Execute($sql);
		foreach ($this->arrChildren as $idx=>$Children) { // Puis on rattache.
				$rs = $db->Execute("SELECT * FROM t_lexique WHERE ID_LEX=".intval($Children['ID_LEX']));
				$sql = $db->GetUpdateSQL($rs, array("LEX_ID_GEN" => intval($this->t_lexique['ID_LEX'])));
				if (!empty($sql)) $ok=$db->Execute($sql);
				if (!$ok) {$this->dropError(kErrorLexiqueAttachChildren);$db->CompleteTrans();return false;}
		}
		$db->CompleteTrans();
	}



	/** Sauve les associations de terme lexique
	 *  IN : tableau classe arrAsso
	 *  OUT : mise à jour DB
	 *  NOTE : cette table est bijective => requetes faites dans les 2 sens
	 */
	function saveAsso($purge=true, $create = true, $update = false) {
		global $db;
		$db->StartTrans();
		if ($purge) {
			$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']));
			$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX_ASSO=".intval($this->t_lexique['ID_LEX']));
		}
		foreach ($this->arrAsso as $idx=>$asso) {
			// VP 19/10/09 : possibilité de créer plusieurs termes associés
			//if ($idx==='new' && !empty($asso)) { // Création à la volée d'un nv syno !
			if (empty($asso['ID_LEX'])) { // Création à la volée d'un nv syno !
				if(!empty($asso['LEX_TERME'])) $terme=$asso['LEX_TERME'];
				$terme = trim($terme);
				$newAsso=new Lexique();
				$newAsso->t_lexique['LEX_TERME']=trim($terme);

				if(empty($asso['LEX_ID_ETAT_LEX']))
					$newAsso->t_lexique['LEX_ID_ETAT_LEX']=1;
				else
					$newAsso->t_lexique['LEX_ID_ETAT_LEX']=$asso['LEX_ID_ETAT_LEX'];
				
				$newAsso->t_lexique['LEX_ID_TYPE_LEX']=$this->t_lexique['LEX_ID_TYPE_LEX'];
				$newAsso->t_lexique['ID_LANG']=$this->t_lexique['ID_LANG'];
				
				//Si la valeur existe déjà , on retourne juste son id
				$exist = $newAsso->checkExist(false, false, false);
				//Sinon on la créé ou on la sauvegarde
				$existId = $newAsso->checkExistId();
				
				if ($exist || $existId) {
					//$this->t_lexique['ID_LEX'] = $exist;
					if(!$update) return $newAsso->t_lexique['ID_LEX'];
					$ok = $newAsso->save();
				} else {
					if (!$create) return false;
					$ok = $newAsso->create(false);
				}
	
				foreach ($_SESSION['arrLangues'] as $lang) {
					if($lang != strtoupper($this->t_lexique['ID_LANG'])){
						$lexVersions[$lang] = array(
							'LEX_TERME' =>	(!empty($asso['LEX_TERME_'.$lang]))?$asso['LEX_TERME_'.$lang]:$newAsso->t_lexique['LEX_TERME'],
							'LEX_NOTE'	=>	(!empty($asso['LEX_NOTE_'.$lang]))?$asso['LEX_NOTE_'.$lang]:$newAsso->t_lexique['LEX_NOTE']
						);
					}
				}
				$newAsso->saveVersions($lexVersions);
				unset($lexVersions);

				if (!$ok) {$this->dropError(kErrorLexiqueCreationAsso.$newAsso->error_msg);}

				if (!$purge) {
					$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']) . " AND ID_LEX_ASSO=".intval($newAsso->t_lexique['ID_LEX']));
					$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX=".intval($newAsso->t_lexique['ID_LEX']) . " AND ID_LEX_ASSO=".intval($this->t_lexique['ID_LEX']));
				}
				
				$sql="INSERT INTO t_lexique_asso (ID_LEX,ID_LEX_ASSO) values (".intval($this->t_lexique['ID_LEX']).",".intval($newAsso->t_lexique['ID_LEX']).")";
				$db->Execute($sql);
				$sql="INSERT INTO t_lexique_asso (ID_LEX_ASSO,ID_LEX) values (".intval($this->t_lexique['ID_LEX']).",".intval($newAsso->t_lexique['ID_LEX']).")";
				$db->Execute($sql);
				unset($newAsso);
			} else {

				if (!$purge) {
					$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']) . " AND ID_LEX_ASSO=".intval($asso['ID_LEX']));
					$db->Execute("DELETE from t_lexique_asso WHERE ID_LEX=".intval($asso['ID_LEX']) . " AND ID_LEX_ASSO=".intval($this->t_lexique['ID_LEX']));
				}
				
				$sql="INSERT INTO t_lexique_asso (ID_LEX,ID_LEX_ASSO) values (".intval($this->t_lexique['ID_LEX']).",".intval($asso['ID_LEX']).")";
				$db->Execute($sql);
				$sql="INSERT INTO t_lexique_asso (ID_LEX_ASSO,ID_LEX) values (".intval($this->t_lexique['ID_LEX']).",".intval($asso['ID_LEX']).")";
				$db->Execute($sql);
			}
		}
		$db->CompleteTrans();
	}


	/** Attache un lexique à un père. Cette relation est unique et non-bijective.
	 * IN : objet lieu, tableau du lieu père à attacher (à voir si on utilise pas objet),
	 *
	 * OUT : màj de l'objet (PAS de sauvegarde).
	 */
	function attachTo ($lexToAttach) {

		if ((int)$lexToAttach['LEX_ID_GEN']!=0 ) {
			$this->dropError(kErrorLexiqueAttache);
			return false;
		}

		if ((int)$this->t_lexique['LEX_ID_GEN']!=0 ) {
			$this->dropError(kWarningLexiqueDejaAttache);
		}

		$this->t_lexique['LEX_ID_GEN']=$lexToAttach['ID_LEX'];
	}

	/**
	 * Détache le lexique de son lexique père
	 * IN : objet lexique
	 * OUT : objet lexique màj.
	 */
	function detach () {
		$this->t_lexique['LEX_ID_GEN']=0;
	}

	/**
	 * Détache tous les fils d'un objet.
	 * IN : objet
	 * OUT : mise à jour BDD;
	 */
	function detachChildren () {
		global $db;
		$rs = $db->Execute("SELECT * FROM t_lexique WHERE LEX_ID_GEN=".intval($this->t_lexique['ID_LEX']));
		$sql = $db->GetUpdateSQL($rs, array("LEX_ID_GEN" => 0));
		
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!empty($sql) && !$ok) {$this->dropError(kErrorLexiqueDetacheTous);return false;}
		return true;
	}

	function detachLink($link) {
		global $db;
		if (!in_array($link,array('PERS','DOC'))) return false;
		
		if(($link=="doc" || $link == 'DOC')){
			require_once(modelDir.'model_doc.php');
			$this->getDocs();
		}
		
		$sql="DELETE FROM t_".strtolower($link)."_lex where ID_LEX=".intval($this->t_lexique['ID_LEX']);
		$ok=$db->Execute($sql);
		if (!$ok) {$this->dropError(kErrorLexiqueDetacheLien);return false;}
		$this->dropError(kSuccesLexiqueDetacheLien);
		
		if(($link=="doc" || $link == 'DOC') && !empty($this->arrDoc)){
			foreach($this->arrDoc as $id_doc){
				$doc_lie = new Doc();
				$doc_lie->t_doc['ID_DOC'] = $id_doc;
				$doc_lie->updateChampsXML("DOC_LEX");
			}
			unset($this->arrDoc);
		}
		
		return true;
	}



	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 *
	 */
	 //@update VG 02/06/2010 : ajout de trim()
	 //@update VG 01/07/2010 : retrait de init + ajout du controle sur les champs que l'on enregistre
	 //@update VG 08/07/2010 : ajout du controle pour savoir si ID_LANG est déjà renseigné
 	function updateFromArray ($tab_valeurs) {

	 	//if (empty($this->t_lexique)) $this->init();
	 	global $db;
	 	$cols=$db->MetaColumns('t_lexique');
	 	foreach ($tab_valeurs as $fld=>$val) {
			//if (isset($this->t_lexique[strtoupper($fld)])) $this->t_lexique[strtoupper($fld)]=trim(stripControlCharacters($val));
			if (array_key_exists(strtoupper($fld), $cols)) $this->t_lexique[strtoupper($fld)]=trim(stripControlCharacters($val));
		}
		//$this->t_pl_rec=$tab_valeurs['t_pl_rec'];
		if(empty($this->t_lexique['ID_LANG']))$this->t_lexique['ID_LANG']=$_SESSION['langue'];
		
		if (isset($tab_valeurs['arrFather'][0]['ID_LEX']))
			$this->t_lexique['LEX_ID_GEN']=$tab_valeurs['arrFather'][0]['ID_LEX']; // Récup du parent depuis le tableau
		//else
			//$this->t_lexique['LEX_ID_GEN']=null; // Récup du parent depuis le tableau
		
		if (isset($tab_valeurs['arrSyno']))
			$this->arrSyno=$tab_valeurs['arrSyno'];
		else
			$this->arrSyno=null;
		
		if (isset($tab_valeurs['arrAsso']))
			$this->arrAsso=$tab_valeurs['arrAsso'];
		else
			$this->arrAsso=null;
		
		if (isset($tab_valeurs['arrChildren']))
			$this->arrChildren=$tab_valeurs['arrChildren'];
		else
			$this->arrChildren=null;
	}

	/**
	 * Ajoute un message d'erreur pour la sortie
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function dropError($errLib) { //: A ETOFFER EN TRY CATCH ?
		$this->error_msg.=$errLib."<br/>";
	}

	function delete() {
		global $db;
		if ($this->t_lexique['ID_LEX']=='0' || empty($this->t_lexique['ID_LEX'])) { $this->dropError(kErrorLexiqueSuppr);return false; }
		$db->StartTrans();
		$db->Execute("DELETE FROM t_lexique WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']));
		$db->Execute("DELETE FROM t_pers_lex WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']));
		$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (
							SELECT ID_DOC_LEX from t_doc_lex WHERE ID_LEX=".intval($this->t_lexique['ID_LEX'])."
						 )");
		$db->Execute("DELETE FROM t_doc_lex WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']));
		$db->Execute("DELETE FROM t_lexique_asso WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']));
		$db->Execute("DELETE FROM t_lexique_asso WHERE ID_LEX_ASSO=".intval($this->t_lexique['ID_LEX']));

		if (!$db->CompleteTrans()) {$this->dropError(kErrorLexiqueSuppr);return false;}
		else return true;
	}

	/**
	 * Récupère les ID des doc liés à ce lexique
	 * IN : rien
	 * OUT : tab de classe arrDoc
	 */
	function getDocs($withDocLex=true) {
		global $db;
		// VP 9/9/10 : initialisation tableau arrDoc
		$this->arrDoc=array();
        $this->arrDocLex=array();
		if(!empty($this->t_lexique['ID_LEX'])){
            // tableau d'ID documents
			$sql="SELECT DISTINCT ID_DOC from t_doc_lex WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']);
            $this->arrDoc = $db->GetCol($sql);
			if($withDocLex){
				// tableau de documents
				$sql="SELECT dl.* ,DOC_COTE, DOC_TITRE, DOC_TITRE_COL, DOC_DATE_PROD, TYPE_DOC
					from t_doc_lex dl, t_doc d
					LEFT OUTER JOIN t_type_doc td ON td.ID_TYPE_DOC=d.DOC_ID_TYPE_DOC and td.ID_LANG=d.ID_LANG
					WHERE ID_LEX=".intval($this->t_lexique['ID_LEX'])." and dl.id_doc=d.ID_DOC and d.ID_LANG=".$db->Quote($this->t_lexique['ID_LANG'])." order by DOC_TITRE";
				$this->arrDocLex=$db->GetAll($sql);
			}
		}
	}
	
	function getPossibleSolrFields(){
		global $db;
		if(!defined("useSolr") || !useSolr || empty($this->t_lexique['ID_LEX']) || empty($this->t_lexique['LEX_ID_TYPE_LEX'])){
			return false ; 
		}
		if(empty($this->arrDoc)){
			$this->getDocs(false);
		}
		if(empty($this->arrDoc)){
            return false;
        }
		$this->solr_fields = array();
		
		if(defined("kDbType") && strpos(kDbType,'postgre')!==false){
			$sql = "SELECT string_agg(id_type_desc,',') as ARR_DESC, string_agg(dlex_id_role,',') as ARR_ROLE from t_doc_lex where id_doc in (".implode(',',$this->arrDoc).") and ID_LEX=".$db->Quote($this->t_lexique['ID_LEX']).";";
		}else{
			$sql = "SELECT group_concat(id_type_desc,',') as ARR_DESC, group_concat(dlex_id_role,',') as ARR_ROLE from t_doc_lex where id_doc in (".implode(',',$this->arrDoc).") and ID_LEX=".$db->Quote($this->t_lexique['ID_LEX']).";";
		}
		$dumb=$db->GetRow($sql);
		if(!empty($dumb) && !empty($dumb['ARR_DESC']) && !empty($dumb['ARR_ROLE'])){
			$desc_types =  explode(',',$dumb['ARR_DESC']); 
			$role_types = explode(',',$dumb['ARR_ROLE']);
			unset($dumb);
			
			foreach($desc_types as $idx=>$desc){
				$this->solr_fields[] = trim(strtolower("l_".$desc."_".$role_types[$idx]."_".$this->t_lexique['LEX_ID_TYPE_LEX']),"_");
			}
			$this->solr_fields = array_unique($this->solr_fields);
		}
		return true ; 
	}

	/**
	 * Récupère les ID des lexiques "enfants" liés à ce lieu
	 * IN : rien
	 * OUT : tab de classe arrChildren
	 */
	function getChildren() {
		global $db;
		$sql="SELECT * from t_lexique WHERE LEX_ID_GEN=".intval($this->t_lexique['ID_LEX'])."  AND ID_LANG=".$db->Quote($this->t_lexique['ID_LANG']);

		$this->arrChildren=$db->GetAll($sql);
		//foreach ($dumb as $idx=>$child) $this->arrChildren[]=$child['ID_LEX'];

	}


	/** Récupère les libellés pour toutes les langues de l'appli.
	 *  IN : rien (objet)
	 * 	OUT : tableau de classe arrVersions
	 *  NOTE : si le lexique existe déjà en base, on ramènes ttes les langues même si le lib n'existe pas encore (right join)
	 * 		   si le lexique  n'existe pas (création), on ramène toutes les langues.
	 */
	function getVersions () {
		global $db;
		$tabLg=$_SESSION['tab_langue'];
		unset($tabLg[$_SESSION['langue']]); // retrait de la langue en cours.
		if (!empty($this->t_lexique['ID_LEX'])) {
		$sql="SELECT t_lang.ID_LANG, LEX_TERME, LEX_NOTE, LANG, ID_LEX FROM t_lexique
				right join t_lang on (t_lang.ID_LANG=t_lexique.ID_LANG)
				WHERE
				t_lexique.ID_LANG<>".$db->Quote($this->t_lexique['ID_LANG'])." AND ID_LEX=".intval($this->t_lexique['ID_LEX']);
		$this->arrVersions=$db->GetAll($sql);
			// VP 12/03/10 : spaceQuote pour versions
			foreach($this->arrVersions as &$version){
				$version['LEX_TERME']=str_replace(array(" ' "," ’ "),array("'","’"),$version['LEX_TERME']);
			}
		}

		if (empty($this->arrVersions)){ //Pas d'ID OU Pas de version enregistrée, on charge la liste des langages
		$sql="SELECT *,'' as LEX_TERME, '' as LEX_NOTE, null as ID_LEX FROM t_lang
				WHERE
				ID_LANG<>".$db->Quote($this->t_lexique['ID_LANG']);
		$this->arrVersions=$db->GetAll($sql);
		}
	}


	/** Effectue la fusion des termes
	 *  IN : terme créé et ID_LEX du document d'origine
	 *  OUT : true/false selon réussite ou échec
	 *  NOTE : règles de fusion => c'est le nouveau terme ie le terme en cours d'édition qui est gardé
	 * 	au détriment du terme original qui sera supprimé.
	 *  Avant la destruction, on récupère et on associe au nouveau terme :
	 * 	les liens avec doc/personnes, les fils, les synonymes, les termes associés (bijection)
	 *  et le père si le nouveau terme n'en a pas déjà un.
	 */
	function fusion($fusionLexId) {
		global $db;
		$db->StartTrans();
		trace('fusion lexique');
		if (empty($fusionLexId) || !is_numeric($fusionLexId)) {$this->dropError('XXX FUSION PAS POSSIBLE PB ID'); return false;}
		
		$lex_fusion=new Lexique();
		$lex_fusion->t_lexique['ID_LEX']=intval($fusionLexId);
		$lex_fusion->getLexique();
		
		// fusion des valeurs de t_lexique
		// si la valeur du lexique courant est vide on prends la valeur du lexique cible
		foreach ($this->t_lexique as $idx=>$val)
		{
			if ($idx!='ID_LEX' && $idx!='ID_LANG' && empty($val))
			{
				$this->t_lexique[$idx]=$lex_fusion->t_lexique[$idx];
			}
		}
		
		
		$ok=$this->save(true); //on sauve en mode fusion -> pas de checkExist si création

		//On sauve le reste du terme en cours d'édition avant d'effectuer la fusion proprement dite
		if ($ok) $this->saveVersions($_POST['version'],true); //pareil pour les versions
		$this->saveSyno();
		$this->saveAsso();
		$this->saveChildren();

		foreach (array('t_doc_lex','t_pers_lex','t_lexique_asso') as $table) { //copie des tables liées
			$sql="UPDATE ".$table." SET ID_LEX=".intval($this->t_lexique['ID_LEX'])." WHERE ID_LEX=".intval($fusionLexId);
			$ok=$db->Execute($sql);
			if (!$ok) {$this->dropError('XXX PB FUSION'); return false;}
		}
		$db->Execute("UPDATE t_lexique SET LEX_ID_GEN=".intval($this->t_lexique['ID_LEX'])."
						WHERE LEX_ID_GEN=".intval($fusionLexId)); //copie des fils
		$db->Execute("UPDATE t_lexique SET LEX_ID_SYN=".intval($this->t_lexique['ID_LEX'])."
						WHERE LEX_ID_SYN=".intval($fusionLexId));	//copie des syno
		$db->Execute("UPDATE t_lexique_asso SET ID_LEX_ASSO=".intval($this->t_lexique['ID_LEX'])."
						WHERE ID_LEX_ASSO=".intval($fusionLexId)); //copie des asso

		//Si le doc n'a pas de père, on va chercher celui du doc d'origine.
		if (empty($this->t_lexique['LEX_ID_GEN'])) $orgFather=$db->GetOne("SELECT LEX_ID_GEN from t_lexique WHERE ID_LEX=".intval($fusionLexId)); //récup père
		if (!empty($orgFather)) $db->Execute("UPDATE t_lexique SET LEX_ID_GEN=".intval($orgFather)." WHERE ID_LEX=".intval($this->t_lexique['ID_LEX']) );
		//Si non vide ou bien 0, on récupère le père du doc. d'origine
		$db->Execute("DELETE FROM t_lexique WHERE ID_LEX=".intval($fusionLexId));
		return $db->CompleteTrans();
	}

	//update VG 15/12/11
	function updateDocChampsXML($saveSolr=false) {
		require_once(modelDir.'model_doc.php');
		if(empty($this->arrDoc)){
			$this->getDocs(false);
		}
		foreach ($this->arrDoc as $idDoc) {
			$oDoc = new Doc();
			$oDoc->t_doc['ID_DOC'] = $idDoc;
			$oDoc->updateChampsXML("DOC_LEX");
			if($saveSolr && ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))){
				$oDoc->saveSolr();
			}
			unset($oDoc);
		}
	}


	
	// MS - 01.04.16 - Voir fonction setCatPath dans class_categorie : il me semble que ce code fonctionne mal si on ne l'appelle qu'une fois (en effet il est appelé dans la fonction save avant la sauvegarde BDD)
	// or rechercheLex utilise la base de données lors de l'appel à getFather. DONC si on change le pere, le lex_path ne devrait pas être correctement mis à jour 
    function setLexPath() {
        require_once(libDir."class_chercheLex.php");
        $mySearch=new RechercheLex();
        if(!empty($this->t_lexique['ID_LANG'])){
            $mySearch->lang=$this->t_lexique['ID_LANG'];
        }
        $mySearch->getFather($this->t_lexique['ID_LEX'],true,$arrNodes);
        $lex_path="";
        if(count($arrNodes)>0){
            $first=true;
            foreach($arrNodes as $node){
                if($first){
                     $lex_path=$node['terme'];
                     $first=false;
                }else{
                     $lex_path=$node['terme'].chr(9).$lex_path;
                }
            }
        }
        $this->t_lexique['LEX_PATH'] = $lex_path;
    }
}
?>
