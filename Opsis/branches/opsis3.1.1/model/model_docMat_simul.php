<?
require_once(modelDir.'model_docMat.php');

class DocMat_Simul extends DocMat
{
var $error_msg;

var $t_mat;
var $t_doc;
var $t_doc_mat;

var $id_lang; // NOTE : id_lang n'est pas dans la table, mais il est intéressant pour récupérer les intitulés des tables liées

var $debrief;

function __construct($id=null,$version=''){
	parent::__construct($id,$version);
	$this->debrief=array();
}



	/**
	 * Sauve les informations d'un lien DOC_MAT / VALEUR
	 * IN : tableau DOC_MAT_VAL
	 * OUT : debrief
	 */
	function saveDocMatVal() {
		global $db;
		if (empty($this->t_doc_mat_val)) return;
		foreach ($this->t_doc_mat_val as $idx=>$dmVal) {

			if (!empty($dmVal['ID_VAL'])) {

				//Précaution en cas de duplication : on spécifie bien que l'id_doc_mat est celui en cours.
				// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_VAL originel dedans.
				$dmVal['ID_DOC_MAT']=$this->t_doc_mat['ID_DOC_MAT'];
				if ($dmVal['VALEUR']) unset($dmVal['VALEUR']); //On ôte l'objet encapsulé s'il existe

				$this->debrief['doc_mat_val']['new']++;

			}
		}
		return true;
	}



	
	/**
	 * Supression des liens DOC_MAT (+ t_doc_mat_val)
	 * IN : tab de classe t_doc_mat
	 * OUT : bdd maj et tableau t_doc_mat vidé
	 * NOTE : supprime juste le lien DOC_MAT, PAS le document
	 */
	function delete() {
		global $db;
		$this->debrief['dmat']['delete']++;
	}






	/**
	 * Sauve en base un lien document / matériel (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save () {
		global $db;

		if (empty($this->t_doc_mat['ID_DOC_MAT'])) {$this->dropError(kErrorDocMatVide);return false;}

		$this->debrief['dmat']['update']++;

		$this->dropError(kSuccesDocMatSauve);
		return true;
	}

	
	/**
	 * Crée un matériel en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	 /*
	function create () {
		global $db;
		if (empty($this->t_mat['ID_MAT'])) {$this->dropError(kErrorMaterielVide);return false;}
		$exist=$this->checkExist();
		if ($exist) { //mat existe, on redirige donc vers une sauvegarde std (sans renommage)
			$this->t_mat['ID_MAT']=$exist;$this->id_mat_ref=$exist;return $this->save();} //by ld 01/04/08 retour mis this->save au lieu de false


		$this->t_mat['MAT_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));

		$sql="INSERT INTO t_mat (";
		$sql.=implode(",",array_keys($this->t_mat));
		$sql.=") values ('";
		$tmp=implode("§",array_values($this->t_mat));
		$sql.=str_replace("§","','",simpleQuote($tmp));
		$sql.="')";
		$ok=$db->Execute($sql);
		if (!$ok) {$this->dropError(kErrorMaterielCreation);trace($sql);return false;}

		$this->dropError(kSuccesMaterielCreation);
		$this->hasfile=$this->checkFileExists(); // On regarde si le fichier n'existe pas déjà sur le disque
		// NOTE : cela peut être le cas si le fichier a déjà été transféré (via FTP ou copie) ou en cas de reprise de données

		return true;
	}*/

    //*** XML

     /** Effectue le mappage entre un tableau et des champs de doc_mat
	 *
	 */
	function mapFields($arr) {
				
		require_once(modelDir."model_val.php");		
								
		foreach ($arr as $idx=>$val) {
				
				$myField=$idx;
				if ($myField=='ID_DOC_MAT') $this->t_doc_mat['ID_DOC_MAT']=$val;
				// analyse du champ
				$arrCrit=split("_",$myField);
				switch ($arrCrit[0]) {
					case 'DMAT': //champ table DOC
						$val=trim($val); //Important, car souvent, les tags ID3 ont des espaces supplémentaires
						$flds=split(",",$myField);
						if (!empty($val)) foreach ($flds as $_fld) $this->t_doc_mat[$_fld]=$val;
					break;

					case 'V': //valeur
						if(empty($val)) break;
						$myVal=new Valeur();
						$myVal->t_val['VALEUR']=$val;
						$myVal->t_val['VAL_ID_TYPE_VAL']=$arrCrit[1];
						$myVal->t_val['ID_LANG']=$_SESSION['langue'];
						$_val=$myVal->checkExist();
						if ($_val) {
							$myVal->t_val['ID_VAL']=$_val;
							$this->debrief['valeur']['update']++;
						} else {
							$myVal->t_val['ID_VAL']=999; //fictif
							$this->debrief['valeur']['new']++;
						}
						$this->t_doc_mat_val[]=array('ID_TYPE_VAL'=>$arrCrit[1],'ID_VAL'=>$myVal->t_val['ID_VAL']);
					break;

				}
		}
	}


}
?>