<?
require_once(modelDir.'model_doc.php');

class Reportage extends Doc {
	var $t_doc_mat = array();
    var $t_doc_lex = array();
    var $t_doc_val = array();
	var $t_doc_cat = array();
    var $t_images= array();
    var $t_doc_redif =array();
    var $t_doc_seq =array();
	var $t_pers_lex = array();
	var $t_doc_lex_droit = array();
	var $t_doc_prex = array();
	var $t_doc_exp = array();
	var $t_doc_acc = array();
	var $t_doc_right = array();
	var $t_doc_comment = array();

    var $error_msg;

    var $id_doc2link; // a gérer via un tableau
    var $relation;

 	var $t_doc = array();
 	var $vignette; // Chemin de la vignette associée au doc


	var $type_doc;
	var $etat_arch;

	var $arrFather= array();
	var $arrChildren= array();
	var $arrVersions=array();
	var $arrDocAcc=array();
	var $arrDocLiesSRC=array();
	var $arrDocLiesDST=array();
	var $arrDocFilles=array();

 	var $uniqueFields;

 	private $arrFieldsToSaveWithIdLang = array(

	   "DOC_TITRE" , "DOC_SOUSTITRE" , "DOC_RES","DOC_SEQ", "DOC_NOTE_INTERNE"
 	      , "DOC_TITRE_COL","DOC_AUTRE_TITRE", "DOC_RECOMP", "DOC_ACC", "DOC_IM_ARCH",
 	      "DOC_COMMENT","DOC_COMMISSION", "DOC_COPYRIGHT", "DOC_PROCAV","DOC_RES_CAT", "DOC_VISA","DOC_AUD"
 	      , "DOC_ID_ETAT","DOC_ID_ETAT_DOC", "DOC_LIEU_PV", "DOC_XML", "DOC_VAL", "DOC_LEX");

	private $arrFieldsToSaveWithoutIdLang= array(

 	     "DOC_TITREORI", "DOC_ORI_TYPE_SON","DOC_ORI_FORMAT","DOC_ORI_COULEUR" ,"DOC_ID_FONDS","DOC_ID_MEDIA",
 	     "DOC_DIFFUSEUR" ,"DOC_ID_GENRE","DOC_ID_TYPE_DOC" , "DOC_ID_GEN","DOC_TITREORI",
 	     "DOC_TYPE","DOC_DUREE", "DOC_NB_EPISODES","DOC_COTE_ANC", "DOC_COTE","DOC_DATE_DIFF","DOC_DATE_DIFF_FIN","DOC_DROITS",
 	     "DOC_DATE","DOC_DATE_ETAT","DOC_DATE_PROD","DOC_DATE_PV_FIN", "DOC_DATE_PV_DEBUT","DOC_NOTES", "DOC_VI","DOC_AUD"
 	     , "DOC_AUD_PDM","DOC_ID_USAGER_MODIF","DOC_DATE_MOD", "DOC_NUM", "DOC_TCIN", "DOC_TCOUT", "DOC_DATE_FIN_DROITS", "DOC_DUREE_DIFF","DOC_DATE_CREA", "DOC_ID_USAGER_CREA");


    private $arrTextFields=array("doc_titre","doc_soustitre","doc_titre_col","doc_autre_titre","doc_titreori",
			"doc_res","doc_seq","doc_notes","doc_recomp","doc_acc",
			"doc_comment","doc_procav","doc_note_interne");
			
	// Constructeur

    function __construct(){
    	parent::__construct();
    }
	
	function getFirstVignette() {
		global $db;
		$this->getDocLiesDST();
		if (count($this->arrDocLiesDST) > 0) {
			$id_img = $db->getOne("SELECT DOC_ID_IMAGE FROM t_doc WHERE ID_DOC = ".intval($this->arrDocLiesDST[0]['ID_DOC'])." AND ID_LANG like '".$_SESSION['langue']."'");
			if ($id_img)
				return $this->t_doc['DOC_ID_IMAGE'] = $id_img;
		}
		return false;
	}
	
	function getChildrenIds() {
		global $db;
		$this->getDocLiesDST();
		$arrIds = array();
		foreach ($this->arrDocLiesDST as $dst) {
			$arrIds[] = $dst['ID_DOC'];
		}
		return $arrIds;
	}
	
	function saveDocLies($purge=true) {
		parent::saveDocLies($purge);
		
		$this->heritReportFields();
	}
	
	function heritReportFields() {
	
		global $db;
		if (defined("gReportInheritedFields")){
			require_once(libDir."class_modifLot.php");
			if (is_array(unserialize(gReportInheritedFields))) $arrFields = unserialize(gReportInheritedFields);
			else $arrFields = explode('/', gReportInheritedFields);
			//var_dump($arrFields);return;
			
			//Chargement données si inexistantes
			if (count($this->t_doc) <= 2) $this->getDoc();
			if (count($this->t_doc_lex) == 0) $this->getLexique();
			if (count($this->t_pers_lex) == 0) $this->getPersonnes();
			if (count($this->t_doc_val) == 0) $this->getValeurs();
			if (count($this->t_doc_cat) == 0) $this->getCategories();
			
			$myMod=new ModifLot("doc","");
			$myMod->arrFields=array();
			$myMod->setSearchSQL(true, "SELECT distinct ID_DOC,'".$_SESSION['langue']."' as ID_LANG from t_doc where ID_DOC in (select id_doc_src from t_doc_lien where id_doc_dst = ".intval($this->t_doc['ID_DOC']).")");
			$hasResult=$myMod->getResults();
			if ($hasResult){
				$multiArrParams=array();
				foreach ($arrFields as $field=>$commande){
					$arrWF = explode("_", $field);
					switch ($arrWF[0]) {
						case "DOC":
							if (isset($this->t_doc[$field])) {
								$multiArrParams[] = array("field" => $field, "value" => $this->t_doc[$field], 'commande' => $commande);
								//$myMod->updateFromArray(array("field" => $field, "value" => $this->t_doc[$field], 'commande' => $commande));
								//$myMod->applyModif();
							}
							break;
						case "CAT" :
							$arrIds = array();
							foreach ($this->t_doc_cat as $docCat) if ($docCat['ID_TYPE_CAT'] == $arrWF[1]) $arrIds[$docCat['ID_CAT']] = $docCat['ID_CAT'];
							if (count($arrIds) > 0 ) {
								$multiArrParams[] = array("field" => $field, "id" => $arrIds, 'commande' => $commande);
								//$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								//$myMod->applyModif();
							}
							break;
						case "L" :
							$arrIds = array();
							foreach ($this->t_doc_lex as $docLex) if ($docLex['ID_TYPE_DESC'] == $arrWF[1] && (!isset($arrWF[2]) || $docLex['DLEX_ID_ROLE'] == $arrWF[2])) $arrIds[$docLex['ID_LEX']] = array('ID_LEX' => $docLex['ID_LEX'], "DLEX_ID_ROLE" => $docLex['DLEX_ID_ROLE'], "ID_TYPE_DESC" => $docLex['ID_TYPE_DESC']);
							if (count($arrIds) > 0 ) {
								$multiArrParams[] = array("field" => $field, "id" => $arrIds, 'commande' => $commande);
								//$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								//$myMod->applyModif();
							}
							break;
						case "P" :
							$arrIds = array();
							foreach ($this->t_pers_lex as $persLex)
							if ($persLex['ID_TYPE_DESC'] == $arrWF[1] && (!isset($arrWF[2]) || $persLex['DLEX_ID_ROLE'] == $arrWF[2]))
								$arrIds[$persLex['ID_PERS']] = array('ID_PERS' => $persLex['ID_PERS'], "DLEX_ID_ROLE" => $persLex['DLEX_ID_ROLE'], "ID_TYPE_DESC" => $persLex['ID_TYPE_DESC']);
							if (count($arrIds) > 0 ) {
								$multiArrParams[] = array("field" => $field, "id" => $arrIds, 'commande' => $commande);
								//$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								//$myMod->applyModif();
							}
							break;
						case "V" :
							$arrIds = array();
							foreach ($this->t_doc_val as $docVal) if ($docVal['ID_TYPE_VAL'] == $arrWF[1]) $arrIds[] = $docVal['ID_VAL'];
							if (count($arrIds) > 0 ) {
								$multiArrParams[] = array("field" => $field, "id" => $arrIds, 'commande' => $commande);
								//$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								//$myMod->applyModif();
							}
							break;
					}
				}
				
				$myMod->updateFromArray($multiArrParams, true);
				$myMod->applyAllModifs();
			}
			unset($myMod);
			// foreach ($hasResult as $res) {
			// 	$db->execute("delete from t_doc_lex where id_doc_lex in (
			// 					select dl.id_doc_lex
			// 					from t_doc_lex dl
			// 					,(select min(id_doc_lex) as id_doc_lex, id_doc, id_lex from t_doc_lex WHERE id_doc = ".intval($res['ID_DOC'])." group by id_doc, id_lex having count(1) > 1) db
			// 					where db.id_doc = dl.id_doc AND db.id_lex = dl.id_lex and dl.id_doc_lex != db.id_doc_lex AND dl.id_doc = ".intval($res['ID_DOC']).");");
			// }
		}
	}

	function heritAllFieldsToReport() {
		global $db;
		if (defined("gReportToHeritFields")){
			require_once(libDir."class_modifLot.php");
			if (is_array(unserialize(gReportToHeritFields))) $arrFields = unserialize(gReportToHeritFields);
			else $arrFields = explode('/', gReportToHeritFields);
			
			//Chargement données si inexistantes
			if (count($this->t_doc) <= 2) $this->getDoc();
			if (count($this->t_doc_lex) == 0) $this->getLexique();
			if (count($this->t_doc_val) == 0) $this->getValeurs();
			if (count($this->arrDocLiesDST) == 0) $this->getDocLiesDST();
			foreach ($this->arrDocLiesDST as $idx=>&$dl) if (!empty($dl['ID_DOC_SRC'])) {
				require_once(modelDir.'model_doc.php');
				$doc = New Doc();
				$doc->t_doc['ID_DOC'] = $dl['ID_DOC_SRC'];
				$doc->getDoc();
				$doc->getLexique();
				$doc->getValeurs();
				$dl['DOC'] = $doc;
			}
			
			$myMod=new ModifLot("doc","");
			$myMod->arrFields=array();
			$myMod->setSearchSQL(true, "SELECT distinct ID_DOC,'".$_SESSION['langue']."' as ID_LANG from t_doc where ID_DOC = ".intval($this->t_doc['ID_DOC']));
			$hasResult=$myMod->getResults();
			if ($hasResult)
				foreach ($arrFields as $field=>$commande){
					$arrWF = explode("_", $field);
					switch ($arrWF[0]) {
						case "DOC":
							if ($commande == "MIN") {
								$value = null;
								foreach ($this->arrDocLiesDST as $idx=>&$dl)
									if (!empty($dl['DOC']) && isset($dl['DOC']->t_doc[$field])) {
										if ($value == null || strcmp($dl['DOC']->t_doc[$field], $value) < 0) $value = $dl['DOC']->t_doc[$field];
									}
								if ($value != null) {
									$myMod->updateFromArray(array("field" => $field, "value" => $value, 'commande' => "INIT"));
									$myMod->applyModif(); }
							}
							break;
						case "L" :
							$arrIds = array();
							foreach ($this->arrDocLiesDST as $idx=>&$dl) if (!empty($dl['DOC'])) {
								foreach ($dl['DOC']->t_doc_lex as $docLex)
									if ($docLex['ID_TYPE_DESC'] == $arrWF[1] && (!isset($arrWF[2]) || $docLex['DLEX_ID_ROLE'] == $arrWF[2])) 
										$arrIds[$docLex['ID_LEX']] = array('ID_LEX' => $docLex['ID_LEX'], "DLEX_ID_ROLE" => $docLex['DLEX_ID_ROLE'], "ID_TYPE_DESC" => $docLex['ID_TYPE_DESC']);
							}
							if (count($arrIds) > 0 ) {
								$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								$myMod->applyModif(); }
							break;
						case "V" :
							$arrIds = array();
							foreach ($this->arrDocLiesDST as $idx=>&$dl) if (!empty($dl['DOC'])) {
								foreach ($dl['DOC']->t_doc_val as $docVal) if ($docVal['ID_TYPE_VAL'] == $arrWF[1]) $arrIds[$docVal['ID_VAL']] = $docVal['ID_VAL'];
							}
							if (count($arrIds) > 0 ) {
								$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								$myMod->applyModif(); }
							break;
					}
				}
			unset($myMod);
			$db->execute("delete from t_doc_lex where id_doc_lex in (
							select dl.id_doc_lex
							from t_doc_lex dl
							,(select min(id_doc_lex) as id_doc_lex, id_doc, id_lex from t_doc_lex WHERE id_doc = ".intval($report->t_doc['ID_DOC'])." group by id_doc, id_lex having count(1) > 1) db
							where db.id_doc = dl.id_doc AND db.id_lex = dl.id_lex and dl.id_doc_lex != db.id_doc_lex AND dl.id_doc = ".intval($report->t_doc['ID_DOC']).");");
		}
	}
	
	
	function updateDocNum(){
		$id_doc = intval($this->t_doc['ID_DOC']);
		if(empty($id_doc)) {
			return false;
		}
		global $db;
		
		if(!isset($this->t_doc['DOC_NUM'])){
			$this->getDoc();
		}
		
		if(empty($this->arrDocLiesDST)){
			$this->getDocLiesDST();
		}
		$updated_doc_num = false ; 
		foreach($this->arrDocLiesDST as $idx=>$doc){
			$updated_doc_num = ($updated_doc_num  || intval($doc['DOC_NUM']));
		}
		
		
		if($updated_doc_num){
			$updated_doc_num= '1' ;
		}else{
			$updated_doc_num = '0';
		}
		
		$rs = $db->Execute('SELECT * FROM t_doc WHERE ID_DOC='.intval($this->t_doc['ID_DOC']));
		$record = array("DOC_NUM" =>($updated_doc_num));
		$sql = $db->GetUpdateSQL($rs, $record);

		
		if (!empty($sql)) $ok = $db->Execute($sql);
		

		if ($ok &&	(defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))
			$this->saveSolr();
	}
	
	
	static function checkReportageWithId($id_doc){
		global $db;
		$id_report = $db->getOne("SELECT ID_TYPE_DOC from t_type_doc WHERE ID_TYPE_DOC = ".intval(gReportagePicturesActiv)." AND ID_LANG like 'FR'");
		if ($id_report) {
			$type = $db->getOne("SELECT DOC_ID_TYPE_DOC FROM t_doc WHERE ID_DOC = ".intval($id_doc)." AND ID_LANG like '".$_SESSION['langue']."'");
			return ($type == $id_report);
		}
		return false;
	}
	
	static function isReportageEnabled(){
		global $db;
		return (defined("gReportagePicturesActiv") && gReportagePicturesActiv > 0);
	}
} // fin classes



?>
