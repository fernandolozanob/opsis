<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- D�veloppeurs : Vincent Prost, Fran�ois Duran, J�rome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

// XS : 28/09/2005 : Cr�ation du fichier, d�finition de la classe Groupe

//************************************************** GROUPE ***************************************************************
require_once(modelDir.'model.php');
class Groupe extends Model
{

	var $t_groupe;

	var $t_groupe_fonds;

	var $error_msg;


	function __construct($id=null,$version='') {
		parent::__construct('t_groupe',$id,$version) ; 
	}

	// a priori pas besoin de refonte getData, rien de specifique à la fonction getGroupe
	function getGroupe($arrGrp=array()){
		return $this->getData($arrGrp) ; 
	}
	


	function getGroupeFonds() {
		require_once(modelDir.'model_fonds.php');
		global $db;
		$sql="SELECT * from t_groupe_fonds WHERE ID_GROUPE=".intval($this->t_groupe['ID_GROUPE']);

		$this->t_groupe_fonds=$db->GetAll($sql);

		foreach ($this->t_groupe_fonds as $idx=>&$fnd) {
			$arrId[]=$fnd['ID_FONDS'];
			$fnd['PRIV']=$db->GetOne("SELECT PRIV from t_privilege WHERE ID_PRIV=".intval($fnd['ID_PRIV'])." AND ID_LANG=".$db->Quote($_SESSION['langue']));
		}

		$dumb=new Fonds;
		$arrFnd=$dumb->getFonds($arrId); unset($dumb);
		foreach ($arrFnd as $idx=>$fndObj) $this->t_groupe_fonds[$idx]['FONDS']=$fndObj;
	}


	function getUsagerGroupe() {

		global $db;
		$sql="SELECT * from t_usager_groupe WHERE ID_GROUPE=".intval($this->t_groupe['ID_GROUPE']);

		$this->t_usager_groupe=$db->GetAll($sql);

		foreach ($this->t_usager_groupe as $idx=>$grp) $arrId[]=$grp['ID_USAGER'];
		require_once(modelDir.'model_usager.php');
		$dumb=new Usager;
		$arrGrp=$dumb->getUsager($arrId); 
		unset($dumb);
		
		 // L'association précédente pouvait associé une instance de t_usager_groupe ID_USAGER=1 à un objet usager ID_USAGER=6 suivant l'ordre de retour des résultats
		foreach($this->t_usager_groupe as $idx=>$us_grp){
			foreach($arrGrp as $grpObj){
				if($grpObj->t_usager['ID_USAGER'] == $us_grp['ID_USAGER']){
					$this->t_usager_groupe[$idx]['USAGER']=$grpObj;
				}
			}
		}
	}

	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {

	 	if (empty($this->t_groupe)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_groupe[strtoupper($fld)])) $this->t_groupe[strtoupper($fld)]=$val;
		}

	}

	function checkExist() {
		global $db;
		return $db->GetOne("SELECT id_groupe from t_groupe
							WHERE LOWER(TRIM(GROUPE))=".$db->Quote(trim(strtolower($this->t_groupe['GROUPE'])))
							.(!empty($this->t_groupe['ID_GROUPE'])?" AND ID_GROUPE<>".intval($this->t_groupe['ID_GROUPE']):"") );
	}


	function save() {
		global $db;
		if (empty($this->t_groupe['ID_GROUPE'])) {return $this->create();}
		if ($this->checkExist()) {$this->dropError(kErrorGroupeExisteDeja);return false;}

		$ok=$db->Execute('UPDATE t_groupe SET GROUPE='.$db->Quote($this->t_groupe['GROUPE']).' WHERE ID_GROUPE='.intval($this->t_groupe['ID_GROUPE']));

		if (!$ok) {$this->dropError(kErrorGroupeSauve);return false;}
		return true;
	}

	function create() {
		global $db;
		if (!empty($this->t_groupe['ID_GROUPE'])) {return $this->save();}
		if ($this->checkExist()) {$this->dropError(kErrorGroupeExisteDeja);return false;}

		if (isset($this->t_groupe['ID_GROUPE']) && intval($this->t_groupe['ID_GROUPE']) == 0){
            unset($this->t_groupe['ID_GROUPE']);
        }
		$ok =$db->insertBase("t_groupe","id_groupe",$this->t_groupe);

		if (!$ok) {$this->dropError(kErrorGroupeCreation);return false;}
        $this->t_groupe['ID_GROUPE']=$ok;
		return true;
	}


	function saveUsager() {
		global $db;

		if (empty($this->t_groupe['ID_GROUPE'])) {$this->dropError(kErrorGroupeNoID);return false;}
		$db->StartTrans();
		$db->Execute('DELETE from t_usager_groupe WHERE ID_GROUPE='.intval($this->t_groupe['ID_GROUPE']));

		foreach ($this->t_usager_groupe as $ug) {
			$db->Execute('INSERT INTO t_usager_groupe (ID_USAGER,ID_GROUPE) values ('.intval($ug['ID_USAGER']).','.intval($ug['ID_GROUPE']).')');
		}
		$ok=$db->CompleteTrans();

		if (!$ok) {$this->dropError(kErrorGroupeSauveUsager);return false;}
		return true;

	}

	function saveFonds() {
		global $db;

		if (empty($this->t_groupe['ID_GROUPE'])) {$this->dropError(kErrorGroupeNoID);return false;}
		$db->StartTrans();
		$db->Execute('DELETE from t_groupe_fonds WHERE ID_GROUPE='.intval($this->t_groupe['ID_GROUPE']));

		foreach ($this->t_groupe_fonds as $gf) {
			$db->Execute('INSERT INTO t_groupe_fonds (ID_FONDS,ID_PRIV,ID_GROUPE) values ('.intval($gf['ID_FONDS']).','.intval($gf['ID_PRIV']).','.intval($this->t_groupe['ID_GROUPE']).')');
		}
		$ok=$db->CompleteTrans();

		if (!$ok) {$this->dropError(kErrorGroupeSauveFonds);return false;}
		return true;

	}

	function delete() {

		global $db;
		$db->StartTrans();

		$db->Execute("DELETE from t_groupe_fonds WHERE ID_GROUPE=".intval($this->t_groupe['ID_GROUPE']));
		$db->Execute("DELETE from t_usager_groupe WHERE ID_GROUPE=".intval($this->t_groupe['ID_GROUPE']));
		$db->Execute("DELETE from t_groupe WHERE ID_GROUPE=".intval($this->t_groupe['ID_GROUPE']));
		$ok=$db->CompleteTrans();
		if (!$ok) {$this->dropError(kErrorGroupeSuppr);return false;}
		return true;

	}



 	/** Export XML de l'objet usager
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {


        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_groupe>\n";
		// USAGER
		foreach ($this->t_groupe as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}


       if (!empty($this->t_groupe_fonds)) {
        // I.4. Propri�t�s relatives � t_doc_mat
        $content.="\t<t_groupe_fonds nb=\"".count($this->t_groupe_fonds)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_groupe_fonds as $name => $value) {
                $content .= "\t\t<GROUPE_FONDS ID_FONDS=\"".$this->t_groupe_fonds[$name]["ID_FONDS"]."\">\n";
                // Pour chaque valeur
                foreach ($this->t_groupe_fonds[$name] as $val_name => $val_value) {
                    if (is_object($val_value)) {$content.=$val_value->xml_export();}
                    else {
                    	$content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                    }
                }
                $content .= "\t\t</GROUPE_FONDS>\n";
            }
        $content.="\t</t_groupe_fonds>\n";
       }

       if (!empty($this->t_usager_groupe)) {
        // I.4. Propri�t�s relatives � t_doc_mat
        $content.="\t<t_usager_groupe nb=\"".count($this->t_usager_groupe)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_usager_groupe as $name => $value) {
                $content .= "\t\t<USAGER_GROUPE ID_USAGER=\"".$this->t_usager_groupe[$name]["ID_USAGER"]."\">\n";
                // Pour chaque valeur
                foreach ($this->t_usager_groupe[$name] as $val_name => $val_value) {
                    if (is_object($val_value)) {$content.=$val_value->xml_export();}
                    else {
                    	$content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                    }
                }
                $content .= "\t\t</USAGER_GROUPE>\n";
            }
        $content.="\t</t_usager_groupe>\n";
       }


		$content.=$indent."</t_groupe>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
	
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){
		global $db;
		
		if($mode == null){
			$mode=array();
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		$limit="";
		if ($nb!="all"){
			$limit= $db->limit($nb_offset,$nb);
		}
	
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		$xml_file_path = kServerTempDir."/export_opsis_groupe_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
	
		$sql_gr = "SELECT g.* from t_groupe g ".$critere.$limit;
		
		$results = $db->Execute($sql_gr);
		$ids = implode(',',$db->GetCol($sql_gr));
		if($ids == ''){
			fwrite($xml_file,"</EXPORT_OPSIS>\n");
			return false;
		}
		
		foreach ($results as $usr){
			$xml.="\t<t_groupe>";
			foreach($usr as $fld=>$val){
					 $xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
				}
			$xml.="\t</t_groupe>";
			fwrite($xml_file,$xml);
			$xml = "";
		}
		
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
		flock($xml_file, LOCK_UN);
		fclose($xml_file);
		return $xml_file_path;
	}
}

	

?>