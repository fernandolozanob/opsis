<?
require_once(modelDir."/model.php");
include_once(modelDir.'model_imageur.php');
require_once(libDir.'class_matInfo.php');
require_once(modelDir.'model_matTrack.php');
require_once(modelDir."model_val.php");

class Materiel extends Model
{
	var $error_msg;

	var $t_mat;
	var $t_mat_val;
	var $t_doc_mat;
    var $t_mat_pers;
	var $t_mat_track;


	var $vignette;
	var $id_lang; // NOTE : id_lang n'est pas dans la table, mais il est intéressant pour récupérer les intitulés des tables liées

	var $hasfile; // Fichier présent sur le disque ou non ?

	var $id_mat_ref; // ID MAT de référence, instancié au chargement de la page.
	// Il contient l'ID MAT originel, avant TOUTE modification de l'ID_MAT dans le formulaire



	var $id_media; //type de media (A/V) déterminé par le format du matériel

	var $etat_arch; //Libellé état archivage
	var $taille; //File size compréhensible (ex : "15.2 Mo")
	var $user_crea,$user_modif; //nom des usager crea et modif
	var $arrChildren=array(); // Tableau des matériels fils

    private $arrDateFields=array("MAT_DATE_FICHIER", "MAT_DATE_COPIE","MAT_DATE_RETOUR","MAT_DATE_ETAT");

	function __construct($id=null, $version=""){
		$this->id_lang=$_SESSION['langue']; // par défaut
		parent::__construct('t_mat',$id, $version); 
		$this->t_mat_track=array();
	}

	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {
 		//VG 02/07/2010 : plutôt que d'utiliser init pour initialiser les colonnes présentes en BDD, on n'initialise ici QUE les champs envoyés ET présents en base.
	 	//if (empty($this->t_mat)) $this->init();
		global $db;
		$cols=$db->MetaColumns('t_mat');

	 	foreach ($tab_valeurs as $fld=>$val) {
			// VP 30/06/09 : remplacement isset par array_key_exists à cause des valeurs nulles
			//if (isset($this->t_mat[strtoupper($fld)])) $this->t_mat[strtoupper($fld)]=stripControlCharacters($val);
			//if (array_key_exists(strtoupper($fld),$this->t_mat)) $this->t_mat[strtoupper($fld)]=stripControlCharacters($val);
			if (array_key_exists(strtoupper($fld),$cols) && strtoupper($fld) =='MAT_NOM') $this->t_mat[strtoupper($fld)]=removeTrickyChars(stripAccents(stripControlCharacters($val)));
			else if (array_key_exists(strtoupper($fld),$cols)) $this->t_mat[strtoupper($fld)]=stripControlCharacters($val);
		}
	}

	/**
	 * Récupère les infos sur un ou plusieurs matériels.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets mat, sinon màj de l'objet en cours
	 */
	function getMat($arrMats=array(), $version="",$getFileInfo=true ) {
		return $this->getData($arrMats,$version,$getFileInfo);
	}
	function getData($arrData=array(), $version="", $getFileInfo = true) {
		global $db;
		// Si arrMats est rempli et correspond a plusieurs docs, getdata renverra le tableau des objets en question,
		// sinon $this->t_mat sera alimenté mais arrMats sera vide
		$arrMats = parent::getData($arrData,$version);
		
		// VP 11/6/09 : suppression CacheGetOne qui ne fonctionne pas en CLI apparemment
		if (empty($arrMats) && !empty($this->t_mat) && !$arrData) {// Un seul résultat : on affecte le résultat à la valeur en cours
			$this->user_crea=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_mat['MAT_ID_USAGER_CREA']));
			$this->user_modif=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_mat['MAT_ID_USAGER_MOD']));
			if($getFileInfo) {
				$this->hasfile=$this->checkFileExists();
                $this->format_online=$this->isFormatOnline();
				$this->getMediaType();
			}
			//PC 02/10/12 : ajout libellé etat archivage
			$this->etat_arch=GetRefValue("t_etat_arch",$this->t_mat["MAT_ID_ETAT_ARCH"],$_SESSION['langue']);
			$this->taille=convertFileSize($this->t_mat["MAT_TAILLE"]);
			$this->debit=filesize_format($this->t_mat["MAT_BIT_RATE"],'.',' ',false,'bit').'ps';
			$this->t_mat["MAT_DUREE_FORMAT_SEC"]=timeToSec($this->t_mat["MAT_DUREE"]);
		}
		else {
			foreach ($arrMats as $idx=>$Mat) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				if($getFileInfo) {
					$arrMats[$idx]->hasfile=$this->checkFileExists();
					$arrMats[$idx]->format_online=$arrMats[$idx]->isFormatOnline();
					$arrMats[$idx]->getMediaType();
				}
				$arrMats[$idx]->user_crea=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($arrMats[$idx]->t_mat['MAT_ID_USAGER_CREA']));
				$arrMats[$idx]->user_modif=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($arrMats[$idx]->t_mat['MAT_ID_USAGER_MOD']));

				//PC 02/10/12 : ajout libellé etat archivage
				$arrMats[$idx]->etat_arch=GetRefValue("t_etat_arch",$arrMats[$idx]->t_mat["MAT_ID_ETAT_ARCH"],$_SESSION['langue']);
				$arrMats[$idx]->taille=convertFileSize($arrMats[$idx]->t_mat["MAT_TAILLE"]);
				$arrMats[$idx]->debit=filesize_format($arrMats[$idx]->t_mat["MAT_BIT_RATE"],'.',' ',false,'bit').'ps';
			}
			unset($this); // plus besoin
			return $arrMats;
		}
	}

	function getVignette(){
	global $db;
		$vignetteChemin = $db->GetRow('SELECT IM_CHEMIN,IM_FICHIER FROM t_image INNER JOIN t_mat ON ID_IMAGEUR=MAT_ID_IMAGEUR WHERE ID_MAT='.intval($this->t_mat['ID_MAT']).' order by IM_TC limit 1 OFFSET 0');
		$relativePathFromMedia=str_replace(kCheminHttpMedia,'',storyboardChemin);
		if ($vignetteChemin) $this->vignette= $relativePathFromMedia.$vignetteChemin['IM_CHEMIN']."/".$vignetteChemin['IM_FICHIER'];

	}


	function getFileSize()
	{
		$this->t_mat['MAT_TAILLE']=0;

		if (!is_dir($this->getFilePath()))
		{
			$taille_fichier=getFileSize($this->getFilePath());
			$this->t_mat['MAT_TAILLE']=$taille_fichier;

			return $taille_fichier;
		}

		return 0;
	}


	/** Récupère le type de media d'un format depuis la table t_format_mat
	 * IN : objet materiel (et plus particulière t_mat['MAT_FORMAT'])
	 * OUT : maj propriété id_media (A/V)
	 */

	function getMediaType() {
		global $db;
		if($this->hasfile){
            // detection du type MIME
            if (!isset($this->t_mat['MAT_TYPE_MIME']) || empty($this->t_mat['MAT_TYPE_MIME']))
                $this->t_mat['MAT_TYPE_MIME']=MatInfo::getTypeMimeFromFile($this->getFilePath());


            //date de derniere modification du fichier
            if (!isset($this->t_mat['MAT_DATE_FICHIER']) || empty($this->t_mat['MAT_DATE_FICHIER']))
                $this->t_mat['MAT_DATE_FICHIER']=MatInfo::getDateTimeFromFile($this->getFilePath());

            //taille du fichier
            if (!isset($this->t_mat['MAT_TAILLE']) || empty($this->t_mat['MAT_TAILLE']))
                $this->getFileSize();
		}
		// VP 26/6/09 : suppression CacheGetOne qui ne fonctionne pas en CLI apparemment
		//Par défaut on se base sur le format...
		//if (!empty($this->t_mat['MAT_FORMAT'])) $this->id_media=$db->CacheGetOne(600,"SELECT FORMAT_ID_MEDIA from t_format_mat WHERE FORMAT_MAT=".$db->Quote($this->t_mat['MAT_FORMAT']));
		if( !empty( $this->t_mat['MAT_ID_MEDIA'] ) ) {
			$this->id_media = $this->t_mat['MAT_ID_MEDIA'];
			
		} elseif (is_dir($this->getFilePath())) {
			$this->id_media='R';
		}
		else
		{
			$type_mime=explode('/',$this->t_mat['MAT_TYPE_MIME']);
			$type=$type_mime[0];
			//$sous_type=$type_mime[1];


			if ($type=='video')
				$this->id_media='V';
			else if ($type=='audio')
				$this->id_media='A';
			else if ($type=='image')
				$this->id_media='P';
			else
			{
				if (!empty($this->t_mat['MAT_FORMAT'])) $this->id_media=$db->GetOne("SELECT FORMAT_ID_MEDIA from t_format_mat WHERE FORMAT_MAT=".$db->Quote($this->t_mat['MAT_FORMAT']));

				// mais si le format manque (info non renseignée, échec infomedia, etc.) on se base sur l'extension
				if (empty($this->id_media)) {
					$ext=getExtension($this->t_mat['MAT_NOM']);
					// par defaut le media est de type data.
					$this->id_media='X';

					if(gVideoTypes) {
						if (in_array($ext,unserialize(gVideoTypes))) $this->id_media='V';
					}
					if(gAudioTypes){
						if (in_array($ext,unserialize(gAudioTypes))) $this->id_media='A';
					}
					if(gImageTypes){
						if (in_array($ext,unserialize(gImageTypes))) $this->id_media='P'; //photo
					}
					if(defined('gDocumentTypes') && gDocumentTypes){
						if (in_array(strtolower($ext),array_change_key_case(unserialize(gDocumentTypes)))) $this->id_media='D'; //bureautique
					}
				}
			}
		}
	}

	
	function isFormatOnline(){
		global $db ; 
		if(!empty($this->t_mat['MAT_FORMAT'])){
			$arr_formats = getFormatsMatVisu ();
			if($arr_formats && in_array($this->t_mat['MAT_FORMAT'],$arr_formats)){
				return 1;
			}else{
				return 0;
			}
		}
		
	}
	
	/**
	 * Vérification si le fichier correspondant à l'ID_MAT existe
	 * physiquement sur le disque
	 */
	function checkFileExists($file="") {
		if (empty($file))
			$file=$this->getFilePath();
		else
		{
			$file=dirname($this->getFilePath()).'/'.$file;
//			$lieu=$this->setLieuByIdMat($file);
//			$file=$lieu['LIEU_PATH'].$file;
		}

		if (!empty($file) && file_exists($file))
			return true;
		else
			return false;
	}


	static function checkFileExistsById($id) {
		if (intval($id) > 0)  {
			$myMat = new Materiel;
			$myMat->t_mat["ID_MAT"] = $id;
			$myMat->getMat();
			return $myMat->checkFileExists();
		}
	}

	/**
	 * Vérification si un fichier correspondant àâ€  l'ID_MAT principal (sans extension) existe
	 * physiquement sur le disque
	 */
	function checkOneFileExists($file="") {

		if (empty($file))
			$file=$this->getFilePath();
		else
		{
			// cf checkFileExists
			$file=dirname($this->getFilePath()).'/'.$file;
			// $lieu=$this->setLieuByIdMat($file);
			// $file=$lieu['LIEU_PATH'].$file;
		}
		$array_types_docs=array();

		if (defined(gDocumentTypes))
			$array_types_docs=unserialize(gDocumentTypes);

		$arrFormats = array_merge(unserialize(gAudioTypes),unserialize(gVideoTypes),unserialize(gImageTypes),$array_types_docs);
		foreach ($arrFormats as $format) {
			if (!empty($file) && file_exists(stripExtension($file).".".$format)) {
				$rtn = true;
			} elseif(!empty($file) && is_dir($file)) {
				$rtn = true;
			} else {
				$rtn = false;
			}
		}

		return $rtn;
	}

	/**	Vérifie si le fichier est un média par analyse de l'extension
	 * 	IN : id_mat de l'objet
	 * 	OUT : true/false
	 */
	function checkIfIsFile() {
		//Concaténation des formats audio et video
		$arrFormats = array_merge(unserialize(gAudioTypes),unserialize(gVideoTypes),unserialize(gImageTypes),unserialize(gDocumentTypes));
		if (defined('gSubtitleTypes')) {
			$arrFormats = array_merge($arrFormats, unserialize(gSubtitleTypes));
		}
		$filelist=implode('|', $arrFormats);

		if (preg_match('/\\.('.$filelist.')$/i', $this->t_mat['MAT_NOM']))
			return true;
		else
			return false;
	}


	/**
	 * Ajoute un message d'erreur pour la sortie
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function dropError($errLib) { //: A ETOFFER EN TRY CATCH ?
		$this->error_msg.=$errLib."<br/>";
	}


	/**
	 * Sauve les informations d'un lien MATERIEL / VALEUR
	 * IN : tableau MAT_VAL
	 * OUT : base mise à jour
	 */
	// VP 16/9/09 : purge optionnelle
	// VP 19/10/09 : ajout purge par ID_TYPE_VAL (tableau ou chaine)
	function saveMatVal($purge=true,$id_type_val=null) {
		global $db;
		$db->StartTrans();
		if ($purge) {
			if(!empty($id_type_val)){
				if(is_array($id_type_val)) $sqltypeval=" and ID_TYPE_VAL in ('".str_replace("/","','",implode("/",$id_type_val))."')";
				else $sqltypeval=" and ID_TYPE_VAL=".$db->Quote($id_type_val);
			}
			$db->Execute('DELETE FROM t_mat_val WHERE ID_MAT='.intval($this->t_mat['ID_MAT']).$sqltypeval);
		}

        if(isset($this->t_mat_val) and !empty($this->t_mat_val)) {
			foreach ($this->t_mat_val as $idx=>$matVal) {

                if (!empty($matVal['VALEUR'])){
                    $myVal=new Valeur();
                    $matVal['VAL_ID_TYPE_VAL']=$matVal['ID_TYPE_VAL'];
                    $matVal['ID_LANG']=$_SESSION['langue'];
                    $myVal->createFromArray($matVal, true, false, true);
                    $matVal['ID_VAL']=$myVal->t_val['ID_VAL'];
                    unset($myVal);
                }
				if (!empty($matVal['ID_VAL'])) {

					//Précaution en cas de duplication : on spécifie bien que l'ID_PERS est celui en cours.
					// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_VAL originel dedans.
					$matVal['ID_MAT']=$this->t_mat['ID_MAT'];
					if ($matVal['VALEUR']) unset($matVal['VALEUR']); //On ôte l'objet encapsulé s'il existe

					if (!$db->getOne("SELECT * FROM t_mat_val WHERE ID_MAT=".intval($matVal['ID_MAT'])." AND ID_VAL=".intval($matVal['ID_VAL'])." AND ID_TYPE_VAL='".$matVal['ID_TYPE_VAL']."'")) { // NB 13 01 2016 - on ajoute ID_TYPE_VAL dans la req pour les ID_VAL identiques sur des ID_TYPE_VAL différents (exemple cfrt AUD->AUD1,AUD2...)
						$ok = $db->insertBase("t_mat_val","id_mat",$matVal);
						if (!$ok) trace($db->ErrorMsg());
					}
				}
			}
        }
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveMatVal);trace($sql); return false;} else return true;
	}

	/**
	 * Sauve les informations d'un lien MATERIEL / PERSONNE
	 * IN : tableau t_mat_pers
	 * OUT : base mise à jour
	 */
	function saveMatPers($purge=true,$id_type_desc=null) {
		global $db;
		require_once(modelDir.'model_personne.php');
		$db->StartTrans();
		if ($purge) {
			if(!empty($id_type_desc)){
				if(is_array($id_type_desc)) $sqltypedesc=" and ID_TYPE_DESC in ('".str_replace("/","','",implode("/",$id_type_desc))."')";
				else $sqltypedesc=" and ID_TYPE_DESC=".$db->Quote($id_type_desc);
			}
			$db->Execute('DELETE FROM t_mat_pers WHERE ID_MAT='.intval($this->t_mat['ID_MAT']).$sqltypedesc);
		}
		
        if(isset($this->t_mat_pers) and !empty($this->t_mat_pers)) {
            foreach ($this->t_mat_pers as $idx=>$matPers) {
				/**
				 * @author B.RAVI 22/03/17 12:35
				 * sur mémorial on a besoin de crée une t_personne(issu des données du webservice) sur l'édition d'un matériel
				 */
				if (!empty($matPers['PERS_NOM']) && (!isset($matPers['ID_PERS']) || empty($matPers['ID_PERS']))) {
					$myPers = new Personne();
					$myPers->t_personne['PERS_NOM'] = trim($matPers['PERS_NOM']);
					$myPers->t_personne['PERS_PRENOM'] = trim($matPers['PERS_PRENOM']); //@update VG 29/07/2010
					$myPers->t_personne['PERS_ID_TYPE_LEX'] = (empty($matPers['ID_TYPE_LEX']) ? $matPers['PERS_ID_TYPE_LEX'] : $matPers['ID_TYPE_LEX']);
					$pers_code = (int) $matPers['PERS_CODE'];
					if (!empty($pers_code)) {
						$myPers->t_personne['PERS_CODE'] = $pers_code;
					}
					$myPers->t_personne['ID_LANG'] = $this->id_lang;
					$exist = $myPers->checkExist(false);


					if (!$exist) {
						$myPers->save();
						$myPers->saveVersions();
					} else
						$myPers->t_personne['ID_PERS'] = $exist;
					$matPers['ID_PERS'] = $myPers->t_personne['ID_PERS'];
					unset($myPers);
				}


				if (!empty($matPers['ID_PERS'])) {
                    $matPers['ID_MAT']=$this->t_mat['ID_MAT'];
                    if (!$db->getOne("SELECT * FROM t_mat_pers WHERE ID_MAT=".intval($matPers['ID_MAT'])."
                                     AND ID_TYPE_DESC=".$db->Quote($matPers['ID_TYPE_DESC'])."
                                     AND DLEX_ID_ROLE=".$db->Quote($matPers['DLEX_ID_ROLE'])."
                                     AND ID_PERS=".intval($matPers['ID_PERS']))) {
                        $ok = $db->insertBase("t_mat_pers","id_mat",$matPers);
                        if (!$ok) trace($db->ErrorMsg());
                    }
                }
            }
            if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveMatPers);trace($sql); return false;} else {return true;}
        //B.RAVI 2015-11-06 Pour éviter d'afficher message kErrorSauveMatPers si rien à insérer dans t_mat_pers
        }else{
            $db->CompleteTrans();
            return false;
        }     
                
                
	}

	/**
	 * Vérifie existance lien doc_mat
	 * IN : tableau doc_mat
	 * OUT : identifiant ID_DOC_MAT
	 */
	function checkDocMatExists($dm) {

		global $db;
		$sql = 'SELECT ID_DOC_MAT FROM t_doc_mat WHERE ID_DOC='.intval($dm['ID_DOC']).'
		AND ID_MAT='.intval($dm['ID_MAT']);
		if(!defined("gCheckDocMatWithoutTc") || !gCheckDocMatWithoutTc ){
			if(!empty($dm['DMAT_TCIN'])) {
				$sql .= " AND DMAT_TCIN = ".$db->Quote($dm['DMAT_TCIN']);
			}
			if(!empty($dm['DMAT_TCOUT'])) {
				$sql .= " AND DMAT_TCOUT = ".$db->Quote($dm['DMAT_TCOUT']);
			}
		}

		debug($sql, "gold", true);
		return $db->GetOne($sql);
	}

	/**
	 * Sauve les associations DOCUMENT / MATERIEL ainsi que les VALEURS associées à cette association + les DOCS
	 * IN : tableau de classe t_doc_mat (et son sous tableau t_doc_mat_val)
	 * OUT : base mise à jour
	 */
	function saveDocMat() {

		debug("saveDocMat", "blue", true);
		require_once(libDir."class_visualisation.php");
		require_once(modelDir.'model_doc.php');
		global $db;
		if (empty($this->t_doc_mat)) return;
		$db->StartTrans();
		//		$db->Execute('DELETE FROM t_doc_mat_val WHERE ID_DOC_MAT in (SELECT ID_DOC_MAT from t_doc_mat WHERE ID_MAT='.$db->Quote($this->id_mat_ref).' )');
		//		$db->Execute('DELETE FROM t_doc_mat WHERE ID_MAT='.$db->Quote($this->id_mat_ref));
		foreach ($this->t_doc_mat as $idx=>&$docMat) {

			//Précaution en cas de duplication : on spécifie bien que l'ID_MAT est celui en cours.
			// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_MAT originel dedans.
			$docMat['ID_MAT']=$this->t_mat['ID_MAT'];


			$dmVal=$docMat['t_doc_mat_val'];unset($docMat['t_doc_mat_val']);
			$dmDoc=$docMat['t_doc'];unset($docMat['t_doc']);
			if (empty($dmDoc['ID_DOC'])) $dmDoc['ID_DOC']=$docMat['ID_DOC']; //Si ID_DOC de t_doc vide, on récupère celui de t_doc_mat

			if (!empty($dmDoc)) { //Il y a des champs qui concernent le document proprement dit
				$newDoc=new Doc();
				$newDoc->updateFromArray($dmDoc,false);

				//debug($newDoc->t_doc,'gold',true);
				$newDoc->doc_trad=1; //Pour le moment, on sauve tjs les versions, à faire évoluer
				$newDoc->save();

				if (empty($docMat['ID_DOC'])) { //ID_DOC vide ? C'était un nouveau document
					include_once(modelDir.'model_imageur.php'); //donc on affecte une image par défaut selon les TC saisis

					$myImg=new Imageur();
					$myImg->getImageur(array(0=>$this->t_mat['MAT_ID_IMAGEUR']),'',array(0=>array('TCIN'=>$docMat['DMAT_TCIN'],'TCOUT'=>$docMat['DMAT_TCOUT'])));
					if ($myImg->t_images[0]->t_image['ID_IMAGE']) $newDoc->saveVignette('DOC_ID_IMAGE',$myImg->t_images[0]->t_image['ID_IMAGE']);
					unset($myImg); //pas la peine de garder cet objet
					$docMat['ID_DOC']=$newDoc->t_doc['ID_DOC']; //Maj ID_DOC s'il était vide
				}
			}

			if (!empty($docMat['ID_DOC'])) {
                // VP 7/03/2013 : checkDocMatExists si ID_DOC_MAT vide
				if(empty($docMat['ID_DOC_MAT']))
                    $docMat['ID_DOC_MAT']=$this->checkDocMatExists($docMat); //récup id_doc_mat si ligne existe déjà. Evite les doublons doc_mat

				if (empty($docMat['ID_DOC_MAT'])) { // INSERTION
					// VP 16/3/09 : ajout date et user création par défaut
					$docMat['DMAT_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
					$docMat['DMAT_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];

					/*$sql="INSERT INTO t_doc_mat (";
					$sql.=implode(",",array_keys($docMat));
					$sql.=") values ('";
					$tmp=implode("ÃŸ",array_values($docMat));
					$sql.=str_replace("ÃŸ","','",simpleQuote($tmp));
					$sql.="')";

					$ok=$db->Execute($sql);*/
					if (!$docMat['ID_DOC_MAT']) unset($docMat['ID_DOC_MAT']);
					$id_doc_mat = $db->insertBase("t_doc_mat","id_doc_mat",$docMat);
					if($id_doc_mat){
						$docMat['ID_DOC_MAT']=$id_doc_mat; //maj objet
					}
				} else { // UPDATE
					// VP 16/3/09 : ajout date et user modif par défaut
					$docMat['DMAT_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
					$docMat['DMAT_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
					$rs = $db->Execute("SELECT * FROM t_doc_mat WHERE ID_DOC_MAT=".intval($docMat['ID_DOC_MAT']));
					$sql = $db->GetUpdateSQL($rs, $docMat);
					if (!empty($sql)) $ok = $db->Execute($sql);
					$ok=$db->Execute($sql);
				}
				if (!empty($dmVal)) {
					$db->Execute('DELETE from t_doc_mat_val WHERE ID_DOC_MAT='.intval($docMat['ID_DOC_MAT']));
					foreach ($dmVal as $val) {
						$val['ID_DOC_MAT'] = $docMat['ID_DOC_MAT'];
						 $db->insertBase("t_doc_mat_val","id_doc_mat",$val);
					}
				}

				if (in_array($this->t_mat['MAT_FORMAT'],Visualisation::getFormatsForVisu()) && $this->hasfile) {
					//Le media contient un fichier de format visionnable et le fichier existe ?
					// Alors on met à jour le DOC_NUM du document.
					$db->Execute("UPDATE t_doc SET DOC_NUM='1' WHERE ID_DOC=".intval($docMat['ID_DOC']));
				}

				if (defined("gAlwaysUpdateDureeWithDocMat") && gAlwaysUpdateDureeWithDocMat==true) {
					//Update DOC_DUREE du document à partir des DMAT tcin et tcout 16/5/08 ld
					// VP 16/12/09 : ajout condition DOC_DUREE='00:00:00'
					if (!empty($docMat['DMAT_TCIN']) && !empty($docMat['DMAT_TCOUT'])) {
						$frames=(tcToFrame($docMat['DMAT_TCOUT'])-tcToFrame($docMat['DMAT_TCIN']));
						if ($frames !== 0 ) {
							if (defined("gUpdateDureeWithLastDocMat") && gUpdateDureeWithLastDocMat==true){ 
								$db->Execute("UPDATE t_doc SET DOC_DUREE=".$db->Quote(frameToTime($frames))." WHERE ID_DOC=".intval($docMat['ID_DOC']));
							}
							else{
								$db->Execute("UPDATE t_doc SET DOC_DUREE=".$db->Quote(frameToTime($frames))." WHERE (DOC_DUREE='00:00:00' or DOC_DUREE is null) and ID_DOC=".intval($docMat['ID_DOC']));
							}
						}
					}
				}

			}
		}

		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocMat); return false;} else return true;
	}

	/**
	 * Supression des liens DOC_MAT (+ t_doc_mat_val)
	 * IN : tab de classe t_doc_mat
	 * OUT : bdd maj et tableau t_doc_mat vidé
	 * NOTE : supprime juste le lien DOC_MAT, PAS le document
	 */
	function deleteDocMat() {
		global $db;
		foreach ($this->t_doc_mat as $dm) {
	        deleteSQLFromArray(array("t_doc_mat","t_doc_mat_val"),"ID_DOC_MAT",$dm['ID_DOC_MAT']);
		}
		unset ($this->t_doc_mat);
	}


	/**
	 * Récupère les valeurs liées à un matériel
	 * IN : ID_MAT
	 * OUT : tableau t_mat_val (classe)
	 */
	function getValeurs() {
		global $db;
		// VP 9/07/09 : ajout critère ID_TYPE_VAL
		$sql = "SELECT t_val.*,t_mat_val.ID_TYPE_VAL FROM t_mat_val
				INNER JOIN t_val ON t_mat_val.ID_VAL=t_val.ID_VAL
				where t_mat_val.ID_MAT=".intval($this->t_mat['ID_MAT'])." AND t_val.ID_LANG=".$db->Quote($this->id_lang);
		$this->t_mat_val=$db->GetAll($sql);
	}


	/**
	 *  Pour l'instant pas d'objet doc_mat, donc on construit juste un tableau
	 */
	function getDocMat() {
		global $db;
		$sql="SELECT distinct * FROM t_doc_mat WHERE id_mat=".intval($this->t_mat['ID_MAT'])." ORDER BY DMAT_TCIN asc";
		$this->t_doc_mat=$db->GetAll($sql);
	}


	/** Dans le tableau t_doc_mat, ajout des objets DOC
	 * IN : t_doc_mat (chargé si vide)
	 * OUT : t_doc_mat avec un objet DOC pour chaque 'ligne'
	 *
	 */

	function getDocs($withVals=false) {
		require_once(modelDir.'model_doc.php');
		global $db;
		if (empty($this->t_doc_mat)) $this->getDocMat();

		foreach ($this->t_doc_mat as $idx=>&$dm) {

			$sql="SELECT * from t_doc_mat_val WHERE ID_DOC_MAT=".intval($dm['ID_DOC_MAT']);
			$dm['t_doc_mat_val']=$db->GetAll($sql);

			$doc=new Doc();
			$doc->t_doc['ID_DOC']=$dm['ID_DOC'];
			$doc->t_doc['ID_LANG']=$this->id_lang;
			$doc->getDoc();
			if ($withVals) $doc->getValeurs();
			$dm['DOC']=$doc;
		}
		unset($doc); // libération mémoire
	}

	/**
	 * OBSOLETE MAIS GARDE TANT QUE TOUS LES SITES PAS EN V2 AVEC VISIO
	 */
	function getMaterielVisu() {
		global $db;
		// VP 29/11/10 : correction majuscule minuscule dans requête sql
		$sql="SELECT T1.ID_MAT FROM t_mat T1 LEFT OUTER JOIN t_format_mat T2
		ON T1.MAT_FORMAT=T2.FORMAT_MAT WHERE T2.FORMAT_ONLINE='1'
		AND T1.ID_MAT=".intval($this->t_mat['ID_MAT']);
	    return $db->GetOne($sql);
	}


	/**
	 * Récupère les sauvegardes du matériel
	 * IN : ID_MAT
	 * OUT : tableau t_tape_file (classe)
	 // VP 22/1/09 : création
	 */
	function getTapeFile() {
		global $db;
        // VP 7/0/2016 : ajout TAPE_STATUS
		//$sql="SELECT distinct t_tape_file.*, t_tape.TAPE_STATUS FROM t_tape_file inner join t_tape on t_tape.ID_TAPE = t_tape_file.ID_TAPE WHERE TF_MEDIA='videos' and TF_ID_MAT=".intval($this->t_mat['ID_MAT'])." ORDER BY TF_DATE_COPIE asc";
        $sql="SELECT distinct tf.*,t.TAPE_STATUS FROM t_tape_file tf, t_tape t WHERE TF_MEDIA='videos' and TF_ID_MAT=".intval($this->t_mat['ID_MAT'])." and tf.ID_TAPE=t.ID_TAPE ORDER BY TF_DATE_COPIE asc";
		$this->t_tape_file=$db->GetAll($sql);
	}

	public function getMatTrack()
	{
		$this->t_mat_track=MatTrack::getAllMatTrack($this->t_mat['ID_MAT']);
		return $this->t_mat_track;
	}

	public function saveMatTrack($mat_info=null)
	{
		if (!empty($this->t_mat_track))
		{
			foreach ($this->t_mat_track as $track)
			{
				$track->delete();
			}
		}

		$this->t_mat_track=array();

		if ($mat_info!=null)
			$tracks=$mat_info;
		else
			$tracks=MatInfo::getMatInfo($this->getLieu($server).$this->t_mat['MAT_NOM']);

		//var_dump($tracks);

		foreach($tracks['tracks'] as $track)
		{
			$tmp_track=new MatTrack();
			$tmp_track->set('ID_MAT',$this->t_mat['ID_MAT']);

			$tmp_track->set('MT_INDEX',$track['index']);
			$tmp_track->set('MT_TYPE',$track['type']);
			$tmp_track->set('MT_FORMAT',$track['format']);
			$tmp_track->set('MT_FORMAT_LONG',$track['format_long']);
			$tmp_track->set('MT_FORMAT_PROFILE',$track['format_profile']);
			$tmp_track->set('MT_FORMAT_GOP',$track['format_settings_gop']);
			$tmp_track->set('MT_FORMAT_WRAPPING',$track['format_setings_wrapping']);
			$tmp_track->set('MT_FORMAT_VERSION',$track['format_version']);
			$tmp_track->set('MT_CODEC_ID',$track['codec_id']);
			if(isset($track['codec_long'])){
				$tmp_track->set('MT_CODEC_LONG',$track['codec_long']);
			}
			$tmp_track->set('MT_DURATION',$track['duration']);
			$tmp_track->set('MT_START_TIME',$track['start_time']);
			$tmp_track->set('MT_HEIGHT',$track['height']);
			$tmp_track->set('MT_WIDTH',$track['width']);
			$tmp_track->set('MT_DISPLAY_HEIGHT',$track['display_height']);
			$tmp_track->set('MT_DISPLAY_WIDTH',$track['display_width']);
			$tmp_track->set('MT_DAR',$track['display_aspect_ratio']);
			//$tmp_track->set('MT_SAR',);
			$tmp_track->set('MT_BIT_RATE',$track['bit_rate']);
			$tmp_track->set('MT_PIX_FMT',$track['pix_fmt']);
			$tmp_track->set('MT_FRAME_RATE',$track['frame_rate']);
			$tmp_track->set('MT_BIT_DEPTH',$track['bit_depth']);
			$tmp_track->set('MT_LANGUAGE',$track['language']);
			$tmp_track->set('MT_TIMECODE',$track['timecode']);
			$tmp_track->set('MT_CHANNELS',$track['channels']);
			$tmp_track->set('MT_SAMPLE_RATE',$track['sample_rate']);
			$tmp_track->set('MT_SCAN_TYPE',$track['scan_type']);
			$tmp_track->set('MT_SCAN_ORDER',$track['scan_order']);

			$tmp_track->save();

		}
	}


	/**
	 * Récupération des personnes liées au document via la table t_mat_pers
	 * IN : id_mat (var de classe)
	 * OUT : tableau t_mat_pers
	 */
	function getPersonnes() {
        global $db;
        $sql = "SELECT t_mat_pers.*, t_personne.PERS_ID_TYPE_LEX, t_personne.PERS_NOM,t_personne.PERS_PRENOM, t_personne.PERS_PARTICULE, t_doc_acc.DA_CHEMIN, t_doc_acc.DA_FICHIER, t_role.ROLE
         FROM t_mat_pers LEFT OUTER JOIN t_personne ON t_mat_pers.ID_PERS=t_personne.ID_PERS 
         LEFT OUTER JOIN t_doc_acc ON (t_personne.PERS_PHOTO=t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG=".$db->Quote($this->id_lang).") 
         LEFT OUTER JOIN t_role ON (t_mat_pers.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($this->id_lang).") 
         WHERE t_mat_pers.ID_MAT=".intval($this->t_mat['ID_MAT'])." AND t_personne.ID_LANG=".$db->Quote($this->id_lang)."
         ORDER BY t_mat_pers.ID_MAT_PERS";
        
        $this->t_mat_pers=$db->GetAll($sql);
	}

	/**
	 * Vérification existance basée sur mat_chemin et mat_nom
	 * IN : id_mat (var de classe)
	 * OUT : id_mat|null
	 */
	function checkExist($troncature=false) {
		global $db;
		// recherche sans prendre en compte l'extension du fichier
		// VP 29/03/11 : recherche par troncature optionnelle
		//return $idMat; //false si n'existe pas
		// MS 19/09/13 : Ajout de la comparaison sur le MAT_LIEU en plus du MAT_CHEMIN.

		if (isset($this->t_mat['ID_MAT']) && !empty($this->t_mat['ID_MAT']))
		{
			$compte=$db->GetOne('SELECT COUNT(*) AS compte FROM t_mat WHERE ID_MAT='.intval($this->t_mat['ID_MAT']));

			if ($compte==1)
				return $this->t_mat['ID_MAT'];
		}
		else
		{
            $sql_chemin=(isset($this->t_mat['MAT_CHEMIN'])?" AND MAT_CHEMIN=".$db->Quote($this->t_mat['MAT_CHEMIN']):"");
            $sql_lieu=(isset($this->t_mat['MAT_LIEU'])?" AND MAT_LIEU=".$db->Quote($this->t_mat['MAT_LIEU']):"");
			if ($troncature)
				$id_mat=$db->GetOne("SELECT ID_MAT from t_mat WHERE MAT_NOM LIKE ".$db->Quote(stripExtension($this->t_mat['MAT_NOM']).".%").$sql_chemin.$sql_lieu);
			else
				$id_mat=$db->GetOne("SELECT ID_MAT from t_mat WHERE MAT_NOM=".$db->Quote($this->t_mat['MAT_NOM']).$sql_chemin.$sql_lieu);

			return $id_mat;
		}

		return null;
	}

	/**
	 * Sélecteur d'entités à sauvegarder
	 * IN : tableau des entités à sauver
	 * OUT : sauvegardes
	 * NOTE : utilisé pour les modifs par lots
	 */
	function multiSave($arr2save=array()) {
		$this->save();
		if ($arr2save['VAL']) $this->saveMatVal();
		if ($arr2save['LEX']) $this->saveMatPers();
		if ($arr2save['MAT']) $this->saveDocMat();
		return true;
	}

	/**
	 * Sauve en base un matériel (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save($failIfExists=false) {
		global $db;

		if (empty($this->t_mat['MAT_NOM']))
		{
			$this->dropError(kErrorMaterielVide);
			return false;
		}

        if (empty($this->t_mat['ID_MAT'])) {
			return $this->create($failIfExists); //redirection vers la creation, retour = create au lieu de true
		} elseif(empty($this->id_mat_ref)) {
			$this->dropError(kErrorMaterielSauve);
			return false;
		}

		if ($this->id_mat_ref!=$this->t_mat['MAT_NOM'] &&  $this->checkFileExists($this->t_mat['MAT_NOM']))
		{
			$this->dropError(kErrorMaterielExisteDeja);
			return false;
		}

		$this->t_mat['MAT_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_mat['MAT_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		unset($this->t_mat['MAT_DATE_CREA']);
		unset($this->t_mat['MAT_ID_USAGER_CREA']);
		$this->convDateFields();

		$rs = $db->Execute("SELECT * FROM t_mat WHERE ID_MAT=".intval($this->t_mat['ID_MAT']));
		$sql = $db->GetUpdateSQL($rs, $this->t_mat);
		if (!empty($sql)) $ok = $db->Execute($sql);

		if (!empty($sql) && !$ok)
		{
			$this->dropError(kErrorMaterielSauve);
			trace($sql);
			return false;
		}

		if ($this->t_mat['MAT_NOM']!=$this->id_mat_ref ) {
			if ($this->checkFileExists($this->id_mat_ref)) { //Matériel renommé ? On renomme le fichier !
				// VP 30/03/10 : suppression changement lieu du fichier en cas de renommage
				// VP 20/04/12 : changement calcul lieu
				//$lieu=$this->setLieuByIdMat($this->id_mat_ref);
				//$ok=mv_rename($lieu['LIEU_PATH'].$this->id_mat_ref,$this->setLieu().$this->t_mat['ID_MAT']);
				//$oldLieu=$this->getLieuByIdMat($this->id_mat_ref);
				$oldLieu=$this->getLieu();
				$ok=mv_rename($oldLieu.$this->id_mat_ref,$oldLieu.$this->t_mat['MAT_NOM']);
				if (!$ok)
				{
					$this->dropError(kErrorMaterielRenommageFichier);
					return false;
				}
				else
				{
					$this->hasfile=true;
					$this->dropError(kSuccesMaterielRenommageFichier);
				}

			}
		}

		$this->dropError(kSuccesMaterielSauve);
		return true;
	}

	/**
	 * Sauve l'association à un imageur
	 */
	function saveImageur($imageur) {
		global $db;
    	$this->t_mat['MAT_ID_IMAGEUR']=$imageur;
    	$this->id_mat_ref=$this->t_mat['MAT_NOM']; //Pour bien être en mode UPDATE matériel (et pas créa)
    	$this->save();
    	return $this->error_msg;
	}

	/**
	 * Crée un matériel en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function create ($failIfExists=false) {
		global $db;
		if (empty($this->t_mat['MAT_NOM'])) {$this->dropError(kErrorMaterielVide);return false;}
		$exist=$this->checkExist();
		if ($exist) { //mat existe, on redirige donc vers une sauvegarde std (sans renommage)
			if ($failIfExists) { //by ld 141108 cas de failifexists
				$this->dropError(kErrorMaterielExisteDeja);
				return false;
			} else {
				$this->t_mat['ID_MAT']=$exist;
				$this->id_mat_ref=$this->t_mat['MAT_NOM'];
				return $this->save();
			}
		} //by ld 01/04/08 retour mis this->save au lieu de false
		
		// MS - unset id_mat si vide pour éviter insertion avec ID_MAT=0
		if(isset($this->t_mat['ID_MAT']) && empty($this->t_mat['ID_MAT'])){
			unset($this->t_mat['ID_MAT']);
		}
		
		$this->t_mat['MAT_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];
		$this->t_mat['MAT_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		$this->t_mat['MAT_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_mat['MAT_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->convDateFields();

		$ok =  $db->insertBase("t_mat","id_mat",$this->t_mat);
		if (!$ok) {$this->dropError(kErrorMaterielCreation);return false;}
		$this->t_mat['ID_MAT'] = $ok;

		$this->dropError(kSuccesMaterielCreation);
		$this->hasfile=$this->checkFileExists(); // On regarde si le fichier n'existe pas déjà sur le disque
		// NOTE : cela peut être le cas si le fichier a déjà été transféré (via FTP ou copie) ou en cas de reprise de données

		return true;
	}
									 
									 
	 /**
	  * Duplique l'obget materiel en base
	  * IN : objet
	  * OUT : base màj
	  */

	function duplicateMat($tab_valeurs, $withDocMat=false){
		// Il faut avoir fait un $this->getMat() avant
		if (empty($this->t_mat_pers)) $this->getPersonnes();
		if (empty($this->t_mat_val)) $this->getValeurs();
		if (empty($this->t_doc_mat) && $withDocMat) $this->getDocMat();
		$dupMat=new Materiel();
		$dupMat= clone $this; // Recopie des infos
		$dupMat->t_mat['ID_MAT']='';
		$dupMat->id_mat_ref='';
		$dupMat->updateFromArray($tab_valeurs);
		$dupMat->save();
		$dupMat->saveMatVal();
		foreach ($dupMat->t_mat_pers as $idx=>&$matPers) {
			$matPers["ID_MAT"]=$dupMat->t_mat['ID_MAT'];
			$matPers["ID_MAT_PERS"]='';
		}
		$dupMat->saveMatPers();
		if($withDocMat) {
			foreach ($dupMat->t_doc_mat as $idx=>&$docMat) {
				$docMat["ID_MAT"]=$dupMat->t_mat['ID_MAT'];
				$docMat["ID_DOC_MAT"]='';
			}
			$dupMat->saveDocMat();
		}
		return $dupMat;
	}

	function convDateFields() {
		foreach ($this->t_mat as $fld=>&$val) {
			if (strpos($fld,'MAT_')===0 && in_array($fld,$this->arrDateFields))
				$val = convDateTime($val);
		}
	}



	/** Efface un matériel
	 *
	 */
	// VP 02/04/10 : ajout paramètre purge, par défaut à false
	// VG 10/06/2010 : mise à jour du flag doc_num des docs liés
    function delete($purge=false, $bypass_puttrash=false){
    	require_once(modelDir.'model_doc.php');
    	global $db;
    	if (empty($this->t_mat['ID_MAT'])) {$this->dropError(kErrorMaterielNoId);return false;}
    	if(empty($this->t_doc_mat)) $this->getDocMat();
		if(!$bypass_puttrash && defined("gUseTrash") && gUseTrash==true){
			$ok = $this->putTrash();
			if(!$ok) {
				$this->dropError(kErreurTrash);
				return false;
			}
		}
		
        // VP 26/11/123 : suppression t_mat_track
        deleteSQLFromArray(array("t_mat","t_doc_mat","t_mat_val","t_mat_pers","t_mat_track"),"ID_MAT", $this->t_mat['ID_MAT']);
		// VP 20/01/11 : appel méthode purge fichier
        if ($purge || (defined("gMaterielDeleteFiles") && gMaterielDeleteFiles==true)) $this->purge();
        //On met aussi à jour le flag doc_num des docs liés
        foreach ($this->t_doc_mat as $doc_mat) {
        	$doc = new Doc();
        	$doc->t_doc['ID_DOC'] = $doc_mat['ID_DOC'];
        	$doc->updateDocNum();
        	unset($doc);
        }
		
		return true;
    }


	// VP 20/01/11 : création purge fichier
    function purge(){
        $matFile=$this->getFilePath();
		trace("Purge ".$matFile);
        if(is_file($matFile)) unlink($matFile);
        elseif(is_dir($matFile)) {
            $this->getChildren();
            foreach($this->arrChildren as $child){
                $objChild=new Materiel();
                $objChild->updateFromArray($child);
                $objChild->purge();
                unset($objChild);
            }
            rmdir($matFile);
        }

		$this->hasfile=$this->checkFileExists();

		//PC 01/10/12 : mise à jour de l'état archivage
		$this->updateEtatArch();
	}

	//PC 01/10/12 : met à jour l'état d'archivage
	function updateEtatArch() {
		global $db;
		$new_etat = 5;

		if (empty($this->t_mat['ID_MAT'])) return false;

		if ($this->t_mat['MAT_TYPE_MIME'] == "directory") {
			$new_etat = $db->getOne("SELECT max(MAT_ID_ETAT_ARCH) FROM t_mat WHERE MAT_ID_GEN=".intval($this->t_mat['ID_MAT']));
			if (is_null($new_etat)) $new_etat = 0;
		}
		else {
			$is_present = file_exists($this->getFilePath());
			$res_tape = $db->getOne("SELECT max(t.TAPE_STATUS)
										FROM t_tape_file tf
										INNER JOIN t_tape t ON t.ID_TAPE = tf.ID_TAPE
										WHERE tf.TF_ID_MAT =".intval($this->t_mat['ID_MAT']));
			$is_saved = (is_null($res_tape) ? false : true);
			$is_in_lib = ($res_tape == "1" ? true : false);

			if ($is_present && $is_saved)
				$new_etat = 2;
			elseif ($is_present)
				$new_etat = 1;
			elseif ($is_saved && $is_in_lib)
				$new_etat = 3;
			elseif ($is_saved)
				$new_etat = 4;
			else
				$new_etat = 5;
		}
		$this->t_mat['MAT_ID_ETAT_ARCH'] = $new_etat;
		$sqlUpdate = "UPDATE t_mat SET MAT_ID_ETAT_ARCH = $new_etat WHERE ID_MAT=".intval($this->t_mat['ID_MAT']);
		$ok = $db->execute($sqlUpdate);

		//mise à jour doc
		if (empty($this->t_doc_mat) || !$this->t_doc_mat[0]['DOC'])
			$this->getDocs();
		foreach ($this->t_doc_mat as $dm){
			$dm['DOC']->updateEtatArch();
		}
	}

    //*** XML
	function xml_export($entete=0,$encodage=0,$indent=""){

        $content="";
		$prefix='';

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
        $content.=$indent."<t_mat id_mat=\"".str_replace($search,$replace,$this->t_mat['ID_MAT'])."\">\n";

		// D'abord l'objet

		foreach ($this->t_mat as $name => $value) {
			//$content .= $indent."\t<".strtoupper($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".strtoupper($name).">\n";
			// VP 27/8/09 : prise en compte contenu XML
			if(substr(strtoupper($value),0,5)=="<XML>") $content .= $prefix."\t<".strtoupper($name).">".stripControlCharacters(str_replace(array(" ' ","&"),array("'","&amp;"),$value))."</".strtoupper($name).">\n";
			elseif(strtoupper($name) == "MAT_TEXTE") {
				// VP 15/06/2011 : inhibé car ajoute des espaces lors de l'export
				//$valueReplaced = trim(str_replace($search,$replace, $value));
				//trace("MAT_TEXTE : ".print_r($valueReplaced,1));
				//$valueTest = eval("echo (string)' ".htmlentities($value, ENT_QUOTES)." ';");
				//                    	if(!empty($valueTest))
				//                    		$content .= $indent."\t<".strtoupper($name)."><![CDATA[".$valueReplaced."]]></".strtoupper($name).">\n";
				//                    	else{
				//                    		$content .= $indent."\t<".strtoupper($name)."> </".strtoupper($name).">\n";
				//                    	}
				$value=trim(stripControlCharacters($value));
				if(!empty($value)) $content .= $indent."\t<".strtoupper($name)."><![CDATA[ ".str_replace($search,$replace, $value)."]]></".strtoupper($name).">\n";
			}
			else $content .= $indent."\t<".strtoupper($name).">".str_replace($search,$replace, $value)."</".strtoupper($name).">\n";
		}

		$content .= "\t<US_CREA>".$this->user_crea."</US_CREA>\n";
		$content .="\t<US_MODIF>".$this->user_modif."</US_MODIF>\n";
		$content .="\t<LIEU_PATH>".$this->getLieu()."</LIEU_PATH>\n";
		$content .="\t<ETAT_ARCH>".$this->etat_arch."</ETAT_ARCH>\n";
		$content .="\t<TAILLE>".$this->taille."</TAILLE>\n";
		$content .="\t<DEBIT>".$this->debit."</DEBIT>\n";
        $content .="\t<HASFILE>".$this->hasfile."</HASFILE>\n";
        $content .="\t<FORMAT_ONLINE>".$this->format_online."</FORMAT_ONLINE>\n";
        
        if (!empty($this->t_mat_track)) {
            $content.="\t<tracks nb=\"".count($this->t_mat_track)."\">\n";
            foreach ($this->t_mat_track as $track) {
              if(is_object($track)) $content .=$track->xml_export();
            }
            $content.="\t</tracks>\n";
        }

		if (!empty($this->t_mat_val)) {
            // I.3. Propri?t?s relatives ? t_doc_val
            $content.="\t<t_mat_val nb=\"".count($this->t_mat_val)."\">\n";
			// Pour chaque type de valeur
			// VP 9/07/09 : changement VAL_ID_TYPE_VAL en ID_TYPE_VAL
			foreach ($this->t_mat_val as $name => $value) {
				$content .= "\t\t<TYPE_VAL ID_TYPE_VAL=\"".$value['ID_TYPE_VAL']."\" >";
				// Pour chaque valeur

				$content .= "\t\t<t_val ID_VAL=\"".$value['ID_VAL']."\">\n";
				$content .= "\t\t\t<ID_VAL>".$value['ID_VAL']."</ID_VAL>\n";
				$content .= "\t\t\t<VALEUR>".str_replace($search,$replace, $value['VALEUR'])."</VALEUR>\n";
				$content .= "\t\t</t_val>\n";

				$content .= "</TYPE_VAL>\n";
			}
            $content.="\t</t_mat_val>\n";
		}

        if (!empty($this->t_mat_pers)) {
            $content.=$prefix."\t<t_mat_pers nb=\"".count($this->t_mat_pers)."\">\n";
            
            // Constitution du tableau hiérarchique TYPE_DESC => ROLE => lexique
            foreach ($this->t_mat_pers as $idx => $mp) {
                $arrRoles[$mp["ID_TYPE_DESC"]][$mp["DLEX_ID_ROLE"]][]=$mp;
            }
            foreach ($arrRoles as $type_desc => $rol) {
                $content .= $prefix."\t\t<TYPE_DESC ID_TYPE_DESC=\"".$type_desc."\" >\n";
                foreach ($rol as $role=>$lexs) {
                    $content .=$prefix. "\t\t\t<t_rol DLEX_ID_ROLE=\"".$role."\" >\n";
                    foreach ($lexs as $idx=>$lex)	{
                        $content .=$prefix. "\t\t\t\t<t_personne ID_PERS=\"".$lex['ID_PERS']."\" >\n";
                        // Pour chaque propri?t? du terme
                        foreach ($lex as $fld=>$value) {
                            $content .=$prefix. "\t\t\t\t\t<".$fld.">".str_replace($search,$replace, $value)."</".$fld.">\n";
                        }
                        $content .=$prefix. "\t\t\t\t</t_personne>\n";
                    }
                    $content .=$prefix."\t\t\t</t_rol>\n";
                }
                $content .=$prefix. "\t\t</TYPE_DESC>\n";
            }
            $content.=$prefix."\t</t_mat_pers>\n";
        }
		if (!empty($this->t_doc_mat)) {
            // I.4. Propri?t?s relatives ? t_doc_mat
            $content.="\t<t_doc_mat nb=\"".count($this->t_doc_mat)."\">\n";
			// Pour chaque type de valeur
			foreach ($this->t_doc_mat as $name => $value) {
				$content .= "\t\t<DOC_MAT ID_DOC=\"".$this->t_doc_mat[$name]["ID_DOC"]."\">\n";
				// Pour chaque valeur
				foreach ($this->t_doc_mat[$name] as $val_name => $val_value) {
					if (is_object($val_value)) {$content.="\t\t\t".$val_value->xml_export()."\n";}
					else if(is_array($val_value)) {
						$content.="\t\t\t<t_doc_mat_val>\n";
						foreach ($val_value as $_i=>$dmv) {
							$content .= "\t\t\t\t<VAL ID_TYPE_VAL=\"".$dmv['ID_TYPE_VAL']."\">\n";
							$content .= "\t\t\t\t\t<ID_VAL>".$dmv['ID_VAL']."</ID_VAL>\n";
							$content .= "\t\t\t\t\t<ID_TYPE_VAL>".$dmv['ID_TYPE_VAL']."</ID_TYPE_VAL>\n";
							$content .= "\t\t\t\t\t<ID_DOC_MAT>".$this->t_doc_mat[$name]["ID_DOC_MAT"]."</ID_DOC_MAT>\n";
							$content .= "\t\t\t\t</VAL>\n";
						}
						$content.="\t\t\t</t_doc_mat_val>\n";
					} else {
						$content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
					}
				}
				$content .= "\t\t</DOC_MAT>\n";
			}
            $content.="\t</t_doc_mat>\n";
		}


        if (!empty($this->arrChildren)) {
            $content.=$prefix."\t<t_mat_children nb=\"".count($this->arrChildren)."\">\n";
            foreach ($this->arrChildren as $name => $value) {
                $content .=$prefix."\t\t<t_mat ID_MAT=\"".$this->arrChildren[$name]["ID_MAT"]."\">";
                // Pour chaque valeur
                foreach ($this->arrChildren[$name] as $val_name => $val_value) {
                    $content .= $prefix."\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                }
                $content .=$prefix. "</t_mat>\n";
            }
            $content.=$prefix."\t</t_mat_children>\n";
        }

        if (!empty($this->t_tape_file)) {
            $content.="\t<t_tape_file nb=\"".count($this->t_tape_file)."\">\n";
            foreach ($this->t_tape_file as $name => $value) {
                $content .= "\t\t<TAPE_FILE ID_TAPE_FILE=\"".$this->t_tape_file[$name]["ID_TAPE_FILE"]."\">\n";
                foreach ($this->t_tape_file[$name] as $val_name => $val_value) {
                    if (is_object($val_value)) {$content.=$val_value->xml_export();}
                    else {
                        $content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                    }
                }
                $content .= "\t\t</TAPE_FILE>\n";
            }
            $content.="\t</t_tape_file>\n";
        }
		
 		$content.=$indent."</t_mat>\n";
        if($encodage!=0){
        	//trace("Export mat : ".mb_convert_encoding($content,$encodage,"UTF-8"));
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
        	//trace("Export mat : ".$content);
            return $content;
        }
    }

    /** A partir d'un codec, renvoie un format de fichier media acceptable par l'application
     *  IN : format source (~=codec) tel que renvoyé par infomedia (très divers)
     *  OUT : format utilisable par Opsis
     */
    static function retrieveFormat($formatSrc) {
		global $db;
		$formatSrc=strtoupper($formatSrc);
		$sql="SELECT * FROM t_codec WHERE CODEC_ID=".$db->Quote($formatSrc);
		$fmt=$db->GetAll($sql);
		if (empty($fmt)) return $formatSrc;
		else return $fmt[0]['CODEC_FORMAT'];
    }

	/** Récupération des paramètres de classe
	 * pour l'upload Flash **/
    static function getUploadParams() {
    	$arrFormats=array_merge(unserialize(gVideoTypes),unserialize(gAudioTypes),unserialize(gImageTypes));
    	$arrParams["strFilesFormat"]=implode("|",$arrFormats);
		$arrParams["maxFileCount"]=1;
		$arrParams["maxTotalSize"]=ini_get("upload_max_filesize"); // récup du param PHP.ini
		$arrParams["maxFileSize"]=ini_get("upload_max_filesize"); // récup du param PHP.ini
 		$arrParams["destinationDir"]=kVideosDir;

 		$colors["backgroundColor"]='#c1f8ff';
		$colors["buttonBackgroundColor"]='#9ac0c4';
		$colors["listBackgroundColor"]='#c4fbff';
		$caption["labelText"]='Select&nbsp;material&nbsp;files';


    	return array("arrParams"=>$arrParams,"colors"=>$colors,"caption"=>$caption);   // le paramétrage par déft convient très bien, on retourne false
    }


	function transcodeImage($arrParams,$self=false,&$myTransMat) {
		// MS 25/06/13 - ajout possibilité de définir le mat_type (VISIO/MASTER) et le mat_type_mime dans le XML de l'encodage (table t_encodage_preset);

		if (!$this->checkFileExists()) {$this->dropError(kUploadTranscodageErreurNoSource);return false;}

		if (empty($arrParams['format']) )
		{ $this->dropError(kUploadTranscodageErreurNoFormat); return false; }

   		$format=$arrParams['format'];
		if(isset($arrParams['mat_type'])){
			$mat_type = $arrParams['mat_type'];
		}
		if(isset($arrParams['mat_type_mime'])){
			$mat_type_mime = $arrParams['mat_type_mime'];
		}
    	$ext=$this->getExtensionByFormat($format); //Le fichier de sortie a le même nom que le matériel avec une extension différente

		if (empty($format) || empty($ext)) {$this->dropError(kUploadTranscodageErreurFormatNonGere);return false;}
		$orgExt=getExtension($this->t_mat['MAT_NOM']);
		$filename=stripExtension($this->t_mat['MAT_NOM']);
		$fileSrc=$this->getFilePath();
		debug($fileSrc);
		$lieu=Materiel::setLieuByIdMat($filename.".".$ext); //calcul du lieu de stockage
    	debug($lieu,'yellow');
    	if (!is_dir($lieu['LIEU_PATH'])) mkdir($lieu['LIEU_PATH']);
    	$fileout=$lieu['LIEU_PATH'];

		if(!empty($this->t_mat['MAT_CHEMIN']))
			$fileout.=$this->t_mat['MAT_CHEMIN'].'/';

		$fileout.= $filename.".".$ext;

    	//On ne peut pas créer un fichier avec le même nom que l'original.
    	// Pour le moment, on empêche la création. à l'avenir, renommage ?
    	if($self==false && $ext==$orgExt) {
			if(isset($mat_type)){
				$this->t_mat['MAT_TYPE']=$mat_type;
				$this->id_mat_ref=$this->t_mat['MAT_NOM'];
				$ok=$this->save();
    		}
			$this->dropError(kUploadTranscodageErreurFormatExisteDeja);return false;
		}

									setImagickPath();
									
		if (!extension_loaded('imagick') && !defined("kConvertPath")) {$this->dropError(kExtensionImagickNotLoaded);return false;}
		if (extension_loaded('imagick')) { //par extension
			$image=new Imagick();
			$image->readImage($fileSrc);
			$image->setImageFileName($fileout);
			$image->writeImage();
			$image->clear();
			$image->destroy();
		} else { //par ligne de commande
			if (!defined("kConvertPath")) {$this->dropError(kFichierImagickNotFound);return false;}
			exec(kConvertPath." ".$fileSrc." ".$fileout);
		}


    	if ($self) { // MAJ du matériel en cours
    		$this->id_mat_ref=$this->t_mat['MAT_NOM'];
     		$this->t_mat['MAT_FORMAT']=$format;
    		$this->t_mat['MAT_NOM']=$filename.".".$ext;
    		$this->setLieu();
    		$this->t_mat['MAT_INFO']='';
    		$ok=$this->save();
    	} else { //Création d'un nouveau matériel
    		$myTransMat=new Materiel;
    		$myTransMat->t_mat=$this->t_mat; //copie des infos, notamment TC
    		$myTransMat->t_mat['MAT_FORMAT']=$format;
			if(isset($mat_type)){
				$myTransMat->t_mat['MAT_TYPE']=$mat_type;
    		}
			if(isset($mat_type_mime)){
				$myTransMat->t_mat['MAT_TYPE_MIME']=$mat_type_mime;
    		}
			unset($myTransMat->t_mat['ID_MAT']);
    		$myTransMat->t_mat['MAT_NOM']=$filename.".".$ext;
    		$myTransMat->setLieu();
    		$myTransMat->t_mat['MAT_INFO']='';
    		$myTransMat->id_mat_ref='';
    		$ok=$myTransMat->save();
    		$this->dropError(kUploadTranscodageBilan." ".$filename.".".$ext." : ".str_replace('<br/>','',$myTransMat->error_msg)); //Récupération des msg de sauvegarde du mat transcodé
    	}
    	return true;
	}

    /** Fonction de transcodage de fichier bureautique, génère un fichier d'un autre type
     *  IN :  $self=> transcodage du fichier matériel (true) ou création d'un autre (false)
     */
	function transcodeToPdf(&$myTransMat) {

		if (!$this->checkFileExists()) {$this->dropError(kUploadTranscodageErreurNoSource);return false;}

		$fileSrc=$this->getFilePath();
		$ext="pdf";
		if(getExtension($fileSrc)!=$ext){
			$filename=stripExtension($this->t_mat['MAT_NOM']);
			$lieu=Materiel::setLieuByIdMat($filename.".".$ext); //calcul du lieu de stockage
			if (!is_dir($lieu['LIEU_PATH'])) mkdir($lieu['LIEU_PATH']);
			$filePdf=$lieu['LIEU_PATH'].$filename.".".$ext;

			// Conversion pdf si nécesaire
			$filePdf = stripExtension($fileSrc).".".$ext;
			$ok = $this->convertToPdf($fileSrc,$filePdf);

			if ($ok) {  //Création d'un nouveau matériel
				$myTransMat=new Materiel;
				$myTransMat->t_mat=$this->t_mat; //copie des infos, notamment TC
				$myTransMat->t_mat['MAT_FORMAT']=$ext;
				$myTransMat->t_mat['MAT_NOM']=basename($filePdf);
				$myTransMat->setLieu();
				$myTransMat->t_mat['MAT_INFO']='';
				$myTransMat->id_mat_ref='';
				$ok=$myTransMat->save();
				$this->dropError(kUploadTranscodageBilan." ".$filename.".".$ext." : ".str_replace('<br/>','',$myTransMat->error_msg)); //Récupération des msg de sauvegarde du mat transcodé
				$ok = $myTransMat->extractText($filePdf);
			}
		}else{
			$ok = $this->extractText($fileSrc);
		}

		return $ok;
	}

    /** Fonction de transcodage de media, génère un fichier d'un autre type
     *  IN : arrParams => tableau des params du fichier à générer, $self=> transcodage du fichier matériel (true) ou création d'un autre (false)
     */
    function transcodeMedia($arrParams,$self=false,&$myTransMat) {

    	//Vérif matériel source et outil
    	if (!$this->checkFileExists()) {$this->dropError(kUploadTranscodageErreurNoSource);return false;}
        // VP 5/06/2012 : vérif existence ffmpeg
		$_binary=explode(" ",kFFMPEGpath);
	 	$_binary=$_binary[0];
    	if (!is_file($_binary)) {$this->dropError(kUploadTranscodageErreurNoBinary);return false;}

    	//Vérif paramètres
    	if (empty($arrParams['vcodec']) && empty($arrParams['acodec']) )
		{ $this->dropError(kUploadTranscodageErreurNoFormat); return false; }
    	if (empty($arrParams['s_w']) || empty($arrParams['s_h'])) $arrParams['s']=''; else $arrParams['s']=$arrParams['s_w']."x".$arrParams['s_h'];
    	unset($arrParams['s_w']);unset($arrParams['s_h']);

    	if (!$arrParams['bt']) $arrParams['bt']=round($arrParams['vb']/10); //bitrate tolerance = 1/10 du video bitrate

    	//Nom de fichier de sortie
    	$format=$this->getFormatByCodec($arrParams['vcodec'],$arrParams['acodec']);
    	$ext=$this->getExtensionByFormat($format); //Le fichier de sortie a le même nom que le matériel avec une extension différente

		if (empty($format) || empty($ext)) {$this->dropError(kUploadTranscodageErreurFormatNonGere);return false;}
		$filename=explode("\.",$this->t_mat['MAT_NOM']);
		$orgExt=$filename[count($filename)-1]; //extension du matériel
		unset($filename[count($filename)-1]); //retrait extension
		$filename=implode("\.",$filename);

    	//On ne peut pas créer un fichier avec le même nom que l'original.
    	// Pour le moment, on empêche la création. à l'avenir, renommage ?
    	if($self==false && $ext==$orgExt) {$this->dropError(kUploadTranscodageErreurFormatExisteDeja);return false;}

    	//Process ligne de commande
    	$cmd=kFFMPEGpath." -i \"".$this->getFilePath()."\"";
    	foreach ($arrParams as $_prm=>$_val) {
    		if (!empty($_val)) $cmd.=" -".strtolower($_prm)." ".strtolower($_val).($_prm=='ab' || $_prm=='bt' || $_prm=='vb'?'k':'');
    	}
    	if ($arrParams['vcodec']=='') $cmd.=" -vn "; //pas de video
    	if ($arrParams['acodec']=='') $cmd.=" -an "; //pas d'audio

    	$lieu=Materiel::setLieuByIdMat($filename.".".$ext); //calcul du lieu de stockage
    	if (!is_dir($lieu['LIEU_PATH'])) mkdir($lieu['LIEU_PATH']);
    	$fileout=$lieu['LIEU_PATH'].$filename.".".$ext;

		// VP 26/01/11 : suppression fichier destination si existe déjà
    	$cmd.=" -y \"".$fileout."\"";

    	trace($cmd);
    	exec($cmd); //lancement du transcodage

		if (!is_file($fileout)) {$this->dropError(kUploadTranscodageErreurEchec);return false;}

		//trace($fileout);

    	if ($format=='MPEG4' || $ext=='mp4') { //traitements additionnels : qt-faststart (sur mp4 only !)

    		$ok=mv_rename($fileout,$fileout.".tmp");

    		// lancement qtfaststart
    		if (!$ok) { $this->dropError(kUploadTranscodageErreurFichierTmp);return false; }
    		else {
	    		$cmd=kBinQTFastStart." "."\"".$fileout.".tmp\" \"".$fileout."\"";
				trace($cmd);
				exec($cmd);
				//exec ("ls -l ".kVideosDir." > ".kVideosDir."snapshot.txt");

				if (!file_exists($fileout)) { //echec
					$this->dropError(kUploadTranscodageErreurQTFastStart); //et on remet le fichier de départ

	    			rename($fileout.".tmp",$fileout);
				}
				else { //succes, on vire le fichier tmp
					//$this->dropError(kUploadTranscodageSuccesQTFastStart);
					unlink($fileout.".tmp");
				}
    		}
    	}

    	if ($self) { // MAJ du matériel en cours
    		$this->id_mat_ref=$this->t_mat['MAT_NOM'];
     		$this->t_mat['MAT_FORMAT']=$format;
    		$this->t_mat['MAT_NOM']=$filename.".".$ext;
    		$this->setLieu(); //maj lieu
    		$this->t_mat['MAT_INFO']='';
    		$ok=$this->save();
    	} else { //Création d'un nouveau matériel
    		$myTransMat=new Materiel;
    		$myTransMat->t_mat=$this->t_mat; //copie des infos, notamment TC
    		$myTransMat->t_mat['MAT_FORMAT']=$format;
    		$myTransMat->t_mat['ID_MAT']='';
    		$myTransMat->t_mat['MAT_NOM']=$filename.".".$ext;
    		$myTransMat->setLieu(); //calcul lieu
    		$myTransMat->t_mat['MAT_INFO']='';
    		$myTransMat->id_mat_ref='';
    		$ok=$myTransMat->save();
    		$this->dropError(kUploadTranscodageBilan." ".$filename.".".$ext." : ".str_replace('<br/>','',$myTransMat->error_msg)); //Récupération des msg de sauvegarde du mat transcodé
    	}
    	return true;
    }


    /** Fonction de conversion texte
     *  IN : arrParams => tableau des params du fichier à générer, $self=> transcodage du fichier matériel (true) ou création d'un autre (false)
     */
	function extractText($fileSrc) {
		global $db;
		if(!file_exists($fileSrc)) {
			$this->dropError(kUploadTranscodageErreurNoSource);
			return false;
		}
	 	if (!defined("kBinConvertText")) {$this->dropError(kErrorConvertTextNoTool);return false; }
		$_binary=explode(" ",kBinConvertText);
	 	$_binary=$_binary[0];
	 	if (!is_file($_binary)) {$this->dropError(kErrorConvertTextNoTool);return false; }

		$ok = true;
		$filenameTxt = stripExtension($fileSrc).".txt";
		//$cmd = "/usr/local/bin/pdftotext -enc UTF-8 ".$fileSrc." ".$filenameTxt;
		$cmd = kBinConvertText." ".$fileSrc." ".$filenameTxt;
		trace($cmd);
		exec($cmd);
		$contenu = stripControlCharacters(file_get_contents($filenameTxt));
		unlink($filenameTxt);

		if(empty($contenu)) {
			$this->dropError(kErreurConversionTexte);
			trace(kErreurConversionTexte." (".$fileSrc.")");
			$ok = false;
		} else {
			$this->dropError(kSuccesRecuperationTexte);
			//echo $contenu;
			$sql="UPDATE t_mat SET MAT_TEXTE=".$db->Quote($contenu)." WHERE ID_MAT=".intval($this->t_mat['ID_MAT']);
			$db->Execute($sql);
		}

		return $ok;
	}

    /** Fonction de conversion texte
     *  IN : arrParams => tableau des params du fichier à générer, $self=> transcodage du fichier matériel (true) ou création d'un autre (false)
     */
	function convertToPdf($fileSrc,$fileOut) {
		global $db;
		if(!file_exists($fileSrc)) {
			$this->dropError(kUploadTranscodageErreurNoSource);
			return false;
		}
	 	if (!defined("kBinConvertPdf")) {$this->dropError(kErrorConvertPdfNoTool);return false; }
		$_binary=explode(" ",kUtilFramePath);
	 	$_binary=$_binary[0];
	 	if (!is_file($_binary)) {$this->dropError(kErrorConvertPdfNoTool);return false; }

		$ok = true;
		$cmd = kBinConvertPdf." \"".$fileSrc."\" \"".$fileOut."\"";
		trace($cmd);
		exec($cmd);
		$ok = is_file($fileout);
		if (!$ok) {$this->dropError(kErreurConversionPdf);return false;}
		return $ok;
	}


	/** Renvoie la résolution du média
	 * 	IN : object matériel
	 * 	OUT : tableau width,height
	 * 	NOTE : la résolution est calculée depuis MAT_INFO de la faêon suivante prioritairement
	 * 	1. display_width / height : paramètre de résol d'affichage intégré dans un mp4 notamment par Canopus
	 * 	2. video_window : résolution native de la vidéo
	 * 	3. par déft : 320x240
	 * **/
	function getVideoResolution() {
		$native_resol=@preg_match("%<video_window>(.*?)</video_window>%",$this->t_mat['MAT_INFO'],$out);
		list($nativeX,$nativeY)=explode('x',$out[1]);
		$resol=@preg_match("%<display_width>(.*?)</display_width><display_height>(.*?)</display_height>%",$this->t_mat['MAT_INFO'],$out);
		$width=($out[1]?$out[1]:($nativeX?$nativeX:320));
		$height=($out[2]?$out[2]:($nativeY?$nativeY:240));
		return array($width,$height);
	}


    /** Récupère l'extension de fichier en se basant sur le format
     * 	IN : var de classe t_mat['MAT_FORMAT']
     * 	OUT : extension, en minuscules
     */
	static function getExtensionByFormat($format) {
     	global $db;
     	return $db->CacheGetOne(0,"select distinct FORMAT_FILE_EXT from t_format_mat WHERE FORMAT_MAT=".$db->Quote($format));
	}


	/** Récupère le format de fichier en fonction des codecs audio et video
	 *  Le codec video est prédominant pour le format.
	 *  IN : codec video, codec audio
	 *  OUT : format
	 */
	static function getFormatByCodec($vcodec,$acodec) {
     	global $db;
     	return $db->CacheGetOne(0,"select distinct CODEC_FORMAT, CODEC_ID_MEDIA from t_codec WHERE CODEC_ID=".$db->Quote($vcodec)." OR CODEC_ID=".$db->Quote($acodec)." ORDER BY CODEC_ID_MEDIA DESC");
	}

	/** Récupère les formats de fichiers numériques
	 *
	 */
	static function getOnlineFormats() {
     	global $db;
     	$exts=array_merge(unserialize(gAudioTypes),unserialize(gVideoTypes),unserialize(gImageTypes));

     	return $db->CacheGetCol(0,"select distinct FORMAT_MAT from t_format_mat WHERE FORMAT_FILE_EXT IN('".implode("','",$exts)."')");
	}


	/** Effectue le mappage entre un tableau et des champs de mat
	 *
	 */
	function mapFields($arr) {

		require_once(modelDir."model_val.php");
		require_once(modelDir.'model_doc.php');
        require_once(modelDir.'model_personne.php');

		foreach ($arr as $idx=>$val) {

			$myField=$idx;
			// analyse du champ
			if ($myField=='ID_MAT') $this->t_mat['ID_MAT']=$val;
			$arrCrit=explode("_",$myField);
			switch ($arrCrit[0]) {
				case 'MAT': //champ table MAT
					$val=trim($val); //Important, car souvent, les tags ID3 ont des espaces supplémentaires
					$flds=explode(",",$myField);
					// VP 18/11/10 : ajout test pour mise à jour val=0
					if ($val==='0' || !empty($val)) foreach ($flds as $_fld) $this->t_mat[$_fld]=$val;
					break;

				case 'V': //valeur
                    if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
                    
                    foreach($val as $thisVal) {
                        if(empty($thisVal['VALEUR'])) {
                            continue;
                        }
                        $myVal=new Valeur();
                        $thisVal['VAL_ID_TYPE_VAL']=isset($thisVal['VAL_ID_TYPE_VAL'])?$thisVal['VAL_ID_TYPE_VAL']:$arrCrit[1];
                        $thisVal['ID_LANG']=$_SESSION['langue'];
                        $myVal->createFromArray($thisVal, true, true);
                        
                        if (isset($thisVal['ID_TYPE_VAL'])){
                            $val_id_type_val=$thisVal['ID_TYPE_VAL'];
                        }elseif ($arrCrit[2]!=''){
                            $val_id_type_val=$arrCrit[2];
                        }else {
                            $val_id_type_val=$myVal->t_val['VAL_ID_TYPE_VAL'];
                        }
                        
                        $this->t_mat_val[]=array('ID_TYPE_VAL'=>$val_id_type_val,'ID_VAL'=>$myVal->t_val['ID_VAL']);
                    }
                                    
                    /*
					if (empty($val)) break;
					$myVal=new Valeur();
					
					if(is_array($val)){
						foreach($val as $valeur){
							$myVal->t_val['ID_LANG']=$_SESSION['langue'];
							$myVal->t_val['VAL_ID_TYPE_VAL'] = isset($valeur['VAL_ID_TYPE_VAL'])?$valeur['VAL_ID_TYPE_VAL']:$arrCrit[1];
							if(isset($valeur['ID_VAL'])){
								$myVal->t_val['ID_VAL'] = $valeur['ID_VAL'];
								$_val=$myVal->checkExistId();

							}else{
								$myVal->t_val['VALEUR'] = $valeur['VALEUR'];
								$_val=$myVal->checkExist();
							}
							

							if ($_val) {
								$myVal->t_val['ID_VAL']=$_val;
								$myVal->save(); }
							else {
								$myVal->create(); //création dans la langue
								foreach ($_SESSION['arrLangues'] as $lg) {
									if ($lg!=$_SESSION['langue']) $arrVersions[$lg]= $valeur['VALEUR'];
								}
								$myVal->saveVersions($arrVersions); //sauve dans autres langues
								unset($arrVersions);
							}
							$this->t_mat_val[]=array('ID_TYPE_VAL'=>$myVal->t_val['VAL_ID_TYPE_VAL'],'ID_VAL'=>$myVal->t_val['ID_VAL']);
						}
					}else if(is_string($val)){
						$myVal->t_val['VALEUR']=$val;
						$myVal->t_val['VAL_ID_TYPE_VAL']=$arrCrit[1];
						$myVal->t_val['ID_LANG']=$_SESSION['langue'];
						$_val=$myVal->checkExist();
						if ($_val) {
							$myVal->t_val['ID_VAL']=$_val;
							$myVal->save(); }
						else {
							$myVal->create(); //création dans la langue
							foreach ($_SESSION['arrLangues'] as $lg) {
								if ($lg!=$_SESSION['langue']) $arrVersions[$lg]=$val;
							}
							$myVal->saveVersions($arrVersions); //sauve dans autres langues
							unset($arrVersions);
						}
						$this->t_mat_val[]=array('ID_TYPE_VAL'=>$arrCrit[1],'ID_VAL'=>$myVal->t_val['ID_VAL']);
						}
                     */
					break;

                case 'P': //personne
                    if ($val!=='') {
                        if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
                        foreach($val as $thisVal) {
                        
                            $myPers=new Personne();
                            if (!$thisVal['PERS_NOM'])
                                $thisVal['PERS_NOM']=$thisVal['VALEUR'];
                            if(!$thisVal['PERS_ID_TYPE_LEX'])
                                $thisVal['PERS_ID_TYPE_LEX']=$arrCrit[3]; //par déft, extrait du champ
                            
                            $thisVal['ID_LANG']=$_SESSION['langue'];
                            
                            $myPers->createFromArray($thisVal, true, true, true);
                            
                            $tmp_mat_pers=array('DLEX_ID_ROLE'=>($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]),
                                               'ID_TYPE_DESC'=>($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]),
											   'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
                                               'ID_PERS'=>$myPers->t_personne['ID_PERS']);
                            
                            $this->t_mat_pers[]= $tmp_mat_pers;
                        }
                    }
                    break;
                                    
				case 'DOC': //document
					foreach ($val as $thisDoc) {
						$myDoc=new Doc();
                        // VP 7/06/2012 : utilisation de mapFields à la place de updateFromArray
						//$myDoc->updateFromArray($thisDoc);
                        $myDoc->mapFields($thisDoc);

						// VP 21/10/10 : utilisation de save (create n'existe pas dans class_doc !)
						/*if ($myDoc->checkExist()) {
						 $myDoc->save();
						 }
						 else {
						 $myDoc->create(); //création
						 }*/
						$ok=$myDoc->save(0,false);
						//debug($myMat,'yellow');
						$tcin=(empty($thisDoc['DMAT_TCIN'])?$this->t_mat['MAT_TCIN']:$thisDoc['DMAT_TCIN']);
						$tcout=(empty($thisDoc['DMAT_TCOUT'])?$this->t_mat['MAT_TCOUT']:$thisDoc['DMAT_TCOUT']);
						$this->t_doc_mat[]=array('ID_DOC'=>$myDoc->t_doc['ID_DOC'],
												 'ID_MAT'=>$this->t_mat['ID_MAT'],
												 'DMAT_TCIN'=>$tcin,
												 'DMAT_TCOUT'=>$tcout,
												 'DMAT_LIEU'=>$thisDoc['DMAT_LIEU']);
					}
					break;
			}
		}
        // Création format mat
        if(!empty($this->t_mat['MAT_FORMAT'])){
            $this->createFormatMat();
        }
	}

	/**
	 * Récupération de l'emplacement du matériel
	 * IN : obj
	 * OUT : chemin lieu_path
	 * NOTE : si lieu_path est une constante Opsis (kVideosDir par ex), on l'interprète
	 */
	function getLieu(&$server=null) {
		// VP 16/9/09 : renvoi LIEU_SERVER
		global $db;
		$server="localhost";
		if (empty($this->t_mat['MAT_LIEU']))
		{
			if (isset($this->t_mat['MAT_CHEMIN']) && !empty($this->t_mat['MAT_CHEMIN']))
				return kVideosDir.$this->t_mat['MAT_CHEMIN'].'/';
			else
				return kVideosDir;
		}
		//$lieu=$db->CacheGetOne(30,"select LIEU_PATH from t_lieu where LIEU=".$db->Quote($this->t_mat['MAT_LIEU']));
 		$sql="SELECT LIEU_PATH,LIEU_SERVER from t_lieu where LIEU=".$db->Quote($this->t_mat['MAT_LIEU']);
		$rows=$db->CacheGetAll(30,$sql);
		if(count($rows)>0){
			$lieu=$rows[0]['LIEU_PATH'];
			$server=$rows[0]['LIEU_SERVER'];
		
			if(isset($lieu) && !empty($lieu)){
				if (defined($lieu)){
					$lieu=constant($lieu); //pour pouvoir utiliser des constantes comme chemin
				}else if(substr($lieu,0,1) != '/' && !eregi('WINNT',PHP_OS)){
					if(strpos($lieu,'public/') === 0  ){		
						$lieu = str_replace('public/',kCheminLocalMedia.'/',$lieu);			
					}else if(strpos($lieu,'private/') === 0  ){		
						$lieu = str_replace('private/',kCheminLocalPrivateMedia.'/',$lieu);
					}else{
						$lieu = kCheminLocalPrivateMedia."/".$lieu;
					}
				}
			}
		}else{
			$lieu=kVideosDir;
		}
		if(empty($server)) $server="localhost";

		if (isset($this->t_mat['MAT_CHEMIN']) && !empty($this->t_mat['MAT_CHEMIN']))
			$lieu.=$this->t_mat['MAT_CHEMIN'].'/';

		return $lieu;
	}

	/**
	 * Calcul de l'emplacement du matériel selon le pattern matching du matériel
	 * IN : obj
	 * OUT : maj tab de classe t_mat[MAT_LIEU] + chemin lieu_path
	 * NOTE : si lieu_path est une constante Opsis (kVideosDir par ex), on l'interprète
	 */
 	function setLieu(&$server=null) {
		// VP 16/9/09 : renvoi LIEU_SERVER
 		global $db;
 		$arrLines=Materiel::setLieuByIdMat($this->t_mat['MAT_NOM']);
 		$server=$arrLines['LIEU_SERVER'];
 		$this->t_mat['MAT_LIEU']=$arrLines['LIEU'];
 		if (defined($arrLines['LIEU_PATH'])) $arrLines['LIEU_PATH']=constant($arrLines['LIEU_PATH']);
 		//debug($arrLines,'cyan');

		if (isset($this->t_mat['MAT_CHEMIN']) && !empty($this->t_mat['MAT_CHEMIN']))
			return $arrLines['LIEU_PATH'].$this->t_mat['MAT_CHEMIN'].'/';
		else
			return $arrLines['LIEU_PATH'];
 	}

	function getFilePath()
	{
		return str_replace("&amp;","&", $this->getLieu().$this->t_mat['MAT_NOM']);
	}

 	/**
 	 * Calcul de l'emplacement du matériel selon le pattern matching, méthode statique
 	 * IN : MAT_NOM
 	 * OUT : array Lieu, Lieu_path
 	 */
 	static function setLieuByIdMat($filename) {
 		global $db;
		// VP 21/07/09 : changement ordre de tri par DESC
  		
 		$arrLines=$db->CacheGetRow(30,"SELECT LIEU,LIEU_PATH,LIEU_SERVER,LIEU_DEFAUT,LIEU_PATTERN from t_lieu where ".$db->Quote($filename)."".$db->like('LIEU_PATTERN',false)." ORDER BY LIEU_DEFAUT DESC");
 		if (empty($arrLines)){
 			$arrLines=$db->CacheGetRow(30,"SELECT LIEU,LIEU_PATH,LIEU_SERVER,LIEU_DEFAUT,LIEU_PATTERN from t_lieu where ".$db->getRegexpSQL($db->Quote($filename), 'LIEU_PATTERN' ,false)." and LIEU_PATTERN != '' ORDER BY LIEU_DEFAUT DESC");
 		}
		if (empty($arrLines)){
 			return array("LIEU"=>'',"LIEU_PATH"=>kVideosDir,"LIEU_SERVER"=>"localhost");
		}else{
			// MS - 28.05.15 - si LIEU_PATH ne commence pas par un slash, alors on considère qu'il s'agit d'un chemin relatif par rapport au répertoire <media_dir_path>/private
			if(substr($arrLines['LIEU_PATH'],0,1) !='/' && !eregi('WINNT',PHP_OS)){
				if(strpos($arrLines['LIEU_PATH'],'public/') === 0  ){		
					$arrLines['LIEU_PATH'] = str_replace('public/',kCheminLocalMedia.'/',$arrLines['LIEU_PATH']);
				}else if(strpos($arrLines['LIEU_PATH'],'private/') === 0  ){		
					$arrLines['LIEU_PATH'] = str_replace('private/',kCheminLocalPrivateMedia.'/',$arrLines['LIEU_PATH']);
				}else{
					$arrLines['LIEU_PATH'] = kCheminLocalPrivateMedia."/".$arrLines['LIEU_PATH'];
				}
			}
			return $arrLines;
		}
	}


 	static function getLieuByIdMat($id_mat, &$server=null) {
 		global $db;
		// VP 16/9/09 : renvoi LIEU_SERVER (localhost par défaut)
 		$sql="SELECT l.LIEU_PATH,l.LIEU_SERVER,l.LIEU, m.ID_MAT, m.MAT_CHEMIN, m.MAT_LIEU from t_mat m
				INNER JOIN t_lieu l ON m.MAT_LIEU=l.LIEU
				WHERE m.ID_MAT=".intval($id_mat);
 		$rows=$db->CacheGetAll(30,$sql);
		if(count($rows)>0){
			$path=$rows[0]['LIEU_PATH'];
			// MS - 28.05.15 - si LIEU_PATH ne commence pas par un slash, alors on considère qu'il s'agit d'un chemin relatif par rapport au répertoire <media_dir_path>/private
			if(substr($path,0,1) != '/' && !eregi('WINNT',PHP_OS)){
				if(strpos($path,'public/') === 0  ){		
					$path = str_replace('public/',kCheminLocalMedia.'/',$path);				
				}else if(strpos($path,'private/') === 0  ){		
					$path = str_replace('private/',kCheminLocalPrivateMedia.'/',$path);				
				}else{
					$path = kCheminLocalPrivateMedia."/".$path;
				}
			}
			if(!empty($rows[0]['MAT_CHEMIN']))
				$path.=$rows[0]['MAT_CHEMIN'].'/';

			$server=$rows[0]['LIEU_SERVER'];
		}else{
			$path=kVideosDir;
		}
		if(empty($server)) $server="localhost";
 		//$path=$db->CacheGetOne(30,$sql);
 		//if ($path===false) return kVideosDir; else return $path;
		return $path;
 	}

	/**
	 * Application de l'offset du fichier à un time code
	 * IN : tc
	 * OUT : tc
	 */
	function TCwithOffset($tc){
		if(!$this->offsetTC) $this->offsetTC=(empty($this->t_mat['MAT_TCIN'])?"00:00:00:00":$this->t_mat['MAT_TCIN']);
        // VP 8/09/13 : suppression test sur MAT_TCFORCE et MAT_FORMAT
		//if($this->t_mat['MAT_TCFORCE']=='1' || ($this->t_mat['MAT_FORMAT']!= "MPEG2" && $this->t_mat['MAT_FORMAT']!= "MPEG1")) {
			$delta=tcToFrame($tc) - tcToFrame($this->offsetTC);
			if($delta < 0 && substr($this->t_mat['MAT_TCIN'],0,3) =="23:") $delta+=2160000;
			else if ($delta < 0 && $delta > -1500) $delta=0;
			$tc=frameToTC($delta);
		//}
		return $tc;
	}


	/**
	 * Ingest fichier
	 * IN : filename
	 */
	// VP 6/10/10 : ajout paramètre mat pour remplir des champs de t_mat
    function ingest($filename, $mat=null, $nomenclature="", $renameIfExist=true , &$infos, $mat_parent=null,$set_mat_groupe=false, $keepIntact=false ) {
        // VP 11/01/12 : sauvegarde nom fichier origine pour alimenter MAT_TITRE
        $filename_ori=$filename;
		// MS - 11/10/16 - ajout correction encodage des noms de fichiers avant envoi en base  => devrait corriger des pbs de special char lors d'imports FTP / batch
        $this->t_mat['MAT_NOM_ORI']=mb_convert_encoding(basename($filename_ori),'UTF-8');
        if(isset($mat['MAT_CHEMIN']) && !empty($mat['MAT_CHEMIN'])){$this->t_mat['MAT_CHEMIN'] = $mat['MAT_CHEMIN'];}
        
        if ($mat_parent!=null)
        {
            $this->t_mat['MAT_ID_GEN']=$mat_parent->t_mat['ID_MAT'];
            //strpos('',$this->t_mat['MAT_CHEMIN']);
            $this->t_mat['MAT_CHEMIN']=$mat_parent->t_mat['MAT_NOM'].substr($this->t_mat['MAT_CHEMIN'],strpos($this->t_mat['MAT_CHEMIN'],$mat_parent->t_mat['MAT_NOM_ORI'])+strlen($mat_parent->t_mat['MAT_NOM_ORI']));
        }
		
        if(!$keepIntact) {
			// MS - 11/01/13 - Si newName vide, il reçoit le nom du fichier => permet d'éviter l'écrasement de matériels ayant le même nom lors d'un import sans upload (par ex : FTP)
			if($renameIfExist) {
				if(empty($nomenclature)){
					$newName = stripExtension(basename($filename));
				} else {
	                // VP 10/12/2015 : possibilité de définir une nomenclature basée sur le nom d'origine
					//$newName = $nomenclature;
	                $newName = sprintf($nomenclature, stripExtension(basename($filename)));
				}
			}
	
	      if(!empty($newName)) {
			$id_mat=$newName.".".getExtension($filename);
			$first = true ;
			if($renameIfExist && empty($this->t_mat['ID_MAT'])) {
				$this->t_mat['MAT_NOM']= $id_mat;
				$exist=$this->checkExist($renameIfExist);
				if(!$exist)
					$exist=$this->checkOneFileExists();
				while($exist){
					if($first && (empty($nomenclature) || (isset($mat['addSuffixWhenExists']) && $mat['addSuffixWhenExists']))){
						$newName.="_00";
					}
					$newName=stringIteration($newName);
					$id_mat=$newName.".".getExtension($filename);
					$this->t_mat['MAT_NOM']= $id_mat;
					$exist=$this->checkExist($renameIfExist);
					if(!$exist)
						$exist=$this->checkOneFileExists();
	
					$first=false ;
				}
			}
	
			// VP 2/12/10 : correction bug sur renommage
			$newFilename=kVideosDir.getFileFromPath($id_mat);
			// MS - 11/10/16 - ajout correction encodage des noms de fichiers avant envoi en base  => devrait corriger des pbs de special char lors d'imports FTP / batch
			if(!mb_check_encoding($newFilename,'UTF-8')){
				$newFilename = mb_convert_encoding($newFilename,'UTF-8');
			}
			mv_rename($filename,$newFilename);
			if(is_file($newFilename)){
				$filename=$newFilename;
			}
			$this->t_mat['MAT_NOM']=basename($filename);
	       }else{
			   // MS - 11/10/16 - ajout correction encodage des noms de fichiers avant envoi en base  => devrait corriger des pbs de special char lors d'imports FTP / batch
				if(!mb_check_encoding($filename,'UTF-8')){
					$newFilename = mb_convert_encoding($filename,'UTF-8');
					mv_rename($filename,$newFilename);
					if(is_file($newFilename)){
						$filename=$newFilename;
					}
				}
		   }
        }

		if($mat) {
			$this->updateFromArray($mat);

			if (!empty($mat["t_mat_val"])){
				foreach ($mat["t_mat_val"] as $key=>$value){
					if (!empty($value)){
						$this->t_mat_val[]=array('ID_VAL'=>$value,'ID_TYPE_VAL'=>$key);
					}
				}
			}
		}

        //PC 25/07/12 : filtre des caractères accentués et autres pour éviter les bug de chargements
        if(!$keepIntact) {
        	$this->t_mat['MAT_NOM']= getFileFromPath(basename($filename));
        } else {
        	$this->t_mat['MAT_NOM']= basename($filename);
        }
        // VP 9/10/12 : utilisation de t_mat['MAT_LIEU'] si existant
        if(!empty($this->t_mat['MAT_LIEU'])) $path=$this->getLieu($server);
        else $path=$this->setLieu($server);

        // VP 24/01/11 : tests lieu serveur sur localhost ou empty
        if(empty($server) || $server=="localhost"){
            if (!is_dir($path))
                mkdir($path,0755,true);

            $begin=gettimeofday(true);
            if(!$keepIntact) {
            	mv_rename($filename,$path.$this->t_mat['MAT_NOM']);
            }
			if(!is_file($path.$this->t_mat['MAT_NOM'])){
				$this->t_mat['MAT_NOM']=basename($filename);
			}
            trace('duree copie fichier : '.(gettimeofday(true)-$begin));

            //require_once(libDir."class_upload.php");
			$begin=gettimeofday(true);
			
			// MS - 25.05.16 -  l'update des infos matériel est fait dans updateMatInfo
			$this->updateMatInfo($infos) ; 
			
            //PC 21/02/11 : ajout du format matériel si il n'existe pas en base
            $this->createFormatMat();
        }

		if ($set_mat_groupe)
		{
			$get_format_mat=$this->getFormatMat($this->t_mat['MAT_FORMAT']);

			if (!isset($this->t_mat['MAT_GROUPE']) || empty($this->t_mat['MAT_GROUPE']))
				$this->t_mat['MAT_GROUPE']=$get_format_mat['FORMAT_GROUPE_MAT'];

			if (!isset($this->t_mat['MAT_TYPE']) || empty($this->t_mat['MAT_TYPE']))
				$this->t_mat['MAT_TYPE']=$get_format_mat['FORMAT_TYPE_MAT'];
		}

        $idMat = $this->checkExist();
        if($idMat) {
        	//Initalisation d'id_mat_ref et ID_MAT sinon save() bifurque vers create()
        	$this->id_mat_ref = $this->t_mat['MAT_NOM'];
        	$this->t_mat['ID_MAT'] = $idMat;
        	$ok = $this->save();			
			$this->getDocMat();
			//faire getdocmat récupere tcout si vide mettre a jout les Tc mat tcin
			if($this->t_doc_mat[0]['DMAT_TCOUT'] == ''){
				$this->t_doc_mat[0]['DMAT_TCIN']=$this->t_mat['MAT_TCIN'];
				$this->t_doc_mat[0]['DMAT_TCOUT']=$this->t_mat['MAT_TCOUT'];
				$ok=$this->saveDocMat();
			}
			// Récupération tracks pour mise à jour
			$this->getMatTrack();
		} else {
        	$ok = $this->create();
		}
									
		if ($this->t_mat['MAT_ID_MEDIA']=='V' || $this->t_mat['MAT_ID_MEDIA']=='A'){
			$this->saveMatTrack($infos);
		}
		if(isset($this->t_mat_val) and !empty($this->t_mat_val)) {
			$this->saveMatVal(false);
		}
		// Init etat_arch
		$this->updateEtatArch();
									
        return $ok;

    }

	public function updateMatInfo(&$infos,$filename_ori=null){
		$infos=MatInfo::getMatInfo($this->getFilePath());
		// VP 20/10/10 : ajout test sur MAT_TCIN et MAT_TCOUT
		if(empty($this->t_mat['MAT_TCIN']) || $this->t_mat['MAT_TCIN'] == "00:00:00:00")
		{
			if ($infos['tcin']!=null && !empty($infos['tcin']))
				$this->t_mat['MAT_TCIN']=$infos['tcin'];
			else
				$this->t_mat['MAT_TCIN']='00:00:00:00';
		}

		if(empty($this->t_mat['MAT_TCOUT']) || $this->t_mat['MAT_TCOUT'] == "00:00:00:00")
		{
			$this->t_mat['MAT_TCOUT']=$infos['tcout'];
		}

		// Modification de la classe t_mat pour prendre en compte le champ MAT_RATIO
		$this->t_mat['MAT_RATIO']=$infos['display_aspect_ratio'];
		//$this->t_mat['MAT_RATIO']=$infos['MEDIAINFO'][0]['FILE'][0]['TRACK'][1]['DISPLAY_ASPECT_RATIO'];
		$this->t_mat['MAT_DUREE']=secToTime($infos['duration']);

		if(empty($this->t_mat['MAT_TITRE']) && !empty($this->t_mat['MAT_NOM_ORI'])){
			$this->t_mat['MAT_TITRE']=$this->t_mat['MAT_NOM_ORI'];
		}else if(empty($this->t_mat['MAT_TITRE'])){
			$this->t_mat['MAT_TITRE']=basename($filename_ori);
		}
		//if(empty($this->t_mat['MAT_FORMAT']))
		//$this->t_mat['MAT_FORMAT']=$this->retrieveFormat($infos['format']); //récup format depuis codec
		//VP 27/03/12 : récup format
		$this->t_mat['MAT_FORMAT']=$infos['format_calc'];
		$this->t_mat['MAT_FORMAT_ENV']=$infos['format_env'];
		$this->t_mat['MAT_TYPE_MIME']=$infos['type_mime'];
		$this->t_mat['MAT_TAILLE']=$infos['file_size'];
		$this->t_mat['MAT_DATE_FICHIER']=$infos['file_time'];
		$this->t_mat['MAT_RESOLUTION']=$infos['resolution'];

		if(isset($infos['format_calc_long']) && !empty($infos['format_calc_long'])){
			$this->t_mat['MAT_FORMAT_LONG']=$infos['format_calc_long'];
		}else if(isset($infos['format_long']) && !empty($infos['format_long'])){
			$this->t_mat['MAT_FORMAT_LONG']=$infos['format_long'];
		}
		if(isset($infos['bit_rate']) && !empty($infos['bit_rate'])){
			$this->t_mat['MAT_BIT_RATE']=$infos['bit_rate'];
		}
		if(isset($infos['pages']) && !empty($infos['pages'])){
			$this->t_mat['MAT_NB_PAGES']=$infos['pages'];
		}
		$this->t_mat['MAT_FORMAT_PROFILE']=$infos['format_profile'];
		$this->t_mat['MAT_SYSTEM_TC']=$infos['system_timecode'];
		$this->t_mat['MAT_HEIGHT']=$infos['height'];
		$this->t_mat['MAT_WIDTH']=$infos['width'];
		$this->t_mat['MAT_DISPLAY_HEIGHT']=$infos['display_height'];
		$this->t_mat['MAT_DISPLAY_WIDTH']=$infos['display_width'];

		$this->t_mat['MAT_INFO']=str_replace(array('<?','?>'),array('&lt;?','?&gt;'),$infos['xml']);
		// MS 24.04.15 - Puisqu'on vient de calculer MAT_TAILLE par la classe matInfo, il n'y a aucune raison de se baser sur getFileSize si on a déja l'information.
		if( empty($this->t_mat['MAT_TAILLE'])){
			$this->getFileSize();
		}
		// VP 29/11/10 : affectation MAT_ID_MEDIA
		// MS 30/09/13 : affectation de MAT_ID_MEDIA dans un premier temps par les infos remontées par getMatInfo, sinon par le type mime
		if(isset($infos['id_media']) && !empty($infos['id_media'])){
			$this->t_mat['MAT_ID_MEDIA']=$infos['id_media'];
		}else{
			// ms - to rework 
			$this->getMediaType();
			$this->t_mat['MAT_ID_MEDIA']=$this->id_media;
		}
	}

	private static function getFilesInDir($repertoire)
	{
		$repertoire=str_replace('//','/',$repertoire.'/');
		$dir=opendir($repertoire);

		$files=array();

		if ($dir!==false)
		{
			while (($file=readdir($dir))!==false)
			{
				//if ($file!='.' && $file!='..')
                if ($file[0]!='.' )
				{
					if (is_dir($repertoire.$file))
					{
						$files=array_merge($files,Materiel::getFilesInDir($repertoire.$file));
					}
					else
						$files[]=$repertoire.$file;
				}
			}

			closedir($dir);

			return $files;
		}

		return null;
	}

	private function ingestFilesInDir($repertoire_root,$repertoire,$arr_mat,&$taille,&$aIdMatCreated = array())
	{
		$repertoire=str_replace('//','/',$repertoire.'/');

        if(strpos($this->t_mat['MAT_FORMAT'],'DPX') === false && (!isset($this->DCP_map['TYPE_MAP']) || $this->DCP_map['TYPE_MAP'] != 'DCP_unwrap')){
		// VP 5/12/2012 : constitution de tar pour certains types de fichiers
			$this->tarFilesByExtension($repertoire);
		}

        $dir=opendir($repertoire);

		if ($dir!==false)
		{
			if ( strpos($this->t_mat['MAT_FORMAT'],'DCP')!==false && isset($this->DCP_map['TYPE_MAP']) && !empty($this->DCP_map['TYPE_MAP']) &&  $this->DCP_map['TYPE_MAP'] == 'DCP_unwrap'){
				$this->ingestDCPUnwrap($repertoire,$arr_mat,$taille);
			}else{			
				while (($file=readdir($dir))!==false)
				{
					//if ($file!='.' && $file!='..' )
					if ($file[0]!='.' )
					{
						if (is_dir($repertoire.$file) && strpos($this->t_mat['MAT_FORMAT'],'DPX')===false  ){
							$this->ingestFilesInDir($repertoire_root,$repertoire.$file,$arr_mat,$taille,$aIdMatCreated);
						}else if (is_dir($repertoire.$file) && strpos($this->t_mat['MAT_FORMAT'],'DPX')!==false){
							// trace("cas special DPX_main ");
							$fichier=$repertoire.$file;
							$mat_fichier=new Materiel();
							$chemin=substr(dirname($fichier),1+strlen(dirname($repertoire_root)));
							$mat_fichier->t_mat['MAT_CHEMIN_ORI']=$chemin;
							$mat_fichier->t_mat['MAT_CHEMIN']=getDirFromPath($chemin);
							unset($arr_mat['MAT_TITRE']);
							$mat_fichier->ingest($fichier,$arr_mat,'',false,$infos,$this);
							$mat_fichier->id_mat_ref = $mat_fichier->t_mat['MAT_NOM'];
							// $mat_fichier->t_mat['MAT_INFO'] = tab2xml($this->DPX_map);
							$mat_fichier->t_mat['MAT_FORMAT'] = 'DPX_dir';
							$mat_fichier->save();
							$aIdMatCreated[] = $mat_fichier->t_mat['ID_MAT'];
						}else {
							$fichier=$repertoire.$file;
							$mat_fichier=new Materiel();
							$chemin=substr(dirname($fichier),1+strlen(dirname($repertoire_root)));
							$mat_fichier->t_mat['MAT_CHEMIN_ORI']=$chemin;
							$mat_fichier->t_mat['MAT_CHEMIN']=getDirFromPath($chemin);
							// gestion informations DCP
							if(isset($this->DCP_map) && !empty($this->DCP_map) && isset($this->DCP_map[$file])){
								if(in_array($this->DCP_map[$file]['TYPE_DCP'],array('SUBTITLE','SOUND','VIDEO')) && isset($this->DCP_map[$file]['REF_CPL'])){
									$mat_fichier->t_mat['MAT_COTE_ORI'] = $this->DCP_map[$file]['REF_CPL'];
								}
								if(isset($this->DCP_map[$file]['ANNOTATIONTEXT']) && !empty($this->DCP_map[$file]['ANNOTATIONTEXT'])){
									$mat_fichier->t_mat['MAT_NOTES'] = $this->DCP_map[$file]['ANNOTATIONTEXT'];
								}
								if(isset($this->DCP_map[$file]['CONTENTTITLETEXT']) && !empty($this->DCP_map[$file]['CONTENTTITLETEXT'])){
									$mat_fichier->t_mat['MAT_TITRE'] = $this->DCP_map[$file]['CONTENTTITLETEXT'];
								}else{
									$mat_fichier->t_mat['MAT_TITRE']=basename($file);
								}
								
							}else{
								$mat_fichier->t_mat['MAT_TITRE']=basename($fichier);
							}
							
							unset($arr_mat['MAT_TITRE']);
							
							$mat_fichier->ingest($fichier,$arr_mat,'',false,$infos,$this);
							$mat_fichier->updateEtatArch();
							$taille+=$mat_fichier->t_mat['MAT_TAILLE'];
							if(isset($this->DCP_map) && !empty($this->DCP_map) && isset($this->DCP_map[$file])
							&& $this->DCP_map[$file]['TYPE_DCP'] == 'CPL'){
								$this->DCP_map[$file]['ID_MAT'] = $mat_fichier->t_mat['ID_MAT'];
								if( isset($this->DCP_map[$file]['REEL']) && isset($mat_fichier->t_mat['ID_MAT']) && !empty($mat_fichier->t_mat['ID_MAT'])){
									$mat_fichier->id_mat_ref = $mat_fichier->t_mat['MAT_NOM'];
									$mat_fichier->t_mat['MAT_INFO'] = tab2xml($this->DCP_map[$file]);
									$mat_fichier->t_mat['MAT_FORMAT'] = 'DCP-CPL';
									$mat_fichier->createFormatMat();
									$mat_fichier->save();
								}
							}
							$aIdMatCreated[] = $mat_fichier->t_mat['ID_MAT'];
							//trace("ingestFilesInDir mat : ".$mat_fichier->t_mat['ID_MAT']." ".$mat_fichier->t_mat['MAT_NOM']." ".print_r($mat_fichier,true));
						}
					}
					else {
						$fichier=$repertoire.$file;
						$mat_fichier=new Materiel();
						$chemin=substr(dirname($fichier),1+strlen(dirname($repertoire_root)));
						$mat_fichier->t_mat['MAT_CHEMIN_ORI']=$chemin;
						$mat_fichier->t_mat['MAT_CHEMIN']=getDirFromPath($chemin);
						$mat_fichier->t_mat['MAT_TITRE']=basename($fichier);

						unset($arr_mat['MAT_TITRE']);

						$mat_fichier->ingest($fichier,$arr_mat,'',false,$infos,$this);
						$mat_fichier->updateEtatArch();
						$taille+=$mat_fichier->t_mat['MAT_TAILLE'];
						$aIdMatCreated[] = $mat_fichier->t_mat['ID_MAT'];
						trace('import '.$repertoire.'. New mat : '.$mat_fichier->t_mat['MAT_NOM'].', '.$mat_fichier->t_mat['ID_MAT']);
					}
				}
				closedir($dir);
			}
		}
	}
	
	// fonction spécifique pour l'ingest des medias composés de type DCP_unwrap (DCP exporté par easyDCP)
	private function ingestDCPUnwrap($repertoire,$arr_mat,&$taille){
		foreach($this->DCP_map as $key=>$val){
			// filtrage metadonnées
			if($key != 'TYPE_MAP' && $key != 'FRAMERATE'){
				// pour les videos / sons / sous titres on importe le répertoire
				if(isset($val['TYPE_DCP']) && in_array($val['TYPE_DCP'] , array('VIDEO','SOUND','SUBTITLE','EASY_DCP_MAP'))){
					$fichier=str_replace('//','/',$repertoire.'/'.$key);
					$mat_fichier=new Materiel();
					$chemin=substr(dirname($fichier),1+strlen(dirname($repertoire)));
					$mat_fichier->t_mat['MAT_CHEMIN_ORI']=$chemin;
					$mat_fichier->t_mat['MAT_CHEMIN']=getDirFromPath($chemin);
					unset($arr_mat['MAT_TITRE']);
					$mat_fichier->ingest($fichier,$arr_mat,'',false,$infos,$this);
					$this->DCP_map[$key]['ID_MAT'] = $mat_fichier->t_mat['ID_MAT'];
					trace("ingestDCPUnwrap : ".$key." => mat : ".$mat_fichier->t_mat['ID_MAT']);
					// $mat_fichier->id_mat_ref = $mat_fichier->t_mat['MAT_NOM'];
					// $mat_fichier->t_mat['MAT_FORMAT'] = 'DPX_dir';
					// $mat_fichier->save();
					unset($mat_fichier);
				}else if (isset($val['TYPE_DCP']) && $val['TYPE_DCP'] == 'CPL_unwrap'){
					// création matériel factice qui prendra la "place" du CPL dans le cas des DCP normaux => c'est sur ce matériel qu'on lancera l'encodage(puisqu'il référence l'ensemble de ses fils )
					$mat_fichier = new Materiel() ; 
					$fichier=str_replace('//','/',$repertoire.'/'.$key);
					// MSTEST MAT_CHEMIN pour matériel factice de génération à vérifier
					$mat_fichier->t_mat['MAT_CHEMIN_ORI']=basename($repertoire);
					$mat_fichier->t_mat['MAT_CHEMIN']=basename($repertoire);
					$mat_fichier->t_mat['MAT_NOM']=$key;
					$mat_fichier->id_mat_ref = $mat_fichier->t_mat['MAT_NOM'];
					$mat_fichier->t_mat['MAT_INFO'] = tab2xml($this->DCP_map[$key]);
					$mat_fichier->t_mat['MAT_FORMAT'] = 'DCP-CPL';
					$mat_fichier->save();
					$this->DCP_map[$key]['ID_MAT'] = $mat_fichier->t_mat['ID_MAT'];
					unset($mat_fichier);
				}
			}
		}
	}

    public function checkDpxFolder($repertoire, &$infos_dpx, $nom_prores="")
    {
        require_once(libDir."class_matInfo.php");
        $dir_dpx='';
        $dir_prores='';
        $dir_wav='';

        $duree_mov=0;
        $son_mov=false;
        $duree_wav=0;
        $ok=true;
        $erreur_duree_mov=false;
        $erreur_duree_wav=false;
        $nb_dpx=0;
        $infos_dpx=array();
        $nom_dpx=basename($repertoire);

        if($dh=opendir($repertoire)){
            while ($dir=readdir($dh)){
                if ($dir !== '.' && $dir !== '..' ){
                    $dir_element=explode('_',$dir);

                    // recherche des repertoire DPX_kk_cc
                    if (strtolower($dir_element[0])=='dpx'){
                        $dir_dpx=$dir;
                        $dpx_files=scandir($repertoire.'/'.$dir_dpx);
                        //print_r($dpx_files);
                        foreach ($dpx_files as $file){
                            if ($file!='.' && $file!='..' && getExtension($file)=='dpx'){
                                if($nb_dpx==0){
                                    $identify=shell_exec('identify "'.$repertoire.'/'.$dir_dpx.'/'.$file.'"');
                                    $chunks=explode(' ',$identify);
                                    list($width,$height)=explode('x',$chunks[2]);
                                }
                                $nb_dpx++;
                            }
                        }
                    }

                    // dossiers contenant les prores
                    if (strtolower($dir_element[0])=='prores'){
                        $dir_prores=$dir;
                        $prores_files=scandir($repertoire.'/'.$dir_prores);
                        //print_r($prores_files);
                        foreach ($prores_files as $file){
                            if ($file!='.' && $file!='..' && getExtension($file)=='mov'){
                                $file_mov=$file;
                                // analyse fichier mov
                                $mat_info=new MatInfo();
                                $infos=$mat_info->getMatInfo($repertoire.'/'.$dir_prores.'/'.$file_mov);
                                //print_r($infos);
                                foreach($infos['tracks'] as $track)
                                {
                                    if (strtolower($track['type'])=='video'){
                                        $nb_fps=$track['frame_rate'];
                                        $duree=$track['duration'];
                                        if(empty($duree)){
                                            $this->dropError("Error $nom_dpx : empty duration $file_mov");
                                            $ok=false;
                                            $erreur_duree_mov=true;
                                        }
                                        else $duree_mov+=$duree;
                                    }elseif(strtolower($track['type'])=='audio'){
                                        $son_mov=true;
                                    }
                                }
                            }
                        }
                    }

                    // dossiers contenant les wav
                    if (strtolower($dir_element[0])=='wav'){
                        $dir_wav=$dir;
                        $wav_files=scandir($repertoire.'/'.$dir_wav);
                        //print_r($wav_files);
                        foreach ($wav_files as $file){
                            if ($file!='.' && $file!='..' && getExtension($file)=='wav'){
                                $file_wav=$file;
                                // analyse fichier wav
                                $mat_info=new MatInfo();
                                $infos=$mat_info->getMatInfo($repertoire.'/'.$dir_wav.'/'.$file_wav);
                                //print_r($infos);
                                $duree=$infos['duration'];
                                if(empty($duree)) {
                                    $this->dropError("Error $nom_dpx : empty duration $file_wav");
                                    $erreur_duree_wav=true;
                                    $ok=false;
                                } else $duree_wav+=$duree;
                            }
                        }
                    }
                }
            }
            closedir($dh);

            // Analyse prores
            if(!empty($nom_prores) && is_file($nom_prores)){
                $mat_info=new MatInfo();
                $infos=$mat_info->getMatInfo($nom_prores);
                //print_r($infos);
                foreach($infos['tracks'] as $track){
                    if (strtolower($track['type'])=='video'){
                        $nb_fps=$track['frame_rate'];
                        $duree=$track['duration'];
                        if(empty($duree)){
                            $this->dropError("Error $nom_dpx : empty duration $nom_prores");
                            $ok=false;
                            $erreur_duree_mov=true;
                        }
                        else $duree_mov+=$duree;
                    }elseif(strtolower($track['type'])=='audio'){
                        $son_mov=true;
                    }
                }
            }

            // Retour informations
            $infos_dpx['height']=$height;
            $infos_dpx['width']=$width;
            $infos_dpx['nbFrames']=$nb_dpx;
            if($nb_fps > 0) $infos_dpx['duration']=$nb_dpx / $nb_fps;
            // Track Video
            $infos_dpx['tracks'][]=array('type'=>'Video','format'=>'DPX','duration'=>$infos_dpx['duration'],'height'=>$infos_dpx['height'],'width'=>$infos_dpx['width'],'frame_rate'=>$nb_fps);
            // Track Audio
            if($duree_wav>0){
            $infos_dpx['tracks'][]=array('type'=>'Audio','format'=>'WAV','duration'=>$duree_wav);
            }
            // Nb frames mov
            $nbframes_mov=$duree_mov*$nb_fps;

            // Vérification
            if($nb_dpx==0){
                $ok=false;
                $this->dropError("Error $nom_dpx : nb frames dpx = 0");
            }
            if($nbframes_mov==0){
                $ok=false;
                $this->dropError("Error $nom_dpx : nb frames mov = 0");
            }
            if(abs($nb_dpx-$nbframes_mov)>60){
                $ok=false;
                $this->dropError("Error $nom_dpx : nb frames dpx ($nb_dpx) != mov ($nbframes_mov)");
            }
            if($son_mov && $duree_wav==0){
                $ok=false;
                $this->dropError("Error $nom_dpx : missing wav");
            }
            if(!$son_mov && $duree_wav>0){
                $ok=false;
                $this->dropError("Error $nom_dpx : missing track audio in mov");
            }
            if($son_mov && abs($duree_mov-$duree_wav)>4){
                $ok=false;
                $this->dropError("Error $nom_dpx : duration mov ($duree_mov) != wav ($duree_wav)");
            }
        }else{
            $this->dropError("Error opening directory: ".$repertoire);
            $ok=false;
        }

        if($ok){
            trace($nom_dpx.": OK");
        } else {
            trace(str_replace("<br/>","\n",$this->error_msg));
        }

        return $ok;
    }

	public function ingestDpxFolder($repertoire, $newName="", $renameIfExist=true ,$arr_mat=null)
	{
        require_once(libDir."class_matInfo.php");
		$dir_dpx='';
		$dir_prores='';
		$dir_wav='';

		$resolution='';
		$couleur='';

		if($dh=opendir($repertoire))
		{
			while ($file=readdir($dh))
			{
				if ($file !== '.' && $file !== '..' )
				{
					// recherche et concatenation fichiers wav
					/*if (getExtension($file)=='wav')
					{
						$wav_files[]=$file;
					}*/
					$files_element=explode('_',$file);

					// recherche des repertoire DPX_kk_cc
					if (strtolower($files_element[0])=='dpx')
					{
						$dir_dpx=$file;
						$resolution=$files_element[1];
						$couleur=$files_element[2];
					}

					// dossiers contenant les prores
					if (strtolower($files_element[0])=='prores')
					{
						$dir_prores=$file;
					}

					if (strtolower($files_element[0])=='wav')
					{
						$dir_wav=$file;
					}
				}
			}
			closedir($dh);
		}

		// detection ssh ffmpeg
		if (strstr(kFFMPEGpath,'ssh')!==false)
			$ssh_ffmpeg=true;
		else
			$ssh_ffmpeg=false;

		if ($ssh_ffmpeg)
		{
			$tmp=explode(' ',trim(kFFMPEGpath));
			$exec_ffmpeg=$tmp[0].' '.$tmp[1].' "'.$tmp[2].' %s"';
		}
		else
			$exec_ffmpeg=kFFMPEGpath.' %s';

		// concatenation de fichier WAV
		// /usr/local/bin/ffmpeg-1.2.1 -i 1.wav -i 2.wav -filter_complex '[0:0][1:0] concat=n=2:v=0:a=1 [a]' -map '[a]' -f wav -acodec pcm_s16le -y 3.wav
		if (!empty($dir_wav))
		{
			$fichiers_wav=list_directory($repertoire.'/'.$dir_wav);

			if (!empty($fichiers_wav))
				$tmp_wav=$repertoire.'/tmp.wav';
			else
				$tmp_wav='';

            $nom_wav=$repertoire.'/'.$dir_wav.'/'.basename($repertoire).'.wav';

			if (count($fichiers_wav)>1)
			{
				$fichier_in_ffmpeg='';
				$filter_ffmpeg='';

				$num_fichier=0;
				foreach($fichiers_wav as $fichier)
				{
					$fichier_in_ffmpeg.=' -i '.escapeQuoteShell($fichier).'';
					$filter_ffmpeg.='['.$num_fichier.':0]';

					$num_fichier++;
				}

				$cmd=$fichier_in_ffmpeg.' -filter_complex \''.$filter_ffmpeg.' concat=n='.count($fichiers_wav).':v=0:a=1 [a]\' -map \'[a]\' -f wav -acodec pcm_s16le -y '.escapeQuoteShell($tmp_wav).'';


				if ($ssh_ffmpeg)
					$cmd=sprintf($exec_ffmpeg,str_replace('"','\"',$cmd));
				else
					$cmd=sprintf($exec_ffmpeg,$cmd);

				//echo $cmd."\n";
				shell_exec($cmd);

				foreach($fichiers_wav as $fichier)
					unlink($fichier);

				rename($tmp_wav,$nom_wav);
			}
            else if (count($fichiers_wav)==1)
            {
                // renommage fichier
                rename($fichiers_wav[0],$nom_wav);
            }
		}

		// concatenation des fichiers prores
		// /usr/local/bin/ffmpeg-1.2.1 -i 1.mov -i 2.mov -filter_complex '[0:0][1:0] concat=n=2:v=1 [v]' -map '[v]' -f mov -vcodec prores -y 3.mov
		// /usr/local/bin/ffmpeg-1.2.1 -i 1.mov -i 2.mov -i 3.mov -filter_complex '[0:0][1:0][2:0] concat=n=3:v=1 [v]' -map '[v]' -f mov -vcodec prores -y 4.mov
		if (!empty($dir_prores))
		{
			$list_prores=list_directory($repertoire.'/'.$dir_prores);

			$fichiers_prores=array();
			foreach($list_prores as $file_tmp)
			{
				if (substr(basename($file_tmp),0,1)!='.') // suppression des fichier caches
					$fichiers_prores[]=$file_tmp;
			}

			if (!empty($fichiers_prores))
				$nom_prores=$repertoire.'/'.basename($repertoire).'.mov';
			else
				$nom_prores='';

			if (count($fichiers_prores)>1)
			{
				$fichier_in_ffmpeg='';
				$filter_ffmpeg='';

				$num_fichier=0;
				$has_audio=false;

				foreach ($fichiers_prores as $vid_prores)
				{
					$id_video=null;
					$id_audio=null;

					$mat_info=new MatInfo();
					$infos=$mat_info->getMatInfo($vid_prores);

					foreach ($infos['tracks'] as $track)
					{
						if ($id_video==null && strtolower($track['type'])=='video')
							$id_video=$track['index'];
						else if ($id_audio==null && strtolower($track['type'])=='audio')
						{
                            // VP 1/12/2014 : $id_audio déterminé par $track['index'] plutôt que par 2 en dûr
							$id_audio=$track['index'];
							//$id_audio=2;
							$has_audio=true;
						}
					}

					$fichier_in_ffmpeg.=' -i '.escapeQuoteShell($vid_prores).'';
					//filtre video
					$filter_ffmpeg.='['.$num_fichier.':'.$id_video.']';

					//filtre audio
					if ($id_audio!=null)
					{
						$filter_ffmpeg.='['.$num_fichier.':'.$id_audio.']';
					}

					$num_fichier++;
				}

				if ($has_audio)
					$cmd=$fichier_in_ffmpeg.' -filter_complex \''.$filter_ffmpeg.' concat=n='.count($fichiers_prores).':v=1:a=1 [v][a]\' -map \'[v]\' -map \'[a]\' -f mov -vcodec prores -y "'.$nom_prores.'"';
				else
					$cmd=$fichier_in_ffmpeg.' -filter_complex \''.$filter_ffmpeg.' concat=n='.count($fichiers_prores).':v=1 [v]\' -map \'[v]\' -f mov -vcodec prores -y "'.$nom_prores.'"';

				if ($ssh_ffmpeg)
					$cmd=sprintf($exec_ffmpeg,str_replace('"','\"',$cmd));
				else
					$cmd=sprintf($exec_ffmpeg,$cmd);

				//echo $cmd."\n";
				shell_exec($cmd);
				trace($cmd);

				// suppression de fichier prores
				foreach ($fichiers_prores as $vid_prores)
				{
					unlink($vid_prores);
				}

			}
			else if (count($fichiers_prores)==1)
			{
				// deplacement fichier vers repertoire parent
				rename($fichiers_prores[0],$nom_prores);
			}
		}


        // Vérification DPX
        $ok_dpx=$this->checkDpxFolder($repertoire, $infos_dpx, $nom_prores);

        // creation de l'archive tar
		if(defined('kBinTar') && kBinTar !="")
			$tar=kBinTar;
		else
			$tar="tar";

		$cmd_cd='cd '.escapeQuoteShell($repertoire).'';
		//$nom_archive='archive.tar';
		$nom_archive=''.basename($repertoire).'.tar';
		$cmd_tar=' cvf '.$nom_archive;

		if (!empty($dir_dpx))
			$cmd_tar.=' '.escapeQuoteShell($dir_dpx);

		if (!empty($dir_wav))
			$cmd_tar.=' '.escapeQuoteShell($dir_wav);

		/*echo $cmd_cd."\n";
		echo $cmd_tar."\n";*/


		if(strpos($tar,'ssh')!==false)
			$ssh=true;
		else
			$ssh=false;

		if ($ssh)
		{
			$tmp = explode(" ",trim($tar));
			$tar_ = $tmp[count($tmp)-1];
			$ssh_ = $tmp[0]." ".$tmp[1];

			$cmd=$ssh_.' "'.str_replace('"','\"',$cmd_cd).'; '.$tar_.str_replace('"','\"',$cmd_tar).'"';
		}
		else
			$cmd=$cmd_cd.'; '.$tar.$cmd_tar;

		//echo $cmd;
		shell_exec($cmd);

		// ingest du prores
		if (isset($nom_prores) && !empty($nom_prores))
		{
			$mat_prores=new Materiel();
			//$mat_prores->ingest($repertoire.'/'.$nom_prores);
			$mat_prores->ingest($nom_prores, $arr_mat, $newName, $renameIfExist , $infos, null, true);
			$mat_prores->id_mat_ref=$mat_prores->t_mat['MAT_NOM'];
		}

		// ingest du tar
		$mat_tar=new Materiel();
		//$mat_tar->ingest($repertoire.'/'.$nom_archive);
		$mat_tar->ingest($repertoire.'/'.$nom_archive, $arr_mat, $newName, $renameIfExist , $infos, null, true);
		$mat_tar->id_mat_ref=$mat_tar->t_mat['MAT_NOM'];
		$mat_tar->getMat();

		$idx_val=0;
		$arr_vals=array();
		foreach($arr_mat as $key=>$value)
		{
			$elt_key=explode('_',$key);
            if ($elt_key[0]=='DMV')
			{
				$myVal=new Valeur();
				$myVal->t_val['VALEUR']=$value;
				$myVal->t_val['VAL_ID_TYPE_VAL']=strtoupper($elt_key[1]);
				$myVal->t_val['ID_LANG']=$_SESSION['langue'];
				$_val=$myVal->checkExist();
				if ($_val)
				{
					$myVal->t_val['ID_VAL']=$_val;
					$myVal->save();
				}
				else
				{
					$myVal->create(); //création dans la langue
					foreach ($_SESSION['arrLangues'] as $lg)
					{
						if ($lg!=$_SESSION['langue'])
							$arrVersions[$lg]=$val;
					}
					$myVal->saveVersions($arrVersions); //sauve dans autres langues
					unset($arrVersions);
				}

				$arr_vals[$idx_val]=array();
				$arr_vals[$idx_val]['ID_VAL']=$myVal->t_val['ID_VAL'];
				$arr_vals[$idx_val]['ID_TYPE_VAL']=strtoupper($elt_key[1]);
				$idx_val++;
			}
		}

        if(isset($arr_mat['MAT_FORMAT']) && !empty($arr_mat['MAT_FORMAT'])) $mat_tar->t_mat['MAT_FORMAT']=$arr_mat['MAT_FORMAT'];

		if (isset($nom_prores) && !empty($nom_prores))
		{
			$mat_tar->t_mat['MAT_TCIN']=$mat_prores->t_mat['MAT_TCIN'];
			$mat_tar->t_mat['MAT_TCOUT']=$mat_prores->t_mat['MAT_TCOUT'];
		}
        if($infos_dpx){
            if($ok_dpx===true) $mat_tar->t_mat['MAT_ID_STATUT']=1;
            elseif($ok_dpx===false) $mat_tar->t_mat['MAT_ID_STATUT']=-1;

            $mat_tar->t_mat['MAT_HEIGHT']=$infos_dpx['height'];
            $mat_tar->t_mat['MAT_WIDTH']=$infos_dpx['width'];
            $mat_tar->t_mat['MAT_DUREE']=secToTime($infos_dpx['duration']);
            $mat_tar->t_mat['MAT_NB_PAGES']=$infos_dpx['nbFrames'];
            // Tracks
            $mat_tar->getMatTrack();
            $mat_tar->saveMatTrack($infos_dpx);
        }
        $mat_tar->save();

		// association des materiels au document
		if (isset($nom_prores) && !empty($nom_prores))
		{
			$mat_prores->t_doc_mat[0]["ID_MAT"]=$mat_prores->t_mat['ID_MAT'];
			$mat_prores->t_doc_mat[0]["ID_DOC"]=$arr_mat['id_doc'];
			$mat_prores->t_doc_mat[0]['DMAT_TCIN']=$mat_prores->t_mat['MAT_TCIN'];
			$mat_prores->t_doc_mat[0]['DMAT_TCOUT']=$mat_prores->t_mat['MAT_TCOUT'];
			$mat_prores->t_doc_mat[0]['t_doc_mat_val']=$arr_vals;
			$ok=$mat_prores->saveDocMat();
		}

		$mat_tar->t_doc_mat[0]["ID_MAT"]=$mat_tar->t_mat['ID_MAT'];
		$mat_tar->t_doc_mat[0]["ID_DOC"]=$arr_mat['id_doc'];
		$mat_tar->t_doc_mat[0]['DMAT_TCIN']=$mat_tar->t_mat['MAT_TCIN'];
		$mat_tar->t_doc_mat[0]['DMAT_TCOUT']=$mat_tar->t_mat['MAT_TCOUT'];
		$mat_tar->t_doc_mat[0]['t_doc_mat_val']=$arr_vals;
		$ok=$mat_tar->saveDocMat();

		// suppression des fichiers dpx
		$fichiers_dpx=list_directory($repertoire.'/'.$dir_dpx);

		foreach ($fichiers_dpx as $fichier)
			unlink($fichier);

		rmdir($repertoire.'/'.$dir_dpx);

		// suppression du repertoire contenant les prores
		if (!empty($dir_prores))
			rmdir($repertoire.'/'.$dir_prores);

		//suppression du wav et de son repertoire
		if (!empty($dir_wav))
		{
			$fichiers_wav=list_directory($repertoire.'/'.$dir_wav);

			foreach ($fichiers_wav as $fichier)
				unlink($fichier);

			rmdir($repertoire.'/'.$dir_wav);
		}

		rmdir($repertoire);

		return array($mat_prores,$mat_tar);
	}

	public function getFormatMat($format)
	{
		if (!empty($format))
		{
			global $db;
			$result=$db->Execute('SELECT * FROM t_format_mat WHERE FORMAT_MAT=\''.$format.'\'')->getRows();
			return $result[0];
		}
		else
			return null;
	}

    /**
     * Création d'un fichier tar pour les dossiers contenant plus de 100 fichiers
     *   et dont l'extension est bmp, dpx, hsr, jpg, png
     */
     private function tarFilesByExtension($repertoire){
        $extensions=array('bmp', 'dpx', 'hsr', 'jpg', 'png','tga');
        $files=scandir($repertoire);
        $nb_ext=array();
        foreach($files as $file){
            $true_ext=getExtension($repertoire.$file,true);
            $ext = strtolower($true_ext);
			if(in_array($ext,$extensions)) $nb_ext[$ext]["count"]++;
			if(!isset($nb_ext[$ext]["variantes"]) || !in_array($true_ext, $nb_ext[$ext]["variantes"]))$nb_ext[$ext]["variantes"][] = $true_ext;
        }
        if(count($nb_ext)>0){
            foreach($nb_ext as $ext=>$arr){
// VP 29/08/13 : tarFilesByExtension de >100 à >500
//                if($nb>100){
                if($arr['count']>500){
                    // Constitution du tar
                    $tarFile=$ext."_files.tar";
                    if(defined('kBinTar') && kBinTar !="") $tar=kBinTar;
                    else $tar="tar";

					if(strpos($tar,'ssh')!==false){
						$ssh = true ;
					}else{
						$ssh = false ;
					}

                    // VP 29/08/2013 : tar avec extensions minuscules et majuscules
                    if($ssh){
						$tmp = explode(" ",trim($tar));
						$tar_ = $tmp[count($tmp)-1];
						$ssh_ = $tmp[0]." ".$tmp[1];

						$cmd=$ssh_." \"cd \\\"".str_replace('"','\\\\\"',$repertoire)."\\\"; ".$tar_." -cf ".($tarFile)." ";
						foreach($arr['variantes'] as $idx=>$var_ext){
							$cmd.="*.".$var_ext." ";
						}
						$cmd.="\"";
					}else{
						$cmd="cd ".escapeQuoteShell($repertoire)."; ".$tar." -cf ".($tarFile)." ";
						foreach($arr['variantes'] as $idx=>$var_ext){
							$cmd.="*.".$var_ext." ";
						}
					}
                    exec($cmd, $array = array(), $return);
                    trace($cmd." -> ".$return);
                    if($return==0 && is_file($repertoire.$tarFile)){
                        // Pas d'erreur, on peut supprimer les fichiers
                        if (!eregi('WINNT',PHP_OS))  $cmd="cd ".escapeQuoteShell($repertoire).";rm -f *.".$ext.";rm -f *.".strtoupper($ext);
                        else  $cmd="cd ".escapeQuoteShell($repertoire).";del *.".$ext.";del *.".strtoupper($ext);
					   trace($cmd);
                        exec($cmd);
                    }
                }
            }
        }
    }

    function ingestFolder($repertoire,$new_name='',$rename_if_exist=true,$arr_mat=null,&$aIdMatCreated=array())
    {
        if (substr($repertoire,-1)!='/')
            $repertoire.='/';

		// XB: dossier XD CAM ou GOPRO
		if($dh=opendir($repertoire))
		{
			$cpt_xd=0;
			$cpt_gopro=0;
			$cpt_dcp=0;
			$cpt_dpx_files=0;
			$cpt_dpx_files_sub=0;
			$cpt_wav_files=0;
			$cpt_dcp_unwrap=0;
			while ($file=readdir($dh))
			{
				if ($file!='.' && $file!='..'){
					//dossier xdcam
					if($file=='Clip' ||$file=='Edit' || $file=='Sub' || $file=='MEDIAPRO.XML' || $file=='DISCMETA.XML' )
						$cpt_xd += 1;	
						//dossier GOPRO
					else if(  $file=='MISC' || $file=='PRIVATE' || $file=='DCIM'){
							$cpt_gopro += 1;	
							//si dossier DCIM, on regarde si il contient go pro	
							if($file==	'DCIM')
								{
									$rep=$repertoire.'/'.$file; 
									if($d=opendir($rep))
										while ($file=readdir($d))
										{
											if ($file!='.' && $file!='..')
												if($file=='100GOPRO')					
													$cpt_gopro += 1;
											
										}			
								}	
					}else if($file == 'ASSETMAP' || $file == 'VOLINDEX'){
						$cpt_dcp++;
					}else if($file == 'project.dcpproj'){
						$cpt_dcp_unwrap++;
					}else if(strtolower(getExtension($file)) == 'wav'){
						$cpt_wav++;
					}else if(in_array(strtolower(getExtension($file)),array('dpx','jpg','j2c'))){
						$cpt_dpx_files++;
					}else if(is_dir($repertoire.'/'.$file)){
						$dir_dpx=$file;
						$dpx_files=scandir($repertoire.'/'.$dir_dpx);
						$nb_dpx = 0 ; 
						foreach ($dpx_files as $file_){
							if ($file_!='.' && $file_!='..' && in_array(strtolower(getExtension($file_)),array('dpx','jpg','j2c'))){
								$nb_dpx++;
							}
						}
						$cpt_dpx_files_sub+=$nb_dpx;
					}
				}		
			}
			if ($cpt_xd == 5) $type_dossier='XDCAM';
			if ($cpt_gopro == 4) $type_dossier='GOPRO';
			if ($cpt_dcp >= 2) $type_dossier='DCP';
			if ($cpt_dcp_unwrap == 1) $type_dossier='DCP_unwrap';
			if ($cpt_dpx_files_sub >= 100) $type_dossier='DPX_main';
			if ($cpt_dpx_files >= 100) $type_dossier='DPX_dir';
		}

        $nom_origine=str_replace('/','',str_replace(dirname($repertoire),'',$repertoire));
        // trace('repertoire : '.$repertoire);
        // trace('nom_origine : '.$nom_origine);
        //on genere le nouveau nom du dossier de rushes
        if (!empty($new_name))
        {
            if ($rename_if_exist===true)
            {
                $exists=false;
                do
                {
                    // VP 31/10/2012 : utilisation checkExist et checkOneFileExists
                    if (isset($arr_mat) && !empty($arr_mat) && $arr_mat!=null)
                        $this->updateFromArray($arr_mat);
                    $this->t_mat['MAT_NOM']=$new_name;
                    $exists=$this->checkExist(false); // false pour faire une recherche exacte sur le nom
                    if(!$exists) $exists=$this->checkOneFileExists();
                    if($exists) $new_name=stringIteration($new_name);
//                    if (file_exists(kVideosDir.$new_name))
//                        {
//                        $exists=true;
//                        $new_name=stringIteration($new_name);
//                        }
//                    else
//                        $exists=false;
                }
                while ($exists);
            }
        }
        else {
			$new_name=getDirFromPath($nom_origine);
			// $new_name=$nom_origine;
        }
        // creation du materiel du dossier de rushes
        //creation du repertoire
        //$mat_parent=new Materiel();
        $this->t_mat['MAT_NOM']=$new_name;
        $this->id_mat_ref=$new_name;
        $this->t_mat['MAT_NOM_ORI']=$nom_origine;
        // trace('folder : '.$repertoire.', file '.$nom_origine.' : new_name : '.$new_name);

        if (isset($arr_mat) && !empty($arr_mat) && $arr_mat!=null)
            $this->updateFromArray($arr_mat);
        // trace('getFilePath : '.$this->getFilePath());
        mkdir($this->getFilePath());
        $this->getMediaType();
        $this->t_mat['MAT_ID_MEDIA']=$this->id_media;
		if(isset($type_dossier)) {
			if ($type_dossier == "XDCAM") $this->t_mat['MAT_FORMAT']='XDCAM HD';
			elseif ($type_dossier == "GOPRO") $this->t_mat['MAT_FORMAT']='GOPRO';
			elseif ($type_dossier == "DCP" || $type_dossier == 'DCP_unwrap'){
				$this->t_mat['MAT_FORMAT']='DCP';
				$this->t_mat['MAT_TYPE']='MASTER';
				$this->buildDCPMap($repertoire);
			}elseif ($type_dossier == "DPX_main"){
				$this->t_mat['MAT_FORMAT'] = 'DPX';
				$this->t_mat['MAT_TYPE']='MASTER';
				$this->buildDPXMap($repertoire);
				$this->t_mat['MAT_INFO'] =  tab2xml($this->DPX_map);
			}
		}
		else  $this->t_mat['MAT_FORMAT']='DOSSIER';

	   $this->save();

		
		if(isset($this->DPX_map) && isset($this->DPX_map['tracks'][0]['duration'])){
			$this->t_mat['MAT_TCOUT'] = secDecToTc($this->DPX_map['tracks'][0]['duration'],(isset($this->DPX_map['frame_rate'])?$this->DPX_map['frame_rate']:25));
		}
		
		if(isset($this->DPX_map) && isset($this->DPX_map['tracks'])){
			$this->saveMatTrack($this->DPX_map);
		}
		
        $taille=0;
	    $this->ingestFilesInDir($repertoire,$repertoire,$arr_mat,$taille,$aIdMatCreated);
        // VP 9/10/12 : mise à jour taille et état archivage*/
        $this->t_mat['MAT_TAILLE']=$taille;
        $this->save();
        //$this->updateEtatArch(); // Fait dans finUpload

		// MS 16/09/13 : réactivation suppression des repertoires importés uniquement si ils sont vides, ou ne contenaient que des fichiers cachés.
		Materiel::viderDirRecursive($repertoire,true);
		if(count(scandir($repertoire))==2){
			rmdir($repertoire);
		}

        return true;
    }

	function getIdMatsEnfants($onlyMaster=NULL)
	{
		//XB XDCAM ou GOPRO on ne sélectionne pas toutes les lignes
		global $db;
		if(!empty($onlyMaster) && strpos($onlyMaster,'XDCAM')!==false){
			//seul fichier V et format MXF dont le chemin finit par /clip et id_mat_gen est XDCAM HD
			$res=$db->Execute("SELECT ID_MAT FROM t_mat WHERE MAT_ID_GEN=".intval($this->t_mat['ID_MAT'])." AND ".$db->getExtensionSQL("MAT_NOM")."='MXF'  AND MAT_ID_MEDIA='V' AND MAT_CHEMIN LIKE '%/Clip' AND EXISTS (SELECT ID_MAT FROM t_mat WHERE ID_MAT=".intval($this->t_mat['ID_MAT'])." AND MAT_FORMAT='XDCAM HD')" )->getRows();

		}
		elseif(!empty($onlyMaster) && $onlyMaster=='GOPRO'){
			//seul fichier V et format mp4 dont le chemin finit par /DCIM/GOPRO et id_mat_gen est GOPRO 
			$res=$db->Execute("SELECT ID_MAT FROM t_mat WHERE MAT_ID_GEN=".intval($this->t_mat['ID_MAT'])." AND MAT_ID_MEDIA='V' AND ".$db->getExtensionSQL("MAT_NOM")."='MP4'  AND MAT_CHEMIN LIKE '%/DCIM/100GOPRO' AND EXISTS (SELECT ID_MAT FROM t_mat WHERE ID_MAT=".intval($this->t_mat['ID_MAT'])." AND MAT_FORMAT='GOPRO')" )->getRows();

		}
		else{
			$res=$db->Execute('SELECT ID_MAT FROM t_mat WHERE MAT_ID_GEN='.intval($this->t_mat['ID_MAT']))->getRows();
		}
		$list_id_mat=array();
		foreach($res as $id_mat)
		{
			$list_id_mat[]=$id_mat['ID_MAT'];
		}

		return $list_id_mat;
	}

    /**
     * Récupère les ID des matériels fils
     * IN : rien
     * OUT : tab de classe arrChildren
     */

    function getChildren() {
        global $db;
        $sql="SELECT ID_MAT, MAT_LIEU, MAT_NOM, MAT_CHEMIN, MAT_TAILLE, MAT_FORMAT from t_mat WHERE MAT_ID_GEN=".intval($this->t_mat['ID_MAT']);
        $this->arrChildren=$db->GetAll($sql);
        // Conversion taille
        foreach($this->arrChildren as &$child){
             $child['TAILLE']=convertFileSize($child["MAT_TAILLE"]);
        }
    }


	//PC 21/02/11 : ajout du format matériel si il n'existe pas en base
	function createFormatMat() {
		global $db;
		if (empty($this->t_mat['MAT_FORMAT'])){
			$this->t_mat['MAT_FORMAT'] = strtoupper(getExtension($this->t_mat['MAT_NOM']));
		}
		$format_id_media = $this->t_mat['MAT_ID_MEDIA'];
		$format_mat = $this->t_mat['MAT_FORMAT'];
		$format_file_ext = getExtension($this->t_mat['MAT_NOM']);

		//$sql = "select FORMAT_MAT from t_format_mat where FORMAT_MAT like '$format_mat' and FORMAT_ID_MEDIA like '$format_id_media'";
		$sql = 'select FORMAT_MAT from t_format_mat where FORMAT_MAT='.$db->Quote($format_mat);
		$res = $db->getOne($sql);

		// MS on ne veut rajouter le format à format_mat que si la requete a renvoyé null (pas de row correspondant), si le resultat est false (erreur requete), ou que le champ retourné est vide (''),
		// alors on ne veut pas insérer le champ ==> test sur is_null
		if(is_null($res)){
			$arrFormatMat = array("FORMAT_MAT" => $format_mat, "FORMAT_ID_MEDIA" => $format_id_media, "FORMAT_FILE_EXT" => $format_file_ext);
			$ok =  $db->insertBase("t_format_mat","FORMAT_MAT",$arrFormatMat );
		}
	}


	static function getMatByMatNom($mat_nom, $mat_chemin = null)
	{
		global $db;

		if (isset($mat_nom) && !empty($mat_nom))
		{
			$sql = "SELECT ID_MAT FROM t_mat WHERE MAT_NOM=".$db->Quote(addslashes($mat_nom));
			if (isset($mat_chemin) && !empty($mat_chemin))
				$sql .= " AND MAT_CHEMIN=".$db->Quote($mat_chemin);
			$res=$db->Execute($sql)->getRows();

			if (count($res)>0)
			{
				$mat=new Materiel();
				$mat->t_mat['ID_MAT']=$res[0]['ID_MAT'];
				$mat->getMat();

				return $mat;
			}
		}

		return null;
	}


	// MS getFileInfo supprimée, remplacée par l'utilisation de la class_matInfo & getMatInfo


	// MS 16/09/13 -
	// ajout paramètre : $del_only_if_empty,
	//	si false (default), supprime récursivement tout les fichiers & dossiers,
	// 	si true, on ne supprime que les fichiers cachés & les repertoires qui sont vides.

	public static function viderDirRecursive($filename,$del_only_if_empty=false)
	{
		debug("call viderDirRecursive sur ".$filename."<br />");
		if (file_exists($filename) && is_dir($filename))
		{
			$dh=opendir($filename);

			while (($file=readdir($dh))!==false)
			{
				if ($file!='.' && $file!='..')
				{
					if (is_dir($filename.'/'.$file))
					{
						Materiel::viderDirRecursive($filename.'/'.$file,$del_only_if_empty);
						if(!$del_only_if_empty || ($del_only_if_empty && count(scandir($filename.'/'.$file))==2)){
							rmdir($filename.'/'.$file);
							debug("rmdir : ".$filename.'/'.$file."<br />");
						}
					}
					else if(!$del_only_if_empty || ($del_only_if_empty && substr($file,0,1)=='.')){
						unlink($filename.'/'.$file);
						debug("unlink file : ".$filename.'/'.$file."<br />");
					}
				}
			}

			closedir($dh);

			return true;
		}

		return false;
	}

    //XB param id_base : id_doc , si défini rattache le matériel à cette fiche
	public function makeDocFromMat($myPrms,$infos=null,$id_doc2link=null,&$objpanier=null,$id_base=NULL){
		if(function_exists("displayMsg")){
			displayMsg(kNouveauDoc."...");
		}
		require_once(modelDir.'model_doc.php');

		$myDoc=new Doc;

		// $fileInfo=explode("\.",$myPrms["file_".$index."_original"]); //on prend le file_original qui contient les accents, etc
		$fileInfo=explode("\.",$this->t_mat['MAT_TITRE']); //on prend le file_original qui contient les accents, 
		
		$myDoc->t_doc['DOC_ID_MEDIA']=(!empty($this->id_media)?$this->id_media:$this->t_mat['MAT_ID_MEDIA']);
		$myDoc->t_doc['DOC_TITRE']=($myPrms['prefix']?$myPrms['prefix']."_":"").$fileInfo[0]; //par déft, nom original sans ext
		$myDoc->t_doc['DOC_COTE']=substr(stripExtension($this->t_mat['MAT_NOM']),0,80); //Par déft, TITRE=COTE=ID_MAT sans extension
		if(isset($this->t_mat['MAT_ID_IMPORT'])){
			$myDoc->t_doc['DOC_ID_IMPORT'] = $this->t_mat['MAT_ID_IMPORT'] ; 
		}
		
		if(!defined("gNoDefaultDocDateProd") || !gNoDefaultDocDateProd){
			if(defined("gDefaultFmtDocDateProd")) $myDoc->t_doc['DOC_DATE_PROD']=date(gDefaultFmtDocDateProd);
			else $myDoc->t_doc['DOC_DATE_PROD']=date("Y-m-d"); //par déft, date du jour
		}
		$myDoc->t_doc['ID_LANG']=$_SESSION['langue'];

		// Tags ID3/EXIF : Traitement xsl
		if(!empty($myPrms["xsl"]) && file_exists(getSiteFile("designDir","print/".$myPrms["xsl"].".xsl"))){ //Formulaire de mapping passé par le form d'upload?
            // VP 4/06/2015 : passage export mat à la place de $infos['xml']
			//flag pour mapping 
			//$mapping=1;
            $datas_xml=$this->xml_export();
            $log = new Logger("makeDocFromMat.xml");
            $log->Log($datas_xml);
            $data = TraitementXSLT($datas_xml,getSiteFile("designDir","print/".$myPrms["xsl"].".xsl"),array(),0);
			$mappedInfos=xml2tab($data);
			debug($mappedInfos,'plum');
			if ($myPrms['getInfos']) {
				//PC 18/01/18 : sécurité pour éviter d'écraser les données récupérées du mapping
				foreach ($mappedInfos['DATA'][0]['DOC'][0] as $field=>$val)
					if (isset($myPrms[$field]) && empty($myPrms['getInfosForm']))
						unset($myPrms[$field]);
				$myDoc->mapFields($mappedInfos['DATA'][0]['DOC'][0]);
			}
		}

		
		
		// MS 25-09-2014 => report comportement docSaisie, 
		//	=> permet de proposer des multifield sur le formulaire d'upload
		//	=> pour l'instant fait AVANT le mapfields ,
		//  => à reporter au besoin pour le cas t_doc_val, t_pers_lex
		if (!empty($myPrms['t_doc_lex'])){
			foreach ($myPrms['t_doc_lex'] as $idx=>$lex) {
				if (isset($lex['ID_LEX']) || isset($lex['ID_PERS'])) {
					$lastID=$idx;
					$myPrms['t_doc_lex'][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
					if(isset($lex_terme)) {$myPrms['t_doc_lex'][$idx]['LEX_TERME']=$lex_terme;$myPrms['t_doc_lex'][$idx]['ID_LEX']=0;unset($lex_terme);}
					if(isset($pers_nom)) {$myPrms['t_doc_lex'][$idx]['PERS_NOM']=$pers_nom;$myPrms['t_doc_lex'][$idx]['ID_PERS']=0;unset($pers_nom);}
				}
				if (isset($lex['LEX_TERME'])) {$lex_terme=$lex['LEX_TERME'];unset($myPrms['t_doc_lex'][$idx]);}
				if (isset($lex['PERS_NOM'])) {$pers_nom=$lex['PERS_NOM'];unset($myPrms['t_doc_lex'][$idx]);}
				if (isset($lex['DLEX_ID_ROLE'])) {$myPrms['t_doc_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($myPrms['t_doc_lex'][$idx]);}
				if (isset($lex['ID_TYPE_LEX'])) {$myPrms['t_doc_lex'][$lastID]['ID_TYPE_LEX']=$lex['ID_TYPE_LEX'];unset($myPrms['t_doc_lex'][$idx]);}
				if (isset($lex['PERS_ID_TYPE_LEX'])) {$myPrms['t_doc_lex'][$lastID]['PERS_ID_TYPE_LEX']=$lex['PERS_ID_TYPE_LEX'];unset($myPrms['t_doc_lex'][$idx]);}
				if (isset($lex['DLEX_REVERSEMENT'])) {$myPrms['t_doc_lex'][$lastID]['DLEX_REVERSEMENT']=$lex['DLEX_REVERSEMENT'];unset($myPrms['t_doc_lex'][$idx]);}
				if (isset($lex['LEX_PRECISION'])) {$myPrms['t_doc_lex'][$lastID]['LEX_PRECISION']=$lex['LEX_PRECISION'];unset($myPrms['t_doc_lex'][$idx]);}
				if (isset($lex['ID_TYPE_DESC'])) {$myPrms['t_doc_lex'][$lastID]['ID_TYPE_DESC']=$lex['ID_TYPE_DESC'];unset($myPrms['t_doc_lex'][$idx]);}
			}
            if (empty($myDoc->t_doc_lex))
                $myDoc->t_doc_lex = $myPrms['t_doc_lex'];
            else
                $myDoc->t_doc_lex=array_merge($myDoc->t_doc_lex,$myPrms['t_doc_lex']);
		}

		// MS - 08.06.15 -  => report comportement docSaisie, adaptation t_doc_val
		//	=> permet de proposer des multifield sur le formulaire d'upload
		//	=> pour l'instant fait AVANT le mapfields ,
		if (!empty($myPrms['t_doc_val'])){
			// GT 02/11/10 : si la valeur ID_VAL n'existe pas lastID n'a pas de valeur
			// VG 31/05/11 : $lastId=0 => $lastId=-1, pour corriger le bug de mise à  jour lorsque toutes les valeurs d'un type sont supprimées
			$lastID=-1;
			foreach ($myPrms['t_doc_val'] as $idx=>$val) {
				if (isset($val['ID_VAL'])) {
					$lastID=$idx;
				}
				if (isset($val['VALEUR'])) {$valeur=$val['VALEUR'];unset($myPrms['t_doc_val'][$idx]);}
				if (isset($val['ID_TYPE_VAL'])){
					if(!isset($myPrms['t_doc_val'][$lastID]['ID_TYPE_VAL'])) {$myPrms['t_doc_val'][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];}
					unset($myPrms['t_doc_val'][$idx]);
				}
			}
			$myDoc->t_doc_val = $myPrms['t_doc_val'];
		}
		// MS 04.04.16 - => report comportement docSaisie, adaptatiojn t_doc_cat
		if (!empty($myPrms['t_doc_cat']))
		{
			foreach($myPrms['t_doc_cat'] as $idx=>$cat)
			{
				if (!empty($cat['ID_CAT'])){
					$lastID=$idx;
					// $myPrms['t_doc_cat'][$lastID]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
				}
				if (isset($cat['ID_TYPE_CAT'])) {$myPrms['t_doc_cat'][$lastID]['ID_TYPE_CAT']=$cat['ID_TYPE_CAT'];unset($myPrms['t_doc_cat'][$idx]);}
			}
			$myDoc->t_doc_cat = $myPrms['t_doc_cat'];
		}
		
		// Ajout des champs du formulaire seulement si mapping pas deja fait dans la xsl
		// if(empty($mapping)){
			$myDoc->mapFields($myPrms);
		// }

		if ($myPrms['doc_trad']) $myDoc->doc_trad=1; //sauve les autres versions ?

		if (isset($id_doc2link) && !empty($id_doc2link) && is_numeric(intval($id_doc2link))) $myDoc->id_doc2link = intval($id_doc2link);

		// echo "MS id_doc2link : ".$myDoc->id_doc2link."<br />";

		debug($myDoc,'silver');
		//$okDoc=$myDoc->save(0,false); //mode maj si doc existe déjà
		
		
		$okDoc=true;
	//XB si identifiant id_doc passé, on rattache le matériel a une fiche	
	if(isset($id_base) && $id_base!='' && $id_base!=NULL){
		$myDoc->t_doc['ID_DOC']=$id_base;
	}
	else 
	{
		if(!$myDoc->checkExist()){
			$okDoc=$myDoc->save();
			if ($okDoc) {
				$myDoc->saveDocLex();
				$myDoc->saveDocVal();
				$myDoc->saveDocCat();
				$myDoc->updateChampsXML('DOC_LEX');
				$myDoc->updateChampsXML('DOC_VAL');
				$myDoc->updateChampsXML('DOC_XML');
				$myDoc->updateChampsXML('DOC_CAT');

				//Héritage reportage
				require_once(modelDir.'model_docReportage.php');
				if (Reportage::isReportageEnabled()/* && isset($myDoc->arrDocLiesSRC) && !empty($myDoc->arrDocLiesSRC) && is_array($myDoc->arrDocLiesSRC)*/)
					$myDoc->heritFieldsToReport();
					foreach ($myDoc->arrDocLiesSRC as $idx=>&$dl)
						if (!empty($dl['ID_DOC_DST']) && Reportage::checkReportageWithId($dl['ID_DOC_DST'])) {
							$myRep = new Reportage;
							$myRep->t_doc['ID_DOC']=intval($dl['ID_DOC_DST']);
							$myRep->heritReportFields();
						}

				if(isset($objpanier) && !empty($objpanier)){
					$objpanier->insertLigne($myDoc->t_doc['ID_DOC']);
				}
			}
		}
	}	
		
		if ($okDoc) {
			$myDoc->metAJourDocNum($this->t_mat['ID_MAT']);
			$myDoc->t_doc_mat[0]['ID_MAT']=$this->t_mat['ID_MAT']; //lien doc_mat
			$myDoc->t_doc_mat[0]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
			$myDoc->t_doc_mat[0]['MAT_FORMAT']=$this->t_mat['MAT_FORMAT'];
			$myDoc->t_doc_mat[0]['DMAT_TCIN']=$this->t_mat['MAT_TCIN'];
			$myDoc->t_doc_mat[0]['DMAT_TCOUT']=$this->t_mat['MAT_TCOUT'];
			if (!empty($myPrms['DOC_ID_LANG'])) $myDoc->t_doc_mat[0]['DMAT_ID_LANG']=$myPrms['DOC_ID_LANG'];
			$myDoc->saveDocMat();
			$myDoc->saveDocMatForSequences($this->t_mat['ID_MAT']);

			//L'extration des tags ID3 contenant un cover art ?
			//On le sauve comme document d'accompagnement du doc
			if ($infos['image_path'] && is_file(kDocumentDir.$infos['image_path'])) {
				require_once(modelDir.'model_docAcc.php');
				$myDA=new DocAcc();
				$myDA->t_doc_acc['DA_FICHIER']=$infos['image_path'];
				$myDA->t_doc_acc['DA_TITRE']=$infos['image_path'];
				$myDA->t_doc_acc['ID_DOC']=$myDoc->t_doc['ID_DOC'];
				$myDA->save();
				foreach ($_SESSION['arrLangues'] as $lg) { //on sauve pr ttes les versions
					$myDA->t_doc_acc['ID_LANG']=$lg;
					$myDA->create(); //note => le même ID_DOC_ACC est utilisé pr tous
				}
				$myDoc->saveVignette('doc_acc',$myDA->t_doc_acc['ID_DOC_ACC']);

			}
            if (defined("useSinequa") && useSinequa) $myDoc->saveSinequa();
            if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))
				$myDoc->saveSolr();

            if(function_exists("displayMsg")){
				displayMsg(kSuccesDocSauve);
			}
		}
		else {
			if(function_exists("displayError")){
				displayError(kErrorSauveDoc." : ".$myDoc->error_msg);
			}
		}

		if($okDoc){
			return $myDoc;
		}else{
			return false;
		}
	}


	function putTrash(){
		if(!defined("kTrashDir")){
			trace("kTrashDir not defined ! ");
		}
		global $db;
		$this->getMat();
		$this->getDocMat();
		$this->getValeurs();
        $this->getPersonnes();
		$this->getMatTrack();
		$xmldata=$this->xml_export(0);
	
		$xmlTrash = TraitementXSLT($xmldata,getSiteFile("designDir","print/trashMat.xsl"),array(),0,"",null,false);

		require_once(modelDir.'model_trash.php');
		$myTrash= new Trash();
		$myTrash->t_trash["TRA_ENTITE"]="mat";
		$myTrash->t_trash["TRA_TITRE"]=$this->t_mat['MAT_NOM'];
		$xmlTrash = preg_replace("/<\?.*?\?>/", "", $xmlTrash);

		$myTrash->t_trash["TRA_XML"]=$xmlTrash;
	    $ok = $myTrash->save();
		
		if($ok) {
			// Listage des fichiers à mettre dans la corbeille, en les regroupant par type (mat, doc_acc, storyboard) : fichiers (mat, doc_acc) et dossiers (storyboard) liés au document qui ne sont pas utilisés ailleurs 
			$trashDir=kTrashDir.$myTrash->t_trash['ID_TRASH'];
	    	$tab_fichier=array();
			
			$tab_fichier[]=array('type'=>'mat','file'=>$this->getFilePath());		
			
			
			// Imageur
			$mat_imageur=$dm['MAT']->t_mat['MAT_ID_IMAGEUR'];
						
			if(!empty($mat_imageur)){
				$objImageur=new Imageur();
				$objImageur->t_imageur['ID_IMAGEUR']=$mat_imageur;
				$objImageur->getImageur();
			}
			
			if(!empty($mat_imageur)) $tab_fichier[]=array('type'=>'storyboard','file'=>kStoryboardDir.$objImageur->getImageurPath());	
			// Suppression imageur en base
			if(!empty($mat_imageur)){
				$objImageur->deleteImageur(false); // On ne supprime pas les fichiers car on va les copier ensuite	
				unset($objImageur);
			}
			// Appel méthode putFiles de l’objet trash 
		    $myTrash->putFiles($tab_fichier);
			unset($myTrash);
		}
		return $ok;
	}
	
	
	
	
	function buildDCPMap($path){
		$DCP_map = array('TYPE_MAP'=>'DCP');
		$mapping_file = array();
		$assetmaps = array();
		$pkl_files = array();
		$pkl_files_found = array();
		
		if(!file_exists($path."/ASSETMAP") && !file_exists($path."/project.dcpproj") ){
			return false ; 
		}
		
		if (file_exists($path."/project.dcpproj")){
			$easy_dcp_map = file_get_contents($path."/project.dcpproj");
			$tab_easy_dcp = xml2tab($easy_dcp_map,true);
			$DCP_map['TYPE_MAP'] = 'DCP_unwrap';
			$ref_dir = basename($path);
			// echo $ref_dir;
			$DCP_map['project.dcpproj']['TYPE_DCP'] = 'EASY_DCP_MAP';
		}else{
			$it = new RecursiveDirectoryIterator($path);
			foreach(new RecursiveIteratorIterator($it) as $file)
			{
				if (strpos($file,'AppleDouble')===false &&  basename($file) == 'ASSETMAP'){
					$assetmaps[] = str_replace($path,'',$file);
				}
			}
			// if(!empty($assetmaps) && count($assetmaps)>1){
				// $DCP_map = array('TYPE_MAP'=>'DCP_partials');
				// $assetmap= file_get_contents($path.'/'.$assetmaps[0]);
				// $tab_map = xml2tab($assetmap,true);
			
			// } // else if( count($assetmaps)== 1 && file_exists($path."/ASSETMAP")){
			// récupération ASSETMAP présent dans le dossier 
				// $assetmap= file_get_contents($path."/ASSETMAP");
				// $tab_map = xml2tab($assetmap,true);
			//}
		}
			
		if($DCP_map['TYPE_MAP'] == 'DCP' ){ // || $DCP_map['TYPE_MAP'] == 'DCP_partials'
			foreach($assetmaps as $assetmap_path){
				$assetmap= file_get_contents($path.'/'.$assetmap_path);
				$tab_map = xml2tab($assetmap,true);
				$diff_path='';
				$diff_path = str_replace('ASSETMAP','',$assetmap_path);
				
				// contrôle présence assetList
				if(!isset($tab_map['ASSETMAP'][0]['ASSETLIST'][0])){
					trace("buildDCPMap - ".$path." ASSETMAP mal formé");
					return false ; 
				}
				//  pour chaque asset, on garde la correspondance PATH <=> ID dans DCP_map, ID => PATH dans mapping_file pour l'instant
				//	=> infos toujours issues de l'ASSETMAP
				foreach($tab_map['ASSETMAP'][0]['ASSETLIST'][0]['ASSET'] as $asset){
					$DCP_map[$asset['CHUNKLIST'][0]['CHUNK'][0]['PATH']]['ID_DCP'] = $asset['ID'];
					$mapping_file[$asset['ID']] = $asset['CHUNKLIST'][0]['CHUNK'][0]['PATH'];
					if (!isset($asset['PACKINGLIST'])){
						continue;
					}
					if(isset($diff_path) && !empty($diff_path)){
						$DCP_map[$asset['CHUNKLIST'][0]['CHUNK'][0]['PATH']]['DIFF_PATH'] = $diff_path;
					}
					
					$pkl_files[] = $diff_path.$asset['CHUNKLIST'][0]['CHUNK'][0]['PATH'];
					$DCP_map[$asset['CHUNKLIST'][0]['CHUNK'][0]['PATH']]['TYPE_DCP'] = 'PKL';

				}
				unset($tab_map);
			}
			
			if(!is_array($pkl_files) || empty($pkl_files)){
				trace("buildDCPMap - ".$path." PKL files not found");
				return false ; 
			}
			
			
			foreach($pkl_files as $pkl_file_path){
				if(!isset($pkl_file_path) || !file_exists($path.'/'.$pkl_file_path)){
					trace("buildDCPMap - ".$path." PKL files not found");
					return false ; 
				}
				
				// récupération du fichier PKL (pour l'instant PKL unique, à voir)
				$pkl= file_get_contents($path.'/'.$pkl_file_path);
				$tab_pkl = xml2tab($pkl,true);
				
				if(!isset($tab_pkl['PACKINGLIST'][0]['ASSETLIST'])){
					trace("buildDCPMap - ".$path." PKL mal formé");
					return false ; 
				}
				// recup nom PKL 
				$DCP_map[basename($pkl_file_path)]['ANNOTATIONTEXT'] = $tab_pkl['PACKINGLIST'][0]['ANNOTATIONTEXT'];
				
				if(isset($DCP_map[basename($pkl_file_path)]['DIFF_PATH'])){
					$diff_path = $DCP_map[basename($pkl_file_path)]['DIFF_PATH'];
				}else{
					$diff_path = '';
				}
				
				// ici, depuis l'assetlist du pkl, on ajoute des valeurs à DCP_map déja créé
				// 	=> ATTENTION : on accède à des valeurs DCP_map déja existante depuis une clé potentiellement différente :
				//	- $DCP_map[$asset['CHUNKLIST'][0]['CHUNK'][0]['PATH']]['ID_DCP'] = $asset['ID']; <= ici, $asset vient de ASSETMAP
				//	- $DCP_map[$asset['ORIGINALFILENAME']]['ANNOTATIONTEXT'] = $asset['ANNOTATIONTEXT']; <= ici $asset vient de PKL
				// => Peut être corrigé si on ne construit pas le DCP map avant ce stade. => le construire juste ici
				foreach($tab_pkl['PACKINGLIST'][0]['ASSETLIST'][0]['ASSET'] as $asset){
					if(isset($asset['ANNOTATIONTEXT']) && !empty($asset['ANNOTATIONTEXT'])){
						$DCP_map[$asset['ORIGINALFILENAME']]['ANNOTATIONTEXT'] = $asset['ANNOTATIONTEXT'];
					}
					if(isset($asset['TYPE']) && $asset['TYPE'] == 'text/xml;asdcpKind=CPL'){
						$DCP_map[$asset['ORIGINALFILENAME']]['TYPE_DCP'] = 'CPL';
					}
				}
				
				foreach($DCP_map as $file=>$prop){
					if($prop['TYPE_DCP'] == 'CPL'){
						if(!isset($DCP_map[$file]['REEL']) || empty($DCP_map[$file]['REEL'])){
							$cpl = file_get_contents($path.'/'.$diff_path.$file);
							$cpl_tab = xml2tab($cpl,true);
							if(!empty($diff_path)){
								$DCP_map[$file]['DIFF_PATH'] = $diff_path;
							}
							
							$DCP_map[$file]['CONTENTTITLETEXT'] = $cpl_tab['COMPOSITIONPLAYLIST'][0]['CONTENTTITLETEXT'];
							foreach($cpl_tab['COMPOSITIONPLAYLIST'][0]['REELLIST'][0]['REEL'] as $reel){
								$arr_reel = array();
								$arr_reel['ID'] = $reel['ID'];
								
								if(isset($reel['ASSETLIST'][0]['MAINPICTURE'][0])){
									if(!empty($diff_path) && file_exists($path.'/'.$diff_path.$mapping_file[$reel['ASSETLIST'][0]['MAINPICTURE'][0]['ID']])){
										$arr_reel['VIDEO'] = $diff_path.$mapping_file[$reel['ASSETLIST'][0]['MAINPICTURE'][0]['ID']];
									}else{
										$arr_reel['VIDEO'] = $mapping_file[$reel['ASSETLIST'][0]['MAINPICTURE'][0]['ID']];
									}
									$DCP_map[$mapping_file[$reel['ASSETLIST'][0]['MAINPICTURE'][0]['ID']]]["REF_CPL"] = $file;
									$DCP_map[$mapping_file[$reel['ASSETLIST'][0]['MAINPICTURE'][0]['ID']]]["TYPE_DCP"] = 'VIDEO';
								}
								if(isset($reel['ASSETLIST'][0]['MAINSOUND'][0])){
									if(!empty($diff_path) && file_exists($path.'/'.$diff_path.$mapping_file[$reel['ASSETLIST'][0]['MAINSOUND'][0]['ID']])){
										$arr_reel['SOUND'] = $diff_path.$mapping_file[$reel['ASSETLIST'][0]['MAINSOUND'][0]['ID']];
									}else{
										$arr_reel['SOUND'] = $mapping_file[$reel['ASSETLIST'][0]['MAINSOUND'][0]['ID']];
									}
									$DCP_map[$mapping_file[$reel['ASSETLIST'][0]['MAINSOUND'][0]['ID']]]["REF_CPL"] = $file;
									$DCP_map[$mapping_file[$reel['ASSETLIST'][0]['MAINSOUND'][0]['ID']]]["TYPE_DCP"] = 'SOUND';
								}
								if(isset($reel['ASSETLIST'][0]['MAINSUBTITLE'][0])){
									if(!empty($diff_path) && file_exists($path.'/'.$diff_path.$mapping_file[$reel['ASSETLIST'][0]['MAINSUBTITLE'][0]['ID']])){
										$arr_reel['SUBTITLE'] = $diff_path.$mapping_file[$reel['ASSETLIST'][0]['MAINSUBTITLE'][0]['ID']];
									}else{
										$arr_reel['SUBTITLE'] = $mapping_file[$reel['ASSETLIST'][0]['MAINSUBTITLE'][0]['ID']];
									}
									$DCP_map[$mapping_file[$reel['ASSETLIST'][0]['MAINSUBTITLE'][0]['ID']]]["REF_CPL"] = $file;
									$DCP_map[$mapping_file[$reel['ASSETLIST'][0]['MAINSUBTITLE'][0]['ID']]]["TYPE_DCP"] = 'SUBTITLE';
								}
								$DCP_map[$file]['REEL'][] = $arr_reel;
							}
						}
					}
				}
			}
			unset($mapping_file);
		}else if ($DCP_map['TYPE_MAP']=='DCP_unwrap'){
			if(!isset($tab_easy_dcp['FHG_DCP_GENERATOR_PROJECT'][0])){
				trace("buildDCPMap - ".$path." project.dcpproj mal formé");
				return false ; 
			}
			$tab_easy_dcp = $tab_easy_dcp['FHG_DCP_GENERATOR_PROJECT'][0];
						
			if(isset($tab_easy_dcp['FRAMERATE']) && isset($tab_easy_dcp['FRAMERATE'][0]['NUMERATOR'])){
				$DCP_map['FRAMERATE'] = intval(intval($tab_easy_dcp['FRAMERATE'][0]['NUMERATOR']) / (isset($tab_easy_dcp['FRAMERATE'][0]['DENOMINATOR'])?intval($tab_easy_dcp['FRAMERATE'][0]['DENOMINATOR']):1));
			}
			
			if(!isset($tab_easy_dcp['COMPOSITIONS'][0]['COMPOSITION']) || count($tab_easy_dcp['COMPOSITIONS'][0]['COMPOSITION'])==0){
				trace("buildDCPMap - unwrap - project.dcpproj sans 'compositions'");
				return false ; 
			}
			foreach($tab_easy_dcp['PICTURETRACKFILES'][0]['PICTURETRACKFILE'] as $pictrack){
				$DCP_map[dirname(substr($pictrack['INPUTS'][0]['INPUT'][0]['FILENAME'],strpos($pictrack['INPUTS'][0]['INPUT'][0]['FILENAME'],'video/')))] = array('INTERNALID'=>$pictrack['INTERNALID'],'TYPE_DCP'=>'VIDEO');
			}
			foreach($tab_easy_dcp['SOUNDTRACKFILES'][0]['SOUNDTRACKFILE'] as $soundtrack){
				$DCP_map[dirname(substr($soundtrack['INPUTS'][0]['INPUT'][0]['FILENAME'],strpos($soundtrack['INPUTS'][0]['INPUT'][0]['FILENAME'],'audio/')))] = array('INTERNALID'=>$soundtrack['INTERNALID'],'TYPE_DCP'=>'SOUND');
			}
			foreach($tab_easy_dcp['SUBPICTURETRACKFILES'][0]['SUBPICTURETRACKFILE'] as $subtrack){
				$DCP_map[substr($subtrack['INPUTS'][0]['INPUT'][0]['FILENAME'],strpos($subtrack['INPUTS'][0]['INPUT'][0]['FILENAME'],'subtitles/'))] = array('INTERNALID'=>$subtrack['INTERNALID'],'TYPE_DCP'=>'SUBTITLE');
			}
			
			foreach($tab_easy_dcp['COMPOSITIONS'][0]['COMPOSITION'] as $compo => $props){
				if(isset($DCP_map[$props['CONTENTTITLE']])){
					// on évite de merge 2 compositions ayant le même titre... on prends juste la première occurence pr l'instant. 
					continue; 
				}
				$DCP_map[$props['CONTENTTITLE']]['CONTENTTITLE']= $props['CONTENTTITLE'];
				$DCP_map[$props['CONTENTTITLE']]['ANNOTATIONTEXT']= $props['ANNOTATIONTEXT'];
				$DCP_map[$props['CONTENTTITLE']]['TYPE_DCP']= "CPL_unwrap";
				$DCP_map[$props['CONTENTTITLE']]['FRAMERATE']= $DCP_map['FRAMERATE'];
				foreach($props['REELS'][0]['REEL'] as $idx_reel=>$reel){
					if(isset($reel['MAINPICTURE']) && !empty($reel['MAINPICTURE'])){
						$vid_id = $reel['MAINPICTURE'][0]['INTERNALID'];
						foreach($tab_easy_dcp['PICTURETRACKFILES'][0]['PICTURETRACKFILE'] as $pic_refs){
							if($vid_id == $pic_refs['INTERNALID']){
								// => a priori "bonne" facon de récupérer le filename, sauf que dans l'exemple, le basename du repertoire est : IoeTe_IT-DE_export, et les valeurs stockées sont "IoeTe_IT-DE_Export"...
								//$DCP_map[$props['CONTENTTITLE']]['REEL'][$idx_reel]['VIDEO'] = substr($pic_refs['INPUTS'][0]['INPUT'][0]['FILENAME'],strpos($pic_refs['INPUTS'][0]['INPUT'][0]['FILENAME'],$ref_dir)+strlen($ref_dir));
								// du coup on récupère juste le path à partir de "video/
								$DCP_map[$props['CONTENTTITLE']]['REEL'][$idx_reel]['VIDEO']['PATH'] = substr($pic_refs['INPUTS'][0]['INPUT'][0]['FILENAME'],strpos($pic_refs['INPUTS'][0]['INPUT'][0]['FILENAME'],'video/'));
								$DCP_map[$props['CONTENTTITLE']]['REEL'][$idx_reel]['VIDEO']['CPT'] = $pic_refs['INPUTS'][0]['INPUT'][0]['NUMOFREPETITIONS'];
								break ; 
							}
						}
					}
					if(isset($reel['MAINSOUND']) && !empty($reel['MAINSOUND'])){
						$sound_id = $reel['MAINSOUND'][0]['INTERNALID'];
						foreach($tab_easy_dcp['SOUNDTRACKFILES'][0]['SOUNDTRACKFILE'] as $snd_refs){
							if($sound_id == $snd_refs['INTERNALID']){
								foreach($snd_refs['INPUTS'][0]['INPUT'] as $idx_input => &$input){
									$input['FILENAME'] = substr($input['FILENAME'],strpos($input['FILENAME'],'audio/'));
								}
								unset($input);
								$DCP_map[$props['CONTENTTITLE']]['REEL'][$idx_reel]['SOUND'] = $snd_refs['INPUTS'][0]['INPUT'];
								break ; 
							}
						}
					}
					if(isset($reel['MAINSUBPICTURE']) && !empty($reel['MAINSUBPICTURE'])){
						$sub_id = $reel['MAINSUBPICTURE'][0]['INTERNALID'];
						foreach($tab_easy_dcp['SUBPICTURETRACKFILES'][0]['SUBPICTURETRACKFILE'] as $sub_refs){
							if($sub_id == $sub_refs['INTERNALID']){
								$DCP_map[$props['CONTENTTITLE']]['REEL'][$idx_reel]['SUBTITLE'] = substr($sub_refs['INPUTS'][0]['INPUT'][0]['FILENAME'],strpos($sub_refs['INPUTS'][0]['INPUT'][0]['FILENAME'],'subtitles/'));
								break ; 
							}
						}
					}
				}
			}
		}
		
		$this->DCP_map = $DCP_map;
		unset($DCP_map);
		
		return $this->DCP_map;
	}
	
	function buildDPXMap($path){
		$DPX_map = array('TYPE_MAP'=>'DPX');
		if (!is_dir($path)){
			trace("buildDPXMap - path ne pointe pas un dossier : ".$path);
			return false ; 
		}
		$array_tracks = array();
		
		if($dh=opendir($path))
		{
			while ($file=readdir($dh))
			{
				if ($file!='.' && $file!='..'){
					if(strtolower(getExtension($file)) == 'wav'){
						try{
							$full_infos_wav = MatInfo::getMatInfo($path.'/'.$file);
							$full_infos_wav['tracks'][0]['index'] = 1;
							$array_tracks['tracks'][1] = $full_infos_wav['tracks'][0];
							unset($full_infos_wav);
						}catch(exception $e){trace("buildDPXMap => récupération info track audio : echec");}
						$DPX_map['WAV_FILES'][]=  array('REL_PATH'=>"","ABS_PATH"=>$path,"FILE"=>$file);
					}else if(in_array(strtolower(getExtension($file)),array('dpx','jpg','j2c'))){
						$path_found = false ; 
						if(isset($DPX_map['DPX_FILES'])){
							foreach($DPX_map['DPX_FILES'] as $dpx_dir){
								if($dpx_dir['ABS_PATH'] == $path){
									$path_found = true ;
									break ; 
								}
							}
						}
						if(!$path_found){
							try{
								$full_infos_dpx = MatInfo::getMatInfo($path."/".$file);
								$infos = array(
											'index'=>$full_infos_dpx["tracks"][0]['index'],
											'type'=>$full_infos_dpx["tracks"][0]['type'],
											'format'=>$full_infos_dpx["tracks"][0]['format'],
											'format_version'=>$full_infos_dpx["tracks"][0]['format_version'],
											'width'=>$full_infos_dpx["tracks"][0]['width'],
											'height'=>$full_infos_dpx["tracks"][0]['height'],
											'scan_type'=>$full_infos_dpx["tracks"][0]['scan_type'],
											'bit_depth'=>$full_infos_dpx["tracks"][0]['bit_depth'],
											'display_aspect_ratio'=>$full_infos_dpx["tracks"][0]['display_aspect_ratio'],
											'pix_fmt'=>$full_infos_dpx["tracks"][0]['pix_fmt']
									);
								$array_tracks['tracks'][0] = $infos;
								unset($full_infos_dpx);
								unset($infos);
							}catch(exception $e){trace("buildDPXMap => récupération info track video : echec");}
							$DPX_map['DPX_FILES'][] = array('REL_PATH'=>"","ABS_PATH"=>$path,"FIRST"=>$file);
						}
						$cpt_dpx_files++;
					}else if(is_dir($path.'/'.$file)){
						$dir_dpx=$file;
						$dpx_files=scandir($path.'/'.$dir_dpx);
						$nb_dpx = 0 ; 
						foreach ($dpx_files as $file_){
							if ($file_!='.' && $file_!='..' && in_array(strtolower(getExtension($file_)),array('dpx','jpg','j2c'))){
								if($nb_dpx == 0 ){
									try{
										$full_infos_dpx = MatInfo::getMatInfo($path."/".$dir_dpx."/".$file_);
										$infos = array(
											'index'=>$full_infos_dpx["tracks"][0]['index'],
											'type'=>$full_infos_dpx["tracks"][0]['type'],
											'format'=>$full_infos_dpx["tracks"][0]['format'],
											'format_version'=>$full_infos_dpx["tracks"][0]['format_version'],
											'width'=>$full_infos_dpx["tracks"][0]['width'],
											'height'=>$full_infos_dpx["tracks"][0]['height'],
											'scan_type'=>$full_infos_dpx["tracks"][0]['scan_type'],
											'bit_depth'=>$full_infos_dpx["tracks"][0]['bit_depth'],
											'display_aspect_ratio'=>$full_infos_dpx["tracks"][0]['display_aspect_ratio'],
											'pix_fmt'=>$full_infos_dpx["tracks"][0]['pix_fmt']
										);
										$array_tracks['tracks'][0] = $infos;
										unset($full_infos_dpx);
										unset($infos);
									}catch(exception $e){trace("buildDPXMap => récupération info track video : echec");}
									$DPX_map['DPX_FILES'][] = array('REL_PATH'=>$dir_dpx,"ABS_PATH"=>$path."/".$dir_dpx,"FIRST"=>$file_,"INFOS"=>$infos);									
									unset($infos);
								}
								$current = $file_;
								$nb_dpx++;
							}
						}
						if(isset($DPX_map['DPX_FILES'])){
							$DPX_map['DPX_FILES'][count($DPX_map['DPX_FILES'])-1]['LAST'] = $current ; 
							$DPX_map['DPX_FILES'][count($DPX_map['DPX_FILES'])-1]['CPT_DPX'] = $nb_dpx ; 
						}
						$cpt_dpx_files+=$nb_dpx;
					}
				}
			}
			$DPX_map['CPT_DPX'] = $cpt_dpx_files;
		}
		if(isset($array_tracks) && !empty($array_tracks)){
			//calcul framerate si on a les deux tracks : 
			if(isset($array_tracks['tracks'][1]) && isset($array_tracks['tracks'][1]['duration']) && isset($DPX_map['CPT_DPX'])){
				$framerate= round($DPX_map['CPT_DPX']/$array_tracks['tracks'][1]['duration'],2);
				$DPX_map['frame_rate'] = $framerate;
				// report infos dans track video
				$array_tracks['tracks'][0]['duration'] = $array_tracks['tracks'][1]['duration'];
				$array_tracks['tracks'][0]['frame_rate'] = $framerate;
			}
			$DPX_map['tracks'] = $array_tracks['tracks'];
		}
		
		if(count($DPX_map)>1){
			$this->DPX_map = $DPX_map;
			unset($DPX_map);
			
			return $this->DPX_map;
		}else{
			return false ; 
		}
		
	}
	
	
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){
		global $db;
		
		if($mode == null){
			$mode=array('mat_track'=>1,'docmat'=>1,'docmat_val'=>1,'mat_val'=>1,'mat_pers'=>1,'mat_children'=>1,'mat_tape'=>1);
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		if(strpos(strtolower($critere),'order by') === false){
			$critere.=" order by id_mat asc ";
		}
		
		
		$limit="";
		if ($nb!="all"){
			// $limit=" LIMIT $nb OFFSET $nb_offset";
			$limit= $db->limit($nb_offset,$nb);
		}
	
		// définition des caractères à remplacer, 
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		$xml_file_path = kServerTempDir."/export_opsis_mat_".microtime(true).rand(0,100).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
			
			
		$sql_mat = "SELECT M.*, ".$db->Concat('t2.US_NOM',"' '",'t2.US_PRENOM')." as US_CREA, ".$db->Concat('t3.US_NOM',"' '",'t3.US_PRENOM')." as US_MODIF, t4.LIEU_PATH, t5.ETAT_ARCH, t6.FORMAT_ONLINE
			FROM t_mat M 
			LEFT OUTER JOIN t_usager t2 ON M.MAT_ID_USAGER_CREA= t2.ID_USAGER 
			LEFT OUTER JOIN t_usager t3 ON M.MAT_ID_USAGER_MOD = t3.ID_USAGER 
			LEFT OUTER JOIN t_lieu t4 ON M.MAT_LIEU = t4.LIEU 
			LEFT OUTER JOIN t_etat_arch t5 ON M.MAT_ID_ETAT_ARCH = t5.ID_ETAT_ARCH AND t5.ID_LANG =".$db->Quote($_SESSION['langue'])."
			LEFT OUTER JOIN t_format_mat t6 ON M.MAT_FORMAT = t6.FORMAT_MAT
			".$critere.$limit;
				
		$results_mat = $db->Execute($sql_mat);

		//trace("\$sql_mat :".$sql_mat  );
		
		$arr_ids =  array();
		while($row = $results_mat->FetchRow()){
			$arr_ids[] = $row['ID_MAT'];
		}
		$ids = implode(',',$arr_ids);
		if($ids == ''){
			flock($xml_file, LOCK_UN);
			fclose($xml_file);
			unlink($xml_file_path);
			return false;
		}
		
		
		if($mode['mat_track']){
			$sql_tracks = "SELECT distinct t_mat_track.* 
			FROM t_mat_track
			WHERE ID_MAT in ($ids) ORDER BY ID_MAT, ID_MAT_TRACK ASC";
			$results_tracks = $db->Execute($sql_tracks);
		}
		
		
		if($mode['docmat']){
			$sql_docmat="SELECT distinct t_doc_mat.* from t_doc_mat
						where t_doc_mat.ID_MAT in ($ids) ORDER BY  t_doc_mat.ID_MAT ASC";
						//inner join t_doc ON t_doc.ID_DOC=t_doc_mat.ID_DOC AND t_doc.ID_LANG=".$db->Quote($_SESSION['langue'])."
			$results_docmat = $db->Execute($sql_docmat);
			if(!empty($results_docmat)) {
				$ids_dmat=array();
				while($docmat = $results_docmat->FetchRow()){
					$ids_dmat[] = $docmat['ID_DOC_MAT'];
				}
				// important de se replacer au début du resultSet de doc_mat
				$results_docmat->Move(0);
				$ids_dmat = implode(',',$ids_dmat);
				
				$sql_docmat_doc="SELECT distinct t_doc_mat.ID_DOC_MAT, t_doc.* 
								from t_doc_mat
								inner join t_doc ON t_doc.ID_DOC=t_doc_mat.ID_DOC AND t_doc.ID_LANG=".$db->Quote($_SESSION['langue'])."
								where t_doc_mat.ID_DOC_MAT in ($ids_dmat) ORDER BY  t_doc_mat.ID_DOC_MAT ASC";
							//inner join t_doc ON t_doc.ID_DOC=t_doc_mat.ID_DOC AND t_doc.ID_LANG=".$db->Quote($_SESSION['langue'])."
				$results_docmat_doc = $db->Execute($sql_docmat_doc);
				unset($sql_docmat_doc);
			} else {
				$results_docmat_doc = array();
			}
		}
		
		if($mode['docmat_val'] && isset($ids_dmat)){			
			$sql_docmat_val="SELECT distinct t_doc_mat_val.*, t_val.*
						from t_doc_mat_val 
						LEFT OUTER JOIN t_val on t_doc_mat_val.ID_VAL=t_val.ID_VAL AND ID_LANG=".$db->Quote($_SESSION['langue'])."
						where t_doc_mat_val.ID_DOC_MAT in ($ids_dmat) ORDER BY  t_doc_mat_val.ID_DOC_MAT ASC";
			$results_docmat_val = $db->Execute($sql_docmat_val);
			unset($sql_docmat_val);
		}
		
		if($mode['mat_val']){			
			$sql_mat_val="SELECT distinct t_mat_val.*, t_val.*
						from t_mat_val 
						LEFT OUTER JOIN t_val on t_mat_val.ID_VAL=t_val.ID_VAL AND ID_LANG=".$db->Quote($_SESSION['langue'])."
						where t_mat_val.ID_MAT in ($ids) ORDER BY  t_mat_val.ID_MAT ASC";
			$results_mat_val = $db->Execute($sql_mat_val);
			unset($sql_mat_val);
		}
		
		if($mode['mat_pers']){			
			$sql_mat_pers="SELECT distinct t_mat_pers.*, t_personne.*
						from t_mat_pers 
						LEFT OUTER JOIN t_personne on t_mat_pers.ID_PERS=t_personne.ID_PERS AND ID_LANG=".$db->Quote($_SESSION['langue'])."
						where t_mat_pers.ID_MAT in ($ids) ORDER BY  t_mat_pers.ID_MAT ASC";
			$results_mat_pers = $db->Execute($sql_mat_pers);
			unset($sql_mat_pers);
		}
		
		if($mode['mat_children']){			
			$sql_mat_children="SELECT t_mat.* 
						from t_mat 
						WHERE MAT_ID_GEN in ($ids) ORDER BY MAT_ID_GEN ASC";
			$results_mat_children = $db->Execute($sql_mat_children);
			unset($sql_mat_children);
		}
		if($mode['mat_tape']){			
			$sql_mat_tape="SELECT distinct t_tape_file.* 
						FROM t_tape_file 
						WHERE TF_MEDIA='videos' and TF_ID_MAT in ($ids) ORDER BY TF_ID_MAT,TF_DATE_COPIE asc";
			$results_mat_tape = $db->Execute($sql_mat_tape);
			unset($sql_mat_tape);
		}	
		
		while($mode['mat_track'] && !empty($results_tracks) && $track = $results_tracks->FetchRow()){
			$results_tracks_ref[$track['ID_MAT']][]=$results_tracks->CurrentRow()-1;
		}		
		while($mode['docmat'] && !empty($results_docmat) && $docmat = $results_docmat->FetchRow()){
			$results_docmat_ref[$docmat['ID_MAT']][]=$results_docmat->CurrentRow()-1;
		}
		while($mode['docmat'] && !empty($results_docmat_doc) && $docmat_doc = $results_docmat_doc->FetchRow()){
			$results_docmat_doc_ref[$docmat_doc['ID_DOC_MAT']][]=$results_docmat_doc->CurrentRow()-1;
		}
		while($mode['docmat_val'] && !empty($results_docmat_val) && $docmat_val = $results_docmat_val->FetchRow()){
			$results_docmat_val_ref[$docmat_val['ID_DOC_MAT']][]=$results_docmat_val->CurrentRow()-1;
		}
		while($mode['mat_val'] && !empty($results_mat_val) && $mat_val = $results_mat_val->FetchRow()){
			$results_mat_val_ref[$mat_val['ID_MAT']][]=$results_mat_val->CurrentRow()-1;
		}
		while($mode['mat_pers'] && !empty($results_mat_pers) && $mat_pers = $results_mat_pers->FetchRow()){
			$results_mat_pers_ref[$mat_pers['ID_MAT']][]=$results_mat_pers->CurrentRow()-1;
		}
		while($mode['mat_children'] && !empty($results_mat_children) && $mat_children = $results_mat_children->FetchRow()){
			$results_mat_children_ref[$mat_children['MAT_ID_GEN']][]=$results_mat_children->CurrentRow()-1;
		}
		while($mode['mat_tape'] && !empty($results_mat_tape) && $mat_tape = $results_mat_tape->FetchRow()){
			$results_mat_tape_ref[$mat_tape['ID_MAT']][]=$results_mat_tape->CurrentRow()-1;
		}	

		foreach($results_mat as $mat){
			$xml .= "\t<t_mat ID_MAT=\"".$mat['ID_MAT']."\">\n";
			foreach($mat as $fld=>$val){
				//on ajoute pas de CDATA sur les champs docs qui contiennent du XML car on veut pouvoir les traiter dans le xsl, si ils sont CDATA, ils ne sont pas parcourable via xpath
				if(in_array($fld,array('MAT_VAL','MAT_LEX','MAT_XML','MAT_INFO'))){
					$xml .= "\t\t<".strtoupper($fld).">".str_replace($search_chars,$replace_chars,$val)."</".strtoupper($fld).">\n";
				}else{
					$xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
				}
			}
			
			//======== GESTION MAT_TRACKS ==============
			
			if(isset($results_tracks_ref[$mat['ID_MAT']][0])){
				$results_tracks->Move($results_tracks_ref[$mat['ID_MAT']][0]);
				$array_mat_tracks = $results_tracks->GetRows(count((array)$results_tracks_ref[$mat['ID_MAT']]));
			}else{
				$array_mat_tracks=array();
			}
			if(count($array_mat_tracks)>0){
				$xml.="\t\t\t\t<tracks>\n";
				foreach($array_mat_tracks as $mat_track){
					$xml.="\t\t\t\t\t<t_mat_track>\n";
					foreach($mat_track as $fld=>$val){
						$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t\t\t</t_mat_track>\n";
				}
				$xml.="\t\t\t\t</tracks>\n";
			}
			
			
			
			//======== GESTION DOC_MAT ==============
		
			if(isset($results_docmat_ref[$mat['ID_MAT']][0])){
				$results_docmat->Move($results_docmat_ref[$mat['ID_MAT']][0]);
				$array_docmat = $results_docmat->GetRows(count((array)$results_docmat_ref[$mat['ID_MAT']]));
			}else{
				$array_docmat=array();
			}
			if(count($array_docmat)>0){
				$xml.="\t\t<t_doc_mat nb=\"".count($array_docmat)."\" ID_MAT=\"".$mat['ID_MAT']."\">\n";
				foreach($array_docmat as $docmat){
					$xml.="\t\t\t<DOC_MAT ID_DOC=\"".$docmat['ID_DOC']."\">\n";
					foreach($docmat as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					// t_doc
					if(isset($results_docmat_doc_ref[$docmat['ID_DOC_MAT']][0])){
						$results_docmat_doc->Move($results_docmat_doc_ref[$docmat['ID_DOC_MAT']][0]);
						$array_docmat_doc = $results_docmat_doc->GetRows(count((array)$results_docmat_doc_ref[$docmat['ID_DOC_MAT']]));
					}else{
						$array_docmat_doc= array();
					}
					if(count($array_docmat_doc)>0){
						foreach($array_docmat_doc as $doc){
							$xml.="\t\t\t\t\t<t_doc ID_DOC=\"".$doc['ID_DOC']."\">\n";
							foreach($doc as $fld=>$val){
								$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
							}
							$xml.="\t\t\t\t\t</t_doc>\n";
						}
					}
					// t_doc_mat_val
					if(isset($results_docmat_val_ref[$docmat['ID_DOC_MAT']][0])){
						$results_docmat_val->Move($results_docmat_val_ref[$docmat['ID_DOC_MAT']][0]);
						$array_docmat_val = $results_docmat_val->GetRows(count((array)$results_docmat_val_ref[$docmat['ID_DOC_MAT']]));
					}else{
						$array_docmat_val= array();
					}
					if(count($array_docmat_val)>0){
						$xml.="\t\t\t\t<t_doc_mat_val>\n";
						foreach($array_docmat_val as $dmat_val){
							$xml.="\t\t\t\t\t<t_val ID_TYPE_VAL=\"".$dmat_val['ID_TYPE_VAL']."\">\n";
							foreach($dmat_val as $fld=>$val){
								$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
							}
							$xml.="\t\t\t\t\t</t_val>\n";
						}
						$xml.="\t\t\t\t</t_doc_mat_val>\n";
					}
			
					$xml.="\t\t\t</DOC_MAT>\n";
				}
				$xml.="\t\t</t_doc_mat>\n";
				
				//======== GESTION MAT_VAL ==============
				
				if(isset($results_mat_val_ref[$mat['ID_MAT']][0])){
					$results_mat_val->Move($results_mat_val_ref[$mat['ID_MAT']][0]);
					$array_mat_val = $results_mat_val->GetRows(count((array)$results_mat_val_ref[$mat['ID_MAT']]));
				}else{
					$array_mat_val= array();
				}
				if(count($array_mat_val)>0){
					$xml.="\t\t\t\t<t_mat_val>\n";
					foreach($array_mat_val as $mat_val){
						$xml.="\t\t\t\t\t<t_val ID_VAL=\"".$mat_val['ID_VAL']."\" ID_TYPE_VAL=\"".$mat_val['ID_TYPE_VAL']."\">\n";
						foreach($mat_val as $fld=>$val){
							$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
						}
						$xml.="\t\t\t\t\t</t_val>\n";
					}
					$xml.="\t\t\t\t</t_mat_val>\n";
				}
				
				//======== GESTION MAT_PERS ==============
				
				if(isset($results_mat_pers_ref[$mat['ID_MAT']][0])){
					$results_mat_pers->Move($results_mat_pers_ref[$mat['ID_MAT']][0]);
					$array_mat_pers = $results_mat_pers->GetRows(count((array)$results_mat_pers_ref[$mat['ID_MAT']]));
				}else{
					$array_mat_pers= array();
				}
				if(count($array_mat_pers)>0){
					$xml.="\t\t\t\t<t_mat_pers>\n";
					foreach($array_mat_pers as $mat_pers){
						$xml.="\t\t\t\t\t<t_personne ID_PERS=\"".$mat_pers['ID_PERS']."\">\n";
						foreach($mat_pers as $fld=>$val){
							$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
						}
						$xml.="\t\t\t\t\t</t_personne>\n";
					}
					$xml.="\t\t\t\t</t_mat_pers>\n";
				}

				//======== GESTION MAT_CHILDREN ==============
				
				if(isset($results_mat_children_ref[$mat['ID_MAT']][0])){
					$results_mat_children->Move($results_mat_children_ref[$mat['ID_MAT']][0]);
					$array_mat_children = $results_mat_children->GetRows(count((array)$results_mat_children_ref[$mat['ID_MAT']]));
				}else{
					$array_mat_children= array();
				}
				if(count($array_mat_children)>0){
					$xml.="\t\t\t\t<t_mat_children>\n";
					foreach($array_mat_children as $mat_child){
						$xml.="\t\t\t\t\t<t_mat ID_MAT=\"".$mat_child['ID_MAT']."\">\n";
						foreach($mat_child as $fld=>$val){
							$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
						}
						$xml.="\t\t\t\t\t</t_mat>\n";
					}
					$xml.="\t\t\t\t</t_mat_children>\n";
				}
				
				//======== GESTION TAPE_FILE ==============
				// MS - non testé... 
				if(isset($results_mat_tape_ref[$mat['ID_MAT']][0])){
					$results_mat_tape->Move($results_mat_tape_ref[$mat['ID_MAT']][0]);
					$array_mat_tape = $results_mat_tape->GetRows(count((array)$results_mat_tape_ref[$mat['ID_MAT']]));
				}else{
					$array_mat_tape= array();
				}
				if(count($array_mat_tape)>0){
					$xml.="\t\t\t\t<t_tape_file>\n";
					foreach($array_mat_tape as $mat_tape){
						$xml.="\t\t\t\t\t<TAPE_FILE ID_TAPE_FILE=\"".$mat_tape['ID_TAPE_FILE']."\">\n";
						foreach($mat_tape as $fld=>$val){
							$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
						}
						$xml.="\t\t\t\t\t</TAPE_FILE>\n";
					}
					$xml.="\t\t\t\t</t_tape_file>\n";
				}
				
				
			}
			$xml .= "\t</t_mat>\n";
            fwrite($xml_file,$xml);
            $xml = "";
		}
		
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
        flock($xml_file, LOCK_UN);
        fclose($xml_file);
        
        return $xml_file_path;
	}

	 function getRoles($id_type_desc='',$add_blank=false) {
		global $db;
		if ($add_blank) $arrRoles[]='---';
		$lang = $_SESSION['langue'];
		$sql = "SELECT ID_ROLE,ROLE FROM t_role WHERE ID_LANG=".$db->Quote($lang);
		if(!empty($id_type_desc)){
			$sql .= " and ID_TYPE_DESC=".$db->Quote($id_type_desc);
		}
		$sql .=" ORDER BY ROLE";
		$rs=$db->Execute($sql);
		$arrRoles = array();
		while ($row=$rs->FetchRow()) {
			$arrRoles[$row['ID_ROLE']]=$row['ROLE'];
		}
		$rs->Close();
		return $arrRoles;
	 }
	
}
?>
