<?php

require_once(modelDir."/model.php");
class Conso extends Model
{
	private $parametre;
	private $valeur;
	
	function __construct($id=null,$version='')
	{
		$this->parametre='';
		$this->valeur='';
		parent::__construct('t_conso',$id, $version); 
	}
	
	function getData($arrData=array(),$version=""){
		$arrConso = parent::getData($arrData,$version);
		return $arrConso;
	}	
	
	public function getConso($param){
		return $this->getData($param);
	}
	
	
	// getXMLList CONSO 
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){
		trace("modelConso getXMLList :".$sql);
		
		global $db;
		
		if($mode == null){
			$mode=array();
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		$limit="";
		if ($nb!="all"){
			$limit= $db->limit($nb_offset,$nb);
		}
	
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		$xml_file_path = kServerTempDir."/export_opsis_".$this->entity."_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
	
		$sql_ent = "SELECT c.* , tc.type_conso, tc.type_conso_unit
				FROM t_conso as c
				LEFT JOIN t_type_conso tc on c.conso_id_type = tc.id_type_conso AND tc.ID_LANG=".$db->Quote($_SESSION['langue'])." ".$critere.$limit;
		
		$results = $db->Execute($sql_ent);
		$ids = implode(',',$db->GetCol($sql_ent));
		if($ids == ''){
			fwrite($xml_file,"</EXPORT_OPSIS>\n");
			return false;
		}
		
		foreach ($results as $ent){
			$xml.="\t<".$this->table.">";
				if($ent['CONSO_ID_TYPE'] == 'stockage'){
					require_once(libDir."fonctionsGeneral.php");
					$ent['CONSO_QUANTITE_HUMREAD'] = filesize_format($ent['CONSO_QUANTITE'],'.',' ',false,(strtoupper($_SESSION['langue']) == 'FR')?'octet':false);
				}else{
					$ent['CONSO_QUANTITE_HUMREAD'] = $ent['CONSO_QUANTITE'];
				}
				foreach($ent as $fld=>$val){
					 $xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
				}
			$xml.="\t</".$this->table.">";
			fwrite($xml_file,$xml);
			$xml = "";
		}
		
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
		flock($xml_file, LOCK_UN);
		fclose($xml_file);
		return $xml_file_path;
	}
	
}

?>