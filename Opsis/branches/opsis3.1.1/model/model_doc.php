<?
require_once(modelDir."/model.php");
require_once(modelDir.'model_categorie.php');

class Doc extends Model{


    var $t_doc_mat = array();
    var $t_doc_lex = array();
    var $t_doc_val = array();
	var $t_doc_cat = array();
    var $t_images= array();
    var $t_doc_redif =array();
    var $t_doc_seq =array();
	var $t_pers_lex = array();
	var $t_doc_lex_droit = array();
	var $t_doc_prex = array();
	var $t_doc_exp = array();
	var $t_doc_acc = array();
	var $t_doc_right = array();
	var $t_doc_comment = array();
	var $t_doc_publication = array();

    var $error_msg;

    //Lien entre doc (à la création uniquement, si on crée un nv doc depuis le formulaire de saisie de liens)

    var $id_doc2link; // a gérer via un tableau
    var $relation;

 	var $t_doc = array();
 	var $vignette; // Chemin de la vignette associée au doc


	var $type_doc; // Type de doc, en toutes lettres
	var $etat_arch; // Etat archivage, en toutes lettres

	var $arrFather= array();
	var $arrChildren= array();
	var $arrVersions=array();
	var $arrDocAcc=array();
	var $arrDocLiesSRC=array();
	var $arrDocLiesDST=array();
	var $arrDocFilles=array();

 	var $uniqueFields; // tableau (optionnel) contenant les champs sur lesquels lors d'une sauvegarde le test d'unicité est basé

	var $user_modif;
	var $user_crea;
	
	var $initDoc = false;

 	// Par ex: certains clients considèrent DOC_COTE comme obligatoire & unique, d'autres pas.

 	private $arrFieldsToSaveWithIdLang = array(
	   "DOC_TITRE" , "DOC_SOUSTITRE" , "DOC_RES","DOC_SEQ", "DOC_NOTE_INTERNE"
 	      , "DOC_TITRE_COL","DOC_AUTRE_TITRE", "DOC_RECOMP", "DOC_ACC", "DOC_IM_ARCH",
 	      "DOC_COMMENT","DOC_COMMISSION", "DOC_COPYRIGHT", "DOC_PROCAV","DOC_RES_CAT", "DOC_VISA"
 	      , "DOC_LIEU_PV", "DOC_XML", "DOC_VAL", "DOC_LEX","DOC_CAT", "DOC_TRANSCRIPT","DOC_NOTES","DOC_DROITS"
		  ,"DOC_TEXT_1","DOC_TEXT_2","DOC_TEXT_3","DOC_TEXT_4","DOC_TEXT_5","DOC_TEXT_6","DOC_TEXT_7","DOC_TEXT_8","DOC_TEXT_9");


	// VP 26/06/09 : ajout champ DOC_DATE_ETAT
	// VP 25/02/10 : ajout champ DOC_DATE_FIN_DROITS
	// PC 16/02/11 : ajout champ DOC_DATE_CREA et DOC_ID_USAGER_CREA
 	private $arrFieldsToSaveWithoutIdLang= array(
 	     "DOC_TITREORI", "DOC_ORI_TYPE_SON","DOC_ORI_FORMAT","DOC_ORI_COULEUR" ,"DOC_ID_FONDS","DOC_ID_MEDIA",
 	     "DOC_DIFFUSEUR" ,"DOC_ID_GENRE","DOC_ID_TYPE_DOC" , "DOC_ID_GEN","DOC_TITREORI",
 	     "DOC_TYPE","DOC_DUREE", "DOC_NB_EPISODES","DOC_COTE_ANC", "DOC_COTE","DOC_DATE_DIFF","DOC_DATE_DIFF_FIN",
 	     "DOC_DATE","DOC_DATE_ETAT","DOC_DATE_PROD","DOC_DATE_PV_FIN", "DOC_DATE_PV_DEBUT","DOC_NOTE", "DOC_VI","DOC_AUD"
 	     , "DOC_AUD_PDM","DOC_ID_USAGER_MODIF","DOC_DATE_MOD", "DOC_NUM", "DOC_TCIN", "DOC_TCOUT", "DOC_DATE_FIN_DROITS", "DOC_DUREE_DIFF",
		"DOC_DATE_CREA", "DOC_ID_USAGER_CREA","DOC_DATE_DROIT_DEB","DOC_DATE_DROIT_FIN","DOC_ID_IMPORT","DOC_SHORT_URL","DOC_NB_VIEWS","DOC_NB_LIKES","DOC_ID_ETAT_DOC","DOC_DATE_1","DOC_DATE_2","DOC_DATE_3","DOC_DATE_4");


    private $arrTextFields=array("doc_titre","doc_soustitre","doc_titre_col","doc_autre_titre","doc_titreori",
			"doc_res","doc_seq","doc_notes","doc_recomp","doc_acc",
			"doc_comment","doc_procav","doc_note_interne");

    private $arrDateFields=array("DOC_DATE_DIFF","DOC_DATE_DIFF_FIN","DOC_DATE_ETAT","DOC_DATE_PV_FIN","DOC_DATE_PV_DEBUT",
			"DOC_DATE","DOC_DATE_FIN_DROITS","DOC_DATE_DROIT_DEB","DOC_DATE_DROIT_FIN","DOC_DATE_1","DOC_DATE_2","DOC_DATE_3","DOC_DATE_4"); // liste des champs de type texte qui représentent des dates

// Constructeur
    function __construct($id=null, $version=""){
    	parent::__construct('t_doc',$id, $version); 
    	if (defined("gDocLangNotDependent") && gDocLangNotDependent){
    		array_push($this->arrFieldsToSaveWithoutIdLang, "DOC_ACCES");
		}else{
    		array_push($this->arrFieldsToSaveWithIdLang, "DOC_ACCES");
		}
		
		if (defined('gDocUniqueFields')){
			$this->uniqueFields=(array)unserialize(gDocUniqueFields);
		}
	}

	function getArrFieldsToSaveWithIdLang(){
		return $this->arrFieldsToSaveWithIdLang;
	}


	/**
	 * Initialise l'objet en récupérant les champs depuis la base
	 */

	function init() {
		parent::init();
    	$this->t_doc['ID_USAGER']=User::getInstance()->UserID;
    	$this->t_doc['ID_LANG']=$_SESSION['langue']; // par deft, langue en cours
	}

	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)

	 * 	IN : tableau

	 * 	OUT : objet màj

	 */

 	function updateFromArray ($tab_valeurs,$allFields=true) {
	 	if (empty($this->t_doc)) $this->init();

		foreach ($tab_valeurs as $fld=>$val) {
			//Correction injection SQL sur l'identifiant du document
			if (strtoupper($fld)=='ID_DOC')
				$val=intval($val);

			// VP 30/06/09 : remplacement isset par array_key_exists à cause des valeurs nulles
			//if (isset($this->t_doc[strtoupper($fld)])) $this->t_doc[strtoupper($fld)]=stripControlCharacters($val);
			if (array_key_exists(strtoupper($fld),$this->t_doc)) $this->t_doc[strtoupper($fld)]=stripControlCharacters($val);
		}

		 //Si on n'utilise que des champs dans tab_valeurs (ex: sequences, sauv doc)
		//on vire les champs non présents dans tab_valeurs, qui ne sont pas des clés primaires.
		if (!$allFields) {
			//préparation d'un tableau contenant juste les clés uppercased de tab_valeurs
			$arrUpperKeys=array_keys(array_change_key_case($tab_valeurs,CASE_UPPER));

			foreach ($this->t_doc as $fld=>$_v) { //07/04/08 ai viré le test && empty($_v) car une valeur vide EST significative !
				if (empty($this->arrPrimaryKeys)) $this->arrPrimaryKeys = array();
				if (!in_array($fld,$arrUpperKeys) && !in_array($fld,$this->arrPrimaryKeys)) {
					unset($this->t_doc[$fld]);
				}
			}

		}
		if (isset($tab_valeurs['arrFather']) && $tab_valeurs['arrFather']) $this->t_doc['DOC_ID_GEN']=$tab_valeurs['arrFather'][0]['ID_DOC']; // Récup du parent depuis le tableau

		if (isset($tab_valeurs['arrChildren']))
			$this->arrChildren=$tab_valeurs['arrChildren'];

		if (isset($tab_valeurs['arrFather']))
			$this->arrFather=$tab_valeurs['arrFather'];

	}


	/**
	 *	getData alias getDoc, 
	 * Récupère les infos sur un ou plusieurs doc.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets doc, sinon màj de l'objet en cours
	 
	 Ex d'appels : 
		getData() récupère l'objet et alimente la table principale de l'objet courant
			=> il faut que l'objet contienne les informations (id_doc / id_lang) necessaires
		getData(<array ids docs>) // MSVERIF - A terme je ne suis pas sur que cette partie de la fonctionnalité soit conservée, devrait être une fonction statique à part
			=> renvoi un array d'objets 
		
	 */
	function getData($arrData=array(),$version="") {
		global $db;
		// Si arrData est rempli, getdata renverra un array contenant les objets en question,
		// sinon $this->t_doc sera alimenté mais arrDocs sera vide
		$arrDocs = parent::getData($arrData,$version);
		
		if (empty($arrDocs) && !empty($this->t_doc) && !$arrData) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->spaceQuotes(array(" ' "," ’ "),array("'","’"));
				$this->type_doc=GetRefValue("t_type_doc",$this->t_doc["DOC_ID_TYPE_DOC"],$this->t_doc['ID_LANG']);
				//PC 02/10/12 : ajout libellé etat archivage
				$this->etat_arch=GetRefValue("t_etat_arch",$this->t_doc["DOC_ID_ETAT_ARCH"],$this->t_doc['ID_LANG']);
				$this->etat_doc=GetRefValue("t_etat_doc",$this->t_doc["DOC_ID_ETAT_DOC"],$this->t_doc['ID_LANG']);
				$this->getVignette();

				if (!empty($this->t_doc['DOC_ID_GEN'])) {
						$sql="select distinct T1.*, TD.TYPE_DOC,
							t_image.IM_CHEMIN,t_image.IM_FICHIER
							from t_doc T1
							left join t_image on (T1.DOC_ID_IMAGE=t_image.ID_IMAGE  and t_image.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
							left outer join t_type_doc TD on (TD.ID_TYPE_DOC = T1.DOC_ID_TYPE_DOC and TD.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
							where T1.ID_DOC=".intval($this->t_doc['DOC_ID_GEN'])." and T1.ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])." order by T1.DOC_TCIN"; //modifié le 31/1 par LD pour trier sur TCIN
					
						$this->arrFather=$db->GetAll($sql);
					}
				}
		else {
			foreach ($arrDocs as $idx=>$doc) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrDocs[$idx]->type_doc=GetRefValue("t_type_doc",$arrDocs[$idx]->t_doc["DOC_ID_TYPE_DOC"],$arrDocs[$idx]->t_doc['ID_LANG']);
				$arrDocs[$idx]->etat_doc=GetRefValue("t_etat_doc",$arrDocs[$idx]->t_doc["DOC_ID_ETAT_DOC"],$arrDocs[$idx]->t_doc['ID_LANG']);
				$arrDocs[$idx]->spaceQuotes(array(" ' "," ’ "),array("'","’"));
				$arrDocs[$idx]->getVignette();
				//PC 02/10/12 : ajout libellé etat archivage
				$arrDocs[$idx]->etat_arch=GetRefValue("t_etat_arch",$arrDocs[$idx]->t_doc["DOC_ID_ETAT_ARCH"],$arrDocs[$idx]->t_doc['ID_LANG']);
			}
			unset($this); // plus besoin
			return $arrDocs;
		}
	}
	function getDoc($arrData=array(), $version="") {
		return $this->getData($arrData,$version);
	}


	/**
	 * Récupère la vignette de la notice
	 * IN : objet DOC
	 * OUT : var de classe vignette mise à jour.
	 * NOTE : selon que les champs DOC_ID_IMAGE ou DOC_ID_DOC_ACC sont renseignés, on ira
	 *        chercher la vignette dans le storyboard ou dans les documents d'accompagnement
	 */
	function getVignette() {
		global $db;
		if (!empty($this->t_doc['DOC_ID_IMAGE'])) {
			$vignetteChemin=$db->GetRow('SELECT IM_CHEMIN,IM_FICHIER FROM t_image WHERE ID_IMAGE='.intval($this->t_doc['DOC_ID_IMAGE']));
			$relativePathFromMedia=str_replace(kCheminHttpMedia,'',storyboardChemin);
			if ($vignetteChemin) $this->vignette= $relativePathFromMedia.$vignetteChemin['IM_CHEMIN']."/".$vignetteChemin['IM_FICHIER'];
		}

		if (!empty($this->t_doc['DOC_ID_DOC_ACC'])) {
			$vignetteChemin=$db->GetRow('SELECT DA_CHEMIN, DA_FICHIER FROM t_doc_acc WHERE ID_DOC_ACC='.intval($this->t_doc['DOC_ID_DOC_ACC']));
			$relativePathFromMedia=str_replace(kCheminHttpMedia,'',kDocumentUrl);
			if ($vignetteChemin) {
				$vignetteChemin = $vignetteChemin['DA_CHEMIN']."/".$vignetteChemin['DA_FICHIER'];
				$this->vignette= (strpos($vignetteChemin,'http://')===0?$vignetteChemin:$relativePathFromMedia.$vignetteChemin);
			}
		}
	}


	/** Récupère les libellés pour toutes les langues de l'appli.
	 *  IN : rien (objet)
	 * 	OUT : tableau de classe arrVersions
	 *  NOTE : si le lexique existe déjà en base, on ramènes ttes les langues même si le lib n'existe pas encore (right join)
	 * 		   si le lexique  n'existe pas (création), on ramène toutes les langues.
	 */
	function getVersions () {
		global $db;

		if (!empty($this->t_doc['ID_DOC'])) {
		$sql="SELECT t_lang.ID_LANG, DOC_TITRE,DOC_RES,DOC_ACCES, ID_DOC FROM t_doc
				 join t_lang on (t_lang.ID_LANG=t_doc.ID_LANG)
				WHERE
				t_doc.ID_LANG<>".$db->Quote($this->t_doc['ID_LANG'])." AND ID_DOC=".intval($this->t_doc['ID_DOC']);
		$this->arrVersions=$db->GetAll($sql);
		}
	}

	/**
	 * Récupère les documents d'accompagnement, toutes versions confondues.
	 */

	function getDocAcc() {
		global $db;
		require_once(modelDir.'model_docAcc.php');
		unset($this->t_doc_acc);

		$sql="SELECT ID_DOC_ACC,ID_LANG FROM t_doc_acc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])."
				AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])." ORDER BY DA_ORDRE,ID_DOC_ACC,ID_LANG";//by ld 250808 restriction langue
		$rs=$db->GetAll($sql);
		foreach ($rs as $idx=>$fld) {
			$docAcc= new DocAcc();
			$docAcc->t_doc_acc['ID_DOC_ACC']=$fld['ID_DOC_ACC'];
			$docAcc->t_doc_acc['ID_LANG']=$fld['ID_LANG'];
			$docAcc->getDocAcc();
			$docAcc->getValeurs();
			$this->t_doc_acc[]=$docAcc;
		}
		//debug($this->t_doc_acc,'yellow');
	}

	function getDocRights() {
		global $db;
		require_once(modelDir.'model_right.php');
		unset($this->t_doc_right);
		$sql="SELECT distinct ID_RIGHT FROM t_right WHERE ID_PANIER in (SELECT ID_PANIER FROM t_panier_doc WHERE ID_DOC =".intval($this->t_doc['ID_DOC'])."
				) ORDER BY ID_RIGHT";//by ld 250808 restriction langue
		$rs=$db->GetAll($sql);
		foreach ($rs as $idx=>$fld) {
			$right = new Right();
			$right->t_right['ID_RIGHT']=$fld['ID_RIGHT'];
			$right->getRights();
			$right->getRightsVal();
			$right->getUsager();
			$this->t_doc_right[]=$right;
		}
		//debug($this->t_doc_acc,'yellow');
	}

	//NB 24 03 2015 Récupération des commentaire et de valeurs associées pour systèlme de notation (initialement pour Projet DGA)
	function getDocComments($withVals=false) {
		global $db;
		unset($this->t_doc_comment);

		// NB 18 03 2015 le str_replace est indispensable lorque $this->t_doc['ID_DOC'] ='135' par ex
		$sql="SELECT distinct dc.* FROM t_doc_comment dc WHERE ID_DOC =".intval(str_replace("'","",$this->t_doc['ID_DOC']))." ORDER BY DC_DATE_CREA DESC";
		$rs=$db->GetAll($sql);

		if ($rs) {
			$this->t_doc_comment = $rs;
			if($withVals==true){
				foreach ($rs as $key => $value) {
					$this->t_doc_comment[$key]['DOC_COMMENTS_VAL']= $this->getDocCommentsVal($value['ID_COMMENT']);
				}
			}
			return $this->t_doc_comment;
		} else {
			return false;
		}
	}

	function getDocCommentsVal($id_comment='') {
		global $db;
		$sql="SELECT dcv.id_comment, dcv.id_val,v.valeur, dcv.dcv_note FROM t_doc_comment_val dcv
						JOIN t_val v ON dcv.id_val = v.id_val
						WHERE ID_COMMENT =".intval($id_comment)." AND ID_LANG = '".$_SESSION['langue']."'
				 		ORDER BY dcv.id_comment";
		$rs=$db->GetAll($sql);
		return $rs;
	}

	function getDocPublication() {
		global $db;
		unset($this->t_doc_publication);
		$sql="SELECT distinct dp.*, j.JOB_ID_ETAT as ETAT_PUB FROM t_doc_publication dp LEFT JOIN t_job j ON dp.PUB_ID_JOB = j.ID_JOB WHERE ID_DOC =".intval($this->t_doc['ID_DOC']);
		$rs=$db->GetAll($sql);
		if ($rs){
			$this->t_doc_publication = $rs;
			// foreach ($rs as $dp) $this->t_doc_publication[$dp['PUB_ID_ETAPE']] = $dp;
			return $this->t_doc_publication;
		}else{
			return false;
		}
	}

	/**
	 * Récupère les ID des séquences liées à ce document
	 * IN : rien
	 * OUT : tab de classe arrChildren
	 */

	function getChildren() {
		global $db;
		$sql="SELECT * from t_doc WHERE DOC_ID_GEN=".intval($this->t_doc['ID_DOC'])."  AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
		$this->arrChildren=$db->GetAll($sql);
	}

	/**
	 * Récupère la liste des rôles possibles pour un ID_TYPE_DESC
	 * IN : ID_TYPE_DESC
	 * OUT : tableau de valeurs de type ID/LIBELLE
	 * NOTE : cette fonction peut paraître hors sujet mais cette relation ID_TYPE_DESC / ROLES ne se pose que lors de la
	 * saisie des docs
	 */

	 function getRoles($id_type_desc='',$add_blank=false) {
		global $db;
		if ($add_blank) $arrRoles[]='---';
		if(!empty($this->t_doc['ID_LANG'])){
			$lang = $this->t_doc['ID_LANG'];
		}else{
			$lang = $_SESSION['langue'];
		}
		$sql = "SELECT ID_ROLE,ROLE FROM t_role WHERE ID_LANG=".$db->Quote($lang);
		if(!empty($id_type_desc)){
			$sql .= " and ID_TYPE_DESC=".$db->Quote($id_type_desc);
		}
		$sql .=" ORDER BY ROLE";
		//$rs=$db->Execute("SELECT ID_ROLE,ROLE  FROM t_role WHERE ID_TYPE_DESC=".$db->Quote($id_type_desc)." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])." ORDER BY ROLE" );
		$rs=$db->Execute($sql);
		$arrRoles = array();
		while ($row=$rs->FetchRow()) {
			$arrRoles[$row['ID_ROLE']]=$row['ROLE'];
		}
		$rs->Close();
		return $arrRoles;
	 }


	/** MAJ dans la base des champs de type XML : doc_val, doc_xml, doc_lex
 	 *  Cette fonction doit être appelée APRES la sauvegarde complète de la fiche
 	 *  IN : objet doc, type (champ à mettre à jour)
 	 *  OUT : ce champs est mis à jour dans t_doc
 	 *  Note : ces champs sont utilisés en cas de recherche FULLTEXT sur tous les champs texte d'un document
 	 */

    function updateChampsXML($type,$arrLangues=null){
        global $db;
			if ($arrLangues===null) $arrLangues=$_SESSION['arrLangues'];
            switch ($type)
            {
                case "DOC_LEX": // MAJ LEXIQUE : on notera la boucle sur les langues
                        foreach($arrLangues as $value)
                        {
						// Lexique
							// VP 9/02/10 : ajout LEX_PATH dans DOC_LEX
                        	$sql="SELECT t_lexique.ID_LEX,LEX_TERME,LEX_PATH, t_doc_lex.ID_TYPE_DESC, DLEX_ID_ROLE, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE
									FROM t_lexique
									INNER JOIN t_doc_lex ON t_lexique.ID_LEX=t_doc_lex.ID_LEX
									left outer join t_doc_lex_precision on (t_doc_lex.ID_DOC_LEX=t_doc_lex_precision.ID_DOC_LEX and t_doc_lex_precision.ID_LANG='".$value."')
                        			LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG='" . $value . "')";
                        	$sql.=" WHERE t_doc_lex.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_lexique.ID_LANG='".$value."'";

                            $result=$db->Execute($sql);
                            $doc_lex="";
                            while($list=$result->FetchRow()) {
                                $doc_lex.="<LEX TYPE='".trim($list["ID_TYPE_DESC"])."' ROLE='".trim($list["DLEX_ID_ROLE"])."'>\n";
                                $doc_lex.="\t<TERME> ".strtr($list["LEX_TERME"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </TERME>\n";
                                $doc_lex.="\t<ROLE> ".strtr($list["ROLE"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </ROLE>\n";
								$mots=explode(chr(9),$list["LEX_PATH"]);
								foreach($mots as $mot){
									if(!empty($mot)){
										$doc_lex.="\t<PATH> ".strtr($mot,array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </PATH>\n";
									}
								}
                				$doc_lex.="\t<ID_LEX> ".$list["ID_LEX"]." </ID_LEX>\n";
                				if (trim($list["LEX_PRECISION"])!="") {$doc_lex.="\t<PRECISION> ".strtr($list["LEX_PRECISION"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )." </PRECISION>\n";}
                            	$doc_lex.="</LEX>\n";
                            }

						// Personnes
                        	$sql="SELECT t_personne.ID_PERS,PERS_NOM,PERS_PRENOM, PERS_PARTICULE, PERS_ORGANISATION, t_doc_lex.ID_TYPE_DESC, DLEX_ID_ROLE, t_doc_lex_precision.LEX_PRECISION, t_doc_lex.DLEX_REVERSEMENT, t_role.ROLE
									FROM t_personne
									INNER JOIN t_doc_lex ON t_personne.ID_PERS=t_doc_lex.ID_PERS";
							$sql.=" left outer join t_doc_lex_precision on (t_doc_lex.ID_DOC_LEX=t_doc_lex_precision.ID_DOC_LEX and t_doc_lex_precision.ID_LANG='".$value."')";
							$sql.=" LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG='" . $value . "')";
							$sql.=" WHERE t_doc_lex.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_personne.ID_LANG='".$value."'";
							$sql.=" ORDER BY t_doc_lex.ID_DOC_LEX";
                            $result=$db->Execute($sql);
                            while($list=$result->FetchRow()) {
                                $doc_lex.="<LEX TYPE='".trim($list["ID_TYPE_DESC"])."' ROLE='".trim($list["DLEX_ID_ROLE"])."'>\n";
                 				//$doc_lex.="\t<TERME> ".strtr($list["PERS_NOM"]." ".$list["PERS_PRENOM"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </TERME>\n";
                				//by LD 18 03 09 : suppression du terme au profit de nom, prenom, particule, organisation
                				$doc_lex.="\t<TERME>";
                				$doc_lex.="\t<PERS_PARTICULE> ".strtr($list["PERS_PARTICULE"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </PERS_PARTICULE>\n";
                				$doc_lex.="\t<PERS_NOM> ".strtr($list["PERS_NOM"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </PERS_NOM>\n";
                				$doc_lex.="\t<PERS_PRENOM> ".strtr($list["PERS_PRENOM"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </PERS_PRENOM>\n";
                				$doc_lex.="\t</TERME>";
                				$doc_lex.="\t<ROLE> ".strtr($list["ROLE"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </ROLE>\n";
                				$doc_lex.="\t<ORGANISATION> ".strtr($list["PERS_ORGANISATION"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))." </ORGANISATION>\n";
                				$doc_lex.="\t<ID_PERS> ".$list["ID_PERS"]." </ID_PERS>\n";
                				if (trim($list["LEX_PRECISION"])!="") {$doc_lex.="\t<PRECISION> ".strtr($list["LEX_PRECISION"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )." </PRECISION>\n";}
                				if (trim($list["DLEX_REVERSEMENT"])!="") {$doc_lex.="\t<REVERSEMENT> ".strtr($list["DLEX_REVERSEMENT"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )." </REVERSEMENT>\n";}
                            	$doc_lex.="</LEX>\n";
                            }

							// FT : filtrage apostrophe
                            if(kDbType != 'postgres' && kDbType != 'postgres7'){
                                $doc_lex=str_replace("'"," ' ",$doc_lex);
                            }
							if (!empty($doc_lex)) $doc_lex="<XML>".$doc_lex."</XML>";
							$result->Close();

                            // II.A.2 Update du champs DOC_LEX de $quoi
							$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($value));
							$record = array();
							$record["DOC_LEX"] = $doc_lex;
							$sql = $db->GetUpdateSQL($rs, $record);
							if (!empty($sql)) $db->Execute($sql);
                            $result->Close();
                        }
                    break;

                case "DOC_VAL": // MAJ VALEURS
                    foreach($arrLangues as $value)
                    {
                        $sql="SELECT t_val.VALEUR, t_doc_val.ID_TYPE_VAL as VAL_ID_TYPE_VAL
								FROM t_val
								INNER JOIN t_doc_val ON t_val.ID_VAL=t_doc_val.ID_VAL
								WHERE t_doc_val.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_val.ID_LANG='".$value."'";
                        $result=$db->Execute($sql);
                        $doc_val="";
                        while($list=$result->FetchRow()) {
                            $doc_val.="<".$list["VAL_ID_TYPE_VAL"].">".strtr($list["VALEUR"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</".$list["VAL_ID_TYPE_VAL"].">\n";
                        }
						// FT : filtrage apostrophe
						if(kDbType != 'postgres' && kDbType != 'postgres7'){
                            $doc_val=str_replace("'"," ' ",$doc_val);
                        }
						if (!empty($doc_val)) $doc_val="<XML>".$doc_val."</XML>";
						$result->Close();

                        // II.B.2 Mise ? jour de DOC_VAL de $quoi
                        $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($value));
						$record = array();
						$record["DOC_VAL"] = $doc_val;
						$sql = $db->GetUpdateSQL($rs, $record);
                        if (!empty($sql)) $db->Execute($sql);
                        $this->t_doc['DOC_VAL']=$doc_val;
                    }
                    break;

				case "DOC_CAT": // MAJ Catégories
                    foreach($arrLangues as $value)
                    {
                        $sql="SELECT t_categorie.CAT_NOM, t_categorie.CAT_PATH, t_categorie.ID_CAT, t_categorie.CAT_ID_TYPE_CAT,t_categorie.CAT_CODE
								FROM t_categorie
								INNER JOIN t_doc_cat ON t_categorie.ID_CAT=t_doc_cat.ID_CAT
								WHERE t_doc_cat.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_categorie.ID_LANG='".$value."'";
                        $result=$db->Execute($sql);
                        $doc_cat="";
                        while($list=$result->FetchRow()) {
							$doc_cat.="<CAT TYPE='".trim($list["CAT_ID_TYPE_CAT"])."' >\n";
							foreach($list as $key=>$val){
								$doc_cat.="<".$key.">".strtr($val,array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ))."</".$key.">\n";
							}
							$doc_cat.="</CAT>\n";
                           }
						// FT : filtrage apostrophe
						if(kDbType != 'postgres' && kDbType != 'postgres7'){
                            $doc_cat=str_replace("'"," ' ",$doc_cat);
                        }
						if (!empty($doc_cat)) $doc_cat="<XML>".$doc_cat."</XML>";
						$result->Close();

                        // II.B.2 Mise a jour de doc_cat
                        $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($value));
						$record = array();
						$record["DOC_CAT"] = $doc_cat;
						$sql = $db->GetUpdateSQL($rs, $record);
                        if (!empty($sql)) $db->Execute($sql);
                        $this->t_doc['DOC_CAT']=$doc_cat;
                    }
                    break;

                case "DOC_XML": // MAJ XML (champs un peu fourre-tout)
                    foreach($arrLangues as $value)
                    {
                        $sql = "SELECT T1.DOC_COTE,T2.DOC_TITRE, T2.DOC_COTE as PARENT_COTE, t_genre.GENRE, t_fonds.FONDS, t_type_doc.TYPE_DOC, t_etat_doc.ETAT_DOC,t_fonds.FONDS_COL,T3.fonds AS FONDS_PARENT
							FROM t_doc T1
							LEFT OUTER JOIN t_doc T2 ON (T2.ID_DOC=T1.DOC_ID_GEN AND T1.ID_LANG=T2.ID_LANG)
							LEFT OUTER JOIN t_genre ON (T1.DOC_ID_GENRE=t_genre.ID_GENRE AND T1.ID_LANG=t_genre.ID_LANG)
							LEFT OUTER JOIN t_fonds ON (T1.DOC_ID_FONDS=t_fonds.ID_FONDS AND T1.ID_LANG=t_fonds.ID_LANG)
							LEFT OUTER JOIN t_type_doc ON (T1.DOC_ID_TYPE_DOC=t_type_doc.ID_TYPE_DOC AND T1.ID_LANG=t_type_doc.ID_LANG)
							LEFT OUTER JOIN t_etat_doc ON (T1.DOC_ID_ETAT_DOC=t_etat_doc.ID_ETAT_DOC AND T1.ID_LANG=t_etat_doc.ID_LANG)
							LEFT OUTER JOIN t_fonds T3 ON (t_fonds.ID_FONDS_GEN = T3.ID_FONDS AND T1.ID_LANG=T3.ID_LANG)
							WHERE T1.ID_DOC=".intval($this->t_doc['ID_DOC'])." and T1.ID_LANG='".$value."'";
                        $result=$db->Execute($sql);
                        $doc_xml="";
						// VP 15/01/10 : ajout filtrage caractères XML interdits
						// VP 29/01/10 : ajout DOC_COTE dans DOC_XML
                        while($list=$result->FetchRow()) {

							// VP 12/02/10 : possibilité de ne pas sauver le titre parent
							if (defined('gDontStoreTitreParent') && gDontStoreTitreParent==true) $doc_xml.="<DOC>";
							else {
								$doc_xml.="<DOC><PARENT_TITRE>".strtr($list["DOC_TITRE"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</PARENT_TITRE>";
								$doc_xml.="<PARENT_COTE>".strtr($list["DOC_TITRE"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</PARENT_COTE>";
							}
							if (!defined('gDontStoreFonds') || gDontStoreFonds==false){
                            	$doc_xml.="<FONDS>".strtr($list["FONDS"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</FONDS>";
							}
                            $doc_xml.="<FONDS_COL>".strtr($list["FONDS_COL"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</FONDS_COL>";
                            $doc_xml.="<FONDS_PARENT>".strtr($list["FONDS_PARENT"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</FONDS_PARENT>";
                            $doc_xml.="<GENRE>".strtr($list["GENRE"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</GENRE>";
                            $doc_xml.="<TYPE_DOC>".strtr($list["TYPE_DOC"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</TYPE_DOC>";
                            $doc_xml.="<ETAT_DOC>".strtr($list["ETAT_DOC"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</ETAT_DOC>";
							if (!empty($this->etat_arch))
								$doc_xml.="<ETAT_ARCH>".strtr($this->etat_arch,array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</ETAT_ARCH>";
                            $doc_xml.="<DOC_COTE>".strtr($list["DOC_COTE"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</DOC_COTE></DOC>";
                        }
                        $result->Close();

                        //by LD 30/10/08, on ajoute les festivals
                        $sql="SELECT f.*
							from t_festival f
							inner join t_doc_fest df ON df.id_fest=f.id_fest
							where f.id_lang=".$db->Quote($this->t_doc['ID_LANG'])." and df.id_doc=".intval($this->t_doc['ID_DOC']);
						$arrFest=$db->GetAll($sql);
						if (!empty($arrFest)) {
							$doc_xml.="<FESTIVAL>";
							foreach ($arrFest as $fest) {
	                            $doc_xml.="<FEST><ID_FEST>".$fest["ID_FEST"]."</ID_FEST>";
	   		                    $doc_xml.="<FEST_ANNEE>".$fest["FEST_ANNEE"]."</FEST_ANNEE>";
	                            $doc_xml.="<FEST_DESC>".strtr($fest["FEST_DESC"],array("<"=>"&lt;", ">"=>"&gt;","&"=>"&amp;" ) )."</FEST_DESC></FEST>";
							}
							$doc_xml.="</FESTIVAL>";
						}
                        if(kDbType != 'postgres' && kDbType != 'postgres7'){
                            $doc_xml=str_replace("'"," ' ",$doc_xml);
                        }
						if (!empty($doc_xml)) $doc_xml="<XML>".$doc_xml."</XML>";
                        $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($value));
						$record = array();
						$record["DOC_XML"] = $doc_xml;
						$sql = $db->GetUpdateSQL($rs, $record);
                        if (!empty($sql)) $ok=$db->Execute($sql);

                        if (!empty($sql) && !$ok) echo "Pb SQL Maj XML >>> ".$sql;
                    }
                    break;
            }
    }


    /**
     * Création en base d'un nouveau doc avec les infos de base (initialisées à la créa objet)
     * IN : id_mat (si on crée des doc_mat dans la foulée)
     * OUT : nv doc créé en base, var de classe ID_DOC renseignée avec nv ID
     *
     */

    function initDoc($id_mat=""){
        global $db;
		// LD 11/12/08 : extension à d'autres champs que DOC_TITRE
		// VP 15/07/09 : extension à DOC_COTE
        if (empty($this->t_doc['DOC_TITRE']) && empty($this->t_doc['DOC_SOUSTITRE'])
        	&& empty($this->t_doc['DOC_TITREORI']) && empty($this->t_doc['DOC_AUTRE_TITRE']) && empty($this->t_doc['DOC_COTE'])) return false;

		if (isset($_SESSION['USER']['ID_USAGER']) && !empty($_SESSION['USER']['ID_USAGER']))
		{
			if (empty($this->t_doc['DOC_ID_USAGER_CREA'])) $this->t_doc['DOC_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];
			$this->t_doc['DOC_ID_USAGER_MODIF']=$_SESSION['USER']['ID_USAGER'];
		}
		//by LD 10/11/08 possté de créer avec un ID spécifique, sinon 0
		if (!empty($this->t_doc['ID_DOC'])) $id=$this->t_doc['ID_DOC']; else $id=0;
		// VP 16/09/09 : langue par défaut au cas où
    	if(empty($this->t_doc['ID_LANG'])) $this->t_doc['ID_LANG']=$_SESSION['langue']; // par deft, langue en cours

        // Insert avec langue de saisie
        //$sql ="INSERT INTO t_doc (ID_DOC, ID_LANG, DOC_ID_MEDIA, DOC_ID_USAGER_CREA, DOC_ID_USAGER_MODIF, DOC_DATE_CREA, DOC_DATE_MOD)";
        //$sql.=" VALUES ($id,".$db->Quote($this->t_doc['ID_LANG']).",".$db->Quote($this->t_doc['DOC_ID_MEDIA']).",".intval($this->t_doc['DOC_ID_USAGER_CREA']).",".intval($this->t_doc['DOC_ID_USAGER_MODIF']).",NOW(),NOW())";
		// $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC = -1");
		$record = array("ID_LANG" => "".$this->t_doc['ID_LANG'],"DOC_ID_MEDIA" => "".$this->t_doc['DOC_ID_MEDIA'],"DOC_ID_USAGER_CREA" => intval($this->t_doc['DOC_ID_USAGER_CREA']),"DOC_ID_USAGER_MODIF" => intval($this->t_doc['DOC_ID_USAGER_MODIF']),"DOC_DATE_CREA" => "CURRENT_TIMESTAMP","DOC_DATE_MOD" => "CURRENT_TIMESTAMP");
		if (intval($id) > 0){
			$record["ID_DOC"] = $id;
			$this->update_pg_id_doc_seq();
		}
		// $sql = $db->GetInsertSQL($rs, $record);
		// $sql = str_replace("'NOW'", "CURRENT_TIMESTAMP", $sql);
		// $ok=$db->Execute($sql);
		   $ok = $db->insertBase("t_doc","id_doc",$record);

        if (!$ok) {$this->dropError(kErrorCreationDoc);return false;}

       	//if (empty($this->t_doc['ID_DOC'])) $this->t_doc['ID_DOC']=$db->getOne("SELECT max(ID_DOC) from t_doc");
       	if (empty($this->t_doc['ID_DOC'])) $this->t_doc['ID_DOC']=$ok;

		// VP 1/12/2017 : flag création doc
		$this->initDoc = true;
		
        if($id_mat!=""){
            $this->insererDocMat($id_mat);
        }
        $this->linkToAnotherDoc();	//Si on crée un nv document depuis les liens doc.
    	return true;
    }


	/**
	 * Met à jour le DOC_NUM de t_doc lorsque le matériel existe physiquement sur le serveur
	 * IN : ID_MAT, ID_DOC
	 * OUT : rtn TRUE/FALSE + update BDD
	 * Note : OBSOLETE ! A retirer partout, utiliser updateDocNum à la place
	 */

	function metAJourDocNum($id_mat){
	    global $db;
	    include_once(modelDir.'model_materiel.php');
		$mat=new Materiel();
		$mat->t_mat['ID_MAT']=$id_mat;
		$mat->getMat();
	    if (is_file($mat->getFilePath())){
	        $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC = ".intval($this->t_doc['ID_DOC']));
			$record = array("DOC_NUM" => "1");
			if (!empty($mat->id_media) && empty($this->t_doc['DOC_ID_MEDIA'])) $record["DOC_ID_MEDIA"] = $mat->id_media;
			$sql = $db->GetUpdateSQL($rs, $record);
			if (!empty($sql)) $db->Execute($sql);
	        return TRUE;
	    }else{
	        return FALSE;
	    }
	}

	/** Met à jour le champ DOC_NUM indiquant qu'un doc est visionnable
	 *  Pour cela, on récupère la liste des matériels liés puis on vérifie qu'au moins un fichier existe,
	 *  qu'il est d'un format visionnable
	 *  IN : tab de classe t_doc_mat
	 *  OUT : maj DOC_NUM de l'objet ET en base
	 */

	function updateDocNum() {
		$id_doc = intval($this->t_doc['ID_DOC']);
		if(empty($id_doc)) {
			return false;
		}
		global $db;
		$doc_num='0';
		require_once(libDir."class_visualisation.php");
		if (empty($this->t_doc_mat) || !$this->t_doc_mat[0]['MAT']) $this->getMats();
		//debug($this->t_doc_mat,'violet');
		foreach ($this->t_doc_mat as $idx=>$dm) {
			if ($dm['MAT']->hasfile=='1' && $doc_num=='0' && in_array($dm['MAT']->t_mat['MAT_FORMAT'],Visualisation::getFormatsForVisu())) {
				$doc_num='1';
				$id_media = $dm['MAT']->id_media;
			}
		}
		$this->t_doc['DOC_NUM']=$doc_num;
		$this->t_doc['DOC_ID_MEDIA']=$id_media;
		//LD 26/05/08 => on met aussi à jour le doc_num des séquences (doc fils)
		$rs = $db->Execute('SELECT * FROM t_doc WHERE (ID_DOC='.intval($this->t_doc['ID_DOC']).' OR DOC_ID_GEN='.intval($this->t_doc['ID_DOC'])." )");
		$record = array("DOC_NUM" => $doc_num);
		if (!empty($id_media)) $record["DOC_ID_MEDIA"] = $id_media;
		$sql = $db->GetUpdateSQL($rs, $record);
		if (!empty($sql)) $db->Execute($sql);
		// VP 09/11/09 : mise à jour DOC_NUM dans Sinequa
		if (defined("useSinequa") && useSinequa) $this->saveSinequa(array("DOC_NUM"));

		if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))
			$this->saveSolr();

		if(defined('gReportagePicturesActiv')){
			require_once(modelDir.'model_docReportage.php');
			$this->getDocLiesSRC();
			if (empty($this->arrDocLiesSRC)){
				return true ;
			}
			foreach($this->arrDocLiesSRC as $doc){
				if($doc['DOC_ID_TYPE_DOC'] == gReportagePicturesActiv){
					$report = new Reportage();
					$report->t_doc['ID_DOC'] = $doc['ID_DOC'];
					$report->getDoc();
					$report->updateDocNum();
				}
			}
		}

	}


	function updateEtatArch() {
		global $db;
		if (!empty($this->t_doc['ID_DOC'])) {
			$new_etat = $db->getOne("SELECT max(MAT_ID_ETAT_ARCH)
										FROM t_mat m
										INNER JOIN t_doc_mat dm ON dm.ID_MAT = m.ID_MAT
										WHERE dm.ID_DOC = " . intval($this->t_doc['ID_DOC']));
			if (is_null($new_etat)) $new_etat = 0;
			$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC = " . intval($this->t_doc['ID_DOC']));
			$record = array("DOC_ID_ETAT_ARCH" => $new_etat);
			$sql = $db->GetUpdateSQL($rs, $record);
			if (!empty($sql)) $ok = $db->Execute($sql);
		}
	}


    /**
     * Duplication du document
     * IN : this
     * OUT : nouveau document dupliqué.
     */
    function duplicateDoc(){
    global $db;
		if (empty($this->arrVersions)) $this->getVersions(); // Récup des infos si pas encore loadées
		if (empty($this->t_doc_lex)) $this->getLexique();
        if (empty($this->t_pers_lex)) $this->getPersonnes();
		if (empty($this->t_doc_val)) $this->getValeurs();
		if (empty($this->t_doc_cat)) $this->getCategories();

        $dupDoc=new Doc();
        $dupDoc= clone $this; // Recopie des infos
		// $id_temp=$dupDoc->t_doc['ID_DOC'];
        unset($dupDoc->t_doc['ID_DOC']); // pour éviter les effets de bord
		// VP 24/2/09 : mise à 0 de DOC_NUM (pas de materiel lié)
		$dupDoc->t_doc['DOC_NUM']='0';


        $dupDoc->initDoc(); // récupération du nouvel ID
		trace('id doc dupliqué'.$dupDoc->t_doc['ID_DOC']);
		$dupDoc->t_doc['DOC_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
        $dupDoc->save(1); // Sauvegarde avec duplication à 1.

        // VP 3/09/2014 : récup des personnes dans t_pers_lex, mais sauvegarde via t_doc_lex
        $dupDoc->t_doc_lex=array_merge($dupDoc->t_doc_lex, $dupDoc->t_pers_lex);
        $dupDoc->saveDocLex();
        $dupDoc->saveDocVal();
        $dupDoc->saveDocCat();
		$dupDoc->updateChampsXML('DOC_LEX');
		$dupDoc->updateChampsXML('DOC_VAL');
		$dupDoc->updateChampsXML('DOC_CAT');
		$dupDoc->updateChampsXML('DOC_XML');
									
			foreach($_SESSION['arrLangues'] as $lg){
				if($lg!=$_COOKIE['cook_lang'])
				{
					$dupDoc1=new Doc();
					$dupDoc1->t_doc['ID_DOC']=$this->t_doc['ID_DOC'];
					$dupDoc1->t_doc['ID_LANG']=$lg;
					if($dupDoc1->checkExistId()){
						$dupDoc1->getDoc();
						$dupDoc1->t_doc['ID_DOC']=$dupDoc->t_doc['ID_DOC'];
						$dupDoc1->t_doc['DOC_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
						//trace(print_r($dupDoc1,true));
						$dupDoc1->initDoc();
						$dupDoc1->save(1);

					}
				}
			}

        return $dupDoc;
    }

	/**
		* Sélecteur d'entités à sauvegarder
	 * IN : tableau des entités à sauver
	 * OUT : sauvegardes
	 * NOTE : utilisé pour les modifs par lots
	 */
	function multiSave($arr2save=array()) {
		$this->save();

		if (isset($this->t_doc['VERSION'])){
			$version = $this->t_doc['VERSION'];
			unset($this->t_doc['VERSION']);
			$this->updateVersions($version);
		}


		// VP 7/09/11 : ajout updateChampsXML pour cas LEX
		if ($arr2save['VAL']) {$this->saveDocVal();$this->updateChampsXML('DOC_VAL');}

		if ($arr2save['CAT'])
		{
			$this->saveDocCat();
			$this->updateChampsXML('DOC_CAT');
		}
		if ($arr2save['LIENS']){$this->saveDocLies();}
		if ($arr2save['LEX']) {$this->saveDocLex();$this->updateChampsXML('DOC_LEX');}
		if ($arr2save['FEST']) {$this->saveDocFest();}
		if ($arr2save['MAT']) $this->saveDocMat();
		if ($arr2save['REDIF']) $this->saveDocRedif();
		if ($arr2save['SEQ']) $this->saveSequences();
		if ($arr2save['MAT_SEQ']) $this->saveDocMatForSequences();
		//@update VG
		if ($arr2save['DOC_XML']) $this->updateChampsXML('DOC_XML');
		if (defined("useSinequa") && useSinequa) $this->saveSinequa();

		if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))
            $this->saveSolr();
        return true;
	}


    /**
     * Sauve le doc en base
     * 1. Si créa d'un nv doc, vérifie si un doc existe (d'après DOC_COTE) et abandonne si oui
     * 2. Sinon, crée un nv document provisoire et récupère l'ID.
     * 3. QQ soit le mode, les valeurs des champs sont updatées en base...
     * 4. ... en deux phases : update pour la langue demandée, puis update pr ttes les langues dispo
     *
     * IN : langue (version à sauver), flag dupliquer O/N,
     * OUT : infos sauvées en base ou retour à false en cas d'erreur.
     */
    function save($dupliquer=0,$failIfExists=true,$updateUsagerMod = true){
        global $db;
        $new = false;
		if ($dupliquer==0) {
			$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
		}
		//by LD 11/12/08 : extension à d'autres champs que DOC_TITRE
		//VP 15/07/09 : extension à DOC_COTE
        if (empty($this->t_doc['DOC_TITRE']) && empty($this->t_doc['DOC_SOUSTITRE'])
        	&& empty($this->t_doc['DOC_TITREORI']) && empty($this->t_doc['DOC_AUTRE_TITRE']) && empty($this->t_doc['DOC_COTE'])) {
					$this->dropError(kErrorNoTitre);
					return false;
				}

        $db->StartTrans();
		if ($dupliquer==0) {
			// VP 15/04/10 : ajout méthode checkExist
			if($this->checkExist()){
				if ($failIfExists) {
					$this->dropError(kErrorCoteExistant);
					$db->CompleteTrans();
					return false;
				}
			}elseif(empty($this->t_doc['ID_DOC']) || !$this->checkExistId()) {
				$new = true;
				$this->initDoc();
				if (empty($this->t_doc['DOC_DATE_CREA'])) {
					$this->t_doc['DOC_DATE_CREA'] = str_replace("'","",$db->DBTimeStamp(time()));
				}
			}
		}
        //update VG 13/07/11
        if(isset($this->t_doc['DOC_ID_GEN']) && $this->t_doc['DOC_ID_GEN'] == $this->t_doc['ID_DOC']) {
        	$this->t_doc['DOC_ID_GEN'] = '0';
        }
				/*
		 //By LD => si on met le champ ID_DOC en premier uniqueFields, on fera systématiquement
		 //un test d'existence de l'ID_DOC. Ceci a été fait pour Gaumont, car ils importent des documents
		 //qui ont déjà un ID_DOC
		 if (empty($this->t_doc['ID_DOC']) || $this->uniqueFields[0]=='ID_DOC') {
			if ($dupliquer==0) {
	           if (!empty($this->uniqueFields)) { //Le test d'unicité se base sur le param config uniqueFields qui contient les champs constituant la clé unique
					$sql="SELECT ID_DOC FROM t_doc WHERE 1=1 ";
					foreach ($this->uniqueFields as $fld) $sql.=" AND ".strtoupper($fld)."=".$db->Quote($this->t_doc[strtoupper($fld)]);
					if (!in_array('ID_DOC',$this->uniqueFields)) $sql.=" AND ID_DOC!=".$db->Quote($this->t_doc['ID_DOC']);
		           // debug($sql,'pink');
		            $existDeja=$db->GetOne($sql);

		            if (!$existDeja){
		                $this->initDoc();
		            }
		            else {
		            	$this->t_doc['ID_DOC']=$existDeja;
		                if ($failIfExists) {
		                	$this->dropError(kErrorCoteExistant);
			                $db->CompleteTrans();
			                return FALSE;
		                }
	            	}
	           } else $this->initDoc(); //pas de test d'unicité => on crée systématiquement un nv ID_DOC
			}
		}
		*/
		// si $updateUsagerMod est vrai, alors on met à jour l'utilisateur 
		// => important d'avoir la possibilité de bloquer ce comportement pour les updates de nombre de vue / de champs cachés à l'utilisateur.  
		if($updateUsagerMod){
			$this->t_doc['DOC_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
			$this->t_doc['DOC_ID_USAGER_MODIF']=$_SESSION['USER']['ID_USAGER'];
		}
		// VP 5/10/09 : conversion timezone
		// VP 15/10/09 : ajout DOC_DATE_ETAT et DOC_DATE
		/*
		if(isset($this->t_doc['DOC_DATE_DIFF'])) $this->t_doc['DOC_DATE_DIFF']=convertTZ($this->t_doc['DOC_DATE_DIFF']);
		if(isset($this->t_doc['DOC_DATE_DIFF_FIN'])) $this->t_doc['DOC_DATE_DIFF_FIN']=convertTZ($this->t_doc['DOC_DATE_DIFF_FIN']);
		if(isset($this->t_doc['DOC_DATE_PV_DEBUT'])) $this->t_doc['DOC_DATE_PV_DEBUT']=convertTZ($this->t_doc['DOC_DATE_PV_DEBUT']);
		if(isset($this->t_doc['DOC_DATE_PV_FIN'])) $this->t_doc['DOC_DATE_PV_FIN']=convertTZ($this->t_doc['DOC_DATE_PV_FIN']);
		if(isset($this->t_doc['DOC_DATE_ETAT'])) $this->t_doc['DOC_DATE_ETAT']=convertTZ($this->t_doc['DOC_DATE_ETAT']);
		if(isset($this->t_doc['DOC_DATE'])) $this->t_doc['DOC_DATE']=convertTZ($this->t_doc['DOC_DATE']);
		 */

		//Convertion des dates
		foreach ($this->t_doc as $fld=>&$val){
			if (strpos($fld,'DOC_')===0 && in_array($fld,$this->arrDateFields))  {
				$val = convDateTime($val);
			}
		}
		unset($val);
		$id_doc = $this->t_doc['ID_DOC'];

		//Afin de spécifier des traitements liés au client
		if(function_exists("saveDocCustom")){
			saveDocCustom($this, $new);
		}

		$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']));
		$record = array();
		foreach ($this->t_doc as $fld=>$val) {
			if (strpos($fld,'DOC_')===0 && in_array($fld,$this->arrFieldsToSaveWithIdLang)){
				$record[$fld] = $val;
			}
		}
		$sql = $db->GetUpdateSQL($rs, $record);
		if (!empty($sql)) $ok = $db->Execute($sql);
		$this->t_doc['ID_DOC'] = $id_doc;

		$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC']));
		$record = array();
		foreach ($this->t_doc as $fld=>$val) {
			if (strpos($fld,'DOC_')===0 && in_array($fld,$this->arrFieldsToSaveWithoutIdLang))
				if (strpos($fld,'DOC_ID_')===0 && empty($val))
					$record[$fld] = 0;
				else
					$record[$fld] = $val;
		}
		$sql = $db->GetUpdateSQL($rs, $record);
		if (!empty($sql)) $ok = $db->Execute($sql);


		$this->t_doc['ID_DOC'] = $id_doc;

        // MAJ de DOC_SEQ_TC du document parent (s'il y en a un) (i.e. : liste des titres des s?quences associ? ? un doc)
        if (!empty($this->t_doc['DOC_ID_GEN'])){$this->updateParentTC();}

        // I.5. G?n?ration des versions traduites. On ne g?n?re pas de version traduite pour les documents dupliqu?s
        if($this->doc_trad == 1 && $dupliquer==0){ $this->saveVersions('', $this->t_doc['VERSION']);}

       if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDoc);trace($sql); return false;}

        // By LD : ajout du lien entre doc. Si on est en train de créer un document fils ou parent
        // depuis la page de liens, on crée directement le lien.
        // doc2link contient l'id du doc à lier au doc en cours de création.
        // relation détermine le sens du lien : doc parent ou doc fils.
        if ($dupliquer== 0) {
			$this->linkToAnotherDoc();
	      } // if dupliquer =0

		// if (defined('useSolr') && useSolr==true)
            // $this->saveSolr();

		return true;
    }


	/** Sauve les informations des DOC_LEX
	 *  IN : tableau DOC_LEX
	 *  OUT : base mise à jour
	 */
	// VP 30/06/09 : modification méthode saveDoclex
	// VP 16/11/09 : ajout champ LEX_TERME et PERS_NOM pour saisie en liste
	// VG 19/04/10 : ajout compatibilité clé/champ 'ID_TYPE_LEX' ET 'PERS_ID_TYPE_LEX' OU 'LEX_ID_TYPE_LEX'
	function saveDocLex($purge=true) {
		global $db;
		require_once(modelDir.'model_personne.php');
		require_once(modelDir.'model_lexique.php');

		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		if (empty($this->t_doc_lex)) return false;
		$db->StartTrans();

		$tab=$db->GetAll("select ID_DOC_LEX from t_doc_lex where ID_DOC=".intval($this->t_doc['ID_DOC']));
		foreach($tab as $row) {$arrIDs[$row['ID_DOC_LEX']]=$row['ID_DOC_LEX'];}
		//trace(print_r($this->t_doc_lex,true));

		foreach($this->t_doc_lex as $idx=>$docLex) {

			if (isset($docLex['LEXIQUE'])) unset($docLex['LEXIQUE']); //On ôte l'objet encapsulé s'il existe

			// On notera que ID_DOC est celui en cours et pas celui contenu dans le tableau.
			// Effectivement, en cas de duplication, on récupère le tableau avec l'ID_DOC de l'original
			//$docLex['ID_DOC']=$this->t_doc['ID_DOC'];

			// Recherche ID_LEX si LEX_TERME
			// VP 30/05/2018 : pas de recherche de lexique si ID_LEX existe
			if (!empty($docLex['LEX_TERME']) && (!isset($docLex['ID_LEX']) || empty($docLex['ID_LEX']))){
				$myLex=new Lexique();
				//@update VG 02/06/2010
				/*$myLex->t_lexique['LEX_TERME']=trim($docLex['LEX_TERME']);
				$myLex->t_lexique['LEX_ID_TYPE_LEX']=(empty($docLex['ID_TYPE_LEX'])?$docLex['LEX_ID_TYPE_LEX']:$docLex['ID_TYPE_LEX']);
				$myLex->t_lexique['ID_LANG']=$this->t_doc['ID_LANG'];
				$exist=$myLex->checkExist(false);
				if (!$exist) {
					$myLex->create(); //création langue en cours
					foreach ($_SESSION['arrLangues'] as $lg) {
						if (strtoupper($lg)!=strtoupper($this->t_doc['ID_LANG'])) $arrVersions[$lg]=$myLex->t_lexique;
					}
					$myLex->saveVersions($arrVersions); //on sauve la valeur dans toutes les langues
					unset($arrVersions);
				}*/

				$docLex['LEX_TERME']=trim($docLex['LEX_TERME']);
				if(!empty($docLex['ID_TYPE_LEX']))
					$docLex['LEX_ID_TYPE_LEX'] = $docLex['ID_TYPE_LEX'];
				$docLex['ID_LANG']=$this->t_doc['ID_LANG'];

				$myLex->createFromArray($docLex, true, true);

				$docLex['ID_LEX']=$myLex->t_lexique['ID_LEX'];
				unset($myLex);
			}
			// Recherche ID_PERS si PERS_NOM
			// MS 18/02/14 - On ne fait la recherche de l'ID_PERS à partir du PERS_NOM que si on n'a pas d'ID_PERS.
			// Pouvait sinon créer des parasites puisque la recherche d'un ID_PERS a partir du PERS_NOM sans avoir de PERS_PRENOM créait une nouvelle instance personne avec le meme nom mais sans prenom ...
			if (!empty($docLex['PERS_NOM']) && (!isset($docLex['ID_PERS']) || empty($docLex['ID_PERS']))){
				$myPers=new Personne();
				$myPers->t_personne['PERS_NOM']=trim($docLex['PERS_NOM']);
				$myPers->t_personne['PERS_PRENOM'] = trim($docLex['PERS_PRENOM']);//@update VG 29/07/2010
				$myPers->t_personne['PERS_ID_TYPE_LEX']=(empty($docLex['ID_TYPE_LEX'])?$docLex['PERS_ID_TYPE_LEX']:$docLex['ID_TYPE_LEX']);
				$pers_code=trim($docLex['PERS_CODE']);
				if (!empty($pers_code)){
				$myPers->t_personne['PERS_CODE']=$pers_code;
				}
				$myPers->t_personne['ID_LANG']=$this->t_doc['ID_LANG'];
				$exist=$myPers->checkExist(false);


				if (!$exist) {
					$myPers->save();
					$myPers->saveVersions();
				} else $myPers->t_personne['ID_PERS']= $exist;
				$docLex['ID_PERS']=$myPers->t_personne['ID_PERS'];
				unset($myPers);
			}
			//debug($docLex,'yellow',true);
			if ((isset($docLex['ID_LEX']) && !empty($docLex['ID_LEX'])) || (isset($docLex['ID_PERS']) && !empty($docLex['ID_PERS']))) {
				// Vérification existence
				$sqlone='SELECT ID_DOC_LEX FROM t_doc_lex WHERE ID_DOC='.intval($this->t_doc['ID_DOC']).'
				AND ID_TYPE_DESC='.$db->Quote($docLex['ID_TYPE_DESC']).'
				AND DLEX_ID_ROLE='.$db->Quote($docLex['DLEX_ID_ROLE']);
				if(!empty($docLex['ID_PERS'])) $sqlone.=' AND ID_PERS='.intval($docLex['ID_PERS']);
				else if(!empty($docLex['ID_LEX'])) $sqlone.=' AND ID_LEX='.intval($docLex['ID_LEX']);
				$idDocLex=$db->GetOne($sqlone);

				if(empty($idDocLex)){
					// INSERT t_doc_lex
					// $rs = $db->Execute("SELECT * FROM t_doc_lex WHERE ID_DOC_LEX=-1");
					$record = array("ID_DOC" => intval($this->t_doc['ID_DOC']),"ID_LEX" => intval($docLex['ID_LEX']),"ID_PERS" => intval($docLex['ID_PERS']),"ID_TYPE_DESC" => "".$docLex['ID_TYPE_DESC'],"DLEX_ID_ROLE" => "".$docLex['DLEX_ID_ROLE'],"DLEX_REVERSEMENT" => "".$docLex['DLEX_REVERSEMENT']);
					// $sql = $db->GetInsertSQL($rs, $record);
					$ok = $db->insertBase("t_doc_lex","id_doc_lex",$record);
					// if (!empty($sql)) $ok = $db->Execute($sql);

					if (!$ok) trace($db->ErrorMsg());
					//$idDocLex=$db->Insert_ID("t_doc_lex");
					$idDocLex=$db->getOne($sqlone);
					$this->t_doc_lex[$idx]['ID_DOC_LEX'] = $idDocLex;
					// INSERT t_doc_lex_precision
					if (!empty($docLex['LEX_PRECISION'])) {
						$sql="INSERT INTO t_doc_lex_precision (ID_DOC_LEX,ID_LANG,LEX_PRECISION)
							select ".$idDocLex.", ID_LANG,".$db->Quote($docLex['LEX_PRECISION'])." from t_lang";
						$ok=$db->Execute($sql);
						if (!$ok) trace($db->ErrorMsg());
					}
					//suppression ID_DOC_LEX du tableau
					unset($arrIDs[$idDocLex]);
				}else{
					// UPDATE t_doc_lex
					$rs = $db->Execute("SELECT * FROM t_doc_lex WHERE ID_DOC_LEX=".intval($idDocLex));
					$record = array("ID_DOC" => intval($this->t_doc['ID_DOC']),"ID_LEX" => intval($docLex['ID_LEX']),"ID_PERS" => intval($docLex['ID_PERS']),"ID_TYPE_DESC" => "".$docLex['ID_TYPE_DESC'],"DLEX_ID_ROLE" => "".$docLex['DLEX_ID_ROLE'],"DLEX_REVERSEMENT" => "".$docLex['DLEX_REVERSEMENT']);
					$sql = $db->GetUpdateSQL($rs, $record);
					if (!empty($sql)){
						$ok = $db->Execute($sql);
						if (!$ok)
							trace($db->ErrorMsg());
					}
					// REPLACE t_doc_lex_precision
					if (!empty($docLex['LEX_PRECISION'])) {
						/*
						* @author B.RAVI 2016-01-19
						*  Dans docSaisie.php, saveVersions() (quand on traduit une notice) est fait dans l'                                                    *  action SAVE_DOC, il ne faut pas que saveDocLex() appelé APRES le saveVersions()                                                      *  efface les lignes traduite crées dans la table t_doc_lex_precision crées par                                                         *  saveVersions()
						*  il faut donc "AND id_lang=X' dans DELETE FROM t_doc_lex_precision ci dessous
						*/
									
//						$sql="DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX = ".intval($idDocLex).";";
						$sql="DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX = ".intval($idDocLex)." AND id_lang=".$db->Quote($this->t_doc['ID_LANG']);
						$ok=$db->Execute($sql);
						if ($ok) {
							$sql="INSERT INTO t_doc_lex_precision (ID_DOC_LEX,ID_LANG,LEX_PRECISION) VALUES (".$idDocLex.",".$db->Quote($this->t_doc['ID_LANG']).",".$db->Quote($docLex['LEX_PRECISION']).");";
							$ok=$db->Execute($sql);
						}
						if (!$ok) trace($db->ErrorMsg());
					}
					//suppression ID_DOC_LEX du tableau
					unset($arrIDs[$idDocLex]);

				}
			}
		}
		// Purge éventuelle
		if($purge && isset($arrIDs) && count($arrIDs)>0){
			$db->Execute("DELETE from t_doc_lex_precision where ID_DOC_LEX in (".implode(",",$arrIDs).")");
			$db->Execute("DELETE from t_doc_lex where ID_DOC_LEX in (".implode(",",$arrIDs).")");
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocLex);trace($sql); return false;} else return true;
	}

	/** Sauve les informations des DOC_LEX_DROIT
	 *  IN : tableau DOC_LEX_DROIT
	 *  OUT : base mise à jour
	 */
	function saveDocLexDroit($purge=true) {
		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDeleteDocNoId);return false;}
		global $db;
		$db->StartTrans();

		//Nettoyage orphelin (requête trop longue !!!)
		//$db->Execute("DELETE FROM t_doc_lex_droit WHERE id_doc_lex not in (select distinct id_doc_lex from t_doc_lex)");

		$query = "select t_doc_lex_droit.ID_DOC_LEX from t_doc_lex_droit inner join t_doc_lex on t_doc_lex_droit.ID_DOC_LEX = t_doc_lex.ID_DOC_LEX where ID_DOC=".intval($this->t_doc['ID_DOC']);
		$tab=$db->GetAll($query);
		foreach($tab as $row)
		$arrIDs[$row['ID_DOC_LEX']]=$row['ID_DOC_LEX'];

		if (empty($this->t_doc_lex_droit)) return false;

		foreach ($this->t_doc_lex_droit as $idx=>$docLD) {

			// Vérification existence
			$sqlone = "select ID_DOC_LEX from t_doc_lex_droit WHERE ID_DOC_LEX = " . intval($docLD['ID_DOC_LEX']);
			$idDocLex=$db->GetOne($sqlone);

			if(empty($idDocLex)){
				// INSERT t_doc_lex
				// $rs = $db->Execute("SELECT * FROM t_doc_lex_droit WHERE ID_DOC_LEX=-1");
				// $sql = $db->GetInsertSQL($rs, $docLD);
				// if (!empty($sql)) $ok = $db->Execute($sql);
				//XB
				$ok = $db->insertBase("t_doc_lex_droit","id_doc_lex",$docLD);
				if (!$ok) trace($db->ErrorMsg());


				//$idDocLex=$db->Insert_ID("t_doc_lex");
				$idDocLex=$db->getOne($sqlone);
				//suppression ID_DOC_LEX du tableau
				unset($arrIDs[$idDocLex]);
			}else{
				// UPDATE t_doc_lex
				$rs = $db->Execute("SELECT * FROM t_doc_lex_droit WHERE ID_DOC_LEX = " . intval($idDocLex));
				$sql = $db->GetUpdateSQL($rs, $docLD);
				if (!empty($sql)) $ok = $db->Execute($sql);
				if (!$ok) trace($db->ErrorMsg());
				//suppression ID_DOC_LEX du tableau
				unset($arrIDs[$idDocLex]);
			}
		}
		// Purge éventuelle
		if($purge && count($arrIDs)>0){
			$db->Execute("DELETE from t_doc_lex_droit where ID_DOC_LEX in (".implode(",",$arrIDs).")");
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocLexDroit);trace($sql); return false;} else return true;
	}

	/** Sauve les informations des DOC_LEX_DROIT
	 *  IN : tableau DOC_LEX_DROIT
	 *  OUT : base mise à jour
	 */
	function saveDocPrex($purge=true) {
		include_once(modelDir.'model_doc_prex.php');
		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDeleteDocNoId);return false;}
		global $db;
		$db->StartTrans();

		//Nettoyage orphelin
		$db->Execute("DELETE FROM t_doc_prex WHERE id_doc not in (select distinct id_doc from t_doc)");

		$query = "select t_doc_prex.ID_DOC_PREX from t_doc_prex where ID_DOC=".intval($this->t_doc['ID_DOC']);
		$tab = $db->GetAll($query);
		foreach($tab as $row)
			$arrIDs[$row['ID_DOC_PREX']] = $row['ID_DOC_PREX'];

                // B.RAVI 20150127 Eviter Warning: Invalid argument supplied for foreach()
                if (is_object($this->t_doc_prex) || is_array($this->t_doc_prex)){
                    foreach ($this->t_doc_prex as $idx=>&$docPrex) {
                            $prex = new DroitPrex();
                            $docPrex['ID_DOC'] = $this->t_doc['ID_DOC'];
                            if (isset($docPrex['t_dp_pers'])){
                                    $prex->t_dp_pers = $docPrex['t_dp_pers'];
                                    unset($docPrex['t_dp_pers']);
                            }
                            if (isset($docPrex['t_dp_val'])){
                                    $prex->t_dp_val = $docPrex['t_dp_val'];
                                    unset($docPrex['t_dp_val']);
                            }
                            $prex->t_prex = $docPrex;
                            $prex->save();
                            $prex->savePersonne();
                            $prex->saveValeurs('EXPL');
                            $docPrex = $prex->t_prex;
                            $docPrex['PREX'] = $prex;
                            unset($arrIDs[$prex->t_prex['ID_DOC_PREX']]);
                    }
                }
		// Purge éventuelle
		if($purge && count($arrIDs)>0){
			foreach ($arrIDs as $val){
				$prexToDel = new DroitPrex();
				$prexToDel->t_prex['ID_DOC_PREX'] = $val;
				$prexToDel->delete();
			}
		}
		//Calcule des états des droits
		$this->updateDroitAudite();
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocPrex); return false;} else return true;
	}

	/** Sauve les informations des DOC_EXP
	 *  OUT : base mise à jour
	 */
	function saveDocExp($purge=true) {
		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDeleteDocNoId);return false;}
		global $db;
		$db->StartTrans();

		$query = "select ID_EXP from t_exp_doc where ID_DOC=".intval($this->t_doc['ID_DOC']);
		$tab = $db->GetAll($query);
		foreach($tab as $row)
		$arrIDs[$row['ID_EXP']]=$row['ID_EXP'];

		foreach ($this->t_doc_exp as $idx=>$docExp) {

			// Vérification existence
			$sqlone = "select ID_EXP from t_exp_doc WHERE ID_EXP = " . intval($docExp['ID_EXP']);
			$idDocExp = $db->GetOne($sqlone);

			if(empty($idDocExp)){
				// INSERT t_exp_doc
				$docExp['ID_DOC'] = $this->t_doc['ID_DOC'];
				//XB
				// $rs = $db->Execute("SELECT * FROM t_exp_doc WHERE ID_EXP=-1");
				// $sql = $db->GetInsertSQL($rs, $docExp);
				// if (!empty($sql)) $ok = $db->Execute($sql);

				$ok = $db->insertBase("t_exp_doc","id_exp",$docExp);


				if (!$ok) trace($db->ErrorMsg());
				//suppression ID_EXP du tableau
				unset($arrIDs[$docExp['ID_EXP']]);
			}else{
				//suppression ID_EXP du tableau
				unset($arrIDs[$idDocExp]);
			}
		}
		// Purge éventuelle
		if($purge && count($arrIDs)>0){
			$db->Execute("DELETE from t_exp_doc where ID_EXP in (".implode(",",$arrIDs).") AND ID_DOC=".intval($this->t_doc['ID_DOC']));
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocExp);trace($sql); return false;} else return true;
	}

	/** Calcule des états des droits
	 *  OUT : base mise à jour
	 */
	function updateDroitAudite() {
		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorUpdateDocNoId);return false;}
		global $db;
		$db->StartTrans();

		$arrType = array('AARC', 'DARC', 'AMUS', 'DMUS');

		$tmp = implode("§", $arrType);
		//Suppression des états
//		$query = "delete from t_doc_val where ID_TYPE_VAL in ('" . str_replace("§","','",$db->Quote($tmp)) . "')
//		and ID_DOC = " . intval($this->t_doc['ID_DOC']);

                //B.RAVI 2015-08-31 Correction erreur syntaxe SQL car on obtenait un double '' sur la 1ère et dernière valeur => in(''AARC', 'DARC', 'AMUS', 'DMUS'')
                $query = "delete from t_doc_val where ID_TYPE_VAL in (" . str_replace("§","','",$db->Quote($tmp)) . ")
		and ID_DOC = " . intval($this->t_doc['ID_DOC']);
		$ok = $db->Execute($query);
		if (!$ok) trace($db->ErrorMsg());

		//Construction tableau contenant les ID_VAL
                //B.RAVI 2015-08-31 Correction erreur syntaxe SQL car on obtenait un double '' sur la 1ère et dernière valeur => in(''AARC', 'DARC', 'AMUS', 'DMUS'')

//		$query = "select ID_VAL, VAL_ID_TYPE_VAL as ID_TYPE_VAL
//		from t_val
//		where VAL_ID_TYPE_VAL in ('" . str_replace("§","','",$db->Quote($tmp)) . "')
//		and ID_LANG = 'FR' and VALEUR = 'Oui'";
                $query = "select ID_VAL, VAL_ID_TYPE_VAL as ID_TYPE_VAL
		from t_val
		where VAL_ID_TYPE_VAL in (" . str_replace("§","','",$db->Quote($tmp)) . ")
		and ID_LANG = 'FR' and VALEUR = 'Oui'";
		$tmp = $db->GetAll($query);
		$tabVal = array();
		foreach($tmp as $row){
			$tabVal[$row['ID_TYPE_VAL']] = $row['ID_VAL'];
		}

		$query = "select DP_TYPE, DP_AUDITE, DP_DATE_DEBUT, DP_DATE_FIN, cast(now() as DATE) as NOW
		from t_doc_prex where ID_DOC=".intval($this->t_doc['ID_DOC']);
		$tabPrex = $db->GetAll($query);

		$audArc = true;
		$audMus = true;
		$drArc = true;
		$drMus = true;
		foreach($tabPrex as $row){
			if ($row['DP_TYPE'] == "MUS"){
				//Audité Musique
				if ($row['DP_AUDITE'] == "0")
					$audMus = false;
				//Droit Musique
				$datedebut = date_create($row['DP_DATE_DEBUT']);
				$datefin = date_create($row['DP_DATE_FIN']);
				$datenow = date_create($row['NOW']);
				$interdeb = date_diff($datedebut, $datenow);
				$interfin = date_diff($datenow, $datefin);
				if ($interfin->invert == 1 || $interdeb->invert == 1)
					$drMus = false;
			}
			else {
				//Audité Archive
				if ($row['DP_AUDITE'] == "0")
					$audArc = false;
				//Droit Archive
				$datedebut = date_create($row['DP_DATE_DEBUT']);
				$datefin = date_create($row['DP_DATE_FIN']);
				$datenow = date_create($row['NOW']);
				$interdeb = date_diff($datedebut, $datenow);
				$interfin = date_diff($datenow, $datefin);
				if ($interfin->invert == 1 || $interdeb->invert == 1)
					$drArc = false;
			}
		}
		$id_doc = intval($this->t_doc['ID_DOC']);
		// $rs = $db->Execute("SELECT * FROM t_doc_val WHERE ID_DOC=-1 AND ID_VAL=-1");
		if ($audArc && isset($tabVal['AARC'])){
			$id_val = intval($tabVal['AARC']);
			$record = array("ID_DOC" => $id_doc,"ID_VAL" => $id_val, "ID_TYPE_VAL" => 'AARC');
			// $sql = $db->GetInsertSQL($rs, $record);
			// $ok = $db->Execute($sql);
			$ok = $db->insertBase("t_doc_val","id_val",$record);
			if (!$ok) trace($db->ErrorMsg());
		}
		if ($audMus && isset($tabVal['AMUS'])){
			$id_val = intval($tabVal['AMUS']);
			$record = array("ID_DOC" => $id_doc,"ID_VAL" => $id_val, "ID_TYPE_VAL" => 'AMUS');
			// $sql = $db->GetInsertSQL($rs, $record);
			// $ok = $db->Execute($sql);
			$ok = $db->insertBase("t_doc_val","id_val",$record);
			if (!$ok) trace($db->ErrorMsg());
		}
		if ($drArc && isset($tabVal['DARC'])){
			$id_val = intval($tabVal['DARC']);
			$record = array("ID_DOC" => $id_doc,"ID_VAL" => $id_val, "ID_TYPE_VAL" => 'DARC');
			// $sql = $db->GetInsertSQL($rs, $record);
			// $ok = $db->Execute($sql);
			$ok = $db->insertBase("t_doc_val","id_val",$record);
			if (!$ok) trace($db->ErrorMsg());
		}
		if ($drMus && isset($tabVal['DMUS'])){
			$id_val = intval($tabVal['DMUS']);
			$record = array("ID_DOC" => $id_doc,"ID_VAL" => $id_val, "ID_TYPE_VAL" => 'DMUS');
			// $sql = $db->GetInsertSQL($rs, $record);
			// $ok = $db->Execute($sql);
			$ok = $db->insertBase("t_doc_val","id_val",$record);
			if (!$ok) trace($db->ErrorMsg());
		}


		$this->getValeurs();
		if (!$db->CompleteTrans()) {$this->dropError(kErrorUpdateDocPrex);return false;} else return true;
	}


	/**
	 * Sauve les informations d'un lien DOC / VALEUR
	 * IN : tableau t_doc_val, $purge=>vidage t_doc_val avant full insert
	 * OUT : base mise à jour
	 * NOTE : si on a des champs VALEUR et TYPE_VAL mais pas d'ID_VAL (par exemple pour les imports)
	 * 	on va tenter de le récupérer
	 */
	// VP 19/10/09 : ajout purge par ID_TYPE_VAL (tableau ou chaine)
	function saveDocVal($purge=true,$id_type_val=null) {
		global $db;
		require_once(modelDir."model_val.php");

		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		 if (empty($this->t_doc_val)) return false;
		$db->StartTrans();
		// Purge générale ? Utile si on crée une fiche en mode édition. Pas en mode import
		if ($purge) {
			$sqltypeval="";
			if(!empty($id_type_val)){
				if(is_array($id_type_val)) $sqltypeval=" and ID_TYPE_VAL in ('".str_replace("/","','",implode("/",$id_type_val))."')";
				else $sqltypeval=" and ID_TYPE_VAL=".$db->Quote($id_type_val);
			}
			$db->Execute('DELETE FROM t_doc_val WHERE ID_DOC='.intval($this->t_doc['ID_DOC']).$sqltypeval);
		}
        $existing_keys = array();
		foreach ($this->t_doc_val as $idx=>$docVal) {
			// Recherche ID_VAL si VALEUR
			// VP 30/05/2018 : pas de recherche de valeur si ID_VAL existe
			if (!empty($docVal['VALEUR']) && (!isset($docVal['ID_VAL']) || empty($docVal['ID_VAL']))){
				$myVal=new Valeur();

				//@update VG 02/06/2010
/*				$myVal->t_val['VALEUR']=trim($docVal['VALEUR']);
				$myVal->t_val['VAL_ID_TYPE_VAL']=$docVal['ID_TYPE_VAL'];
				$myVal->t_val['ID_LANG']=$this->t_doc['ID_LANG'];

				$exist=$myVal->checkExist(false);
				if ($exist) {
					$myVal->t_val['ID_VAL']=$exist;
					$myVal->save();
				} else {
					$myVal->create(); //création dans la langue
					foreach ($_SESSION['arrLangues'] as $lg) {
						if (strtoupper($lg)!=strtoupper($this->t_doc['ID_LANG'])) $arrVersions[$lg]=$val;
					}
					$myVal->saveVersions($arrVersions); //sauve dans autres langues
					unset($arrVersions);
				}
				$docVal['ID_VAL']=$myVal->t_val['ID_VAL'];
				unset($myVal);*/

				$docVal['VAL_ID_TYPE_VAL']=$docVal['ID_TYPE_VAL'];
				$docVal['ID_LANG']=$this->t_doc['ID_LANG'];

				//@update VG 26/07/2010 : on ne fait pas de mise à jour d'une valeur lors de la sauvegarde d'un document ! => 2ème paramètre à false
				$myVal->createFromArray($docVal, true, false, true);
				$docVal['ID_VAL']=$myVal->t_val['ID_VAL'];
				unset($myVal);
			}
			/*
			if ($docVal['ID_VAL']=='' && $docVal['VALEUR']!='')
				$docVal['ID_VAL']=$db->GetOne('SELECT ID_VAL from t_val WHERE VALEUR LIKE '.$db->Quote($docVal['VALEUR']).
											  ' AND VAL_ID_TYPE_VAL='.$db->Quote($docVal['ID_TYPE_VAL']));
			 */
            // VP 22/07/2016 : contrôle doublon par clé composée de ID_TYPE_VAL et ID_VAL
			if ($docVal['ID_VAL'] && !empty($docVal['ID_VAL']) && !in_array($docVal['ID_TYPE_VAL'].$docVal['ID_VAL'], $existing_keys)) { //vérif existence id_val=> peut-être vide si l'on choisit une valeur parmi une dropliste qui
										 // qui contient une valeur vide.
                $existing_keys[]=$docVal['ID_TYPE_VAL'].$docVal['ID_VAL'];
				//Précaution en cas de duplication : on spécifie bien que l'ID_DOC est celui en cours.
				// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_DOC originel dedans.
				$docVal['ID_DOC']=$this->t_doc['ID_DOC'];

				//if ($docVal['VALEUR']) unset($docVal['VALEUR']); //On ôte l'objet encapsulé s'il existe

				//Pas de purge générale, donc on micro-purge la valeur. Ajouter TYPE_VAL ?
				if (!$purge) $db->Execute('DELETE FROM t_doc_val WHERE ID_DOC='.intval($docVal['ID_DOC'])
													." AND ID_VAL=".intval($docVal['ID_VAL'])
													." AND ID_TYPE_VAL=".$db->Quote($docVal['ID_TYPE_VAL']));

				// $rs = $db->Execute("SELECT * FROM t_doc_val WHERE ID_DOC=-1");
				// $sql = $db->GetInsertSQL($rs, $docVal);
				// if (!empty($sql)) $ok = $db->Execute($sql);
				   $ok = $db->insertBase("t_doc_val","id_doc",$docVal);

				debug($sql, "gold", true);
				if (!$ok) trace($db->ErrorMsg());
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocVal);trace($sql); return false;} else return true;
	}

	/*
	methode de sauvegarde des categories
	*/

	function saveDocCat($purge=true)
	{
		global $db;
		
		// Ici on récupère les catégories courantes de l'objet, ce qui va permettre de déterminer les modifications qu'on est en train de réaliser
		if(true ){ // pour l'instant toujours le cas, mais gagnerait probablement a etre limité par une constante activant / desactivant le comportement et / ou une liste de catégorie ayant ce comportement
			$oriDoc = new Doc();
			$oriDoc->t_doc['ID_DOC'] = $this->t_doc['ID_DOC'];
			$oriDoc->t_doc['ID_LANG'] = $this->t_doc['ID_LANG'];
			$oriDoc->getDoc() ;
			$oriDoc->getCategories() ;
			// on marque les catégories qui n'ont pas changées (existent dans oriDoc->t_doc_cat et this->t_doc_cat)
			foreach($oriDoc->t_doc_cat as $i=>$ori_doc_cat){
				foreach($this->t_doc_cat as $j=>$doc_cat){
					if($ori_doc_cat['ID_CAT'] == $doc_cat['ID_CAT']){
						$this->t_doc_cat[$j]['state'] = 'unchanged';
						$oriDoc->t_doc_cat[$i]['state'] = 'unchanged';
						// trace("unchanged ? ".$this->t_doc['ID_DOC']." ".$doc_cat['ID_CAT']." ".$this->t_doc_cat[$j]['CDOC_ORDRE']." ".$oriDoc->t_doc_cat[$i]['CDOC_ORDRE']);
						if(!isset($this->t_doc_cat[$j]['CDOC_ORDRE']) && isset($oriDoc->t_doc_cat[$i]['CDOC_ORDRE'])){
							$this->t_doc_cat[$j]['CDOC_ORDRE'] = $oriDoc->t_doc_cat[$i]['CDOC_ORDRE'] ; 
							// trace("copy cdoc_ordre ".$this->t_doc['ID_DOC']." ".$doc_cat['ID_CAT']." | ".$this->t_doc_cat[$j]['CDOC_ORDRE']);k
						}
					}
				}
			}
			// on marque les cats pour lesquelles on est en train d'insérer quelque chose 
			foreach($this->t_doc_cat as $j => $doc_cat){
				if(isset($doc_cat['state'])){
					continue ; 
				}else{
					$this->t_doc_cat[$j]['state']  = 'to_insert';
				}
			}
			
		}
		// purge de l'existant
		if ($purge)
		{
			$db->Execute('DELETE FROM t_doc_cat WHERE ID_DOC='.intval($this->t_doc['ID_DOC']).'');
		}
		
		if (!empty($this->t_doc_cat) && $this->t_doc_cat!=null)
		{
			foreach ($this->t_doc_cat as $idx=>$doc_cat)
			{
				if (!empty($doc_cat) && !empty($doc_cat['ID_CAT']))
				{
					require_once(modelDir.'model_categorie.php');
					$cat = new Categorie($doc_cat['ID_CAT']) ;
					if(!isset($doc_cat['ID_TYPE_CAT']) || empty($doc_cat['ID_TYPE_CAT'])){
						$doc_cat['ID_TYPE_CAT'] = $cat->t_categorie['CAT_ID_TYPE_CAT'];
					}
					
					// pour les catégories auxquelles on est en train d'insérer des valeurs, on update les CDOC_ORDRE pour que le dernier élément se retrouvent au début de la catégorie
					if(!isset($doc_cat['CDOC_ORDRE']) && isset($doc_cat['state']) && $doc_cat['state'] == 'to_insert' && isset($doc_cat['ID_CAT']) && !empty($doc_cat['ID_CAT'])){
						$doc_cat['CDOC_ORDRE'] = 0;
						// trace("new insert ".$doc_cat['ID_CAT']." ".$doc['ID_DOC']);
						$db->Execute('UPDATE t_doc_cat SET CDOC_ORDRE = CDOC_ORDRE + 1 WHERE ID_CAT ='.$doc_cat['ID_CAT']);
						unset($this->t_doc_cat[$idx]['state']);
					}
					
					// trace("Doc:saveDocCat : ".print_r($doc_cat,true));
					//insertion dans la table t_doc_cat
					$doc_cat['ID_DOC'] = intval($this->t_doc['ID_DOC']);
					$exist = $db->GetOne("select id_cat from t_doc_cat where id_doc=".intval($doc_cat['ID_DOC'])." and id_cat=".intval($doc_cat['ID_CAT'])." and id_type_cat=".$db->Quote($doc_cat['ID_TYPE_CAT']));
					if(!$exist){
						$ok = $db->insertBase("t_doc_cat","id_doc",$doc_cat);
					}

					// Si la catégorie qu'on est en train de lier au document est défini dans la constante kSaveVignetteAsHDForCatIds, alors on lance la génération de la vignette HD 
					if(defined("kSaveVignetteAsHDForCatIds")){
						$arr_catHD_ids = explode(',',(string)(kSaveVignetteAsHDForCatIds));
						if(in_array($doc_cat['ID_CAT'],$arr_catHD_ids )){
							require_once(modelDir.'model_imageur.php');
							Imageur::refreshImagesCatHD($doc_cat['ID_CAT'],intval($this->t_doc['ID_DOC']));
						}
					}
					
					// a priori on ne devrait lancer la sauvegarde de la catégorie que si on détecte un changement dans l'image par setVignette
					$changed = $cat->setVignette(); 
					if($changed ){
						$cat->save() ;
						$cat->saveVersions() ;
					}
					
					// mise à jour cat_date_mod
					$rs = $db->Execute("SELECT * FROM t_categorie WHERE ID_CAT=".intval($doc_cat['ID_CAT']));
					$sql = $db->GetUpdateSQL($rs, array("CAT_DATE_MOD" => str_replace("'","",$db->DBTimeStamp(time()))));
					if (!empty($sql)) $ok = $db->Execute($sql);
				}
			}
			// ici on s'occupe des catégories auxquelles le doc n'appartient plus apres la sauvegarde, on veut pouvoir mettre à jour leur vignettes
			foreach($oriDoc->t_doc_cat as $ori_doc_cat){
				if(!isset($ori_doc_cat['state'])){
					$catDel = new Categorie($ori_doc_cat['ID_CAT']) ;
					// a priori on ne devrait lancer la sauvegarde de la catégorie que si on détecte un changement dans l'image par setVignette
					$changed = $catDel->setVignette(); 
					if($changed ){
						$catDel->save() ;
						$catDel->saveVersions() ;
					}
				}
			}
		}
	}

	/**
	 * Sauve les informations d'un lien DOC / FESTIVAL
	 * IN : tableau t_doc_fest, $purge=>vidage t_doc_fest avant full insert
	 * OUT : base mise à jour
	 */
	function saveDocFest($purge=true) {
		require_once(modelDir."model_val.php");
		global $db;

		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		$db->StartTrans();
		// Purge générale ? Utile si on crée une fiche en mode édition. Pas en mode import
		if ($purge) $db->Execute('DELETE FROM t_doc_fest WHERE ID_DOC='.intval($this->t_doc['ID_DOC']));
		if (empty($this->t_doc_fest)) {
			$db->CompleteTrans();
			return false;
		}
		foreach ($this->t_doc_fest as $idx=>&$docFest) {

			if ($docFest['ID_FEST']=='' && $docFest['FEST_LIBELLE']!='' && $docFest['FEST_ANNEE']) //Récup d'ID, par exemple, en cas d'import, où pas d'ID
				$docFest['ID_FEST']=$db->GetOne('SELECT ID_FEST from t_festival WHERE FEST_LIBELLE LIKE '.$db->Quote($docFest['FEST_LIBELLE']).
											  ' AND FEST_ANNEE='.$db->Quote($docFest['FEST_ANNEE']));

			if ($docFest['ID_FEST'] && $docFest['ID_FEST']!='') { //vérif existence id_fest=> peut-être vide si l'on choisit une valeur parmi une dropliste qui
										 // qui contient une valeur vide.
				//Précaution en cas de duplication : on spécifie bien que l'ID_DOC est celui en cours.
				// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_DOC originel dedans.
				$docFest['ID_DOC']=$this->t_doc['ID_DOC'];


				//Pas de purge générale, donc on micro-purge la valeur.
				if (!$purge) $db->Execute('DELETE FROM t_doc_fest WHERE ID_DOC='.intval($docFest['ID_DOC'])
													." AND ID_FEST=".intval($docFest['ID_FEST']));
				if (empty($docFest['ID_DOC_FEST'])) unset($docFest['ID_DOC_FEST']);
			     $ok = $db->insertBase("t_doc_fest","id_doc_fest",$docFest);

				if (!$ok) trace($db->ErrorMsg());
				$docFest['ID_DOC_FEST']=$ok;

				//2ème partie, sauvegarde de DOC_FEST_VAL
				if ($docFest['DOC_FEST_VAL']) {
					$db->Execute("DELETE FROM t_doc_fest_val where ID_DOC_FEST=".intval($docFest['ID_DOC_FEST']));
					foreach ($docFest['DOC_FEST_VAL'] as $val) {
						if (!$val['ID_TYPE_VAL']) continue; //pas de type ? on arrête
						// pas d'ID_VAL, on le récupère avec la valeur et le type

						if (!empty($val['VALEUR'])) {
							$myVal=new Valeur();
							$myVal->t_val['VALEUR']=$val['VALEUR'];
							$myVal->t_val['VAL_ID_TYPE_VAL']=$val['ID_TYPE_VAL'];
							$myVal->t_val['ID_LANG']=$_SESSION['langue'];
							$_val=$myVal->checkExist();
							if ($_val)
								$val['ID_VAL']=$_val;
							else {
								$myVal->create();
								foreach ($_SESSION['arrLangues'] as $lg) {
									if ($lg!=$_SESSION['langue']) $arrVersions[$lg]=$val;
								}
								$myVal->saveVersions($arrVersions);
								unset($arrVersions);
								$val['ID_VAL']=$myVal->t_val['ID_VAL'];
							}
						}

						if ($val['ID_VAL']) {
							$db->Execute("INSERT INTO t_doc_fest_val (ID_DOC_FEST,ID_VAL,ID_TYPE_VAL) values (".intval($docFest['ID_DOC_FEST']).",".intval($val['ID_VAL']).",".$db->Quote($val['ID_TYPE_VAL']).")");
						}
					}
				}
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocFest);trace($sql); return false;} else return true;
	}

	function saveDocRedif() {
		 if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDeleteDocNoId);return false;}
		 if (empty($this->t_doc_redif)) return false;
		global $db;
		$db->StartTrans();
		$db->Execute('DELETE FROM t_doc_redif WHERE ID_DOC='.intval($this->t_doc['ID_DOC']));

		foreach ($this->t_doc_redif as $idx=>$docRed) {

			//Précaution en cas de duplication : on spécifie bien que l'ID_DOC est celui en cours.
			// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_VAL originel dedans.
			if (isset($docRed['REDIF_DATE']) && !empty($docRed['REDIF_DATE']))
			{
				$docRed['ID_DOC']=$this->t_doc['ID_DOC'];
				foreach ($docRed as $fld=>$val) if (empty($val)) unset($docRed[$fld]);
				// $rs = $db->Execute("SELECT * FROM t_doc_redif WHERE ID_REDIF=-1");
				// $sql = $db->GetInsertSQL($rs, $docRed);
				// if (!empty($sql)) $ok = $db->Execute($sql);
				$ok = $db->insertBase("t_doc_redif","id_redif",$docRed);


				if (!$ok) trace($db->ErrorMsg());
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocRedif);trace($sql); return false;} else return true;
	}
	// MS traite d'un coup l'ajout d'un nouveau commentaire, la modification des anciens 
	// /!\ NE TRAITE PAS LA SUPPRESSION, POUR LINSTANT RESERVEE A DES TRAITEMENTS AJAX
	function updateDocComments($data_post,$autosort = true ){
		require_once(libDir.'class_user.php');
		$myUsr = User::getInstance();
		if(isset($data_post['t_doc_comment']) && is_array($data_post['t_doc_comment'])){
			$post_comments = $data_post['t_doc_comment'] ; 
		}elseif (is_array($data_post)){
			$post_comments = $data_post ; 
		}else{
			return false ; 
		}
		if(empty($this->t_doc_comment)){
			$this->getDocComments(true);
		}
		
		global $db ; 
		foreach($post_comments as $comment){
			if(empty($comment['ID_COMMENT']) || empty($comment['DC_ID_USAGER_CREA'])){
				$comment['DC_AUTEUR'] = $myUsr->Prenom . " " . $myUsr->Nom;
				$comment['DC_ID_USAGER_CREA'] = $myUsr->UserID;
				$comment['DC_ID_USAGER_MOD'] = $myUsr->UserID;
				$comment['DC_DATE_CREA'] = trim($db->DBTimeStamp(time()),"'");
				$comment['DC_DATE_MOD'] = trim($db->DBTimeStamp(time()),"'");
				$comment['ID_DOC'] = $this->t_doc['ID_DOC'];
				// trace("new comments ".print_r($comment ,true));
				$this->t_doc_comment[] = $comment ; 
			}else if (isset($comment['ID_COMMENT']) && !empty($comment['ID_COMMENT'])){
				foreach($this->t_doc_comment as $idx=>$dcomment){
					if($comment['ID_COMMENT'] == $dcomment['ID_COMMENT'] 
					&& preg_replace('/\R/u', "\r\n",trim($comment['DC_COMMENT'])) != preg_replace('/\R/u', "\r\n",trim($dcomment['DC_COMMENT']))){
						// MS les preg_replace dans la comparaison permettent d'uniformiser les sauts de lignes, l'un des problèmes dans la comparaison des commentaires multilignes que j'ai pu tester. 
						$comment['ID_DOC'] = $this->t_doc['ID_DOC'];
						$comment['DC_ID_USAGER_MOD'] = $myUsr->UserID;
						$comment['DC_DATE_MOD'] = trim($db->DBTimeStamp(time()),"'");
						$this->t_doc_comment[$idx] = $comment ; 
						// trace("update old comments ".print_r($comment ,true));
						break ;
					}
				}
			}
		}
		// MS - si autosort on trie dans l'ordre par défaut des commentaires : DC_DATE_CREA desc
		if($autosort){
			function date_crea_sort_desc($a,$b){
				if ($a['DC_DATE_CREA'] == $b['DC_DATE_CREA']) {
					return 0;
				}
				return ($a['DC_DATE_CREA'] > $b['DC_DATE_CREA']) ? -1 : 1;
			}
			uasort($this->t_doc_comment,'date_crea_sort_desc');
		}
	}

	function saveDocComments($erasePreviousComment=true,$doc_note='', &$new_id_comment=NULL, $updateDocNote=false) {
		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDeleteDocNoId);return false;}
		//if (empty($this->t_doc_comment)) return false;
		global $db;
		$db->StartTrans();
		if ($erasePreviousComment==true){
			$db->Execute('DELETE FROM t_doc_comment WHERE ID_DOC='.intval($this->t_doc['ID_DOC']));
			// NB 24 03 2015 ajout pour la prise en compte de t_doc_comment_val
			foreach ($this->t_doc_comment as $key => $value) {
					$db->Execute('DELETE FROM t_doc_comment_val WHERE ID_COMMENT='.intval($value['ID_COMMENT']));
			}
		}
		$new_id_comment= 0;
		foreach ($this->t_doc_comment as $idx=>$docCom) {

			if (empty($docCom['ID_COMMENT']))
				unset($docCom['ID_COMMENT']);
			
			if(!isset($docCom['ID_COMMENT']) && (!isset($docCom['DC_DATE_CREA'])||empty($docCom['DC_DATE_CREA']))){
				$docCom['DC_DATE_CREA'] = trim($db->DBTimeStamp(time()),"'");
			}
			
			if((!isset($docCom['DC_DATE_MOD'])||empty($docCom['DC_DATE_MOD']))){
				$docCom['DC_DATE_MOD'] = trim($db->DBTimeStamp(time()),"'");
			}
			
			if (empty($docCom['ID_DOC']))
				$docCom['ID_DOC'] = $this->t_doc['ID_DOC'];
			
			// $rs = $db->Execute("SELECT * FROM t_doc_comment WHERE ID_COMMENT=-1");
			// $sql = $db->GetInsertSQL($rs, $docCom);
			// if (!empty($sql)) $ok = $db->Execute($sql);
			$ok = $db->insertBase("t_doc_comment","id_comment",$docCom);

			if (!$ok) trace($db->ErrorMsg());
			//recuperation de l'id_comment pour insert des notes par critères dans t_doc_comment_val ou update des valeurs déjà enregistré
			if ($ok && !isset($docCom['ID_COMMENT'])) {
				$this->t_doc_comment[$idx]['ID_COMMENT'] = $ok;
				$new_id_comment=$ok;
			} elseif (isset($docCom['ID_COMMENT'])) {
				$this->t_doc_comment[$idx]['ID_COMMENT'] = $docCom['ID_COMMENT'];
				$new_id_comment=$docCom['ID_COMMENT'];
			}

			if (!empty($this->t_doc_comment[$idx]['DOC_COMMENTS_VAL'])) {
				foreach ($this->t_doc_comment[$idx]['DOC_COMMENTS_VAL'] as $idx=>$docComVal) {
					$docComVal['ID_COMMENT']=$new_id_comment;
					$ok = $db->insertBase("t_doc_comment_val","id_comment",$docComVal);

					if (!$ok) trace($db->ErrorMsg());
				}
			}
		}
		//Pour système de vote-> stockage de la note finale dans doc_note
		if($updateDocNote){
			$this->calcAverageNote();
			$doc_note = $this->t_doc['DOC_NOTE'];
		}
		if ($doc_note !=''){
			$sql="UPDATE t_doc SET doc_note = ".floatval($doc_note)." WHERE id_doc = ".intval($this->t_doc['ID_DOC'])."";
			trace($sql);
			$ok=$db->Execute($sql);
			if (defined('useSolr') && useSolr==true) {
					$this->saveSolr();
				}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocComment);trace($sql); return false;} else return true;
	}

	function calcAverageNote() {
		global $db;
		$this->t_doc['DOC_NOTE'] = $db->GetOne("select avg(DC_NOTE) from t_doc_comment where id_doc=".intval($this->t_doc['ID_DOC']));
		return;
	}

	function saveDocPublication() {
	trace('saveDocPublication');
		if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDeleteDocNoId);return false;}
		global $db;
		$db->StartTrans();
		$db->Execute('DELETE FROM t_doc_publication WHERE ID_DOC='.intval($this->t_doc['ID_DOC']));
		if (!empty($this->t_doc_publication)){
			foreach ($this->t_doc_publication as $idx=>$docPub) {
				$docPub['ID_DOC'] = $this->t_doc['ID_DOC'];
				$ok = $db->insertBase("t_doc_publication","id_doc_publication",$docPub);
				if (!$ok && !empty($sql)) trace($db->ErrorMsg());
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocRedif);trace($sql); return false;} else return true;
	}

	function createFromArrayDocPublication($pub_value,$id_ligne_panier,$pub_id_etape,$pub_cible,$pub_value2, $id_job = ''){
		$this->getDocPublication();
		$index=count($this->getDocPublication());
		if(!empty($id_ligne_panier)){
			foreach ($this->t_doc_publication as $idx=>$pub) {
				if($pub['PUB_ID_ETAPE']==$pub_id_etape && $pub['ID_LIGNE_PANIER']==$id_ligne_panier){
					$index=$idx;
				}
			}
		}else{
			foreach ($this->t_doc_publication as $idx=>$pub) {
				if($pub['PUB_ID_ETAPE']==$pub_id_etape){
					$index=$idx;
				}
			}
		}
		$this->t_doc_publication[$index]['PUB_VALUE'] = $pub_value;
		$this->t_doc_publication[$index]['ID_LIGNE_PANIER'] = $id_ligne_panier;
		$this->t_doc_publication[$index]['PUB_ID_ETAPE'] = $pub_id_etape;
		$this->t_doc_publication[$index]['PUB_CIBLE'] = $pub_cible;
		$this->t_doc_publication[$index]['PUB_VALUE2'] = $pub_value2;
		if(!empty($id_job))
			$this->t_doc_publication[$index]['PUB_ID_JOB'] = $id_job;
		$this->saveDocPublication();
	}


/** Supprime tous les liens Doc_Mat d'un document en base
 *  NOTE : ne touche pas à la structure t_doc_mat
 * OBSOLETE, ne plus utiliser car la méthode saveDocMat a été revue pour gérer du DELETE
 */
	function purgeDocMat() {

		global $db;
		$db->Execute('DELETE FROM t_doc_mat WHERE ID_DOC='.intval($this->t_doc['ID_DOC']));
	}

	function checkDocMatExists($dm) {

		global $db;

		$sql = 'SELECT ID_DOC_MAT FROM t_doc_mat WHERE ID_DOC='.intval($dm['ID_DOC']).'
				AND ID_MAT='.intval($dm['ID_MAT']);
		if(!defined("gCheckDocMatWithoutTc") || !gCheckDocMatWithoutTc ){
			if(!empty($dm['DMAT_TCIN'])) {
				$sql .= " AND DMAT_TCIN = ".$db->Quote($dm['DMAT_TCIN']);
			}
			if(!empty($dm['DMAT_TCOUT'])) {
				$sql .= " AND DMAT_TCOUT = ".$db->Quote($dm['DMAT_TCOUT']);
			}
		}
		return $db->GetOne($sql);
	}

/**
	 * Sauve les associations DOCUMENT / MATERIEL ainsi que les VALEURS associées à cette association
	 * IN : tableau de classe t_doc_mat (et son sous tableau t_doc_mat_val), withMats pour créer les mat à la volée
	 * OUT : base mise à jour
	 */
	function saveDocMat($withMats=true) {
		global $db;
		 if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		 if (empty($this->t_doc_mat)) return false;
		require_once(modelDir.'model_materiel.php');

		$db->StartTrans();

		foreach ($this->t_doc_mat as $idx=>&$docMat) {
			if (empty($docMat['ID_DOC'])) $docMat['ID_DOC']=$this->t_doc['ID_DOC'];
			if (!empty($docMat['ID_DOC'])) {
				// VP 7/12/10 : prise en compte MAT_ID_MEDIA
				if ($withMats && !($docMat['action']=='delete' && !empty($docMat['ID_DOC_MAT']))) {
					// Cas "normal" => on sauve le matériel systématiquement parce qu'il faut au moins sauver le format.
					$mat=new Materiel();
					if(isset($docMat['ID_MAT'])){
						$mat->t_mat['ID_MAT']=$docMat['ID_MAT'];
					}
					foreach ($docMat as $f=>$v)
						if (strpos($f, "MAT_") === 0)
							$mat->t_mat[$f]=$docMat[$f];
					if(!empty($docMat['MAT_ID_MEDIA'])) $mat->t_mat['MAT_ID_MEDIA']=$docMat['MAT_ID_MEDIA'];
					if (!$id_mat = $mat->checkExist()) { //Nvelle ligne dans le formulaire doc_mat => insert
						$mat->create();
						$docMat['ID_MAT'] = $mat->t_mat['ID_MAT'];
					} else {
						$mat->t_mat['ID_MAT'] = $id_mat;
						$docMat['ID_MAT'] = $id_mat;
                        $mat->id_mat_ref=$mat->t_mat['MAT_NOM']; //Mat. existant => update
						$mat->getMat();
						if(!empty($docMat['MAT_NOM'])) $mat->t_mat['MAT_NOM']=$docMat['MAT_NOM'];
						if(!empty($docMat['MAT_ID_MEDIA'])) $mat->t_mat['MAT_ID_MEDIA']=$docMat['MAT_ID_MEDIA'];
						if(!empty($docMat['MAT_FORMAT'])) $mat->t_mat['MAT_FORMAT']=$docMat['MAT_FORMAT'];
						$mat->save();
					}
					$this->dropError($mat->error_msg);
					unset($mat);
				}

				// Note sur le champ MAT_FORMAT : il est systématique présent dans le formulaire doc_mat
				// mais il ne relève pas du lien doc_mat mais du matériel. Il faut donc le retirer après sauv. matériel.
				// C'est pas super propre.
				unset($docMat['MAT_FORMAT']);
				unset($docMat['MAT_ID_MEDIA']);
				unset($docMat['MAT_NOM']);
				unset($docMat['MAT_CHEMIN']);

				$docMat['ID_DOC']=$this->t_doc['ID_DOC']; // Synchro ID_DOC dans doc_mat et ID_DOC du DOC
				$dmVal=$docMat['t_doc_mat_val'];unset($docMat['t_doc_mat_val']);
				$buff=$docMat['ID_DOC_MAT']; //on stocke un éventuel ID_DOC_MAT déjà présent (import)
				//update VG 20/07/11 : ajout du test
				if(empty($docMat['ID_DOC_MAT']))
					$docMat['ID_DOC_MAT']=$this->checkDocMatExists($docMat); //Récup de l'ID_DOC_MAT s'il existe


				//BY LD 02 06 09 : pour docSaisie, on peut avoir des actions sur chaque ligne (delete,save,new)
				//dans les autres cas (pour le moment) il n'y a pas d'action, la gestion classique s'opère
                $deleted=false;
				if ($docMat['action']=='delete' && !empty($docMat['ID_DOC_MAT'])) {
					$sql="DELETE from t_doc_mat where ID_DOC_MAT=".intval($docMat['ID_DOC_MAT']);
					$ok=$db->Execute($sql);
					if ($ok) $deleted=true; //flag suppression
				}
				else if (empty($docMat['ID_DOC_MAT']) || $docMat['action']=='new') { //Pas de ID_DOC_MAT ?
					// VP 16/3/09 : ajout date et user création par défaut
					$docMat['DMAT_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
					$docMat['DMAT_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];
					unset($docMat['action']);
					if (!empty($buff))
						$docMat['ID_DOC_MAT']=$buff; //on repasse le bufferisé, vide si pas d'ID_DOC_MAT originel
					else unset($docMat['ID_DOC_MAT']);
					// $rs = $db->Execute("SELECT * FROM t_doc_mat WHERE ID_DOC_MAT=-1");
					// $sql = $db->GetInsertSQL($rs, $docMat);
					// $ok = $db->Execute($sql);
					$ok = $db->insertBase("t_doc_mat","id_doc_mat",$docMat);
					$docMat['ID_DOC_MAT']=$this->checkDocMatExists($docMat);

				} else { //l'ID_DOC_MAT existe déjà => update
					// VP 16/3/09 : ajout date et user modif par défaut
					$docMat['DMAT_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
					$docMat['DMAT_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
					$rs = $db->Execute("SELECT * FROM t_doc_mat WHERE ID_DOC_MAT=".intval($docMat['ID_DOC_MAT']));
					$sql = $db->GetUpdateSQL($rs, $docMat);
					if (!empty($sql)) $ok = $db->Execute($sql);
					unset($tab);
				}


				if (!empty($dmVal)) { //sauvegarde des valeurs associées
					$db->Execute('DELETE FROM t_doc_mat_val WHERE ID_DOC_MAT='.intval($docMat['ID_DOC_MAT']));

					if (!$deleted) { //pas en mode "delete" => on sauve les t_doc_mat_val
						foreach ($dmVal as $val) {
							// VP 9/03/09 : ajout test existence ID_VAL avant insertion t_doc_mat_val
							if(!empty($val['ID_VAL'])){
								$sql="INSERT INTO t_doc_mat_val (ID_DOC_MAT,ID_VAL,ID_TYPE_VAL) values
								(".intval($docMat['ID_DOC_MAT']).",".intval($val['ID_VAL']).",".$db->Quote($val['ID_TYPE_VAL']).")";
								$ok=$db->Execute($sql);
							}
						}
					}
				}

				//Pas en mode delete ? on màj éventuellement DOC_DUREE
				if (defined("gAlwaysUpdateDureeWithDocMat") && gAlwaysUpdateDureeWithDocMat==true && !$deleted) {
				//Update DOC_DUREE du document à partir des DMAT tcin et tcout 16/5/08 ld
					// VP 16/12/09 : ajout condition DOC_DUREE='00:00:00'
					// VP 15/04/10 : ajout condition or DOC_DUREE is null
					
					if (!empty($docMat['DMAT_TCIN']) && !empty($docMat['DMAT_TCOUT'])) {
						$frames=(tcToFrame($docMat['DMAT_TCOUT'])-tcToFrame($docMat['DMAT_TCIN']));
						if ($frames!==0) { //$db->Execute("UPDATE t_doc SET DOC_DUREE=".$db->Quote(frameToTime($frames))." WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." and (DOC_DUREE='00:00:00' or DOC_DUREE is null)");
						//gUpdateDureeWithLastDocMat => update la durée du doc avec la durée du dernier mat ( meme si doc_duree existe)
							if (defined("gUpdateDureeWithLastDocMat") && gUpdateDureeWithLastDocMat==true){ 	
								$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC']));
							}
							else{
								$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." and (DOC_DUREE='00:00:00' or DOC_DUREE is null)");
							}
							$sql = $db->GetUpdateSQL($rs, array("DOC_DUREE" => frameToTime($frames)));
							if (!empty($sql)) $ok = $db->Execute($sql);
						}
					}
				}

				//mode delete ? on supprime la ligne dans l'objet
				if ($deleted) unset($this->t_doc_mat[$idx]); //note:on n'utilise pas la réf $docMat car ca marche pas bien

			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocMat); return false;} else return true;
	}

	/**
	 * Actualise le lien DOC_MAT de séquences avec les DOCTCIN et DOCTCOUT de ces séquences.
	 * Utilisé en finupload pour créer les liens DOC_MAT des séquences. C'est notamment le cas pour la
	 * LNR où les séquences sont créées par import et le matériel uploadé APRES.
	 * IN : id_doc PARENT, id_mat PARENT
	 */
	function saveDocMatForSequences($id_mat) {
		global $db;
		$sql="DELETE FROM t_doc_mat WHERE id_mat=".intval($id_mat)."
				AND id_doc in (SELECT id_doc FROM t_doc WHERE doc_id_gen = ".intval($this->t_doc['ID_DOC']).");";
		$ok=$db->Execute($sql);
		$sql="INSERT INTO t_doc_mat (id_mat,id_doc,dmat_tcin,dmat_tcout)
				SELECT dm.id_mat,ds.id_doc,ds.doc_tcin, ds.doc_tcout
				FROM t_doc_mat dm
				INNER JOIN t_doc dp ON dm.id_doc=dp.id_doc
				INNER JOIN t_doc ds ON dp.id_doc=ds.doc_id_gen
				WHERE dp.id_lang='FR' and ds.id_lang='FR'
				and dp.id_doc=".intval($this->t_doc['ID_DOC'])."
				and dm.id_mat=".intval($id_mat)."
				";
		$ok=$db->Execute($sql);

	}



	function linkToAnotherDoc()  {
		global $db;
		if (!empty($this->id_doc2link)) {
			if ($this->relation=="SRC") {$fils="DST";$parent="SRC";} else {$fils="SRC";$parent="DST";}
			if (!$db->getOne("SELECT ID_DOC_SRC FROM t_doc_lien WHERE ID_DOC_".$fils." = ".intval($this->t_doc['ID_DOC'])." AND ID_DOC_".$parent." = ".intval($this->id_doc2link))) {
				//$sql="INSERT INTO t_doc_lien (ID_DOC_".$fils.",ID_DOC_".$parent.",DD_DUREE) select ".intval($this->t_doc['ID_DOC']).",".intval($this->id_doc2link).",'".$this->t_doc['DOC_DUREE']."'";
				/*$arr=array("ID_DOC_".$fils => intval($this->t_doc['ID_DOC']), "ID_DOC_".$parent => intval($this->id_doc2link), "DD_DUREE" => $this->t_doc['DOC_DUREE']);
				$rs = $db->Execute("SELECT * FROM t_doc_lien WHERE ID_DOC_SRC = -1");
				$sql = $db->getInsertSql($rs, $arr);

				$ok=$db->Execute($sql);
				if (!$ok) {$this->dropError(kErrorSauveDocLien);trace($sql);return false;} else return true;*/

				if ($this->relation=="SRC") $this->arrDocLiesDST[] = array("ID_DOC_".$parent => intval($this->id_doc2link), "DD_DUREE" => $this->t_doc['DOC_DUREE']);
				else $this->arrDocLiesSRC[] = array("ID_DOC_".$parent => intval($this->id_doc2link), "DD_DUREE" => $this->t_doc['DOC_DUREE']);
				return $this->saveDocLies(false);
			}
			return false;
		}
	}

	/**
	 * Met à jour le champs DOC_SEQ_TC du parent du document.
	 * IN : var de classe doc_id_gen (id du parent), langue
	 * OUT : update en base
	 */

	function updateParentTC() {
	        global $db;
	         // on r?cup?re le titre de tous les fils du document parent
            $titre="";
          	if ($this->t_doc['DOC_ID_GEN']==0) return false;
            $result=$db->Execute("SELECT DISTINCT ID_DOC, DOC_TITRE FROM t_doc WHERE DOC_ID_GEN=".intval($this->t_doc['DOC_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']));
            while($row=$result->FetchRow()){
                $titre.=$row["DOC_TITRE"].";";
            }
            $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['DOC_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']));
			$sql = $db->GetUpdateSQL($rs, array("DOC_SEQ_TC" => $titre));
			if (!empty($sql)) $result = $db->Execute($sql);
			$result->Close();
	}

	/** Sauve un document dans les autres langues disponibles dans OPSIS
	 *  IN : tableau de classe t_doc, $tabLangues (opt) = tableau des langues que l'on veut sauver
	 *  OUT : bdd màj avec insertion t_doc des nv langues avec contrôle d'existence
	 * 		  + recopie des t_doc_lex_precision pour ces nouvelles versions
	 *  NOTES : pas d'update, juste insertion (contrôle si version existe)
	 * 			se base sur le tableau des champs dans t_doc -> on a retiré l'insertion de ts les champs (15/02/08)
	 */
	function saveVersions($tabLangues='', $tabVersions = array()) {
	        global $db;

			//si on ne passe pas de tableau, on utilise le tableau de ttes les langues en session
			if ($tabLangues=='') $tabLangues=$_SESSION['arrLangues'];
	        if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
			$ok=true;

			// MS - 13.04.2015 - Avant de débuter le saveVersions, on récupère l'ensemble du doc, et non uniquement ce qui a été envoyé dans le $_POST
			$this->getDoc();

			foreach($tabLangues as $lg){
				$arrDoc = $this->t_doc;
				if (!empty($tabVersions) && !empty($tabVersions[$lg]))
					$arrDoc = $tabVersions[$lg];
				
                // I.5.a. On s'assure que la version ? cr?er n'existe pas d?j?
                $existVersion=$db->Execute("SELECT ID_DOC FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($lg));
                 if($existVersion->RecordCount()==0){

					// $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=-1");
					$arrDoc["ID_LANG"] = $lg;
					// $sql = $db->GetInsertSQL($rs, $arrDoc);
					// if (!empty($sql)) $ok = $db->Execute($sql);
					 $ok = $db->insertBase("t_doc","id_doc",$arrDoc);




                    if (!$ok) {$this->dropError(kErrorSauveAutreVersion." (".$lg.")");}

				   // I.5.b Insertion des pr?cisions des versions traduites dans t_doc_lex_precision
				    $db->Execute("DELETE FROM t_doc_lex_precision WHERE t_doc_lex_precision.ID_LANG like '".($lg)."' AND t_doc_lex_precision.ID_DOC_LEX in (SELECT t_doc_lex.ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_DOC=".intval($this->t_doc['ID_DOC']).")");
					$db->Execute("INSERT INTO t_doc_lex_precision SELECT t_doc_lex_precision.ID_DOC_LEX, '".($lg)."', t_doc_lex_precision.LEX_PRECISION FROM t_doc_lex_precision, t_doc_lex WHERE t_doc_lex.ID_DOC_LEX=t_doc_lex_precision.ID_DOC_LEX AND t_doc_lex.ID_DOC=".intval($this->t_doc['ID_DOC']));
                }
            }
    if (!$ok) return false;
	return true;
	}


function updateVersions($tabVersions) {
	global $db;

	foreach ($tabVersions as $lang=>$trad_vals) {
	/*	if(!in_array($lang,$this->arrVersions)){
			return false;
		}*/

		$doc_lang = new Doc;
		$doc_lang->t_doc = $this->t_doc;
		$doc_lang->t_doc['ID_LANG'] = $lang;
		foreach ($trad_vals as $field=>$val){
			if(array_key_exists($field,$doc_lang->t_doc)){
				$doc_lang->t_doc[$field] = $val;
			}
		}

		$ok=$doc_lang->save();

		if ($ok==false && $doc_lang->error_msg) {$this->dropError(kErrorSauveDoc.' : '.$doc_lang->error_msg);return false;}

		unset($doc_lang);
	}

	return true;
}

    /**
     * Récupère les infos sur le document + d'autre types d'infos selon les params
     * IN : bool pour charger LEX, VAL, MAT, IMG, SEQ, REDIF
     * OUT : tableaux de classe doc et autres tableaux selon les extensions.
     * NOTE : ramène aussi les libellés utilisateurs
     */
    function getDocFull($charger_lex=0,$charger_val=0,$charger_mat=0,$charger_img=0,$charger_seq=0,$charger_redif=0,$charger_lexdroit=0,$charger_prex=0,$charger_exp=0,$charger_cat=0,$charger_comment=0,$charger_publication=0,$charger_festival=0,$getFileInfo=true){
		global $db;
		$this->getDoc();
		$this->getUserCreaModif();

		if ($charger_lex!=0)  {$this->getLexique();$this->getPersonnes();}
		if ($charger_val!=0)  $this->getValeurs();
		if ($charger_cat!=0)  $this->getCategories();
		//PC 15/10/10 récupération des valeurs des Matériels
		if ($charger_mat!=0)  {$this->getDocMat();$this->getMats(true,$getFileInfo);}
		if ($charger_img!=0)  $this->getImages();
		if ($charger_redif!=0) $this->getRedif();
		if ($charger_seq!=0) $this->getSequences();
		if ($charger_lexdroit!=0) $this->getDocLexDroit();
		if ($charger_prex!=0) $this->getDroitPrex();
		if ($charger_exp!=0) $this->getDocExp();
		if ($charger_comment!=0) $this->getDocComments();
		if ($charger_publication!=0) $this->getDocPublication();
		if ($charger_festival!=0) $this->getFestival();
    }



	function getUserCreaModif(){
		global $db;
		$this->user_crea=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_doc['DOC_ID_USAGER_CREA']));
		$this->user_modif=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_doc['DOC_ID_USAGER_MODIF']));
	}
	/**

	 * Récupère les informations sur un document (t_doc) et les stocke dans autant de variables

	 * de classe. On retire l'espace autour des apostrophes pour les champs texte

	 * IN : $id_doc, $id_lang

	 * OUT : variables de classe

	 *

	 */

	function getDocExtended($id_doc,$id_lang) {
	    global $db;
        $sql="SELECT T1.*, T2.DOC_TITRE as DOC_TITRE_PARENT, T3.US_NOM as US_NOM_MODIF,
				T4.US_NOM as US_NOM_CREA, t_genre.GENRE, t_type_doc.TYPE_DOC,
				t_fonds.FONDS, t_fonds.FONDS_COL, t_image.ID_IMAGEUR, t_image.IM_FICHIER, t_image.IM_CHEMIN";
        $sql.=" FROM t_doc T1";
        $sql.=" LEFT OUTER JOIN t_doc T2 ON (T1.DOC_ID_GEN=T2.ID_DOC AND T2.ID_LANG=".$db->Quote($id_lang).")";
        $sql.=" LEFT OUTER JOIN t_usager T3 ON (T1.DOC_ID_USAGER_MODIF=T3.ID_USAGER)";
        $sql.=" LEFT OUTER JOIN t_usager T4 ON (T1.DOC_ID_USAGER_CREA=T4.ID_USAGER)";
        $sql.=" LEFT OUTER JOIN t_fonds ON (T1.DOC_ID_FONDS=t_fonds.ID_FONDS)";
        $sql.=" LEFT OUTER JOIN t_image ON (T1.DOC_ID_IMAGE=t_image.ID_IMAGE AND t_image.ID_LANG=".$db->Quote($id_lang).")";
        $sql.=" LEFT OUTER JOIN t_genre ON (t_genre.ID_GENRE=T1.DOC_ID_GENRE AND t_genre.ID_LANG=".$db->Quote($id_lang).")";
        $sql.=" LEFT OUTER JOIN t_type_doc ON (t_type_doc.ID_TYPE_DOC=T1.DOC_ID_TYPE_DOC AND t_type_doc.ID_LANG=".$db->Quote($id_lang).")";
        $sql.=" WHERE T1.ID_DOC=".intval($id_doc)." and T1.ID_LANG=".$db->Quote($id_lang);
        $result=$db->Execute($sql);

        if ($result->RecordCount()>0){
            $list = $result->FetchRow();
            foreach ($list as $key => $value) {
                $champ = strtolower($key);
                $this->$champ = $value;
            }
			$this->spaceQuotes(array(" ' "," ’ "),array("'","’"));
       }
	}


	/**

	 * Récupère les termes de lexique + précisions + rôles associés à un document

	 * IN : ID_DOC et ID_LANG

	 * OUT : tableau t_doc_lex (classe)

	 */

	function getLexique () {
		    global $db;

		    //$sql = "SELECT t_doc_lex.*, t_lexique.LEX_ID_TYPE_LEX, t_lexique.LEX_TERME, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE";
		// VP 23/02/10 : ajout LEX_PATH à requête
		    $sql = "SELECT t_doc_lex.*, t_lexique.*, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE, syn.LEX_TERME as SYNO FROM t_doc_lex
                    LEFT OUTER JOIN t_lexique ON t_doc_lex.ID_LEX=t_lexique.ID_LEX
                    LEFT OUTER JOIN t_doc_lex_precision ON (t_doc_lex_precision.ID_DOC_LEX=t_doc_lex.ID_DOC_LEX AND t_doc_lex_precision.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
                    LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
                    LEFT OUTER JOIN t_lexique syn ON (t_lexique.LEX_ID_SYN=syn.ID_LEX AND syn.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
                    WHERE t_doc_lex.ID_PERS=0  AND t_doc_lex.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_lexique.ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])." order by ID_DOC_LEX";
            $this->t_doc_lex=$db->GetAll($sql);
	}


	/**
	 * Récupération des personnes liées au document via la table t_doc_lex
	 * IN : id_doc, id_lang (var de classe)
	 * OUT : tableau t_pers_lex.
	 * NOTE : ATTENTION ce tableau s'appelle bien pour commodité t_pers_lex mais n'a RIEN à voir avec la table t_pers_lex
	 */
	function getPersonnes() {
			global $db;
		    $sql = "SELECT t_doc_lex.*, t_personne.PERS_ID_TYPE_LEX, t_personne.PERS_NOM,t_personne.PERS_PRENOM, t_personne.PERS_PARTICULE, t_doc_acc.DA_CHEMIN, t_doc_acc.DA_FICHIER, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE, t_personne.PERS_ID_ETAT_LEX";
            $sql.= " FROM t_doc_lex LEFT OUTER JOIN t_personne ON t_doc_lex.ID_PERS=t_personne.ID_PERS ";
            $sql.= " LEFT OUTER JOIN t_doc_acc ON (t_personne.PERS_PHOTO=t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).") ";
            $sql.= " LEFT OUTER JOIN t_doc_lex_precision ON (t_doc_lex_precision.ID_DOC_LEX=t_doc_lex.ID_DOC_LEX AND t_doc_lex_precision.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).") ";
            $sql.= " LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).") ";
            $sql.= " WHERE t_doc_lex.ID_LEX=0 AND t_doc_lex.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_personne.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
			$sql.= " ORDER BY t_doc_lex.ID_DOC_LEX";

            $this->t_pers_lex=$db->GetAll($sql);
	}


	/**
	 * Récupération des personnes liées au document via la table t_doc_lex ainsi que leurs ayant-droit
	 * IN : id_doc, id_lang (var de classe)
	 * OUT : tableau t_doc_lex_droit.
	 */
	function getDocLexDroit() {
		global $db;
		$sql = "SELECT t_doc_lex_droit.*, t_doc_lex.*, t_personne.PERS_ID_TYPE_LEX, ".$db->Concat('t_personne.PERS_NOM',"' '",' t_personne.PERS_PRENOM')." as PERS_NOM, t_personne.PERS_PARTICULE, t_doc_acc.DA_CHEMIN, t_doc_acc.DA_FICHIER, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE";
		$sql.= " ,CASE t_doc_lex_droit.DR_GESTION WHEN 'COL' THEN 'Gestion collective' WHEN 'CFR' THEN 'Géré par le CFRT' WHEN 'OTH' THEN 'Autres' END as DR_GERE_PAR ";
		$sql.= " FROM t_doc_lex INNER JOIN t_personne ON t_doc_lex.ID_PERS=t_personne.ID_PERS ";
		$sql.= " LEFT OUTER JOIN t_doc_acc ON (t_personne.PERS_PHOTO=t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).") ";
		$sql.= " LEFT OUTER JOIN t_doc_lex_precision ON (t_doc_lex_precision.ID_DOC_LEX=t_doc_lex.ID_DOC_LEX AND t_doc_lex_precision.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).") ";
		$sql.= " LEFT OUTER JOIN t_doc_lex_droit ON (t_doc_lex_droit.ID_DOC_LEX = t_doc_lex.ID_DOC_LEX) ";
		$sql.= " LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).") ";
		$sql.= " WHERE t_doc_lex.ID_LEX=0 AND t_doc_lex.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_personne.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);

		$this->t_doc_lex_droit = $db->GetAll($sql);
	}


	/**
	 * Récupération des droits préexistant
	 * IN : id_doc, id_lang (var de classe)
	 * OUT : tableau t_doc_prex.
	 */
	function getDocPrex() {
		global $db;
		$sql = "SELECT d.*,
				CASE DP_TYPE WHEN 'MUS' THEN 'Musique' WHEN 'ICO' THEN 'Iconographie' WHEN 'ART' THEN 'Oeuvre d''art' WHEN 'IMG' THEN 'Image' END as DP_LABEL_TYPE,
				CASE DP_AUDITE WHEN '1' THEN 'Oui' ELSE 'Non' END as DP_AUDITE_LABEL,
				".$db->Concat('PERS_NOM',"' '",' PERS_PRENOM')." as CONTACT,
				".$db->Concat('DPP_PRIX',"' '",' DPP_DEVISE')." as PRIX
				FROM t_doc_prex d
				left outer join t_dp_pers dp on dp.id_doc_prex = d.id_doc_prex
				left outer join t_personne p on p.id_pers = dp.id_pers
				where ID_DOC=".intval($this->t_doc['ID_DOC'])."
				ORDER BY DP_TCIN, ID_DOC_PREX";
		$this->t_doc_prex = $db->GetAll($sql);
	}

	/**
	 * Récupération des droits préexistant
	 * IN : id_doc, id_lang (var de classe)
	 * OUT : tableau t_doc_prex.
	 */
	function getDroitPrex() {
		include_once(modelDir.'model_doc_prex.php');
		global $db;
	    if (empty($this->t_doc_prex)) {
			$this->getDocPrex();
	    }
		foreach ($this->t_doc_prex as $idx=>&$dp) {
			$prex = new DroitPrex();
			$prex->t_prex['ID_DOC_PREX'] = $dp['ID_DOC_PREX'];
			$prex->getPrex();
			$prex->getPersonne();
			$prex->getValeurs();
			$dp['PREX'] = $prex;
			unset($prex);
		}
	}

	/**
	 * Récupération des exploitations
	 * OUT : tableau t_doc_exp.
	 */
	function getDocExp() {
		global $db;
		$sql = "SELECT t_exp_doc.*, t_exploitation.EXP_ARTICLE, t_exploitation.EXP_SDRM, t_exploitation.EXP_TITRE, t_type_exp.type_exp as EXP_TYPE, t_type_exp.ID_TYPE_EXP
		FROM t_exploitation
		INNER JOIN t_exp_doc ON  t_exp_doc.ID_EXP = t_exploitation.ID_EXP
		LEFT OUTER JOIN t_type_exp on t_type_exp.id_type_exp = t_exploitation.EXP_ID_TYPE_EXP
		WHERE ID_DOC = ".intval($this->t_doc['ID_DOC'])."
		ORDER BY t_exploitation.ID_EXP";
		$this->t_doc_exp = $db->GetAll($sql);
	}

	/**
	 * Récupère les valeurs liées à un document
	 * IN : ID_DOC et ID_LANG
	 * OUT : tableau tab_val (classe)
	 */
	function getValeurs() {
			global $db;
			//by LD 11/12/08 : récup également du ID_TYPE_VAL de l'asso
		    $sql = "SELECT t_val.*,t_doc_val.ID_TYPE_VAL FROM t_doc_val
					INNER JOIN t_val ON t_doc_val.ID_VAL=t_val.ID_VAL
					WHERE t_doc_val.ID_DOC=".intval($this->t_doc['ID_DOC'])." AND t_val.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
		   $this->t_doc_val=$db->GetAll($sql);
	}


	/*
	recuperation des categories
	*/
	function getCategories()
	{
		global $db;
		$sql='SELECT t_categorie.*,t_doc_cat.CDOC_ORDRE,t_doc_acc.ID_DOC_ACC,t_doc_acc.DA_FICHIER, t_doc_acc.DA_CHEMIN
		FROM t_doc_cat
		LEFT JOIN t_categorie ON t_doc_cat.ID_CAT=t_categorie.ID_CAT
		LEFT JOIN t_doc_acc ON t_categorie.CAT_ID_DOC_ACC=t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG='.$db->Quote($this->t_doc['ID_LANG']).'
		WHERE t_doc_cat.ID_DOC='.intval($this->t_doc['ID_DOC']).' AND t_categorie.ID_LANG='.$db->Quote($this->t_doc['ID_LANG']);
		$this->t_doc_cat=$db->GetAll($sql);
	}


	/**
	 * Récupère la liste des matériels associés au doc (doc_mat)
	 * IN : ID_DOC et ID_LANG
	 * OUT : tableau t_doc_mat (classe)
	 */
	function getDocMat() {
	        global $db;
            // VP 11/06/2014 : suppression tri sur DMAT_TCIN
	        //$sql = "SELECT t_doc_mat.* FROM t_doc_mat where ID_DOC=".intval($this->t_doc['ID_DOC'])." ORDER BY DMAT_TCIN,ID_MAT";
            $sql = "SELECT t_doc_mat.* FROM t_doc_mat where ID_DOC=".intval($this->t_doc['ID_DOC'])." ORDER BY ID_MAT";
            $this->t_doc_mat=$db->GetAll($sql);
	}

	/**
	 * Récupère la liste des matériels associés au doc en entrée(doc_mat)
	 * IN : ID_DOC et ID_LANG
	 * OUT : tableau t_doc_mat (classe)
	 */
	function getDocMatById($ID_DOC) {
	        global $db;
            // VP 11/06/2014 : suppression tri sur DMAT_TCIN
	        //$sql = "SELECT t_doc_mat.* FROM t_doc_mat where ID_DOC=".intval($ID_DOC)." ORDER BY DMAT_TCIN,ID_MAT";
            $sql = "SELECT t_doc_mat.* FROM t_doc_mat where ID_DOC=".intval($ID_DOC)." ORDER BY ID_MAT";
            $this->t_doc_mat=$db->GetAll($sql);
	}

	/** Récupération du détail d'un matériel au sein du tableau t_doc_mat
	 * IN : t_doc_mat (chargé si n'existe pas)
	 * OUT : t_doc_mat avec un objet 'MAT' à chaque ligne
	 */
	function getMats($withVals=false, $getFileInfo = true) {
		include_once(modelDir.'model_materiel.php');
		global $db;
	    if (empty($this->t_doc_mat)) {
	    		$this->getDocMat();
	    }
		foreach ($this->t_doc_mat as $idx=>&$dm) {
			// VP 15/10/08 : ajout jointure avec t_val
			$sql="SELECT * from t_doc_mat_val
					INNER JOIN t_val ON t_val.ID_VAL=t_doc_mat_val.ID_VAL
					WHERE ID_DOC_MAT=".intval($dm['ID_DOC_MAT'])." and t_val.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
			$dm['t_doc_mat_val']=$db->GetAll($sql);
			$mat=new Materiel();
			$mat->t_mat['ID_MAT']=$dm['ID_MAT'];
			$mat->getMat(array(),'', $getFileInfo);
			if ($withVals) $mat->getValeurs();
			$dm['MAT']=$mat;
			unset($mat); // libération mémoire
		}
	}


	/**
	 * récupère la liste des images + imageur associés à un doc
	 * IN :var de classe : id_doc, id_lang doc_tcin et doc_tcout (opt)
	 * OUT : tableau tab_images (classe)
	 */
	// VP 2/9/09 : réécriture de getImages inspirée de processStoryboard
	function getImages() {
		global $db;

		if (!empty($this->t_doc['DOC_ID_IMAGEUR'])) {
			$imageur=$this->t_doc['DOC_ID_IMAGEUR'];
			$tcin=$this->t_doc['DOC_TCIN'];
			$tcout=$this->t_doc['DOC_TCOUT'];
		}
		if (empty($this->t_doc_mat)) $this->getMats();
	    //si on a pas d'imageur document OU BIEN on a le même dans l'imageur matériel, on prend les bornes de l'imageur matériel
		// VP 12/04/10 : ajout test sur imageur mat vide
    	foreach ($this->t_doc_mat as $idx=>$dm) {
			if (isset($dm['MAT']->t_mat['MAT_ID_IMAGEUR']) && !empty($dm['MAT']->t_mat['MAT_ID_IMAGEUR']) && (empty($this->t_doc['DOC_ID_IMAGEUR']) || $this->t_doc['DOC_ID_IMAGEUR']==$dm['MAT']->t_mat['MAT_ID_IMAGEUR'])) {
				$imageur=$dm['MAT']->t_mat['MAT_ID_IMAGEUR'];
				$tcin=$dm['DMAT_TCIN'];
				$tcout=$dm['DMAT_TCOUT'];
			}
    	}
		if(!empty($imageur)){
			$sql = "SELECT * from t_image inner join t_imageur on t_image.ID_IMAGEUR = t_imageur.ID_IMAGEUR where t_imageur.ID_IMAGEUR =".$db->Quote($imageur)." AND t_image.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
			if(($tcin!= '') && ($tcout!= '') && ($tcout!= '00:00:00:00')){
				$sql.=" and IM_TC >= ".$db->Quote($tcin)." and IM_TC <= ".$db->Quote($tcout);
			}
            // VP 22/01/2013 : tri par TC puis Chemin
            $sql.=" ORDER BY IM_TC, IM_CHEMIN";
			$this->t_images=$db->GetAll($sql);
		}
	}

	function getImagesOld() {
			global $db;

	        if ($this->t_doc['DOC_ID_IMAGEUR']!='') {
	        //1ère méthode, héritée de la V1, lien direct entre un doc et un imageur
	        $sql = "SELECT ID_IMAGE,IM_CHEMIN, ID_IMAGEUR, IMAGEUR, IM_TEXT, IM_TC from t_image i,t_doc d where
					d.DOC_ID_IMAGEUR=i.ID_IMAGEUR and d.ID_DOC=".$db->Quote($this->t_doc['ID_DOC'])."
				AND i.ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])." AND d.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
			if(($this->t_doc['DOC_TCIN']!= '') && ($this->t_doc['DOC_TCOUT']!= '') && ($this->t_doc['DOC_TCOUT']!= '00:00:00:00')){
				$sql.=" and i.IM_TC >= '".$this->t_doc['DOC_TCIN']."' and i.IM_TC <= '".$this->t_doc['DOC_TCOUT']."'";
			}
            $this->t_images=$db->GetAll($sql);
	        }
	        else {
	        //2ème méthode, depuis la v2, les imageurs ne sont liés qu'aux matériels. On passe donc par les matériels
	        //pour récupérer les images.
			// VP 11/12/08 : restrictions sur DMAT_TCIN et DMAT_TCOUT
	        $sql="select distinct i.*
				from
				t_image i, t_mat m, t_doc_mat dm
				where i.ID_IMAGEUR=m.MAT_ID_IMAGEUR
				and m.ID_MAT=dm.ID_MAT
				and dm.ID_DOC=".$db->Quote($this->t_doc['ID_DOC'])."
				and i.ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])."
				and i.IM_TC >= dm.DMAT_TCIN and i.IM_TC <= dm.DMAT_TCOUT
				order by i.IM_TC asc";
			/*
			 if(($this->t_doc['DOC_TCIN']!= '') && ($this->t_doc['DOC_TCOUT']!= '') && ($this->t_doc['DOC_TCOUT']!= '00:00:00:00')){
				$sql.=" and i.IM_TC >= '".$this->t_doc['DOC_TCIN']."' and i.IM_TC <= '".$this->t_doc['DOC_TCOUT']."'";
			}*/
				$this->t_images=$db->GetAll($sql);
	        }

	}


	/**
	 * Récupère la liste des rediffusions liées à un document
	 * IN : ID_DOC
	 * OUT : tableau tab_redif (classe)
	 */
	function getRedif() {
	        global $db;
			$sql="SELECT * FROM t_doc_redif where ID_DOC=" . intval($this->t_doc['ID_DOC']) . " ORDER BY ID_REDIF";
	        $this->t_doc_redif=$db->GetAll($sql);
	}

	/**
	 * Récupère la liste des séquences associées à un document
	 * IN : ID_DOC
	 * OUT : tableau tab_seq (classe)
	 */
	function getSequences() {
		global $db;
		// LD 31/03/08 => changé T1.ID_DOC,T1.DOC_TCIN,T1.DOC_TCOUT,T1.DOC_TITRE,T1.DOC_ID_IMAGE,T1.DOC_DUREE,T1.DOC_NUM en T1.*
		//PC 15/11/10 => ajout de la jointure avec t_type_doc
		// MS 28/05/2012 => ajout contrôle critère ID_LANG sur les jointures
        // VP 7/02/2013 : ajout t_image.IMAGEUR
        $sql="select distinct T1.*, TD.TYPE_DOC,
			t_image.IM_CHEMIN,t_image.IM_FICHIER,t_doc_acc.DA_CHEMIN,t_doc_acc.DA_FICHIER
			from t_doc T1
			inner join t_doc T2 ON T1.DOC_ID_GEN=T2.ID_DOC
			left join t_image on (T1.DOC_ID_IMAGE=t_image.ID_IMAGE  and t_image.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
			left join t_doc_acc on (T1.DOC_ID_DOC_ACC=t_doc_acc.ID_DOC_ACC  and t_doc_acc.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
			left outer join t_type_doc TD on (TD.ID_TYPE_DOC = T1.DOC_ID_TYPE_DOC and TD.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']).")
		where T2.ID_DOC=".intval($this->t_doc['ID_DOC'])." and T1.ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])." order by T1.DOC_TCIN"; //modifié le 31/1 par LD pour trier sur TCIN
		$this->t_doc_seq=$db->GetAll($sql);
	}

	function getFestival() {
		global $db;
		$sql="select distinct df.ID_DOC_FEST,f.*
		from t_doc_fest df
		inner join t_festival f ON f.ID_FEST=df.ID_FEST
		where df.ID_DOC=".intval($this->t_doc['ID_DOC'])."  and f.ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
		$this->t_doc_fest=$db->GetAll($sql);
		foreach ($this->t_doc_fest as $i=>$df) {
			$sql="SELECT * from t_doc_fest_val dfv
					inner join t_val v ON dfv.ID_VAL=v.ID_VAL
					where v.ID_LANG=".$db->Quote($this->t_doc['ID_LANG'])." and dfv.ID_DOC_FEST=".intval($df['ID_DOC_FEST']);
			$this->t_doc_fest[$i]['DOC_FEST_VAL']=$db->GetAll($sql);
		}
	}


    /**
     * Supprime un document de la base ou bien juste une version (si id_lang est passé)
     * En cas de suppression totale du doc, celui-ci est effacé + liens au lexique, matériel, valeurs et panier
     * IN : id_lang (opt, pour supprimer juste une version), delete_assoc_mats (opt, supprimer les matériels liés au doc si ils ne sont liés à aucun autre document)
     * OUT : update BDD
     */

    function delete($id_lang="",$delete_assoc_mats=false){
        global $db;

		if(isset($delete_assoc_mats) && $delete_assoc_mats){
			if(empty($this->t_doc_mat)){
				$this->getMats() ;
			}

			foreach($this->t_doc_mat as $dm){
				$dm['MAT']->getDocMat();
				$delMat=true;
				foreach($dm['MAT']->t_doc_mat as $mat_dm){
					if($mat_dm['ID_DOC']!=$this->t_doc['ID_DOC']) $delMat=false;
				}
				if($delMat){
					$dm['MAT']->delete();
					// trace("delete mat from doc ".$dm['MAT']->t_mat['ID_MAT']);
				}
			}
		}

        if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}

        if (empty($id_lang)) {
			// Avant suppresion effective, appel de putTrash() si paramètre gUseTrash existe et est à true
			if(defined("gUseTrash") && gUseTrash==true){
				$ok = $this->putTrash();
				if(!$ok) {
					$this->dropError(kErreurTrash);
					return false;
				}
			}
	        // II.1.C. Suppression des enregistrements li?s
	        deleteSQLFromArray(array("t_doc","t_doc_lex","t_doc_val","t_doc_mat","t_panier_doc","t_doc_cat"),"ID_DOC",intval($this->t_doc['ID_DOC']));
			$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_DOC = '".(intval($this->t_doc['ID_DOC']))."')");
			// VP 10/9/09 : suppression dans t_doc_lien
			$db->Execute("DELETE FROM t_doc_lien WHERE ID_DOC_SRC = '".($this->t_doc['ID_DOC'])."' or ID_DOC_DST = '".intval($this->t_doc['ID_DOC'])."'");
            // VP 13/11/14 : log action DEL
            logAction("DEL",array("ID_DOC"=>$this->t_doc['ID_DOC']));
       } else {
        	if ($this->t_doc['DOC_ID_GEN']!=0) {
	            $this->updateParentTC();
	        }
	        deleteSQLFromArray(array("t_doc"),array("ID_DOC","ID_LANG"),array(intval($this->t_doc['ID_DOC']),$id_lang));
			$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_DOC = '".(intval($this->t_doc['ID_DOC']))."') and ID_LANG='".($id_lang)."'");
            // VP 13/11/14 : log action DEL
            logAction("DEL",array("ID_DOC"=>$this->t_doc['ID_DOC']));
        }
        if (defined("useSinequa") && useSinequa) $this->deleteSinequa();

		if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true)){
			$this->deleteSolr($id_lang);
		}

        return true;
}
    /**
     * Place le document dans la corbeille
     * IN :
     * OUT : update BDD
     */

    function putTrash(){
    	global $db;
		// Export XML structure document complet (y compris séquences)
		$this->getDocFull(1,1,1,1,1,1);
		$xmldata=$this->xml_export(0);
		// VP 5/12/2011 : ajout xsl trashDoc
		$xmlTrash = TraitementXSLT($xmldata,getSiteFile("designDir","print/trashDoc.xsl"),array(),0,"",null,false);
		// Création objet trash
		require_once(modelDir.'model_trash.php');
		$myTrash= new Trash();
		$myTrash->t_trash["TRA_ENTITE"]="doc";
		$myTrash->t_trash["TRA_TITRE"]=$this->t_doc['DOC_TITRE'];
		$xmlTrash = preg_replace("/<\?.*?\?>/", "", $xmlTrash);

		$myTrash->t_trash["TRA_XML"]=$xmlTrash;
	    $ok = $myTrash->save();

	    if($ok) {
			// Listage des fichiers à mettre dans la corbeille, en les regroupant par type (mat, doc_acc, storyboard) : fichiers (mat, doc_acc) et dossiers (storyboard) liés au document qui ne sont pas utilisés ailleurs
			$trashDir=kTrashDir.$myTrash->t_trash['ID_TRASH'];
	    	$tab_fichier=array();
			// mat
			foreach ($this->t_doc_mat as $idx=>$dm){
				// On regarde si le matériel est utilisé ailleurs
				$dm['MAT']->getDocMat();
				$trashMat=true;
				foreach($dm['MAT']->t_doc_mat as $mat_dm){
					if($mat_dm['ID_DOC']!=$this->t_doc['ID_DOC']) $trashMat=false;
				}
				if($trashMat){
					$mat_fichier=$dm['MAT']->getFilePath();
					if($dm['MAT']->hasfile=='1'){
						$tab_fichier[]=array('type'=>'mat','file'=>$mat_fichier);
					}
					// Ajout imageur
					$mat_imageur=$dm['MAT']->t_mat['MAT_ID_IMAGEUR'];

					if(!empty($mat_imageur)){
						$objImageur=new Imageur();
						$objImageur->t_imageur['ID_IMAGEUR']=$mat_imageur;
						$objImageur->getImageur();
					}

					if(!empty($mat_imageur)) $tab_fichier[]=array('type'=>'storyboard','file'=>kStoryboardDir.$objImageur->getImageurPath());
					// Suppression en base (purge = false, bypass_puttrash = true => permet d'éviter d'appeler le puttrash de la classMat lorsqu'on trash un doc)
					$dm['MAT']->delete(false,true);
					// Suppression imageur en base
					if(!empty($mat_imageur)){
						$objImageur->deleteImageur(false); // On ne supprime pas les fichiers car on va les copier ensuite
						unset($objImageur);
					}
				}
			}
	    	// doc_acc
			foreach ($this->t_doc_acc as $idx=>$da){
				$da_fichier=kDocumentDir.$da['DA_CHEMIN'].$da['DA_FICHIER'];
				if(is_file($da_fichier)) $tab_fichier[]=array('type'=>'doc_acc','file'=>$da_fichier);

			}
	    	// storyboard
			$doc_imageur=$this->t_doc['DOC_ID_IMAGEUR'];
			if(!empty($doc_imageur)) {
				$sql = "select ID_MAT from t_mat WHERE MAT_ID_IMAGEUR = ".$db->Quote($doc_imageur);
				$id_mat = $db->GetOne($sql);
				if(!$id_mat) {
					$objImageur=new Imageur();
					$objImageur->t_imageur['ID_IMAGEUR']=$doc_imageur;
					$objImageur->getImageur();
					$tab_fichier[]=array('type'=>'storyboard','file'=>kStoryboardDir.$objImageur->getImageurPath());
					$objImageur->deleteImageur(false);
					unset($objImageur);
				}
			}
			// Appel méthode putFiles de l’objet trash
		    $myTrash->putFiles($tab_fichier);
			unset($myTrash);
	    }

		return $ok;
	}

	/** Supprime les séquences ainsi que les liens de ces séquences : matériel, valeur, lex, etc
	 * 	IN : array t_doc_seq de l'objet (chargé automatiquement si vide)
	 *  OUT : update BDD puis vidage tableau t_doc_seq
	 */
	function deleteSequences() {
		global $db;

		if (empty($this->t_doc_seq)) $this->getSequences();
		foreach ($this->t_doc_seq as $seq) {
			// VP 18/05/10 : suppression par appel méthode delete pour chaque séquence
			$mySeq=new Doc();
			$mySeq->t_doc['ID_DOC']=$seq['ID_DOC'];

			if(defined("gDecouperSequence") && gDecouperSequence){
				if(empty($mySeq->t_doc_mat)) $mySeq->getMats();
				// on cherche le matériel associé, puis on le delete avec purge du fichier
				foreach($mySeq->t_doc_mat as $idx=>$dmat){
					if(strpos($dmat['MAT']->t_mat['MAT_NOM'],$seq['ID_DOC'].'_vis') != false ){
						$dmat['MAT']->delete(true);
					}
				}
			}
			$mySeq->delete();
			unset($mySeq);

//	        if (!empty($seq['ID_DOC'])) {
//		        deleteSQLFromArray(array("t_doc","t_doc_lex","t_doc_val","t_doc_mat","t_panier_doc"),"ID_DOC",$seq['ID_DOC']);
//				$db->Execute("DELETE FROM t_doc_lex_precision WHERE ID_DOC_LEX IN (SELECT ID_DOC_LEX FROM t_doc_lex WHERE t_doc_lex.ID_DOC = '".($seq['ID_DOC'])."')");
//	        }
		}
		unset ($this->t_doc_seq);


	}

    //*** XML
    function xml_export($entete=0,$encodage=0,$prefix="") {
        $content="";
        $search = array(" ' ",'&','<','>');
        $replace = array("'",'&#38;','&#60;','&#62;');
		
        if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
		if(defined('useSolr') && useSolr && isset($this->solrObj)){
			$content.=$this->xml_export_solrObj($entete,$encodage,$prefix);
		}else{
			$content.=$prefix."<t_doc id_doc=\"".$this->t_doc['ID_DOC']."\">\n";

            // D'abord l'objet
			$list_class_vars = get_class_vars(get_class($this));
			foreach ($this->t_doc as $name => $value) {
				if(substr(strtoupper($value),0,5)=="<XML>" || strtoupper($name) == "DOC_TRANSCRIPT") 
					$content .= $prefix."\t<".strtoupper($name).">".str_replace(array(" ' ","&"),array("'","&amp;"),$value)."</".strtoupper($name).">\n";
				else
					$content .= $prefix."\t<".strtoupper($name).">".str_replace($search,$replace, $value)."</".strtoupper($name).">\n";
			}
				
			// $content .=$prefix. "\t<TITRE_PARENT>".str_replace($search,$replace, ereg_replace("\n|\r","|",$this->arrFather[0]['DOC_TITRE']))."</TITRE_PARENT>\n";
			$content .=$prefix. "\t<TITRE_PARENT>".str_replace($search,$replace, (isset($this->arrFather[0]['DOC_TITRE']))?$this->arrFather[0]['DOC_TITRE']:'')."</TITRE_PARENT>\n";

			$content .=$prefix. "\t<TYPE_DOC>".$this->type_doc."</TYPE_DOC>\n";
			//PC 02/10/12 : ajout libellé état archivage
			$content .=$prefix. "\t<ETAT_ARCH>".$this->etat_arch."</ETAT_ARCH>\n";

			//by ld 17/07/08 : retrait du chemin serveur
			$content .=$prefix. "\t<VIGNETTE>".$this->vignette."</VIGNETTE>\n";
			$content .= $prefix."\t<US_CREA>".$this->user_crea."</US_CREA>\n";
			$content .=$prefix. "\t<US_MODIF>".$this->user_modif."</US_MODIF>\n";


            if (!empty($this->t_doc_lex)) {
			// I.2. Propri?t?s relatives ? t_doc_lex
				$content.=$prefix."\t<t_doc_lex nb=\"".count($this->t_doc_lex)."\">\n";

            	// Constitution du tableau hiérarchique TYPE_DESC => ROLE => lexique
                foreach ($this->t_doc_lex as $idx => $lex) {
                        $arrRoles[$lex["ID_TYPE_DESC"]][$lex["DLEX_ID_ROLE"]][]=$lex;
                }
                   foreach ($arrRoles as $type_desc => $rol) {
                    $content .=$prefix. "\t\t<TYPE_DESC ID_TYPE_DESC=\"".$type_desc."\" >\n";
                        foreach ($rol as $role=>$lexs) {
                        $content .=$prefix. "\t\t\t<t_rol DLEX_ID_ROLE=\"".$role."\" >\n";
	                     foreach ($lexs as $idx=>$lex)	{
	                        $content .=$prefix. "\t\t\t\t<t_lex ID_DOC_LEX=\"".$lex['ID_LEX']."\" >\n";
	                        foreach ($lex as $fld=>$value) {
	                                if(substr($value,0,5)=="<XML>") $content .=$prefix. "\t\t\t\t\t<".$fld.">".str_replace(" ' ","'",$value)."</".$fld.">\n";
	                                else $content .=$prefix. "\t\t\t\t\t<".$fld.">".str_replace($search,$replace, $value)."</".$fld.">\n";
	                        }
	                        $content .=$prefix. "\t\t\t\t</t_lex>\n";
	                        }
	                       	$content .=$prefix."\t\t\t</t_rol>\n";
                        }
                     $content .= $prefix."\t\t</TYPE_DESC>\n";
                    }
				$content.=$prefix."\t</t_doc_lex>\n";
            }
            unset($arrRoles); //reset tableau
			
            if (!empty($this->t_pers_lex)) {
				$content.=$prefix."\t<t_pers_lex nb=\"".count($this->t_pers_lex)."\">\n";

            	// Constitution du tableau hiérarchique TYPE_DESC => ROLE => lexique
                foreach ($this->t_pers_lex as $idx => $lex) {
                        $arrRoles[$lex["ID_TYPE_DESC"]][$lex["DLEX_ID_ROLE"]][]=$lex;
                }
                  //echo "<PRE>";print_r($arrRoles);echo "</PRE>";
	                   foreach ($arrRoles as $type_desc => $rol) {
                    $content .= $prefix."\t\t<TYPE_DESC ID_TYPE_DESC=\"".$type_desc."\" >\n";
                        foreach ($rol as $role=>$lexs) {
                        $content .=$prefix. "\t\t\t<t_rol DLEX_ID_ROLE=\"".$role."\" >\n";
	                     foreach ($lexs as $idx=>$lex)	{
	                        $content .=$prefix. "\t\t\t\t<t_personne ID_PERS=\"".$lex['ID_PERS']."\" >\n";
	                        // Pour chaque propri?t? du terme

	                        foreach ($lex as $fld=>$value) {
	                                $content .=$prefix. "\t\t\t\t\t<".$fld.">".str_replace($search,$replace, $value)."</".$fld.">\n";
	                        }
	                        $content .=$prefix. "\t\t\t\t</t_personne>\n";
                        }
	                         $content .=$prefix."\t\t\t</t_rol>\n";
                        }

                     $content .=$prefix. "\t\t</TYPE_DESC>\n";
                    }
				$content.=$prefix."\t</t_pers_lex>\n";
            }
			
            if (!empty($this->t_doc_val)) {
				// I.3. Propri?t?s relatives ? t_doc_val
				$content.=$prefix."\t<t_doc_val nb=\"".count($this->t_doc_val)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_doc_val as $name => $value) {
                	//by ld 11/12/08 => on utilise ID_TYPE_VAL (type de l'asso) plutôt que VAL_ID_TYPE_VAL (type de la valeur).
                	//ceci pour afficher le cas où la même valeur de base sert dans plusieurs types d'asso
                	//par exemple CDR : LANG => langue dialogue (LANG) + langue soustitre (SUB)
                    $content .=$prefix. "\t\t<TYPE_VAL ID_TYPE_VAL=\"".$value['ID_TYPE_VAL']."\" >";
                    // Pour chaque valeur
                	// VP 23/11/09 : ajout VAL_CODE dans export
                	// VP 20/08/10 : ajout VAL_ID_GEN dans export

                        $content .=$prefix. "\t\t<t_val ID_VAL=\"".$value['ID_VAL']."\">\n";
                        $content .=$prefix. "\t\t\t<ID_VAL>".$value['ID_VAL']."</ID_VAL>\n";
                        $content .=$prefix. "\t\t\t<VALEUR>".str_replace($search,$replace, $value['VALEUR'])."</VALEUR>\n";
						$content .=$prefix. "\t\t\t<VAL_CODE>".str_replace($search,$replace, $value['VAL_CODE'])."</VAL_CODE>\n";
						$content .=$prefix. "\t\t\t<VAL_ID_GEN>".str_replace($search,$replace, $value['VAL_ID_GEN'])."</VAL_ID_GEN>\n";
                        $content .=$prefix. "\t\t</t_val>\n";

                    $content .=$prefix. "</TYPE_VAL>\n";
                }
				$content.=$prefix."\t</t_doc_val>\n";
            }
		}

		if (!empty($this->t_doc_cat))
		{
			$content.=$prefix."\t".'<t_doc_cat nb="'.count($this->t_doc_cat).'">'."\n";
			foreach($this->t_doc_cat as $cat)
			{
				$content.=$prefix."\t\t".'<t_categorie id_cat="'.$cat['ID_CAT'].'">'."\n";
				$content.=$prefix."\t\t\t".'<CAT_NOM>'.$cat['CAT_NOM'].'</CAT_NOM>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_CODE>'.$cat['CAT_CODE'].'</CAT_CODE>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_PATH>'.$cat['CAT_PATH'].'</CAT_PATH>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_DESC>'.$cat['CAT_DESC'].'</CAT_DESC>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_ID_GEN>'.$cat['CAT_ID_GEN'].'</CAT_ID_GEN>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_ID_TYPE_CAT>'.$cat['CAT_ID_TYPE_CAT'].'</CAT_ID_TYPE_CAT>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_ID_USAGER_CREA>'.$cat['CAT_ID_USAGER_CREA'].'</CAT_ID_USAGER_CREA>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_ID_USAGER_MOD>'.$cat['CAT_ID_USAGER_MOD'].'</CAT_ID_USAGER_MOD>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_DATE_CREA>'.$cat['CAT_DATE_CREA'].'</CAT_DATE_CREA>'."\n";
				$content.=$prefix."\t\t\t".'<CAT_DATE_MOD>'.$cat['CAT_DATE_MOD'].'</CAT_DATE_MOD>'."\n";
				$content.=$prefix."\t\t\t".'<ID_DOC_ACC>'.$cat['ID_DOC_ACC'].'</ID_DOC_ACC>'."\n";
				$content.=$prefix."\t\t\t".'<DA_FICHIER>'.$cat['DA_FICHIER'].'</DA_FICHIER>'."\n";
				$content.=$prefix."\t\t\t".'<DA_CHEMIN>'.$cat['DA_CHEMIN'].'</DA_CHEMIN>'."\n";
				$content.=$prefix."\t\t".'</t_categorie>'."\n";
			}
			$content.=$prefix."\t".'</t_doc_cat>'."\n";
		}

		if (!empty($this->t_doc_mat)) {
			// I.4. Propri?t?s relatives ? t_doc_mat
			$content.=$prefix."\t<t_doc_mat nb=\"".count($this->t_doc_mat)."\">\n";
				// Pour chaque type de valeur
				foreach ($this->t_doc_mat as $name => $value) {
					// VP 6/01/12 : encodage caractères interdits pour id_mat
				   $content .= $prefix."\t\t<DOC_MAT ID_MAT=\"".str_replace($search,$replace,$this->t_doc_mat[$name]["ID_MAT"])."\">\n";
					// Pour chaque valeur
					// VP 15/10/08 : ajout export tableau (pour t_doc_mat_val)
					foreach ($this->t_doc_mat[$name] as $val_name => $val_value) {
						if (is_object($val_value)) {$content.=$val_value->xml_export();}
						elseif (is_array($val_value)) {$content.= array2xml($val_value,$val_name);}
						else {
							$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
						}
					}
					$content .=$prefix. "\t\t</DOC_MAT>\n";
				}
			$content.=$prefix."\t</t_doc_mat>\n";
		}

            // I.5 Propri?t?s relatives ? t_image
		if (!empty($this->t_images)) {
			$content.=$prefix."\t<t_image nb=\"".count($this->t_images)."\">\n";
			// Pour chaque type de valeur
			foreach ($this->t_images as $name => $value) {
				$content .=$prefix. "\t\t<DOC_IMG ID_IMAGE=\"".$this->t_images[$name]["ID_IMAGE"]."\">";
				// Pour chaque valeur
				foreach ($this->t_images[$name] as $val_name => $val_value) {
					$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
				}
				$content .=$prefix. "\t\t</DOC_IMG>\n";
			}
			$content.=$prefix."\t</t_image>\n";
		}

        //I.6 Propriétés relatives aux rediffusions
		if (!empty($this->t_doc_redif)) {
			$content.=$prefix."\t<t_redif nb=\"".count($this->t_doc_redif)."\">\n";
			// Pour chaque type de valeur
			foreach ($this->t_doc_redif as $name => $value) {
				$content .= $prefix."\t\t<DOC_REDIF ID_REDIF=\"".$this->t_doc_redif[$name]["ID_REDIF"]."\">";
				// Pour chaque valeur
				foreach ($this->t_doc_redif[$name] as $val_name => $val_value) {
					$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
				}
				$content .= $prefix."\t\t</DOC_REDIF>\n";
			}
			$content.=$prefix."\t</t_redif>\n";
		}


		//I.7 Propriétés relatives aux droits
		if (!empty($this->t_doc_lex_droit)) {
			$content.=$prefix."\t<t_lex_droit nb=\"".count($this->t_doc_lex_droit)."\">\n";
			// Pour chaque type de valeur
			foreach ($this->t_doc_lex_droit as $name => $value) {
				$content .= $prefix."\t\t<DOC_LEX_DROIT ID_DOC_LEX=\"".$this->t_doc_lex_droit[$name]["ID_DOC_LEX"]."\">";
				// Pour chaque valeur
				foreach ($this->t_doc_lex_droit[$name] as $val_name => $val_value) {
					$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
				}
				$content .= $prefix."\t\t</DOC_LEX_DROIT>\n";
			}
            $content.=$prefix."\t</t_lex_droit>\n";
		}
		
		//I.8 Propriétés relatives aux droits préexistants
		if (!empty($this->t_doc_prex)) {
			$content.=$prefix."\t<t_prex nb=\"".count($this->t_doc_prex)."\">\n";
			// Pour chaque type de valeur
			foreach ($this->t_doc_prex as $name => $value) {
				$content .= $prefix."\t\t<DOC_PREX ID_DOC_PREX=\"".$this->t_doc_prex[$name]["ID_DOC_PREX"]."\">";
				// Pour chaque valeur
				foreach ($this->t_doc_prex[$name] as $val_name => $val_value) {
					if (!is_object($val_value))
						$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
					else {
						if (count($val_value->t_dp_val) > 0) {
							$arrvals = array();
							foreach ($val_value->t_dp_val as $num_val=>$val_val)
							$arrvals[$val_val['ID_TYPE_VAL']][] = $val_val['VALEUR'];
							foreach ($arrvals as $type_val=>$val_val)
							$content .=$prefix. "\t\t\t<".$type_val.">".str_replace($search,$replace, implode(",", $val_val))."</".$type_val.">\n";

				            $content.=$prefix."\t\t\t<t_dp_val nb=\"".count($val_value->t_dp_val)."\">\n";
				                // Pour chaque type de valeur
				                //update VG 04/10/10 : ajout de t_dp_val en cette version
				                foreach ($val_value->t_dp_val as $nameDPVal => $valueDPVal) {
				                    $content .=$prefix. "\t\t\t\t<TYPE_VAL ID_TYPE_VAL=\"".$valueDPVal['ID_TYPE_VAL']."\" >";
			                        $content .=$prefix. "\t\t\t\t<t_val ID_VAL=\"".$valueDPVal['ID_VAL']."\">\n";
			                        $content .=$prefix. "\t\t\t\t\t<ID_VAL>".$valueDPVal['ID_VAL']."</ID_VAL>\n";
			                        $content .=$prefix. "\t\t\t\t\t<VALEUR>".str_replace($search,$replace, $valueDPVal['VALEUR'])."</VALEUR>\n";
									$content .=$prefix. "\t\t\t\t\t<VAL_CODE>".str_replace($search,$replace, $valueDPVal['VAL_CODE'])."</VAL_CODE>\n";
									$content .=$prefix. "\t\t\t\t\t<VAL_ID_GEN>".str_replace($search,$replace, $valueDPVal['VAL_ID_GEN'])."</VAL_ID_GEN>\n";
									$content .=$prefix. "\t\t\t\t\t<DPV_NB>".str_replace($search,$replace, $valueDPVal['DPV_NB'])."</DPV_NB>\n";
			                        $content .=$prefix. "\t\t\t\t</t_val>\n";

				                    $content .=$prefix. "\t\t\t\t</TYPE_VAL>\n";
				                }
				            $content.=$prefix."\t\t\t</t_dp_val>\n";

						}
						//update VG 04/10/10 : ajout de t_dp_pers
						if(count($val_value->t_dp_pers) > 0) {
							$content .=$prefix. "\t\t\t<t_dp_pers nb=\"".count($val_value->t_dp_pers)."\">\n";
							foreach ($val_value->t_dp_pers as $pers) {
								$content.=$prefix. "\t\t\t\t<DP_PERS ID_PERS=\"".$pers['ID_PERS']."\">";
								foreach ($pers as $pers_val_name => $pers_val_value)
									$content .=$prefix. "\t\t\t\t\t<".$pers_val_name.">".str_replace($search,$replace, $pers_val_value)."</".$pers_val_name.">\n";
								$content.=$prefix. "\t\t\t\t</DP_PERS>";
							}
							$content .=$prefix. "\t\t\t</t_dp_pers>\n";
						}
					}
				}
				$content .= $prefix."\t\t</DOC_PREX>\n";
			}
            $content.=$prefix."\t</t_prex>\n";
		}

		//I.9 Propriétés relatives aux documents filles
		if (!empty($this->arrDocFilles)) {
			$content.=$prefix."\t<t_doc_filles nb=\"".count($this->arrDocFilles)."\">\n";
			foreach ($this->arrDocFilles as $name => $value) {
				$content .= $prefix."\t\t<t_doc ID_DOC=\"".$this->arrDocFilles[$name]["ID_DOC"]."\">";
				foreach ($this->arrDocFilles[$name] as $val_name => $val_value) {
					if (!is_object($val_value))
						$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
					elseif (count($val_value->t_dp_val) > 0) {
						$arrvals = array();
						foreach ($val_value->t_dp_val as $num_val=>$val_val)
						$arrvals[$val_val['ID_TYPE_VAL']][] = $val_val['VALEUR'];
						foreach ($arrvals as $type_val=>$val_val)
						$content .=$prefix. "\t\t\t<".$type_val.">".str_replace($search,$replace, implode(",", $val_val))."</".$type_val.">\n";
					}
				}
				$content .= $prefix."\t\t</t_doc>\n";
			}
            $content.=$prefix."\t</t_doc_filles>\n";
		}

		if (!empty($this->t_doc_seq)) {
            $content.=$prefix."\t<t_seq nb=\"".count($this->t_doc_seq)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_doc_seq as $name => $value) {
                    $content .= $prefix."\t\t<SEQ ID_DOC=\"".$this->t_doc_seq[$name]["ID_DOC"]."\">";
                    // Pour chaque valeur
                    foreach ($this->t_doc_seq[$name] as $val_name => $val_value) {
						//pas d'encodage des chevrons pour les champs contenant du XML
						if(substr($val_value,0,5)=="<XML>") $content .= $prefix."\t\t\t<".strtoupper($val_name).">".str_replace(array(" ' ","&"),array("'","&amp;"),$val_value)."</".strtoupper($val_name).">\n";
						else $content .= $prefix."\t\t\t<".strtoupper($val_name).">".str_replace($search,$replace,$val_value)."</".strtoupper($val_name).">\n";
                    }
                    $content .=$prefix. "\t\t</SEQ>\n";
                }
            $content.=$prefix."\t</t_seq>\n";
          }

          if (!empty($this->t_doc_acc)) {
            $content.=$prefix."\t<t_doc_acc nb=\"".count($this->t_doc_acc)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_doc_acc as $name => $value) {

					$content.=$prefix.$value->xml_export(0,0,chr(9).chr(9));
                }
            $content.=$prefix."\t</t_doc_acc>\n";
          }


          if (!empty($this->arrDocLiesSRC)) {
          	$content.=$prefix."\t<t_doc_lies_src nb=\"".count($this->arrDocLiesSRC)."\">\n";
                 foreach ($this->arrDocLiesSRC as $name => $value) {
                    $content .=$prefix."\t\t<t_doc ID_DOC=\"".$this->arrDocLiesSRC[$name]["ID_DOC"]."\">";
                    // Pour chaque valeur
                    foreach ($this->arrDocLiesSRC[$name] as $val_name => $val_value) {
						if(substr(strtoupper($val_value),0,5)=="<XML>" || strtoupper($val_name) == "DOC_TRANSCRIPT"){
							$content .= $prefix."\t<".$val_name.">".str_replace(array(" ' ","&"), array("'","&amp;"), $val_value)."</".$val_name.">\n";
						}
						else{
							$content .= $prefix."\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
						}

                    }
                    $content .=$prefix. "</t_doc>\n";
                }
          	$content.=$prefix."\t</t_doc_lies_src>\n";
          }

          if (!empty($this->arrDocLiesDST)) {
          	$content.=$prefix."\t<t_doc_lies_dst nb=\"".count($this->arrDocLiesDST)."\">\n";
                 foreach ($this->arrDocLiesDST as $name => $value) {
                    $content .= $prefix."\t\t<t_doc ID_DOC=\"".$this->arrDocLiesDST[$name]["ID_DOC"]."\">";
                    // Pour chaque valeur
                    foreach ($this->arrDocLiesDST[$name] as $val_name => $val_value) {
						if(substr(strtoupper($val_value),0,5)=="<XML>" || strtoupper($val_name) == "DOC_TRANSCRIPT"){
							$content .= $prefix."\t<".$val_name.">".str_replace(array(" ' ","&"), array("'","&amp;"), $val_value)."</".$val_name.">\n";
						}
						else{
							$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search , $replace, $val_value)."</".$val_name.">\n";
						}
                        
                    }
                    $content .=$prefix. "</t_doc>\n";
                }
          	$content.=$prefix."\t</t_doc_lies_dst>\n";
          }

		if (!empty($this->arrDocFreres)) {
			$content.=$prefix."\t<t_doc_freres nb=\"".count($this->arrDocFreres)."\">\n";
			foreach ($this->arrDocFreres as $name => $value) {
				$content .= $prefix."\t\t<t_doc ID_DOC=\"".$this->arrDocFreres[$name]["ID_DOC"]."\">";
				// Pour chaque valeur
				foreach ($this->arrDocFreres[$name] as $val_name => $val_value) {
					if(substr(strtoupper($val_value),0,5)=="<XML>" || strtoupper($val_name) == "DOC_TRANSCRIPT"){
						$content .= $prefix."\t<".$val_name.">".str_replace(array(" ' ","&"), array("'","&amp;"), $val_value)."</".$val_name.">\n";
					}
					else{
						$content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search , $replace, $val_value)."</".$val_name.">\n";
					}
				}
				$content .=$prefix. "</t_doc>\n";
			}
			$content.=$prefix."\t</t_doc_freres>\n";
		}
									

          if (!empty($this->t_doc_fest)) {
            $content.=$prefix."\t<t_doc_fest nb=\"".count($this->t_doc_fest)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_doc_fest as $name => $doc) {
					$content.=$prefix."\t\t<FEST>";
					foreach ($doc as $name => $v) {
                    	if (is_array($v))
                    	 $content .= "\t\t\t".array2xml($v,$name)."\n";
                    	elseif(substr($v,0,5)=="<XML>") $content .= $prefix."\t\t\t<".strtoupper($name).">".str_replace(array(" ' ","&"),array("'","&amp;"),$v)."</".strtoupper($name).">\n";
                    	 else
                   		 $content .= "\t\t\t<".strtoupper($name).">".str_replace($search,$replace, $v)."</".strtoupper($name).">\n";

                    }
                    $content.=$prefix."\t\t</FEST>";
                }
            $content.=$prefix."\t</t_doc_fest>\n";
          }

	    //I.10 Propriétés relatives aux droits
		if (!empty($this->t_doc_right)) {
			$content.=$prefix."\t<t_right nb=\"".count($this->t_doc_right)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_doc_right as $name => $value) {
                $content .= $prefix."\t\t<RIGHT ID_RIGHT=\"".$this->t_doc_right[$name]->t_right["ID_RIGHT"]."\">";
                // Pour chaque valeur
                foreach ($this->t_doc_right[$name]->t_right as $val_name => $val_value) {
                    $content .=$prefix. "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                }
                if (!empty($value->t_right_val)){
                	$content .= "\t\t\t<t_right_val>\n";
                	foreach ($value->t_right_val as $rval) {
                		$content.="\t\t\t\t<RIGHT_VAL ID_TYPE_VAL=\"".$rval['ID_TYPE_VAL']."\">\n";
                		foreach ($rval as $vkey=>$vval)
                			$content.="\t\t\t\t\t<$vkey>$vval</$vkey>\n";
                		$content.="\t\t\t\t</RIGHT_VAL>\n";
                	}
                	$content .= "\t\t\t</t_right_val>\n";
                }
                if (isset($value->t_usager)){
                	$content .=$prefix. "\t\t\t<USAGER_NAME>".$value->t_usager['US_PRENOM']." ".$value->t_usager['US_NOM']."</USAGER_NAME>\n";
                	$content .=$prefix. "\t\t\t<USAGER_ID>".$value->t_usager['ID_USAGER']."</USAGER_ID>\n";
                }
                $content .= $prefix."\t\t</RIGHT>\n";
            }
            $content.=$prefix."\t</t_right>\n";
		}
		//I.11 Propriétés relatives aux commentaires ou notation NB 24 03 2015
		if (!empty($this->t_doc_comment)) {
			$content.=$prefix."\t<t_doc_comment nb=\"".count($this->t_doc_comment)."\">\n";
			foreach ($this->t_doc_comment as $name => $value) {
				$content.=$prefix."\t\t<COMMENT>";
				foreach ($value as $name => $v) {
					if (is_array($v))
						$content .= "\t\t\t".array2xml($v,$name)."\n";
					else
						$content .= "\t\t\t<".strtoupper($name).">".str_replace($search,$replace, $v)."</".strtoupper($name).">\n";

				}
				if (isset($value['DC_ID_USAGER_CREA']) && isset($_SESSION ["USER"]["ID_USAGER"]))
					$content .= "\t\t\t<DC_OWNER>".($value['DC_ID_USAGER_CREA']==$_SESSION ["USER"]["ID_USAGER"])."</DC_OWNER>\n";
				$content.=$prefix."\t\t</COMMENT>";
			}
			$content.=$prefix."\t</t_doc_comment>\n";
		}

		if (!empty($this->t_doc_comment_val)) {
			$content.=$prefix."\t<t_doc_comment_val nb=\"".count($this->t_doc_comment_val)."\">\n";
			foreach ($this->t_doc_comment_val as $name => $value) {
				$content.=$prefix."\t\t<DOC_COMMENT_VAL>";
				foreach ($value as $name => $v) {
					if (is_array($v))
						$content .= "\t\t\t".array2xml($v,$name)."\n";
					else
						$content .= "\t\t\t<".strtoupper($name).">".str_replace($search,$replace, $v)."</".strtoupper($name).">\n";

				}
				$content.=$prefix."\t\t</DOC_COMMENT_VAL>";
			}
			$content.=$prefix."\t</t_doc_comment_val>\n";
		}

		if (!empty($this->t_doc_publication)) {
			$content.=$prefix."\t<t_doc_publication nb=\"".count($this->t_doc_publication)."\">\n";
			foreach ($this->t_doc_publication as $name => $value) {
				$content.=$prefix."\t\t<PUBLICATION>";
				foreach ($value as $name => $v) {
					if (is_array($v))
						$content .= "\t\t\t".array2xml($v,$name)."\n";
					else
						$content .= "\t\t\t<".strtoupper($name).">".str_replace($search,$replace, $v)."</".strtoupper($name).">\n";

				}
				$content.=$prefix."\t\t</PUBLICATION>";
			}
			$content.=$prefix."\t</t_doc_publication>\n";
		}
		
		$this->getVersions();
		if (!empty($this->arrVersions)) {
			$content.=$prefix . "\t<t_doc_version nb=\"" . count($this->arrVersions) . "\">\n";
			foreach ($this->arrVersions as $key => $tableau_values) {
				$content.=$prefix . "\t\t<VERSION>";
				foreach ($tableau_values as $name => $v) {
					if (is_array($v)) {
						$content .= "\t\t\t" . array2xml($v, $name) . "\n";
					} else {
						$content .= "\t\t\t<" . strtoupper($name) . ">" . str_replace($search, $replace, $v) . "</" . strtoupper($name) . ">\n";
					}
				}
				$content.=$prefix . "\t\t</VERSION>";
			}
			$content.=$prefix . "\t</t_doc_version>\n";
		}


		$content.=$prefix."</t_doc>\n";

        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }

	// MSVERIF - déplacement de la partie génération du xml depuis do_docSolr vers cette fonction xml_export_solr
	// je  ne suis pas franchement ravi de ce que ça donne, le début est imprimé intégralement depuis le doc solr passé en paramètre, tandis que les infos précises viennent de l'objet sur lequel cette fonction est appelée. ce n'est pas très intuitif ..
	// + cas du morelikethis qui est présenté séparément dans la reponse solr et qui doit donc etre passé comme un second paramètre
	private function  xml_export_solrObj($entete=0,$encodage=0,$prefix=""){
		
		$xml = ""; 
		$xml_l="";
		$xml_p="";
		$xml_v="";
		$xml_ct="";
		$langue_param = (isset($this->t_doc['ID_LANG']) && !empty($this->t_doc['ID_LANG']) && in_array($this->t_doc['ID_LANG'],$_SESSION['arrLangues']))?$this->t_doc['ID_LANG']:$_SESSION['langue'];
		
		if ($this->solrObj != null ) {
			if(!isset($this->t_doc['ID_DOC']) || empty($this->t_doc['ID_DOC'])){
				$id_doc = $myDoc->t_doc['ID_DOC'];
			}else if(defined('gNomChampIdSolr') && $docSolr->offsetExists(gNomChampIdSolr)){
				$id_doc = $this->solrObj->offsetGet($gNomChampIdSolr);
			}else{
				$id_doc = $this->solrObj->offsetGet('id_doc'); 
			}
			$xml.= "\t<t_doc id_doc=\"$id_doc\" type=\"solr\">\n";
			foreach ($this->solrObj->getPropertyNames() as $key)
			{
				$value = $this->solrObj->offsetGet($key);
				if (!is_array($value))
				{
					$key=trim($key);
					if(preg_match('/_'.strtolower($langue_param).'$/',$key) && in_array(strtoupper(str_replace('_'.strtolower($langue_param),'',$key)),$this->arrFieldsToSaveWithIdLang )){
						$key = strtoupper(str_replace('_'.strtolower($_SESSION['langue']),'',$key));
					}
					if(substr(strtoupper($value),0,5)=="<XML>" || strtoupper($key) == "DOC_TRANSCRIPT")
						$xml.="\t\t".'<'.strtoupper($key).'>'.str_replace(array(" ' ","&"),array("'","&amp;"), $value).'</'.strtoupper($key).'>'."\n";
					else
						$xml.="\t\t".'<'.strtoupper($key).'>'.str_replace($search,$replace, $value).'</'.strtoupper($key).'>'."\n";
					//$xml.="\t\t".'<'.strtoupper($key).'>'.str_replace($search,$replace, $response->getResponse()->response->docs[0]->offsetGet($key)).'</'.strtoupper($key).'>'."\n";
				}else{
					$key=trim($key);
					if(preg_match('/_'.strtolower($langue_param).'$/',$key) && in_array(strtoupper(str_replace('_'.strtolower($langue_param),'',$key)),$this->arrFieldsToSaveWithIdLang )){
						$key = strtoupper(str_replace('_'.strtolower($langue_param),'',$key));
					}
					$key_split=explode('_',$key);
					switch($key_split[0]){
						case 'l':
							$xml_l.="\t\t\t".'<TYPE_DESC ID_TYPE_DESC="'.strtoupper($key_split[1]).'" >'."\n";
							$xml_l.="\t\t\t\t".'<t_rol KEY_SOLR="'.strtoupper($key).'" DLEX_ID_ROLE="'.strtoupper($key_split[2]).'" >'."\n";
							
							foreach ($value as $valeur) {
								$xml_l.="\t\t\t\t\t".'<t_lex>'."\n";
								// $xml_l.="\t\t\t\t\t\t".'<KEY_SOLR>'.strtoupper($key).'</KEY_SOLR>'."\n";
								$xml_l.="\t\t\t\t\t\t".'<ID_TYPE_DESC>'.strtoupper($key_split[1]).'</ID_TYPE_DESC>'."\n";
								
								if (isset($key_split[2]) && !empty($key_split[2]))
									$xml_l.="\t\t\t\t\t\t".'<DLEX_ID_ROLE>'.strtoupper($key_split[2]).'</DLEX_ID_ROLE>'."\n";
								
								if (isset($key_split[3]) && !empty($key_split[3]))
									$xml_l.="\t\t\t\t\t\t".'<LEX_ID_TYPE_LEX>'.strtoupper($key_split[3]).'</LEX_ID_TYPE_LEX>'."\n";
								
								$xml_l.="\t\t\t\t\t\t".'<LEX_TERME>'.str_replace($search,$replace,$valeur).'</LEX_TERME>'."\n";
								$xml_l.="\t\t\t\t\t".'</t_lex>'."\n";
							}
							
							$xml_l.="\t\t\t\t".'</t_rol>'."\n";
							$xml_l.="\t\t\t".'</TYPE_DESC>'."\n";
							break;
							
						case 'p':
							$xml_p.="\t\t\t".'<TYPE_DESC ID_TYPE_DESC="'.strtoupper($key_split[1]).'" >'."\n";
							$xml_p.="\t\t\t\t".'<t_rol  KEY_SOLR="'.strtoupper($key).'" DLEX_ID_ROLE="'.strtoupper($key_split[2]).'" >'."\n";
							
							foreach ($value as $valeur) {
								$xml_p.="\t\t\t\t\t".'<t_personne>'."\n";
								// $xml_p.="\t\t\t\t\t".'<KEY_SOLR>'.strtoupper($key).'</KEY_SOLR>'."\n";
								
								$xml_p.="\t\t\t\t\t\t".'<ID_TYPE_DESC>'.strtoupper($key_split[1]).'</ID_TYPE_DESC>'."\n";
								
								if (isset($key_split[2]) && !empty($key_split[2]))
									$xml_p.="\t\t\t\t\t\t".'<DLEX_ID_ROLE>'.strtoupper($key_split[2]).'</DLEX_ID_ROLE>'."\n";
								
								if (isset($key_split[3]) && !empty($key_split[3]))
									$xml_p.="\t\t\t\t\t\t".'<LEX_ID_TYPE_LEX>'.strtoupper($key_split[3]).'</LEX_ID_TYPE_LEX>'."\n";
								$xml_p.="\t\t\t\t\t\t".'<PERS_TERME>'.str_replace($search,$replace,$valeur).'</PERS_TERME>'."\n";
								
								$xml_p.="\t\t\t\t\t".'</t_personne>'."\n";
							}
							
							$xml_p.="\t\t\t\t".'</t_rol>'."\n";
							$xml_p.="\t\t\t".'</TYPE_DESC>'."\n";
							break;
							
						case 'v':
							$xml_v.="\t\t\t".'<TYPE_VAL KEY_SOLR="'.strtoupper($key).'" ID_TYPE_VAL="'.strtoupper($key_split[1]).'" >'."\n";
							foreach ($value as $valeur) {
								$xml_v.="\t\t\t\t".'<t_val>'."\n";
								$xml_v.="\t\t\t\t\t".'<KEY_SOLR>'.strtoupper($key).'</KEY_SOLR>'."\n";
								$xml_v.="\t\t\t\t\t".'<VALEUR>'.str_replace($search,$replace,$valeur).'</VALEUR>'."\n";
								$xml_v.="\t\t\t\t".'</t_val>'."\n";
							}
							$xml_v.="\t\t\t".'</TYPE_VAL>'."\n";
							break;
							
						case 'ct':
							foreach ($value as $valeur) {
								$xml_ct.="\t\t\t\t".'<t_categorie>'."\n";
								//$xml_ct.="\t\t\t\t\t".'<KEY_SOLR>'.strtoupper($key).'</KEY_SOLR>'."\n";
								$xml_ct.="\t\t\t\t\t".'<CAT_ID_TYPE_CAT>'.strtoupper($key_split[1]).'</CAT_ID_TYPE_CAT>'."\n";
								$xml_ct.="\t\t\t\t\t".'<CAT_NOM>'.str_replace($search,$replace,$valeur).'</CAT_NOM>'."\n";
								$xml_ct.="\t\t\t\t".'</t_categorie>'."\n";
							}
							break;


						default:
							foreach ($value as $valeur) {
								$xml.="\t\t".'<'.strtoupper($key).'>'.str_replace($search,$replace, $valeur).'</'.strtoupper($key).'>'."\n";
							}
							break;
					}
					
				}
			}
			
			if($xml_l){
				$xml.="\t\t".'<t_doc_lex>'."\n";
				$xml.=$xml_l;
				$xml.="\t\t".'</t_doc_lex>'."\n";
			}
			if($xml_p){
				$xml.="\t\t".'<t_pers_lex>'."\n";
				$xml.=$xml_p;
				$xml.="\t\t".'</t_pers_lex>'."\n";
			}
			if($xml_v){
				$xml.="\t\t".'<t_doc_val>'."\n";
				$xml.=$xml_v;
				$xml.="\t\t".'</t_doc_val>'."\n";
			}
			if($xml_ct){
				$xml.="\t\t".'<t_doc_cat>'."\n";
				$xml.=$xml_ct;
				$xml.="\t\t".'</t_doc_cat>'."\n";
			}
		}
		
		// more_like_this
		if(isset($this->moreLikeThis)){
			$xml.="\t\t".'<more_like_this>'."\n";
			$nb_mlt=0;
			if(!empty($this->moreLikeThis->offsetGet($id_doc.'_'.$langue_param)->docs)){
				foreach ($this->moreLikeThis->offsetGet($id_doc.'_'.$langue_param)->docs as $doc)
				{
					if ($doc->offsetGet('id_lang')==$langue_param)
					{
						$xml.="\t\t\t".'<doc>'."\n";
						$xml.="\t\t\t\t".'<ID_DOC>'.$doc->offsetGet('id_doc').'</ID_DOC>'."\n";
						$xml.="\t\t\t\t".'<DOC_TITRE>'.str_replace($search,$replace,$doc->offsetGet('doc_titre')).'</DOC_TITRE>'."\n";
						$xml.="\t\t\t".'</doc>'."\n";
						$nb_mlt++;
					}
					
					if ($nb_mlt>=5)
						break;
				}
			}
			$xml.="\t\t".'</more_like_this>'."\n";
		}
			
		$xml .= "\t\t<US_CREA>".$this->user_crea."</US_CREA>\n";
		$xml .= "\t\t<US_MODIF>".$this->user_modif."</US_MODIF>\n";
		
		// on ne referme volontairement pas le <t_doc> ici, car la suite de l'export est traité dans la fonction xml_export généraliste.
		return $xml;
	}
	

	/** Export XML optimisé
	 *  IN : requete SQL, page, nombre de documents, modes d'export, valeurs à rechercher, valeurs de remplacement
	 *  OUT : chemin fichier XML au format UTF8.
	 */
	// function getXMLList($sql,$page=1,$nb="all",$mode=array("val"=>1,"lex"=>1,"pers_lex"=>1,"docmat"=>1,"docmat_val"=>1,"mat_val"=>1,"mat_pers"=>1,"cat"=>1,"img"=>1,"seq"=>1,"doc_acc"=>1,"doc_lies_src"=>1,"doc_lies_dst"=>1,"redif"=>1,"lex_droit"=>1,"doc_fest"=>1),$search_sup=array(),$replace_sup=array()){
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){
		global $db;

		if($mode == 'pdoc'){
			$mode=array("val"=>1,"lex"=>1,"pers_lex"=>1,"docmat"=>1,"docmat_val"=>1,"mat_val"=>1,"mat_pers"=>1,"cat"=>1,"img"=>1,"seq"=>1,"doc_acc"=>1,"doc_lies_src"=>1,"doc_lies_dst"=>1,"redif"=>1,"lex_droit"=>1,"doc_fest"=>1,"pdoc"=>1);
		}else if($mode == null){
			$mode=array("val"=>1,"lex"=>1,"pers_lex"=>1,"docmat"=>1,"docmat_val"=>1,"mat_val"=>1,"mat_pers"=>1,"cat"=>1,"img"=>1,"seq"=>1,"doc_acc"=>1,"doc_lies_src"=>1,"doc_lies_dst"=>1,"redif"=>1,"lex_droit"=>1,"doc_fest"=>1);
		}



		// définition des caractères à remplacer,
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");

		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}

		// definition du fichier de sortie
		$xml_file_path = kServerTempDir."/export_opsis_doc_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
		if (defined("useSinequa") && useSinequa && $_SESSION["useMySQL"]!="1") {
			// MS - 07.09.15 - recopie du code d'export.php propre à sinequa,
			// Non testé, si besoin, il faut faire fonctionner ce code (+ vérifier récupération de la requete sinequa depuis la session dans la class_export, de ce que j'en vois elle est bien stockée au meme endroit que la req SQL )
			//Mode sinequa && recherche doc --> utilisation sinequa
			require_once(libDir."sinequa/fonctions.inc"); //chargement libs
			require_once(libDir."sinequa/Intuition.inc");

			if (isset($nb_offset) && isset($nb) && $nb != 'all') {
				$limit=" SKIP $nb_offset COUNT $nb"; //si limites, réécriture à la syntaxe Sinequa
				$page_size=$nb;
			}else $page_size=500000;
			$iSession=new iSession;
			// VP (23/10/08) : changement max_answers_count et connexion par méthode connect
			// VP (29/04/09) : ajout paramètre page_size
			$prms= array ('host' => sinequa_host,
						  'port' => sinequa_port,
						  'read_only' => 1,
						  'charset' => in_UTF8,
						  'page_size' => $page_size,
						  'max_answers_count' => 500000,
						  'default-language' => $_SESSION['langue']
						  );

			//$link = $iSession->in_connect($prms);
			$link = $iSession->connect($prms);
			$iQuery=$iSession->in_query($sql.$limit);
			$rows=$iQuery->the_available_tuples_count;
			for($i=0;$i<$rows;$i++) $result[$i]=$iQuery->in_fetch_array();
			if(isset($iQuery))$iQuery->close();
			if(isset($iSession))$iSession->close();
		}else if($type=='doc' && defined("useBasis") && useBasis && isset($_SESSION['DB'])){
			// MS - 07.09.15 - recopie du code d'export.php propre à basis,
			// Non testé, si besoin, il faut faire fonctionner ce code (+ vérifier récupération de la requete  session dans la class_export)
			if($_GET['sql']== "recherche_DOC"){
				$result = $_SESSION["recherche_DOC"]['data'][$_SESSION['DB']];
			}else {
				$result=$db->GetArray($sql);
			}
		}else if(!is_string($sql) && get_class($sql)=="SolrQuery" && defined("useSolr") && useSolr){

			$tab_options = unserialize(kSolrOptions);
			// MS - 13/05/14 instauration d'un timeout solr minimum pour les fonctions d'export
			if(!isset($tab_options['timeout']) || intval($tab_options['timeout'])<300){
				$tab_options['timeout'] = "300";
			}


			if (!empty($_SESSION['DB']))
			{
				$tab_options['path']=$_SESSION['DB'];
			}

			$solr_client=new SolrClient($tab_options);

			if (isset($nb) && !empty($nb) && $nb!="all")
			{
				$nb_rows=intval($nb);
			}
			else
				$nb_rows=50000;



			// MS - on ne gère plus le calcul des pages ici, cela posait probleme pour le découpage en chunk d'une requete d'export d'un résultat paginé (si je demande la 2e page de 500 éléments, j'ai besoin de pouvoir faire la découpe dans la class_export, et je dois donc m'affranchir des pages)

			$sql->setStart($nb_offset);
			if (isset($nb_rows))
				$sql->setRows($nb_rows);

			// récupération du tri actif sur solr pr le transposer en pgsql
			$sort = $sql->getSortFields() ;
			if(!empty($sort)){
				$order = "";
				foreach($sort as $field){
					// on ne peut prendre en compte que des éléments triable directement sur la table t_doc
					if(strpos($field,'id_doc')===0 || strpos($field,'doc')===0){
						if($order == ''){
							$order.='order by ';
						}else{
							$order.=',';
						}
						$order .= $field;
					}
				}
			}

			$result_solr=$solr_client->query($sql);
			$critere ="";
			$limit ="";
			$result=array();
			if(empty($result_solr->getResponse()->response->docs)){
				trace("export solr : 0 résultats -> arrete l'export");
				flock($xml_file, LOCK_UN);
				fclose($xml_file);
				unlink($xml_file_path);
				return false ;
			}
			foreach ($result_solr->getResponse()->response->docs as $doc)
			{
				// $result[] = array('ID_DOC'=>$doc->id_doc,'ID_LANG'=>$doc->id_lang);
				$result[$doc->id_lang][] = $doc->id_doc;
			}

			if(!empty($result)){
				$critere = "WHERE ";
				$idx = 0 ;
				foreach($result as  $k=>$v){
					if($idx != 0){
						$critere.=" AND ";
					}
					$critere .= "( t1.ID_DOC in (".implode($v,',').") and t1.ID_LANG=".$db->Quote($k).")";
					$idx ++;
				}
			}
			// MS on ajoute le tri au critere pgsql
			$critere .= $order ;

		}else{

			$critere = "" ;
			if(isset($sql) && !empty($sql)){
				$parsed = parseSimpleSQL($sql);
				foreach($parsed as $part){
					if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
						$critere .=$part['segment'] ;
					}
				}
			}

			if(strpos(strtolower($critere),'order by') === false){
				$critere.=" order by t1.id_doc ";
			}

			$limit="";
			if ($nb!="all"){
				$limit = $db->limit($nb_offset,$nb);
			}
		}

		if(isset($mode['pdoc']) && $mode['pdoc'] ==1){
			$sql_doc = str_replace(array("FROM"),array(" , ".$db->Concat('t2.US_NOM',"' '",'t2.US_PRENOM')." as US_CREA, ".$db->Concat('t3.US_NOM',"' '",'t3.US_PRENOM')." as US_MODIF FROM "),substr($sql,0,strpos($sql,"WHERE")));
			$sql_doc.="LEFT OUTER JOIN t_usager t2 ON t1.DOC_ID_USAGER_CREA= t2.ID_USAGER LEFT OUTER JOIN t_usager t3 ON t1.DOC_ID_USAGER_MODIF = t3.ID_USAGER ".$critere.$limit;

		}else{
			// sql récupération champs doc
			$sql_doc = "SELECT t1.*, ".$db->Concat('t2.US_NOM',"' '",'t2.US_PRENOM')." as US_CREA, ".$db->Concat('t3.US_NOM',"' '",'t3.US_PRENOM')." as US_MODIF, i.IM_CHEMIN, i.IM_FICHIER, da.DA_CHEMIN, da.DA_FICHIER
			FROM t_doc t1
			LEFT OUTER JOIN t_usager t2 ON t1.DOC_ID_USAGER_CREA= t2.ID_USAGER
			LEFT OUTER JOIN t_usager t3 ON t1.DOC_ID_USAGER_MODIF = t3.ID_USAGER
            LEFT OUTER JOIN t_image i ON t1.DOC_ID_IMAGE = i.ID_IMAGE and i.ID_LANG=t1.ID_LANG
            LEFT OUTER JOIN t_doc_acc da ON t1.DOC_ID_IMAGE = da.ID_DOC_ACC and da.ID_LANG=t1.ID_LANG ".$critere.$limit;
		}

		//trace("sql_doc".$sql_doc);

		$results_doc = $db->Execute($sql_doc);

		$arr_ids =  array();
		// MS - Ajout sécurité 
		if(is_object($results_doc)){
			while($row = $results_doc->FetchRow()){
				$arr_ids[] = $row['ID_DOC'];
			}
		}else{
			trace("Doc::getXMLList - WARNING alimentation results_doc a échoué. sql : $sql");
		}
		$ids = implode(',',$arr_ids);
		if($ids == ''){
			flock($xml_file, LOCK_UN);
			fclose($xml_file);
			unlink($xml_file_path);
			return false;
		}


		if($mode['lex']){
            $sql_lex = "select distinct t_doc_lex.*,lex.*, t_role.role,syn.LEX_TERME as SYNO from t_doc_lex
                inner join t_lexique lex ON lex.ID_LEX=t_doc_lex.ID_LEX
                left join t_role ON t_doc_lex.DLEX_ID_ROLE=t_role.id_role and t_role.ID_LANG=".$db->Quote($_SESSION['langue'])."
                left join t_lexique syn ON (lex.LEX_ID_SYN=syn.ID_LEX AND syn.ID_LANG=".$db->Quote($_SESSION['langue']).")
                where lex.ID_LANG=".$db->Quote($_SESSION['langue'])." and t_doc_lex.ID_DOC in ($ids) ORDER BY  t_doc_lex.ID_DOC ASC, t_doc_lex.id_doc_lex ASC";
			$results_lex = $db->Execute($sql_lex);
			unset($sql_lex);
		}

		if($mode['pers_lex']){
			// $sql_pers_lex = "select distinct t_doc_lex.*,t_personne.* from t_doc_lex , t_personne
				// where t_personne.ID_PERS=t_doc_lex.ID_PERS and t_personne.ID_LANG=".$db->Quote($_SESSION['langue'])." and t_doc_lex.ID_DOC in ($ids) ORDER BY  t_doc_lex.ID_DOC ASC";

			$sql_pers_lex = "SELECT t_doc_lex.*, t_personne.*, t_doc_acc.DA_FICHIER, t_doc_acc.DA_CHEMIN, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE";
            $sql_pers_lex.= " FROM t_doc_lex INNER JOIN t_personne ON t_doc_lex.ID_PERS=t_personne.ID_PERS";
            $sql_pers_lex.= " LEFT OUTER JOIN t_doc_acc ON (t_personne.PERS_PHOTO=t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG=".$db->Quote($_SESSION['langue']).") ";
            $sql_pers_lex.= " LEFT OUTER JOIN t_doc_lex_precision ON (t_doc_lex_precision.ID_DOC_LEX=t_doc_lex.ID_DOC_LEX AND t_doc_lex_precision.ID_LANG=".$db->Quote($_SESSION['langue']).") ";
            $sql_pers_lex.= " LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($_SESSION['langue']).") ";
            $sql_pers_lex.= " WHERE t_doc_lex.ID_LEX=0 AND t_doc_lex.ID_DOC in ($ids) AND t_personne.ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY  t_doc_lex.ID_DOC ASC, t_doc_lex.id_doc_lex ASC";
			$results_pers_lex = $db->Execute($sql_pers_lex);

			//trace("class_doc getXmlList - sql_pers_lex : ".$sql_pers_lex);

			unset($sql_pers_lex);
		}

		if($mode['val']){
			// sql récupération t_doc_val
			$sql_val = "select distinct t_val.ID_VAL, t_val.VALEUR ,t_val.VAL_CODE,t_val.VAL_ID_GEN, t_doc_val.ID_DOC, t_doc_val.ID_TYPE_VAL
				from t_doc_val
				inner join t_val ON  t_doc_val.ID_VAL=t_val.ID_VAL
				where t_val.ID_LANG=".$db->Quote($_SESSION['langue'])." AND t_doc_val.ID_DOC in ($ids) ORDER BY  t_doc_val.ID_DOC ASC";
			$results_val = $db->Execute($sql_val);
			unset($sql_val);
		}

		if($mode['docmat']){
			$sql_docmat="SELECT distinct t_doc_mat.*, t_mat.*, t_lieu.* from t_doc_mat
						inner join t_mat ON t_mat.ID_MAT=t_doc_mat.ID_MAT
						left outer join t_lieu on lieu=mat_lieu
						where t_doc_mat.ID_DOC in ($ids) ORDER BY  t_doc_mat.ID_DOC ASC";
			$results_docmat = $db->Execute($sql_docmat);
			unset($sql_docmat);
		}
		if($mode['docmat_val']){
			$sql_docmat_val="SELECT distinct t_doc_mat_val.* ,t_val.*
						from t_doc_mat
						inner join t_doc_mat_val on t_doc_mat.id_doc_mat = t_doc_mat_val.id_doc_mat
						inner join t_val on t_doc_mat_val.ID_VAL=t_val.ID_VAL AND ID_LANG=".$db->Quote($_SESSION['langue'])."
						where t_doc_mat.ID_DOC in ($ids) ORDER BY  t_doc_mat_val.ID_DOC_MAT ASC";
			$results_docmat_val = $db->Execute($sql_docmat_val);
			unset($sql_docmat_val);
		}
		if($mode['mat_val']){
			$sql_mat_val="SELECT distinct t_mat_val.*, t_val.*
						from t_doc_mat
						inner join t_mat_val on t_doc_mat.id_mat = t_mat_val.id_mat
						LEFT JOIN t_val on t_mat_val.ID_VAL=t_val.ID_VAL AND ID_LANG=".$db->Quote($_SESSION['langue'])."
						where t_doc_mat.ID_DOC in ($ids) ORDER BY  t_mat_val.ID_MAT ASC";
			$results_mat_val = $db->Execute($sql_mat_val);
			unset($sql_mat_val);
		}
		if($mode['mat_pers']){
			$sql_mat_pers="SELECT distinct t_mat_pers.*, t_personne.*
			from t_doc_mat
			inner join t_mat_pers on t_doc_mat.id_mat = t_mat_pers.id_mat
			LEFT JOIN t_personne on t_mat_pers.ID_PERS=t_personne.ID_PERS AND ID_LANG=".$db->Quote($_SESSION['langue'])."
			where t_doc_mat.ID_DOC in ($ids) ORDER BY  t_mat_pers.ID_MAT ASC";
			$results_mat_pers = $db->Execute($sql_mat_pers);
			unset($sql_mat_pers);
		}

		if($mode['cat']){
			$sql_cat = "SELECT distinct t_categorie.*, t_doc_acc.ID_DOC_ACC, t_doc_acc.DA_FICHIER, t_doc_acc.DA_CHEMIN, t_doc_cat.ID_DOC FROM t_doc_cat
				LEFT OUTER JOIN t_categorie ON t_doc_cat.ID_CAT=t_categorie.ID_CAT
				LEFT OUTER JOIN t_doc_acc ON t_categorie.ID_CAT=t_doc_acc.ID_CAT
				where t_categorie.ID_LANG=".$db->Quote($_SESSION['langue'])." and t_doc_cat.ID_DOC in ($ids) ORDER BY t_doc_cat.ID_DOC ASC";
			$results_cat = $db->Execute($sql_cat);
			unset($sql_cat);
		}

		if($mode['img']){
			$sql_img = 	"SELECT distinct t_image.*, t_doc.ID_DOC from t_doc
						INNER JOIN t_image ON t_image.ID_IMAGEUR = t_doc.DOC_ID_IMAGEUR
						where t_image.ID_LANG=".$db->Quote($_SESSION['langue'])."
						and t_doc.ID_DOC in ($ids)
						and t_doc.DOC_ID_IMAGEUR IS NOT NULL
						and t_image.IM_TC >= t_doc.DOC_TCIN
						and t_image.IM_TC <= t_doc.DOC_TCOUT
						UNION
						SELECT distinct t_image.*, t_doc.ID_DOC from t_doc
						LEFT OUTER JOIN t_doc_mat ON t_doc.ID_DOC = t_doc_mat.ID_DOC
						LEFT OUTER JOIN t_mat ON t_doc_mat.ID_MAT = t_mat.ID_MAT
						LEFT OUTER JOIN t_image ON t_image.ID_IMAGEUR=t_mat.MAT_ID_IMAGEUR where
						t_image.ID_LANG=".$db->Quote($_SESSION['langue'])."
						and t_doc.ID_DOC in ($ids)
						and t_doc.DOC_ID_IMAGEUR IS NULL
						and t_image.IM_TC >= t_doc_mat.DMAT_TCIN
						and t_image.IM_TC <= t_doc_mat.DMAT_TCOUT
						UNION
						SELECT distinct t_image.*, t_doc.ID_DOC from t_doc
						LEFT OUTER JOIN t_image ON t_image.ID_IMAGE=t_doc.DOC_ID_IMAGE
						where t_image.ID_LANG=".$db->Quote($_SESSION['langue'])."
						and t_doc.ID_DOC in ($ids)
						ORDER BY ID_DOC ASC ";
			$results_img = $db->Execute($sql_img);
			//trace($sql_img);

			unset($sql_img);
		}

		if($mode['seq']){
			$sql_seq="SELECT distinct T1.*, TD.TYPE_DOC, t_image.IM_CHEMIN, T2.ID_DOC
			from t_doc T1
			inner join t_doc T2 ON T1.DOC_ID_GEN=T2.ID_DOC
			left OUTER join t_image on (T1.DOC_ID_IMAGE=t_image.ID_IMAGE  and t_image.ID_LANG=".$db->Quote($_SESSION['langue']).")
			left outer join t_type_doc TD on (TD.ID_TYPE_DOC = T1.DOC_ID_TYPE_DOC and TD.ID_LANG=".$db->Quote($_SESSION['langue']).")
			where T2.ID_DOC in ($ids) and T1.ID_LANG=".$db->Quote($_SESSION['langue'])." order by T2.ID_DOC ASC, T1.ID_DOC ASC";
			$results_seq = $db->Execute($sql_seq);
			unset($sql_seq);
		}

		if($mode['doc_acc']){
			$sql_doc_acc="SELECT t_doc_acc.* from t_doc_acc
			WHERE t_doc_acc.ID_LANG=".$db->Quote($_SESSION['langue'])."
			AND t_doc_acc.ID_DOC in ($ids) order by ID_DOC ASC";
			$results_doc_acc = $db->Execute($sql_doc_acc);
			unset($sql_doc_acc);
		}

		if($mode['doc_acc']){
			$sql_docacc_val ="SELECT t_doc_acc_val.ID_DOC_ACC, t_val.* from t_doc_acc
			LEFT OUTER JOIN t_doc_acc_val on (t_doc_acc.ID_DOC_ACC=t_doc_acc_val.ID_DOC_ACC and t_doc_acc.ID_LANG=".$db->Quote($_SESSION['langue']).")
			LEFT OUTER JOIN t_val on t_doc_acc_val.ID_VAL=t_val.ID_VAL
			where t_doc_acc.ID_DOC in ($ids)
			and t_val.ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY ID_DOC_ACC ASC";
			$results_docacc_val = $db->Execute($sql_docacc_val);
			unset($sql_docacc_val);
		}

		if($mode['doc_lies_src']){
			$sql_lies_src = "select doc.*,td.TYPE_DOC,dl.* from t_doc doc,t_doc_lien dl, t_type_doc td
			where td.id_type_doc=doc.doc_id_type_doc and dl.ID_DOC_DST=doc.id_doc and dl.ID_DOC_SRC in ($ids)
			and doc.ID_LANG=".$db->Quote($_SESSION['langue'])." and td.ID_LANG=".$db->Quote($_SESSION['langue'])." order by dl.dd_ordre";
			$results_lies_src = $db->Execute($sql_lies_src);
			//trace(" sql_lies_src :".$sql_lies_src);
			unset($sql_lies_src);
		}

		if($mode['doc_lies_dst']){
			/*$sql_lies_dst = "select doc.*,td.TYPE_DOC,dl.* from t_doc doc,t_doc_lien dl, t_type_doc td
			where td.id_type_doc=doc.doc_id_type_doc and dl.ID_DOC_SRC=doc.id_doc and dl.ID_DOC_DST in ($ids)
			and doc.ID_LANG=".$db->Quote($_SESSION['langue'])." and td.ID_LANG=".$db->Quote($_SESSION['langue'])." order by dl.dd_ordre";*/
			$sql_lies_dst = "select doc.*,td.TYPE_DOC,dl.* from t_doc doc
			LEFT JOIN t_doc_lien dl ON dl.ID_DOC_SRC=doc.id_doc
			LEFT JOIN t_type_doc td ON td.id_type_doc = doc.doc_id_type_doc AND td.ID_LANG=doc.ID_LANG
			where dl.ID_DOC_DST in ($ids)
			and doc.ID_LANG=".$db->Quote($_SESSION['langue'])." order by dl.dd_ordre";
			$results_lies_dst = $db->Execute($sql_lies_dst);
			//trace(" sql_lies_dst :".$sql_lies_dst);
			unset($sql_lies_dst);
		}

		if($mode['redif']){
			$sql_redif="SELECT * FROM t_doc_redif where ID_DOC in ($ids) ORDER BY ID_DOC";
			$results_redif = $db->Execute($sql_redif);
			unset($sql_redif);
		}

		if($mode['lex_droit']){
			$sql_lex_droit = "SELECT t_doc_lex_droit.*, t_doc_lex.*, t_personne.PERS_ID_TYPE_LEX, ".$db->Concat('t_personne.PERS_NOM',"' '",'t_personne.PERS_PRENOM')." as PERS_NOM, t_personne.PERS_PARTICULE, t_doc_acc.DA_FICHIER, t_doc_acc.DA_CHEMIN, t_doc_lex_precision.LEX_PRECISION, t_role.ROLE
			,CASE t_doc_lex_droit.DR_GESTION WHEN 'COL' THEN 'Gestion collective' WHEN 'CFR' THEN 'Géré par le CFRT' WHEN 'OTH' THEN 'Autres' END as DR_GERE_PAR
			FROM t_doc_lex
			INNER JOIN t_personne ON t_doc_lex.ID_PERS=t_personne.ID_PERS
			LEFT OUTER JOIN t_doc_acc ON (t_personne.PERS_PHOTO=t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG=".$db->Quote($_SESSION['langue']).")
			LEFT OUTER JOIN t_doc_lex_precision ON (t_doc_lex_precision.ID_DOC_LEX=t_doc_lex.ID_DOC_LEX AND t_doc_lex_precision.ID_LANG=".$db->Quote($_SESSION['langue']).")
			LEFT OUTER JOIN t_doc_lex_droit ON (t_doc_lex_droit.ID_DOC_LEX = t_doc_lex.ID_DOC_LEX)
			LEFT OUTER JOIN t_role ON (t_doc_lex.DLEX_ID_ROLE=t_role.ID_ROLE AND t_role.ID_LANG=".$db->Quote($_SESSION['langue']).")
			WHERE t_doc_lex.ID_LEX=0 AND t_doc_lex.ID_DOC in ($ids) AND t_personne.ID_LANG='".$_SESSION['langue']."' ORDER BY ID_DOC ASC";
			$results_lex_droit = $db->Execute($sql_lex_droit);
			//trace("class_doc getXmlList - sql_lex_droit : ".$sql_lex_droit);
			unset($sql_lex_droit);
		}

		if($mode['doc_fest']){
			$sql_doc_fest="select distinct df.ID_DOC , df.ID_DOC_FEST,f.*
			from t_doc_fest df
			inner join t_festival f ON f.ID_FEST=df.ID_FEST
			where df.ID_DOC in ($ids)  and f.ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY df.ID_DOC ASC";
			$results_doc_fest = $db->Execute($sql_doc_fest);
			unset($sql_doc_fest);

			$sql_doc_fest_val="SELECT * from t_doc_fest_val dfv, t_val v where dfv.ID_VAL=v.ID_VAL and
			v.ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY ID_DOC_FEST";
			$results_doc_fest_val=$db->Execute($sql_doc_fest_val);
			unset($sql_doc_fest_val);
		}


		// MS droits pré existants => A mettre en place

		// on range les results_mat pour pouvoir y acceder plus rapidement par la suite;

		while($mode['lex'] && $lex = $results_lex->FetchRow()){
			$results_lex_ref[$lex['ID_DOC']][]=$results_lex->CurrentRow()-1;
		}
		while($mode['pers_lex'] && $pers_lex = $results_pers_lex->FetchRow()){
			$results_pers_lex_ref[$pers_lex['ID_DOC']][]=$results_pers_lex->CurrentRow()-1;
		}
		while($mode['val'] && $val = $results_val->FetchRow()){
			$results_val_ref[$val['ID_DOC']][]=$results_val->CurrentRow()-1;
		}
		while($mode['docmat'] && $docmat = $results_docmat->FetchRow()){
			$results_docmat_ref[$docmat['ID_DOC']][]=$results_docmat->CurrentRow()-1;
		}
		while($mode['docmat_val'] && $docmat_val = $results_docmat_val->FetchRow()){
			$results_docmat_val_ref[$docmat_val['ID_DOC_MAT']][]=$results_docmat_val->CurrentRow()-1;
		}
		while($mode['mat_val'] && $mat_val = $results_mat_val->FetchRow()){
			$results_mat_val_ref[$mat_val['ID_MAT']][]=$results_mat_val->CurrentRow()-1;
		}
		while($mode['mat_pers'] && $mat_pers = $results_mat_pers->FetchRow()){
			$results_mat_pers_ref[$mat_pers['ID_MAT']][]=$results_mat_pers->CurrentRow()-1;
		}
		while($mode['cat'] && $cat = $results_cat->FetchRow()){
			$results_cat_ref[$cat['ID_DOC']][]=$results_cat->CurrentRow()-1;
		}
		while($mode['img'] && $img = $results_img->FetchRow()){
			$results_img_ref[$img['ID_DOC']][]=$results_img->CurrentRow()-1;
		}
		while($mode['seq'] && $seq = $results_seq->FetchRow()){
			$results_seq_ref[$seq['DOC_ID_GEN']][]=$results_seq->CurrentRow()-1;
		}
		while($mode['doc_acc'] && $doc_acc = $results_doc_acc->FetchRow()){
			$results_doc_acc_ref[$doc_acc['ID_DOC']][]=$results_doc_acc->CurrentRow()-1;
		}
		while($mode['doc_acc'] && $docacc_val = $results_docacc_val->FetchRow()){
			$results_docacc_val_ref[$docacc_val['ID_DOC_ACC']][]=$results_docacc_val->CurrentRow()-1;
		}
		while($mode['doc_lies_src'] && $lies_src = $results_lies_src->FetchRow()){
			$results_lies_src_ref[$lies_src['ID_DOC_SRC']][]=$results_lies_src->CurrentRow()-1;
		}
		while($mode['doc_lies_dst'] && $lies_dst = $results_lies_dst->FetchRow()){
			$results_lies_dst_ref[$lies_dst['ID_DOC_DST']][]=$results_lies_dst->CurrentRow()-1;
		}
		while($mode['redif'] && $redif = $results_redif->FetchRow()){
			$results_redif_ref[$redif['ID_DOC']][]=$results_redif->CurrentRow()-1;
		}
		while($mode['lex_droit'] && $lex_droit = $results_lex_droit->FetchRow()){
			$results_lex_droit_ref[$lex_droit['ID_DOC']][]=$results_lex_droit->CurrentRow()-1;
		}

		while($mode['doc_fest'] && $doc_fest = $results_doc_fest->FetchRow()){
			$results_doc_fest_ref[$doc_fest['ID_DOC']][]=$results_doc_fest->CurrentRow()-1;
		}

		while($mode['doc_fest'] && $doc_fest_val = $results_doc_fest_val->FetchRow()){
			$results_doc_fest_val_ref[$doc_fest_val['ID_DOC_FEST']][]=$results_doc_fest_val->CurrentRow()-1;
		}


		foreach($results_doc as $doc){
			$xml .= "\t<t_doc ID_DOC=\"".$doc['ID_DOC']."\">\n";
			foreach($doc as $fld=>$val){
				//on ajoute pas de CDATA sur les champs docs qui contiennent du XML car on veut pouvoir les traiter dans le xsl, si ils sont CDATA, ils ne sont pas parcourable via xpath
				if(in_array($fld,array('DOC_CAT','DOC_VAL','DOC_LEX','DOC_XML','DOC_TRANSCRIPT')) || strpos($val,'XML') !== false){
					if($fld=='DOC_TRANSCRIPT')
						$val=str_replace(array('<VIRG>','&'),array('<VIRG/>','&amp;'),$val);
					$xml .= "\t\t<".strtoupper($fld).">".str_replace($search_chars,$replace_chars,$val)."</".strtoupper($fld).">\n";
				}else {
					$xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
				}
			}



            //==========GESTION DOC_LEX===========
            // récupération des lex associé au doc courant
            if(isset($results_lex_ref[$doc['ID_DOC']][0])){
                $results_lex->Move($results_lex_ref[$doc['ID_DOC']][0]);
                $array_lex_doc = $results_lex->GetRows(count((array)$results_lex_ref[$doc['ID_DOC']]));
            }else{
                $array_lex_doc=array();
            }

            if(count($array_lex_doc)>0){
                $xml.="\t\t<t_doc_lex nb=\"".count($array_lex_doc)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";

                foreach($array_lex_doc as $lex){
                    $xml.="\t\t\t<t_lex ID_TYPE_DESC=\"".$lex['ID_TYPE_DESC']."\">\n";
                    foreach($lex as $fld=>$val){
                    	if(strpos($val,'XML') !== false) {
                    		$xml .= "\t\t\t\t<".strtoupper($fld).">".str_replace($search_chars,$replace_chars,$val)."</".strtoupper($fld).">\n";
                    	} else {
                        	$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    	}
                    }
                    $xml.="\t\t\t</t_lex>\n";
                }
                $xml.="\t\t</t_doc_lex>\n";
            }

            //==========GESTION DOC_LEX PERSONNE ===========
            // récupération des lex associé au doc courant
            if(isset($results_pers_lex_ref[$doc['ID_DOC']][0])){
                $results_pers_lex->Move($results_pers_lex_ref[$doc['ID_DOC']][0]);
                $array_pers_lex = $results_pers_lex->GetRows(count((array)$results_pers_lex_ref[$doc['ID_DOC']]));
            }else{
                $array_pers_lex=array();
            }

            if(count($array_pers_lex)>0){
                $xml.="\t\t<t_pers_lex nb=\"".count($array_pers_lex)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";

                foreach($array_pers_lex as $pers_lex){
                	//TODO : vérifier présence de <XML> avant de mettre CDATA
                    $xml.="\t\t\t<t_personne ID_PERS=\"".$pers_lex['ID_PERS']."\" ID_TYPE_DESC=\"".$pers_lex['ID_TYPE_DESC']."\">\n";
                    foreach($pers_lex as $fld=>$val){
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
					$xml.="\t\t\t</t_personne>\n";
                }
                $xml.="\t\t</t_pers_lex>\n";
            }

            //======== GESTION DOC_VAL ==============
            // récupération des vals associées au doc courant
            if(isset($results_val_ref[$doc['ID_DOC']][0])){
                $results_val->Move($results_val_ref[$doc['ID_DOC']][0]);
                $array_val_doc = $results_val->GetRows(count((array)$results_val_ref[$doc['ID_DOC']]));
            }else{
                $array_val_doc=array();
            }
            if(count($array_val_doc)>0){
                $xml.="\t\t<t_doc_val nb=\"".count($array_val_doc)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_val_doc as $valeur){
                    $xml.="\t\t\t<t_val ID_TYPE_VAL=\"".$valeur['ID_TYPE_VAL']."\">\n";
                    foreach($valeur as $fld=>$val_){
                    	//TODO : vérifier présence de <XML> avant de mettre CDATA
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val_)."]]></".strtoupper($fld).">\n";
                    }
                $xml.="\t\t\t</t_val>\n";
                }
                $xml.="\t\t</t_doc_val>\n";
            }
            //======== GESTION DOC_MAT ==============

            if(isset($results_docmat_ref[$doc['ID_DOC']][0])){
                $results_docmat->Move($results_docmat_ref[$doc['ID_DOC']][0]);
                $array_docmat = $results_docmat->GetRows(count((array)$results_docmat_ref[$doc['ID_DOC']]));
            }else{
                $array_docmat=array();
            }
            if(count($array_docmat)>0){
                $xml.="\t\t<t_doc_mat nb=\"".count($array_docmat)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_docmat as $docmat){
                    $xml.="\t\t\t<DOC_MAT ID_MAT=\"".$docmat['ID_MAT']."\">\n";
                    foreach($docmat as $fld=>$val){
                    $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                    // t_mat_val
                    if(isset($results_mat_val_ref[$docmat['ID_MAT']][0])){
                        $results_mat_val->Move($results_mat_val_ref[$docmat['ID_MAT']][0]);
                        $array_mat_val = $results_mat_val->GetRows(count((array)$results_mat_val_ref[$docmat['ID_MAT']]));
                    }else{
                        $array_mat_val= array();
                    }
                    if(count($array_mat_val)>0){
                        $xml.="\t\t\t\t<t_mat_val>\n";
                        foreach($array_mat_val as $mat_val){
                            $xml.="\t\t\t\t\t<t_val ID_VAL=\"".$mat_val['ID_VAL']."\" ID_TYPE_VAL=\"".$mat_val['ID_TYPE_VAL']."\">\n";
                            foreach($mat_val as $fld=>$val){
                                $xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                            }
                            $xml.="\t\t\t\t\t</t_val>\n";
                        }
                        $xml.="\t\t\t\t</t_mat_val>\n";
                    }

					// t_mat_pers
					if(isset($results_mat_pers_ref[$docmat['ID_MAT']][0])){
						$results_mat_pers->Move($results_mat_pers_ref[$docmat['ID_MAT']][0]);
						$array_mat_pers = $results_mat_pers->GetRows(count((array)$results_mat_pers_ref[$docmat['ID_MAT']]));
					}else{
						$array_mat_pers= array();
					}
					if(count($array_mat_pers)>0){
						$xml.="\t\t\t\t<t_mat_pers>\n";
						foreach($array_mat_pers as $mat_pers){
							$xml.="\t\t\t\t\t<t_personne ID_PERS=\"".$mat_pers['ID_PERS']."\" ID_TYPE_DESC=\"".$mat_pers['ID_TYPE_DESC']."\">\n";
							foreach($mat_pers as $fld=>$val){
								$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
							}
							$xml.="\t\t\t\t\t</t_personne>\n";
						}
						$xml.="\t\t\t\t</t_mat_pers>\n";
					}

					// t_doc_mat_val
					if(isset($results_docmat_val_ref[$docmat['ID_DOC_MAT']][0])){
						$results_docmat_val->Move($results_docmat_val_ref[$docmat['ID_DOC_MAT']][0]);
						$array_docmat_val = $results_docmat_val->GetRows(count((array)$results_docmat_val_ref[$docmat['ID_DOC_MAT']]));
					}else{
						$array_docmat_val= array();
					}
					if(count($array_docmat_val)>0){
						$xml.="\t\t\t\t<t_doc_mat_val>\n";
						foreach($array_docmat_val as $dmat_val){
							$xml.="\t\t\t\t\t<t_val ID_TYPE_VAL=\"".$dmat_val['ID_TYPE_VAL']."\">\n";
							foreach($dmat_val as $fld=>$val){
								//TODO : vérifier présence de <XML> avant de mettre CDATA
								$xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
							}
							$xml.="\t\t\t\t\t</t_val>\n";
						}
						$xml.="\t\t\t\t</t_doc_mat_val>\n";
					}




                $xml.="\t\t\t</DOC_MAT>\n";
                }
                $xml.="\t\t</t_doc_mat>\n";
            }

            //======== GESTION DOC_CAT ==============
            // récupération des cats associées au doc courant
            if(isset($results_cat_ref[$doc['ID_DOC']][0])){
                $results_cat->Move($results_cat_ref[$doc['ID_DOC']][0]);
                $array_cat_doc = $results_cat->GetRows(count((array)$results_cat_ref[$doc['ID_DOC']]));
            }else{
                $array_cat_doc=array();
            }
            if(count($array_cat_doc)>0){
                $xml.="\t\t<t_doc_cat nb=\"".count($array_cat_doc)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_cat_doc as $categ){
                    $xml.="\t\t\t<t_categorie ID_CAT=\"".$categ['ID_CAT']."\">\n";
                    foreach($categ as $fld=>$val){
                    	//TODO : vérifier présence de <XML> avant de mettre CDATA
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                    $xml.="\t\t\t</t_categorie>\n";
                }
                $xml.="\t\t</t_doc_cat>\n";
            }

            // ========== GESTION DOC_IMG ==============
            // récupération des images associées au doc courant
            if(isset($results_img_ref[$doc['ID_DOC']][0])){
                $results_img->Move($results_img_ref[$doc['ID_DOC']][0]);
                $array_img_doc = $results_img->GetRows(count((array)$results_img_ref[$doc['ID_DOC']]));
            }else{
                $array_img_doc=array();
            }
            if(count($array_img_doc)>0){
                $xml.="\t\t<t_image nb=\"".count($array_img_doc)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_img_doc as $img){
                    $xml.="\t\t\t<DOC_IMG ID_IMAGE=\"".$img['ID_IMAGE']."\">\n";
                    foreach($img as $fld=>$val){
                    	//TODO : vérifier présence de <XML> avant de mettre CDATA
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                $xml.="\t\t\t</DOC_IMG>\n";
                }
                $xml.="\t\t</t_image>\n";
            }

            //========== GESTION DOC_SEQ =================

            if(isset($results_seq_ref[$doc['ID_DOC']][0])){
                $results_seq->Move($results_seq_ref[$doc['ID_DOC']][0]);
                $array_seq_doc = $results_seq->GetRows(count((array)$results_seq_ref[$doc['ID_DOC']]));
            }else{
                $array_seq_doc=array();
            }
            if(count($array_seq_doc)>0){
                $xml.="\t\t<t_seq nb=\"".count($array_seq_doc)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_seq_doc as $seq){
                    $xml.="\t\t\t<DOC_SEQ ID_DOC=\"".$seq['ID_DOC']."\">\n";
                    foreach($seq as $fld=>$val){
                    	//TODO : vérifier présence de <XML> avant de mettre CDATA
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                    $xml.="\t\t\t</DOC_SEQ>\n";
                }
                $xml.="\t\t</t_seq>\n";
            }


            // ======== GESTION DOC_ACC ===============
            if(isset($results_doc_acc_ref[$doc['ID_DOC']][0])){
                $results_doc_acc->Move($results_doc_acc_ref[$doc['ID_DOC']][0]);
                $array_doc_acc = $results_doc_acc->GetRows(count((array)$results_doc_acc_ref[$doc['ID_DOC']]));
            }else{
                $array_doc_acc=array();
            }
            if(count($array_doc_acc)>0){
                $xml.="\t\t<t_doc_acc nb=\"".count($array_doc_acc)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_doc_acc as $doc_acc){
                    $xml.="\t\t\t<t_doc_acc ID_DOC_ACC=\"".$doc_acc['ID_DOC_ACC']."\">\n";
                    foreach($doc_acc as $fld=>$val){
                    	//TODO : vérifier présence de <XML> avant de mettre CDATA
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }

            // ===DOC_ACC_VAL========
                    if(isset($results_docacc_val_ref[$doc_acc['ID_DOC_ACC']][0])){
                        $results_docacc_val->Move($results_docacc_val_ref[$doc_acc['ID_DOC_ACC']][0]);
                        $array_docacc_val = $results_docacc_val->GetRows((array)count($results_docacc_val_ref[$doc_acc['ID_DOC_ACC']]));
                    }else{
                        $array_docacc_val=array();
                    }
                    if(count($array_docacc_val)>0){
                        $xml.="\t\t\t\t<t_doc_acc_val nb=\"".count($array_docacc_val)."\" >\n";
                        foreach($array_docacc_val as $docacc_val){
                            $xml.="\t\t\t\t\t<TYPE_VAL ID_TYPE_VAL=\"".$docacc_val['ID_TYPE_VAL']."\">\n";
                            foreach($docacc_val as $fld=>$val){
                            	//TODO : vérifier présence de <XML> avant de mettre CDATA
                                $xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                            }
                            $xml.="\t\t\t\t\t</TYPE_VAL>\n";
                        }
                        $xml.="\t\t\t\t</t_doc_acc_val>\n";
                    }

                    $xml.="\t\t\t</t_doc_acc>\n";
                }
                $xml.="\t\t</t_doc_acc>\n";
            }

            //========== GESTION DOC_LIES_SRC =================

            if(isset($results_lies_src_ref[$doc['ID_DOC']][0])){
                $results_lies_src->Move($results_lies_src_ref[$doc['ID_DOC']][0]);
                $array_doc_lies_src = $results_lies_src->GetRows(count((array)$results_lies_src_ref[$doc['ID_DOC']]));
            }else{
                $array_doc_lies_src=array();
            }
            if(count($array_doc_lies_src)>0){
                $xml.="\t\t<t_doc_lies_src nb=\"".count($array_doc_lies_src)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_doc_lies_src as $doc_lies){
                    $xml.="\t\t\t<t_doc ID_DOC=\"".$doc_lies['ID_DOC']."\">\n";
                    foreach($doc_lies as $fld=>$val){
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                    $xml.="\t\t\t</t_doc>\n";
                }
                $xml.="\t\t</t_doc_lies_src>\n";
            }
            //========== GESTION DOC_LIES_DST =================

            if(isset($results_lies_dst_ref[$doc['ID_DOC']][0])){
                $results_lies_dst->Move($results_lies_dst_ref[$doc['ID_DOC']][0]);
                $array_doc_lies_dst = $results_lies_dst->GetRows((array)count($results_lies_dst_ref[$doc['ID_DOC']]));
            }else{
                $array_doc_lies_dst=array();
            }
            if(count($array_doc_lies_dst)>0){
                $xml.="\t\t<t_doc_lies_dst nb=\"".count($array_doc_lies_dst)."\" ID_DOC=\"".$doc['ID_DOC']."\">\n";
                foreach($array_doc_lies_dst as $doc_lies){
                    $xml.="\t\t\t<t_doc ID_DOC=\"".$doc_lies['ID_DOC']."\">\n";
                    foreach($doc_lies as $fld=>$val){
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                    $xml.="\t\t\t</t_doc>\n";
                }
                $xml.="\t\t</t_doc_lies_dst>\n";
            }

            // ========== GESTION DOC_REDIF ============
            if(isset($results_redif_ref[$doc['ID_DOC']][0])){
                $results_redif->Move($results_redif_ref[$doc['ID_DOC']][0]);
                $array_doc_redif = $results_redif->GetRows((array)count($results_redif_ref[$doc['ID_DOC']]));
            }else{
                $array_doc_redif=array();
            }
            if(count($array_doc_redif)>0){
                $xml.="\t\t<t_redif nb=\"".count($array_doc_redif)."\" >\n";
                foreach($array_doc_redif as $doc_redif){
                    $xml.="\t\t\t<DOC_REDIF ID_REDIF=\"".$doc_redif['ID_REDIF']."\">\n";
                    foreach($doc_redif as $fld=>$val){
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                    $xml.="\t\t\t</DOC_REDIF>\n";
                }
                $xml.="\t\t</t_redif>\n";
            }

            // ===== GESTION LEX_DROIT ========

            if(isset($results_lex_droit_ref[$doc['ID_DOC']][0])){
                $results_lex_droit->Move($results_lex_droit_ref[$doc['ID_DOC']][0]);
                $array_lex_droit = $results_lex_droit->GetRows((array)count($results_lex_droit_ref[$doc['ID_DOC']]));
            }else{
                $array_lex_droit=array();
            }
            if(count($array_lex_droit)>0){
                $xml.="\t\t<t_lex_droit nb=\"".count($array_lex_droit)."\" >\n";
                foreach($array_lex_droit as $lex_droit){
                    $xml.="\t\t\t<DOC_LEX_DROIT ID_DOC_LEX=\"".$lex_droit['ID_DOC_LEX']."\">\n";
                    foreach($lex_droit as $fld=>$val){
                    	//TODO : vérifier présence de <XML> avant de mettre CDATA
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                $xml.="\t\t\t</DOC_LEX_DROIT>\n";
                }
                $xml.="\t\t</t_lex_droit>\n";
            }



            // ======= GESTION FESTIVAL ======
            if(isset($results_doc_fest_ref[$doc['ID_DOC']][0])){
                $results_doc_fest->Move($results_doc_fest_ref[$doc['ID_DOC']][0]);
                $array_doc_fest = $results_doc_fest->GetRows(count((array)$results_doc_fest_ref[$doc['ID_DOC']]));
            }else{
                $array_doc_fest=array();
            }
            if(count($array_doc_fest)>0){
                $xml.="\t\t<t_doc_fest nb=\"".count($array_doc_fest)."\" >\n";
                foreach($array_doc_fest as $doc_fest){
                    $xml.="\t\t\t<FEST>\n";
                    foreach($doc_fest as $fld=>$val){
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }

            // ===FEST_VAL========
                    if(isset($results_doc_fest_val_ref[$doc_fest['ID_DOC_FEST']][0])){
                        $results_doc_fest_val->Move($results_doc_fest_val_ref[$doc_fest['ID_DOC_FEST']][0]);
                        $array_doc_fest_val = $results_doc_fest_val->GetRows(count((array)$results_doc_fest_val_ref[$doc_fest['ID_DOC_FEST']]));
                    }else{
                        $array_doc_fest_val=array();
                    }

                    if(count($array_doc_fest_val)>0){
                        $xml.="\t\t\t\t<DOC_FEST_VAL>\n";
                        foreach($array_doc_fest_val as $doc_fest_val){
                            foreach($doc_fest_val as $fld=>$val){
                                $xml .= "\t\t\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                            }
                        }
                        $xml.="\t\t\t\t</DOC_FEST_VAL>\n";
                    }

                    $xml.="\t\t\t</FEST>\n";
                }
                $xml.="\t\t</t_doc_fest>\n";
            }



            $xml .= "\t</t_doc>\n";
            fwrite($xml_file,$xml);
            $xml = "";
        }

        fwrite($xml_file,"</EXPORT_OPSIS>\n");
        
        flock($xml_file, LOCK_UN);
        fclose($xml_file);
        // XB: en cas de nombreux chunk le fait de réécrire un gros fichier de log semble provoquer un crash mémoire, et comme on le réécrase a chaque passage, il n'est pas pertinent
        // $log = new Logger("data_export.xml");
        // $log->Log(file_get_contents($xml_file_path));
        return $xml_file_path;
    }


    /**
     * Récupère l'ID_MAT associé à la visualisation d'un document.
     * IN : var de class id_doc
     * OUT : id_mat_vis (return)
     */

	function getMaterielVisu($getIdsOnly=true, $return_multiple_mats = false, $pattern = null, $id_lang=null,$formats = null ){
		global $db;
		$update_key_pattern  = false;
		// alimentation de l'array t_doc_mat de l'objet si il n'est pas déja chargé
		// Le test est un peu tordu, mais il reste ainsi compatible avec php 5.3 en évitant les appels nested des fonctions natives php.

		if(empty($this->t_doc_mat) || reset($this->t_doc_mat) && !isset($this->t_doc_mat[key($this->t_doc_mat)]['MAT'])){
			$this->getMats();
		}
		// normalisation du pattern sous forme d'array
		if(!empty($pattern) && $pattern !== 'null'){
			if(!is_array($pattern)){
				$pattern = array($pattern);
			}
			// suppression des éventuels doublons ...
			$pattern = array_unique($pattern);
		}

		$mats_visu = array() ;

		
		// echo" pattern : ".$pattern."\n";
		foreach($this->t_doc_mat as $key_dm=>$dmat){
      // Avant de classer les matériels, on disqualifie :
			// - les matériels qui n'ont pas un format_online (issu de t_format_mat) ou un format correspondant aux valeurs fournies dans l'array formats en parametre
			// - les matériels dont on ne trouve pas le fichier (has_file)
			// - les matériels dont le lien avec le doc est inactif (DMAT_INACTIF)
			// - les matériels dont le lien avec le doc ne correspond pas à la langue en paramètre si la langue du lien & la langue en paramètre sont définies

                        //laisser ces debug_affich (en commentaires) pour pouvoir debugger facilement
                       // echo 'debug_affich $dmat[\'MAT\']->t_mat[\'MAT_NOM\'] '.$dmat['MAT']->t_mat['MAT_NOM'].'<br />';
                       // echo 'debug_affich $dmat[\'MAT\']->t_mat[\'MAT_FORMAT\'] '.$dmat['MAT']->t_mat['MAT_FORMAT'].'<br />';
                       // echo 'debug_affich $dmat[\'MAT\']->hasfile '.$dmat['MAT']->hasfile.'<br />';
                       // echo 'debug_affich $dmat[\'DMAT_INACTIF\'] '.$dmat['DMAT_INACTIF'].'<br />';
                       // echo 'debug_affich FORMAT'.print_r($formats,true).'<br />';
                       // echo 'debug_affich $dmat[\'MAT\']->format_online '.$dmat['MAT']->format_online.'<br />';
						// trace($dmat['MAT']->t_mat['MAT_FORMAT'].' : '.$dmat['MAT']->format_online." ". $dmat['MAT']->hasfile." ".$dmat['DMAT_INACTIF']);

			if(((empty($formats) && $dmat['MAT']->format_online) || (!empty($formats) && in_array($dmat['MAT']->t_mat['MAT_FORMAT'],$formats)))
			&& $dmat['MAT']->hasfile
			&& $dmat['DMAT_INACTIF'] != "1"
			&& ((!empty($id_lang)) || empty($id_lang) || empty($dmat['DMAT_ID_LANG']))){
				if(!empty($dmat['DMAT_ID_LANG']) && $dmat['DMAT_ID_LANG'] == $id_lang){
					$weight = 1;
				}else{
					$weight = 0 ;
				}
				// si le matériel à un MAT_TYPE à VISIO, il sera mieux classé (+1)
				if($dmat['MAT']->t_mat['MAT_TYPE'] == 'VISIO' || strpos($dmat['MAT']->t_mat['MAT_TYPE'],'VISIO')===0){
					$weight++;
				}

				// si un pattern est défini et qu'on le retrouve dans le nom d'un matériel, alors le mat est mieux classé (+5)
				if(!empty($pattern) ){
					$found = false ;
					foreach($pattern as $key=>$p){
						if(stristr($dmat['MAT']->t_mat['MAT_NOM'],$p)){
							$weight+=5 ;
							$found = true ;
							if(is_string($key)){
								$update_key_pattern = true ;
								$dmat['key_pattern'] = $key;
							}
							break ;
						}
					}

					// si pattern défini et que le matériel ne satisfait pas le pattern, alors il est fortement déclassé (-5)
					if(!$found){
						$weight-=5 ;
					}
				}


				// Si le matériel est un HLS, si on est sur mobile  alors il est bien mieux classé(+10)
				// si on est pas sur mobile (OS android, ou device apple), on enleve du poids car on ne veut pas qu'il ressorte avant un matériel de visionnage plus classique
				// si le document est une séquence, on enlève également du poids car le découpage dynamique du HLS est trop complexe pour l'instant


				if( strpos($dmat['MAT']->t_mat['MAT_FORMAT'],'HLS')!==false ){
					if(( empty($this->t_doc['DOC_ID_GEN']) || $this->t_doc['DOC_ID_GEN'] == '0')
					&& !empty($_SESSION['env_user']['env_OS']) && in_array($_SESSION['env_user']['env_OS'],array('Android','iPod','iPad','iPhone'))){
						$weight =$weight+11; // (on doit contrebalancer le pattern -5 et passer devant le mat de visio standard donc poids = 5 + 5 + 1)
					}else{
						$weight -= 12 ;
					}
				}

				// MS - ICI éventuellement il faudrait tester la présence en conf (fonction ? constante ?) d'un test custom qu'on pourrait executer ici
				// => permettrait d'avoir une définition custom du poids selon différents critères pr certains clients



				// on ajoute à la liste uniquement les matériels qui ont un poids supérieur ou égal à 0 ;
				// on garde les == 0 pour backward compat...
				if($weight >=0){
					$dmat['weight'] = $weight ;
					$mats_visu[] =$dmat;
				}
			}
		}
		unset($weight);
		if($update_key_pattern){
			$mats_visu_ = array() ;
			foreach($mats_visu as $key => $dm){
				if(isset($dm['key_pattern'])){
					$mats_visu_[$dm['key_pattern']] = $dm;
				}else{
					$mats_visu_[$key] = $dm;
				}
			}
			$mats_visu = $mats_visu_;
			unset($mats_visu_);
		}
		
		if(defined('gAllowMultipleVisFiles_key_field')) {
			$fieldKey = gAllowMultipleVisFiles_key_field;
			$mats_visu_ = array() ;
			foreach($mats_visu as $key => $dm){
				$mats_visu_[$dm[$fieldKey]] = $dm;
			}
			$mats_visu = $mats_visu_;
			unset($mats_visu_);
		}		

		// on classe l'array mats_visu en fonction du poids
		if(!function_exists("sort_weight")){
			function sort_weight($a,$b){
				if($a['weight'] == $b['weight'] ){
					if($a['ID_MAT'] == $b['ID_MAT']) return 0 ;
					if(defined("gDocGetMatVisu_sortIdMatDesc") && gDocGetMatVisu_sortIdMatDesc){
						return ($a['ID_MAT']>$b['ID_MAT'])?-1:1;
					}else{
						return ($a['ID_MAT']<$b['ID_MAT'])?-1:1;
					}
				}
				return ($a['weight']<$b['weight'] )?1:-1;
			}
		}

		
		// le tri uasort permet de trier sur la valeur weight, mais ne réindexe pas l'array,
		// par clarté on préfère avoir une réorganisation des index numériques => on passe donc l'array triée dans array_values
		uasort($mats_visu,"sort_weight");

		// on ne réorganise les index que si les clés sont numériques

		$mats_visu_keys = array_keys($mats_visu); // pour éviter une lévée d'erreur strict standards
		
		$reorganise_index = true ; 
		foreach($mats_visu_keys as $idx=>$visu_key){
			$reorganise_index = $reorganise_index && is_int($visu_key);
		}
		
	  	if($reorganise_index) {
	   		$mats_visu = array_values($mats_visu);
	  	}

		if(!empty($mats_visu)){
			// si le flag return_multiple_mats est true, on renvoi les infos de touts les matériels
			if($return_multiple_mats){
				// si getIdsOnly est true, on ne renvoi que les ID_MATs
				if($getIdsOnly){
					foreach($mats_visu as &$mats){
						$mats = $mats['ID_MAT'];
					}
					return ($mats_visu);
				}else{
					return $mats_visu;
				}
			}else{
				$arr_tmp = array_values($mats_visu);
				if($getIdsOnly){
					return $arr_tmp[0]['ID_MAT'];
				}else{
					return $arr_tmp[0];
				}
			}
		}
	}
	// pour backward compatibility, on conserve cette fonction et on la redirige vers la nouvelle version de getMaterielVisu avec les bons paramètres
	function getMaterielsVisu($arrExt = array("_vis")){
		return $this->getMaterielVisu(true,true,$arrExt);
	}

	/** recuperer les materiels de livraison
	*  IN : array contenant les criteres de recherche array('champ'=>'valeur', ...)
	*  OUT : array contenant le doc repondant au criteres
	*/
	function getMatLivraison($criteres)
	{
		if (!is_array($criteres))
		{
			return null;
		}

		$mat_eligible=array();
		foreach($this->t_doc_mat as $mat)
		{
			$mat_valide=true;
			if (!empty($criteres))
			{
				foreach ($criteres as $cle=>$crit)
				{
					if (!isset($mat['MAT']) || (isset($mat['MAT']->t_mat[$cle]) && ((is_array($crit) && !in_array($mat['MAT']->t_mat[$cle], $crit)) || (!is_array($crit) && $crit!=$mat['MAT']->t_mat[$cle]))))
						$mat_valide=false;
				}
			}

			if ($mat_valide)
				$mat_eligible[]=$mat;
		}
		foreach ($mat_eligible as $mat)
		{
			if ($mat['DMAT_ID_LANG']==$_SESSION['langue'] && $mat['DMAT_INACTIF']==0)
			{
				return $mat['MAT'];
			}
		}

        // VP 28/05/2014 : choix du matériel avec l'identifiant maximum
        //return $mat_eligible[0]['MAT'];
		return $mat_eligible[count($mat_eligible)-1]['MAT'];
		//var_dump($mat_eligible);

	}

	/** Vérifie si le doc existe déjà en base
	 *  IN : objet doc
	 *  OUT : TRUE si existe, FALSE si libre
	 *  NOTE : critères d'unicité définis par une constante.
	 */
	// VP 15/04/10 : création méthode checkExist
	function checkExist() {
		global $db;

		if (!empty($this->uniqueFields)) { //Le test d'unicité se base sur le param config uniqueFields qui contient les champs constituant la clé unique
			$sql="SELECT ID_DOC FROM t_doc WHERE 1=1 ";
			foreach ($this->uniqueFields as $fld) $sql.=" AND ".strtoupper($fld)."=".$db->Quote($this->t_doc[strtoupper($fld)]);
			if (!in_array('ID_DOC',$this->uniqueFields)){
				$sql.=" AND ID_DOC!=".intval($this->t_doc['ID_DOC']);
			}
			// debug($sql,'pink');
			$existDeja=$db->GetOne($sql);
			if (!$existDeja){
				return false;
			}
			else {
				$this->t_doc['ID_DOC']=$existDeja;
				return true;
			}
		}else return false;
	}


    //LD 31 03 09 : vérifie si un id_doc existe en base, utilisé pour le webservice detail
    function checkExistId() {
		global $db;
		$sql="SELECT ID_DOC FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC']);
		$sql.=" AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);

		return $db->GetOne($sql);

	}

	//VP 27/11/09 : vérification existance par cote
    function checkExistCote() {
		global $db;
		$sql="SELECT distinct ID_DOC FROM t_doc WHERE DOC_COTE=".$db->Quote($this->t_doc['DOC_COTE']);

		return $db->GetOne($sql);
	}

	/**
	 *
	 * Pour chaque version, vérifie que la vignette ne correspond pas à un matériel tout juste dissocié du document
	 * @param int $idDoc
	 * @param array $dmDeleted
	 */
	static function checkVignette($idDoc, $dmDeleted) {
		global $db;
		include_once(modelDir.'model_imageur.php');
		include_once(modelDir.'model_materiel.php');
		$oDoc = new Doc();
		$oDoc->t_doc['ID_DOC'] = $idDoc;
		$oDoc->t_doc['ID_LANG'] = $_SESSION['langue'];
		$oDoc->getDoc();
		$oDoc->getDocMat();
		$aIdMatDel = array();
		//Il se peut que le matériel ait été associé plusieurs fois au document :
		//on récupère les matériels totalement dissociés
		foreach($dmDeleted as $numLigne => $ligneDM) {
			$finded = false;
			if(!empty($oDoc->t_doc_mat)) {
				foreach ($oDoc->t_doc_mat as $dmat) {
					if(strcmp($dmat['ID_MAT'],$ligneDM['ID_MAT']) === 0) {
						$finded = true;
						break;
					}
				}
			}
			if(!$finded) {
				$aIdMatDel[] = $ligneDM['ID_MAT'];
			}
		}
		if(empty($aIdMatDel)) {
			return true;
		}

		//On récupère leurs imageurs
		$oMatTemp = new Materiel();
		$aMatTemp = $oMatTemp->getData($aIdMatDel);
		$aImageurs = array();
		if(!empty($aMatTemp)){
			foreach ($aMatTemp as $matTemp) {
				if(!empty($matTemp->t_mat['MAT_ID_IMAGEUR'])){
					$aImageurs[] = $matTemp->t_mat['MAT_ID_IMAGEUR'];
				}
			}
			unset($aMatTemp);
		}
		if(empty($aImageurs)) {
			return true;
		}

		//pour le document courant
		if(!empty($oDoc->t_doc['DOC_ID_IMAGE'])) {
			$oImage = new Image();
			$oImage->getImage(array("ID_IMAGE" => array($oDoc->t_doc['DOC_ID_IMAGE']) ));
			//On regarde ensuite si l'imageur du document correspond à une de ces imageurs
			if(in_array($oImage->t_image['ID_IMAGEUR'],$aImageurs)) {
				//Si oui, on supprime la vignette
				$oDoc->saveVignette("DOC_ID_IMAGE", "", null, null, $oDoc->t_doc['ID_LANG']);
				$oDoc->saveImageur('');
			}
			unset($oImage);
		}

		//on récupère les autres versions linguistiques du document
		$oDoc->getVersions();
		if(!empty($oDoc->arrVersions)) {
			foreach($oDoc->arrVersions as $versions) {
				//On regarde ensuite si la vignette de chaque version linguistique du document correspond à une de ces images
				$oDocVers = new Doc();
				$oDocVers->t_doc['ID_DOC'] = $oDoc->t_doc['ID_DOC'];
				$oDocVers->t_doc['ID_LANG'] = $versions['ID_LANG'];
				$oDocVers->getDoc();
				if(empty($oDocVers->t_doc['DOC_ID_IMAGE'])) {
					continue;
				}
				$oImage = new Image();
				$oImage->getImage(array("ID_IMAGE" => array($oDocVers->t_doc['DOC_ID_IMAGE'])));
				//On regarde ensuite si la vignette du document correspond à une de ces images
				if(in_array($oImage->t_image['IMAGEUR'],$aImageurs)) {
					//Si oui, on supprime la vignette
					$oDocVers->saveVignette("DOC_ID_IMAGE", "", null, null, $oDocVers->t_doc['ID_LANG']);
				}
				unset($oImage);
				unset($oDocVers);
			}
		}

		return true;
	}


	/**
	 * Retourne tous les privilèges à ce document.
	 * IN : Singleton User + var de classe id_doc
	 * OUT : tableau de privilèges
	 */
	function getPrivileges() {
									
		$arrPriv = array();
		$myUsr = User::getInstance();
		if(isset($myUsr->Groupes[$this->t_doc['DOC_ID_FONDS']])){
			$arrPriv = $myUsr->Groupes[$this->t_doc['DOC_ID_FONDS']]["arrPriv"];
		}
		if($myUsr->getTypeLog()==kLoggedAdmin){
			$arrPriv[]=99;
		}
		if(function_exists("getPrivilegesCustom")){
			$arrPriv = getPrivilegesCustom($this, $arrPriv);
		}
		return $arrPriv;
	}

     /**
     * Vérifie qu'un utilisateur a le droit d'accès à ce document.
     * Pour cela, on vérifie le type de privilège sur le fonds du document.
     * IN : Niveau (acces demandé) 1=consult,2=visu,3=crea extr,4=reserve,5=modif
     * 		Singleton User + var de classe id_doc
     * OUT : boolean (acces=true, refuse=false)
     */
    function checkAccess($niveau) {
    	global $db;
		$id_priv_us_doc=0;
		/*
		$sqlFonds="SELECT DISTINCT DOC_ID_FONDS from t_doc WHERE ID_DOC=".$db->Quote($this->t_doc['ID_DOC']);
		$resultFonds=$db->Execute($sqlFonds);
		while($listFonds=$resultFonds->FetchRow()){
		    $doc_id_fonds = $listFonds["DOC_ID_FONDS"];
		}
		*/

        $id_priv_us_doc=User::getInstance()->verifieDroits($this->t_doc['DOC_ID_FONDS'],$niveau);

        // VP 11/02/13 : possibilité d'utiliser une fonction verifieDroitsCustom définie dans un fichier include local
        if(function_exists("verifieDroitsCustom")){
            $id_priv_us_doc=verifieDroitsCustom($this,$niveau);
        }
        if(empty($id_priv_us_doc) && $this->t_doc['DOC_ID_FONDS']>0 && (User::getInstance()->getTypeLog() < kLoggedUsagerAdv)) return false; else return true;
    }


    /**
     * Remplit un tableau avec les langues et titres des versions d'un document.
     * IN : id_doc (var classe) + ID_LANG (opt)
     * 		Si ID_LANG est passé, on récupère toutes les versions SAUF celle de l'ID_LANG
     * OUT : tableau versions (classe)

    function getVersionsOld($id_lang="") {
    	global $db;
    	unset($this->versions);
		$sql="SELECT ID_LANG, DOC_TITRE FROM t_doc WHERE ID_DOC=".$db->Quote($this->id_doc);
		if (!empty($id_lang)) $sql.="	and ID_LANG<>".$db->Quote($id_lang);
		$sql.=" ORDER BY 1";
    	$result=$db->Execute($sql);
    	while ($row=$result->FetchRow()) {
    		$this->versions[$row["ID_LANG"]]=str_replace(" ' ","'",$row["DOC_TITRE"]);
    	}
    	$result->Close();
    }
    */



	function updateDocTranscriptParent(){

		global $db;
		$split=$this->t_doc['DOC_TRANSCRIPT'];
		$origine='';

		//on récupre la transcriptoon parente
			if ($this->t_doc['DOC_ID_GEN']==0) return false;
				$result=$db->Execute("SELECT *  FROM t_doc WHERE ID_DOC=".intval($this->t_doc['DOC_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']));
				while($row=$result->FetchRow()){
					$origine.=trim($row["DOC_TRANSCRIPT"]);
				}



		//recupère l entete
		// preg_match('#<AudioDoc(.+)<SegmentList>#isU', $origine, $entete);
		// $entete=$entete[0];

		//recupere le premier et dernier wordstime
		$firstWordStime =  strpos($split,'<Word stime="');
		$lastWordStime =   strrpos($split,'<Word stime="');
		//partie utile du split
		$split_utile=(substr($split,$firstWordStime,($lastWordStime-$firstWordStime)));
		 //echo htmlentities($firstWordStime);

		 //a partrir du firstWordStime on cherche le premier nombre entre ""
		$debut_firstStime= strpos($split,'"',$firstWordStime)+1;
		$fin_firstStime= strpos($split,'"',$debut_firstStime+1);
		$firstStime=(substr($split,$debut_firstStime,($fin_firstStime-$debut_firstStime)));

		 //a partrir du lasWordStime on cherche le premier nombre entre ""
		$debut_lastStime= strpos($split,'"',$lastWordStime)+1;
		$fin_lastStime= strpos($split,'"',$debut_lastStime+1);
		$lastStime=(substr($split,$debut_lastStime,($fin_lastStime-$debut_lastStime)));

		//$fisrStime et $lastStime contiennent les valeurs min et max de la chaine éditée
		//on va chercher dans la transcription parente le bloc compris entre ces valeurs min et mex et la remplacer
		//il faut prendre en compte que les valeurs peuvent etre effacées, du coup $firstStimeOrigine et $LastStimeOrigine contiennent les valeurs les plus proche  ou égales

		//recuprere la transcription avec les timecodes qui vont bien
		//liste de tous les timecodes de la transcription parente
		if(isset($origine) && $origine){
			preg_match_all('#<Word stime="((?:\d+)(?:\.\d*)?)"#', $origine, $all_stime);
			}

		 //récupère la valeur de début la plus proche supérieur et de fin
		foreach ($all_stime[1] as $val){
			if($firstStime<=$val){
				$tab_debut[].=$val;
			}
			if($lastStime<=$val){
				$tab_fin[].=$val;
			}
		}

			if(!isset($tab_fin)){
				$LastStimeOrigine=$val;
			}
			else
			$LastStimeOrigine=min($tab_fin);

		$firstStimeOrigine=min($tab_debut);

		 //on cherche la position des deux valurs trouvé en entrée
		$pos_timecode_deb=strpos($origine,'<Word stime="'.$firstStimeOrigine);
		$pos_timecode_fin=strpos($origine,'<Word stime="'.$LastStimeOrigine);

		$chaine_editee= substr_replace($origine,$split_utile,$pos_timecode_deb,($pos_timecode_fin-$pos_timecode_deb));

	     $rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['DOC_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']));
		$sql = $db->GetUpdateSQL($rs, array("DOC_TRANSCRIPT" => trim($chaine_editee)));
		if (!empty($sql)) $result = $db->Execute($sql);
		$result->Close();

	}

		function saveDocTranscriptSequence($doc) {
			global $db;
			$mySeq=new Doc();

			$mySeq->t_doc['DOC_TRANSCRIPT']=$this->t_doc['DOC_TRANSCRIPT'];
			//recupère l entete
			$ok = preg_match('#<AudioDoc(.+)<SegmentList>#isU', $mySeq->t_doc['DOC_TRANSCRIPT'], $entete);
			// MS - si on ne trouve pas l'entete de la structure de transcription mediaspeech, on ne traite pas le champ doc_transcript
			if($ok){
				$DOC_TRANSCRIPT_ENTETE=$entete[0];

				//recuprere la transcription avec les timecodes qui vont bien
				//recupère tous les timecodes de la transcription  par expression régu
				if(isset($mySeq->t_doc['DOC_TRANSCRIPT']) && $mySeq->t_doc['DOC_TRANSCRIPT']){
					preg_match_all('#<Word stime="((?:\d+)(?:\.\d*)?)"#', $mySeq->t_doc['DOC_TRANSCRIPT'], $entete);
				}

				$val_debut=tcToSecDec($doc["DOC_TCIN"]);
				$val_fin=tcToSecDec($doc["DOC_TCOUT"]);


				$tab_debut=array();
				//on récupère toutes les valeurs supérieur au valeurs tcin et tcout
				foreach ($entete[1] as $val){
					if($val_debut<=$val){
						$tab_debut[].=$val;
					}
					if($val_fin<=$val){
						$tab_fin[].=$val;
					}
				}
				// on prend les mins afin d'avoir la valeur supérieur de tcin et tcout la plus proche
				//si aucune valeur n est supérieur à la val de fin, la valeur de fin sera la dernière valeur de <word stime de la chaine
				if(!isset($tab_fin)){
					$val_fin_sup=$val;
				}
				else
				$val_fin_sup=min($tab_fin);
				$val_debut_sup=min($tab_debut);

				 //on cherche la position des deux valurs trouvé en entrée
				$pos_timecode_deb=strpos($mySeq->t_doc['DOC_TRANSCRIPT'],'<Word stime="'.$val_debut_sup);
				$pos_timecode_fin=strpos($mySeq->t_doc['DOC_TRANSCRIPT'],'<Word stime="'.$val_fin_sup);
				//on cherche à la fin de la découpe la balise de fin et on rajoute 7 pour l'inclure
				$pos_timecode_fin=strpos($mySeq->t_doc['DOC_TRANSCRIPT'],'</Word>',$pos_timecode_fin)+7;

				$lg=($pos_timecode_fin)-$pos_timecode_deb;
				//on recupere tous le code entre les deux timecodes
				$doc_transcript=(substr($mySeq->t_doc['DOC_TRANSCRIPT'],$pos_timecode_deb,$lg));

				// recupérer la balise speechsegment juste au dessus du timecode de début
				//on tronque le texte du debut à la premiere position tcin, la balise speech qui va bien  est repéré par la dernière occurence de <speech> rencontré
				 $texte= (substr($mySeq->t_doc['DOC_TRANSCRIPT'],0,$pos_timecode_deb));

				 $debutSpeechsegment =  strrpos($texte,'<SpeechSegment');
				 //pour récupérer la balise entirement on coupe avant la balise word qui suit
				 $finSpeechsegment =   strpos($mySeq->t_doc['DOC_TRANSCRIPT'],'<Word stime="',$debutSpeechsegment);
				 $SpeechSegment_deb=(substr($mySeq->t_doc['DOC_TRANSCRIPT'],$debutSpeechsegment,($finSpeechsegment-$debutSpeechsegment)));


				//recupere la fin
				$mySeq->t_doc['DOC_TRANSCRIPT']=$DOC_TRANSCRIPT_ENTETE.$SpeechSegment_deb.$doc_transcript.'</SpeechSegment></SegmentList></AudioDoc>';
				return $mySeq->t_doc['DOC_TRANSCRIPT'];
			}else{
				return $this->t_doc['DOC_TRANSCRIPT'];
			}
		}


	/**
	 * Insère une séquence d'un document en base + lien avec matériel + image représentative + maj doc_seq parents
	 * IN : TC IN, TC OUT, DUREE, TITRE SEQUENCE, LANGUE
	 * OUT : Insertion en base
	 */
    function insertSequence($doc, $saveSolr = true) {
    	
    	global $db;
    	$mySeq=new Doc();

		if(!empty($doc['ID_DOC'])){
			$mySeq->t_doc['ID_DOC'] = $doc['ID_DOC'];
			$mySeq->getDoc();
		}
		$doc['DOC_DUREE']=secToTime(tcToSec($doc["DOC_TCOUT"])-tcToSec($doc["DOC_TCIN"]));
		$doc['DOC_ID_GEN']=$this->t_doc['ID_DOC'];
		$doc['DOC_ID_MEDIA']=$this->t_doc['DOC_ID_MEDIA']; //Héritage automatique
		if (empty($doc['DOC_ID_FONDS'])){
			$doc['DOC_ID_FONDS']=$this->t_doc['DOC_ID_FONDS'];
		}
		

		if(!isset($mySeq->t_doc['DOC_DATE_PROD']) || empty($mySeq->t_doc['DOC_DATE_PROD'])){
			$doc['DOC_DATE_PROD']=$this->t_doc['DOC_DATE_PROD'];
		}
    	if(!isset($mySeq->t_doc['IMAGEUR']) || empty($mySeq->t_doc['IMAGEUR'])){
			$doc['IMAGEUR']=$this->t_doc['IMAGEUR'];
		}

    	$mySeq->updateFromArray($doc,false);

    	$mySeq->t_doc['DOC_NUM']=$this->t_doc['DOC_NUM'];
      	$mySeq->t_doc['ID_LANG']=$this->t_doc['ID_LANG'];
		$mySeq->doc_trad=1;



	//XB: ajout transcription
		if(isset($this->t_doc['DOC_TRANSCRIPT']) && $this->t_doc['DOC_TRANSCRIPT']!=''){

			$mySeq->t_doc['DOC_TRANSCRIPT']=$this->saveDocTranscriptSequence($doc);
       // $mySeq->t_doc['DOC_TRANSCRIPT']=$this->t_doc['DOC_TRANSCRIPT'];
      //  $mySeq->saveDocTranscriptSequence();

		}

		//trace(print_r($mySeq->t_doc,true));
        // VP 17/07/2012 : analyse du POST dans frame/processSequence
//      	// By LD 24/04/08 => ajout récup tableaux LEX et VAL
//      	$FORM=$_POST; //Pour le moment, on analyse directement le POST, il faudrait faire passer le POST dans $doc
//      					//dans frame/processSequence
//		if (!empty($FORM['t_doc_lex']))
//     	foreach ($FORM['t_doc_lex'] as $idx=>$lex) {
//			if (isset($lex['ID_LEX']) || isset($lex['ID_PERS'])) {$lastID=$idx;$FORM['t_doc_lex'][$idx]['ID_DOC']=$mySeq->t_doc['ID_DOC'];}
//			if (isset($lex['DLEX_ID_ROLE'])) {$FORM['t_doc_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM['t_doc_lex'][$idx]);}
//			if (isset($lex['DLEX_ID_ROLE'])) {$FORM['t_doc_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM['t_doc_lex'][$idx]);}
//			if (isset($lex['ID_TYPE_LEX'])) {$FORM['t_doc_lex'][$lastID]['ID_TYPE_LEX']=$lex['ID_TYPE_LEX'];unset($FORM['t_doc_lex'][$idx]);}
//			if (isset($lex['DLEX_REVERSEMENT'])) {$FORM['t_doc_lex'][$lastID]['DLEX_REVERSEMENT']=$lex['DLEX_REVERSEMENT'];unset($FORM['t_doc_lex'][$idx]);}
//			if (isset($lex['LEX_PRECISION'])) {$FORM['t_doc_lex'][$lastID]['LEX_PRECISION']=$lex['LEX_PRECISION'];unset($FORM['t_doc_lex'][$idx]);}
//			if (isset($lex['ID_TYPE_DESC'])) {$FORM['t_doc_lex'][$lastID]['ID_TYPE_DESC']=$lex['ID_TYPE_DESC'];unset($FORM['t_doc_lex'][$idx]);}
//		}
//
//		if (!empty($FORM['t_doc_val']))
//		foreach ($FORM['t_doc_val'] as $idx=>$val) {
//			if (isset($val['ID_VAL'])) {$lastID=$idx;$FORM['t_doc_val'][$idx]['ID_DOC']=$mySeq->t_doc['ID_DOC'];}
//			if (isset($val['ID_TYPE_VAL'])){
//			if(!isset($FORM['t_doc_val'][$lastID]['ID_TYPE_VAL'])) {$FORM['t_doc_val'][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];}
//				unset($FORM['t_doc_val'][$idx]);
//			}
//		}

// si nouvelle séquence : affectation des t_doc_mat du parent à la sequence avant save ci dessous
		if(empty($doc['ID_DOC'])){
			$doc['t_doc_mat'] = array() ;
			foreach($this->t_doc_mat as $dmat){
				unset($dmat['ID_DOC_MAT']);
				unset($dmat['ID_DOC']);
				$dmat['DMAT_TCIN'] = $doc['DOC_TCIN'] ;
				$dmat['DMAT_TCOUT'] = $doc['DOC_TCOUT'] ;
				$doc['t_doc_mat'][] = $dmat;
			}
			if(!empty($doc['t_doc_mat'])){
				$mySeq->t_doc_mat = $doc['t_doc_mat'];
			}
			// initialisation du flag permettant l'appel de saveDocMat(false) => sauvegarde des liens doc_mat sans création de matériels
			$save_seq_doc_mat = true ; 
		}

		//Source import (mapFields) ? on la prend, sinon on prend la source formulaire
		if (!empty($doc['t_doc_lex']))	$mySeq->t_doc_lex=$doc['t_doc_lex']; //else $mySeq->t_doc_lex=$FORM['t_doc_lex'];
		if (!empty($doc['t_doc_val']))  $mySeq->t_doc_val=$doc['t_doc_val']; //else $mySeq->t_doc_val=$FORM['t_doc_val'];
		// Fin des modifs du 24/04/08
		if (!empty($doc['t_doc_cat']))  $mySeq->t_doc_cat=$doc['t_doc_cat'];

    	//trace(print_r($mySeq,true));
    	$rtn=$mySeq->save(0,false);
    	if (!$rtn) {$this->dropError(kErrorCreationDocSeq." : ".$mySeq->error_msg);return false;}

    	$idExtrait=$mySeq->t_doc['ID_DOC'];
		if(isset($save_seq_doc_mat) && !empty($save_seq_doc_mat)){
			// sauvegarde des liens doc_mat sans création de matériels dans le cas d'une nouvelle sequence uniquement. 
			// pourrait etre étendue au cas standard (meme pour les updates de sequence existante), mais necessite nettoyage de la fonction savesequence en parallele. 
			$mySeq->saveDocMat(false);
		}
    	$mySeq->saveDocLex();
    	$mySeq->saveDocVal();
    	$mySeq->saveDocCat(false);
    	//$mySeq->saveVersions(); pas utile car on crée déjà les versions au moment du SAVE
    	$mySeq->updateChampsXML('DOC_LEX');$mySeq->updateChampsXML('DOC_VAL');$mySeq->updateChampsXML('DOC_XML');$mySeq->updateChampsXML('DOC_CAT');



		/*Vignette automatique, by LD 27/12/07
		On récupère la liste des id_mat du parent, car c'est les mêmes que pour le fils.
		NOTE : à voir si cette dernière règle est vraie, stt en cas de chevauchement de matériels sur les TC
		pour le moment, on considère que c'est le cas... */
        $matListe=array();
		if (empty($mySeq->t_doc_mat)) $mySeq->getMats();
		foreach ($mySeq->t_doc_mat as $dmat) {
			if(empty($dmat['DMAT_INACTIF'])) {
				$matListe[]=$dmat['ID_MAT'];
			}
		}

		
		if(isset($doc['DOC_ID_DOC_ACC']) && !empty($doc['DOC_ID_DOC_ACC']) && empty($doc['DOC_ID_IMAGE'])){
			$mySeq->saveVignette('doc_acc',$doc['DOC_ID_DOC_ACC']);
				
		}else if(!empty($mySeq->t_doc['ID_DOC']) && !isset($mySeq->t_doc['DOC_ID_IMAGE'])){
			$tmp_img = getItem("t_doc","DOC_ID_IMAGE",array("ID_DOC"=>$idExtrait),array("ID_DOC"=>"ASC"));
			$mySeq->t_doc['DOC_ID_IMAGE'] = $tmp_img['DOC_ID_IMAGE']; unset($tmp_img);
		}

		if (!empty($matListe)) {
			$sql="SELECT DISTINCT i.ID_IMAGE, i.IM_TC
						FROM t_image i
						INNER JOIN t_mat m ON i.ID_IMAGEUR=m.MAT_ID_IMAGEUR
						WHERE i.IM_TC>='".$mySeq->t_doc["DOC_TCIN"]."' AND m.ID_MAT IN ('".implode("','",$matListe)."') AND i.IM_TC<='".$mySeq->t_doc["DOC_TCOUT"]."'
						ORDER BY i.IM_TC";

			$arr_id_img=$db->getCol($sql); //On récupère juste l'id de la première image trouvée
			//trace('VIGNETTE SEQUENCE '.$id_img.' >>> '.$sql);

			// trace(s"arr_id_img : ".print_r($arr_id_img,true));
			
			if(empty($mySeq->t_doc['DOC_ID_DOC_ACC']) && (empty($mySeq->t_doc['DOC_ID_IMAGE']) || !in_array($mySeq->t_doc['DOC_ID_IMAGE'],$arr_id_img))){
				$id_img = $arr_id_img[0];
				if (!empty($id_img)) $db->Execute("UPDATE t_doc set DOC_ID_IMAGE=".intval($id_img)." WHERE ID_DOC=".intval($idExtrait));
				}
			}

		// VP 18/05/10 : sauvegarde Sinequa pour les séquences
		// MS déplacé en fin de fonction => permet l'export vers solr de la vignette de la séquence...
		if (defined("useSinequa") && useSinequa) $mySeq->saveSinequa();

		if ($saveSolr && ( (defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true) ))
            $mySeq->saveSolr();

        $this->dropError($mySeq->error_msg); //transmission


		//Maj DOC_SEQ
		$this->updateParentTC();
		unset ($mySeq);
		return $idExtrait;
    }

    function updateSequence($doc) { //OBSOLETE
       	global $db;
       	//$doc["DOC_TCIN"],$doc["DOC_TCOUT"],trim($doc["DOC_TITRE"]),$doc['ID_DOC'],$this->t_doc['ID_LANG'],$doc["DOC_ID_TYPE_DOC"]

       	$db->StartTrans();
       	$duree = secToTime(tcToSec($doc["DOC_TCOUT"])-tcToSec($doc["DOC_TCIN"]));
        // Infos dépendants de ID_LANG
        $sql="update t_doc set DOC_TITRE=".$db->Quote(trim($doc["DOC_TITRE"]))." where ID_DOC=".$db->Quote($doc['ID_DOC'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
        $db->Execute($sql);
        // Infos indépendantes de ID_LANG
        $sql="update t_doc set DOC_ID_GEN=".$db->Quote($this->t_doc['ID_DOC']).", DOC_DUREE=".$db->Quote($duree).", DOC_TCIN=".$db->Quote($doc["DOC_TCIN"]).", DOC_TCOUT=".$db->Quote($doc["DOC_TCOUT"]).",DOC_ID_USAGER_MODIF='".$this->id_usager."', DOC_DATE_MOD=NOW()";
		if(!empty($doc["DOC_ID_TYPE_DOC"])) $sql.=", DOC_ID_TYPE_DOC=".$db->Quote($doc["DOC_ID_TYPE_DOC"]);
		$sql.=" where ID_DOC=".intval($doc["ID_DOC"]);
       	$db->Execute($sql);
		// Liens materiels
		$sql="UPDATE t_doc_mat set DMAT_TCIN=".$db->Quote($doc["DOC_TCIN"]).",DMAT_TCOUT=".$db->Quote($doc["DOC_TCOUT"])." where ID_DOC=".intval($doc["ID_DOC"]);
		$db->Execute($sql);

 		$rtn=$db->CompleteTrans();
		if (!$rtn) $this->dropError(kErrorSauveDocSeq);
    }

	/**
	 * Sauvegarde des séquences par appel de insertSequence() ou updateSequence() suivant le cas
	 * IN :
	 * OUT : Insertion en base
	 */
    function saveSequences($saveSolr = true) {
        if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
    	global $db;
    	$db->StartTrans();
		$doc_seq_tc="";
		//$db->Execute("UPDATE t_doc SET DOC_ID_GEN='0' WHERE DOC_ID_GEN=".$db->Quote($this->t_doc['ID_DOC']));
		foreach ($this->t_doc_seq as $idx=>$doc) {
			if (!empty($doc['DOC_TITRE'])) { //by LD 27/12=> controle doc_titre pour les imports
				$doc_seq_tc.=$doc["DOC_TITRE"].";";
				if (empty($doc['ID_DOC'])) {
					$idExtrait=$this->insertSequence($doc, $saveSolr);
	                // I.B.3.c. Liens materiels => on resynchronise les tc in/out de la séquence avec ceux du doc_mat
					if ($idExtrait) {
						$this->t_doc_seq[$idx]['ID_DOC'] = $idExtrait;
						//cas nouvelle séquence, si constante definie, flag de découpe passe à true
						if(defined("gDecouperSequence") && defined("gDecouperSequenceEtape")  && gDecouperSequence ){
							$trigger_decoupe_seq = true ;
						}
					}
				} else {
					$this->insertSequence($doc, $saveSolr);
					$rs = $db->Execute("SELECT * FROM t_doc_mat where ID_DOC=".intval($doc["ID_DOC"]));
					$tmp_results = $rs->getArray();


					// cas ancienne séquence, si constante découpage séquence à true && nouveau timecodes != anciens timecodes => flag découpe passe à true
					if(defined("gDecouperSequence") && defined("gDecouperSequenceEtape") && gDecouperSequence
					  && ((tcToSecDec($tmp_results[0]['DMAT_TCIN']) != tcToSecDec($doc['DOC_TCIN'])) || (tcToSecDec($tmp_results[0]['DMAT_TCOUT']) != tcToSecDec($doc['DOC_TCOUT'])))){
						$trigger_decoupe_seq = true ;
					}

					$sql = $db->GetUpdateSQL($rs, array("DMAT_TCIN" => $doc["DOC_TCIN"], "DMAT_TCOUT" => $doc["DOC_TCOUT"]));
					if (!empty($sql)) $ok = $db->Execute($sql);
				}

				if(defined("gDecouperSequence") && defined("gDecouperSequenceEtape")  && gDecouperSequence){
					if (empty($this->t_doc_mat)) $this->getMats();
					foreach($this->t_doc_mat as $dmat){
						if(isset($dmat['MAT']) && $dmat['MAT']->t_mat['MAT_TYPE'] == 'VISIO'){
							$mat_visio = $dmat['MAT'];
							if (defined("gListePatternVisio")) $mat_nom_seq = str_replace(unserialize(gListePatternVisio),'_'.((isset($doc['ID_DOC']) && !empty($doc['ID_DOC']))?$doc['ID_DOC']:$idExtrait),stripExtension($dmat['MAT']->t_mat['MAT_NOM']));
							else $mat_nom_seq = str_replace('_vis','_'.((isset($doc['ID_DOC']) && !empty($doc['ID_DOC']))?$doc['ID_DOC']:$idExtrait),stripExtension($dmat['MAT']->t_mat['MAT_NOM']));
							// si on est dans le cas d'un update de séquence & que le materiel n'existe pas
							$nomatvis = true;
							if (defined("gListePatternVisio")) {
								foreach (unserialize(gListePatternVisio) as $pattern)
									if(!file_exists($mat_visio->getlieu().$mat_nom_seq.$pattern.getExtension($mat_visio->t_mat['MAT_NOM'])))
										$nomatvis = false;
							}
							else $nomatvis = !file_exists($mat_visio->getlieu().$mat_nom_seq."_vis.".getExtension($mat_visio->t_mat['MAT_NOM']));
							if((defined("gForceRegenVisSequence") && gForceRegenVisSequence) || $nomatvis){
								$trigger_decoupe_seq = true ;
							}

							if(!empty($doc['ID_DOC']) && $trigger_decoupe_seq){
								// vérification que le job d'encodage/découpe de la séquence n'existe pas deja
								$jobs = $db->GetArray("SELECT * from t_job where JOB_ID_ETAT<=".jobEnCours." AND JOB_OUT LIKE ".$db->Quote($mat_nom_seq."%")." AND ID_JOB_PREC=0");
								// si jobs d'encodage de la séquence existent deja, on récupère les timecodes
								foreach($jobs as $job){
									if(!empty($job) &&  preg_match("|<TcIn>(.*)</TcIn+>|i",$job["JOB_PARAM"],$matches_in) && preg_match("|<TcOut>(.*)</TcOut+>|i",$job["JOB_PARAM"],$matches_out)){
										$job_tcin = $matches_in[1];
										$job_tcout = $matches_out[1];
										// si les timecodes sont égaux
										if($job_tcin ==  $doc["DOC_TCIN"] && $job_tcout ==  $doc["DOC_TCOUT"]){
											// alors la génération n'est pas necessaire => on annule la génération
											$trigger_decoupe_seq = false ;
										}else{
											// génération necessaire, mais désactivation du job en cours
											$db->Execute("UPDATE t_job set JOB_ID_ETAT=".jobEnDemandeAnnulation." WHERE ID_JOB=".$job['ID_JOB']." OR ID_JOB_PREC=".$job['ID_JOB']);
										}
									}
								}
							}
							break ;
						}
					}
				}
				// génération du job de découpe de la séquence
				if(defined("gDecouperSequence") && defined("gDecouperSequenceEtape") && gDecouperSequence && isset($trigger_decoupe_seq) && $trigger_decoupe_seq && isset($mat_visio)){

				/*
					// Methode par etape => deprecated puisqu'on a besoin de générer les storyboard également.
					// on garde la trace pr l'instant, à supprimer une fois le procédé par proc verifié
					require_once(modelDir.'model_etape.php');
					$etape = new Etape();
					$etape->t_etape['ID_ETAPE'] = gDecouperSequenceEtape;
					$etape->getEtape();
					$tcin = $doc['DOC_TCIN'];
					$tcout = $doc['DOC_TCOUT'];
					$offset=(empty($mat_visio->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_visio->t_mat['MAT_TCIN']);
					if($offset != "00:00:00:00"){
						$tcin=frameToTC(tcToFrame($doc['DOC_TCIN']) - tcToFrame($offset));
						$tcout=frameToTC(tcToFrame($doc['DOC_TCOUT']) - tcToFrame($offset));
					}

					$params = '<param><tcin>'.$tcin.'</tcin><tcout>'.$tcout.'</tcout><DESACTIVE_IN>1</DESACTIVE_IN><ID_DOC>'.((isset($doc['ID_DOC']) && !empty($doc['ID_DOC']))?$doc['ID_DOC']:$idExtrait).'</ID_DOC></param>';
					$dynArgs = array("rename"=>$mat_nom_seq);
					$etape->createJob($mat_visio->t_mat['ID_MAT'],$mat_visio->t_mat['MAT_NOM'],0,0,$jobValid,$jobDecoupe,$params,$dynArgs);

					$trigger_decoupe_seq = false ; */
					require_once(modelDir.'model_processus.php');
					$procDecoup = new Processus();
					$procDecoup->t_proc['ID_PROC'] = gDecouperSequenceProc;
					$procDecoup->getProc();
					$procDecoup->getProcEtape();
					$tcin = $doc['DOC_TCIN'];
					$tcout = $doc['DOC_TCOUT'];
					$offset=(empty($mat_visio->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_visio->t_mat['MAT_TCIN']);
					if($offset != "00:00:00:00"){
						$tcin=frameToTC(tcToFrame($doc['DOC_TCIN']) - tcToFrame($offset));
						$tcout=frameToTC(tcToFrame($doc['DOC_TCOUT']) - tcToFrame($offset));
					}

					$params = '<param><tcin>'.$tcin.'</tcin><tcout>'.$tcout.'</tcout><COPY_IMAGEUR>1</COPY_IMAGEUR><DESACTIVE_IN>1</DESACTIVE_IN><ID_DOC>'.((isset($doc['ID_DOC']) && !empty($doc['ID_DOC']))?$doc['ID_DOC']:$idExtrait).'</ID_DOC></param>';
					$dynArgs = array("rename"=>$mat_nom_seq);

					$procDecoup->createJobs($mat_visio->t_mat['ID_MAT'],$mat_visio->t_mat['MAT_NOM'],$params,$idJobDecoupe,$dynArgs);


					$trigger_decoupe_seq = false ;


				}
			}
		}
		// Mise à jour champ DOC_SEQ_TC
		$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']));
		$sql = $db->GetUpdateSQL($rs, array("DOC_SEQ_TC" => $doc_seq_tc));
		if (!empty($sql))
		{
			$result = $db->Execute($sql);
			if ($result!=null)
				$result->Close();
		}



		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocSeq); return false;} else return true;

    }


    function updateMateriels() {
    	global $db;
    	foreach ($this->t_doc_mat as $key=>$mat) {
    		if ($mat['action']=='update') {
    			$rs = $db->Execute("SELECT * FROM t_doc_mat WHERE id_doc=".intval($this->t_doc['ID_DOC'])." AND id_mat=".intval($mat['ID_MAT']));
				$sql = $db->GetUpdateSQL($rs, $mat);
				if (!empty($sql)) $rtn = $db->Execute($sql);
    			if (!$rtn) $this->dropError(kErrorSauveDocMat);
    		}
    	}
    }


    /** Récupération des liens inter-documents non hiérarchiques
    */
    function getDocLiesSRC($default_sort="dl.dd_ordre") {
		global $db;
		$sql="select doc.*,td.TYPE_DOC, td.ID_TYPE_DOC, dl.* , im.ID_IMAGEUR, im.IM_CHEMIN, im.IM_FICHIER, im.ID_IMAGE, da.DA_CHEMIN, da.DA_FICHIER, da.ID_DOC_ACC ";
		$sql .= ", case when im.IM_FICHIER is not null THEN ".$db->concat("'/storyboard/'", 'im.IM_CHEMIN', 'im.IM_FICHIER')." END as VIGNETTE ";
		$sql .= " from t_doc doc ";
		$sql.="INNER JOIN t_doc_lien dl ON dl.ID_DOC_DST=doc.id_doc ";
		$sql.="LEFT OUTER JOIN t_type_doc td ON td.ID_TYPE_DOC=doc.DOC_ID_TYPE_DOC and td.ID_LANG='".$this->t_doc['ID_LANG']."' ";
		$sql.="LEFT OUTER JOIN t_image im ON im.ID_IMAGE=doc.DOC_ID_IMAGE and im.ID_LANG='".$this->t_doc['ID_LANG']."' ";
		$sql.="LEFT OUTER JOIN t_doc_acc da ON da.ID_DOC_ACC=doc.DOC_ID_DOC_ACC and da.ID_LANG='".$this->t_doc['ID_LANG']."' ";
        $sql.=" where dl.ID_DOC_SRC=".intval($this->t_doc['ID_DOC'])." and doc.ID_LANG='".$this->t_doc['ID_LANG']."'";
		if(!empty($default_sort)){
			$sql.=" order by ".$default_sort;
		}
		$this->arrDocLiesSRC=$db->GetAll($sql);
		if ($this->arrDocLiesSRC) $this->t_doc['CNT_DOC_LIES_SRC'] = count($this->arrDocLiesSRC);
		if ($this->arrDocLiesSRC) return true; else return false;
    }


    function getDocLiesDST($default_sort="dl.dd_ordre") {
		global $db;
        $sql="select doc.*,td.TYPE_DOC, td.ID_TYPE_DOC, dl.* , im.ID_IMAGEUR, im.IM_CHEMIN, im.IM_FICHIER, im.ID_IMAGE, da.DA_CHEMIN, da.DA_FICHIER, da.ID_DOC_ACC from t_doc doc ";
		$sql.="INNER JOIN t_doc_lien dl ON dl.ID_DOC_SRC=doc.id_doc ";
		$sql.="LEFT OUTER JOIN t_type_doc td ON td.ID_TYPE_DOC=doc.DOC_ID_TYPE_DOC and td.ID_LANG='".$this->t_doc['ID_LANG']."' ";
		$sql.="LEFT OUTER JOIN t_image im ON im.ID_IMAGE=doc.DOC_ID_IMAGE and im.ID_LANG='".$this->t_doc['ID_LANG']."' ";
		$sql.="LEFT OUTER JOIN t_doc_acc da ON da.ID_DOC_ACC=doc.DOC_ID_DOC_ACC and da.ID_LANG='".$this->t_doc['ID_LANG']."' ";
        $sql.=" where dl.ID_DOC_DST=".intval($this->t_doc['ID_DOC'])." and doc.ID_LANG='".$this->t_doc['ID_LANG']."'";
		if(!empty($default_sort)){
			$sql.=" order by ".$default_sort;
		}
		$this->arrDocLiesDST=$db->GetAll($sql);
		if ($this->arrDocLiesDST) $this->t_doc['CNT_DOC_LIES_DST'] = count($this->arrDocLiesDST);
		if ($this->arrDocLiesDST) return true; else return false;
    }

	function getDocFreres() {
		$arrReportPhotos = array();
		foreach ($this->arrDocLiesSRC as $idx=>$dlsrc){
			if (Reportage::checkReportageWithId($dlsrc['ID_DOC'])) {
				$tmpdoc = new Doc;
				$tmpdoc->t_doc['ID_DOC'] = $dlsrc['ID_DOC'];
				$tmpdoc->t_doc['ID_LANG'] =$_SESSION['langue'];
				$tmpdoc->getDocLiesDST();
				
				if (is_array($tmpdoc->arrDocLiesDST)){
					$arrReportPhotos = array_merge($arrReportPhotos, $tmpdoc->arrDocLiesDST);
				}
				unset($tmpdoc);
			}
		}
		if (is_array($arrReportPhotos) && count($arrReportPhotos) > 0) {
			foreach ($arrReportPhotos as $idx=>&$dldst) {
				if ($dldst['ID_DOC'] == $this->t_doc['ID_DOC']){
					unset($arrReportPhotos[$idx]);
				}
			}
			$this->arrDocFreres = $arrReportPhotos;
		}
		if ($this->arrDocFreres) return true; else return false;
	}
									
	// VP 23/06/09 : ajout critère purge
    function saveDocLies($purge=true) {
    	 if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
    	global $db;
    	$db->StartTrans();
		if($purge) $db->Execute('DELETE FROM t_doc_lien WHERE ID_DOC_DST='.intval($this->t_doc['ID_DOC']));
		if($purge) $db->Execute('DELETE FROM t_doc_lien WHERE ID_DOC_SRC='.intval($this->t_doc['ID_DOC']));
		$ordre=0;

		if (isset($this->arrDocLiesSRC) && !empty($this->arrDocLiesSRC) && is_array($this->arrDocLiesSRC))
		{
			foreach ($this->arrDocLiesSRC as $idx=>&$dl) {

				if (!empty($dl['ID_DOC_DST'])) {

					$dl['ID_DOC_SRC']=$this->t_doc['ID_DOC'];
					$dl['DD_ORDRE']=$ordre++;

					// $rs = $db->Execute("SELECT * FROM t_doc_lien WHERE id_doc_dst=-1");
					// $sql = $db->GetInsertSQL($rs, $dl);
					// if (!empty($sql)) $ok = $db->Execute($sql);
					$ok = $db->insertBase("t_doc_lien","id_doc_dst",$dl);



					//Héritage
					require_once(modelDir.'model_docReportage.php');
					if ($ok && Reportage::isReportageEnabled() && !empty($dl['ID_DOC_DST']) && Reportage::checkReportageWithId($dl['ID_DOC_DST'])) {
						$myRep = new Reportage;
						$myRep->t_doc['ID_DOC']=intval($dl['ID_DOC_DST']);
						$myRep->t_doc['ID_LANG']=$this->t_doc['ID_LANG'];
						$myRep->heritReportFields();
					}
					if ($ok && Reportage::isReportageEnabled() && !empty($this->t_doc['ID_DOC'])) {
						$this->heritFieldsToReport();
					}
				}
			}
		}

		$ordre=0;
		if (isset($this->arrDocLiesDST) && !empty($this->arrDocLiesDST) && is_array($this->arrDocLiesDST))
		{
			foreach ($this->arrDocLiesDST as $idx=>&$dl) {
				if (!empty($dl['ID_DOC_SRC'])) {
					$dl['ID_DOC_DST']=$this->t_doc['ID_DOC'];
					$dl['DD_ORDRE']=$ordre++;
					// $rs = $db->Execute("SELECT * FROM t_doc_lien WHERE id_doc_dst=-1");
					// $sql = $db->GetInsertSQL($rs, $dl);
					// if (!empty($sql)) $ok = $db->Execute($sql);
					$ok = $db->insertBase("t_doc_lien","id_doc_dst",$dl);



					//Héritage
					require_once(modelDir.'model_docReportage.php');
					if ($ok && get_class($this) == "Reportage" && !empty($dl['ID_DOC_SRC'])) {
						$myDoc = new Doc;
						$myDoc->t_doc['ID_DOC']=intval($dl['ID_DOC_SRC']);
						$myDoc->t_doc['ID_LANG']=$this->t_doc['ID_LANG'];
						$myDoc->heritFieldsToReport();
					}
				}
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocLies); return false;} else return true;

    }

	/**
	 * Sauve l'association à un imageur
	 */
	function saveImageur($imageur) {
		global $db;
    	$rs = $db->Execute("SELECT * FROM t_doc WHERE ID_DOC=".intval($this->t_doc['ID_DOC']));
		$sql = $db->GetUpdateSQL($rs, array("DOC_ID_IMAGEUR" => $imageur,"DOC_ID_IMAGE" => ""));
		if (!empty($sql)) $ok = $db->Execute($sql);
    	if (!$ok) {$this->dropError(kErrorDocImpossibleAssocierImageur);return false;}
    	$this->t_doc['DOC_ID_IMAGEUR']=$imageur;
    	$this->t_doc['DOC_ID_IMAGE']='';
    	return true;
	}

    /**
     * Mise à jour base + objet de la vignette représentative de la notice.
     * IN : from (vignette issue du storyboard ou des documents d'accompagnement ?), ID (id_doc_acc ou id_image)
     * OUT : base + objet t_doc mis à jour
     * NOTE : 2 champs peuvent héberger une image représentative, aussi pour éviter toute confusion, l'autre est vidé
     * 		  lorsque l'un est mis à jour si une image est donnée en paramètre
	 *		CF code pour une explication des cas possibles
     */
	// VP 12/10/09 : sauvegarde vignette d'après imageur et TC
	// VP 28/06/11 : ajout paramètre optionnel langue
	function saveVignette($from,$id,$imageur=null,$tcin=null, $lang=null) {
		//debug("saveVignette", "red", true);
		// trace("saveVignette");
		//if (empty($id)) return false; Viré car il faut pouvoir déselectionner une image
		global $db;
		if ($from=='doc_acc'){$fld='DOC_ID_DOC_ACC';
			$fld2='DOC_ID_IMAGE';
		} else{
			$fld='DOC_ID_IMAGE';
			$fld2='DOC_ID_DOC_ACC';
		}
		if(empty($id) && !empty($imageur)){
			if(!empty($this->t_doc['DOC_ID_MEDIA']))
				$id_media = $this->t_doc['DOC_ID_MEDIA'];
			else {
				$sqlMedia = "select distinct(DOC_ID_MEDIA) from t_doc WHERE ID_DOC = ".intval($this->t_doc['ID_DOC']);
				$id_media = $db->GetOne($sqlMedia);
			}
			// PREMIER CAS : id est vide mais pas imageur : on capture la première image d'après le TC
			if($id_media == 'V') {
                //VP 4/07/13 : cas où le story n'a qu'une image
                $countImages=$db->GetOne("select count(distinct ID_IMAGE) from t_image where ID_IMAGEUR=".$db->Quote($imageur)." and IM_TC>=".$db->Quote($tcin));
                $rs = $db->Execute("SELECT * FROM t_doc  WHERE ID_DOC=".intval($this->t_doc['ID_DOC']));
                if($countImages>1){
					$id_img = $db->getOne("(select id_image from t_image where ID_IMAGEUR=".$db->Quote($imageur)."  and IM_TC>".$db->Quote($tcin)." order by im_tc ASC LIMIT 1)");
					$sql = $db->GetUpdateSQL($rs, array("DOC_ID_IMAGE" => $id_img));
                }else{
					$id_img = $db->getOne("(select id_image from t_image where ID_IMAGEUR=".$db->Quote($imageur)."  and IM_TC>=".$db->Quote($tcin)." order by im_tc ASC LIMIT 1)");
					$sql = $db->GetUpdateSQL($rs, array("DOC_ID_IMAGE" => $id_img));
                }
			} else {
				//$sql="UPDATE t_doc,t_image SET DOC_ID_IMAGE=ID_IMAGE WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." and IMAGEUR=".$db->Quote($imageur)."
				//and IM_CHEMIN = (select min(IM_CHEMIN) from t_image where IMAGEUR=".$db->Quote($imageur).")";
				$rs = $db->Execute("SELECT * FROM t_doc  WHERE ID_DOC=".intval($this->t_doc['ID_DOC']));
				$id_img = $db->getOne("(select id_image from t_image where ID_IMAGEUR=".$db->Quote($imageur)."  order by IM_CHEMIN,IM_FICHIER ASC LIMIT 1)");
				$sql = $db->GetUpdateSQL($rs, array("DOC_ID_IMAGE" => $id_img));
			}
		} elseif (empty($id)){ // MODIFIE 10/10/2007 LD => ajout de l'ID_LANG pour différencier les vignettes par langue
			// DEUXIEME CAS : id est vide, SOIT parce que l'on a déselectionné une vignette, soit parce que la vignette
			// ne provient pas du mode en cours (ex : edition de doc_acc alors la vignette vient du SB)
			// Dans ce cas, on ne touche pas à l'autre champ.
				$rs = $db->Execute("SELECT * FROM t_doc  WHERE ID_DOC=".intval($this->t_doc['ID_DOC']));
				$sql = $db->GetUpdateSQL($rs, array($fld => $id));
			// 16/11 Commenté car le choix d'image par langue est compliqué niveau saisie: ." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);

		}else{
			// TROISIEME CAS : id non vide, on a bien sélectionné une vignette, on la sauve et on vide l'autre champ.
			$rs = $db->Execute("SELECT * FROM t_doc  WHERE ID_DOC=".intval($this->t_doc['ID_DOC']));
			$sql = $db->GetUpdateSQL($rs, array($fld => $id, $fld2 => "", ));
			// LD 27/11 retrait de l'ID_LANG = même vignette pour ttes langues." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
		}
		if(!empty($lang)) $sql.=" AND t_doc.ID_LANG=".$db->Quote($lang);
		//trace("saveVignette $sql");


		if (!empty($sql)) $ok = $db->Execute($sql);
		if(empty($id))
			$id = $db->GetOne("select ".$fld." from t_doc where ID_DOC = ".intval($this->t_doc['ID_DOC']));
		$this->t_doc[$fld]=$id;
		//debug("saveVignette : ".$sql, "red", true);
		// VP 06/04/10 : mise à jour DOC_ID_IMAGE et DOC_ID_DOC_ACC dans Sinequa
		if (defined("useSinequa") && useSinequa) $this->saveSinequa();

		if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))
            $this->saveSolr();

		$this->saveVignetteParent($from,$id,$imageur,$tcin, $lang);



		// ENREGISTREMENT VIGNETTE HD SI LE DOCUMENT APPARTIENT A UN PANIER DEFINI DANS kSaveVignetteAsHDForPanIds
		if(defined("kSaveVignetteAsHDForPanIds")){
			require_once(modelDir.'model_panier.php');
			$list_pans = Panier::listPanContainingDocs($this->t_doc['ID_DOC'],true);
			$arr_panHD_ids = explode(',',(string)(kSaveVignetteAsHDForPanIds));
			foreach($arr_panHD_ids as $panHD_id){
				if(in_array($panHD_id,$list_pans)){
					// le document appartient bien à un panier dit "HD"
					Imageur::refreshImagesPanHD($panHD_id,$this);
				}
			}
		}
		// ENREGISTREMENT VIGNETTE HD SI LE DOCUMENT APPARTIENT A UNE CATEGORIE DEFINIE DANS kSaveVignetteAsHDForCatIds
		if(defined("kSaveVignetteAsHDForCatIds")){
			if(empty($this->t_doc_cat)){
				$this->getCategories();
			}
			if(!empty($this->t_doc_cat)){
				$arr_catHD_ids = explode(',',(string)(kSaveVignetteAsHDForCatIds));
				foreach($this->t_doc_cat as $cat){
					if(in_array($cat['ID_CAT'],$arr_catHD_ids)){
						// le document appartient bien à une catégorie dite "HD"
						Imageur::refreshImagesCatHD($cat['ID_CAT'],$this);
					}
				}
			}
		}



	}

	function saveVignetteParent($from,$id,$imageur=null,$tcin=null, $lang=null) {
		global $db;

		if (empty($this->t_doc['ID_LANG'])) $this->t_doc['ID_LANG'] = $_SESSION['langue'];
		$this->getDocLiesSRC();

		foreach ($this->arrDocLiesSRC as $src) {
			$id_doc = $src['ID_DOC'];

			require_once(modelDir.'model_docReportage.php');
			if ((Reportage::checkReportageWithId($id_doc) && intval($src['DOC_ID_IMAGE']) == 0)
			    || 	( $src['DOC_ID_MEDIA'] == 'R' && intval($src['DOC_ID_IMAGE']) == 0  )){
				$myDoc=new Doc;
				$myDoc->t_doc['ID_DOC']=$id_doc;
				$myDoc->saveVignette($from,$id,$imageur,$tcin,$lang);
			}
		}
	}

	/** Récupère les TCIN et TCOUT d'un lien entre DOC et MATERIEL
	 *  IN : id_doc et id_mat
	 *  OUT : array(TCIN,TCOUT)
	 */
	static function getTC($id_doc,$id_mat,$format) {
		global $db;
		$sql="SELECT dm.DMAT_TCIN,dm.DMAT_TCOUT
				from t_doc_mat dm
				INNER JOIN t_mat m ON dm.ID_MAT=m.ID_MAT
				WHERE dm.ID_DOC=".intval($id_doc)." AND dm.ID_MAT=".intval($id_mat)."
				AND m.MAT_FORMAT=".$db->Quote($format);
		$rs=$db->GetAll($sql);
		if (empty($rs) ) { return array("-1","-1"); }
		else return array($rs[0]['DMAT_TCIN'],$rs[0]['DMAT_TCOUT']);

	}

	/** Effectue le mappage entre un tableau et des champs de doc
	 *
	 */
	function mapFields($arr) {

		global $db;
		require_once(modelDir.'model_personne.php');
		require_once(modelDir.'model_lexique.php');
		require_once(modelDir."model_val.php");
		require_once(modelDir.'model_materiel.php');
		require_once(modelDir.'model_imageur.php');
		require_once(modelDir.'model_docAcc.php');
		require_once(modelDir.'model_festival.php');
		require_once(modelDir."model_role.php");

		foreach ($arr as $idx=>$val) {
				$myField=$idx;
				//cas spécial ID_DOC, en cas de forçage de l'ID_DOC (par ex: gaumont)
				if ($myField=='ID_DOC') $this->t_doc['ID_DOC']=$val;
			//VP 30/08/10 : ajout cas ID_LANG
				if ($myField=='ID_LANG') $this->t_doc['ID_LANG']=$val;

				// analyse du champ
				$arrCrit=explode("_",$myField);
				switch ($arrCrit[0]) {

					case 'DOC': //champ table DOC
						$val=trim($val); //Important, car souvent, les tags ID3 ont des espaces supplémentaires
						$flds=explode(",",$myField);
                        if ($val != '') foreach ($flds as $_fld) {
                        	$this->t_doc[$_fld]=$val; // htmlentities à proscrire (VP)
                        }
					break;

					case 'CT': //table liée à la table DOC, recherche par texte
						$_dumb=explode("_",$myField,2);
						$table="t_".strtolower($_dumb[1]);
						$champ=strtoupper($_dumb[1]);
						$champId=strtoupper('ID_'.$_dumb[1]);
						$champDoc=strtoupper('DOC_ID_'.$_dumb[1]);
						$id=$db->GetOne("SELECT $champId from $table where $champ =".$db->Quote(trim($val)));
						if(empty($id)){
							$cols=$db->MetaColumns($table);
							if(array_key_exists('ID_LANG',$cols)){
							   // $rs = $db->Execute("SELECT * FROM $table  WHERE $champId=-1");
								// $sql = $db->GetInsertSQL($rs, array("ID_LANG" => $_SESSION['langue'], $champ => trim($val)));
								// if (!empty($sql)) $ok = $db->Execute($sql);

							   // $id=$db->getOne("SELECT max($champId) FROM $table");
							   $ok = $db->insertBase("$table","$champId", array("ID_LANG" => $_SESSION['langue'], $champ => trim($val)));
                               $id=$ok;


							}else{
								// $rs = $db->Execute("SELECT * FROM $table  WHERE $champId=-1");
								// $sql = $db->GetInsertSQL($rs, array($champ => trim($val)));
								// if (!empty($sql)) $ok = $db->Execute($sql);
								$ok = $db->insertBase("$table","$champId",array($champ => trim($val)));


							}
						}
						$this->t_doc[$champDoc]=$id;
					break;

					case 'L': //lexique
						if ($val!=='') {
							if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste

							foreach($val as $thisVal) {
								//@update VG 02/06/2010
								/*$myLex=new Lexique();
								//by LD 20/02/09, réimport OPSIS, ajout champ LEX_TERME & LEX_NOTE
								if ($thisVal['LEX_TERME'])
									$myLex->t_lexique['LEX_TERME']=$thisVal['LEX_TERME']; else
									$myLex->t_lexique['LEX_TERME']=$thisVal['VALEUR'];
								$myLex->t_lexique['LEX_ID_TYPE_LEX']=$arrCrit[3]; //par déft, extrait du champ

								if ($thisVal['LEX_NOTE']) $myLex->t_lexique['LEX_NOTE']=$thisVal['LEX_NOTE'];
								if ($thisVal['ID_LEX']) $myLex->t_lexique['ID_LEX']=$thisVal['ID_LEX'];
								if ($thisVal['LEX_ID_TYPE_LEX']) $myLex->t_lexique['LEX_ID_TYPE_LEX']=$thisVal['LEX_ID_TYPE_LEX'];

								$myLex->t_lexique['ID_LANG']=$this->t_doc['ID_LANG'];

								if ($thisVal['ID_LEX']) $exist=$myLex->checkExistId(); else $exist=$myLex->checkExist(false);

								if ($exist) $myLex->save(); else
								{
									$myLex->create(); //création langue en cours
									foreach ($_SESSION['arrLangues'] as $lg) {
										if (strtoupper($lg)!=strtoupper($this->t_doc['ID_LANG'])) $arrVersions[$lg]=$myLex->t_lexique;
									}
									$myLex->saveVersions($arrVersions); //on sauve la valeur dans toutes les langues
									unset($arrVersions);
								}*/

								$myLex=new Lexique();
								//by LD 20/02/09, réimport OPSIS, ajout champ LEX_TERME & LEX_NOTE
								if (!$thisVal['LEX_TERME']){
									$thisVal['LEX_TERME']=$thisVal['VALEUR'];
									//par defaut état valide
									$thisVal['LEX_ID_ETAT_LEX']=1;
								}
								if(!$thisVal['LEX_ID_TYPE_LEX'])
									$thisVal['LEX_ID_TYPE_LEX']=$arrCrit[3]; //par déft, extrait du champ

								$thisVal['ID_LANG']=$this->t_doc['ID_LANG'];

								$myLex->createFromArray($thisVal, true, false);
									
								// Création role si nécessaire
								$id_type_desc = ($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]);
								$dlex_id_role = ($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]);
								
								if(empty($dlex_id_role) && !empty($thisVal['ROLE'])){
									$arrRole=array();
									$arrRole['ID_TYPE_DESC'] = $id_type_desc;
									$arrRole['ID_LANG']=$this->t_doc['ID_LANG'];
									$arrRole['ROLE'] = $thisVal['ROLE'];
									$myRole = new Role();
									$dlex_id_role = $myRole->CreateFromArray($arrRole);
									unset($myRole);
								}

								//Lien doc_lex
								$this->t_doc_lex[]=array('DLEX_ID_ROLE'=>$dlex_id_role,
														 'ID_TYPE_DESC'=>$id_type_desc,
														 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
														 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
														 'ID_LEX'=>$myLex->t_lexique['ID_LEX']);
							}
						}
					break;


					case 'P': //personne
						if ($val!=='') {
							if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
							foreach($val as $thisVal) {

								$myPers=new Personne();
								if (!$thisVal['PERS_NOM'])
                                $thisVal['PERS_NOM']=$thisVal['VALEUR'];
                                if(!$thisVal['PERS_ID_TYPE_LEX'])
                                $thisVal['PERS_ID_TYPE_LEX']=$arrCrit[3]; //par déft, extrait du champ

                                $thisVal['ID_LANG']=$this->t_doc['ID_LANG'];

                                $myPers->createFromArray($thisVal, true, true, true);

								$id_type_desc = ($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]);
								$dlex_id_role = ($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]);
									
								if(empty($dlex_id_role) && !empty($thisVal['ROLE'])){
									$arrRole=array();
									$arrRole['ID_TYPE_DESC'] = $id_type_desc;
									$arrRole['ID_LANG']=$this->t_doc['ID_LANG'];
									$arrRole['ROLE'] = $thisVal['ROLE'];
									$myRole = new Role();
									$dlex_id_role = $myRole->CreateFromArray($arrRole);
									unset($myRole);
								}
									
								$tmp_doc_lex=array('DLEX_ID_ROLE'=>$dlex_id_role,
														 'ID_TYPE_DESC'=>$id_type_desc,
														 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
														 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
														 'PERS_ID_TYPE_LEX'=>$thisVal['PERS_ID_TYPE_LEX'],
														 'PERS_NOM'=>$thisVal['PERS_NOM'],
														 'ID_PERS'=>$myPers->t_personne['ID_PERS']);

								if($thisVal['PERS_PRENOM']){
									$tmp_doc_lex['PERS_PRENOM'] = $thisVal['PERS_PRENOM'];
								}

								$this->t_doc_lex[]= $tmp_doc_lex;
							}
						}
					break;

					case 'V': //valeur
						if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste

						//BY LD 20 02 09 => passage en tableau et mise en boucle
						foreach($val as $thisVal) {
								if(empty($thisVal['VALEUR'])) {
									continue;
								}
							// VP 15/02/10 : correction import multiple valeurs
								$myVal=new Valeur();
								//@update VG 02/06/2010
								//par défaut
								/*$myVal->t_val['VAL_ID_TYPE_VAL']=$arrCrit[1];

								$myVal->t_val['VALEUR']=$thisVal['VALEUR'];
								if (isset($thisVal['VAL_ID_TYPE_VAL'])) $myVal->t_val['VAL_ID_TYPE_VAL']=$thisVal['VAL_ID_TYPE_VAL'];

								$myVal->t_val['ID_LANG']=$this->t_doc['ID_LANG'];
								if ($thisVal['ID_VAL']) $myVal->t_val['ID_VAL']=$thisVal['ID_VAL'];

								if ($thisVal['ID_VAL']) $exist=$myVal->checkExistId(); else $exist=$myVal->checkExist(false);

								if ($exist) {
									$myVal->t_val['ID_VAL']=$exist;
									$myVal->save();
								} else {
									$myVal->create(); //création dans la langue
									foreach ($_SESSION['arrLangues'] as $lg) {
										if (strtoupper($lg)!=strtoupper($this->t_doc['ID_LANG'])) $arrVersions[$lg]=$val;
									}
									$myVal->saveVersions($arrVersions); //sauve dans autres langues
									unset($arrVersions);
								}*/

								$thisVal['VAL_ID_TYPE_VAL']=isset($thisVal['VAL_ID_TYPE_VAL'])?$thisVal['VAL_ID_TYPE_VAL']:$arrCrit[1];
								$thisVal['ID_LANG']=$this->t_doc['ID_LANG'];
								$myVal->createFromArray($thisVal, true, true);

								//NOTE : pour le ID_TYPE_VAL, on peut forcer le type via arrCrit pour ne pas avoir le même que VAL_ID_TYPE_VAL
								//Cette différence entre id_type_val et val_id_type_val existe chez qq sites (CDR)

                                                                /* B.RAVI 2015-06-05 Exemple: lors d'un import CSV, pour un champ tirant sa valeur (et crée (& testé si existe)) dans PAYS mais devant être inséré dans la                                                                        table t_doc_val avec PAPR (Pays de production), on teste si nouveau paramètre $arrCrit[2] */

                                                                if (isset($thisVal['ID_TYPE_VAL'])){
                                                                    $val_id_type_val=$thisVal['ID_TYPE_VAL'];
                                                                }elseif ($arrCrit[2]!=''){
                                                                    $val_id_type_val=$arrCrit[2];
                                                                }else {
                                                                    $val_id_type_val=$myVal->t_val['VAL_ID_TYPE_VAL'];
                                                                }

								$this->t_doc_val[]=array('ID_TYPE_VAL'=>$val_id_type_val,'ID_VAL'=>$myVal->t_val['ID_VAL']);
						}
					break;

					//update VG 20/09/10 : ajout du cas VI, qui permet de passer directement un id d'une valeur, sans tableau (cas spécial pour l'upload pour lequel les tableaux ne apssent pas)
					case 'VI': //id valeur
						if (!is_array($val)) {$_x[0]['ID_VAL']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste

						foreach($val as $thisVal) {
								$myVal=new Valeur();

								//NOTE : pour le ID_TYPE_VAL, on peut forcer le type via arrCrit pour ne pas avoir le même que VAL_ID_TYPE_VAL
								//Cette différence entre id_type_val et val_id_type_val existe chez qq sites (CDR)
								$this->t_doc_val[]=array('ID_TYPE_VAL'=>$arrCrit[1]!=''?$arrCrit[1]:$myVal->t_val['VAL_ID_TYPE_VAL'],'ID_VAL'=>$thisVal['ID_VAL']);
						}
					break;

					//update VG 20/09/10 : ajout du cas VI, qui permet de passer directement un id d'une valeur, sans tableau (cas spécial pour l'upload pour lequel les tableaux ne apssent pas)
					case 'LI': //id valeur


						if ($val!=='') {
							if (!is_array($val)) {$_x[0]['ID_LEX']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
							foreach($val as $thisVal) {
								//Lien doc_lex
								$this->t_doc_lex[]=array('DLEX_ID_ROLE'=>($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]),
														 'ID_TYPE_DESC'=>($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]),
														 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
														 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
														 'ID_LEX'=>$thisVal['ID_LEX']
													);
							}
						}
					break;

					case 'PI': //personne
						if ($val!=='') {
							if (!is_array($val)) {$_x[0]['ID_PERS']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
							foreach($val as $thisVal) {

								$myPers=new Personne();
								//LD 23/03/09, update depuis tableau + on importe que les champs présents
								$myPers->updateFromArray($thisVal, false);
								//rétro compatibilité
								if ($thisVal['VALEUR']) $myPers->t_personne['PERS_NOM']=$thisVal['VALEUR'];

								$this->t_doc_lex[]=array('DLEX_ID_ROLE'=>($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]),
														 'ID_TYPE_DESC'=>($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]),
														 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
														 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
														 'PERS_ID_TYPE_LEX'=>($thisVal['PERS_ID_TYPE_LEX']?$thisVal['PERS_ID_TYPE_LEX']:$arrCrit[3]),
														 'PERS_NOM'=>$thisVal['PERS_NOM'],
														 'ID_PERS'=>$thisVal['ID_PERS']);
							}
						}
					break;

					case 'CAT': //Catégorie
						if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste

						foreach($val as $thisVal) {
							$myCat=new Categorie();

							$thisVal['CAT_ID_TYPE_CAT']=isset($thisVal['CAT_ID_TYPE_CAT'])?$thisVal['CAT_ID_TYPE_CAT']:$arrCrit[1];
							$thisVal['ID_LANG']=$this->t_doc['ID_LANG'];
							if (!$thisVal['CAT_NOM'])
								$thisVal['CAT_NOM']=$thisVal['VALEUR'];
							$myCat->createFromArray($thisVal, true, true);
							$val_categ=$myCat->getValues();
							//NOTE : pour le ID_TYPE_CAT, on peut forcer le type via arrCrit pour ne pas avoir le même que CAT_ID_TYPE_CAT
							$this->t_doc_cat[]=array('ID_TYPE_CAT'=>$arrCrit[1]!=''?$arrCrit[1]:$val_categ['CAT_ID_TYPE_CAT'],'ID_CAT'=>$val_categ['ID_CAT']);
						}
						break;
					case 'CATID':
						if (!is_array($val))
						{
							$_x[0]['VALEUR']=$val;
							$val=$_x;
						} //pas de tableau ? on en crée un minimaliste

						foreach($val as $thisVal)
						{
							$this->t_doc_cat[]=array('ID_CAT'=>$thisVal['VALEUR']);
						}
						break;
					case 'MAT': //materiel
						if (!is_array($val) && $myField=="MAT_NOM") {
							$_x[0]=array($myField=>$val);
							$val=$_x;
						}
						// MS - ajout is_array, evite les warning sur foreach;
						if (is_array($val) &&  !empty($val)){
							foreach ($val as $thisMat) {
								$myMat=new Materiel();
								// VP (26/06/09) : remplacement updateFromArray par mapFields
								// VP (28/04/11) : ajout saveMatVal
								//$myMat->updateFromArray($thisMat);
								$myMat->mapFields($thisMat);
								if ($myMat->checkExist()) {
									$myMat->save();
									$myMat->saveMatVal(false);
									$myMat->saveMatPers(false);
								} else {
									$myMat->create(); //création
									$myMat->saveMatVal(false);
									$myMat->saveMatPers(false);
								}
								$this->t_doc_mat[]=array('ID_MAT'=>$myMat->t_mat['ID_MAT'],
														 'ID_DOC'=>$this->t_doc['ID_DOC'],
														 'DMAT_TCIN'=>$thisMat['DMAT_TCIN'],
														 'DMAT_TCOUT'=>$thisMat['DMAT_TCOUT'],
														 'DMAT_LIEU'=>$thisMat['DMAT_LIEU'],
														 'ID_DOC_MAT'=>$thisMat['ID_DOC_MAT']);
							}
						}
						break;

					case 'SEQ' : //séquences
						// VP 21/09/10 : traitement seq par mapfields
						foreach($val as $thisVal) {

							if (strpos($thisVal['DOC_TCIN'],":")===false) //juste des minutes ou des secs
								$thisVal['DOC_TCIN']=secToTc($thisVal['DOC_TCIN']);
							if (strpos($thisVal['DOC_TCOUT'],":")===false) //juste des minutes ou des secs
								$thisVal['DOC_TCOUT']=secToTc($thisVal['DOC_TCOUT']);
							if (!empty($thisVal['DOC_TITRE'])) {
								$mySeq=new Doc();
								$mySeq->mapFields($thisVal);
								$mySeq->doc_trad=1;
								$mySeq->save(0,false);
								if(!empty($mySeq->t_doc_val)) {
									$mySeq->saveDocLex(false);
									$mySeq->updateChampsXML('DOC_LEX');
								}
								if(!empty($mySeq->t_doc_lex)) {
									$mySeq->saveDocVal(false);
									$mySeq->updateChampsXML('DOC_VAL');
								}
								if(!empty($mySeq->t_doc_cat)) {
									$mySeq->saveDocCat(false);
									$mySeq->updateChampsXML('DOC_CAT');
								}
								$mySeq->updateChampsXML('DOC_XML');

								$this->t_doc_seq[]=array('ID_DOC'=>$mySeq->t_doc['ID_DOC'],
														 'DOC_TITRE'=>$mySeq->t_doc['DOC_TITRE'],
														 'DOC_TCIN'=>$mySeq->t_doc['DOC_TCIN'],
														 'DOC_TCOUT'=>$mySeq->t_doc['DOC_TCOUT']);
								debug($mySeq,'lightblue');
								unset($mySeq);
							}
						}
						break;

					//by LD 20 02 09 => import de storyboard
					case 'IMG': //image, storyboard
						foreach ($val as $thisImg) {
							$myImg=new Image();
							$myImg->updateFromArray($thisImg);

							//création imageur via sql, plus simple
							$ok=$db->getOne("SELECT IMAGEUR from t_imageur where IMAGEUR=".$db->Quote($thisImg['IMAGEUR']));
							if (!$ok) {
								$rs = $db->Execute("SELECT * FROM t_imageur  WHERE IMAGEUR is null");
								$sql = $db->GetInsertSQL($rs, array('IMAGEUR' => $thisImg['IMAGEUR']));
								if (!empty($sql)) $ok = $db->Execute($sql);
							}

							if ($thisImg['ID_IMAGE']) $exist=$myImg->checkExistId(); else $exist=$myImg->checkExist();
							if ($exist) {
								$ok=$myImg->save(); }
							else {
								$ok=$myImg->create($thisImg['ID_IMAGE']); //création
							}
							if($ok) $this->t_images[]=$myImg;
							unset($ok);	unset($myImg);
						}
					break;

					//by LD 20 02 09 => import de doc accompagnement
					case 'DA': //document accompagnement
						foreach ($val as $thisDA) {
							// VP 22/06/09 : sauvegarde doc_acc par saveDocAcc() pour avoir un ID_DOC
							$this->t_doc_acc[]=$thisDA;
							/*
							$myDA=new DocAcc();
							$myDA->updateFromArray($thisDA);
							if ($myDA->checkExist()) {
								$ok=$myDA->save();
							}
							else {
								$ok=$myDA->create();
							}
							if ($ok) $this->t_doc_acc[]=$myDA;
							unset($ok);	unset($myDA);
							 */
						}
					break;

					//by LD 20 03 09 => import de festival
					case 'FEST': // festival

						foreach ($val as $thisFest) {
							$myFest=new Festival();
							$myFest->updateFromArray($thisFest);

							$ok=$myFest->save();

							$this->t_doc_fest[]=array('ID_FEST'=>$myFest->t_festival['ID_FEST'],'DOC_FEST_VAL'=>$thisFest['DOC_FEST_VAL']);

							unset($ok);	unset($myFest);
						}
					break;

					case 't':
						if ($myField=='t_doc_val')
						{
							$myDoc->t_doc_val=$val;
						}
						break;


			}
		}
	}

	// VP 22/06/09 : ajout méthode saveDocAcc, utilisée dans importXML
	function saveDocAcc(){
		foreach ($this->t_doc_acc as $thisDA) {
			if(!($thisDA instanceof DocAcc)) {
				if(empty($thisDA['ID_DOC'])) $thisDA['ID_DOC']=$this->t_doc['ID_DOC'];
				if(empty($thisDA['ID_LANG'])) $thisDA['ID_LANG']=$this->t_doc['ID_LANG'];
				$myDA=new DocAcc();
				// VP 15/02/10 : utilisation de mapFields à la place de updateFromArray
				//$myDA->updateFromArray($thisDA);
				$myDA->mapFields($thisDA);
			} else {
				if(empty($thisDA->t_doc_acc['ID_DOC'])) $thisDA->t_doc_acc['ID_DOC']= $thisDA->t_doc['ID_DOC'];
				if(empty($thisDA->t_doc_acc['ID_LANG'])) $thisDA->t_doc_acc['ID_LANG']= $thisDA->t_doc['ID_LANG'];
				$myDA = $thisDA;
			}
			$id_doc_acc = $myDA->checkExist();
			if ($id_doc_acc) {
				// MS 17/10/12 si on a trouvé le doc_acc, on update l'objet myDa avant de sauver.
				$myDA->t_doc_acc['ID_DOC_ACC'] = $id_doc_acc;
				$okDA=$myDA->save();
			}
			else {

				foreach ($_SESSION['arrLangues'] as $lg) {
					$myDA->t_doc_acc['ID_LANG'] = $lg;
					$okDA=$myDA->create(false);
				}
			}
			if($okDA) $okDA=$myDA->saveDocAccVal();
			if (!$okDA) {
				$ok=$okDA;
				$this->dropError($myDA->error_msg);
			}
			unset($okDA);
			unset($myDA);
		}
		return $ok;
	}

	function insererDocMat($id_mat) { //OBSOLETE, à supprimer quand on aura abandonné Orao
	    global $db;

	    // Recherche des materiels lies
	    $tab=array();
	    // Materiels et fils
	    $sql="select T1.ID_MAT,T1.MAT_TCIN,T1.MAT_TCOUT from t_mat T1 where T1.ID_MAT=".intval($id_mat)." or T1.MAT_COTE_ORI=".$db->Quote($id_mat);
	    $result=$db->Execute($sql);
	    while($row=$result->FetchRow()){
	        $tab[]=$row;
	    }
	    $result->Close();
	    // Materiels ayant meme original
	    $sql="select T1.ID_MAT,T1.MAT_TCIN,T1.MAT_TCOUT from t_mat T1,t_mat T2 where T1.ID_MAT!=".intval($id_mat)." and T1.MAT_COTE_ORI !=".$db->Quote($id_mat);
	    $sql.=" and T2.ID_MAT=".intval($id_mat)." and T1.MAT_COTE_ORI=T2.MAT_COTE_ORI and T2.MAT_COTE_ORI != ''";
	    $result=$db->Execute($sql);
	    while($row=$result->FetchRow()){
	        $tab[]=$row;
	    }
	    $result->Close();
	    // Materiel p?re
	    $sql="select T1.ID_MAT,T1.MAT_TCIN,T1.MAT_TCOUT from t_mat T1,t_mat T2 where T1.ID_MAT||''=T2.MAT_COTE_ORI and T2.ID_MAT =".intval($id_mat)." and T2.MAT_COTE_ORI != ''";
	    $result=$db->Execute($sql);
	    while($row=$result->FetchRow()){
	        $tab[]=$row;
	    }
	    $result->Close();
	    $tab=array_unique($tab);
	    // Insertion dans t_doc_mat
	    foreach($tab as $k => $mat){
	        $id_mat_row=$mat['ID_MAT'];
	        $exist=$db->Execute("SELECT ID_MAT FROM t_doc_mat where ID_MAT=".intval($id_mat_row)." and ID_DOC=".$this->t_doc['ID_DOC']);
	        if($exist->RecordCount()==0){
				$dmat_tcin=(($this->t_doc['DOC_TCIN']=="00:00:00:00"||$this->t_doc['DOC_TCIN']=="")? $mat["MAT_TCIN"]:$this->t_doc['DOC_TCIN']);
				$dmat_tcout=(($this->t_doc['DOC_TCOUT']=="00:00:00:00"||$this->t_doc['DOC_TCOUT']=="")? $mat["MAT_TCOUT"]:$this->t_doc['DOC_TCOUT']);
	            $db->Execute("insert into t_doc_mat (ID_DOC,ID_MAT,DMAT_TCIN,DMAT_TCOUT) values (".$this->t_doc['ID_DOC'].",".intval($id_mat_row).",'".$dmat_tcin."','".$dmat_tcout."')");
	        }
	    }
		// I.D.3. Mise ? jour du champs DOC_IMAGEUR de t_doc avec l'imageur du mat?riel selectionn? (s'il en a un) et si le doc n'en a pas d?j?
		if($this->t_doc['DOC_ID_IMAGEUR']==""){
			$result = $db->Execute("select DISTINCT t_mat.MAT_ID_IMAGEUR from t_mat WHERE t_mat.ID_MAT=".intval($id_mat));
			while($list=$result->FetchRow()){
				if ($list["MAT_ID_IMAGEUR"]!=""){
					$db->Execute("update t_doc set DOC_ID_IMAGEUR =".$db->Quote($list["MAT_ID_IMAGEUR"])." where ID_DOC=".intval($this->t_doc['ID_DOC']));
					// S?lection image repr?sentative
					$result1 = $db->Execute("select ID_IMAGE from t_image where ID_IMAGEUR='".$list["MAT_ID_IMAGEUR"]."' and IM_TC=(select min(IM_TC) from t_image where ID_IMAGEUR='".$list["MAT_ID_IMAGEUR"]."' and IM_TC>='".$this->t_doc['DOC_TCIN']."')");
					if($list1=$result1->FetchRow())
						$db->Execute("update t_doc set DOC_ID_IMAGE ='".$list1["ID_IMAGE"]."' where ID_DOC=".intval($this->t_doc['ID_DOC']));
					else $db->Execute("update t_doc set DOC_ID_IMAGE ='' where ID_DOC=".intval($this->t_doc['ID_DOC']));
				}
			}
		}
		// I.D.4. Met ? jour le champs indiquant qu'un document est num?ris? si le materiel qui lui est associ? est en ligne
	    $this->metAJourDocNum($id_mat);

	}

	/**
	 * Indexation de la fiche pour le moteur Sinequa
	 * IN : objet Doc, tableau de champs à mettre à jour, path script indexation sinequa sur serveur
	 * OUT : true / false, MAJ DB Sinequa
	 * NOTES : l'indexation utilise deux modes
	 * 	1. si un tableau de champs est passé et que ce champ appartient aux colonnes SINEQUA, alors UPDATE champ
	 *  2. dans les autres cas, on procède à une réindexation complète de la fiche
	 * 		- récupération du script d'indexation sineque sur le serveur et mise en forme
	 * 		- export XML des données du document, puis ingestion par le xsl d'export sinequa
	 * 		- insert dans la base de la ligne, en mode replace
	 *  (Attention, le mode replace crée des ghosts en base, il faut donc réindexer régulièrement)
	 */
	function saveSinequa($arrFlds=array()) {
		global $db;
		require_once(libDir."sinequa/fonctions.inc");
		require_once(libDir."sinequa/Intuition.inc");

		if (!defined("useSinequa") || !defined("kPathSinequaScriptCreation") || useSinequa==false) {
			$this->dropError(kErrorDocSinequaParametres);
			return false;
		}
		//Récupération du script qui d'indexation qui définit les options d'indexation
		$content=file_get_contents(kPathSinequaScriptCreation); //ouverture du script d'indexation sur le serveur
		if (empty($content)) {
			$this->dropError(kErrorDocSinequaLectureScriptCreation);return false;
		}
		$content=split("\n",$content); //split par line
		foreach ($content as $line) {
			$line=trim($line,"\r");
			if ($line[0]!=';' && !empty($line)) //si contenu et pas commentaire
			$options.=" ".trim($line); //concaténation
			$_dumb=preg_match("/--xml-fields=(.*)=(.*)/i",$line,$out); //récupération de la liste des colonnes
			if ($out[2]) $colonnesXML[]=$out[2];
		}
		//on est dans le mode 1 => colonnes
		if (!empty($arrFlds) && is_array($arrFlds)) {
			$fields2update=array_intersect($arrFlds,$colonnesXML); //on obtient la liste des champs pour màj

			if (!empty($fields2update)) {
			$sql="UPDATE ".sinequa_database." SET ";
			foreach ($fields2update as $fld) $sql.=" ".$fld."=".$db->Quote($this->t_doc[$fld]).",";
			// VP (9/11/09) : champ ID_LANG optionnel
			$sql=substr($sql,0,-1)." WHERE ID_DOC=".intval($this->t_doc['ID_DOC']);
			if(!empty($this->t_doc['ID_LANG']))	$sql.=" AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
			debug($sql,'lime');
			$iSession=new iSession;
			// VP (23/10/08) : changement max_answers_count et connexion par méthode connect
			$prms= array ('host' => sinequa_host,
						  'port' => sinequa_port,
						  'read_only' => 0,
						  'charset' => in_UTF8,
						  'page_size' => empty($this->nbLignes)?$this->nbLignes:10,
						  'max_answers_count' => 500000,
						  'default-language' => $_SESSION['langue']
						  );

			//$link = $iSession->in_connect($prms);
			$link = $iSession->connect($prms);

			$result=$iSession->in_query("alter table ".sinequa_database." set mode='write'");
			$result=$iSession->in_query($sql);
			$result=$iSession->in_query("alter table ".sinequa_database." set mode='read'");
			if ($result<0) {$this->dropError(kErrorDocSauveSinequa);return false;}
			if(isset($iSession))$iSession->in_close();

			return true; // on ne continue pas en mode complet
			} //note : si on n'a pas de champs (param arrFlds), on continue dans le mode réindex complète
		}

		// Mode 2 : réindex complète
		// Données => export xml et mise en forme puis passage par la XSL d'import/export Sinequa
		// VP 12/01/09 : suppression getMats fait dans docSaisie
		//$this->getMats();
		// VP 23/1/09 : conservation des \n jusqu'à insert
		// VP 6/04/10 : appel de getDocFull dans saveSinequa()
		$this->getDocFull(0,0,1,0,0,0);
		$xml=$this->xml_export();
		$xml=str_replace(array("||","’","&amp;","&lt;","&gt;","&#38;","&#60;","&#62;"),
			array("\n","'","&amp;amp;","&amp;lt;","&amp;gt;","&amp;amp;","&amp;lt;","&amp;gt;"),$xml);
		$xml="<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";


		//debug($xml,'plum',true);
		//trace($xml);
		$xml2=TraitementXSLT($xml,getSiteFile("designDir","print/exportSinequa.xsl"),null);

		if (empty($xml2)) {$this->dropError(kErrorDocImportXMLSinequaVide);return false;}
		//debug($xml2,'tan',true);
		//trace('APRES TRANSFO SINEQUA');
		//trace($xml2);
		$sql="INSERT INTO ".sinequa_database." (replace,language,format,options,text)
		VALUES('true','".strtolower($this->t_doc['ID_LANG'])."','xml',".$db->Quote($options).",'".str_replace(array("'","\r","\t"),array("''","",""),$xml2)."');";

		//debug(str_ireplace(array(',','<','WHERE','--'),array(','.chr(10),'<'.chr(10),'WHERE'.chr(10),'--'.chr(10)),$sql),'pink');
		debug($sql,'lime');
		trace($sql);
		// Ouverture base et lancement ordre
		$iSession=new iSession;
  		// VP (23/10/08) : changement max_answers_count et connexion par méthode connect
		$prms= array ('host' => sinequa_host,
						'port' => sinequa_port,
						'read_only' => 0,
						'charset' => in_UTF8,
						'page_size' => 10,
					  'max_answers_count' => 500000,
					  'default-language' => $_SESSION['langue']
				);

		//$link = $iSession->in_connect($prms);
		$link = $iSession->connect($prms);
		$result=$iSession->in_query("alter table ".sinequa_database." set mode='write'");
		$result=$iSession->in_query($sql);
		$result=$iSession->in_query("alter table ".sinequa_database." set mode='read'");
		if(isset($iSession))$iSession->in_close();
		if ($result<0) {$this->dropError(kErrorDocSauveSinequa);return false;}
		return true;
	}


	/**Enlève un document de la base Sinequa **/
	function deleteSinequa() {
		global $db;
		require_once(libDir."sinequa/fonctions.inc");
		require_once(libDir."sinequa/Intuition.inc");
		$sql="DELETE FROM ".sinequa_database." WHERE ID_DOC=".$db->Quote($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
		// Ouverture base et lancement ordre
		$iSession=new iSession;
  		// VP (23/10/08) : changement max_answers_count et connexion par méthode connect
		$prms= array ('host' => sinequa_host,
						'port' => sinequa_port,
						'read_only' => 0,
						'charset' => in_UTF8,
						'page_size' => 10,
					  'max_answers_count' => 500000,
					  'default-language' => $_SESSION['langue']);

		$link = $iSession->connect($prms);
		$result=$iSession->in_query("alter table ".sinequa_database." set mode='write'");
		$result=$iSession->in_query($sql);
		$result=$iSession->in_query("alter table ".sinequa_database." set mode='read'");
		if(isset($iSession))$iSession->in_close();
		return true;
	}

    function saveSolr()
    {
		if(ini_get('max_execution_time')<'120' && ini_get('max_execution_time')!='0')
			ini_set('max_execution_time','120');

		if ($tabLangues=='') $tabLangues=$_SESSION['arrLangues'];

        //champs tri multi-valués
        $arr_sort_fields = array();
        if(defined("gSolrSortFields") && is_array(unserialize(gSolrSortFields))){
            $arr_sort_fields = unserialize(gSolrSortFields);
        }
		
		$arrDocs = array();
		global $arr_cache_solr_exp ;
		if(!is_array($arr_cache_solr_exp) || empty($arr_cache_solr_exp)){
			$arr_cache_solr_exp = array() ; 
		}
		foreach($tabLangues as $lg){
            // Initialisation tableau de champs
            $arr_sort_values = array();
               
			$cloneDoc = new Doc();
			$cloneDoc->t_doc['ID_DOC'] = $this->t_doc['ID_DOC'];
			$cloneDoc->t_doc['ID_LANG'] = $lg;
			$cloneDoc->getDoc();
			if(!$cloneDoc->loaded_from_db){
				// MS - Si l'objet n'a pas été chargé depuis la base SQL, on ne continue pas l'export vers solr. 
				continue ; 
			}
			
			$cloneDoc->getMats();
			$cloneDoc->getLexique();
			$cloneDoc->getValeurs();
			$cloneDoc->getCategories();
			$cloneDoc->getPersonnes();
			$cloneDoc->getRedif();
			$cloneDoc->getDocLiesSRC();
			$cloneDoc->getDocLiesDST();
			$cloneDoc->getFestival();

			$solr_opts=unserialize(kSolrOptions);

			if (!empty($_SESSION['DB']))
			{
				$solr_opts['path']=$_SESSION['DB'];
			}

			$solr_opts['timeout'] = 300;

			$client=new SolrClient($solr_opts);
			$doc=new SolrInputDocument();
			$doc->addField('id',$cloneDoc->t_doc['ID_DOC'].'_'.$cloneDoc->t_doc['ID_LANG']);
			foreach ($cloneDoc->t_doc_mat as $doc_mat)
			{
				if (!empty($doc_mat['MAT']->t_mat))
				{
					$doc->addField('mat_nom',$doc_mat['MAT']->t_mat['MAT_NOM']);
					$doc->addField('mat_ratio',$doc_mat['MAT']->t_mat['MAT_RATIO']);
					$doc->addField('mat_format',$doc_mat['MAT']->t_mat['MAT_FORMAT']);
					$doc->addField('mat_type',$doc_mat['MAT']->t_mat['MAT_TYPE']);
					if(isset($doc_mat['MAT']->t_mat['MAT_NB_PAGES']) && !empty($doc_mat['MAT']->t_mat['MAT_NB_PAGES'])){
						$doc->addField('mat_nb_pages',$doc_mat['MAT']->t_mat['MAT_NB_PAGES']);
					}else{
						$doc->addField('mat_nb_pages',0);
					}
					if(isset($doc_mat['MAT']->t_mat['MAT_WIDTH']) && !empty($doc_mat['MAT']->t_mat['MAT_WIDTH'])){
						$doc->addField('mat_width',$doc_mat['MAT']->t_mat['MAT_WIDTH']);
					}else{
						$doc->addField('mat_width',0);
					}

					if(isset($doc_mat['MAT']->t_mat['MAT_HEIGHT']) && !empty($doc_mat['MAT']->t_mat['MAT_HEIGHT'])){
						$doc->addField('mat_height',$doc_mat['MAT']->t_mat['MAT_HEIGHT']);
					}else{
						$doc->addField('mat_height',0);
					}
				}
				foreach($doc_mat['t_doc_mat_val'] as $doc_mat_val){
					$doc->addField('doc_mat_val',$doc_mat_val['VALEUR']);
				}
			}

			foreach ($cloneDoc->t_pers_lex as $lexique)
			{
				// TYPE_TYPEDESC_TYPELEX_MODE_ROLE  ex: L_GEN_PP_ASSO_AUT
				$cle=trim('P_'.$lexique['ID_TYPE_DESC'].'_'.$lexique['DLEX_ID_ROLE'].'_'.$lexique['PERS_ID_TYPE_LEX'],'_');
				if (defined("gFieldsForPersTerme")) {
					$valeur = "";
					$fields = unserialize(gFieldsForPersTerme);
					foreach ($fields as $f)
						$valeur .= $lexique[strtoupper($f)] . " ";
					$valeur = trim($valeur);
				} else
					$valeur=trim($lexique['PERS_NOM'].' '.$lexique['PERS_PRENOM']);

				if (!empty($valeur))
				   $doc->addField(strtolower($cle),$valeur);
			}

			foreach ($cloneDoc->t_doc_lex as $lexique)
			{
			   // TYPE_TYPEDESC_TYPELEX_MODE_ROLE  ex: L_GEN_PP_ASSO_AUT
			   $cle=trim('L_'.$lexique['ID_TYPE_DESC'].'_'.$lexique['DLEX_ID_ROLE'].'_'.$lexique['LEX_ID_TYPE_LEX'],'_');
				$valeur=trim($lexique['LEX_TERME']);
				
				if (!empty($valeur)){
					$doc->addField(strtolower($cle),$valeur);
				}

				// VP 3/04/14 : Ajout champ thesaurus pour les termes validés ex : THES_LG
				if($lexique['LEX_ID_ETAT_LEX']==2)
				{
				   $cle=trim('THES_'.$lexique['LEX_ID_TYPE_LEX'],'_');

				   // Substituion synonyme
				   if(!empty($lexique['SYNO']))
					  $valeur=trim($lexique['SYNO']);
					else
					  $valeur=trim($lexique['LEX_TERME']);

					if (!empty($valeur))
						$doc->addField(strtolower($cle),$valeur);

                    // Lexique hiérarchique
                    $cle = trim('HIER_THES_' . $lexique['LEX_ID_TYPE_LEX'], '_');
                    if (!empty($lexique['LEX_PATH'])) {
                        $arrFatherLex = explode(chr(9), $lexique['LEX_PATH']);
                        $valeur = "";
                        foreach ($arrFatherLex as $idx => $father) {
                            $level = $idx;
                            $valeur.="/" . trim(str_replace('/','%SLASH%',$father));
                            $doc->addField(strtolower($cle), $level . $valeur);
                        }
                        $level = count($arrFatherLex);
                        $valeur.="/" . trim(str_replace('/','%SLASH%',$lexique['LEX_TERME']));
                        $doc->addField(strtolower($cle), $level . $valeur);
                    } else {
                        $doc->addField(strtolower($cle), "0/" . trim(str_replace('/','%SLASH%',$lexique['LEX_TERME'])));
                    }
			   
					// Coordonnées géographiques
					$valeur=trim($lexique['LEX_COOR']);
					if (!empty($valeur)){
						$cle=trim('COOR_'.$lexique['LEX_ID_TYPE_LEX'],'_');
						$doc->addField(strtolower($cle),$valeur);
					}


				}
			}

			foreach ($cloneDoc->t_doc_val as $valeur)
			{
				$cle=trim('V_'.$valeur['ID_TYPE_VAL']);
				$valeur=trim($valeur['VALEUR']);

                if (!empty($valeur)){
                    $doc->addField(strtolower($cle),$valeur);
                    //echo 'V_',$valeur['ID_TYPE_VAL'],'=',$valeur['VALEUR'],'';
                    if(in_array(strtolower($cle), $arr_sort_fields) && !isset($arr_sort_values[strtolower("sort_".$cle)])){
                        $arr_sort_values[strtolower("sort_".$cle)] = $valeur;
                    }
                }
			}


			foreach ($cloneDoc->t_doc_cat as $categ)
			{
				$cle=trim('CT_'.$categ['CAT_ID_TYPE_CAT']);
				$valeur=trim($categ['CAT_NOM']);

				if (!empty($categ)){
				   $doc->addField(strtolower($cle),$valeur);
				}
				// Lexique hiérarchique
				$cle = trim('HIER_CT_' . $categ['CAT_ID_TYPE_CAT']);
				if (!empty($categ['CAT_PATH'])) {
					$arrFatherCat = explode(chr(9), $categ['CAT_PATH']);
					$valeur = "";
					foreach ($arrFatherCat as $idx => $father) {
						$level = $idx;
						$valeur.="/" . trim(str_replace('/','%SLASH%',$father));
						$doc->addField(strtolower($cle), $level . $valeur);
					}
					$level = count($arrFatherCat);
					$valeur.="/" . trim(str_replace('/','%SLASH%',$categ['CAT_NOM']));
					$doc->addField(strtolower($cle), $level . $valeur);
				} else {
					$doc->addField(strtolower($cle), "0/" . trim(str_replace('/','%SLASH%',$categ['CAT_NOM'])));
				}
			}

			foreach ($cloneDoc->t_doc_fest as $fest)
			{
				$cle="fest";
				$valeur=trim($fest['FEST_LIBELLE']);

                if (!empty($valeur)){
                    $doc->addField(strtolower($cle),$valeur);
                }
			}


			foreach ($cloneDoc->t_doc_redif as $redif)
			{
				if(!empty($redif)){
					foreach($redif  as $field => $val){
						if(!in_array(strtolower($field),array('id_doc','id_redif')) && $val != ''){
							$doc->addField(strtolower($field),$val);
						}
					}
				}
			}

			// champs t_doc
			//$champs_date=array('DOC_DATE_DIFF','DOC_DATE_PV_FIN','DOC_DATE_PV_DEBUT','DOC_DATE_CREA','DOC_DATE_MOD','DOC_DATE_DIFF_FIN','DOC_DATE_ETAT','DOC_DATE','DOC_DATE_FIN_DROITS','DOC_DUREE_DIFF');
			$champs_date=array('DOC_DATE_CREA','DOC_DATE_MOD');
            // champs à NE PAS envoyer à Solr => ces champs, même si ils sont remplis, ne seront pas exportés vers la version Solr du document
            $champs_prevent_export_solr = array("DOC_ID_IMPORT");
			
			foreach($cloneDoc->t_doc as $cle=>$valeur)
			{
				// champs de t_doc
                if (!in_array($cle,$champs_prevent_export_solr ) && (isset($valeur) && (!empty($valeur) || (($cle=='DOC_NUM' || $cle=='DOC_ACCES' || $cle=='DOC_VI') && $valeur!=null))))
				{
					if (in_array($cle,$champs_date))
					{
						if (!empty($valeur))
						{
							//le format des champs de date est different pour Solr
							// exemple : 2013-08-20T11:59:23Z
							$split_date=explode(' ',$valeur);

							if (count($split_date)<2)
								$split_date[1]='00:00:00';

							$doc->addField(strtolower($cle),$split_date[0].'T'.$split_date[1].'Z');
						}
					}else if($cle == "DOC_ID_FONDS"){
						// gestion de l'export des fonds ; 
						$doc->addField(strtolower($cle),$valeur);
						if(isset($arr_cache_solr_exp['FONDS'][$valeur])){
							// trace("fonds get from cache : ".$valeur);
							$fonds = $arr_cache_solr_exp['FONDS'][$valeur] ; 
						}else{
							require_once(modelDir.'model_fonds.php');
							$fonds = new Fonds($valeur,$cloneDoc->t_doc['ID_LANG']) ; 
							$fonds->t_fonds['FONDS_PATH'] = $fonds->getFondsPath(true,'/');
							if($fonds->t_fonds['FONDS_PATH'] !== false){
								$fonds_tmp = explode('/',$fonds->t_fonds['FONDS_PATH']);
								
								if(intval($fonds_tmp[0]) > 0){
									$fonds->t_fonds['FONDS_PARENT'] = $fonds_tmp[count($fonds_tmp)-2];
								}
								
								// Fonds hiérarchique
								$cle = trim('HIER_FONDS');
								$valeur_path = "";
								$fonds->t_fonds['HIER_FONDS']=array() ; 
								unset($fonds_tmp[0]);
								$fonds_tmp = array_values($fonds_tmp);
								foreach ($fonds_tmp as $idx => $father) {
									$level = $idx;
									$valeur_path.="/" . trim(str_replace('/','%SLASH%',$father));
									$fonds->t_fonds['HIER_FONDS'][] = $level . $valeur_path ; 
								}
							}
							// trace("fonds store in cache : ".$valeur);
							$arr_cache_solr_exp['FONDS'][$valeur] = $fonds ;
							
						}	
						
						if(isset($fonds->t_fonds['FONDS'])){
							$doc->addField('fonds',$fonds->t_fonds['FONDS']);
							// trace("saveSolr add fonds :".($fonds->t_fonds['FONDS']));
						}
						if(isset($fonds->t_fonds['FONDS_PARENT'])){
							$doc->addField('fonds_parent',$fonds->t_fonds['FONDS_PARENT']);
							// trace("saveSolr add fonds_parent :".($fonds->t_fonds['FONDS_PARENT']));
						}
						
						if(is_array($fonds->t_fonds['HIER_FONDS']) && !empty($fonds->t_fonds['HIER_FONDS'])){
							foreach($fonds->t_fonds['HIER_FONDS'] as $idx_=>$fonds_){
								$doc->addField('hier_fonds',$fonds_);
								// trace("saveSolr add hier_fonds loop : ".$fonds_);
							}
						}
						
					// MS - 27.01.16 - Si doc_acces est stocké en mode "dépendant de la langue", il se retrouve donc dans arrFieldsToSaveWithIdLang,
					//	mais on ne veut pas le stocker dans solr avec un suffix de langue. (test sous forme in_array pr éventuel ajout d'autres champs ... )
					}else if(in_array(strtoupper($cle),$cloneDoc->arrFieldsToSaveWithIdLang) && !in_array(strtoupper($cle),array('DOC_ACCES'))){
						if ($cle == "DOC_LEX") {
							$newVal = "";
							preg_match_all("'<lex(.*?)>(.*?)</lex>'si", $valeur, $elemsLex);
							foreach ($elemsLex[0] as $elemLex) {
								preg_match("'<id_lex>(.*?)</id_lex>'si", $elemLex, $matchLex);
								preg_match("'<id_pers>(.*?)</id_pers>'si", $elemLex, $matchPers);
								$supl = ' ID_LEX="'.trim($matchLex[1]).'"';
								if (count($matchPers) > 0)
									$supl = ' ID_PERS="'.trim($matchPers[1]).'"';
								$newVal .= preg_replace(array("'<id_lex>(.*?)</id_lex>'si", "'<id_pers>(.*?)</id_pers>'si", "'<LEX'"), array("", "", '<LEX'.$supl), $elemLex);
							}
							$newVal = "<XML>".$newVal."</XML>";
							$doc->addField(strtolower($cle.'_'.$cloneDoc->t_doc['ID_LANG']),$newVal);
						} else
							$doc->addField(strtolower($cle.'_'.$cloneDoc->t_doc['ID_LANG']),$valeur);
					}else{
						$doc->addField(strtolower($cle),$valeur);
					}
				}
			}
			// MS - 21.03.16 - ces infos sont maintenant alimentées directement dans l'objet doc, pas besoin de les alimenter ici, donc. (+ génère des exceptions solr si on le fait... )
			// ajout count des documents liés
			/*if(!empty($cloneDoc->arrDocLiesSRC)){
				$doc->addField('cnt_doc_lies_src',count($cloneDoc->arrDocLiesSRC));
			}
			if(!empty($cloneDoc->arrDocLiesDST)){
				$doc->addField('cnt_doc_lies_dst',count($cloneDoc->arrDocLiesDST));
			}*/


			// ajout des champs dates
			$champ_date_copie=array('DOC_DATE_DIFF','DOC_DATE_PROD','DOC_DATE');
			if(defined('gSolrChampDateCopieExtension')){
				$tmp_arr = unserialize(gSolrChampDateCopieExtension) ; 
				$champ_date_copie = array_merge($champ_date_copie,$tmp_arr );
				unset($tmp_arr);
			}
			
			foreach($champ_date_copie as $champ_date)
			{
				if (isset($cloneDoc->t_doc[$champ_date]) && !empty($cloneDoc->t_doc[$champ_date]))
				{
					//$split_date=explode(' ',$cloneDoc->t_doc['DOC_DATE']);
					//$split_date=explode(' ',date('Y-m-d H:i:s',strtotime($cloneDoc->t_doc[$champ_date])));
					$split_date=explode(' ',convDateTime($cloneDoc->t_doc[$champ_date]));

					if(isset($split_date[0])){
						$split_date[0] = trim($split_date[0],' -_./');
					}
					if (strlen($split_date[0])==4)
						$split_date[0]=$split_date[0].'-00-00';
					else if (strlen($split_date[0])==7)
						$split_date[0]=$split_date[0].'-00';

					if (!(isset($split_date[1]) && !empty($split_date[1])))
						$split_date[1]='00:00:00';

					$tmp_date_split=explode('-',$split_date[0]);
					$tmp_heure_split=explode(':',$split_date[1]);

					if(
						(strlen($split_date[0])==10 && is_numeric($tmp_date_split[0]) && is_numeric($tmp_date_split[1]) && is_numeric($tmp_date_split[2])) &&
						(strlen($split_date[1]) == 8 && is_numeric($tmp_heure_split[0]) && is_numeric($tmp_heure_split[1]) && is_numeric($tmp_heure_split[2]))
					){
						if($tmp_date_split[0]!='0000'){
							$split_date[0]=str_replace('-00','-01',$split_date[0]);
							$doc->addField(strtolower($champ_date).'_solr',$split_date[0].'T'.$split_date[1].'Z');
						}
					}else{
						trace("saveSolr - fail sauvegarde date solr doc ".(($cloneDoc->t_doc['ID_DOC'])?$cloneDoc->t_doc['ID_DOC']:'new')." : format incorrect : ".strtolower($champ_date).'_solr'.$split_date[0].'T'.$split_date[1].'Z');
					}
				}
			}

			// ajout de la vignette
			if(strstr(dirname($cloneDoc->vignette),'/storyboard/')){
				$doc->addField('im_chemin',str_replace(str_replace(kCheminHttpMedia,'',storyboardChemin),'',dirname($cloneDoc->vignette)).'/');
				$doc->addField('im_fichier',basename($cloneDoc->vignette));
			}else if (strstr(dirname($cloneDoc->vignette),'/doc_acc/')){
				$doc->addField('da_chemin',str_replace(str_replace(kCheminHttpMedia,'',kDocumentUrl),'',dirname($cloneDoc->vignette)).'/');
				$doc->addField('da_fichier',basename($cloneDoc->vignette));
			}

			//$doc->addField('imageur',$cloneDoc->t_doc['DOC_IMAGEUR']);
			$doc->addField('type_doc',$cloneDoc->type_doc);
			$doc->addField('etat_doc',$cloneDoc->etat_doc);
			
			//calcul de la duree en secondes pour la recherche
			if (isset($cloneDoc->t_doc['DOC_DUREE']) && !empty($cloneDoc->t_doc['DOC_DUREE']))
				$doc->addField('duree_sec',timeToSec($cloneDoc->t_doc['DOC_DUREE']));

            // Ajout des champs de tri multivalués
            if(count($arr_sort_values)>0){
                foreach($arr_sort_values as $key => $value){
                    $doc->addField($key , $value);
                }
            }
               
			$arrDocs[] = $doc;
		}

        // VP 8/3/2015 : sauvegarde solr via curl alternatif à addDocuments (expérimental)
//       $xml="<add>";
//       foreach($arrDocs as $d){
//           $xml.="<doc>";
//           $tabdoc = $d->toArray();
//           foreach($tabdoc['fields'] as $tabfld){
//               $tag = $tabfld->name;
//               foreach($tabfld->values as $val){
//                    $xml.="<field name=\"".$tag."\"><![CDATA[".$val."]]></field>";
//               }
//           }
//           $xml.="</doc>";
//       }
//       $xml.="</add>";
//       $filepath=kServerTempDir."/".kDatabaseName.$this->t_doc['ID_DOC'].".xml";
//       file_put_contents($filepath, $xml);
//       $opts_solr = unserialize(kSolrOptions);
//       $url = "http://".$opts_solr['hostname'].":".$opts_solr['port']."/".$opts_solr['path']."/update?commit=true -H \"Content-Type: text/xml\" --data-binary @".$filepath;
//       //trace("curl update : ".$url);
        //$rtn = shell_exec("curl ".$url);
//       $curl = curl_init($url);
//       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//       $return = curl_exec($curl);
//       if($return===false){
//            trace("*** ERROR curl update failed : ".$url);
//       }
//       curl_close($curl);
//       unlink($filepath);

        // VP 29/7/14 : Modif paramètre 2 à partir de PECL Solr version 2.0
        // SolrClient::addDocument ( SolrInputDocument $doc [, bool $overwrite = true [, int $commitWithin = 0 ]] )
        // PECL Solr < 2.0 $allowDups était utilisé à la place de $overwrite ; il s'agit de la même fonctionalité, mais avec une valeur opposée : $allowDups = false est identique à $overwrite = true
        if(!empty($client)){
	        if(intval(solr_get_version())<2){
	               $client->addDocuments($arrDocs,false,1);
	        } else {
	               $client->addDocuments($arrDocs,true,1);
	        }
    	}

    }

    function deleteSolr($id_lang="")
    {
		//MS 25.04.14 - la librairie solrClient contient un bug dans la fonction commit, qui envoi un paramètre deprecié (waitFlush).
		// l'envoi de ce paramètre fait buggé le commit et n'execute donc pas réellement la suppression de document dans solr
		// en attendant mise à jour de la librairie SolrClient => on passe la commande de suppression par curl

		if(!isset($this->t_doc['ID_DOC']) || !defined("kSolrOptions")){
			trace("deleteSolr failed => il manque des paramètres");
			return false ;
		}
		$opts_solr = unserialize(kSolrOptions);

		if (!empty($id_lang)) $tabLangues=array($id_lang);
		elseif (!empty($_SESSION['arrLangues'])) $tabLangues=$_SESSION['arrLangues'];
		elseif (!empty($this->t_doc['ID_LANG'])) $tabLangues=array($this->t_doc['ID_LANG']);
		else return false;

		foreach($tabLangues as $lg){
			$url = "http://".$opts_solr['hostname'].":".$opts_solr['port']."/".$opts_solr['path']."/update?stream.body=<delete><query>id:".$this->t_doc['ID_DOC']."_".$lg."</query></delete>&commit=true";
			trace("curl delete : ".$url);
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$return = curl_exec($curl);
			if($return){
				trace("curl delete OK");
			}else{
				trace("curl delete failed");
			}

			curl_close($curl);
		}
		// BUG : La methode commit ne fonctionne pas
        // $client=new SolrClient(unserialize(kSolrOptions));
        // $client->deleteById($this->t_doc['ID_DOC']."_".$this->t_doc['ID_LANG']);
        // $client->commit();
    }

	static function setAutoIncrement() {
		global $db;
		$db->Execute("ALTER TABLE t_doc AUTO_INCREMENT=0"); //Attention, ne fonctionne que ss mysql (p-ê)
	}

	function heritFieldsToReport() {
		global $db;
		require_once(modelDir.'model_docReportage.php');
		if (defined("gReportToHeritFields") && Reportage::isReportageEnabled()){
			require_once(libDir."class_modifLot.php");
			if (is_array(unserialize(gReportToHeritFields))) $arrFields = unserialize(gReportToHeritFields);
			else $arrFields = explode('/', gReportToHeritFields);

			//Chargement données si inexistantes
			if (count($this->t_doc) <= 2) $this->getDoc();
			if (count($this->t_doc_lex) == 0) $this->getLexique();
			if (count($this->t_pers_lex) == 0) $this->getPersonnes();
			if (count($this->t_doc_val) == 0) $this->getValeurs();
			if (empty($this->t_doc_cat)) $this->getCategories();
			if (count($this->arrDocLiesSRC) == 0) $this->getDocLiesSRC();

			foreach ($this->arrDocLiesSRC as $idx=>&$dl) if (!empty($dl['ID_DOC_SRC'])) {
				require_once(modelDir.'model_doc.php');
				$report = New Reportage();
				$report->t_doc['ID_DOC'] = $dl['ID_DOC_DST'];
				$report->getDoc();
				$report->getLexique();
				$report->getValeurs();
				$dl['DOC'] = $report;

				$myMod=new ModifLot("doc","");
				$myMod->arrFields=array();
				$myMod->setSearchSQL(true, "SELECT distinct ID_DOC,'".$_SESSION['langue']."' as ID_LANG from t_doc where ID_DOC = ".intval($report->t_doc['ID_DOC']), false);
				$hasResult=$myMod->getResults();
				if ($hasResult)
					foreach ($arrFields as $field=>$commande){
						$arrWF = explode("_", $field);
						switch ($arrWF[0]) {
							case "DOC":
								if ($commande == "MIN") {
									$value = $this->t_doc[$field];
									if (!empty($dl['DOC']) && isset($dl['DOC']->t_doc[$field])) {
										if ($value == null || strcmp($dl['DOC']->t_doc[$field], $value) < 0) $value = $dl['DOC']->t_doc[$field];
									}
									if ($value != null) {
										$myMod->updateFromArray(array("field" => $field, "value" => $value, 'commande' => "INIT"));
										$myMod->applyModif(); }
								}
								break;
							case "L" :
								$arrIds = array();
								foreach ($this->t_doc_lex as $docLex)
									if ($docLex['ID_TYPE_DESC'] == $arrWF[1] && (!isset($arrWF[2]) || $docLex['DLEX_ID_ROLE'] == $arrWF[2]))
										$arrIds[$docLex['ID_LEX']] = array('ID_LEX' => $docLex['ID_LEX'], "DLEX_ID_ROLE" => $docLex['DLEX_ID_ROLE'], "ID_TYPE_DESC" => $docLex['ID_TYPE_DESC']);
								if (count($arrIds) > 0 ) {
									$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
									$myMod->applyModif(); }
								break;
						   case "P" :
							   $arrIds = array();
							   foreach ($this->t_pers_lex as $persLex)
							   if ($persLex['ID_TYPE_DESC'] == $arrWF[1] && (!isset($arrWF[2]) || $persLex['DLEX_ID_ROLE'] == $arrWF[2]))
							   $arrIds[$persLex['ID_PERS']] = array('ID_PERS' => $persLex['ID_PERS'], "DLEX_ID_ROLE" => $persLex['DLEX_ID_ROLE'], "ID_TYPE_DESC" => $persLex['ID_TYPE_DESC']);
							   if (count($arrIds) > 0 ) {
								   $myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								   $myMod->applyModif(); }
							   break;
						   case "CAT" :
							   $arrIds = array();
							   foreach ($this->t_doc_cat as $docCat) if ($docCat['CAT_ID_TYPE_CAT'] == $arrWF[1]) $arrIds[$docCat['ID_CAT']] = $docCat['ID_CAT'];
							   if (count($arrIds) > 0 ) {
								   $myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
								   $myMod->applyModif(); }
							   break;
							case "V" :
								$arrIds = array();
								foreach ($this->t_doc_val as $docVal) if ($docVal['ID_TYPE_VAL'] == $arrWF[1]) $arrIds[$docVal['ID_VAL']] = $docVal['ID_VAL'];
								if (count($arrIds) > 0 ) {
									$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => $commande));
									$myMod->applyModif(); }
								break;
						}
					}
				unset($myMod);
				$db->execute("delete from t_doc_lex where id_doc_lex in (
								select dl.id_doc_lex
								from t_doc_lex dl
								,(select min(id_doc_lex) as id_doc_lex, id_doc, id_lex from t_doc_lex WHERE id_doc = ".intval($report->t_doc['ID_DOC'])." group by id_doc, id_lex having count(1) > 1) db
								where db.id_doc = dl.id_doc AND db.id_lex = dl.id_lex and dl.id_doc_lex != db.id_doc_lex AND dl.id_doc = ".intval($report->t_doc['ID_DOC']).");");
			}
		}
	}


	function update_pg_id_doc_seq(){
		global $db;
		$maxId = $db->getOne("SELECT max(ID_DOC) FROM t_doc");
		if ($maxId && defined("kDbType") && strpos(kDbType,"postgres") === 0) {
			//$ok = $db->Execute("ALTER SEQUENCE t_doc_id_doc_seq RESTART WITH ".(intval($maxId)+1));
			$ok = $db->Execute("SELECT setval('t_doc_id_doc_seq', ".intval(intval($maxId)+1) .")");
			trace("class_doc : SELECT setval('t_doc_id_doc_seq', ".intval(intval($maxId)+1) .")");
		}
	}
	
	
	

} // fin classes
?>
