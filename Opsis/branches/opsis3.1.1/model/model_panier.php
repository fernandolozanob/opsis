<?
include_once(libDir."class_user.php");
require_once(modelDir.'model.php');
class Panier extends Model 
{
    var $t_panier= array(); // Propriétés générales
    var $t_panier_doc = array(); // Tableau des documents

    var $currentLine; //Ligne du panier en cours de modification / suppression => ID_LIGNE_PANIER

    var $titre; // titre du panier (à afficher dans la page)

    var $previousID; //dans le cas de la validation du panier, valeur du panier d'origine.

	var $arrTarifAddCrit; // Critères additionnels de récupération des prix
	var $calc_prices; //boolean : calcul des prix ? true/false
	var $User; //User en cours
	var $owner; // Usager propriétaire du panier

	var $blnOutOfRange; //booleen (0/1) indiquant un dépassement ou non des fourchettes min/max (constantes) de montant

	var $sqlOrder; // sql de order by, utilisé dans getPanierDoc pour donner un ordre

	var $error_msg;

	var $vignette;

    //*** Constructeur
    function __construct($id=null,$version=''){
		parent::__construct('t_panier',$id,$version);
        $this->User=User::getInstance();
        $this->titre = "";

        $this->currentLine="";
		$this->error_msg='';

		/** Tableau des critères additionnels, doit avoir obligatoirement la structure suivante
		 * un tableau pour chaque critère composé d'un tableau à 3 dimensions :
		 * object (indique l'objet dans lequel chercher : panier ou panier_doc,...)
		 * where : optionnel, indique le noeud du tableau où chercher le critère. Chaque niveau est délimité par un /
		 * what : le nom du champ du tableau où se situe l'info
		 *
		 * */
		$this->arrTarifAddCrit = unserialize(garrCriteresAddCalculPrix);
    }


   /**
	 * Initialise l'objet en récupérant les champs depuis la base
	 */
	function init() {
		parent::init();
		$this->t_panier["PAN_ID_USAGER"]=$this->User->UserID; //Usager par défaut
	}


	/**
	 * Récupère les infos sur un ou plusieurs paniers.
	 * IN : array de ID (opt), getPanierDoc (true/false) : récupère aussi les lignes
	 * OUT : si array spécifié -> tableau d'objets paniers, sinon màj de l'objet en cours
	 */
	function getPanier($arrPanier=array(),$getPanierDoc=true){
		return $this->getData($arrPanier,'',$getPanierDoc);
	}
		 
	function getData($arrData=array(),$version="",$getPanierDoc = true) {
		global $db;
		if(empty($version)) {
		    $version = $_SESSION['langue'];
		}
		$arrPanier=parent::getData($arrData,$version);
		if (empty($arrPanier) && !empty($this->t_panier) && !$arrData) { 
			$this->t_panier['XML']=xml2array($this->t_panier['PAN_XML']);
			$this->etat_panier=GetRefValue("t_etat_pan",$this->t_panier["PAN_ID_ETAT"],$version);
			if ($getPanierDoc==true){
			    $this->getPanierDoc(false, '', $version);
			}
			if (in_array($this->t_panier['PAN_ID_TYPE_COMMANDE'],unserialize(gModesAvecCalculPrix))==true){
				$this->calc_prices=true;
			}
			$this->getVignette();
		}else{
			foreach ($arrPanier as $idx=>$Panier){ // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrPanier[$idx]->t_panier['XML']=xml2array($arrPanier[$idx]->t_panier['PAN_XML']);
				$arrPanier[$idx]->etat_panier=GetRefValue("t_etat_pan",$arrPanier[$idx]->t_panier["PAN_ID_ETAT"],$version);
				if ($getPanierDoc==true){
				    $arrPanier[$idx]->getPanierDoc(false, '', $version);
				}
				if (isset($this->t_panier['PAN_ID_TYPE_COMMANDE']) && in_array($this->t_panier['PAN_ID_TYPE_COMMANDE'],unserialize(gModesAvecCalculPrix))==true){
					$this->calc_prices=true;
				}
				$arrPanier[$idx]->getVignette();
			}
			unset($this); // plus besoin
			return $arrPanier;
		}
	}
	 
	/*function getPanier($arrPanier=array(),$getPanierDoc=true) {
		global $db;
		if (!$arrPanier) $arrVals=array($this->t_panier['ID_PANIER']); else $arrVals=(array)$arrPanier;
		$arrCrit['ID_PANIER']=$arrVals;

		$this->t_panier=getItem("t_panier",null,$arrCrit,null,0); //AJOUTER GESTION DES ORDRES

		if (count($this->t_panier)==1 && !$arrPanier) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->t_panier=$this->t_panier[0];
				$this->t_panier['XML']=xml2array($this->t_panier['PAN_XML']);
				$this->etat_panier=GetRefValue("t_etat_pan",$this->t_panier["PAN_ID_ETAT"],$_SESSION['langue']);
				if ($getPanierDoc==true) $this->getPanierDoc();
				if (in_array($this->t_panier['PAN_ID_TYPE_COMMANDE'],unserialize(gModesAvecCalculPrix))==true) $this->calc_prices=true;
				$this->getVignette();
		}
		else {
            $arrVals=array();
			foreach ($this->t_panier as $idx=>$Panier) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx]= new Panier();
				$arrVals[$idx]->t_panier=$Panier;
				$arrVals[$idx]->t_panier['XML']=xml2array($arrVals[$idx]->t_panier['PAN_XML']);
				$arrVals[$idx]->etat_panier=GetRefValue("t_etat_pan",$arrVals[$idx]->t_panier["PAN_ID_ETAT"],$_SESSION['langue']);
				if ($getPanierDoc==true) $arrVals[$idx]->getPanierDoc();
				if (isset($this->t_panier['PAN_ID_TYPE_COMMANDE']) && in_array($this->t_panier['PAN_ID_TYPE_COMMANDE'],unserialize(gModesAvecCalculPrix))==true) $this->calc_prices=true;
				$arrVals[$idx]->getVignette();
			}
			unset($this); // plus besoin
			return $arrVals;
		}
	}*/

    function getPanierDoc($onlyCmd=false, $id_ligne_panier='', $langPanier = '') {
    	global $db;
    	
    	if(empty($langPanier)) {
    	    $langPanier = $_SESSION['langue'];
    	}
		//VP 5/10/10 : possibilité de passer id_ligne_panier
    	if (empty($this->t_panier['ID_PANIER'])&&empty($id_ligne_panier)) return false;
		// VP (25/9/08) : remplacement  DOC_DATE_PROD pr DOC_DATE et ajout FONDS_COL
		// VP (14/10/08) : ajout calcul de la durée
		// VP (1/07/09) : modif requête suite à suppression rappatriement objets doc dans panier
    	/*$sql="SELECT t_panier_doc.*,t_doc.DOC_NUM,t_doc.DOC_DATE,t_doc.DOC_DUREE,t_fonds.FONDS,t_fonds.FONDS_COL
			from t_panier_doc, t_doc
			left join t_fonds on (t_doc.DOC_ID_FONDS=t_fonds.ID_FONDS and t_fonds.ID_LANG=".$db->Quote($_SESSION['langue']).")
			WHERE
			t_panier_doc.ID_DOC=t_doc.ID_DOC AND t_doc.ID_LANG=".$db->Quote($_SESSION['langue'])."
			AND ID_PANIER=".$db->Quote($this->t_panier['ID_PANIER']);*/
		// VP 4/10/10 : ajouts champs PDOC_ID_IMAGE et vignette pour capture d'image
		// GT 7/12/10 : ajout du champ DOC_IMAGEUR pour afficher la reference
		// VP 7/02/13 : ajout du champ DOC_ID_ETAT_DOC
        if(defined("gsqlselectPanierDoc")) {
			$sql = str_replace("%lang%", $db->Quote($langPanier), gsqlselectPanierDoc);
        }
		else{
			// MS - 02.06.15 - Ajout de la récupération des infos de base propre à un média => durée pour la video / l'audio, mat_width / mat_height pour l'image, mat_nb_pages pr les documents,
			// suite à cet ajout dans la requete on a besoin de rajouter la clause group by version pgsql, qui est donc très longue ......
			// NB - 27 11 15 - Ajout de paramètre spécifique à un site grace à la constante gDocSelectFieldsPanier definit dans initialisationGeneral
            $sql="SELECT t_panier_doc.*,t_doc.ID_DOC, t_doc.ID_LANG, t_doc.DOC_ID_FONDS, t_doc.DOC_ID_TYPE_DOC, t_doc.DOC_COTE,
                t_doc.DOC_ID_MEDIA,t_doc.DOC_NUM,t_doc.DOC_TITRE, t_doc.DOC_TITRE_COL, t_doc.DOC_AUTRE_TITRE, t_doc.DOC_SOUSTITRE, t_doc.DOC_TITREORI,
                t_doc.DOC_ACCES, t_doc.DOC_DUREE,t_doc.DOC_DATE_DIFF, t_doc.DOC_VAL, t_doc.DOC_XML, t_doc.DOC_LEX, t_doc.DOC_DATE_PROD, t_doc.DOC_DATE,t_doc.DOC_ID_IMAGEUR,t_doc.DOC_ID_ETAT_DOC,t_doc.DOC_COPYRIGHT,t_doc.DOC_DATE_MOD,t_doc.DOC_ID_USAGER_CREA,
                t_doc_acc.DA_CHEMIN, t_doc_acc.DA_FICHIER, i1.IM_CHEMIN, i1.IM_FICHIER, i1.ID_IMAGEUR, i2.IM_CHEMIN as IM_CHEMIN2,i2.IM_FICHIER as IM_FICHIER2,t_fonds.FONDS,t_fonds.FONDS_COL, ea.ETAT_ARCH, MAX(t_mat.MAT_WIDTH) as MAT_WIDTH, MAX(t_mat.MAT_HEIGHT) as MAT_HEIGHT,MAX(t_mat.MAT_NB_PAGES) as MAT_NB_PAGES,MAX(t_mat.MAT_TYPE) as MAT_TYPE, MAX(t_mat.MAT_FORMAT) as MAT_FORMAT, MAX(t_mat.MAT_ID_MEDIA) as MAT_ID_MEDIA";

		    if(defined("gDocSelectFieldsPanier") && (gDocSelectFieldsPanier)){
				$sql.=','.gDocSelectFieldsPanier;
			}

            $sql.=" from t_panier_doc
                left join t_doc ON t_panier_doc.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG=".$db->Quote($langPanier)."
				left join t_doc_mat on (t_doc.ID_DOC = t_doc_mat.ID_DOC)
				left  join t_mat on (t_doc_mat.ID_MAT = t_mat.ID_MAT and t_mat.MAT_TYPE".(defined("gMainMatType") && gMainMatType ? gMainMatType : "='MASTER'").")
				left outer join t_fonds on (t_doc.DOC_ID_FONDS=t_fonds.ID_FONDS and t_fonds.ID_LANG=".$db->Quote($langPanier).")
                left outer join t_image i1 on (t_doc.DOC_ID_IMAGE= i1.ID_IMAGE AND i1.ID_LANG=".$db->Quote($langPanier).")
                left outer join t_image i2 on (t_panier_doc.PDOC_ID_IMAGE= i2.ID_IMAGE AND i2.ID_LANG=".$db->Quote($langPanier).")
                left outer join t_etat_arch ea on (ea.ID_ETAT_ARCH = t_doc.DOC_ID_ETAT_ARCH AND ea.ID_LANG=".$db->Quote($langPanier).")
                left outer join t_doc_acc on (t_doc.DOC_ID_DOC_ACC= t_doc_acc.ID_DOC_ACC AND t_doc_acc.ID_LANG=".$db->Quote($langPanier).")
                ";

				$groupby = " GROUP BY id_ligne_panier, t_doc.id_doc,t_doc.id_lang,t_doc.doc_id_fonds,t_doc.doc_id_type_doc, t_doc.doc_cote,t_doc.doc_id_media,t_doc.doc_num,t_doc.DOC_TITRE, t_doc.DOC_TITRE_COL, t_doc.DOC_AUTRE_TITRE, t_doc.DOC_SOUSTITRE, t_doc.DOC_TITREORI,
                t_doc.DOC_ACCES, t_doc.DOC_DUREE,t_doc.DOC_DATE_DIFF, t_doc.DOC_VAL, t_doc.DOC_XML, t_doc.DOC_LEX, t_doc.DOC_DATE_PROD, t_doc.DOC_DATE,t_doc.DOC_ID_IMAGEUR,t_doc.DOC_ID_ETAT_DOC,t_doc.DOC_COPYRIGHT,
                t_doc_acc.DA_CHEMIN, t_doc_acc.DA_FICHIER, i1.IM_CHEMIN, i1.IM_FICHIER,IM_CHEMIN2,IM_FICHIER2, i1.ID_IMAGEUR, FONDS,FONDS_COL,ETAT_ARCH, id_fonds, i1.id_image,i2.id_image, id_etat_arch,id_doc_acc ";
		}

        $sql.="WHERE t_doc.ID_LANG=".$db->Quote($langPanier);

		if (!empty($id_ligne_panier)) $sql.=" AND t_panier_doc.ID_LIGNE_PANIER=".intval($id_ligne_panier);
		else $sql.=" AND t_panier_doc.ID_PANIER=".intval($this->t_panier['ID_PANIER']);
		if ($onlyCmd) $sql.=" and PDOC_CMD_TMP=1";

		if(isset($groupby) && !empty($groupby)){
			if(defined("gDocSelectFieldsPanier") && (gDocSelectFieldsPanier)){
				$groupby.=','.gDocSelectFieldsPanier;
			}
			$sql .= $groupby;
		}

		// VP (26/9/08) ajout tri selon ID_LIGNE_PANIER au cas où PDOC_ORDRE est 0
		if (!empty($this->sqlOrder))  $sql.=$this->sqlOrder; //ajouté par LD 15/11/2007 pour gérer un ordre.
		elseif (defined("gDocTriPanier") && (gDocTriPanier))  $sql.=gDocTriPanier; // ajouté par NB 10 12 2015 pour géré un tri différent dans le panier
		else $sql.=" ORDER BY PDOC_ORDRE,ID_LIGNE_PANIER ASC"; //LD 260808 ordre par défaut sur n° ligne
		//Ne fonctionne qu'avec une colonne de t_panier_doc ou autres colonnes de cette requete !!!
		$this->t_panier_doc=$db->GetAll($sql);
		
		$this->duree_panier="00:00:00";
		foreach ($this->t_panier_doc as &$pdoc) {
			if ( !empty($pdoc['IM_FICHIER2'])){
				$relativePathFromMedia=str_replace(kCheminHttpMedia,'',storyboardChemin);
				$pdoc['VIGNETTE'] = $relativePathFromMedia.$pdoc['IM_CHEMIN2']."/".$pdoc['IM_FICHIER2'];
			}else if(!empty($pdoc['DA_CHEMIN']) && !empty($pdoc['DA_FICHIER'])){
				$relativePathFromMedia=str_replace(kCheminHttpMedia,'',kDocumentUrl);
				$pdoc['VIGNETTE'] = $relativePathFromMedia.$pdoc['DA_CHEMIN']."/".$pdoc['DA_FICHIER'];
			}else if ( !empty($pdoc['IM_FICHIER'])){
				$relativePathFromMedia=str_replace(kCheminHttpMedia,'',storyboardChemin);
				$pdoc['VIGNETTE'] = $relativePathFromMedia.$pdoc['IM_CHEMIN']."/".$pdoc['IM_FICHIER'];
			}else{
				$pdoc['VIGNETTE'] ="" ;
			}
			//trace("getPanierDoc : ".$pdoc['ID_LIGNE_PANIER']." =>".$pdoc['VIGNETTE']) ;
			
			$pdoc['ETAT_LIGNE']=GetRefValue('t_etat_pan',$pdoc['PDOC_ID_ETAT'],$langPanier);
			$pdoc['EXT_DUREE']=secToTime(abs(tcToSec($pdoc['PDOC_EXT_TCOUT'])-tcToSec($pdoc['PDOC_EXT_TCIN'])));
			$duree=($pdoc["PDOC_EXTRAIT"]=="1"?$pdoc["EXT_DUREE"]:$pdoc["DOC_DUREE"]);
			$this->duree_panier=secToTime(timeToSec($this->duree_panier)+timeToSec($duree));

			if( $pdoc['PDOC_ID_JOB'] != 0){
				require_once(modelDir.'model_job.php');
				$pdoc_job = new Job();
				$pdoc_job->t_job['ID_JOB'] = $pdoc['PDOC_ID_JOB'];
				$pdoc_job->getJob();
				$pdoc_job->getEtatJob();

				$pdoc['LIGNE_JOB_NOM'] = $pdoc_job->t_job['JOB_NOM'];
				$pdoc['LIGNE_ID_JOB'] = $pdoc_job->t_job['ID_JOB'];
				$pdoc['LIGNE_ID_JOB_ETAT'] = $pdoc_job->t_job['JOB_ID_ETAT'];
				$pdoc['LIGNE_JOB_ETAT'] = $pdoc_job->t_job['ETAT_JOB'];
				$pdoc['LIGNE_JOB_IN'] = $pdoc_job->t_job['JOB_IN'];
				$pdoc['LIGNE_JOB_ID_MAT'] = $pdoc_job->t_job['JOB_ID_MAT'];
				$pdoc['LIGNE_JOB_OUT'] = $pdoc_job->t_job['JOB_OUT'];
				$pdoc['LIGNE_JOB_PARAM'] = ($pdoc_job->t_job['JOB_PARAM']);
			}

		}
		
		require_once(libDir.'class_page.php');
		// On vérifie si un tri sur doc_duree a été demandé 
		if(Page::getInstance()->col == 'doc_duree'){
			$this->sortPanierDocByDuree() ; 
		}
		
		$this->ajoutPrivResult();
    }
	/**
	 * fonction ajoutPrivResult ### identique à celle de class_chercheDoc ###
	 * Alimente le tableau de résultats avec le niveau de privilège de l'utilisateur pour chaque résultat.
	 */
	function ajoutPrivResult(){
		$usr=User::getInstance();
		$fld="DOC_ID_FONDS";
		
		for ($i=0;$i<count($this->t_panier_doc);$i++){
			$this->t_panier_doc[$i]["ID_PRIV"]=5;
			if ($usr->Type!=kLoggedAdmin){
				$this->t_panier_doc[$i]["ID_PRIV"]=0; //par défaut
				
				foreach($usr->Groupes as $value){
					if(isset($this->t_panier_doc[$i][$fld])){
						if($value["ID_FONDS"]==$this->t_panier_doc[$i][$fld]){
							$this->t_panier_doc[$i]["ID_PRIV"]=$value["ID_PRIV"];
						}
					}
				}
			}
		}
		// MS - à optimiser / patcher, je n'aime pas trop l'idée d'avoir une fonction externe pouvant faire n'importe quoi sur le tableau.
		// Conservé pour rétrocompatibilité.
		if(function_exists("ajoutPrivResultCustom")){
			$this->t_panier_doc=ajoutPrivResultCustom($this->t_panier_doc);
		}
		
		return true;
	}
	
	// permet de classer l'array t_panier_doc en fonction de la durée (des extraits ou du doc complet, suivant le cas) ce qui n'est pas faisable en sql
	// se base pour l'instant sur les variables de tri de la class_page mais peut etre etoffée pour plus de controle sur la fonction & le tri
	function sortPanierDocByDuree(){
		if(!function_exists('sort_pdoc_duree')){
			function sort_pdoc_duree($a,$b){
				require_once(libDir.'class_page.php');
				$sens = $ordre=substr(Page::getInstance()->order,-1,1); 
				if(!is_numeric($sens)){
					$sens = 0 ; 
				}
				
				if(($a['PDOC_EXTRAIT']==1?$a['EXT_DUREE']:$a['DOC_DUREE'])<($b['PDOC_EXTRAIT']==1?$b['EXT_DUREE']:$b['DOC_DUREE'])){
					return ($sens == 1 ? 1 : -1);
				}else if (($a['PDOC_EXTRAIT']==1?$a['EXT_DUREE']:$a['DOC_DUREE'])>($b['PDOC_EXTRAIT']==1?$b['EXT_DUREE']:$b['DOC_DUREE'])){
					return ($sens == 1 ? -1 : 1);
				}else if (($a['PDOC_EXTRAIT']==1?$a['EXT_DUREE']:$a['DOC_DUREE']) == ($b['PDOC_EXTRAIT']==1?$b['EXT_DUREE']:$b['DOC_DUREE'])){
					return 0 ;
				}
			}
		}
		uasort($this->t_panier_doc,'sort_pdoc_duree');
	}

    function getDocs() {
    	global $db;
    	require_once(modelDir.'model_doc.php');
    	if (!empty($this->t_panier_doc)) {
			// VP 20/10/2011 : calcul durée panier
			$this->duree_panier="00:00:00";
	    	foreach ($this->t_panier_doc as $idx=>&$pdoc) {

	    		$myDoc=new Doc();
	    		$myDoc->t_doc['ID_DOC']=$pdoc['ID_DOC'];
	    		if(!empty($this->t_panier['ID_LANG']))$myDoc->t_doc['ID_LANG']=$this->t_panier['ID_LANG'];
	    		$myDoc->getDoc();
	    		$myDoc->getValeurs();
	    		$pdoc['DOC']=$myDoc;
				// Calcul durée
				$pdoc['ETAT_LIGNE']=GetRefValue('t_etat_pan',$pdoc['PDOC_ID_ETAT'],$_SESSION['langue']);
				$pdoc['EXT_DUREE']=secToTime(tcToSec($pdoc['PDOC_EXT_TCOUT'])-tcToSec($pdoc['PDOC_EXT_TCIN']));
				$duree=($pdoc["PDOC_EXTRAIT"]=="1"?$pdoc["EXT_DUREE"]:$myDoc->t_doc["DOC_DUREE"]);
				$this->duree_panier=secToTime(timeToSec($this->duree_panier)+timeToSec($duree));
				unset($myDoc);

	    	}
			$this->ajoutPrivResult();
    	}
    }

    function getDocsFull($charger_lex=0,$charger_val=0,$charger_mat=0,$charger_img=0,$charger_seq=0,$charger_redif=0) {
    	global $db;
    	require_once(modelDir.'model_doc.php');
    	if (!empty($this->t_panier_doc)) {
	    	foreach ($this->t_panier_doc as $idx=>&$pdoc) {

	    		$myDoc=new Doc();
	    		$myDoc->t_doc['ID_DOC']=$pdoc['ID_DOC'];
	    		$myDoc->t_doc['ID_LANG']=$this->t_panier['ID_LANG'];
	    		$myDoc->getDocFull($charger_lex,$charger_val,$charger_mat,$charger_img,$charger_seq,$charger_redif,0,0,0,0,0,0,0,false);
	    		$pdoc['DOC']=$myDoc;
	    		unset($myDoc);

	    	}
			$this->ajoutPrivResult();
    	}
    }

    /**
     * Crée ou récupère une commande
     * S'il existe déjà une commande en cours pour cet USAGER/TYPE COMMANDE, on l'utilise,
     * sinon on en crée une (insert t_panier).
     * OUT : version allégée de t_panier si créa, complète si récup
     */
    function getCommande() {
    	global $db;
    	$this->t_panier["PAN_ID_USAGER"]=$_SESSION['USER']['ID_USAGER']; //Usager par défaut

    	// récupération d'une éventuelle commande en cours => ETAT=1 && TYPE_COMMANDE!=0
    	$sql="SELECT * from t_panier WHERE PAN_ID_USAGER=".intval($this->t_panier['PAN_ID_USAGER'])." AND PAN_ID_ETAT=1 AND PAN_ID_TYPE_COMMANDE=".intval($this->t_panier['PAN_ID_TYPE_COMMANDE']);
    	$result=$db->GetAll($sql);
    	if (!$result) { // pas de récupération de commande possible => création d'une commande (=> nv panier)
    		$this->t_panier['PAN_ID_ETAT']=1;

			$record["PAN_ID_ETAT"] = 1;
			$record["PAN_ID_USAGER"] = intval($this->t_panier['PAN_ID_USAGER']);
			$record["PAN_DATE_COM"] = time();
			$record["PAN_DATE_CREA"] = time();
			$record["PAN_ID_TYPE_COMMANDE"] = $this->t_panier['PAN_ID_TYPE_COMMANDE'];

			$ok = $db->insertBase("t_panier","id_panier",$record);
    		if (!$ok) {$this->dropError(kPanierErrorCreationCommande);return false;}
    		$this->t_panier['ID_PANIER']=$ok;

			if (empty($this->t_panier['ID_PANIER'])) $this->t_panier['ID_PANIER']=$db->getOne("SELECT max(ID_PANIER) FROM t_panier WHERE PAN_ID_ETAT=1 AND PAN_ID_USAGER=".intval($this->t_panier['PAN_ID_USAGER'])." AND PAN_ID_TYPE_COMMANDE=".intval($this->t_panier['PAN_ID_TYPE_COMMANDE']));

    	} else {
			if(defined("gCommandeRAZ") && gCommandeRAZ=='1'){
                // VP 10/03/2015 : suppression des lignes si identifiant panier différent de identifiant trouvé (cf bug CFRT)
                if($result[0]['ID_PANIER'] != $this->t_panier['ID_PANIER']){
                    $this->t_panier=$result[0];
                    $this->t_panier['XML']=xml2array($this->t_panier['PAN_XML']);
                    $this->etat_panier=GetRefValue("t_etat_pan",$this->t_panier["PAN_ID_ETAT"],$_SESSION['langue']);
                    $this->deleteToutesLignes();
                }
			}else{
				debug($result, "gold", true);
				if (count($result)>1) {$this->dropError(kPanierErrorPlusieursCommandesEnCours);}
					//plusieurs commande de même type en cours, pas normal, donc avertissement...
				$this->t_panier=$result[0];
				$this->t_panier['XML']=xml2array($this->t_panier['PAN_XML']);
				$this->etat_panier=GetRefValue("t_etat_pan",$this->t_panier["PAN_ID_ETAT"],$_SESSION['langue']);
				$this->dropError(kWarningPanierCommandeEnCours);
			}
		}
    }




    /**
     * createPanier : création d'un panier basique en bdd
     * IN : objet t_panier
     * OUT : ajout en bdd et récupération ID PANIER
     */
    function createPanier() {
    	global $db;
		// VP 25/2/09 : ajout PAN_PUBLIC
		// VP 20/5/11 : ajout Quote pour PAN_ID_TYPE_COMMANDE et affectation PAN_ID_USAGER le cas échéant
        if(!empty($this->t_panier['ID_PANIER'])){
           $record["ID_PANIER"] = intval($this->t_panier['ID_PANIER']);
        }
		$record["PAN_ID_ETAT"] = intval($this->t_panier['PAN_ID_ETAT']);
		$record["PAN_ID_GEN"] = intval($this->t_panier['PAN_ID_GEN']);
		$record["PAN_DOSSIER"] = intval($this->t_panier['PAN_DOSSIER']);
		$record["PAN_PUBLIC"] = intval($this->t_panier['PAN_PUBLIC']);
		$record["PAN_ID_USAGER"] = intval($this->t_panier['PAN_ID_USAGER']);
		$record["PAN_DATE_CREA"] = time();
		$record["PAN_ID_USAGER_MOD"] = intval($this->t_panier['PAN_ID_USAGER']);
		$record["PAN_DATE_MOD"] = time();
		$record["PAN_ID_TYPE_COMMANDE"] = $this->t_panier['PAN_ID_TYPE_COMMANDE'];
		$record["PAN_TITRE"] = $this->t_panier['PAN_TITRE'];
		$record["PAN_FRAIS_HT"] = $this->t_panier['PAN_FRAIS_HT'];
		$record["PAN_TOTAL_HT"] = $this->t_panier['PAN_TOTAL_HT'];
		$record["PAN_TVA"] = $this->t_panier['PAN_TVA'];
		$record["PAN_OBJET"] = $this->t_panier['PAN_OBJET'];
        $record["PAN_DATE_COM"] = $this->t_panier['PAN_DATE_COM'];
		if (!empty($this->t_panier['PAN_XML'])) $record["PAN_XML"] = $this->t_panier['PAN_XML'];
		$ok = $db->insertBase("t_panier","id_panier",$record);
    	if (!$ok) {$this->dropError(kPanierErrorCreation);return false;}
    	$this->t_panier['ID_PANIER']=$ok;
		// if (!$this->t_panier['ID_PANIER'])
			// $this->t_panier['ID_PANIER']=$db->getOne('SELECT max(ID_PANIER) FROM t_panier WHERE PAN_ID_USAGER = '.intval($this->t_panier['PAN_ID_USAGER']));
    	return true;
    }



/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {
	 	if (empty($this->t_panier)) $this->init();

	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_panier[strtoupper($fld)])) $this->t_panier[strtoupper($fld)]=stripControlCharacters($val);
		}

		// Mise à jour de "pan_xml"
		if(!empty($this->t_panier['PAN_XML'])){
			$tabxml=xml2array($this->t_panier['PAN_XML']);

		}else{
			$tabxml = array("commande"=>array());
		}
		// On cherche les propri?t?s relatives ? pan_xml qui sont au format : "PANXML_[mon champs]"
		foreach ($tab_valeurs as $key=>$value){
			$tab_explode = explode("_",$key);
			if (strtoupper($tab_explode[0])=="PANXML"){
				$tabxml['commande'][strtoupper($key)] = $value;
				// $content.="<".$key.">".str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),trim($value))."</".$key.">";

			}
		}

		$this->t_panier['XML']=$tabxml;
		$this->t_panier['PAN_XML']=array2xml($tabxml['commande'],"commande");

		// MS 05/02/13 update des t_panier_doc également
		// Si l'ID_LIGNE_PANIER de l'array est trouvable dans les t_panier_doc existants, on update ceux ci
		// Si l'ID_LIGNE_PANIER ne corresponds à aucun élément du panier, on prends le dernier id de l'array +1 pour créer un t_panier_doc supplémentaire sans impacter l'existant.

		if(!empty($tab_valeurs['t_panier_doc'])) {
			foreach ($tab_valeurs['t_panier_doc'] as $idx=>$pd) {
				if (isset($pd['ID_LIGNE_PANIER'])){
					if($pd['ID_LIGNE_PANIER'] == 'new'){
						$current_pdoc_id = count($this->t_panier_doc);
					}else{
						foreach($this->t_panier_doc as $idx=>$pdoc){
							if($pdoc['ID_LIGNE_PANIER'] == $pd['ID_LIGNE_PANIER']){
								$current_pdoc_id = $idx; break;
							}
						}
					}
					if(!isset($current_pdoc_id)){
						$current_pdoc_id = count($this->t_panier_doc);
					}
				}
				if (isset($pd['ID_DOC'])) {$this->t_panier_doc[$current_pdoc_id]['ID_DOC'] = $pd['ID_DOC'] ;unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_NB_SUP'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_NB_SUP']=$pd['PDOC_NB_SUP'];unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_ID_ETAT'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_ID_ETAT']=$pd['PDOC_ID_ETAT'];unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_SUPPORT_LIV'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_SUPPORT_LIV']=trim($pd['PDOC_SUPPORT_LIV']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_VERSION'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_VERSION']=trim($pd['PDOC_VERSION']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_EXTRAIT'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_EXTRAIT']=$pd['PDOC_EXTRAIT'];unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_EXT_TITRE'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_EXT_TITRE']=trim($pd['PDOC_EXT_TITRE']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_EXT_TCIN'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_EXT_TCIN']=trim($pd['PDOC_EXT_TCIN']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_EXT_TCOUT'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_EXT_TCOUT']=trim($pd['PDOC_EXT_TCOUT']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_PRIX_CALC'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_PRIX_CALC']=$pd['PDOC_PRIX_CALC'];unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['EXT_DUREE'])) {$this->t_panier_doc[$current_pdoc_id]['EXT_DUREE']=trim($pd['EXT_DUREE']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_ID_MAT'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_ID_MAT']=$pd['PDOC_ID_MAT'];unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_ORDRE'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_ORDRE']=trim($pd['PDOC_ORDRE']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['ID_LIGNE_PANIER_ORIG'])) {$this->t_panier_doc[$current_pdoc_id]['ID_LIGNE_PANIER_ORIG']=trim($pd['ID_LIGNE_PANIER_ORIG']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['RESIZE_IMG'])) {$this->t_panier_doc[$current_pdoc_id]['RESIZE_IMG']=trim($pd['RESIZE_IMG']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['ID_MAT'])) {$this->t_panier_doc[$current_pdoc_id]['ID_MAT']=trim($pd['ID_MAT']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_ID_JOB'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_ID_JOB']=trim($pd['PDOC_ID_JOB']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				//XB nouveux champ
				if (isset($pd['PDOC_EXT_DESC'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_EXT_DESC']=trim($pd['PDOC_EXT_DESC']);unset($tab_valeurs['t_panier_doc'][$idx]);}
				if (isset($pd['PDOC_EXT_TAGS'])) {$this->t_panier_doc[$current_pdoc_id]['PDOC_EXT_TAGS']=trim($pd['PDOC_EXT_TAGS']);unset($tab_valeurs['t_panier_doc'][$idx]);}

				if (isset($pd['ID_PROC']))
				{
					$this->t_panier_doc[$current_pdoc_id]['ID_PROC']=trim($pd['ID_PROC']);
					unset($tab_valeurs['t_panier_doc'][$idx]);
				}
			}
		}
	}


    /** Mise � jour de l'objet avec les données du formulaire
     * $tab_valeurs : formulaire (tableau POST)
     * $allow_extension (dft=0), si 1, possibilité d'ajouter des champs à l'objet
	* $updatePanierDoc (dft=true), si true, met à jour aussi t_panier_doc
     * c'est-à-dire, que TOUS les champs du formulaire sont ajoutés à l'objet.
     *
     * **/
	// VP (8/09/09) : ajout paramètre $updatePanierDoc pour mettre à jour ou non t_panier_doc
	// @update VG 25/06/2010 : ajout de trim() sur les valeurs
	//VG 08/12/10 : ajout du test !empty sur t_panier_doc avant de faire un foreach dessus
    function met_a_jour($tab_valeurs, $allow_extension=1, $updatePanierDoc=true){ // $tab_valeurs correspond au $_POST du formulaire
		global $db;
		//30 01 09 by ld => suppression du simpleQuote
		foreach ($tab_valeurs as $fld=>$val) {
			if ((isset($this->t_panier[strtoupper($fld)]) || $allow_extension==1) && !is_array($val)) 
                            $this->t_panier[strtoupper($fld)]=trim($val);
		}

		// Mise � jour de "pan_xml"
		$content="";
		// On cherche les propri?t?s relatives ? pan_xml qui sont au format : "PANXML_[mon champs]"
		foreach ($tab_valeurs as $key=>$value){
			$tab_explode = explode("_",strtoupper($key));
			if ($tab_explode[0]=="PANXML"){
				if(strtoupper($key ) == 'PANXML_MONTAGE'){
					
					$content.="<".strtoupper($key).">".trim($value)."</".strtoupper($key).">";
				}else{
					if(is_array($value)){
						foreach($value as $v){
						$content.="<".strtoupper($key).">".str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),trim($v))."</".strtoupper($key).">";
	
						}
						
					}else{
					$content.="<".strtoupper($key).">".str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),trim($value))."</".strtoupper($key).">";
					}
				}
			}
		}
		if($content!=""){
			$content = "<commande>".$content."</commande>";
			$this->t_panier['PAN_XML']=$content;
			$this->t_panier['XML']=xml2array($this->t_panier['PAN_XML']);
		}
		// Mise à jour des lignes du panier
		/*
		$i=1;
		while (isset($tab_valeurs["id_ligne_panier".$i])) {
			$this->t_panier_doc[$i-1]["PDOC_SUPPORT_LIV"]=$tab_valeurs["pdoc_support_liv".$i];
			$this->t_panier_doc[$i-1]["PDOC_NB_SUP"]=$tab_valeurs["pdoc_nb_sup".$i];
			$this->t_panier_doc[$i-1]["ID_LIGNE_PANIER"]=$tab_valeurs["id_ligne_panier".$i];
			$i++;
		}
		*/
		if($updatePanierDoc){
			// VP 25/11/10 : ajout paramètre ID_MAT
			if(!empty($tab_valeurs['t_panier_doc'])) {
				//PC 09/04/13 : Permet de ne pas détruire les tableaux t_panier_doc préalablement bien formés
				if (isset($tab_valeurs['t_panier_doc'][0]) && is_array($tab_valeurs['t_panier_doc'][0]) && count($tab_valeurs['t_panier_doc'][0]) == 1)
					foreach ($tab_valeurs['t_panier_doc'] as $idx=>$pd) {
						if (isset($pd['ID_LIGNE_PANIER'])) {$lastID=$idx;$tab_valeurs['t_panier_doc'][$idx]['ID_PANIER']=$this->t_panier['ID_PANIER'];}
						if (isset($pd['ID_DOC'])) {$tab_valeurs['t_panier_doc'][$lastID]['ID_DOC']=$pd['ID_DOC'];unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_NB_SUP'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_NB_SUP']=$pd['PDOC_NB_SUP'];unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_ID_ETAT'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_ID_ETAT']=$pd['PDOC_ID_ETAT'];unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_SUPPORT_LIV'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_SUPPORT_LIV']=trim($pd['PDOC_SUPPORT_LIV']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_VERSION'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_VERSION']=trim($pd['PDOC_VERSION']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_EXTRAIT'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_EXTRAIT']=$pd['PDOC_EXTRAIT'];unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_EXT_TITRE'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_EXT_TITRE']=trim($pd['PDOC_EXT_TITRE']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_EXT_TCIN'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_EXT_TCIN']=trim($pd['PDOC_EXT_TCIN']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_EXT_TCOUT'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_EXT_TCOUT']=trim($pd['PDOC_EXT_TCOUT']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_PRIX_CALC'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_PRIX_CALC']=$pd['PDOC_PRIX_CALC'];unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['EXT_DUREE'])) {$tab_valeurs['t_panier_doc'][$lastID]['EXT_DUREE']=trim($pd['EXT_DUREE']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_ID_MAT'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_ID_MAT']=$pd['PDOC_ID_MAT'];unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_ORDRE'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_ORDRE']=trim($pd['PDOC_ORDRE']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['ID_LIGNE_PANIER_ORIG'])) {$tab_valeurs['t_panier_doc'][$lastID]['ID_LIGNE_PANIER_ORIG']=trim($pd['ID_LIGNE_PANIER_ORIG']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['RESIZE_IMG'])) {$tab_valeurs['t_panier_doc'][$lastID]['RESIZE_IMG']=trim($pd['RESIZE_IMG']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['ID_MAT'])) {$tab_valeurs['t_panier_doc'][$lastID]['ID_MAT']=trim($pd['ID_MAT']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						//XB nouveux champ
						if (isset($pd['PDOC_EXT_DESC'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_EXT_DESC']=trim($pd['PDOC_EXT_DESC']);unset($tab_valeurs['t_panier_doc'][$idx]);}
						if (isset($pd['PDOC_EXT_TAGS'])) {$tab_valeurs['t_panier_doc'][$lastID]['PDOC_EXT_TAGS']=trim($pd['PDOC_EXT_TAGS']);unset($tab_valeurs['t_panier_doc'][$idx]);}

					}
				//trace("fin met_a_jour ".print_r($tab_valeurs['t_panier_doc'],true));
				$this->t_panier_doc=$tab_valeurs['t_panier_doc'];
			}
		}

		// Si j'ai une ligne spécifiée dans le formulaire, on l'intègre dans l'objet.
		// Attention, il s'agit  de l'ID_LIGNE_PANIER
		if (isset($tab_valeurs["ligne"])) $this->currentLine=(integer)$tab_valeurs['ligne'];

    }

	/** Mise à jour d'une propriété pour toutes les lignes du panier **/
	function met_a_jour_lignes ($property,$value) {
		foreach ($this->t_panier_doc as $idx=>$ligne) $this->t_panier_doc[$idx][$property]=$value;
	}


	/** Sauve en base le panier en l'état, lignes comprises **/
	function updatePanier () {
			global $db;

			$sql="UPDATE t_panier SET ";

			foreach ($this->t_panier as $fld=>$val) {
				//if ($val==null) $val="NULL"; else
				$val=$db->Quote($val); //Toute valeur non null est quotée et échappée
				if (strpos($fld,'PAN_')===0) $tab[]= " ".strtoupper($fld)."=".$val." ";
			}
			$sql.=implode(",",$tab);
			$sql.=" WHERE ID_PANIER=".$this->t_panier["ID_PANIER"];

			$db->Execute($sql);

			foreach ($this->t_panier_doc as $idx=>$pdoc) {
				$sql="UPDATE t_panier_doc SET ";
				unset($tab);


				foreach ($pdoc as $fld=>$val) {

					$val=$db->Quote($val);
					if (strpos($fld,'PDOC_')===0 || $fld=='ID_PANIER') $tab[]= " ".strtoupper($fld)."=".$val." ";
				}
				$sql.=implode(",",$tab);
				$sql.=" WHERE ID_PANIER='".$this->t_panier["ID_PANIER"]."' AND ID_LIGNE_PANIER=".intval($this->t_panier_doc[$idx]["ID_LIGNE_PANIER"]);

				$db->Execute($sql);
			}

	}

	function duplicate($id_gen, $dataToUpd){
		global $db;
		if (empty($this->t_panier_doc)) $this->getPanierDoc();
        $newCart=new Panier();
		$newCart->t_panier=array(
				"PAN_ID_ETAT"=>($this->t_panier['PAN_ID_ETAT']=="1"?"0":$this->t_panier['PAN_ID_ETAT']),
				"PAN_ID_GEN"=>$this->t_panier['PAN_ID_GEN'],
				"PAN_DOSSIER"=>$this->t_panier['PAN_DOSSIER'],
				"PAN_PUBLIC"=>$this->t_panier['PAN_PUBLIC'],
				"PAN_FRAIS_HT"=>$this->t_panier['PAN_FRAIS_HT'],
				"PAN_TOTAL_HT"=>$this->t_panier['PAN_TOTAL_HT'],
				"PAN_TVA"=>$this->t_panier['PAN_TVA'],
				"PAN_OBJET"=>$this->t_panier['PAN_OBJET'],
				"PAN_XML"=>$this->t_panier['PAN_XML'],
				"PAN_DATE_COM"=>$this->t_panier['PAN_DATE_COM'],
				"PAN_TITRE"=>$this->t_panier['PAN_TITRE'],
				"PAN_ID_TYPE_COMMANDE"=>$this->t_panier['PAN_ID_TYPE_COMMANDE'],
				"PAN_ID_DOC_ACC"=>$this->t_panier['PAN_ID_DOC_ACC'],
				"PAN_ID_USAGER"=>$this->t_panier['PAN_ID_USAGER']);//User::getInstance()->UserID);
		if(isset($id_gen))
			$newCart->t_panier['PAN_ID_GEN']=$id_gen;
		if( !empty( $dataToUpd ) ) {
			$newCart->met_a_jour( $dataToUpd );
		}
		$ok=$newCart->createPanier();
		if($this->t_panier['PAN_ID_ETAT']=="0") $this->t_panier['CARTLIST']=$newCart->t_panier['ID_PANIER'];

		$err=0;
		foreach ($this->t_panier_doc as $pdoc){ //transfert de toutes les lignes du panier dans le nouveau
				$pdoc['ID_LIGNE_PANIER']="";
				$pdoc['ID_PANIER']=$newCart->t_panier['ID_PANIER'];
                $pdoc['PDOC_ID_JOB']=0;
				if (!$newCart->savePanierDoc($pdoc)) $err++;
				else $newCart->t_panier_doc[] = $pdoc;
		}

		$newCart->t_panier['XML']=xml2array($newCart->t_panier['PAN_XML']);
		$newCart->etat_panier=GetRefValue("t_etat_pan",$newCart->t_panier["PAN_ID_ETAT"],$_SESSION['langue']);
		if (in_array($this->t_panier['PAN_ID_TYPE_COMMANDE'],unserialize(gModesAvecCalculPrix))==true) $this->calc_prices=true;
		$this->getVignette();
		if ($err==0) $this->dropError(kSuccesPanierDupliquer);
		$this->getChildren();
		foreach ($this->arrChildren as $child){
			$cart=new Panier();
			$cart->t_panier['ID_PANIER']=$child['ID_PANIER'];
			$cart->getPanier();
			$cart->duplicate($newCart->t_panier['ID_PANIER'], $dataToUpd);
			}
		return $newCart;
    }

	function getChildren(){
    	global $db;
        $id_panier=intval($this->t_panier['ID_PANIER']);
        if(!empty($id_panier)){
            $sql=" select * from t_panier  where pan_id_gen=".intval($this->t_panier['ID_PANIER']);
            $this->arrChildren=$db->GetAll($sql);
        }
    }


	/**
	 * Update une ligne de type panier_doc en base en se basant sur les champs présents dans l'objet (et commençant par PDOC)
	 */
	function savePanierDoc(&$lignePanierDoc,$ordre=null,$chkDoublon=false) {
		global $db;
		if (empty($lignePanierDoc["ID_LIGNE_PANIER"])) { //pas d'id ? il faut créer la ligne !
			//note => on crée en mode 'silencieux' pour ne pas afficher d'erreurs si le prog est déjà en cours de commande
			// VP 30/06/09 : retour insertPanierDoc plutôt que false
			if (isset($lignePanierDoc["ID_LIGNE_PANIER"])) unset($lignePanierDoc["ID_LIGNE_PANIER"]);
			return $this->insertPanierDoc($lignePanierDoc,1,$ordre);
			//return false;
		}
		if ($chkDoublon) { //CAS TRANSFERT DE LIGNES ENTRE PANIERS : check doublon
			//LD 18 11 08  : avant de faire l'update, on va vérifier s'il n'y pas de doublon, ce cas peut arriver en cas
			//de transfert d'un panier vers un autre qui contiendrait déjà le même programme

//                        echo 'DEBUG_affich : $lignePanierDoc[\'PDOC_EXTRAIT\']'.var_dump($lignePanierDoc['PDOC_EXTRAIT']).'<br />';
//                        echo 'DEBUG_affich : $lignePanierDoc[\'PDOC_EXT_TCIN\']'.var_dump($lignePanierDoc['PDOC_EXT_TCIN']).'<br />';
//                        echo 'DEBUG_affich : $lignePanierDoc[\'PDOC_EXT_TCOUT\']'.var_dump($lignePanierDoc['PDOC_EXT_TCOUT']).'<br />';
                    
                        /*  @author B.RAVI 2016-01-07
                         *  Bug insérait en double
                         *  car si ce n'est pas un extrait, $lignePanierDoc['PDOC_EXTRAIT']=$lignePanierDoc['PDOC_EXT_TCIN']=
                         *  $ lignePanierDoc['PDOC_EXT_TCOUT']   = NULL
                         *  dans $sql, on recherchait ....PDOC_EXTRAIT=0 AND PDOC_EXT_TCIN='' AND 
                            PDOC_EXT_TCOUT=''
                         *  or en BDD, il y avait pourtant déja des élements non extrait avec pdoc_ext_tcin=00:00:00:00,
                         *  pdoc_ext_tcout=00:00:00:00,pdoc_extrait=NULL
                        */ 
                        
                        if (empty($lignePanierDoc['PDOC_EXTRAIT'])) {
                        $sql_extrait='(PDOC_EXTRAIT=0 or PDOC_EXTRAIT is NULL)';}
                        else{
                        $sql_extrait="PDOC_EXTRAIT=".intval($lignePanierDoc['PDOC_EXTRAIT']);    
                        }
                        
                        if (empty($lignePanierDoc['PDOC_EXT_TCIN']) || $lignePanierDoc['PDOC_EXT_TCIN']=='00:00:00:00') {
                        $sql_tcin="(PDOC_EXT_TCIN='' or PDOC_EXT_TCIN is NULL or PDOC_EXT_TCIN='00:00:00:00')";}
                        else{
                        $sql_tcin="PDOC_EXT_TCIN='".$lignePanierDoc['PDOC_EXT_TCIN']."'";   
                        }
                        
                        if (empty($lignePanierDoc['PDOC_EXT_TCOUT']) || $lignePanierDoc['PDOC_EXT_TCOUT']=='00:00:00:00') {
                        $sql_tcout="(PDOC_EXT_TCOUT='' or PDOC_EXT_TCOUT is NULL or PDOC_EXT_TCOUT='00:00:00:00')";}
                        else{
                        $sql_tcout="PDOC_EXT_TCOUT='".$lignePanierDoc['PDOC_EXT_TCOUT']."'";   
                        }
                        
                    
			$sql="SELECT ID_LIGNE_PANIER FROM t_panier_doc WHERE ID_PANIER=".intval($lignePanierDoc['ID_PANIER'])."
					AND ID_DOC=".intval($lignePanierDoc["ID_DOC"])." AND ".$sql_extrait." AND ".$sql_tcin." AND ".$sql_tcout;
                        
//                        echo 'debug_affich $sql1 '.$sql.'<br />';
			if ($db->GetOne($sql)!=false) {
					//by le 20 11 08 : msg erreur rendu muet : on gère les erreurs dans le fichier appelant pour groupe de compte-rendu
					//$this->dropError(kErrorPanierLigneExisteDeja);
					return false;
			}
		}

		// VP (14/10/08) : ajout champ ID_LIGNE_PANIER_ORIG dans la sauvegarde
		if ($ordre!==null && (!isset($lignePanierDoc['PDOC_ORDRE']) || !$lignePanierDoc['PDOC_ORDRE'])) {
			$lignePanierDoc["PDOC_ORDRE"]= $ordre;
		}
		$rs = $db->Execute("SELECT * FROM t_panier_doc WHERE ID_LIGNE_PANIER=".intval($lignePanierDoc["ID_LIGNE_PANIER"]));
		$sql = $db->GetUpdateSQL($rs, $lignePanierDoc);
//                echo 'debug_affich $sql2 '.$sql.'<br />';
		if (!empty($sql)) $ok = $db->Execute($sql);
		debug($sql, "red", true);
		if (!empty($sql) && !$ok) {$this->dropError(kErrorPanierSauveLigne);return false;}
		return true;
	}



	/**
	 * Insère une ligne dans la table panier_doc basée sur les champs présents dans
	 * le tab de classe t_panier_doc.
	 * IN : ByRef lignePanierDoc = un item du tableau t_panier_doc, NoMsg 0/1, si 1, pas de msg de sortie en contrôle de doublon
	 * OUT : true/false si succès/échec, MAJ BDD et maj de la ligne avec ID_LIGNE_PANIER.
	 * NOTE : le param NoMsg est utilisé dans le cas de commande => on évite l'affichage de msg d'erreur si l'utilisateur
	 * ajoute à la commande en cours des programmes déjà en cours de commande.
	 * NOTE2 : le contrôle de doublons s'effectue sur des programmes ENTIERS (EXTRAIT = null).
	 */
	function insertPanierDoc(&$lignePanierDoc,$nomsg=0,$ordre=null) {
		global $db;
		if (empty($lignePanierDoc['ID_DOC'])) {$this->dropError(kErrorPanierSauveLigneManqueIdDoc);return false;}
		/*$sql="SELECT * FROM t_panier_doc WHERE ID_PANIER=".$db->Quote($this->t_panier['ID_PANIER'])."
				AND ID_DOC=".$db->Quote($lignePanierDoc["ID_DOC"])." AND (PDOC_EXTRAIT is null or PDOC_EXTRAIT='0'
				or (PDOC_EXTRAIT='1' AND PDOC_EXT_TCIN='".$lignePanierDoc['PDOC_EXT_TCIN']."' AND PDOC_EXT_TCOUT='".$lignePanierDoc['PDOC_EXT_TCOUT']."' ) ) ";*/
                
//                echo 'DEBUG_affich : $lignePanierDoc[\'PDOC_EXTRAIT\']'.var_dump($lignePanierDoc['PDOC_EXTRAIT']).'<br />';
//                echo 'DEBUG_affich : $lignePanierDoc[\'PDOC_EXT_TCIN\']'.var_dump($lignePanierDoc['PDOC_EXT_TCIN']).'<br />';
//                echo 'DEBUG_affich : $lignePanierDoc[\'PDOC_EXT_TCOUT\']'.var_dump($lignePanierDoc['PDOC_EXT_TCOUT']).'<br />';
                
                /*  @author B.RAVI 2016-01-07
                *  Bug insérait en double
                *  car si ce n'est pas un extrait, $lignePanierDoc['PDOC_EXTRAIT']=$lignePanierDoc['PDOC_EXT_TCIN']=
                *  $ lignePanierDoc['PDOC_EXT_TCOUT']   = NULL
                *  dans $sql, on recherchait ....PDOC_EXTRAIT=0 AND PDOC_EXT_TCIN='' AND 
                   PDOC_EXT_TCOUT=''
                *  or en BDD, il y avait pourtant déja des élements non extrait avec pdoc_ext_tcin=00:00:00:00,
                *  pdoc_ext_tcout=00:00:00:00,pdoc_extrait=NULL
                */ 
                        
                if (empty($lignePanierDoc['PDOC_EXTRAIT'])) {
                $sql_extrait='(PDOC_EXTRAIT=0 or PDOC_EXTRAIT is NULL)';}
                else{
                $sql_extrait="PDOC_EXTRAIT=".intval($lignePanierDoc['PDOC_EXTRAIT']);    
                }

                if (empty($lignePanierDoc['PDOC_EXT_TCIN']) || $lignePanierDoc['PDOC_EXT_TCIN']=='00:00:00:00') {
                $sql_tcin="(PDOC_EXT_TCIN='' or PDOC_EXT_TCIN is NULL or PDOC_EXT_TCIN='00:00:00:00')";}
                else{
                $sql_tcin="PDOC_EXT_TCIN='".$lignePanierDoc['PDOC_EXT_TCIN']."'";   
                }

                if (empty($lignePanierDoc['PDOC_EXT_TCOUT']) || $lignePanierDoc['PDOC_EXT_TCOUT']=='00:00:00:00') {
                $sql_tcout="(PDOC_EXT_TCOUT='' or PDOC_EXT_TCOUT is NULL or PDOC_EXT_TCOUT='00:00:00:00')";}
                else{
                $sql_tcout="PDOC_EXT_TCOUT='".$lignePanierDoc['PDOC_EXT_TCOUT']."'";   
                }

                
		$sql="SELECT * FROM t_panier_doc WHERE ID_PANIER=".intval($this->t_panier['ID_PANIER'])."
				AND ID_DOC=".intval($lignePanierDoc["ID_DOC"])." AND ".$sql_extrait." AND ".$sql_tcin." AND ".$sql_tcout;
                
//              echo 'debuf_affich $sql '.$sql.'<br />';

		if (count($db->GetAll($sql))>0) {
				if ($nomsg==0) $this->dropError(kErrorPanierLigneExisteDeja);
				return false;
		}
                

		$lignePanierDoc["ID_PANIER"]=$this->t_panier['ID_PANIER'];

		$copyPanierDoc=$lignePanierDoc; // on copie le tableau car on doit y oter des champs
		unset($copyPanierDoc["ETAT_LIGNE"]); // Ces deux champs sont calculés et ne sont pas dans la base
		unset($copyPanierDoc["EXT_DUREE"]); // donc on les retire

		foreach ($copyPanierDoc as $fld=>$val) { //LD 270808, on vire ts les champs non présents dans t_panier_doc
			if (strpos($fld,'PDOC_')===false && $fld!='ID_LIGNE_PANIER_ORIG' && $fld!='ID_PANIER' && $fld!='ID_DOC' && $fld!='ID_LIGNE_PANIER') unset ($copyPanierDoc[$fld]);
		}

		//LD 26082008 : si on fournit un ordre, qui n'est pas dans le formulaire, on l'ajoute
		//Note : celui provenant éventuellement d'un formulaire est prioritaire évidemment.
		if ($ordre!==null && !$copyPanierDoc["PDOC_ORDRE"]) $copyPanierDoc["PDOC_ORDRE"]=$ordre;

		$ok = $db->insertBase("t_panier_doc","id_ligne_panier",$copyPanierDoc);
		if (!$ok) {$this->dropError(kErrorPanierSauveLigne);return false;}
		$lignePanierDoc["ID_LIGNE_PANIER"]=$ok;
		unset($copyPanierDoc);
		return true;

	}

	/** Update toutes les lignes Panier_doc en base
	 *
	 */
	function saveAllPanierDoc() {
		$err=0;
		//trace(print_r($this->t_panier_doc,1));
		$this->t_panier_doc=array_values($this->t_panier_doc); //ld 260808 pour réinitialiser les n° de ligne
		foreach ($this->t_panier_doc as $idx=>&$pdoc) { if(!$this->savePanierDoc($pdoc,$idx)) $err++; }
		//by ld 18/12/08 : ajout retour de fonction
		if ($err==0) return true; else return false;
	}

	/** Ajout de doc à un panier, avec contrôle de doublons
	 *  IN : tab class t_panier_doc, $arr (tableau des docs à ajouter)
	 *  OUT : t_panier_doc mergé, nb d'items ajoutés
	 *  NOTE : fonction utilisée pour reverser un panier de session dans le panier user après login
	 *  NOTE2 : un contrôle de doublon est fait au moment de l'insertion mais on préfère anticiper
	 * 	pour alléger les ordres sql.
	 */
	function mergePanierDoc($arr) {
		$nb=0;
		$arrIds=array();
		foreach ($this->t_panier_doc as $pdoc) {
			// on constitue d'abord un tableau des id_doc existant déjà dans le panier (hors extraits)
			if (!in_array($pdoc['ID_DOC'],$arrIds) && $pdoc['PDOC_EXTRAIT']==0) $arrIds[]=$pdoc['ID_DOC'];
		}
		foreach ($arr as $newdoc) {  //parcours du tableau de docs à ajouter
			//si le doc n'est pas déjà présent dans le panier ou qu'il s'agit d'un extrait
			if (!in_array($newdoc['ID_DOC'],$arrIds) || $newdoc['PDOC_EXTRAIT']==1) {
				unset($newdoc['ID_LIGNE_PANIER']); //raz n° de ligne pour insertion
				$this->t_panier_doc[]=$newdoc;
				$nb++;
			}
		}
		return $nb;
	}

	/** Récupération du max PDOC_ORDRE
	 * 	IN :
	 *  OUT : ordre
	 */
	function getMaxPdocOrdre(){
	 	global $db;
		return $db->GetOne("select max(PDOC_ORDRE) from t_panier_doc where ID_PANIER=".intval($this->t_panier['ID_PANIER']));
	}
	/** Récupération du nombre de téléchargements
	 * 	IN : tableau de classe t_panier_doc (pour calcul de toutes les lignes) ou ligne pdoc (pour une ligne)
	 *  OUT : nv colonne 'volatile' (non sauvée, mais exportée) NB_DOWNLOAD ajoutée à chaque ligne panier_doc
	 */
	 function getNbDownload(&$pdoc) {
	 	global $db;
	 	if (isset($pdoc) && is_array($pdoc)) { //juste une ligne (passée en argument)
			$sql="select count(*) as cnt
					from t_action a
					INNER JOIN t_livraisons l ON a.ACT_REQ = ''||l.Dossier
					WHERE a.ACT_SQL=".$db->Quote($pdoc['ID_LIGNE_PANIER'])."
					AND l.octets_emis=l.octets_reels";
		    $pdoc['NB_DOWNLOAD']=$db->GetOne($sql);
			return true;
	 	}
	 	else
	 	foreach ($this->t_panier_doc as $idx=>&$pdoc) { //toutes les lignes
	 		$sql="select count(*) as cnt
					from t_action a
					INNER JOIN t_livraisons l ON a.ACT_REQ = ''||l.Dossier
					WHERE a.ACT_SQL=".$db->Quote($pdoc['ID_LIGNE_PANIER'])."
					AND l.octets_emis=l.octets_reels";
			$pdoc['NB_DOWNLOAD']=$db->GetOne($sql);
	 	}
	 }

	/** Insère une ligne panier_doc et MAJ le tableau t_panier_doc
	 *
	 */

	function insertLigne($id_doc,$ordre='auto') {


		global $db;
		$sql="SELECT * FROM t_panier_doc WHERE ID_PANIER=".intval($this->t_panier['ID_PANIER'])."
				AND ID_DOC=".intval($id_doc)." AND (PDOC_EXTRAIT is null or PDOC_EXTRAIT=0) ";
		if (count($db->GetAll($sql))>0) {
				if ($this->t_panier['PAN_ID_ETAT']==1) //panier
				$this->dropError(kErrorPanierLigneExisteDeja); //on distingue les msg
				else //folder
				$this->dropError(kErrorFolderLigneExisteDeja);
				return false;}

		if ($ordre=='auto' && intval($this->getMaxPdocOrdre())!=0 ){
			$pordre = intval($this->getMaxPdocOrdre());
		} else if (is_int($ordre)) {
			$pordre = $ordre;
		} else {
			$pordre = 0;
		}
		$ok = $db->insertBase("t_panier_doc","id_ligne_panier", array("ID_PANIER" => intval($this->t_panier['ID_PANIER']), "ID_DOC" => intval($id_doc), "PDOC_NB_SUP" => 1, "PDOC_ORDRE" => $pordre));
		if (!$ok) {$this->dropError(kErrorPanierSauveLigne);return false;}
		$idlp = $ok;
		$this->t_panier_doc[]=array("ID_LIGNE_PANIER"=> $idlp, "ID_PANIER"=>$this->t_panier['ID_PANIER'], "ID_DOC"=>$id_doc,"PDOC_NB_SUP"=>1, "PDOC_ORDRE" => $pordre);

		return true;

	}

	/** Enlève une ligne du panier par id_doc : uniquement le programme entier et pas des extraits
	 *  IN : ID_DOC
	 *  OUT : base maj
	 */
	function removeLigne($id_doc) {

	global $db;
		$sql="SELECT ID_LIGNE_PANIER FROM t_panier_doc WHERE ID_PANIER=".intval($this->t_panier['ID_PANIER'])."
				AND ID_DOC=".intval($id_doc)." AND (PDOC_EXTRAIT is null or PDOC_EXTRAIT=0) ";
		$id_ligne_panier=$db->GetOne($sql);

		if (!$id_ligne_panier) return false;
		$sql="DELETE FROM t_panier_doc WHERE ID_LIGNE_PANIER=".intval($id_ligne_panier);
		$ok=$db->Execute($sql);
		if (!$ok) {$this->dropError(kErrorPanierRemoveLigne);return false;}

		foreach ($this->t_panier_doc as $idx=>$pdoc) { //on retire la ligne de l'objet
			if ($pdoc['ID_LIGNE_PANIER']==$id_ligne_panier) unset ($this->t_panier_doc[$idx]);
		}
		return true;


	}


	function save() {
		global $db;

		// VP 24/05/10 : mise à jour date et usager modification
		$this->t_panier['PAN_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_panier['PAN_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];

		$rs = $db->Execute("SELECT * FROM t_panier WHERE ID_PANIER=".intval($this->t_panier['ID_PANIER']));
		$sql = $db->GetUpdateSQL($rs, $this->t_panier);
		if (!empty($sql)) $ok = $db->Execute($sql);
		debug($sql , "red", true);
		if (!$ok) {$this->dropError(kErrorPanierSauve);return false;}
		return true;
	}

/** Sauvegarde récursive d'une valeur d'un panier : la valeur est propagée à tous les sous-dossiers
 *  IN : $fields => array type FIELD/VALUE contenant les champs à sauver, ($id, vide à l'appel mais rempli récursivement)
 * 	OUT : base mise à jour, message si erreur
 */
	function saveRecursif($fields,$id='') {
		global $db;
  		if (empty($this->t_panier['ID_PANIER'])&& empty($id)) {$this->dropError(kErrorPanierNoId);return false;}
        if (empty($id)) $id=$this->t_panier['ID_PANIER'];
        //récupération des fils
        $sql="SELECT ID_PANIER from t_panier WHERE pan_id_gen=".intval($id);
        $sons=$db->GetAll($sql);

		$rs = $db->Execute("SELECT * FROM t_panier WHERE ID_PANIER=".intval($id));
		$sql = $db->GetUpdateSQL($rs, $fields);
        // VP 18/4/14 : $ok par défaut à true car si'il n'y a rien à sauvegarder $sql est vide et on ne passe pas par $db->Execute($sql);
        $ok=true;
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok) {$this->dropError(kErrorPanierSauve);return false;}

        foreach ($sons as $son) { $this->saveRecursif($fields,$son['ID_PANIER']); } //appel suppression des fils



	}

    /** Enregistrement des propriétés d'une ligne en particulier  */
    function updateLigne() {
        global $db;

		$rs = $db->Execute("SELECT * FROM t_panier_doc WHERE ID_PANIER=".intval($this->t_panier["ID_PANIER"])." AND ID_LIGNE_PANIER=".intval($this->t_panier_doc[$this->currentLine]['ID_LIGNE_PANIER']));
		$sql = $db->GetUpdateSQL($rs, $this->t_panier_doc[$this->currentLine]);
		if (!empty($sql)) $ok = $db->Execute($sql);
    }

    /** Suppression définitive d'une ligne de panier **/
    function deletePanierDoc($lignePanierDoc) {
    	 if (empty($lignePanierDoc['ID_LIGNE_PANIER'])) {return false;}
     	deleteSQLFromArray(array("t_panier_doc"),"ID_LIGNE_PANIER",$lignePanierDoc['ID_LIGNE_PANIER']);
     	$key=array_search($lignePanierDoc,$this->t_panier_doc,true);
     	if ($key!==false) unset($this->t_panier_doc[$key]); //la ligne est également supprimée de l'objet
    }

    /** Vidage des lignes du panier **/
    function deleteToutesLignes () {
    	if (empty($this->t_panier['ID_PANIER'])) {$this->dropError(kErrorPanierNoId);return false;}
    	deleteSQLFromArray(array("t_panier_doc"),"ID_PANIER",$this->t_panier['ID_PANIER']);
    	return true;
    }

    /** Suppression totale du panier et des lignes
     * Transformée en fonction récursive pour effacer tous les sous-dossiers, sélections etc.
     * **/
    function deletePanier($id='') {
    	global $db;
    	if (empty($this->t_panier['ID_PANIER'])&& empty($id)) {$this->dropError(kErrorPanierNoId);return false;}
        if (empty($id)) $id=$this->t_panier['ID_PANIER'];
        //récupération des fils
        $sql="SELECT ID_PANIER from t_panier WHERE pan_id_gen=".intval($id);
        $sons=$db->GetAll($sql);
        deleteSQLFromArray(array("t_panier","t_panier_doc"),"ID_PANIER",$id); //suppression panier et contenu
        foreach ($sons as $son) { $this->deletePanier($son['ID_PANIER']); } //appel suppression des fils
        return true;

    }

    /**
     * Transfères les item du panier en cours vers le nouveau panier créé
     * à la validation de la commande.
     */
    function transfereLignes() {
    	global $db;

    	foreach ($this->t_panier_doc as $idx=>$doc) {
    		$lstID[]=$doc['ID_LIGNE_PANIER']; //on ne transfère que les lignes avec prix
		}

    	$rs = $db->Execute("SELECT * FROM t_panier_doc WHERE ID_PANIER=".intval($this->previousId)." AND ID_LIGNE_PANIER IN (".implode(",",$lstID).")".($this->calc_prices?" AND PDOC_PRIX_CALC<>0":""));
		$sql = $db->GetUpdateSQL($rs, array("ID_PANIER" => intval($this->t_panier['ID_PANIER'])));
		if (!empty($sql)) $ok = $db->Execute($sql);
    	//echo $sql;
    	$db->Execute($sql);
    }




	/** Envoie d'un mail ÔøΩ l'usager pour lui dire que son statut est validÔøΩ
	 *
	 * Enter description here ...
	 * @param unknown_type $envoieAdmin
	 * @param unknown_type $envoieUser
	 * @param unknown_type $sujet
	 * @return boolean
	 *
	 * @update VG 13/04/11 : ajout du paramètre sujet dans la signature
	 * @update VG 28/06/11 : ajout de la personnalisation du mail admin
	 * @update VG 05/03/12 : différenciation du mail d'expéditeur et du mail admin
	 * **/

    function send_mail_usager($envoieAdmin=0,$envoieUser=1,$sujet='',$mailAdmin = ''){
        trace('send_mail_usager');
    	if(empty($mailAdmin) && defined('gMailCommande')) {
    		$mailAdmin = gMailCommande;
    	}
        // b. Cr�ation du message
		if($envoieAdmin == 0 && $envoieUser == 0 ){
			return false ; 
		}

		$mailLanguage = $_SESSION[ 'langue' ];
		// MS - ajout récupération du mail de l'utilisateur associé au panier si on a pas d'utilisateur courant (ex : appel par frontal)
		if(!empty($this->t_panier['PAN_ID_USAGER'])){
			if(empty($this->owner)){
				$this->getProprioPanier();
			}

			$email = $this->owner->t_usager['US_SOC_MAIL'];
			
			if( !empty( $this->owner->t_usager[ 'US_ID_LANG' ] ) && strcasecmp( $this->owner->t_usager[ 'US_ID_LANG' ], $_SESSION[ 'langue' ] ) != 0 ) {
			    $mailLanguage = $this->owner->t_usager[ 'US_ID_LANG' ];

			}
			
		}
		if(empty($mailLanguage) && defined('langueDefaut'))
		    $mailLanguage = langueDefaut;
		
		$oPanierTmp = new Panier();
		$oPanierTmp->t_panier['ID_PANIER'] = $this->t_panier['ID_PANIER'];
		$oPanierTmp->getData( array(), $mailLanguage, true );
		
		if(empty($email)) $email=$this->User->Mail;

		$frontiere = "_----------=_parties_".md5(uniqid (rand()));
        $param = array("etat" => $this->panier['PAN_ID_ETAT'],
					   "playerVideo" => gPlayerVideo,
					   "imgurl"=>kCheminHttp."/design/images/",
					   "profil"=>User::getInstance()->getTypeLog(),
					   "boundary"=>$frontiere,
					   "envoieAdmin"=>$envoieAdmin,
					   "envoieUser"=>$envoieUser,
        				"mail_to_admin" => 0 //Pour identifier qui est le destinataire, et gérer une version différente du mail pour l'admin et l'utilisateur
                );

        $xml=$oPanierTmp->xml_export();
        $xml.=$oPanierTmp->xml_export_panier_doc();
       	$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";

		// VP 6/03/09 : utilisation de getSiteFile
       	//PC 15/10/10 : supression du numéro du xsl
       	$message= TraitementXSLT($xml,getSiteFile("designDir","print/commandeImp.xsl"),$param,1);
        ob_start();
        eval("?>". str_replace(array('||'," ' "),array(chr(10),"'"),$message)."<?");
        $message=ob_get_contents();
		ob_end_clean();

        //NOTE : conversion en ISO-8859-1 pour compatibilité maximum avec les clients mail (notamment EUDORA & HOTMAIL)
        //$message = utf8_decode($message);
		$message=str_replace("€","&euro;",$message);

		//$message = mb_convert_encoding($message,"ISO-8859-1","UTF-8");
		$message=utf8_decode($message);
		//$message=sprintf(gMailBody,$message);

		if(defined('gMailCommandeFrom')){
			$mailExp =gMailCommandeFrom;
		}else if( defined('gMailCommande')){
			$mailExp =gMailCommande;
        }else{
			$mailExp =gMail;
		}
		$headers="From:".$mailExp."\nMIME-Version: 1.0\nContent-Type: multipart/alternative; boundary=\"".$frontiere."\"";

		//update VG 13/04/11
        if(empty($sujet)) {
			switch($this->t_panier['PAN_ID_ETAT']) {
				case gPanierFini :
					if(defined("kMailSujetCommandeDisponible".$this->t_panier['PAN_ID_TYPE_COMMANDE'])) {
						$sujet=sprintf(constant("kMailSujetCommandeDisponible".$this->t_panier['PAN_ID_TYPE_COMMANDE']),$this->t_panier['ID_PANIER']);
					} else {
					    $sujet=constant('kMailSujetCommandeDisponible_'.$mailLanguage);
					}
				break;
				default :
					if(defined("kMailSujetCommande".$this->t_panier['PAN_ID_TYPE_COMMANDE'])) {
						$sujet=sprintf(constant("kMailSujetCommande".$this->t_panier['PAN_ID_TYPE_COMMANDE']),$this->t_panier['ID_PANIER']);
					} else {
					    $sujet=constant('kMailSujetCommandeDisponible_'.$mailLanguage);
					}
			}
        }

		//Pour tests
		$rand = rand();

		include_once(modelDir.'model_message.php');

		//PC 17/12/10 : ajout possibilité bloquer mail utilisateur
        if($envoieUser!=0 && $email!=""){
			$msg = new Message();
			trace ("Mail vers user : ".$email.", url :".$_SERVER['QUERY_STRING'].", ".$rand);
            //$rtn=mail($email,$sujet,$message,$headers); //NOTE : La fonction mail est configur�e dans php.ini ("SMTP", "sendmail_from", ";sendmail_path")
			$rtn=$msg->send_mail($email, $mailExp, $sujet, $message, "multipart/alternative", $frontiere);
			unset($msg);
        }

		// Envoie du meme message a l'administrateur des commandes
		if ($envoieAdmin!=0 && !empty($mailAdmin)){
			
			$param["mail_to_admin"] = 1;
			$message= TraitementXSLT($xml,getSiteFile("designDir","print/commandeImp.xsl"),$param,1);
			ob_start();
			eval("?>". str_replace(array('||'," ' "),array(chr(10),"'"),$message)."<?");
			$message=ob_get_contents();
			ob_end_clean();
			$message=str_replace("€","&euro;",$message);
			$message=utf8_decode($message);
			
			$msg = new Message();
			trace('Mail admin commande : '.$mailAdmin.", url :".$_SERVER['QUERY_STRING'].", ".$rand);
			$msg->send_mail($mailAdmin, $mailExp, $sujet, $message, "multipart/alternative", $frontiere);
			unset($msg);
		}
        return $rtn;
    }


	/** Récupération du panier et des lignes le composant
	 * $arrCriterias : critères supplémentaires (ajoutés au WHERE)
	 * $arrOrders : colonnes à trier et sens de tri
	 * $idLang : langue dans laquelle récupérer les notices
	 * $blnOnlyCmd : si 1, récupère uniquement les lignes en cours de commande
	 *
	 * NOTE : cette fonction éclate le champ PAN_XML en un tableau de paramètres.
	 *
	 * **/
    /**
    function getPanier($arrCriterias="",$arrOrders="",$idLang,$blnOnlyCmd="") {
    	global $db;

		$idLang=strtoupper($idLang);
		// Récupération du panier selon l'ID ou selon l'USAGER si pas d'ID disponible.
    	if (empty($this->t_panier["ID_PANIER"])) {
    	$this->t_panier=getItem("t_panier",null,array("PAN_ID_USAGER"=>$this->t_panier["PAN_ID_USAGER"],"PAN_ID_ETAT"=>1),null);
    	}
    	else {
    	$this->t_panier=getItem("t_panier",null,array("ID_PANIER"=>$this->t_panier["ID_PANIER"]),null);
    	}

    	// Le champ PAN_XML contient des infos au format XML. Nous les transformons en tableau.
    	$this->t_panier['XML']=xml2array($this->t_panier['PAN_XML']);


    	// Récupération du contenu du panier
    	$this->t_panier_doc=array();
            if($idLang!=""){
                $sql ="SELECT t_panier_doc.*, t_doc.DOC_COTE, t_doc.DOC_TITRE, t_doc.DOC_DATE_PROD,
						t_doc.DOC_ID_FONDS, t_fonds.FONDS_COL as FONDS, t_type_doc.TYPE_DOC, t_doc.DOC_DUREE
						FROM t_panier_doc, t_doc
						LEFT JOIN t_type_doc ON t_doc.DOC_ID_TYPE_DOC=t_type_doc.ID_TYPE_DOC AND t_type_doc.ID_LANG=".$db->Quote($idLang)."
               			LEFT JOIN t_fonds ON t_doc.DOC_ID_FONDS=t_fonds.ID_FONDS
               			WHERE t_panier_doc.ID_PANIER=".$db->Quote($this->t_panier["ID_PANIER"])."
                		AND t_doc.id_doc=t_panier_doc.ID_DOC AND t_doc.ID_LANG=".$db->Quote($idLang);

               $sql.=addCriterias($arrCriterias);
               $sql.=addOrders($arrOrders);

               if ($blnOnlyCmd) $sql.= " AND t_panier_doc.PDOC_CMD_TMP=1";

               $rset = $db->Execute($sql);
               // Récupération des valeurs
               while ($row_mm = $rset->FetchRow()) {

					$arrVal=getVal($row_mm['ID_DOC']);


					if (!empty($arrVal['DIF'])) $row_mm['VAL_DIF']="§".implode("§",array_keys($arrVal['DIF']))."§";
               		//$row_mm['DOC_VAL']=$arrVal;
               		$this->t_panier_doc[] = $row_mm;

               		}
            	$rs=$db->Execute("SELECT US_NOM, US_PRENOM from t_usager WHERE ID_USAGER='".$this->t_panier['PAN_ID_USAGER']."'");
            	$row_us = $rs->FetchRow();
            	$this->t_panier['USAGER']=$row_us['US_NOM']." ".$row_us['US_PRENOM'];

            	if (in_array($this->t_panier['PAN_ID_TYPE_COMMANDE'],unserialize(gModesAvecCalculPrix))==true) $this->calc_prices=true;
            }
    }
*/

	function getXMLList($sql, $nb_offset = 0, $nb = "all", $mode = null, $search_sup = array(), $replace_sup = array()) {

		if ($mode == null) {
			$mode = array("val" => 1, "lex" => 1, "pers_lex" => 1, "docmat" => 1, "cat" => 1, "img" => 1, "seq" => 1, "doc_acc" => 1, "doc_lies_src" => 1, "doc_lies_dst" => 1, "redif" => 1, "lex_droit" => 1, "doc_fest" => 1);
		}

		
		if($nb_offset>0){
			return false ; 
		}
		
		// 2016-10-18 B.RAVI  car dans class_export, on envoie sous la forme array(1) [0]=>string(3) "197"}
		if (is_array($this->t_panier['ID_PANIER'])) {
			$this->t_panier['ID_PANIER'] = $this->t_panier['ID_PANIER'][0];
		}

		// 2016-10-18 B.RAVI TODO à vérifier, on a déja le cas panierdoc dans getXMLList() de class_export.php donc le select * from t_doc ci dessous ne sert potentiellement à rien et n'est utilisé dans aucun site
		require_once(modelDir.'model_doc.php');
		$docs = new Doc();
		$sql = "select * from t_doc t1 WHERE t1.ID_DOC in (SELECT ID_DOC from t_panier_doc where ID_PANIER = " . intval($this->t_panier['ID_PANIER']) . ")";

		
		$this->getPanier(array(), false);
		$contentTpanier = $this->xml_export();
		$xml_path = $docs->getXMLList($sql, $nb_offset, $nb, $mode, $search_sup, $replace_sup);

		//si retourne false, c'est qu'il n'y a pas de t_panier_doc affecté à ce panier
		if (!$xml_path) {
			$xml_file_path = kServerTempDir . "/export_opsis_doc_" . microtime(true) . ".xml";
			$xml_file = fopen($xml_file_path, "w");
			flock($xml_file, LOCK_EX);
			fwrite($xml_file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			fwrite($xml_file, "<EXPORT_OPSIS>\n");
			fwrite($xml_file, $contentTpanier);
			fwrite($xml_file, "</EXPORT_OPSIS>\n");
			flock($xml_file, LOCK_UN);
			fclose($xml_file);
			$xml_path = $xml_file_path;
		} else {//on merge doc->getXMLList() && $panier->xml_export()
			$content_xml_path = file_get_contents($xml_path, $content_xml_path);
			//attention la balise remplacée doit être en MAJUSCULE =><EXPORT_OPSIS>
			$content_xml_path = str_ireplace('<export_opsis>', '<EXPORT_OPSIS>' . $contentTpanier, $content_xml_path);
			file_put_contents($xml_path, $content_xml_path);
		}


		return $xml_path;
	//		return $docs->getXMLList($sql,$nb_offset,$nb,$mode,$search_sup,$replace_sup);
	}	


    /** Sauvegarde d'un panier
     *  utilisé uniquement lors de la validation (on crée un nv panier pour la commande)
     * **/
    function insertPanier() {
    	global $db;
		$this->previousId=$this->t_panier['ID_PANIER'];
		$record['PAN_ID_USAGER'] = $this->t_panier['PAN_ID_USAGER'];
		$record['PAN_TITRE'] = $this->t_panier['PAN_TITRE'];
		$record['PAN_OBJET'] = $this->t_panier['PAN_OBJET'];
		$record['PAN_XML'] = $this->t_panier['PAN_XML'];
		$ok = $db->insertBase("t_panier","id_panier",$record);
		return $this->t_panier['ID_PANIER']=$ok;

    }

	//by ld 29/01/09 => récup de la vignette
	function getVignette() {
		global $db;
		if (!empty($this->t_panier['PAN_ID_DOC_ACC'])) {
			$vignetteChemin=$db->GetOne('SELECT '.$db->Concat('DA_CHEMIN',"DA_FICHIER").' FROM t_doc_acc WHERE ID_DOC_ACC='.intval($this->t_panier['PAN_ID_DOC_ACC']));
			$relativePathFromMedia=str_replace(kCheminHttpMedia,'',kDocumentUrl);
			if ($vignetteChemin) $this->vignette= (strpos($vignetteChemin,'http://')===0?$vignetteChemin:$relativePathFromMedia.$vignetteChemin);
		} else $this->vignette='';
	}


    /** Export XML de l'objet panier
     * $entete, si 1, l'entete XML est ajouté
     * $encodage, si spécifié, export converti dans l'encodage choisi (sinon UTF-8)
     * **/
    function xml_export($entete=0,$encodage=0,$prefix=""){
    	global $refTables,$db;

		$content='';

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.=$prefix."<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
	
        $content.=$prefix."<t_panier>\n";
                foreach ($this->t_panier as $name => $value) {
					if($name=="XML") {
						$content .= $prefix."\t<".strtoupper($name).">".array2xml($value,"")."</".strtoupper($name).">\n";
					}else if($name=='PAN_XML'){
						$content .= $prefix."\t<".strtoupper($name).">".str_replace(" ' ","'",$value)."</".strtoupper($name).">\n";
					}else {
						$content .=$prefix. "\t<".strtoupper($name).">".str_replace($search,$replace, $value)."</".strtoupper($name).">\n";
					}
                    //$content .=$prefix. "\t<".strtoupper($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".strtoupper($name).">\n";
                    }
            $content.=$prefix."\t<TYPE_COMMANDE>".GetRefValue("t_type_commande",$this->t_panier['PAN_ID_TYPE_COMMANDE'],$_SESSION['langue'])."</TYPE_COMMANDE>\n";
            $content.=$prefix."\t<ETAT_PANIER>".$this->etat_panier."</ETAT_PANIER>\n";
			if(isset($this->estimate_size_visio)){
				$content.=$prefix."\t<ESTIMATE_SIZE_VISIO>".$this->estimate_size_visio."</ESTIMATE_SIZE_VISIO>\n";
			}
			if(isset($this->estimate_size_master)){
				$content.=$prefix."\t<ESTIMATE_SIZE_MASTER>".$this->estimate_size_master."</ESTIMATE_SIZE_MASTER>\n";
            }
			$content.=$prefix."\t<DUREE_PANIER>".$this->duree_panier."</DUREE_PANIER>\n";

            $content.=$prefix."\t<PRIX_TOTAL_CDE>".(isset($this->t_panier['TOTAL']['PRIX'])?$this->t_panier['TOTAL']['PRIX']:'')."</PRIX_TOTAL_CDE>\n";
            $content.=$prefix."\t<FRAIS_HT>".$this->t_panier['PAN_FRAIS_HT']."</FRAIS_HT>\n";
            $content.=$prefix."\t<TOTAL_HT>".$this->t_panier['PAN_TOTAL_HT']."</TOTAL_HT>\n";
            $content.=$prefix."\t<TOTAL_FRAIS_HT>".(isset($this->t_panier['TOTAL']['TOTAL_HT'])?$this->t_panier['TOTAL']['TOTAL_HT']:'')."</TOTAL_FRAIS_HT>\n";
            $content.=$prefix."\t<TOTAL_TTC>".(isset($this->t_panier['TOTAL']['TOTAL_TTC'])?$this->t_panier['TOTAL']['TOTAL_TTC']:'')."</TOTAL_TTC>\n";
            $content.=$prefix."\t<TVA>".kTVA."</TVA>\n";
            $content.=$prefix."\t<TOTAL_OUT_OF_RANGE>".$this->blnOutOfRange."</TOTAL_OUT_OF_RANGE>\n";

			$content.=$prefix."\t<VIGNETTE>".$this->vignette."</VIGNETTE>\n";

			if (!empty($this->t_panier['PAN_ID_GEN'])) {
				$titre_parent=$db->getOne('SELECT PAN_TITRE from t_panier where ID_PANIER='.intval($this->t_panier['PAN_ID_GEN']));
				$content.=$prefix."\t<TITRE_PARENT>".$titre_parent."</TITRE_PARENT>\n";
			}

            require_once(modelDir.'model_usager.php');
            $us=new Usager();
            $us->t_usager['ID_USAGER']=$this->t_panier['PAN_ID_USAGER'];
            $us->getUsager();
            $content.=$prefix."\t".$us->xml_export(0,0);
            unset($us);


        $content.=$prefix."</t_panier>\n";

        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }


    function xml_export_panier_doc ($entete=0,$encodage=0,$prefix=""){
    	global $refTables;

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.=$prefix."<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		foreach ($this->t_panier_doc as $idx=>$tab_value){

			$content.=$prefix."\t<t_panier_doc ID_LIGNE_PANIER=\"".$tab_value["ID_LIGNE_PANIER"]."\">\n";
			foreach ($tab_value as $name => $value) {
				// VP 18/11/10 : supression passage balise en minuscule
				//if (!is_array($value) && !is_object($value) ) $content .=$prefix. "\t\t<".strtolower($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".strtolower($name).">\n";
				if($name=='LIGNE_JOB_PARAM') {
					$content .=$prefix. "\t\t<".$name.">".ereg_replace("\n|\r","|",$value)."</".$name.">\n";
				} elseif (!is_array($value) && !is_object($value) ) $content .=$prefix. "\t\t<".$name.">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".$name.">\n";
			}
			$content.=array2xml($tab_value["SUPPORTS"],"supports")."\n";

	 		if (!is_object($tab_value['DOC'])) {
	 					require_once(modelDir.'model_doc.php');
						$myDoc=new Doc();
						$myDoc->t_doc['ID_DOC']=$tab_value["ID_DOC"];
						$myDoc->t_doc['ID_LANG']=$_SESSION['langue'];
						$myDoc->getDoc();

						$content.="<DOC>".$prefix.$myDoc->xml_export(0,0,"\t\t\t")."</DOC>";
						unset($myDoc);
			} else  $content.="<DOC>".$prefix.$tab_value['DOC']->xml_export(0,0,"\t\t\t")."</DOC>";
			
			$content.=$prefix."\t</t_panier_doc>\n";
		}

		 if($encodage!=0){
			return mb_convert_encoding($content,$encodage,"UTF-8");
		}
		else{
			return $content;

		}

    }



 	/** Récupération du titre à afficher en haut du panier, selon l'état du panier et le type de commande **/
	function getTitre() {
	    // Titre : panier ou commande
		if($this->t_panier['PAN_ID_ETAT']==1) $this->titre = kPanier;
		else $this->titre = kSelection;
	}


	/**
	 * Pour chaque document dans le panier, vérification de la dispo du produit par support.
	 */
	function chkDispoDocPanier() {
		global $db;

		$params=unserialize(gsqlCheckDispoDocPanierParams); // récupéré d'une constante
		
		if (!empty($params)) foreach ($params as $param) $values[]=$this->t_panier[$param];
		
		if (empty($params) || !defined('gsqlCheckDispoDocPanier') || gsqlCheckDispoDocPanier=="") { // Aucun SQL de vérification, donc tous les articles sont dispo.
			foreach ($this->t_panier_doc as $idx=>$doc) $this->t_panier_doc[$idx]['DISPO']='1';
		} else {
			$sql=vsprintf(gsqlCheckDispoDocPanier,$values); //récupéré d'une constante + injection des params.
			$rset=$db->Execute($sql);
			while ($rset && $row=$rset->FetchRow()) $allowedDocs[]=$row['ID_DOC'];

			foreach ($this->t_panier_doc as $idx=>$doc) {
				if (is_array($allowedDocs) && in_array($this->t_panier_doc[$idx]['ID_DOC'],$allowedDocs)) {
					$this->t_panier_doc[$idx]['DISPO']='1'; }
				else {
					$this->t_panier_doc[$idx]['DISPO']='0'; }
			}
		}
	}

		/**
		 * Pour chaque document, récupération de la liste des supports existants
		 */
	function getSupports(){
		global $db;
		if(defined("gsqlGetSupportByDoc") && gsqlGetSupportByDoc!=''){
			if ($this->t_panier['PAN_ID_TYPE_COMMANDE']==null) $type=''; else $type='AND ID_TYPE_COMMANDE='.intval($this->t_panier['PAN_ID_TYPE_COMMANDE']).'';
			foreach ($this->t_panier_doc as $idx=>$doc) {

				$sql=sprintf(gsqlGetSupportByDoc,$this->t_panier_doc[$idx]['ID_DOC'],$type); //SQL récupéré d'une constante (initialisationGeneral)

				$rset=$db->Execute($sql);
				unset($this->t_panier_doc[$idx]['SUPPORTS']);

				while ($row=$rset->FetchRow()) {
					$this->t_panier_doc[$idx]['SUPPORTS'][]=array("VALEUR"=>$row['VALEUR']);
					}
			}
		}
	}
		/**
		 * Génère le SQL complémentaire correspondant aux critères additionnels
		 * Cette partie se base sur 2 informations : le tableau des critères additionnels
		 * (cf initialisationGeneral) qui dit où trouver l'information nécessaire dans l'objet.
		 * (rappel : l'objet est généré selon la structure du formulaire).
		 * Tous les critères additionnels sont stockés dans le champ AUTRE_CRIT de la table t_tarifs
		 * sous forme XML.
		 */
	function filterByAdditionnalCriterias () {
		// NOTE : pour l'instant, ne fonctionne pas avec l'objet panier_doc.
		foreach ($this->arrTarifAddCrit as $crit) { // Moteur de mise en forme des critères additionnels
				$myobj=$this->$crit['object'];
				if (!empty($crit['where'])) { //si j'ai un chemin...
					$nodes=array_values(split("/",$crit['where'])); // décorticage du chemin où se trouve l'info dans l'objet panier
					$i=0;
					$val=$myobj[$nodes[$i]];
					while (is_array($val)) {
						$i++;
						 $val=$val[$nodes[$i]];	//tant qu'on n'a pas trouvé la valeur dans l'objet panier.
					}
				} else $val=$myobj[$crit['what']];	//pas de chemin (racine de l'objet).

				// Si aucune valeur trouvée dans l'objet, on n'ajoute pas de critère SQL.
				// Par exemple, si le champ n'existe pas dans le formulaire.
				if ($val!="") $sqlAddCrit.=" AND AUTRE_CRIT LIKE '%<".$crit['what'].">".$val."</".$crit['what'].">%' ";
			}
			return 	$sqlAddCrit;
	}

	/**
	 * Calcul des tarifs pour chaque programme.
	 * On vérifie d'abord la disponibilité de chaque programme, puis la liste des supports distribués,
	 * puis la durée du document ou de l'extrait (si extrait).
	 * On récupère ensuite les tarifs applicables (pour ce support, cette durée, ce nb de support et autres critères)
	 * Si un tarif spécifique est trouvé pour le document, on lui applique sinon on utilise un tarif "générique".
	 * Le prix est ensuite calculé, ainsi que le total des prix et durée du panier.
	 *
	 */
	function getTarifs() {
		global $db;

		//Avant tout, on vérifie que les documents sont bien dispo à la vente / prêt,...
		$this->chkDispoDocPanier();
		$this->getSupports();

		// Calcul du taux de TVA
		$this->t_panier['PAN_TVA']=kTVA;
		if(($this->t_panier['XML']['commande']['PANXML_DESTINATION']=='INTERNATIONAL')||($this->t_panier['XML']['commande']['PANXML_DESTINATION']=='EUROPE' && $this->t_panier['XML']['commande']['PANXML_TVA_INTRA']!='')) $this->t_panier['PAN_TVA']=0.0;
		foreach ($this->t_panier_doc as $idx=>$doc) {

			$this->t_panier_doc[$idx]['PRIX_UNIT']=null; //init prix.
			//$this->t_panier_doc[$idx]['PRIX_CALC']=null;
			$this->t_panier_doc[$idx]['PDOC_PRIX_CALC']=null;

			if ($this->t_panier_doc[$idx]['DISPO']=='1') { // on ne calcul que si le doc est dispo en pret ou vente, etc..



					//Calcul de la durée : récupérée depuis l'extrait, sinon la durée du doc.
					$secondes=tcToSec($doc['PDOC_EXT_TCOUT'])-tcToSec($doc['PDOC_EXT_TCIN']);
					$duree=secToTime($secondes);
					if ($duree=='00:00:00') {
							$duree=$doc['DOC']->t_doc['DOC_DUREE'];
							$secondes=timeToSec($duree);
							}
					if (empty($duree)) $duree='00:00:00'; // Si pas de durée dispo, tarif min (à confirmer)

                    //VP 4/07/2014 : conversion durée le cas échéant
                    $duree=secToTime(timeToSec($duree));

					$total['DUREE']+=$secondes;
					$this->t_panier_doc[$idx]['DUREE']=$duree;


					$foundPrice=false; //flag qui indique si un prix spécifique au programme a été découvert

					$sql="SELECT MIN(PRIX_UNIT) as PRIX_UNIT,MIN(PRIX_MINUTE) as  PRIX_MINUTE,ID_DOC,AUTRE_CRIT FROM t_tarifs
							WHERE DUREE_MAX>='".$duree."'
							AND NB_SUP_MAX >= ".intval($this->t_panier_doc[$idx]['PDOC_NB_SUP'])."
							AND SUPPORT_LIV='".$this->t_panier_doc[$idx]['PDOC_SUPPORT_LIV']."'
							AND ID_TYPE_COMMANDE=".intval($this->t_panier['PAN_ID_TYPE_COMMANDE'])." ";

					//On ajoute la vérification sur les critères additionnels
					if (!empty($this->arrTarifAddCrit)) $sql=$sql.$this->filterByAdditionnalCriterias();

					//pour placer les lignes avec ID_DOC en premier...
					$sql.=" GROUP BY ID_DOC,AUTRE_CRIT ORDER BY ID_DOC DESC,PRIX_UNIT DESC";
					//echo $sql;
					$rset=$db->Execute($sql);

					while ($row=$rset->FetchRow()) {

							if ($doc['ID_DOC']==$row['ID_DOC'] && $foundPrice==false) {	// Il y a un tarif spécifique pour ce document
											$foundPrice=true; //trouvé prix spécifique
											$prix_unit=(real)$row['PRIX_UNIT'];
											$prix_minute=(real)$row['PRIX_MINUTE'];
											$minutes=floor($secondes/60);

											$prix_calc=round($prix_unit+$prix_minute*$minutes,2)*(int)$this->t_panier_doc[$idx]['PDOC_NB_SUP'];
											$this->t_panier_doc[$idx]['PRIX_UNIT']=round($prix_unit,2);
											//$this->t_panier_doc[$idx]['PRIX_CALC']=$prix_calc;
											$this->t_panier_doc[$idx]['PDOC_PRIX_CALC']=sprintf('%01.2f',$prix_calc);
											$total['PRIX']+=$prix_calc;
											$total['QTE']+=$this->t_panier_doc[$idx]['PDOC_NB_SUP'];

							//J'ai un prix générique (sans indication d'ID_DOC) et toujours pas de prix défini : on prend celui-là
							} elseif (empty($row['ID_DOC']) && $foundPrice==false) {
											$foundPrice=true;
											$prix_unit=(real)$row['PRIX_UNIT'];
											$prix_minute=(real)$row['PRIX_MINUTE'];
											$minutes=floor($secondes/60);

											$prix_calc=round($prix_unit+$prix_minute*$minutes,2)*(int)$this->t_panier_doc[$idx]['PDOC_NB_SUP'];
											$this->t_panier_doc[$idx]['PRIX_UNIT']=round($prix_unit,2);
											//$this->t_panier_doc[$idx]['PRIX_CALC']=$prix_calc;
											$this->t_panier_doc[$idx]['PDOC_PRIX_CALC']=sprintf('%01.2f',$prix_calc);
											$total['PRIX']+=$prix_calc;
											$total['QTE']+=$this->t_panier_doc[$idx]['PDOC_NB_SUP'];
							}
					} // Fin parcours du tableau des tarifs
					$rset->Close();
			} // Si doc dispo
		} // Fin parcours du tableau des documents

		$total['DUREE']=secToTime($total['DUREE']);

		$this->t_panier['TOTAL']=$total;

		$this->getFrais();

		$this->t_panier['PAN_TOTAL_HT']=sprintf('%01.2f',$this->t_panier['TOTAL']['PRIX']);
		$this->t_panier['PAN_FRAIS_HT']=sprintf('%01.2f',$this->t_panier['TOTAL']['FRAIS']);
		$this->t_panier['TOTAL']['TOTAL_HT']=sprintf('%01.2f',round($this->t_panier['TOTAL']['PRIX']+$this->t_panier['TOTAL']['FRAIS'],2));
		$this->t_panier['TOTAL']['TOTAL_TTC']=sprintf('%01.2f',round($this->t_panier['TOTAL']['TOTAL_HT']*(($this->t_panier['PAN_TVA']+100)/100),2));

		$montantmin=(defined('gCommandeMontantMin') && is_numeric(gCommandeMontantMin)?gCommandeMontantMin:0);
		$montantmax=(defined('gCommandeMontantMax') && is_numeric(gCommandeMontantMax)?gCommandeMontantMax:100000);

		if ($this->t_panier['TOTAL']['TOTAL_TTC']<$montantmin || $this->t_panier['TOTAL']['TOTAL_TTC']>$montantmax) $this->blnOutOfRange='1'; else $this->blnOutOfRange='0';

	}


	/**
	 * Fonction destinée à calculer les frais autour d'une commande.
	 * Notamment, les frais d'expédition.
	 */
	function getFrais() {
		global $db;
		if(!empty($this->t_panier['TOTAL']['QTE'])){
			// VP 20/10/2011 : ajout critère FRS_DUREE_MAX
			$sql="
			SELECT MIN(FRS_PRIX_UNIT) as PRIX_UNIT,MIN(FRS_PRIX_LIN) as PRIX_LIN
			FROM t_frais
			WHERE
			FRS_QTE_MAX >= ".floatval($this->t_panier['TOTAL']['QTE'])."
			AND FRS_DUREE_MAX >= '".$this->duree_panier."'
			AND FRS_TOTAL_PANIER_MAX >= ".floatval($this->t_panier['TOTAL']['PRIX'])."
			AND lower(FRS_TRANSPORTEUR)=".$db->Quote(strtolower($this->t_panier['XML']['commande']['PANXML_TRANSPORTEUR']))."
			AND lower(FRS_DESTINATION)=".$db->Quote(strtolower($this->t_panier['XML']['commande']['PANXML_DESTINATION']))."
			";

			//echo $sql;

			$rset=$db->Execute($sql);
			$row=$rset->FetchRow();

			$prix_unit=(real)$row['PRIX_UNIT'];
			$prix_lin=(real)$row['PRIX_LIN'];
			$this->t_panier['TOTAL']['FRAIS']=round($prix_unit+$prix_lin*$this->t_panier['TOTAL']['QTE'],2);

		}
	}

	/**
	* Met à jour PAN_ID_ETAT en fonction du min des PDOC_ID_ETAT
	 * OUT : nouveau PAN_ID_ETAT
	 */
	function updatePanierEtat(){
	 	global $db;
		$etat_pan=$tab=$db->GetOne("select min(PDOC_ID_ETAT) from t_panier_doc where ID_PANIER=".intval($this->t_panier["ID_PANIER"]));
		//$db->Execute("update t_panier set PAN_ID_ETAT='".$etat_pan."' where ID_PANIER='".$this->t_panier["ID_PANIER"]."'");
		$rs = $db->Execute("SELECT * FROM t_panier WHERE ID_PANIER=".intval($this->t_panier["ID_PANIER"]));
		$sql = $db->GetUpdateSQL($rs, array("PAN_ID_ETAT" => intval($etat_pan)));
		if (!empty($sql)) $ok = $db->Execute($sql);

		// POUR TRACE : A EFFACER
		if($this->t_panier['PAN_ID_ETAT'] > 1) {
			trace("sauvegarde commande ".$this->t_panier['ID_PANIER'].", etat panier : ".$this->t_panier['PAN_ID_ETAT']);
		}
		//
		return $etat_pan;
	}
	/**
	 * Récupère la liste des folders pour un utilisateur avec ID, TITRE, ETAT et nb de documents/extraits dans ce folder
	 * IN : $_SESSION['USER']['ID_USAGER'] (id user) et addNew (bln) => ajout d'une ligne vide au panier
	 * OUT : tableau des paniers
	 * NOTE : l'ajout d'une ligne vide permet de constituer une liste des paniers où figure une ligne 'Nouveau Panier'
	 * permettant la création à la volée d'un nouveau panier (cf docListe, panier, etc.)
	 */
	static function getFolders($addNew=false) {

		global $db;
			//bu ld 20/11/08 => aj PAN_ID_GEN clause order by pour le bon comptage des sous-entités en aff. hiérarchique
			//ld 29/01/09 => récup des dossiers publics
			$sql="  SELECT p.ID_PANIER, PAN_TITRE, PAN_ID_ETAT, PAN_ID_GEN, PAN_DOSSIER, PAN_PUBLIC,PAN_ORDRE,
						COUNT(pd.ID_DOC) as NB
						from t_panier p left outer join t_panier_doc pd ON p.ID_PANIER=pd.ID_PANIER AND pd.ID_DOC != 0
					WHERE
						(p.PAN_ID_USAGER=".intval($_SESSION['USER']['ID_USAGER'])." OR p.PAN_PUBLIC=1)
						AND p.PAN_ID_ETAT IN (0,1) and p.PAN_ID_TYPE_COMMANDE=0
					GROUP BY p.ID_PANIER, p.PAN_TITRE, p.PAN_ID_ETAT, p.PAN_ID_GEN, p.PAN_DOSSIER, p.PAN_PUBLIC, p.PAN_DATE_CREA ORDER BY PAN_ORDRE,PAN_ID_GEN,PAN_ID_ETAT DESC,PAN_DATE_CREA ASC";

			$rset=$db->GetAll($sql);
			if ($addNew) $rset[]=array("ID_PANIER"=>-1,"PAN_TITRE"=>kPanierNouveau,"PAN_ID_ETAT"=>0, "NB"=>0,"PAN_PUBLIC"=>0);
			return $rset;

	}

	/**
	 * Idem fonction getFolders, mais permet de récupérer une liste de panier à partir de leur PAN_ID_GROUPE
	 * => Permet de mettre en place la notion de panier partagés dans un groupe d'utilisateur
	 */
	static function getGroupeFolders($addNew=false,$excludeMine=false) {
		global $db;

			if(isset($_SESSION['USER']['US_GROUPES'])){
				$ids_groupes = array();
				if (!empty($_SESSION['USER']['US_GROUPES']['GROUPES'])){
					foreach($_SESSION['USER']['US_GROUPES']['GROUPES'] as $groupe){
						$ids_groupes[] = $groupe['ID_GROUPE'];
					}
				}
			}
			if(is_array($ids_groupes) && !empty($ids_groupes)){
				//bu ld 20/11/08 => aj PAN_ID_GEN clause order by pour le bon comptage des sous-entités en aff. hiérarchique
				//ld 29/01/09 => récup des dossiers publics
				$sql="  SELECT p.ID_PANIER, PAN_TITRE, PAN_ID_ETAT, PAN_ID_GEN, PAN_DOSSIER, PAN_PUBLIC, PAN_ID_GROUPE, t_groupe.GROUPE,
							COUNT(pd.ID_DOC) as NB
							from t_panier p left outer join t_panier_doc pd ON p.ID_PANIER=pd.ID_PANIER
							left join t_groupe on p.PAN_ID_GROUPE=t_groupe.ID_GROUPE
						WHERE
							p.PAN_ID_GROUPE in (".implode(',',$ids_groupes).")";
							if($excludeMine == true){
								$sql.="AND p.PAN_ID_USAGER !=".$_SESSION['USER']['ID_USAGER']; 
							}
							$sql.="
							AND p.PAN_ID_ETAT IN (0,1) and p.PAN_ID_TYPE_COMMANDE=0
						GROUP BY p.ID_PANIER, p.PAN_TITRE, p.PAN_ID_ETAT, p.PAN_ID_GEN, p.PAN_DOSSIER, p.PAN_PUBLIC, PAN_ID_GROUPE,t_groupe.GROUPE, p.PAN_DATE_CREA ORDER BY t_groupe.GROUPE, PAN_ID_GEN,PAN_ID_ETAT DESC,PAN_DATE_CREA ASC";
				$rset=$db->GetAll($sql);
				if ($addNew) $rset[]=array("ID_PANIER"=>-1,"PAN_TITRE"=>kPanierNouveau,"PAN_ID_ETAT"=>0, "NB"=>0,"PAN_PUBLIC"=>0,"PAN_ID_GROUPE"=>0);
				return $rset;
			}else{
				return array();
			}
	}

	/**
	 * Parcourt le tableau de session FOLDERS et ramène celui correspondant au panier
	 * IN : $_SESSION['FOLDERS']
	 * OUT : id_panier
	 */
	static function getDefaultPanierId() {
		// VP 6/01/10 : utilisation de getFolders() plutôt que $_SESSION['FOLDERS']
		$folders=Panier::getFolders();
		$logged=User::getInstance()->loggedIn();
		foreach ($folders as $folder) {
			$idx=$folder['ID_PANIER'];
			// LD 02 04 09 : test sur _session (sécurité si _session reste en session après login)
			// Si un autre panier existe que _session, on ramène celui-là
			if ($folder['PAN_ID_ETAT']==1 && (
					($idx!='_session' && $logged) || !$logged)
				) {return $idx;}
		}
		return false;
	}

/**
 *  Renvoie un tableau des ID_DOC compris dans un panier, en tenant compte uniquement des programmes
 *  complets (pas d'extraits).
 *  IN : id_panier
 * 	OUT : tab d'ID_DOC
 *  NOTE : cette fonction est utilisée pour pré-cocher les doc déjà présents dans le panier lors de l'affichage
 * 	d'un résultat de recherche.
 */
	static function getPanierIdDocOnly ($id_panier) {
		global $db;
		if (empty($id_panier)) return false;
		$sql="select ID_DOC from t_panier_doc where (PDOC_EXTRAIT is null or PDOC_EXTRAIT=0)
				and  ID_PANIER=".intval($id_panier);
		$docs=$db->Execute($sql);
		foreach ($docs as $_doc) {
			$myDocs[]=$_doc['ID_DOC'];
		}
		return $myDocs;
	}

	static function getLignePanier($id_ligne_panier) {
		global $db;
		$sql="SELECT * from t_panier_doc WHERE ID_LIGNE_PANIER=".intval($id_ligne_panier);
		$rs=$db->GetRow($sql);
		return $rs;
	}


	// 	MS 20/11/13
	// 	Fonction qui permet d'alimenter la nouvelle variable owner,
	//	cette variable contient l'objet usager instancié avec les infos du propriétaire du panier (pas forcément identique à User)
	// 	utile pour afficher des infos de l'utilisateur propriétaire du panier (ex : cas commandeSaisie)
	function getProprioPanier(){
		require_once(modelDir.'model_usager.php');
		$this->owner = new Usager();
		if(isset($this->t_panier['PAN_ID_USAGER'] ) && !empty($this->t_panier['PAN_ID_USAGER'])){
			$this->owner->t_usager['ID_USAGER'] = $this->t_panier['PAN_ID_USAGER'];
			$this->owner->getUsager();
			return true;
		}else{
			return false;
		}
	}



	static public function listPanContainingDocs($ids_docs,$getOnlyIds = false){
		global $db;
		if(!is_array($ids_docs)){
			$ids_docs = array(intval($ids_docs));
		}

		if(empty($ids_docs)){
			return false ;
		}
		$sql_find_paniers= "SELECT DISTINCT ID_PANIER from t_panier_doc where id_doc in (".implode(',',$ids_docs).");";
		$results = $db->GetCol($sql_find_paniers);
		if($getOnlyIds){
			return $results;
		}else{
			$results_pan = array();
			foreach($results as $idx=>$id_pan){
				$pan = new Panier();
				$pan = $pan->getPanier($id_pan);
				$results_pan[] = $pan;
			}
			return $results_pan;
		}
	}

	function addPanierDocFromSql($sql=null,$page=1,$nb='all',$default_pdoc_prms=array()){
		global $db ; 
		
		// par défaut la principale utilisation va être d'ajouter des docs depuis la recherche doc en cours 
		if($sql == null){
			if(defined('useSolr') && useSolr){
				require_once(libDir.'class_chercheSolr.php');
				$sql = $_SESSION['recherche_DOC_Solr']['sql'];
			}else{
				require_once(libDir.'class_chercheDoc.php');
				$sql = $_SESSION['recherche_DOC']['sql'];
			}
		}
		if(!is_array($default_pdoc_prms)){
			$default_pdoc_prms = array() ; 
			//trace("Panier:addPanierDocFromSql - wrong type for default_pdoc_prms");
		}
		
		
		if(!is_string($sql) && get_class($sql)=="SolrQuery" && defined("useSolr") && useSolr){

			$tab_options = unserialize(kSolrOptions);
			// MS - 13/05/14 instauration d'un timeout solr minimum
			if(!isset($tab_options['timeout']) || intval($tab_options['timeout'])<300){
				$tab_options['timeout'] = "300";
			}


			if (!empty($_SESSION['DB']))
			{
				$tab_options['path']=$_SESSION['DB'];
			}

			$solr_client=new SolrClient($tab_options);

			if (isset($nb) && !empty($nb) && $nb!="all")
			{
				$nb_rows=intval($nb);
			}
			else
				$nb_rows=50000;
			if($nb  != 'all'){
				$nb_offset = ($page - 1) * $nb;
				$sql->setStart($nb_offset);
			}
			
			
			if (isset($nb_rows))
				$sql->setRows($nb_rows);

			// récupération du tri actif sur solr pr le transposer en pgsql
			$sort = $sql->getSortFields() ;
			if(!empty($sort)){
				$order = "";
				foreach($sort as $field){
					// on ne peut prendre en compte que des éléments triable directement sur la table t_doc
					if(strpos($field,'id_doc')===0 || strpos($field,'doc')===0){
						if($order == ''){
							$order.='order by ';
						}else{
							$order.=',';
						}
						$order .= $field;
					}
				}
			}

			$result_solr=$solr_client->query($sql);

			$critere ="";
			$limit ="";
			$result=array();
			if(empty($result_solr->getResponse()->response->docs)){
				// si aucun résultat on retourne false 
				return false ;
			}
			foreach ($result_solr->getResponse()->response->docs as $doc)
			{
				// $result[] = array('ID_DOC'=>$doc->id_doc,'ID_LANG'=>$doc->id_lang);
				// $result[]['ID_DOC'] = $doc->id_doc;
				$result[] = array_merge($default_pdoc_prms,array('ID_DOC'=>$doc->id_doc));

			}
			
		}else{

			$sql.=$db->limit($nb_offset,$nb);
			
			$results_doc = $db->Execute($sql);

			$result =  array();
			while($row = $results_doc->FetchRow()){
				$result[] = array_merge($default_pdoc_prms,array('ID_DOC'=>$row['ID_DOC']));
			}
			
		}
		if(!empty($result)){
			$this->t_panier_doc = $result ; 
		}
		
		
	}
	
	
	function getSizeEstimate($mat_type = 'MASTER'){
		global $db;
		if (empty($this->t_panier_doc)) $this->getPanierDoc();
		
		$size = 0 ; 
		$arr_size = array( ) ;
		
		foreach($this->t_panier_doc as $pdoc){
			require_once(modelDir.'model_doc.php');
			require_once(libDir."fonctionsGeneral.php");
			$doc = new Doc () ; 
			$doc->t_doc['ID_DOC'] = $pdoc['ID_DOC'];
			$doc->t_doc['ID_LANG'] = $_SESSION['langue'];
			$doc->getDoc() ;
			$doc->getMats() ;
			foreach($doc->t_doc_mat as $dmat){
				if($dmat['MAT']->t_mat['MAT_TYPE'] == $mat_type){
					// On trouve le type de matériel qu'on souhaite estimer. Test à étoffer si necessaire.
					if($doc->t_doc['DOC_TCIN'] != $doc->t_doc['DOC_TCOUT'] 		// on est pas dans le cas où le temps n'a pas été initialisé
					&& ($doc->t_doc['DOC_TCIN'] != $dmat['MAT']->t_mat['MAT_TCIN'] || $doc->t_doc['DOC_TCOUT'] != $dmat['MAT']->t_mat['MAT_TCOUT']) // au moins une différence dans les timecodes doc et mat 
					&& $doc->t_doc['DOC_ID_GEN'] != 0 ){ // un doc parent existe 
						// cas séquences : 
						$tcin_ori = $dmat['MAT']->t_mat['MAT_TCIN'];
						$tcout_ori = $dmat['MAT']->t_mat['MAT_TCOUT'];
						$size_ori = $dmat['MAT']->t_mat['MAT_TAILLE'];
						
						$tcin_seq = $doc->t_doc['DOC_TCIN'];
						$tcout_seq = $doc->t_doc['DOC_TCOUT'];
						
						$duration_ori = tcToSec($tcout_ori) - tcToSec($tcin_ori) ;
						$duration_seq = tcToSec($tcout_seq) - tcToSec($tcin_seq) ;
						
						
						$est_size = round($duration_seq * $size_ori / $duration_ori) ; 
						$size+= $est_size;;
						$arr_size[$pdoc['ID_LIGNE_PANIER']] = $est_size;
					
					}elseif($pdoc['PDOC_EXTRAIT'] == 1){
						// cas extraits : 
						$tcin_ori = $dmat['MAT']->t_mat['MAT_TCIN'];
						$tcout_ori = $dmat['MAT']->t_mat['MAT_TCOUT'];
						$size_ori = $dmat['MAT']->t_mat['MAT_TAILLE'];
						
						$tcin_ext =  $pdoc['PDOC_EXT_TCIN'];
						$tcout_ext = $pdoc['PDOC_EXT_TCOUT'];
						
						$duration_ori = tcToSec($tcout_ori) - tcToSec($tcin_ori) ;
						$duration_ext = tcToSec($tcout_ext) - tcToSec($tcin_ext) ;
						
						$est_size = round($duration_ext * $size_ori / $duration_ori) ; 
						$size+= $est_size;
						$arr_size[$pdoc['ID_LIGNE_PANIER']] = $est_size;
						
					}else{
						// cas default 
						$size+= $dmat['MAT']->t_mat['MAT_TAILLE'];
						$arr_size[$pdoc['ID_LIGNE_PANIER']] = $dmat['MAT']->t_mat['MAT_TAILLE'];
					}
				}
			}
		}
		return $size ; 	
	}

	function createSigniantPackage($updateLog=false){
		global $db;
		
		require_once(libDir."class_signiant.php");
		
		// Init panier
		if (empty($this->t_panier['PAN_XML']))
			$this->getPanier();
		if (empty($this->t_panier_doc))
			$this->getPanierDoc();
		
		// Recherche des fichiers du panier présents sur S3
		$file_keys = array();
		$doc_pub = array();
		foreach($this->t_panier_doc as $pdoc){
			$sql="SELECT distinct dp.*, j.JOB_ID_ETAT as ETAT_PUB FROM t_doc_publication dp LEFT JOIN t_job j ON dp.PUB_ID_JOB = j.ID_JOB WHERE ID_LIGNE_PANIER =".intval($pdoc['ID_LIGNE_PANIER'])." AND PUB_CIBLE='aws'";
			$pub=$db->GetRow($sql);
			if($pub){
				$file_keys[]=$pub['PUB_VALUE'];
				$doc_pub[]=$pub;
			}
		}
		
		if(count($file_keys)==0){
			$this->dropError("Nothing on S3");
			return false;
		}
		
		// Init Signiant
		$sig = new Signiant();
		
		// List portals
		$sig->getPortals();
		$return = $sig->portals->items;
		if(!$return){
			trace("Erreur createSigniantLink getPortals : ".$sig->error_msg);
			return false;
		}
		// On prend le premir portail trouvé
		$portal = $sig->portals->items[0];
		
		// Create a new package
		$package = $sig->createPackage($portal->id);
		if(!$package){
			trace("Erreur createSigniantLink createPackage : ".$sig->error_msg);
			return false;
		}
		
		// Add files
		$return = $sig->addFiles($portal->id, $package->id, $file_keys);
		if(!$return){
			trace("Erreur createSigniantLink addFile : ".$sig->error_msg);
			trace(print_r($file_keys,1));
			return false;
		}
		
		// Update log
		if($updateLog){
			foreach($file_keys as $i=>$key){
				logAction('LIV',array("ID_DOC"=>intval($doc_pub[$i]["ID_DOC"]),"ACT_REQ"=>$key,"ACT_SQL"=>intval($doc_pub[$i]["ID_LIGNE_PANIER"])));
			}
		}
		
		// Create transfer link
		// Mail utilsateur
		if(!empty($this->t_panier['PAN_ID_USAGER'])){
			if(empty($this->owner)){
				$this->getProprioPanier();
			}
			$email = $this->owner->t_usager['US_SOC_MAIL'];
		}
		if(empty($email)) $email=$this->User->Mail;
		$grants="download";
		$expiresOn="";
		//$expiresOn="2018-05-30T10:31:58.547Z";
		$packageToken = $sig->createTransferLink($portal->id, $package->id, $email, $grants, $expiresOn);
		if(!$packageToken){
			trace("Erreur createSigniantLink createTransferLink : ".$sig->error_msg);
			return false;
		}
		// Remplissage PAN_XML
		$tabxml = xml2array($this->t_panier['PAN_XML']);
		
		$tabxml['commande']["PANXML_SIGNIANT_ID"] = $packageToken->id;
		$tabxml['commande']["PANXML_SIGNIANT_URL"] = $packageToken->url;
		$tabxml['commande']["PANXML_SIGNIANT_EXPIRE"] = $packageToken->expiresOn;
		
		$this->t_panier['PAN_XML'] = array2xml($tabxml['commande'],'commande');
		
		$this->save();
		
		return true;
	}


} //FIN CLASSE

?>
