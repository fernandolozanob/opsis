<?


	$myUsr=User::getInstance();
	$myPage=PageWebService::getInstance();
	
	global $db ;
	$upload_dir = kVideosDir;

	// paramètres upload : 
	// enctype = multipart/form-data
	// media : upload fichiers (input type file)
	
	if(isset($_FILES['media']) && !empty($_FILES['media'])){
		// code pour gérer le cas des upload file simple comme des upload file multiples
		foreach($_FILES['media'] as $key=>$subparam){
			if(!is_array($subparam)){
				$_FILES['media'][$key] = array($subparam);
			}
		}
		// recopie vers kVideosDir en suivant le modele établi dans jqueryUploadHandler
		foreach($_FILES['media']['name'] as $idx=>$filename){
			// si on renomme le fichier
			$nom_final = $filename ; 
			if (defined("gUploadModeRewrite") && stripos(gUploadModeRewrite, "rename") !== false) {
				$i = '';
				while (file_exists(kVideosDir . "/" . stripExtension($nom_final) . $i . '.' . getExtension($nom_final)))
					$i++;

				$nom_final = stripExtension($nom_final) . $i . '.' . getExtension($nom_final);
			}
			$ret = move_uploaded_file($_FILES['media']['tmp_name'][$idx],$upload_dir.$nom_final);
			if(!$ret){
				throw new Exception("error move_uploaded_file ".print_r($_FILES["media"]["error"][$idx],true),'001');
			}
			$_POST['files'][] = $nom_final;
		}
		
		// définition des paramètres standard
		$_POST['makedoc'] = 1 ;
		$_POST['noDspProcs'] = 1 ;
		$_POST['noDisplay'] = true ;
		$_REQUEST['type'] = 'batch_mat' ;
		$_REQUEST['type_import'] = 'API';
		
		
		// hack affichage messages finUpload
		global $api_msg ;
		$api_msg = "";
		
		function displayMsg($str){
			global $api_msg ;
			$api_msg.=$str."\n";
		}
		function displayErrorMsg($str){
			throw new Exception("finUpload:".$str,'002');
		}
		
		
		// inclusion d'un fichier site redéfinissable pour pouvoir modifier / editer / rajouter des paramètres à l'upload
		$local_param_file = getSiteFile('formDir','upload_service.inc.php');
		if(basename($local_param_file) !== 'index.php'){
			include($local_param_file);
		}
		
		// inclusion standard finUpload.php
		include(upload_scriptDir.'finUpload.php');
		
		// Si on est arrivé ici à priori tout s'est bien passé avant, écriture du message de retour à partir des messages remontés par finUpload.
		echo "<data>";
		echo "<msg>".$api_msg."</msg>";
		echo "</data>";
	}else{
		throw new Exception ("Media absent",'000');
	}
	
	
	
?>
