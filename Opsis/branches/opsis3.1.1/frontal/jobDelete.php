<?php

require_once(modelDir.'model_engine.php');
require_once(libDir."class_jobProcess.php");

if($argc<3)
{
	echo 'Usage : frontal.php jobCancel MODULE XML_IN',"\n";
	exit(-1);
}

if (!file_exists(jobDeleteDir.'/'.$argv[2].'/'.$argv[3]))
{
	echo 'Le fichier d\'annulation n\'existe pas',"\n";
	exit(-1);
}

// creation du repertoire module dans le repertoire deleting
if (!file_exists(jobDeletingDir.'/'.$argv[2]))
	mkdir(jobDeletingDir.'/'.$argv[2]);

// on deplace le fichier XML d'annulation vers le repertoire deleting
rename(jobDeleteDir.'/'.$argv[2].'/'.$argv[3],jobDeletingDir.'/'.$argv[2].'/'.$argv[3]);

$job_name=stripExtension($argv[3]);

$contenu=file_get_contents(jobRunDir.'/'.$argv[2].'/'.$job_name.'.pid');

$contenu=explode("\n",$contenu);
$pid=$contenu[0];
$id_engine=$contenu[1];

require_once(libDir.'class_jobProcess_'.strtolower($argv[2]).'.php');
call_user_func(array('jobProcess_'.strtolower($argv[2]),'kill'),$argv[2],$argv[3]);

// on ecrit dans le log 
file_put_contents(jobOutDir.'/'.$argv[2].'/'.$job_name.'.log','Suppression du traitement ',FILE_APPEND);

// suppression du fichier .pid
unlink(jobRunDir.'/'.$argv[2].'/'.$job_name.'.pid');


// on deplace le XML vers le repertoire canceled
if (!file_exists(jobDeletedDir.'/'.$argv[2]))
	mkdir(jobDeletedDir.'/'.$argv[2]);

// on supprime
$eng=new Engine(intval($id_engine));
if(is_file(jobRunDir.'/'.$argv[2].'/'.$eng->getNomServeur().'/'.$argv[3])){
    rename(jobRunDir.'/'.$argv[2].'/'.$eng->getNomServeur().'/'.$argv[3],jobDeletedDir.'/'.$argv[2].'/'.$argv[3]);
    unlink(jobDeletingDir.'/'.$argv[2].'/'.$argv[3]);
}else{
    rename(jobDeletingDir.'/'.$argv[2].'/'.$eng->getNomServeur().'/'.$argv[3],jobDeletedDir.'/'.$argv[2].'/'.$argv[3]);
}

// on vide le repertoire tmp
JobProcess::supRep(jobTmpDir.'/'.$argv[2].'/'.$job_name);

?>