<?php

require_once(modelDir.'model_engine.php');
require_once(libDir."class_jobProcess.php");
require_once(libDir."class_jobProcess_audiowaveform.php");
require_once(libDir."class_jobProcess_authot.php");
require_once(libDir."class_jobProcess_aws.php");
require_once(libDir."class_jobProcess_backup.php");
require_once(libDir."class_jobProcess_brightcove.php");
require_once(libDir."class_jobProcess_cut.php");
require_once(libDir."class_jobProcess_dailymotion.php");
require_once(libDir."class_jobProcess_dmcloud.php");
require_once(libDir."class_jobProcess_dvd.php");
require_once(libDir."class_jobProcess_episode.php");
require_once(libDir."class_jobProcess_facebook.php");
require_once(libDir."class_jobProcess_ffmbc.php");
require_once(libDir."class_jobProcess_ffmpeg.php");
require_once(libDir."class_jobProcess_ftp.php");
require_once(libDir."class_jobProcess_imagick.php");
require_once(libDir."class_jobProcess_kewego.php");
require_once(libDir."class_jobProcess_mediaspeech.php");
require_once(libDir."class_jobProcess_ocr.php");
require_once(libDir."class_jobProcess_record.php");
require_once(libDir."class_jobProcess_soundcloud.php");
require_once(libDir."class_jobProcess_story.php");
require_once(libDir."class_jobProcess_storyimg.php");
require_once(libDir."class_jobProcess_storypdf.php");
require_once(libDir."class_jobProcess_streamsave.php");
require_once(libDir."class_jobProcess_streamz.php");
require_once(libDir."class_jobProcess_unoconv.php");
require_once(libDir."class_jobProcess_webservice.php");
require_once(libDir."class_jobProcess_youtube.php");
require_once(libDir."class_jobProcess_zip.php");

// Usage : frontal.php processJob MODULE XML_IN
if($argc<5)
{
	print "Syntaxe : frontal.php processJob MODULE XML_IN ID_ENGINE\n";
	die;
}

$file_xml_in=$argv[3];
$engine=new Engine(intval($argv[4]));
try
{
	switch($argv[2])
	{
		case 'audiowaveform':
			$job=new JobProcess_audiowaveform($file_xml_in,$engine);
			break;
		case 'authot':
			$job=new JobProcess_authot($file_xml_in,$engine);
			break;
		case 'aws':
			$job=new JobProcess_aws($file_xml_in,$engine);
			break;
		case 'backup':
			$job=new JobProcess_backup($file_xml_in,$engine);
			break;
		case 'brightcove':
			$job=new JobProcess_brightcove($file_xml_in,$engine);
			break;
		case 'cut':
			$job=new JobProcess_cut($file_xml_in,$engine);
			break;
		case 'dailymotion':
			$job=new JobProcess_dailymotion($file_xml_in,$engine);
			break;
		case 'dmcloud':
			$job=new JobProcess_dmcloud($file_xml_in,$engine);
			break;
		case 'dvd':
			$job=new JobProcess_dvd($file_xml_in,$engine);
			break;
		case 'episode':
			$job=new JobProcess_episode($file_xml_in,$engine);
			break;
		case 'facebook':
			$job=new JobProcess_facebook($file_xml_in,$engine);
			break;
		case 'ffmbc':
			$job=new JobProcess_ffmbc($file_xml_in,$engine);
			break;
		case 'ffmpeg':
			$job=new JobProcess_ffmpeg($file_xml_in,$engine);
			break;
		case 'ftp':
			$job=new JobProcess_ftp($file_xml_in,$engine);
			break;
		case 'imagick':
			$job=new JobProcess_imagick($file_xml_in,$engine);
			break;
		case 'kewego':
			$job=new JobProcess_kewego($file_xml_in,$engine);
			break;
		case 'mediaspeech':
			$job=new JobProcess_mediaspeech($file_xml_in,$engine);
			break;
		case 'ocr':
			$job=new JobProcess_ocr($file_xml_in,$engine);
			break;
		case 'record':
			$job=new JobProcess_record($file_xml_in,$engine);
			break;
		case 'soundcloud':
			$job=new JobProcess_soundcloud($file_xml_in,$engine);
			break;
		case 'story':
			$job=new JobProcess_story($file_xml_in,$engine);
			break;
		case 'storyimg':
			$job=new JobProcess_storyimg($file_xml_in,$engine);
			break;
		case 'storypdf':
			$job=new JobProcess_storypdf($file_xml_in,$engine);
			break;
		case 'streamsave':
			$job=new JobProcess_streamsave($file_xml_in,$engine);
			break;
		case 'streamz':
			$job=new JobProcess_streamz($file_xml_in,$engine);
			break;
		case 'unoconv':
			$job=new JobProcess_unoconv($file_xml_in,$engine);
			break;
		case 'webservice':
			$job=new JobProcess_webservice($file_xml_in,$engine);
			break;
		case 'youtube':
			$job=new JobProcess_youtube($file_xml_in,$engine);
			break;
		case 'zip':
			$job=new JobProcess_zip($file_xml_in,$engine);
			break;
	}
	
	$job->prepareProcess();
	$job->doProcess();
	$job->finishJob();
	
	//supression du fichier .pid
	unlink(jobRunDir.'/'.$argv[2].'/'.stripExtension(basename($file_xml_in)).'.pid');
}
catch (Exception $e)
{
	$job->dropError('('.$e->getCode().') '.$e->getMessage());
}

?>
