<style>
#form1 {
	background: #eeeeb8;
}
</style>

<?

require_once(libDir."class_user.php");
if (!User::getInstance()->loggedIn()) {
	echo "<div class='error'>".kAccesReserve."</div>";
	return false;
}

require_once(libDir."class_upload.php");
global $db;
$myUpload=new Upload();

$id=$_REQUEST["id"];

switch ($_REQUEST['type']) {

	case "doc_acc":
		require_once(modelDir.'model_docAcc.php');
		$myUpload->arrParams=DocAcc::getUploadParams(); //appel statique

		$lib=kDocumentAcc;
		$myUpload->mode='doc_acc';

		// VP 30/11/09 : ajout dialogue upload
		include(getSiteFile("formDir","upload.inc.php"));
		$myUpload->updateParamFile();
		break;

	case "doc_mat":
		require_once(modelDir.'model_materiel.php');
		$myUpload->arrParams=Materiel::getUploadParams();
		$lib=kMaterielsLies;
		$myUpload->mode='materiel';

		include(getSiteFile("formDir","upload.inc.php"));
		$myUpload->updateParamFile();

	break;

	case "mat":
		require_once(modelDir.'model_materiel.php');
		$myUpload->arrParams=Materiel::getUploadParams();
		if (!empty($id)) $arrParams['arrParams']["strFilesFormat"]=$id; //Nom imposé - en cas d'update du fichier depuis une fiche matériel existant !
		// Script JS de mise à jour de l'ID_MAT sur la saisie matériel dès sélection d'un fichier dans le browser Flash
		$lib=kMateriel;
		$myUpload->mode='materiel';
		include(getSiteFile("formDir","upload.inc.php"));
		$myUpload->updateParamFile();

	break;

	case "batch_mat" :

		require_once(modelDir.'model_materiel.php');
		$myUpload->arrParams=Materiel::getUploadParams();

		$lib=kBatchUpload;
		$myUpload->mode='materiel';

		$myUpload->arrParams['arrParams']['maxFileCount']="99"; //pas de limite de nombre
		include(getSiteFile("formDir","upload.inc.php"));

		$myUpload->updateParamFile();

	break;

	case "batch_img" :
		$myUpload->mode='image';
		$lib=kAjoutImagesParUpload;
		require_once(modelDir.'model_imageur.php');
		$myUpload->arrParams=Imageur::getUploadParams();
		$myUpload->arrParams['arrParams']['maxFileCount']="99"; //pas de limite de nombre
		$myUpload->arrParams['arrParams']['ext_folder']=urldecode($id); //répertoire de l'imageur
		$myUpload->innerHTML.="<input type='hidden' name='IMAGEUR' value='".urldecode($id)."' />";
		$myUpload->updateParamFile();

	break;

}


Page::getInstance()->titre=kUpload." ".$lib;


echo "<div style='font-size:15px;font-weight:bold;font-family:\"Trebuchet MS\";margin:10px 0 5px 45px'>".Page::getInstance()->titre."</div>";

$myUpload->renderHTML(); //affichage composant / applet

echo "<div style=\"font-size:11px;text-align:center;\">";
echo kUploadPrecisions;
echo "</div>";

?>