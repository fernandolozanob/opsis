<?php

	// VP 28/03/11 : tests existence fonctions en cas d'appel successif
	//XB noDisplay: aucun affichage apres upload
	
	require_once(modelDir.'model_import.php');

	if(!function_exists("displayMsg")){
		function displayMsg($str) {
			$time=date('H:i:s',time());
			if(php_sapi_name()=="cli"){
				$msg= $time." : ".$str."\n";	
			}else{
				$msg= $time." : ".$str."<br/>";
			}
			$_SESSION['finUploadMsg'].=$msg;
			if($_POST['noDisplay']) echo '<div style="display:none;">'.$msg.'</div>';
			else 	echo $msg;
		}

		function displayError($str) {
			$msg= "<div class='error'>".displayMsg($str)."</div>";
			$_SESSION['finUploadMsg'].=$msg;
			if($_POST['noDisplay']) echo '<div style="display:none;">'.$msg.'</div>';
			else 	echo $msg;
		}
	}


	ini_set('max_execution_time','0');

	Page::getInstance()->titre=kUploadResultat;
	// VP 13/12/09 : ajout variable session finUploadMsg pour affichage dans fenêtre upload
	$_SESSION['finUploadMsg']='';





	// VP 1/04/11 : Initialisation variables
	$mats=array();
	$myPrms=$_POST; // Par défaut, on récupère les params POST (upload AJAX)
//	
//	echo '<div style="position:relative;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_affich $myPrms<pre>';
//	var_dump($myPrms);
//	echo '</pre></div><br />';
//	die();
	
	
	if (!empty($_SESSION['javaUploadPOST'])) //Params upload post en Session, on actualise le tableau (upload JAVA)
	{
		$myPrms=$_SESSION['javaUploadPOST'];
		//Prétraitement du tableau des fichiers uploadés :
		//En cas de découpage JAVA renvoie non pas le fichier final mais les X parties découpées
		//On va donc concaténer le tableau en cas de "chunks"

		$_bufferedFileName='';$_bufferedIndex=''; //prépa buffers
		foreach ($myPrms as $_key=>&$_prm) {
			$_fld=preg_split('/^filename/',$_key,-1);
			$_prm=utf8_encode($_prm);
			if (count($_fld)==2) { //champ de type filename+n°
				$idx=$_fld[1]; //récup n° de fichier

				if (is_numeric(getExtension($_prm))) { //fichier "chunk" de type xxx.ext.n°
					$_refName=stripExtension($_prm); //nom de référence, sans suffixe n°
					if ($_bufferedFileName!=$_refName) { //on vient de changer de fichier "de base"
						$_bufferedFileName=$_refName; //on stocke le nouveau nom de référence
						$_bufferedIndex=$idx; // et la ligne
						$myPrms['filename'.$_bufferedIndex]=$_refName; //maj de la ligne
						} else { //on parcours un autre "chunk" du fichier

						$myPrms['filesize'.$_bufferedIndex]+=$myPrms['filesize'.$idx]; //on additionne les tailles de chunks
						unset($myPrms['filesize'.$idx]); //on efface les chunks : name + size
						unset($myPrms['filename'.$idx]);
					}
				}
			}
		}
		// Ici, il ne reste plus que le premier "chunk" de chaque fichier avec le nom sans suffixe n° et la taille cumulée des chunks
			//trace(print_r($myPrms,true));
		foreach ($myPrms as $_key=>&$_prm) {
			$_fld=preg_split('/^filename/',$_key,-1);
			//$_prm=utf8_encode($_prm);

			if (count($_fld)==2) { //champ filename+n°
				$idx=$_fld[1]; //récup n° de fichier

				//Cette fonction transforme aussi certains caractères. Je veux les garder tel quels
				//$myPrms["file_".$idx."_original"]=getFileFromPath($_prm);
				$name=explode("/",str_replace("\\","/",$_prm));
				$name=$name[count($name)-1];
				$myPrms["file_".$idx."_original"]=$name;

				$key=removeTrickyChars(stripAccents($myPrms["file_".$idx."_original"],true));
				//$key=getFileFromPath($_prm); //On extrait le nom du fichier "original" (=valeur dans JAVA)
				//$realFileName=$key;
				// On recopie dans le repertoire destination si FTP
				if(($myPrms["mode"]=="ftp")){
					// VP 16/03/11 : traitement cas gUploadModeRewrite
					if (defined("gUploadModeRewrite") && stripos(gUploadModeRewrite,"rename")!==false) { //Renommage demandé ?
						$i='';
						while (file_exists(kVideosDir."/".stripExtension($key).$i.'.'.getExtension($key))) $i++; //Vérif existence fichier et incrément si vrai
						$realFileName=stripExtension($key).$i.'.'.getExtension($key); //On tient un nom libre !
					} else {
						$realFileName=$key; //mode overwrite
					}
					mv_rename(kUploadFTPDir.basename(str_replace("\\","/",$_prm)),kVideosDir.$realFileName);
				} elseif (file_exists(kVideosDir.$key.".txt")) { //On va chercher le nouveau nom après renommage dans le fichier texte s'il existe
					$realFileName=file_get_contents(kVideosDir.$key.".txt");
					if(strpos($realFileName,">>>")>0) $realFileName=substr($realFileName,strpos($realFileName,">>>")+3);
					unlink(kVideosDir.$key.".txt");
				} else { //pas de fichier txt => ts noms identiques
					$realFileName=$myPrms["file_".$idx."_original"];
				}

				$myPrms["file_".$idx]=$realFileName;
				$myPrms["file_".$idx."_status"]="ok";
				$myPrms["file_".$idx."_size"]=$myPrms["filesize".$idx];
				if ($myPrms["file_".$idx."_size"]==0) $myPrms["file_".$idx."_size"]=filesize(kVideosDir.$myPrms["file_".$idx]);
				unset($myPrms["filename".$idx]);
				unset($myPrms["filesize".$idx]);
			}
		}


		unset($_SESSION['javaUploadPOST']); //vidage tableau POST de JAVA
		unset($_SESSION['uploadNames']); //on vide le tableau
	}

//Enfin, dernier cas, celui des noms de fichiers passés depuis un formulaire (par ex, reprise de post-prod sur des
//matériels orphelins.

if ($myPrms['files'])
{
	foreach ($myPrms['files'] as $_i=>$_file)
	{
		if(!empty($_file)){
			$myPrms['file_'.$_i]=$_file;
		}
	}
}
debug($myPrms,'pink');


if(php_sapi_name()=="cli"){
	echo kUploadResultat."\n";
}else{
	//XB cacher affichage
	if(!isset($_POST['noDisplay'])) {
	echo "<div id='pageTitre'>".kUploadResultat."</div>
	<br/><hr/>";
	}
}



global $db;
$type=urldecode($_REQUEST['type']);


	
	// MS 29.09.14 - création de l'objet import 
	try{
		$myUsr = User::getInstance();			
		$usr_id =  $myUsr->UserID;
	}catch(exception $e){
		trace("finUpload - echec récupération id user");
	}
	if($type != 'doc_acc' && !isset($myImp)){
		$myImp = new Import() ; 
		$myImp->t_import['IMP_TYPE'] = (isset($_REQUEST['type_import'])?$_REQUEST['type_import']:"");
		$id_usager_POST = (!empty($_POST['ID_USAGER'])? $_POST['ID_USAGER'] : $_POST['DOC_ID_USAGER_CREA'] );
		$myImp->create(true,$id_usager_POST);
	}




//trace('finupload POST '.print_r($_POST,true));
//trace('finupload GET '.print_r($_GET,true));
//debug($myPrms,'plum');

switch ($type) {

	case "doc_acc" :
		require_once(modelDir.'model_docAcc.php');
		

		for($i=0; $i<count($myPrms); $i++)
		{
			$myDA= new DocAcc;
			if(isset($myPrms["file_". $i]))
			{
				if (empty($myPrms["file_". $i])) displayError($myPrms["file_". $i."_status"]);
				else {
				$id_doc=urldecode($_REQUEST['id_doc']);
				$id_pers=urldecode($_REQUEST['id_pers']);
				$id_fest=urldecode($_REQUEST['id_fest']);
				$id_panier=urldecode($_REQUEST['id_panier']);
				$id_cat=urldecode($_REQUEST['id_cat']);

                // VP 10/07/13 : possibilité de passer un id_doc par fichier
                if(isset($myPrms["id_doc_". $i])&& !empty($myPrms["id_doc_". $i])){
                    $id_doc=urldecode($myPrms["id_doc_". $i]);
                }
                // VP 10/07/13 : déplacement fichier pour rangement
                if(isset($myPrms["file_". $i."_path"])&& !empty($myPrms["file_". $i."_path"]) && is_file($myPrms["file_". $i."_path"])){
                    rename($myPrms["file_". $i."_path"], kDocumentDir.getFileFromPath($myPrms["file_". $i]));
                }
                
    			$myDA->t_doc_acc['ID_LANG']= $_SESSION['langue'];
			    $myDA->t_doc_acc['ID_DOC']= $id_doc;
			    $myDA->t_doc_acc['ID_PERS']= $id_pers;
			    $myDA->t_doc_acc['ID_FEST']= $id_fest;
			    $myDA->t_doc_acc['ID_PANIER']= $id_panier;
			    $myDA->t_doc_acc['ID_CAT']= $id_cat;

				// if(isset($myImp) && !empty($myImp->t_import['ID_IMPORT'])){
					// $myDA->t_doc_acc['DA_ID_IMPORT'] = $myImp->t_import['ID_IMPORT'];
				// }
				
				$myDA->t_doc_acc['DA_FICHIER']=getFileFromPath($myPrms["file_". $i]);

                //Le fichier est absent: ceci peut arriver si pb avec le Flashupload.
                // Nous avons rencontré le cas avec un max_file_size mal interprété par flahsupload
                // L'upload se faisait, mais le fichier, trop gros, était finalement rejeté par PHP.
				if (!$myDA->checkFileExists()) {
                    $myDA->error_msg=kDocAccFichierManquant;
                    displayError(kDocAccFichierManquant);
                    continue;
                }

				$myDA->t_doc_acc['DA_TITRE']=$myPrms["file_". $i];
                    
                // VP 10/07/13 : Ajout des champs du formulaire
                $myDA->mapFields($myPrms);
                    
				$myPrms["file_" . $i."_size"]=filesize(kDocumentDir.$myPrms["file_". $i]);
				
				foreach ($_SESSION['arrLangues'] as $lg) {
					$myDA->t_doc_acc['ID_LANG']=$lg;
					//ld 4/12/8 créa sans chk doublons car posait pb en cas de fichier déjà affecté à un autre doc
					$ok=$myDA->create(false);
				}
                
                if($ok){
                    // VP 10/07/13 : Sauvegarde éventuelle des valeurs
                    $myDA->saveDocAccVal();
                }

                    // VP 4/07/13 : Extraction texte pour pdf
                $fileSrc=$myDA->getFilePath();
                if(getExtension($fileSrc)=='pdf') $myDA->extractText($fileSrc);

                displayMsg($myPrms["file_". $i]);
				displayMsg(kUploadFileSucces. " (" . filesize_format($myPrms["file_" . $i."_size"]) . ")");
				if ($ok) displayMsg(kUploadDocAccSucces); else displayError(kUploadDocAccEchec);
                    
                // VP 4/07/13 : remplissage $mats pour traitements
                $mats[]["ID_DOC_ACC"]=$myDA->t_doc_acc['ID_DOC_ACC'];

/*
				$msg= "<script type=\"text/javascript\">
				if (window.opener
					&& window.opener.location.href.indexOf('docAccSaisie') !=-1
					&& window.opener.document.getElementById('id_doc').value=='".$id_doc."'
					&& window.opener.document.getElementById('id_pers').value=='".$id_pers."') {

					params=new Array();
					params['DA_TITRE']=".quoteField($myDA->t_doc_acc['DA_FICHIER']).";
					params['DA_FICHIER']=".quoteField($myDA->t_doc_acc['DA_FICHIER']).";
					params['ID_DOC_ACC']=".quoteField($myDA->t_doc_acc['ID_DOC_ACC']).";
					";
*/
				//by ld 30/01/09 : tests allégés pour lancer la fonction addLine le plus possible.
				$aFormatsDocacc = unserialize(gListeFormatDocAcc);
				debug($myPrms, "gold", true);
				debug($aFormatsDocacc[$myPrms['file_'.$i.'_mime']], "red", true);
				$msg= "<script type=\"text/javascript\">
                    params=new Array();
                    params['DA_TITRE']=".quoteField($myDA->t_doc_acc['DA_FICHIER']).";
                    params['DA_FICHIER']=".quoteField($myDA->t_doc_acc['DA_FICHIER']).";
                    params['DA_CHEMIN']=".quoteField($myDA->t_doc_acc['DA_CHEMIN']).";
                    params['ID_DOC_ACC']=".quoteField($myDA->t_doc_acc['ID_DOC_ACC']).";
                    params['FORMAT']='".$aFormatsDocacc[$myPrms['file_'.$i.'_mime']]."';
					
                    if (typeof window.opener != 'undefined' && window.opener  && window.opener.addLine) {  ";
                        foreach ($_SESSION['arrLangues'] as $id=>$lg) {
                            $msg.="params['ID_LANG']=".quoteField($lg).";
                            window.opener.addLine(params,".($id==count($_SESSION['arrLangues'])-1?1:0).");";
                        }                    

                $msg.= "} else if (window.parent  && window.parent.addLine) {  ";
                        foreach ($_SESSION['arrLangues'] as $id=>$lg) {
                            $msg.="params['ID_LANG']=".quoteField($lg).";
                            window.parent.addLine(params,".($id==count($_SESSION['arrLangues'])-1?1:0).");";
                        }
				$msg.= "}</script>";
				
				//XB cacher affichage
				if(!isset($_POST['noDisplay'])) 
					$msg.= "<hr/>";

				displayMsg($msg);
			}

		}
	}
	break;



	case "mat" :
            trace("mat finUpload : myPrms");
            trace(print_r($myPrms,1));
			require_once(modelDir.'model_materiel.php');
			$myMat= new Materiel;
            
			for($i=0; $i<count($myPrms); $i++)
			{
				if(isset($myPrms["file_". $i]))
				{
				  if (empty($myPrms["file_". $i])) displayError( $myPrms["file_". $i."_status"]);
				  else{
                      if(!empty($myPrms['id_mat'])){
                          $myMat->t_mat['ID_MAT'] = $myPrms['id_mat'];
                          $myMat->GetMat();
                      }
					  $filename=kVideosDir.$myPrms["file_". $i];
					  $myPrms["file_" . $i."_size"]=filesize($filename);
					  displayMsg( kUploadFileSucces. $myMat->t_mat['MAT_NOM']. " (" . filesize_format($myPrms["file_" . $i."_size"]) . ")");
					  $myPrms["MAT_TITRE"] = $myPrms["file_" . $i."_original"];
						
					  if(isset($myImp) && !empty($myImp->t_import['ID_IMPORT'])){
						$myPrms['MAT_ID_IMPORT'] =$myImp->t_import['ID_IMPORT'];
					  }
                      if (isset($myPrms['checkbox_rename']) && !empty($myPrms['checkbox_rename'])) {
                          $rename_file=true;
                      } elseif(!isset($myPrms['checkbox_rename']) && !empty($myPrms["rename"])) {
                          $rename_file=true;
                      }else{
                          $rename_file = false;
                      }
					  $ok=$myMat->ingest($filename,$myPrms,$myPrms["rename"],$rename_file, $infos); // création du matériel.


					// Le fichier est absent: ceci peut arriver si pb avec le Flashupload.
					// Nous avons rencontré le cas avec un max_file_size mal interprété par flahsupload
					// L'upload se faisait, mais le fichier, trop gros, était finalement rejeté par PHP.
					if (!$myMat->checkFileExists()) {$myMat->error_msg=kMaterielFichierManquant;break;}
					elseif(!$ok) displayError(kUploadMaterielEchec);
					else{
						displayMsg(kUploadMaterielSucces);
                        if(isset($myPrms["id_proc_" . $i]) && !empty($myPrms["id_proc_" . $i])) {
                            $mats[]=array('ID_MAT'=>$myMat->t_mat['ID_MAT'], 'id_proc'=>$myPrms["id_proc_" . $i]);
                        } else {
                            $mats[]["ID_MAT"]=$myMat->t_mat['ID_MAT'];
                        }
                        
						displayMsg( "<script type=\"text/javascript\">
						if (window.opener
							&& window.opener.location.href.indexOf('matSaisie') !=-1) {

							if (window.opener.document.getElementById('id_mat')) {
								window.opener.document.getElementById('id_mat').value='".addslashes($myMat->t_mat['ID_MAT'])."';
								window.opener.document.getElementById('id_mat_ref').value='".addslashes($myMat->t_mat['MAT_NOM'])."';
								}

							if (window.opener.document.getElementById('file_exists')) {
								window.opener.document.getElementById('file_exists').innerHTML='".kMaterielFichierExiste."';
								}

							if (window.opener.document.getElementById('mat_format')) {
								setValue('".$infos['format']."',window.opener.document.getElementById('mat_format'),false);
								}
							if (window.opener.document.getElementById('mat_tcin')) {
								setValue('".$infos['tcin']."',window.opener.document.getElementById('mat_tcin'),false);
								}
							if (window.opener.document.getElementById('mat_tcout')) {
								setValue('".$infos['tcout']."',window.opener.document.getElementById('mat_tcout'),false);
								}
							if (window.opener.document.getElementById('mat_info')) {

								setValue('".(str_replace(array(chr(10),chr(13),chr(9),'<?','?>'),array('\n','','\t','&lt;?','?&gt;'),$infos['xml']))."',window.opener.document.getElementById('mat_info'),false);
								}
							}</script><hr/>");
					}
				}
			}
		}
	break;


	case "doc_mat":
        trace("finUpload : myPrms");
        trace(print_r($myPrms,1));
		require_once(modelDir.'model_materiel.php');
		require_once(modelDir.'model_doc.php');
		// Import des materiels
		for($i=0; $i<count($myPrms); $i++) {
			if(isset($myPrms["file_". $i])) {
				
				/* 	Traitement spécifique si on uploade 1 fichier csv dans l'onglet sous-titrage (cas $_GET['subtitle']), on le met à part
				 * pour + de clarté */
				if (isset($_REQUEST['subtitle'])) {
					$id_doc = (int) $_REQUEST['id_doc'];
					$filepath_uploaded = kVideosDir . $myPrms["file_" . $i];

					
					$myPrms["doc_mat_rename"]=true;
					
					
					if (defined('kSousTitrage_lang')) {
						$tab_subtitleLang_VAL = unserialize(kSousTitrage_lang);
						if (is_array($tab_subtitleLang_VAL) && !empty($tab_subtitleLang_VAL)) {
							if (trim($tab_subtitleLang_VAL['id_type_val']) != '') {
								$id_type_valSubtitle = strtoupper(trim($tab_subtitleLang_VAL['id_type_val']));

								if (!empty($myPrms["t_mat_val"][$id_type_valSubtitle]) && !empty($myPrms["override_subtitle"])) {
									if (!empty($_SESSION['sousTitrageEditionVolee']['lastInCatLang'][$id_doc][$myPrms["t_mat_val"][$id_type_valSubtitle]])) {
										$matTodelete = new Materiel();
										$matTodelete->t_mat['ID_MAT'] = $_SESSION['sousTitrageEditionVolee']['lastInCatLang'][$id_doc][$myPrms["t_mat_val"][$id_type_valSubtitle]];
										$matTodelete->delete(); //si on delete le sous-titre actif, il faut le maj, voir + bas
//							$myPrms["doc_mat_rename"]=false;
									}
								}
							}
						}
					}

					$filepath_uploadedVTT = null;
					if (substr($filepath_uploaded, -4) == '.csv') {

						//		trace('subtitle_SRC ' . $filepath_uploaded);
						if (file_exists($filepath_uploaded)) {
							require_once(libDir . "class_matInfo.php");
							$filepath_uploadedVTT = preg_replace('/\..*$/', '.vtt', $myPrms["file_" . $i]);
								trace('$filepath_uploaded '.$filepath_uploaded);
								trace('$filepath_uploadedVTT-1 '.$filepath_uploadedVTT);
							$TABfilepath_uploadedVTT = explode('.vtt', $filepath_uploadedVTT);
							//il ne faut pas que $filepath_uploadedVTT corresponde à un nom de matériel deja existant
							$newNameVTT = null;
							trace('SELECT COUNT(*) AS compte FROM t_mat WHERE MAT_NOM ' . $db->like("%$filepath_uploadedVTT%"));
							if ($db->GetOne('SELECT COUNT(*) AS compte FROM t_mat WHERE MAT_NOM ' . $db->like("%$filepath_uploadedVTT%"))) {
								$newNameVTT = "_00";
								while ($db->GetOne('SELECT COUNT(*) AS compte FROM t_mat WHERE MAT_NOM ' . $db->like("%$TABfilepath_uploadedVTT[0]$newNameVTT.vtt%"))) {
									trace('SELECT COUNT(*) AS compte FROM t_mat WHERE MAT_NOM ' . $db->like("%$TABfilepath_uploadedVTT[0]$newNameVTT.vtt%"));
									$newNameVTT = stringIteration($newNameVTT);

									//	echo $newName;
								}
							}

							$filepath_uploadedVTT = $TABfilepath_uploadedVTT[0] . $newNameVTT . '.vtt';

							trace('$filepath_uploadedVTT-2 '.$filepath_uploadedVTT);
							//@see http://php.net/manual/fr/function.fgetcsv.php 
//							Note: Si vous avez des problèmes avec PHP qui ne reconnaît pas certaines lignes lors de la lecture de fichiers qui ont été créés ou lus sur un MacIntosh, vous pouvez activer l'option de configuration auto_detect_line_endings.
							ini_set('auto_detect_line_endings',1);
							MatInfo::subtitle_convertCSV($filepath_uploaded,kVideosDir.$filepath_uploadedVTT);

							if (!unlink($filepath_uploaded)) {
								trace('unlink failed to :' . $filepath_uploaded);
							}
						}

						// jqueryUploadHandler dans kVideoDir
//						break;
					}
				}


				if (empty($myPrms["file_". $i])) displayError($myPrms["file_". $i."_status"]);
				elseif (getExtension($myPrms["file_". $i])!="xml") {
					// VP 9/12/10 correction bug sur sauvegarde doc_mat
					$id_doc=urldecode($_REQUEST['id_doc']);
					$myMat= new Materiel;
					
					//VP 25/10/10 : prise en compte importFolder dans import doc_mat
					$filename=kVideosDir.$myPrms["file_". $i];
					if (!empty($filepath_uploadedVTT)){
						$filename=kVideosDir.$filepath_uploadedVTT;	
//						trace('$filepath_uploadedVTT2 '.$filename);
					}
					
					
					if(!empty($myPrms["file_".$i."_path"]) && file_exists($myPrms["file_".$i."_path"])){
						$filename=$myPrms["file_".$i."_path"];
					}elseif(!empty($myPrms["importFolder"])){
						$filename= $myPrms["importFolder"]."/".$myPrms["file_". $i];
						if(file_exists($filename)){
							$myPrms["file_" . $i."_original"]=$myPrms["file_". $i];
						}
					}
					
					
                    if(isset($myImp) && !empty($myImp->t_import['ID_IMPORT'])){
                        $myPrms['MAT_ID_IMPORT'] =$myImp->t_import['ID_IMPORT'];
                    }
					
                    // VP 14/12/2015 : remplacement doc_mat_rename par checkbox_rename
                    if(isset($myPrms['doc_mat_rename'])) {
                        $myPrms['checkbox_rename'] = $myPrms['doc_mat_rename'];
                    }
					if(!isset($myPrms['checkbox_rename']) || empty($myPrms['checkbox_rename'])){
						$myPrms['checkbox_rename'] = false ;
					}
					
					$myPrms["file_" . $i."_size"]=filesize($filename);
					$myPrms["MAT_TITRE"] = $myPrms["file_" . $i."_original"];
					displayMsg( kUploadFileSucces. $myPrms["file_". $i]. " (" . filesize_format($myPrms["file_" . $i."_size"]) . ")");
					
					$ingest_dpx_folder=false;
					
					if (is_dir($filename))
					{
						//liste des repertoires presents dans le dossier
						$contenu=array();
						$has_dpx_folder=false;
						$has_prores_folder=false;
						
						if (($dh=opendir($filename)))
						{
							while ($file=readdir($dh))
							{
								if ($file!='.' && $file!='..' && is_dir($filename.'/'.$file))
								{
									$elt_file=explode('_',$file);
									
									if (strtolower($elt_file[0])=='dpx')
										$has_dpx_folder=true;
									
									if (strtolower($elt_file[0])=='prores')
										$has_prores_folder=true;
									
									$contenu[]=$file;
								}
							}
							closedir($dh);
						}
						if ($has_dpx_folder && $has_prores_folder && count($contenu)<=3 && count($contenu)>=2 && $myMat->checkDpxFolder($filename, $infos_dpx)) /// le dossier contient un repertoire prores, un dossier DPX, et contient maximum 3 dossiers ==> ingestDpxFolder
						{
							$mats_ingest=$myMat->ingestDpxFolder($filename,$myPrms["rename"],false,$myPrms);
							$ingest_dpx_folder=true;
							$mats[]['ID_MAT']=$mats_ingest[0]->t_mat['ID_MAT'];
							$mats[]['ID_MAT']=$mats_ingest[1]->t_mat['ID_MAT'];
						}
						elseif($myPrms['onlyDpx']=='1')
                        {
                            displayError(kErrorImportDpx);
                            $ok=false;
                        }
						else
							$ok=$myMat->ingestFolder($filename,$myPrms["rename"],$myPrms["checkbox_rename"],$myPrms);
						
					}
					else
						$ok=$myMat->ingest($filename,$myPrms,$myPrms["rename"],$myPrms["checkbox_rename"], $infos); // création du matériel.

					if (!$ok) {//Echec création matériel
						$message.= displayError(kUploadMaterielEchec." : ".$myMat->error_msg);
					} else {
						
						
						if (isset($_REQUEST['subtitle'])) {
							$_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc]['id_mat'] = $myMat->t_mat['ID_MAT'];
						}
						
//						trace('finUpload.php - t_mat_val');
						
//						trace(print_r($myPrms["t_mat_val"],true));
					
						//B.RAVI 2016-11-16 Pour les sous-titres nous avons besoin de sauvegarder des mat_val de la langue
						// VP 8/06/2017 : déplacé dans méthode ingest() de class_materiel
						/*
						if (!empty($myPrms["t_mat_val"])){
							$myMat->t_mat_val=array();

							foreach ($myPrms["t_mat_val"] as $key=>$value){
								if (!empty($value)){
									$myMat->t_mat_val[]=array('ID_VAL'=>$value,'ID_TYPE_VAL'=>$key);
								}
							}
						}
						$myMat->saveMatVal();
						 */
						
						
						
						
						//Matériel créé
						displayMsg(kUploadMaterielSucces);
                        if(isset($myPrms["id_proc_" . $i]) && !empty($myPrms["id_proc_" . $i])) {
                            $mats[]=array('ID_MAT'=>$myMat->t_mat['ID_MAT'], 'id_proc'=>$myPrms["id_proc_" . $i]);
                        } else {
                            $mats[]["ID_MAT"]=$myMat->t_mat['ID_MAT'];
                        }
						//Sauvegarde doc_mat
						if (!$ingest_dpx_folder) // dans le cas d'un import DPX les materiels sont associés dans la methode ingestDpxFolder
						{
							// VP 20/10/10 : test existance doc_mat
							$createDocMat = true;
							$myMat->getDocMat();
							foreach($myMat->t_doc_mat as $idx => $dm){
								if($dm["ID_DOC"] == $id_doc) {
									$createDocMat=false;
									$id_doc_mat=$dm['ID_DOC_MAT'];
									break;
								}
							}
							
							if($createDocMat){
								unset($myMat->t_doc_mat);
								$myMat->t_doc_mat[0]["ID_MAT"]=$myMat->t_mat['ID_MAT'];
								$myMat->t_doc_mat[0]["ID_DOC"]=$id_doc;
                                                                
								//B.RAVI 2016-04-26 Besoin sur colas de pouvoir uploader à inactif (sur simpleUploadJS)
								if (isset($myPrms["DMAT_INACTIF"])){
								$myMat->t_doc_mat[0]['DMAT_INACTIF']=(int)$myPrms["DMAT_INACTIF"];
								}

                                if (isset($myPrms["dmat_tcin"]) && $myPrms["dmat_tcin"] != "00:00:00:00" && !empty($myPrms["dmat_tcin"])) $myMat->t_mat['MAT_TCIN']=$myPrms["dmat_tcin"];
								if (isset($myPrms["dmat_tcout"]) && $myPrms["dmat_tcout"] != "00:00:00:00" && !empty($myPrms["dmat_tcout"])) $myMat->t_mat['MAT_TCOUT']=$myPrms["dmat_tcout"];
                                $myMat->t_doc_mat[0]['DMAT_TCIN']=$myMat->t_mat['MAT_TCIN'];
                                $myMat->t_doc_mat[0]['DMAT_TCOUT']=$myMat->t_mat['MAT_TCOUT'];
								$ok=$myMat->saveDocMat();
								$id_doc_mat=$myMat->t_doc_mat[0]['ID_DOC_MAT'];
							} elseif ( $myPrms['update_tc'] == '1' ) {
								$nb = 0;
								foreach ($myMat->t_doc_mat as $key => $docMat) {
									if($docMat['ID_DOC'] == $id_doc && $docMat['ID_MAT'] == $myMat->t_mat['ID_MAT']) {
										$nb++;
										$numMat = $key;
									}
								}
								//S'il y a plusieurs matériels qui correspondent, on risque de modifier le mauvais, donc on ne met à jour que quand il n'y a qu'un seul matériel
								if($nb == 1) {
									$myMat->t_doc_mat[$numMat]['DMAT_TCIN']=$myMat->t_mat['MAT_TCIN'];
									$myMat->t_doc_mat[$numMat]['DMAT_TCOUT']=$myMat->t_mat['MAT_TCOUT'];
									$ok=$myMat->saveDocMat();
								}
							}

							//Mise à jour doc
							$myDoc=new Doc;
							$myDoc->t_doc["ID_DOC"]=$id_doc;
							$myDoc->getDoc();
							if($myDoc->t_doc["DOC_DUREE"]=="00:00:00" || empty($myDoc->t_doc["DOC_DUREE"])) {
								$myDoc->t_doc["DOC_DUREE"]=secToTime(tcToSec($myMat->t_mat['MAT_TCOUT'])-tcToSec($myMat->t_mat['MAT_TCIN']));
								$myDoc->save();
							}
							$myDoc->metAJourDocNum($myMat->t_mat['ID_MAT']);
							if (isset($myPrms["save_mat_seq"])) $myDoc->saveDocMatForSequences($myMat->t_mat['ID_MAT']);
							unset($myDoc);
							// Mise à jour fenetre doc_mat
							echo( "<script type=\"text/javascript\">
								if (typeof window.opener != 'undefined' && window.opener && window.opener.addDocMat)
								 window.opener.addDocMat(null,null,".htmlentities(quoteField($id_doc_mat)).");
                                 else if (typeof window.parent != 'undefined' && window.parent && window.parent.addDocMat)
								 window.parent.addDocMat(null,null,".htmlentities(quoteField($id_doc_mat)).");
								 else if (typeof window.opener != 'undefined' && window.opener && window.opener.addMat)
								 window.opener.addMat(null,null,".htmlentities(quoteField($myMat->t_mat['ID_MAT'])).");
                                 else if (typeof window.parent != 'undefined' && window.parent && window.parent.addMat)
								 window.parent.addMat(null,null,".htmlentities(quoteField($myMat->t_mat['ID_MAT'])).");
								  ");
					//XB cacher affichage
						if(!isset($_POST['noDisplay'])) 
							echo ("</script><hr/>");
						 else
							 echo ("</script>");
							
						}
					}
					unset($myMat);
				}
			}
		}
		break;

	case "batch_mat" :
		trace("finUpload : myPrms");
		trace(print_r($myPrms,1));
		require_once(modelDir.'model_materiel.php');
		require_once(modelDir.'model_doc.php');
		if(defined("gUploadToSelection") && gUploadToSelection && isset($_POST['uploadToSelection']) && $myPrms['makedoc']){
			$upload_to_sel = true;
		}
		require_once(modelDir.'model_docReportage.php');
		if (isset($_POST['id_doc2link']) && !empty($_POST['id_doc2link']) && $myPrms['makedoc'] && isset($_POST['uploadToReportage']) && Reportage::isReportageEnabled()) {
			$doc2link = new Reportage;
			$doc2link->t_doc["ID_LANG"]=$_SESSION['langue'];
			if (is_numeric($_POST['id_doc2link'])) {
				$doc2link->t_doc['ID_DOC'] = intval($_POST['id_doc2link']);
				$doc2link->getDoc();
			}
			elseif ($_POST['id_doc2link'] == "new") {
				$doc2link->t_doc['DOC_TITRE'] = $_POST["reportName"];
				if(!empty( $_POST["reportCote"])){
					$doc2link->t_doc['DOC_COTE'] = $_POST["reportCote"];
				}
				else{
					$doc2link->t_doc['DOC_COTE'] = $_POST["reportName"];
				}
				$doc2link->mapFields($myPrms);
				if(defined('gReportageTypeMedia') ){
					$doc2link->t_doc['DOC_ID_MEDIA'] = gReportageTypeMedia;
				}
				$doc2link->t_doc['DOC_ID_TYPE_DOC']=intval(gReportagePicturesActiv);
				if ($myPrms['doc_trad']) $doc2link->doc_trad=1;
				$doc2link->save();
				$doc2link->saveDocLex();
				$doc2link->saveDocVal();				
				$doc2link->updateChampsXML('DOC_XML');
				$doc2link->updateChampsXML('DOC_VAL');
				$doc2link->updateChampsXML('DOC_LEX');
				if (defined("useSinequa") && useSinequa)
					$doc2link->saveSinequa();
				if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))
					$doc2link->saveSolr();
			}
			else unset($doc2link);
			$id_doc2link = intval($doc2link->t_doc['ID_DOC']);
		}else{
			$id_doc2link = null;
		}
		if($upload_to_sel){
			require_once(modelDir.'model_panier.php');
			$myCartUpload = new Panier();
			$myCartUpload->t_panier['PAN_ID_ETAT'] = 0 ;
			$myCartUpload->t_panier['PAN_ID_TYPE_COMMANDE'] = 0 ;
			if(isset($_POST['uploadSelectionName']) && !empty($_POST['uploadSelectionName'])){
				$name_sel = $_POST['uploadSelectionName'];
			}else if (defined("gUploadSelectionName")){
				$name_sel =gUploadSelectionName;
			}else {
				$name_sel = "import ".date("Y.m.d H:i");
			}
			$myCartUpload->t_panier['PAN_TITRE'] = $name_sel;
			if(isset($_POST['uploadSelectionUsager']) && !empty($_POST['uploadSelectionUsager'])){
				$usr_id = $_POST["uploadSelectionUsager"];
			}else{
				$myUsr = User::getInstance();			
				$usr_id =  $myUsr->UserID;
			}
			$myCartUpload->t_panier['PAN_ID_USAGER'] =$usr_id;
			$myCartUpload->createPanier();
		}
		

		// Import de documents
		for($i=0; $i<count($myPrms); $i++) 
		{
			if(isset($myPrms["file_". $i])) 
			{
				if (getExtension($myPrms["file_". $i])=="xml") 
				{
					$cntDoc=0;
					// Fichier xml à importer
					
					// GT 26/10/2010 prise en compte parametre importFolder si le fichier XML est dans le repertoire media/import 
					if(!empty($myPrms["importFolder"]))
						$filename= $myPrms["importFolder"]."/".$myPrms["file_". $i];
					else
						$filename=kVideosDir.$myPrms["file_". $i];
					
					$data = implode(" ",file($filename));
					

					// GT 26/10/2010 sauvegarde du fichier XML une fois traité
					// GT 05/04/2011 sauvegarde dans un repertoire d'archive défini dans kArchiveDir
					if (defined('kArchiveDir'))
					{
						//echo 'NOM FICHIER : '.$filename;
						mv_rename($filename,kArchiveDir.basename($filename).".done");
					}
					else
						rename($filename,$filename.".done");
					
					// Traitement xsl
					if(!empty($myPrms["xsl"])){
						
						$param = array("xml_file_name" => $myPrms["file_". $i]);
						// VP 29/8/11 : ajout paramètre htmlOutput=false
						//$data = TraitementXSLT($data,getSiteFile("designDir","print/".$myPrms["xsl"].".xsl"),$param,0);
						$data = TraitementXSLT($data,getSiteFile("designDir","print/".$myPrms["xsl"].".xsl"),$param,0,"",null,false);
					}
					$docs=xml2tab($data);
					foreach($docs["DATA"][0]["DOC"] as $idx=>$doc){
						$myDoc=new Doc;
						if(isset($doc["ID_LANG"])) $myDoc->t_doc["ID_LANG"]=$doc["ID_LANG"];
						else $myDoc->t_doc["ID_LANG"]=$_SESSION['langue'];
						$myDoc->mapFields($doc);
						$okDoc=$myDoc->save();
						if ($okDoc) {
							$myDoc->saveDocLex();
							$myDoc->saveDocVal();
							// On remplit au préalable le tableau doc_mat avec l'ID du doc
							foreach($myDoc->t_doc_mat as $idx2=>&$dm) {$dm['ID_DOC']=$myDoc->t_doc['ID_DOC'];}
							$myDoc->saveDocMat();
							$cntDoc++;
						}
						unset($myDoc);
					}
					displayMsg(kUploadFileSucces.$myPrms["file_". $i]." (".$cntDoc." ".kDocumentsCrees.")<br/>");
				}
			}
		}
		
		$numFile = 0;//Compteur de fichiers importés
		for($i=0; $i<count($myPrms); $i++) 
		{
			if(isset($myPrms["file_". $i])) 
			{
				if (empty($myPrms["file_". $i])) 
					displayError($myPrms["file_". $i."_status"]);
				elseif (getExtension($myPrms["file_". $i])!="xml") 
				{
					$myMat= new Materiel;

					// VP 15/04/10 : Traitement de fichier existant
					if(!empty($myPrms["file_".$i."_path"]) && file_exists($myPrms["file_".$i."_path"]))
					{
						$filename=$myPrms["file_".$i."_path"];
					}
					else
					{
						// VP 11/10/10 : test sur importFolder plutôt que FCPWatchFolder
						if(!empty($myPrms["importFolder"]))
						{
							$filename= $myPrms["importFolder"]."/".$myPrms["file_". $i];
							//if(defined('FCPWatchFolder')){
							//$filename= FCPWatchFolder.$myPrms["file_". $i];
							// Recopie si fichier existant
							if(file_exists($filename))
							{
								$myPrms["file_" . $i."_size"]=filesize($filename);
								$myPrms["file_" . $i."_original"]=$myPrms["file_". $i];
							}
						}
						else
						{
							$filename=kVideosDir.$myPrms["file_". $i];
							$myPrms["file_" . $i."_size"]=filesize($filename);
						}
						// VP 26/8/09 : Renommage éventuel avec calcul d'un nouveau nom
						if(!empty($myPrms["rename"]))
						{
							//@update VG 18/06/2010 : on utilise $numFile plutôt que $i, puisque $myPrms["file_". $i] n'existe pas forcément pour tout $i entier
							if($numFile>0) 
								$myPrms["rename"]=stringIteration($myPrms["rename"]);
						}

					}
					$myPrms["MAT_TITRE"] = $myPrms["file_" . $i."_original"];
					
					if(isset($myImp) && !empty($myImp->t_import['ID_IMPORT'])){
						$myPrms['MAT_ID_IMPORT'] =$myImp->t_import['ID_IMPORT'];
					}	

					if(isset($myPrms['file_'.$i.'_mat']) && is_array($myPrms['file_'.$i.'_mat'])){
						$makeMatPrms = array_merge($myPrms,$myPrms['file_'.$i.'_mat']);
					}else{
						$makeMatPrms = $myPrms;
					}
					$import_dir=false;
					if (is_dir($filename))
					{
						$rename_file=false;
						if (isset($myPrms['checkbox_rename']) && !empty($myPrms['checkbox_rename'])) {
							$rename_file=true;
						//Pour les sites qui n'ont pas checkbox_rename pour lesquels on ne se base que sur myPrms["rename"]
						} elseif(!isset($myPrms['checkbox_rename']) && !empty($myPrms["file_". $i."_rename"])) {
							$rename_file=true;
						} elseif(!isset($myPrms['checkbox_rename']) && !empty($myPrms["rename"])) {
							$rename_file=true;
						}
						$aIdMatCreated = array();
                        $rename_tmp = (isset($myPrms["file_". $i."_rename"])&&!empty($myPrms["file_". $i."_rename"]))?stripExtension($myPrms["file_". $i."_rename"]):$myPrms["rename"] ;
						$ok=$myMat->ingestFolder($myPrms['file_'. $i.'_path'],$rename_tmp,$rename_file,$makeMatPrms,$aIdMatCreated);
						unset($rename_tmp);
						$import_dir=true;
					} else {
						$rename_file=false;
						if (isset($myPrms['checkbox_rename']) && !empty($myPrms['checkbox_rename'])) {
							$rename_file=true;
						//Pour les sites qui n'ont pas checkbox_rename pour lesquels on ne se base que sur myPrms["rename"]
						} elseif(!isset($myPrms['checkbox_rename']) && !empty($myPrms["file_". $i."_rename"])) {
							$rename_file=true;
						} elseif(!isset($myPrms['checkbox_rename']) && !empty($myPrms["rename"])) {
							$rename_file=true;
						}
						$rename_tmp = (isset($myPrms["file_". $i."_rename"])&&!empty($myPrms["file_". $i."_rename"]))?stripExtension($myPrms["file_". $i."_rename"]):$myPrms["rename"] ;
						$ok=$myMat->ingest($filename,$makeMatPrms,$rename_tmp,$rename_file, $infos); // création du matériel.
						unset($rename_tmp);
					}
					if (!$ok) {//Echec création matériel
						trace('finUpload : echec materiel');
						displayError(kUploadMaterielEchec." : ".$myMat->error_msg);
					} else {
						displayMsg(kUploadMaterielSucces." : ".$myMat->t_mat['MAT_NOM']. " (" . filesize_format($myMat->t_mat['MAT_TAILLE']) . ")");
						// la variable $mats est la liste de materiels a traiter par la page jobListe.php 
						if ($import_dir) // import d'un repertoire
						{
							foreach ($myMat->getIdMatsEnfants() as $id_mat)
							{
								
								$tmp_mat=new Materiel();
								$tmp_mat->t_mat['ID_MAT']=$id_mat;
								$tmp_mat->getMat();
								//MS 16/09/13 - suppression filtrage sur le MAT_ID_MEDIA pour l'ajout aux matériels à traiter, 
								if((isset($myPrms['procChildrenFilter']) && in_array($tmp_mat->id_media,unserialize($myPrms["procChildrenFilter"])))){
									$mats[]['ID_MAT']=$id_mat;
								}
								
								unset($tmp_mat);
							}
						} elseif(isset($myPrms["id_proc_" . $i]) && !empty($myPrms["id_proc_" . $i])) {
                            $mats[]=array('ID_MAT'=>$myMat->t_mat['ID_MAT'], 'id_proc'=>$myPrms["id_proc_" . $i]);
                        } else {
                            $mats[]["ID_MAT"]=$myMat->t_mat['ID_MAT'];
                        }
                                 
						trace('finUpload : before makedoc');
						if ($myPrms['makedoc']) {
							trace('finUpload : makedoc');// l'utilisateur a demandé également la création doc
							
							// MS - 08.06.15 - afin de pouvoir préciser des infos propres à un seul document, 
							// on stocke les infos dans l'array file_$i_doc, et on les remonte au niveau de myPrms uniquement
							// pour le makedocfrommat du file_$i (file courant)
							if(isset($myPrms['file_'.$i.'_doc']) && is_array($myPrms['file_'.$i.'_doc'])){
								$makeDocPrms = array_merge($myPrms,$myPrms['file_'.$i.'_doc']);
							}else{
								$makeDocPrms = $myPrms;
							}
							
							//PARAM -> id_doc_i :relié au doc existant au lieu d en créer un nouveau par la methode makeDocFromMat
							if(!empty($myPrms["id_doc_".$i]) && $myPrms["id_doc_".$i] != '')
							{
								if($upload_to_sel){
									$myDoc = $myMat->makeDocFromMat($makeDocPrms,$infos,$id_doc2link,$myCartUpload,$myPrms["id_doc_".$i]);							
								}else{
									$myCartUpload=NULL;
									$myDoc = $myMat->makeDocFromMat($makeDocPrms,$infos,$id_doc2link,$myCartUpload,$myPrms["id_doc_".$i]);
								}							
							}
							else{
								if($upload_to_sel){
									$myDoc = $myMat->makeDocFromMat($makeDocPrms,$infos,$id_doc2link,$myCartUpload);							
								}else{
									$myDoc = $myMat->makeDocFromMat($makeDocPrms,$infos,$id_doc2link);
								}
							}
							if($myDoc && $import_dir && isset($myPrms["makeDocChildrenFilter"]) && is_array(unserialize($myPrms["makeDocChildrenFilter"])) 
							&& (strpos($myMat->t_mat['MAT_FORMAT'],'DCP')===false && strpos($myMat->t_mat['MAT_FORMAT'],'DPX')===false)){
								$id_doc_dir = $myDoc->t_doc['ID_DOC'];
								if(isset($myPrms['DOC_ID_TYPE_DOC'])){
									$type_parent = $myPrms['DOC_ID_TYPE_DOC'];
								}
								$backup_notes = ((isset($myPrms['DOC_NOTES']) && !empty($myPrms['DOC_NOTES']))?$myPrms['DOC_NOTES']:'');
								foreach ($aIdMatCreated as $id_mat)
								{
									$tmp_mat=new Materiel();
									$tmp_mat->t_mat['ID_MAT']=$id_mat;
									$tmp_mat->getMat();
									// MS - ajout conservation du MAT_CHEMIN_ORI dans la fiche documentaire pour des questions de recherche
									$makeDocPrms['DOC_NOTES'] = $tmp_mat->t_mat['MAT_CHEMIN_ORI'];
									
									// MS - type doc pour les fiches docs générés 
									if(isset($myPrms['CHILD_DOC_ID_TYPE_DOC']) && !empty($myPrms['CHILD_DOC_ID_TYPE_DOC'])){
										$makeDocPrms['DOC_ID_TYPE_DOC'] = $myPrms['CHILD_DOC_ID_TYPE_DOC'];
									}
									
									if( in_array($tmp_mat->id_media,unserialize($myPrms["makeDocChildrenFilter"]))){
										// MS : transférer le upload_to_sel ? pas forcément necessaire d'avoir toute l'arborescence dans la selection ... 
										$tmp_doc = $tmp_mat->makeDocFromMat($makeDocPrms,$infos,$id_doc_dir);
										trace('import '.$filename.'. New doc : '.$tmp_doc->t_doc['DOC_TITRE'].', '.$tmp_doc->t_doc['DOC_COTE'].', '.$tmp_doc->t_doc['ID_DOC']);
									}
									unset($tmp_mat);
								}
								if(isset($type_parent)){
									$myPrms['DOC_ID_TYPE_DOC'] = $type_parent;
								}
								$myPrms['DOC_NOTES'] = $backup_notes;
							}
						
						}
						if(strpos($myMat->t_mat['MAT_FORMAT'],'DCP')!==false){
							$myPrms['job_without_mat'] = "1";
							$myDoc->getDocMat();
							foreach($myMat->DCP_map as $file=>$props){
								if(isset($props['TYPE_DCP']) && ($props['TYPE_DCP'] == 'CPL' || $props['TYPE_DCP'] == 'CPL_unwrap' )){
									$myDoc->t_doc_mat[]=array(
										'ID_MAT'=>$props['ID_MAT'],
										'ID_DOC'=>$myDoc->t_doc['ID_DOC'],
										'DMAT_ID_LANG'=>$myDoc->t_doc['ID_LANG']
									);
								
									if(!isset($props['CONTENTTITLETEXT']) && empty($props['CONTENTTITLETEXT'])){
										$props['CONTENTTITLETEXT'] = $file;
									}
									$mats[]=array('ID_MAT'=>$props['ID_MAT'],
									'ID_DOC'=>$myDoc->t_doc['ID_DOC'],
									'MAT_COTE_ORI'=>$file,
									'FILE_NAME_MASK'=>array('rename'=>$props['CONTENTTITLETEXT']),
									'MAT_TITRE'=>$props['CONTENTTITLETEXT'],
									'MAT_NOTES'=>$props['ANNOTATIONTEXT'],
									'superJobId'=>$id_job_dcp
									);
								}
							}
							$myDoc->saveDocMat();
						}
												
						if(strpos($myMat->t_mat['MAT_FORMAT'],'DPX')!==false){
							$mats[]=array('ID_MAT'=>$myMat->t_mat['ID_MAT'],
							'ID_DOC'=>$myDoc->t_doc['ID_DOC']);
						}
							
						// trace("arr mats ".print_r($mats,true));
						if(php_sapi_name()!='cli' && !isset($_POST['noDisplay']))
							echo("<hr/>");

					}

	                    // VP 9/10/12 : Mise à jour Etat archivage
	                $myMat->updateEtatArch();
	                             
					unset($myMat);
					$numFile++;//@update VG 18/06/2010
				}
			}
		}
		
		if (isset($myPrms['rtn']) && !empty($myPrms['rtn'])) {
			$msg= "<script type=\"text/javascript\">
					if (window.opener && window.opener.".$myPrms['rtn'].") {
						window.opener.".$myPrms['rtn']."();
					}</script>";
			displayMsg($msg);
		}
	break;

		

case "batch_fcp" :
		require_once(modelDir.'model_materiel.php');
		require_once(modelDir.'model_doc.php');

		if(isset($myPrms['xmldata'])){
			$datas=xml2tab($myPrms['xmldata']);
			$docs=$datas['DATA'][0]['DOC'];
			foreach($docs as $idx=>$doc){
				// VP 29/07/09 : lecture timebase
				// VP 18/11/09 : traitement NTSC avec conversion de timebase
				$timebase=(empty($doc['TIMEBASE'])?25:$doc['TIMEBASE']);
				if($timebase=="30") $timebase=29.97;
				$id_mat=getFileFromPath(urldecode($doc["T_DOC_MAT"][0]["PATHURL"]));
				$tab=explode("/",urldecode($doc["T_DOC_MAT"][0]["PATHURL"]));
				
				if(!empty($myPrms["importFolder"])) $filename= $myPrms["importFolder"].$tab[count($tab)-1];
				else $filename= FCPWatchFolder.$tab[count($tab)-1];
				
				if(!file_exists($filename)){
					displayError( kUploadMaterielEchec. $filename);
				}
				$myMat= new Materiel;
				$myPrms["MAT_TITRE"] = $myPrms["file_" . $i."_original"];
				if(isset($myImp) && !empty($myImp->t_import['ID_IMPORT'])){
					$myPrms['MAT_ID_IMPORT'] =$myImp->t_import['ID_IMPORT'];
				}
				
				$ok=$myMat->ingest($filename,$myPrms,"",false, $infos); // création du matériel.
				if (!$ok) {//Echec création matériel
					displayError(kUploadMaterielEchec." : ".$myMat->error_msg);
				} else {
					displayMsg(kUploadMaterielSucces.$myMat->t_mat['ID_MAT']);
					$mats[]["ID_MAT"]=$myMat->t_mat['ID_MAT'];
					
					//$myMat->t_mat['MAT_TCIN']=$doc["t_doc_mat"]["timecode"];
					$myMat->t_mat['MAT_TCIN']=frameToTC($doc["T_DOC_MAT"][0]["IN"],25,$timebase);
					$myMat->t_mat['MAT_TCOUT']=frameToTC($doc["T_DOC_MAT"][0]["OUT"],25,$timebase);
					$myMat->t_mat['MAT_COTE_ORI']=$doc["T_DOC_MAT"][0]["MAT_COTE_ORI"];
					$myMat->save();
					
					if ($myPrms['makedoc']) { // l'utilisateur a demandé également la création doc
						displayMsg( kNouveauDoc."...");

						$myDoc=new Doc;
						 //Maj par le fichier fcp
						$myDoc->mapFields($doc);
						if($doc["IN"]!=-1 && $doc["OUT"]!=-1) $myDoc->t_doc['DOC_DUREE']=secToTime(round(($doc['OUT']-$doc['IN'])/$timebase));
						else $myDoc->t_doc['DOC_DUREE']=secToTime(round($doc['DURATION']/$timebase));
						$myDoc->t_doc['DOC_ID_MEDIA']=$myMat->id_media;
						 //Maj par le formulaire d'upload
						$myDoc->mapFields($myPrms);

						//debug($myDoc);
						$okDoc=true;
						if(!$myDoc->checkExist()){
							$okDoc=$myDoc->save();
							if ($okDoc) {
								$myDoc->saveDocLex();
								$myDoc->saveDocVal();
								$myDoc->updateChampsXML('DOC_LEX');
								$myDoc->updateChampsXML('DOC_VAL');
								$myDoc->updateChampsXML('DOC_XML');
								$myDoc->updateChampsXML('DOC_CAT');
							}
						}
						if ($okDoc) {
							
							$myDoc->metAJourDocNum($myMat->t_mat['ID_MAT']);

							$myDoc->t_doc_mat[0]['ID_MAT']=$myMat->t_mat['ID_MAT']; //lien doc_mat
							$myDoc->t_doc_mat[0]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
							$myDoc->t_doc_mat[0]['MAT_FORMAT']=$myMat->t_mat['MAT_FORMAT'];
							$myDoc->t_doc_mat[0]['DMAT_TCIN']=($doc["IN"]==-1?$myMat->t_mat['MAT_TCIN']:frameToTC($doc["IN"],25,$timebase));
							$myDoc->t_doc_mat[0]['DMAT_TCOUT']=($doc["OUT"]==-1?$myMat->t_mat['MAT_TCOUT']:frameToTC($doc["OUT"],25,$timebase));

							$okDoc=$myDoc->saveDocMat();
							if ($okDoc) {
								displayMsg(kSuccesDocSauve);
								// Séquences
								$myDoc->t_doc_seq=$doc["T_DOC_SEQ"];
								foreach ($myDoc->t_doc_seq as $idx=>$seq) {
									$seq['DOC_TCIN']=(($seq["IN"]==-1)?$myMat->t_mat['MAT_TCIN']:frameToTC($seq["IN"],25,$timebase));
									if($seq["OUT"]!=-1) $seq['DOC_TCOUT']=frameToTC($seq["OUT"],25,$timebase);
									else if($idx<count($myDoc->t_doc_seq)-1) $seq['DOC_TCOUT']=frameToTC($myDoc->t_doc_seq[$idx + 1]["IN"] - 1,25,$timebase);
									else $seq['DOC_TCOUT']= $myMat->t_mat['MAT_TCOUT'];
									 //Maj par le fichier fcp
									$mySeq=new Doc;
									 //Maj par le fichier fcp
									$mySeq->updateFromArray($seq);
									$mySeq->t_doc['DOC_ID_FONDS']= $myDoc->t_doc['DOC_ID_FONDS'];
									$mySeq->t_doc['DOC_ACCES']=$myDoc->t_doc['DOC_ACCES'];
									$mySeq->t_doc['DOC_ID_MEDIA']=$myDoc->t_doc['DOC_ID_MEDIA'];
									$mySeq->t_doc['DOC_ID_GEN']=$myDoc->t_doc['ID_DOC'];
									$mySeq->t_doc['DOC_DUREE']=secToTime(tcToSec(trim($seq["DOC_TCOUT"]))-tcToSec(trim($seq["DOC_TCIN"])));

									//debug($mySeq);
									$okSeq=$mySeq->save();
									if ($okSeq) {
										displayMsg(kSuccesDocSauve);
										// S�lection image repr�sentative à mettre dans processFile
										if ($okSB) {
											// Selection imageur
											require_once(modelDir.'model_imageur.php');
											$myImageur = New Imageur();
											$myImageur->t_imageur['ID_IMAGEUR']=$myMat->t_mat['MAT_ID_IMAGEUR'];
											$okGrab=$myImageur->grabImage($myMat,frameToTC(tcToFrame($seq['DOC_TCIN'],25,$timebase)+gStoryboardGrabOffset),$imgFromStoryboard,320,240);
											if(!empty($imgFromStoryboard))$mySeq->saveVignette('doc_id_image',$imgFromStoryboard);
											unset($myImageur);
										}
										$mySeq->metAJourDocNum($myMat->t_mat['ID_MAT']);

										$mySeq->t_doc_mat[0]['ID_MAT']=$myMat->t_mat['ID_MAT']; //lien doc_mat
										$mySeq->t_doc_mat[0]['ID_DOC']=$mySeq->t_doc['ID_DOC'];
										$mySeq->t_doc_mat[0]['MAT_FORMAT']=$myMat->t_mat['MAT_FORMAT'];
										$mySeq->t_doc_mat[0]['DMAT_TCIN']=$seq['DOC_TCIN'];
										$mySeq->t_doc_mat[0]['DMAT_TCOUT']=$seq['DOC_TCOUT'];
										$okSeq=$mySeq->saveDocMat();
										
									}
								}
								if(!okSeq) displayError(kErrorSauveDocSeq." : ".$mySeq->error_msg);
							}
							else {
								displayError(kErrorSauveDocMat." : ".$myDoc->error_msg);
							}
						}
						else {
							displayError(kErrorSauveDoc." : ".$myDoc->error_msg);
						}
						unset($myDoc);
					}
				}
				unset($myMat);
			}// end foreach
		}

	break;


	case "batch_img" :
		require_once(modelDir.'model_imageur.php');

		for($i=0; $i<count($myPrms); $i++) {
			if(isset($myPrms["file_". $i])) {
			if (empty($myPrms["file_". $i])) displayMsg( "<div class='error'>".$myPrms["file_". $i."_status"]."</div>");
				  else {
					$myImage= new Image(urldecode($myPrms['IMAGEUR']));
					$myImage->t_image['IMAGEUR']=urldecode($myPrms['IMAGEUR']);

					//$myImage->t_image['ID_IMAGE']= NULL;
					//trace( stripAccents($_POST["filename". $i]));
					$myImage->t_image['IM_CHEMIN']= getFileFromPath($myPrms["file_". $i]);
					//trace(print_r($myImage,true));
					$imgExists=$myImage->checkExist();
					if (!$imgExists) { //image n'existe pas : on la crée
						$myImage->t_image['ID_IMAGE']=null;
						$ok=$myImage->save(); //Création image dans la langue
						$ok=$myImage->saveVersions($_SESSION['arrLangues'],$ok); //Sauvegarde pour les autres langues
					} else $ok=true; //image existe, on ne sauve rien, on a juste maj le fichier
					displayMsg( kUploadFileSucces. $myImage->t_image['IM_CHEMIN']. " (" . filesize_format($myPrms["file_" . $i."_size"]) . ")");
					if ($ok) displayMsg(kUploadImageSucces); else displayMsg(kUploadImageEchec);

					displayMsg("<script type=\"text/javascript\">
					if (window.opener
						&& (window.opener.location.href.indexOf('imageur') !=-1
							|| window.opener.location.href.indexOf('Imageur') !=-1
							)) {

						if (window.opener.document.form1) { //save & refresh de la page appelante
							window.opener.document.form1.submit();
							}
						}</script>
					<hr/>");

					unset($myImage);
				}
			}
		}

	break;

}


if(!isset($_POST['noDisplay']) 
	&& file_exists(getSiteFile("designDir","form/finUpload.inc.php")) 
	&& strpos(getSiteFile("designDir","form/finUpload.inc.php"), 'index.php') === false){
	include(getSiteFile("designDir","form/finUpload.inc.php"));
}

if($myPrms['lancement']=="proc"){
	include(includeDir."jobListe.php");
}else{
	include(upload_scriptDir."processFiles.php");

	if ($myPrms['send_mail']=='1' && defined('gMailUpload')) {
        trace("finUpload : send a mail");
		$sujet=kUploadSujetMailFinUpload;
		//$mailContent=mb_convert_encoding($_SESSION['finUploadMsg'],'ISO-8859-1','UTF-8');
		$mailContent=$_SESSION['finUploadMsg'];
		$sujet=mb_convert_encoding($sujet,'ISO-8859-1','UTF-8');
		//$headers="From:".gMail."\r\nReturn-Path:".gMail."\r\nX-Sender:OPSIS\r\nX-Mailer:PHP\r\nMIME-Version: 1.0\r\nContent-Type: text/html";
		mb_language('en'); // equiv ISO LATIN 1
		include_once(modelDir.'model_message.php');
		$msg = new Message();
		$msg->send_mail(gMailUpload, gMail, $sujet, $mailContent, "text/html");
		if (!empty($myPrms['other_mail']))	{
			$msg = new Message();
			$msg->send_mail($myPrms['other_mail'], gMail, $sujet, $mailContent, "text/html");
		}
	}
	
}

?>
