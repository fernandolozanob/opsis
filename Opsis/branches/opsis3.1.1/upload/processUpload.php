<?php
trace("processUpload : begin");
include(libDir."class_javaUpload.php");
header("Content-Range : bytes");
$myUpd=new JavaUpload();


$type=urldecode($_REQUEST['type']);


//trace(print_r($_FILES,true));

switch ($type) {
	
	case 'doc_acc' : $upload_dir=kDocumentDir; break;
	case 'mat' : $upload_dir=kVideosDir; break;
	case 'doc_mat' : $upload_dir=kVideosDir; break;
	case 'batch_mat' : $upload_dir=kVideosDir; break;
	case 'batch_img' : $upload_dir=kStoryboardDir.urldecode($_REQUEST['id']).'/';break;
	default: $upload_dir=kDocumentDir; break;

}

$max_size = 315000000000; //315Go !
$createsubfolders = "false";
$keepalive = "false";
error_reporting(0);
$message ="";
	
if (!is_dir($upload_dir))
{
  if (!JavaUpload::recursiveMkdir($upload_dir)) die ("cannot access upload directory");
  if (!chmod($upload_dir,0755)) die ("change permission to 755 failed.");
}

if ($_POST['todo']=="upload")
{
  $account = "";
  if (isset($_POST['account']) && !empty($account))
  {
    $account = $_POST['account'];
    if (substr($account,0,1) != "/") $account = "/".$account;
  }
  $target_folder=$upload_dir.$account;
  // relalivefilename support for folders and subfolders creation.
  $relative = $_POST['relativefilename'];
  if(get_magic_quotes_gpc()) $relative = stripslashes($relative);
  if (($createsubfolders == "true") && ($relative != ""))
  {
	$file_name = $_FILES['uploadfile']['name'];
	if(get_magic_quotes_gpc()) $file_name = stripslashes($file_name);
	$inda=strlen($relative);
	$indb=strlen($file_name);
	if (($indb > 0) && ($inda > $indb))
	{
		$subfolder = substr($relative,0,($inda-$indb)-1);
        $subfolder = str_replace("\\","/",$subfolder);
        $target_folder = $upload_dir.$account."/".$subfolder;
        JavaUpload::recursiveMkdir($target_folder);
    }
  }
	
  if ($_FILES['uploadfile'])
  {
    if ($keepalive == "false")
    {
       header("Connection: close");
    }
    $message = JavaUpload::do_upload($target_folder,$max_size,$_POST);
    // trace(print_r($_POST,true));
    // Recompose file from chunks (if any).
    $chunkid = $_POST['chunkid'];
    $chunkamount = $_POST['chunkamount'];
    $chunkbaseStr = $_POST['chunkbase'];
    if(get_magic_quotes_gpc()) $chunkbaseStr = stripslashes($chunkbaseStr);
    
    //if ($chunkid != "") header("Connection: close");
    
    if (($chunkid != "") && ($chunkamount != "") && ($chunkbaseStr != ""))
    {
		if ($chunkid == $chunkamount)
        {
			// recompose file.
						
			$fname =$chunkbaseStr;
		    $fname=removeTrickyChars(stripAccents($chunkbaseStr));
	
			//Vérif doublon selon param site
			if (defined("gUploadModeRewrite") && stripos(gUploadModeRewrite,"rename")!==false) { //Renommage demandé ?
				$i='';		
				while (file_exists($target_folder."/".stripExtension($fname).$i.'.'.getExtension($fname))) $i++; //Vérif existence fichier et incrément si vrai
				$new_file_name=stripExtension($fname).$i.'.'.getExtension($fname); //On tient un nom libre !
			} else {
				$new_file_name=$fname; //mode overwrite
			}
			
		   
		    
		    
		    if ($new_file_name!=$fname) {
			    $f=fopen($target_folder."/".$fname.".txt","wb"); //on stocke le nv nom ds un fichier
			    fputs($f,$chunkbaseStr.'>>>'.$new_file_name);
			    fclose($f);
		    }	
			
			// VP 14/12/09 : changement concaténation (ATTENTION UNIX ONLY)
			if (!strpos(PHP_OS,'WINNT')) { //OSX / UNIX : utilisation de cat
			$fileout = $target_folder."/".$new_file_name;
            for ($c=1;$c<=$chunkamount;$c++)
			{
				$filein = $target_folder."/".$chunkbaseStr.".".$c;
				// VP 8/12/10 : ajout de "" autour du fichier dans commande cat
				print "cat \"".$filein."\" >> \"".$fileout."\"";
				exec("cat \"".$filein."\" >> \"".$fileout."\"");
			    unlink($filein);
			}
			}else{
				// Ne marche pas pour fichiers > 2Go
				$fout = fopen ($target_folder."/".$new_file_name, "wb");
				for ($c=1;$c<=$chunkamount;$c++)
				{
					$filein = $target_folder."/".$chunkbaseStr.".".$c;
					$fin = fopen ($filein, "rb");
					while (!feof($fin))
					{
						$read = fread($fin,4096);
						fwrite($fout,$read);
					}
					fclose($fin);
					unlink($filein);
				}
				fclose($fout);
			}
		}
     }
  }
  else
  {
     $emptydirectory = $_POST['emptydirectory'];
     if ($emptydirectory != "")
     {
         JavaUpload::recursiveMkdir($upload_dir.$account."/".$emptydirectory);
     }
     $message = "No uploaded file(s).";
  }
}

?>