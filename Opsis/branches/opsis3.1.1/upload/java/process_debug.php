<?php

$upload_dir = "/usr/local/www/jclient19/uploads";
$max_size = 4000000000;
$createsubfolders = "true";
$keepalive = "false";
$debug=true;

$REQUEST_METHOD=$_SERVER["REQUEST_METHOD"];
//if ($debug)
//{
  $lg = fopen($upload_dir."/process_debug.log", "a");
  fwrite($lg,"----- ".date("D M j G:i:s T Y")." -----\r\n");
  fwrite($lg,"PHP.ini setup:".phpversion()."\r\n");
  fwrite($lg," Free space for ".$upload_dir.":".diskfreespace($upload_dir)."\r\n");
  fwrite($lg," file_uploads:".ini_get('file_uploads')."\r\n");
  fwrite($lg," upload_max_filesize:".ini_get('upload_max_filesize')." (script max_size:".$max_size.")\r\n");
  fwrite($lg," post_max_size:".ini_get('post_max_size')."\r\n");
  fwrite($lg," max_execution_time:".ini_get('max_execution_time')."\r\n");
  fwrite($lg," memory_limit:".ini_get('memory_limit')."\r\n");
  fwrite($lg," magic_quotes_gpc:".ini_get('magic_quotes_gpc')."\r\n");
  fwrite($lg,"Method dump:".$REQUEST_METHOD."\r\n");
  fwrite($lg,"Headers dump:"."\r\n");
  $headers = emu_getallheaders();
  while (list ($header, $value) = each ($headers))
  {
    fwrite($lg," ".$header."=".$value."\r\n");
  }
  fwrite($lg,"GET dump:"."\r\n");
  while (list ($header, $value) = each ($_GET))
  {
    fwrite($lg," ".$header."=".$value."\r\n");
  }
  fwrite($lg,"POST dump:"."\r\n");
  while (list ($header, $value) = each ($_POST))
  {
    fwrite($lg," ".$header."=".$value."\r\n");
  }

//}

error_reporting(0);
$message ="";
if (!is_dir($upload_dir))
{
  if (!recursiveMkdir($upload_dir)) die ("cannot access upload directory");
  if (!chmod($upload_dir,0755)) die ("change permission to 755 failed.");
}

if ($debug) fwrite($lg,"Processing request"."\r\n");

if ($_POST['todo']=="upload")
{
  $account = "";
  if (isset($_POST['account']))
  {
    $account = $_POST['account'];
    if (substr($account,0,1) != "/") $account = "/".$account;
  }
  $target_folder=$upload_dir.$account;
  // relalivefilename support for folders and subfolders creation.
  $relative = $_POST['relativefilename'];
  if(get_magic_quotes_gpc()) $relative = stripslashes($relative);
  if (($createsubfolders == "true") && ($relative != ""))
  {
	$file_name = $_FILES['uploadfile']['name'];
	if(get_magic_quotes_gpc()) $file_name = stripslashes($file_name);
	$inda=strlen($relative);
	$indb=strlen($file_name);
	if (($indb > 0) && ($inda > $indb))
	{
		$subfolder = substr($relative,0,($inda-$indb)-1);
        $subfolder = str_replace("\\","/",$subfolder);
        $target_folder = $upload_dir.$account."/".$subfolder;
        if ($debug) fwrite($lg,"Creating directory:".$target_folder."\r\n");
        recursiveMkdir($target_folder);
    }
  }
  if ($_FILES['uploadfile'])
  {
    if ($keepalive == "false")
    {
       header("Connection: close");
    }
    $message = do_upload($target_folder,$max_size);
    if ($debug) fwrite($lg,"do_upload completed:".$message."\r\n");
    // Recompose file from chunks (if any).
    $chunkid = $_POST['chunkid'];
    $chunkamount = $_POST['chunkamount'];
    $chunkbaseStr = $_POST['chunkbase'];
    if(get_magic_quotes_gpc()) $chunkbaseStr = stripslashes($chunkbaseStr);
    if (($chunkid != "") && ($chunkamount != "") && ($chunkbaseStr != ""))
    {
		if ($chunkid == $chunkamount)
        {
			// recompose file.
			if ($debug) fwrite($lg,"Recomposing file from chunks"."\r\n");
			$fname = $target_folder."/".$chunkbaseStr;
			if (file_exists($fname)) $fname = $fname.".".time();
			if ($debug) fwrite($lg," Creating:".$fname."\r\n");
			$fout = fopen ($fname, "wb");
            for ($c=1;$c<=$chunkamount;$c++)
			{
				$filein = $target_folder."/".$chunkbaseStr.".".$c;
				if ($debug) fwrite($lg," Opening:".$filein."\r\n");
				$fin = fopen ($filein, "rb");
			    while (!feof($fin))
			    {
			      $read = fread($fin,8192);
			      fwrite($fout,$read);
			    }
			    fclose($fin);
			    if ($debug) fwrite($lg," Deleting:".$filein."\r\n");
			    unlink($filein);
			}
			fclose($fout);
			if ($debug) fwrite($lg,"Recomposition completed"."\r\n");
        }
     }
  }
  else
  {
     $emptydirectory = $_POST['emptydirectory'];
     if ($emptydirectory != "")
     {
         recursiveMkdir($upload_dir.$account."/".$emptydirectory);
     }
     $message = "No uploaded file(s).";
  }
}

function do_upload($upload_dir,$max_size)
{
    global $lg, $debug;
    if ($debug) fwrite($lg," do_upload ".$upload_dir."\r\n");
    $temp_name = $_FILES['uploadfile']['tmp_name'];
    $file_name = $_FILES['uploadfile']['name'];
    $file_size = $_FILES['uploadfile']['size'];
    $file_type = $_FILES['uploadfile']['type'];
    if ($debug)
    {
       fwrite($lg," temp_name:".$temp_name."\r\n");
       fwrite($lg," file_name:".$file_name."\r\n");
       fwrite($lg," file_size:".$file_size."\r\n");
       fwrite($lg," file_type:".$file_type."\r\n");
    }
    if(get_magic_quotes_gpc()) $file_name = stripslashes($file_name);
    //$file_name = str_replace("\\","/",$file_name);
    $file_path = $upload_dir."/".$file_name;

    // Check filename.
    if ($file_name =="")
    {
  	  $message = "Error - Invalid filename";
  	  return $message;
    }

    // Check file size.
    if ($file_size > $max_size)
    {
      $errormsg = "- File size is over ".$max_size." bytes";
      header("HTTP/1.1 405");
      header("custommessage: ".$errormsg);
      $message = "Error ".$errormsg;
      return $message;
    }

    if ($debug) fwrite($lg," move_uploaded_file:".$temp_name." to ".$file_path."\r\n");
    $result = move_uploaded_file($temp_name, $file_path);
    if ($debug) fwrite($lg," move_uploaded_file:".$result."\r\n");
    if ($result)
    {
      chmod($file_path,0755);
      $message = "$file_name uploaded successfully.";
      return $message;
    }
    else
    {
      $errormsg = "- PHP upload failed";
      header("HTTP/1.1 405");
      header("custommessage: ".$errormsg);
      $message = "Error ".$errormsg;
      return $message;
    }
}

function recursiveMkdir($path)
{
	if (!file_exists($path))
    {
		recursiveMkdir(dirname($path));
        return mkdir($path, 0755);
    }
    else return true;
}

function emu_getallheaders()
{
   foreach($_SERVER as $h=>$v)
       if(ereg('HTTP_(.+)',$h,$hp))
           $headers[$hp[1]]=$v;
   return $headers;
}

?>

<html>
<head>
<title>Upload file : PHPScript sample</title>
</head>
<body>
<center>
   <br>
   <? echo $message ?>
  <form action="" method="post" ENCTYPE="multipart/form-data" name="upload" id="upload">
    Select file to upload :
    <input type="hidden" name="todo" value="upload">
    <input type="file" name="uploadfile">
    <input type="submit" name="upload" value="Upload">
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p align="center">&nbsp;</p>
  <p align="center">&nbsp;</p>
  <p align="center"><font size="-1" face="Courier New, Courier, mono">Copyright
    &copy; <a href="http://www.javazoom.net" target="_blank">JavaZOOM</a> 1999-2007</font></p>
   </form>
</center>
</body>
</html>
