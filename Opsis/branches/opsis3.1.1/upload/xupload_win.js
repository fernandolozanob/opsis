function $(elem){return document.getElementById(elem);}

function getScrollXY()
{
  if( typeof( window.pageYOffset ) == 'number1' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
}

//---------------------
var show_fname_chars=50;
var UID,NF=0,cx=0,slots=1,fnames,scrOfX,scrOfY;
var FF;
function openStatusWindow(f1)
{
 var pbmode;
 f1=FF;
 for(var i=0; i<f1.pbmode.length; i++)
 {
   if(f1.pbmode[i].checked)pbmode = f1.pbmode[i].value;
 }
 if(f1.pbmode.length == undefined)pbmode=f1.pbmode.value;

 var cgi_url = form_action.split('upload_win.cgi')[0]+'upload_status_win.cgi';
 var url = cgi_url+'?uid='+UID+'&nfiles='+NF+'&css='+f1.css_name.value+'&tmpl='+f1.tmpl_name.value+'&xmode='+f1.xmode.value+'&files='+fnames;

 if(pbmode=='popup')
 {
   var win1 = window.open(url,"","width=390,height=320,resizable=1,status=0");
   win1.focus();
 }
 if(pbmode=='inline')
 {
   showLayer();
   self.transfer.document.location = url+'&inline=1';
 }
 if(pbmode=='inline2')
 {
   xy = findPos( $('div1') );
   var p2  = $('progress2');
   var p2f = $('progress2f');
   p2.style.left = 1+parseInt(xy[0])+'px';
   p2.style.top = xy[1]+'px';
   p2f.style.height = $('div1').clientHeight+'px';
   p2f.style.width = $('div1').clientWidth + 'px';
   p2.style.height = p2f.style.height;
   p2.style.width  = p2f.style.width;
   self.transfer2.document.location = url+'&inline=1';
 }
 if(pbmode=='inline3')
 {
     if($('progress_bar'))$('progress_bar').style.display='block';
     if($('stop_btn'))$('stop_btn').style.visibility='visible';
     self.transfer2.document.location = url+'&inline_pro=1';
     if($('upload_form'))
     {
         $('upload_form').style.position='absolute';
         $('upload_form').style.left='-9999px';
     }
 }
}

function generateSID(f1)
{
 UID='';
 for(var i=0;i<12;i++)UID+=''+Math.floor(Math.random() * 10);
}

function StartUpload(f1)
{
    NF=0;
    f1.target='xupload';

    // Check that external folder don't contain '..'
    if(f1.ext_folder && f1.ext_folder.value.indexOf('..')!=-1){alert("Invalid upload folder!");return false;}

    for (var i=0;i<f1.length;i++)
    {
     current = f1.elements[i];
     if(current.type=='file' && current.value!='')
      {
         if(!checkExt(current))return false;
         NF++;
      }
    }
    cx=0;
    fnames='';
    for (var i=0;i<=f1.length;i++)
    {
      current = f1[i];
      if(current && current.type && current.type=='file')
      {
         var descr = $(current.name+'_descr');
         if(descr)descr.name = 'file_'+cx+'_descr';
         current.name = 'file_'+cx;
         cx++;
         name = current.value.match(/[^\\\/]+$/);
         if(name && name!='null')fnames+=':'+name;
      }
    }

    if(NF==0){alert('Select at least one file to upload');return false;};
    if(pass_required && f1.upload_password && f1.upload_password.value==''){alert("Password required");return false;}
    if(email_required && f1.email_notification && !f1.email_notification.value.match(/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i)){alert("Valid email address required");return false;}
    if($('submit_btn'))$('submit_btn').style.visibility='hidden';
    generateSID(f1);

    FF=f1;
    setTimeout("openStatusWindow()",500);

    var password='';
    if(f1.upload_password)password=f1.upload_password.value;
    form_action = form_action.split('?')[0]+'?upload_id='+UID+'&js_on=1&xpass='+b64_md5(password)+'&xmode='+f1.xmode.value; //cleaning old query to avoid ReUpload bugs
    setFormAction(f1,form_action);
    f1.action=form_action;
}

function checkExt(obj)
{
    value = obj.value;
    if(value=="")return true;
    var re1 = new RegExp("^.+\.("+ext_allowed+")$","i");
    var re2 = new RegExp("^.+\.("+ext_not_allowed+")$","i");
    if( (ext_allowed && !re1.test(value)) || (ext_not_allowed && re2.test(value)) )
    {
        str='';
        if(ext_allowed)str+="\nOnly these extensions are allowed: "+ext_allowed.replace(/\|/g,',');
	if(ext_not_allowed)str+="\nThese extensions are not allowed:"+ext_not_allowed.replace(/\|/g,',');
        alert("Extension not allowed for file: \"" + value + '"'+str);
        return false;
    }

    return true;
}

function addUploadSlot()
{
  cx++;
  slots++;

  //if(slots>=max_upload_files){$('x_add_slot').style.visibility='hidden';}

  var new_slot = document.createElement( 'input' );
  new_slot.type = 'file';
  new_slot.name = 'file_'+cx;
  new_slot.size = 40;
  new_slot.onchange=addUploadSlot;
  $('slots').appendChild(new_slot);

  slot_trash=document.createElement('img');
  slot_trash.src='./design/images/button_drop.gif'
  slot_trash.style.cursor='pointer';
  slot_trash.style.display='inline';
  slot_trash.onclick=function () {this.parentNode.removeChild(this.previousSibling);
  								this.parentNode.removeChild(this);
  								}
  $('slots').appendChild(slot_trash);
  $('slots').appendChild( document.createElement('br') );
}


function fixLength(str)
{
 if(str.length<show_fname_chars)return str;
 return '...'+str.substring(str.length-show_fname_chars-1,str.length);
}

function MultiSelector( list_target, max_files, max_size, descr_mode )
{
	this.list_target = $(list_target);
	this.count = 0;
	this.id = 0;
	if( max_files ){
		this.max = max_files;
	} else {
		this.max = -1;
	};
	$('x_max_files').innerHTML = max_files;
    $('x_max_size').innerHTML = " ("+max_size+" Mb total)";
	this.addElement = function( element )
    {

		if(element!=null) {
		if(element.tagName == 'INPUT' && element.type == 'file' )
        {
           element.name = 'file_' + this.id++;
           element.multi_selector = this;
           element.onchange = function()
           {
               if(!checkExt(element))return;
               if (navigator.appVersion.indexOf("Mac")>0 && navigator.appVersion.indexOf("MSIE")>0)return;
               var new_element = document.createElement( 'input' );
               new_element.type = 'file';

               //this.parentNode.insertBefore( new_element, this );
		this.parentNode.appendChild( new_element, this );
               this.multi_selector.addElement( new_element );
               this.multi_selector.addListRow( this );

               // Hide this: we can't use display:none because Safari doesn't like it
               this.style.position = 'absolute';
               this.style.left = '-1000px';
           };
           // If we've reached maximum number, disable input element
           if( this.max != -1 && this.count >= this.max ){element.disabled = true;};

           this.count++;
           this.current_element = element;
		}
        else {alert( 'Error: not a file input element' );};
        }
	};

	this.addListRow = function( element )
    {
		var new_row = document.createElement( 'div' );

		var new_row_button = document.createElement( 'input' );
		new_row_button.type = 'button';
		new_row_button.value = 'Delete';

		new_row.element = element;

		new_row_button.onclick= function()
        {
			this.parentNode.element.parentNode.removeChild( this.parentNode.element );
			this.parentNode.parentNode.removeChild( this.parentNode );
			this.parentNode.element.multi_selector.count--;
			this.parentNode.element.multi_selector.current_element.disabled = false;
			return false;
		};

		new_row.appendChild( new_row_button );

		currenttext=document.createTextNode(" "+fixLength(element.value));
                var span1 = document.createElement( 'span' );
                span1.className = 'xfname';
                span1.appendChild( currenttext );

        new_row.appendChild( span1 );

        if(descr_mode)
        {
            var new_row_descr = document.createElement( 'input' );
    		new_row_descr.type = 'text';
    		new_row_descr.value = '';
            new_row_descr.name = element.name+'_descr';
            new_row_descr.className='fdescr';
            new_row_descr.setAttribute('id',element.name+'_descr');
            new_row_descr.setAttribute('maxlength', 32);
            new_row.appendChild( document.createElement('br') );
            var span2 = document.createElement( 'span' );
                span2.className = 'xdescr';
                span2.appendChild( document.createTextNode('Description:') );
            new_row.appendChild( span2 );
            new_row.appendChild( new_row_descr );
        }


		this.list_target.appendChild( new_row );
	};
};

//--MD5 part ----
var b64pad  = "";
var chrsz   = 8;
function b64_md5(s){ return binl2b64(core_md5(str2binl(s), s.length * chrsz));}
function core_md5(x, len)
{
x[len >> 5] |= 0x80 << ((len) % 32);
x[(((len + 64) >>> 9) << 4) + 14] = len;
var a =  1732584193;
var b = -271733879;
var c = -1732584194;
var d =  271733878;
for(var i = 0; i < x.length; i += 16)
{
var olda = a;
var oldb = b;
var oldc = c;
var oldd = d;
a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);
a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);
a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);
a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);
a = safe_add(a, olda);
b = safe_add(b, oldb);
c = safe_add(c, oldc);
d = safe_add(d, oldd);
}
return Array(a, b, c, d);
}
function md5_cmn(q, a, b, x, s, t){return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);}
function md5_ff(a, b, c, d, x, s, t){return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);}
function md5_gg(a, b, c, d, x, s, t){return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);}
function md5_hh(a, b, c, d, x, s, t){return md5_cmn(b ^ c ^ d, a, b, x, s, t);}
function md5_ii(a, b, c, d, x, s, t){return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);}
function safe_add(x, y){var lsw = (x & 0xFFFF) + (y & 0xFFFF);var msw = (x >> 16) + (y >> 16) + (lsw >> 16);return (msw << 16) | (lsw & 0xFFFF);}
function bit_rol(num, cnt){return (num << cnt) | (num >>> (32 - cnt));}
function str2binl(str)
{
  var bin = Array();
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < str.length * chrsz; i += chrsz)
    bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);
  return bin;
}
function binl2b64(binarray)
{
  var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i += 3)
  {
    var triplet = (((binarray[i   >> 2] >> 8 * ( i   %4)) & 0xFF) << 16)
                | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 )
                |  ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
    for(var j = 0; j < 4; j++)
    {
      if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
      else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
    }
  }
  return str;
}

function fixIE(height,over)
{
    bod = document.getElementsByTagName('body')[0];
    bod.style.height = height;
    bod.style.overflow = over;

    htm = document.getElementsByTagName('html')[0];
    htm.style.height = height;
    htm.style.overflow = over;
}

function showLayer()
{
    getScrollXY()
    fixIE('100%','hidden');
    selects = document.getElementsByTagName('select');
    for(i = 0; i < selects.length; i++){ selects[i].style.visibility='hidden'; }
    window.scrollTo(0,0);
    $('overlay').style.left = '0px';
    $('lightbox').style.left = '50%';
}

function hideLayer()
{
    if($('overlay'))$('overlay').style.left = '-9999px';
    if($('lightbox'))$('lightbox').style.left = '-9999px';
    fixIE("auto", "auto");
    selects = document.getElementsByTagName('select');
    for(i = 0; i < selects.length; i++){ selects[i].style.visibility='visible'; }
    window.scrollTo(scrOfX,scrOfY);
}

function getFormAction(f)
{
   return f.getAttribute('action'); //remplac� par LD en getAttribute, plus fiable et plus universel
}

function setFormAction(f,val)
{
	f.setAttribute('action',val); //remplac� par LD en setAttribute, plus fiable et plus universel

}

function InitUploadSelector()
{
    if($('files_list'))
    {
        var multi_selector = new MultiSelector( 'files_list', max_upload_files, max_upload_size, descr_mode );
        multi_selector.addElement( $( 'my_file_element' ) );
    }
}

//--Initialization------
var form_action;
form_action = getFormAction(document.F1Upload);
document.write('<script src="' + form_action +'&mode=settings' + '&xmode=' +document.F1Upload.xmode.value+ '" type="text/javascript"><\/script>');
document.write('<div id="overlay"></div><div id="lightbox"><div id="lhdr">Upload in progress...<img id="close" src="UPLOAD2/btn-close.gif" onclick="if(window.transfer.StopUpload)window.transfer.StopUpload();" title="Close this window" /></div><iframe src="UPLOAD2/blank.html" name="transfer" border=0 SCROLLING=NO topmargin=0 leftmargin=0 frameborder=0 style="width: 390px; height: 320px;"></iframe></div>');
document.write('<div id="progress2" style="position: absolute; left: -9999px;top:-9999px"><iframe src="UPLOAD2/blank.html" id="progress2f" name="transfer2" border=0 SCROLLING=NO topmargin=0 leftmargin=0 frameborder=0 style="width: 395px; height: 260px;"></iframe></div>');

