<?php
// TESTS : indexPopupUpload.php?urlaction=processFiles
//	$mats=array("newsfromkorea.mp4");
//	$myPrms=array("makeStoryboard"=>1,"frameInterval"=>10,"frame_width"=>320,"frame_height"=>240,"transcode_add"=>1,"transcode_preset"=>"MP3");
	
	// Construction structure jobs
	foreach($mats as $mat){
        // VP 4/07/13 : possibilité d'appeler makePDF ppour les doc_acc (on ajoute type=doc_acc aux paramètres
		if(isset($mat["ID_MAT"])) $id_mat=$mat["ID_MAT"];
        elseif(isset($mat["ID_DOC_ACC"])) $id_mat=$mat["ID_DOC_ACC"]; 
		//Attention, bien garder la génération du PDF avant la génération du storyboard !!!
		//C'est en effet à partir de ce format que l'on génère le storyboard de certains documents
		//update VG 01/03/11 : ajout gestion param "makePDF"
		if ($myPrms['makePDF']){
			$job_nom=kConversionPDF." ".$id_mat;
            if(isset($mat["ID_MAT"])) $jobs[]=array("job"=>"makePDF","job_nom"=>$job_nom,"job_in"=>$mat,"format"=>"pdf");
            elseif(isset($mat["ID_DOC_ACC"])) $jobs[]=array("job"=>"makePDF","job_nom"=>$job_nom,"job_in"=>$mat,"format"=>"pdf","type"=>"doc_acc"); 
		}
		if ($myPrms['makeStoryboard']){
			// On fait le story à partir du pdf
			if($myPrms['makePDF']) $id_mat=stripExtension($id_mat).".pdf";
			$job_nom=kStoryboard." ".$id_mat;
			$jobs[]=array("job"=>"storyboard","job_nom"=>$job_nom,"job_in"=>$id_mat,"frameInterval"=>$myPrms['frameInterval'],"frame_width"=>$myPrms['frame_width'],"frame_height"=>$myPrms['frame_height']);
		}
		if ($myPrms['transcode_add']){
			$job_nom=kEncodage." ".$myPrms['transcode_preset']." ".$id_mat;
			$jobs[]=array("job"=>"encodage","job_nom"=>$job_nom,"job_in"=>$id_mat,"transcode_preset"=>$myPrms['transcode_preset']);
		}
	}
	
	if(php_sapi_name() == 'cli'){
		echo "\n".kUploadDebutTraitementCLI;
		foreach ($jobs as $job){
			echo 
			$_GET = null;
			echo $job["job_nom"]."\n";
			foreach($job as $varname=>$val){
				$_GET[$varname] = $val;
			}
			
			include(frameDir."processJob.php");
		}
		echo kUploadFinTraitementCLI."\n";
	}else{
	
	// VP 4/12/10 : prise en compte matériel transcodé dans out (endJob)
	?>
<div id="msg"></div>
<img src="<?=imgUrl?>wait30trans.gif" id="myClock" style="display:none"/>
<iframe name="iframeJob" id="iframeJob" src="" frameborder="0" scrolling="no" width="0" height="0" allowtransparency="true"></iframe>

<script type="text/javascript" >
	var jobs=<?=array2json($jobs);?> //version json des jobs
	var jobNum=0;
	var msg="<?= kUploadDebutTraitement ?><br/>";

	function startJob(){
		if(jobNum<jobs.length){
			var url="<?=kCheminHttp?>/empty.php?urlaction=processJob";
			for(var k in jobs[jobNum]){
				url+="&"+k + "="+encodeURI(jobs[jobNum][k]);
			}
			document.getElementById("iframeJob").src=url;
			dspMsg(jobs[jobNum].job_nom);
			if(document.getElementById('myClock')) document.getElementById('myClock').style.display="inline";
		}
		
	}

	function endJob(message,out){
		if(message!='')  dspMsg(message);
		if (out && window.opener && window.opener.addMat)
			window.opener.addMat(null,null,out);
		jobNum++;
		if(jobNum<jobs.length) startJob();
		else {
			dspMsg("<?= kUploadFinTraitement ?>");
			if(document.getElementById('myClock')) document.getElementById('myClock').style.display="none";
			
			<? if (isset($myPrms['rtn']) && !empty($myPrms['rtn'])) { ?>
				if (window.opener.<? echo $myPrms['rtn']; ?>)
					window.opener.<? echo $myPrms['rtn']; ?>(); 
			<? } ?>
		}
	}

	function dspMsg(message){
		var ladate=new Date();
		msg+=ladate.getHours()+":"+ladate.getMinutes()+":"+ladate.getSeconds()+" : "+ message +"<br/>";
		if(document.getElementById("msg"))document.getElementById("msg").innerHTML=msg;
	}

	startJob();
</script>
<?}?>