
<div id="contenu">
<?php
	
	function importFichier($file,$xsl) {
		$data = implode(" ",file($file));
		if(!empty($xsl)){
			$param=array();
			$data = TraitementXSLT($data,getSiteFile("designDir","print/".$xsl.".xsl"),$param,0);
		}
		return $data;
	}
	require_once(modelDir.'model_materiel.php');


	//ORIGINE : Liste des fichiers orphelins
	if ($_GET['from']=='maintenanceMat') {
		
		if ($_GET['id_mat']) $mediaFile[]=Materiel::getLieuByIdMat($_GET['id_mat']).$_GET['id_mat']; //un id_mat renseigné ? on utilise juste lui
		elseif ($_GET['sql']) { // résultat de recherche (matListe) ? on utilise cette liste
			
			$sql=$_SESSION[$_GET['sql']]['sql'];
			if ($sql) {
				global $db;
				$_mats=$db->GetAll($sql);
				foreach ($_mats as $_mat) {
					$mediaFile[]=Materiel::getLieuByIdMat($_GET['ID_MAT']).$_mat['ID_MAT'];
				}
			}
		}
		else {  // sinon, par défaut : scan des matériels orphelins et constitution d'une liste
			global $db;
			// 1. Récupération de la liste des matériels
			/*
			$sql="SELECT * from t_mat ORDER BY ID_MAT ASC";
			$myMats=$db->GetAll($sql);
			$arrMats=array();
			foreach ($myMats as $mat) {
				if (!file_exists(Materiel::getLieuByIdMat($mat['ID_MAT']).$mat['ID_MAT'])) {
					$arrMissingFile[]=$mat;
				}
				$arrMats[]=Materiel::getLieuByIdMat($mat['ID_MAT']).$mat['ID_MAT'];
			}*/
			// VP 17/07/09 : optimisation récupération lieu
			$sql="SELECT ID_MAT,MAT_NOM,MAT_CHEMIN,LIEU_PATH from t_mat left join t_lieu on (MAT_LIEU=LIEU) ORDER BY ID_MAT ASC";
			$myMats=$db->GetAll($sql);
			$arrMats=array();
			foreach ($myMats as $mat) {
				$path=(empty($mat['LIEU_PATH'])?kVideosDir:((substr($mat['LIEU_PATH'],0,1)!='/')?kCheminLocalPrivateMedia.'/'.$mat['LIEU_PATH']:$mat['LIEU_PATH']));
				$path.= (!empty($mat['MAT_CHEMIN'])?$mat['MAT_CHEMIN']."/":"");
				if (!file_exists($path.$mat['MAT_NOM'])) {
					$arrMissingFile[]=$mat;
				}
				$arrMats[]=$path.$mat['MAT_NOM'];
			}
			// 2. Récupération de la liste des fichiers
			// VP 17/07/09 : exclusion fichiers commençant par . 
			$arrFiles=list_directory(kVideosDir,'',array('xml','db'));  //passé en récursif pour les ss-répertoires
	    	foreach ($arrFiles as $file) {
				$file=str_replace('//','/',$file); //Précaution car il peut arriver que l'on ait des "double /"...
				$filename=basename($file);
				if (!in_array($file,$arrMats) && $filename[0]!='.') {
		 	       	 $mediaFile[]=$file;
		      }
		   	}

			// VP 10/06/10 : liste des fichiers inclus dans les répertoires lieux
			$sql="SELECT * from t_lieu";
			$rows=$db->GetAll($sql);
			foreach ($rows as $row) {
				if(substr($row['LIEU_PATH'],0,1)!='/'){
					$row['LIEU_PATH'] = kCheminLocalPrivateMedia.'/'.$row['LIEU_PATH'];
				}
				$arrFiles=list_directory($row['LIEU_PATH'],'',array('xml','db'));
				foreach ($arrFiles as $file) {
					$file=str_replace('//','/',$file); //Précaution car il peut arriver que l'on ait des "double /"...
					$filename=basename($file);
					if (!in_array($file,$arrMats) && $filename[0]!='.') {
						$mediaFile[]=$file;
					}
				}
			}
		}
	}

	else {
		// VP 28/07/09 :  paramètre watchFolder si pas maintenanceFichier
		if(isset($_GET['watchFolder'])) $watchFolder=FCPWatchFolder.$_GET['watchFolder']."/";
		else $watchFolder=FCPWatchFolder;

		$tabFile = scandir($watchFolder);
		// VP 04/06/10 : prise en compte des extensions video, audio et image
		foreach($tabFile as $idx => $f){
			switch(getExtension($f)){
				case "xml": // Import FCP
				$xmlFile=$f;
				break;
				
				default:
				//MS 24/04/2012 - Si parametre GET 'dir', on rajoute au mediafile aussi les noms des répertoires du watchFolder
				if(($f[0]!=".") && (in_array(getExtension($f),unserialize(gVideoTypes)) || in_array(getExtension($f),unserialize(gAudioTypes)) || in_array(getExtension($f),unserialize(gImageTypes))||(isset($_GET['dir']) && is_dir(FCPWatchFolder.'/'.$f)))) 
						$mediaFile[]=$f;
				break;
			}
		}
	}

	// GT 26/10/2010 test si l'envoi est de type batch_mat ou batch_fcp
	if(isset($xmlFile) && $_GET['type']=='batch_fcp')
	{
		// On a un fichier xml au format FCP a priori
		$xsl="importFCP";
	} else if(isset($mediaFile)) {
	// On a des médias sans fichier XML
	} else if(isset($_POST["bOK"])) {
	// sinon récupération dialogue POST
		if(($_FILES["importFichier"]["size"]>0)&& ($_POST["xsl"]!="")) {
			$xmlFile=$_FILES["importFichier"]["tmp_name"];
			$xsl=$_POST["xsl"];
		}
	} 
	
	// Traitement
	if(isset($xmlFile) && isset($xsl) && $_GET['type']=='batch_fcp') {
	// Traitement XML FCP
		// VP 17/12/08 : ajout chemin au fichier XML
		$xmlFile=$watchFolder.$xmlFile;
		$xmldata=importFichier($xmlFile,$xsl);
		$datas=xml2tab($xmldata);
		if(isset($datas['DATA'][0]['MAT'])) {
			$mats=$datas['DATA'][0]['MAT'];
			// On renomme le fichier
			rename($xmlFile,$xmlFile.".done");
			// Affichage du dialogue de configuration
			include(getSiteFile("formDir","importFCP.inc.php"));
		} else {
			print "<div class='error'>Fichier XML incorrect : $file</div>";
			print_r($datas);
			// On renomme le fichier
			rename($xmlFile,$xmlFile.".err");
		}
	} else if(isset($mediaFile)){
			// Affichage du dialogue de configuration
			include(getSiteFile("formDir","importFCP.inc.php"));
	
	} else {
		//print_r($_FILES);
		print "<div class='error'>".kErrorMaintenanceFichierNoFile."</div>";
	}

?>
</div>