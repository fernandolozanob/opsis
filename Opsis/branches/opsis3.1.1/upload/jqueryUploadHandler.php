<?php

require_once("conf/conf.inc.php");
require_once("conf/initialisationGeneral.inc.php");
require_once(libDir . "session.php");
require_once(libDir . "fonctionsGeneral.php");

//gUploadModeRewrite == Rename ==> renommage unique
if ($_GET['upload_type'] == 'doc_acc') {
	$upload_dir = kDocumentDir;
	$upload_url = kDocumentUrl;
} else {
	$upload_dir = kVideosDir;
	$upload_url = videosServer;
}

$upload_tmp_dir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();

//header('Content-type: application/json');
//header('Content-type: text/plain');
header('Content-type: text/html');

if (isset($_GET['type']) && $_GET['type'] == 'url') {
	if (!empty($_GET['url'])) {
		$arr_url = explode('/', $_GET['url']);
		$nom_final = $arr_url[count($arr_url) - 1];
		$nom_ori = $arr_url[count($arr_url) - 1];

		if (defined("gUploadModeRewrite") && stripos(gUploadModeRewrite, "rename") !== false) {
			$i = '';
			while (file_exists(kVideosDir . '/' . stripExtension($nom_final) . $i . '.' . getExtension($nom_final)))
				$i++;

			$nom_final = stripExtension($nom_final) . $i . '.' . getExtension($nom_final);
		}


		if (getExtension($nom_final) != '') {

			$fd = fopen(kVideosDir . '/' . $nom_final, 'w');

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, urldecode($_GET['url']));
			curl_setopt($ch, CURLOPT_FILE, $fd);
			curl_exec($ch);
			curl_close($ch);

			fclose($fd);
		}

		if (file_exists(kVideosDir . '/' . $nom_final) && is_file(kVideosDir . '/' . $nom_final)) {
			echo '[{"name":"' . $nom_final . '","name_ori":"' . $nom_ori . '","type":"' . mime_content_type(kVideosDir . '/' . $nom_final) . '","size":' . filesize(kVideosDir . '/' . $nom_final) . '}]';
		} else {
			echo 'false';
		}
	}
} else {
	// TRAITEMENT DIRECT UPLOAD JS (plugin jquery fileupload by blueimp)
	$content_range_header = (isset($_SERVER['HTTP_CONTENT_RANGE']) ? $_SERVER['HTTP_CONTENT_RANGE'] : null);
	$content_range = $content_range_header ? preg_split('/[^0-9]+/', $content_range_header) : null;
	$file_fullsize = $content_range ? $content_range[3] : null;


	// MS 07.12.15 - un certain nombre de traces ont �t� laiss�es pour pouvoir les activer rapidement en cas de debuggage necessaire
	// trace("CALL JQUERYUPLOADHANDLER");
	// trace('get memory_limit php : '.ini_get('memory_limit'));
	// trace("HTTP_CONTENT_RANGE : ".$_SERVER['HTTP_CONTENT_RANGE']);
	// trace("content_range_header ".$content_range_header."- content_range ".print_r($content_range,true)."- size ".$file_fullsize."-");
	// si on detect upload par chunk
	if (!empty($content_range_header)) {
		$allChunksReceived = false;
		// r�cup nom r�pertoire chunk du fichier, compos� � partir des donn�es envoy�es via le formulaire => permet d'assurer la continuit� du nom pr les diff�rents chunks et l'unicit� de ce nom par rapport � d'autres uploads
		$name_tmp_chunk_dir = 'chunks_' . md5(session_id() . $_FILES['files']['name'][0] . $file_fullsize);
		if (isset($_POST['fileTimestamp']) && !empty($_POST['fileTimestamp'])) {
			$name_tmp_chunk_dir.= '_' . $_POST['fileTimestamp'];
		}

		$tmp_chunk_fullpath = $upload_tmp_dir . '/' . $name_tmp_chunk_dir;

		// r�cup nom fichier final 
		$nom_final = removeTrickyChars(stripAccents($_FILES['files']['name'][0], true));

		// Si $content_range[1] == 0, alors on est en train de traiter le premier chunk d'un upload par chunks
		if (intval($content_range[1]) == 0) {
			// cr�ation du r�pertoire temporaire contenant le fichier en train d'�tre recu par chunk
			if (!is_dir($tmp_chunk_fullpath)) {
				// trace("cr�ation rep temp de concat�nation : ".$tmp_chunk_fullpath);
				mkdir($tmp_chunk_fullpath);
			}
			// si le fichier existe d�j� on le supprime => on est en train de traiter le premier chunk d'un upload, on supprime des restes �ventuels d'un upload stopp�.
			if (file_exists($tmp_chunk_fullpath . "/" . $nom_final)) {
				unlink($tmp_chunk_fullpath . "/" . $nom_final);
			}

			// d�placement du premier chunk dans le r�pertoire temporaire de concat�nation
			$ret = move_uploaded_file($_FILES['files']['tmp_name'][0], $tmp_chunk_fullpath . "/" . $nom_final);
			// trace("premier chunk deplac� vers rep temp de concat�nation : ".$tmp_chunk_fullpath."/".$nom_final." // ".intval($ret));
			if(!$ret){
				trace("error move_uploaded_file ".print_r($_FILES["file"]["error"],true));
			}
		
		}else{
		// sinon, on est en train de traiter un des chunk de l'upload
			//Si le header de range indique un offset diff�rent de la taille du fichier de concat�nation, il y a eu un probl�me quelque part, on cancel
			if (intval($content_range[1]) != filesize($tmp_chunk_fullpath . "/" . $nom_final)) {
				trace('comparaison chunk NOK : ' . $nom_final . " " . (intval($content_range[1])) . " VS " . filesize($tmp_chunk_fullpath . "/" . $nom_final));
				header("HTTP/1.0 406 Not Acceptable");
				echo "wrong chunk";
				exit;
			}
			// /!\ attention, le retour de la fonction filesize sur un fichier est mis en cache, il faut clear ce cache pour pouvoir recalculer correctement la taille du fichier
			clearstatcache(true, $tmp_chunk_fullpath . "/" . $nom_final);
			// $start = microtime(true);
			// on append le chunk courant au fichier contenu dans le r�pertoire temporaire de concat�nation
			file_put_contents(
					$tmp_chunk_fullpath . "/" . $nom_final, fopen($_FILES['files']['tmp_name'][0], 'r'), FILE_APPEND
			);

			// 2e impl�mentation append : 
			// => pb , file_get_contents charge le deuxieme fichier => overflow de la m�moire dispo pr php 
			/* $fh = fopen($tmp_chunk_fullpath."/".$nom_final,'a');
			  fwrite($fh,file_get_contents($_FILES['files']['tmp_name'][0]));
			  fclose($fh); */

			// 3e impl�mentation : 
			// => pb potentiels : SE linux, cat n'est pas forc�ment la meilleure fa�on de concat => voir si d'autres utilitaires linux de concat�nation de binary files 
			// shell_exec("cat ".$_FILES['files']['tmp_name'][0]." >> ".$tmp_chunk_fullpath."/".$nom_final);
			// trace("temps d'execution de l'append chunk >> fichier concat�n� : ".floatval(microtime(true) - $start));
			// Apr�s la concat�nation, si le fichier temporaire dans le r�pertoire de concat�nation fait la m�me taille que la fullsize du fichier en cours d'upload, l'upload par chunk est termin�
			if (filesize($tmp_chunk_fullpath . "/" . $nom_final) == $file_fullsize) {
				$allChunksReceived = true;
				// trace("concat�nation chunks termin�e , flag allChunksReceived = true ;");
			}
		}
	}


	if(isset($_GET['getUploadedBytes']) && !empty($_GET['getUploadedBytes'])){
		// méthode de récupération de la taille de la part uploadée du fichier
		$filename = $_GET['file'];
		$file_fullsize = $_GET['fileSize'];
		
		$name_tmp_chunk_dir = 'chunks_' . md5(session_id() . $filename . $file_fullsize);
		if (isset($_POST['fileTimestamp']) && !empty($_POST['fileTimestamp'])) {
			$name_tmp_chunk_dir.= '_' . $_POST['fileTimestamp'];
		}

		$tmp_chunk_fullpath = $upload_tmp_dir . '/' . $name_tmp_chunk_dir;

		// récup nom fichier final 
		$nom_final = removeTrickyChars(stripAccents($filename, true));
		//trace("getUploadedBytes, filepath / filename : ".$tmp_chunk_fullpath.' '.$nom_final.' '.file_exists($tmp_chunk_fullpath).' '.file_exists($tmp_chunk_fullpath.'/'.$nom_final));
		$file_size = filesize($tmp_chunk_fullpath.'/'.$nom_final);
		
		echo '[{"name":"' . $nom_final . '","size":' . $file_size. '}]';

		
	}elseif (empty($content_range_header) || (isset($allChunksReceived) && $allChunksReceived)) {
		//renommage du fichier sans espaces et accents
		$nom_ori = $_FILES['files']['name'][0];
		if (empty($content_range_header)) {
			$nom_final = removeTrickyChars(stripAccents($_FILES['files']['name'][0], true));
		}
		if (isset($allChunksReceived) && $allChunksReceived) {
			$nom_final_chunk_file = $nom_final;
		}

		// si on renomme le fichier
		if (defined("gUploadModeRewrite") && stripos(gUploadModeRewrite, "rename") !== false) {
			$i = '';
			while (file_exists(kVideosDir . "/" . stripExtension($nom_final) . $i . '.' . getExtension($nom_final)))
				$i++;

			$nom_final = stripExtension($nom_final) . $i . '.' . getExtension($nom_final);
		}

		if ((isset($allChunksReceived) && $allChunksReceived) && $ret = rename($tmp_chunk_fullpath . "/" . $nom_final_chunk_file, $upload_dir . $nom_final)) {
			echo '[{"name":"' . $nom_final . '","name_ori":"' . $nom_ori . '","type":"' . $_FILES['files']['type'][0] . '","size":' . $file_fullsize . '}]';
			// trace("upload par chunk complet, d�placement du fichier upload� vers r�pertoire vid�os // ".intval($ret));
			$ret = rmdir($tmp_chunk_fullpath);
		}elseif ($ret = move_uploaded_file($_FILES['files']['tmp_name'][0],$upload_dir.$nom_final)){
			echo '[{"name":"'.$nom_final.'","name_ori":"'.$nom_ori.'","type":"'.$_FILES['files']['type'][0].'","size":'.$_FILES['files']['size'][0].'}]';
		}else{
			trace("error move_uploaded_file ".print_r($_FILES["file"]["error"],true));
			//trace("full \$_FILES :".print_r($_FILES,true));
			$err ="";
			echo 'false';
		}
	} else {
		header('Range: 0-' . $content_range[2]);
		// trace("retour jqueryUploadHandler apres upload d'un chunk (upload total non termin�)");
		$file = new StdClass();
		$file->name = $_FILES['files']['name'][0];
		$file->size = $_FILES['files']['size'][0];
		$file->type = $_FILES['files']['type'][0];
		$file->error = $_FILES['files']['error'][0];
		$file->tmp_name = $_FILES['files']['tmp_name'][0];

		echo json_encode(array($file));
	}

	// trace("FIN JQUERY UPLOADHANDLER=== memory peak : ".memory_get_peak_usage(true)." en octets \n\n\n");
}
?>