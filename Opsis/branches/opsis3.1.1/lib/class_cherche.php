<?
/**
 * Classe du moteur de recherche générique
 * Cette classe est destinée à être surchargée par les recherches spécifiques
 * DOC, LEX, etc
 * Elle contient le traitement des champs, le traitement des valeurs saisies (découpage de mots etc), le highlight
 * les extensions lexicales (lexique), et la plupart des types de recherches usuels (texte, date, nombre, etc)
 * Elle ne contient pas le spécifique aux recherches :
 * SQL de préparation et de finalisation de requete notamment.
 */
class Recherche{
	// VP 5/10/09 : prise en compte paramètre NOHIST dans les différents modes de recherche

    //Variables non utilisées dans cette classe mais utilisées dans la surcharge
    var $sql; // Debut du SQL (partie SELECT champs et tables)
	var $sqlRecherche; // Suite du SQL clauses WHERE, remplie par le moteur de recherche
	var $sqlSuffixe; // Ordres complémentaires ajoutés en fin de requête. Ex : clauses additionnelles, GROUP BY / HAVING
    var $sqlWhere; //Partie du SQL contenant la ligne WHERE ainsi que des critères primordiaux ou constants.
    				//Cette variable peut rester vide, mais il faudra mettre alors le Where dans le $sql;
    var $useSession; // Flag utilisation oui ou non d'une session
	var $sessVar; //Nom de la variable de session où stocker l'objet
    protected $prefix; // Prefixe de table pour les jointures
    var $entity; // ENTITE LIEE A LA RECHERCHE : ex DOC, LEX, VAL  càd table principale dans laquelle on cherche
    protected $name; // Nom de l'entité : Document, Lexique, Evènement, etc.
    //Fin des variables surchargées

    var $etape; // Chemin de fer des critères
    var $tab_recherche; // tableau des CHAMP => array(CHAMP, VALEUR, OPERANDE, LIBELLE, VALEUR2)
    var $tabHighlight=array(); //Tableau des critères et valeurs à mettre en highlight

	var $saveHisto; // toggle sauvegarde recherche o/n

    protected $tab_libelles=array('AND'=>kEt,'OR'=>kOu,'AND NOT'=>kSauf,'HAVING'=>kEt,'NOT'=>kSauf); //libellés types de recherche

    protected $FToperator; // opérateur pour la recherche FT, pour mySQL, si + recherche en OU sur les termes, si vide recherche en ET

    var $params; //array de Paramètres propres à la recherche.
    				   //A terme, j'aimerais mettre tout le paramétrage dedans, pour éviter les
    				   //X paramètres d'appels de fonctions et autres constantes (cf ChooseType, extension lexicale, etc.)

	//@update VG 08/04/2010 : ajout du nombre de lignes retournées par la requete
	var $rows;

	var $start_offset;
	
	// critere de tri
	private $sql_order_by;

    private function __construct() { //Constructeur, pas vraiment utile puisque systématiquement surchargé mais mis à toutes fins utiles
     	$this->tab_recherche=array();
     	$this->useSession=true;
     	$this->saveHisto=false; //by ld 141108 par déft, pas de sv de la recherche (navigation, refine)
     	if (defined("gParamsRecherche")) $this->params=unserialize(gParamsRecherche);

		$this->sql_order_by='';
    }

    function setPrefix($pre) {$this->prefix=$pre;}

	function getPrefix() {return $this->prefix;}

	function ajouteTableRecherche($table,$join)
	{
		$this->sql.=' LEFT OUTER JOIN '.$table.' ON '.$join;
	}

	function ajouteCritere($operateur,$critere)
	{
		$this->sqlWhere.=' '.$operateur.' '.$critere;
	}

	function ajouterTri($champ,$ordre='')
	{
		$this->sql_order_by=' ORDER BY '.$champ;

		if (!empty($ordre))
			$this->sql_order_by.=' '.$ordre;
	}

	function getTri()
	{
		return $this->sql_order_by;
	}
	function setTri($tri_sql)
	{
		$this->sql_order_by = $tri_sql;
		return true ;
	}

    /** AnalyzeFields
     * Prépare les couples valeurs / opérande (OR/AND/NOT) pour la recherche
     * IN : $myTab, formulaire de champs ($_POST);
     * OUT : $tab_recherche : tableau des critères / valeurs et opérandes
     * NOTE : les critères VALEUR2 et NOHIGHLIGHT sont optionnels
     */
	// VP 25/9/09 : ajout critères optionnels NOHIST et OPTIONS
    function analyzeFields ($myFields,$myVals,$myTypes,$myOps=array(),$myLibs=array(),$myVals2=array(),$myNOHL=array(),$myNOHist=array(),$myOptions=array()) {
	        if (!empty($myFields)) {
		        foreach ($myFields as $idx=>$field) {
			        if ($field!='' && isset($myVals[$idx]) && $myVals[$idx]!='' && isset($myTypes[$idx]) && $myTypes[$idx]!='' ) {

							if (empty($myOps[$idx])) $myOps[$idx]="AND";
							if (empty($myLibs[$idx])) $myLibs[$idx]=kCritere;
							//si plusieurs valeurs en tableau, on les concatène (cas des checkbox)
							//if (is_array($myVals[$idx])) $myVals[$idx]=implode(',',$myVals[$idx]);

							// les obligatoires
							$this->tab_recherche[$idx]['FIELD']=$field;
							$this->tab_recherche[$idx]['VALEUR']=$this->removeWeirdCharacters($myVals[$idx]);
							$this->tab_recherche[$idx]['OP']=$myOps[$idx];
							$this->tab_recherche[$idx]['TYPE']=$myTypes[$idx];
							$this->tab_recherche[$idx]['LIB']=$myLibs[$idx];
							// les optionnels
							if (isset($myVals2[$idx])) $this->tab_recherche[$idx]['VALEUR2']=$myVals2[$idx];
							if (isset($myNOHL[$idx])) $this->tab_recherche[$idx]['NOHIGHLIGHT']=$myNOHL[$idx];
							if (isset($myNOHist[$idx])) $this->tab_recherche[$idx]['NOHIST']=$myNOHist[$idx];
                            if (isset($myOptions[$idx])) $this->tab_recherche[$idx]['OPTIONS']=$myOptions[$idx];
			        }
		        }
	        }
	      $this->makeSQL(); //Lancement du moteur
     }

	function removeWeirdCharacters ($str) {
		$str=str_replace(array("’"),array("'"),$str);

		return $str;
	}

	public function makeSQL () { //parcours des critères
		
		if(defined("rearrangeSearchByOperator") && rearrangeSearchByOperator){
			$arr_search_elt = array('AND'=>array(),'AND NOT'=>array(),'OR'=>array()) ; 
			foreach($this->tab_recherche as $idx=>$tab_elt){
				if($tab_elt['OP'] == 'AND'){
					$arr_search_elt['AND'][] = $tab_elt;
				}else if($tab_elt['OP'] == 'OR'){
					$arr_search_elt['OR'][] = $tab_elt;
				}else if($tab_elt['OP'] == 'NOT' || $tab_elt['OP'] == 'AND NOT'){
					$arr_search_elt['AND NOT'][] = $tab_elt;
				}
			}
			
			if(!empty($arr_search_elt['AND']) || !empty($arr_search_elt['AND NOT'])){
				$this->str_recherche.='(';
				foreach($arr_search_elt['AND'] as $elt){
					$this->ChooseType($elt,$this->prefix);
				}
				foreach($arr_search_elt['AND NOT'] as $elt){
					$this->ChooseType($elt,$this->prefix);
				}
				$this->str_recherche.=')';
			}
			if((!empty($arr_search_elt['AND']) || !empty($arr_search_elt['AND NOT']))
				&& !empty($arr_search_elt['OR'])){
				$this->str_recherche.=' AND ';
			}
			if(!empty($arr_search_elt['OR'])){
				$this->str_recherche.='(';
				foreach($arr_search_elt['OR'] as $elt){
					$this->ChooseType($elt,$this->prefix);
				}
				$this->str_recherche.=')';
			}
			
			$this->str_recherche = preg_replace('/(\([\s]*?(?:OR|AND|NOT))/','(',$this->str_recherche);
			
		}else{
			foreach($this->tab_recherche as $field=>$tab){
				$this->ChooseType($tab,$this->prefix);
			}
		}
	}

/**
 * En fonction des types de recherche, appelle les fonctions de traitement dédiée.
 * Cette fonction peut se trouver surchargée si une classe introduit des recherches spécifiques
 */
    protected function ChooseType($tabCrit,$prefix) { 
            // I.2.A.1 Tests des champs et appel des fonctions de recherche ad?quates :
            switch($tabCrit['TYPE'])
            {
                case "FT": //FullText
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    break;
                case "FTS": //FullText avec découpage de chaque mot
                	$this->FToperator='+';
                	$this->chercheTexteAmongFields($tabCrit,$prefix);
                	break;
                case "FI": //Fonds par ID
                case "FPI": //Fonds parent par ID
                    $this->chercheFondsId($tabCrit,$prefix);
                break;
				case "FTE": //FullText exact
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    break;
                case "C":   // Recherche sur un champ précis
                    $this->chercheChamp($tabCrit,$prefix);
                    break;
                case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote) (recherche sur la string entière (pas de découpage en mots))
                    $this->chercheChamp($tabCrit,$prefix,true);
                    break;
                case "CB":   // VP 24/07/09 : Recherche sur un champ booléen
                    $this->chercheChampBooleen($tabCrit,$prefix);
                    break;
                case "CI":    // Recherche sur un champ d'une table annexe par ID
                    $this->chercheIdChamp($tabCrit,$prefix);
                    break;
                case "CT": // Recherche sur un champ par texte exact dans une table liée (fonds, genre,...)
                    $this->chercheChampTable($tabCrit,$prefix);
                    break;
                case "D":   // Recherche sur date(s)
                    $this->chercheDate($tabCrit,$prefix);
                    break;
                case "H":   // Recherche sur durées(s)
                    $this->chercheDuree($tabCrit,$prefix);
                    break;
					// VP 23/12/08 : ajout recherche VF
                case "V":  // Recherche sur valeur
                case "VF":  // Recherche sur valeur en FT
                    $this->chercheVal($tabCrit,$prefix);
                    break;
                case "VI":    // Recherche sur valeur par ID (récursif)
                    $this->chercheIdVal($tabCrit,$prefix,true);
                    break;
                case "CD" : //comparaison de date
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CDLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CDGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CH" : //comparaison de date
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CV" : //comparaison de valeur =
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVGT" : //comparaison de valeur >
                	$tabCrit['VALEUR2']='>';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVLT" : //comparaison de valeur <
                	$tabCrit['VALEUR2']='<';
                	$this->compareValeur($tabCrit,$prefix);
                	break;

                case "LI" : // Recherche sur ID LEX unique (récursif)
                	$this->chercheIdLex($tabCrit,$prefix,true);
                	break;
                case "L" : // Recherche sur termes lexique (av. extension lex PRE)
                	$this->chercheLex($tabCrit,$prefix,true,false);
                	break;
                case "LP" : // Recherche sur termes lexique (av. extension lex PRE et POST)
                	$this->chercheLex($tabCrit,$prefix,true,true);
                	break;

                case "P" :
                	$this->cherchePers($tabCrit,$prefix);
                	break;
                case "PI" :
                	$this->chercheIdPers($tabCrit,$prefix);
                	break;
                case "U" :
				case "UF" :
					$this->chercheUsager($tabCrit,$prefix);
					break;
				case "XML" :
					$this->chercheXML($tabCrit,$prefix);
				break;
                default : break;
            }

    }

    /**
     * Fonction vide car systématiquement surchargée
     *
     */
	function prepareSQL () {}


	/**
	 * Fonction vide car systématiquement surchargée
	 *
	 */
	function appliqueDroits() {
	}

    /**
     * Traitement final de la chaine de critères :
     * 1er opérateur, ajout du suffixe SQL, étape et mise en session si nécessaire
     */

    function finaliseRequete(){

		// VP 3/12/10 : prise en compte langue pour troncature étape (coupe au premier espace)
        $this->sqlRecherche=trim($this->sqlRecherche);
        // if (strpos($this->sqlRecherche,"AND NOT")===0){
			// //Dans ce cas, on fait une recherche en NOT

            // $this->sqlRecherche=substr($this->sqlRecherche,7);
            // $this->sqlRecherche=" NOT ( ".$this->sqlRecherche." ) ";
            // $this->etape=kTousDocuments." ".$this->etape;
        // }else
		if (strpos($this->sqlRecherche,"AND")===0){ //retrait du premier opérateur (évite le WHERE AND...)
            $this->sqlRecherche=substr($this->sqlRecherche,3);
            //$this->etape=substr($this->etape,3);
            $this->etape=substr($this->etape,strpos($this->etape,' '));
		} else if (strpos($this->sqlRecherche,"OR")===0){
            $this->sqlRecherche=substr($this->sqlRecherche,2);
            //$this->etape=substr($this->etape,2);
			$this->etape=substr($this->etape,strpos($this->etape,' '));
        }

		$this->sql.=" ".$this->sqlWhere;
        if($this->sqlRecherche!=""){
			$this->sql.=" AND (".$this->sqlRecherche.")";
        }

        $this->sql.=$this->sqlSuffixe;
    	if (empty($this->etape)) $this->etape=$this->name;
    	
        // VP 14/03/2014 : suppression htmlentities, à faire dans pages XXXListe
		// $this->etape=htmlentities($this->etape,ENT_COMPAT,'UTF-8');
    	if ($this->useSession) $this->putSearchInSession(); // On sauve la recherche en session
        //trace($this->sql);
    }

/**
 * Analyse et split de chaine après analyse par expression régulière (LD 24/04/09)
 * IN : chaine
 * OUT : tableau de mots + tableau (arrOps) des opérateurs +/-
 * NOTE : testé avec chaine :
 * grande muraille,angleterre -lunettes arch* "c d","a b" mot1 - mot2 l'@venture "c'est moi" -rouge-gorge
 *
 */
	static function analyzeString2($valeur_champ,&$arrOps) {
		
		$keywords = preg_split( "/[\s]*(\\\"[^\\\"]+\\\")([\s,]*)|([\s,'])+/",
		 $valeur_champ, 0, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY );

		//En sortie d'analyse ici, on a un tableau contenant tous les mots, et les séparateurs (, espace ' )
		//Les expressions entre guillemets sont préservées ainsi que les mots-composés
		//Les caractères spéciaux comme @ sont gérés comme des caractères normaux
		//Les chaînes < 2 caractères sont gardées ici, elle seront ôtées par ailleurs

		//debug($keywords,'lightblue');
		$keywords=array_values($keywords);

		foreach ($keywords as $idx=>$wd) {
			//update VG 18/07/2011 : on ne retourne plus les mots ne contenants que "*"
			if (trim($wd)=='' || trim($wd=="'") || trim($wd=="*") || trim($wd==':')) continue;//on vire les ' seuls
			//virgule seule ? on place les 2 termes attenants en opérateur + (ou)
			if (trim($wd)==',') {$arrOps[$idx-1]='+';$arrOps[$idx+1]='+';}
			elseif (trim($wd)=='-') {$arrOps[$idx+1]='-';} //tiret seul ? on place le terme suivant en opérateur -
			//tiret au début ? on met l'opérateur - et on retire le - du mot
			elseif ($wd[0]=='-') {$arrOps[$idx]='-';$myWords[$idx]=str_replace("'"," ' ",trim($wd,'-'));}
			// VP (26/8/09) + au début ? on met l'opérateur + et on retire le + du mot
			elseif ($wd[0]=='+') {$arrOps[$idx]='+';$myWords[$idx]=str_replace("'"," ' ",trim($wd,'+'));}
			else $myWords[$idx]=$wd; //mot ou expr entre "" normale
		}
		//debug($myWords,'yellow');
		//debug($arrOps,'pink');
        //trace(print_r($myWords,true));
        
		return $myWords;
	}

     protected function chercheFondsId($tabCrit,$prefix,$ext_children=true) {
        global $db;
        if (is_array($tabCrit['VALEUR'])) $myVals=implode(",",$tabCrit['VALEUR']);
        else $myVals=$tabCrit['VALEUR'];
        if (empty($myVals)) $myVals='0';
        
        if($ext_children){
            // MS - correction de la récupération des fonds fils pour permettre la gestion d'une hierarchie de fonds sur plus de 2 niveaux
            $tmp_arr = explode(',',$myVals);
            $tmp_id_fond = $myVals;
            $tmp_ids = $db->GetCol('SELECT t_fonds.ID_FONDS from t_fonds where ID_FONDS_GEN in ('.$tmp_id_fond.') GROUP BY ID_FONDS');
            
            while(!empty($tmp_ids)){
                foreach($tmp_ids as $id){
                    // ajout des ids récupérés à la valeur myVals avec controle de doublons
                    if(!in_array($id,$tmp_arr)){
                        array_push($tmp_arr,$id);
                        $myVals.=",".$id;
                    }
                }
                $tmp_id_fond = implode(',',$tmp_ids);
                $tmp_ids = $db->GetCol('SELECT t_fonds.ID_FONDS from t_fonds where ID_FONDS_GEN in ('.$tmp_id_fond.') GROUP BY ID_FONDS');
            }
            unset($tmp_arr);
            unset($tmp_ids);
            unset($tmp_id_fond);
        }
        //Création requete SQL
        $this->sqlRecherche.= $tabCrit['OP']." DOC_ID_FONDS IN (".$myVals.") ";

        $fld=($tabCrit['FIELD']!=''?$tabCrit['FIELD']:'FONDS');
        //Récupération des intitulés si on n'a pas de VALEUR2
        if (empty($tabCrit['VALEUR2'])) {
            $sql3="SELECT DISTINCT ID_FONDS,".$fld." from t_fonds WHERE ID_FONDS IN(".$myVals.") ";
            $libs=$db->GetAll($sql3);
            foreach ($libs as $lib) {
                $highlight[]=$lib[$fld]; //liste des fonds pour le highlight
                // Le libellé correspond à une valeur d'origine (hors extension)
                // MS 26/06/2012 : update du test depuis : if(in_array($lib['ID_FONDS'],(array)$tabCrit['VALEUR']))
                if(in_array(intval($lib['ID_FONDS']),explode(',',$tabCrit['VALEUR']))) $orgLibs[]=$lib[$fld];
            }   
        } else { //On a déjà un intitulé ou une valeur dans VALEUR2 => c notamment le cas quand on est passé par chercheFonds
            $orgLibs=(array)$tabCrit['VALEUR2'];
            $highlight=$orgLibs;
        }
        if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kFonds;
        if (empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$orgLibs)." ";
        if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=array_merge($this->tabHighlight, $highlight);
    }
   /**
    * Recherche dans un champ de type FULLTEXT
    * IN : tab de Critères, prefixe table (opt), extension lexicale true/false
    * OUT : sql + etape + highlight
    * NOTE : structure du tab de Critères
    * 	FIELD : chaines de champs dans lesquels effectuer la recherche. Ces champs doivent EVIDEMMENT être indexés en fulltext !
    * 	VALEUR : chaine tapée par l'utilisateur
    * 	VALEUR2 : flag O/N, pour effectuer ou non l'extension lexicale.
    * NOTE :
    *
    */
    protected function chercheTexteAmongFields($myField,$prefix="") {
		global $db;
		debug("chercheTexteAmongFields", "red", true);
		$prefix=($prefix!=''?$prefix.".":"");
		$valeur_champ=trim($myField['VALEUR']);
        if (empty($valeur_champ)) return false;

        if (defined('gRechercheFTSliceWords') && gRechercheFTSliceWords==true) {
        	$arrWords=$this->analyzeString($valeur_champ,$op); //Doit-on découper chaque mot de la chaine ?
        	foreach ($arrWords as $i=>$wd) $arrOps[$i]=$op;
        	//$this->FToperator=$op; //maj de l'opérateur avec le résultat du découpage
        } else {
			$arrWords=$this->analyzeString2($valeur_champ,$arrOps);
        }
		//$arrWords[]=str_replace("'"," ' ",$valeur_champ); //Sinon, on l'utilise comme telle

    	//PC 08/12/10 : recherche sur champ vide avec le caratère #
		if (count($arrWords) == 1 && count(explode(",",$myField['FIELD'])) == 1 && strcmp(trim($arrWords[0]), '#') == 0){
			$arrChamps=explode(",",$myField['FIELD']);
			$fld = $prefix.$arrChamps[0];
			$this->sqlRecherche.= $myField['OP']." (".$fld." IS NULL ";
			$this->sqlRecherche.= " OR  trim(".$fld.") LIKE '')";
			if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$myField['OP']]." ".$myField['LIB']." : ".(defined("kVide")?kVide:"vide")." ";

			return;
		}

		//VP 17/05/10 : recherche sur mots <2 au lieu de mots<=2
//		foreach ($arrWords as $i=>$word) if (strlen($word)<2) {
//			unset ($arrWords[$i]); unset($arrOps[$i]);
//		}//clean mots <2 lettres (empêchent le FT)

		$joker='';
		// VP 25/9/09 : Option troncature
		if (isset($myField['OPTIONS']) && !empty($myField['OPTIONS']) && is_array($myField['OPTIONS']))
		{
			foreach ($myField['OPTIONS'] as $option=>$val) {
				if($option=="TRONCATURE" && $val=="1") $joker="*";
			}
		}
		// VP 29/10/09 : Prise en compte option troncature dans highlight
		// Eviter Warning: Invalid argument supplied for foreach()
		if (is_object($arrWords) || is_array($arrWords)){
			foreach ($arrWords as &$word) { //constitution du tableau des mots recherchés pour highlight
				$word.=$joker;
				$highlightVals[]=str_replace(" ' ","'",$word); //LD 04/06/08 : suppression du spaceQuote
			}
		}

		// VP 9/02/10 : extension lexicale limitée aux synonymes
		if (isset($myField['VALEUR2']) && $myField['VALEUR2']=='1') { //Extension lexicale ?
			// VP 19/11/09 : ajout paramètre $arrWords pour limiter l'extension lexicale
 		//MS MaJ (anciennement : $arrLex=$this->extLexique($myField, $arrWords); )
 			$arrLex=$this->extLexique($arrWords, $arrWords);
		}elseif (isset($myField['VALEUR2']) && $myField['VALEUR2']=='2'){ //Extension lexicale uniquement pour les synonymes
 			$arrLex=$this->extLexique($arrWords, $arrWords, true);
		}

		if (isset($arrLex) && $arrLex) { // Si on a qq chose dans l'extension lexicale
		//	$sqlExtension=$arrLex['VALEURS']; //la recherche porte sur tous les termes
			foreach($arrLex as $idx=>$lex ){
				if (isset($arrLex[$idx]['HIGHLIGHT']) && !empty($arrLex[$idx]['HIGHLIGHT']) && is_array($arrLex[$idx]['HIGHLIGHT']))
					$highlightVals=array_merge($highlightVals,$arrLex[$idx]['HIGHLIGHT']);
			}
		}
		// VP 13/12/09 : utilisation d'une autre variable que word car elle est utilisée plus haut en pointeur (?)
		$arrChamps=explode(",",$myField['FIELD']);

		//by LD 20/04/09 => strVals peut être vide (si que des mots < 2 lettres), donc on n'ajoute pas le critère
		if (count($arrWords) > 0) {
			$typeField = $myField['TYPE'];
			$this->sqlRecherche.= " ".$myField['OP'].$db->getFullTextSQL($arrWords, $arrChamps, $prefix, $arrLex, $arrOps,$typeField);
			if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$myField['OP']]." ".$myField['LIB']." : ".$myField['VALEUR'].$joker." ";
		}

    	//foreach ($arrChamps as $i=>$chmp) $highlight[$chmp]=$highlightVals;
    	$highlight[$myField['FIELD']]=$highlightVals;

        if (empty($myField['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
    }

    /**
     * Procédure d'extension lexicale
     * IN : tableau de mots
     * OUT : tableau ArrLex contenant, aux index correspondants à arrWords
     * 			ID => tableau des ID lex d'un mot
     * 			VALEURS : chaine SQL des termes pour recherche FT d'un mot
     * 			HIGHLIGHT => tableau des mots pour surbrillance d'un mot
     *
     * NOTE : cette fonction est faite pour être surchargée avec des paramètres appelant
     * qui correspondent au contexte : extension verticale récursive, restriction de langue,...
     */
	// VP 9/02/10 : extension limitée aux synonymes (défaut à false)
	// MS 14/06/12 : nouveau fonctionnement, on recherche les extensions lexicales mot par mot (la chaîne a déja été parsée)
	// 		la recherche lexicale mot par mot est nécessaire pour conserver les critères logiques recherchés
	//  	(on pourra ensuite remplacer le mot par tout son bloc de synonymes sans altérer la logique de la requete)
    protected function extLexique($arrWords, $arrExcludeWords=null, $onlySyno=false, $restrict_to_type = null, $deep = -1) {
		global $db;
        if(empty($deep)) $deep=-1;
		foreach($arrWords as $idx=>$w){
			$arrLex[$idx]=$this->getIDFromLexique($w,$restrict_to_type);

			if($arrLex[$idx] != array()){
				//--VP 4/02/10 : ajout recherche sur synonyme avant extension aux spécifiques
				$extSyno1=$this->extLexiqueSyno($arrLex[$idx]);
				if (!empty($extSyno1)) $arrLex[$idx]['ID']=array_merge($arrLex[$idx]['ID'],$extSyno1);

				if(!$onlySyno){
					$extHier=$this->extLexiqueHier($arrLex[$idx],true,'',$deep);
					if (!empty($extHier)) $arrLex[$idx]['ID']=array_merge($arrLex[$idx]['ID'],$extHier);

					$extSyno=$this->extLexiqueSyno($arrLex[$idx]);
					if (!empty($extSyno)) $arrLex[$idx]['ID']=array_merge($arrLex[$idx]['ID'],$extSyno);
				}

				$this->getTermesFromLexique($arrLex[$idx], $arrExcludeWords);


				//--VP 19/11/09 : ajout paramètre $arrExludeWords contenant les termes du lexique à exclure
				/*by LD 02/06/08 : le terme saisi est placé au début et non plus à la fin, permet de résoudre un comportement
				bizarre de mySQL/FT, comme en atteste l'exemple suivant (cnrs) avec la recherche papillon*
				L'extension lexicale renvoie (entre autres) 'Papillon' et donc la chaine +('Papillon') +('papillon*')
				Dans ce cas précis, le deuxième critère est ignoré et ne renvoie pas les notices avec papillons, papillonage, etc
				Mais si l'on place le critère 'papillon*' avant les résultats d'extension, cela fonctionne !
				DONC "+('Papillon') +('papillon*')" != "+('papillon*') +('Papillon')", nous plaçons donc le terme saisi en
				premier.
				LD 24/04/08=> supprimé ICI et remis dans fonction chercheTextAmongFields
				**/
				//--$arrLex['VALEURS']=" +(".$db->Quote($tabCrit['VALEUR']).") ".$arrLex['VALEURS'];
				if (isset($arrLex[$idx]['HIGHLIGHT']) && !empty($arrLex[$idx]['HIGHLIGHT']) && is_array($arrLex[$idx]['HIGHLIGHT']))
				{
					if (!in_array($tabCrit['VALEUR'],$arrLex[$idx]['HIGHLIGHT']) && !empty($tabCrit['VALEUR']))
						$arrLex[$idx]['HIGHLIGHT'][]=$tabCrit['VALEUR'];
				}
			}
		}
    	return $arrLex;
     }



    /**
     *  Récupère les IDs des termes d'une chaine (recherche FT)
     *  IN : chaine de termes + params recherche (restriction langage + mode(FT/EXACT) = mode de recherche des mots Fulltext ou sql_op))
     *  OUT : tableau lexical de structure
     * 		ID => Array des IDs
     * 		VALEURS => chaine des valeurs pour le SQL
     * 		HIGHLIGHT => tableau des termes à mettre en surbrillance
     */
    private function getIDFromLexique($word, $restrict_to_type) {

			global $db;

			// Analyse de la valeur tapée, séparation des mots et mise en forme FT
			//$arrWords=$this->analyzeString2($tabCrit['VALEUR'],$op);
			//foreach ($arrWords as $i=>$word) if (strlen($word)<=2) {
			// MS 06.05.14 - désactivation du filtrage des mots d'une lettre => ajoute du bruit mais necessaire pr recherche Solr
			// if(strlen($word)<=1){
				// $tabLexique['ID'][] = ''; //strip des mots <2 lettres qui empèchent le FT ( MS MaJ : on renvoi juste ID vide => stop le traitement pour ce mot)
					// return $tabLexique ;
			 // }

			//foreach ($arrWords as $idx=>&$word) {
				//By LD 12 12 08 => espacement des ' pour effectuer la recherche
				// MS 21/11/13 ce remplacement est a priori redondant (le terme arrive avec les single quote déja échapées)
				// $word=str_replace("'"," ' ",$word);
				$strVals.="(".$word.") ";

				// MS 07/05/14 - Si on détecte des doubles quotes de part et d'autre du mot => on empeche la recherche fulltext => le terme doit être recherché tel quel
				if(substr($word,0,1) == '"' && substr($word,-1) == '"'){
					$flag_prevent_FT = true ;
				}

			//}
			//By LD 12 12 08 => espacement des ' pour effectuer la recherche
			//$strVals=str_replace(array(",","'"),array(" "," ' "),$strVals);
			if ($this->params['RechercheLexiqueMode']=='FULLTEXT' && (!isset($flag_prevent_FT))) { //MODE FULLTEXT de recherche des ID sur la base des mots renseignés
				if(isset($restrict_to_type) && !empty($restrict_to_type)){
					$where_cond = $db->getFullTextSQL(array($word,$restrict_to_type), array("LEX_TERME","LEX_ID_TYPE_LEX"));
				}else{
					$where_cond = $db->getFullTextSQL(array($word), array("LEX_TERME"));
				}
				$sql = "select DISTINCT ID_LEX from t_lexique
						where ".$where_cond;
			} else { //MODE "EXACT" par sql_op (like/=) de recherche des ID
			$sql= "select DISTINCT ID_LEX from t_lexique WHERE LOWER(LEX_TERME) ".$this->sql_op(strtolower($word),true);
				if(isset($restrict_to_type) && !empty($restrict_to_type)){
					$sql.=" and LEX_ID_TYPE_LEX=".$db->Quote($restrict_to_type);
				}
				
			}

			if ($this->params['RechercheLexiqueRestrictionLangage']==true) $sql.=" AND ID_LANG=".$db->Quote($_SESSION['langue']);
			debug($sql, "red", true);
trace("sql IDFromLex");
		  trace($sql);
		  trace($strVals);
trace("sql IDFromLex");
            $result2 = $db->Execute($sql);
            if (!$result2) return false;
            while($list2=$result2->FetchRow()) $tabLexique['ID'][]=$list2["ID_LEX"];
			$result2->Close();
			return $tabLexique;
    }

     function getIDFromValeur($word, $restrict_to_type=null) {

            global $db;
                $strVals.="(".$word.") ";

                // MS 07/05/14 - Si on détecte des doubles quotes de part et d'autre du mot => on empeche la recherche fulltext => le terme doit être recherché tel quel
                if(substr($word,0,1) == '"' && substr($word,-1) == '"'){
                    $flag_prevent_FT = true ;
                }

            //}
            //By LD 12 12 08 => espacement des ' pour effectuer la recherche
            //$strVals=str_replace(array(",","'"),array(" "," ' "),$strVals);
            if ($this->params['RechercheLexiqueMode']=='FULLTEXT' && (!isset($flag_prevent_FT))) { //MODE FULLTEXT de recherche des ID sur la base des mots renseignés
                if(isset($restrict_to_type) && !empty($restrict_to_type)){
                    $where_cond = $db->getFullTextSQL(array($word,$restrict_to_type), array("VALEUR","VAL_ID_TYPE_VAL"));
                }else{
                    $where_cond = $db->getFullTextSQL(array($word), array("VALEUR"));
                }
                $sql = "select DISTINCT ID_VAL from t_val
                        where ".$where_cond;
            } else { //MODE "EXACT" par sql_op (like/=) de recherche des ID
            $sql= "select DISTINCT ID_VAL from t_val WHERE LOWER(VALEUR) ".$this->sql_op(strtolower($word),true);
            }

            if ($this->params['RechercheLexiqueRestrictionLangage']==true) $sql.=" AND ID_LANG=".$db->Quote($_SESSION['langue']);
            debug($sql, "red", true);
            $result2 = $db->Execute($sql);
            if (!$result2) return false;
            while($list2=$result2->FetchRow()) $tabVal['ID'][]=$list2["ID_VAL"];
            $result2->Close();
            return $tabVal;
    }

         function getTermesFromVal(&$tabVal,$arrExludeWords=null) {

        global $db;
        if(is_array($tabVal['ID'])) {

            $tabVal['ID']=array_unique($tabVal['ID']);
            $id_val_list=implode(',',$tabVal['ID']);

             //MB 02/07/12 : sécurité pour ne pas executer les requetes si id_lex_list est vide
            if(isset($id_val_list) && !empty($id_val_list)) {
                $sql= "select DISTINCT VALEUR from t_val where ID_VAL in (".$id_val_list.")";

                if ($this->params['RechercheLexiqueRestrictionLangage']==true) $sql.=" AND ID_LANG=".$db->Quote($_SESSION['langue']);
                if(!empty($arrExludeWords)){
                    $sql.=" AND NOT ".$db->getFullTextSQL($arrExludeWords, array("VALEUR"));
                }
                $rs = $db->Execute($sql);
                if (!$rs) return false;
                while($list = $rs->FetchRow()) {
                    $tabVal['VALEURS'].=$db->getSqlExtension($list["VALEUR"]);

                    if (count($tabVal['HIGHLIGHT'])<kMaxExtensionLexicale) {
                        $mot=str_replace(array(" ' ","\""),array("'",""),$list["VALEUR"]);
                        $tabVal['HIGHLIGHT'][]='"'.$mot.'"';

                    }
                }
                return $tabVal;
            } else {
                return false;
            }
        }
    }

	/**
	 * Récupère les termes pour une série d'IDs lexique.
	 * IN : tableau lexique, restriction à la langue en cours (opt)
	 * OUT : tableau lex MAJ (VALEURS & HIGHLIGHT)
	 * NOTE : a exécuter obligatoire APRES la fonction getIDFromLexique.
	 */
	// VP 19/11/09 : ajout paramètre $arrExludeWords contenant les termes du lexique à exclure
	// VP 9/02/10 : récupératon termes de LEX_PATH
	protected function getTermesFromLexique(&$tabLexique,$arrExludeWords=null) {

        global $db;
        if(is_array($tabLexique['ID'])) {

			$tabLexique['ID']=array_unique($tabLexique['ID']);
			$id_lex_list=implode(',',$tabLexique['ID']);

             //MB 02/07/12 : sécurité pour ne pas executer les requetes si id_lex_list est vide
            if(isset($id_lex_list) && !empty($id_lex_list)) {
                $sql= "select DISTINCT LEX_TERME,LEX_PATH from t_lexique where ID_LEX in (".$id_lex_list.")";

                if ($this->params['RechercheLexiqueRestrictionLangage']==true) $sql.=" AND ID_LANG=".$db->Quote($_SESSION['langue']);
                if(!empty($arrExludeWords)){
                    $sql.=" AND NOT ".$db->getFullTextSQL($arrExludeWords, array("LEX_TERME"));
                }
                $rs = $db->Execute($sql);
                if (!$rs) return false;
                while($list = $rs->FetchRow()) {
                    //$tabLexique['VALEURS'].=" +(\"".str_replace("'"," \' ",$list["LEX_TERME"])."\") ";
					$tabLexique['VALEURS'].=$db->getSqlExtension($list["LEX_TERME"]);
                    //mod by LD 6/11 : les termes sont systématiquement encadrés par des "" -> permet de conserver des termes composés lors du highlight
    //             	if (count($rs)<kMaxExtensionLexicale) $tabLexique['HIGHLIGHT'][]='"'.str_replace(array(" ' ","\""),array("'",""),$list["LEX_TERME"]).'"';
                    // VP 11/02/10 : activation max extension lexicale
                    if (count($tabLexique['HIGHLIGHT'])<kMaxExtensionLexicale) {
                        $mot=str_replace(array(" ' ","\""),array("'",""),$list["LEX_TERME"]);
                        $tabLexique['HIGHLIGHT'][]='"'.$mot.'"';
    //					$mots=explode(chr(9),$list["LEX_PATH"]);
    //					foreach($mots as $mot){
    //						if(!empty($mot)){
    //							$mot=str_replace(array(" ' ","\""),array("'",""),$mot);
    //							if(!in_array($mot,$tabLexique['HIGHLIGHT'])) $tabLexique['HIGHLIGHT'][]='"'.$mot.'"';
    //						}
    //					}
                    }
                }
                return $tabLexique;
            } else {
                return false;
            }
        }
    }

    /**
     * Extension lexicale horizontale (termes synonymes)
     * IN : tab Lexique
     * OUT : tabLexique (ID étendus)
     * NOTE : pas de restriction de langue car syno identiques pour tte langue
     */
    protected function extLexiqueSyno($tabLexique) {
    	global $db;
		// VP 4/02/10 : correction bug sur recherche extension lexique si pas de synonyme préférentiel
		$arrSyno=array();
                $arrSyno2=array();

    	// Extension aux termes synonymes préférentiels
    	$id_lex_list=implode(',',$tabLexique['ID']);

        //MB 28/06/12 : sécurité pour ne pas executer les requetes si id_lex_list est vide
        if(isset($id_lex_list) && !empty($id_lex_list)) {
            $sql = "select DISTINCT LEX_ID_SYN from t_lexique where ID_LEX in (".$id_lex_list.") AND LEX_ID_SYN<>0";
            $rows=$db->GetAll($sql);
            foreach($rows as $row) {$arrSyno[]=$row["LEX_ID_SYN"];}

            // VP 29/10/09 : Extension aux termes synonymes non préférentiels
            $sql = "select DISTINCT ID_LEX from t_lexique where LEX_ID_SYN in (".$id_lex_list.")";
            $rows=$db->GetAll($sql);
            foreach($rows as $row) {$arrSyno2[]=$row["ID_LEX"];}
            /*
            $sql4 = "select DISTINCT LEX_ID_SYN from t_lexique where ID_LEX in (".$id_lex_list.") AND LEX_ID_SYN<>0";
            $result4 = $db->Execute($sql4);
            if (!$result4 || $result4->RecordCount()==0) return false;
            while($list4 = $result4->FetchRow()) {$arrSyno[]=$list4["LEX_ID_SYN"];}
            return $arrSyno;
            */
        }
	if(count($arrSyno2)>0) $arrSyno=array_merge($arrSyno,$arrSyno2);
	if(count($arrSyno)>0) return $arrSyno;
	else return false;
    }

    /**
     * Extension lexicale verticale (hiérarchique)
     * IN : tab Lexique, recursif (opt), tableau d'IDs direct (utilisé uniquement en mode récursif)
     * 		Si recursif, tous les termes de hiérarchie inférieure son ramenés
     * deep: niveau de profondeur de la recherche
     * OUT : tab Lexique avec ID étendus
   	 * NOTE : le mode récursif peut ramener bcp de termes !
     */

    protected function extLexiqueHier($tabLexique,$recursif=false,$arrSons=array(),$deep=-1) {
   		global $db;
   		if (!$arrSons) $id_lex_list=implode(',',$tabLexique['ID']);
   		else $id_lex_list=implode(',',$arrSons);
   		if (!$id_lex_list) return false;
   		$sql3 = "select DISTINCT ID_LEX from t_lexique where LEX_ID_GEN in (".$id_lex_list.")";
        $result3 = $db->Execute($sql3);
            if (!$result3 || $result3->RecordCount()==0) return false;
            while($list3 = $result3->FetchRow()){
				$arrSons[]=$list3['ID_LEX'];
                if ($recursif && ($deep > 0)) {
                    $deep--;
                    $sons=$this->extLexiqueHier($tabLexique,true,array($list3['ID_LEX']),$deep);
                }	
            	if ($recursif && $deep==-1) {
                    $sons=$this->extLexiqueHier($tabLexique,true,array($list3['ID_LEX']));                 
                }
				if ($sons) $arrSons=array_merge($arrSons,$sons);
            }

        return $arrSons;
    }
	
	protected function extCategorie($arrWords, $arrExcludeWords=null, $onlySyno=false, $restrict_to_type = null, $deep = -1) {
		global $db;
        if(empty($deep)) $deep=-1;
		foreach($arrWords as $idx=>$w){
			$arrCat[$idx]=$this->getIDFromCategorie($w,$restrict_to_type);

			if($arrCat[$idx] != array()){
				$extHier=$this->extCatHier($arrCat[$idx],true,'',$deep);
				if (!empty($extHier)) $arrCat[$idx]['ID']=array_merge($arrCat[$idx]['ID'],$extHier);


				$this->getTermesFromCategorie($arrCat[$idx], $arrExcludeWords);


				if (isset($arrCat[$idx]['HIGHLIGHT']) && !empty($arrCat[$idx]['HIGHLIGHT']) && is_array($arrCat[$idx]['HIGHLIGHT']))
				{
					if (!in_array($tabCrit['VALEUR'],$arrCat[$idx]['HIGHLIGHT']) && !empty($tabCrit['VALEUR']))
						$arrCat[$idx]['HIGHLIGHT'][]=$tabCrit['VALEUR'];
				}
			}
		}
    	return $arrCat;
    }
	
	
    private function getIDFromCategorie($word, $restrict_to_type) {

			global $db;

			$strVals.="(".$word.") ";

			// MS 07/05/14 - Si on détecte des doubles quotes de part et d'autre du mot => on empeche la recherche fulltext => le terme doit être recherché tel quel
			if(substr($word,0,1) == '"' && substr($word,-1) == '"'){
				$flag_prevent_FT = true ;
			}

			//}
			//By LD 12 12 08 => espacement des ' pour effectuer la recherche
			//$strVals=str_replace(array(",","'"),array(" "," ' "),$strVals);
			if ($this->params['RechercheCategorieMode']=='FULLTEXT' && (!isset($flag_prevent_FT))) { //MODE FULLTEXT de recherche des ID sur la base des mots renseignés
				if(isset($restrict_to_type) && !empty($restrict_to_type)){
					$where_cond = $db->getFullTextSQL(array($word,$restrict_to_type), array("CAT_NOM","CAT_ID_TYPE_CAT"));
				}else{
					$where_cond = $db->getFullTextSQL(array($word), array("CAT_NOM"));
				}
				$sql = "select DISTINCT ID_CAT from t_categorie
						where ".$where_cond;
			} else { //MODE "EXACT" par sql_op (like/=) de recherche des ID
			$sql= "select DISTINCT ID_CAT from t_categorie WHERE LOWER(CAT_NOM) ".$this->sql_op(strtolower($word),true);
			}

			if ($this->params['RechercheCategorieRestrictionLangage']==true) $sql.=" AND ID_LANG=".$db->Quote($_SESSION['langue']);
trace("sql IDFromCat");
		  trace($sql);
		  trace($strVals);
trace("sql IDFromCat");
            $result2 = $db->Execute($sql);
            if (!$result2) return false;
            while($list2=$result2->FetchRow()) $tabCateg['ID'][]=$list2["ID_CAT"];
			$result2->Close();
			return $tabCateg;
    }
	
	
	/**
	 * Récupère les termes pour une série d'IDs categorie.
	 * IN : tableau categorie, restriction à la langue en cours (opt)
	 * OUT : tableau lex MAJ (VALEURS & HIGHLIGHT)
	 * NOTE : a exécuter obligatoire APRES la fonction getIDFromCategorie.
	 */
	// VP 19/11/09 : ajout paramètre $arrExludeWords contenant les termes du lexique à exclure
	// VP 9/02/10 : récupératon termes de LEX_PATH
	protected function getTermesFromCategorie(&$tabCategorie,$arrExludeWords=null) {

        global $db;
        if(is_array($tabCategorie['ID'])) {

			$tabCategorie['ID']=array_unique($tabCategorie['ID']);
			$id_cat_list=implode(',',$tabCategorie['ID']);

             //MB 02/07/12 : sécurité pour ne pas executer les requetes si id_lex_list est vide
            if(isset($id_cat_list) && !empty($id_cat_list)) {
                $sql= "select DISTINCT CAT_NOM,CAT_PATH from t_categorie where ID_CAT in (".$id_cat_list.")";

                if ($this->params['RechercheCategorieRestrictionLangage']==true) $sql.=" AND ID_LANG=".$db->Quote($_SESSION['langue']);
                if(!empty($arrExludeWords)){
                    $sql.=" AND NOT ".$db->getFullTextSQL($arrExludeWords, array("CAT_NOM"));
                }
                $rs = $db->Execute($sql);
                if (!$rs) return false;
                while($list = $rs->FetchRow()) {
                    //$tabCategorie['VALEURS'].=" +(\"".str_replace("'"," \' ",$list["CAT_NOM"])."\") ";
					$tabCategorie['VALEURS'].=$db->getSqlExtension($list["CAT_NOM"]);
                    //mod by LD 6/11 : les termes sont systématiquement encadrés par des "" -> permet de conserver des termes composés lors du highlight
    //             	if (count($rs)<kMaxExtensionLexicale) $tabCategorie['HIGHLIGHT'][]='"'.str_replace(array(" ' ","\""),array("'",""),$list["CAT_NOM"]).'"';
                    // VP 11/02/10 : activation max extension lexicale
                    if (count($tabCategorie['HIGHLIGHT'])<kMaxExtensionLexicale) {
                        $mot=str_replace(array(" ' ","\""),array("'",""),$list["CAT_NOM"]);
                        $tabCategorie['HIGHLIGHT'][]='"'.$mot.'"';
                    }
                }
                return $tabCategorie;
            } else {
                return false;
            }
        }
    }
	
	
	
    /**
     * Extension catégories verticale (hiérarchique)
     * IN : tab Catégorie, recursif (opt), tableau d'IDs direct (utilisé uniquement en mode récursif)
     * 		Si recursif, tous les termes de hiérarchie inférieure son ramenés
     * deep: niveau de profondeur de la recherche
     * OUT : tab Catégorie avec ID étendus
   	 * NOTE : le mode récursif peut ramener bcp de termes !
     */

    protected function extCatHier($tabCat,$recursif=false,$arrSons=array(),$deep=-1) {
   		global $db;
   		if (!$arrSons) $id_cat_list=implode(',',$tabCat['ID']);
   		else $id_cat_list=implode(',',$arrSons);
   		if (!$id_cat_list) return false;
   		$sql3 = "select DISTINCT ID_CAT from t_categorie where CAT_ID_GEN in (".$id_cat_list.")";
        $result3 = $db->Execute($sql3);
		if (!$result3 || $result3->RecordCount()==0) return false;
		while($list3 = $result3->FetchRow()){
			$arrSons[]=$list3['ID_CAT'];
			if ($recursif && ($deep > 0)) {
				$deep--;
				$sons=$this->extCatHier($tabCat,true,array($list3['ID_CAT']),$deep);
			}	
			if ($recursif && $deep==-1) {
				$sons=$this->extCatHier($tabCat,true,array($list3['ID_CAT']));                 
			}
			if ($sons) $arrSons=array_merge($arrSons,$sons);
		}
        return $arrSons;
    }



   /**
    * Recherche sur un champ de la table avec un ou plusieurs valeurs
    * IN : tab de Critères, prefixe table (opt)
    * OUT : sql + etape + highlight
    * NOTE : structure du tab de Critères
    * 	FIELD : un seul champ
    * 	VALEUR : un ou plusieurs valeurs
    * 	VALEUR2 : prefixe => remplace le préfixe par défaut. Certaines recherches portent en effet sur des tables liées ou champs
    * 				calculés qui n'ont pas de préfixe. EX: la recherche lexique peut filtrer le nb de docs liés
    * NOTE2 : cette recherche accepte les critères en HAVING (=restriction APRES un group by)
    *
    */
    protected function chercheChamp($tabCrit,$prefix=null,$keepIntact=false){ // Cherche VALS dans 1 CHAMP de la table PRINC

        global $db;
       		$prefix=(!empty($prefix)?$prefix.".":"");
			// VP 18/05/10 : test sur empty plutôt que isset
        	//if (isset($tabCrit['VALEUR2'])) $prefix=$tabCrit['VALEUR2']; // 'forçage' du préfixe. Note: pas de test empty car on peut vouloir passer un préfixe vide
			if (!empty($tabCrit['VALEUR2']))
				$prefix=$tabCrit['VALEUR2'];

			if (isset($tabCrit['VALEUR2']) && $tabCrit['VALEUR2']=='NO_PREFIX') // si valeur2 est egale a NO_PREFIX alors on garde le prefix vide
				$prefix='';

			if(strpos($tabCrit['FIELD'],',')!==false && strpos($tabCrit['FIELD'],'NULLIF')===false){ // NB 24 11 2015 SI NULLIF est utilisé dans le $tabCrit['FIELD'], la req ne dois pas etre découpé par ','
				$tabCrit['FIELD'] = explode(',',$tabCrit['FIELD']);
			}
			
			if (is_array($tabCrit['VALEUR']) && count($tabCrit['VALEUR']) == 1)
				$tabCrit['VALEUR'] = $tabCrit['VALEUR'][0];

			//PC 08/12/10 : recherche sur champ vide avec le caratère #
			if (!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0){				
				if ($tabCrit['OP']=='HAVING')
					$this->sqlSuffixe.= " AND (".$prefix.$tabCrit['FIELD']." != 0)";
				else {	
					$this->sqlRecherche.= " ".$tabCrit['OP']." (";
					if(is_array($tabCrit['FIELD'])){
						foreach($tabCrit['FIELD'] as $idx=>$fld){
							if($idx!=0){$this->sqlRecherche.=" OR ";}
							$this->sqlRecherche.="( ".$prefix.$fld." IS NULL OR trim(".$prefix.$tabCrit['FIELD']."') LIKE '')";
						}
					}else{
						$this->sqlRecherche.=$prefix.$tabCrit['FIELD']." IS NULL OR trim(".$prefix.$tabCrit['FIELD']."') LIKE ''";
					}
					$this->sqlRecherche.=")";
				}
			}elseif ($tabCrit['OP']!='HAVING'){
			//casttostring est défini dans class connection pour forcer le typage sous postgre
				if(is_array($tabCrit['FIELD'])){
					$this->sqlRecherche .=  " ".$tabCrit['OP']."(";
					foreach($tabCrit['FIELD'] as $idx=>$fld){
						if($idx!=0){$this->sqlRecherche.=" OR ";}
						$this->sqlRecherche.=" (".$prefix.$db->cast_to_string($fld)." ".$this->sql_op($tabCrit['VALEUR'],$keepIntact).")";
					}
					$this->sqlRecherche .= ")";
				}else{
					$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$db->cast_to_string($tabCrit['FIELD'])." ".$this->sql_op($tabCrit['VALEUR'],$keepIntact).")";
				}
			}
            else{
				if(is_array($tabCrit['FIELD'])){
					$this->sqlSuffixe .= " AND (";
					foreach($tabCrit['FIELD'] as $idx=>$fld){
						if($idx!=0){$this->sqlSuffixe.=" OR ";}
						$this->sqlSuffixe.= "(".$prefix.$fld." ".$this->sql_op($tabCrit['VALEUR'],$keepIntact).")";
					}
					$this->sqlSuffixe .= ")";
				}else{
					$this->sqlSuffixe.= " AND (".$prefix.$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],$keepIntact).")";
				}
			}
			// VP 24/07/09 : traitement cas où $tabCrit['VALEUR'] est un tableau
			if (is_array($tabCrit['VALEUR'])){
				if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(',',$tabCrit['VALEUR'])." ";
				$highlight[$tabCrit['FIELD']]=$tabCrit['VALEUR'];
			}else{
				if (strcmp(trim($tabCrit['VALEUR']), "#") == 0 && empty($tabCrit['NOHIST']))
					$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".(defined("kVide")?kVide:"vide")." ";
				elseif(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".(isset($tabCrit['LIB'])?$tabCrit['LIB']:'')." : ".$tabCrit['VALEUR']." ";
				//$highlight[$tabCrit['FIELD']]=array(str_replace(array(' +',' -','"'),' ',$tabCrit['VALEUR']));
				//By LD 04/06/08 => pas de remplacement dans les highlight
				$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);
			}

        	 if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=array_merge($this->tabHighlight, $highlight);
    }

	protected function chercheChampStrict($tabCrit,$prefix=null,$keepIntact=false){ // Cherche VALS dans 1 CHAMP de la table PRINC
        global $db;
       	$prefix=(!empty($prefix)?$prefix.".":"");
		if (!empty($tabCrit['VALEUR2']))
			$prefix=$tabCrit['VALEUR2'];

		if ($tabCrit['VALEUR2']=='NO_PREFIX')
			$prefix='';

		if (!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0)
			$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." IS NULL OR trim(".$prefix.$tabCrit['FIELD'].") LIKE '')";
		elseif ($tabCrit['OP']!='HAVING') {
			$val = $db->Quote(preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),trim($tabCrit['VALEUR'])));
			$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']."  = $val)";
		}
        else
            $this->sqlSuffixe.= " AND (".$prefix.$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],$keepIntact).")";

        if (is_array($tabCrit['VALEUR'])){
			if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(',',$tabCrit['VALEUR'])." ";
			$highlight[$tabCrit['FIELD']]=$tabCrit['VALEUR'];
		}else{
			if (strcmp(trim($tabCrit['VALEUR']), "#") == 0 && empty($tabCrit['NOHIST']))
				$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".(defined("kVide")?kVide:"vide")." ";
			elseif(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
			//$highlight[$tabCrit['FIELD']]=array(str_replace(array(' +',' -','"'),' ',$tabCrit['VALEUR']));
			//By LD 04/06/08 => pas de remplacement dans les highlight
			$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);
		}

         if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
    }

/**
* Recherche sur un champ de la table avec un ou plusieurs valeurs
* IN : tab de Critères, prefixe table (opt)
* OUT : sql + etape
* NOTE : structure du tab de Critères
* 	FIELD : un seul champ
* 	VALEUR : un ou plusieurs valeurs
*
*/
// VP 24/07/09 : création fonction
protected function chercheChampBooleen($tabCrit,$prefix=null){ // Cherche VALS dans 1 CHAMP de la table PRINC

	global $db;
	$prefix=(!empty($prefix)?$prefix.".":"");

	$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
	// VP 4/02/10 : correction bug valeur booléenne
	if (is_array($tabCrit['VALEUR'])) $valeur=$tabCrit['VALEUR'][0];
	else $valeur=$tabCrit['VALEUR'];
	if($valeur=="1") $valeur=kOui;
	else $valeur=kNon;
	if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$valeur." ";
	// Pas d'highlight
	//$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);
	//if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
}



    /**
     * Fonction pour chercher dans une table annexe en se basant sur un ID
     * IN : tableau critère,
     * 		préfixe de table (opt),
     * 		flag recherche aussi dans les pères(opt :
     * 			Si TRUE, alors on cherchera avec ID=VAL et ID_GEN=VAL. Ne convient qu'aux tables hiérarchiques !
     * OUT : sql + etape + highlight
     * NOTES : le tableau de critère doit être le suivant:
     * 		FIELD : nom du champ de 'valeur' dans la table annexe (ex: ETAT_DOC)  => détermine aussi le nom du champ ID de la table annexe, de la table annexe
     * 		VALEUR : valeur type ID => accepte indifféremment une valeur seule, une liste (avec virgules) ou un tableau de valeurs (1 dimension)
     * 		VALEUR2 : ID_LANG si nécessaire (à utiliser pour les tables où la langue est présente)
     * La récupération des libellés correspondant aux ID est faite préférentielle par scan des tables de référence (session)
     * ou sinon par un ordre SQL parallèle. Cette(ces) valeur(s) est(sont) mise(s) dans les highlight et etape.
     *
     * Si l'ID_LANG n'est pas spécifié dans une table qui en contient un (ex : t_type_doc), les résultats peuvent
     * varier (si les données varient suivant les langues) mais surtout, tous les libellés sont récupérés pour toutes les langues.
     * Ex: Votre recherche => Type document : Program / Programme / Excerpt / Extrait, etc...
     *
     */

    protected function chercheIdChamp($tabCrit,$prefix="",$includeFather=false){
       // $champ_table,$valeur_champ_form,$operateur="AND"


        global $db;
        $critereRech="";
        $prefix=($prefix!=''?$prefix.".":"");
        // Traitement des cas particuliers
        $table="t_".strtolower($tabCrit['FIELD']);
        $champ_id="ID_".$tabCrit['FIELD'];
        $champ_lie=$this->entity."_ID_".$tabCrit['FIELD'];


        if (!$includeFather) {
             $sql2 = "select ".$tabCrit['FIELD']." from ".$table." where ".$champ_id." ".$this->sql_op($tabCrit['VALEUR'], false, true)." ";
             $this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$champ_lie." ".$this->sql_op($tabCrit['VALEUR'], false, true).")";
        }else
        { // t_fonds : recherche dans le fonds demandé ET dans les fonds fils. Note:pour la recherche fonds, il y du spécifique dans chercheDoc
            $sql2 = "select ".$tabCrit['FIELD']." from ".$table." where ( ".$champ_id." ".$this->sql_op($tabCrit['VALEUR'], false, true)." OR ".$champ_id."_GEN ".$this->sql_op($tabCrit['VALEUR'], false, true).")";
            $this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix.$champ_lie." IN (SELECT ".$champ_id." FROM ".$table." where ".$champ_id." ".$this->sql_op($tabCrit['VALEUR'], false, true)." OR ".$champ_id."_GEN ".$this->sql_op($tabCrit['VALEUR']).")";
        }
        // Y a t-il une restriction de langue ? ajouter ?
        if(in_array($tabCrit['VALEUR2'],$_SESSION['arrLangues'])){ //vérif que la langue existe.
            $sql2 .= "AND ID_LANG=".$db->Quote($tabCrit['VALEUR2']);
        }

        //On lance une petite recherche dans les tables de référence en session.
        //Effectivement, il y a de bonnes chances que la table liée soit DEJA en mémoire
        //VP 27/07/09 : correction bug quand $tabCrit['VALEUR'] n'est pas un tableau
        if(!is_array($tabCrit['VALEUR'])) $tabCrit['VALEUR']=array($tabCrit['VALEUR']);
        $missing=false;
        foreach ($tabCrit['VALEUR'] as $idx) {
            $val=GetRefValue($table,$idx,$_SESSION['langue']);
            if ($val) $arrWords[]=$val." "; elseif ($missing==false) $missing=true;
        }

        //Si il manque au moins une valeur, tant pis, on passe par du SQL
        if (empty($arrWords) || $missing==true) {
            debug("sql", "pink", true);
            debug($sql2, "red", true);
            $critereRech=$db->GetAll($sql2);
            foreach ($critereRech as $idx=>$word) {$arrWords[]=$word[$tabCrit['FIELD']]." ";}
            debug($critereRech, "red", true);
        }
        if(!empty($arrWords)){
			//XB
            if (is_array($arrWords))
			     $arrWords=array_unique($arrWords);

            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(' / ',$arrWords)." ";

         	$highlight[$tabCrit['FIELD']]=$arrWords;
            if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
        }
    }


/**
 * Fonction de recherche dans une table liée à partir d'un texte (sans passer par un ID)
 * IN : tableau critères, préfixe de table
 * OUT : sql + etape + highlight
 * NOTES : structure du tableau de critères
 * 	FIELD : nom du champ de 'valeur' dans la table liée (ex : ETAT_DOC) => détermine aussi la table et le nom du champ lié (ex: DOC_ID_ETAT_DOC)
 *  VALEUR : (texte), peut être multivalué (séparateur virgule) (n'accepte pas de tableau de valeurs)
 * 	VALEUR2 : ID_LANG optionnel, si la table liée en comporte un.
 *
 * Si l'ID_LANG n'est pas spécifié dans une table qui en contient un (ex : t_type_doc), les résultats peuvent
 * varier si les données varient suivant les langues !
 *
 */
    protected function chercheChampTable($tabCrit,$prefix){

            global $db;
			$prefix=($prefix!=''?$prefix.".":"");
			// Traitement des cas particuliers
			$table="t_".strtolower($tabCrit['FIELD']);
			$champ_id="ID_".$tabCrit['FIELD'];
			$champ_lie=$this->entity."_ID_".$tabCrit['FIELD'];

            // Y a t-il une restriction de langue ? ajouter ?
            $sqlLang = "";
            //@update VG 12/07/2010 : remplacement de l'index 'tabLangues' par arrLangues, tabLangues étant apparamment vide, et arrLangues renseigné...
            //if(in_array($tabCrit['VALEUR2'],$_SESSION['tabLangues'])){
            if(in_array($tabCrit['VALEUR2'],$_SESSION['arrLangues'])){
                $sqlLang= " AND ".$prefix."ID_LANG=".$db->Quote($tabCrit['VALEUR2']);
            }

            $this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$champ_lie." IN (SELECT $champ_id FROM ".$table;
 			$this->sqlRecherche.= " WHERE ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR']).") $sqlLang)";

            //


            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";

        	$highlight[$tabCrit['FIELD']]=array(str_replace(array(' +',' -','"'),' ',$tabCrit['VALEUR']));
            if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
    }




    /**
     * Recherche par DATE dans la table principale
     * IN : tab critère, prefixe tables (opt)
     * OUT : sql + etape + highlight
     * NOTE :
     * 	le tableau de critère aura la structure suivante.
     * 		FIELD : un ou plusieurs champs (? a voir)
     * 		VALEUR : date de début ou date
     * 		VALEUR2 (opt) : si spécifié dante de fin, la recherche devient BETWEEN
     */
    protected function chercheDate($tabCrit,$prefix=""){
        //$champ_table,$valeur_date1, $valeur_date2,$operateur="AND"
        global $db;
		$prefix=($prefix!=""?$prefix.".":"");
       	if (!is_array($tabCrit['VALEUR'])) $tabCrit['VALEUR']=(array)$tabCrit['VALEUR'];

       	if (empty($tabCrit['VALEUR'])) return;

		// VP 24/05/10 : reprise fonctionnement recherche date si plusieurs champs de recherche
       	//$sql= " ".$tabCrit['OP']." ("; //prépa sql
		$sql="";
		$arrFldDate=explode(',',$tabCrit['FIELD']);
       	foreach ($tabCrit['VALEUR'] as $myDate) {
			if (!is_array($myDate) && strcmp(trim($myDate), "#") == 0) {
				foreach ($arrFldDate as $i=>$date) { //boucle sur les champs
					if($i>0) $sql.=" OR ";
					$sql.="(".$prefix.$date." is null)";
				}
				if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".kVide;
			} else {
				// GT: : on supprime les etoiles (Ex: 196* => 196)
				$myDate=str_replace('*','',$myDate);
				$valeur_date1=convDate($myDate);
				$valeur_date2=convDate($tabCrit['VALEUR2']);
				if (empty($valeur_date1)) break; //pb avec la date ? on sort

				if(strlen($valeur_date2)>0) {
					//PC 02/09/11 : correction du bug qui ne prenait pas toutes les dates d'un intervalle
					//VP 18/04/13 : conversin date début
					$valeur_date1 = convToFirstDate($valeur_date1);
					$valeur_date2_sql = convToLastDate($valeur_date2);
					foreach ($arrFldDate as $i=>$date) { //boucle sur les champs
	//					$innersql[]="(".$prefix.$date." between ".$db->Quote($valeur_date1)." AND ".$db->Quote($valeur_date2).")";
	//					if (count($arrFldDate)!=($i+1)) $sql.=" OR ";
						if($i>0) $sql.=" OR ";
						$sql.="(".$prefix.$date." between ".$db->Quote($valeur_date1)." AND ".$db->Quote($valeur_date2_sql).")";
					}

					if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".kEntre." (".$valeur_date1." ".kEt." ".$valeur_date2.")  ";
				}
				else{

					foreach ($arrFldDate as $i=>$date) {
	//					$innersql[]="(".$prefix.$date." like '".$valeur_date1."%')";
	//					if (count($arrFldDate)!=($i+1)) $sql.=" OR ";
						$highlight[$date]=array($valeur_date1);
						if($i>0) $sql.=" OR ";
						$sql.="(".$db->Concat($prefix.$date, "''")." like '".$valeur_date1."%')"; //Concaténation avec un chaine vide afin de forcer la conversion en string (bug postgres)
					}

					if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$myDate." ";
					if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
				}
			}
	    }
		if(!empty($sql)){
			$this->sqlRecherche.=" ".$tabCrit['OP']." (".$sql.") ";
		}
//        if (!empty($innersql)) {
//        	$this->sqlRecherche.=$sql.implode(" OR ",$innersql).")"; //si sql "interne" on ajoute
//        }
   }

   /**
    * Compare une date avec la valeur selon les critères > et <
    * IN : tableau critère, prefixe table (opt)
    * OUT : sql + etape (pas de highlight)
    * NOTE : structure du tableau de critères
    * 		FIELD : champ (un seul)
    * 		VALEUR : date (une seule)
    * 		VALEUR2 : opérateur (accepte uniquement >,<,>=,<=)
    */
    //@update VG 09/06/2010 : modif pour prendre en compte plusieurs champs date
   protected function compareDate ($tabCrit,$prefix="") {
   		global $db;
   		$date=convDate($tabCrit['VALEUR']);

   		if ($date=="") return false;
   		$sens=$tabCrit['VALEUR2'];

   		if (!in_array($sens,array(">","<",">=","<="))) return false;

   		$prefix=($prefix!=""?$prefix.".":"");
   		$sql='';

   		$arrFldDate=explode(',',$tabCrit['FIELD']);
		foreach ($arrFldDate as $i=>$dateField) {
			if($i>0) $sql.=" OR ";
			$sql.="(".$prefix.$dateField.$sens.$db->Quote($date).")";
		}
   		$this->sqlRecherche.=" ".$tabCrit['OP']." (".$sql.") ";
   		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".$sens." ".$tabCrit['VALEUR']." ";
   }

   /**
    * Compare une durée avec la valeur selon les critères > et <
    * IN : tableau critère, prefixe table (opt)
    * OUT : sql + etape (pas de highlight)
    * NOTE : structure du tableau de critères
    * 		FIELD : champ (un seul)
    * 		VALEUR : date (une seule)
    * 		VALEUR2 : opérateur (accepte uniquement >,<,>=,<=)
    */
   protected function compareDuree ($tabCrit,$prefix="") {
   		global $db;
   		$duree=fDuree($tabCrit['VALEUR']);
   		if ($duree=="") return false;
   		$sens=$tabCrit['VALEUR2'];
   		if (!in_array($sens,array(">","<",">=","<="))) return false;
   		$prefix=($prefix!=""?$prefix.".":"");

   		$this->sqlRecherche.=$tabCrit['OP']." ".$prefix.$tabCrit['FIELD'].$sens.$db->Quote($duree);
   		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".$sens." ".$tabCrit['VALEUR']." ";
   }


   /**
    * Compare une valeur selon les critères > et <
    * IN : tableau critères, prefix table (opt)
    * OUT : sql + etape + highlight (pertinent ?)
    * NOTE : structure du tableau de critères
    * 		FIELD : champ (1 seul)
    * 		VALEUR : valeur (1 seule)
    * 		VALEUR2 : opérateur (parmi <,>,>=,<=)
    * Si la valeur est un nombre, la comparaison est mathématique.
    * Si la valeur est une chaine, la comparaison est une comparaison de chaine.
    */

   protected function compareValeur($tabCrit,$prefix="") {
   		global $db;
     	$prefix=($prefix!=''?$prefix.".":"");
		$sens=$tabCrit['VALEUR2'];
		if (is_numeric($tabCrit['VALEUR'])) $valeur=$tabCrit['VALEUR']; else $valeur=$db->Quote($tabCrit['VALEUR']);
   		if (!in_array($sens,array(">","<",">=","<="))) return false;
		$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD'].$sens.$valeur.")";
        if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".$sens." ".$tabCrit['VALEUR']." ";
    	$highlight[$tabCrit['FIELD']]=array(str_replace(array(' +',' -','"'),' ',$tabCrit['VALEUR']));
    	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;

   }



    /**
     * Recherche par Duree dans la table principale (OBSOLETE, chercheDate(true) à la place)
     * IN : tab Critère, préfixe table (opt)
     * OUT : sql + etape + highlight
     * NOTE : structure du tableau de critère
     * 		FIELD : un seul champ
     * 		VALEUR  : durée
     * 		VALEUR2 : (opt) si spécifié, recherche en BETWEEN entre les 2 durée, sinon recherche en durée exacte
     */
    protected function chercheDuree($tabCrit,$prefix=""){
        global $db;
        if(strlen($tabCrit['VALEUR'])>0) {
        	$prefix=($prefix!=""?$prefix.".":"");
            if(strlen($tabCrit['VALEUR2'])>0) {
                $this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." between ".$db->Quote(fDuree($tabCrit['VALEUR']))." AND ".$db->Quote(fDuree($tabCrit['VALEUR2'])).")";
                if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".kEntre." (".$tabCrit['VALEUR']." ".kEt." ".$tabCrit['VALEUR2'].") ";
            }
            else{
                $this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." = ".$db->Quote(fDuree($tabCrit['VALEUR'])).")";
                if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";

            	$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);
            	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
            }
        }
    }



    /**
     * Cherche parmi la table des valeurs pour un type et une ou plusieurs valeurs données
     * IN : tab Critères, prefixe table (opt)
     * OUT : sql + etape + highlight
     * NOTE : structure de la table critère
     * 		FIELD : type de valeur (cf. ID_TYPE_VAL)
     * 		VALEUR : valeur(s) cherchée(s)
     *
     */

	// VP 2/3/09 : ajout paramètre keepIntact
	// VP 21/3/09 : paramètre keepIntact à true par défaut
    protected function chercheVal($tabCrit,$prefix="",$keepIntact=true){

            global $db;
            $prefix=($prefix!=""?$prefix.".":"");
            //@update VG 07/06/2010 : il peut y avoir différentes "entités" selon les tables...
			$entity = !empty($this->entity2)?$this->entity2:$this->entity;
            $this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_".$entity." ";

            //PC 17/12/2010 : recherche sur champ vide avec le caratère #
			if (!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0){
				$this->sqlRecherche.= " not in (select ID_".$entity." from t_".strtolower($entity)."_val where ID_VAL ";
				if(strpos($tabCrit['FIELD'],'%')!==false){
					$this->sqlRecherche.= " in (select ID_VAL from t_val where ".$db->getNormalizedLike('VAL_ID_TYPE_VAL',$tabCrit['FIELD'])."))";				
				}else{
					$this->sqlRecherche.= " in (select ID_VAL from t_val where VAL_ID_TYPE_VAL=".$db->Quote($tabCrit['FIELD'])."))";
				}
				if(empty($tabCrit['NOHIST']))
					$this->etape .= $this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".(defined("kVide")?kVide:"vide")." ";
			}
			else {
				if(strpos($tabCrit['FIELD'],'%')!==false){
					$this->sqlRecherche.= " in (select ID_".$entity." from t_".strtolower($entity)."_val where ".$db->getNormalizedLike('ID_TYPE_VAL',$tabCrit['FIELD'])." AND ID_VAL ";
	            }else{
					$this->sqlRecherche.= " in (select ID_".$entity." from t_".strtolower($entity)."_val where ID_TYPE_VAL=".$db->Quote($tabCrit['FIELD'])." AND ID_VAL ";				
				}
				//by LD 06/06/08=> sql_op passé en mode keepIntact pour ne pas analyser la chaine, notamment pb de spaceQuote
	            // VP 23/12/08 : ajout recherche en fulltext
				if($tabCrit['TYPE']=="VF") $this->sqlRecherche.= " in (select ID_VAL from t_val where ".$db->getFullTextSQL(array($tabCrit['VALEUR']), array("VALEUR"))."))";
	            else $this->sqlRecherche.= " in (select ID_VAL from t_val where VALEUR ".$this->sql_op($tabCrit['VALEUR'],$keepIntact)."))";
	            if (!$tabCrit['LIB']) $tabCrit['LIB']=GetRefValue('t_type_val',$tabCrit['FIELD'],$_SESSION['langue']);

	            //$highlight["t_".strtolower($this->entity)."_val"]=array(str_replace(array(' +',' -','"'),' ',$tabCrit['VALEUR']));
	        	$highlight["VALEUR"]=array(str_replace(array(' +',' -','"',','),' ',$tabCrit['VALEUR']));
	        	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;

	            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
			}
    }



  /**
     * Cherche parmi la table des valeurs pour un type et un ID
     * IN : tab Critere, prefixe table (opt), RECURSIF o/n (on cherche ds chaque sous valeur récursivement)
     * OUT : sql + etape + highlight
     * NOTE : structure de la table critère
     * 		FIELD : type de valeur (cf. ID_TYPE_VAL)
     * 		VALEUR : valeur(s) cherchée(s)
	 * 		intraAND : ET dans les valeurs cherchées (défaut)
     *
     */
	// VP 15/04/10 : changement intraAND par défaut à false
    protected function chercheIdVal($tabCrit,$prefix="",$recursif=false,$intraAND=false){

            global $db;

            $prefix=($prefix!=""?$prefix.".":"");

			//PC 21/02/13 : permet d'utiliser un autre champs de recherche
			$field = $tabCrit['FIELD'];
			if (isset($tabCrit['VALEUR2']) && !empty($tabCrit['VALEUR2'])) $field = $tabCrit['VALEUR2'];

			$strVals=$tabCrit['VALEUR'];
			//@update VG 07/06/2010 : il peut y avoir différentes "entités" selon les tables...
			$entity = !empty($this->entity2)?$this->entity2:$this->entity;

			if($intraAND && is_array($tabCrit['VALEUR'])){
				$this->sqlRecherche.= " ".$tabCrit['OP']." ( ";
				foreach($tabCrit['VALEUR'] as $u=>$valeur){
					$arrVal['ID']=(array)$valeur;
					$arrExtHier=$this->extValHier($arrVal,false);
					if ($arrExtHier) $arrVal['ID']=array_merge($arrVal['ID'],$this->extValHier($arrVal,true));
					if($u>0) $this->sqlRecherche.= " AND ";
					$this->sqlRecherche.= $prefix."ID_".$entity." in
					(select ID_".$entity." from t_".strtolower($entity)."_val where ID_VAL ".$this->sql_op($arrVal['ID'])." AND ID_TYPE_VAL=".$db->Quote($field).")";
				}
				$this->sqlRecherche.=")";
			}else{
				$arrVal['ID']=(array)$tabCrit['VALEUR'];
				$arrExtHier=$this->extValHier($arrVal,false);
				if ($arrExtHier) $arrVal['ID']=array_merge($arrVal['ID'],$this->extValHier($arrVal,true));

				//PC 30/06/11 : remplacement du signe égal par sql_op
				$this->sqlRecherche.= " ".$tabCrit['OP']." ( ".$prefix."ID_".$entity." in
				(select ID_".$entity." from t_".strtolower($entity)."_val where ID_VAL ".$this->sql_op($arrVal['ID'])." AND ID_TYPE_VAL".$this->sql_op($field).")
				)";
			}
            //Valeur en clair
			if(is_array($tabCrit['VALEUR'])){
				// VP 14/9/09 : correction bug sur étape avec valeur multiple
				$sql2 = "select VALEUR from t_val where ID_VAL in (".implode(",",$tabCrit['VALEUR']).") AND ID_LANG=".$db->Quote($_SESSION['langue']);
				$rows=$db->GetAll($sql2);
				$valeur="";
				foreach ($rows as $row) {$valeur.=", ".$row['VALEUR'];}
				$valeur=substr($valeur,1);
			}else{
                //VP 25/11/16 : remplacement du signe égal par sql_op
				$sql2 = "select VALEUR from t_val where ID_VAL ".$this->sql_op($tabCrit['VALEUR'])." AND ID_LANG=".$db->Quote($_SESSION['langue']);
                $rows=$db->GetAll($sql2);
                $valeur="";
                foreach ($rows as $row) {$valeur.=", ".$row['VALEUR'];}
                $valeur=substr($valeur,1);
			}

           if (!$tabCrit['LIB']) $tabCrit['LIB']=GetRefValue('t_type_val',$tabCrit['FIELD'],$_SESSION['langue']);


            //$this->etape.=$operateur." ".$this->tab_libelles[$type_index].":\"".$lex_terme."\" ";
            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$valeur." ";

            //$highlight["t_".strtolower($this->entity)."_val"]=array(str_replace(array(' +',' -','"',','),' ',$valeur));
            $highlight["VALEUR"]=array(str_replace(array(' +',' -','"',','),' ',$valeur));

        	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;

    }

     function extValHier($tabVal,$recursif=false,$arrSons=array()) {
   		global $db;
		
   		if (!$arrSons && is_array($tabVal) && count($tabVal)>0) $id_val_list=implode(',',$tabVal['ID']);
   		elseif(is_array($arrSons) && count($arrSons)>0) $id_val_list=implode(',',$arrSons);
   		if (!$id_val_list) return false;
   		$sql3 = "select DISTINCT ID_VAL from t_val where VAL_ID_GEN in (".$id_val_list.") ";
        $result3 = $db->Execute($sql3);

            if (!$result3 || $result3->RecordCount()==0) return false;
            while($list3 = $result3->FetchRow()){

				$arrSons[]=$list3['ID_VAL'];
				if ($recursif) {trace("recursif");$sons=$this->extValHier($tabVal,true,array($list3['ID_VAL']));}
				if ($sons) $arrSons=array_merge($arrSons,$sons);
            }
        return $arrSons;
    }

    // Filtrage des valeurs rentr?es pour une recherche full-text
    protected function ft_filtre($valeur){
		// Recherche des "\"," de retour d'index
		if(strpos($valeur,"\",")>0) {
			$valeur="+(".str_replace("\",","\") +(",$valeur).")";
		}
        return $valeur;
    }



    // Lexique (valeur)
    protected function chercheLex($tabCrit,$prefix="",$extLexiquePre=true,$extLexiquePost=true){
    	 global $db;
         $prefix=($prefix!=""?$prefix.".":"");
   		 //PC 08/12/10 : recherche sur champ vide avec le caratère #
		if (!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0){
			$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."ID_DOC not in (select ID_DOC from t_doc_lex where ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD'])."))";
			if(empty($tabCrit['NOHIST']))
				$this->etape .= $this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".(defined("kVide")?kVide:"vide")." ";

		}
        elseif(!empty($tabCrit['VALEUR'])) {

			if ($extLexiquePre) { // Extension lexicale PRE : analyse de la chaine pour trouver les ID_LEX
				$arrWords = $this->analyzeString2($tabCrit['VALEUR'],$op);
		 		$arrLex=$this->extLexique($arrWords,null,false,null,$tabCrit['OPTIONS']['DEEP']);
			}

			$strVals='';
			$arrHL= array();

			if ($arrLex) { // Si extension demandée ET existe
				foreach($arrLex as $idx => $val){
					$strVals.=$arrLex[$idx]['VALEURS'];

					$arrHL = array_merge($arrHL, $arrLex[$idx]['HIGHLIGHT']);
				}
			}

			else { //Pas d'extension PRE, on se contente de retrouver les ID_LEX par fulltext
				  $arrHL=array(0=>$strVals);
		 		  $sql2 = "select DISTINCT ID_LEX from t_lexique where ".$db->getFullTextSQL(array($strVals), array("LEX_TERME"));

			  $arrIDs=$db->GetAll($sql2);
			  if (!empty($arrIDs)) foreach ($arrIDs as $lx) $arrLex[0]['ID'][]=$lx['ID_LEX'];
			  else $arrLex[0]['ID']=array('-1');

			}


			//Extension lexique Post Recherche : un ou plusieurs ID ont été trouvés, on les étend => comme dans chercheIdLex
			if ($extLexiquePost=='true') {
				foreach($arrLex as $idx=>$lex){
					$extHier=$this->extLexiqueHier($arrLex['$idx'],true);
					if (!empty($extHier)) $arrLex[$idx]['ID']=array_merge($arrLex[$idx]['ID'],$extHier);

					$extSyno=$this->extLexiqueSyno($arrLex[$idx]);
					debug($extSyno, 'red', true);
					if (!empty($extSyno)) $arrLex[$idx]['ID']=array_merge($arrLex[$idx]['ID'],$extSyno);
					$this->getTermesFromLexique($arrLex[$idx]);
				}
			}

            $lexCritereSuffixe='';

            if($tabCrit['VALEUR2']){ // Un rôle est spécifié
                    //PC 12/04/13 : Ajout possibilité de chercher sur plusieurs rôles
                    if (strpos($tabCrit['VALEUR2'], ",")) {
                        $vals2 = explode(",", $tabCrit['VALEUR2']);
                        $lexCritereSuffixe.= " AND ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." AND DLEX_ID_ROLE in ('".implode("','", $vals2)."')";
                        if (empty($tabCrit['LIB']))
                            $tabCrit['LIB']=$db->GetOne("select ROLE from t_role where ID_ROLE=".$db->Quote($tabCrit['VALEUR2'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
                    }
                    else {
                        $lexCritereSuffixe.= " AND ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." AND DLEX_ID_ROLE=".$db->Quote($tabCrit['VALEUR2']);
                        if (empty($tabCrit['LIB']))
                            $tabCrit['LIB']=$db->GetOne("select ROLE from t_role where ID_ROLE=".$db->Quote($tabCrit['VALEUR2'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
                    }

            } else {
                    $lexCritereSuffixe.= " AND ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." ";
                    if (empty($tabCrit['LIB']))
                        $tabCrit['LIB']=$db->GetOne("select TYPE_DESC from t_type_desc where ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
            }



			//debug($arrLex,"red");
            $lexCriterePreReq='';

           	$lexCriterePreReq.= $prefix."ID_DOC in (select ID_DOC from t_doc_lex where ID_LEX ";

            $sqlLex='';

			foreach($arrLex as $idx => $lex ){
                if(is_array($op) && isset($op[$idx])){
                    switch ($op[$idx]) {
                        case '+':
                            $op_tmp='OR';
                            break;
                        case '-':
                            $op_tmp='AND NOT';
                            break;
                        default:
                            $op_tmp='AND';
                            break;
                    }
                } else {
                    $op_tmp='AND';
                }

                if($sqlLex!=''){
                    $sqlLex.=' '.$op_tmp.' ';
                }
                $sqlLex .= $lexCriterePreReq . $this->sql_op(implode(",",$arrLex[$idx]['ID'])) . $lexCritereSuffixe.")";

			}
			if (empty($arrLex[0]['ID'])) $arrLex[0]['ID'][] = "0";

			$this->sqlRecherche.= $tabCrit['OP']." (".$sqlLex.")";


			//LD 02 06 09 => traitement de field via sql_op pour pouvoir spécifier plusieurs TYPE_DESC dans le formulaire,
			//séparés par des virgules. Idem pour chercheIdLex

            //trace(print_r($this->sqlRecherche, 1));

        	//if (isset($arrHL) && empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight["LEX_TERME"][]=array_values($arrHL);
            $highlightField = "LEX_TERME";

            if (isset($tabCrit['FIELD'])) {
                $highlightField .= "$".$tabCrit['FIELD'];
            }
            if(isset($tabCrit['VALEUR2'])){
                $highlightField .= "$".$tabCrit['VALEUR2'];
            }

            if (isset($arrHL) && empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight[$highlightField][]=array_values($arrHL);

            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
        }
    }

	/**
	 * Permet une recherche lexicale avec les termes exacts
	 * @update VG 21/06/2010
	 * @update VG 29/07/2010 : prise en compte des lexiques "enfants", avec $extLexiqueHier
	 */
    protected function chercheLexExacte($tabCrit,$prefix="",$keepIntact=true, $extLexiqueHier=false, $extLexiqueSyno=false){
    		global $db;

            $prefix=($prefix!=""?$prefix.".":"");
            if (!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0){
            	$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."ID_DOC not in (select ID_DOC from t_doc_lex where ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD'])."))";
            	if(empty($tabCrit['NOHIST']))
            		$this->etape .= $this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".(defined("kVide")?kVide:"vide")." ";
            
            }
            elseif(!empty($tabCrit['VALEUR'])) {
	            //update VG 16/02/2012
	            $strVal = str_replace('"',"",$tabCrit['VALEUR']);
	            //$strVal = str_replace(array('"'),array(""),$tabCrit['VALEUR']);
	
				//@update VG 29/07/2010 : prise en compte des lexiques "enfants", avec $extLexiqueHier
				//$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC in ( select ID_DOC from t_doc_lex where ID_LEX IN ( select ID_LEX from t_lexique where ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." AND LEX_TERME = ".$db->Quote($strVal)."))";
				$sqlExt = '';
	
				if($extLexiqueHier || $extLexiqueSyno) {
					$sqlIdLex = "select ID_LEX from t_lexique where LEX_TERME  ".$db->like($strVal);
					$resultIdLex = $db->Execute($sqlIdLex);
					if($resultIdLex) {
						while($listIdLex=$resultIdLex->FetchRow()) $tabIdLex['ID'][]=$listIdLex["ID_LEX"];
						$resultIdLex->Close();
					}
				}
				$arrExtLexId=array();
				if(!empty($tabIdLex['ID'])){
					if($extLexiqueHier) {
						//On récupère les ID_LEX des lexique enfants
						$extHier=$this->extLexiqueHier($tabIdLex,true);
						if($extHier) $arrExtLexId=array_merge($arrExtLexId,$extHier);
					}
	
					if($extLexiqueSyno) {
	
			    		$extSyno=$this->extLexiqueSyno($tabIdLex);
				 		if ($extSyno) $arrExtLexId=array_merge($arrExtLexId,$extSyno);
	
					}
	
					if($arrExtLexId) $sqlExt = " OR ID_LEX IN (".implode(",",$arrExtLexId).")";
				}
	
				$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC in ( select ID_DOC from t_doc_lex where (ID_LEX IN ( select ID_LEX from t_lexique where LEX_TERME ".$db->like($strVal)." ) ".$sqlExt." ) ";
	            if($tabCrit['VALEUR2']){ // Un rôle est spécifié
					//PC 12/04/13 : Ajout possibilité de chercher sur plusieurs rôles
					if (strpos($tabCrit['VALEUR2'], ",")) {
						$vals2 = explode(",", $tabCrit['VALEUR2']);
						$this->sqlRecherche.= " AND ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." AND DLEX_ID_ROLE in ('".implode("','", $vals2)."')";
						if (empty($tabCrit['LIB']))
							$tabCrit['LIB']=$db->GetOne("select ROLE from t_role where ID_ROLE=".$db->Quote($tabCrit['VALEUR2'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
					}
					else {
						$this->sqlRecherche.= " AND ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." AND DLEX_ID_ROLE=".$db->Quote($tabCrit['VALEUR2']);
						if (empty($tabCrit['LIB']))
							$tabCrit['LIB']=$db->GetOne("select ROLE from t_role where ID_ROLE=".$db->Quote($tabCrit['VALEUR2'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
					}
				} else {
						$this->sqlRecherche.= " AND ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." ";
						if (empty($tabCrit['LIB']))
							$tabCrit['LIB']=$db->GetOne("select TYPE_DESC from t_type_desc where ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
				}
	
				$this->sqlRecherche.=")";
	
	            if (!$tabCrit['LIB']) $tabCrit['LIB']=$db->GetOne("select TYPE_DESC from t_type_desc where ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
	
				$highlightTmp=str_replace('"','',$tabCrit['VALEUR']);
	        	$highlight=array(str_replace(array(' +',' -',','),' ',$highlightTmp));
	
	            $highlightField = "LEX_TERME";
	
	            if (isset($tabCrit['FIELD'])) {
	                $highlightField .= "$".$tabCrit['FIELD'];
	            }
	            if(isset($tabCrit['VALEUR2'])){
	                $highlightField .= "$".$tabCrit['VALEUR2'];
	            }
	
	        	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight[$highlightField][]=$highlight;
	
	            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
			}
	}            


    /** Recherche dans le lexique par ID.
     * IN : tableau critère, préfixe, flag récursif
     * OUT
     * Tableau de critère structure
     * 		FIELD : id type desc (GEN, DE, etc).
     * 		VALEUR : id terme (unique)
     * 		VALEUR2 : rôle
     *	ATTENTION : PAS d'extension lexicale dans ce mode !
     */
    protected function chercheIdLex($tabCrit,$prefix="",$recursif=false){
    	//by LD 29/10/08 : test sur la valeur 0 non vide => cas d'un select multi sans item sélectionné
        if(is_array($tabCrit['VALEUR']) || (strlen($tabCrit['VALEUR'])>0 && $tabCrit['VALEUR'][0]!='')) {
            global $db;

 		$arrLex['ID']=(array)$tabCrit['VALEUR'];


		if($recursif == true){
			$extHier=$this->extLexiqueHier($arrLex,true);
			if (!empty($extHier)) $arrLex['ID']=array_merge($arrLex['ID'],$extHier);

				$extSyno=$this->extLexiqueSyno($arrLex);
			if (!empty($extSyno)) $arrLex['ID']=array_merge($arrLex['ID'],$extSyno);
		}


 		$this->getTermesFromLexique($arrLex);
		if ($arrLex) { // Si extension demandée ET existe
				$strVals=$arrLex['VALEURS'];
				$arrHL=$arrLex['HIGHLIGHT'];

			}


          	$prefix=($prefix!=""?$prefix.".":"");

            $this->sqlRecherche.= $tabCrit['OP']." (".$prefix."ID_DOC IN (select ID_DOC from t_doc_lex where ID_LEX ".$this->sql_op(implode(",",$arrLex['ID']));


                //Parametres suppl?mentaires :
                if($tabCrit['VALEUR2']){ // rôle renseigné
                    $this->sqlRecherche.= " AND  ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true)." AND DLEX_ID_ROLE=".$db->Quote($tabCrit['VALEUR2']);

                    if (empty($tabCrit['LIB'])) { // Si pas de libellé on le récupère en base

	                    $result = $db->GetOne("select ROLE from t_role where ID_ROLE=".$db->Quote($tabCrit['VALEUR2'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
	                    if ($result) $tabCrit['LIB']=$result;
                    }
                } else {
                    $this->sqlRecherche.= " AND ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'],true);
                    if (empty($tabCrit['LIB'])) {
	                    $result=$db->GetOne("select TYPE_DESC from t_type_desc where ID_TYPE_DESC ".$this->sql_op($tabCrit['FIELD'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
	                    if($result) $tabCrit['LIB']=$result;
                    }
                }

            $this->sqlRecherche.="))";

            if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight["LEX_TERME"][]=array_values($arrHL);

			//Requete pour retrouver le terme original, pas terrible
            //By LD 29/10/08 modifié pour gérer un tableau d'IDs en entrée => select multivalués
			//VP (26/02/09) : modif requête de récupération du terme original en cas d'ID seul
            if (!empty($tabCrit['VALEUR'])) {
            if(is_array($tabCrit['VALEUR'])) $sql2="SELECT LEX_TERME from t_lexique where ID_LANG=".$db->Quote($_SESSION['langue'])." AND ID_LEX IN (".implode(",",$tabCrit['VALEUR']).")";
				else $sql2="SELECT LEX_TERME from t_lexique where ID_LANG=".$db->Quote($_SESSION['langue'])." AND ID_LEX IN (".str_replace('"','',$tabCrit['VALEUR']).")";
				$libs=$db->GetAll($sql2);
			foreach ($libs as $_lib) $myOrgTerme[]=$_lib['LEX_TERME'];
			$myOrgTerme=implode(", ",$myOrgTerme);
			unset($libs);
            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$myOrgTerme." ";
            }

        }
    }

    /** Recherche de personnes par libellé
     *  IN : tableau critère, préfixe
     *  OUT : sql + arrHighlight
     * 	Tableau de critère : structure
     * 		VALEUR : nom de la ou des personnes (plusieurs valeurs permises, la chaine sera analysée)
     * 		FIELD : type DESC (GEN, PRD, etc)
     * 		VALEUR2 (optionnel) : type de personne (PM ou PP)
     * 	NOTE : l'analyse de chaine permet d'extraire une ou plusieurs personnes (selon la présence
     *   de guillemets, ou de wildcards).
     * 	NOTE 2 : comme la recherche porte sur le nom ET le prénom, les deux bénéficient du highlight.
     *
     */
    protected function cherchePers($tabCrit,$prefix="",$intersect=false) {
    	global $db;
    	$prefix=($prefix!=""?$prefix.".":"");
		
		// VP 10/03/2017 : prise en compte recherche personne pour matériels
		if (get_class($this)=='RechercheMat' ){
			$table_link = "t_mat_pers";
			$id_link = "ID_MAT";
		}else{
			$table_link = "t_doc_lex";
			$id_link = "ID_DOC";
		}
		
		//update VG 22/02/2012 : correction bug de recherche fulltext multivaluée

		/*$arrWords=$this->analyzeString($tabCrit['VALEUR'],$op);

		foreach ($arrWords as $i=>$word) if (strlen($word)<=2) unset ($arrWords[$i]); //strip des mots <2 lettres qui empèchent le FT
		foreach ($arrWords as $idx=>$word) {
			$strVals.=" ".$this->FToperator."(".$word.") ";
		}*/

		$arrWords=$this->analyzeString2($tabCrit['VALEUR'],$op);
		foreach ($arrWords as $i=>$word) if (strlen($word)<=2) {
			unset ($arrWords[$i]); unset($op[$i]); }//strip des mots <2 lettres qui empèchent le FT

		foreach ($arrWords as $idx=>&$word) {
			//By LD 12 12 08 => espacement des ' pour effectuer la recherche
			//$word=str_replace("'"," ' ",$word);
			$strVals.=" ".$op[$idx]."(".$word.") ";
			//MS 12/11/13 - On supprime les guillemets encadrants et on les remplace par des guillemets simples, pour qu'ils soient correctement traités par la fonction getFullTextSQL
			$word = trim($word,'"\'');
			$word = "'".$word."'";
		}

		$arrHL=array(0=>@implode(' ',$arrWords)); //Tableau des noms (libellé)
		$sql2 = "select DISTINCT ID_PERS from t_personne where ".$db->getFullTextSQL($arrWords, array("PERS_NOM", "PERS_PRENOM"), "", array(), $op);
		if (!empty($tabCrit['VALEUR2']))
			$sql2.=" AND PERS_ID_TYPE_LEX=".$db->Quote($tabCrit['VALEUR2']);
		$arrIDs=$db->GetAll($sql2);
		if (!empty($arrIDs)){
			foreach ($arrIDs as $pers){
				$arrLex['ID'][]=$pers['ID_PERS'];  // Tableau des personnes (ID)
			}
		} else {
			$arrLex['ID']=array('-1');
		}

		// MS - 12/11/13 Ajout d'un type "PX" dans la fonction ChooseType de la classe chercheDoc qui permet de lancer une recherche sur l'intersection de tout les termes entrés
		if($intersect){
			$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."$id_link IN (";
			$unit_sql = "(select $id_link from $table_link where ID_PERS ";
			$unit_suffix = "";
			if (!empty($tabCrit['FIELD'])) $unit_suffix.= " AND ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD']);
			//PC 15/03/2011 : Ajout du paramètre chOptions_role pour affiner la recherche sur personne avec leur role
			if (!empty($tabCrit['OPTIONS'])) $unit_suffix.= " AND DLEX_ID_ROLE=".$db->Quote($tabCrit['OPTIONS']);
			$unit_suffix.=")";

			foreach($arrLex['ID'] as $n=>$id_pers){
				$this->sqlRecherche.= $unit_sql.$this->sql_op($id_pers).$unit_suffix;
				if($n<count($arrLex['ID'])-1){
					$this->sqlRecherche.= " INTERSECT ";
				}
			}
		}else{
			$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."$id_link IN (select $id_link from $table_link where ID_PERS ";
			$this->sqlRecherche.= $this->sql_op(@implode(",",$arrLex['ID']));
			if (!empty($tabCrit['FIELD'])) $this->sqlRecherche.= " AND ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD']);
			//PC 15/03/2011 : Ajout du paramètre chOptions_role pour affiner la recherche sur personne avec leur role
			if (!empty($tabCrit['OPTIONS'])) $this->sqlRecherche.= " AND DLEX_ID_ROLE=".$db->Quote($tabCrit['OPTIONS']);
		}

		$this->sqlRecherche.=" ))";

	     $this->tabHighlight["pers_nom"]=array_values($arrHL);
	     $this->tabHighlight["pers_prenom"]=array_values($arrHL);

	     if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
    }

     /** Recherche de personnes par ID
     *  IN : tableau critère, préfixe
     *  OUT : sql + arrHighlight
     * 	Tableau de critère : structure
     * 		VALEUR : un ou plusieurs ID
     * 		FIELD : type DESC (GEN, PRD, etc)
     * 		VALEUR2 (optionnel) : type de personne (PM ou PP)
     *
     */
    protected function chercheIdPers($tabCrit,$prefix="") {
    	global $db;
    	$prefix=($prefix!=""?$prefix.".":"");

		// VP 10/03/2017 : prise en compte recherche personne pour matériels
		if (get_class($this)=='RechercheMat' ){
			$table_link = "t_mat_pers";
			$id_link = "ID_MAT";
		}else{
			$table_link = "t_doc_lex";
			$id_link = "ID_DOC";
		}
       	$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."$id_link IN (select $id_link from $table_link where ID_PERS ";

	   	$this->sqlRecherche.= $this->sql_op($tabCrit['VALEUR']);
	   	if (!empty($tabCrit['FIELD'])) $this->sqlRecherche.= " AND ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD']);

		$this->sqlRecherche.=" ))";

		$listName="";
		$sql2="SELECT PERS_NOM,PERS_PRENOM from t_personne WHERE ID_PERS ".$this->sql_op($tabCrit['VALEUR'])." AND ID_LANG='".$_SESSION['langue']."'";
		$arrNames=$db->GetAll($sql2);
		foreach ($arrNames as $name) {$listName.=$name['PERS_NOM'].($name['PERS_PRENOM']!=''?" ".$name['PERS_PRENOM']:""); }
	    unset($arrNames);
	    if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$listName." ";
    }

    /** Recherche par utilisateur **/
    protected function chercheUsager($tabCrit,$prefix="") {

    	global $db;
    	$prefix=($prefix!=""?$prefix.".":"");
 		$arrWords=$this->analyzeString($tabCrit['VALEUR'],$op);
		foreach ($arrWords as $i=>$word) if (strlen($word)<=2) unset ($arrWords[$i]); //strip des mots <2 lettres qui empèchent le FT
		/*foreach ($arrWords as $idx=>$word) {
			$strVals.=" ".$this->FToperator."(".$word.") ";
		}*/

		  $arrHL=array(0=>@implode(' ',$arrWords)); //Tableau des noms (libellé)
		  $sql2 = "select DISTINCT ID_USAGER from t_usager where ".$db->getFullTextSQL($arrWords, array("US_NOM", "US_PRENOM", "US_LOGIN"), "", array(), $op);

		  if (!empty($tabCrit['VALEUR2'])) $sql2.=" AND US_ID_TYPE_USAGER=".$db->Quote($tabCrit['VALEUR2']);
		  $arrIDs=$db->GetAll($sql2);
 		  debug($arrIDs,'silver');
		   if (!empty($arrIDs)) foreach ($arrIDs as $us) $arrLex['ID'][]=$us['ID_USAGER'];  // Tableau des usagers (ID)
		   else $arrLex['ID']=array('-1');

       	$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix.$tabCrit['FIELD']." ";
	   	$this->sqlRecherche.= $this->sql_op(@implode(",",$arrLex['ID']));

	     $this->tabHighlight["us_nom"]=array_values($arrHL);
	     $this->tabHighlight["us_prenom"]=array_values($arrHL);
	     $this->tabHighlight["us_login"]=array_values($arrHL);

	     if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";

    }

	protected function chercheXML($tabCrit,$prefix){ // Cherche VALS dans 1 CHAMP de la table PRINC

        global $db;
		$prefix=(!is_null($prefix)?$prefix.".":"");
		if(!is_array($tabCrit['VALEUR'])){
			$valeur=trim($tabCrit['VALEUR']); //suppression des espaces
			$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
			$valeur=str_replace("*","%",$valeur);
			$valeur = array($valeur);
		}
		foreach($tabCrit['VALEUR'] as $val){
			$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." ".$db->like("%<".$tabCrit['VALEUR2'].">".$val."</".$tabCrit['VALEUR2'].">%").")";
		}
		//$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." like ".$db->Quote("%<".$tabCrit['VALEUR2'].">".$valeur."%</".$tabCrit['VALEUR2'].">%").")";
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
		//$highlight[$tabCrit['FIELD']]=array(str_replace(array(' +',' -','"'),' ',$tabCrit['VALEUR']));
		//By LD 04/06/08 => pas de remplacement dans les highlight
		$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);
		if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
    }


    /**
     * Découpe une chaîne en mots et expressions
     * IN : chaîne
     * OUT : tableau des mots et expressions
     * NOTE : séparation selon les espaces, virgules, etc.
     * Les expressions entre guillemets sont conservées.
     * NOTE2 : la variable glue sert à lier des mots qui se retrouvent séparés
     * par le token_get_all, notamment, les mots avec tiret
     * NOTE3 : le tokenize donne des résultats assez étonnant est n'est pas 100% fiable
     */
	static function analyzeString($str,&$FToperator) {
			//LD => 04/06/08 param $dontSpaceQuotes pour séparer ou non les quotes
			//nécessaire en recherche et analyse mais pas pour le highlight
			
			$str=str_replace("'","`",$str);
			$tokens = token_get_all('<?php '. $str .' ?>');
			//debug($tokens,'plum');
			//trace(print_r($tokens,true));
			$curIdx=0;

			foreach($tokens as $idx=>$tok) { // parcours de la chaine découpée

			   if (is_array($tok) ) { //type token, et pas espace, ou balises php -> on ajoute à la liste des mots
			   //LD 09 04 09 : viré le T_ENCAPSED_AND_WHITESPACE car peut bouffer une partie de la recherche
			   //notamment si le T_CLOSE_TAG est concaténé avec le dernier token, et pas une entité à part
			   		if(!in_array(token_name(($tok[0])), array('T_OPEN_TAG','T_WHITESPACE', 'T_CLOSE_TAG'))) {
						//by LD 13/06/08 : chaine entourée de " => on retire le spaceQuote
						//ex : recherche av/experte avec index sur lexique ou valeur
						$pat='/^\"(.*)\"$/i';
						if ($tok[1] && preg_match($pat,$tok[1],$_dumb)==1) {
							//09 04 09 : remis les espaces "'"=>" ' " car ca pose problème en recherche exacte
							//avec des guillemets => "chasseur d'étoiles"
							// + distinction du cas spacequote pour ne pas garder des ` si pas de spacequote
							$tok[1]=str_replace("`","'",$tok[1]);
						}

						$arrWords[$curIdx].=trim(str_replace(' ?>','',isset($tok[1])?$tok[1]:'')); //Strange, parfois le T_CLOSE_TAG est mis dans le dernier token !

			      		if ( trim($tok[1])=='' || $tokens[$idx+1]==',' || (is_numeric($tokens[$idx+1][0])&& (token_name($tokens[$idx+1][0])=='T_ENCAPSED_AND_WHITESPACE' || token_name($tokens[$idx+1][0])=='T_WHITESPACE' )  ) ) $curIdx++;
			      		}
			   }
			  else { // si simple mot (pas token), on concatène avec le dernier mot connu.
			   			// C'est notamment le cas pour les recherches en * (car le tokenize retire l'étoile)
			   		if (in_array($tok,array('*','%','?','-','@'))) { //by LD 11/03/09,  ajout @ pour qq cas (Etr@nger)
			   		$arrWords[$curIdx].=$tok;
			   			if (in_array($tok,array('*','?'))) $curIdx++; //Ces caractères terminent un mot
			   			//contrairement à - qui peut être au milieu d'un mot (trait d'union)
			   			// ou % qui peut être placé au début (mod by LD le 2/11)
			   		}
			   		if ($tok==',') $FToperator='+'; //S'il y a des virgules entre les mots, on passe la recherche en OU

			   }
			}
			//debug($arrWords,'lightblue');
			//trace(print_r($arrWords,true));
			return $arrWords;
	}


    /**
     * Complétion "intelligente" selon les critères : si XX* -> LIKE, si array -> IN(,,) sinon =
     * IN : valeur (string comprenant 1 ou plusieurs vals), $keepIntact=>pas d'analyse de la chaine (utilisé pr DOC_COTE par ex)
     * OUT : string SQL
     */
    protected function sql_op($valeur,$keepIntact=false,$isId=false) {
        global $db;
		//By LD 26/06, utilisation du split , pour pouvoir chercher sur plusieurs valeurs, sans analyse de chaine
		// si is_array & keepIntact, aucune raison d'implode, puis d'explode, cela risquerait de perturber des valeur[] contenant une virgule.
		if(is_array($valeur) && $keepIntact){
			$arrWords = $valeur ; 
			// si l'array ne contient qu'un élément, on remplace valeur par le seul élément que son array contient pour qu'il soit traité correctement par les cas ci-dessous
			if(count($valeur) <= 1 ){
				$valeur = reset($valeur);
			}		
		}else{
			if (is_array($valeur)) $valeur=implode(',',$valeur);
			if (is_numeric($valeur) || $keepIntact) {
				$matches = array();
				if(preg_match('|^("[^"]+"(\s*\,\s*)*)+$|',$valeur, $matches, 0, 0) > 0)
					$arrWords=preg_split('|"\s*,\s*"|',$valeur);
				else
					$arrWords=explode(",",$valeur);//une seule valeur, de type ID (numérique)

			} else $arrWords=$this->analyzeString($valeur,$op); //Découpage intelligent des mots.
		}

		if(count($arrWords)>1) {

			$valeur="";
        	for($i = 0; $i <= (count($arrWords) - 1); $i++){
			// suppression caract?res parasites et quotes
				$valeur.=",".$db->Quote(preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),trim($arrWords[$i])));
			}
			$valeur=" IN (".substr($valeur,1).")";
		} else if((strpos($valeur,"%")!==false)||(strpos($valeur,"*")!==false)) {
			// suppression caract?res parasites et quotes
			$valeur=trim($arrWords[0]); //suppression des espaces
			$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
			$valeur = $db->like($valeur);
		} elseif (is_numeric($arrWords[0]) && $isId && intval($arrWords[0]) < 0) {
			$valeur=intval($arrWords[0]) * -1;
			$valeur=" != ".$db->Quote($valeur);
		}elseif (is_numeric($arrWords[0])) {
            $valeur=trim($arrWords[0]);
			$valeur=" = ".$db->Quote($valeur);
		}else {
			// suppression caract?res parasites
			$valeur=trim($arrWords[0]); //suppression des espaces
			$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
			$valeur = $db->like($valeur);
		}
		return $valeur;
    }



    function putSearchInSession($id="") {
		// On veut stocker l'objet mySearch à l'instant T pour le stocker dans la session
		// On doit donc réaliser une copie de l'objet et la stocker en session, on ne peut pas juste passer la référence car l'objet en session continuerait d'être modifié en même temps que $this, 
		// Etant donné que cette fonction n'est pas redéfinie dans un certain nombre de classe héritées, on rend le nom de l'objet copy_this dynamique;
		$current_classname = get_class($this);
		$copy_this = new $current_classname();
		foreach (get_object_vars($this) as $key => $value){
			$copy_this->$key = $value;
		}
		$_SESSION[$this->sessVar]["searchObj"] = $copy_this;        
		$_SESSION[$this->sessVar]["etape"] = $this->etape;
        $_SESSION[$this->sessVar]["sql"] = $this->sql;
        $_SESSION[$this->sessVar]["form"] = $this->tab_recherche;
		$_SESSION[$this->sessVar]["highlight"] = $this->tabHighlight;
        $_SESSION[$this->sessVar]["tri"] = $this->sql_order_by;
        $_SESSION[$this->sessVar]["refine"] = "";
        $_SESSION[$this->sessVar]["page"] = "1";
        $_SESSION[$this->sessVar]["hist"] = $id;
        $_SESSION[$this->sessVar]["rows"] = $this->rows; //@update VG 08/04/2010
    }

    function getSearchFromSession() {
		if(isset($_SESSION[$this->sessVar]['searchObj'])){
			// récupération de l'objet RechercheSolr depuis la session, cf RechercheDocSolr::putSearchInSession pour voir comment on réalise le stockage
			foreach (get_object_vars($_SESSION[$this->sessVar]['searchObj']) as $key => $value) {
				$this->$key = $value;
			}
		}
		if(isset($_SESSION[$this->sessVar])){
            $this->etape=$_SESSION[$this->sessVar]["etape"];
            $this->sql=$_SESSION[$this->sessVar]["sql"] ;
            $this->tab_recherche=$_SESSION[$this->sessVar]["form"] ;
            $this->tabHighlight=$_SESSION[$this->sessVar]["highlight"];
            $this->rows = $_SESSION[$this->sessVar]["rows"]; //@update VG 08/04/2010
            $this->sql_order_by = $_SESSION[$this->sessVar]["tri"];
        }
    }

    function saveSearchInDB ($requete) {
		global $db;
		require_once(modelDir.'model_requete.php');
		require_once(libDir."class_page.php");
		$myPage=Page::getInstance();
        $myQuery = New Requete();
        $myQuery->t_requete['REQ_SQL']=$this->sql;
        $myQuery->t_requete['REQ_LIBRE']=$this->etape;

		debug('saveSearch','cyan');
		$exist=$myQuery->checkExist();
		if ($exist) {

			$myQuery->t_requete['ID_REQ']=$exist;
			$myQuery->getRequete();
		}

        $myQuery->t_requete['REQ_NB_DOC']=$myPage->found_rows;
        $myQuery->t_requete['REQ_LIBRE']=$this->etape;
       	$myQuery->t_requete['REQ_PARAMS']=$this->tab_recherche;
       	$myQuery->t_requete['REQ_DATE_EXEC']=str_replace("'","",$db->DBTimeStamp(time()));

		if (isset($requete['SAUVE']) && $requete['SAUVE']) { //on sauve "sciemment" la requête ?
	        $myQuery->t_requete['REQUETE']=$requete['REQUETE'];
	        if (!empty($requete['REQUETE']))  {
	        	$myQuery->t_requete['REQ_SAVED']=1;
	        	$myQuery->t_requete['REQ_ALERTE']=$requete['REQ_ALERTE']; }
	        else {
	        	$myQuery->t_requete['REQ_ALERTE']=0; //... ou on la met juste dans l'histo...
	        	$myQuery->t_requete['REQ_SAVED']=0; }
		}

       	$myQuery->save(false); //ly ld 21 11 08 sauv sans chk car fait au dessus

		if (isset($myQuery->t_requete['REQUETE']))
			$this->requete['REQUETE']=$myQuery->t_requete['REQUETE'];
		else
			$this->requete['REQUETE']='';

		if (isset($myQuery->t_requete['REQ_ALERTE']))
			$this->requete['REQ_ALERTE']=$myQuery->t_requete['REQ_ALERTE'];
		else
			$this->requete['REQ_ALERTE']='';
        unset($myQuery);
    }

    function getSearchFromDB ($id) {
 		require_once(modelDir.'model_requete.php');
        $myQuery = New Requete();
        $myQuery->t_requete['ID_REQ']=$id;
        $myQuery->getRequete();
        $this->etape = $myQuery->t_requete['REQ_LIBRE'];
		// MS - 30.03.17 - suite à l'ajout de removescripttags, les caractères spéciaux sont échappés dans REQ_SQL, on doit donc les retransformer lors d'une récupération depuis la base
		$this->sql = htmlspecialchars_decode($myQuery->t_requete['REQ_SQL'],ENT_NOQUOTES | ENT_HTML401);
		$this->tab_recherche = $myQuery->t_requete['REQ_PARAMS'];
        $this->tabHighlight=(isset($myQuery->t_requete['REQ_HIGHLIGHT']))?$myQuery->t_requete['REQ_HIGHLIGHT']:'';
        $this->requete['REQUETE']=$myQuery->t_requete['REQUETE'];
        $this->requete['REQ_ALERTE']=$myQuery->t_requete['REQ_ALERTE'];

		$this->rows = $myQuery->t_requete['REQ_NB_DOC'];//@update VG 08/04/2010 : ajout du nombre de lignes

        $this->putSearchInSession($id);
        unset($myQuery);
    }

	function setTabRechercheFromUrl() {
		$aFields = explode(':', $_GET['chFields']);
		$aValues = explode(':', $_GET['chValues']);
		$aTypes = explode(':', $_GET['chTypes']);
		$aOp = explode(':', $_GET['chOps']);

		foreach ($aFields as $key => $field ) {
			$this->tab_recherche[] = array (
										'FIELD' 	=>	$field,
										'VALEUR'	=>	$aValues[$key],
										'TYPE'		=>	$aTypes[$key],
										'OP'		=>	$aOp[$key]
									);
		}

	}

	function getTabRechercheForUrl() {
		$sValues = '';
		$sFields = '';
		$sTypes = '';
		$sOp = '';
		$i = 0;
		foreach ($this->tab_recherche as $aSearch) {
			$i++;
			if($i > 1) {
				$sField.=':';
				$sValues.=':';
				$sTypes.=':';
				$sOp.=':';
			}
			$sField.=$aSearch['FIELD'];
			$sValues.=$aSearch['VALEUR'];
			$sTypes.=$aSearch['TYPE'];
			$sOp.=$aSearch['OP'];
		}

		$urlParams = array(
							"chFields" 	=> $sField,
							"chValues" 	=> $sValues,
							"chTypes" 	=> $sTypes,
							"chOps" 		=> $sOp
					);
		return $urlParams;
	}

    function initSession() {
        $_SESSION[$this->sessVar]=array();
    }

	function getEntity() {
		return $this->entity;
	}


	/**
	 * Ramène la valeur pour le critère dans le formulaire.
	 * Effectivement, entre la recherche normale et experte, les champs ne sont pas dans le même ordre que la session
	 * IN :  tableau contenant 1. le nom des champs concernés, 2. le type de recherche
	 * 		 chaine correspondant à la valeur à ramener (deft : VALEUR) ex: VALEUR2.
	 * OUT : valeur correspondant dans le tableau de session pour ces 2 critères, ou false si : absent / échec / pas de session
	 * NOTE : by LD 13/12/07 modifié pour pouvoir marcher avec tab_recherche ou session, donc marche aussi si pas de session
	 */
	//update VG : ajout de $this->useSession pour la condition d'utilisation du champ en session.
	function getValueForField($arrCriteres,$champVal='VALEUR', $idxCrit=null) {

		global $db;
		if (isset($_SESSION[$this->sessVar]['form']) && $_SESSION[$this->sessVar]['form'] && $this->useSession) $srcTab=$_SESSION[$this->sessVar]['form']; //Si en session on le prend
		if (!empty($this->tab_recherche)) $srcTab=$this->tab_recherche;
              
		if (!isset($srcTab) || !$srcTab || empty($arrCriteres)) return false;
		foreach ($srcTab as $idx=>$tabCrit) {
			if($idxCrit!=null && $idxCrit!=$idx)
				continue;
			$match=true;
			foreach ($arrCriteres as $fld=>$crit) {
                            //BR 2015-09-17 ex: que dans le xml, cela soit <chfields>DOC_RES</chfields> ou <chfields>doc_res</chfields>, si le nom matche il faut que cela renvoie la valeur
				if (!isset($arrCriteres[$fld]) || !isset($tabCrit[$fld]) || (strtolower($arrCriteres[$fld])!==strtolower($tabCrit[$fld]) && !in_array(strtolower($tabCrit[$fld]), explode(",", strtolower($arrCriteres[$fld])))) && $match) $match=false;
			}
			if ($match)
			{
				if (isset($tabCrit[$champVal]))
					return $tabCrit[$champVal];
				else
					return false;
			}// pas de htmlentities (cf note de fin de page)
		}
		return false; //si on a parcouru le tableau de session sans succès...
	}

    /**
     * Execution requête
     * IN : SQL, max_rows (nb de lignes max), $secstocache, $highlight (compatibilité solr)
     * OUT : var de classe : Result (resultat de requete "tronqué"),
     * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales)
     */
    function execute( $max_rows = 10, $secstocache=0, $highlight=false)
    {
        global $db;
        
        if(empty($this->sql))
            return;
        
        $sql = $this->sql;
        if(!empty($this->sql_order_by))
            $sql .= $this->sql_order_by;
        
        if (!isset($this->nbLignes))
            $this->max_rows = $max_rows;
        else
            $this->max_rows=$this->nbLignes;
        
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) )
            $this->page=1;
        
        if($this->max_rows == "all"){
            $i=0;
            if ($secstocache==0) $rsfull= $db->Execute($sql); else $rsfull=$db->CacheExecute($secstocache,$sql);
            
            if (!$rsfull) { //0 résultats retournés !
                $this->rows=0;
                $this->found_rows=0;
                $this->page=1;
                $this->result=array();
                $this->error_msg="Problem with request.";
                trace("Error execute : $sql");
                return false;
            }
            
            $this->max_rows = $rsfull->RecordCount();
            
            if ($this->max_rows>kMaxRows) $this->max_rows=kMaxRows; // précaution : si trop de lignes, on revient à un mode paginé pour ne pas surcharger le serveur
            else {
                $this->result=$rsfull->GetArray();
                $this->rows =$this->max_rows;
                $this->found_rows=$this->max_rows;
                $this->page=1;
                $rsfull->Close();
            }
        }
        
        if ($this->max_rows!="all")
        {
            
            $i=0;
			// cas recherche avec offset manuel (start_offset) défini 
			if(isset($this->start_offset) && !empty($this->start_offset)){
				$rs_found_rows=$db->Execute($sql.$db->limit($this->start_offset,($this->max_rows*$this->page)),false);
				if ($rs_found_rows) {
					$this->result=$rs_found_rows->GetArray();
					$this->rows =count($this->result);
					$this->found_rows = $this->rows ; // Attention : propriété "cachée" de ADODB
					$rs_found_rows->Close();

				} else {$this->found_rows=0;$this->page=1;$this->error_msg="Problem with request.";return false;}
			}else{
				// cas recherche paginée standard
				$rs_found_rows=$db->PageExecute($sql,$this->max_rows,$this->page,false,$secstocache);
				if ($rs_found_rows) {
					$this->found_rows = $db->_maxRecordCount; // Attention : propriété "cachée" de ADODB
					$i_min = ($this->page - 1) * $this->max_rows;
					if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
					$this->result=$rs_found_rows->GetArray();
					$this->rows =count($this->result);
					$rs_found_rows->Close();
					
				} else {
					$this->found_rows=0;
					$this->page=1;
					$this->error_msg="Problem with request.";
					trace("Error execute : $sql");
					return false;
				}
			}
        }
        $this->setNbPages();
		// Comportement automatique d'ajustement de la page courante : Si la page demandée est supérieure au nombre de page de résultat, on redéfini la page demandée
		// On ne veut pas de ce comportement automatique lorsqu'on accede à une recherche avec un offset manuellement défini
        if ($this->page > $this->nb_pages && (!isset($this->start_offset) || empty($this->start_offset))){
            $this->page = $this->nb_pages;
            //PC 04/12/12 : Rappel de PageExecute quand la page courante est supérieur au nombre de pages totales
            $rs_found_rows=$db->PageExecute($sql,$this->max_rows,$this->page,false,$secstocache);
            if ($rs_found_rows) {
                $this->found_rows = $db->_maxRecordCount;
                $i_min = ($this->page - 1) * $this->max_rows;
                if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
                $this->result=$rs_found_rows->GetArray();
                $this->rows =count($this->result);
                $rs_found_rows->Close();
            }
        }
        
        return true;
    }
    
    /** Détermine le type d'affichage
     *  IN : affichage demandé
     *  OUT : paramètre affichage
     */
    function setAffichage($affichage){
        if (!empty($affichage)) {
            $this->affichage = $affichage;
        } elseif (isset($_SESSION[$this->sessVar]['affichage']) && !empty($_SESSION[$this->sessVar]['affichage'])) {
            $this->affichage = $_SESSION[$this->sessVar]['affichage'];
        } else {
            $this->affichage = "LISTE";
        }
    }
    
    /** Calcule le nombre de pages à afficher
     *  IN : var classe Found_rows et max_rows
     *  OUT : nombre de pages (int)
     */
    function setNbPages(){
        if ($this->max_rows==0) return 0;
        $num = round($this->found_rows / $this->max_rows);
        if($num < ($this->found_rows / $this->max_rows)) $num++;
        $this->nb_pages = $num;
    }

    /**
     * Détermine le nombre le résultats par page à afficher
     * IN : Nb de lignes demandé, Nb lignes par défaut
     * OUT : maj var nbLignes
     */
    function setNbLignes($nbLignes, $nbLignesDefaut=10) {
        if(isset($nbLignes) && !empty($nbLignes)) {
            $this->nbLignes = $nbLignes;
        } elseif(isset($_SESSION[$this->sessVar]['nbLignes']) && !empty($_SESSION[$this->sessVar]['nbLignes'])) {
            $this->nbLignes = $_SESSION[$this->sessVar]['nbLignes'];
        } else {
            $this->nbLignes = $nbLignesDefaut;
        }
    }

    /**
     * Détermine la page à afficher
     * IN : page demandée
     * OUT : maj var page
     */
    function setPage($page=0) {
        if($this->nbLignes != $_SESSION[$this->sessVar]['nbLignes'] && $this->useSession==true){
            $this->page = 1;
        } elseif(isset($page) && !empty($page)) {
            $this->page = $page;
        } elseif(isset($_SESSION[$this->sessVar]['page']) && !empty($_SESSION[$this->sessVar]['page'])) {
            $this->page = $_SESSION[$this->sessVar]['page'];
        } else {
            $this->page = 1;
        }
    }

    /**
     * Préparation d'un ordre SQL à partir de l'analyse du paramètre sort dans l'url.
     * IN : paramètre tri , tableau de champ
     * OUT : SQL (ORDER BY..., direct) et màj des variables order (col), triInvert (sens passé à l'url)
     */
    function setOrderBy ($tri="", $tabOrder=array()) {
        global $db;
        
        if(isset($tri) && !empty($tri)){ //Remplissage tableau $tabOrder d'après tri
            $tabOrder = array();
            $_hasbrackets=preg_match("/^\[(.*)\](.*)$/i",$tri,$matches); //Présence de crochet autour des champs => concaténation
            if ($_hasbrackets) {
                $tri=$matches[1].$matches[2]; //on se débarrasse des crochets (mais on garde le sens)
                $sqlTri=str_replace(';',',',$tri);
                if (substr($sqlTri,-1,1)=='1') {
                    $_cols=strtoupper(substr($sqlTri,0,-1));
                    $_sens=" DESC "; }
                elseif (substr($sqlTri,-1,1)=='0') {
                    $_cols=strtoupper(substr($sqlTri,0,-1));
                    $_sens=" ASC "; }
                elseif (!is_numeric(substr($sqlTri,-1,1))) {
                    $_cols=strtoupper($sqlTri);
                    $_sens=" ASC ";
                }
                if (!empty($_cols)){
                    $this->sql_order_by = " ORDER BY ".$db->Concat($_cols).$_sens;
                }
            } else {
                $sqlTri="";
                if(stripos($tri,'ORDER BY') !== false){
                    $tri = trim(preg_replace('/ORDER BY/i','',$tri));
                }
                $fields=explode(",",$tri);
                foreach ($fields as $field) {
                    
                    $field=trim($field);
                    $ordre=substr($field,-1,1);
                    // ajout gestion si $tri embarque deja une direction => évite un bug constaté sur démo / jobListe.php
                    if(stripos($field,'ASC') !== false || stripos($field,'DESC') !== false){
                        preg_match('/DESC|ASC/i',$field,$matches);
                        if(!empty($matches)){
                            $fld = trim(str_replace($matches[0],'',$field));
                            $direction = $matches[0];
                        }else{
                            $fld = $field;
                            $direction ="";
                            
                        }
                    }elseif (is_numeric($ordre)){
                        if($ordre==1){$direction="DESC";}else{$direction="ASC";}
                        $fld=substr($field,0,-1);
                    }
                    else {
                        $direction="ASC";
                        $fld=$field;
                    }
                    $tabOrder['COL'][]=$fld;
                    $tabOrder['DIRECTION'][]=$direction;
                }
                
            }
        } elseif(isset($_SESSION[$this->sessVar]['tabOrder']) && !empty($_SESSION[$this->sessVar]['tabOrder'])) {
            $tabOrder = $_SESSION[$this->sessVar]['tabOrder'];
        }
        
        // Calcul chaine order by
        $this->addDeftOrder($tabOrder);
    }
    
    /**
     * Calcul chaine tri
     * IN : paramètre tri , tableau de champ
     * OUT : SQL (ORDER BY..., direct) et màj des variables order (col), triInvert (sens passé à l'url)
     */
    function addDeftOrder($tabOrder) {
        $this->tabOrder = $tabOrder;
        if (!empty($tabOrder)) {
            if($tabOrder['COL'][0]!='none'){
                $sqlOrderByDeft=" ORDER BY ";
                foreach ($tabOrder['COL'] as $idx=>$key) {
                    // Remplacement espace par , pour tri multiple
                    $key = str_replace(' ',',',trim($key));
                    
                    //Gestion de la casse
                    if(strpos($key,'.') === false) {
                        $fld = strtoupper($key);
                    } else {
                        $aFld = explode(".",$key);
                        $fld = strtolower($aFld[0]).".".strtoupper($aFld[1]);
                    }
                    
                    $sqlOrderByDeft.=" ".(!empty($tabOrder['PREFIX'][$idx])?$tabOrder['PREFIX'][$idx].".":"").($fld);
                    if ($tabOrder['DIRECTION'][$idx] && in_array(strtoupper($tabOrder['DIRECTION'][$idx]),array('ASC','DESC',''))) $sqlOrderByDeft.=" ".$tabOrder['DIRECTION'][$idx]." ";
                    if ($idx<count($tabOrder['COL'])-1) $sqlOrderByDeft.=",";
                }
                $this->order=$tabOrder['COL'][0]; //du coup dans le tableau, on trie sur la première colonne
                $this->triInvert=($tabOrder['DIRECTION'][0]=='DESC'?0:1);
                
                $this->sql_order_by = $sqlOrderByDeft;
            }
        }
    }
	
	// ajout fonction permettant de calculer rapidement le tri depuis le tabOrder (puisque sur les dernières versions on stock moins le tri que le taborder );
	// était necessaire pour les appels de la class_export, 
	// volontairement KO pour la RechercheSolr 
	public static function getSQLorderFromTabOrder($tabOrder){
		$srch = new Recherche() ;
		$srch->addDeftOrder($tabOrder);
		if(isset($srch->sql_order_by)){
			$sql_order_by = $srch->sql_order_by;
			$isset = isset($sql_order_by);
			if(!empty($isset))
			   return $sql_order_by;
		}
		return '';
		//return (isset($srch->sql_order_by) && !empty(isset($srch->sql_order_by)))?$srch->sql_order_by:'' ;
	}
	
	
	// Fonction d'initialisation de l'offset manuel 
	function setOffset($n){
		if(isset($n) && !empty($n) && is_numeric($n))
			$this->start_offset = $n;
	}

}//Fin de la classe Cherche

/* Un petit mot sur htmlentities qui vaut pour toute l'application OPSIS
 * On évite de l'utiliser car sous UTF8 cette fonction
 * endommage les accents.
 * Pour traiter les guillemets et apostrophes, on préfère des fonctions ciblées
 */
?>
