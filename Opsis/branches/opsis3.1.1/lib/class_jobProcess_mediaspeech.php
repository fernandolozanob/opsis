<?php
require_once('class_jobProcess.php');
require_once('class_matInfo.php');
require_once('interface_Process.php');
require_once('exception/exception_uploadException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');

class JobProcess_mediaspeech extends JobProcess implements Process
{

	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);

		$this->setModuleName('mediaspeech');

		$this->required_xml_params=array
		(
		'url'=>'string',
		'url_ftp'=>'string',
		'user'=>'string',
		'pass'=>'string',
		'folder_video'=>'string',
		'function'=>'string'
		);
		
		$this->optional_xml_params=array
		(
		'file_transcript'=>'string',
		'start_time'=>'string',
        'end_time'=>'string',
        'folder_in'=>'string',
		'champ_parol'=>'string',
		'file_xml'=>'string',
		'file_srt'=>'string',
		'file_txt'=>'string'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		$path_in=$this->getFileInPath();
		$name_fichier=basename($path_in);
		$url_ftp=$params_job['url_ftp'];	

		///////////////////////////////////////////////
		/////////////////// UPLOAD ///////////////////
		//////////////////////////////////////////////

		//on envoie direct file transcript sur le ftp
		 if (!empty($params_job['filetranscript'])){
			 $name_file_transcript=basename($params_job['filetranscript']);
			$this->total_file_size=filesize($this->getFileInPath());
			$this->curl = curl_init();	
			$fd = fopen($params_job['filetranscript'], "rb");
			curl_setopt($this->curl, CURLOPT_URL, $url_ftp.$name_file_transcript);
			curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user'].':'.$params_job['pass']);
			curl_setopt($this->curl, CURLOPT_UPLOAD, 1);
			curl_setopt($this->curl, CURLOPT_INFILE, $fd);
			curl_setopt($this->curl, CURLOPT_INFILESIZE, filesize($params_job['filetranscript']));		
			curl_setopt($this->curl, CURLOPT_NOPROGRESS, false);
			$curl_result=curl_exec($this->curl);		
			if ($curl_result===false)
				throw new UploadException('Error sending file ('.curl_errno($this->curl).' -> '.curl_error($this->curl).')');		
			fclose($fd);					
			curl_close($this->curl);
		}
			// calcul de la taille totale a uploader		
		$this->total_file_size=filesize($this->getFileInPath());
		$this->curl = curl_init();	
		$fd = fopen($this->getFileInPath(), "rb");
		curl_setopt($this->curl, CURLOPT_URL, $url_ftp.$name_fichier);
		curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user'].':'.$params_job['pass']);
		curl_setopt($this->curl, CURLOPT_UPLOAD, 1);
		curl_setopt($this->curl, CURLOPT_INFILE, $fd);
		curl_setopt($this->curl, CURLOPT_INFILESIZE, filesize($this->getFileInPath()));		
		curl_setopt($this->curl, CURLOPT_NOPROGRESS, false);
		curl_setopt($this->curl, CURLOPT_PROGRESSFUNCTION, array($this,'updateUploadProgress'));		
		$curl_result=curl_exec($this->curl);
		
		if ($curl_result===false)
			throw new UploadException('Error sending file ('.curl_errno($this->curl).' -> '.curl_error($this->curl).')');
		
		fclose($fd);	
		curl_close($this->curl);

		 ini_set('soap.wsdl_cache_enabled', 0);
		$this->setProgression(25);

		///////////////////////////////////////////////
		/////////////////// ATTENTE ///////////////////
		//////////////////////////////////////////////
		
	if (!empty($params_job['start_time'])){	 
		$sleepuntil = strtotime($params_job['start_time']);
		if(time() > $sleepuntil ) $sleepuntil = strtotime('+1 days',$sleepuntil);
		while (time() < $sleepuntil)
		   time_sleep_until($sleepuntil);
	}


		///////////////////////////////////////////////
		/////////////////// FONCTION /////////////////
		//////////////////////////////////////////////

	 if (!empty($params_job['function'])){	
		//SOAP fonction trans/ alignement
			$url = $params_job['url']; 
			$soap=new SoapClient($url,array(   
						'trace'=> 1,
						'soap_version'=> SOAP_1_1
					));
			//login
			$retour_ws =  $soap-> __call('login',array('username'=>$params_job['user'], 'password'=>$params_job['pass']));	
			 $filename=$path_in; //local file path
			 $f = file_get_contents($filename); 

			//fonction trans		
			if ($params_job['function'] == 'trans')
			{			   
				$trans=$response =  $soap -> __call('trans',array(
											'filename'=>$name_fichier,
											'type'=>'bn',
											'language'=>'fre',
											'channel'=>'0',
											'quality'=>'0',
											'duration'=>'0',
											'corpus'=>''
											));
			$resultat_id=strip_tags($trans[5]);
			$resultat_id=intval($resultat_id);
			}

			else if	($params_job['function'] == 'alignment'){
				$align=$response =  $soap -> __call('alignment',array(
								'filename'=>$name_fichier,
								'type'=>'bn',
								'language'=>'fre',
								'textfiletoalign'=>$name_file_transcript,
								'alignmode'=>'1'
								));
				$resultat_id=strip_tags($align[5]);
				$resultat_id=intval($resultat_id);			
			}
			
		//////////////////////////////////////////////
		/////////////////// RESULTATS ////////////////
		//////////////////////////////////////////////

         // Boucle d'attente
         $status="";
         while($status!='E' && $status!='F' && $status!='C'){
             $result_status =  $soap -> __call('status',array(
                                                  'jobid'=>$resultat_id
                                                             ));
             $xml_status=implode("", $result_status);
             if(preg_match("|<jobstatus>(.*)</jobstatus+>|i",$xml_status,$matches)){
                 $status=$matches[1];
               
             }
             sleep(10);
         }
         // Resultat
         $result_txt =  $soap -> __call('result',array(
                                                       'jobid'=>$resultat_id,
                                                       'format'=>'txt'
                                                       ));

			$length_tab=count($result_txt);
			foreach ($result_txt as $idx=>$v) {
				if(trim($v)=='<![CDATA[') $ind_start= $idx;
				if(trim($v)==']]>') $ind_end= $idx;
			}
			for($i=0;$i<=$ind_start;$i++){
				unset($result_txt[$i]);
			}
			for($i=$ind_end;$i<=$length_tab;$i++){
				unset($result_txt[$i]);
			}
			// trace(print_r($result_txt,true));
			file_put_contents($params_job['folder_video'].'/'.$name_fichier.'.txt',$result_txt);
		
		
			//XML

			   $result_xml =  $soap -> __call('result',array(
                                                       'jobid'=>$resultat_id,
                                                       'format'=>'xml'
                                                       ));
	
			$length_tab=count($result_xml);
			foreach ($result_xml as $idx=>$v) {
				if(trim($v)=='<![CDATA[') $ind_start= $idx;
				if(trim($v)==']]>') $ind_end= $idx;
				if(trim($v)=='<?xml version="1.0" encoding="UTF-8"?>') unset($result_xml[$idx]);
				if(trim($v)=='<TextDoc name="align.txt" path="/">') unset($result_xml[$idx]);
			}
			for($i=0;$i<=$ind_start;$i++){
				unset($result_xml[$i]);
			}
			for($i=$ind_end;$i<=$length_tab;$i++){
				unset($result_xml[$i]);
			}
			file_put_contents($params_job['folder_video'].'/'.$name_fichier.'.xml',$result_xml);
		
		
			//SRT
		   $result_srt =  $soap -> __call('result',array(
	                                   'jobid'=>$resultat_id,
	                                   'format'=>'srt'
	                                   ));
			$length_tab=count($result_srt);
			foreach ($result_srt as $idx=>$v) {
				if(trim($v)=='<![CDATA[') $ind_start= $idx;
				if(trim($v)==']]>') $ind_end= $idx;
			}
			for($i=0;$i<=$ind_start;$i++){
				unset($result_srt[$i]);
			}
			for($i=$ind_end;$i<=$length_tab;$i++){
				unset($result_srt[$i]);
			}
			file_put_contents($params_job['folder_video'].'/'.$name_fichier.'.srt',$result_srt);
									
		$retour_ws = $soap -> __call('logout',array());
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
		}

	
	}
	
	public function finishJob()
	{
		if (file_exists($this->tmp_xml_file))
		{
			if (unlink($this->tmp_xml_file)===false)
				throw new FileException('failed to delete XML file ('.$this->tmp_xml_file.')');
		}
		
		parent::finishJob();
	}
	
	public function updateProgress()
	{
		if($this->getProgression == "25%"){
			$this->writeOutLog('Pourcentage à 25%');
			//mail?
		}
		$this->writeOutLog(($this->uploaded_size[0]+$this->uploaded_size[1]+$this->uploaded_size[2]).'/'.$this->total_file_size);
		$this->setProgression((($this->uploaded_size[0]+$this->uploaded_size[1]+$this->uploaded_size[2])/$this->total_file_size*100)/4,'uploading file');
	}
	
	public function updateUploadProgress($ressource_curl,$download_size, $downloaded, $upload_size, $uploaded)
	{
		// PHP 5.5 modifie l'ordre des arguments des callback progress curl, il faut donc réarranger les paramètres si ce script est executé sur une version PHP inférieure à 5.5.0
		if (version_compare(PHP_VERSION, '5.5.0') < 0) {
			$uploaded = $upload_size;
			$upload_size = $downloaded;
			$downloaded = $download_size;
			$download_size = $ressource_curl;
		}
		$this->uploaded_size[0]=$uploaded;
		$this->updateProgress();
	}

	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}


?>