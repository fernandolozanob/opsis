<?php
require_once(libDir."class_cherche.php");


class RechercheEtape extends Recherche {

	function __construct() {
	  	$this->entity='';
     	$this->name=kEtape;
     	$this->prefix='e';
     	$this->sessVar='recherche_ETAP';
		$this->tab_recherche=array();
		$this->sqlSuffixe='';
		$this->useSession=true;
	}


    function prepareSQL(){
		global $db;
		$this->sql = "select e.*,m.MODULE_NOM,m.MODULE_TYPE
						from t_etape e left join t_module m on (e.ETAPE_ID_MODULE=m.ID_MODULE)
						where 1=1 ";
        $this->etape="";
    }



}
?>