<?php
require_once(libDir."class_cherche.php");


class RechercheJob extends Recherche {
	var $sqlChildren;
	
	function __construct() {
	  	$this->entity='';
     	$this->name=kTraitement;
     	$this->prefix='j';
     	$this->sessVar='recherche_JOB';
		$this->tab_recherche=array();
		$this->sqlSuffixe='';
		$this->useSession=true;
	}


    function prepareSQL($aff_hierarchique=false){
		global $db;
		$this->sql = "select j.*, t_module.MODULE_NOM, t_etat_job.ETAT_JOB, ".$db->Concat('US_NOM',"' '",'US_PRENOM')." USAGER, m.MAT_ID_FONDS
		 FROM t_job j 
		 LEFT JOIN t_usager u ON u.ID_USAGER = j.JOB_ID_USAGER
		 LEFT JOIN t_module ON j.JOB_ID_MODULE=t_module.ID_MODULE 
		 LEFT JOIN t_etat_job ON j.JOB_ID_ETAT=t_etat_job.ID_ETAT_JOB and t_etat_job.ID_LANG='".$_SESSION['langue']."'
		 LEFT JOIN t_mat m ON j.JOB_ID_MAT = m.ID_MAT
		 WHERE 1=1 ";
		 
		if (!$aff_hierarchique)	$this->sql .= " AND j.JOB_ID_MODULE!='0' ";
		else {
			$this->sqlChildren = $this->sql . " AND j.ID_JOB_GEN!='0' ";
			$this->sql .= " AND j.ID_JOB_GEN='0' ";
		}
        $this->etape="";
    }

	function finaliseRequeteChildren(){
		$this->sqlRecherche=trim($this->sqlRecherche);
        if (strpos($this->sqlRecherche,"AND")===0){
            $this->sqlRecherche=substr($this->sqlRecherche,3);
		} else if (strpos($this->sqlRecherche,"OR")===0){
            $this->sqlRecherche=substr($this->sqlRecherche,2);
        }
		
		$sqlInt = "select jc.ID_JOB FROM t_job jc WHERE jc.ID_JOB_GEN='0' ".$this->sqlWhere;
		$sqlInt = "select jc2.ID_JOB FROM t_job jc2 WHERE jc2.ID_JOB_GEN in ($sqlInt) union $sqlInt";
		if($this->sqlRecherche!="") $sqlInt.=" AND (".str_replace("j.", "jc.", $this->sqlRecherche).")";
		$this->sqlChildren .= " AND j.ID_JOB_GEN in ($sqlInt)";
		
		$_SESSION[$this->sessVar]["sqlChildren"] = $this->sqlChildren;
    }

	function searchChildren() {
		global $db;
		
		$result=array();
		if(isset($_SESSION[$this->sessVar]["sqlChildren"])) {
			$sql=$_SESSION[$this->sessVar]["sqlChildren"];
			if(isset($_SESSION[$this->sessVar]["tri"])){
				$sql.= $_SESSION[$this->sessVar]["tri"];
			}			
			$rsfull = $db->Execute($sql);
			$result = $rsfull->GetArray();
			$rsfull->Close();
		}
		return $result;
	}
}
?>