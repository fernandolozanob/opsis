<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_uploadException.php');

require_once(libDir.'kewego/KCurl.class.php');

class JobProcess_kewego extends JobProcess implements Process
{	
	private $app_token;
	private $upload_key;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('kewego');
		
		$this->required_xml_params=array
		(
			'app_url'=>'string',
			'login_url'=>'string',
			'upload_url'=>'string',
			'appkey'=>'string',
			'user'=>'string',
			'pass'=>'string',
			'url'=>'string',
			'xml_data'=>'array'
		);
		
		$this->optional_xml_params=array
		(
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		
		// recuperation des parametres pour kewego
		$xml_data=explode('<xml_data>',$this->in_xml_data);
		$xml_data=explode('</xml_data>',$xml_data[1]);
		$str_param_xml_kewego=$xml_data[0];
		
        // VP 5/04/2013 : ajout proxy http
        if (defined('kProxyHTTP')) APICall()->proxy = kProxyHTTP;

		// recuperation du token
		$data_array=array('appKey' => $params_job["appkey"]);
		$response=APICall()->Call($params_job["app_url"], $data_array);
		$xml=simplexml_load_string($response);
		
		if (isset($xml->message->appToken) && !empty($xml->message->appToken))
			$this->app_token=$xml->message->appToken;
		else
			throw new UploadException('app token is empty ('.$xml->kewego_error.')');
		
		$data_array=array('appToken' => $this->app_token,
						'username' => $params_job["user"],
						'password' => $params_job["pass"],
						'site_url' => $params_job["url"]);
		
		$response=APICall()->Call($params_job['login_url'], $data_array);
		$xml=simplexml_load_string($response);
		
		if (isset($xml->message->token) && !empty($xml->message->token))
			$login_token=$xml->message->token;
		else
			throw new UploadException('kewego login failed ('.$xml->kewego_error.')');
		
		$this->upload_key=md5($xml->message->sig);
		$data_array=array('appToken' => $this->app_token,
						'file' => '@'.$this->getFileInPath(),
						'xml_data' => $str_param_xml_kewego,
						'token' => $login_token,
						'sound_sig' => "",
						'background_sig' => "",
						
						'site_url' => $params_job["url"]);
		
		$response=APICall()->Call($params_job["upload_url"].'?X-Progress-ID='.$this->upload_key, $data_array,true,true);
		$xml=simplexml_load_string($response);
		
		if (isset($response) && !empty($response) && isset($xml->message->sig) &&  !empty($xml->message->sig))
			$this->writeOutLog('Kewego video id : '.$xml->message->sig);
		else
			throw new UploadException('Upload failed ('.$this->getFileInPath().')');
		
		$data_array=array('appToken' => $this->app_token,
						'upload_key' => $this->upload_key);
		
		$response=APICall()->Call($params_job["progress_url"], $data_array,true,true);
		$xml=simplexml_load_string($response);
		
		//$this->setProgression(($xml->message->received/$xml->message->size)*100,$xml->message->state);
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
		// recuperation du progress avec url_progress
		// progression
		
		$params_job=$this->getXmlParams();
		
		$data_array=array('appToken' => $this->app_token,
						'upload_key' => $this->upload_key);
		
		$response=APICall()->Call($params_job["progress_url"], $data_array,true,true);
		$xml=simplexml_load_string($response);
		
		$this->setProgression(($xml->message->received/$xml->message->size)*100,$xml->message->state);
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>