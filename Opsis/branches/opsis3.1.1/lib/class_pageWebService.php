<?php
include_once(libDir."class_page.php");

class PageWebService extends Page {

private static $instance;


	function PageWebService () { //contructeur
		$this->prmAction="urlaction";
		$this->prmPage="page";
		$this->prmSort="tri";
		$this->prmNbLignes="nbLignes";
		$this->page=1;
		$this->titre=""; // titre par défaut
		$this->nomEntite=""; // titre par défaut
		$this->titreSite=gSite; // titre par défaut
		$this->initParams4XSL();
		$this->includePath=includeDir;
	}

	/** Singleton : une seule classe Page  en même temps !*/
   public static function getInstance()
   {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
   }

   
   /** Assigne le fichier de design à la page**/
	function setDesign ($xslFile) {
		// En fonction des constantes "gate", on limite l'accès au site en redirigeant vers le template "locked.html"
		// On applique pas ce comportement pour les appels à empty.php (pr interaction ajax)
		// ATTENTION LE TEST DOIT RESTER DANS CET ORDRE
		//trace("ws setdesign");
		if($this->isLockedByGateBehavior()){
			// throw new Exception ("locked");
			$this->template = templateDir."empty.html";
		}else if (file_exists($xslFile)){
			$this->template=$xslFile; 
		}else{
			print('Le design de la page n existe pas');
		}
	}
	
	function isLockedByGateBehavior(){
		$this->locked = false ;
		require_once(libDir.'class_user.php');
		$user = new User() ; 
		$user->getFromSession();

		if(defined("gAPILoggedOnlyGate")){
			if(isset($user) && !$user->loggedIn()){
				$this->locked = "APILoggedOnlyGate";
			}else{
				$this->locked = false;
			}
		}
		return $this->locked ; 
	}
	


	/** Calcule et renvoie le HTML d'une page
	 * Analyse du template : extraction et traitement des balises spéciales
	 * IN : template (init par méthode SetDesign)
	 * OUT : HTML
	*/
	function render($buffered=true, $param_in_header=true) {
		try {
			
			$xmlHeader="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<rsp stat='%s'>";
			//$xmlHeader.="<service>".htmlentities($_SERVER['REQUEST_URI'])."</service>";				
			
			if ($param_in_header) {
				$xmlHeader.="<params>
					<get>";
				$xmlHeader.=$this->request2xml($_GET,'');
				$xmlHeader.="</get>
					<post>";
				$xmlHeader.=$this->request2xml($_POST,'');		
				$xmlHeader.="</post>\n";
				if(isset($_FILES) && !empty($_FILES)){
					$xmlHeader.="<files>";		
					$xmlHeader.=$this->request2xml($_FILES,'',array('tmp_name'));		
					$xmlHeader.="</files>\n";
				}
				$xmlHeader.="</params>";
			}
			$xmlFooter="</rsp>";
			
			if($this->locked){
				switch($this->locked){
					case "APILoggedOnlyGate" : 
						if(isset($this->error_msg) && !empty($this->error_msg)){
							$this->error_msg = kErrorWSLoggedOnlyGate.' : '.$this->error_msg;
						}else{
							$this->error_msg = kErrorWSLoggedOnlyGate;
						}
						$this->error_code = 13 ; 
					break ; 
				}
				throw new Exception ($this->error_msg,$this->error_code);
			}
			
			$src=file_get_contents($this->template);
			$regexp='`<php_param name="(.*)" />`siu';
			$src=preg_replace ($regexp,"<?=$1?>",$src); //Remplacement des balises <php_param... par la valeur

			$src=preg_replace ("`/\*.*\*/`siu","",$src);

			$motifbalise='`<include[^>]+/>`siu';
			$html=@preg_split($motifbalise,$src); // Pour le HTML hors balise

			@preg_match_all($motifbalise,$src,$includes,PREG_SET_ORDER); // Pour les balises include

			
			if ($buffered) ob_start();
			///ob_start("ob_gzhandler"); //experimental : compression


			for ($i=0;$i<count($includes);$i++) {
				print($html[$i]);

				$attribs=get_attributes_from_tag($includes[$i][0],array("id","file"));
				
				switch ($attribs["id"][0]) {
					case "content" : $this->dspContent(); break;
					default : print ("Included order not existing"); break;
				}
			}
			print($html[$i]);
		
			if ($buffered) {
				$html=ob_get_contents(); // récupération du buffer
				ob_end_clean(); //vidage du buffer		
				ob_start();
				//Si flux binaire, pas d'eval du code
				if ($this->binary===true) 
					echo $html;
					else eval("?".chr(62).$html.chr(60)."?"); //eval du code pour interpréter le PHP embarqué.
				$html2=ob_get_contents(); // récupération du buffer
				ob_end_clean(); //vidage du buffer	
				
				if ($html2===false) { //erreur lors du rendu => erreur interne
					header("HTTP/1.0 500 Internal Server Error",TRUE,500);
			        header("Status: 500 Internal Server Error",TRUE,500);
			        $_SERVER['REDIRECT_STATUS'] = 500;
					if (!defined('debugMode') || debugMode==false) { //si en prod, on redirige vers une page d'erreur
																		
					}else {
						//debug($html); //sinon on crache le code fautif
					}
				
			}
				
			//On a importé les données brutes (include php + interprétation)
			//On va maintenant vérifier d'éventuels headers
				
			//Si on a une date de modif => ajout au header (fichiers + page détails)
			if (isset($this->lastModified)) {
				header("Last-Modified: " . $this->lastModified);
			}
		
			//Requête avec un header if-modified-since + date => on vérifie si le contenu est plus récent,
			//sinon on renvoie un header 304 => la page n'est pas transmise alors
			if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])&& isset($this->lastModified)) {
				$if_modified_since = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
				$lastModified = strtotime($this->lastModified);
				if ($if_modified_since >= $lastModified)
		         {
		        	header("HTTP/1.0 304 Not Modified",TRUE,304);
		         	header("Status: 304 Not Modified",TRUE,304);
		         	$_SERVER['REDIRECT_STATUS'] = 304;
		        	exit;
		         }
			}
			
				if ($this->encoding=='ISO-8859-1') $html2 = mb_convert_encoding($html2,"ISO-8859-1","UTF-8");
				if ($this->encoding=='MAC-ROMAN') $html2 = utf8_to_MacRoman($html2);
				
				
				if ($this->binary===true) { //flux binaires => renvoyé tel quel
					return $html2;
				}
				else { //sinon c du xml => ajout des headers & footers
					header("Content-Type: text/xml");
					return sprintf($xmlHeader,"ok").$html2.$xmlFooter;
				}
			}
		}
		catch (Exception $e) { //si erreur quelconque, throw depuis le fichier include
			header("HTTP/1.0 400 Bad Request",TRUE,400);
            header("Status: 400 Bad Request",TRUE,400);
            header("Content-Type: text/xml");
            $_SERVER['REDIRECT_STATUS'] = 400;
           
			return sprintf($xmlHeader,"fail")."<data><error_code>".$e->getCode()."</error_code><error_msg>".$e->getMessage()."</error_msg></data>".$xmlFooter;
		}
	}


	function getActionFromURL() {
		$this->urlaction=$_REQUEST[$this->prmAction]; //XXX : changé par LD le 10/05/07 pour AJAX type POST
        if(strpos($this->urlaction, '../')!==false) $this->urlaction="";
		return $this->urlaction;
	}



	function getAccess($type) {
		global $adminPages,$docPages,$usagerPages,$loggedPages,$usagerAdvPages;

		switch ($type) {

			case kLoggedAdmin : $pages=$adminPages;break;
			case kLoggedDoc : $pages= $docPages;break;
			case kLoggedNorm : $pages = $usagerPages;break;
			case kLoggedUsagerAdv : $pages = $usagerAdvPages;break;
			case kNotLogged : $pages = $loggedPages;break;
			default : $pages = $loggedPages;
		}
		//trace(strpos($pages,$this->urlaction.".php"));
		//On cherche la page demandée parmi les url interdites (extension php pour être backward compatible avec la v1)
		if(!is_array($pages)) {
			$aPages = explode(",",$pages);
		} else {
			$aPages = $pages;
		}
		$aPages = array_map("trim", $aPages);
		
		if(!in_array($this->urlaction.".php",$aPages)) {
			return true;
		} else {
			return false;
		}
		//if (strpos($pages,$this->urlaction.".php")===false) return true; else return false;
	}

	private function dspContent () {
//



	 	 if(isset($_GET["page"])) $page=$_GET["page"];
//   			 else $page="html/index_".$_SESSION["langue"].".html";
	  	$this->getActionFromURL();
		$user=User::getInstance();

	  	if (substr($this->urlaction,-4)==".php") {
	  			$this->urlaction=substr($this->urlaction,0,-4);
	  			trace("Attention : nom de page PHP dans l'url :".$this->urlaction.".php");
	  	}

	    if (!empty($this->urlaction) ) { // J'ai retiré && !empty($user->UserID) car ça empêche la navigation anonyme !

		    if (file_exists($this->includePath.$this->urlaction.".php")) {

				// Si pas de droit d'accès, message.
				if ($this->getAccess($user->getTypeLog()) ) { //By LD 07/05/08 => si un jour il faut ajouter l'appartenance à un groupe ajouter : && !empty($user->Groupes)
						$incfile=$this->includePath.$this->urlaction.".php";
						$error="";
						}
					else {throw new Exception(kAccesLogin,"013");
						//$error="<h3>".kAccesLogin."</h3>";$incfile="";
						}
		    }
		    else { //tentative d'accès à une page qui n'existe pas
		    	throw new Exception(kErreurPageNotFound,"012");
		    	//$error="<h3>".kErreurPageNotFound."</h3>"; $incfile="";// juste un message, mais à logger ?
		    }
	    } else if(isset($_GET["html"])){ // page HTML
			require_once(modelDir.'model_html.php');
			$myHtml=new Html();
			$myHtml->t_html['ID_HTML']=$_GET["html"];
			//$myHtml->t_html['ID_LANG']="FR";
			$myHtml->getHtml();
			echo $myHtml->t_html['HTML_CONTENU'];
	    } else { //pas d'action, ou pas de user : on affiche la page par déft : design/index.php
	    	//if (empty($user->UserID)) echo kErrorNoLogin;
	    	$incfile=getSiteFile("designDir","index.php");
	    }
	    if (!empty($error)) {echo $error;} elseif (!empty($incfile)) {require($incfile);} else echo '';
		
	}




	/**
	 * Préparation d'un ordre SQL à partir de l'analyse du paramètre sort dans l'url.
	 * IN : paramètre tri dans le GET, récup ordre par défaut true/false
	 * OUT : SQL (ORDER BY..., direct) et màj des variables order (col), triInvert (sens passé à l'url)
	 * 		et params4XSL (params de base passés aux XSL)
	 */
	function getSort ($withDefault=true,&$default=true,$default_field='') {
		global $db;
        $tri=$this->getSortFromURL();
        
        if($tri!=""){ //Calcul de la colonne de tri, cette valeur sera envoyée à la XSL pour afficher les flèches
            $ordre=substr($tri,-1,1);
            $col = substr($tri,0,-1);
			if (!is_numeric($ordre)) {
				$ordre=0;
				$this->order=$tri;
				$col=$tri;
			}
			if ($ordre==0) {
				$this->triInvert="1";
				$this->col=$col;
			}else{
				$this->triInvert="0";
				$this->col=$col;
			}

			$this->initParams4XSL();

			//Deuxième partie, calcul du SQL de tri qui peut comporter plusieurs colonnes
			$_hasbrackets=preg_match("/^\[(.*)\](.*)$/i",$tri,$matches); //Présence de crochet autour des champs => concaténation
			if ($_hasbrackets) {
				$tri=$matches[1].$matches[2]; //on se débarrasse des crochets (mais on garde le sens)
				$sqlTri=str_replace(';',',',$tri);
				if (substr($sqlTri,-1,1)=='1') {
						$_cols=strtoupper(substr($sqlTri,0,-1));
						$_sens=" DESC "; }
				elseif (substr($sqlTri,-1,1)=='0') {
						$_cols=strtoupper(substr($sqlTri,0,-1));
						$_sens=" ASC "; }
				elseif (!is_numeric(substr($sqlTri,-1,1))) {
						$_cols=strtoupper($sqlTri);
						$_sens=" ASC ";
				}
				if (!empty($_cols)) return " ORDER BY ".$db->Concat($_cols).$_sens;
			} else {
				// VP 19/1/09 : ajout tri multiple
				$sqlTri="";
				$fields=explode(",",$tri);
				foreach ($fields as $field) {
					$field=trim($field);
					$ordre=substr($field,-1,1);
					if (is_numeric($ordre)){
						if($ordre==1){$direction="DESC";}else{$direction="ASC";}
						$fld=substr($field,0,-1);
					}
					else {
						$direction="ASC";
						$fld=$field;
					}
					$sqlTri.=",".strtoupper($fld)." ".$direction;
				}
				if(!empty($sqlTri)) return " ORDER BY ".substr($sqlTri,1);
			}
        } elseif ($withDefault) {
        	if ($_POST['orderbydeft'])
        	$_SESSION['sqlorderbydeft'][$this->urlaction]=$this->addDeftOrder($_POST['orderbydeft']);
        	//by ld 22/12/08: introduction de urlaction pour gérer des ordres par deft différents suivant la page
        	//puisqu'on n'a pas l'objet recherche ici...
			
			return $_SESSION['sqlorderbydeft'][$this->urlaction];
        }
	}

	private function request2xml($array, $root = '',$exclude = array()) {
	
		if (!is_array($array)) return false;
	    if ($root!='' && !in_array($root,$exclude)) $xml = "<$root>";
	    foreach($array as $k => $v) {
	    	
	        if (is_array($v)) {
	            $xml .= $this->request2xml($v, (is_numeric($k) ? $root."_".$k : $k),$exclude);
	        } else {
	            if (is_numeric($k) && !in_array($root,$exclude)) {
	                $xml .= "<".$root."_".$k.">".xmlformat($v)."</".$root."_".$k.">";
	            } else if(!is_numeric($k) && !in_array($k,$exclude)) {
	                $xml .= "<$k>".xmlformat($v)."</$k>";
	            }
	        }
	    }
	     if ($root!='' && !in_array($root,$exclude)) $xml .= "</$root>";
	    if (preg_match("/<$root>(<$root>.*<\/$root>)<\/$root>/", $xml, $parts)) {
	        $xml = $parts[1];
	    }
	    return $xml;
	
	}


	/**
	 * Initialisation du Pager.
	 * IN : SQL, new_max (opt, nb de lignes max),
	 * 		$altVar = autre mode d'initialisation de la page en cours (ex : session),
	 * 			NOTE: cette variable n'est utile que si le sélecteur du nb de lignes est mis dans la XSL.
	 * 			Si ce sélecteur est appelé dans la page PHP, la var de session est déjà initialisée
	 * 		$addGETVars = variables additionnelles à passer aux liens du Pager
	 * OUT : var de classe : Result (resultat de requete "tronqué"),
	 * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales), PagerLink (url passée de page en page)
	 */
    function initPager($sql, $new_max = 10,$altVar=null,$addGETVars=null,$secstocache=0)
    {
        global $db;
		
        if (!isset($this->nbLignes)) $this->max_rows = $new_max; else $this->max_rows=$this->nbLignes;

       	$this->getPageFromURL();
        if ($this->page=="" && $altVar!=null) $this->page=$altVar;
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) ) $this->page=1;

        if($this->max_rows == "all"){
        	$i=0;
            if ($secstocache==0) $rsfull= $db->Execute($sql); else $rsfull=$db->CacheExecute($secstocache,$sql);

            if (!$rsfull) { //0 résultats retournés !
            	$this->rows=0;
            	$this->found_rows=0;
            	$this->page=1;
            	$this->PagerLink="";
            	$this->result=array();
            	$this->error_msg=kErrorWSRequeteNonValide;
            	return false;
            	}

            $this->max_rows = $rsfull->RecordCount();

            if ($this->max_rows>kMaxRows) $this->max_rows=kMaxRows; // précaution : si trop de lignes, on revient à un mode paginé pour ne pas surcharger le serveur
            else {
            $this->result=$rsfull->GetArray();
            $this->rows =$this->max_rows;
            $this->found_rows=$this->max_rows;
            $this->page=1;
            $rsfull->Close();
            }
        }

		if ($this->max_rows!="all")
		{
				
	        $i=0;
	        $rs_found_rows=$db->PageExecute($sql,$this->max_rows,$this->page,false,$secstocache);
		    if ($rs_found_rows) {
		        $this->found_rows = $db->_maxRecordCount; // Attention : propriété "cachée" de ADODB
		        $i_min = ($this->page - 1) * $this->max_rows;
		        if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
		       	$this->result=$rs_found_rows->GetArray();
		        //$this->result=array_slice($rs_found_rows,$i_min,$this->max_rows);
		        $this->rows =count($this->result);
				$rs_found_rows->Close();

		    } else {$this->found_rows=0;$this->page=1;$this->error_msg=kErrorWSRequeteNonValide;return false;}
		}
        $this->PagerLink=$this->getName()."?urlaction=".$this->getActionFromUrl().$this->addUrlParams($addGETVars)."&page=";
        if ($this->page>$this->num_pages()) $this->page=$this->num_pages();
        $this->params4XSL['nb_pages']=$this->num_pages();
        $this->params4XSL['nb_rows']=$this->found_rows;
        return true;
    }


	/**
	$nomelement : nom de l'�lement dans le xml
	$nomFichierXsl : nom du fichier xsl de transformation
	$options : 1 user, 2 doc, 3 admin
	$page : num�ro de la page

	$ajoutPrivilegeDoc : Faut-il ajouter aux r�sultats le statuts de leurs privil�ges (seulement utile pour la gestion des documents). N.b. Ceci pourrait �tre g�rer de fa�on plus propre dans une classe Doc
	*/
	function afficherListe($nomelement,$nomFichierXsl,$ajoutPrivilegeDoc=FALSE,$arrHighlight=null,$xml2merge='',$eval=false)
	{


	        if ($ajoutPrivilegeDoc){
	            $this->result = ajoutPrivResult($this->result);
	        }

			// if ($this->result!=null)
			// {
			$xml = TableauVersXML($this->result,$nomelement,4,"select",1,$xml2merge); //classique

			// VP 6/03/2013 : transformation de &lt; &gt; et &amp; en &amp;lt; &amp;gt; &amp;amp;
			$xml=str_replace("&","&amp;",$xml);

			$log = new Logger("data.xml");
			$log->Log($xml);
			foreach($this->params4XSL as $ind => $data) $tab[$ind] = $data;
			$html=TraitementXSLT($xml,$nomFichierXsl,$tab,0,$xml,$arrHighlight);
			if(!empty($eval) && $eval=true){
				$html = eval("?".chr(62).$html.chr(60)."?");
			}
			echo $html;
	      //  }
	      //  else print kAucunResultat;
	}




} // fin de classe PAGE
?>
