<?php
include_once(libDir."class_visualisation.php");

class VisualisationQT extends Visualisation {

	var $typeVisu;
	var $mediaUrl; // url fichier media
	var $mediaPath; //path media sur disk
	private $vignette;

	function __construct ($type,$src_name = null ) {	
		$this->typeVisu=$type;
		if(isset($src_name) && $src_name){
			$this->sourceVisu=$src_name;
		}
	}
        
        
	/** R√©cup√®re et pr√©pare le visionnage et le d√©coupage en fonction du type de visionnage
	 * et lance les traitements
	 * ID => ID de l'entité à visionner : doc, matériel, etc.
	 * ID_LANG => langue de l'entité à visionner
	 *
	 */

	function prepareVisu($id,$id_lang,$flag_decoup = true,$hash=null,$id_mat=null) {
		
		switch ($this->typeVisu) {
			case 'materiel' :
				include_once(modelDir.'model_materiel.php');
				$this->objMat= new Materiel();
				$this->objMat->t_mat['ID_MAT']=$id;

				$this->objMat->getMat();
				$this->tcin=$this->objMat->t_mat['MAT_TCIN'];
				$this->tcout=$this->objMat->t_mat['MAT_TCOUT'];
				$this->duration=$this->objMat->t_mat['MAT_DUREE'];
				$this->offset=$this->objMat->t_mat['MAT_TCIN'];
				$this->id_mat=$this->objMat->t_mat['ID_MAT'];
				$this->objMat->id_lang=$id_lang;
				$ok=$this->objMat->hasfile;
				$this->calcResolution($this->objMat->t_mat['MAT_INFO']);
				$this->MimeType=getMimeType($this->objMat->t_mat['MAT_NOM']);

				if(isset($this->objMat->t_mat['MAT_ID_IMAGEUR']) && !empty($this->objMat->t_mat['MAT_ID_IMAGEUR'])){
					$dir=floor(intval($this->objMat->t_mat['MAT_ID_IMAGEUR'])/1000);
					$sub_dir=intval($this->objMat->t_mat['MAT_ID_IMAGEUR'])-(intval($dir)*1000);
					$path_mosaic= sprintf('%04d/%04d/',$dir,$sub_dir)."mosaic.jpg";
					$path_idx_mosaic= sprintf('%04d/%04d/',$dir,$sub_dir)."mosaic_tc.txt";
					if(is_file(kStoryboardDir.$path_mosaic) && is_file(kStoryboardDir.$path_idx_mosaic)){
						$this->mosaic_path = storyboardChemin.$path_mosaic;
						$this->mosaic_pics_index =  file_get_contents(kStoryboardDir.$path_idx_mosaic);
					}
				}

				if(empty($this->tempDir)) $ok=$this->makeDir();
                                
				if ($ok) $ok=$this->makeFileName(true);

				//if ($ok) $ok=$this->copyRefFiles();
				$ok=$this->copyMovie();
				if ($ok) {
					$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
					$this->mediaPath=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
				}
				else {$this->dropError(kErrorVisioNoMedia);}
			break;

			case 'playlist' :
				// VP 29/01/10 : corrections diverses pour playlist
				$ok=$this->makeDir();
				$idDocList=explode(",",$id);
				$this->typeVisu="document";
				$xml_playlist = "&lt;tracklist&gt;\n";
				$duration_playlist = 0;

				if($flag_decoup){
					// initialisation des paramètres pour la génération du fichier de montage
					if($_SESSION['etat_montage']['state']!="error" && $_SESSION['etat_montage']['state']!="ended" && $_SESSION['etat_montage']['state']!=NULL){
						$this->dropError(kErrorGenPlaylistBusy);
						return false;
					}
					session_start();
					$_SESSION['etat_montage']['state']="init";
					$_SESSION['etat_montage']['nb_seqs'] = count($idDocList);
					$_SESSION['etat_montage']["file"] = "";
					session_write_close();

					if(isset($_GET['montage_ratio']) && strlen($_GET['montage_ratio'])<=4){
						eval('$this->playlist_ratio = '.$_GET["montage_ratio"].';');
					}else{
						$this->playlist_ratio = 16/9;
					}
				}
				
				
				
				foreach($idDocList as $idx=>$idDoc){
					if(isset($this->montage_xml)){
						$nodeclip_item = $this->montage_xml->xpath('//clipitem[position() = '.($idx + 1).']');
					}else{
						$nodeclip_item = null ; 
					}
					
					if(strpos($idDoc,'pic') === 0){
						$ok = $this->genVisuCarton($idDoc,$nodeclip_item );
					}else if(strpos($idDoc,'carton') === 0){
						$ok = $this->genVisuCarton($idDoc,$nodeclip_item );
					}else{
						$ok=$this->prepareVisu($idDoc,$id_lang,false);
					}

					if($ok && $flag_decoup){
						session_start();
						if($_SESSION['etat_montage']["state"] == "init"){$_SESSION['etat_montage']["state"] = "prep";}
						$_SESSION['etat_montage']["current"] = $idx+1;
						session_write_close();

						//lancement découpage
						$ok = $this->genPartMontage();
						if(!$ok){
							$this->dropError(kErrorPrepElemPlaylist);
							session_start();
							$_SESSION['etat_montage']["state"] = "error";
							session_write_close();
							return false;
						}
					}

					if($ok) {
						$mediaPaths[]=$this->mediaPath;

						$duration = (tcToSecDec($this->tcout)-tcToSecDec($this->tcin));
						$duration_playlist = $duration_playlist+$duration;
						// VP 6/06/2018 : On prend le premier élément dans le cas d'un visionnage à plusieurs qualités (amélioré en opsis3.1.1)
						if(is_array($this->mediaUrl)){
							$mediaUrl=reset($this->mediaUrl);
						}else{
							$mediaUrl=$this->mediaUrl;
						}
						$xml_playlist .="\t&lt;track&gt;\n";
						if($_REQUEST['pattern'] == 'gListePatternVisio' && is_array($this->mediaUrl)){
							if(defined("gListePatternVisioPrevMontage")){
								$xml_playlist .="\t\t&lt;location&gt;".$this->mediaUrl[gListePatternVisioPrevMontage]."&lt;/location&gt;\n";
							}else{
								$xml_playlist .="\t\t&lt;location&gt;".reset($this->mediaUrl)."&lt;/location&gt;\n";
							}
						}else{
							$xml_playlist .="\t\t&lt;location&gt;".$this->mediaUrl."&lt;/location&gt;\n";
						}
						$xml_playlist .="\t\t&lt;start_offset&gt;".(tcToSecDec($this->tcin)-tcToSecDec($this->offset))."&lt;/start_offset&gt;\n";
						$xml_playlist .="\t\t&lt;duration&gt;".$duration."&lt;/duration&gt;\n";
						$xml_playlist .="\t&lt;/track&gt;\n";
					}
					unset($this->tcin);
					unset($this->tcout);
					unset($this->height);
					unset($this->width);
					unset($this->offset);

				}
				if($_REQUEST['pattern'] == 'gListePatternVisio' && is_array($this->MimeType)){
					if(defined("gListePatternVisioPrevMontage")){
						$this->MimeType = $this->MimeType[gListePatternVisioPrevMontage];
					}else{
						$this->MimeType = reset($this->MimeType);
					}
				}
				$xml_playlist .="&lt;list_elems&gt;&lt;![CDATA[".$id."]]&gt;&lt;/list_elems&gt;";
				$xml_playlist .="&lt;nb_elems&gt;".count($idDocList)."&lt;/nb_elems&gt;";
				$xml_playlist .= "&lt;/tracklist&gt;\n";

				$this->xml_playlist = $xml_playlist;
				$this->tcout = secDecToTc($duration_playlist);
				unset($xml_playlist);
				unset($this->mediaPath);
				unset($this->mediaUrl);
				unset($this->id_mat);

				if ($flag_decoup){
					if($mediaPaths ){
						session_start();
						$_SESSION['etat_montage']["state"] = "encod";
						session_write_close();

						$ok=$this->catMovie($mediaPaths);
						if(!$ok){
							$this->dropError(kErrorConcatPlaylist);
							session_start();
							$_SESSION['etat_montage']["state"] = "error";
							session_write_close();
							return false;
						}
						if ($ok) {
							session_start();
							$_SESSION['etat_montage']["state"] = "ended";
							$_SESSION['etat_montage']["file"] = $this->mediaPath;
							trace("set [etat_montage][file] : ".$this->mediaPath." ==> ".$_SESSION['etat_montage']['file']);
							session_write_close();
						}
					}else {$this->dropError(kErrorVisioNoMedia);}
				}
			break;

			case 'playlistMat' :
				// VP 29/01/10 : corrections diverses pour playlist
				$ok=$this->makeDir();
				$idMatList=explode(",",$id);
				$this->typeVisu="materiel";
				$dureeSec=0;
				foreach($idMatList as $idMat){
					unset($this->tcin);
					unset($this->tcout);
					$ok=$this->prepareVisu($idMat,$id_lang);
					$dureeSec+=timeToSec($this->duration);
					if($ok) $mediaPaths[]=array("mediaUrl"=>$this->mediaUrl,"duration"=>$this-duration);

				}
				if($mediaPaths){
					$this->fileOut="playlist.xml";
					$ok=$this->makePlaylistXML($mediaPaths);
					if ($ok) {
						$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
						$this->mediaPath=$this->tempDir."/".$this->fileOut;
						$this->offset="00:00:00:00";
						$this->tcin="00:00:00:00";
						$this->tcout=secToTc($dureeSec);
					}
				}else {$this->dropError(kErrorVisioNoMedia);}

				break;

			case 'playlistDocMat' :
				include_once(modelDir.'model_doc.php');
				$xml_playlist = "&lt;tracklist&gt;\n";
				$duration_playlist = 0;
				$mediaPaths = array();
				// VP 29/01/10 : corrections diverses pour playlist
				$this->objDoc=new Doc();
				$this->objDoc->t_doc['ID_DOC']=$id;
				$this->objDoc->t_doc['ID_LANG']=$id_lang;
				$this->objDoc->getDoc();
				$this->objDoc->getMats(); //récup ID séquences liées
				$this->objDoc->getChildren(); //récup matériel

				//A partir de là, on peut extraire tous les TC nécessaires.
				//D'abord il faut récupérer le matériel de visualisation
				$gotOne=false;
				$ok=$this->makeDir();
				if ($ok) $ok=$this->copyRefFiles();

				// VP (18/04/08) : Tri du tableau multidim par création d'un tableau intermédiaire
				foreach ($this->objDoc->t_doc_mat as $key => $dm) {
					$tmpsort[$key]=$dm['MAT']->t_mat["MAT_NOM"];
				}
				array_multisort($tmpsort,SORT_ASC,$this->objDoc->t_doc_mat);
				unset($tmpsort);

				if (isset($_GET['format'])) {$formats = array($_GET['format']);}
				else $formats=$this->getFormatsForVisu();
				foreach ($formats as &$format) $format = strtoupper($format);
				$pattern = $_GET['pattern'];

				foreach ($this->objDoc->t_doc_mat as $dm) {
					if (in_array(strtoupper($dm['MAT']->t_mat['MAT_FORMAT']),$formats) && $dm['MAT']->hasfile==1  && $dm['DMAT_INACTIF']!='1' && ((!empty($pattern) && stristr($dm['MAT']->t_mat["MAT_NOM"],$pattern)) || empty($pattern))) {
						if(!$gotOne) {
							$this->calcResolution($dm['MAT']->t_mat['MAT_INFO']);
							$this->MimeType=getMimeType($dm['MAT']->t_mat['MAT_NOM']);
						}

						$this->id_mat=$dm['ID_MAT'];
						$this->tcin=$dm['DMAT_TCIN'];
						$this->tcout=$dm['DMAT_TCOUT'];
						$this->objMat=$dm['MAT'];

						$numer=tcToSec($this->tcout)-tcToSec($this->tcin);  //durée de l'extrait
						$denom=tcToSec($this->objMat->t_mat['MAT_TCOUT'])-tcToSec($this->objMat->t_mat['MAT_TCIN']); //durée totale du matériel
						$decoup =(($denom==0  || $numer/$denom!=1 ) && $numer!=0 );
						$this->offset=(empty($this->objMat->t_mat['MAT_TCIN'])?"00:00:00:00":$this->objMat->t_mat['MAT_TCIN']);

						//Préparation du découpage
						if ($ok && $decoup ) {
							$ok=$this->sliceMovie();
							$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;

						} else { //Pas de découpage, on fait une copie simple du media
							$this->makeFileName(true);
							$ok=$this->copyMovie();
							$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
						}

						$duration = (tcToSecDec($this->tcout)-tcToSecDec($this->tcin));
						$duration_playlist = $duration_playlist+$duration;

						$xml_playlist .="\t&lt;track&gt;\n";
						$xml_playlist .="\t\t&lt;location&gt;".$this->mediaUrl."&lt;/location&gt;\n";
						$xml_playlist .="\t\t&lt;start_offset&gt;".(tcToSecDec($this->tcin)-tcToSecDec($this->offset))."&lt;/start_offset&gt;\n";
						$xml_playlist .="\t\t&lt;duration&gt;".$duration."&lt;/duration&gt;\n";
						$xml_playlist .="\t&lt;/track&gt;\n";

						$mediaPaths[]=$this->mediaPath;

						$gotOne=true;
					}
				}
				if (!$gotOne) {
					$this->dropError(kErrorVisioNoMedia);
					$ok = false;
				} else {
					if ($ok) {
						$xml_playlist .="&lt;list_elems&gt;".$id."&lt;/list_elems&gt;";
						$xml_playlist .="&lt;nb_elems&gt;".count($mediaPaths)."&lt;/nb_elems&gt;";
						$xml_playlist .= "&lt;/tracklist&gt;\n";

						$this->xml_playlist = $xml_playlist;
						$this->tcout = secDecToTc($duration_playlist);
						unset($xml_playlist);
						unset($this->mediaPath);
						unset($this->mediaUrl);
						unset($this->id_mat);
						//$ok=$this->makeXML(); // génération du XML
					}
				}

				break;

			case 'stream_hls' : 
				if(isset($_REQUEST['pattern']) && !empty($_REQUEST['pattern'])){
					$this->mediaUrl=kStreamServerUrl.'/'.$_REQUEST['pattern']."/playlist.m3u8";
				}
				$ok = true ; 
				break ; 
				
			case 'visuExtrait' :
			case 'commande':
				global $db;
				$lp=$db->GetRow("SELECT * from t_panier_doc WHERE id_ligne_panier=".intval($id));
				$id=$lp['ID_DOC'];
				$this->tcin=$lp['PDOC_EXT_TCIN'];
				$this->tcout=$lp['PDOC_EXT_TCOUT'];

				// MS - correction pour que le cas commande fallback sur les tc normaux si il ne s'agit pas d'extraits
				if(isset($this->tcin) && isset($this->tcout) && $this->tcin == $this->tcout){
					$fallback_commande = true ;
				}else{
					$fallback_commande = false ;
				}

				// NOTE : pas de break car on veut continuer sur la suite

			default :
				if(defined("visioUseHash") && visioUseHash){
					if ($hash == null) {
						exit;
					} else {
						preg_match('/http[s]*:\/\/([a-zA-Z0-9\.\-_]+)/',$_SERVER['HTTP_REFERER'],$matches);
						if(isset($matches[1]) && !empty($matches[1])) {
							$from = $matches[1];
						} else {
							trace("visualisation - impossible d'extraire le referrer de l'url");
						}
						$hash_key = md5($id.$id_lang.$from.PLAYER_EXPORT_SECRET_KEY);

						if ($hash_key != $hash) {
							echo 'bad hash';
							exit;
						}
					}
				}

				$ok=true;
				// Type document
				// VP 28/01/10 : cas où id est sous la forme id_tcin_tcout
				$idsplit=explode("_",$id);
				if(count($idsplit)==3){
					$id=$idsplit[0];
					$this->tcin=$idsplit[1];
					$this->tcout=$idsplit[2];
				}
				include_once(modelDir.'model_doc.php');

				$search = array(" ' ",'"',"&","<",">");
				$replace = array("'","&quot;","&amp;","&lt;","&gt;");

				$this->objDoc=new Doc();
				$this->objDoc->t_doc['ID_DOC']=$id;
				$this->objDoc->t_doc['ID_LANG']=$id_lang;
				$this->objDoc->getDoc();
				$this->objDoc->getMats(); //r√©cup ID s√©quences li√©es
				$this->objDoc->getChildren(); //r√©cup mat√©riel
				$this->objDoc->getVignette(); //recuperation vignette
				if(isset($this->getDocSeq) && $this->getDocSeq){
					$this->objDoc->getSequences(); //recuperation sequences
				}
				//A partir de l√†, on peut extraire tous les TC n√©cessaires.
				//D'abord il faut r√©cup√©rer le mat√©riel de visualisation
				// Format
				if (isset($_GET['format'])) {
					$formats = array($_GET['format']);
				}
				else $formats=$this->getFormatsForVisu();
				
				// VP 10/03/09 : ajout fonction de recherche du matériel à visionner
				$gotOne=false;
				// VP 22/07/10 : ajout paramètre pattern pour choisir de préférence un fichier
				if (!empty($_REQUEST['gListePatternVisioDynamic'])) {
					$arrDm = $this->objDoc->getMaterielVisu(false,true,json_decode($_REQUEST['gListePatternVisioDynamic'], true),$id_lang,$formats);
					if (empty($arrDm)) $arrDm[] = $this->objDoc->getMaterielVisu(false,false,$_REQUEST['gListePatternVisioDynamic'],$id_lang,$formats);
				}
				else if ($_GET['pattern'] == "gListePatternVisio" && defined("gListePatternVisio")) {
					// $arrDm = $this->getMatsForVisio($id_lang,$formats,unserialize(gListePatternVisio));
					$arrDm = $this->objDoc->getMaterielVisu(false,true,unserialize(gListePatternVisio),$id_lang,$formats);
					if (empty($arrDm)){
						$this->objDoc->getMaterielVisu(false,false,$_GET['pattern'],$id_lang,$formats);
					}
				}else if(defined("gAllowMultipleVisFiles") && gAllowMultipleVisFiles){
					// $arrDm=$this->getMatsForVisio($id_lang,$formats,$_GET['pattern']);
					$arrDm= $this->objDoc->getMaterielVisu(false,true,$_GET['pattern'],$id_lang,$formats); 
                                //B.RAVI 2016-04-27 on veut un matériel particulier mais avec "id=[id_doc]&type=document" car on veut pouvoir recupérer les matériels sous-titres (fichier .srt,. vtt) reliés à la notice (id_doc) (besoin sur le projet Colas)  
				}else if($id_mat>0){     
                    include_once(modelDir.'model_materiel.php');
                    $matWanted= new Materiel();
                    $matWanted->t_mat['ID_MAT']=$id_mat;
                    $matWanted->getMat();
                    $matWanted->getDocMat();
                    
                    //Comme un matériel peut-être lié à plusieurs notices (donc avoir plusieurs t_doc_mat) on parcourt le tableau t_doc_mat à la recherche du bon
                    foreach ($matWanted->t_doc_mat as $key => $value) {
                        if ($value["ID_DOC"]==$id){
                            $key_t_doc_mat=$key;
                            break;
                        }
                    }
                    
                    //On restructure le tableau pour $arrDm garde la même structure que dans les autres cas
                    $tab_matWanted=$matWanted->t_doc_mat[$key_t_doc_mat];
                    $tab_matWanted["MAT"]=$matWanted;
                 
                    $arrDm=array($tab_matWanted);
				}else {
					$arrDm[]= $this->objDoc->getMaterielVisu(false,false,$_GET['pattern'],$id_lang,$formats); 
				}
								
				if ($fallback_commande)
					$this->tcin = $this->tcout = null;
				$this->mediaUrl=array();
				$this->mediaPath=array();
				

                             
				foreach ($arrDm as $key=>$dm)
				if(!empty($dm)){
					if(empty($this->tempDir)) $ok=$this->makeDir();

					$this->id_mat[$key]=$dm['ID_MAT'];
					// MS - 19.01.18 - suppression de ce test / continue , je ne comprends pas à quoi il sert : 
					// lors de l'ajout des gListePatternVisio, ce test avait été ajouté, avait déjà posé des pbs sur les extraits, 
					// j'avais donc limité le test au cas pattern == gListePatternVisio, mais maintenant ce meme test pose problème avec les montages avec extrait dans le cas gListePatternVisio
					// pour l'instant ce test est donc commenté, à voir avec pierre
					/*if ( ($_GET['pattern'] == "gListePatternVisio" && defined("gListePatternVisio")) && (($this->tcin != '' && $this->tcin != $dm['DMAT_TCIN']) || ($this->tcout != '' && $this->tcout != $dm['DMAT_TCOUT']))){ 
					continue;
					}*/
					
					
					
					if (!$this->tcin || (isset($fallback_commande) &&  $fallback_commande) || (defined("gAllowMultipleVisFiles") && gAllowMultipleVisFiles)) $this->tcin=$dm['DMAT_TCIN'];
					if (!$this->tcout|| (isset($fallback_commande) &&  $fallback_commande) || (defined("gAllowMultipleVisFiles") && gAllowMultipleVisFiles)) $this->tcout=$dm['DMAT_TCOUT'];
					
					$this->vignette=$this->objDoc->vignette;


					$this->objMat=$dm['MAT'];

					if ((empty($this->tcout) || $this->tcout == "00:00:00:00") && (empty($this->tcin) || $this->tcin == "00:00:00:00")) {
						require_once(libDir.'class_matInfo.php');
						require_once(libDir."fonctionsGeneral.php");

						$infos=MatInfo::getMatInfo($this->objMat->getFilePath());
						if ($infos['timecode']!=null && !empty($infos['timecode']))
							$this->tcin=$infos['timecode'];
						else
							$this->tcin='00:00:00:00';
						$this->tcout = secDecToTc(tcToSecDec($this->tcin)+$infos['duration']);
					}

					$this->MimeType[$key]=getMimeType($this->objMat->t_mat['MAT_NOM']);
					$numer=tcToSec($this->tcout)-tcToSec($this->tcin);  //dur√©e de l'extrait
					$denom=tcToSec($this->objMat->t_mat['MAT_TCOUT'])-tcToSec($this->objMat->t_mat['MAT_TCIN']); //dur√©e totale du mat√©riel
					//trace("ratio decoup :".$numer."/".$denom);
					// cas de d√©coupage : pas de dur√©e du mat√©riel (dans le doute on d√©coupe) ou bien ratio inf√©rieur √† la limite)
					// sinon on ne d√©coupe pas : ratio sup√©rieur, dur√©e extrait inconnue,...
					if (($denom==0  || $numer/$denom<0.9 ) && $numer!=0 && $flag_decoup) $decoup=true;
					if ($this->typeVisu=='commande' && $flag_decoup ) $decoup=true; //commande ? on force le d√©coupage
					trace("flag_decoup : ".intval($flag_decoup)." decoup : ".intval($decoup));

					$this->offset=(empty($this->objMat->t_mat['MAT_TCIN'])?"00:00:00:00":$this->objMat->t_mat['MAT_TCIN']);


					//Pr√©paration du d√©coupage

					if ($ok && $decoup) {
						// VP 5/05/10 : appel des outils de découpage dans méthode sliceMovie
						$ok=$this->sliceMovie();
						$this->mediaUrl[$key]=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
						$this->mediaPath[$key]=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
						// VP (27/02/09) Ajout de logo après découpage
						// VP 5/10/09 : ajout paramètre "nologo"
						if (!isset($_GET["nologo"]) && defined("gVideoLogoPath") && defined("gVideoLogoUtil")  && strpos(PHP_OS,'Darwin')===0 ) {
							// Uniquement pour les fichiers MPEG4
							if (strpos($this->objMat->t_mat['MAT_FORMAT'],'MPEG4')!==false || strpos($this->objMat->t_mat['MAT_FORMAT'],'MPG4')!==false ) {
								$tmpFile=stripExtension($this->fileOut)."_logo.mov";
								$tmpFilePath=kCheminLocalVisionnage.$this->tempDirShort."/".$tmpFile;
								$_binary=explode(" ",gVideoLogoUtil); //analyse du path pour trouver le chemin bin en laissant de cote les autres cmd (sudo, etc)
								$_binary=$_binary[count($_binary)-1];
								if (is_file($_binary)){
									$cmd=gVideoLogoUtil." ".($this->mediaUrl[$key])." ".(gVideoLogoPath)." \"".$tmpFilePath."\" \"".kCheminLocalVisionnage.$this->tempDirShort."/logo.log\"";
									trace($cmd);
									exec($cmd);
									if(is_file($tmpFilePath)) {
										$this->fileOut=$tmpFile;
										$this->mediaUrl[$key]=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
										$this->mediaPath[$key]=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
									}
								}

							}
						}
					} else { //Pas de d√©coupage, on fait une copie simple du media
						// VP (27/02/09) Ajout de logo avant copie
						if (!isset($_GET["nologo"]) && defined("gVideoLogoPath") && defined("gVideoLogoUtil")  && strpos(PHP_OS,'Darwin')===0 ) {
							// Uniquement pour les fichiers MPEG4
							if (strpos($this->objMat->t_mat['MAT_FORMAT'],'MPEG4')!==false || strpos($this->objMat->t_mat['MAT_FORMAT'],'MPG4')!==false ) {
								$tmpFile=stripExtension($this->id_mat[$key])."_logo.mov";
								$tmpFilePath=kVideosDir.$tmpFile;
								// VP 27/05/10 : ajout du logo si date fichier modifié
								if(is_file($tmpFilePath) && (filemtime(kVideosDir.$this->id_mat)<filemtime($tmpFilePath))){
									$this->id_mat[$key]=$tmpFile;
								}else{
									$_binary=explode(" ",gVideoLogoUtil); //analyse du path pour trouver le chemin bin en laissant de cote les autres cmd (sudo, etc)
									$_binary=$_binary[count($_binary)-1];
									if (is_file($_binary)){
										$cmd=gVideoLogoUtil." ".(videosServer.$this->id_mat[$key])." ".(gVideoLogoPath)." \"".$tmpFilePath."\" \"".kVideosDir."logo.log\"";
										trace($cmd);
										exec($cmd);
										if(is_file($tmpFilePath)) $this->id_mat[$key]=$tmpFile;
									}
								}
							}
						}


						if(isset($this->objMat->t_mat['MAT_ID_IMAGEUR']) && !empty($this->objMat->t_mat['MAT_ID_IMAGEUR'])){
							$dir=floor(intval($this->objMat->t_mat['MAT_ID_IMAGEUR'])/1000);
							$sub_dir=intval($this->objMat->t_mat['MAT_ID_IMAGEUR'])-(intval($dir)*1000);
							$path_mosaic= sprintf('%04d/%04d/',$dir,$sub_dir)."mosaic.jpg";
							$path_idx_mosaic= sprintf('%04d/%04d/',$dir,$sub_dir)."mosaic_tc.txt";
							if(is_file(kStoryboardDir.$path_mosaic) && is_file(kStoryboardDir.$path_idx_mosaic)){
								$this->mosaic_path = storyboardChemin.$path_mosaic;
								$this->mosaic_pics_index =  file_get_contents(kStoryboardDir.$path_idx_mosaic);
							}
						}
						
						foreach ($this->objDoc->t_doc_mat as $dmat){
							if($dmat['MAT']->t_mat['MAT_TYPE'] == 'AUDIOWAVEFORM'){
								$waveform_ori = $dmat['MAT']->getFilePath() ; 
								symlink($waveform_ori,$this->tempDir.'/'.basename($waveform_ori));
								$this->waveform_path = kVisionnageUrl.$this->tempDirShort."/".basename($waveform_ori);
								unset($waveform_ori);
							}
						}

						if($this->sourceVisu  && $this->sourceVisu == 'limelight'){
							$this->fileOut = $this->objMat->t_mat['MAT_NOM'];
							// VP 19/05/2018 : prise en compte kLimelightDir pour calculer chemin relatif
							if(defined("kLimelightDir") && is_dir(kLimelightDir)){
								$mat_path = $this->objMat->getFilePath();
								if(strpos($mat_path, kLimelightDir)===0){
									$mat_path=substr($mat_path, strlen(kLimelightDir));
									$this->fileOut = trim($mat_path, "/");
								}
							}
							// MS remplacement du chemin dans le cas HLS
							if(isset($this->objMat->t_mat['MAT_FORMAT']) && stripos($this->objMat->t_mat['MAT_FORMAT'],'HLS')!==false ){
 								$this->fileOut = $this->fileOut."/".$this->objMat->t_mat['MAT_NOM'].".m3u8";
 							}
							$limelight_url=kCheminLimelight."/".$this->fileOut;
							if(defined("kLimelightMediavaultSecret") && kLimelightMediavaultSecret){
								if(defined("kLimelightMediavaultParams") && is_array(unserialize(kLimelightMediavaultParams))){
									$mediavault_params = unserialize(kLimelightMediavaultParams);
									$params = array() ; 
								
									if(array_key_exists('r',$mediavault_params)){
										preg_match('/http[s]*:\/\/([a-zA-Z0-9\.\-_]+)/',kCheminHttp,$matches);
										if(isset($matches[1]) && !empty($matches[1])){
											$params[] = array('param'=>'r','value'=>$matches[1]);
										}else{
											trace("visualisation limelight - impossible d'extraire le referrer de l'url");
										}
									}
									if(!empty($params)){
										foreach($params as $idx=>$assoc){
											if(is_array($assoc) && isset($assoc['param']) && isset($assoc['value'])){
												if($idx==0){
													$limelight_url.="?";
												}else{
													$limelight_url.="&";
												}
												$limelight_url.=implode('=',$assoc);
											}
										}
									}
								}
								
								$hash = md5(kLimelightMediavaultSecret.$limelight_url);
								if(strpos($limelight_url,'?')!==false){
									$limelight_url.="&h=".$hash;
								}else{
									$limelight_url.="?h=".$hash;
								}
							}
							$this->mediaUrl[$key]=htmlentities($limelight_url);
							$this->mediaPath[$key]=$this->objMat->getLieu().$this->objMat->t_mat['MAT_NOM'];
						}else{
							$this->makeFileName(true);
							$ok=$this->copyMovie();
							 if(defined('visioUseHash') && visioUseHash){
                                
                                $currentDomain= $_SERVER['HTTP_REFERER'];
                                $current_hash = md5($this->tempDirShort."/".$this->fileOut.$currentDomain);
                                
                                $this->mediaUrl[$key]="video_wrapper.php?file=".$this->tempDirShort."/".$this->fileOut."&amp;hash=".$current_hash;
                                //$this->mediaPath[$key]=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
                            }else if(is_dir(kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut) && isset($this->objMat->t_mat['MAT_FORMAT']) && stripos($this->objMat->t_mat['MAT_FORMAT'],'HLS')!==false ){
								$this->mediaUrl[$key]=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut."/".$this->objMat->t_mat['MAT_NOM'].".m3u8" ;
								$this->mediaPath[$key]=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut."/".$this->objMat->t_mat['MAT_NOM'].".m3u8"  ;
							}else{
								$this->mediaUrl[$key]=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;								
								$this->mediaPath[$key]=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
							}
                        }
                    }
                    $this->generateXmlSubtitle();
					$this->calcResolution($this->objMat->t_mat['MAT_INFO']);

					$gotOne=true;

				}
				
				
				
				/**
				 * B.RAVI 07/03/17 12:40
				 * quand il y a 1 seule version de langue pour la vidéo + sous-titre, on veut afficher "FR + FR ST FR" (pour activer et désactiver les sous titres)
				 * (au lieu de "Sans + ST FR")
				 */
				if (!defined('gAllowMultipleVisFiles_keep_single_file_as_array') ||  intval(gAllowMultipleVisFiles_keep_single_file_as_array)==0 ){
                                
					if (is_array($this->mediaPath) && count($this->mediaPath) == 1) $this->mediaPath = array_shift(array_values($this->mediaPath));
					if (is_array($this->mediaUrl) && count($this->mediaUrl) == 1) $this->mediaUrl = array_shift(array_values($this->mediaUrl));
					if (is_array($this->MimeType) && count($this->MimeType) == 1) $this->MimeType = array_shift(array_values($this->MimeType));
					if (is_array($this->id_mat) && count($this->id_mat) == 1) $this->id_mat = array_shift(array_values($this->id_mat));
					
				}
				
				if (!$gotOne) {
					$this->dropError(kErrorVisioNoMedia);
					$ok = false;
				}
			break;
		}


		/** Ajout de logo **/
		/*
		if (defined("gVideoLogoPath") &&
			defined("gVideoLogoUtil")) {
	 		$_binary=split(" ",gVideoLogoUtil); //analyse du path pour trouver le chemin bin en laissant de cote les autres cmd (sudo, etc)
	 		$_binary=$_binary[count($_binary)-1];
			if (!is_file($_binary)) return false;

			$tmpFileUrl=kVisionnageUrl.$this->tempDirShort."/logo.mov";
			$tmpFilePath=kCheminLocalVisionnage.$this->tempDirShort."/logo.mov";
			$cmd=gVideoLogoUtil." ".($this->mediaUrl)." ".(gVideoLogoPath)." \"".$tmpFilePath."\" \"".kCheminLocalVisionnage.$this->tempDirShort."/log.log\"";
			trace($cmd);
			exec($cmd);
			$this->mediaPath=$tmpFilePath;
			$this->mediaUrl=$tmpFileUrl;
		}
		 */


		if (!$ok) return false; else {
				if ($this->objDoc) logAction("VIS",Array("ID_DOC"=>$this->objDoc->t_doc['ID_DOC'], "ACT_REQ" => "DOC"));
				else logAction("VIS",array("ID_DOC"=>$this->objMat->t_mat['ID_MAT'], "ACT_REQ" => "MAT"));
				return true;
				}
	}
	function convertSrtToVtt($srt,$vtt){
		$fp = fopen($srt,"r"); //lecture du fichier
		if (!$fp) {
			$this->dropError('fichier non trouvé');
			return false;
		}
		$page="WEBVTT"."\n"."\n";
		while (!feof($fp)) { //on parcoure toutes les lignes
			$ligne = fgets($fp, 4096); // lecture du contenu de la ligne
			if (preg_match("#-->#", $ligne))
			{
			   $ligne=str_replace(',', '.', $ligne);
			}
			$page.= $ligne;
		}
		$monfichier = fopen($vtt, 'w');
		fseek($monfichier, 0); // On remet le curseur au début du fichier
		fputs($monfichier, $page); 
		fclose($monfichier);
		fclose($fp);
		return $url;

	}	
	// VP 10/03/09 : ajout fonction de recherche du matériel à visionner
	/** Choisit le meilleur matériel parmi le tableau des matériels d'un document
		*  IN : var de classe : tableau t_doc_mat du doc, $id_lang, $formats
		* 	OUT : la ligne de t_doc_mat correspondant au meilleur matériel
		*  NOTE : par meilleur matériel on entend :
		* 		- matériel au format visionnable (ex : mpeg4)
		* 		- matériel avec un fichier en ligne
		* 		- et SI une version existe, matériel dont la version correspond à la langue demandée (svt : la langue de l'appli)
		*/
		/* //MS - 13.05.15 - On utilise plus ces fonctions, je les laisse pour l'instant pour legacy
		// On doit maintenant utiliser getMaterielVisu de class_doc pour récup les mats de visionnage
	function getMatForVisio($id_lang, $formats, $pattern="") {

		// VP 22/07/10 : ajout paramètre pattern pour choisir un fichier de préférence
		// Tri du tableau
		foreach ($this->objDoc->t_doc_mat as $key => $dm) {
			$tmpsort[$key]=$dm["ID_MAT"];
			//trace("getMatForVisio docmat possibles :");
			//trace($dm["ID_MAT"]);
		}
		array_multisort($tmpsort,SORT_ASC,$this->objDoc->t_doc_mat);
		unset($tmpsort);
		// Recherche du premier matériel visionnable avec la bonne langue
		foreach ($this->objDoc->t_doc_mat as $dm) {
			// VP 16/10/12 : correction bug sur MAT_NOM
			//Récupération du matériel pour la visio
			// VP 16/10/09 : ajout critère DMAT_INACTIF
			//trace($dm["ID_MAT"]." : ".$dm['MAT']->t_mat['MAT_FORMAT']." : ".$dm['MAT']->hasfile." : ".$dm['DMAT_INACTIF']);

			//PC 30/11/12 : comparaison insensible à la cassepour éviter bug
			foreach ($formats as &$format) $format = strtoupper($format);
			if (in_array(strtoupper($dm['MAT']->t_mat['MAT_FORMAT']),$formats) && $dm['MAT']->hasfile==1  && $dm['DMAT_INACTIF']!='1') {
				//Ce matériel est visionnable (format + existence fichier)
				//Certains matériels (peu) sont versionnés, dans ce cas, on choisit ce matériel si c'est la même langue
				//update VG 14/02/12 : si on a un pattern on le prend comme second filtre
				// VP 21/03/12 : retour du fichier si pattern et langue trouvés
                if (strtoupper($dm['DMAT_ID_LANG'])==strtoupper($id_lang) && (empty($pattern) || strstr($dm['MAT']->t_mat["MAT_NOM"],$pattern))) {
					//$_dm=$dm;
                    return $dm;
				} else if (!empty($pattern) && strstr($dm['MAT']->t_mat["MAT_NOM"],$pattern)) {
					//return $dm; //on choisit le fichier qui correspond au pattern
                    $_dm=$dm;
				} else if (!isset($_dm)) $_dm=$dm; //Sinon on garde le premier qui convient
			}
		}
		return $_dm;
	}

	function getMatsForVisio($id_lang, $formats, $patterns=array("_vis")) {
		foreach ($this->objDoc->t_doc_mat as $key => $dm) {
			$tmpsort[$key]=$dm["ID_MAT"];
		}
		array_multisort($tmpsort,SORT_ASC,$this->objDoc->t_doc_mat);
		unset($tmpsort);
		$arrDM = array();
		if(is_array($patterns)){
			foreach ($patterns as $keyp => $pattern)
				foreach ($this->objDoc->t_doc_mat as $dm) {
					foreach ($formats as &$format) $format = strtoupper($format);
					if (in_array(strtoupper($dm['MAT']->t_mat['MAT_FORMAT']),$formats) && $dm['MAT']->hasfile==1  && $dm['DMAT_INACTIF']!='1') {
						if (strtoupper($dm['DMAT_ID_LANG'])==strtoupper($id_lang) && (empty($pattern) || strstr($dm['MAT']->t_mat["MAT_NOM"],$pattern))) { 
							$arrDM[$keyp]=$dm;
						} else if (!empty($pattern) && strstr($dm['MAT']->t_mat["MAT_NOM"],$pattern) && !isset($arrDM[$keyp])) {
							$arrDM[$keyp]=$dm;
						}
					}
				}
		}else{ 
			if(is_string($patterns)){
				
				
				//B.RAVI 2012-12-21 Quand on utilise cette fonction pour récuperer les .srt et .vtt, on ne veut pas rentrer dans le cas gAllowMultipleVisFiles_key_field (pb de $key)
				$getting_subtitles=FALSE;
				if (is_array($formats)){
					if (in_array('SRT',$formats) || in_array('srt',$formats) ){
						$getting_subtitles=TRUE;
					}
				}
				
				
			$pattern = $patterns; 
				foreach ($this->objDoc->t_doc_mat as $dm) {
					foreach ($formats as &$format) $format = strtoupper($format);
					if (in_array(strtoupper($dm['MAT']->t_mat['MAT_FORMAT']),$formats) && $dm['MAT']->hasfile==1  && $dm['DMAT_INACTIF']!='1') {
						// MS - 18.11.16 - Dans le cas gAllowMultipleVisFiles, possibilité de définir le champ à utiliser comme clé de l'array avec la constante : gAllowMultipleVisFiles_key_field 
						if(defined('gAllowMultipleVisFiles_key_field')) {
								$gAllowMultipleVisFiles_key_field = gAllowMultipleVisFiles_key_field;
						}						
						if(is_string($gAllowMultipleVisFiles_key_field) && !empty($gAllowMultipleVisFiles_key_field) && !$getting_subtitles){
							if(strpos(gAllowMultipleVisFiles_key_field,'DMAT_') === 0 || in_array(gAllowMultipleVisFiles_key_field,array('ID_DOC_MAT'))){
								$key = $dm[gAllowMultipleVisFiles_key_field];
							}else if (strpos(gAllowMultipleVisFiles_key_field,'MAT_') === 0 || in_array(gAllowMultipleVisFiles_key_field,array('ID_MAT'))){
								$key = $dm['MAT']->t_mat[gAllowMultipleVisFiles_key_field];
							}
						}else if (isset($dm['MAT']->t_mat['MAT_NOM'])){
							$key = $dm['MAT']->t_mat['MAT_NOM'];
						}else{
							$key = null ; 
						}
							
						if (strtoupper($dm['DMAT_ID_LANG'])==strtoupper($id_lang) && (empty($pattern) || strstr($dm['MAT']->t_mat["MAT_NOM"],$pattern))) {
 							$arrDM[$key]=$dm;
						} else if (!empty($pattern) && strstr($dm['MAT']->t_mat["MAT_NOM"],$pattern) && !isset($arrDM[$keyp])) {
							$arrDM[$key]=$dm;
						}
					}
				}
			}
		}
		return $arrDM;
	}*/
	
	/** Cr√©ation du code HTML pour lancer le visionnage
	 *  IN :
	 * 	OUT :
	 */
	function renderComponent($print=true) {

		ob_start();
		include(getSiteFile("designDir","visualisationQT.inc.php"));
		$html=ob_get_contents();
		ob_end_clean();

		if ($print) echo $html; else return $html;

	}

	function makePlaylistXML($list) {

		$handle=fopen($this->tempDir."/".$this->fileOut,w);
		if (!$handle) {
			$this->dropError(kErrorVisioCreationXML);
			return false;
		}

		$xml="<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\">\n
		<trackList>\n";
		foreach($list as $item)
		{
			$xml.="<track><location>".$item['mediaUrl']."</location><duration>".timeToSec($item['duration'])."</duration></track>\n";
		}
		$xml.="</trackList></playlist>";

		fwrite($handle,$xml);
		fclose($handle);
		return true;
	}
	
	function xml_export($entete=0,$encodage=0,$prefix="",$print=true){
        $content="";
        $search = array(" ' ",'&','<','>');
        $replace = array("'",'&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

        $content.=$prefix."\t<visionnage>";
 		if (!is_array($this->mediaPath)) $content.=$prefix."\t\t<mediapath>".$this->mediaPath."</mediapath>";
		else {
			$content.=$prefix."\t\t<mediapath>";
			foreach ($this->mediaPath as $key=>$mediaPath){
				if (empty($key)||is_int($key)){// B.RAVI pour eviter <> generateur d'erreur XML
					$key='idx_'.intval($key);	
				}
				$content.=$prefix."\t\t<$key>".$mediaPath."</$key>";
			}
			$content.=$prefix."\t\t</mediapath>";
		}
 		if (!is_array($this->mediaUrl)) $content.=$prefix."\t\t<mediaurl>".$this->mediaUrl."</mediaurl>";  
		else {
			$content.=$prefix."\t\t<mediaurl>";
			foreach ($this->mediaUrl as $key=>$mediaUrl){
				if (empty($key) || is_int($key)){// B.RAVI pour eviter <> generateur d'erreur XML
					$key='idx_'.intval($key);	
				}
				$content.=$prefix."\t\t<$key>".$mediaUrl."</$key>";
			}
			$content.=$prefix."\t\t</mediaurl>";
		}
		
		$content.=$prefix."\t\t<tcin>".$this->tcin."</tcin>";  
		$content.=$prefix."\t\t<tcout>".$this->tcout."</tcout>";  		
		$content.=$prefix."\t\t<offset>".$this->offset."</offset>";  
 		if (!is_array($this->id_mat)) $content.=$prefix."\t\t<id_mat>".$this->id_mat."</id_mat>";  
		else {
			$content.=$prefix."\t\t<id_mat>";
			foreach ($this->id_mat as $key=>$id_mat){
				if (empty($key) || is_int($key)){// B.RAVI pour eviter <> generateur d'erreur XML
					$key='idx_'.intval($key);	
				}
				$content.=$prefix."\t\t<$key>".$id_mat."</$key>";
			}
			$content.=$prefix."\t\t</id_mat>";
		}
		
		$content.=$prefix."\t\t<width>".$this->width."</width>";  
		$content.=$prefix."\t\t<height>".$this->height."</height>";  
		$content.=$prefix."\t\t<scale>".$this->scale."</scale>";  						
		
 		if (!is_array($this->MimeType)) $content.=$prefix."\t\t<mimetype>".$this->MimeType."</mimetype>";  
		else {
			$content.=$prefix."\t\t<mimetype>";
			foreach ($this->MimeType as $key=>$MimeType){
				if (empty($key) || is_int($key)){// B.RAVI pour eviter <> generateur d'erreur XML
					$key='idx_'.intval($key);	
				}
				$content.=$prefix."\t\t<$key>".$MimeType."</$key>";
			}
			$content.=$prefix."\t\t</mimetype>";
		}
		$content.=$prefix."\t\t<vignette>".$this->vignette."</vignette>";  	
		$content.=$prefix."\t\t<playlist>".$this->xml_playlist."</playlist>";  	
		
		if(isset($this->mosaic_path) && !empty($this->mosaic_path) && isset($this->mosaic_pics_index) && !empty($this->mosaic_pics_index)){
			$content.=$prefix."\t\t<mosaic_path>".$this->mosaic_path."</mosaic_path>";
			$content.=$prefix."\t\t<mosaic_pics_index>".$this->mosaic_pics_index."</mosaic_pics_index>";
		}
		if(isset($this->waveform_path) && !empty($this->waveform_path) ){
			$content.=$prefix."\t\t<waveform_path>".$this->waveform_path."</waveform_path>";
		}

		if(!empty($this->srt_path)){
			 $content.=$prefix."\t\t<subtitles>";
			 foreach ($this->srt_path as $key => $value) {
//				trace('foreach'.$key."=>".$value);
					$content.="<subtitle>";
					$content.='<key>'.$key."</key>";
					$content.='<url>'.$value['url']."</url>";
					if (!empty($value['label'])){
						$content.='<label>'.$value['label']."</label>";
					}
					$content.="</subtitle>";
			 }
			 $content.=$prefix."</subtitles>";
		}
		if(isset($this->objDoc) && isset($this->objDoc->t_doc_seq) &&!empty($this->objDoc->t_doc_seq)){
			$content.=$prefix."<sequences>";
			foreach($this->objDoc->t_doc_seq as $seq){
				$content.=$prefix."\t<seq>";
				$content.=$prefix."\t\t<type_seq>".$seq['DOC_ID_TYPE_DOC']."</type_seq>";
				$content.=$prefix."\t\t<tcin>".$seq['DOC_TCIN']."</tcin>";
				$content.=$prefix."\t\t<tcout>".$seq['DOC_TCOUT']."</tcout>";
				$content.=$prefix."\t\t<title>".$seq['DOC_TITRE']."</title>";					
				if(!empty($seq['DOC_ID_DOC_ACC'])){
					$content.=$prefix."\t\t<img>/doc_acc/".$seq['DA_CHEMIN'].'/'.$seq['DA_FICHIER']."</img>";					
				}else if(!empty($seq['DOC_ID_IMAGE'])){
					$content.=$prefix."\t\t<img>".$seq['IM_CHEMIN'].$seq['IM_FICHIER']."</img>";					
				}
				$content.=$prefix."\t</seq>";
			}
			$content.=$prefix."</sequences>";
		}

        $content.=$prefix."\t</visionnage>";
        if($encodage!=0){
            $content=mb_convert_encoding($content,$encodage,"UTF-8");
        }
        if ($print) echo $content; else return $content;

	}


        
        
        
        /* ex <srt_path>
(EN)http://192.168.0.150/media/colas/public/visionnage/1461756594.20239600/7avril_en.vtt.vtt(FR)http://192.168.0.150/media/colas/public/visionnage/1461756594.20239600/1avril2_fr.vtt.vtt
</srt_path> */
        function generateXmlSubtitle() {
			
        $format_srt = array("SRT", "VTT");
        //$arrSRT = $this->getMatsForVisio($id_lang, $format_srt, '.');
        $arrSRT = $this->objDoc->getMaterielVisu(false,true,null,$id_lang,$format_srt);

        //docmat contient des fichier ss titres
        // flag pour considérer un sous titre dans le player : $type_doc_mat_srt
        //seul les fichiers vtt sont correctement lus, soit on le transforme en vtt 

            if (!empty($arrSRT)) {
                if (!empty($_GET["type_doc_mat_srt"])) {
                    $type_doc_mat_srt = $_GET["type_doc_mat_srt"];
                } else {
                    $type_doc_mat_srt = 'SRT';
                }
				
                foreach ($arrSRT as $srt) {
                    $lang = $lang2 = $label = $label2 = null;
                    foreach ($srt['t_doc_mat_val'] as $lang) {
                        if ($lang['ID_TYPE_VAL'] == $type_doc_mat_srt && !empty($lang['VAL_CODE'])) {
                            $lang = $lang['VAL_CODE'];
                            $label = $lang['VALEUR'];
                        }
                    }

                    // B.RAVI mis en commentaire car MAT_CHEMIN pas toujours rempli (ex: sur colas)
        //							if(!empty($srt['MAT']->t_mat['MAT_CHEMIN']) && !empty($srt['MAT']->t_mat['MAT_NOM'])){					
                    if (!empty($srt['MAT']->t_mat['MAT_NOM'])) {


                        $mat = new Materiel;
                        $mat->t_mat['ID_MAT'] = $srt['MAT']->t_mat['ID_MAT'];
                        $mat->getMat();
                        $mat->getValeurs();
                        $doc_mat_path = $mat->getLieu() . $srt['MAT']->t_mat['MAT_NOM'];
                        if (getExtension($srt['MAT']->t_mat['MAT_NOM']) == 'srt') {
                            $path_vtt = kCheminLocalVisionnage . $this->tempDirShort . "/" . $srt['MAT']->t_mat['MAT_NOM'] . '.vtt';
                            $this->convertSrtToVtt($doc_mat_path, $path_vtt);
                        } else {
                            $path_vtt = $doc_mat_path;
                            copy($path_vtt, kCheminLocalVisionnage . $this->tempDirShort . "/" . $srt['MAT']->t_mat['MAT_NOM'] . '.vtt');
                        }
						
        //                                                                        echo $srt['MAT']->t_mat['MAT_NOM'].'debug_affich<pre>';
        //                                                                        var_dump($mat->t_mat_val);
        //                                                                        echo '</pre><br /><br />';
                        //B.RAVI 2016-04-08 si on a pas trouvé la langue dans t_doc_mat_val, on va                                                                                //  chercher la langue dans t_mat_val					
                        foreach ($mat->t_mat_val as $t_mat_val) {
                            if ($t_mat_val['ID_TYPE_VAL'] == $type_doc_mat_srt && $t_mat_val['VAL_CODE'] != '') {
                                $lang2 = $t_mat_val['VAL_CODE'];
								$label2=$t_mat_val['VALEUR'];
                                break;
                            }
                        }
						
						
        //                                                                        echo 'debug_affich $lang '.$lang.'<br /><br />';

                        if (empty($lang)) {
                            $lang = $lang2;
                            $label = $label2;
                        }				
						//update 2016-11-09 B.RAVI finalement on  commente ce bloc ci dessous, car l'utilisateur pourrait publier un vtt avec _nimportequoi.vtt (et dans les bouton de sous-titre du player, on aurait le libellé nimportequoi)
//						//B.RAVI 2016-11-02 si on n'a pas toujours pas trouvé le code langue, on va chercher dans mat_nom, ex: transcription_fr.vtt=> fr
//						if (empty($lang)) {
//                            $tab_lang=explode('_',$srt['MAT']->t_mat['MAT_NOM']);
//							$tab_langEND=array_pop($tab_lang);//même si il y a plusieurs _, il ne prendra que la fin
//							$lang=substr($tab_langEND,0,strpos($tab_langEND,'.'));//même s'il y a 2 points (ex: transcription_fr.vtt.srt (il n'ira que jusqu'au 1er point)
//                        }
						
        //                                                                        echo 'debug_affich $lang2 '.$lang.'<br /><br />';

                        if (!empty($lang)) {
                            $this->srt_path[strtoupper($lang)] = array('url'=>kVisionnageUrl . $this->tempDirShort . "/" . $srt['MAT']->t_mat['MAT_NOM'] . '.vtt','label'=>$label);
                        }
                    }
                }
            }
        }

}
?>
