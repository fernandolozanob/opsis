<?php
require_once(libDir."class_cherche.php");


class RechercheConso extends Recherche {

	private $requestsType;
	
	function __construct() {
	  	$this->entity='CONSO';
	  	$this->name=kConso;
     	$this->prefix='c';
     	$this->sessVar='recherche_CONSO';
		$this->tab_recherche=array();
		$this->useSession=true;
	}

    function prepareSQL(){
		global $db;
		$this->sql = "SELECT c.* , tc.type_conso, tc.type_conso_unit
				FROM t_conso as c
				LEFT JOIN t_type_conso tc on c.conso_id_type = tc.id_type_conso AND tc.ID_LANG=".$db->Quote($_SESSION['langue'])."
				where  1 = 1 ";
		
		// trace($this->sql);
		$this->sqlSuffixe = "";
        $this->etape="";
    }
	
	
	function execute($max_rows = 10, $secstocache=0, $highlight=false) {
    	parent::execute($max_rows, $secstocache, $highlight);
    	foreach ($this->result as $key => &$row)
    	{
			if($row['CONSO_ID_TYPE'] == 'stockage'){
				require_once(libDir."fonctionsGeneral.php");
				$row['CONSO_QUANTITE_HUMREAD'] = filesize_format($row['CONSO_QUANTITE'],'.',' ',false,(strtoupper($_SESSION['langue']) == 'FR')?'octet':false);
			}else{
				$row['CONSO_QUANTITE_HUMREAD'] = $row['CONSO_QUANTITE'];
			}
		}
    }
 

}
?>
