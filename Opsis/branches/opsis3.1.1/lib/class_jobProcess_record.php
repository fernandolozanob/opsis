<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once(modelDir.'model_materiel.php');

require_once('exception/exception_fileNotFoundException.php');

class JobProcess_record extends JobProcess implements Process
{
	private $id_job;

	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('record');
		
		$this->required_xml_params=array
		(
			'action'=>'string'
		);
		
		$this->optional_xml_params=array
		(
			'channel'=>'string',
			'date'=>'string',
			'time'=>'string',
			'duration'=>'string',
			'format'=>'string'
		);
		
		$donnees_xml_name=explode('_',stripextension($file_xml));
		$this->id_job=intval($donnees_xml_name[3]);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		// verification des parametres et de leurs types
		if (!empty($this->required_xml_params))
		{
			foreach($this->required_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
				{
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
				}
				else if (isset($this->params_job[$param_name]) && empty($this->params_job[$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($this->optional_xml_params))
		{
			foreach($this->optional_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
			}
		}
	}
	
	function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		
		switch($params_job['action'])
		{
			case 'create';
				$this->shellExecute(gRecordPath,$this->getFileOutPath().' '.$params_job['channel'].' '.$this->id_job.' '.$params_job['date'].' '.$params_job['time'].' '.$params_job['duration'].' '.$this->getOutLogPath().' '.$params_job['format'].'');
				$this->writeOutLog('Début enregistrement');
				break;
			case 'delete':
				//$this->shellExecute(gDelRecPath,$this->id_job.' '.$this->getOutLogPath());
                $this->shellExecute(gDelRecPath,$params_job['date'].' '.$params_job['time'].' '.$this->getOutLogPath());
				$this->writeOutLog('Annulation enregistrement');
				break;
			default:
				throw new InvalidXmlParamException('unknown param action ('.$params_job['action'].')');
				break;
		}
		
		/*$this->setProgression(100);
		$this->writeOutLog('Fin traitement');*/
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>