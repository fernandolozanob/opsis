<?php
require_once(libDir."class_cherche.php");


class RechercheExp extends Recherche {

	function __construct() {
	  	$this->entity = 'EXP';
     	$this->name = kExploitation;
     	$this->prefix = 'E';
     	$this->sessVar='recherche_EXP';
		$this->tab_recherche=array();
		$this->useSession=true;
	}


    function prepareSQL(){
		global $db;
		$this->sql = "select E.*, T.*
						from t_exploitation E
						inner join t_type_exp T on id_type_exp = exp_id_type_exp
						WHERE 1=1 ";
    }

    protected function ChooseType($tabCrit,$prefix) {
		// I.2.A.1 Tests des champs et appel des fonctions de recherche ad�quates :
		switch($tabCrit['TYPE'])
		{
			case "FT": //FullText
				$this->chercheTexteAmongFields($tabCrit,$prefix);
				break;
			case "FTS": //FullText avec découpage de chaque mot
				$this->FToperator='+';
				$this->chercheTexteAmongFields($tabCrit,$prefix);
				break;
			case "C":   // Recherche sur un champ précis
				$this->chercheChamp($tabCrit,$prefix);
				break;
			case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
				$this->chercheChamp($tabCrit,$prefix,true);
				break;
			case "CI":    // Recherche sur un champ d'une table annexe par ID
				$this->chercheIdChamp($tabCrit,$prefix);
				break;
			case "CT": // Recherche sur un champ par texte exact dans une table liée (fonds, genre,...)
				$this->chercheChampTable($tabCrit,$prefix);
				break;
			case "D":   // Recherche sur date(s)
				$this->chercheDate($tabCrit,$prefix);
				break;
			case "H":   // Recherche sur durées(s)
				$this->chercheDuree($tabCrit,$prefix);
				break;
				// VP 23/12/08 : ajout recherche VF
			case "V":  // Recherche sur valeur
			case "VF":  // Recherche sur valeur en FT
				$this->chercheVal($tabCrit,$prefix);
				break;
			case "VI":    // Recherche sur valeur par ID (récursif)
				$this->chercheIdVal($tabCrit,$prefix,true);
				break;
			case "CD" : //comparaison de date
				$this->compareDate($tabCrit,$prefix);
			 //@update VG 07/06/2010 : ajout de la comparaison de dates
			case "CDLT" : //comparaison de date
				$tabCrit['VALEUR2']='<=';
				$this->compareDate($tabCrit,$prefix);
				break;
			case "CDGT" : //comparaison de date
				$tabCrit['VALEUR2']='>=';
				$this->compareDate($tabCrit,$prefix);
				break;
			case "CH" : //comparaison de date
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CHLT" : //comparaison de date
				$tabCrit['VALEUR2']='<=';
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CHGT" : //comparaison de date
				$tabCrit['VALEUR2']='>=';
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CV" : //comparaison de valeur =
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVGT" : //comparaison de valeur >
				$tabCrit['VALEUR2']='>';
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVLT" : //comparaison de valeur <
				$tabCrit['VALEUR2']='<';
				$this->compareValeur($tabCrit,$prefix);
				break;

			case "LI" : // Recherche sur ID LEX unique (récursif)
				$this->chercheIdLex($tabCrit,$prefix,true);
				break;
			case "L" : // Recherche sur termes lexique (av. extension lex)
				$this->chercheLex($tabCrit,$prefix,true);
				break;

			case "P" :
				$this->cherchePers($tabCrit,$prefix);
				break;
			case "PI" :
				$this->chercheIdPers($tabCrit,$prefix);
				break;

			case "PAN" :
				$this->cherchePanier($tabCrit,$prefix);
				break;

			default : break;
		}
    }

	protected function cherchePanier($tabCrit,$prefix) {
		global $db;

		$prefix = (!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche .= $tabCrit['OP']." ".$prefix."ID_EXP IN
			(SELECT distinct t_exp_doc.ID_EXP from t_exp_doc, t_panier_doc WHERE t_exp_doc.ID_DOC=t_panier_doc.ID_DOC and ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kPanier;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
	}

}
?>