<?php

require_once(libDir."class_cherche.php");

class RechercheCat extends Recherche
{
    var $treeParams;
    var $affichage;
    var $entity;

    function __construct()
	{
	  	$this->entity='CAT';
     	$this->name=kCategorie;
     	$this->prefix='CAT';
     	$this->sessVar='recherche_CAT';
		$this->tab_recherche=array();
		$this->useSession=true;
		$this->sqlSuffixe=" GROUP BY CAT.ID_CAT, CAT.ID_LANG, TYP.ID_TYPE_CAT,TYP.TYPE_CAT,DOC_ACC.ID_DOC_ACC,DOC_ACC.DA_FICHIER,DOC_ACC.DA_CHEMIN,CAT_PERE HAVING 1=1 ";

		$this->highlightedFieldInHierarchy='CAT_NOM';
        $this->lang=$_SESSION['langue'];
	}
	
	function prepareSQL()
	{
		global $db;
		// MS - 26.04.16 - Ajout des noms du parent & des fils (il faudra éventuellement décider d'un ordre de tri pour l'affichage des fils)
		// (Pour cela il faudrait qu'existe la notion d'un "tri secondaire" ajouté au tri de l'utilisateur 
		// et dépendant de la class_chercheXXX utilisée, ici on aurait un tri sur CAT_FILS.CAT_NOM ajouté à la fin du tri utilisateur)
		$this->sql = 'SELECT CAT.*, CAT_PERE.CAT_NOM as CAT_PERE, '.$db->stringAggregate('DISTINCT CAT_FILS.CAT_NOM',', ').' as CAT_FILS,TYP.ID_TYPE_CAT,TYP.TYPE_CAT,DOC_ACC.ID_DOC_ACC,DOC_ACC.DA_FICHIER,DOC_ACC.DA_CHEMIN, COUNT(DISTINCT TDC.ID_DOC) as DOC_LIES
						FROM t_categorie CAT 			
						LEFT JOIN t_categorie CAT_PERE ON  CAT.cat_id_gen = CAT_PERE.id_cat and CAT.id_lang=CAT_PERE.id_lang
						LEFT JOIN t_categorie CAT_FILS ON  CAT.ID_CAT = CAT_FILS.cat_id_gen and CAT.id_lang=CAT_FILS.id_lang 
						LEFT JOIN t_type_cat TYP ON CAT.CAT_ID_TYPE_CAT=TYP.ID_TYPE_CAT AND CAT.ID_LANG=TYP.ID_LANG
						LEFT JOIN t_doc_acc DOC_ACC ON CAT.CAT_ID_DOC_ACC=DOC_ACC.ID_DOC_ACC AND CAT.ID_LANG=DOC_ACC.ID_LANG
						LEFT JOIN t_doc_cat as TDC ON TDC.ID_CAT = CAT.ID_CAT';
		$this->sqlWhere=" WHERE CAT.ID_LANG='".$_SESSION["langue"]."' ";	
		$this->etape="";
    }
	
    /**
     * CONTRUCTION ARBRE HIERARCHIQUE
     * La partie ci-dessous est exclusivement consacrŽe ˆ la construction d'un arbre hiŽrarchique (appelŽe par DTREE)
     * RŽcupre les fils d'un noeud id_cat.
     */
    function getChildren($id_cat,&$arrNodes,$fullTree = false ) {
        global $db;
		
        $sql="SELECT l1. * , count( l2.ID_CAT ) AS NB_CHILDREN FROM t_categorie l1
        LEFT JOIN t_categorie l2 ON ( l2.CAT_ID_GEN = l1.ID_CAT AND l2.CAT_ID_TYPE_CAT = l1.CAT_ID_TYPE_CAT
                               AND l2.ID_LANG = '".$this->lang."' )
        WHERE l1.ID_LANG = '".$this->lang."'
        AND l1.CAT_ID_GEN = ".intval($id_cat)."
        AND l1.CAT_ID_TYPE_CAT ".$this->sql_op($this->treeParams['ID_TYPE_CAT']);
		
		if(isset($this->treeParams['filter_values']) && !empty($this->treeParams['filter_values'])){
			$sql .=  " AND l1.ID_CAT NOT IN (".implode(',',array_map(intval,$this->treeParams['filter_values'])).")";		
		}
		
        $sql.=" GROUP BY l1.ID_CAT, l1.ID_LANG";
        if(empty($this->treeParams['tri'])) $sql.=" ORDER BY l1.CAT_ID_GEN, l1.CAT_NOM";
        else $sql.=" ORDER BY l1.CAT_ID_GEN, l1.".$this->treeParams['tri'];
        
		$rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_CAT']]['id']=$row['ID_CAT'];
            $arrNodes['elt_'.$row['ID_CAT']]['id_pere']=$row['CAT_ID_GEN'];
            $arrNodes['elt_'.$row['ID_CAT']]['terme']=str_replace (" ' ", "'",trim($row['CAT_NOM']));
            $arrNodes['elt_'.$row['ID_CAT']]['valide']=true;
            $arrNodes['elt_'.$row['ID_CAT']]['context']=false;
            $arrNodes['elt_'.$row['ID_CAT']]['nb_children']=$row['NB_CHILDREN'];
            $arrNodes['elt_'.$row['ID_CAT']]['nb_asso']=$row['NB_ASSO'];
            $_rowIDs[]=$row['ID_CAT'];
            if($fullTree){
                $arrNodes['elt_'.$row['ID_CAT']]['openAtLoad']=true;
                $this->getChildren($row['ID_CAT'],$arrNodes,$fullTree);
            }
        }
        
        return $arrNodes;
    }
    
    function getFather($id_cat,$recursif=false,&$arrNodes,$withChildren=false) {
        global $db;
        $sql=" SELECT ID_CAT,CAT_NOM,CAT_ID_GEN from t_categorie where ID_LANG='".$this->lang."' and ID_CAT
        in (select CAT_ID_GEN from t_categorie where ID_CAT=".intval($id_cat).")";
        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_CAT']]['id']=$row['ID_CAT'];
            $arrNodes['elt_'.$row['ID_CAT']]['id_pere']=$row['CAT_ID_GEN'];
            $arrNodes['elt_'.$row['ID_CAT']]['terme']=str_replace (" ' ", "'",trim($row['CAT_NOM']));
            
            $arrNodes['elt_'.$row['ID_CAT']]['valide']=true;
            $arrNodes['elt_'.$row['ID_CAT']]['context']=false;
            $arrNodes['elt_'.$row['ID_CAT']]['open']=true;
            
            if ($recursif && $row['CAT_ID_GEN']!=0 && $row['CAT_ID_GEN']!=$row['ID_CAT']) $this->getFather($row['ID_CAT'],$recursif,$arrNodes,$withChildren);
            
            if (!isset($arrNodes['elt_'.$row['ID_CAT']]['nb_children']))  $arrNodes['elt_'.$row['ID_CAT']]['nb_children']=1;
            
            if($withChildren) $this->getChildren($row['ID_CAT'],$arrNodes);
        }
        return $arrNodes;
    }
    
    function getNode($id_cat,&$arrNodes,$style='highlight',$withBrother=false) {
        global $db;
        $this->getFather($id_cat,true,$arrNodes,$withBrother);
        $sql="select * from t_categorie where ID_CAT=".intval($id_cat)." AND ID_LANG=".$db->Quote($this->lang);
        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_CAT']]['id']=$row['ID_CAT'];
            $arrNodes['elt_'.$row['ID_CAT']]['id_pere']=$row['CAT_ID_GEN'];
            $arrNodes['elt_'.$row['ID_CAT']]['terme']=str_replace (" ' ", "'",trim($row['CAT_NOM']));
            
            $arrNodes['elt_'.$row['ID_CAT']]['valide']=true;
            $arrNodes['elt_'.$row['ID_CAT']]['context']=false;
            $arrNodes['elt_'.$row['ID_CAT']]['style']=$style;
        }
        
        $this->getChildren($id_cat,$arrNodes);
        return $arrNodes;
        
    }
    
    function getRootName() {
        return GetRefValue('t_type_cat',$this->treeParams['ID_TYPE_CAT'],$this->lang);
        
    }

	function appliqueDroits() {
		if(function_exists("appliqueDroitsCustomCat")){
			$this->sqlRecherche.= appliqueDroitsCustomCat($this->sqlRecherche);
		}
	}

}

?>
