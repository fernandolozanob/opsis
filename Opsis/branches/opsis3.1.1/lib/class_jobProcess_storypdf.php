<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_directoryException.php');
require_once('exception/exception_fileException.php');

require_once(modelDir.'model_materiel.php');

class JobProcess_storypdf extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('storypdf');
		
		$this->required_xml_params=array
		(
		);
		
		$this->optional_xml_params=array
		(
			'largeur'=>'int',
			'hauteur'=>'int',
			'use_log'=>'int'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		
		if( !in_array(getExtension($this->getFileInPath()),array("ai", "pdf", "eps", "ps")) )
			throw new FileException('input is not a PostScript file ('.$this->getFileInPath().')');
		
		// creation du repertoire de sortie
		if (is_dir(dirname($this->getFileOutPath()))===false)
		{
			if (mkdir(dirname($this->getFileOutPath()))===false)
				throw new DirectoryException('failed to create storyboard directory ('.dirname($this->getFileOutPath()).')');
		}
		
		
		//kPdfToJpgPath
		// $this->shellExecute(kPdfToJpgPath,'"'.$this->getFileInPath().'" '.$params_job['largeur'].' '.$params_job['hauteur'].' "'.$this->getFileOutPath().'"');
		$cmd = escapeQuoteShell($this->getFileInPath()).' '.$params_job['largeur'].' '.$params_job['hauteur'].' "'.$this->getFileOutPath().'"';
		if(!empty($params_job['use_log'])) {
			$cmd .= ' "'.$this->getOutLogPath().'"';
		}
		$this->shellExecute(kPdfToJpgPath,$cmd);
        
        if(isset($params_job['logo']) && !empty($params_job['logo']) && file_exists(jobImageDir.$params_job['logo'])) {
			if ($arr_files = scandir(dirname($this->getFileOutPath()))){
                foreach($arr_files as $file) {
                    if (strpos(strtolower($file), '.jpg')) {
                        $this->addLogoToImage($params_job, dirname($this->getFileOutPath()).'/'.$file);
                    }
                }
            }
        }
		
		$this->setProgression(100);
		
		if(!empty($this->exitCode)) {
			switch ($this->exitCode) {
				case 1:
					$this->dropError('Génération des .ppm échouée');
				break;
				case 2:
					$this->dropError('Génération des .jpg échouée');
				break;
				case 42:
					$this->dropError('Mauvais paramètres d\'entrée');
			}
		}
		
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
    
    private function addLogoToImage($params_job,$imagePathFilename)
    {
        if(isset($params_job['logo']) && !empty($params_job['logo']) && file_exists(jobImageDir.$params_job['logo'])) {
                
            $image=new Imagick();
			$image->readImage($imagePathFilename);
            
            if (isset($params_job['logo_marge']) && !empty($params_job['logo_marge'])) {
                $logo_marge=$params_job['logo_marge'];
            } else {
                $logo_marge=10;
            }

            $width_image = $image->getImageWidth();
            $height_image = $image->getImageHeight();

            $logo = new Imagick(jobImageDir.$params_job['logo']);
            if($width_image > $height_image) {
                $resize = $height_image - ($logo_marge * $height_image /100)*2;
            } else {
                $resize = $width_image - ($logo_marge * $width_image /100)*2;
            }
            $logo->resizeImage($resize, $resize, imagick::FILTER_LANCZOS, 1, TRUE);

            $checkPosition = '';
            if(isset($params_job['logo_position'])) { $checkPosition = $params_job['logo_position']; } 
            switch($checkPosition)
            {
                case 'center':
                    $x_logo = ($width_image / 2) - ($logo->getImageWidth() / 2);
                    $y_logo = ($height_image / 2) - ($logo->getimageheight() / 2);
                    break;
                default: //center
                    $x_logo = ($width_image / 2) - ($logo->getImageWidth() / 2);
                    $y_logo = ($height_image / 2) - ($logo->getimageheight() / 2);
            }
            $image->compositeImage($logo, imagick::COMPOSITE_DEFAULT, $x_logo, $y_logo);
            $logo->clear();
            $logo->destroy();
            
            $image->writeImage();
			$image->clear();
			$image->destroy();
        }
    }
}

?>