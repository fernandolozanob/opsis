<?php


// class toolbox pour communication avec l'API V3 de youtube
Class DM{
	var $username;
	var $password;
	var $api_key;
	var $api_secret;
	var $api;
	var $type_user;
	private $aPlaylists;

	function __construct(){

	}

	function setDocXsl($doc_xsl){
		if(!file_exists($doc_xsl)){
			$this->dropError("Erreur - fichier doc_xsl non trouvé");
			return false ;
		}else{
			$this->doc_xsl = $doc_xsl;

			return true ;
		}
	}



	// récupération des paramètres de login depuis une etape
	function getLogParamsFromEtape($etape_ref=null){
		global $db ;
		if(isset($etape_ref) && !empty($etape_ref) ){
			$id_etp = intval($etape_ref);
		}elseif(isset($this->etape_ref) && !empty($this->etape_ref)){
			$id_etp = $this->etape_ref;
		}

		$etape_arr = $db->GetRow("SELECT * from t_etape where id_etape=".intval($id_etp));

		if(!isset($etape_arr) || empty($etape_arr) ||   !isset($etape_arr['ETAPE_PARAM'])){
			$this->dropError("Erreur recupération identifiants depuis etape");
			return false ;
		}
		$tab_xml=xml2tab($etape_arr['ETAPE_PARAM']);
		$params_job = $tab_xml['PARAM'][0];
		$this->api_key	  = $params_job['API_KEY'];
		$this->api_secret  = $params_job['API_SECRET'];
		$this->username = $params_job['USER'];
		$this->password = $params_job['PASS'];
		if(!empty($params_job['PRIVATE'])) $this->private = $params_job['PRIVATE'];
		if(empty($this->doc_xsl)){
			$this->setDocXsl(getSiteFile("designDir","print/".$params_job['DOC_XSL']));
		}


		if(!empty($this->username) && !empty($this->api_secret) && !empty($this->api_key)){
			return true ;
		}else{
			$this->dropError("Error - La recuperation des params de connexion a echouee");
			return false ;
		}
	}

	// connexion youtube obligatoire avant n'importe quelle autre action
	// necessite que les informations client_id & client_secret soit définis (voir getLogParamsFromEtape, ou implémenter une autre méthode)
	function connectDM(){
		require_once(libDir.'dailymotion_sdk/Dailymotion.php');

		if(!isset($this->api_key) || empty($this->api_key) || !isset($this->api_secret) || empty($this->api_secret)){
			$this->dropError("Erreur connexion youtube - paramètres manquants");
			return false ;
		}
		$user_login = array('username'=>$this->username, 'password' =>$this->password,'scope'=>'read write manage_playlists');
		$this->api = new Dailymotion();
		if (defined('kProxyHTTPS'))
			$this->api->proxy = kProxyHTTPS;

		$droits=array("manage_videos","write","delete","manage_playlists");
		try
		{
			$this->api->setGrantType(Dailymotion::GRANT_TYPE_PASSWORD, $this->api_key, $this->api_secret,$droits,$user_login);
		}


		catch(InvalidArgumentException $e)
		{
			throw new InvalidXmlParamException('Dailymotion authentification error : '.$e->getMessage(),$e->getCode());
			trace('Dailymotion authentification error : '.$e->getMessage(),$e->getCode());

		}

		// on recupere le type de compte de l'utilisateur
		// thumbnail_url necessite un compte official ou motionmaker
		try
		{
			$this->type_user=$this->api->call('GET /me',array('fields'=>'type'));
		}

		catch(DailymotionApiException $e)
		{
			throw new Exception('Dailymotion API - GET /me : '.$e->getMessage(),$e->getCode());
			trace('Dailymotion API - GET /me : '.$e->getMessage(),$e->getCode());
		}

	}







	function publish_video_dm_job($TmpDirPath=null,$FileInPath=null,$params_job=null){
		// upload de la video
		try
		{
			// Si le filename commence par des chiffres => upload DailyMotion NOK
			// On crée un lien avec 'DM' en préfixe du nom du fichier
			$prefixe = '/DM';
			if(substr($TmpDirPath, -1) === '/') { $prefixe = 'DM'; }
			$targetLink = $FileInPath;
			$this->link_file_path = $TmpDirPath.$prefixe.basename($FileInPath);
			if(is_file($targetLink)){
				if(is_link($this->link_file_path)) {
					unlink($this->link_file_path);
				}
				if(symlink($targetLink,$this->link_file_path)===false){
					$this->dropError('Error linking file ('.$targetLink.' -> '.$this->link_file_path.')');
					throw new FileNotFoundException('Error linking file ('.$targetLink.' -> '.$this->link_file_path.')');
				} else {
						$this->dropError('Linking file ('.$targetLink.' -> '.$this->link_file_path.')');
				}
			} else {
				trace('Error target file ('.$targetLink.' -> '.$this->link_file_path.')');
				$this->dropError('Error target file ('.$targetLink.' -> '.$this->link_file_path.')');
				throw new FileNotFoundException('Error target file ('.$targetLink.' -> '.$this->link_file_path.')');
			}
			if(!is_link($this->link_file_path)) {   trace('Is not a link : '.$this->link_file_path);   }
			$url=$this->api->uploadFile($this->link_file_path);

			// traitement des paramatres
			if ($params_job['donnees'][0]['private']=='true' || $params_job['donnees'][0]['private']=='1' ||$params_job['private']=='true' || $params_job['private']=='1')
				$private=true;
			else
				$private=false;
	
			if ($params_job['donnees'][0]['creative']=='true')
				$creative=true;
			else
				$creative=false;
	
			if ($params_job['donnees'][0]['official']=='true')
				$official=true;
			else
				$official=false;
	
			if ($params_job['donnees'][0]['published']=='true')
				$published=true;
			else
				$published=false;
	
			if ($params_job['donnees'][0]['allow_comments']=='true')
				$allow_comments=true;
			else
				$allow_comments=false;
	
			if(isset($params_job['donnees'][0]['tags']) && !empty($params_job['donnees'][0]['tags']))
			{
				$tags=explode(',',$params_job['donnees'][0]['tags']);
				if(count($tags)==1)
					$tags = $params_job['donnees'][0]['tags'];
			}
			else
				$tags=array();
	
			$donnees_video=array
			(
				'url' => $url,
				'title'=>$params_job['donnees'][0]['title'],
				'channel'=>$params_job['donnees'][0]['channel'],
				'tags'=> $tags,
				'description'=>str_replace('\n',"\n",$params_job['donnees'][0]['description']),
				'published'=>$published,
				'private'=>$private,
				'allow_comments'=>$allow_comments,
				'language'=>strtolower($params_job['donnees'][0]['language'])
			);
	
			 trace('Dailymotion video_id:<id>'.$this->id_video_dailymotion.'</id>');
			if (isset($params_job['donnees'][0]['country']) && !empty($params_job['donnees'][0]['country']))
				$donnees_video['country']=$params_job['donnees'][0]['country'];
			// ajout du parametre thumbnail_url
			// attention il faut un compte official ou motionmaker
			if (($this->type_user['type']=='motionmaker' || $this->type_user['type']=='official')){
				if (!empty($params_job['donnees'][0]['thumbnail_url'])) {
					$donnees_video['thumbnail_url'] = $params_job['donnees'][0]['thumbnail_url'];
				} elseif (!empty($params_job['donnees'][0]['thumbnail_path']) && file_exists($params_job['donnees'][0]['thumbnail_path'])) {
					$donnees_video['thumbnail_url'] = $this->api->uploadFile($params_job['donnees'][0]['thumbnail_path']);
				}
			}
			trace('vignette '. $donnees_video['thumbnail_url']);

			// donnees de la video
			$result=$this->api->post('/videos', $donnees_video);
			
			$this->id_video_dailymotion=$result['id'];
			unlink($this->link_file_path);

			if($private == true){
				$this->private_id = $this->api->call('GET /video/'.$this->id_video_dailymotion,array('fields'=>'private_id'));
			}

			if ((!empty($params_job['donnees'][0]['srt'][0]['lang']) && !empty($params_job['donnees'][0]['srt'][0]['url']))){
				$langue=explode(',',$params_job['donnees'][0]['srt'][0]['lang']);
				$url=explode(',',$params_job['donnees'][0]['srt'][0]['url']);
				foreach ($langue as $key=>$lg) {
					if(!empty($lg) && !empty($url[$key])){		
						$path_srt=$url[$key];
						//trace('url srt => '.$path_srt);
						$url_srt=$this->api->uploadFile($path_srt);
						//trace('url upload =>');
						//trace(print_r($url_srt,1));
						$this->api->call('POST /video/'.$this->id_video_dailymotion.'/subtitles',array('format'=>'SRT','language'=>strtolower($lg),'url'=>$url_srt));
					}
				}
			}


			// ajout de la video a une playlist
			if (!empty($params_job['donnees'][0]['playlist']))
			{
				$liste_video=array();
				$page=1;
				do
				{
					$list_videos_pl=$this->api->call('GET /playlist/'.$params_job['donnees'][0]['playlist'].'/videos',array('page'=>$page));

					foreach ($list_videos_pl['list'] as $donnees_playlists)
					{
						$liste_video[]=$donnees_playlists['id'];
					}
					$page++;
				}
				while ($list_videos_pl['has_more']);

				$liste_video[]=$this->id_video_dailymotion;
				$result=$this->api->call('POST /playlist/'.$params_job['donnees'][0]['playlist'].'/videos',array('ids'=>$liste_video));
			}
			
			if (!empty($params_job['video_star']) && $private == false) {
				$id_user = $this->api->call('GET /video/'.$this->id_video_dailymotion,array('fields'=>'owner.id'));
				$id_user = $id_user['owner.id'];

				$this->api->call('POST /user/'.$id_user,array('videostar'=>$this->id_video_dailymotion));
			}
		}
		catch(DailymotionAuthRequiredException $e)
		{
			$this->delete_video_from_dm($this->id_video_dailymotion);
			throw new InvalidXmlParamException('Dailymotion authentification error 2 : '.$e->getMessage(),$e->getCode());
		}
		catch(DailymotionTransportException $e)
		{
			$this->delete_video_from_dm($this->id_video_dailymotion);
			throw new UploadException('Dailymotion upload error : '.$e->getMessage(),$e->getCode());
		}
		catch(DailymotionAuthException $e)
		{
			$this->delete_video_from_dm($this->id_video_dailymotion);
			throw new InvalidXmlParamException('Dailymotion authentification error : '.$e->getMessage(),$e->getCode());
		}
		catch(DailymotionApiException $e)
		{
			//Patch pour le code d'erreur 500 qui apparait alors que tout est correct. On vérifie simplement que l'on accède bien à la vidéo.
			if($e->getCode() == '500') {
				try {
					$test_title_video = $this->api->call('GET /video/'.$this->id_video_dailymotion,array('fields'=>'title'));
					$test_title_video = $test_title_video['title'];
				} catch(Exception $e2) {

				}
			}

			if(empty($test_title_video) || $test_title_video != $donnees_video['title']) {
				unset($test_title_video);
				$this->delete_video_from_dm($this->id_video_dailymotion);
				throw new InvalidXmlParamException('Dailymotion API error : '.$e->getMessage(),$e->getCode());
			}
		}
	}


	function update_video_dm($id_doc=null,$etape=null){
		global $db;
		if(!isset($id_doc) || empty($id_doc)){
			$this->dropError("Erreur update_video_dm, pas d'id_doc défini ");
			return false ;
		}
		if(!empty($id_doc)){
			require_once(modelDir.'model_doc.php');
			$myDoc = new Doc();
			$myDoc->t_doc['ID_DOC'] = $id_doc;
			$myDoc->getDoc();
			$myDoc->getDocMat();
			$myDoc->getValeurs();
			$myDoc->getDocPublication();
			$myDoc->getMats(true);

			if(empty($myDoc->t_doc_publication)){
				$this->dropError("Erreur, pas de publication détectée pour la vidéo n:".$id_doc);
				return false ;
			}

			foreach($myDoc->t_doc_publication as $dpub){
				if($dpub['PUB_CIBLE'] == 'dailymotion' && $dpub['PUB_ID_ETAPE'] == $etape){
					$id_dm = $dpub['PUB_VALUE'];
					break ;
				}
			}
			if( !isset($id_dm) || empty($id_dm)){
				$this->dropError("Erreur récupération de l'id youtube de la video");
				return false ;
			}


			try
			{
				/*------------- update données depuis Opsis ---------*/
				$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$myDoc->xml_export()."</EXPORT_OPSIS>";
				$xml_params = TraitementXSLT($xml,$this->doc_xsl,null,0,"",null,false);

				$xml_tab = xml2tab($xml_params);
				$dm_donnees = $xml_tab['DONNEES'][0];
				$donnees_video=array
				(
					'id'=>$id_dm,
				);


				if(isset($dm_donnees['TITLE'])){
					$donnees_video['title'] = $dm_donnees['TITLE'];
				}
				if(isset($dm_donnees['DESCRIPTION']) ){
					$donnees_video['description'] = $dm_donnees['DESCRIPTION'];
				}
				if(isset($dm_donnees['CHANNEL']) ){
					$donnees_video['channel'] = $dm_donnees['CHANNEL'];
				}
				if(isset($dm_donnees['TAGS'])){
					$donnees_video['tags'] = explode(',',$dm_donnees['TAGS']);
				}
				if(isset($dm_donnees['SRT_URL'])){
					$srt['srt_url'] =  kCheminMediaPublic.'/'.$dm_donnees['SRT_URL'];
				}
				if(isset($dm_donnees['SRT_LANG'])){
					$srt['srt_lang'] = strtolower($dm_donnees['SRT_LANG']);
				}

				//champs a priori déprécié
				// if(isset($dm_donnees['OFFICIAL']) ){
				// 	$donnees_video['official'] = $dm_donnees['OFFICIAL'];
				// }
				// if(isset($dm_donnees['CREATIVE']) ){
				// 	$donnees_video['creative'] = $dm_donnees['CREATIVE'];
				// }

				if(isset($dm_donnees['PUBLISHED']) && !empty($dm_donnees['PUBLISHED']) && ($dm_donnees['PUBLISHED'] == '1' || $dm_donnees['PUBLISHED'] =='true')){
					$donnees_video['published'] = true ;
				}else{
					$donnees_video['published'] = false;
				}
				if(isset($dm_donnees['PRIVATE']) && !empty($dm_donnees['PRIVATE']) && ($dm_donnees['PRIVATE'] == '1' || $dm_donnees['PRIVATE'] =='true') ||isset($this->private) && !empty($this->private) && ($this->private == '1' || $this->private =='true') ){
					$donnees_video['private'] = true ;
				}else{
					$donnees_video['private'] = false;
				}

				if(isset($dm_donnees['ALLOW_COMMENTS']) && !empty($dm_donnees['ALLOW_COMMENTS']) && ($dm_donnees['ALLOW_COMMENTS'] == '1' || $dm_donnees['ALLOW_COMMENTS'] =='true')){
					$donnees_video['allow_comments'] = true ;
				}else{
					$donnees_video['allow_comments'] = false;
				}
				$result=$this->api->call('video.edit',$donnees_video);


		 if (($this->type_user['type']=='motionmaker' || $this->type_user['type']=='official') && !empty($srt['srt_url']) && !empty($srt['srt_lang'])){
				$this->api->call('POST /video/'.$id_dm.'/subtitles',array('format'=>'SRT','language'=>strtolower($srt['srt_lang']),'url'=>$srt['srt_url']));
			}

			}
			catch (DailymotionAuthRequiredException $e)
			{
					echo 'DailymotionAuthRequiredException '.$e->getMessage();
			}
			catch (DailymotionApiException $e)
			{
					echo 'DailymotionApiException '.$e->getMessage();
			}

		}
	}

	function delete_from_dm($id_doc,$etape=null){
		global $db;
		if(!isset($id_doc) || empty($id_doc)){
			$this->dropError("Erreur delete_from_yt, pas d'id_doc défini ");
			return false ;
		}
		if(!empty($id_doc)){
			require_once(modelDir.'model_doc.php');
			$myDoc = new Doc() ;
			$myDoc->t_doc['ID_DOC'] = $id_doc;
			$myDoc->getDoc();
			$myDoc->getDocPublication();
			if(!empty($myDoc->t_doc_publication)){
				foreach($myDoc->t_doc_publication as $pub){
					if(!empty($etape)){
						if($pub['PUB_CIBLE'] == 'dailymotion' && $pub['PUB_ID_ETAPE'] == $etape){
							$id_video = $pub['PUB_VALUE'];
							if(!isset($id_video) || empty($id_video)){
								$this->dropError("Erreur delete_from_dm, id video dailymotion non trouvé");
								return false ;
							}
							$ok=$this->api->delete('/video/' . $id_video);
							trace("retour delete video ".$id_video.": ".print_r($ok,true));
						}
					}
					else{
						if($pub['PUB_CIBLE'] == 'dailymotion'){
						$id_video = $pub['PUB_VALUE'];
						if(!isset($id_video) || empty($id_video)){
							$this->dropError("Erreur delete_from_dm, id video dailymotion non trouvé");
							return false ;
						}
						$ok=$this->api->delete('/video/' . $id_video);
						trace("retour delete video ".$id_video.": ".print_r($ok,true));
						}
				}
			}

		}else{
			$this->dropError("Erreur - non connecté ou params manquants");
			return false ;
		}
	}
	}

	
	function get_videos_dm(){
		$videos=array();
		$page=1;
		do
		{
			$listResponse=$this->api->call('GET /me/videos',array('page'=>$page,'limit'=>100));
			if(isset($listResponse['list'])){
				$videos=array_merge($videos, $listResponse['list']);
			}
			$page++;
		}
		while ($listResponse['has_more']);
		return $videos;
	 }

	function delete_video_from_dm($id_video){
		if(!isset($id_video) || empty($id_video)){
			$this->dropError("Erreur delete_from_dm, id video dailymotion non trouvé");
			return false ;
		}
		$ok=$this->api->delete('/video/' . $id_video);
		trace("retour delete video ".$id_video.": ".print_r($ok,true));
	}

	//Récupère les playlists depuis la BDD
	//TODO : à la place d'utiliser un paramètre pour identifier le compte, la classe présente étant censée représenter ce compte, il faudrait donner un identifiant à l'objet, et l'utiliser ici.
	function getPlaylists($idAccount) {
		global $db;
		
		if(!empty($this->aPlaylists))
			return $this->aPlaylists;
		
		$DM_PlaylistsIds = unserialize(gAccountDailyPlaylist);
		$typeValPlaylist = $DM_PlaylistsIds[$idAccount];
		
		$sql = "select id_val as ID_OPSIS, valeur as NOM, val_code as ID_DM from t_val where val_id_type_val = ".$db->Quote($typeValPlaylist)." order by NOM;";
		$this->aPlaylists = $db->GetAll($sql);
		
		return $this->aPlaylists;
	}

	function xml_export_playlist($entete="",$encodage="",$indent="") {

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
		$content .= $indent . "<t_account_playlists count='" . count ( $this->aPlaylists ) . "'>\n";
		
		if (!empty ( $this->aPlaylists )) {
			foreach ( $this->aPlaylists as $idx => $aPlaylist ) {
				$content .= $indent . "\t<t_playlist>";
				foreach ( $aPlaylist as $fld => $val ) {
					$content .= $indent . "\t\t<" . strtoupper ( $fld ) . ">" . xml_encode ( $val ) . "</" . strtoupper ( $fld ) . ">\n";
				}
				$content .= $indent . "\t</t_playlist>\n";
			}
		}

		$content.=$indent."</t_account_playlists>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
	
	function dropError($errLib) { 
		$this->error_msg.=$errLib."<br/>";
	}

	



}


?>
