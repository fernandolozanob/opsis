function toggleFields(myCheck,myTab) {
	state=!myCheck.checked;
	myTab.style.opacity=(state==true?0.5:1);
	myTab.style.filter='Alpha:opacity='+(state==true?50:100);
	
	inputs=myTab?myTab.getElementsByTagName('input'):null;
	for (i=0;i<inputs.length;i++){ 
		if(inputs[i].name != 'makedoc' ){
			inputs[i].disabled=state;
		}
	}
	inputs=myTab?myTab.getElementsByTagName('select'):null;
	for (i=0;i<inputs.length;i++) inputs[i].disabled=state;
}

function urlencode(str)
{
    return escape(str.replace(/%/g, '%25').replace(/\+/g, '%2B')).replace(/%25/g, '%');
}
/*
function validerForm(json)
{
	file_upload_num=1;
	json = $j.parseJSON(json);
	
	$j("#form_envoi_fichiers").append('<input type="hidden" name="file_'+file_upload_num+'" value="'+json[0]["name"]+'" />');
	$j("#form_envoi_fichiers").append('<input type="hidden" name="file_'+file_upload_num+'_original" value="'+json[0]["name"]+'" />');
	file_upload_num++;
	$j("#form_envoi_fichiers").submit();
}*/
function validerForm(json)
{
	file_upload_num=1;
	if(json === 'false'){
		alert(str_lang.kUploadFtpFileNotFound);
		return false ; 
	}
	
	json = $j.parseJSON(json);

	current_row = $j("#tabs_upload div[id^='upload_'] .files:visible");
	if (current_row.find("input.fileRenameInput").eq(0).val() == ''){
		current_row.find("input.fileRenameInput").eq(0).attr('disabled','true');
	}
	current_row.find("select[name*='%N%'],input[name*='%N%']").each(function(idx,elt){
		$j(elt).attr('name',$j(elt).attr('name').replace('%N%',file_upload_num));
	});
	current_row.find("input.fileName").eq(0).attr('value',json[0]["name"]);
	 
	
		 
	$j("#form_envoi_fichiers").submit();
}

function envoyerFichierURL(type)
{
	$j("#type_import").val("URL");
	var prefixe='';
	// normalisation du 'http://', on l'ajoute au début, mais on le supprime de l'input si il est présent dedans 
	if($j('#url_upload').val().search('https')==0) prefixe='https://'; else prefixe='http://';
	url_upload = prefixe+$j('#url_upload').val().replace(prefixe, '');
	sendData('GET','empty.php','urlaction=jqueryUploadHandler&upload=1&type=url&url='+urlencode(url_upload)+'&upload_type='+type+'','validerForm');
}

function envoyerFichierFTP(type)
{
var url_ftp;
var ftp_upload;
$j("#type_import").val("FTP");
ftp_upload = $j('#ftp_upload').val();
ftp_upload = ftp_upload.replace('ftp://', '');
url_ftp = 'ftp://'+$j('#login_ftp').val()+':'+ $j('#mdp_ftp').val()+'@'+ ftp_upload;
sendData('GET','empty.php','urlaction=jqueryUploadHandler&upload=1&type=url&url='+urlencode(url_ftp)+'&upload_type='+type+'','validerForm');
}




var formInherits = [];
// Fonction permettant de créer une relation master / slave avec héritage de valeur (ex formulaire d'import, relation entre select dans le formulaire & select par row)
// La fonction est orientée pour fonctionner avec des select, en revanche "master_selector" et "slave_selector" en paramètres font references à des sélecteurs css utilisés par jquery
// ex :  createSelectInheritance("select#selectTheme","select.fileTheme");
function createSelectInheritance(master_selector,slave_selector){
	master_elem = $j(master_selector).eq(0);
	formInherits.push({ 
		'master_selector' : master_selector,
		'slave_selector' : slave_selector 
	});
	
	master_elem.change(function(){
		if( typeof master_elem.attr('disabled') == 'undefined'){
			$j(slave_selector).val(master_elem.val());
		}
	});
}

// Fonction permettant de créer un comportement d'activation d'un ou plusieurs input(s) en fonction de l'état d'activation d'une checkbox
// checkbox_selector & affected_selector sont des selecteurs css tels que ceux utilisés par jquery (ex : createCheckboxActivation("input#addThemeCheckbox","select#selectTheme"))
// checkInheritance permet de tester l'existence d'une relation d'héritage dans l'array formInherits et d'appliquer les memes effets aux éventuels slaves d'un élément modifié

function createCheckboxActivation(checkbox_selector, affected_selector, checkInheritance){
	if(typeof checkInheritance == 'undefined'){
		checkInheritance = false ; 
	}
	$j(checkbox_selector).change(function(){
		// récupération des héritages éventuels : 
		childsSelector = "";
		if(checkInheritance){
			for (i=0 ; i < formInherits.length ; i ++ ) {
				if(formInherits[i]['master_selector'] == affected_selector){
					childsSelector+= ',';
					childsSelector += formInherits[i]['slave_selector'];
				}
			}
		}
	
		if ($j(this).prop("checked")){
			$j(affected_selector+childsSelector).removeAttr("disabled");
		}else{
			$j(affected_selector+childsSelector).attr("disabled","true");
		}
	})
	

}




function toggleReportage(checked){
	if (checked){
		$j("select#selectReport, #newReportName").removeAttr("disabled");
	}else{
		$j("select#selectReport, #newReportName").attr("disabled","true");
	}
	toggleReportName($j("select#selectReport").val());
}

function toggleReportName(value) {
	if (value == "new" && typeof $j("select#selectReport").attr('disabled')=='undefined')
		$j("label.labelNewReportName, #newReportName").show();
	else
		$j("label.labelNewReportName, #newReportName").hide();
}


