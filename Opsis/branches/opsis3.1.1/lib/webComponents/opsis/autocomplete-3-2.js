//update VG 21/06/2011
//valueSep :  Désigne la chaine séparant la valeur à insérer dans le champ, des données d'affichage seul
var valueSep;
var autocompl_style_initialized = false ; 
// retourne un objet xmlHttpRequest.
// m�thode compatible entre tous les navigateurs (IE/Firefox/Opera)
function getXMLHTTP(){
  var xhr=null;
  if(window.XMLHttpRequest) // Firefox et autres
  xhr = new XMLHttpRequest();
  else if(window.ActiveXObject){ // Internet Explorer
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e1) {
        xhr = null;
      }
    }
  }
  else { // XMLHttpRequest non support� par le navigateur
    alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
  }
  return xhr;
}




var _documentForm=null; // le formulaire contenant notre champ texte
var _inputField=null; // le champ texte lui-m�me
var _submitButton=null; // le bouton submit de notre formulaire
var _useCache = true;	//Définit si on utilise le cache, ou bien si on refait la requête à chaque fois
var _quoteSuggestion = false;	//Définit si on doit ajouter des doubles quotes autour de la valeur sélectionnée

function initAutoComplete(form,field,submit,php_process,useCache,quoteSuggestion){
  _documentForm=form;
  if(typeof useCache != 'undefined' && useCache != '') {
	  _useCache = useCache;
  }
  
   if(typeof quoteSuggestion != 'undefined' && quoteSuggestion != null) {
	  _quoteSuggestion = quoteSuggestion ; 
  }

  // 21/08/2007 Modifié par LD & VP pour pouvoir gérer l'autocomplétion sur plusieurs champs d'une même page
  if (_inputField && _inputField!=field) { //RAZ Champ précédent (ex: focus sur un autre champ avec autocompletion
	  _inputField.onkeyup=null;
	  _inputField.onblur=null;
	  _resultCache=new Object(); // RAZ cache car on peut changer de type de recherche
  }
  if (_inputField==null) setTimeout("mainLoop()",200) // Jamais chargé ? => Premier d�clenchement de la fonction dans 200 millisecondes
  // Fin modifs

  _inputField=field;
  _submitButton=submit;
  _inputField.autocomplete="off";
  creeAutocompletionDiv();
  _currentInputFieldValue=_inputField.value;
  _oldInputFieldValue=_currentInputFieldValue;
  cacheResults("",new Array());
  document.onkeydown=onKeyDownHandler;
  _inputField.onkeyup=onKeyUpHandler;
  _inputField.onblur=onBlurHandler;
  _adresseRecherche=HTMLentitiesdecode(php_process);
  window.onresize=onResizeHandler;

}


var _oldInputFieldValue=""; // valeur pr�c�dente du champ texte
var _currentInputFieldValue=""; // valeur actuelle du champ texte
var _resultCache=new Object(); // m�canisme de cache des requetes

// tourne en permanence pour suggerer suite � un changement du champ texte
function mainLoop(){
  if(_oldInputFieldValue!=_currentInputFieldValue){
    var valeur=escapeURI(_currentInputFieldValue.trim());
    var suggestions=_resultCache[_currentInputFieldValue];
    if(suggestions){ // la r�ponse �tait encore dans le cache
      metsEnPlace(valeur,suggestions)
    }else{
      callSuggestions(valeur) // appel distant
    }
    _inputField.focus()
  }
  _oldInputFieldValue=_currentInputFieldValue;
  setTimeout("mainLoop()",200); // la fonction se red�clenchera dans 200 ms
  return true
}

// echappe les caract�re sp�ciaux
function escapeURI(La){
  if(encodeURIComponent) {
    return encodeURIComponent(La);
  }
  if(escape) {
    return escape(La)
  }
}
// equivalent de lastIndexOf version  regex (renvoi l'index de la fin de la chaine matchée)
function regexlast(string,re){
  var tokens=string.split(re);
  return (tokens.length>1)?(string.length-tokens[tokens.length-1].length):null;
}

var _xmlHttp = null; //l'objet xmlHttpRequest utilis� pour contacter le serveur


function callSuggestions(valeur){
  if(_xmlHttp&&_xmlHttp.readyState!=0){
    _xmlHttp.abort()
  }
  _xmlHttp=getXMLHTTP();
  if(_xmlHttp){
    //appel � l'url distante
	if(_quoteSuggestion){
	lastIndexTest = regexlast(valeur,/"(%20|\s)*(%2C|%26)/g);
	}else{
		lastIndexTest = regexlast(valeur,/(%20|\s)*(%2C|%26)/g);
	}
	
	if (lastIndexTest>0) // on cherche la derniere virgule
		valeur=valeur.substring(lastIndexTest);
		
	valeur = valeur.replace(/^(\s|%20|')+|(\s|%20|')+$/g, '');
		
    _xmlHttp.open("GET",_adresseRecherche+"&debut="+valeur,true);
    _xmlHttp.onreadystatechange=function() {
      if(_xmlHttp.readyState==4&&_xmlHttp.responseXML) {
        var liste = traiteXmlSuggestions(_xmlHttp.responseXML)
        cacheResults(valeur,liste)
        metsEnPlace(valeur,liste)
      }
    };
    // envoi de la requete
    _xmlHttp.send(null)
  }
}

// Mecanisme de caching des r�ponses
function cacheResults(debut,suggestions){
	if(_useCache) { 
		_resultCache[debut]=suggestions
	}
}

// Transformation XML en tableau
function traiteXmlSuggestions(xmlDoc) {
  var options = xmlDoc.getElementsByTagName('option');
  var optionsListe = new Array();
  for (var i=0; i < options.length; ++i) {
    optionsListe.push(options[i].firstChild.data);
  }
  return optionsListe;
}

//ins�re une r�gle avec son nom
function insereCSS(nom,regle){
  if (document.styleSheets) {
    var I=document.styleSheets[0];
    if(I.addRule){ // m�thode IE
      I.addRule(nom,regle)
    }else if(I.insertRule){ // m�thode DOM
      I.insertRule(nom+" { "+regle+" }",I.cssRules.length)
    }
  }
}

function initStyle(){
/* var AutoCompleteDivListeStyle="text-align:left;font-size: 11px; font-family: arial,sans-serif; display:block;";
  var AutoCompleteDivStyle=" display: block; padding-left: 3px; padding-right: 3px; overflow: hidden; background-color: #F5F5F5;";
  var AutoCompleteDivActStyle="padding-left: 3px; padding-right: 3px;background-color: #3366cc; color: white ! important; ";
  insereCSS(".AutoCompleteDivListeStyle",AutoCompleteDivListeStyle);
  insereCSS(".AutoCompleteDiv",AutoCompleteDivStyle);
  insereCSS(".AutoCompleteDivAct",AutoCompleteDivActStyle);*/

 // MS 15.05.14 - Le comportement standard consistait a ajouté à document.styleSheets[0] une règle css supplémentaire
 // Sur Firefox, une sécurité interdit d'ajouter une règle à document.styleSheets lorsque ces règles sont issues d'un fichier provenant d'un autre domaine 
 // j'ajoute donc les règles CSS au document directement et non à la 'représentation' du fichier css externe
 // + petite amélioration pour éviter de charger ces règles une fois par tentative d'autocomplete ... 
 
  if(!autocompl_style_initialized){
	style_autocompl = document.createElement("style");
	css = ".AutoCompleteDivListeStyle{text-align:left;font-size: 11px; font-family: arial,sans-serif; display:block;}\n"
		+".AutoCompleteDiv{ display: block; padding-left: 3px; padding-right: 3px; overflow: hidden; background-color: #F5F5F5;}\n"
		+".AutoCompleteDivAct{padding-left: 3px; padding-right: 3px;background-color: #3366cc; color: white ! important;}\n";
	
	head = document.head || document.getElementsByTagName('head')[0];
	head.appendChild(style_autocompl);
  
	style_autocompl.type = 'text/css';
	if (style_autocompl.styleSheet){
	  style_autocompl.styleSheet.cssText = css;
	} else {
	  style_autocompl.appendChild(document.createTextNode(css));
	}
  
  
	autocompl_style_initialized = true ; 
  }
}

function setStylePourElement(c,name){
  c.className=name;
}

// calcule le d�calage � gauche
function calculateOffsetLeft(r){
  return $j(r).offset().left;//calculateOffset(r,"offsetLeft")
}

// calcule le d�calage vertical
function calculateOffsetTop(r){
  return $j(r).offset().top;//calculateOffset(r,"offsetTop")
}

function calculateOffset(r,attr){
  var kb=0;
  while(r){
    kb+=r[attr];
    r=r.offsetParent
  }
  return kb
}

// calcule la largeur du champ
function calculateWidth(){
  return _inputField.offsetWidth-2*1
}

function setCompleteDivSize(){
  if(_completeDiv){
	_completeDiv.style.left=calculateOffsetLeft(_inputField)+"px";
    _completeDiv.style.top=calculateOffsetTop(_inputField)+_inputField.offsetHeight-1+"px";
    _completeDiv.style.width=calculateWidth()+"px"
  }
}

function creeAutocompletionDiv() {
	if(document.getElementById('completeDiv')){
		elem = document.getElementById('completeDiv');
		document.body.removeChild(elem);
	}
	
  initStyle();
  _completeDiv=document.createElement("DIV");
  _completeDiv.id="completeDiv";
  var borderLeftRight=1;
  var borderTopBottom=1;
  _completeDiv.style.borderRight="black "+borderLeftRight+"px solid";
  _completeDiv.style.borderLeft="black "+borderLeftRight+"px solid";
  _completeDiv.style.borderTop="black "+borderTopBottom+"px solid";
  _completeDiv.style.borderBottom="black "+borderTopBottom+"px solid";
  _completeDiv.style.zIndex="100";
  _completeDiv.style.paddingRight="0";
  _completeDiv.style.paddingLeft="0";
  _completeDiv.style.paddingTop="0";
  _completeDiv.style.paddingBottom="0";
  setCompleteDivSize();
  _completeDiv.style.visibility="hidden";
  _completeDiv.style.position="absolute";
  _completeDiv.style.backgroundColor="white";
  document.body.appendChild(_completeDiv);
  $j(_completeDiv).mousedown(test_block_blur);
  setStylePourElement(_completeDiv,"AutoCompleteDivListeStyle");
}

function test_block_blur(evt){
	// console.log("test_block_blur",evt);
	if($j(evt.target).hasClass('AutoCompleteDivListeStyle')){
		// console.log("test_block_blur 2 pass ");
		_cursorUpDownPressed = true ; 
	}	
}

function metsEnPlace(valeur, liste){
  while(_completeDiv.childNodes.length>0) {
    _completeDiv.removeChild(_completeDiv.childNodes[0]);
  }
  // mise en place des suggestions
  for(var f=0; f<liste.length; ++f){
    var nouveauDiv=document.createElement("DIV");
    nouveauDiv.onmousedown=divOnMouseDown;
    nouveauDiv.onmouseover=divOnMouseOver;
    nouveauDiv.onmouseout=divOnMouseOut;
	
    setStylePourElement(nouveauDiv,"AutoCompleteDiv");
    var nouveauSpan=document.createElement("SPAN");
    nouveauSpan.innerHTML=liste[f]; // le texte de la suggestion
    nouveauDiv.appendChild(nouveauSpan);
    _completeDiv.appendChild(nouveauDiv)
  }
  PressAction();
  if(_completeDivRows>0) {
    _completeDiv.height=16*_completeDivRows+4;
  } else {
    hideCompleteDiv();
  }

}

var _lastKeyCode=null;
var _keyModifierOn = false ; 
var _macOsMetakeyCode = null ;
// Handler pour le keydown du document
var onKeyDownHandler=function(event){
  // acc�s evenement compatible IE/Firefox
  if(!event&&window.event) {
    event=window.event;
  }
  if( _macOsMetakeyCode  == null && event.metaKey ){
	// console.log(event.metaKey);
	_macOsMetakeyCode = event.keyCode;
	// console.log("set mac keymodifier on ");
	_keyModifierOn = true ; 
  }
  
    if(event.keyCode == 17 && !_keyModifierOn){
	// console.log("set keymodifier on ");
	_keyModifierOn = true ; 
  }
  // on enregistre la touche ayant d�clench� l'evenement
  if(event) {
    _lastKeyCode=event.keyCode;
  }
  
}

var _eventKeycode = null;
var _val_champ_orig='';
// Handler pour le keyup de lu champ texte
var onKeyUpHandler=function(event){
  // acc�s evenement compatible IE/Firefox
  if(!event&&window.event) {
    event=window.event;
  }
  
  _eventKeycode=event.keyCode;
  // Dans les cas touches touche haute(38) ou touche basse (40)
  if(_eventKeycode==40||_eventKeycode==38) {
    // on autorise le blur du champ (traitement dans onblur)
	//PC 08/12/10 : blurThenGetFocus désactivé car provoque bug des propositions qui ne disparaissent pas
	  //blurThenGetFocus();
  }
  // taille de la selection
  var N=rangeSize(_inputField);
  // taille du texte avant la selection (selection = suggestion d'autocompl�tion)
  var v=beforeRangeSize(_inputField);
  // contenu du champ texte
  var V=_inputField.value;
  
 /* V=V.split(',');
  //alert(V);
  _val_champ_orig='';
  for (i=0;i<V.length-1;i++)
	_val_champ_orig+=V[i]+',';
  //alert(_val_champ_orig)
  V=V[V.length-1];
  if(V!='' && _eventKeycode!=0){*/
    if(N>0&&v!=-1) {
      // on recupere uniquement le champ texte tap� par l'utilisateur
      V=V.substring(0,v);
    }
    // 13 = touche entr�e
	if(_eventKeycode == 17 && _keyModifierOn){
		// console.log("key modifier release");
		_keyModifierOn = false ; 
	}
	
    if(_eventKeycode == 17  // control
	|| _eventKeycode == 91 || _eventKeycode == 92  // left & right windows key 
	|| _eventKeycode == 16 || _eventKeycode == 18  // shift & alt
	|| _keyModifierOn 								// n'importe quelle autre key pendant que ctrl est pressé
	|| _eventKeycode == 37 || _eventKeycode == 39 ){ // fleches gauche / droite
		// console.log("return false, controls keys or modifier on ");
		return false ; 
	}
	
	if(_eventKeycode==13||_eventKeycode==3){
      var d=_inputField;
      // on mets en place l'ensemble du champ texte en repoussant la selection
      if(_inputField.createTextRange){
        var t=_inputField.createTextRange();
        t.moveStart("character",_inputField.value.length);
        _inputField.select()
      } else if (d.setSelectionRange){
        _inputField.setSelectionRange(_inputField.value.length,_inputField.value.length)
      }
    } else {
      // si on a pas pu agrandir le champ non selectionn�, on le mets en place violemment.
      if(_inputField.value!=V) {
        _inputField.value=V;//_val_champ_orig+V
      }
    }
 /* }*/
  // si la touche n'est ni haut, ni bas, on stocke la valeur utilisateur du champ
  if(_eventKeycode!=40&&_eventKeycode!=38) {
    // le champ courant n est pas change si key Up ou key Down
  	_currentInputFieldValue=V;
  }
  if(handleCursorUpDownEnter(_eventKeycode)&&_eventKeycode!=0) {
    // si on a pr�ss� une touche autre que haut/bas/enter
    PressAction();
  }
}

// Change la suggestion selectionn�.
// cette m�thode traite les touches haut, bas et enter
function handleCursorUpDownEnter(eventCode){
  if(eventCode==40){
    highlightNewValue(_highlightedSuggestionIndex+1);
    return false
  }else if(eventCode==38){
    highlightNewValue(_highlightedSuggestionIndex-1);
    return false
  }else if(eventCode==13||eventCode==3){
    return false
  }
  return true
}

var _completeDivRows = 0;
var _completeDivDivList = null;
var _highlightedSuggestionIndex = -1;
var _highlightedSuggestionDiv = null;

// g�re une touche press�e autre que haut/bas/enter
function PressAction(){
  _highlightedSuggestionIndex=-1;
  var suggestionList=_completeDiv.getElementsByTagName("div");
  var suggestionLongueur=suggestionList.length;
  // on stocke les valeurs pr�c�dentes
  // nombre de possibilit�s de compl�tion
  _completeDivRows=suggestionLongueur;
  // possiblit�s de compl�tion
  _completeDivDivList=suggestionList;
  // si le champ est vide, on cache les propositions de compl�tion
  if(_currentInputFieldValue==""||suggestionLongueur==0){
    hideCompleteDiv()
  }else{
    showCompleteDiv()
  }
  var trouve=false;
  // si on a du texte sur lequel travailler
  if(_currentInputFieldValue.length>0){
    var indice;
    // T vaut true si on a dans la liste de suggestions un mot commencant comme l'entr�e utilisateur
    for(indice=0; indice<suggestionLongueur; indice++){
      if(getSuggestion(suggestionList.item(indice)).toUpperCase().indexOf(_currentInputFieldValue.toUpperCase())==0) { // _val_champ_orig.toUpperCase()
        trouve=true;
        break
      }
    }
  }
  // on d�s�lectionne toutes les suggestions
  for(var i=0; i<suggestionLongueur; i++) {
    setStylePourElement(suggestionList.item(i),"AutoCompleteDiv");
  }
  // si l'entr�e utilisateur (n) est le d�but d'une suggestion (n-1) on s�lectionne cette suggestion avant de continuer
  if(!trouve){
    _highlightedSuggestionIndex=-1;
    _highlightedSuggestionDiv=null
  }
  var supprSelection=false;
  switch(_eventKeycode){
    // cursor left, cursor right, page up, page down, others??
    case 8:
    case 33:
    case 34:
    case 35:
    case 35:
    case 36:
    case 37:
    case 39:
    case 45:
    case 46:
      // on supprime la suggestion du texte utilisateur
      supprSelection=true;
      break;
    default:
      break
  }
  // si on a une suggestion (n-1) s�lectionn�e
  if(!supprSelection && _highlightedSuggestionDiv){
    setStylePourElement(_highlightedSuggestionDiv,"AutoCompleteDivAct");
    var z;
    if(trouve) {
      z=getSuggestion(_highlightedSuggestionDiv).substr(0);
    } else {
      z=_currentInputFieldValue;//_val_champ_orig+_currentInputFieldValue;
    }

    if(z!=_inputField.value){
      if(_inputField.value!=_currentInputFieldValue) { // _val_champ_orig+
        return;
      }

      // si on peut cr�er des range dans le document
      /*if(_inputField.createTextRange||_inputField.setSelectionRange) {
        _inputField.value=z;
      }*/ 
	  //addValueOn_inputField(z);
		
      // on s�lectionne la fin de la suggestion
      if(_inputField.createTextRange){
        var t=_inputField.createTextRange();
        t.moveStart("character",(_currentInputFieldValue).length); // _val_champ_orig+
        t.select()
      }else if(_inputField.setSelectionRange){
        _inputField.setSelectionRange((_currentInputFieldValue).length,_inputField.value.length) // _val_champ_orig+
      }
    }
  }else{
    // sinon, plus aucune suggestion de s�lectionn�e
    _highlightedSuggestionIndex=-1;
  }
}

var _cursorUpDownPressed = null;

// permet le blur du champ texte apr�s que la touche haut/bas ai �t� press�.
// le focus est r�cup�r� apr�s traitement (via le timeout).
function blurThenGetFocus(){
  _cursorUpDownPressed=true;
  _inputField.blur();
  setTimeout("_inputField.focus();",10);
  return
}

// taille de la selection dans le champ input
function rangeSize(n){
  var N=-1;
  if(document.selection && n.createTextRange){
    var fa=document.selection.createRange().duplicate();
    N=fa.text.length
  }else if(n.setSelectionRange){
    N=n.selectionEnd-n.selectionStart
  }
  return N
}

// taille du champ input non selectionne
function beforeRangeSize(n){
  var v=0;
  if(document.selection && n.createTextRange){
    var fa=document.selection.createRange().duplicate();
    fa.moveEnd("textedit",1);
    v=n.value.length-fa.text.length
  }else if(n.setSelectionRange){
    v=n.selectionStart
  }else{
    v=-1
  }
  return v
}

// Place le curseur � la fin du champ
function cursorAfterValue(n){
  if(n.createTextRange){
    var t=n.createTextRange();
    t.moveStart("character",n.value.length);
    t.select()
  } else if(n.setSelectionRange) {
    n.setSelectionRange(n.value.length,n.value.length)
  }
}


// Retourne la valeur de la possibilite (texte) contenu dans une div de possibilite
//update VG 10/06/11 : ajout de stripDataForAff, qui supprime les données relèvant uniquement de l'affichage
function getSuggestion(uneDiv){
  if(!uneDiv) {
    return null;
  }
  //return trimCR(uneDiv.getElementsByTagName('span')[0].firstChild.data)
  return trimCR(stripDataForAff(uneDiv.getElementsByTagName('span')[0].firstChild.data)); //_val_champ_orig+
}

// supprime les caract�res retour chariot et line feed d'une chaine de caract�res
function trimCR(chaine){
  for(var f=0,nChaine="",zb="\n\r"; f<chaine.length; f++) {
    if (zb.indexOf(chaine.charAt(f))==-1) {
      nChaine+=chaine.charAt(f);
    }
  }
  return nChaine;
}

//Supprime les données relèvant uniquement de l'affichage
//update VG 21/06/2011
function stripDataForAff(string) {
	if(typeof(valueSep) != 'undefined' && valueSep != '' ) {
		//On considère que tout ce qui est après une tabulation ne fait pas partie la valeur effective
	    if (string.indexOf(valueSep)==-1) {
	    	return string;
	    } else {
	    	return string.substring(0,string.indexOf(valueSep));
	    }
	} else {
		return string;
	}
	
}

// Cache completement les choix de completion
function hideCompleteDiv(){
	// console.log("call hideCompleteDiv");
  _completeDiv.style.visibility="hidden"
}

// Rends les choix de completion visibles
function showCompleteDiv(){
  _completeDiv.style.visibility="visible";
  setCompleteDivSize()
}

// Change la suggestion en surbrillance
function highlightNewValue(C){
  if(!_completeDivDivList||_completeDivRows<=0) {
    return;
  }
  showCompleteDiv();
  if(C>=_completeDivRows){
    C=_completeDivRows-1
  }
  if(_highlightedSuggestionIndex!=-1&&C!=_highlightedSuggestionIndex){
    setStylePourElement(_highlightedSuggestionDiv,"AutoCompleteDiv");
    _highlightedSuggestionIndex=-1
  }
  if(C<0){
    _highlightedSuggestionIndex=-1;
    _inputField.focus();
    return
  }
  _highlightedSuggestionIndex=C;
  _highlightedSuggestionDiv=_completeDivDivList.item(C);
  setStylePourElement(_highlightedSuggestionDiv,"AutoCompleteDivAct");
	addValueOn_inputField(getSuggestion(_highlightedSuggestionDiv));
	
	/*if(_quoteSuggestion){
		lastIndexTest = regexlast(_inputField.value,/"(\s)*(\,|\&)/g);
		if (lastIndexTest<0 || lastIndexTest == null){
			_inputField.value="\""+getSuggestion(_highlightedSuggestionDiv)+"\"";
		}else{
			_inputField.value=_inputField.value.substring(0,lastIndexTest)+" \""+getSuggestion(_highlightedSuggestionDiv)+"\"";
		}
	}else{
		lastIndexTest = regexlast(_inputField.value,/(\s)*(\,|\&)/g);
		if (lastIndexTest<0 || lastIndexTest == null){
		 _inputField.value=getSuggestion(_highlightedSuggestionDiv);
		 }else{
		_inputField.value=_inputField.value.substring(0,lastIndexTest)+" "+getSuggestion(_highlightedSuggestionDiv);
		}
	}*/
}

// Handler de resize de la fenetre
var onResizeHandler=function(event){
  // recalcule la taille des suggestions
  setCompleteDivSize();
}

// Handler de blur sur le champ texte
var onBlurHandler=function(event){
	// console.log("call onBlurHandler ",_cursorUpDownPressed);
  if(!_cursorUpDownPressed){
    // si le blur n'est pas caus� par la touche haut/bas
    hideCompleteDiv();
    // Si la derni�re touche pr�ss� est tab, on passe au bouton de validation
    if(_lastKeyCode==9){
		// VP 2/12/09 : ajout test sur _submitButton
      if(_submitButton) _submitButton.focus();
      _lastKeyCode=-1
    }
  }
  _cursorUpDownPressed=false
};

// declenchee quand on clique sur une div contenant une possibilite
var divOnMouseDown=function(){
	// console.log("onmousedown");
	var suggestion;
	
	addValueOn_inputField(getSuggestion(this));
	_cursorUpDownPressed = false ; 
	onBlurHandler();
	// VP 2/12/09 : ajout test sur _documentForm
	//MS : a priori, utilisé pour avoir un submit lors d'un click sur une des propositions de l'autocomplete, non souhaité chez nous.
	//if(_documentForm) _documentForm.submit()
};

// declenchee quand on passe sur une div de possibilite. La div pr�c�dente est passee en style normal
var divOnMouseOver=function(){
	// console.log("onmouseover");
  if(_highlightedSuggestionDiv) {
    setStylePourElement(_highlightedSuggestionDiv,"AutoCompleteDiv");
  }
  setStylePourElement(this,"AutoCompleteDivAct")
};

// declenchee quand la sourie quitte une div de possiblite. La div repasse a l'etat normal
var divOnMouseOut = function(){
	// console.log("onmouseout");
  setStylePourElement(this,"AutoCompleteDiv");
};



var addValueOn_inputField=function(sText){
	if(_quoteSuggestion){
		lastIndexTest = regexlast(_inputField.value,/"(\s)*(\,|\&)/g);
		if (lastIndexTest<0 || lastIndexTest == null){
			_inputField.value="\""+sText+"\"";
		}else{
			_inputField.value=_inputField.value.substring(0,lastIndexTest)+" \""+sText+"\"";
		}
	}else{
		lastIndexTest = regexlast(_inputField.value,/(\s)*(\,|\&)/g);
		if (lastIndexTest<0 || lastIndexTest == null){
			_inputField.value=sText;
		 }else{
			_inputField.value=_inputField.value.substring(0,lastIndexTest)+" "+sText;
		}
	}
}


