
// obj_refine_solr => array d'objet js qui représente chacun des critères d'affinage en cours d'utilisation
var obj_refine_solr;

$j(document).ready(function(){
	// sélection du bon formulaire
	form = getFormSolr();

	try{
		// récupération de l'objet depuis le champ refine du formulaire de recherche
		if(form != null && typeof form.refine != 'undefined' && form.refine.value!=""){
			obj_refine_solr = JSON.parse(form.refine.value);
		}else{
			obj_refine_solr=new Array();
		}
	}catch(e){
		console.log("obj_refine_solr - erreur recuperation refine courant ");
	}

	// génération du fil d'ariane des critères de refine
	if($j("#refine_crits_wrapper").is(":last-child")){
		tokenize_refine_crits();
	}else{
		tokenize_refine_crits(true);
	}
});

// sélection du formulaire sur lequel agir, à améliorer éventuellement
function getFormSolr(){
	if(document.getElementById('formRefine')!=null){
		form = document.formRefine;
	}else if(typeof $j != "undefined" && $j("form.facet_form").length>0){
		form = $j("form.facet_form").get(0);
	}else if(document.documentSelection){
		form = document.documentSelection;
	}else{
		form= document.getElementById("chercheForm");
	}
	return form ;
}



//Affinage via filterquery solr
function filterQueryByFacet(type,value,label_facet,text_value, reset_refine_value) {
	var refineE;
	var form;
	if(typeof text_value =="undefined"){
		text_value = "";
	}
	if(typeof reset_refine_value =="undefined"){
		reset_refine_value = 0;
	}
	// sélection du bon formulaire
	form = getFormSolr();
        

	if(typeof form != "undefined" && form != null){
		// création de l'objet refine
		tmp_obj = {
			"label_facet" : label_facet,
			"type" : type,
			"value" : value,
			"readable_value" : text_value
		};

		already_exist = false ;
		// on teste si ce critère de refine existe déjà (comparaison JSON, améliorable)
		for (i=0;i < obj_refine_solr.length;i++){
			if(JSON.stringify(obj_refine_solr[i]) == JSON.stringify(tmp_obj)){
				// si trouvé, flag already_exist
				already_exist = true ;
			}
		}
		// si n'existe pas,
		if(!already_exist){
			// on ajoute le critère de refine à l'objet refine solr
			obj_refine_solr.push(tmp_obj);
		}
		// update champ hidden refine
		form.refine.value = JSON.stringify(obj_refine_solr).replace('"','\"');

		// on désactive le flag qui réinitialise les paramètres de refine lors de la soumission d'une recherche
		// MS - 17.09.15 - modification des valeurs possibles de reset_refine : 
		// 0 -> on applique le refine, mais on ne recompose pas la requete SQL (pas de prepareSQL)
		// 1 -> on recompose la requete SQL (prepareSQL) mais on n'applique pas le refine
		// 2 -> on recompose la requete SQL ET on applique le refine 
		form.reset_refine.value=reset_refine_value;
		// on soumet le form
		form.submit();
	}
}

// fonction permettant de supprimer les
function removeFilter(n){
	// sélection du bon formulaire
	form = getFormSolr();
	obj_refine_solr.splice(n);
	
	form.refine.value=JSON.stringify(obj_refine_solr);
	// on désactive le flag qui réinitialise les paramètres de refine lors de la soumission d'une recherche
	form.reset_refine.value=0;
	// alert("removeFilter : "+form.refine.value);
	if (typeof updateNbLignesForm == 'function'){
		updateNbLignesForm();
	}
	form.submit();

}

// fonction qui génère & affiche la partie "refine" du fil d'ariane
function tokenize_refine_crits(leaf_with_link) {
	var chercheForm;

	if(typeof leaf_with_link =='undefined'){
		leaf_with_link = false ; 
	}
	
	holder=document.getElementById('refine_crits_wrapper');
	
	if(typeof holder =='undefined' || holder == null) return ;
	if (obj_refine_solr.length==0) return;
	if ($j(holder).children().length>0){
		// MS - 22.09.15 - si holder a déja des enfants, c'est qu'on a déja joué tokenize_refine_crits => on annule l'appel
		return false ; 
	}

	// pour chaque élément, on ajoute un div clickable qui représente le critère et donne la possibilité de le supprimer
	for (i=0;i < obj_refine_solr.length;i++) {
		myelt=document.createElement('div');
		// myelt.id=obj_refine_solr[i];

		myelt.className='refine_crits';
		// NB 09062015 - Si on est pas sur le dernier des critères du fil d'arianne, on a un lien pour le supprimer de la recherche
		if (i != (obj_refine_solr.length)-1 || leaf_with_link){
			if(obj_refine_solr[i]['type'] == 'doc_id_media'){
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span><a href='javascript:removeFilter("+(i+1)+")'>" + obj_refine_solr[i]['label_facet']+ "</a>";
			}else if(obj_refine_solr[i]['type'].indexOf('doc_date')!== -1  || obj_refine_solr[i]['type'].indexOf('duree_sec')!== -1 || obj_refine_solr[i]['type'].indexOf('hier_')!== -1 ){
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span><a href='javascript:removeFilter("+(i+1)+")'>" + obj_refine_solr[i]['label_facet']+' : '+obj_refine_solr[i]['readable_value']+ "</a>";
            }else if(obj_refine_solr[i]['type'] == 'doc_num'){
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span><a href='javascript:removeFilter("+(i+1)+")'>" + obj_refine_solr[i]['label_facet']+ "</a>";
			}else{
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span><a href='javascript:removeFilter("+(i+1)+")'>" + obj_refine_solr[i]['label_facet']+' : '+obj_refine_solr[i]['value'] + "</a>";
			}

		// NB 09062015 - Si on est sur le dernier des critères du fil d'arianne, on n'a pas de lien pour le supprimer de la recherche
		} else {
			if(obj_refine_solr[i]['type'] == 'doc_id_media'){
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span>" + obj_refine_solr[i]['label_facet'];
			}else if(obj_refine_solr[i]['type'].indexOf('doc_date')!== -1 || obj_refine_solr[i]['type'].indexOf('duree_sec')!== -1 || obj_refine_solr[i]['type'].indexOf('hier_')!== -1 ){
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span>" + obj_refine_solr[i]['label_facet']+' : '+obj_refine_solr[i]['readable_value'];
			}else if(obj_refine_solr[i]['type'] == 'doc_num'){
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span>" + obj_refine_solr[i]['label_facet'];
            }else{
				myelt.innerHTML="<span class=\"breadcrumb_arrow\"></span>" + obj_refine_solr[i]['label_facet']+' : '+obj_refine_solr[i]['value'];
			}
		}
		holder.appendChild(myelt);
	}
}
function clearRefine(){
	form = getFormSolr();
	try{
		form.refine.value = "" ;
	}catch(e){
		console.log(e);
	}
}

// fonction de réinitialisation du formulaire, permet de conserver les facettes solr
function reinitForm(id,submit_form){
	if(typeof submit_form == "undefined"){
		submit_form = true ;
	}

	arr_id_field_facet = [];
	arr_id_field_other = [];
	i = 0;
	j = 0;
	$j("#"+id).find("input[name^='chTypes'],select[name^='chTypes'],select[id^='selectType'],checkbox[name^='chTypes'],textarea[name^='chTypes']").each(function(idx,elt){
		if(typeof $j(elt).attr('value') != "undefined" && $j(elt).attr('value').indexOf('FACET') == 0){
			if($j(elt).attr('name').indexOf("[")!=false){
				arr_id_field_facet[i] = $j(elt).attr('name').split('[')[1].replace(']','');
			}else if ($j(elt).attr('name').indexOf("$")!=false){
				arr_id_field_facet[i] = $j(elt).attr('name').split('$')[1];
			}
			i++;
		}else{
			if(typeof $j(elt).attr('name') != "undefined" && $j(elt).attr('name').indexOf("[")!=false){
				arr_id_field_other[j] = $j(elt).attr('name').split('[')[1].replace(']','');
			}else if (typeof $j(elt).attr('id') !="undefined" &&  $j(elt).attr('id').indexOf("$")!=false){
				arr_id_field_other[j] = $j(elt).attr('id').split('$')[1];
			}

			j++;
		}
	});

	$j(arr_id_field_facet).each(function(idx,elt){
		$j("#"+id).find("*[name^='chVal'][name$='["+arr_id_field_facet[idx]+"]']").val("");
	});
	$j(arr_id_field_other).each(function(idx,elt){
		$j("#"+id).find("*[name^='ch'][name$='["+arr_id_field_other[idx]+"]']").val("");
	});


	$j("#"+id).find("input[name='refine']").attr('disabled','true');
	// console.log("reinitForm : "+$j("#"+id).find("input[name='refine']").val());
	$j("#"+id).find("input[name='nbLignes']").val(50);
	if(submit_form){
		$j("#"+id).submit();
	}
}

function rebondSolr(val,fld_solr,label,text_value){
	if(typeof fld_solr=="string")
		fld_solr = fld_solr.toLowerCase()

	reinitForm('quick_search',false );
	form = getFormSolr();

	form.refine.disabled=false;
	form.refine.value = "" ;
	obj_refine_solr = [];
	
	filterQueryByFacet(fld_solr,val,label,text_value,2);
}


// backward compatibility
function refineFacet(type,label,action){
	filterQueryByFacet(type,label,'');
}

function refineConcept(n){
	removeFilter(n);
}

