// Fonctions d'édition des paniers types dossiers Thématiques.
// elles sont utilisées dans menuPanier & dossier.xsl quand on est dans le cas des dossiers thématiques.
// gestion de l'arborescence des paniers etc ... + fonctions plus standard necessaires aux formulaire

function updateCheckedLines(){
	checkedLines=new Array();
	checkedItems=document.getElementsByTagName('input');
	for (i=0;i<checkedItems.length;i++) {
		if (checkedItems[i].type=='checkbox' && checkedItems[i].checked==true) {
			// ID_LIGNE_PANIER
			checkedLines[checkedLines.length]=checkedItems[i].value;
		}
	}
}


function addExtrait(ligne) {
	if(ligne!=""){

		// Clonage noeud
		blankDiv=document.getElementById('row$blank');
		myNewRow=blankDiv.cloneNode(true);
		myNewRow.id='row$cloned'; //chgt id car il ne faut qu'un seul row$blank
		if(document.getElementById("li_"+ligne)) document.getElementById("li_"+ligne).appendChild(myNewRow);
		else document.form1.appendChild(myNewRow);

		// Affectation valeurs
		_n=getChildById(myNewRow,'id_doc$');
		if (_n) _n.value=document.getElementById("DOC_"+ligne).value;
		_n=getChildById(myNewRow,'id_mat$');
		if (_n) _n.value=document.getElementById("MAT_"+ligne).value;
		_n=getChildById(myNewRow,'extrait$');
		if (_n) _n.value="1";
		_n=getChildById(myNewRow,'ext_titre$');
		//if (_n) _n.value='Sans titre';

		// Sauvegarde
		document.form1.commande.value="SAVE";
		submitForm();

	}
}

function addDoc(input,id,lib) {
	if(lib!=""){
		// Clonage noeud
		blankDiv=document.getElementById('row$blank');
		myNewRow=blankDiv.cloneNode(true);
		myNewRow.id='row$cloned'; //chgt id car il ne faut qu'un seul row$blank
		document.form1.appendChild(myNewRow);

		// Affectation valeurs
		_n=getChildById(myNewRow,'id_doc$');
		if (_n) _n.value=lib;
		_n=getChildById(myNewRow,'extrait$');
		if (_n) _n.value="0";

		// Sauvegarde
		document.form1.commande.value="SAVE";
		submitForm();
	}
}

function addUsager(input,id,lib) {
	if(lib!=""){
		document.getElementById("USAGER").value=id;
		document.getElementById("USAGER_IDX").value=lib;
		// Sauvegarde
		document.form1.commande.value="SAVE";
		submitForm();
	}
}


function moveTo(elt,ligne) {
	if(ligne > 0){
		div=elt.parentNode;
		nxt=div.firstChild;
		n=div.childNodes.length;
		if(ligne < 1) ligne=1;
		if(ligne > n) ligne=n;
		if(ligne==1) div.insertBefore(elt,nxt);
		else{
			offset=0;
			if(nxt==elt) offset=1;
			for (i=1;i<ligne;i++) {
				nxt=nxt.nextSibling;
				if(nxt==elt) offset=1;
			}
			if(offset==1) div.insertBefore(elt,nxt.nextSibling);
			else div.insertBefore(elt,nxt);
		}
	}
}

function updateEtatLignes(val){
	inputItems=document.getElementsByTagName('input');
	for (x=0;x<inputItems.length;x++) {
		if (inputItems[x].name.indexOf("PDOC_ID_ETAT")>0) {
			inputItems[x].value=val;
		}
	}

}

function updateEtatPanier(){
	var etat_pan=5;
	selectItems=document.getElementsByTagName('select');
	for (x=0;x<selectItems.length;x++) {
		if (selectItems[x].id.indexOf("ETAT")==0) {
			etat_pan=Math.min(selectItems[x].value,etat_pan);
		}
	}
	document.form1.pan_id_etat.value=etat_pan;
}




function saveLine(ligne){
	// Mise à jour ligne panier
	if(document.getElementById("action_"+ligne)){
		document.getElementById("action_"+ligne).value='edit';
		// Sauvegarde t_panier_doc
		savePanierDoc();
		document.getElementById("action_"+ligne).value='';
	}
}

function moveDossierCallBack(str) {
	myDom=importXML(str);
	myBalise=myDom.getElementsByTagName('msg_out');
	showAlert(myBalise[0].firstChild.nodeValue,'alertBox','slideDown',200,0); //ne PAS oublier le firstChild
	refreshFolders();
	moveSelectionDossier('','',-1); //réinit dossiers
	moveSelectionDossierParent('','',-1); //réinit dossiers
}

function selectUser(input,lib,id) {

	if (id>=0) {
		document.form1.new_user_id.value=id;
		document.getElementById('new_user_nom').innerHTML=lib;
		document.getElementById('confirm_user_change').style.display='inline';
		document.getElementById('cancel_user_change').style.display='inline';
	}  else
	{
		document.form1.new_user_id.value='';
		document.getElementById('new_user_nom').innerHTML='...';
		document.getElementById('confirm_user_change').style.display='none';
		document.getElementById('cancel_user_change').style.display='none';
	}
	document.getElementById('chooser').style.display='none';
}



function pasteLines() {
	//document.form1.commande.value="COLLER_LIGNES";
	$j("form[name='form1'] input[name='commande']").val("COLLER_LIGNES");
	submitForm();
}

function addDocByCote(id) {
	if(id!=""){
		return !sendData('GET','ajax.php','xmlhttp=1&commande=getDocByCote&doc_cote='+id,'rtnDocByCote');
	}
}

//update VG 14/04/2010 : ajout d'une valeur de retour

function addDocIfEnter(e){
	var keynum;
	var retour = true;

	if(window.event) keynum = e.keyCode; // IE
	else if(e.which) keynum = e.which;// Netscape/Firefox/Opera

		if (keynum==13)
		{
			addDocByCote(document.form1.DOC_COTE.value);
			retour = false;
		}
		e.cancelBubble = true;
		if (e.stopPropagation) e.stopPropagation();

	return retour;
}

function checkDocIfEnter(e){
	var keynum;
	if(window.event) keynum = e.keyCode; // IE
	else if(e.which) keynum = e.which;// Netscape/Firefox/Opera
		if (keynum==13)  checkDocByCote(document.form1.DOC_COTE.value);
		e.cancelBubble = true;
		if (e.stopPropagation) e.stopPropagation();
}

function updateCheckedMats(){
	updateCheckedLines();
	if(checkedLines.length==0){
		alert('<?=addslashes(kMsgNoSelectLignes)?>');
		return;
	}
	var checkedMats="";
	inputItems=document.getElementsByTagName('input');
	for (i=0;i<inputItems.length;i++) {
		if (inputItems[i].type=='checkbox') {
			checked=inputItems[i].checked;
		}else if(inputItems[i].name=='DOC_COTE[]' && checked==true){
			checkedMats=checkedMats+","+inputItems[i].value;
			checked=false;
		}
	}
	return checkedMats.substring(1);
}

function sortirMat(){
	selectAll(true);
	ids=updateCheckedMats();
	updateEtatLignes(4);
	document.getElementById("pan_id_etat").value=4;
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=updateMateriel&commande=update&MAT_SORTI=1&id_mat='+ids,'updateMaterielCallBack');
}

function rangerMat(){
	selectAll(true);
	ids=updateCheckedMats();
	updateEtatLignes(5);
	document.getElementById("pan_id_etat").value=5;
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=updateMateriel&commande=update&MAT_SORTI=0&id_mat='+ids,'updateMaterielCallBack');
}

function stockerMat(){
	selectAll(true);
	ids=updateCheckedMats();
	updateEtatLignes(5);
	document.getElementById("pan_id_etat").value=5;
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=updateMateriel&commande=update&V_LIEU=Paris&id_mat='+ids,'updateMaterielCallBack');
}

function updateMaterielCallBack(str) {
	// Retour updateMateriel()
	//alert(str);
	document.form1.commande.value="SAVE";
	submitForm();

}

function updateEtatPanier(){
	var etat_pan=5;
	selectItems=document.getElementsByTagName('select');
	for (x=0;x<selectItems.length;x++) {
		if (selectItems[x].name.indexOf("PDOC_ID_ETAT")>0) {
			etat_pan=Math.min(selectItems[x].value,etat_pan);
		}
	}
	document.form1.pan_id_etat.value=etat_pan;
}

function updateEtatLignes(val){
	selectItems=document.getElementsByTagName('select');
	for (x=0;x<selectItems.length;x++) {
		if (selectItems[x].name.indexOf("PDOC_ID_ETAT")>0) {
			selectItems[x].value=val;
		}
	}

}

function submitForm() {
	//if(chkFormFields()) document.form1.submit();
	if(chkFormFields()) $j("form[name='form1']").get(0).submit();
}

function ddToggle(id){
	if(!document.getElementById(id + '-ddcontent').style.opacity) document.getElementById(id + '-ddcontent').style.opacity=0;
	if(document.getElementById(id + '-ddcontent').style.opacity < 0.5) {
		var items=new Array('one','two','three','four','five');
		for (i=0; i<items.length ; i++) {
			if(id!=items[i] && document.getElementById(items[i] + '-ddcontent')) ddMenu(items[i],-1,document.getElementById('chooser').style.display);
		}
		ddMenu(id,1,document.getElementById('chooser').style.display);
	}
	else ddMenu(id,-1,document.getElementById('chooser').style.display)
}

function renameCallBack(str) {
	myDom=importXML(str);
	//alert(str);
	myBalise=myDom.getElementsByTagName('msg_out');
	showAlert(myBalise[0].firstChild.nodeValue,'alertBox','slideDown',200,0); //ne PAS oublier le firstChild
	refreshFolders();
	renameSelection('','',-1); //réinit nommage
}

function creaSelection(input,lib,id) {
	if (id>=0) {
		document.form2.new_dossier_id.value=id;
		document.getElementById('dossier_pere').innerHTML=lib;

	}  else
	{
		document.form2.new_dossier_id.value='';
		document.getElementById('dossier_pere').innerHTML='...';
		document.getElementById('addFolder').style.display='none';
	}
	document.getElementById('chooser').style.display='none';
}

function renameSelection(input,lib,id) {
	if (id>=0) {
		document.form1.id_panier_rename.value=id;
		document.form1.pan_titre_rename.value=lib;
		document.form1.pan_titre_rename.disabled=false;
		document.getElementById('confirm_rename').style.display='inline';
		document.getElementById('cancel_rename').style.display='inline';
	}  else
	{
		document.form1.id_panier_rename.value='';
		document.form1.pan_titre_rename.value='';
		document.form1.pan_titre_rename.disabled=true;
		document.getElementById('confirm_rename').style.display='none';
		document.getElementById('cancel_rename').style.display='none';
	}
	document.getElementById('chooser').style.display='none';
}

function dupliquer() {
	if (!confirm(str_lang.kConfirmPanierDupliquer))
		return;
	
	document.form1.commande.value="DUPLIQUER";
	submitForm();
}

function setDossier(input,lib,id) {
	if (id==document.form1.id_panier.value)
	{
		alert(str_lang.kErrorPanierDeplacerDossierInterdit);
		return;
	}
	
	if (id==document.form1.pan_id_gen.value)
	{
		alert(str_lang.kErrorPanierDeplacerDejaParent);
		return;
	}
	
	if (!confirm(str_lang.kConfirmPanierDeplacer+' "'+lib+'"?')) 
		return;

	document.form1.pan_id_gen.value=id;
	document.form1.commande.value='SAVE';
	submitForm();
}

function renameSubmit() {

	idCart=document.form1.id_panier_rename.value;
	newName=document.form1.pan_titre_rename.value;
	if (newName.Trim()=='') {alert(str_lang.kErrorPanierRenameVide);return;}
	if (!confirm(str_lang.kConfirmPanierRename+newName+' ?')) return;
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&commande=save&field=PAN_TITRE&value='+encodeURIComponent(newName),'renameCallBack');

}

function moveSelectionDossier(input,lib,id) {
	if (id>=0) {
		if (id==document.form1.move_pan_id_gen.value) {alert(str_lang.kErrorPanierMoveSame);return;}
		document.form1.move_id_panier.value=id;
		document.getElementById('move_dossier').innerHTML=lib;
		if (document.form1.move_pan_id_gen.value!='') {
			document.getElementById('confirm_move_dossier').style.display='inline';
			document.getElementById('cancel_move_dossier').style.display='inline'; }
	}  else
	{
		document.form1.new_user_id.value='';
		document.getElementById('move_dossier').innerHTML='...';
		document.getElementById('confirm_move_dossier').style.display='none';
		document.getElementById('cancel_move_dossier').style.display='none';
	}
	document.getElementById('chooser').style.display='none';
}

function moveSelectionDossierParent(input,lib,id) {
	if (id>=0) {
		if (id==document.form1.move_id_panier.value) {alert(str_lang.kErrorPanierMoveSame);return;}
		document.form1.move_pan_id_gen.value=id;
		document.getElementById('move_dossier_pere').innerHTML=lib;
		if (document.form1.move_id_panier.value!='') {
			document.getElementById('confirm_move_dossier').style.display='inline';
			document.getElementById('cancel_move_dossier').style.display='inline'; }
	}  else
	{
		document.form1.new_user_id.value='';
		document.getElementById('move_dossier_pere').innerHTML='...';
		document.getElementById('confirm_move_dossier').style.display='none';
		document.getElementById('cancel_move_dossier').style.display='none';
	}
	document.getElementById('chooser').style.display='none';
}

function changeUserSubmit() {
	if (document.form1.new_user_id.value=='') {alert(str_lang.kErrorPanierTransmettreNoUsager);return;}

	if (!confirm(str_lang.kConfirmPanierTransmettre+' "'+document.getElementById('new_user_nom').innerHTML+ '" ?')) return;
	//Transfert des valeurs du mini formulaire vers le vrai formulaire
	document.form1.pan_id_usager.value=document.form1.new_user_id.value;
	document.form1.pan_id_gen.value=0; //il faut remettre à 0 la hiérarchie
	document.form1.commande.value='CHANGE_USER';
	submitForm();

}

function moveDossierSubmit() {

	idCart=document.form1.move_id_panier.value;
	newFather=document.form1.move_pan_id_gen.value;
	if (!confirm(str_lang.kConfirmPanierMove)) return;
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&commande=save&field=PAN_ID_GEN&value='+encodeURIComponent(newFather),'moveDossierCallBack');

}

function supprDossier(input,lib,id) {
	if (!confirm(str_lang.kConfirmPanierSupprimer+' "'+lib+ '" ?')) return;
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+id+'&commande=delete','supprDossierCallBack');
}
function supprDossierCallBack(str) {
	window.location.reload(); //comme cette action peut tout chambouler, on préfère recharger la page.
}

function suppression(what,refer) {
	if (!confirm(str_lang.kConfirmJSDocSuppression))
		return;
	if(refer) document.form1.refer.value=refer;
	document.form1.commande.value=what;
	document.form1.submit();
}

function dupliquerTravail() {
	if (!confirm(str_lang.kJSConfirmPanierTransformer)) return;
	document.form1.pan_id_etat.value="2";
	document.form1.commande.value="DUPLIQUER";
	submitForm();
}

function dupliquerThema() {
	if (!confirm(str_lang.kJSConfirmPanierTransformerThema)) return;
	document.form1.pan_public.value="1";
	document.form1.pan_id_gen.value="0";
	document.form1.xsl.value="dossier";
	document.form1.commande.value="DUPLIQUER";
	submitForm();
}

function chkFormFields (myform) {
	msg='';
	var i;
	//arrElts=document.form1.getElementsByTagName('*');
	arrElts=$j("form[name='form1']").get(0).getElementsByTagName('*');
	for (i=0;i < arrElts.length;i++) {
		if (!arrElts[i].getAttribute('mandatory')) continue; //test si obligatoire

		if (arrElts[i].value.Trim()=='') {
		switch(arrElts[i].getAttribute('label')){
			default :
				lib=arrElts[i].getAttribute('label');
				break;
			}
			msg=msg+' '+lib;
			arrElts[i].className='errorjs';
		} else {
			arrElts[i].className='';
		}
	}
	if (msg!='') {alert(str_lang.kJSErrorOblig+msg);return false; }
	return true;
}

function copyLines() {
	updateCheckedLines();
	if(checkedLines.length==0){
		alert(str_lang.kMsgNoSelectLignes);
		return;
	}
	lignes=Object.toJSON(checkedLines);
	//id=document.form1.id_panier.value;
	id=$j("form[name='form1'] input[name='id_panier']").val();
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+id+'&commande=copyLines&lignes='+lignes,'copyLinesCallBack');
}

function rtnDocByCote(str) {
	//alert(str);
	myXML=importXML(str);
	_xmlFlds=myXML.getElementsByTagName('ID_DOC');
	if(_xmlFlds[0]){
		_xmlFld=_xmlFlds[0].firstChild;
		addDoc('','',_xmlFld.nodeValue);
	}else {
		alert(str_lang.kJSReferenceNonTrouvee);
	}
}

function checkDocByCote(id) {
	if(id!=""){
		id=id.toUpperCase();
		notfound=true;
		inputItems=document.getElementsByTagName('input');
		for (i=0;i<inputItems.length;i++) {
			if (inputItems[i].type=='checkbox') {
				checkObj=inputItems[i];
			}else if(inputItems[i].name=='DOC_COTE[]' && inputItems[i].value.toUpperCase()==id){
				checkObj.checked=true;
				notfound=false;
			}
		}
		if(notfound) alert(str_lang.kJSReferenceNonTrouvee);
	}
}

function creaSubmit() {
	//if (document.form2.new_dossier_id.value=='') {alert('<?=addslashes(kErrorPanierCreationNoParent)?>');return;}
	var form2 = $j("form[name='form2']").get(0);
	if (form2.new_dossier_titre.value.Trim()=='') {alert(str_lang.kErrorPanierCreationNoName);return;}

	if (!confirm(str_lang.kConfirmPanierCreation1+" "+str_lang.kSelection+' "'+form2.new_dossier_titre.value+'" '+str_lang.kConfirmPanierCreation2+' "'+document.getElementById('dossier_pere').innerHTML+ '"?')) return;
	//Transfert des valeurs du mini formulaire vers le vrai formulaire
	form2.pan_id_gen.value=form2.new_dossier_id.value;
	form2.pan_dossier.value=form2.new_dossier_type.value;
	form2.pan_titre.value=form2.new_dossier_titre.value;
	form2.pan_id_type_commande.value=0;
	form2.commande.value='CREER';
	//submitForm();
	form2.submit();
}


// ============ fonctions utilisées dans dossier.xsl 


// dossier.xsl : fonction commentée => inutile ? 
function resetVignette() {
	document.form1.pan_id_doc_acc.value='';
	document.getElementById('vignette').src='design/images/nopicture.gif';
}
	
// dossier.xsl	
function copyInTitle(from) {
	if(document.form1.dossier_ordre.value=='') document.form1.pan_titre.value=from.value;
	else document.form1.pan_titre.value="["+document.form1.dossier_ordre.value+"] "+from.value;
}

// dossier.xsl : fonction commentée => inutile ? 
function initOrdre() {
	var titre=document.form1.pan_titre.value;
	var ordre="";
	if(titre.indexOf("[")==0){
		x=titre.indexOf("]");
		if(x>0) ordre=titre.substr(1,x-1);
	}
  document.form1.dossier_ordre.value=ordre;
}

// dossier.xsl
// calcHeight avait deux versions ... 
// function calcHeight(the_iframe) {
	// var height = document.documentElement.clientHeight!=0?document.documentElement.clientHeight:document.body.clientHeight;
	// height -= the_iframe.offsetTop;
	////not sure how to get this dynamically
	////height= height/2; // whatever you set your body bottom margin/padding to be
	// the_iframe.style.height = height +"px";
	// the_iframe.style.height="500px"; //on resize à 500. Il faut laisser la ligne au-dessus sinon bug IE
	////frames['folders2'].document.body.style.backgroundColor="#5E4036";
// }

function calcHeight(the_iframe) {
	var height = document.documentElement.clientHeight!=0?document.documentElement.clientHeight:document.body.clientHeight;
	height -= the_iframe.offsetTop;
	//alert(height);
	// not sure how to get this dynamically
	height= height/2; // whatever you set your body bottom margin/padding to be
	the_iframe.style.height = height +"px";
}



// aucun appel trouvé dans dossier.xsl...
function confirmSuppress() {
	if (confirm(str_lang.kConfirmerSuppression)) {
	document.form1.commande.value='SUP';
	document.form1.submit();
	}
}

// dossier.xsl
function refresh() {
	var u;
	var errors=0;
	var form1 = document.getElementById("form1");
	if (!form1) form1 = document.getElementsByName("form1")[0];
	
	for (var u=0;u<form1.elements.length;u++) {
		myelt=form1.elements[u];
		if (typeof(myelt)!='object') continue;
		//alert(myelt.name);
		if (!myelt.getAttribute('mandatory')) continue;
		if (myelt.value.Trim()=='') {
			myelt.className='errorjs';errors++;
		}
		else myelt.className='';
	}
	if (errors>0) {showAlert(str_lang.kJSErrorOblig,'alertBox');}
	else {
	form1.commande.value="SAVE";
	form1.submit();
	}
}

// dossier.xsl
function setDossier(input,lib,id) {
	if (id==document.form1.id_panier.value) {alert (str_lang.kErrorPanierDeplacerDossierInterdit);return;}
	if (id==document.form1.pan_id_gen.value) {alert (str_lang.kErrorPanierDeplacerDejaParent);return;}
	if (!confirm(str_lang.kConfirmPanierDeplacer+' "'+lib+ '"?')) return;

	document.form1.pan_id_gen.value=id;
	document.form1.commande.value='SAVE';
	submitForm();
}

// dossier.xsl
function refreshFolders2(id,langue) {
	frm=document.getElementById('folders2');
	
	if (frm) {
		frm.src='blank.php?urlaction=loadPanier&action=expand&imgNode=folder.gif&display=hierarchique&includePublic=2&includeRoot=1&id='+id+'&id_lang='+langue+"&excludePanier=1";
		calcHeight(frm);
	}
	panierCount=document.getElementById('panier_count');
	if (panierCount) {
		return !sendData('GET','blank.php','xmlhttp=1&urlaction=loadPanier&includePublic=2&includeRoot=1&imgNode=folder.gif&display=none&excludePanier=1','updatePanierCount');
			}

}






