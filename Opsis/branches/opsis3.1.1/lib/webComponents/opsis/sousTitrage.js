
$j(document).ready(function () {
				   
				   
				   $j("a").click(function () {
						 //					console.log('askSaveBeforeLeave');
						 if (!$j(this).hasClass('NOaskSaveBeforeLeave')) {
						 //						console.log('askSaveBeforeLeaveIN');
						 
						 //						askSaveBeforeLeave(id_doc_int);
						 }
						 });
				   
				   
				   $j("#dialog_loading").dialog({autoOpen: false, modal: true, maxWidth: 320, minWidth: 320});
				   $j("#dialog_alert").dialog({autoOpen: false,
						  modal: true,
						  maxWidth: 320,
						  minWidth: 320,
						  buttons: {
						  "OK": function () {
						  $j(this).dialog('close');
						  }
						  }
						  
						  
						  });
				   
				   
				   
				   $j("#importSubtitle").click(function () {
					   //					askSaveBeforeLeave(id_doc_int);
					   popupWindow('indexPopupUpload.php?urlaction=simpleUploadJS&type=doc_mat&id_doc='+id_doc_int+'&media_type=subtitle&page=docSaisie&dmat_inactif=1', '','height=400,width=830,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main');
					   });
				   
				   
				   $j("#generateTranscription button").click(function () {
															 
						 
						 // check if element exists
						 if ($j("#error_transcription").length) {
						 $j("#error_transcription").remove();
						 }
						 
						 
						 var vgt = $j("#generateTranscription select option:selected").val().Trim();
						 //                        console.log(typeof(vgt));
						 if (vgt != '') {
						 var tab_option = vgt.split('|');
						 //                            console.log('tab_option[0] |'+tab_option[0]+'|');
						 //                            console.log('tab_option[1] |'+tab_option[1]+'|');
						 if (tab_option[1].Trim() == '') {
						 $j("#dialog_alert").html(str_lang.kSousTitrage_transcriptionLangMissing);
						 $j("#dialog_alert").dialog('open');
						 } else {
						 var dialogTrans;
						 $j.get("empty.php?urlaction=subtitle&isMatAlreadyTranscripted&id_mat=" + tab_option[0], function (data) {
								if (data == 'true') {
								dialogTrans = str_lang.kSousTitrage_transcription_confirmAlreadyExist;
								} else {
								dialogTrans = str_lang.kSousTitrage_transcriptionConfirm;
								}
								}).done(function () {
										if (confirm(dialogTrans)) {
										generateTranscription(tab_option[0], tab_option[1]);
										}
										});
						 
						 
						 }
						 }
						 });
				   
				   
				   $j("#actionFORMSubtitle").submit(function (event) {
													
						if ($j("#publishValue").val() == 'unpublished') {
						
						event.preventDefault();
						$j.get("empty.php?urlaction=subtitle&id_doc=" + id_doc_int +"&publish_isExistsOtherSubtitlePUBLISHEDInThisLanguage", function (data) {
							   console.log('publish_isExistsOtherSubtitlePUBLISHEDInThisLanguage ' + data);
							   
							   if (data == 'false') {
							   $j("#actionFORMSubtitle").unbind("submit");//pour éviter boucle infinie
							   $j("#actionFORMSubtitle").submit();
							   
							   }
							   else if (data == 'error') {
							   alert(str_lang.kSousTitrage_publishTechnicalError);
							   } else {
							   alert(str_lang.kSousTitrage_publishUnpublishOtherFirst);
							   }
							   
							   
							   
							   });
						}
						
						
						});
				   
				   $j("#listAllSubtitledFilesLINK").click(function () {
						  toggleVisibility($j("#listAllSubtitledFiles"), $j("#listAllSubtitledFilesLINK img"));
						  });
				   
				   
				   //Récuperation dynamique de .height() ne marche PAS, a priori car le code html est récupéré via du ajax
				   //                        var ligneSubtitle_height = $j("tr[id^='subTitleLine']").first().height();
				   //                        console.log('ligneSubtitle_height ' + ligneSubtitle_height);
				   
				   throw_cycleHighLightCurrentSubtitle();
				   
				   
				   subTitling_defaultTrackToAdd = true;
				   $j("#media_space #container").click(function () {
					   //Attention, on ne doit pas remplacer le track (car actionSubtitle(ancienne valeur id_mat car page non rechargé) à chaque fois qu'on clique sur le player
					   if (subTitling_defaultTrackToAdd) {
					   checkVideoPlayerReadyBeforeAddTMPtrack();
					   }
					   
					   
					   });
				   
				   
				   
				   $j("#soustitrage_test").click(function () {
						 //                            var position = $j("tr#subTitleLine00\\:00\\:18\\.000").offset();
						 //                            console.log('top ' + position.top);
						 //                            console.log('typeof top ' + typeof (position.top));
						 
						 //                        var ligneSubtitle_height = $j("tr[id^='subTitleLine']").first().height();
						 
						 //                        console.log('ligneSubtitle_height ' + ligneSubtitle_height);
						 //                        goToTimeCodeInPlayer('00:00:19.479');
						 //						isScrolledIntoView($j("tr#subTitleLine00\\:00\\:12\\.290"), $j("#currentSubtitle"));
						 
						 //                            console.log('scrollTop ' + $j("#currentSubtitle").scrollTop());
						 //                            $j("#currentSubtitle").scrollTop(300);
						 $j("#dialog_loading").dialog('open');
						 //
						 //
						 //            $j("#subtitles-EN1").click();
						 //            $j("#ow_bouton_srt ul").hide();
						 
						 //                            clearTrack();
						});
				   
				   
				   
				   
				   $j("#saveSubtitle").click(function () {
											 
						 if (confirm(str_lang.kSousTitrage_saveFileWarning)) {
							 //si on publie le matériel 1 et qu'on switch sur le matériel 2 (on rafraîchis pas la page) donc il faut savoir le mat de sous-titre qu'on est en train d"éditer et regarder si il est dans $tab_mat_published (quand on publie, on rafraichis la page, donc $tab_mat_published est toujours à jour)
							 $j.get("empty.php?urlaction=subtitle&getActiveMat&id_doc="+id_doc_int, function (data1) {
									}).done(function (data1) {
											//console.log('data1 :'+data1);
											
											console.log('inarray' + jQuery.inArray(parseInt(data1), tab_mat_published));
											
											$j.get("empty.php?urlaction=subtitle&id_mat=0&id_doc=" + id_doc_int + "&ask_id_mat=1&info_isModified", function (data) {
												   //attention à bien mettre la sauvegarde du fichier APRES la detection de modif sinon on peut pas detecter les modifs
												   actionSubtitle(0, id_doc_int, 'save'); //Attention on envoie id_mat =0, car si on vient juste de changer de materiel, comme on rafraichis PAS la page, echo $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc_int]['id_mat'] va contenir l'ancienne valeur
												   
												   
												   if (jQuery.inArray(parseInt(data1), tab_mat_published) !== -1) {
												   
												   
												   //										console.log('ismodified'+data);
												   if (data !== 'false') {
												   if(tab_mat_diff=='[]'){
												   alert(str_lang.kSousTitrage_subtitledVersionGenNoMatAfterModif);
												   }
												   } else {
												   console.log('PAS generateDiffMatIncrustedWithSubtitle car pas de modif');
												   }
												   
												   } else {
												   console.log('PAS generateDiffMatIncrustedWithSubtitle car non publié');
												   }
												   
												   
												   });//info_isModified
											
											
											});
							 
							 
							 }
							 
							 });
											 
				 $j("#exportSubtitle").click(function () {
					 actionSubtitle(0, id_doc_int, 'info_isModified'); //Attention on envoie id_mat =0, car si on vient juste de changer de materiel, comme on rafraichis PAS la page, echo $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc_int]['id_mat'] va contenir l'ancienne valeur
					 });
				 $j("#downloadSubtitle").click(function () {
					 actionSubtitle(0, id_doc_int, 'download_file'); //Attention on envoie id_mat =0, car si on vient juste de changer de materiel, comme on rafraichis PAS la page, echo $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc_int]['id_mat'] va contenir l'ancienne valeur
				 });
				 $j("#restoreSubtitle").click(function () {
					 $j.get("empty.php?urlaction=subtitle&getActiveMat&id_doc=" + id_doc_int, function (data) {
						console.log("restore init data",data);
						actionSubtitle(data,id_doc_int,'restore'); 
					 });
				 });
				 
				 if (id_mat_session>0) {
				 
				 actionSubtitle(0, id_doc_int);//Attention on envoie id_mat =0, car si on vient juste de changer de materiel, comme on rafraichis PAS la page, echo $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc_int]['id_mat'] va contenir l'ancienne valeur
				 }
											 
		});//            $j(document).ready(function () {



function askSaveBeforeLeave(id_doc) {
	//Pas besoin d'aller chercher le matActive, info_isModified va déja chercher si on lui fourni l'id doc
	//					$j.get("empty.php?urlaction=subtitle&getActiveMat&id_doc=" + id_doc, function (data) {
	//						1. si on essaye d'afficher un sous-titre différent que celui actif
	//							console.log('sub différent');
	//	1.2 on regarde si il y a eu des modifs sur le sous-titre actif
	$j.get("empty.php?urlaction=subtitle&info_isModified&id_doc=" + id_doc, function (data) {
		   
					if (data == 'true') {
		   //									console.log("y'a eu des modifications sur le sous-titre actif");
		   //	1.2.1 on demande au client si il veut sauvegarder le sous-titre actif
		   if (confirm(str_lang.kSousTitrage_saveCurrentWarning)) {
		   actionSubtitle(0, id_doc, 'save');
		   } else {
		   
		   
		   //								setTimeout(function () {
		   //									$j.get("empty.php?urlaction=subtitle&restore&id_doc=" + id_doc, function (data3) {
		   //	//								actionSubtitle(data, id_doc, 'show');
		   //									});
		   //								}, 1000);
		   
		   $j.get("empty.php?urlaction=subtitle&restore&id_doc=" + id_doc, function (data3) {
				  //								actionSubtitle(data, id_doc, 'show');
				  });
		   
		   
		   }
		   //							1.2.1.1 on affiche le sous-titre demandé
					}
		   
		   });
	
	
	
}

var activeMat = null ;


function askSaveThenShowOther(id_mat, id_doc) {
	$j.get("empty.php?urlaction=subtitle&getActiveMat&id_doc=" + id_doc, function (data) {
					//						1. si on essaye d'afficher un sous-titre différent que celui actif
					if (data != id_mat) {
		   //							console.log('sub différent');
		   //	1.2 on regarde si il y a eu des modifs sur le sous-titre actif
		   $j.get("empty.php?urlaction=subtitle&info_isModified&id_doc=" + id_doc, function (data2) {
				  
				  if (data2 == 'true') {
				  //									console.log("y'a eu des modifications sur le sous-titre actif");
				  //	1.2.1 on demande au client si il veut sauvegarder le sous-titre actif
				  if (confirm(str_lang.kSousTitrage_saveCurrentWarning)) {
				  actionSubtitle(data, id_doc, 'save');
				  } else {
				  console.log('annuler');
				  //										actionSubtitle(data, id_doc, 'restore');
				  //on ne passe pas par actionSubtitle(data, id_doc, 'restore');, car cela reaffiche le fichier qu'on a restauré au lieu du showOther
				  //										actionSubtitle(data, id_doc, 'restoreWithoutShow');
				  ///:ci dessous ne marche pas
				  //										$j.get("empty.php?urlaction=subtitle&restore&id_doc=" + id_doc + "&id_mat=" + data, function (data3) {
				  
				  
				  // Attention, j'ai été obligé de mettre 1 setTimeout car sinon pb aléatoire ou rien n'est modifié sur par ex :
				  //http://ch-ops-dev.opsomai.lan/dev/babou/bycn3.0.2/www/empty.php?urlaction=subtitle&testSession&id_doc=3754&id_mat=7424
				  setTimeout(function () {
							 $j.get("empty.php?urlaction=subtitle&restore&id_doc=" + id_doc + "&id_mat=" + data, function (data3) {
									//								actionSubtitle(data, id_doc, 'show');
									
									//
									});
							 }, 1000);
				  }
				  
				  }
				  //									1.2.1.1 on affiche le sous-titre demandé
				  //								} else {
				  //									//										1.2.3 on affiche le sous-titre demandé
				  //									//									console.log("y'a PAS eu des modifications sur le sous-titre actif");
				  //									actionSubtitle(id_mat, id_doc, 'show');
				  //								}
				  
				  actionSubtitle(id_mat, id_doc, 'show', null, null, true);
				  activeMat = id_mat ; 
				  
				  
						});
		   
					} else {
		   
		   actionSubtitle(id_mat, id_doc, 'show', null, null, true);
		   //							console.log('sub pareil');
					}
		   });
	
}




function generateTranscription(id_mat, lang) {
	var id =id_doc_int;
	if (id != "" && lang != "") {
		var url = "simple.php?include=transcription&id_doc=" + id + "&lang=" + lang + "&id_mat=" + id_mat;
		$j.ajax({
				url: url // La ressource ciblée
				});
		alert(str_lang.kSousTitrage_transcription_processed);
		window.location.reload();
		// popupWindow('simple.php?urlaction=jobListe.php', 'jobs', 'height=400,width=650,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1', 'main');
	}
}



function updateProgressJob(id_job_ligne, redirect_url) {
	$j.ajax({
			type: "POST",
			url: "empty.php?urlaction=processJobProgression&progressionbar&getState=1",
			data: {id_job: id_job_ligne},
			success: function (data) {
			var reg = new RegExp("[:]+", "g");
			var aData = data.split(reg);
			valeur = parseInt(aData[0].replace('%', ''));
			iEtatJob = parseInt(aData[1]);
			sEtatJob = aData[2].replace(/[^\w\d\s]/, '');
			$j("#job_progress_authot").replaceWith('<span id="job_progress_authot" width="50" style="margin-left:30px;">' + sEtatJob + ' : ' + job_nom + '<div id="progressbar" class="beginning" style="margin-left:10px;width:50px;height:16px;display:inline-block;"><span style="position:absolute;margin-left:10px; margin-top:0px;">' + valeur + '%' + '</span></div></span>');
			if (iEtatJob < ow_const.jobAnnule) {
			setTimeout("updateProgressJob(" + id_job_ligne + ",'" + redirect_url + "');", 5000);
			$j(function () {
			   $j("#progressbar").progressbar({
											  value: valeur
											  });
			   });
			} else {
			console.log('action : etatJob');
			$j("#job_progress_authot").after().html(sEtatJob);
			if (iEtatJob == ow_const.jobErreur) {
			//showYoutubeError();
			}
			}
			if (redirect_url != '') {
			if (valeur == '100' && iEtatJob==ow_const.jobFini ) {
			
			
			// en fait, la transcription peut durer très longtemps, et le client peut se déconnecter, donc il vaut mieux que cela soit le frontal qui envoie le mail
			//								$j.get("empty.php?urlaction=subtitle&mailTranscriptionFinished&id_job=" + id_job_ligne, function (data) {
			//								}).done(function () {
			document.location.href = redirect_url;
			//								});
			
			}
			}
			}
			});
}



// SUITE ///
$j(document).ready(function () {
				   
				   
				   
				   
				   }); //$j(document).ready(function () {




function generateDiffMatIncrustedWithSubtitle(tab_id_mat, id_mat_subtitle) {
	
	var str_id_mat = '';
	$j.each(tab_id_mat, function (key, value) {
			str_id_mat += '&tab_id_mat[]=' + value;
			//						str_id_mat = '&tab_id_mat[]=' + value;
			});
	
	
	$j.get("empty.php?urlaction=subtitle&job_generateMatDiffSubtitled&id_doc="+id_doc_int+"&id_mat_subtitle=" + id_mat_subtitle + str_id_mat, function (data) {
		   
		   var r = JSON.parse(data);
		   //						console.log('r ' + r);
		   
		   if (!(typeof r.OK === 'undefined' || r.OK === null)) {
		   var msg = str_lang.kSousTitrage_generateDiffMatIncrustedWithSubtitleSuccess;
		   alert(msg.replace('[nom]', r.OK));
		   
		   } else {
		   alert(str_lang.kSousTitrage_generateDiffMatIncrustedWithSubtitleError);
		   //							console.log(r.KO);
		   }
		   
		   
					});
	
}




function throw_cycleHighLightCurrentSubtitle() {
	var ligneSubtitle_height = 38;
	player_options['onTimecodeChange'] = function (currentTime) {
		
		
		if (currentTime > 0) {
			//							console.log('throw_cycleHighLightCurrentSubtitle');
			cycleHighLightCurrentSubtitle(currentTime, ligneSubtitle_height);
		}
		
	};
}




function piocheTC(elem, dest, isSendSave) {
	//				console.log(elem_videoPlayer);
	var elem_videoPlayer = $j("video#video")[0];
	var isSendSave = typeof isSendSave !== 'undefined' ? isSendSave : false;
	
	
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	if (typeof elem_videoPlayer === 'undefined' || elem_videoPlayer === null) {
		$j("#dialog_alert").html(str_lang.kSousTitrage_errorTc_pioche1);
		$j("#dialog_alert").dialog('open');
	}
	
	else {
		if (_video_id != 'playlist') {
			// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
			var t = secondToTimeSubtitle(elem_videoPlayer.currentTime);
			var ligne = $j(elem).closest('tr');
			
			
			if (dest == 'in') {
				//							$j("#subTitleLine00\\:00\\:12\\.290").removeAttr("onclick");
				//							console.log(ligne);
				ligne.removeAttr("onclick");//pour ne pas aller dans l'ancien tcin dans le player vidéo
				var input_tcin = ligne.find("input[name='tcin']");
				if (input_tcin.attr('disabled') == 'disabled') {
					return;
				}
				input_tcin.val(t);
				ligne.attr("onclick", "goToTimeCodeInPlayer('" + t + "',this)");
			} else {
				var input_tcout = ligne.find("input[name='tcout']");
				if (input_tcout.attr('disabled') == 'disabled') {
					return;
				}
				input_tcout.val(t);
			}
			
			if (isSendSave) {
				var id_ligne = ligne.attr('id').split('subTitleLine').pop();
				actionSubtitle(0, id_doc_int, 'modif', id_ligne, ligne);
			}
		}
	}
}

function alertDisabled(tr) {
	
	
	//					console.log('isDisabled' + tr.children('input, textarea').attr('disabled'));
	
	if (tr.children('input, textarea').attr('disabled') == 'disabled') {
		
		$j("#dialog_alert").html(str_lang.kSousTitrage_disabledAlert);
		$j("#dialog_alert").dialog('open');
		
		
		$j("tr[id^='subTitleLine']").each(function () {
										  //						console.log('OUT of ' + $j(this).attr('id'));
										  if ($j(this).attr('id').indexOf('_new') !== -1) {
										  var t = $j(this).offset();
										  
										  var index = $j(this).index() - 1;
										  //								console.log('index ' + index);
										  
										  
										  //                                console.log('ligneSubtitle_height ' + ligneSubtitle_height);
										  var contain_top = Math.round(index * 38);//todo essayer d'utiliser la variable globale au lieu de 38
										  
										  if (!isScrolledIntoView($j(this), $j("#currentSubtitle"))) {
										  //									console.log('scroll container ' + $j("#currentSubtitle").scrollTop());
										  //									console.log($j(this).attr('id') +' ' + contain_top);
										  $j("#currentSubtitle").scrollTop(contain_top);
										  
										  }
										  return false;
										  }
										  });
		
	}
}

function putDisabled() {
	
	var new_ligne = false;
	$j("tr[id^='subTitleLine']").each(function () {
									  //						console.log('OUT of ' + $j(this).attr('id'));
									  if ($j(this).attr('id').indexOf('_new') !== -1) {
									  //							console.log('found new line  ' + $j(this).attr('id'));
									  new_ligne = true
									  return false;
									  }
									  });
	
	$j("tr[id^='subTitleLine']").each(function () {
									  if ($j(this).attr('id').indexOf('_new') == -1) {
									  //							console.log('OUT2 of ' + $j(this).attr('id'));
									  if (new_ligne) {
									  $j(this).find('input[type=text], textarea').attr('disabled', 'disabled');
									  } else {
									  $j(this).find('input[type=text], textarea').attr('disabled', null);
									  
									  }
									  }
									  });
	
	
	
}






function goToTimeCodeInPlayer(tcin, elem) {
	
	//pour ne pas foirer le focus sur la nouvelle ligne non remplie
	if ($j(elem).find("input[name='tcin']").attr('disabled') != 'disabled') {
		// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
		if (!(typeof tcin === 'undefined' || tcin === null)) {
			var ct = hmsToSecondsOnly(tcin);
			ct += 0.0009; /* important on ajoute car on veut PAS afficher dans la video le sous titre de la ligne préc (dont le tcout peut valoir le tcin de cette ligne), de tt façon c'est invisible à l'oeil nu le moment ou 2 lignes de sous-titres s'affichent au meme moment
						   ex:    00:00:19.480 00:00:23.130 bla
						   00:00:23.130 00:00:26.240 bla2
						   on MET 0.0009 et pas 0.001 pour que l'affichage du timecode dans le player reste reste correct (00:00:23.130 et PAS 00:00:23.131)
						   */
			//                        console.log('goto ' + ct);
			//                        console.log('typeof ' + typeof (ct));
			if (ct > 0) {
				//   console.log('videoD '+$j("video#video")[0]);
				var elem_videoPlayer = $j("video#video")[0];
				// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
				if (!(typeof elem_videoPlayer === 'undefined' || elem_videoPlayer === null)) {
					//									console.log('goToTimeCodeInPlayer '+ct);
					elem_videoPlayer.currentTime = ct;
				}
			}
		}
	}
	
}


function onTrackedVideoFrame(currentTime, duration) {
	//                        console.log('currentTime '+currentTime);
	//                        console.log('duration '+duration);
	$j("#timecodeDisplay").html(secondToTimeSubtitle(currentTime));
}

function secondToTimeSubtitle(d) {
	
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	if (!(typeof d === 'undefined' || d === null)) {
		
		var d = d.toString();
		var tab = d.split('.');
		//                            console.log(tab);
		d = Number(tab[0]);
		var h = Math.floor(d / 3600);
		var m = Math.floor(d % 3600 / 60);
		var s = Math.floor(d % 3600 % 60);
		var r = (h < 10 ? "0" : "") + h + ":" + (m < 10 ? "0" : "") + m + ":" + (s < 10 ? "0" : "") + s;
		// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
		if (typeof tab[1] === 'string' && tab[1].Trim() != '') {
			var r = r + '.' + tab[1].substring(0, 3);
		}
		return r;
	}
}



function cycleHighLightCurrentSubtitle(currentTime, ligneSubtitle_height) {
	//                    console.log('a ' + $j("#timecodeDisplay").text());
	
	
	// check if element exists
	if ($j("#timecodeDisplay").length) {
		$j("#timecodeDisplay").html(secondToTimeSubtitle(currentTime));
	}
	
	//                    console.log('timecodeDisplay ' + $j("#timecodeDisplay").length);
	//                        console.log('timecodeDisplayText ' + $j("#timecodeDisplay").text().Trim());
	
	
	$j("tr[id^='subTitleLine']").each(function () {
									  
									  if ($j(this).attr('id') != 'subTitleLineSTART') {
									  $j(this).removeClass('active');
									  //                        console.log('tcin ' + $j(this).find("input[name='tcin']").val());
									  //                        console.log('tcout ' + $j(this).find("input[name='tcout']").val());
									  
									  
									  var tcin = hmsToSecondsOnly($j(this).find("input[name='tcin']").val().Trim());
									  var tcout = hmsToSecondsOnly($j(this).find("input[name='tcout']").val().Trim());
									  if (currentTime >= tcin && currentTime <= tcout) {
									  $j(this).addClass('active');
									  var index = $j(this).index() - 1;
									  //                            console.log('index ' + index);
									  
									  
									  //                                console.log('ligneSubtitle_height ' + ligneSubtitle_height);
									  var contain_top = Math.round(index * ligneSubtitle_height);
									  //                                console.log('contain_top '+contain_top);
									  //                            console.log('scrollTop ' + $j("#currentSubtitle").scrollTop());
									  if (!isScrolledIntoView($j(this), $j("#currentSubtitle"))) {
									  $j("#currentSubtitle").scrollTop(contain_top);
									  }
									  }
									  }
									  });
}


function checkVideoPlayerReadyBeforeAddTMPtrack()
{
	
	//                    console.log('s ' + subTitling_defaultTrackToAdd);
	//                    console.log($j("video#video").length);
	// call the function again after 100 milliseconds
	if (!$j("video#video").length) {
		setTimeout(checkVideoPlayerReadyBeforeAddTMPtrack, 100);
	} else {
		subTitling_defaultTrackToAdd = false;
		actionSubtitle(0, id_doc_int);//Attention on envoie id_mat =0, car si on vient juste de changer de materiel, comme on rafraichis PAS la page, echo $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc_int]['id_mat'] va contenir l'ancienne valeur
	}
}


/* Attention The colon (":") and period (".") are problematic within the context of a jQuery selector because they indicate a pseudo-class and class, respectively. They must be "escaped" by placing two backslashes    */
function escapeJQuerySelector(myid) {
	return  myid.replace(/(:|\.|\[|\]|,)/g, "\\$1");
}



function hmsToSecondsOnly(str) {
	//					console.log(str);
	//					console.log(typeof(str));
	
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR split on undefined
	if (typeof str === 'undefined' || str === null) {
		return null;
	}
	var a = str.split(':'); // split it at the colons
	var s = new Array();
	if (a.indexOf('.') == -1) {
		s[0] = a[2];
		s[1] = a[3];
	} else {
		var s = a[2].split('.');
	}
	// minutes are worth 60 seconds. Hours are worth 60 minutes.
	var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+s[0]);
	return parseFloat(String(seconds) + '.' + s[1]);
}


// On ne pourra insérer des nouvelles lignes qu'entre des lignes existantes
function addHTMLSubtitle(elem, id_ligne) {
	
	
	
	var ligne_source = $j(elem).closest('tr');
	var ligne_NEXT = ligne_source.next();
	
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	//                    if (!(typeof ligne_NEXT === 'undefined' || ligne_NEXT === null)) { //ne fonctionne pas ici (tester sur la dernière ligne)
	if (ligne_NEXT.is("tr")) {
		//                    console.log('typeof ligne_NEXT '+typeof(ligne_NEXT));
		//                    console.log(ligne_NEXT.is("tr"));
		var ligneID_NEXT = ligne_NEXT.attr('id');
		
		if (ligneID_NEXT.indexOf('new') != '-1') {
			$j("#dialog_alert").html(str_lang.kSousTitrage_alreadyNewLineSubtitle);
			$j("#dialog_alert").dialog('open');
			return;
		}
	}
	var newLigneID = ligne_source.attr('id').split('subTitleLine').pop() + '_new';
	//                    console.log('t'+newLigneID);
	
	ligne_source.after('<tr id="subTitleLine' + newLigneID + '"><td><img onClick="piocheTC(this,\'in\')" src="'+ow_const.designUrl+'/images/b_definir_debut_petit.gif" title="'+str_lang.kUtiliserPositionCommeDebut+'" alt="'+str_lang.kUtiliserPositionCommeDebut+'"/></td><td><input type="text" name="tcin" size="12" maxlength="12" ></td><td><img onClick="piocheTC(this,\'out\')" src="'+ow_const.designUrl+'/images/b_definir_debut_petit.gif" title="'+str_lang.kUtiliserPositionCommeFin+'" alt="'+str_lang.kUtiliserPositionCommeFin +'"/></td><td><input type="text" name="tcout" size="12" maxlength="12" ></td><td><textarea name="text" rows="2" cols="70"></textarea></td><td><img onclick="actionSubtitle(0, id_doc_int,\'modif\',\'' + newLigneID + '\',this)" src="'+ow_const.designUrl+'/images/save.gif" title="'+str_lang.kSousTitrage_enregistrerLigne+'" alt="'+str_lang.kSousTitrage_enregistrerLigne+'"></td><td></td><td><img onclick="deleteHTMLSubtitle(this)" class="miniTrash" src="'+ow_const.designUrl+'/images/button_drop.gif" title="'+str_lang.kSupprimer+'" alt="'+str_lang.kSupprimer+'"></td></tr>');
	
	putDisabled();
	
}



function deleteHTMLSubtitle(elem) {
	
	$j(elem).closest('tr').remove();
	putDisabled();
	
	
}



function checkModif(id_ligne, elem) {
	
	//on se sert pas de id_ligne, mais on le garde au cas ou...
	
	var tcin = $j(elem).closest('tr').find("input[name='tcin']");
	tcin.removeClass('error');
	var tcin_val = tcin.val().Trim();
	var pattern_tc = new RegExp(/\d{2}:\d{2}:\d{2}\.\d{3}/);
	//                    console.log('|'+tcin_val+'|');
	
	
	/******** CHECK DU TCIN **************************/
	
	
	if (!pattern_tc.test(tcin_val)) {
		console.log('format tcin invalide');
		$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcin1);
		$j("#dialog_alert").dialog('open');
		tcin.addClass('error');
		return false;
	} else {
		var tab_tcin_val = tcin_val.split(':');
		var tcin_mn = parseInt(tab_tcin_val[1], 10);
		if (tcin_mn > 59) {
			$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcin_mm);
			$j("#dialog_alert").dialog('open');
			tcin.addClass('error');
			return false;
		}
		
		var tcin_ss = parseInt(tab_tcin_val[2].split('.').shift(), 10);
		//						console.log('tcin_ss '+tcin_ss);
		if (tcin_ss > 59) {
			$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcin_ss);
			$j("#dialog_alert").dialog('open');
			tcin.addClass('error');
			return false;
		}
		
	}
	
	
	var tcin_valS = hmsToSecondsOnly(tcin_val);
	
	
	
	var ligne_prec = $j(elem).closest('tr').prev();
	var ligneID_prec = ligne_prec.attr('id');
	//					console.log('ligneID_prec '+ligneID_prec);
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	if (!(typeof ligneID_prec === 'undefined' || ligneID_prec === null) && ligneID_prec != 'subTitleLineSTART') {
		if (ligneID_prec.indexOf('subTitleLine') !== -1) {
			//                            var tcinPREC = $j(ligne_prec).closest('tr').find("input[name='tcin']");
			//                            var tcin_valPREC = tcinPREC.val();
			var tcoutPREC = $j(ligne_prec).closest('tr').find("input[name='tcout']");
			var tcout_valPREC = hmsToSecondsOnly(tcoutPREC.val());
			//                            console.log('tcin_valPREC' + tcin_valPREC);
			//                            console.log('tcout_valPREC' + tcout_valPREC);
			if (tcin_valS < tcout_valPREC) {
				console.log('le tcin doit être supérieur ou égal au tcout précédent ');
				$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcin2);
				$j("#dialog_alert").dialog('open');
				tcin.addClass('error');
				return false;
			}
			
		}
	}
	
	
	
	/******** CHECK DU TCOUT **************************/
	
	
	
	var tcout = $j(elem).closest('tr').find("input[name='tcout']");
	tcout.removeClass('error');
	var tcout_val = tcout.val().Trim();
	if (!pattern_tc.test(tcout_val)) {
		console.log('format tcout invalide');
		$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcout1);
		$j("#dialog_alert").dialog('open');
		tcout.addClass('error');
		return false;
	} else {
		var tab_tcout_val = tcout_val.split(':');
		var tcout_mn = parseInt(tab_tcout_val[1], 10);
		if (tcout_mn > 59) {
			$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcout_mm);
			$j("#dialog_alert").dialog('open');
			tcout.addClass('error');
			return false;
		}
		
		var tcout_ss = parseInt(tab_tcout_val[2].split('.').shift(), 10);
		//						console.log('tcout_ss '+tcout_ss);
		if (tcout_ss > 59) {
			$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcout_ss);
			$j("#dialog_alert").dialog('open');
			tcout.addClass('error');
			return false;
		}
		
	}
	
	
	var tcout_valS = hmsToSecondsOnly(tcout_val);
	
	var ligne_NEXT = $j(elem).closest('tr').next();
	var ligneID_NEXT = ligne_NEXT.attr('id');
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	if (!(typeof ligneID_NEXT === 'undefined' || ligneID_NEXT === null)) {
		if (ligneID_NEXT.indexOf('subTitleLine') !== -1) {
			var tcinNEXT = $j(ligne_NEXT).closest('tr').find("input[name='tcin']");
			var tcin_valNEXT = hmsToSecondsOnly(tcinNEXT.val());
			//                            console.log('tcin_valPREC' + tcin_valPREC);
			//                            console.log('tcout_valPREC' + tcout_valPREC);
			if (tcout_valS > tcin_valNEXT) {
				console.log('le tcout doit être inférieur ou égal au tcin suivant ');
				$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcout2);
				$j("#dialog_alert").dialog('open');
				tcout.addClass('error');
				return false;
			}
			
		}
	}
	
	if (tcin_valS >= tcout_valS) {
		console.log('tcout doit etre superieur à tcin');
		$j("#dialog_alert").html(str_lang.kSousTitrage_errorTcout3);
		$j("#dialog_alert").dialog('open');
		tcin.addClass('error');
		tcout.addClass('error');
		return false;
	}
	
	
	var text = $j(elem).closest('tr').find("textarea[name='text']");
	text.removeClass('error');
	var text_val = text.val().Trim();
	if (text_val.length > max_chars_line || text_val.length == 0) {//186
		$j("#dialog_alert").html(str_lang.kSousTitrage_errorText.replace('[max]', max_chars_line));
		$j("#dialog_alert").dialog('open');
		
		text.addClass('error');
		return false;
	}
	
	if (text_val.indexOf('-->') !== -1) {
		$j("#dialog_alert").html(str_lang.kSousTitrage_errorText2.replace('[max]', max_chars_line));
		$j("#dialog_alert").dialog('open');
		
		text.addClass('error');
		return false;
	}
	
	
	return true; //Attention, ne pas oublier ce return true à la fin
}


function actionSubtitle(id_mat, id_doc, typeAction, id_ligne, elem, majButtonPublish) {
	
	//                        console.log('majButtonPublish ' + majButtonPublish);
	
	
	var typeAction = typeof typeAction !== 'undefined' ? typeAction : 'show';
	//                    console.log(typeAction);
	
	var noConfirmBox = true;
	if (typeAction == 'restore') {
		noConfirmBox = false;
		var confirmResponse = confirm(str_lang.kConfirmerAbandon);
	}
	else if (typeAction == 'delete') {
		noConfirmBox = false;
		var confirmResponse = confirm(str_lang.kSousTitrage_deleteLineWarning);
	}
	
	
	if (typeAction == 'modif') {
		//                        console.log($j(elem).attr('src'));
		//                        console.log(checkModif(id_ligne, elem));
		if (!checkModif(id_ligne, elem)) {
			return;
		}
		
	}
	
	
	//                    console.log('after');
	
	if (noConfirmBox || confirmResponse) {
		
		
		if (!(typeof id_ligne === 'undefined' || id_ligne === null)) {
			var tcin = $j("tr#subTitleLine" + escapeJQuerySelector(id_ligne)).find("input[name='tcin']").val();
			var tcout = $j("tr#subTitleLine" + escapeJQuerySelector(id_ligne)).find("input[name='tcout']").val();
			var text = $j("tr#subTitleLine" + escapeJQuerySelector(id_ligne)).find("textarea[name='text']").val();
		}
		
		var action_next = null;
		if (typeAction == 'saveAndExport') {
			typeAction = 'save';
			action_next = 'export';
		}
		if (typeAction == 'download_file') {
			typeAction = 'save';
			action_next = 'download_file';
		}
		//                            $j("#dialog_loading").dialog('open'); //ABANDON ça fait moche
		
		$j.ajax({
				url: 'empty.php?urlaction=subtitle&' + typeAction,
				type: 'POST',
				dataType: 'html',
				timeout: 30000, //30 secondes
				data: {/* Attention, on peut pas mettre en post=> include: "subtitle",*/ id_mat: id_mat, id_doc: id_doc, id_ligne: id_ligne,
				tcin: tcin, tcout: tcout, text: text
				}
				}).done(function (data) {
					//console.log("done typeAction",typeAction,"data",data);
						if (typeAction == 'save') {
							if (data == 'OK') {
							$j("#dialog_alert").html(str_lang.kSousTitrage_saveFileOK);
							//									$j("#dialog_alert").dialog('open');//on le commente car quand on ouvre la popup importer, on la perd
							
							if (action_next == 'export') {
								window.location = ow_const.kCheminHttp +"/empty.php?urlaction=subtitle&export&id_doc=" + id_doc;
							}
							if (action_next == 'download_file') {
								window.location = ow_const.kCheminHttp +"/empty.php?urlaction=subtitle&download_file&id_doc=" + id_doc;
							}					
							} else {
							$j("#dialog_alert").html(str_lang.kSousTitrage_saveFileKO);
							//									$j("#dialog_alert").dialog('open');/on le commente car quand on ouvre la popup importer, on la perd
							}
						} else if (typeAction == 'show') {
							$j("#actionFORMSubtitle, div#currentSubtitleWrapper").removeClass('hidden');

							addTmpTrack();
							$j("#currentSubtitle").html(data);
							if (id_mat > 0) {
							highlightActiveSubtitle(id_mat);//id_mat qui peut valoir 0 la 1ère fois qu'on arrive sur la page
							}
							// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
							//                                    if (!(typeof elem === 'undefined' || elem === null)) {
							//                                        highlightActiveSubtitle($j(elem).closest('tr').attr('id').split('matSubtitle').pop());
							//                                    }
							
							throw_cycleHighLightCurrentSubtitle();
							
							
							if (majButtonPublish) {
							actionSubtitle(id_mat, id_doc, 'info_isPublish'); //quand on change de matériel (lien afficher) ET SEULEMENT DANS CE CAS LA, on a besoin de maj le libelle du bouton publié suivant l'état du matériel sur la fiche (DM_INACTIF)
							}
						} else if (typeAction == 'info_isPublish') {
							
							var r = JSON.parse(data);
															console.log('r ',r);
							
							if (!(typeof r.OK === 'undefined' || r.OK === null)) {
							if ($j("#actionFORMSubtitle #publishButton").length && $j("#actionFORMSubtitle #publishValue").length) {
								$j("#actionFORMSubtitle #publishButton").val(r.OK['lib']);
								$j("#actionFORMSubtitle #publishValue").val(r.OK['etat']);
								//                                            console.log('publishButton maj');
							}
							
							} else {
							console.log('Probleme de maj publish');
							}
							
						}
						
						else if (typeAction == 'info_isModified') {
							if (data == 'true') {
							if (confirm(str_lang.kSousTitrage_beforeExportIsModifiedWarning)) {
							actionSubtitle(id_mat, id_doc, 'saveAndExport');
							} else {
							window.location = ow_const.kCheminHttp + "/empty.php?urlaction=subtitle&export&id_doc=" + id_doc;
							}
							} else {
							window.location = ow_const.kCheminHttp + "/empty.php?urlaction=subtitle&export&id_doc=" + id_doc;
							}
							
						
						}
						//quand on on modif des lignes existantes=> on ne recharge pas le form, en fait bug, il faut egalement mettre reafficher le form pour mettre à jour les clés tr id (et pour pouvoir détecter le cas tcin!=tcout et pas ddupliquer la ligne, bug remonté par D.C)
						//quand on ajoute une ligne => on recharge le form pour enlever les disabled
						
						else if (typeAction == 'export') {
						
						}
						
						//							else if (typeAction == 'modif' && id_ligne.indexOf('_new') == -1) {
						//								addTmpTrack();
						//							}
						else {
						actionSubtitle(id_mat, id_doc); //reaffichage du form
						}
						
						//							$j("#dialog_loading").dialog('close');
						}).fail(function (data) {
								//                $j("#loadingImg").hide();
								$j("#currentSubtitle").html(str_lang.kSousTitrage_ajaxResponseDelayExceeded);
								//                                $j("#dialog_loading").dialog('close');//ABANDON ça fait moche
								});
		//
	}
	
	
}





function askSaveThenShowOther_TEST(id_mat, id_doc) {
	$j.get("empty.php?urlaction=subtitle&getActiveMat&id_doc=" + id_doc, function (data) {
		   
					}).done(function (data) {
							
							if (data != id_mat) {
							//							console.log('sub différent');
							//	1.2 on regarde si il y a eu des modifs sur le sous-titre actif
							$j.get("empty.php?urlaction=subtitle&info_isModified&id_doc=" + id_doc, function (data2) {
								   
								   }).done(function (data2) {
										   
										   
										   if (data2 == 'true') {
										   //									console.log("y'a eu des modifications sur le sous-titre actif");
										   //	1.2.1 on demande au client si il veut sauvegarder le sous-titre actif
										   if (confirm(str_lang.kSousTitrage_saveCurrentWarning)) {
										   actionSubtitle(data, id_doc, 'save');
										   } else {
										   console.log('annuler');
										   //										actionSubtitle(data, id_doc, 'restore');
										   //on ne passe pas par actionSubtitle(data, id_doc, 'restore');, car cela reaffiche le fichier qu'on a restauré au lieu du showOther
										   //										actionSubtitle(data, id_doc, 'restoreWithoutShow');
										   ///:ci dessous ne marche pas
										   //										$j.get("empty.php?urlaction=subtitle&restore&id_doc=" + id_doc + "&id_mat=" + data, function (data3) {
										   $j.get("empty.php?urlaction=subtitle&restore&id_doc=" + id_doc + "&id_mat=" + data, function (data3) {
												  //								actionSubtitle(data, id_doc, 'show');
												  
												  //
												  });
										   }
										   //									1.2.1.1 on affiche le sous-titre demandé
										   //								} else {
										   //									//										1.2.3 on affiche le sous-titre demandé
										   //									//									console.log("y'a PAS eu des modifications sur le sous-titre actif");
										   //									actionSubtitle(id_mat, id_doc, 'show');
										   }
										   
										   //								actionSubtitle(id_mat, id_doc, 'show');
										   $j.get("empty.php?urlaction=subtitle&show&id_doc=" + id_doc + "&id_mat=" + id_mat, function (data4) {
												  //								actionSubtitle(data, id_doc, 'show');
												  
												  //
												  });
										   
										   
										   
										   });
							
							} else {
							
							//							actionSubtitle(id_mat, id_doc, 'show');
							
							$j.get("empty.php?urlaction=subtitle&show&id_doc=" + id_doc + "&id_mat=" + id_mat, function (data5) {
								   //								actionSubtitle(data, id_doc, 'show');
								   //
								   });
							
							
							//							console.log('sub pareil');
							}
							
							});
	//						1. si on essaye d'afficher un sous-titre différent que celui actif
	
	
	
}








function highlightActiveSubtitle(id_mat) {
	
	$j("tr[id^='matSubtitle']").each(function () {
									 $j(this).removeClass('active');
									 });
	// check if element exists
	if ($j("tr#matSubtitle" + id_mat).length) {
		$j("tr#matSubtitle" + id_mat).addClass('active');
	}
	
}

function addTmpTrack() {
	
	
	//                    $j("video#video track").each(function () {
	//                        $j(this).remove();
	//                    });
	// check if element exists
	if ($j("#ow_bouton_srt").length) {
		//on supprime les boutons de changement de sous-titre (EN, FR...)
		//                        $j("#ow_bouton_srt").remove();//pour éviter ow_bouton_srt is not defined à cause des appels des différents .js du moteur
		$j("#ow_bouton_srt").hide();
	}
	
	//        console.log(player_options['srt_path']);
	//            player_options['srt_path']='(FR)http://192.168.0.150/media/colas/public/visionnage/1462368608.00054200/2mai1_fr.srt.vtt';
	// Attention !!!, on est obligé de supprimer la balise track avant de la re-ajouter pour que le fichier de sous-titre soit bien changé (on peut pas juste modifier l'attribut src de <track>) (donc .html() et pas .append())
	$j("video#video").html('<track src="'+ow_const.kCheminHttp+'/empty.php?urlaction=subtitleFileLiveEdit&id_doc='+id_doc_int+'" default ></track>');
	var track = $j("video#video track");
	// check if element exists
	if (track.length) {
		//                    console.log(track2[0].track['mode']);
		//                        console.log(typeof (track[0]));
		// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
		if (!(typeof track[0] === 'undefined' || track[0] === null)) {
			track[0].track['mode'] = 'showing';//attention ! firefox met en disabled quand on change la balise track dynamiquement par js
			
		}
		//        console.log(typeof(track[0].track));
	}
	
	
	//                    });
}


function addMat(id_champ, valeur, idx) {
	console.log('id_champ ' + id_champ + 'valeur' + valeur + 'idx' + idx);
	location.reload();//quand on vient d'importer un sous-titre avec indexPopupUpload via le bouton Importer, on rafraîchit la page
	
	//                        console.log('id_champ ' + id_champ);
}


function toggleVisibility(myDiv, imgArrow) {
	//                        console.log(myDiv);
	if (myDiv.css('display') == 'block') {
		myDiv.hide("slow");
		if (imgArrow) {
			imgArrow.attr('src', 'design/images/arrow_right.gif');
		}
	} else {
		myDiv.show("slow");
		if (imgArrow) {
			imgArrow.attr('src', 'design/images/arrow_down.gif');
		}
	}
}


function isScrolledIntoView(elem, container)
{
	
	//                    console.log(elem);
	var docViewTop = container.scrollTop();
	var docViewBottom = docViewTop + container.height();
	//                    console.log('docViewTop ' + docViewTop);
	//                                            console.log('docViewBottom '+docViewBottom);
	
	var elemTop = elem[0].offsetTop;
	var elemBottom = elemTop + elem.height();
	
	//                    console.log('elemTop ' + elemTop);
	//                    console.log('elemBottom ' + elemBottom);
	
	var r_y = (elemBottom <= docViewBottom);
	var r_x = (elemTop >= docViewTop);
	//                    console.log('x ' + r_x);
	//                    console.log('y ' + r_y);
	
	
	var r = r_x && r_y;
	//                    console.log('isScrolledIntoView ' + r);
	
	return r;
}





