/*
	OW_custom scroll permet la création de scrolls basiques que l'on peut stylé via CSS, 
	
	structure standard avant d'appeler l'objet JS : 
	< element wrapper , position : relative; >
		<element track, position absolute, et on l'accroche où l'on veut avec top / left / right / bottom>
			<element handle, position absolute, largeur & hauteur définie ></element handle>
		</element track>
		<element viewport, position relative; overflow : hidden ; height définie>
			<element content, position : absolute> contenu, long texte</element content
		</element viewport>
	</element wrapper>

*/
function OW_CustomScroll(elemViewport, elemTrack, elemHandle, elemContent,type, elemArrowUp,elemArrowDown){
	this.elemViewport = elemViewport;
	this.elemTrack = elemTrack;
	this.elemHandle = elemHandle;
	this.elemContent = elemContent;
	if(typeof elemArrowUp != 'undefined'){
		this.elemArrowUpInit = false ; 
		this.elemArrowUp = elemArrowUp;
	}
	if(typeof elemArrowDown != 'undefined'){
		this.elemArrowDownInit = false ; 
		this.elemArrowDown = elemArrowDown;
	}
	this.type = type;
	this.sliderVar = this.initCustomScroll();
	this.active = false ; 

}

OW_CustomScroll.prototype.initCustomScroll =  function(){
	this.CustomScroll();
	if(window.addEventListener){
		window.addEventListener('resize',function(){this.scroll_display_test(this.sliderVar);}.bind(this),false);
	}else{
		window.attachEvent('onresize',function(){this.scroll_display_test(this.sliderVar);}.bind(this));
	}
	this.scroll_display_test();
	return this.sliderVar;
}

function old_initCustomScroll(elemViewport, elemTrack, elemHandle, elemContent, sliderVar){
	sliderVar = CustomScroll(elemViewport, elemTrack, elemHandle,sliderVar);
	window.addEventListener('resize',function(){ scroll_display_test(elemViewport,elemTrack,elemHandle,elemContent,sliderVar);}.bind(this),false);
	scroll_display_test(elemViewport,elemTrack,elemHandle,elemContent,sliderVar);
	return sliderVar;
}

OW_CustomScroll.prototype.CustomScroll = function(){
	var oldvalue=null;
	
	function scrollFunction(value,element,slider,type){
		if(type=="vertical"){
			element.scrollTop = Math.round(value/slider.maximum*(element.scrollHeight-element.offsetHeight));
		}else if(type=="horizontal") {
			element.scrollLeft = Math.round(value/slider.maximum*(element.scrollWidth-element.offsetWidth));
		}
	}
	
	

	if(this.sliderVar){
		oldvalue = this.sliderVar.value;
		this.sliderVar.maj(this.elemHandle.id,this.elemTrack.id);
	}else{
		if(typeof this.elemArrowUp != 'undefined' && !this.elemArrowUpInit){
			this.elemArrowUpInit = true ; 
			$j(this.elemArrowUp).mousedown(function(){
				goUp = function(slider){
					newVal = slider.value-0.05;
					if(newVal>=-0.05){
						slider.setValue(newVal);
						this.current_timeout = setTimeout(function(){goUp(slider)}.bind(this),25);
					}
				}.bind(this);
				goUp(this.sliderVar);
			}.bind(this));
			
			$j(document).mouseup(function(){
				if(this.current_timeout != null){
					clearTimeout(this.current_timeout);
					this.current_timeout= null ; 
				}
			}.bind(this));
		}
		if(typeof this.elemArrowDown != 'undefined' && !this.elemArrowDownInit){
			this.elemArrowDownInit = true ; 
			$j(this.elemArrowDown).mousedown(function(){

				goDown = function(slider){
					newVal = slider.value+0.05;
					if(newVal<=1.05){
						slider.setValue(slider.value+0.05);
						this.current_timeout = setTimeout(function(){goDown(slider)}.bind(this),25);
					}
				}.bind(this)
				goDown(this.sliderVar);
				
			}.bind(this));
			$j(document).mouseup(function(){
				if(this.current_interval != null){
					clearInterval(this.current_interval);
					this.current_interval = null ; 
				}
			}.bind(this));
		}
	
		$j(this.elemViewport).unbind('mousewheel');
		this.sliderVar = new Control.Slider(this.elemHandle.id,this.elemTrack.id, {
		axis :this.type,
		value : 0,
		onSlide : function(v){scrollFunction(v,this.elemViewport, this.sliderVar,this.type);}.bind(this),
		onChange : function(v){scrollFunction(v,this.elemViewport, this.sliderVar,this.type);}.bind(this)

		});
		
	}	
//	$j(this.elemViewport).on('mousewheel DOMMouseScroll',function(evt){
//		var _delta = evt.wheelDelta || -evt.detail || evt.originalEvent.wheelDelta || -evt.originalEvent.detail;
//		evt.preventDefault();
	try{
		if(typeof this.mousewheel_handler =='undefined'){
			this.mousewheel_handler = function(evt){
				var _delta = evt.wheelDelta || -evt.detail || evt.originalEvent.wheelDelta || -evt.originalEvent.detail;
				evt.preventDefault();
			
				if(_delta<0){
					mouseScrollValue = 0.05;
				}else if (_delta>0){
					mouseScrollValue=-0.05;
				}
				this.sliderVar.setValue(this.sliderVar.value+mouseScrollValue);
				return false;
			}.bind(this);
		}
	
		if(typeof $j(this.elemViewport).on != 'undefined'){
			$j(this.elemViewport).off('mousewheel DOMMouseScroll',this.mousewheel_handler);
			$j(this.elemViewport).on('mousewheel DOMMouseScroll',this.mousewheel_handler);	
		}else{
			$j(this.elemViewport).unbind('mousewheel DOMMouseScroll',mousewheel_handler.bind(this));
			$j(this.elemViewport).bind('mousewheel DOMMouseScroll',mousewheel_handler.bind(this));	
		}
	}catch(e){
		console.log("customScroll - crash init mousewheel handler");
		console.log(e);
	}
	if(oldvalue){
		this.sliderVar.setValue(oldvalue);
	}
	return this.sliderVar;
}

OW_CustomScroll.prototype.scroll_display_test = function(custom_scroll){
	 if ((this.type=='vertical' && this.elemContent.offsetHeight > this.elemViewport.offsetHeight)
		|| (this.type=='horizontal' && this.elemContent.offsetWidth > this.elemViewport.offsetWidth )){
		if(typeof this.elemArrowUp != 'undefined'){
			this.elemArrowUp.style.display = "block";
		}
		if(typeof this.elemArrowDown != 'undefined'){
			this.elemArrowDown.style.display = "block";
		}
		this.elemTrack.style.display = "block";
		custom_scroll = this.CustomScroll();
		this.active = true ; 
	 }else{
		if(typeof this.elemArrowUp != 'undefined'){
			this.elemArrowUp.style.display = "none";
		}
		if(typeof this.elemArrowDown != 'undefined'){
			this.elemArrowDown.style.display = "none";
		}
		this.elemTrack.style.display = "none";
		$j(this.elemViewport).unbind('mousewheel');
		 this.active = false ; 
	 }			
}
	