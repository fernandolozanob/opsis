// fonction qui gère l'activation / désactivation des divers paramètres cachés, 
// repose sur le fait que les inputs sont les frères du radio button, ATTENTION À LA STRUCTURE
function updateHiddenFields(elt){
	if(elt.value == 'custom_export'){
		// fieldSubmit continueToCustomExport
		$j("#formExport #fieldSubmit").css("display","none");
		$j("#formExport #continueToCustomExport").css("display","block");
	}else{
		$j("#formExport #fieldSubmit").css("display","block");
		$j("#formExport #continueToCustomExport").css("display","none");
		
		$j("#formExport .hiddenExportPrms").attr("disabled","disabled");
		$j(elt).siblings(".hiddenExportPrms").removeAttr('disabled');
	}
}
function updateCustomHiddenFields(elt){
	index = $j("#formExport .custom_export_form select[name='export']").get(0).selectedIndex;
	targetclass = $j("#formExport .custom_export_form select[name='export'] option").get(index).className;
	$j("#formExport .custom_export_form *.hiddenExportPrms:input").attr('disabled',true);
	$j("#formExport .custom_export_form *.hiddenExportPrms."+targetclass).removeAttr('disabled');
	
	if(targetclass == 'custom_foppdf_planche'){
		$j("#formExport .custom_export_form .fopPdfPrms_panel").removeClass('hidden');
		$j("#formExport .custom_export_form .fopPdfPrms_panel .hiddenExportPrms").removeAttr('disabled');
	}else{
		$j("#formExport .custom_export_form .fopPdfPrms_panel").addClass('hidden');
	}

}
/*
$j(document).ready(function(){
	refreshSelectedItemsForExport($j("#formExport #type").val());
});
	*/
function refreshSelectedItemsForExport(type_exp){
	str_selection = str_lang.kSelection;
	nb_selection = 0 ; 
	list_ids = "";
	query_str_checkbox = "";
	if(type_exp == "doc"){
		query_str_checkbox = "#resultats .resultsCorps > input[type='checkbox'],#resultats .resultsMos  .mos_checkbox input[type='checkbox']";
	}else if (type_exp == "panierdoc"){
		query_str_checkbox = "#panFrame #formPanier .resultsCorps > input[type='checkbox'],#panFrame #formPanier .resultsMos  .mos_checkbox input[type='checkbox']";
	}
	$j(query_str_checkbox).each(function(idx,elt){
		if($j(elt).get(0).checked){
			if(list_ids != ''){
				list_ids+=",";
			}
			list_ids += $j(elt).val();
			nb_selection++;
		}
	});

	
	if(nb_selection>0){
		$j("#formExport select option.nb_selection").val(nb_selection) ;
		$j("#formExport select option.nb_selection").html(str_selection+" ("+nb_selection+")") ;
		$j("#formExport select option.nb_selection").removeAttr("disabled");
		if($j("#formExport select[name='nb']").length>0){
			$j("#formExport select[name='nb']").get(0).selectedIndex=0;
		}
		$j("#formExport #exp_ids").val(list_ids);
	}else{
		$j("#formExport select option.nb_selection").val(nb_selection) ;
		$j("#formExport select option.nb_selection").html(str_selection) ;
		$j("#formExport select option.nb_selection").attr("disabled","true");
		if($j("#formExport select[name='nb']").length>0){
			$j("#formExport select[name='nb']").get(0).selectedIndex=1;
		}
		$j("#formExport #exp_ids").val("");

	}
	
	if(list_ids != ''){
		return true ; 
	}else{
		return false ; 
	}
}

function checkActivateIds(){
	if($j("#formExport select[name='nb']").get(0).selectedIndex==0){
		$j("#formExport #exp_ids").removeAttr('disabled');
	}else{
		$j("#formExport #exp_ids").attr('disabled',true);
	}
}

function beginExportReport(){
	$j("#fieldSubmit").addClass("export_running");
	$j("#progress_export").progressbar({max : 100 , value : 0});
	$j("#popupModal").css("cursor", "progress");
	if($j("div.progressbar_text").length ==0){
		$j("#progress_export").append('<div class="progressbar_text"></div>');
	}
	$j("#progress_export .progressbar_text").text("Démarrage..."); // str_lang.kExportMsgDemarrage
	setTimeout(function(){getExportStatus();},800);
}

function getExportStatus(){
	$j.ajax({
		url : "export.php?getStatus=true",
		success: function(data){
			try{
				xml =$j(data);
				state = xml.find("state").text();
				max = xml.find("steps").text();
				curr = xml.find("current").text();
				if(xml.find("error_msg")){
					err_msg = xml.find("error_msg").text();
				}else{
					err_msg = "";
				}
				percent = Math.min((curr / (max)*100)-(1/max*100/2),100);
				
				switch (state){
					case "init" : 
						text_state = str_lang.kExportBarTextDemarrage;
						break;
					case "running" : 
						text_state = str_lang.kExportBarTextRunning;
						break;
					case "fop" : 
						text_state = str_lang.kExportBarTextFop;
						break;
					case "ready" : 
						text_state = str_lang.kExportBarTextReady;
						break;
				}
				
				if(state != 'error' && state != 'ready'){ 
					$j("#progress_export").progressbar("value", parseInt(percent,10));
					$j("#progress_export").find(".progressbar_text").text(text_state);
					setTimeout(function(){getExportStatus()},1000);
				}else if(state=="error"){
					$j("#popupModal").css('cursor','');
					$j("#progress_export .progressbar_text").text(err_msg);
					setTimeout(function(){
						$j("#fieldSubmit").removeClass("export_running");
					},500);
				}else if (state=="ready"){
					$j("#popupModal").css('cursor','');
					$j("#progress_export").progressbar({value : 100});
					$j("#progress_export .progressbar_text").text(str_lang.kExportBarTextReady); // "Votre téléchargement va démarrer.."
					setTimeout(function(){
						$j("#fieldSubmit").removeClass("export_running");
						$j("#formExport").addClass('export_done');
						//hideMenuActions();
					},500);
				}
			}catch (e){
				console.log("getExportStatus fail - "+e);
			}
		}
	
	});
}

function showCustomExport(){
	// toggle des blocks standard / custom
	$j("#formExport div.std_export_form").css('display',"none");
	$j("#formExport div.custom_export_form").css('display',"block");
	
	// désactivation des inputs du formulaire d'export standard
	$j("#formExport div.std_export_form *:input").attr('disabled','true');
	// action des inputs du formulaire d'export custom
	$j("#formExport div.custom_export_form *:input").attr('disabled','true');
	$j("#formExport div.custom_export_form select[name='export']"+
	",#formExport div.custom_export_form .cust_field_area.active *:input ").removeAttr('disabled');
	$j("#formExport div.custom_export_form select[name='export']").trigger('change');
	
	$j("#formExport div.custom_export_form .cust_field_checker").removeAttr('disabled');
	$j("#formExport div.custom_export_form .cust_check_all").removeAttr('disabled');
	
	$j("#formExport div.custom_export_form select.cust_export_sel,"
	+"#formExport div.custom_export_form .cust_field_delete").removeAttr('disabled');
	
	$j("#formExport div.custom_export_form .add_custom_field").removeAttr('disabled');
	
	// toggle des boutons télécharger / continuer
	$j("#formExport #fieldSubmit").css("display","block");
	$j("#formExport #continueToCustomExport").css("display","none");
}

function hideCustomExport(){
	// toggle des blocks standard / custom
	$j("#formExport div.std_export_form").css('display',"block");
	$j("#formExport div.custom_export_form").css('display',"none");
	
	// désactivation des inputs du formulaire d'export custom
	$j("#formExport div.custom_export_form *:input").attr('disabled','true');
	// réactivation des inputs du formulaire d'export standard
	$j("#formExport div.std_export_form input[type='radio']").removeAttr('disabled');
	$j("#formExport div.std_export_form input[type='radio']:checked").trigger('change');
	
	// toggle des boutons télécharger / continuer
	$j("#formExport #fieldSubmit").css("display","block");
	$j("#formExport #continueToCustomExport").css("display","none");
}


function loadTemplateExport(tabExportXML){
	// ajout du nombre de champs necessaires
	// return false;
	inactive_area = $j('div.cust_field_area.inactive .customField2');
	
	for (i=0; i < tabExportXML.length ; i++){
		inactive_area.each(function(idx,elt){
			if($j(elt).find('.cust_field_xml').val() == "<![CDATA["+tabExportXML[i]+"]]>" ){
				$j(elt).find('.cust_field_arrow_action').click();
				return false ;
			}
		});
	}
}

function makeCustomFieldsSortable(){
	$j('#formExport .customFields2 .cust_field_area.active').sortable
	(
		{
			appendTo: $j("#formExport .customFields2 .cust_field_area.active"),
			axis:'y',
			forcePlaceholderSize : true,
			placeholder : 'customField customField_placeholder' ,
			containment: 'parent', 
			tolerance : "pointer",
			forceHelperSize  : true 
			}
	);
}

function toggleCustomField(elt, moveFieldToActive){
	// cas click sur le span fleche 
	if($j(elt).hasClass('cust_field_arrow_action')){
		var elt_to_move = $j(elt).parents('.customField2').eq(0) ;
	}else{
		var elt_to_move = $j(elt) ; 
	}
	// récupération du sens de transfert si non précisé 
	if(typeof moveFieldToActive == 'undefined'){
		if(elt_to_move.parents('.cust_field_area').hasClass('inactive')){
			moveFieldToActive = true ; 
		}else{
			moveFieldToActive = false ; 
		}
	}
	// modif DOM
	elt_to_move.remove();
	if(moveFieldToActive){
		elt_to_move.find('.cust_field_val,.cust_field_xml').prop('disabled',false);
		$j("div.cust_field_area.active").append(elt_to_move);
	}else{
		elt_to_move.find('.cust_field_val,.cust_field_xml').prop('disabled',true);
		added = false ; 
		$j('.cust_field_area.inactive .customField2 .cust_field_row .cust_field_text').each(function(idx,elt){
			if(!($j(elt).text().toUpperCase() < elt_to_move.text().toUpperCase())){
				// $j(elt).parents('.customField2').insertBefore(elt_to_move);
				$j(elt_to_move).insertBefore($j(elt).parents('.customField2'));
				added = true ; 
				return false ; 
			}
		});
		
		if(!added){
			$j("div.cust_field_area.inactive").append(elt_to_move);
		}
	}
}

function switchAllChecked(elt_class){
	$j(".cust_field_area."+elt_class+" .customField2").each(function(idx,elt){
		if($j(elt).find('.cust_field_checker').prop('checked')){
			$j(elt).find('.cust_field_arrow_action').click() ; 
		}
	})
}

function exportCheckAllFields(className,elt){
	$j(className).find("input[type='checkbox']").prop('checked',$j(elt).prop('checked'));
}

