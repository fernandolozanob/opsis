var elements=[];

function dragImage(draggable){
    //alert(draggable.element.id);
    if(document.getElementById("selections")){
      if(document.getElementById("selections").style.display!="block") document.getElementById("selections").style.display = "block";
    }
    imgs=draggable.element.getElementsByTagName('img');
    if(imgs.length>1){
      imgs[0].style.display="none";
      imgs[1].style.display="block";
    }
}

function dropItem( dragitem,droparea,finished){
    if(dragitem.parentNode.parentNode.parentNode.id==droparea.id) return;
    split=dragitem.id.split("_");
	typeItem = split[0];
    idItem=split[1];
    if(droparea.id=="link_new_selection"){
      displayNewPanLeft(idItem,droparea);
    }else if (droparea.id.indexOf("classeur") == 0){
      var idCart = droparea.id.substr(8);
      add2folder(idCart,idItem,typeItem);
    }
  }

//creating Draggable and Droppable objects

function refreshDroppables() {
	if(document.getElementById('link_new_selection')){
		Droppables.remove('link_new_selection');
		Droppables.add( 'link_new_selection', { accept:'conteneur_dragable', hoverclass: 'hoverPanier',onDrop: dropItem } );
	}
	
	$j("div[id^='classeur']").each(function () {
		var idObj = $j(this).attr('id');
		Droppables.remove(idObj);
        Droppables.add(idObj, {  accept:'conteneur_dragable', hoverclass: 'hoverPanier',onDrop: dropItem } );
	});
}

window.onload = function() {

	if(elements){
		for (_u=0;_u<elements.length;_u++) {
			if(document.getElementById(elements[_u]))
				new Draggable(elements[_u], {ghosting:false,revert:true,scroll: window,onDrag:dragImage});
		}
	}
	
	refreshDroppables();
}
