//==========================================================================
	// MS - ce  code permet d'eviter que les fonctions n'existant pas sur tel ou tel browser ne déclenche des erreurs (console.log sous IE7 sans console de debug)

	(function() {
	  // Union of Chrome, Firefox, IE, Opera, and Safari console methods
	  var methods = ["assert", "assert", "cd", "clear", "count", "countReset",
		"debug", "dir", "dirxml", "dirxml", "dirxml", "error", "error", "exception",
		"group", "group", "groupCollapsed", "groupCollapsed", "groupEnd", "info",
		"info", "log", "log", "markTimeline", "profile", "profileEnd", "profileEnd",
		"select", "table", "table", "time", "time", "timeEnd", "timeEnd", "timeEnd",
		"timeEnd", "timeEnd", "timeStamp", "timeline", "timelineEnd", "trace",
		"trace", "trace", "trace", "trace", "warn"];
	  var length = methods.length;
	  var console = (window.console = window.console || {});
	  var method;
	  var noop = function() {};
	  while (length--) {
		method = methods[length];
		// define undefined methods as noops to prevent errors
		if (!console[method])
		  console[method] = noop;
	  }
	})();
//==========================================================================
if (!Function.prototype.bind) {
  Function.prototype.bind = function(oThis) {
    if (typeof this !== 'function') {
      // closest thing possible to the ECMAScript 5
      // internal IsCallable function
      throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
    }

    var aArgs   = Array.prototype.slice.call(arguments, 1),
        fToBind = this,
        fNOP    = function() {},
        fBound  = function() {
          return fToBind.apply(this instanceof fNOP && oThis
                 ? this
                 : oThis,
                 aArgs.concat(Array.prototype.slice.call(arguments)));
        };

    fNOP.prototype = this.prototype;
    fBound.prototype = new fNOP();

    return fBound;
  };
}

// Objet JS module de detection de l'environnement 

// Constructeur detectEnv
function detectEnv(sync) {
	this.init(sync);	
}

detectEnv.prototype.init = function(sync){
	this.ready = false ;
	
	if(typeof sync == 'undefined'){
		this.sync = false;
	}else{
		this.sync = sync ; 
	}
	
	this.browser = null ;
	this.browserVersion = null ;
	this.env_OS=null ; 
	this.techs={
		html5 : null,
		qt : null,
		flash : null
	} ; 
	
	if(this.getFromSess() == false){
		this.detectBrowserOs();
		this.detectTechs();
		this.storeInSess();
	}
	// alert(navigator.userAgent+"\n"+this.browser+" "+this.browserVersion+"\n"+this.env_OS+" \n"+this.techs.html5+" "+this.techs.qt+" "+this.techs.flash);
	
}	
	

detectEnv.prototype.detectBrowserOs = function(){
	this.BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
		|| this.searchVersion(navigator.appVersion)
		|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
				  {
				  string: navigator.userAgent,
				  subString: "Chrome",
				  identity: "Chrome"
				  },
				  { 	string: navigator.userAgent,
				  subString: "OmniWeb",
				  versionSearch: "OmniWeb/",
				  identity: "OmniWeb"
				  },
				  {
				  string: navigator.vendor,
				  subString: "Apple",
				  identity: "Safari",
				  versionSearch: "Version"
				  },
				  {
				  prop: window.opera,
				  identity: "Opera"
				  },
				  {
				  string: navigator.vendor,
				  subString: "iCab",
				  identity: "iCab"
				  },
				  {
				  string: navigator.vendor,
				  subString: "KDE",
				  identity: "Konqueror"
				  },
				  {
				  string: navigator.userAgent,
				  subString: "Firefox",
				  identity: "Firefox"
				  },
				  {
				  string: navigator.vendor,
				  subString: "Camino",
				  identity: "Camino"
				  },
				  {		// for newer Netscapes (6+)
				  string: navigator.userAgent,
				  subString: "Netscape",
				  identity: "Netscape"
				  },
				  {
				  string: navigator.userAgent,
				  subString: "MSIE",
				  identity: "Explorer",
				  versionSearch: "MSIE"
				  },
				  {// IE 11
				  string: navigator.userAgent,
				  subString: "Trident",
				  identity: "Explorer",
				  versionSearch: "rv"
				  },
				  {
				  string: navigator.userAgent,
				  subString: "Gecko",
				  identity: "Mozilla",
				  versionSearch: "rv"
				  },
				  { 		// for older Netscapes (4-)
				  string: navigator.userAgent,
				  subString: "Mozilla",
				  identity: "Netscape",
				  versionSearch: "Mozilla"
				  }
				  ],
		dataOS : [
				  {
				  string: navigator.platform,
				  subString: "Win",
				  identity: "Windows"
				  },
				  {
				  string: navigator.platform,
				  subString: "Mac",
				  identity: "Mac"
				  },
				  {
				  string: navigator.userAgent,
				  subString: "iPhone",
				  identity: "iPhone"
				  },
				  {
				  string: navigator.userAgent,
				  subString: "iPad",
				  identity: "iPad"
				  },
				  {
				  string: navigator.userAgent,
				  subString: "Android",
				  identity: "Android"
				  },
				  {
				  string: navigator.platform,
				  subString: "Linux",
				  identity: "Linux"
				  }
				  ]
		
	};

	this.BrowserDetect.init();
	this.browser = this.BrowserDetect.browser;
	this.browserVersion = this.BrowserDetect.version;
	this.env_OS= this.BrowserDetect.OS;
}
	
detectEnv.prototype.detectTechs = function(){
	
	this.techs ={
	"html5" : this.haveHTML5(),
	"qt" :  this.haveQT(),
	"flash" : this.haveFlash()
	}
	
	return true ; 
	
}
detectEnv.prototype.haveQT = function () {

	var isInstalled = false;
	
	if (typeof window.ActiveXObject!='undefined' &&  window.ActiveXObject.prototype ) {
		var control = null;
		try {
			control = new ActiveXObject('QuickTime.QuickTime');
		} catch (e) {}
		if (control) {
			isInstalled = true; // In case QuickTimeCheckObject.QuickTimeCheck does not exist

		} else {
			try {
				control = new ActiveXObject('QuickTimeCheckObject.QuickTimeCheck'); // This generates a user prompt in Internet Explorer 7
			} catch (e) {}
			if (control) {
				isInstalled = true; // In case QuickTime.QuickTime does not exist
			}
		}
	}

	if(isInstalled) {
		haveqt = true;
	}
	
	if (navigator.plugins) {
		for (i=0; i < navigator.plugins.length; i++ ) {
			if (navigator.plugins[i].name.indexOf("QuickTime") >= 0)
				haveqt = true;
		}
	}

	if ((navigator.appVersion.indexOf("Mac") > 0) && (navigator.appName.substring(0,9) == "Microsoft") && (parseInt(navigator.appVersion) < 5) )
		haveqt = true;

	
		
	else if (navigator.appVersion.indexOf("Linux") > 0 
			|| navigator.appVersion.indexOf("X11") > 0 || navigator.appVersion.indexOf("Trident") > 0 // test qui devrait matcher si IE11 
			|| navigator.appVersion.indexOf("Opera") > 0 // test qui devrait matcher si Opera 
			|| navigator.appVersion.indexOf("MSIE") > 0 // test qui devrait matcher si IE<11 
			|| (navigator.userAgent.indexOf("Firefox") > 0 && navigator.userAgent.indexOf("Windows") > 0)){ // test qui devrait matcher si Firefox PC
		haveqt = false;
	}
	if(typeof haveqt =='undefined'){
		haveqt = false ; 
	}
	
	return haveqt;
}


// MS MaJ 21 de Firefox active par défaut le support mp4 sur firefox PC
detectEnv.prototype.haveHTML5 = function () {
	var havehtml5 = true;
	v = document.createElement("video");
	if (v.canPlayType){
		if (v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')==''){
			havehtml5 = false;
		}}
	else
		havehtml5 = false;
	
	return havehtml5;
}

detectEnv.prototype.haveFlash = function(){
	var hasFlash = false;
	try {	
		if(navigator.plugins && navigator.plugins.length > 0){
		var fo = (navigator.mimeTypes && navigator.mimeTypes['application/x-shockwave-flash']) ? navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin : 0; 
		if(fo) 
			hasFlash = true; 
		}else if(this.browser == 'Explorer' && this.browserVersion <= 10){
			var fo = null;
			try { fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash'); } catch (ex) { fo = false }
			// if (fo != null) {
				// var fV;
				// try { fV = fo.GetVariable("$version"); } catch (err) { return false; }
				// v = fV.replace(/^.*?([0-9]+,[0-9]+).*$/, '$1').split(',');
			// }
			if(fo) 
			hasFlash = true; 
		}
	}catch(e){
		if(navigator.mimeTypes ['application/x-shockwave-flash'] != undefined)
			hasFlash = true; 
	}
	return hasFlash;
}

detectEnv.prototype.storeInSess =function () {
	// ajax vers composant qui stock en session	
	var xmlhttp;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(this.browser == null || this.browserVersion == null || this.env_OS == null){
		return false;  
	}
	if(this.browser == null || this.browserVersion == null || this.env_OS == null){
		return false;  
	}
	
	xml = "<xml>"+
	"	<browser>"+this.browser+"</browser>"+
	"	<browserVersion>"+this.browserVersion+"</browserVersion>"+
	"	<env_OS>"+this.env_OS+"</env_OS>"+
	"	<techs>"+
	"		<html5>"+this.techs.html5+"</html5>"+
	"		<qt>"+this.techs.qt+"</qt>"+
	"		<flash>"+this.techs.flash+"</flash>"+
	"	</techs>"+
	"</xml>";
	
	
	params = "xml="+encodeURIComponent(xml);
	
	// ajout d'une partie random qui permettra d'éviter le cache android.
	scrambler = new Date().getTime();
	if(this.sync){
		xmlhttp.open("POST","empty.php?urlaction=detectEnv&action=store_env&scrambler="+scrambler,false);
	}else{
		xmlhttp.open("POST","empty.php?urlaction=detectEnv&action=store_env&scrambler="+scrambler,true);
	}
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	function endStore(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			this.ready = true ; 
			// console.log("response "+xmlhttp.responseText);
		}else{
			// console.log("error storing in sess, http req status :  "+xmlhttp.status+" "+xmlhttp.readyState);
		}
	}
	
	if(this.sync){
		xmlhttp.send(params);
		endStore();
	}else{
		xmlhttp.onreadystatechange= endStore.bind(this);
		xmlhttp.send(params);
	}
}

detectEnv.prototype.getFromSess = function(){
	// ajax recup les infos stockées en session
	// si reponds false || empty
	// => lance les detections et le store
	// sinon alimente les valeurs depuis les infos reçues
	var xmlhttp;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	// ajout d'une partie random qui permettra d'éviter le cache android.
	scrambler = new Date().getTime();
	xmlhttp.open("GET","empty.php?urlaction=detectEnv&action=get_env&scrambler="+scrambler,false);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send();
	
	
	// Pour l'instant, on utilise la version synchrone de get_env qui permet de récup les infos d'environnement depuis la session. 
	// si on devait rendre l'appel ajax async, alors on devrait utiliser "onreadystatechange", je laisse donc le code commenté :
	
	// xmlhttp.onreadystatechange=function()
	// {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			if(xmlhttp.responseText.indexOf('empty') === 0 || xmlhttp.responseText.indexOf('false') === 0){
				return false;
			}else{
				if (window.DOMParser)
				{
				  parser=new DOMParser();
				  xmlDoc=parser.parseFromString(xmlhttp.responseText,"text/xml");
				}
				else // Internet Explorer
				{
				  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
				  xmlDoc.async=false;
				  xmlDoc.loadXML(xmlhttp.responseText); 
				}
				try{
					this.browser = xmlDoc.getElementsByTagName('browser')[0].firstChild.nodeValue;
					this.browserVersion = xmlDoc.getElementsByTagName('browserVersion')[0].firstChild.nodeValue;
					this.env_OS = xmlDoc.getElementsByTagName('env_OS')[0].firstChild.nodeValue;
					this.techs.html5 = xmlDoc.getElementsByTagName('html5')[0].firstChild.nodeValue;
					this.techs.qt = xmlDoc.getElementsByTagName('qt')[0].firstChild.nodeValue;
					this.techs.flash = xmlDoc.getElementsByTagName('flash')[0].firstChild.nodeValue;
					
					// MS - 01.06.15 - pour IE7, on est obligé de faire le cast string => bool quand on récup les infos depuis le XML ... 
					// il y a quelques versions plus élégantes, mais aucune 100% compatible IE7 ...
					if(this.techs.html5 == "true"){
						this.techs.html5 = true; 
					}else if (this.techs.html5 == "false"){
						this.techs.html5 = false ; 
					}
					if(this.techs.qt == "true"){
						this.techs.qt = true; 
					}else if (this.techs.qt == "false"){
						this.techs.qt = false ; 
					}
					if(this.techs.flash == "true"){
						this.techs.flash = true; 
					}else if (this.techs.flash == "false"){
						this.techs.flash = false ; 
					}
					
					this.ready = true ; 
					
					return true ; 
				}catch(e){
					return false; 
				}
			}
		}
	// }.bind(this)
	// return true ;
}


detectEnv.prototype.outputEnvJS = function(){
	return {
		'output' : '1',
		'browser' : this.browser,
		'browserVersion' : this.browserVersion,
		'env_OS' : this.env_OS,
		'techs' : {
			'html5' : this.techs.html5,
			'qt' : this.techs.qt,
			'flash' : this.techs.flash
		}
	}
}


// var OW_env_user = new detectEnv(true);

// =======================================================================================
// MÉTHODES D'INCLUSIONS : 
// inclusion dans general.html classique : 
//	<script type="text/javascript" src="<libUrl>webComponents/opsis/fonctionsJavascript.js"></script>

// sinon ajouter l'un des deux codes suivant : 

// MS - si OW_env_user n'est pas encore défini => à priori n'a pas été défini dans general.html => on tente de charger dynamiquement
/*
// inclusion dynamique & asynchrone du script 
// toute utilisation de la variable OW_env_user doit attendre que OW_env_user.ready soit true. 
// ou etre dans un jquery.ready
if(typeof OW_env_user == 'undefined' && typeof detectEnv != 'object' ){
	var fileref=document.createElement('script');
	fileref.setAttribute("type","text/javascript");
	fileref.setAttribute("src", "<?=libUrl?>webComponents/opsis/detectEnv.js");
	document.getElementsByTagName("head")[0].appendChild(fileref);
}*/

// inclusion dynamique synchrone => à la fin du script, OW_env_user sera défini, mais peut cause des "bloquages" dans l'execution => appeler ce code dans une fonction qui réponds à une action utilisateur semble moins genant
/*if(typeof OW_env_user == 'undefined' && typeof detectEnv != 'object' ){
	var xmlhttp;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET","<?=libUrl?>webComponents/opsis/detectEnv.js",false);
	xmlhttp.send();
	eval(xmlhttp.responseText);
}*/
// ==> il faut soit déclencher en synchrone au cours d'une action qui prends déja du temps (type chargement du player ),
// soit déclencher en asynchrone, mais chaque usage du OW_env_user doit vérifier sa présence  & son initialisation avant de l'utiliser....