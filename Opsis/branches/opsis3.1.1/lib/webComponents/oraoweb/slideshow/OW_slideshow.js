
function OW_slideshow(my_div,options)
{

	// attributs
	this.current_img=0;
	this.nb_images=0;
	this.is_playing=false;
	this.url_images=new Array();
	this.url_miniature=new Array();
	this.txt_legende=new Array();

	// paramatres de la classe
	this.main_container=$(my_div);

	this.url='';
	this.xml='';
	this.width='320px';
	this.height='240px';
	this.name='';
	this.autoplay=false;
	this.loop=false;
	this.language='fr';
	this.debug_mode=false;
	this.stepping_frequency=2;

	this.code_display='';

	this.toggle_thumbs=true;
	this.script_genere_miniatures='';

	this.toggle_buttons=false;
	this.toggle_fullscreen=false;

	this.nb_dsp_img=1;



	this.tooltips_fr={};
	this.tooltips_en={};

	this.init(options);
}

//Initialise le diaporama suivant les options passées
OW_slideshow.prototype.init=function(options)
{
	if (options['url']!=null)
		this.url=options['url'];

		
	if (options['diapo_precis']!=null){
		if(options['diapo_precis']){
			this.diapo_precis = true ; 
			this.url += "&precis=1";
		}
	}
		
		
	if (options['xml']!=null)
		this.xml=options['xml'];

	if (options['width']!=null)
		this.width=options['width'];

	if (options['height']!=null)
		this.height=options['height'];

	if (options['name']!=null)
		this.name=options['name'];

	if (options['autoplay']!=null)
		this.autoplay=options['autoplay'];

	if (options['loop']!=null)
		this.loop=options['loop'];

	if (options['language']!=null)
		this.language=options['language'];

	if (options['debug_mode']!=null)
		this.debug_mode=options['debug_mode'];

	if (options['stepping_frequency']!=null)
		this.stepping_frequency=options['stepping_frequency'];

	if (options['code_display']!=null)
		this.code_display=options['code_display'];

	if (options['toggle_thumbs']!=null)
		this.toggle_thumbs=options['toggle_thumbs'];

	if (options['script_genere_miniatures']!=null)
		this.script_genere_miniatures=options['script_genere_miniatures'];

	if (options['dossier_images']!=null)
		this.dossier_images=options['dossier_images'];

	if (options['toggle_buttons']!=null)
		this.toggle_buttons=options['toggle_buttons'];

	if (options['nb_dsp_img']!=null)
		this.nb_dsp_img=options['nb_dsp_img'];

	if (options['tooltips_fr']!=null)
		this.tooltips_fr=options['tooltips_fr'];

	if (options['tooltips_en']!=null)
		this.tooltips_en=options['tooltips_en'];

	// on recupere les données au format XML
	if(this.xml!='')
	{
		var parser;
		var xmlDoc;
		if (typeof(DOMParser)!='undefined')
		{
			parser=new DOMParser();
			xmlDoc=parser.parseFromString(this.xml,"text/xml");
		}
		else
		{
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async=false;
			xmlDoc.loadXML(this.xml);
		}
		this.traiterFluxXml(xmlDoc);
	}
	else if (this.url!='')
	{
		$ajax=new Ajax.Request(this.url,
		{
			asynchronous:false,
			onSuccess : this.traiterFluxXml.bind(this)
		});
	}

	if (this.code_display=='')
	{
		this.code_display='<div id="ow_boutons">'+
						'<div id="ow_pager">&nbsp;</div>'+
						'<a class = "btn" id = "ow_bouton_go_to_beginning"></a>'+
						'<a class = "btn" id = "ow_bouton_step_rewind"></a>'+
						'<a class = "btn_play" id = "ow_bouton_play_pause"></a>'+
						'<a class = "btn" id = "ow_bouton_step_forward"></a>'+
						'<a class = "btn" id = "ow_bouton_go_to_end"></a>'+
						'<a style="display : none ;" class="btn" id = "ow_bouton_unzoom"></a>'+
						'<a style="display : none ;" class="btn" id = "ow_bouton_zoom"></a>'+
						'<a class = "btn" id = "ow_bouton_fullscreen"></a>'+
						'</div>'+
						'<div id="ow_image">&nbsp;</div>'+
						'<div id="ow_legend">&nbsp;</div>'+
						'<div id="ow_thumbs">&nbsp;</div>';
	}

	this.display();
	this.registerEvents();
}

OW_slideshow.prototype.traiterFluxXml=function(str)
{
	var elements_image;

	if (str.responseXML==null)
		elements_image=str.getElementsByTagName('item');
	else
		elements_image=str.responseXML.getElementsByTagName('item');

	for(i=0;i<elements_image.length;i++)
	{
		this.url_images.push(elements_image[i].getElementsByTagName('image')[0].firstChild.nodeValue);

		if (elements_image[i].getElementsByTagName('thumb')[0]!=null)
			this.url_miniature.push(elements_image[i].getElementsByTagName('thumb')[0].firstChild.nodeValue);
		else
			this.url_miniature.push(this.script_genere_miniatures+'?kr=1&w=67&h=50&image='+(this.url_images[i].replace(this.dossier_images,''))); //makeVignette?kr=1&w=&h=
			
		if (elements_image[i].getElementsByTagName('legend')[0]!=null)
			this.txt_legende.push(elements_image[i].getElementsByTagName('legend')[0].firstChild.nodeValue);
		else
			this.txt_legende.push('');
	}
	this.nb_images=elements_image.length;
}

//fonction qui permet de redimentionner le cadre de l'image
OW_slideshow.prototype.resizeCadreImage=function()
{

	var hauteur_thumbs=0;
	var hauteur_boutons=0;
	var marges_div_images=0;
	var hauteur_legende=0;
	
	var hauteur_conteneur_alt=this.main_container.getHeight()-(parseInt(this.main_container.getLayout().get('border-top'))+parseInt(this.main_container.getLayout().get('border-bottom')))-(parseInt(this.main_container.getLayout().get('padding-top'))+parseInt(this.main_container.getLayout().get('padding-bottom')));
	
	var hauteur_conteneur = this.main_container.offsetHeight;
	
	// console.log("old hauteur contenur : "+hauteur_conteneur_alt +" new : "+hauteur_conteneur);
	
	if ($('ow_boutons') && $('ow_boutons').getStyle('display')=='block')
		hauteur_boutons=$('ow_boutons').getHeight()+parseInt($('ow_boutons').getLayout().get('margin-top'))+parseInt($('ow_boutons').getLayout().get('margin-bottom'));

	if ($('ow_thumbs') && $('ow_thumbs').getStyle('display')=='block')
		hauteur_thumbs=$('ow_thumbs').getHeight()+parseInt($('ow_thumbs').getLayout().get('margin-top'))+parseInt($('ow_thumbs').getLayout().get('margin-bottom'));
		
	if ($('ow_legend') && $('ow_legend').getStyle('display')=='block')
		hauteur_legende=$('ow_legend').getHeight();

	//marges_div_images=parseInt($('ow_image').getStyle('border-top'))+parseInt($('ow_image').getStyle('border-bottom'));//+parseInt($('ow_image').getLayout().get('border-bottom'));
	marges_div_images=parseInt($('ow_image').getLayout().get('border-bottom'))+parseInt($('ow_image').getLayout().get('border-top'))+parseInt($('ow_image').getLayout().get('padding-bottom'))+parseInt($('ow_image').getLayout().get('padding-top'));

	var hauteur_div_images=hauteur_conteneur-hauteur_boutons-hauteur_thumbs-hauteur_legende-marges_div_images;
	
	// console.log("ResizeCadreImage hauteur calculée : "+hauteur_div_images);
	if(this.diapo_precis){
		$('ow_obj_image_wrapper').setStyle({height: hauteur_div_images+'px'});
	}else{
		$('ow_image').setStyle({height: hauteur_div_images+'px'});
	}
}

//affiche le player
OW_slideshow.prototype.display=function()
{
	this.main_container.setStyle({width : this.width,height:this.height});
	this.main_container.insert(this.code_display);
	
	if(this.diapo_precis){
		$('ow_bouton_unzoom').style.display="";
		$('ow_bouton_zoom').style.display="";
	}
	
	if ($('ow_pager'))
		$('ow_pager').innerHTML='<input type="text" value="1" id="ow_champ_pager" size="1" /> / '+this.nb_images;
		
	if ($('ow_legend') && this.txt_legende[0]!='')
	{
		$('ow_legend').show();
		$('ow_legend').innerHTML=this.txt_legende[0];
	}
	else
		$('ow_legend').hide();


	if (this.toggle_thumbs)
		this.displayThumbnails();
	else
	{
		this.displayThumbnails();
		$('ow_thumbs').hide();
	}
		
	
	if(this.diapo_precis){
		$('ow_image').innerHTML='<div id="ow_img_aff">&nbsp;</div>';
		$('ow_img_aff').setOpacity(0);
		$('ow_img_aff').innerHTML='<div id="ow_obj_image_wrapper"><img id="ow_obj_image" src="'+this.url_images[0]+'" alt="image" /></div>';
		

		
		$('ow_obj_image_wrapper').style.overflow="auto";
		this.resizeCadreImage();
	}else{
		this.resizeCadreImage();
		$('ow_image').innerHTML='<div id="ow_img_aff">&nbsp;</div>';
		$('ow_img_aff').setOpacity(0);

		$('ow_img_aff').innerHTML='<img id="ow_obj_image" src="'+this.url_images[0]+'" alt="image" />';
	}
	
	//this.goToImage(0);
}

//Enregistre les événements à observer
OW_slideshow.prototype.registerEvents=function()
{
	Event.observe(window,'resize',function()
	{
		var hauteur_espace_image=$('ow_image').getHeight()-(parseInt($('ow_image').getLayout().get('border-bottom'))+parseInt($('ow_image').getLayout().get('border-top')))-(parseInt($('ow_image').getLayout().get('padding-bottom'))+parseInt($('ow_image').getLayout().get('padding-top')));
		var largeur_espace_image=$('ow_image').getWidth()-(parseInt($('ow_image').getLayout().get('border-left'))+parseInt($('ow_image').getLayout().get('border-right')))-(parseInt($('ow_image').getLayout().get('padding-left'))+parseInt($('ow_image').getLayout().get('padding-right')));
		var ratio_espace=largeur_espace_image/hauteur_espace_image;

		var hauteur_image=$$('#ow_img_aff img')[0].getHeight();
		var largeur_image=$$('#ow_img_aff img')[0].getWidth();
		var ratio_img=largeur_image/hauteur_image;

		var hauteur_finale=0;
		var largeur_finale=0;

		if (ratio_img<ratio_espace)
		{
			//hauteur au max et largeur recaculée
			hauteur_finale=hauteur_espace_image;
			largeur_finale=hauteur_espace_image*ratio_img;
			marge_haut=0;

		}
		else
		{
			//largeur au max et hauteur calculée
			largeur_finale=largeur_espace_image;
			hauteur_finale=largeur_espace_image*(1/ratio_img);

			marge_haut=(hauteur_espace_image-hauteur_finale)/2;
		}
		

		if(this.diapo_precis){
			this.goToImage(this.current_img);
		}else{
		
		//$('ow_img_aff').setStyle({marginTop:marge_haut+'px'});
		
		$$('#ow_img_aff img')[0].setStyle({height : hauteur_finale+'px',width:largeur_finale+'px',marginTop:marge_haut+'px'});

		$('ow_img_aff').appear({from:0.0,to:1.0,duration:0});
		}
	}.bind(this));
	
	this.addListener($('ow_bouton_play_pause'),'click',this.toggleSlideshow.bind(this));

	this.addListener($('ow_bouton_step_rewind'),'click',this.goPrevious.bind(this));
	this.addListener($('ow_bouton_step_forward'),'click',this.goNext.bind(this));

	this.addListener($('ow_bouton_go_to_beginning'),'click',this.goFirst.bind(this));
	this.addListener($('ow_bouton_go_to_end'),'click',this.goLast.bind(this));
	if(this.diapo_precis){
		this.addListener($('ow_bouton_unzoom'),'click',this.unzoom.bind(this));
		this.addListener($('ow_bouton_zoom'),'click',this.zoom.bind(this));
	}
	this.addListener($('ow_bouton_fullscreen'),'click',this.toggleFullscreen.bind(this));

	if ($('ow_champ_pager'))
		this.addListener($('ow_champ_pager'),'change',this.goToImage.bind(this));

	this.addListener($('ow_obj_image'),'load',this.resizeImage.bind(this));

	this.addListener($('ow_thumbs'),'click',
		function(event)
		{
			if (event.element().getAttribute('name')=='ow_thumb')
			{
				this.stopSlideshow();
				this.current_img=parseInt(event.element().getAttribute('usemap'));
				this.goToImage(parseInt(event.element().getAttribute('usemap')));
			}
		}.bind(this));
		
		
	requirejs(['jqueryContextMenu/jquery.contextMenu','jqueryContextMenu/jquery.ui.position.min'],function(){
		requireCSS(ow_const.libUrl+'webComponents/jqueryContextMenu/jquery.contextMenu.css');
		// MS - 08.04.15 permet d'éviter l'ouverture du menu contextuel html5 .		
		$j.contextMenu({
			selector : "#"+this.main_container.id,  // main_container est un élément js natif,
			callback: function(key, options) {
				switch(key){
					case 'opso' : 
						window.open('http://www.opsomai.com');
					break ;
					default :
					return false ; 
				}
			}.bind(this),
			items: {
			   'opso' : {name: "Opsis Media v"+ow_const.coreVersion+" &copy;"+ow_const.kAnneeCopyright+" Opsomai", icon : 'opso'}
			}
		});
	}.bind(this));
}

//Ajoute un événement
OW_slideshow.prototype.addListener=function(element,event,handler)
{
	element.observe(event,handler);
}

//Joue le diaporama
OW_slideshow.prototype.playSlideshow=function()
{
	this.is_playing=true;
	this.thread_affichage=new PeriodicalExecuter(this.goNextPeriodique.bind(this),this.stepping_frequency);


	$('ow_bouton_play_pause').addClassName('btn_pause');
	$('ow_bouton_play_pause').removeClassName('btn_play');
}

//Stoppe le diaporama
OW_slideshow.prototype.stopSlideshow=function()
{
	this.is_playing=false;

	if (this.thread_affichage)
		this.thread_affichage.stop();

	$('ow_bouton_play_pause').removeClassName('btn_pause');
	$('ow_bouton_play_pause').addClassName('btn_play');
}

//Joue ou arrête la vidéo selon le cas
OW_slideshow.prototype.toggleSlideshow=function(event)
{
	if (!this.is_playing)
		this.playSlideshow();
	else
		this.stopSlideshow();
}

//Va à l’image désignée
OW_slideshow.prototype.goToImage=function(id_img)
{
	if (typeof(id_img)!='number')
	{
		id_img=parseInt($('ow_champ_pager').value)-1;
		this.current_img=id_img
	}

	if (id_img>=this.nb_images)
		id_img=this.nb_images-1;
	else if (id_img<0)
		id_img=0;

	if ($('ow_champ_pager'))
		$('ow_champ_pager').value=id_img+1;
		
	if ($('ow_legend') && this.txt_legende[id_img]!='')
	{
		$('ow_legend').show();
		$('ow_legend').innerHTML=this.txt_legende[id_img];
	}
	else
		$('ow_legend').hide();

	if (this.toggle_thumbs)
	{
		tableau_thumbs=$$('#ow_thumbs img[name="ow_thumb"]');
		for (i=0;i<tableau_thumbs.length;i++)
		{
			tableau_thumbs[i].removeClassName('ow_miniature_selected');
		}
		$$('#ow_thumbs img[usemap="'+id_img+'"]')[0].addClassName('ow_miniature_selected');

		var offset_image=(parseInt($$('#ow_container_thumbs tr td div[name="cadre_thumb"]')[id_img].getWidth())+parseInt($$('#ow_container_thumbs tr td')[0].getLayout().get('padding-left'))+parseInt($$('#ow_container_thumbs tr td')[0].getLayout().get('padding-right')))*id_img;
		var marge_gauche=0;

		if ($('ow_container_thumbs').getLayout().get('margin-left')!=null)
			marge_gauche=parseInt($('ow_container_thumbs').getLayout().get('margin-left'));

		var rel_offset_left=offset_image+marge_gauche;
		var rel_offset_right=((offset_image/id_img)*(id_img+1))+marge_gauche+parseInt($$('#ow_container_thumbs tr td')[0].getLayout().get('padding-left'))+parseInt($$('#ow_container_thumbs tr td')[0].getLayout().get('padding-right'));
		var largeur_dispo=parseInt($('ow_thumbs').getWidth())-(parseInt($('ow_thumbs').getLayout().get('border-left'))+parseInt($('ow_thumbs').getLayout().get('border-right')))

		if (rel_offset_left<0)
		{
			var toto=parseInt($('ow_container_thumbs').getWidth())-(parseInt($('ow_thumbs').getWidth())-parseInt($('ow_thumbs').getLayout().get('border-left'))-parseInt($('ow_thumbs').getLayout().get('border-right'))-parseInt($('ow_thumbs').getLayout().get('padding-left'))-parseInt($('ow_thumbs').getLayout().get('padding-right')));
			this.slider_thumbs.setValue((offset_image/toto)*100);
		}
		else if (rel_offset_right>largeur_dispo)
		{
			var toto=parseInt($('ow_container_thumbs').getWidth())-(parseInt($('ow_thumbs').getWidth())-parseInt($('ow_thumbs').getLayout().get('border-left'))-parseInt($('ow_thumbs').getLayout().get('border-right'))-parseInt($('ow_thumbs').getLayout().get('padding-left'))-parseInt($('ow_thumbs').getLayout().get('padding-right')));
			this.slider_thumbs.setValue(((((offset_image/id_img)*(id_img+1))-largeur_dispo+parseInt($$('#ow_container_thumbs tr td')[0].getLayout().get('padding-left'))+parseInt($$('#ow_container_thumbs tr td')[0].getLayout().get('padding-right')))/toto)*100);
		}
	}
	
	this.resizeCadreImage();

	//$('ow_img_aff').fade({from:0.0,to:1.0,duration:0.5});
	if(!this.diapo_precis){
		$('ow_img_aff').setOpacity(0);
		$('ow_obj_image').setAttribute('style','');
		$('ow_obj_image').removeAttribute('style');
		$('ow_obj_image').src=this.url_images[id_img];
		// $('ow_img_aff').setOpacity(0);
	}else{
		$('ow_img_aff').setOpacity(0);
		$('ow_obj_image').src=this.url_images[id_img];		
	}
}

OW_slideshow.prototype.setLimitant = function(){
	$('ow_obj_image').style.width="";
	$('ow_obj_image').style.height="";

	cadre_w = $('ow_obj_image_wrapper').getWidth();
	cadre_h = $('ow_obj_image_wrapper').getHeight();
	
	img_h=$('ow_obj_image').getHeight();
	img_w=$('ow_obj_image').getWidth();
	// !this.toggle_fullscreen &&
	
	if( cadre_w/cadre_h < img_w/img_h){
		 this.limitant ="w";
		$('ow_obj_image').style.width  = "99%";
		$('ow_obj_image').style.height  = "";
	}else{
		this.limitant ="h";
		$('ow_obj_image').style.width = "";
		$('ow_obj_image').style.height = "99%";
	}	
}

OW_slideshow.prototype.resizeImage=function()
{	
	if(this.diapo_precis == true ){
		// if(typeof(this.limitant) == "undefined"){
			this.setLimitant();

		// }
		try{
			Effect.Queues.get('global').invoke('cancel');
		}catch(e){
			console.log("crash while clearing queues : "+e);
		}
		$('ow_img_aff').appear({from:0.0,to:1.0,duration:0.2});
	
		// attr = $('ow_obj_image_wrapper').style;
		// $('ow_obj_image_wrapper').style="";
		// $('ow_obj_image_wrapper').style=attr;


		
	}else{
		// var hauteur_espace_image=$('ow_image').getHeight()-(parseInt($('ow_image').getLayout().get('border-bottom'))+parseInt($('ow_image').getLayout().get('border-top')))-(parseInt($('ow_image').getLayout().get('padding-bottom'))+parseInt($('ow_image').getLayout().get('padding-top')));
		// var largeur_espace_image=$('ow_image').getWidth()-(parseInt($('ow_image').getLayout().get('border-left'))+parseInt($('ow_image').getLayout().get('border-right')))-(parseInt($('ow_image').getLayout().get('padding-left'))+parseInt($('ow_image').getLayout().get('padding-right')));
		
		var hauteur_espace_image=document.getElementById("ow_image").offsetHeight;
		var largeur_espace_image=document.getElementById("ow_image").offsetWidth;
		
		var ratio_espace=largeur_espace_image/hauteur_espace_image;

		
		if(document.querySelector){
			var hauteur_image=document.querySelector('#ow_img_aff img').offsetHeight;
			var largeur_image=document.querySelector('#ow_img_aff img').offsetWidth;
		}else{
			var hauteur_image=$$('#ow_img_aff img')[0].getHeight();
			var largeur_image=$$('#ow_img_aff img')[0].getWidth();
		}
		var ratio_img=largeur_image/hauteur_image;

		var hauteur_finale=0;
		var largeur_finale=0;

		// console.log("resizeImage, hauteur_espace_image "+hauteur_espace_image+" largeur_espace_image : "+largeur_espace_image+" ratio "+ratio_espace);
		// console.log("resizeImage, hauteur_image "+hauteur_image+" largeur_image : "+largeur_image+" ratio "+ratio_img);
		

		if (ratio_img<ratio_espace)
		{
			//hauteur au max et largeur recaculée
			hauteur_finale=hauteur_espace_image;
			largeur_finale=hauteur_espace_image*ratio_img;
			marge_haut=0;

		}
		else
		{
			//largeur au max et hauteur calculée
			largeur_finale=largeur_espace_image;
			hauteur_finale=largeur_espace_image*(1/ratio_img);

			marge_haut=(hauteur_espace_image-hauteur_finale)/2;
		}

		// console.log("largeur_finale : "+largeur_finale+" hauteur_finale "+ hauteur_finale);
		
		//$('ow_img_aff').setStyle({marginTop:marge_haut+'px'});
		$$('#ow_img_aff img')[0].setStyle({height : hauteur_finale+'px',width:largeur_finale+'px',marginTop:marge_haut+'px'});
		
		$('ow_img_aff').appear({from:0.0,to:1.0,duration:0.5});
	}
}

//Va à l'image suivante
OW_slideshow.prototype.goNext=function ()
{
	this.stopSlideshow();

	this.current_img++;

	if(this.current_img>=this.nb_images && this.loop==true)
		this.current_img=0;
	else if (this.current_img>=this.nb_images)
		this.current_img=this.nb_images-1;

	this.goToImage(this.current_img);
}

//Va à l'image suivante
OW_slideshow.prototype.goNextPeriodique=function ()
{
	this.current_img++;

	if(this.current_img>=this.nb_images && this.loop==true)
		this.current_img=0;
	else if (this.current_img>=this.nb_images)
		this.current_img=this.nb_images-1;

	this.goToImage(this.current_img);
}

//Va à l'image precedente
OW_slideshow.prototype.goPrevious=function ()
{
	this.stopSlideshow();

	this.current_img--;

	if (this.current_img<0 && this.loop==true)
		this.current_img=this.nb_images-1;
	else if (this.current_img<0)
		this.current_img=0;

	this.goToImage(this.current_img);
}


//Va à la première image
OW_slideshow.prototype.goFirst=function(event)
{
	this.stopSlideshow();

	this.current_img=0;
	this.goToImage(0);
}

//Va à la dernière image
OW_slideshow.prototype.goLast=function(event)
{
	this.stopSlideshow();

	this.current_img=this.nb_images-1
	this.goToImage(this.nb_images-1);
}



OW_slideshow.prototype.unzoom=function(event){
	if(this.limitant == "h"){
		newlimit = (parseInt($('ow_obj_image').style.height,10)-50);
		if(newlimit < 99){
			newlimit = 99;
		}
		$('ow_obj_image').style.height = newlimit +"%";
	}else if(this.limitant = "w"){
		newlimit = (parseInt($('ow_obj_image').style.width,10)-50);
		if(newlimit < 99){
			newlimit = 99;
		}
		$('ow_obj_image').style.width = newlimit+"%";
	}
}


OW_slideshow.prototype.zoom=function(event){
	if(this.limitant == "h"){
		newlimit = (parseInt($('ow_obj_image').style.height,10)+50);
		if(newlimit > 399){
			newlimit = 399;
		}
		$('ow_obj_image').style.height = newlimit +"%";
	}else if(this.limitant = "w"){
		newlimit = (parseInt($('ow_obj_image').style.width,10)+50);
		if(newlimit > 399){
			newlimit = 399;
		}
		$('ow_obj_image').style.width = newlimit+"%";
	}
}


//Sort ou rentre du mode fullscreen
OW_slideshow.prototype.toggleFullscreen=function()
{
	if (this.toggle_fullscreen==false)
		this.switchToFullScreen();
	else
		this.exitFullScreen();
	
	this.setLimitant();	
}

//Rentre en mode fullscreen
OW_slideshow.prototype.switchToFullScreen=function()
{
	var hauteur;
	var largeur;

	if (Prototype.Browser.IE)
	{
		hauteur=document.getElementsByTagName('html')[0].clientHeight-(parseInt(this.main_container.getLayout().get('border-top'))+parseInt(this.main_container.getLayout().get('border-bottom'))+1);
		largeur=document.getElementsByTagName('html')[0].clientWidth-(parseInt(this.main_container.getLayout().get('border-left'))+parseInt(this.main_container.getLayout().get('border-right'))+1);
	}
	else
	{
		hauteur=window.innerHeight-(parseInt(this.main_container.getLayout().get('border-top'))+parseInt(this.main_container.getLayout().get('border-bottom'))+1);
		largeur=window.innerWidth-(parseInt(this.main_container.getLayout().get('border-left'))+parseInt(this.main_container.getLayout().get('border-right'))+1);
	}

	this.toggle_fullscreen=true;
	//this.main_container.setStyle({position:'absolute',top:'0px',left:'0px',width:largeur+'px',height:hauteur+'px',backgroundColor:'#FFFFFF'});
	//MS 03/02/11	fullscreen > Ok quand on switch de l'un a l'autre, mais bugé si on switchtofullscreen puis resize
	// correctif superficiel : on fixe le background et hauteur / largeur en % pour que le fullscreen prenne tout l'espace disponible meme apres resize 
	//		==> l'image ne se resize pas bien elle
	
	$('ow_img_aff').setOpacity(0);
	this.main_container.setStyle({position : 'fixed', top :'0px', left :'0px', width :'100%', height : '100%'});
	document.body.addClassName("hideover");
	this.main_container.addClassName("slideshowFullscreen");

	
	this.slider_thumbs.setValue(0);

	this.slider_thumbs=new Control.Slider('ow_thumbs_curseur','ow_thumbs_slider',
		{
			range: $R(0,100),
			onChange: function (value)
			{
				this.scrollThumbnails(value);
			}.bind(this),
			onSlide: function (value)
			{
				this.scrollThumbnails(value);
			}.bind(this)
		}
	);
	this.resizeCadreImage();
	setTimeout(function(){
		this.resizeImage();
	}.bind(this),500);
}

//Sort du mode fullscreen
OW_slideshow.prototype.exitFullScreen=function()
{
	this.toggle_fullscreen=false;
	this.main_container.setStyle({position:'static',top:'0px',left:'0px',width:this.width,height:this.height});
	document.body.removeClassName("hideover");
	this.main_container.removeClassName("slideshowFullscreen");
	$('ow_img_aff').setOpacity(0);

	this.slider_thumbs=new Control.Slider('ow_thumbs_curseur','ow_thumbs_slider',
		{
			range: $R(0,100),
			onChange: function (value)
			{
				this.scrollThumbnails(value);
			}.bind(this),
			onSlide: function (value)
			{
				this.scrollThumbnails(value);
			}.bind(this)
		}
	);

	this.goToImage(this.current_img);
	
	// this.resizeCadreImage();
	// this.resizeImage();
}

//fonction affichant les miniatures
OW_slideshow.prototype.displayThumbnails=function(id_img)
{
	var img_centre;
	var list_img='';

	if (id_img!=null)
		img_centre=parseInt(id_img);
	else
		img_centre=this.current_img;

	//for(i=img_centre-Math.floor(this.nb_thumbs/2);i<img_centre+Math.ceil(this.nb_thumbs/2);i++)
	for(i=0;i<this.nb_images;i++)
	{
		if (i<this.nb_images && i>=0 && i==img_centre)
			list_img+='<td><div name="cadre_thumb"><img src="'+this.url_miniature[i]+'" name="ow_thumb" class="ow_miniature ow_miniature_selected" usemap="'+i+'" /></div></td>';
		else if (i<this.nb_images && i>=0)
			list_img+='<td><div name="cadre_thumb"><img src="'+this.url_miniature[i]+'" name="ow_thumb" class="ow_miniature" usemap="'+i+'" /></div></td>';
		else
			list_img+='<td><div name="cadre_thumb">&nbsp;</div></td>';
	}
	$('ow_thumbs').innerHTML='<table id="ow_container_thumbs"><tr>'+list_img+'</tr></table>';
	$('ow_thumbs').innerHTML+='<div id="ow_thumbs_slider"><div id="ow_thumbs_curseur"></div></div>';

	this.slider_thumbs=new Control.Slider('ow_thumbs_curseur','ow_thumbs_slider',
		{
			range: $R(0,100),
			onChange: function (value)
			{
				this.scrollThumbnails(value);
			}.bind(this),
			onSlide: function (value)
			{
				this.scrollThumbnails(value);
			}.bind(this)
		}
	);
	
	//on cache le slider des miniatures si la largeur totale est inferieure a la largeur de leur conteneur
	if ($('ow_container_thumbs').getWidth()<=$('ow_thumbs').getWidth())
		$('ow_thumbs_slider').hide();
}

//Affiche ou masque les miniatures
OW_slideshow.prototype.toggleThumbnails=function()
{
	if (this.toggle_thumbs==false)
		this.showThumbnails();
	else
		this.hideThumbnails();
}

//Affiche les miniatures
OW_slideshow.prototype.showThumbnails=function()
{
	this.toggle_thumbs=true;
	$('ow_thumbs').show();
	this.resizeCadreImage();
	this.resizeImage();
}

//Masque les miniatures
OW_slideshow.prototype.hideThumbnails=function()
{
	this.toggle_thumbs=false;
	$('ow_thumbs').hide();
	this.resizeCadreImage();
	this.resizeImage();
}

//Scroll dans la zone d’affichage des images
OW_slideshow.prototype.scrollThumbnails=function(value)
{
	var largeur_dispo=$('ow_thumbs').getWidth()-(parseInt($('ow_thumbs').getStyle('padding-left'))+parseInt($('ow_thumbs').getStyle('padding-right'))+parseInt($('ow_thumbs').getLayout().get('border-left'))+parseInt($('ow_thumbs').getLayout().get('border-right')));
	var largeur_conteneur=$('ow_container_thumbs').getWidth();
	if (largeur_dispo <= largeur_conteneur)
		marge_left=(largeur_conteneur-largeur_dispo)*(value/100);
	else
		marge_left=0;
	$('ow_container_thumbs').setStyle({marginLeft:'-'+marge_left+'px'});
}


