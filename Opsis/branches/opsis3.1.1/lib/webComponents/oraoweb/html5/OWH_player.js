// Constructeur OWH_player
function OWH_player(container, player_options) {
	this.init(container,player_options);							//appel fonction d'initialisation du player
}



//Fonction d'initialisation du OWH_player
OWH_player.prototype.init = function(container,player_options){
	// Detection browser ============================
	if(typeof player_options['OW_env_user'] == 'undefined'){
		this.BrowserDetect = {
		init: function () {
			this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
			this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
			this.OS = this.searchString(this.dataOS) || "an unknown OS";
		},
		searchString: function (data) {
			for (var i=0;i<data.length;i++)	{
				var dataString = data[i].string;
				var dataProp = data[i].prop;
				this.versionSearchString = data[i].versionSearch || data[i].identity;
				if (dataString) {
					if (dataString.indexOf(data[i].subString) != -1)
						return data[i].identity;
				}
				else if (dataProp)
					return data[i].identity;
			}
		},
		searchVersion: function (dataString) {
			var index = dataString.indexOf(this.versionSearchString);
			if (index == -1) return;
			return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
		},
		dataBrowser: [
					  {
					  string: navigator.userAgent,
					  subString: "Chrome",
					  identity: "Chrome"
					  },
					  { 	string: navigator.userAgent,
					  subString: "OmniWeb",
					  versionSearch: "OmniWeb/",
					  identity: "OmniWeb"
					  },
					  {
					  string: navigator.vendor,
					  subString: "Apple",
					  identity: "Safari",
					  versionSearch: "Version"
					  },
					  {
					  prop: window.opera,
					  identity: "Opera"
					  },
					  {
					  string: navigator.vendor,
					  subString: "iCab",
					  identity: "iCab"
					  },
					  {
					  string: navigator.vendor,
					  subString: "KDE",
					  identity: "Konqueror"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "Firefox",
					  identity: "Firefox"
					  },
					  {
					  string: navigator.vendor,
					  subString: "Camino",
					  identity: "Camino"
					  },
					  {		// for newer Netscapes (6+)
					  string: navigator.userAgent,
					  subString: "Netscape",
					  identity: "Netscape"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "MSIE",
					  identity: "Explorer",
					  versionSearch: "MSIE"
					  },
					  {// IE 11
					  string: navigator.userAgent,
					  subString: "Trident",
					  identity: "Explorer",
					  versionSearch: "rv"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "Gecko",
					  identity: "Mozilla",
					  versionSearch: "rv"
					  },
					  { 		// for older Netscapes (4-)
					  string: navigator.userAgent,
					  subString: "Mozilla",
					  identity: "Netscape",
					  versionSearch: "Mozilla"
					  }
					  ],
			dataOS : [
					  {
					  string: navigator.platform,
					  subString: "Win",
					  identity: "Windows"
					  },
					  {
					  string: navigator.platform,
					  subString: "Mac",
					  identity: "Mac"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "iPhone",
					  identity: "iPhone"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "iPad",
					  identity: "iPad"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "Android",
					  identity: "Android"
					  },
					  {
					  string: navigator.platform,
					  subString: "Linux",
					  identity: "Linux"
					  }
					  ]

		};
		this.BrowserDetect.init();
		this.browser = this.BrowserDetect.browser;
		this.browserVersion = this.BrowserDetect.version;
		this.user_OS= this.BrowserDetect.OS;
	}else{
		this.browser = player_options['OW_env_user'].browser;
		this.browserVersion = player_options['OW_env_user'].browserVersion;
		this.user_OS= player_options['OW_env_user'].env_OS;
	}



	//====================================================================================================

	this.StepVideoMethod;
    this.shuttleRatioSteppPlaybackrate = null;
	//TESTS POUR IE
	//this.shuttleRatioSteppPlaybackrate = function () {
      //  if(this.jogspeed >=0.5 && this.jogspeed <= 2){
        //    return true;
        //} else {
         //   return false;
        //}
    //}

	//En fonction du browser on initialise différentes variables==========================================

	if (this.browser == "Safari" ){//|| this.browser == "Explorer" ){				// Si le browser est safari OU IE
		this.killPeriodExec();
		this.controlSeek = "Ranges";				// méthode de controle des seeks par les myBufferedRange
		this.initRange();							//méthode d'initialisation des ranges "maison"
		this.delayRangeSeek = 0;					// compteur pour delayer les seeks hors des ranges
		this.StepVideoMethod =  this.LimitRangeStepVideo;	// attribution de la méthode de step correspondant aux spécificités du navigateur
		this.displayBufferedRangesDivs=[];			// Initialisation du tableau des divs représentants les ranges
		this.stopUpdateRange = false;				// Flag permettant de bloquer l'update des ranges
		this.reloading = false;						// Flag permettant de savoir si la vidéo est en train de faire un reload (cas du bug bufferisation safari
		this.mediaFullyLoaded = false;
		this.allowResizeVideo = true;

		this.shuttleRatioSteppPlaybackrate = function () {
            if(this.jogspeed >=0.5 && this.jogspeed <= 2){
                return true;
            } else {
                return false;
            }
        }


	} else if (this.browser == "Firefox" || this.browser == "Chrome"|| this.browser == "Explorer" ){		// Si le browser est Chrome
		this.canSeek= true;
		this.displayBufferedRangesDivs = [];
		this.controlSeek = "Events"					// méthode de controle des seeks en utilisant les events
		this.retardSeek = 0;						// variable contenant le retard cumulé dû aux seeks sous chrome pour le stepping
		this.StepVideoMethod = this.LimitSeekStepVideo; // attribution de la méthode de step correspondant aux spécificités du navigateur
		this.allowAdjustTimelineOnResize = true;	// flag pour limiter l'execution parallele des fonctions gerant le resize de la fenetre
		this.allowResizeVideo = true;

		//Fonction qui autorise le seeking pour Chrome
		this.AllowSeeking = function() {
			this.canSeek = true ;
		}
		//Fonction qui interdit le seeking pour Chrome
		this.BlockSeeking = function() {
			this.canSeek = false ;
		}

		if(this.browser == "Firefox"){
			limite_haute = 5;
			limite_basse = 0
		}else if(this.browser == "Chrome"){
			limite_haute = 2;
			limite_basse = 0.5;
		}else {
			limite_haute = 10;
			limite_basse = 0;
		}

   		this.shuttleRatioSteppPlaybackrate = function (){
            if(this.jogspeed >= limite_basse && this.jogspeed <=limite_haute){						// TESTIE
                return true;
            } else {
                return false;
            }
        }
	}




	//=====================variables membres=====================
	this.container = document.getElementById(container);							//div conteneur

	//Variables video
	this.media;																		//élément video (ou audio)

	this.mediaType = this.mediaTypeDetection(player_options.url,player_options.typeMime);

    this.rateWasChanged = false;

	this.fullscreen = false;

    this.useNativeFullscreen = false;                // Paramètre activant l'utilisation du fullscreen natif de Safari.

	this.dontCallOnChange=false;
	this.fastSteppingOn = null;
    this.fastSteppingDirection = 0;
    this.slowSteppingOn = null;
    this.slowStepValue = null;

    this.goingToEnd = false;


	// variables timeline (en réalité deux sliders séparés)
	this.timeline;			// timeline
	this.timelineSel;		// timeline de sélections

    this.prerolling = false ;

	this.ClickOnTimeline = false;   //flag permettant d'identifier la source d'un onChange timeline comme une action de l'utilisateur

	this.bugAfterRateChange = false;        // Flag permettant d'activer des tests une fois que le playbackrate a été modifié (ce qui cause un leger bug
	//      de retour a la keyframe la plus proche avant de lancer une lecture )



	this.periodicalUpdateRangeOn = null;    // Flag permettant d'identifier si un periodical executer est actuellement utilisé pour controler l'update des
	//      ranges de chargements
	// variables jog
	this.jogshuttle;
	this.jogspeed = 0;
	this.jogIsUsed = false;




	//variable sound slider
	this.soundslider;

	// variables boutons extraits :
	this.extsBtns = false ;
	


	// variables de selection
	this.BegSelect = 0;
	this.EndSelect = 0;
	this.selectionExists= false;
	this.SelSlide = false;
	this.selectionSliderDisabled = false ;
	//lecture en boucle
	this.loopVideo=false;


	//  VP 8/12/2011 : PLAYLIST
	this.playlist=false;
	this.current_track=-1;
	this.nb_tracks=0;
	this.duration=0;
	this.BegTrack=0;
	this.EndTrack=0;
	this.url='';
	this.track_init_events = new Array();
	this.track_location=new Array();
	this.track_start=new Array();
	this.track_end=new Array();
	this.track_duration=new Array();
	this.track_srt_path=new Array();
	this.track_media=new Array();
	this.track_start_offset=new Array();
	this.playNextFlag = false ;
	this.currentTrackEnded = false ;

	this.display_buff = true ;
	this.mosaic_initialized  = false ;


	this.sec_mosaic  = null;

	// MS 12/02/12
	// Variables activation des fonctions de style

		this.resizeCustom = player_options['resizeCustom'];

	this.verticalFixed = player_options['verticalFixed'];
	if(player_options['selectionSliderDisabled']){
		this.selectionSliderDisabled =  player_options['selectionSliderDisabled'];
	}

	this.dynamicController = (player_options['dynamicController'] && typeof($j)!="undefined");
	this.dynamicControllerFS = (player_options['dynamicControllerFS'] && typeof($j)!="undefined");
	if(typeof player_options['dynamicSyncQuery'] != 'undefined'){
		this.dynamicSyncQuery = (player_options['dynamicSyncQuery']);
	}

	this.movie_options = {
		// infos opsismedia
		infos : {
			'id' : null , 
			'visu_type' : null 
		},
		
		auto_send_tracked_duration : false ,
		
		//parametres principaux
		url : '',
		xml : '', // VP 8/12/2011 : PLAYLIST
		playlist : false,

		//plusieurs qualités de médias
		urls : '',
		typesMime : '',
		quality_names : '',

		// preroll publicitaire
		preroll : false,
		preroll_url : '',
		preroll_urls : '',
		preroll_text : null,
		preroll_transition_poster : '',
		preroll_just_once : true ,

		width : null,
		height : null,

		poster_width : null ,
		poster_height : null,

		//imageRootURL : "images/",			// Interet a demander a vincent vu que toutes les images se parametrent dans le css.
		typeMime : "",
		frameTimeValue : 0.04,
		name : "video",
		autoplay : true,
		preload : true,
		image : null,
		noPoster : false,
		language : 'fr',
		debug_mode : 'false',
		stepping_frequency : 0.10,
		
		// cross-origin
		// définition de l'attribut crossorigin de l'élement média : peut être necessaire pour les sites où les médias ne sont pas sur le même domaine, dans ce cas là il est necessaire que l'accès aux médias renvoi un header Access-Control-Allow-Origin cohérent, et ce pour les fichiers vidéos, de sous-titres etc ... 
		// le renvoi de ce header doit donc en général être configuré directement sur le serveur web
		define_crossorigin_policy : null, 
		
		// Display timeline
		hauteur_track :10,		// Hauteur en pixel de la track a definir via movie_options si custom
		hauteur_bg_track : 4,		// Hauteur en pixel du background / des divs de bufferisation
		span_time_passed : false,

		// option sound slider 
		span_sound_slider : false,

		// Options preview timeline
		previewOverTimeline : false ,	// Active le preview imagette en rollover sur la timeline
		mosaic_path : null ,			// chemin du fichier mosaique du storyboard
		mosaic_step : 10,				// écart entre chaque image de la mosaique
		mosaic_width : 100,				// largeur des éléments de la mosaique
		tcMosaicExact : false ,
		offset_mouseEvent_timeline : 3, // du au fonctionnement du prototype slider, on doit légèrement ajuster la valeur rapportée par le mouseEvent pour que les temps affichés sur la timeline & dans le preview soit cohérents
		
		// ajout d'une option permettant de passer du fonctionnement 2.8 au dev 3.0
		// Cela dépends de si la mosaique a été générée avec des timecodes relatifs au début du matériel ou au début du document
		// par défaut dans le moteur actuel, on génère la mosaique pour le matériel, sur l'INA on générait les tc par rapport au début du document. 
		mosaic_tc_relative_to_doc : false, 
		
		// variable liste des timecodes des images de la mosaique fournie dans mosaic_path
		mosaic_pics_index : null,
		mosaic_use_index : false,
		
		//Options visualisation waveform pour fichier audio
		activateWaveForms : true , 
		waveform_path : null , 
		waveform_peaks_opts : {
			zoomWaveformColor : 'rgba(212,75,60,1)',
			waveformBuilderOptions : {
				amplitude_scale : 0.7
			},
			zoomLevels: [
				256,
                512,
                1024,
                2048,
                4096
            ],
			zoomStartingIndex : 2, // correspond au zoom 1024 ;
		},
		// option affichage de la barre de selection
		selectionHidden : true,
		
		// dynamicController timer disappear : 
		timer_disappear  : 2000 ,
		timer_disappear_mobile  : 6000 ,
		dynamicSyncQuery_triggers_showController : true ,
		
		// options relatives à la définition d'extraits
		extsBtnsOnStart : false,
		extsContext : "montage",
		extBtnNew : false,
		extsBtnsDisabled : true ,
		extsBtnsMoveToId : null ,
		
		activateAudioContext : false , 
		activateRatioSelect : false , 
		
		xStand : false, // Cas particulier xStand => non détectable car basé sur Safari => paramètre spécial pr le detecter & pouvoir workaround

		// TC
		timecode_lag : null  ,      // option d'offset
		timecode_IN :   null,
		timecode_OUT :   null,
		timecode_with_frame : false  ,
		timecode_display_format : 'hh:mm:ss:ff',
		show_tc_with_lag : true,
		timecode_playback_start : null , 
		
		// permet de bloquer l'apparition du menu click droit 
		prevent_right_click_menu : true , 
		
		// structure kb_controls pour les controles du player via clavier ,
		// pour l'instant ne contient que le raccourci "play/pause" sur la spacebar 		
		kb_controls : {
			spacebar_pause : true, 
			arrows_volume : true,
			arrows_time : true, 
			arrows_time_shift : true, 
			scrollwheel_time : true
		},
		
		//timecodes "references", défini les bornes théoriques de la vidéo (au chargement & avec les fonction gotoBeg & gotoEnd)
		// tout en permettant de visionner de la marge avant et à la faim du programme (cas spécifique Inatheque)
		ref_tc_IN :   null,
		ref_tc_OUT :   null,
		// offset d'affichage des timecodes, purement cosmétique, ajouté pour PCM-INA afin d'unifier le comportement des timecodes
		offset_display_tc : null,
		
		//mode affichage : 
		//scale : 'aspect' ,			// on ne peut pas le parametrer en html 5 , la video garde toujours son ratio normal
		force_ratio_ref : null,
		fit_height_container : false,
		fillFullscreen : false ,
		FS_elem_selector : null ,
		movie_background_color : 'black',

		show_poster_on_ended : false ,
		show_vignette_audio  : false ,

		exitFullscreenOnEnded : true,

		//switchMovie
		switched : false,
		switched_time : 0,

		//sous titre
		subtitles : null,
		
		// sequences : 
		xml_sequences : null,
		seq_use_seq_vignette : false , 
		diapo_type_seq : null , 
		chapter_type_seq : null , 
		show_seq_nums : false ,
		show_seq_encadre : false ,
		
		
		// activation de  l'envoi auto des statistiques de log des durées regardées 
		auto_send_tracked_duration : null,

		// Options affichages btn sous titres / media
		merge_media_subtitles_selectors : false , 
		
		//fonctions callback :
		onMovieFullScreen : null,
		onMovieEndFullScreen : null,
		onPlayerReady : null,
		onMovieReady : null,
		onMovieFinished : null,
		onSelectionSliderChange : null,
		onTrackChange : null,
		onTimecodeChange : null,
		onExtSave : null ,
		urlOfEndImage : '',
		
		displayLoopButton:false,

		//template :
		force_width_controller : false,
		controller_on_video : false,
		code_controller :  '<!-- Controller Design -->														' +
		'       <div id="ow_id_progress_slider">	</div>				' +
		'       <div id="ow_first_row_controller">		' +
		'			<div id="ow_id_jog_slider"></div><p id="joginfo">0x</p> 		' +
		'      		<p id="timecodeDisplay">00:00:00:00</p>                                                             ' +
		'      		<div id="ow_quality_switch"></div>                                                             ' +
		'     		<div id="ow_boutons_extraits">				' +
						'<a class = "btn" style="display:none;" id = "ow_bouton_ext_new"></a>' +
						'<a class = "btn" id ="ow_bouton_ext_in"></a>' +
						'<a class = "btn" id="ow_bouton_ext_out"></a>' +
						'<a class = "btn" style="display:none;" id="ow_bouton_ext_save"></a>' +
						'<a class = "btn" id = "ow_bouton_ext_cancel"></a>		' +
		'			</div>                                                      ' +
		'       </div>                                                         	' +
		'		 <div id = "ow_boutons">										' +
		'		<div id = "containSound"><div id = "ow_id_sound_slider"></div></div>																	' +
		'<span id="sound_channels_btns"><a class = "btn" id = "ow_bouton_left_sound"></a>' +
		'<span id="select_force_ratio_container"><label>Ratio : </label><select id="select_force_ratio"><option value="auto">auto</option><option value="16_9">16/9</option><option value="4_3">4/3</option></select></span>' + 
		'<a class = "btn" id = "ow_bouton_right_sound"></a></span>' +
        '<a class = "btn" id = "ow_bouton_mute_sound"></a>'+
        '<a class = "btn" id = "ow_bouton_go_to_beginning"></a>' +
        '<a class = "btn" id = "ow_bouton_fast_rewind"></a>' +
        '<a class = "btn" id = "ow_bouton_step_rewind"></a>' +
        '<a class = "btn play" id = "ow_bouton_play_pause" ></a>' +
        '<a class = "btn" id = "ow_bouton_step_forward"></a>' +
        '<a class = "btn" id = "ow_bouton_fast_forward"></a>' +
        '<a class = "btn" id = "ow_bouton_go_to_end"></a>' +
		'<a class = "btn" id = "ow_bouton_srt"></a>' +
        '<a class = "btn" id = "ow_bouton_fullscreen"></a>' +
		'<a class="btn" id="ow_bouton_loop"></a>'+
		'</div>'  ,


		basic_function_button : {
			'ow_bouton_play_pause' : this.ToggleVideo.bind(this),
			'ow_bouton_mute_sound' : this.SetMuteState.bind(this),
			'ow_bouton_fullscreen' : this.ToggleFullScreen.bind(this),
			'ow_bouton_go_to_beginning' :  this.GoToBegAvailable.bind(this),
			'ow_bouton_go_to_end' :  this.GoToEndAvailable.bind(this),
			'ow_bouton_step_forward' : this.SlowPlaying.bind(this,1),
			'ow_bouton_step_rewind' : this.SlowPlaying.bind(this,-1),
			'ow_bouton_fast_rewind' :  this.FastPlaying.bind(this,-1),                                             
			'ow_bouton_fast_forward' : this.FastPlaying.bind(this,1),                                             
			'ow_bouton_loop' : this.toggleLoopVideo.bind(this),
			'ow_bouton_ext_in' : this.extMarkTC.bind(this,"in"),
			'ow_bouton_ext_out' : this.extMarkTC.bind(this,"out"),
			'ow_bouton_ext_cancel' : this.cancelExt.bind(this),
			'ow_bouton_ext_new' : this.newExt.bind(this),
			'ow_bouton_ext_save' : this.saveExt.bind(this),
			'ow_bouton_left_sound' : this.RightChannelOnly.bind(this),
			'ow_bouton_right_sound' : this.LeftChannelOnly.bind(this),
			'ow_bouton_prev_seq' : this.GoToClosestSeq.bind(this,0),
			'ow_bouton_next_seq' : this.GoToClosestSeq.bind(this,1)
		},
		//tooltips
		tooltips_fr : {
			'ow_id_timecode' 				: 'Time Code (HH:mm:ss)',
			'ow_id_timecodeLong' 			: 'Time Code (HH:mm:ss.frame)',
			'ow_id_sound_slider'			: 'Volume sonore',
			'ow_id_progress_slider'			: 'Barre de progression',
			'ow_id_jog_slider'				: 'Jog Shuttle',
			'ow_bouton_play' 				: 'Lecture',
			'ow_bouton_pause' 				: 'Pause',
			'ow_bouton_play_pause' 			: 'Lecture ou Pause (suivant contexte)',
			'ow_bouton_play_again'			: 'Relance la visionneuse',
			'ow_bouton_fast_rewind' 		: 'Retour rapide',
			'ow_bouton_fast_forward' 		: 'Avance rapide',
			'ow_bouton_step_rewind'			: 'Reculer d\'une image',
			'ow_bouton_step_forward'		: 'Avancer d\'une image',
			'ow_bouton_go_to_beginning'		: 'Retourner au début du film',
			'ow_bouton_go_to_end'			: 'Aller a la fin',
			'ow_bouton_fullscreen'			: 'Active ou desactive le mode plein ecran',
			'ow_bouton_mute_sound' 			: 'Coupe le son',
			'ow_bouton_ext_in' 				: 'Marquer début extrait',
			'ow_bouton_ext_out'				: 'Marquer fin extrait',
			'ow_bouton_ext_cancel' 			: 'Annuler définition de l\'extrait ',
			'ow_bouton_ext_save' 			: 'Sauver l\'extrait',
			'ow_bouton_left_sound'			: 'Active/désactive le canal audio gauche',
			'ow_bouton_right_sound'			: 'Active/désactive le canal audio droit',
			'ow_bouton_loop'				: 'Lecture en boucle',
			'ow_bouton_download'			: 'Téléchargement',
			'ow_bouton_add2cart'			: 'Ajouter au panier',
			'ow_bouton_removefromcart'		: 'Retirer du panier',
			'ow_bouton_srt'				    : 'Sous-titres',
			'ow_shortcuts_help'				: 'Aide raccourcis clavier',
			'spacebar_pause'				: '<span class="help_label">Barre d\'espace</span> : Lecture ou Pause (suivant contexte)',
			'arrows_volume'					: '<span class="help_label">Flèches haut / bas</span> : Monter / baisser le volume',
			'arrows_time'					: '<span class="help_label">Flèches gauche / droite</span> : Reculer / Avancer d\'une image',
			'arrows_time_shift'				: '<span class="help_label">Shift + gauche / droite</span> : Reculer / Avancer  de 5 secondes',
			'scrollwheel_time'				: '<span class="help_label">Molette souris</span> : Déplacer le curseur dans le média',

		},
		tooltips_en : 	{
			'ow_id_timecode' 				: 'Time Code (HH:mm:ss)',
			'ow_id_timecodeLong' 			: 'Time Code (HH:mm:ss.frame)',
			'ow_id_sound_slider'			: 'Sound volume',
			'ow_id_progress_slider'			: 'Progress bar',
			'ow_id_jog_slider'				: 'Jog Shuttle',
			'ow_bouton_play' 				: 'Play',
			'ow_bouton_pause' 				: 'Pause',
			'ow_bouton_play_pause' 			: 'Play or Pause (switch)',
			'ow_bouton_play_again'			: 'Restart the player',
			'ow_bouton_fast_rewind' 		: 'Fast rewind',
			'ow_bouton_fast_forward' 		: 'Fast forward',
			'ow_bouton_step_rewind'			: 'Back up one image',
			'ow_bouton_step_forward'		: 'Forward by one image',
			'ow_bouton_go_to_beginning'		: 'Go to the beginning',
			'ow_bouton_go_to_end'			: 'Go to the end',
			'ow_bouton_fullscreen'			: 'Toggle fullscreen',
			'ow_bouton_mute_sound' 			: 'Sound off',
			'ow_bouton_ext_in' 				: 'Mark beginning of selection',
			'ow_bouton_ext_out'				: 'Mark ending of selection',
			'ow_bouton_ext_cancel' 			: 'Cancel the current selection',
			'ow_bouton_ext_save' 			: 'Save the current selection',
			'ow_bouton_left_sound'			: 'Enable/disable left audio channel',
			'ow_bouton_right_sound'			: 'Enable/disable right audio channel',
			'ow_bouton_loop'				: 'Loop playing',
			'ow_bouton_download'			: 'Download',
			'ow_bouton_add2cart'			: 'Add to cart',
			'ow_bouton_removefromcart'		: 'Remove from cart',
			'ow_bouton_srt'				    : 'Subtitles',
			'ow_shortcuts_help'				: 'Keyboard shortcuts help',
			'spacebar_pause'				: '<span class="help_label">Spacebar</span> : Play or Pause (switch)',
			'arrows_volume'					: '<span class="help_label">Up / down arrows</span> : Increase / lower volume',
			'arrows_time'					: '<span class="help_label">Left / Right arrows</span> : Move one frame backward / forward',
			'arrows_time_shift'				: '<span class="help_label">Shift + Left / Right</span> : Move 5 seconds backward / forward',
			'scrollwheel_time'				: '<span class="help_label">Scrollwheel</span> : Move the current position in the media',
		}
	}
	
	// Permet d'update les paramètres par défaut à partir de l'objet player_options passé en argument
	// on peut maintenant aussi passer des sous objets pour l'initialisation (non récursif pr l'instant => un seul sous niveau)
	// => cf kb_controls (keyboard controls)
	for (o in player_options){		// Pour chaque clé dans player_options
		if(this.movie_options.hasOwnProperty(o)){		// Si movie_options possède une même clé
			if(typeof player_options[o] == 'object' && typeof this.movie_options[o] == 'object'){
				for (sub_o in player_options[o]){
					if(this.movie_options[o] != null  && this.movie_options[o].hasOwnProperty(sub_o)){
						this.movie_options[o][sub_o] = player_options[o][sub_o];
					}
				}
			}else{
				this.movie_options[o] = player_options[o];	// Mise a jour de movie_options à partir de player_options
			}
		}
	}
	
	
	//Variables controller
	this.play_pause_btn;

	if(this.mediaType=="audio"){
		// this.container.className+=" audio";
		$j(this.container).addClass("audio");
	}
	//console.log(this.movie_options.infos.visu_type,this.movie_options.infos.visu_type.match("/(stream_)/"));
	if (this.movie_options.infos.visu_type && this.movie_options.infos.visu_type.match(new RegExp(/(stream_)/)) != null){
		$j(this.container).addClass('stream');
	}


	// Initialisation du player
	this.AttachPlayer();
}

//Fonction d'initialisation du player
OWH_player.prototype.AttachPlayer = function(){

	// vidage du conteneur pour avoir le meme fonctionnement que sur OW_player
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	if (!(typeof this.container === 'undefined' || this.container=== null)) {
		this.container.innerHTML='';

		var main_container=document.createElement('div');
		main_container.id='ow_main_container';
		this.container.appendChild(main_container);
		this.container = main_container;
		$(this.container.id).addClassName("containerNS");
		/*if(typeof this.header != 'undefined' && $j('#'+this.container.id).has($j(this.header)).length == 0) {
			this.container.appendChild($j(this.header).get(0));
		}*/
	}


	// VP 8/12/2011 : PLAYLIST
	this.playlist=this.movie_options.playlist;

	if(this.playlist ){
		this.display_buff = false ;
	}

	if(this.playlist){
		if(this.movie_options["xml"]!='') {
			var parser;
			var xmlDoc;
			if (typeof(DOMParser)!='undefined')
			{
				parser=new DOMParser();
				xmlDoc=parser.parseFromString(this.movie_options["xml"],"text/xml");
			}
			else
			{
				xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async=false;
				xmlDoc.loadXML(this.movie_options["xml"]);
			}
			this.traiterFluxXml(xmlDoc);
		} else if (this.movie_options["url"]!='') {
			$ajax=new Ajax.Request(this.movie_options["url"],
								   {
								   asynchronous:false,
								   onSuccess : this.traiterFluxXml.bind(this)
								   });
		}
		this.AttachPlaylist(this.movie_options["typeMime"]);
		this.ChangeTrack(0);
		if(this.media){
			this.media.autoplay = this.movie_options.autoplay;
		}
		this.AttachMovieController();
		this.delayedDurationInit();

		if(typeof this.track_srt_path != 'undefined' && this.track_srt_path.length>0){
			this.initSubtitleMenu() ; 
		}else if(document.getElementById('ow_bouton_srt') != null){
			document.getElementById('ow_bouton_srt').style.display='none';
		}


		if(this.movie_options['width'] && this.movie_options['height'] &&( this.mediaType=="video" || (this.mediaType && this.movie_options.show_vignette_audio))&& !this.movie_options['fit_height_container']){
			if(this.movie_options.controller_on_video){
				this.container.style.minHeight = (this.movie_options['height'])+"px" ;
			}else{
				this.container.style.minHeight = (this.movie_options['height']+this.controllerpanel.offsetHeight)+"px" ;
			}
			if(this.mediaType == 'audio'   && this.movie_options.show_vignette_audio){
				this.audio_viewport.style.height = this.movie_options.width* (this.movie_options.poster_height/this.movie_options.poster_width)+"px";
			}
		}
		if(typeof this.movie_options.onPlayerReady == 'function'){
			try{
				this.movie_options.onPlayerReady();
			}catch(e){
				console.log("OWH_player - crash callback onPlayerReady :"+e);
			}
		}

	}else{

		this.AttachMovieController();
		this.AttachMovie(this.movie_options["url"],this.movie_options["typeMime"]); 			//Initialisation de la video
		//this.duration=this.media.duration;
		
		if(typeof this.movie_options.onPlayerReady == 'function'){
			try{
				this.movie_options.onPlayerReady();
			}catch(e){
				console.log("OWH_player - crash callback onPlayerReady :"+e);
			}
		}
	}
	
	
	this.initPeripheralControls() ; 
	
	this.parseSequences();
	this.initSequencesWrapper();
	if(this.movie_options.diapo_type_seq != null){
		this.buildRichMedia();
	}
	
	this.initSimpleDurationTracker()
	
	

	if(this.movie_options.previewOverTimeline ==true && this.movie_options.mosaic_path != null  && this.movie_options.mosaic_path != ""  ){
		if(typeof this.movie_options.mosaic_pics_index == "string"){
			this.movie_options.mosaic_pics_index = this.movie_options.mosaic_pics_index.split(',');
		}
		this.setMosaicPath();
	}

	//this.resizeVideoKeepRatio();		//NEWLAYOUTVISIO

}

// Fonction attribuant une vidéo au player à partir d'une URL et d'un type.
OWH_player.prototype.AttachMovie = function(url,typeMime){
	if(($j && $j(this.container).find(this.capsulevideo).length>0)){					// document.getElementById('capsVid') Si l'élément video existe déjà :
		this.container.removeChild(this.capsulevideo);				//		On le supprime.
	}


    if(this.mediaType == "audio" ){                 // Si le media est audio, on regle le test ShuttleMove en consequence (sur chrome, le playbackRate négatif est ignoré, ce n'est donc pas fonctionnel)

		if ( this.browser =="Safari"){

			this.shuttleRatioSteppPlaybackrate =function (){
				if(this.jogspeed >=-10 && this.jogspeed <= 10) {
					return true;
				} else {
					return false;
				}
			}

		}else if (this.browser == "Explorer" || this.browser == "Chrome" || this.browser == "Firefox"){

			this.shuttleRatioSteppPlaybackrate =function (){
				if(this.jogspeed >=0 && this.jogspeed <= 10) {
					return true;
				} else {
					return false;
				}
			}

		}
    }



	this.media = document.createElement(this.mediaType);			//Création d'un nouvel élement video
	this.media.id = this.movie_options["name"];								// definition id
	this.media.controls = false;
	if(this.movie_options.infos.visu_type == 'stream_hls'){
		try{
			this.hls = new Hls();
			this.hlsErrorWrapper = $j('<div class="hls-error"></div>');
			this.hls.on("hlsError", function(event, data) {
				err_msg ="" ; 
				if(data.details == "manifestLoadError"){
					err_msg = "Erreur chargement du stream  "+data.context.url;
				}else{
					err_msg = "Erreur hls.js" ; 
				}
				err_msg+="<br />Essayez de recharger la page, ou contactez un administrateur.";
				
				
				if(typeof this.capsulevideo != 'undefined'){
					$j(this.capsulevideo).find('.hls-error').remove();
					this.hlsErrorWrapper.html(err_msg);
					$j(this.capsulevideo).append(this.hlsErrorWrapper);
				}
				console.log("event",event,"data",data);
			}.bind(this));
			this.hls.loadSource(url);
			this.hls.attachMedia(this.media);
		}catch(e){
			console.log("crash HLS attachment ",e);
		}
	}else if(this.movie_options.preroll && this.movie_options.preroll_urls != ''){
		this.media.src = this.movie_options.preroll_urls[this.movie_options.quality_names[0]];
	}else if(this.movie_options.preroll && this.movie_options.preroll_url != '' ){
		this.media.src = this.movie_options.preroll_url;
	}else{
		this.media.src = url;									// definition de l'url de la video
	}
	this.media.type = typeMime;								// definition du type de la video
	if(this.movie_options.prevent_right_click_menu){
		/*
		requirejs(['jqueryContextMenu/jquery.contextMenu','jqueryContextMenu/jquery.ui.position.min'],function(){
			requireCSS(ow_const.libUrl+'webComponents/jqueryContextMenu/jquery.contextMenu.css');
			// MS - 08.04.15 permet d'éviter l'ouverture du menu contextuel html5 .		
			$j.contextMenu({
				selector : "#"+this.container.id, 
				callback: function(key, options) {
					switch(key){
						case 'opso' : 
							window.open('http://www.opsomai.com');
						break ;
						case 'help_shortcut' : 
							this.openHelp('help_shortcuts');
						break ; 
						default :
						return false ; 
					}
				}.bind(this),
				items: {
					'help_shortcut' : {name: this.tooltips['ow_shortcuts_help']},
					"sep1": "---------",
					'opso' : {name: "Opsis Media v"+ow_const.coreVersion+" &copy;"+ow_const.kAnneeCopyright+" Opsomai", icon : 'opso'},
				}
			});	
		}.bind(this));*/
	}
	
	if( !this.movie_options['noPoster'] && this.movie_options['image']){
		if(this.mediaType == 'audio'   && this.movie_options.show_vignette_audio){
			this.audio_viewport = document.createElement('div');
			this.audio_viewport.id = 'ow_audioviewport';
			this.audio_viewport.className="ow_audioviewport";
			this.audio_vignette = document.createElement('img');
			if(this.movie_options['image'].indexOf('ol=player') != false){
				img = this.movie_options['image'].replace('&ol=player','');
			}else{
				img = this.movie_options['image'];
			}
			this.audio_vignette.src =img;
			this.audio_vignette.id = 'ow_audiovignette';
			this.audio_vignette.className = 'ow_audiovignette';
			this.audio_viewport.appendChild(this.audio_vignette);
			if (this.movie_options.activateWaveForms ){
				$j(this.audio_viewport).append('<div id="peaks-container"></div>');
			}

		}else{
			this.media.poster= this.movie_options['image'];
		}
	}
	if(this.movie_options['preload']){
		this.media.preload= this.movie_options['preload'];
	}else{
		this.media.preload='metadata';
	}

		//alert(this.movie_options.autoplay);
	if(this.movie_options.autoplay == false){
		this.media.autoplay = false;
	}else{
		this.media.autoplay = true;
	}
//	console.log('movie option srt');
//	console.log(this.movie_options['subtitles']);

	 //subtitles contient une chaine avec toutes les infos
	 // la chaine ressemble à (FR)http://
	 //extrait la langue et l'url par regex
	// console.log("movie_options.subtitles : "+this.movie_options['subtitles']);
	if(this.movie_options['subtitles']){
		this.subtitle_tracks = [] ; 
		var xmlSubtitle = $j.parseXML(this.movie_options['subtitles']);
		var langues = $j(xmlSubtitle).find('subtitle');
	    var langue;
	    var labelSubtitle;
	    for (var i = 0; i < langues.length; i++) {
			try{
				langue = $j(langues[i]).find('key').text();
			}catch(e){
				langue = null ; 
			}
			try{
				url = $j(langues[i]).find('url').text();
			}catch(e){
				url = null ; 
			}
			try{
				labelSubtitle = $j(langues[i]).find('label').text();
			}catch(e){
				labelSubtitle = null ; 
			}
			
	     	var subtitle_track =document.createElement('track'); 
			if (!(typeof labelSubtitle=== 'undefined' || labelSubtitle=== null || labelSubtitle=='')) {
				subtitle_track.label=labelSubtitle;
			}else{
				subtitle_track.label=langue;
			}
			subtitle_track.kind="subtitles";
			subtitle_track.srclang=langue;
		    subtitle_track.src=url;
			this.subtitle_tracks.push(subtitle_track);
			this.media.appendChild(subtitle_track);
	    }
	}else{
		if(document.getElementById('ow_bouton_srt') != null){
			document.getElementById('ow_bouton_srt').style.display='none';
		}
	}

	this.initSubtitleMenu() ; 

	if(this.movie_options.define_crossorigin_policy != null){
		// cf define_crossorigin_policy dans movie_options pour plus d'infos
		$j(this.media).attr('crossorigin',this.movie_options.define_crossorigin_policy);
	}
	

	var text = document.createTextNode("Video not supported");//Creation d'un texte alternatif
	this.media.appendChild(text);


	this.capsulevideo = document.createElement('div');
	this.capsulevideo.id = "capsVid";
	if(typeof this.audio_viewport != 'undefined'){
		this.capsulevideo.appendChild(this.audio_viewport);
	}
	this.capsulevideo.appendChild(this.media);
	if (this.movie_options.activateWaveForms 
	&& $j(this.capsulevideo).find("#peaks-container").length > 0 
	&& this.movie_options.waveform_path != null ){ 
		this.media.addEventListener('loadedmetadata',this.initWaveForm.bind(this),true);
	}
	
	if(this.movie_options['width'] && this.movie_options['height'] && !this.movie_options['fit_height_container']  && ( this.mediaType=="video" || (this.mediaType == 'audio' && this.movie_options.show_vignette_audio))){
		if((this.mediaType == 'audio' && this.movie_options.show_vignette_audio)){
			this.container.style.minHeight = "100px";
		}else if(this.movie_options.controller_on_video){
			this.container.style.minHeight = (this.movie_options['height'])+"px" ;
		}else{
			this.container.style.minHeight = (this.movie_options['height']+this.controllerpanel.offsetHeight)+"px" ;
		}
	}

	this.container.insertBefore(this.capsulevideo,this.controllerpanel);	// ajout de la capsule video au container
	// toujours avant le controller panel


	// redimensionnement des éléments pour le cas du player video avec affichage de la vignette
	if((this.mediaType == 'audio' && this.movie_options.show_vignette_audio)){
		if(this.movie_options.controller_on_video){
			this.container.style.minHeight = this.movie_options.width* (this.movie_options.poster_height/this.movie_options.poster_width)+"px";
		}else{
			this.container.style.minHeight = this.movie_options.width* (this.movie_options.poster_height/this.movie_options.poster_width)+this.controllerpanel.offsetHeight+"px";
		}
		this.audio_viewport.style.height = this.movie_options.width* (this.movie_options.poster_height/this.movie_options.poster_width)+"px";
		//console.log(this.audio_viewport.style.height );
	}

	if(this.movie_options.preroll && this.movie_options.preroll_url != ''){
		// MS - 22.05.15 - ajout possibilité de lire un preroll avant la vidéo, pour l'instant ne fonctionne que dans le cas video simple (audio / playlist non testés)
		this.initMediaEvents();
		this.prerolling = true ;
		this.controllerpanel.addClassName("prerolling");
		if(this.movie_options.preroll_text && typeof document.getElementById("timecodeDisplay") != 'undefined'){
			this.preroll_text = document.getElementById("timecodeDisplay").cloneNode();
			this.preroll_text.id = "prerollTextDisplay";
			this.preroll_text.innerHTML = this.movie_options.preroll_text;
			document.getElementById("timecodeDisplay").parentNode.appendChild(this.preroll_text);
		}

		if(this.movie_options.preroll_transition_poster != '' ){
			this.media.poster = this.movie_options.preroll_transition_poster;
		}

		var queueVideo = function () {
			this.media.autoplay = true ;
			/*this.media.poster = null ;*/
			this.controllerpanel.style.visibility = "";
			this.media.removeEventListener("ended",queueVideo,false);
			if(typeof this.preroll_text != 'undefined'){
				document.getElementById("timecodeDisplay").parentNode.removeChild(this.preroll_text);
			}
			this.controllerpanel.removeClassName("prerolling");
			this.prerolling = false ;
			
			this.SwitchMovie(url,this.movie_options["typeMime"],0,this.movie_options.preroll_transition_poster);
			if(this.movie_options.urls!='' && $j(this.container).find('#ow_quality_switch').is(':visible')){
				$j(this.container).find('#ow_quality_switch select').trigger('change');
			}
		}.bind(this);
		
		this.media.addEventListener("ended",queueVideo,false);
	}else{
		this.initMediaEvents();
	}
}

// ======= Set des listeners propres à la video
OWH_player.prototype.initMediaEvents= function(){
	if(this.movie_options.infos.visu_type == 'stream_hls'){
		this.initStreamHLSMediaEvents() ; 
	}else{
		this.initStandardMediaEvents() ; 
	}
}


OWH_player.prototype.initStandardMediaEvents= function(){
	if((this.mediaType == 'audio' && this.movie_options.show_vignette_audio)){
		this.audio_vignette.addEventListener("click", this.ToggleVideo.bind(this),false);
	}
    this.media.addEventListener("click", this.ToggleVideo.bind(this),false);
	this.media.addEventListener("durationchange", this.delayedDurationInit.bind(this),false );
	this.media.addEventListener("timeupdate",this.UItest.bind(this),false);
	this.media.addEventListener("play", this.changeButtonPlay.bind(this),false);
	this.media.addEventListener("pause", this.changeButtonPlay.bind(this),false);
	this.media.addEventListener("progress",this.testBuff.bind(this),false);
    this.media.addEventListener("ratechange", this.rateHasChanged.bind(this), false);
    this.media.addEventListener("volumechange", this.defaultControlVolumeChange.bind(this), false);

    if(this.movie_options.onMovieFinished!=null && typeof(this.movie_options.onMovieFinished)=='function'){
    	if(!this.playlist) {
			this.media.addEventListener("ended",function(){this.movie_options.onMovieFinished();}.bind(this),false);
		}
	}
	// VP 8/12/2011 : PLAYLIST
	// if(this.playlist) this.media.addEventListener("ended",this.PlayNext.bind(this),false);
	if(this.playlist) this.media.addEventListener("ended",this.OnEndTrack.bind(this),false);
    if(!this.playlist){
		this.media.addEventListener("ended",this.StopTheVideo.bind(this),false); // mstest
		if(this.movie_options.exitFullscreenOnEnded){
			// MS - En fin de vidéo, si on est en fullscreen => on sort du fullscreen
			this.media.addEventListener("ended",function(){
				if(this.fullscreen && !this.prerolling){
					this.FromFullScreenToPopUp();
				}
			}.bind(this),false);
		}
	}
    if(!this.playlist && this.movie_options.show_poster_on_ended ) this.media.addEventListener("ended",this.show_poster_on_ended.bind(this),false);

	
	
	this.media.addEventListener("canplay",function(){
		if( this.movie_options.activateAudioContext && (document.getElementById("ow_bouton_left_sound") || document.getElementById("ow_bouton_right_sound" )) && (typeof window.webkitAudioContext != "undefined" || typeof window.AudioContext != "undefined" ) && typeof this.audio_context == 'undefined'){
			this.initWebAudioNodes();
			$j("#sound_channels_btns").addClass("active");
		}else{
			if($j(this.container).find('#ow_bouton_left_sound').length>0){
				$j(this.container).find('#ow_bouton_left_sound').css('display','none');
			}
			if($j(this.container).find('#ow_bouton_right_sound').length>0){
				$j(this.container).find('#ow_bouton_right_sound').css('display','none');
			}
		}
	}.bind(this),false);
	
	if(this.browser == "Chrome" || this.browser == "Safari" || this.browser == "Firefox"|| (this.browser == "Explorer" && this.browserVersion >= 9 )){
		this.media.addEventListener("durationchange",this.setTimeline.bind(this),false);
		if(this.browser == "Safari"  && this.movie_options.ref_tc_IN == null){
			initCanplaySafari = function(){
				if(typeof this.timeline != "undefined" ){
					// MS - 19.06.14 - hack pour forcer le repaint de la première image de la vidéo sur Safari si range.start == 0;
                    // constaté sur les vidéos du CNC du projet inadl
					if(this.movie_options.xStand && this.timeline.range.start == 0 && !this.playlist){
						this.timeline.setValue(this.movie_options.frameTimeValue);
                        setTimeout(function(){
                            this.timeline.setValue(0);
                        }.bind(this),150);
					}else{
						this.timeline.setValue(this.timeline.range.start);
					}
					if(this.media.src.indexOf('.m3u8') !== false){
						this.media.removeEventListener("canplay",initCanplaySafari,false);
					}
				}
			}.bind(this);
			this.media.addEventListener("canplay",initCanplaySafari,false);
		}
	} else if (this.browser == "Explorer"){
	  this.media.addEventListener("canplay",this.delayedTimelineForIE.bind(this),false);
	}

	
	// sur la première track d'une playlist, si on a ref_tc_in défini, alors on veut jump à ref_tc_in une fois qu'on a le media 
	if(this.movie_options.ref_tc_IN != null ){
		init_ref_tcin = function(){
			this.GoToLongTimeCode(this.movie_options.ref_tc_IN);
			// cette action n'étant necessaire qu'une fois, on supprime aussi immédiatement l'event listener apres execution
			this.media.removeEventListener('durationchange',init_ref_tcin,false);
		}.bind(this);
		this.media.addEventListener("durationchange",init_ref_tcin,false);
	}

	if(this.controlSeek == "Events"){
 		this.media.addEventListener("seeked",this.AllowSeeking.bind(this),false);
 		this.media.addEventListener("seeking",this.BlockSeeking.bind(this),false);
	}

	
	if(typeof this.movie_options.onMovieReady == 'function'){
		try{
			this.onMovieReady_handler = function(evt){
				this.movie_options.onMovieReady();
				this.media.removeEventListener('loadedmetadata',this.onMovieReady_handler,false);
			}.bind(this);
			this.media.addEventListener('loadedmetadata',this.onMovieReady_handler,false);
		}catch(e){
			console.log("OWH_player - crash callback onMovieReady :"+e);
		}
	}

	this.mediaFullyLoaded = false;
	this.stopUpdateRange = false;
}


OWH_player.prototype.initStreamHLSMediaEvents= function(){
	this.media.addEventListener("click", this.ToggleVideo.bind(this),false);
	this.media.addEventListener("play", this.changeButtonPlay.bind(this),false);
	this.media.addEventListener("pause", this.changeButtonPlay.bind(this),false);
	this.media.addEventListener("volumechange", this.defaultControlVolumeChange.bind(this), false);
	this.media.addEventListener("ended",this.StopTheVideo.bind(this),false);	
	if(typeof this.movie_options.onMovieReady == 'function'){
		try{
			this.onMovieReady_handler = function(evt){
				this.movie_options.onMovieReady();
				this.media.removeEventListener('loadedmetadata',this.onMovieReady_handler,false);
			}.bind(this);
			this.media.addEventListener('loadedmetadata',this.onMovieReady_handler,false);
		}catch(e){
			console.log("OWH_player - crash callback onMovieReady :"+e);
		}
	}
	if(this.resizeCustom){
		this.media.addEventListener("canplay",this.resizeVideoKeepRatio.bind(this),false);	
	}
	this.mediaFullyLoaded = false;
	this.stopUpdateRange = false;
}


OWH_player.prototype.CheckSwitchMovie = function(time){
	if (this.movie_options.switched) {
		if(this.prerolling){
			this.media.currentTime = this.movie_options.switched_time;
		}else{
			this.timeline.setValue(this.movie_options.switched_time);
		}
		this.movie_options.switched_time = 0;
		this.movie_options.switched = false;

		this.media.removeEventListener("durationchange", this.CheckSwitchMovie.bind(this),false );
		this.media.addEventListener("durationchange", this.delayedDurationInit.bind(this),false );
	}
}

OWH_player.prototype.QualitySwitch = function(switch_element){
	// if (myVideo) myVideo.SwitchMovie(myVideo.movie_options.urls[this.value], myVideo.movie_options.typesMime[this.value]);
	this.quality_current_idx = $j(switch_element).find("option:selected").val();
	if(this.prerolling && this.preroll_urls != ''){
		this.SwitchMovie(this.movie_options.preroll_urls[this.movie_options.quality_names[this.quality_current_idx]], this.movie_options.typesMime[this.quality_current_idx]);
	}else{
		this.SwitchMovie(this.movie_options.urls[this.quality_current_idx], this.movie_options.typesMime[this.quality_current_idx]);
	}
}

OWH_player.prototype.SwitchMovie = function(url,typeMime,time,poster_url){
	if(typeof poster_url == 'undefined'){
		poster_url = '';
	}

	this.dontCallOnChange=true;
	this.StopTheVideo();
	if(typeof time == 'undefined'){
		if(this.prerolling){
			time = this.media.currentTime ;
		}else{
			time = this.timeline.value ;
		}
	}
	this.media.poster = poster_url;
	this.media.src = url;
	this.media.type = typeMime;
	//var autoplay = this.media.autoplay;
	//this.media.autoplay = false;

	this.movie_options.switched_time = time;
	this.movie_options.switched = true;
	this.media.removeEventListener("durationchange", this.delayedDurationInit.bind(this),false );
	this.media.addEventListener("durationchange", this.CheckSwitchMovie.bind(this),false );
}

OWH_player.prototype.delayedDurationInit = function(){
	if(!this.playlist){
		this.duration = this.media.duration;
	}
	// init affichage durée max
	if(document.getElementById('timecodeDuration')){
		if(this.movie_options.timecode_OUT && this.movie_options.timecode_lag){
			document.getElementById('timecodeDuration').innerHTML = this.DisplayTimeCode(this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag));
		} else if(this.movie_options.timecode_OUT){
			document.getElementById('timecodeDuration').innerHTML = this.DisplayTimeCode(this.timecodeToSecs(this.movie_options.timecode_OUT));
		} else {
			document.getElementById('timecodeDuration').innerHTML = this.DisplayTimeCode(this.duration);
		}
	}
}


//TESTIE
OWH_player.prototype.delayedTimelineForIE = function(){
	setTimeout(this.setTimeline.bind(this),1000);
}





OWH_player.prototype.AttachMovieController = function(){

	this.firstResizeDone = false ;

	this.controllerpanel = document.createElement('div');					//création div controllerpanel
	this.controllerpanel.id = 'ow_controllerpanel';
	this.controllerpanel.innerHTML = this.movie_options.code_controller;	//update controllerpanel depuis le code_controller

	if(this.movie_options['fit_height_container']){
		this.controllerpanel.style.visibility = "hidden";
		 //this.controllerpanel.style.visibility = "";
		// console.log("setInvisible");
	}

	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	if (!(typeof this.container === 'undefined' || this.container=== null)) {
		this.container.appendChild(this.controllerpanel);
	}
	//ajout du controllerpanel au container

	if (this.movie_options.displayLoopButton==false)
	{
		$j('#ow_bouton_loop').hide();
	}
	// creation des éléments internes aux sliders

	if(!this.movie_options.extsBtnsDisabled && (typeof myPanel != "undefined" || this.movie_options.extsBtnsOnStart)){
		this.showExtsButtons();
	}else{
		this.hideExtsButtons();
	}
	if(document.getElementById('ow_id_progress_slider')){
		backgroundTimeline = document.createElement('div');
		backgroundTimeline.id = "backgroundTimeline";

		progresstrack = document.createElement('div');
		progresstrack.id = "progress_track";

		progresshandle = document.createElement('div');
		progresshandle.id = "progress_handle";


		if(this.controlSeek == "Ranges"){
			bufferedDisplay = document.createElement('div');
			bufferedDisplay.id = "0bufferedDisplay";
			this.displayBufferedRangesDivs.push(bufferedDisplay);
			bufferedDisplay.className = "bufferedDisplayDivs";
		}



		progresstrack.appendChild(progresshandle);


		if(!this.selectionSliderDisabled){
		selection = document.createElement('div');
		selection.id = "selection";


		selecttrack = document.createElement('div');
		selecttrack.id = "select_track";

		handleBegSel = document.createElement('div');
		handleBegSel.id = "handle_BegSel";
		handleBegSel.className = "handles_select";
		handleEndSel = document.createElement('div');
		handleEndSel.id = "handle_EndSel";
		handleEndSel.className = "handles_select";

		selecttrack.appendChild(selection);
		selecttrack.appendChild(handleBegSel);
		selecttrack.appendChild(handleEndSel);
		}

		ow_progress_slider = document.getElementById("ow_id_progress_slider");
		ow_progress_slider.appendChild(backgroundTimeline);

		if(this.movie_options.span_time_passed){
			spanpassed = document.createElement('div');
			spanpassed.id = "ow_time_passed";
			ow_progress_slider.appendChild(spanpassed);
		}

		if(this.movie_options.xml_sequences != null ){
			this.sequences_wrapper = document.createElement('div');
			this.sequences_wrapper.id = "ow_seq_wrapper";
			ow_progress_slider.appendChild(this.sequences_wrapper);
		}
		
		ow_progress_slider.appendChild(progresstrack);

		if(this.controlSeek == "Ranges"){
			bufferedDisplayContainer = document.createElement('div');
			bufferedDisplayContainer.id = "bufferedDisplayContainer";
			bufferedDisplayContainer.style.marginTop = "-"+(this.movie_options['hauteur_track']/2+this.movie_options['hauteur_bg_track']/2)+"px";
			ow_progress_slider.appendChild(bufferedDisplayContainer);
			bufferedDisplayContainer.appendChild(bufferedDisplay);
		}
		else
		{
			bufferedDisplayContainer = document.createElement('div');
			bufferedDisplayContainer.id = "bufferedDisplayContainer";
			bufferedDisplayContainer.style.marginTop = "-"+(this.movie_options['hauteur_track']/2+this.movie_options['hauteur_bg_track']/2)+"px";
			ow_progress_slider.appendChild(bufferedDisplayContainer);
			bufferedDisplayContainer.style.height=backgroundTimeline.offsetHeight+'px';
		}


		if(!this.selectionSliderDisabled){
		//	ow_progress_slider.appendChild(selection);
			ow_progress_slider.appendChild(selecttrack);
		}
		progresstrack.style.marginTop = "-" + (this.movie_options['hauteur_bg_track'] + (this.movie_options['hauteur_track'] - this.movie_options['hauteur_bg_track'])/2)+"px";
		//alert((backgroundTimeline.clientHeight + (progresstrack.clientHeight - backgroundTimeline.clientHeight)/2));

	}

	//init du sélecteur de qualité
	$j(this.container).find("#ow_quality_switch").hide();
	if(this.movie_options.merge_media_subtitles_selectors  == false){
		if( Object.prototype.toString.call(this.movie_options.urls) === '[object Array]' && this.movie_options.urls.length > 0) {
			this.quality_current_idx = 0 ; 
			var code= '<select >';
			for (key in this.movie_options.quality_names) {
				if (this.movie_options.quality_names.hasOwnProperty(key) && this.movie_options.urls.hasOwnProperty(key)){
					if (typeof str_lang !="undefined" && typeof str_lang[this.movie_options.quality_names[key]]=="string"){
						quality_label = str_lang[this.movie_options.quality_names[key]];
					} else {
						quality_label = this.movie_options.quality_names[key];
					}
					code += "<option value='"+key+"'>"+quality_label+ "</option>";
				}
			}
			code += "</select>";
			$j(this.container).find("#ow_quality_switch").on('change',function(){this.QualitySwitch($j(this.container).find("#ow_quality_switch").get(0));}.bind(this));
			$j(this.container).find("#ow_quality_switch").html(code);
			$j(this.container).find("#ow_quality_switch").css('display', 'inline-block');//show();
		}
	}

	// init des sliders scriptaculous
	if(document.getElementById('ow_id_progress_slider')){
		this.InitTimeline();
	}
	if(document.getElementById('ow_id_jog_slider')){
		handlejog = document.createElement('div');
		handlejog.id = 'ow_id_jog_handle';
		document.getElementById("ow_id_jog_slider").appendChild(handlejog);
		this.InitJog();
	}
	if(document.getElementById('ow_id_sound_slider')){
		handlesound = document.createElement("div");
		handlesound.id = "ow_sound_handle";
		document.getElementById("ow_id_sound_slider").appendChild(handlesound);
		this.InitSoundSlider();
	}

	// Association des handlers aux boutons existants en fonction de l'objet basic_function_button
	for (o in this.movie_options.basic_function_button){
		if(document.getElementById(o)){
			if(o=="ow_bouton_fullscreen"){
				document.getElementById(o).onclick = this.movie_options.basic_function_button[o];
			}else{
			document.getElementById(o).onmousedown = this.movie_options.basic_function_button[o];
			}
		}
	}


    if(this.mediaType == "audio"){
		if(document.getElementById("ow_bouton_fullscreen")){
			document.getElementById("ow_bouton_fullscreen").addClassName("audioHide");
			this.controllerpanel.style.visibility = "visible";
		}
		if(!this.movie_options.show_vignette_audio ){
			document.getElementById("ow_controllerpanel").style.top = "0px";
		}
	}

    if(document.getElementById('ow_bouton_step_forward')){
		document.getElementById("ow_bouton_step_forward").addEventListener('mousedown', function(){document.getElementById("ow_bouton_step_forward").addClassName("ClickBouton");},false);
    }
	if(document.getElementById('ow_bouton_step_rewind')){
		document.getElementById("ow_bouton_step_rewind").addEventListener('mousedown', function(){document.getElementById("ow_bouton_step_rewind").addClassName("ClickBouton");},false);
    }

    //document.getElementById("boutonInfo").addEventListener('click', this.PrintInfos.bind(this),false);




	if(this.movie_options.language == 'fr'){
		this.tooltips = this.movie_options.tooltips_fr;
	} else {
		this.tooltips = this.movie_options.tooltips_en;
	}

	for (o in this.tooltips){
		if(document.getElementById(o)){
			document.getElementById(o).title = this.tooltips[o];
		}
	}


	//**********************************************************
	// Association de la fonction d'ajustement des timelines à l'event resize (à limiter si jamais la visionneuse a une taille fixe)
	if(this.browser == "Chrome" || this.browser == "Explorer" || this.browser=="Firefox"){
		this.resizeListener = function(){
			//console.log("resizeListener");
			if(this.allowAdjustTimelineOnResize){
				this.allowAdjustTimelineOnResize=false;
				if(this.mediaType == "video"  && !this.movie_options['fit_height_container'] && !this.fullscreen){
					// check if variable exist (not null AND not undefined) to not trigger javascript ERROR OWH_player.js:1143 Uncaught TypeError: Cannot read property 'id' of null
					if (!(typeof this.container.parentNode === 'undefined' || this.container.parentNode=== null)) {
						minHeight = (parseInt(document.getElementById(this.container.parentNode.id).offsetWidth)*this.movie_options['height']/this.movie_options['width']);
						if(!this.movie_options.controller_on_video){
							minHeight = minHeight + this.controllerpanel.offsetHeight;
						}
						this.container.style.minHeight = minHeight+"px";
					}
				}else if (this.mediaType == 'audio' && this.movie_options.show_vignette_audio  && !this.movie_options['fit_height_container']){
					minHeight = (parseInt(document.getElementById(this.container.parentNode.id).offsetWidth)*this.audio_vignette.offsetHeight/this.audio_vignette.offsetWidth);
					if(!this.movie_options.controller_on_video){
						minHeight = minHeight + this.controllerpanel.offsetHeight;
					}
					this.container.style.minHeight = minHeight+"px";
				}
				if(this.resizeCustom){		// Fonction de resize custom (on garde le ratio video),
					this.resizeVideoKeepRatio();}		// et placement du main_container dans le #container
				if (this.movie_options.infos.visu_type.match(new RegExp(/(stream_)/)) == null){
					this.adjustTimeline();
				}
				this.allowAdjustTimelineOnResize = true;
			}
		}.bind(this);
		//window.onresize = limitForChrome.bind(this);
		window.addEventListener('resize',this.resizeListener,false);
	} else {
		this.resizeListener = function(){
			//console.log("resizeListener");
			if(this.allowResizeVideo){
				if(this.mediaType == "video" && !this.movie_options['fit_height_container'] && !this.fullscreen){
					 minHeight =(parseInt(document.getElementById(this.container.parentNode.id).offsetWidth)*this.movie_options['height']/this.movie_options['width']);
					 if(!this.movie_options.controller_on_video){
						minHeight = minHeight + this.controllerpanel.offsetHeight;
					 }
					 this.container.style.minHeight = minHeight+"px";
				}
				if(this.resizeCustom){		// Fonction de resize custom (on garde le ratio video),
					this.resizeVideoKeepRatio();		// et placement du main_container dans le #container
				}
				if (this.movie_options.infos.visu_type.match(new RegExp(/(stream_)/)) == null){
					this.adjustTimeline();
				}
				if(this.display_buff){
					this.displayBufferedRange();
				}
				//salert(this.allowResizeVideo);
			}
		}.bind(this);
		//window.onresize = limitForSafari.bind(this);
		window.addEventListener('resize',this.resizeListener,false);

	}



    // besoin d'un test du player option
    if(this.verticalFixed){
		this.verticalFixedBehavior();	// init du comportement fixed en vertical, absolute en horizontal pour le container
    }

	if(!this.selectionSliderDisabled){
		this.BegSelect = this.timelineSel.values[0];
		this.EndSelect = this.timelineSel.values[1];
	}

    if(this.movie_options.selectionHidden && !this.selectionSliderDisabled){
        this.HideSelectionSlider();
	}

	if( !this.resizeCustom){
		if(this.movie_options['fit_height_container']){
			document.getElementById('ow_controllerpanel').style.visibility = "visible" ;
		}
		this.firstResizeDone = true ;
	}

	if(this.movie_options.controller_on_video && this.dynamicController){
		this.makeMouseActivityToggleController();
	}
	
	this.initRatioSelect() ; 
}




//Fonction qui permet de resize le main_container a l'interieur du div container en gardant un aspect ratio video / controller toujours OK
OWH_player.prototype.resizeVideoKeepRatio = function(){		// NEWLAYOUTVISIO
	//console.log("resizevideokeepratio");
	if( this.mediaType == "video"){
		this.container.minHeight = "auto";
	}

	if(this.movie_options.force_ratio_ref){
		var ratioVid = this.movie_options.force_ratio_ref;
	}else if (this.mediaType == 'audio' && this.movie_options.show_vignette_audio){
		/*if(this.audio_vignette.offsetHeight != 0 ){
			var ratioVid =this.audio_vignette.offsetWidth/this.audio_vignette.offsetHeight;
		}else{*/
		// MS - 08.04.16 - on va chercher à se baser sur une taille de viewport définie en mode absolute ou avec une taille renseignée. L'image à l'intérieur du viewport se redimensionnera en fonction du viewport, 
		// on a alors besoin de faire les calculs de resize en fonction de la taille du audio_viewport et non en fontion de la taille de la vignette
		var ratioVid =this.audio_viewport.style.width/this.audio_viewport.style.height;
		// }
	}else if (this.movie_options['width'] && this.movie_options['height']){
		// Si les dimensions de la vidéos sont correctement récupérées depuis la base,
		// on les utilise en priorité par rapport aux informations de l'élément vidéo (IE donnait parfois une valeur par défaut 4/3 qui posait probleme si la vidéo etait en realité une 16/9)
		var ratioVid = this.movie_options['width']/this.movie_options['height'];
	}else{
		var ratioVid = this.media.videoWidth/this.media.videoHeight;
	}


	// ratioVid = Math.round(100*ratioVid)/100;
	var ratioCapsVid = $('capsVid').offsetWidth / $('capsVid').offsetHeight;
	// ratioCapsVid = Math.round(100*ratioCapsVid)/100;
	// console.log("ratioVid "+ratioVid+" ratioCapsVid "+ratioCapsVid);

	var video_elem ;
	if(this.mediaType == 'audio' && this.movie_options.show_vignette_audio){
		video_elem = this.audio_viewport;
	}else if(this.playlist){
		video_elem = this.track_media[this.current_track];
	}else{
		video_elem = this.media;
	}

	if(typeof video_elem == 'undefined'){
		return false ;
	}

	// si le ratioVid >= ratioCapsVid (cas hauteur >>> largeur)
	if(this.movie_options.force_ratio_ref && this.fullscreen){
		// Permet d'unifier l'affichage en 16/9 ou 4/3 en mode fullscreen pour toutes les vidéos de la playlist
		if(this.playlist){
			$$('video[id^="video"]').each(function(vid_elt){
				vid_elt.style.width = (this.movie_options.force_ratio_ref * this.capsulevideo.offsetHeight)+"px";
				vid_elt.style.left = (( this.capsulevideo.offsetWidth-this.movie_options.force_ratio_ref * this.capsulevideo.offsetHeight)/2) +"px" ;
			}.bind(this));
		}
	}
	else if (this.movie_options.force_ratio_ref){
		if(this.playlist){
			$$('video[id^="video"]').each(function(vid_elt){
				vid_elt.style.width ="";
				vid_elt.style.left = "";
			}.bind(this));
		}
	}

	if(this.fullscreen && (this.movie_options.fillFullscreen || (this.movie_options.fit_height_container && this.dynamicController))){
		video_elem.style.height = "";
		video_elem.style.width = "" ;
		$('ow_controllerpanel').style.top = "";


	}else{
		if (Math.round(100*ratioVid)/100 > Math.round(100*ratioCapsVid)/100){
			if ( !this.playlist && this.browser == "Explorer"){
				video_elem.style.width = "100%";
				if(this.fullscreen){
					this.capsulevideo.style.bottom = "auto";
				}else{
					this.capsulevideo.style.bottom = "";
				}
			}
			// check if variable exist (not null AND not undefined) to not trigger javascript ERROR Cannot read property 'style' of null
			if (!(typeof video_elem === 'undefined' || video_elem=== null)) {
				video_elem.style.height =(video_elem.offsetWidth * (1/ratioVid))+"px";	// On regle la hauteur de la video
				if (this.movie_options.force_width_controller == false){
					$('ow_controllerpanel').style.width = video_elem.offsetWidth +"px";	// on ajuste le controller panel a la meme largeur, et il est centré via css
				}
			}
			
		// sinon cas largeur >>> hauteur
		} else{
			if(this.browser == "Explorer"){this.capsulevideo.style.bottom = "";}
			// check if variable exist (not null AND not undefined) to not trigger javascript ERROR Cannot read property 'style' of null
			if (!(typeof video_elem === 'undefined' || video_elem=== null)) {
				video_elem.style.height = $('capsVid').offsetHeight+ "px";	// la hauteur de l'element video est limité a la hauteur de la capsvid 
			}
			if (this.movie_options.force_width_controller == false){
				// check if variable exist (not null AND not undefined) to not trigger javascript ERROR Cannot read property 'offsetHeight' of null
				if (!(typeof video_elem === 'undefined' || video_elem=== null)) {
					$('ow_controllerpanel').style.width = (video_elem.offsetHeight * ratioVid)+"px"	// le controller panel est set a 100% de la width de la video
				}
			}
			if (this.movie_options.force_ratio_ref || (!this.playlist && this.browser == "Explorer")){
				video_elem.style.marginLeft = "auto"
				video_elem.style.marginRight = "auto";
				video_elem.style.left = "0px";
				video_elem.style.right  = "0px";
				video_elem.style.width = (video_elem.offsetHeight * ratioVid)+"px"	// le controller panel est set a 100% de la width de la video
			}
		}
	}


	//$('ow_controllerpanel').style.width = (video_elem.offsetHeight * this.media.videoWidth / this.media.videoHeight)+"px";




	if(!(this.fullscreen && this.dynamicControllerFS && !this.dynamicController)){
		// check if variable exist (not null AND not undefined) to not trigger javascript ERROR Cannot read property 'offsetHeight' of null
		if (!(typeof video_elem === 'undefined' || video_elem=== null)) {
			// on reajuste l'empalcement du controller par rapport a l'endroit ou on est
			$('ow_controllerpanel').style.top = (video_elem.offsetHeight)+"px";
		}
	}


	if(!this.firstResizeDone && this.dynamicController){
		this.func_showController({clientX:0,clientY :0});
	}

	if(!this.firstResizeDone && this.movie_options['fit_height_container']){
		document.getElementById('ow_controllerpanel').style.visibility = "visible" ;
	}

	
	if(this.mediaType!="audio" && this.movie_options.previewOverTimeline && document.getElementById("ow_prevOverContainer").className==""){
		if((this.media.videoWidth/this.media.videoHeight)>1.5 ){
			document.getElementById("ow_prevOverContainer").className="form_16_9";
		}else{
			document.getElementById("ow_prevOverContainer").className="form_4_3";
		}
	}

	this.firstResizeDone = true ;
}

// MS 12/02/12
// Fonction permettant d'avoir le comportement fixed en scroll vertical (cas usuel : la visionneuse reste a l'écran autant que possible)
// tout en conservant un comportement absolute sur le scroll horiazontal
OWH_player.prototype.verticalFixedBehavior = function () {
	var leftdefault = document.getElementById('container').offsetLeft;		// Initialisation des valeurs défaut en fonction de l'offset reglé
	var topdefault = document.getElementById('container').offsetTop;		// => devrait fonctionner qqsoit le placement du container
	var rightdefault = parseInt(window.getComputedStyle($('container')).getPropertyValue('right')); // valeur du right parametré dans le css
	var tempTop=0;		// on garde en memoire le top courant, on ne le change que si on a scroll
	var getScrollTop, getScrollLeft;

	if(this.browser == "Safari" || this.browser == "Chrome"){
		getScrollTop = function(){return document.body.scrollTop;};
		getScrollLeft = function(){return document.body.scrollLeft;};
	} else if (this.browser =="Explorer" ){
		getScrollTop = function(){return document.documentElement.scrollTop;};
		getScrollLeft = function(){return document.documentElement.scrollLeft;};
	}


	//document.body.scrollTop document.documentElement.scrollTop

	window.onscroll = function(){	// sur l'evenement scroll


		if(getScrollTop() < topdefault-9){	// si on est en haut de la page (+/- taille entete)
				document.getElementById('container').style.position = "absolute";	// on passe en mode absolute
				document.getElementById('container').style.top = topdefault+"px";		// fixation de la video comme sur la demo
				document.getElementById('container').style.left = leftdefault+"px";
				document.getElementById('container').style.right = rightdefault+"px";

		}else {						// Sur le reste de la page :
				document.getElementById('container').style.position = "fixed";	// visionneuse fixed pour deplacement vertical
				if(tempTop != getScrollTop()){		//On ne reinitialise la valeur que si on a changé de scroll vertical
					document.getElementById('container').style.top = "10px";	// calée en haut de la fenetre
				}
				document.getElementById('container').style.left = (leftdefault-getScrollLeft()) +"px";	//  on parametre le left par rapport au décalage de la fenetre avec le doc
				document.getElementById('container').style.right = (rightdefault+getScrollLeft())+"px";
		}


		tempTop = getScrollTop();	// maj du scrollTop courant

	}
}


OWH_player.prototype.rateHasChanged = function(){
    this.rateWasChanged = true;

}





OWH_player.prototype.toggleLoopVideo=function()
{
	if (this.loopVideo==false)
	{
		$('ow_bouton_loop').addClassName('activated');
		this.loopVideo=true;
	}
	else
	{
		$('ow_bouton_loop').removeClassName('activated');
		this.loopVideo=false;
	}
}

// Fonction utilisée tres (trop) souvent pour afficher les valeurs des différents attributs importants de la vidéo lors de la prog // pourra etre utile pour le debugage ?
OWH_player.prototype.PrintInfos = function(){
	//alert(document.getElementById("progress_track").clientHeight);


}





//==========================================PLAY / PAUSE
// Fonction qui lance la lecture
OWH_player.prototype.PlayTheVideo = function() {

	if (this.fastSteppingOn!=null){				// Si avance/recul rapide
		this.StopTheVideo();						// stoppe avant la reprise
	}


	//	if (this.selectionExists &&(this.GetCurrentTime() < this.BegSelect || this.GetCurrentTime()>=this.EndSelect)){
	if(this.movie_options.infos.visu_type != "stream_hls"){
		if(this.outOfSelectionTest(this.GetCurrentTime())|| (this.selectionExists && Math.round(this.GetCurrentTime()*100)/100 == Math.round(this.EndSelect*100)/100)){
			// si la selection existe et que l'on est hors des bornes de selection
			this.GoToBegSelection();				// on renvoi au début de la selection
		}else{
			var currentTC = this.timeline.value;
			if(this.movie_options.timecode_lag){
				currentTC += this.timecodeToSecs(this.movie_options.timecode_lag);
			}
			if(this.movie_options.timecode_OUT && (currentTC >= this.timecodeToSecs(this.movie_options.timecode_OUT))){
				this.GoToBegAvailable();
			}
		}
	}

    if(this.rateWasChanged){
        if(Math.round(this.GetCurrentTime()*100)/100 == Math.round(this.BegSelect*100)/100){
            this.bugAfterRateChange = true;
		}
	}


	this.media.play();			// lecture de la vidéo
}
// Fonction qui met en pause la lecture (et stoppe avance/recul rapide)
OWH_player.prototype.StopTheVideo = function() {
//alert('stop');

	if(this.fastSteppingOn!= null){		// si on est en avance/recul rapide
		this.StopFastStepping();		// on stoppe la repetition des sauts
	}
	// $("infobuff").innerHTML = "slowsteppingOn : "+(this.slowSteppingOn!=null?"défini":"null");
    if(this.slowSteppingOn != null){
        this.slowSteppingOn.stop();
        this.slowSteppingOn = null;
		this.slowStepValue = null ;
	}
    if(this.media && this.media.playbackRate != 1){
		this.media.playbackRate = 1;
	}



	this.media.pause();			// mise en pause de la video
}
// Fonction qui assure le switch lecture / pause
OWH_player.prototype.ToggleVideo = function(){
	if ((this.media.ended || this.media.paused) && this.fastSteppingOn == null){
		this.PlayTheVideo();
	} else {
		this.StopTheVideo();

	}
}

OWH_player.prototype.ToggleVideoSequence = function(){
	if ((this.media.ended || this.media.paused)){
		this.StopTheVideo();
	} else {
		this.PlayTheVideo();

	}
}


//==========================================STEP BY STEP

// Init Steppings
OWH_player.prototype.InitStepping = function (styleOfStepping, stepvalue){
	// def de variables (this ne fonctionne pas dans les PeriodicalExec)
	var self = this;
	var StepVideo = this.StepVideoMethod;					// Fonction d'étape correspondant au browser
	var freq = this.movie_options['stepping_frequency'];	// frequence de modification, passage seconde >> millisecondes
	var JogSteppingFilter = this.JogSteppingFilter;

    tempFastSteppingDirection = this.fastSteppingDirection;

	// Si la vidéo est en lecture ou si un fastStepping est en cours
	if (!this.media.paused || this.fastSteppingOn != null){
		this.StopTheVideo();	// on arrete la video et le stepping
	}
	if (styleOfStepping=="frame"){
		// ************FRAME BY FRAME******************
		var sync = this.CalcSyncIfNeeded();
		StepVideo(stepvalue,self,sync);
		// initialisation éxecution périodique de la fonction step limitée
		this.slowStepValue = stepvalue;
		this.slowSteppingOn = new PeriodicalExecuter(function(){StepVideo(stepvalue,self,sync);},0.50);
        var tempSlowStepp = this.slowSteppingOn;
		// init du listener qui stoppe l'execution periodique quand on lache la souris
		window.onmouseup = function(){
            if(tempSlowStepp != null){
                tempSlowStepp.stop();
                tempSlowStepp = null;
			}
            document.getElementById("ow_bouton_step_forward").removeClassName("ClickBouton");
            document.getElementById("ow_bouton_step_rewind").removeClassName("ClickBouton");
		}


	} else if (styleOfStepping == "fast"){
		// ************FAST FORWARD/REWIND**************

        if(stepvalue > 0){
            this.fastSteppingDirection = 1;
        } else {
            this.fastSteppingDirection = -1;
        }
        if(this.fastSteppingDirection != tempFastSteppingDirection){
            if(this.fastSteppingDirection >0){
                document.getElementById("ow_bouton_fast_forward").addClassName("ClickBouton");
            }else{
                document.getElementById("ow_bouton_fast_rewind").addClassName("ClickBouton");
            }

            if(this.outOfSelectionTest(this.GetCurrentTime())){
                this.GoToBegSelection(); // on se place au debut de la selection
            }
            this.fastSteppingOn = new PeriodicalExecuter(function(){StepVideo(stepvalue,self);},freq);
            this.changeButtonPlay();
        } else {
            this.fastSteppingDirection = 0;
            this.StopTheVideo();
		}


	} else if (styleOfStepping == "jog"){
		//***************JOG STEPPING************************
		this.jogSteppingOn = new PeriodicalExecuter(function(){JogSteppingFilter(self,StepVideo);},freq);

	}


}

// Fonction permettant de stopper le stepping
OWH_player.prototype.StopFastStepping = function (){
	// si le stepping est en cours
	if (this.fastSteppingOn != null){
        if(this.mediaType == "video"){
            this.fastSteppingOn.stop();	// on stop le stepping
        }else if(this.mediaType=="audio"){
			this.media.playbackRate=1;
		}
        this.fastSteppingOn = null;	// on réinitialise le flag de stepping
        this.fastSteppingDirection = 0;
		this.changeButtonPlay();

        if(document.getElementById("ow_bouton_fast_forward").hasClassName("ClickBouton")){
            document.getElementById("ow_bouton_fast_forward").removeClassName("ClickBouton");
        } else {
            document.getElementById("ow_bouton_fast_rewind").removeClassName("ClickBouton");
        }



		// alert(this.dontCallOnChange+"  ");
		//****************************RANGES "MAISON"
		if(this.controlSeek == "Ranges"){		// Si on utilise la méthode des ranges
			this.changeRangeCheck(this.GetCurrentTime());		// test de range (current ? création?)
            this.updateCurrentRange();
        }
	}
}


// Fonction permettant d'attributer la bonne fonction de vitesse variable aux boutons suivant le cas d'un lecteur video ou audio
OWH_player.prototype.FastPlaying = function(direction) {
    if(this.mediaType == "video"){
        if(direction == 1){
            this.InitStepping("fast",1);
        } else {
            this.InitStepping("fast",-1);
        }
    } else {
        if(direction == 1 ){
            this.varSpeedPlaying(10);
        } else {
            this.varSpeedPlaying(-10);
        }
    }

}





// Fonction de vitesse variable utilisée pour le faststepping en mode audio
OWH_player.prototype.varSpeedPlaying = function(playbackRate){
    if(!this.media.paused && this.media.playbackRate == playbackRate ){
        this.StopTheVideo();
        if(playbackRate > 0){
            document.getElementById("ow_bouton_fast_forward").removeClassName("ClickBouton");
        } else {
            document.getElementById("ow_bouton_fast_rewind").removeClassName("ClickBouton");
        }
		this.fastSteppingOn = null;

    } else{
        this.StopTheVideo();
        this.PlayTheVideo();
        this.media.playbackRate = playbackRate;

        this.fastSteppingOn = 1;
        if(playbackRate > 0){
            document.getElementById("ow_bouton_fast_forward").addClassName("ClickBouton");
        } else {
            document.getElementById("ow_bouton_fast_rewind").addClassName("ClickBouton");
        }

    }
}


// Fonction permettant d'attributer la fonction de stepping aux boutons avec les bons parametres suivant le cas d'un lecteur video ou audio
OWH_player.prototype.SlowPlaying = function(direction){
    if(this.mediaType == "video"){
        if(direction == 1){
            this.InitStepping("frame",this.movie_options.frameTimeValue);
        } else {
            this.InitStepping("frame",-this.movie_options.frameTimeValue);
        }
    } else {
        if(direction == 1 ){
            this.InitStepping("frame",1);
        } else {
            this.InitStepping("frame",-1);
        }
    }
}






//"Frame by Frame" pour Chrome, OBSOLETE, cette fonction n'est plus utilisée.
OWH_player.prototype.StepByStepForChrome = function() {
	this.StopTheVideo();
	// Premier click permet de set la vitesse de lecture à 0.10
	if(this.media.defaultPlaybackRate > 0.11){
		this.media.defaultPlaybackRate = 0.10;

	}else{	// second click lance la lecture à vitesse lente
		this.media.play();

		//Fonction RaZ lecture vitesse variable
		StopSlowMotion = function(){
			this.StopTheVideo();			// stop la lecture vitesse variable
			this.media.defaultPlaybackRate = 1;	// init de la vitesse de lecture classique
			window.onmouseup = null;		// suppression de l'event listener
		}
		// Quand on relache la souris, on appelle la RaZ
		window.onmouseup = StopSlowMotion.bind(this);

	}
}

// Etape de stepping limitée pour Chrome
OWH_player.prototype.LimitSeekStepVideo = function (stepvalue, self,sync){
	// if(stepvalue == this.movie_options.frameTimeValue || stepvalue == -this.movie_options.frameTimeValue )
		// $("infobuff").innerHTML = "slowsteppingOn : step ";

	// Si on peut seek
	if (self.canSeek){  // test sur self.canSeek sur chrome, on test avec this.media.seeking pour safari
		//self.media.currentTime += (stepvalue+self.retardSeek);		// on ajoute un step + le retard à la video
		self.timeline.setValue(self.timeline.value + stepvalue + self.retardSeek);
		if(sync != null){
			self.timelineSel.setValue(self.media.currentTime+self.retardSeek, sync);
			if (self.movie_options.onSelectionSliderChange){
				self.movie_options.onSelectionSliderChange();
			}
		}

		self.retardSeek = 0;				// RaZ du retard
		// Si le seek est bloqué
	} else {
		// stockage du retard
		self.retardSeek += stepvalue;
	}

}



// Fonction assurant l'avancée step by step de la stepvalue en argument et avec self = this (probleme de setInterval)
OWH_player.prototype.LimitRangeStepVideo = function(stepvalue,self,sync) {
	//self.outOfBufferedRange(self.media.currentTime);
	if(!self.outOfCurrentRange(self.media.currentTime)){
		if (self == null){			// si aucune valeur self n'est rentré,
			self = this;			// initialisation a this (pas possible lors d'un setInterval)
		}

		self.timeline.setValue(self.timeline.value + stepvalue);		// saut dans la vidéo

		if (sync != null){			// Si l'on doit synchroniser le deplacement avec un des curseurs de selection
			self.timelineSel.setValue(self.media.currentTime, sync);	// synchronisation (sync contient l'index du cvurseur a déplacer)
			if (self.movie_options.onSelectionSliderChange){
				self.movie_options.onSelectionSliderChange();
			}
		}
	}else{
		if (self.delayRangeSeek<10){
			self.delayRangeSeek++;
		} else{
			self.timeline.setValue(self.timeline.value + stepvalue*10);
			self.delayRangeSeek = 0;
		}
		self.outOfBufferedRange(self.media.currentTime);

	}
}

// Fonction filtre selection pour le stepping jog
OWH_player.prototype.JogSteppingFilter = function(self, StepVideo){
	if (!self.selectionExists || self.inTheSelectionTest(self.media.currentTime +(self.jogspeed/10))){
		StepVideo(self.jogspeed/10,self,null);
	} else{
		(self.media.currentTime+(self.jogspeed)/10) < Math.round(self.BegSelect*100)/100? self.GoToBegSelection() : self.GoToEndSelection();
	}
}

// Fonction assurant si besoin la synchronisation du stepping image par image avec les curseurs de selection
OWH_player.prototype.CalcSyncIfNeeded = function(){
	var sync = null;
    if(this.selectionExists){
        if (Math.round(this.GetCurrentTime()*100)/100 == Math.round(this.timelineSel.value*100)/100){
            sync = 0;
        } else if(Math.round(this.GetCurrentTime()*100)/100 == Math.round(this.timelineSel.values[1]*100)/100){
            sync = 1;
        }
    }
	return sync;
}


// ====================== boutons aller début / aller fin
// Fonction aller au début
OWH_player.prototype.GoToBegAvailable = function (){
    this.StopTheVideo();
	if (this.selectionExists){			// Si on a une selection
		this.GoToBegSelection();			// aller au début de la selection
	}else{								// sinon
		if(this.movie_options.ref_tc_IN){
			this.GoToLongTimeCode(this.movie_options.ref_tc_IN);
		}else{
			if(this.playlist) this.ChangeTrack(0);
			this.timeline.setValue(this.timeline.range.start);			// aller au début de la vidéo
		}
		this.UItest();
	}
}

// Fonction aller en fin
OWH_player.prototype.GoToEndAvailable = function (){
    this.StopTheVideo();
	if (this.selectionExists){					// si la selection existe
		this.GoToEndSelection();					// aller en fin de selection
	}else{										// sinon
		if(this.movie_options.ref_tc_OUT ){
			this.GoToLongTimeCode(this.movie_options.ref_tc_OUT);
		}else{
			if(this.playlist) this.ChangeTrack(this.nb_tracks-1);
			this.timeline.setValue(this.timeline.range.end);// aller en fin de video
		}
		this.UItest();
	}
}

//========================Jog/Shuttle
// Initialisation du jogshuttle
OWH_player.prototype.InitJog = function(){
    this.resetjogIDtemp = this.ResetJog.bindAsEventListener(this);

	shuttleUpdate = function(value){
        //alert("shuttleupdate");
		this.CalcJogValue(value);							//  Calcul de la vitesse de lecture
		this.ShuttleMove();
		//document.getElementById('joginfo').innerText = Math.round(this.jogspeed*100)/100+"x";	// actualisation de l'affichage de la vitesse de nav
		this.jogIsUsed = true;		// flag d'utilisation du jog à true
	}

	shuttleChange = function(value){
        //alert("shuttlechange, currentValue =  "+this.jogshuttle.value);
		if(this.jogshuttle.value != 0 && this.jogIsUsed){		// si jog a été utilisé et que la valeur est differente de 0
			this.ResetJog();		// on reset le jog
		} else {
			this.CalcJogValue(value);		// calcul de la vitesse de lecture
			if(value !=0){	// si la valeur est differente de 0
				this.jogIsUsed = true;	// flag d'utilisation du jog à true
				this.ShuttleMove();

            }

			window.addEventListener("mouseup",this.resetjogIDtemp,false);
		}

	}

	this.jogshuttle = new Control.Slider("ow_id_jog_handle","ow_id_jog_slider",{
										 range : $R(-2,2),
										 sliderValue : "0",

										 onSlide : shuttleUpdate.bind(this),
										 onChange : shuttleChange.bind(this)

										 });
}

// calcul de la variable jogspeed à partir du jogshuttle
OWH_player.prototype.CalcJogValue = function(value){
	if (value<-1.5){
		this.jogspeed = 15*value+20;
	} else if (value<-1){
		this.jogspeed = 2*value +1;
	} else if (value <= 1){
		this.jogspeed = value;
	} else if (value < 1.5){
		this.jogspeed = 2*value-1;
	} else {
		this.jogspeed = 15*value -20;
	}
}

// Fonction effectuant les déplacements dans la video à partir de jogspeed
OWH_player.prototype.ShuttleMove = function() {
	// ******************* Implementation en playbackrate uniquement (ne fonctionne nulle part actuellement)
	//	if(this.media.paused){
	//		this.media.play();
	//	}
	//	if(!this.outOfSelectionTest()){
	//		this.media.playbackRate = this.jogspeed;
	//	} else {
	//		this.media.playbackRate = 0;
	//	}
	//	document.getElementById('joginfo').innerText = Math.round(this.jogspeed*100)/100+"x";	// actualisation affichage vitesse de nav



	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Implementation jogshuttle stepping & playbackRate<<<<<<<<<<<<<<<<<<<<<<<<
	// *************************SAFARI ******************************
	if(this.shuttleRatioSteppPlaybackrate()){				// si valeur positive, navigation par vitesse de lecture
		this.StopFastStepping();
		if(this.jogSteppingOn !=null){
			this.jogSteppingOn.stop();
			this.jogSteppingOn = null;
		}
		if(this.media.paused ){		// si video n'est pas en lecture
			this.rateWasChanged = true;
			if(Math.round(this.GetCurrentTime()*100)/100 == Math.round(this.BegSelect*100)/100){
				this.bugAfterRateChange = true;
			}


			this.media.play();			// on la lance
			this.jogIsUsed = true;

		}
		if  (Math.round(this.GetCurrentTime()*100)/100!=Math.round(this.EndSelect*100)/100 || !this.selectionExists){
			this.media.playbackRate = this.jogspeed;	// vitesse de lecture = jogspeed
		} else {
			this.media.playbackRate = 0;
		}

    } else {					// si val négative, navigation par saut

		this.StopTheVideo();	// on stoppe la lecture
		if(this.jogSteppingOn == null){		// si pas de stepping jog en cours
			this.InitStepping("jog",0);					// on en lance un

		}
	}
	document.getElementById("joginfo").style.visibility = "visible";

	document.getElementById('joginfo').innerHTML = Math.round(this.jogspeed*100)/100+"x";	// actualisation affichage vitesse de nav

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}

// Fonction RaZ du jogShuttle lorsqu'on le relache
OWH_player.prototype.ResetJog = function(){
	this.jogspeed = 0;

	this.jogshuttle.setValue(0);

  	this.jogIsUsed = false;

	this.media.playbackRate = 1;
	this.media.pause();
	// MS fix pour firefox => actuellement, la méthode normale de reset du playbackrate à 1 ne fonctionne pas sur FFox, ce workaround semble être suffisant ,
	// On reseek le media à sa position -1 frame pour forcer l'interrogation du playbackrate et donc revenir à une vitesse de lecture normale
	if(this.browser == "Firefox"){this.media.currentTime=(this.media.currentTime-this.movie_options.frameTimeValue);}

	if(this.jogSteppingOn !=null){
		this.jogSteppingOn.stop();
		this.jogSteppingOn = null;
	}

	//************TEST RANGES "maison"
	if(this.controlSeek == "Ranges"){	// Si on utilise les ranges
		this.changeRangeCheck(this.GetCurrentTime());		// test de range (current ? creation ?)
	}
    window.removeEventListener("mouseup", this.resetjogIDtemp, false);

	document.getElementById("ow_id_jog_handle").onmouseup = null;
    document.getElementById("joginfo").style.visibility = "hidden";

}



//==========================================TIMELINE
// Fonction assurant la mise a jour du max de la barre quand la video est chargée
OWH_player.prototype.setTimeline = function (){
	// Paramétrage des timeline de progression et de selection
	// parametrage des range max des timeline
	//*****************TEST TC IN TC OUT



	if(this.movie_options.timecode_IN){
		ffox_bug_flag = this.timeline.range.start;
		this.timeline.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);
		if(!this.selectionSliderDisabled){this.timelineSel.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);}
		// MS - 18.03.15 - firefox lance de façon aléatoire l'event durationchange au lieu de l'évenement ended à la fin d'une vidéo.
		// ce check permet de vérifier que le timeline range start a réellement changé,
		// si il n'a pas changé on bloque le timeline.setValue(timeline.range.start) un peu plus bas dans la fonction.
		// MS - 26.10.16 - il faut aussi bloquer l'appel à timelineSel.setValue qui cause le meme problème. Avait échappé à mon attention puisque n'apparait que dans le cas où on a le selectionSlider actif. 
		if(this.timeline.range.start == ffox_bug_flag){
			ffox_bug_flag = true ;
		}else{
			ffox_bug_flag = false ;
		}
	}

	// reinitialisation des valeurs par défaut
	// MS 23/08/13 plus sur que ce soit tjs utile ?
	// if(this.browser!= "Explorer" && (this.timeline.value != this.timeline.range.start)){ // MODIF TESTIE
		// this.timeline.setValue(this.timeline.range.start);	// Lors de l'init sous IE >> pb de setvalue trop tot pour le media, on bloque donc le onchange pour l'init
		// console.log(" setValue 1 : "+this.timeline.range.start+" , réel : "+this.timeline.value);
	// }


	if(!this.selectionSliderDisabled && !this.selectionExists &&  (typeof ffox_bug_flag =='undefined' || !ffox_bug_flag) ){
		this.timelineSel.setValue(this.timelineSel.range.start,0);
		this.timelineSel.setValue(this.timelineSel.range.start,1);
	}



	if(this.resizeCustom){
		this.resizeVideoKeepRatio();    // NEWLAYOUTVISIO
	}
	this.adjustTimeline();


	
	if(this.movie_options.timecode_playback_start != null 
	&& this.timecodeToSecs(this.movie_options.timecode_playback_start) >= this.timecodeToSecs(this.movie_options.timecode_IN)
	&& this.timecodeToSecs(this.movie_options.timecode_playback_start) <= this.timecodeToSecs(this.movie_options.timecode_OUT)){
		this.GoToLongTimeCode(this.secsToTimeCode(this.timecodeToSecs(this.movie_options.timecode_playback_start)));
	}else if(this.movie_options.timecode_IN && (typeof ffox_bug_flag =='undefined' || !ffox_bug_flag) ){
		if(this.autoplay || this.timeline.range.start != this.GetCurrentTime()){
			this.timeline.setValue(this.timeline.range.start);
		}
	}
}

// fonction permettant d'initialiser la timeline
OWH_player.prototype.InitTimeline = function(){
	// TIMELINE PROGRESSION
	// Fonction onSlide de la timeline progression
	updateTimeline = function(value){
		if(this.prerolling){
			return false ;
		}
		if(this.movie_options.tcMosaicExact && this.timeline.offsetX>this.timeline.handleLength){
			this.timeline.offsetX = this.timeline.handleLength/2;
		}

		if(this.outOfSelectionTest(value) && !this.media.paused){
			this.StopTheVideo();
		}
		if(this.playlist) {

			//window.status="updateTimeline track="+this.current_track+" : "+this.BegTrack+" < "+value+" < "+this.EndTrack;
			if(value<this.BegTrack) {
				tmp_track = this.current_track;
				while(value<this.track_start[tmp_track] && (tmp_track >0)){
					tmp_track = tmp_track -1;
				}
				this.track_media[tmp_track].currentTime =  (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
				this.ChangeTrack(tmp_track);
			} else if(value>this.EndTrack) {
				tmp_track = this.current_track;
				while(value>this.track_end[tmp_track] && (tmp_track < this.nb_tracks-1)){
					tmp_track = tmp_track +1;
				}
				this.track_media[tmp_track].currentTime =  (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
				this.ChangeTrack(tmp_track);
			}else if(this.media){
				this.media.currentTime = (value+this.track_start_offset[this.current_track])-this.BegTrack;
			}

		}
		else this.media.currentTime = value;		// positionne la vidéo à la valeur du curseur
		if(this.controlSeek == "Ranges"){
			this.stopUpdateRange = true;
		}
	}
	// Fonction onChange de la timeline progression
	changeTimeline = function(value){
		if(this.prerolling || typeof this.media == 'undefined'){
			return false ;
		}
		if($j("#ow_prevOverContainer").is(':visible') && this.movie_options.tcMosaicExact && this.ClickOnTimeline && this.sec_mosaic!=null){
			this.ClickOnTimeline = false ;
			this.timeline.setValue(this.sec_mosaic);
			return 0;
		}

		if(this.outOfSelectionTest(value)){
			if(this.fastSteppingOn!= null || !this.media.paused ){

                if(!(this.rateWasChanged && this.bugAfterRateChange)){
                    this.StopTheVideo();
                    if(!this.ClickOnTimeline){
						if(this.loopVideo){
							this.GoToBegAvailable();
							this.dontCallOnChange = true;
							this.PlayTheVideo();
						}else{
							value<this.BegSelect? this.GoToBegSelection() : this.GoToEndSelection();
							this.dontCallOnChange = true;
						}
					}
				}
			}


		} //&& this.GetCurrentTime() < this.EndSelect

		if(this.controlSeek == "Ranges" && !this.mediaFullyLoaded && (!this.dontCallOnChange || this.stopUpdateRange)){
			//var test = this.myBuffered.currentIdx==this.myBuffered.ranges.length-1 && (this.myBuffered.ranges[this.myBuffered.currentIdx][1]>this.duration-1);

			//DURATIONBUG
			// Le flagtest ne marchait plus>>> this.duration �tait NaN car il est initialis� =this.media.duration avant que le media soit
			//suffisament charg�. Cela a �t� chang� pour les playlist INA >>> a voir avec vincent comment fix �a de meilleure facon.
			try{
				this.flagTestBugSafari = (this.myBuffered.currentIdx == this.myBuffered.ranges.length-1) && (this.myBuffered.ranges[this.myBuffered.currentIdx][1]>this.duration-1);
			}catch(e){this.flagTestBugSafari = false ;}
			if(!this.SelSlide){
				this.changeRangeCheck(value);			// On effectue les tests de changements de ranges
			}
			if(this.display_buff){
				this.displayBufferedRange();
			}
		}
		// MS sécu supplémentaire, cas playlist, empeche le chevauchement horaire de 2 fichiers tracks si fin réelle du fichier> à fin attendue
		if(this.playlist && (this.dontCallOnChange|| this.currentTrackEnded)  && value>this.EndTrack){
			this.dontCallOnChange = true ;
			this.PlayNext();

		}
		if(!this.dontCallOnChange ){
			if(this.playlist) {
				if(value<this.BegTrack ) {
					this.dontCallOnChange=true;
					tmp_track = this.current_track;
					while(value<this.track_start[tmp_track] && (tmp_track >0)){
						tmp_track = tmp_track -1;
					}
					this.track_media[tmp_track].currentTime =  (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
					this.ChangeTrack(tmp_track);
				} else if(value>=this.EndTrack) {
					this.dontCallOnChange=true;
					tmp_track = this.current_track;
					while(value>=this.track_end[tmp_track] && (tmp_track < this.nb_tracks-1)){
						tmp_track = tmp_track +1;
					}
					this.track_media[tmp_track].currentTime =  (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
					this.ChangeTrack(tmp_track);
				}else if (this.media){
					this.media.currentTime = (value + this.track_start_offset[this.current_track])-this.BegTrack;
				}
			}else{
				// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
				if (!(typeof this.media === 'undefined' || this.media=== null)) {
                	this.media.currentTime = value;		// positionne la vidéo à la valeur du curseur
				}
			}
		}else if( this.playlist && this.dontCallOnChange && this.mediaType=="audio" && value<=this.BegTrack  && this.fastSteppingOn != null && this.media.playbackRate<1){
			// cas audio, fast rewind, en utilisant le playbackRate du media,
			this.ChangeTrack(this.current_track-1);
			this.dontCallOnChange=true;
			if(this.media) {  this.media.currentTime = value-this.BegTrack-1;}
		}

		this.dontCallOnChange = false;
		if(this.controlSeek == "Ranges" && !this.mediaFullyLoaded && !this.SelSlide && !this.ClickOnTimeline && !this.goingToEnd){
			if(this.stopUpdateRange){
				this.stopUpdateRange = false;
				this.updateCurrentRange();
			}
			this.testBugSafari();
		}
		this.ClickOnTimeline = false;

	}

    timelineIsClicked = function(){
        this.ClickOnTimeline = true ;
	}



    document.getElementById("progress_track").addEventListener("mousedown", timelineIsClicked.bind(this),false);

	// implémentation rollover storyboard (necessite une mosaic générée & les paramètres movie_options.mosaic_* renseignés )
    if(this.movie_options.previewOverTimeline && this.mediaType!="audio"){
		if(!document.getElementById("ow_prevOverContainer")){
			prevOverContainer = document.createElement("div");
			prevOverContainer.id = "ow_prevOverContainer";
			prevOverContainer.style.display="none";

			img_prevOverContainer = document.createElement("div");
			img_prevOverContainer.id = "ow_img_prevOverContainer";
			img_prevOverContainer.style.width=this.movie_options.mosaic_width+"px";

			label_prevOverContainer = document.createElement("div");
			label_prevOverContainer.id = "ow_lab_prevOverContainer";

			// suppression du tooltip sur la barre de progression (un peu genant en + du preview)
			document.getElementById("ow_id_progress_slider").removeAttribute("title");
			document.getElementById("ow_id_progress_slider").appendChild(prevOverContainer);
			prevOverContainer.appendChild(img_prevOverContainer);
			prevOverContainer.appendChild(label_prevOverContainer);
		}

		this.hide_preview_timeout = null;

		document.getElementById("ow_id_progress_slider").addEventListener("mouseover", function(mouseEvent){
			if(
				(typeof mouseEvent.srcElement != 'undefined' &&  
				($j(mouseEvent.srcElement).parents('.ow_seq').length > 0 || $j(mouseEvent.srcElement).is('.ow_seq') || $j(mouseEvent.srcElement).is('#ow_prevSeqContainer')|| $j(mouseEvent.srcElement).parents('#ow_prevSeqContainer').length>0 || $j(mouseEvent.toElement).is('#progress_handle') ))
			|| (typeof mouseEvent.target != 'undefined' 
			&&  ($j(mouseEvent.target).parents('.ow_seq').length > 0 || $j(mouseEvent.target).is('.ow_seq') || $j(mouseEvent.target).is('#ow_prevSeqContainer') || $j(mouseEvent.target).parents('#ow_prevSeqContainer').length>0 || $j(mouseEvent.target).is('#progress_handle') ))){
				// console.log("return false : srcElement : "+mouseEvent.srcElement.id+" "+mouseEvent.srcElement.className+" test is in ow_seq "+($j(mouseEvent.srcElement).parents('.ow_seq').length > 0 || $j(mouseEvent.srcElement).is('.ow_seq')));
				return false ; 
			}
			if(this.mosaic_initialized){
				document.getElementById("ow_prevOverContainer").style.display = "";
			}
			if(this.hide_preview_timeout != null){
				// console.log("clearTimeout from mouse-enter");
				clearTimeout(this.hide_preview_timeout);
				this.hide_preview_timeout = null;
			}
		}.bind(this),false);

		document.getElementById("ow_id_progress_slider").addEventListener("mousemove", function(mouseEvent){
			if((typeof mouseEvent.srcElement != "undefined" && mouseEvent.srcElement.id != "ow_id_progress_slider" && mouseEvent.srcElement.id != "progress_track" && mouseEvent.srcElement.id != "progress_handle")
			|| (typeof mouseEvent.target != "undefined" && mouseEvent.target.id != "ow_id_progress_slider" && mouseEvent.target.id != "progress_track" && mouseEvent.target.id != "progress_handle")){
				return 0 ;
			}
			

			if((typeof mouseEvent.srcElement != "undefined" && mouseEvent.srcElement.id == "progress_handle") || (typeof mouseEvent.target != "undefined" && mouseEvent.target.id=="progress_handle")){
				if(typeof mouseEvent.srcElement != "undefined"){
					value = this.timeline.translateToValue(parseInt(document.getElementById("progress_handle").style.left,10)+mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = parseInt(document.getElementById("progress_handle").style.left,10)+(mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}else if(typeof mouseEvent.target != "undefined"){
					value = this.timeline.translateToValue(parseInt(document.getElementById("progress_handle").style.left,10)+mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = parseInt(document.getElementById("progress_handle").style.left,10)+(mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}
			}else{
				if(typeof mouseEvent.srcElement != "undefined"){
					value = this.timeline.translateToValue(mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = (mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}else if(typeof mouseEvent.target != "undefined"){
					value = this.timeline.translateToValue(mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = (mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}
			}

			if(value < this.timeline.range.start){
				value = this.timeline.range.start;
			}else if (value > this.timeline.range.end){
				value = this.timeline.range.end;
			}
			
			if(this.movie_options.timecode_IN){
				value_pic = value-(this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag));
			}else{
				value_pic = value;
			}
			
			if(typeof this.sequences_wrapper != 'undefined'){
				curr_seq = this.inSequenceTest(value);
				if(typeof curr_seq == 'string'){
					// $j("#ow_id_progress_slider").trigger('mouseout');
					if(this.hide_preview_timeout  != null){
						clearTimeout(this.hide_preview_timeout );
					}
					document.getElementById("ow_prevOverContainer").style.display = "none";
					this.hide_preview_timeout = null;
					this.showEncadreSeq(curr_seq,value,value_pic);
					return 0 ; 
				}else{
					this.hideEncadreSeq() ; 
					if(this.mosaic_initialized){
						document.getElementById("ow_prevOverContainer").style.display = "";
					}
				}
			}


		

			if(this.movie_options.mosaic_use_index){
				this.getPreviewPicFromIndex(value_pic);
			}else{
				this.getPreviewPic(value_pic);
			}

			if(!this.movie_options.tcMosaicExact){
				document.getElementById("ow_lab_prevOverContainer").innerHTML = this.DisplayTimeCode(value);
			}
		}.bind(this),false);

		document.getElementById("ow_id_progress_slider").addEventListener("mouseout", function(mouseEvent){
			if(this.hide_preview_timeout == null){
				this.hide_preview_timeout = setTimeout(function(){
					document.getElementById("ow_prevOverContainer").style.display = "none";
					this.hide_preview_timeout = null;
				}.bind(this),250);
			}
		}.bind(this),false);
	}



	if(this.movie_options.span_time_passed){
		span = "ow_time_passed";
	}else{
		span = 'none';
	}

	// création du slider timeline de progression
	this.timeline = new Control.Slider(["progress_handle"],"progress_track", {
									   alignY: 10,
									   range : $R(0,100),						// initialisation d'un range par défaut
									   onSlide: updateTimeline.bind(this),		//association des évenements aux fonctions
									   onChange: changeTimeline.bind(this),
									   startSpan : span
									   });

	if(!this.selectionSliderDisabled){
	// Fonction onSlide de la timeline selection
	slideSel = function (values){
		this.BegSelect = values[0];						// Mise a jour de la variable début de selection grâce aux valeurs des curseurs
		this.EndSelect = values[1];					// Mise a jour de la variable fin de selection grâce aux valeurs des curseurs
		this.timeline.setValue(Math.round(values[this.timelineSel.activeHandleIdx]*100)/100);		// positionnement de la vidéo au niveau du curseur actif

		if(this.controlSeek == "Ranges"){
			this.SelSlide = true;
			this.stopUpdateRange = true;
		}

		if(values[0]==values[1]){
			if(document.getElementById("handle_BegSel").hasClassName('selected')){
				this.timelineSel.activeHandle = document.getElementById("handle_EndSel");
				this.timelineSel.activeHandleIdx =1;
				document.getElementById("handle_BegSel").removeClassName('selected');
				document.getElementById("handle_EndSel").addClassName('selected');


			}else{
				this.timelineSel.activeHandle = document.getElementById("handle_BegSel");
				this.timelineSel.activeHandleIdx =0;
				document.getElementById("handle_EndSel").removeClassName('selected');
				document.getElementById("handle_BegSel").addClassName('selected');

			}
		}
        if(this.movie_options.onSelectionSliderChange != null){
            this.movie_options.onSelectionSliderChange();
		}


		if (this.movie_options.onSelectionSliderChange){
			this.movie_options.onSelectionSliderChange();
		}




	}

	// Fonction onChange de la timeline selection
	changeSel = function(values){
		this.SelSlide = false;
		this.stopUpdateRange = false;

		//this.timeline.setValue(values[this.timelineSel.activeHandleIdx]); ???
		this.BegSelect = values[0];
		this.EndSelect = values[1];
		if(!this.dontCallOnChange){
			this.timeline.setValue(Math.round(values[this.timelineSel.activeHandleIdx]*100)/100);
		}
		if (this.BegSelect!=this.EndSelect){ this.selectionExists = true;}
		else{this.selectionExists = false; }



	}


	clickSelHandle = function (){
		if(this.timeline.value != this.timelineSel.values[this.timelineSel.activeHandleIdx]){
			this.StopTheVideo();
			this.timeline.setValue(Math.round(this.timelineSel.values[this.timelineSel.activeHandleIdx]*100)/100);
			// ou alors on test activeHandleIdx et on applique la méthode GoToBegSelection ou GoToEndSelection suivant le resultat
		}
	}


	// création du slider timeline de selection
	this.timelineSel = new Control.Slider(["handle_BegSel","handle_EndSel"],"select_track",{
										  range : $R(0,100),				// initialisation d'un range par défaut
										  spans : ["selection"],			// referencement du span permettant de colorer la selection
										  restricted : true,				// pour maintenir les valeurs debut selection et fin selection cohérente
										  sliderValue : [0,0],			// initialisation des valeurs des curseurs par défaut
										  onSlide:slideSel.bind(this),	// association des évenements aux fonctions
										  onChange : changeSel.bind(this)
										  });
	this.timelineSel.track.stopObserving("mousedown", this.timelineSel.eventMouseDown);
	document.getElementById("handle_BegSel").onclick = clickSelHandle.bind(this);
	document.getElementById("handle_EndSel").onclick = clickSelHandle.bind(this);
	}

}


// fonction permettant d'ajuster la timeline lors d'un resize
OWH_player.prototype.adjustTimeline = function(){
	var val=this.timeline.value;			// enregistrement de la derniere valeur timeline progression
	if(!this.selectionSliderDisabled){
	var sel = this.timelineSel.values;		// enregistrement des dernieres valeurs timeline selection
    }
	selectionIsHidden = this.movie_options.selectionHidden;

    if(selectionIsHidden && !this.selectionSliderDisabled){

        this.ShowSelectionSlider();
	}

	this.timeline.dispose();			// suppression de la timeline et de ses listeners
	if(!this.selectionSliderDisabled){
		this.timelineSel.dispose();
	}
	this.InitTimeline();				// réinitialisation de la nouvelle timeline
	//*****************TEST TC IN TC OUT
	if(this.movie_options.timecode_IN){
		this.timeline.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);
			if(!this.selectionSliderDisabled){this.timelineSel.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);}
	}

	if(this.movie_options.timecode_OUT){
		this.timeline.range.end = this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag);
			if(!this.selectionSliderDisabled){this.timelineSel.range.end = this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag);}

	}else{
		this.timeline.range.end = this.duration;		//DURATIONBUG this.duration normalement
			if(!this.selectionSliderDisabled){this.timelineSel.range.end = this.duration;}
	}

	//*************************************
	//	this.timeline.range.end = this.duration;		// réinitialisation du range de la timeline (hors de init, pour des raisons de chargement video)
	//	this.timelineSel.range.end = this.duration;
	this.dontCallOnChange = true;		// flag empechant les instructions de l'évenement onChange (évite les saccades)
	if(!this.selectionSliderDisabled){
		this.timelineSel.setValue(sel[1],1);	// positionnement de la nouvelle timeline selection aux anciennes valeurs
		this.timelineSel.setValue(sel[0],0);
	}
	this.timeline.setValue(val);		// positionnement de la nouvelle timeline progression à l'ancienne position

    if(selectionIsHidden && !this.selectionSliderDisabled){
        this.HideSelectionSlider();
	}

	//MS update de l'affichage des range bufferisés apres un reset timeline (pour forcer un recalcul des divs dans le cas ou toute la video se charge avant que les calculs soient faits
	if (this.controlSeek == "Ranges" && this.display_buff){
		this.displayBufferedRange();
	}
	this.drawSequences() ; 

	
	
	setTimeout(this.correctScrolls.bind(this),1);	// planification d'un test en cas de bug d'affichage dus à la place necessaire aux scrolls
}

// Fonction permettant de corriger l'éventuel probleme de la place reservée aux scrolls (Safari)
OWH_player.prototype.correctScrolls = function(){
	val = this.timeline.value;			// enregistrement de l'ancienne valeur timeline
	if(!this.selectionSliderDisabled){
		sel = this.timelineSel.values;
	}
	// si la taille de la track de la timeline est differente de la taille effective du div timeline
	if (this.timeline.trackLength!= document.getElementById("ow_id_progress_slider").clientWidth){
		this.timeline.trackLength = document.getElementById("ow_id_progress_slider").clientWidth;	// mise a jour de la longueur timeline à partir de la taille effective du div
		this.dontCallOnChange = true;			// flag empechant les instructions de onChange (évite les saccades)

		if(!this.selectionSliderDisabled){
			this.timelineSel.trackLength = this.timeline.trackLength;
			this.timelineSel.setValue(sel[1],1);
			this.timelineSel.setValue(sel[0],0);
		}

		this.timeline.setValue(val);			// mise a jour de la nouvelle timeline avec l'ancienne valeur enregistrée
	}
}


// Fonction application des modifications de temps de la video aux éléments d'affichage
OWH_player.prototype.UItest = function(){
	
	// MS Test attribute media.buffered :
	/*str_buffered="";
	for (i = 0; i < this.media.buffered.length; i ++){
		str_buffered += "["+i+"] ("+this.media.buffered.start(i)+","+this.media.buffered.end(i)+")<br />";
	}
	$("infobuff").innerHTML = str_buffered;*/
	if(!this.prerolling){
		if(this.fastSteppingOn != null && this.fastSteppingDirection == -1 && this.GetCurrentTime() == 00){
			this.StopTheVideo();
		}

		if (this.mediaType == "audio" && this.movie_options.timecode_IN && this.media.playbackRate != 1){
			if (this.GetCurrentTime()<=(this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag))){
				this.StopTheVideo();
				this.timeline.setValue(this.timeline.range.start);
			}
		}else if (this.movie_options.timecode_OUT && this.GetCurrentTime()>(this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag))){
			this.timeline.setValue(this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag));
			this.StopTheVideo();
			
			if(this.movie_options.onMovieFinished!=null && typeof(this.movie_options.onMovieFinished)=='function'){
				this.movie_options.onMovieFinished();
			}
			
			if(this.movie_options.show_poster_on_ended){
				this.show_poster_on_ended() ;
			}
		}else {
			this.dontCallOnChange=true;
			this.timeline.setValue(this.GetCurrentTime());
		}

		if(this.rateWasChanged && (Math.round(this.GetCurrentTime()*100)/100 > Math.round(this.BegSelect*100)/100)){
			this.bugAfterRateChange = false;
		}

		if(document.getElementById('timecodeDisplay')){
			document.getElementById('timecodeDisplay').innerHTML =  this.DisplayTimeCode(this.GetCurrentTime());
		}
		
		if(typeof this.sequences != 'undefined' && typeof this.sequences_wrapper != 'undefined'){
			curr_seq = this.inSequenceTest(this.GetCurrentTime());
			if(typeof curr_seq == 'string'){
				if(!$j("#ow_seq_"+curr_seq).hasClass('current_seq_progress')){
					$j(".ow_seq").removeClass('current_seq_progress');
					$j("#ow_seq_"+curr_seq).addClass('current_seq_progress');
				}
				if(!$j("#ow_rich_media_menu #rich_media_chapitres .rich_media_chapitre").eq(curr_seq).hasClass('current')){
					$j("#ow_rich_media_menu #rich_media_chapitres .rich_media_chapitre").removeClass('current');
					$j("#ow_rich_media_menu #rich_media_chapitres .rich_media_chapitre").eq(curr_seq).addClass('current');
				}
			}else{
				$j("#ow_rich_media_menu #rich_media_chapitres .rich_media_chapitre").removeClass('current');
				$j(".ow_seq").removeClass('current_seq_progress');
			}
			
			closest_prev_seq = this.GetClosestSeq(0);
			closest_next_seq = this.GetClosestSeq(1);	
			if(closest_prev_seq == null){
				$j("#ow_bouton_prev_seq").addClass('inactive');
			}else{
				$j("#ow_bouton_prev_seq").removeClass('inactive');		
			}
			if(closest_next_seq == null){
				$j("#ow_bouton_next_seq").addClass('inactive');
			}else{
				$j("#ow_bouton_next_seq").removeClass('inactive');
			}
			
		}
		if(typeof this.diapos != 'undefined' && typeof this.diapos_wrapper != 'undefined'){
			curr_seq = this.inSequenceTest(this.GetCurrentTime(),'diapos');
			// console.log("curr_seq_diapo : ",curr_seq);MSDIAPOS
			if(typeof curr_seq == 'string'){
				$j(this.diapo_img).attr('src','makeVignette.php?image='+this.diapos[curr_seq]['img']);
				$j(this.diapo_img).removeClass('no_source');
				if(!$j("#ow_rich_media_menu #rich_media_diapos .rich_media_diapo").eq(curr_seq).hasClass('current')){
					$j("#ow_rich_media_menu #rich_media_diapos .rich_media_diapo").removeClass('current');
					$j("#ow_rich_media_menu #rich_media_diapos .rich_media_diapo").eq(curr_seq).addClass('current');
				}
			}else{
				$j("#ow_rich_media_menu #rich_media_diapos .rich_media_diapo").removeClass('current');
				$j(this.diapo_img).addClass('no_source')
			}
		}
		
		if(typeof this.simple_duration_tracker != 'undefined'){
			time = this.GetCurrentTime()  ; 
			if( !this.media.paused && this.simple_duration_tracker['last_pos']){
				delta_pos = time - this.simple_duration_tracker['last_pos']; 
				// console.log("delta_pos : "+delta_pos);
				this.simple_duration_tracker['time_watched'] += delta_pos ;
				
			}
			this.simple_duration_tracker['last_pos'] = time ; 
		}
	

		if (typeof(this.movie_options.onTimecodeChange) == "function"){ this.movie_options.onTimecodeChange(this.GetCurrentTime());}
		else if (typeof(onTimecodeChange) == "function") onTimecodeChange(this.GetCurrentTime());
	
	}
	
	// MSTEST
	// force refresh Webkit : fonctionne mais peu performant (force repaint de la page)
	// $j("#ow_controllerpanel").append('<style id="test_refresh"></style>');
	// $j("#ow_controllepanel #test_refresh").remove
}

// Fonction permettant d'avancer la vidéo jusqu'au pourcentage donné en parametre (A voir si le format est XX% ou 0,XX)
OWH_player.prototype.GoToPercent= function(percent){
	if(percent <100){
		this.timeline.setValue(percent*this.duration/100);
	} else if (percent == 100){
		this.timeline.setValue(this.duration);
	}
}


//====================================== Selection

// Fonction permettant de positionner la vidéo au début de la selection
OWH_player.prototype.GoToBegSelection = function (){
	this.StopTheVideo();
    this.dontCallOnChange = false;
	//this.GetCurrentTime() = Math.round(this.BegSelect*100)/100;
    this.timeline.setValue(this.BegSelect);
}

// Fonction permettant de positionner la video à la fin de la selection
OWH_player.prototype.GoToEndSelection = function(){
	this.StopTheVideo();
    this.dontCallOnChange = false;

    this.timeline.setValue(this.EndSelect);
	//this.GetCurrentTime() = Math.round(this.EndSelect*100)/100;
}






// Fonction permettant de parametrer la selection (A voir pour les questions de formats timecode / secondes)
OWH_player.prototype.SetSelection = function(timecode_debut, timecode_fin){
    // Variables bornes de la selection en secondes
	if(typeof(flag_go_debut) == "undefined"){
		flag_go_debut = false ;
	}

    tc_in = this.timecodeToSecs(timecode_debut)-this.timecodeToSecs(this.movie_options.timecode_lag);
    tc_out = this.timecodeToSecs(timecode_fin)-this.timecodeToSecs(this.movie_options.timecode_lag);
    switchAutreExtrait = false;

	if(typeof transcript_seqsaisie != "undefined"){
		highlightSequence(tc_in,tc_out);
	}


    // test de validité des valeurs en entrée (tc_debut < tc_fin) et correction
    if(this.timecodeToSecs(timecode_debut)>this.timecodeToSecs(timecode_fin)){
        tc_in = this.timecodeToSecs(timecode_fin)-this.timecodeToSecs(this.movie_options.timecode_lag);;
        tc_out = this.timecodeToSecs(timecode_debut)-this.timecodeToSecs(this.movie_options.timecode_lag);;
    }

	//    if(tc_in != this.BegSelect && tc_out != this.EndSelect){
	//      switchAutreExtrait = true;
    //    }


    // Parametrage des variables timecode debut et fin de selection internes a la visionneuse

    // Mise a jour des curseurs de selection
    this.dontCallOnChange = true;

	if(tc_out<this.BegSelect){
		this.timelineSel.setValue(tc_in, 0);
		this.timelineSel.setValue(tc_out,1);
	}else{
		this.timelineSel.setValue(tc_out,1);
		this.timelineSel.setValue(tc_in, 0);
	}
    this.dontCallOnChange = false;



	//  if(switchAutreExtrait){
    // VP 14/01/2013 : suppression calcul hors sélection par arrondi, dans tous les cas on prend une marge d'une image (this.movie_options.frameTimeValue = 1/25)
    //	if(this.selectionExists && (Math.round(this.GetCurrentTime()*100)/100<Math.round(this.BegSelect*100)/100-this.movie_options.frameTimeValue || Math.round(this.GetCurrentTime()*100)/100>Math.round(this.EndSelect*100)/100+this.movie_options.frameTimeValue)){
    if(this.selectionExists && (this.GetCurrentTime()<this.BegSelect-this.movie_options.frameTimeValue) || (this.GetCurrentTime()>this.EndSelect+this.movie_options.frameTimeValue)){
		this.timeline.setValue(this.BegSelect);
	}
    //}


	//if(this.movie_options.timecode_lag){
	//	this.BegSelect = this.BegSelect - this.timecodeToSecs(this.movie_options.timecode_lag);
	//	this.EndSelect = this.EndSelect - this.timecodeToSecs(this.movie_options.timecode_lag);
	//}

}

// Fonction permettant de supprimer le parametrage de selection (== mettre le debut et la fin a 0, inactif)
OWH_player.prototype.UnSetSelection = function(){
    this.dontCallOnChange = true;
    this.timelineSel.setValue(0,0);
    this.timelineSel.setValue(0,1);
    this.dontCallOnChange = false;
	this.BegSelect = this.timelineSel.values[0];
	this.EndSelect = this.timelineSel.values[1];
}


// Fonction permettant de cacher la barre d'extrait (pour l'onglet description Opsis)
OWH_player.prototype.HideSelectionSlider = function(){
    this.movie_options.selectionHidden = true;
    document.getElementById("selection").addClassName("hideselection");
    document.getElementById("select_track").addClassName("hideselection");
	if( document.getElementById('progress_track').offsetHeight!= 0){
		document.getElementById("ow_id_progress_slider").style.height = document.getElementById('progress_track').offsetHeight;//"4px";
	}
	// this.media.webkitEnterFullScreen();
}

// Fonction permettant de réafficher la barre d'extrait (pour l'onglet extrait d'Opsis)
OWH_player.prototype.ShowSelectionSlider = function(){
    this.movie_options.selectionHidden = false;
    document.getElementById("selection").removeClassName("hideselection");
    document.getElementById("select_track").removeClassName("hideselection");
	if( document.getElementById('progress_track').offsetHeight!= 0){
		document.getElementById("ow_id_progress_slider").style.height = document.getElementById('progress_track').offsetHeight;//"8px";
	}
}



// Fonction qui retourne le timecode actuel au format long (+offset si nécessaire)

OWH_player.prototype.GetCurrentLongTimeCode = function(){
    offset = 0;
    if(this.movie_options.timecode_lag){
        offset = this.timecodeToSecs(this.movie_options.timecode_lag);
    }

    return this.secsToTimeCode(this.GetCurrentTime()+offset);
}


OWH_player.prototype.GoToLongTimeCode = function(timecode)
{
	// GT 28/09/11 : prise en compte de l'offset
	var offset=0;
	if(this.movie_options.timecode_lag)
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);

	value =  this.timecodeToSecs(timecode)-offset;
	// MS 17/02/14 - amélioration fonctionnement de cette fonction dans le cas playlist
	if(this.playlist){
		if(value<this.BegTrack) {
			tmp_track = this.current_track;
			while(value<this.track_start[tmp_track] && (tmp_track >0)){
				tmp_track = tmp_track -1;
			}
			this.track_media[tmp_track].currentTime =  (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
			this.ChangeTrack(tmp_track);
		} else if(value>this.EndTrack) {
			tmp_track = this.current_track;
			while(value>this.track_end[tmp_track] && (tmp_track < this.nb_tracks-1)){
				tmp_track = tmp_track +1;
			}
			this.track_media[tmp_track].currentTime =  (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
			this.ChangeTrack(tmp_track);
		}else if(this.media){
			this.media.currentTime = (value+this.track_start_offset[this.current_track])-this.BegTrack;
		}
	}else{
		this.media.currentTime = value-this.BegTrack;
	}
}




OWH_player.prototype.GetTCMax = function() {
    return this.secsToTimeCode(this.timecodeToSecs(this.movie_options.timecode_lag)+this.duration);
}





// Fonction qui retourne le début de la selection (A voir pour les questions de formats timecode / secondes)
OWH_player.prototype.GetCurrentSelectionBeginning = function(){

	var offset = 0;
	if(this.movie_options.timecode_IN){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}


	return this.secsToTimeCode(this.BegSelect+offset);
}

// Fonction qui retourne la fin de la selection (A voir pour les questions de formats timecode / secondes)
OWH_player.prototype.GetCurrentSelectionEnd = function(){

	var offset = 0;
	if(this.movie_options.timecode_IN){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}


	return this.secsToTimeCode(this.EndSelect+offset);
}

// Fonction permettant de tester si la selection existe et si on est en dehors (true si oui)
OWH_player.prototype.outOfSelectionTest = function(valToCompare) {
	if(this.selectionExists && (Math.round(valToCompare*100)/100<Math.round(this.BegSelect*100)/100-0.02 || Math.round(valToCompare*100)/100>Math.round(this.EndSelect*100)/100)){
		return true;
	} else{
		return false;
	}
}

OWH_player.prototype.inTheSelectionTest = function(valToCompare) {
	if(this.selectionExists && (Math.round(valToCompare*100)/100>=Math.round(this.BegSelect*100)/100 && Math.round(valToCompare*100)/100<=Math.round(this.EndSelect*100)/100)){
		return true;
	} else{
		return false;
	}
}

// ====TIMELINE BUFFERISATION

OWH_player.prototype.testBuff = function (event) {
	// console.log("callback buffer progress");
	// $("infobuff").innerHTML += "call testbuff , target : "+event.currentTarget.id+"<br />";

	if(this.controlSeek == "Ranges" && this.fastSteppingOn == null && this.jogSteppingOn == null && !this.stopUpdateRange ){
		this.updateCurrentRange();
	}
	if(this.controlSeek == "Events"  && this.media && this.media.buffered  && !this.stopUpdateRange && this.display_buff){
		this.displayCoreRanges();
	}

	// lorsque la video charge, on actualise le range courant de myBuffered




}


//=========================================FULLSCREEN
//Fonction d'alternance window/fullscreen
OWH_player.prototype.ToggleFullScreen = function() {
	if(this.browser == "Safari" && this.user_OS=="Mac" && typeof(document.webkitIsFullScreen)!="undefined" && !this.movie_options.xStand){
		if(this.fullscreen !=  document.webkitIsFullScreen){
			this.fullscreen = document.webkitIsFullScreen;
		}
	}

	if (!this.fullscreen){
        this.SwitchToFullScreen();
	}else{
		this.FromFullScreenToPopUp();
    }
}

// Fonction assurant le passage au mode fullscreen
OWH_player.prototype.SwitchToFullScreen = function (){
	if(this.movie_options.FS_elem_selector != null  && typeof $j(this.movie_options.FS_elem_selector).get(0) == "object") {
		elem = $j(this.movie_options.FS_elem_selector).get(0) ; 
	}else{
		elem = this.container;
	}
	this.fullscreen = true;
	
	try{
		function initListener(){
			if (elem.requestFullscreen) {
			  elem.onfullscreenchange = this.FullscreenToNormalCSS.bind(this);
			} else if (elem.mozRequestFullScreen) {
			  document.onmozfullscreenchange  = this.FullscreenToNormalCSS.bind(this);
			} else if (elem.webkitRequestFullScreen) {
			elem.onwebkitfullscreenchange  = this.FullscreenToNormalCSS.bind(this);
			}else if (elem.msRequestFullscreen) {
				document.onmsfullscreenchange  = this.FullscreenToNormalCSS.bind(this);
			}
			if(this.user_OS == 'Mac' ||this.browser=='Firefox' || this.browser =='Explorer'){
				this.NormalToFullscreenCSS();
			}else if (this.browser =='Explorer'){
				setTimeout(function(){this.NormalToFullscreenCSS()}.bind(this),1000);
			}
		}


		if (elem.requestFullscreen) {
			elem.onfullscreenchange = initListener.bind(this) ;
		} else if (elem.mozRequestFullScreen) {
			document.onmozfullscreenchange  =  initListener.bind(this);
		} else if (elem.webkitRequestFullScreen) {
			elem.onwebkitfullscreenchange  =  initListener.bind(this);
		}else if (elem.msRequestFullscreen) {
			document.onmsfullscreenchange  =  initListener.bind(this);
			document.onfullscreenchange  =  initListener.bind(this);
		}
		
		// MS - 12.05.15 - modification critères d'appel à la fonction normalFullscreenToCSS
		// à ce jour, iOS ne gère pas l'API fullscreen html5, on doit donc revenir sur le cas standard d'appel à normaltofullscreen, indépendamment des méthodes requestFullscreen
		if( (this.browser != 'Safari' || this.user_OS != 'Mac' || this.movie_options.xStand) && this.browser!='Explorer'){
		//if(this.browser != 'Safari' || this.user_OS != 'Mac' || this.movie_options.xStand || this.browser != 'Explorer'){
			this.NormalToFullscreenCSS();
		}
		if (elem.requestFullscreen) {
		  elem.requestFullscreen();
		} else if (elem.mozRequestFullScreen) {
		  elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullScreen) {
		  elem.webkitRequestFullScreen();
		} else if (elem.msRequestFullscreen) {
		  elem.msRequestFullscreen();
		}else{
			window.moveTo(0,0);
			window.resizeTo(screen.availWidth,screen.availHeight);
		}
	}catch(err){
		console.log(err);
	}

	//$('ow_controllerpanel').style.bottom = '0px';

}
OWH_player.prototype.NormalToFullscreenCSS=function(){
	if( this.user_OS == 'Android' || this.user_OS == 'iPad' || this.user_OS == 'iPhone'){
		if($j("head meta[name='viewport']").length >0 ){
			this.viewportmeta = $j("head meta[name='viewport']").get(0);
			this.viewportmeta.id = "viewport_OW_FS";
			try{
				min_scale  = $j("head meta[name='viewport']").get(0).content.match(/minimum-scale=([0-9\.]+)/);
				max_scale  = $j("head meta[name='viewport']").get(0).content.match(/maximum-scale=([0-9\.]+)/);
				if(min_scale && min_scale.length>1){
					min_scale = min_scale[1];
				}
				if(max_scale && max_scale.length>1){
					max_scale = max_scale[1];
				}
				scale_FS = min_scale || max_scale ;
				if(scale_FS == null || scale_FS == ''){
					scale_FS ='1.0';
				}
			}catch(e){
				scale_FS= '1.0';
			}
		}else{
			scale_FS ="1.0";
		}
		if(typeof this.viewportmeta == 'undefined'){

			this.viewportmeta = document.createElement('meta');
			this.viewportmeta.name = "viewport";
			this.viewportmeta.id = "viewport_OW_FS";
			$j("head").append(this.viewportmeta);
		}

		this.viewportmeta.content = "width=device-width minimum-scale="+scale_FS+", maximum-scale="+scale_FS+", initial-scale="+scale_FS;
	}
	old_pos = null; // MS 05/11/2012 Workaround bug Fullscreen API sous Safari Windows; on doit déclencher un rendering après le switch fullscreen si la vidéo était en pause
	if(this.browser == "Safari" && this.user_OS=="Windows" && parseFloat(this.browserVersion)>=5.1 &&this.media.paused){
		old_pos = this.media.currentTime-0.001;	// approximation de la position courante dans la video ( repasser la même valeur ne provoque de refresh de l'affichage)
		if(old_pos <0 ){
			old_pos = 0.001;
		}
	}

	// MS - 29/01/13 : True fullscreen + controller qui disparait si inactivité de la souris, réapparait sinon
	if(this.dynamicControllerFS && !this.dynamicController){
		// référence timer des animations
		this.timer_dynController = null ;

		this.func_dynController = function(event){
			// Si mouvement detecté
			if(event.clientX != this.old_value_X && event.clientY != this.old_value_Y){
				// Si le controller n'est pas affiché (== pas de timer de disparition enregistré)
				if(this.timer_dynController == null){
					// on affiche le curseur + le controller
					$j('#'+this.container.id).css('cursor','auto');
					$j('#ow_controllerpanel').show("fade",  400);
				}else{
				// Si controller affiché => on stop le timer courant pour le refresh
					clearTimeout(this.timer_dynController);
				}
				// création timer de disparition du controller
				this.timer_dynController  = setTimeout(function(){
					// disparition du curseur + disparition du controller (avec vérification toujours en mode fullscreen) + réinitialisation de la référence timer
					$j('#ow_main_container').css('cursor','none');
					$j('#ow_controllerpanel').hide("fade", {  complete : function(){ if(!this.fullscreen){$j('#ow_controllerpanel').css('display','block');}}.bind(this) }, 400);
					this.timer_dynController=null;
				}.bind(this),2000);

			}
			// enregistrement des valeurs courantes du curseur pour pouvoir detecter un mouvement du curseur lors de l'appel suivant
			this.old_value_X = event.clientX;
			this.old_value_Y = event.clientY;

		}.bind(this);

		// ajout de la classe dynamicController pour info + initialisation evenement.
		document.getElementById(this.container.id).addClassName('dynamicController');
		document.getElementById(this.container.id).addEventListener('mousemove',this.func_dynController,false);
	}


	// ajout de classes aux éléments afin de modifier leur affichage
	document.body.addClassName("hideover");
	document.getElementById(this.container.id).removeClassName("containerNS");
	document.getElementById(this.container.id).addClassName("containerFS"); //ESSAI DE CHANGEMENT DE FS
	document.getElementById(this.capsulevideo.id).addClassName("capsuleVideoFS");
	
	if(this.movie_options.FS_elem_selector != null  && typeof $j(this.movie_options.FS_elem_selector).get(0) == "object") {
		$j(this.movie_options.FS_elem_selector).addClass('custom_elem_FS'); 
	}




	if(this.playlist){
		$$('video[id^="video"]').each(function(vid_elt){vid_elt.addClassName('videoFS')}.bind(this));
	}else{
		document.getElementById(this.media.id).addClassName("videoFS");
	}
	document.getElementById(this.controllerpanel.id).addClassName("controllerpanelFS");

	if(this.browser == "Explorer" || this.browser=="Safari"){
		if(this.user_OS == 'iPad'){
			timer_tmp = 1000;
		}else{
			timer_tmp = 200;
		}
		setTimeout(function(){
			this.resizeVideoKeepRatio();
		}.bind(this),timer_tmp);
		//console.log(parseFloat(this.browserVersion));
	}
	if (this.movie_options.infos.visu_type.match(new RegExp(/(stream_)/)) == null){
		this.adjustTimeline();
	}
	if($("ow_id_progress_slider")){
		$("ow_id_progress_slider").style.overflow = "visible";		// Pour empecher un bug d'affichage du curseur de la timeline
	}
	$('ow_controllerpanel').style.top = "";

	// MS Fin workaround bug fullscreen API sous Safari Windows
	if(this.browser == "Safari" && this.user_OS=="Windows" && parseFloat(this.browserVersion)>=5.1 && old_pos){
		setTimeout(function(){this.media.currentTime = old_pos;}.bind(this),1500);
	}
}



// Fonction assurant le retour au mode window
OWH_player.prototype.FromFullScreenToPopUp = function (){
	
	// retrait des classes afin de rétablir l'affichage initial
	this.fullscreen = false;
	try{
		if (document.exitFullscreen && !this.movie_options.xStand) {
		 document.exitFullscreen();
		  // this.FullscreenToNormalCSS();
		} else if (document.mozCancelFullScreen && !this.movie_options.xStand) {
		  document.mozCancelFullScreen();
		  // this.FullscreenToNormalCSS();
		} else if (document.webkitCancelFullScreen && !this.movie_options.xStand) {
		  document.webkitCancelFullScreen();
		  // this.FullscreenToNormalCSS();
		}else if (document.msExitFullscreen) {
		  document.msExitFullscreen();
		  this.FullscreenToNormalCSS();
		}else{
			if(this.movie_options.xStand){ // dans le cas d'un site configuré pour xstand, mais aussi consulté via des navigateurs classiques récents, on est obligé de conservé cet appel, mais on ne se repose pas sur le callback pour appeler la deuxieme fonction. 
				try{
					document.webkitCancelFullScreen() ; 
				}catch(e){}
			}
			this.FullscreenToNormalCSS();
		}
	}catch(err){
		try{
			console.log("FromFullScreenToPopUp crash - try direct FullscreenToNormalCSS");
			this.FullscreenToNormalCSS();
		}catch(err){
			console.log("FromFullScreenToPopUp crash2 - "+err);
		}
	}


}

OWH_player.prototype.FullscreenToNormalCSS=function(){
	this.fullscreen = false;
	if(this.user_OS == 'Android' || this.user_OS == 'iPad' || this.user_OS == 'iPhone'){
		if(this.user_OS == 'iPad' || this.user_OS == 'iPhone'){
			this.appmobilemeta ='';
		}
		//this.viewportmeta.content = "";
		$j("head").remove("meta[name='viewport']");
	}
	if(this.dynamicControllerFS && !this.dynamicController){
		clearTimeout(this.timer_dynController);
		this.timer_dynController =null;
		document.getElementById(this.container.id).removeClassName('dynamicController');
		document.getElementById(this.container.id).removeEventListener('mousemove',this.func_dynController,false);

		$j('#'+this.container.id).css('cursor','auto');
		if($j('#ow_controllerpanel').css('display') != 'block'){
			$j('#ow_controllerpanel').css("display","block");
		}
	}


	document.body.removeClassName("hideover");
	if(this.playlist){
		$$('video[id^="video"]').each(function(vid_elt){vid_elt.removeClassName('videoFS')}.bind(this));
	}else{
		document.getElementById(this.media.id).removeClassName("videoFS");
	}
	document.getElementById(this.container.id).addClassName("containerNS");
	document.getElementById(this.controllerpanel.id).removeClassName("controllerpanelFS");
	document.getElementById(this.capsulevideo.id).removeClassName("capsuleVideoFS");
	document.getElementById(this.container.id).removeClassName("containerFS");
	if(this.movie_options.FS_elem_selector != null  && typeof $j(this.movie_options.FS_elem_selector).get(0) == "object") {
		$j(this.movie_options.FS_elem_selector).removeClass('custom_elem_FS'); 
	}


	//document.getElementById('fiche_info').setAttribute('style','visibility : visible ;');

	//if($('fiche_info')){
	//	document.getElementById('fiche_info').style.visibility = "visible";
	//}
	if(this.resizeCustom && !this.movie_options['fit_height_container']){
		minHeight = (parseInt(document.getElementById(this.container.parentNode.id).offsetWidth)*this.movie_options['height']/this.movie_options['width']);
		if(!this.movie_options.controller_on_video){
			minHeight = minHeight + this.controllerpanel.offsetHeight;
		 }
		this.container.style.minHeight = minHeight+"px";
		this.resizeVideoKeepRatio();
	}else{
		if(this.browser == "Explorer" || this.browser=="Safari" || this.user_OS == "Mac" || this.browser=="Firefox" || this.browser=="Chrome" ){
			setTimeout(function(){
				if(!this.rich_media_flag){
					this.resizeVideoKeepRatio();
				}
				if (this.movie_options.infos.visu_type.match(new RegExp(/(stream_)/)) == null){
					this.adjustTimeline();
				}
			}.bind(this),200);
		}
		// ce calcul "top" pr ow_controllerpanel ne se fait plus si le controller doit être affiché en superposition de la vidéo (dans ce cas : 'top' du controller < taille de la vidéo)
		if(!this.movie_options.fit_height_container || !this.dynamicController){
			$('ow_controllerpanel').style.top = ($('video').offsetHeight)+"px";
		}
	}
	// ajustement de la timeline
	if (this.movie_options.infos.visu_type.match(new RegExp(/(stream_)/)) == null){
		this.adjustTimeline();
	}
}


//==========================================SOUND


OWH_player.prototype.defaultControlVolumeChange = function(){
    if (this.media.volume!=0 && $j(this.container).find('#ow_bouton_mute_sound').length>0){
        $j(this.container).find('#ow_bouton_mute_sound').removeClass("ClickBouton");
		$j(this.container).find('#containSound').removeClass('muted');
	}else if (this.media.volume ==  0 && $j(this.container).find('#ow_bouton_mute_sound').length>0 ){
        $j(this.container).find('#ow_bouton_mute_sound').addClass("ClickBouton");
		$j(this.container).find('#containSound').addClass('muted');
		
	}
    this.soundslider.setValue(this.media.volume);
}



// Fonction assurant la fonction mute
OWH_player.prototype.SetMuteState = function (){
	this.media.muted = !this.media.muted;

    if(this.media.muted){
        document.getElementById("ow_bouton_mute_sound").addClassName("ClickBouton");
		$j(this.container).find('#containSound').addClass('muted');
        this.soundVolume = this.media.volume;
        this.soundslider.setValue(0);
    }   else {
        document.getElementById("ow_bouton_mute_sound").removeClassName("ClickBouton");
		$j(this.container).find('#containSound').removeClass('muted');
        this.soundslider.setValue(this.soundVolume);
    }
}

//Fonction permettant de creer le slider de son
OWH_player.prototype.InitSoundSlider = function() {

	updateSoundSlider = function(value){
		if(this.media){
			if(this.media.muted && value != 0){
				this.media.muted = false;
			}

			this.media.volume = value;
		}
	}


	changeSoundSlider = function(value) {
		if(this.media){
			if(this.media.muted && value != 0){
				this.media.muted = false;

			}
			this.media.volume = value;
		}
	}
	
	if(this.movie_options.span_sound_slider!= false){
		spanpassed = document.createElement('div');
		spanpassed.id = "ow_sound_start_span";
		$j(this.container).find('#containSound').get(0).appendChild(spanpassed);
		endspanpassed = document.createElement('div');
		endspanpassed.id = "ow_sound_end_span";
		$j(this.container).find('#containSound').get(0).appendChild(endspanpassed);
		start_span = "ow_sound_start_span"; 
		end_span = "ow_sound_end_span"; 
	}else{
		start_span = "none"; 
		end_span = "none"; 
	}
	

	this.soundslider = new Control.Slider("ow_sound_handle","ow_id_sound_slider",{
							range : $R(0,1),
							sliderValue : "1",
							onChange :  changeSoundSlider.bind(this) ,
							onSlide :	updateSoundSlider.bind(this) ,
							startSpan : start_span,
							endSpan : end_span
						});
}



//=====================================SKINS BOUTONS


// Fonction gérant les modifications de l'apparence du bouton play/pause en changeant la classe de l'élément
OWH_player.prototype.changeButtonPlay = function(){
	// Le changement de skin du bouton play / pause se fait uniquement en changeant la classe de l'élément,
	// deux css sont définis pour ce bouton, un #ow_id_bouton_play_pause.play et un #ow_id_bouton_play_pause.pause.
	// Cela permet une plus grande flexibilité au niveau des choix graphiques (hover etc ...)
	// NB : les fonctionnalités sont gérés de façon totalement indépendante de l'affichage
    // B.RAVI 2016-04-27 check if variable exist (not null AND not undefined) to not trigger javascript ERROR=>Uncaught TypeError: Cannot read property 'paused' of null
    if (!(typeof this.media === 'undefined' || this.media=== null)) {
        try{
            if (this.media.paused && this.fastSteppingOn == null){
                document.getElementById("ow_bouton_play_pause").removeClassName("pause");
                document.getElementById("ow_bouton_play_pause").addClassName("play");
            } else {
                document.getElementById("ow_bouton_play_pause").removeClassName("play");
                document.getElementById("ow_bouton_play_pause").addClassName("pause");
            }
        }catch(e){
            console.log("changeButtonPlay - media.paused undefined");
        }
    }
}

// ==================================RANGES
OWH_player.prototype.initRange = function(){
	//************************** Objet remplacant l'attribut .buffered des vidéos (qui ne fonctionne pas sur safari)
	//nom de l'objet
	this.myBuffered = {
		ranges : new Array([0,0]), // array contenant l'ensemble des ranges de video chargée
		currentRange : [],
		currentIdx : 0			// int contenant l'index du range courant
	}
	this.myBuffered.currentRange = this.myBuffered.ranges[0];
	//************************************
}

//Fonction pour se reperer dans les ranges lorsqu'on change la timeline
OWH_player.prototype.changeRangeCheck = function(value){
	// Si pas de stepping en cours et qu'on est hors des ranges actuels
	if( this.jogSteppingOn == null && this.fastSteppingOn == null&&this.outOfBufferedRange(value) ){
		this.myBuffered.ranges.push([value,value]);						// Creation d'un nouveau range, valeurs débuts et fins = valeur actuelle.
		this.myBuffered.currentRange = this.myBuffered.ranges[this.myBuffered.ranges.length-1];		// Le currentRange référence le nouveau range
		this.myBuffered.ranges.sort(this.sortRange);					// classement des ranges dans l'ordre chrono
		this.myBuffered.currentIdx = this.getCurrentRangeIdx();			// recuperation du nouvel index du range courant
	}

	this.BeginPeriodicalUpdateRanges();

}


//Fonction de test de la position par rapport au range courant :
OWH_player.prototype.outOfCurrentRange = function(valueToCompare){
	if(valueToCompare < this.myBuffered.currentRange[0] || valueToCompare > this.myBuffered.currentRange[1]){
		return true;
	} else{
		return false;
	}
}




// Fonction qui actualise le range courant
OWH_player.prototype.updateCurrentRange = function(){
	try{
		if(typeof this.media == 'undefined'){
			return false ;
		}

		if(this.media.readyState > 2 && this.media.buffered.end(0)>= this.myBuffered.currentRange[1]){
			this.myBuffered.currentRange[1] = this.media.buffered.end(0);		// Actualisation de la fin du range via video.buffered

			if (this.movie_options.timecode_OUT)
				if (this.media.buffered.end(0)>(this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag)))
					this.myBuffered.currentRange[1]=this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag);
		}


		if(this.myBuffered.currentIdx < this.myBuffered.ranges.length-1){	// Si le range courant n'est pas le dernier,
			if (this.myBuffered.currentRange[1] >= this.myBuffered.ranges[this.myBuffered.currentIdx+1][0]){		 // si la fin du range courant est supérieure au début du range suivant
				this.myBuffered.ranges[this.myBuffered.currentIdx][1] = this.myBuffered.ranges[this.myBuffered.currentIdx+1][1];	// val de fin range courant reçoit val de fin range suivant
				this.myBuffered.ranges.splice(this.myBuffered.currentIdx+1,1);                      // suppression du range suivant
			}
		}
		if(this.display_buff){
			this.displayBufferedRange();
		}
	}catch(e){
		console.log("updateCurrentRange exception "+e);
	}
	this.StopPeriodicalUpdateRanges();
}

//Fonction permettant de compléter la méthodes de tri de l'array pour les ranges
OWH_player.prototype.sortRange = function(a,b){
	return a[0]-b[0];			// Permet de classer les ranges dans l'ordre chrono de leurs valeurs de début
}

// Fonction pour recuperer l'index du range courant
OWH_player.prototype.getCurrentRangeIdx = function(){
	i = 0;
	while (this.myBuffered.currentRange[0] != this.myBuffered.ranges[i][0]){
		i++;
	}
	return i ;		// renvoi l'index du range courant;
}

// Fonction test hors des ranges déja créés
OWH_player.prototype.outOfBufferedRange = function(value){
	for (i = 0 ; i< this.myBuffered.ranges.length; i ++){		// pour chaque range créé
		if (value >=this.myBuffered.ranges[i][0] && value <= this.myBuffered.ranges[i][1]){		// si value est bornée par les valeurs de début et de fin d'un range
			this.myBuffered.currentRange = this.myBuffered.ranges[i];								// actualisation du range courant
			this.myBuffered.currentIdx = i ;														// actualisation de l'index du range courant
			return false;																			// retourne false (on est dans un range déjà existant)
		}
	}
	return true;	// sinon, retourne true, on est hors des ranges existants
}

OWH_player.prototype.displayCoreRanges = function(){
	// Si le nombre de divs représentant les ranges est différent du nombre de range (cas de jump ou de merge)
	offset= 0 ;
	if(this.movie_options.timecode_IN != "00:00:00:00" && this.movie_options.timecode_lag!="00:00:00:00" && (this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag)>1)){
		offset = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);
	}

	while(this.displayBufferedRangesDivs.length != this.media.buffered.length){
		// Si il y a moins de divs d'affichage que de ranges
		if(this.displayBufferedRangesDivs.length < this.media.buffered.length){
			//Création, paramétrage et stockage dans la liste d'un nouveau div
			newBufferedDisplay = document.createElement("div");
			newBufferedDisplay.id = this.displayBufferedRangesDivs.length+"bufferedDisplay";
			newBufferedDisplay.className = "bufferedDisplayDivs";
			//newBufferedDisplay.style.marginTop = "-"+document.getElementById("backgroundTimeline").clientHeight+"px";
			newBufferedDisplay.style.width = "0%";
			document.getElementById("bufferedDisplayContainer").appendChild(newBufferedDisplay);
			this.displayBufferedRangesDivs.push(newBufferedDisplay);
		}else{
			//Si il y a plus de divs que de ranges (cas des merges de ranges)
			//On supprime le dernier div de la page et de la liste
			document.getElementById("bufferedDisplayContainer").removeChild(this.displayBufferedRangesDivs[this.displayBufferedRangesDivs.length-1]);
			this.displayBufferedRangesDivs.pop();
		}
		// Actualisation des divs existants, les affichages correspondent aux ranges de l'objet myBuffered
	}

	for(i=0;i<this.displayBufferedRangesDivs.length;i++){	// Pour chaque div
		if(document.getElementById(i+"bufferedDisplay")){
			// On place le début du bloc à la position correspondant au début du range
			document.getElementById(i+"bufferedDisplay").style.left = ((
				((this.media.buffered.start(i)-offset-this.timeline.range.start)<0)?"0":(this.media.buffered.start(i)-offset-this.timeline.range.start)
				)*100/(this.timeline.range.end-this.timeline.range.start)) + "%";


			// On règle la largeur du bloc en fonction de la taille du range
			_width = (
				(((this.media.buffered.end(i)-offset)>this.timeline.range.end?this.timeline.range.end:this.media.buffered.end(i))
				-((this.media.buffered.start(i)-offset)<this.timeline.range.start?this.timeline.range.start:this.media.buffered.start(i))
			)*100/(this.timeline.range.end-this.timeline.range.start));
			document.getElementById(i+"bufferedDisplay").style.width = (_width>100?100:_width)+ "%";
		}
	}

	if(this.media.buffered.length == 1 && this.media.buffered.start(0) == 0 && Math.round(this.media.duration - this.media.buffered.end(0)) == 0){
		this.stopUpdateRange = true ;
	}
}


//Fonction qui gère l'affichage des zones bufferisées sous Safari
OWH_player.prototype.displayBufferedRange=function(){
	//alert("call display buff");
	// Si le nombre de divs représentant les ranges est différent du nombre de range (cas de jump ou de merge)



	if(this.displayBufferedRangesDivs.length != this.myBuffered.ranges.length){
		// Si il y a moins de divs d'affichage que de ranges
		if(this.displayBufferedRangesDivs.length < this.myBuffered.ranges.length){
			//Création, paramétrage et stockage dans la liste d'un nouveau div
			newBufferedDisplay = document.createElement("div");
			newBufferedDisplay.id = this.displayBufferedRangesDivs.length+"bufferedDisplay";
			newBufferedDisplay.className = "bufferedDisplayDivs";
			//newBufferedDisplay.style.marginTop = "-"+document.getElementById("backgroundTimeline").clientHeight+"px";
			newBufferedDisplay.style.width = "0%";
			document.getElementById("bufferedDisplayContainer").appendChild(newBufferedDisplay);
			this.displayBufferedRangesDivs.push(newBufferedDisplay);
		}else{
			//Si il y a plus de divs que de ranges (cas des merges de ranges)
			//On supprime le dernier div de la page et de la liste
			document.getElementById("bufferedDisplayContainer").removeChild(this.displayBufferedRangesDivs[this.displayBufferedRangesDivs.length-1]);
			this.displayBufferedRangesDivs.pop();
		}

		// Actualisation des divs existants, les affichages correspondent aux ranges de l'objet myBuffered
		for(i=0;i<this.displayBufferedRangesDivs.length;i++){	// Pour chaque div
			// On place le début du bloc à la position correspondant au début du range
			document.getElementById(i+"bufferedDisplay").style.left = ((this.myBuffered.ranges[i][0]-this.timeline.range.start)*100/(this.timeline.range.end-this.timeline.range.start)) + "%";
			// On règle la largeur du bloc en fonction de la taille du range
			document.getElementById(i+"bufferedDisplay").style.width = ((this.myBuffered.ranges[i][1]-this.myBuffered.ranges[i][0])*100/(this.timeline.range.end-this.timeline.range.start)) + "%";

		}

	} else{// Si le nombre de div == le nombre de ranges , on update le range courant

		document.getElementById(this.myBuffered.currentIdx+'bufferedDisplay').style.width = ((this.myBuffered.currentRange[1]-this.myBuffered.currentRange[0])*100/(this.timeline.range.end-this.timeline.range.start))+ "%";

		document.getElementById(this.myBuffered.currentIdx+'bufferedDisplay').style.left = ((this.myBuffered.currentRange[0]-this.timeline.range.start)*100/(this.timeline.range.end-this.timeline.range.start))+ "%";

	}
}

//Fonction qui gère le lancement d'un periodical executer pour actualiser les divs quand on ne peut plus utiliser video.progress
OWH_player.prototype.BeginPeriodicalUpdateRanges = function(){
	//DURATIONBUG
	//	if(this.periodicalUpdateRangeOn == null && (this.myBuffered.ranges[this.myBuffered.ranges.length -1][1] >= this.media.duration -1 )&& this.myBuffered.currentRange[1] < this.media.duration){

	if(this.periodicalUpdateRangeOn == null && (this.myBuffered.ranges[this.myBuffered.ranges.length -1][1] >= this.duration -1 )&& this.myBuffered.currentRange[1] < this.duration){

		var bufferUpdate = this.testBuff.bind(this);
		this.periodicalUpdateRangeOn = new PeriodicalExecuter(function(){bufferUpdate();},0.5);
	}
}

//Fonction qui gere l'arret du periodical executer d'actualisation des divs
OWH_player.prototype.StopPeriodicalUpdateRanges = function () {
	//DURATIONBUG
	//	if(this.periodicalUpdateRangeOn !=null && (this.myBuffered.currentRange[1]>= this.media.duration-1)){

	if(this.periodicalUpdateRangeOn !=null && (this.myBuffered.currentRange[1]>= this.duration-1)){
		this.periodicalUpdateRangeOn.stop();
		this.periodicalUpdateRangeOn = null;
	}
}

//Fonction permettant de recharger la vidéo depuis le cache (pour correction bug safari)
OWH_player.prototype.reloadVideoFromCacheSafari= function(){
	//		Cette fonction sert à recharger la vidéo depuis le cache sur Safari.
	//		Cela permet de régler le bug qui empeche la lecture dans certaines zones
	//		de la vidéo lors de sauts pendant la bufferisation.
	//		La méthode media.load() utilisée est ne chargera depuis le cache que si
	//		video.currentTime == 0 ce qui n'est pas tout à fait normal et sera peut être corrigé

	var valeur = this.GetCurrentTime();		// On stocke la position actuelle

	this.timeline.setValue(0);					// set de la position actuelle à 0
	reloadVideo = function(){					// definition de la fonction dans une variable pour l'utiliser dans un setTimeout
		this.media.load();						// appel à la fonction load (déclenche l'algorithme de chargement du media, voir la spec HTML5)
		backToPos=function(){					// definition de la fonction de retour à la position précédente (pour utilisation comme event handler)
			this.resetRanges();					// reset des ranges
			this.updateCurrentRange();			// update des ranges apres reset (normalement recrée un range qui représente toute la vidéo si tout est OK)
			this.timeline.setValue(valeur);		// retour à la valeur initiale
			this.reloading = false;
		}


		//		La fonction backToPos doit être appelée une fois que la vidéo a été chargée, sinon les instructions sont ignorées
		//		On place donc la fonction de retour à la position initiale comme event handler de l'evenement media.onloadedmetadata (quand le document a connaissance de la durée totale de la vidéo etc...)
		this.media.addEventListener("loadedmetadata",backToPos.bind(this),false);
	}

	//		L'appelle à la fonction reloadVideo est delayé pour que le seek vers 0 soit effectué et terminé avant
	setTimeout(reloadVideo.bind(this),1);
}




// Fonction permettant de reset les ranges fabriqués lors du reload de la video (bug safari) ou du changement de vidéo.
OWH_player.prototype.resetRanges = function(){
	this.stopUpdateRange = true;		// On bloque les updates des ranges
	this.initRange();					// On initialise à nouveau l'objet myBuffered
	if(this.displayBufferedRangesDivs && this.display_buff && document.getElementById("bufferedDisplayContainer")){
		for (i = 0 ; i < this.displayBufferedRangesDivs.length; i ++){		// Pour chaque div représentant les ranges
			document.getElementById("bufferedDisplayContainer").removeChild(this.displayBufferedRangesDivs[i]);		// on les supprime du document
		}
	}
	this.displayBufferedRangesDivs = [];		// On vide le tableau des divs
	this.stopUpdateRange = false ;				// On autorise à nouveau l'update des ranges
}

//Fonction de test du bug safari (video en cache mais lecture impossible)
OWH_player.prototype.testBugSafari = function(){
	var testbug = function(){		// definition de la fonction dans une variable pour pouvoir l'utiliser dans un timeout
		if(this.flagTestBugSafari){		// Si flagTestBugSafari est vrai ( c a d si on se trouvait dans le dernier range et que celui ci avait bufferisé jusqu'à la fin de la vidéo)
        	if(typeof this.media == 'undefined'){return false ;}
			if(this.media.networkState <= 2 && this.media.readyState <= 2 && !this.reloading){		// Si networkState est NETWORK_LOADING et readyState est HAVE_CURRENT_METADATA

				//Si Safari version 4- >>> la lecture sera impossible dans certaines zones et certaines zones n'apparaissent pas comme chargée dans le default controller
				if(this.browserVersion <= 4){
					this.reloading = true;				// On signale qu'on est en train de reload la vidéo
					this.reloadVideoFromCacheSafari();	// appel à la fonction qui gère le reload rapide depuis le cache
					this.mediaFullyLoaded = true;		// flag indiquant que la vidéo est completement chargée à true
                   	if(this.timeoutTestBugSafari){
                        window.clearTimeout(this.timeoutTestBugSafari);
                        this.timeoutTestBugSafari;
                    }

					// Si Safari version >4  >>> la lecture est OK sur toute la vidéo mais l'affichage de la bufferisation bug encore dans certaines zones
				} else if(!this.mediaFullyLoaded){
					this.resetRanges();										// On reset les ranges persos
					this.myBuffered.currentRange[1] = this.duration;	// on update le range courant afin qu'il représente toute la vidéo
					this.displayBufferedRange();							// on affiche le nouveau range
					//document.getElementById("infobuff").innerText = this.myBuffered.ranges.length +" : "+ this.myBuffered.ranges.join("///");	// Info des ranges;
					this.stopUpdateRange=true;								// On empeche les éventuels updates des ranges
					this.mediaFullyLoaded = true;							// flag indiquant que la video est completement chargée à true
				}
			}
        }

    }


	// appel du test en setTimeout pour ne pas avoir de problèmes avec les network et ready states




	this.timeoutTestBugSafari=setTimeout(testbug.bind(this),1000);
}

//=====================================TOOLS
// fonction de passage d'une valeur en secs à une chaine format timecode
OWH_player.prototype.secsToTimeCode = function(secs){
	var seconds = Math.round(secs*100)/100;

	var h=Math.floor(seconds/3600)<10?"0"+Math.floor(seconds/3600):Math.floor(seconds/3600);
	var m=Math.floor((seconds-h*3600)/60)<10?"0"+Math.floor((seconds-h*3600)/60):Math.floor((seconds-h*3600)/60);
	var s=Math.floor(seconds-(h*3600)-(m*60))<10?"0"+Math.floor(seconds-(h*3600)-(m*60)):Math.floor(seconds-(h*3600)-(m*60));
	// var f=Math.floor((seconds-(h*3600)-(m*60)-s)/this.movie_options.frameTimeValue)<10?"0"+Math.floor((seconds-(h*3600)-(m*60)-s)/this.movie_options.frameTimeValue):Math.floor((seconds-(h*3600)-(m*60)-s)/this.movie_options.frameTimeValue);
	var f=Math.round((seconds-(h*3600)-(m*60)-s)/this.movie_options.frameTimeValue)<10?"0"+Math.round((seconds-(h*3600)-(m*60)-s)/this.movie_options.frameTimeValue):Math.round((seconds-(h*3600)-(m*60)-s)/this.movie_options.frameTimeValue);


	return h+":"+m+":"+s+":"+f;
}


// MS NBFRAME par secs >>> Attention pour l'instant on a pas de possibilité de modifier le nombre de frame par seconde !
// a modifier aussi dans la fonction de stepping si un jour possibilité d'un nb frame/ secs != 25

// fonction de passage d'une chaine format timecode à une valeur en secs
OWH_player.prototype.timecodeToSecs = function(timecode){
	if (timecode==null)
		return 0;


	// MS - dans le cas de fichiers NTSC, on peut avoir un ";" à la place d'un ":" comme délimiteur entre secs & frames
	// le split coupe donc maintenant sur le point virgule ou les deux points
	// arrayTC = timecode.split(":");
	arrayTC = timecode.split(/[:;]+/);
	h = parseFloat(arrayTC[0]);
	m = parseFloat(arrayTC[1]);
	s = parseFloat(arrayTC[2]);
	f = parseFloat(arrayTC[3]);

	return h*3600+m*60+s+f*this.movie_options.frameTimeValue;
}


OWH_player.prototype.DisplayTimeCode = function(secs){
	var offset = 0;
	var display_offset = 0 ; 
	
	
	if(this.movie_options.timecode_lag && this.movie_options.show_tc_with_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}
	if(this.movie_options.offset_display_tc && this.movie_options.offset_display_tc != ''){
		display_offset = this.timecodeToSecs(this.movie_options.offset_display_tc);
	}
	
	var tc=this.secsToTimeCode(secs+offset+display_offset);

	if(this.movie_options.timecode_display_format =='hh:mm:ss:ff'){
		return tc;
	}else{
		var str ;
		tc = tc.split(':');
		if(this.movie_options.timecode_display_format=='hh:mm:ss'){
			str = tc[0]+':'+tc[1]+':'+tc[2];
		}else if(this.movie_options.timecode_display_format=='mm:ss'){
			m = (parseInt(tc[0])*60+parseInt(tc[1]));
			if(m<10){ m= '0'+m ; }
			str = m+':'+tc[2];
		}
	}
	return str;
}






// TESTS
// Fonction de detection du style de media (audio / video) en fonction du TypeMime lors de l'appel du player, sinon grâce à l'extension (switch case à completer eventuellement)
OWH_player.prototype.mediaTypeDetection = function(url, typeMime){

    if(typeMime != ""){

        splits = typeMime.split("/");
        mediaType = splits[0];




	} else{
        splits = url.split(".");
        extension = splits[splits.length-1];

        switch (extension){
            case "mp4":
			case "MP4":
			case "m3u8":
                mediaType = "video";
                break;
            case "m4v" :
                mediaType = "video";
                break;
            case "mp3":
			case "mpa":
			case "MPA":
                mediaType = "audio";
                break;
            case "m4a" :
                mediaType = "audio";
                break;
		}


	}

    return mediaType ;

}



// Fonction test de changement de vidéo en cours de lecture, fonctionne sous cette forme en théorie (peut être reset du controller à envisager (au moins des ranges)
OWH_player.prototype.testLoadNewVideo = function (source){
	this.media.src = source;
	this.media.load();
    this.mediaFullyLoaded = false;
}

//  VP 8/12/2011 : PLAYLIST

OWH_player.prototype.traiterFluxXml=function(str)
{
	var elements_track;
	var track_duration;

	if (str.responseXML==null)
		elements_track=str.getElementsByTagName('track');
	else
		elements_track=str.responseXML.getElementsByTagName('track');

	for(i=0;i<elements_track.length;i++)
	{

		if (elements_track[i].getElementsByTagName('location')[0]!=null){

			this.track_location.push(elements_track[i].getElementsByTagName('location')[0].firstChild.nodeValue);

			if (elements_track[i].getElementsByTagName('duration')[0]!=null){
				track_duration=parseFloat(elements_track[i].getElementsByTagName('duration')[0].firstChild.nodeValue);
			}else
				track_duration=3600;

			
			if(elements_track[i].getElementsByTagName('start_offset')[0]!=null){
				track_start_offset = parseFloat(elements_track[i].getElementsByTagName('start_offset')[0].firstChild.nodeValue);
			}else{
				track_start_offset = 0;
			}
			//console.log(elements_track[i].getElementsByTagName('srt_path')[0]);
			if (elements_track[i].getElementsByTagName('srt_path')[0] !=null ){
				this.track_srt_path[i] = (elements_track[i].getElementsByTagName('srt_path')[0].firstChild.nodeValue);
			}
//console.log(this.track_srt_path);

			this.track_duration.push(track_duration);

			this.track_start.push(this.duration);
			this.track_start_offset.push(track_start_offset);
			this.duration+=track_duration;
			this.track_end.push(this.duration);

			//			if (elements_track[i].getElementsByTagName('start')[0]!=null)
			//				this.track_start.push(elements_track[i].getElementsByTagName('start')[0].firstChild.nodeValue);
			//			else
			//				this.track_start.push('');
			//
			//			if (elements_track[i].getElementsByTagName('end')[0]!=null)
			//				this.track_end.push(elements_track[i].getElementsByTagName('end')[0].firstChild.nodeValue);
			//			else
			//				this.track_end.push('');

			this.nb_tracks++;
		}


	}
}

// Fonction attribuant une vidéo au player à partir d'une URL et d'un type.
OWH_player.prototype.AttachPlaylist = function(typeMime){
    var track_media, text;

	this.capsulevideo = document.createElement('div');
	this.capsulevideo.id = "capsVid";
	if(this.controllerpanel){
		this.container.insertBefore(this.capsulevideo,this.controllerpanel);
	}else{
		this.container.appendChild(this.capsulevideo);
	}


	for(i=0;i<this.nb_tracks;i++){
		track_media = document.createElement(this.mediaType);			//Création d'un nouvel élement video
		track_media.id = this.movie_options["name"]+i;								// definition id
		track_media.src = this.track_location[i];									// definition de l'url de la video
		track_media.type = typeMime;								// definition du type de la video
		track_media.style.display ="none";
		if(typeof this.track_srt_path[i] != 'undefined'){
			//console.log(this.track_location[i]+" "+this.track_srt_path[i]);
			var regLangue = /(\((?!\/)(.*?)\))/gi;
			var regURL = /[-a-zA-Z0-9@:%_\+.~#?&\/\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)?/gi;
			var langues = this.track_srt_path[i].match(regLangue);
			var urls = this.track_srt_path[i].match(regURL);
			var langue;
			for (var j = 0; j < langues.length; j++) {
				//langue = langues[j].split(/<|>/);
				langue = langues[j].split(/\(|\)/);
				//création élément tracks
				var srt_track =document.createElement('track');
				srt_track.label=langue[1];
				srt_track.kind="subtitles";
				srt_track.srclang=langue[1];
				// srt_track['default']="1";
				srt_track.src=urls[j];
				track_media.appendChild(srt_track);
			}
		}

		if(this.movie_options.xStand){
			track_media.preload ="metadata";
		}
		text = document.createTextNode("Video not supported");//Creation d'un texte alternatif
		track_media.appendChild(text);


		this.capsulevideo.appendChild(track_media);
		this.track_media.push(track_media);
		track_media.load();
		this.track_init_events.push(false);
	}
	
	if(this.mediaType == "audio" ){                 // Si le media est audio, on regle le test ShuttleMove en consequence (sur chrome, le playbackRate négatif est ignoré, ce n'est donc pas fonctionnel)
		//this.shuttleRatioSteppPlaybackrate =function (){
		//	if(this.jogspeed >=-10 && this.jogspeed <= 10) {
		//		return true;
		//	} else {
			//	return false;
			//}
		//}
		if( !this.movie_options['noPoster'] && this.movie_options['image']){
			if(this.mediaType == 'audio'   && this.movie_options.show_vignette_audio){
				this.audio_viewport = document.createElement('div');
				this.audio_viewport.id = 'ow_audioviewport';
				this.audio_viewport.className="ow_audioviewport";
				this.audio_vignette = document.createElement('img');
				if(this.movie_options['image'].indexOf('ol=player') != false){
					img = this.movie_options['image'].replace('&ol=player','');
				}else{
					img = this.movie_options['image'];
				}
				this.audio_vignette.src =img;
				this.audio_vignette.id = 'ow_audiovignette';
				this.audio_vignette.className = 'ow_audiovignette';
				this.audio_viewport.appendChild(this.audio_vignette);
				if(typeof this.audio_viewport != 'undefined'){
					this.capsulevideo.appendChild(this.audio_viewport);
				}
				if (this.movie_options.activateWaveForms ){
					$j(this.audio_viewport).append('<div id="peaks-container"></div>');
				}
			}else{
				this.media.poster= this.movie_options['image'];
			}

		}
		if ( this.browser =="Safari"){

			this.shuttleRatioSteppPlaybackrate =function (){
				if(this.jogspeed >=-10 && this.jogspeed <= 10) {
					return true;
				} else {
					return false;
				}
			}

		}else if (this.browser == "Explorer" || this.browser == "Chrome" || this.browser == "Firefox"){

			this.shuttleRatioSteppPlaybackrate =function (){
				if(this.jogspeed >=0 && this.jogspeed <= 10) {
					return true;
				} else {
					return false;
				}
			}

		}


	}
}

OWH_player.prototype.OnEndTrack = function() {
	this.currentTrackEnded = true ;
	this.timeline.setValue(this.timeline.value+0.2);

}
// Fonction qui joue le fichier suivant
OWH_player.prototype.PlayNext = function() {
	if((this.current_track+1) >=this.nb_tracks){
		this.StopTheVideo();
	}else{
		this.playNextFlag = true ;
		this.track_media[this.current_track+1].currentTime = this.track_start_offset[this.current_track+1];
		this.ChangeTrack(this.current_track+1);
	}
}

// Fonction qui joue le fichier track
OWH_player.prototype.ChangeTrack = function(no_track) {
	if(no_track <this.nb_tracks && no_track >=0 && no_track!=this.current_track){
		var playState = false;
		var fastStepState = 0 ;
		var slowStepState = 0 ;
		var jogStepState = 0 ;
		var varSpeedState = 0 ;
		var volume = 1;
		var muteState = false ;
		var current_width = null ; 
		
		if(this.media){

			volume = this.media.volume;
			muteState = this.media.muted;
			current_width = this.media.videoWidth ; 


			if(this.fastSteppingOn || this.slowSteppingOn ){
				playState = false ;
			}else if (this.playNextFlag){
				playState = true ;
			}else {
				playState = !this.media.paused;
			}



			// document.getElementById('infobuff').innerHTML =("playState : "+playState+" media.paused : "+this.media.paused+" playnextflag : "+this.playNextFlag+" fastSteppignOn : "+this.fastSteppingOn+" slowSteppingOn : "+this.slowSteppingOn);

			if(this.slowSteppingOn != null ){
				slowStepState = this.slowStepValue;
			}
			if(this.fastSteppingOn != null ){
				fastStepState = this.fastSteppingDirection;
			}

			if(!this.media.paused && this.media.playbackRate!=1 && this.fastSteppingOn){
				varSpeedState = this.media.playbackRate;
				playState = false ;
				fastStepState = 0 ;
				this.playNextFlag = false ;
			}
			this.StopTheVideo();
		}

		if(typeof this.audioNodes != "undefined" && typeof this.audioNodes.sourceNodes != 'undefined'){
			this.audioNodes.sourceNodes[this.current_track].disconnect();
		}

		this.current_track=no_track;
		this.url=this.track_location[this.current_track];
		this.mediaType= this.mediaTypeDetection(this.url,"");
		this.BegTrack=this.track_start[this.current_track];
		this.EndTrack=this.track_end[this.current_track];
		this.media=this.track_media[this.current_track];

		// ======= Set des listeners propres à la video
		if(this.track_init_events[this.current_track] == false){
			this.media.addEventListener("click", this.ToggleVideo.bind(this),false);
			if(this.current_track == 0 ){
				this.media.addEventListener("durationchange",this.setTimeline.bind(this),false);
				// sur la première track d'une playlist, si on a ref_tc_in défini, alors on veut jump à ref_tc_in une fois qu'on a le media 
				if(this.movie_options.ref_tc_IN != null ){
					init_ref_tcin = function(){
						this.GoToLongTimeCode(this.movie_options.ref_tc_IN);
						// cette action n'étant necessaire qu'une fois, on supprime aussi immédiatement l'event listener apres execution
						this.media.removeEventListener('durationchange',init_ref_tcin,false);
					}.bind(this);
					this.media.addEventListener("durationchange",init_ref_tcin,false);
				}
				this.media.addEventListener("canplay",function(){
					if( this.movie_options.activateAudioContext && (document.getElementById("ow_bouton_left_sound") || document.getElementById("ow_bouton_right_sound" )) && (typeof window.webkitAudioContext != "undefined" || typeof window.AudioContext != "undefined" ) && typeof this.audio_context == 'undefined'){
						this.initWebAudioNodes();
						$j("#sound_channels_btns").addClass("active");
					}else{
						if($j(this.container).find('#ow_bouton_left_sound').length>0){
							$j(this.container).find('#ow_bouton_left_sound').css('display','none');
						}
						if($j(this.container).find('#ow_bouton_right_sound').length>0){
							$j(this.container).find('#ow_bouton_right_sound').css('display','none');
						}
					}
				}.bind(this),false);
			
				// ajout callback onmovieready pour les playlists => devrait se déclencher juste apres la fin de l'initialisation totale du player (quand myVideo a été rempli..) (non testé 17/01/17)
				if(typeof this.movie_options.onMovieReady == 'function'){
					try{
						this.onMovieReady_handler = function(evt){
							this.movie_options.onMovieReady();
							this.media.removeEventListener('loadedmetadata',this.onMovieReady_handler,false);
						}.bind(this);
						this.media.addEventListener('loadedmetadata',this.onMovieReady_handler,false);
					}catch(e){
						console.log("OWH_player - crash callback onMovieReady :"+e);
					}
				}
				// ajout callback onmovieready pour les playlists => devrait se déclencher juste apres la fin de l'initialisation totale du player (quand myVideo a été rempli..) (non testé 17/01/17)
				if(typeof this.movie_options.onMovieReady == 'function'){
					try{
						this.onMovieReady_handler = function(evt){
							this.movie_options.onMovieReady();
							this.media.removeEventListener('loadedmetadata',this.onMovieReady_handler,false);
						}.bind(this);
						this.media.addEventListener('loadedmetadata',this.onMovieReady_handler,false);
					}catch(e){
						console.log("OWH_player - crash callback onMovieReady :"+e);
					}
				}
			}
			if(this.browser == "Safari" && this.current_track == 0 && this.movie_options.ref_tc_IN == null ){
				this.media.addEventListener("canplay",function(){if(typeof this.timeline != "undefined" ){this.timeline.setValue(this.timeline.range.start);}}.bind(this),false);
			}
			this.media.addEventListener("timeupdate",this.UItest.bind(this),false);
			this.media.addEventListener("play", this.changeButtonPlay.bind(this),false);
			this.media.addEventListener("pause", this.changeButtonPlay.bind(this),false);
			this.media.addEventListener("progress",this.testBuff.bind(this),false);
			this.media.addEventListener("ratechange", this.rateHasChanged.bind(this), false);
			this.media.addEventListener("volumechange", this.defaultControlVolumeChange.bind(this), false);

			//playlist
			// this.media.addEventListener("ended",this.PlayNext.bind(this),false);
			this.media.addEventListener("ended",this.OnEndTrack.bind(this),false);

			if(this.controlSeek == "Events"){
				this.media.addEventListener("seeked",this.AllowSeeking.bind(this),false);
				this.media.addEventListener("seeking",this.BlockSeeking.bind(this),false);
			}
			this.track_init_events[this.current_track] = true ;
			// Si le callback onMovieFinished existe, alors on l'ajoute sur la dernière track de la playlist, en meme temps qu'on initialise le reste des events pr ce media
			if(this.movie_options.onMovieFinished!=null && typeof(this.movie_options.onMovieFinished)=='function'){
				if(no_track == (this.nb_tracks-1) ){
					this.media.addEventListener("ended",function(){this.movie_options.onMovieFinished();}.bind(this),false);
				}
			}
		}
		for(i=0;i<this.nb_tracks;i++){
			if(i!=this.current_track) document.getElementById(this.movie_options["name"]+i).style.display ="none";
			else document.getElementById(this.movie_options["name"]+i).style.display ="block";
		}

		if(typeof this.audioNodes != "undefined" && typeof this.audioNodes.sourceNodes != 'undefined'){
			this.audioNodes.sourceNodes[this.current_track].connect(this.audioNodes.splitter);
		}

		this.mediaFullyLoaded = false;
		this.stopUpdateRange = false;




		if(playState){
			setTimeout(this.PlayTheVideo.bind(this),10);
		}
		playState = false ;

		if(this.playNextFlag){
			this.playNextFlag = false ;
		}

		if(slowStepState != 0){
			this.InitStepping('frame',slowStepState);
			slowStepState = 0;
		}
		if(fastStepState != 0){
			this.InitStepping('fast',fastStepState);
			fastStepState = 0;
		}

		if(this.jogIsUsed ){
			setTimeout(function(){
				this.ShuttleMove();
			}.bind(this),100);
		}

		if(varSpeedState !=0){
			setTimeout(this.varSpeedPlaying.bind(this,varSpeedState),10);
			varSpeedState=0;
		}

		if(this.media){
			 this.media.volume = volume ;
			this.media.muted = muteState ;
		}

		if(this.media && $('ow_controllerpanel') && (this.resizeCustom || ($j(this.capsulevideo).hasClass('video_fill') && this.movie_options.force_ratio_ref != null ))){
			this.resizeVideoKeepRatio();
		}
		
		// ajout d'un cas spécifique dans le cas ou plusieurs vidéo d'une playlist ont des ratio différents et qu'on a pas activé un ratio forcé, 
		// lors du passage de l'un à l'autre on doit mettre à jour la timeline afin que le curseur ne sorte pas du rail 
		if(current_width  != null && this.media.videoWidth != current_width  && this.movie_options.force_ratio_ref == null ){
			this.adjustTimeline() ; 
		}

		if(this.movie_options.onTrackChange != null){
            this.movie_options.onTrackChange(no_track);
		}
		this.currentTrackEnded = false ;
		if(this.movie_options.xStand && this.media.readyState>0){
			this.media.currentTime = this.media.currentTime;
		}
	}
}


OWH_player.prototype.killPeriodExec = function () {

	if(this.periodicalUpdateRangeOn){

		this.periodicalUpdateRangeOn.stop();
	}

	if(this.slowSteppingOn){
		this.slowSteppingOn.stop();
		this.slowStepValue = null;
	}
	if(this.fastSteppingOn){
		this.fastSteppingOn.stop();
	}
	if(this.jogSteppingOn){
		this.jogSteppingOn.stop();
	}
}


// Fonction qui renvoi le temps courant

OWH_player.prototype.GetCurrentTime = function() {
    // B.RAVI 2016-04-27 check if variable exist (not null AND not undefined) to not trigger javascript=> ERROR Cannot read property 'currentTime' of null
    if (typeof this.media === 'undefined' || this.media=== null) {
        return null;
    }

    try{
		if(this.playlist) return (this.BegTrack  + this.media.currentTime-this.track_start_offset[this.current_track]);
		else return this.media.currentTime;
	}catch(e){
		console.log("GetCurrentTime - media undefined");
		return 0;
	}
}


OWH_player.prototype.destroy = function(){
	this.StopTheVideo();

	if(this.movie_options.auto_send_tracked_duration){
		this.sendLogDurationVisio();
		window.removeEventListener("beforeunload",this.unload_func,false);
	}
	
	
	if(window.addEventListener){
		window.removeEventListener('resize',this.resizeListener,false);
	}else{
		window.detachEvent('onresize',this.resizeListener);
	}

	if(this.dynamicController){
		if(this.timer_showController){
			clearTimeout(this.timer_showController);
		}
		document.getElementById(this.container.id).removeEventListener('mousemove',this.func_showController,false);
		if(this.movie_options.dynamicSyncQuery_triggers_showController){
			$j(this.dynamicSyncQuery).unbind('mousemove',this.func_showController);
		}
	}
	if(this.timeoutTestBugSafari){
		window.clearTimeout(this.timeoutTestBugSafari);
	}
	try{
		if(typeof this.audio_context != 'undefined' && this.audio_context != null && typeof this.audio_context.close =='function'){
			this.audio_context.close() ; 
		}
	}catch(e){}

	try{
		this.killPeriodExec();
		this.StopPeriodicalUpdateRanges();
	}catch(e){}
	if (this.playlist){
		for (i = 0 ; i< this.track_media.length ; i++){

			this.track_media[i].pause();
			// MS workaround firefox => les connexions aux media ne sont pas stoppées lorsqu'on supprimer l'élément vidéo sur firefox, on doit donc forcer manuellement l'arret du téléchargement des medias
			if(this.browser == "Firefox" || this.browser == "Safari" || this.browser == 'Chrome'){
				this.track_media[i].src = "";
				this.track_media[i].load();
			}

			this.track_media[i].parentNode.removeChild(this.track_media[i]);
		}
	}else{
		this.media.pause();
		// MS workaround firefox => les connexions aux media ne sont pas stoppées lorsqu'on supprime l'élément vidéo sur firefox, on doit donc forcer manuellement l'arret du téléchargement des medias
		//  MS 25.05.15 -> ajout meme workaround pour safari => semble corriger un bug lors de l'enchainement de bcp de players sans reload de la page (cf filmco)
		if(this.browser == "Firefox" || this.browser == "Safari" || this.browser == 'Chrome'){
			this.media.src = "";
			this.media.load();
			if(this.browser == "Firefox" ){
				document.onmozfullscreenchange =null;
			}
		}
		this.media.parentNode.removeChild(this.media);
		delete this.media;
		delete this;
	}
}


// ========================================Fonctions définition d'extraits

OWH_player.prototype.showExtsButtons = function(){
	if(this.movie_options.extsBtnsMoveToId !== null){
		if($j("#ow_boutons_extraits").parent('#'+this.movie_options.extsBtnsMoveToId ).length == 0){
			$j("#ow_boutons_extraits").appendTo('#'+this.movie_options.extsBtnsMoveToId);
		}
	}
	if(document.getElementById('ow_boutons_extraits') && !this.movie_options.extsBtnsDisabled){
		this.extsBtns = true;
		document.getElementById('ow_boutons_extraits').style.display = "";
	}
	if(this.movie_options.extsContext == "montage"){
		document.getElementById("ow_bouton_ext_save").style.display = "";
	}
	
	if(this.movie_options.extBtnNew){
		document.getElementById("ow_bouton_ext_new").style.display = "";
	}
	
	if(typeof $j != 'undefined' && !$j(this.container).hasClass("exts_btns_active")){
		$j(this.container).addClass("exts_btns_active");
	}
}
OWH_player.prototype.hideExtsButtons = function(){
	if(document.getElementById('ow_boutons_extraits')){
		this.extsBtns = false;
		document.getElementById('ow_boutons_extraits').style.display = "none";
	}
	if(typeof $j != 'undefined' && $j(this.container).hasClass("exts_btns_active")){
		$j(this.container).removeClass("exts_btns_active");
	}

}

OWH_player.prototype.extMarkTC= function(type) {
	TC=this.GetCurrentTime();
	if(this.movie_options.timecode_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_IN);
	}

    this.dontCallOnChange = true;

	if(type=="in" && this.BegSelect == this.EndSelect ){
		this.timelineSel.setValue(this.duration,1);
		this.timelineSel.setValue(TC, 0);
	}else if (type=="in" && TC < this.EndSelect){
		this.timelineSel.setValue(TC, 0);
	}else if (type=="out" && TC>this.BegSelect){
		this.timelineSel.setValue(TC,1);
	}

	 this.dontCallOnChange = false;


	if (this.movie_options.onSelectionSliderChange)         {       this.movie_options.onSelectionSliderChange();   }
}

OWH_player.prototype.newExt = function(){
	if(typeof myPanel != "undefined"){
		myPanel.addExtrait();
	}
	if(this.selectionIsHidden){
		this.ShowSelectionSlider();
	}
	/* -- code prévu pour une rotation click "new" => définition d'ext => "save", rejeté pour l'instant
	if(document.getElementById("ow_bouton_ext_save") && document.getElementById("ow_bouton_ext_save").style.display =="none"){
		document.getElementById("ow_bouton_ext_save").style.display = "";
		document.getElementById("ow_bouton_ext_new").style.display = "none";
	}*/
}

OWH_player.prototype.saveExt = function(){
	if(this.movie_options.onExtSave){
		this.movie_options.onExtSave();
	}

	this.cancelExt();
}


OWH_player.prototype.cancelExt = function(){
	/* -- code prévu pour une rotation click "new" => définition d'ext => "save", rejeté pour l'instant
	if(document.getElementById("ow_bouton_ext_new") && document.getElementById("ow_bouton_ext_new").style.display =="none"){
		document.getElementById("ow_bouton_ext_new").style.display = "";
		document.getElementById("ow_bouton_ext_save").style.display = "none";
	}*/

	this.UnSetSelection();
	if(typeof myPanel != "undefined"){
		try{
		myPanel.selectExtrait('');
		}catch(e){
			console.log("Erreur communication cancelExt visionneuse vers rightPanel");
			console.log(e);
		}
	}
}



// ========================================Fonctions Traitement audio (channel redirection)

OWH_player.prototype.initWebAudioNodes = function(){
	// création du "contexte" objet permettant de creer les différents noeuds / connexions pour l'api web audio
	if(typeof window.webkitAudioContext != 'undefined'){
		this.audio_context = new webkitAudioContext();
	}else{
		this.audio_context = new AudioContext();
	}
	this.audioNodes = {};
	// création d'un splitter & d'un merger, deux noeuds qui fonctionnent en tandem.
	// splitter reçoit un signal stereo & le divise en de multiples signaux d'output
	// merger reçoit plusieurs signaux & les regroupe en 1 signal d'output
	// ici on set par défaut les limites de canaux de sorties pour el splitter et d'entrée pour le merger à 2
	this.audioNodes.splitter = this.audio_context.createChannelSplitter(2);
	this.audioNodes.merger = this.audio_context.createChannelMerger(2);
	/*if(this.playlist){
		this.audioNodes.sourceNodes = new Array();

		for(i = 0 ; i < this.track_media.length;i++){
			this.audioNodes.sourceNodes.push(this.audio_context.createMediaElementSource(this.track_media[i]));
		}
		this.audioNodes.sourceNodes[this.current_track].connect(this.audioNodes.splitter);

	}else{
		this.audioNodes.sourceNode = this.audio_context.createMediaElementSource(this.media);
		this.audioNodes.sourceNode.connect(this.audioNodes.splitter);
	}*/

	this.audioNodes.splitter.connect(this.audioNodes.merger,0,0);
	this.audioNodes.splitter.connect(this.audioNodes.merger,1,1);

	this.audioNodes.merger.connect(this.audio_context.destination);

}

OWH_player.prototype.initAudioSourceElement = function(){
	if(typeof this.audio_context != 'undefined'){
		//console.log("call initAudioSourceElement") ; 
		if(this.playlist){
			this.audioNodes.sourceNodes = new Array();

			for(i = 0 ; i < this.track_media.length;i++){
				this.audioNodes.sourceNodes.push(this.audio_context.createMediaElementSource(this.track_media[i]));
			}
			this.audioNodes.sourceNodes[this.current_track].connect(this.audioNodes.splitter);

		}else{
			this.audioNodes.sourceNode = this.audio_context.createMediaElementSource(this.media);
			this.audioNodes.sourceNode.connect(this.audioNodes.splitter);
		}
		
	}
}



OWH_player.prototype.LeftChannelOnly = function(){
	if(typeof this.audioNodes == "undefined"){
		return false;
	}
	if((this.playlist && typeof this.audioNodes.sourceNodes == 'undefined') || (!this.playlist && typeof this.audioNodes.sourceNode == 'undefined' )){
		this.initAudioSourceElement();
	}
	
	if( !$("ow_bouton_right_sound").hasClassName("ClickBouton")){
		$("ow_bouton_right_sound").addClassName("ClickBouton");

		if( $("ow_bouton_left_sound").hasClassName("ClickBouton")){
			$("ow_bouton_left_sound").removeClassName("ClickBouton");
		}
		// reconnexion du canal 0 (L), si jamais il a été déconnecté, déconnexion du canal 1 (R).
		this.audioNodes.splitter.connect(this.audioNodes.merger,0,0);
		this.audioNodes.splitter.disconnect(1);
	}else{
		this.audioNodes.splitter.connect(this.audioNodes.merger,0,0);
		this.audioNodes.splitter.connect(this.audioNodes.merger,1,1);
		$("ow_bouton_right_sound").removeClassName("ClickBouton");
	}


	// MSTEST : fonctionnement pour test uniquement :
	// this.audioNodes.splitter.connect(this.audioNodes.merger,0);
	// this.audioNodes.splitter.connect(this.audioNodes.merger,1);

}

OWH_player.prototype.RightChannelOnly = function(){
	if(typeof this.audioNodes == "undefined"){
		return false;
	}
	if((this.playlist && typeof this.audioNodes.sourceNodes == 'undefined') || (!this.playlist && typeof this.audioNodes.sourceNode == 'undefined' )){
		this.initAudioSourceElement();
	}
	if( !$("ow_bouton_left_sound").hasClassName("ClickBouton")){
		$("ow_bouton_left_sound").addClassName("ClickBouton");
		if( $("ow_bouton_right_sound").hasClassName("ClickBouton")){
			$("ow_bouton_right_sound").removeClassName("ClickBouton");
		}

		// reconnexion du canal 1 (R), si jamais il a été déconnecté, déconnexion du canal 0 (L).
		this.audioNodes.splitter.disconnect(0);
		this.audioNodes.splitter.connect(this.audioNodes.merger,1,1);
	}else{
		this.audioNodes.splitter.connect(this.audioNodes.merger,0,0);
		this.audioNodes.splitter.connect(this.audioNodes.merger,1,1);
		$("ow_bouton_left_sound").removeClassName("ClickBouton");
	}


	// MSTEST : fonctionnement pour test uniquement :
	// this.audioNodes.splitter.disconnect(0);
	// this.audioNodes.splitter.disconnect(1);
}

//============= FONCTIONS MOSAIQUE PREVIEW

OWH_player.prototype.setMosaicPath = function(){
	if(this.movie_options.mosaic_path && typeof this.movie_options.mosaic_path != "undefined" && document.getElementById("ow_img_prevOverContainer") != null){
		document.getElementById("ow_img_prevOverContainer").style.backgroundImage = "";
		document.getElementById("ow_img_prevOverContainer").style.backgroundImage = "url('"+this.movie_options.mosaic_path+"')";
		this.mosaic_initialized = true ; 
		if(typeof this.sequences_wrapper != 'undefined' && document.getElementById("ow_img_prevSeqContainer") != null && !this.movie_options.seq_use_seq_vignette){
			document.getElementById("ow_img_prevSeqContainer").style.backgroundImage = "";
			document.getElementById("ow_img_prevSeqContainer").style.backgroundImage = "url('"+this.movie_options.mosaic_path+"')";
			document.getElementById("ow_img_prevSeqContainer").style.backgroundSize = "auto";
		}
		this.sec_mosaic = 0;
	}

}
OWH_player.prototype.getPreviewPic = function(secs,img_box_id,lab_box_id){
	if(typeof img_box_id == 'undefined'){
		img_box_id = "ow_img_prevOverContainer" ; 
	}
	if(typeof lab_box_id == 'undefined'){
		lab_box_id = "ow_lab_prevOverContainer" ; 
	}
	if(document.getElementById(img_box_id).style.backgroundImage && this.movie_options.mosaic_path ){
		pos = secs/this.movie_options.mosaic_step ; // on divise par la valeur du pas entre deux images de la mosaique
		pos = pos/10; // on divise par le nombre de vignette par ligne
		row = parseInt(pos,10);
		col = Math.floor((pos % 1)*10);
		// console.log("coordonnée image : "+row+" "+col);
		document.getElementById(img_box_id).style.backgroundPosition = "-"+col*document.getElementById(img_box_id).clientWidth+"px -"+row*document.getElementById(img_box_id).clientHeight+"px";

		if(this.movie_options.tcMosaicExact ){
			offset = 0
			if(this.movie_options.timecode_IN){
				offset = (this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag));
			}
			this.sec_mosaic = col*this.movie_options.mosaic_step+row*10*this.movie_options.mosaic_step+offset;
			document.getElementById(lab_box_id).innerHTML = this.DisplayTimeCode(this.sec_mosaic);
		}
	}
}

OWH_player.prototype.getPreviewPicFromIndex = function(secs,img_box_id,lab_box_id){
	if(typeof img_box_id == 'undefined'){
		img_box_id = "ow_img_prevOverContainer" ; 
	}
	if(typeof lab_box_id == 'undefined'){
		lab_box_id = "ow_lab_prevOverContainer" ; 
	}
	
	if(document.getElementById(img_box_id).style.backgroundImage && this.movie_options.mosaic_path && this.movie_options.mosaic_pics_index != null){
		i = 0;
		offset=0;
		index_pics = this.movie_options.mosaic_pics_index;
		if(this.movie_options.timecode_IN){
			offset = (this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag));
		}
		if(this.movie_options.mosaic_tc_relative_to_doc == true){
			var tc=this.secsToTimeCode(secs);
		}else{
			var tc=this.secsToTimeCode(secs+offset);
		}
		while (tc > index_pics[i] && i< index_pics.length){
			i++;
		}
		i--;
		row = parseInt(i/10,10);
		col = i-row*10;
		document.getElementById(img_box_id).style.backgroundPosition = "-"+col*document.getElementById(img_box_id).clientWidth+"px -"+row*document.getElementById(img_box_id).clientHeight+"px";
		if(this.movie_options.tcMosaicExact ){
			if(this.movie_options.mosaic_tc_relative_to_doc == true){			
				this.sec_mosaic = this.timecodeToSecs(index_pics[i])+offset;
			}else{
				this.sec_mosaic = this.timecodeToSecs(index_pics[i]);
			}
			document.getElementById(lab_box_id).innerHTML = this.DisplayTimeCode(this.sec_mosaic);
		}
	}
}
//============= ============= ============= =============

OWH_player.prototype.makeMouseActivityToggleController=function(){
	// référence timer des animations
	this.timer_showController = null ;

	if(this.user_OS == 'Android' || this.user_OS == 'iPad' || this.user_OS == 'iPhone'){
		this.timer_disappear = this.movie_options.timer_disappear_mobile;
	}else{
		this.timer_disappear = this.movie_options.timer_disappear;
	}
	//this.show_first_only = true ;

	
	this.func_hideController = function(event){
		// disparition du curseur + disparition du controller (avec vérification toujours en mode fullscreen) + réinitialisation de la référence timer
		$j(this.container).css('cursor','none');
		//console.log("hide after timeout : "+this.timer_showController);
		$j(this.controllerpanel).clearQueue();
		$j(this.controllerpanel).fadeTo(300,0, function(){
			$j(this.controllerpanel).css('visibility','hidden');
			if(this.timer_showController != null){
				clearTimeout(this.timer_showController);
			}
			this.timer_showController=null;
		}.bind(this));
		if(typeof (this.dynamicSyncQuery) != 'undefined'){
			$j(this.dynamicSyncQuery).clearQueue();
			$j(this.dynamicSyncQuery).fadeTo(300,0, function(){
				$j(this.dynamicSyncQuery).css('visibility','hidden'); 
				if(this.timer_showController != null){
					clearTimeout(this.timer_showController);
				}
				this.timer_showController=null;
			}.bind(this));
		}
	}
	
	this.func_showController = function(event){
		// Si mouvement detecté
		/*if(this.show_first_only){
			console.log(this.timer_showController);
			this.show_first_only = false ;
		}*/
		if(this.firstResizeDone && (event.clientX != this.old_value_X || event.clientY != this.old_value_Y)){
			// Si le controller n'est pas affiché (== pas de timer de disparition enregistré)
			if(this.timer_showController == null){
				// on affiche le curseur + le controller
				$j(this.container).css('cursor','auto');
				$j(this.controllerpanel).css('visibility','');
				$j(this.controllerpanel).clearQueue();
				$j(this.controllerpanel).fadeTo(100,1);
				if(typeof (this.dynamicSyncQuery) != 'undefined'){
					$j(this.dynamicSyncQuery).css('visibility','');
					$j(this.dynamicSyncQuery).clearQueue();
					$j(this.dynamicSyncQuery).fadeTo(100,1);
				}
				// utilisation de l'opacité au lieu du display pour cacher le controllerpanel sans risquer de mal initialiser la timeline
			}else{
			// Si controller affiché => on stop le timer courant pour le refresh
				clearTimeout(this.timer_showController);
			}
			// création timer de disparition du controller
			this.timer_showController  = setTimeout(function(){
				if(!this.flagMouseOnController && ($j(this.container).find("#ow_quality_switch").length == 0 || !$j(this.container).find("#ow_quality_switch").is(":focus"))){
					this.func_hideController() ; 
					// utilisation de l'opacité au lieu du display pour cacher le controllerpanel sans risquer de mal initialiser la timeline
					// this.timer_showController=null;
				}else{
					setTimeout(this.func_showController,200);
				}

			}.bind(this),this.timer_disappear);

		}
		// enregistrement des valeurs courantes du curseur pour pouvoir detecter un mouvement du curseur lors de l'appel suivant
		this.old_value_X = event.clientX;
		this.old_value_Y = event.clientY;

	}.bind(this);

	this.setFlagOnController = function(value){
		this.flagMouseOnController = value;
		// console.log("setFlagOnController "+this.flagMouseOnController );
	}
	
	// ajout de la classe dynamicController pour info + initialisation evenement.
	// check if variable exist (not null AND not undefined) to not trigger javascript ERROR
	if (!(typeof this.container === 'undefined' || this.container=== null)) {
		document.getElementById(this.container.id).addClassName('dynamicController');
		document.getElementById(this.container.id).addEventListener('mousemove',this.func_showController,false);
		$j(this.controllerpanel).get(0).addEventListener('mouseenter',this.setFlagOnController.bind(this,true),false);
		$j(this.controllerpanel).get(0).addEventListener('mouseleave',this.setFlagOnController.bind(this,false),false);
		if(this.movie_options.dynamicSyncQuery_triggers_showController){
			$j(this.dynamicSyncQuery).bind('mousemove',this.func_showController);
			$j(this.dynamicSyncQuery).bind('mouseenter',this.setFlagOnController.bind(this,true));
			$j(this.dynamicSyncQuery).bind('mouseleave',this.setFlagOnController.bind(this,false));
		}
	}


	// MS - on initialise le timeout pour que, même sans aucun mouvement de la souris, le comportement s'initialise correctement (apparition onload => fade 2s après )
	this.timer_showController  = setTimeout(function(){
		if(!this.flagMouseOnController && ($j(this.container).find("#ow_quality_switch").length == 0 || !$j(this.container).find("#ow_quality_switch").is(":focus"))){
			// disparition du curseur + disparition du controller (avec vérification toujours en mode fullscreen) + réinitialisation de la référence timer
			$j(this.container).css('cursor','none');
			$j(this.controllerpanel).clearQueue();
			//console.log("hide after timeout : "+this.timer_showController);
			$j(this.controllerpanel).fadeTo(300,0, function(){$j('#ow_controllerpanel').css('visibility','hidden'); this.timer_showController=null; }.bind(this));
			if(typeof (this.dynamicSyncQuery) != 'undefined'){
				$j(this.dynamicSyncQuery).clearQueue();
				$j(this.dynamicSyncQuery).fadeTo(300,0, function(){$j(this.dynamicSyncQuery).css('visibility','hidden'); this.timer_showController=null; }.bind(this));
			}

			// utilisation de l'opacité au lieu du display pour cacher le controllerpanel sans risquer de mal initialiser la timeline
			// this.timer_showController=null;
		}else{
			setTimeout(this.func_showController,200);
		}

	}.bind(this),this.timer_disappear);
}


OWH_player.prototype.show_poster_on_ended = function(){
		if(this.media.autoplay){
			this.media.autoplay = false ;
		}
		if((typeof this.media.poster == 'undefined'|| this.media.poster=='') && typeof this.movie_options.image != 'undefined'){
			this.media.poster= this.movie_options['image'];
		}

		tmp = this.media.src ;
        this.media.src = tmp;
    }

OWH_player.prototype.initPeripheralControls = function(){
	
	
	this.playerHasFocus = true; 
	// console.log("default start playerfocus");
	
	$j(this.container).on('click', function(){
		this.playerHasFocus = true ; 
		// console.log("start playerfocus");
	}.bind(this));
	
	$j(document).on('click',function(evt){
		target = evt.target || evt.srcElement ;
		if (!$j.contains(this.container,target) && !$j(target).is(this.container)){
			this.playerHasFocus = false ; 
			// console.log("release playerfocus");
		}
	}.bind(this));
	
	
	// Fonction de traitement des frappes clavier.
	key_controls = function(event){
		
		// code gardé pour debug éventuel.. 
		/*var id = parseInt(Math.random()*100000);
		this['seek_'+id] = function(id_){
			console.log("seeking "+id_);
			this.media.removeEventListener("seeking",this['seek_'+id_],false);
			
		}.bind(this,id);
		this['seek2_'+id] = function(id_){
			console.log("seeked "+id_);
			this.media.removeEventListener("seeking",this['seek_'+id_],false);
			this.media.removeEventListener("seeked",this['seek2_'+id_],false);
		}.bind(this,id);
		this.media.addEventListener("seeking",this['seek_'+id],false);
		this.media.addEventListener("seeked",this['seek2_'+id],false);
		*/
				
		
		try{
			keyCode = event.which || event.keyCode;
			shiftKey = event.shiftKey ; 
		}catch(e){
			keyCode = null ; 
			shiftKey = null ; 
		}
		// console.log("call key_controls");
		// console.log("event charcode : "+keyCode);
		
		// Le test permet de s'assurer que la fenetre contenant le player est bien la fenetre au premier plan pr l'utilisateur, 
		// et que le focus n'est pas sur un input text (on ne veut pas play/pause à chaque espace tapé dans un champ ...)
		if(this.playerHasFocus && document.hasFocus && !$j(document.activeElement).is('input[type="text"],textarea')){
			// spacebar => toggle play / pause 
			if(this.movie_options.kb_controls.spacebar_pause){
				if(keyCode == 32){
					event.preventDefault();
					this.ToggleVideo() ; 
					//console.log("spacebar play/pause, id: "+id);
				}
			}
			
			// flèches haut / bas  => volume up / down par cran de 10%
			if(this.movie_options.kb_controls.arrows_volume){
				if(keyCode == 38){
					event.preventDefault();
					this.media.volume = Math.min(this.media.volume+0.1,1);
					// console.log(" VOLUME GOES UP , id: "+id);
				}
				if(keyCode == 40){
					event.preventDefault();
					this.media.volume = Math.max(this.media.volume-0.1,0);
					// console.log(" VOLUME GOES DOWN , id: "+id);
				}
			}
			// flèches gauche & droite => recule / avance de 1 frame | +SHIFT : recule / avance de 5 sec
			if(this.movie_options.kb_controls.arrows_time 
			&& ( keyCode==37 || keyCode == 39 )){
				event.preventDefault();
				if(this.movie_options.kb_controls.arrows_time_shift && keyCode == 37 && shiftKey){
					// console.log(" BACK 5sec, id: "+id);
					this.media.currentTime -= 5;
				}else if(keyCode == 37){
					// console.log(" BACK 1frame, id: "+id);
					this.media.currentTime -= this.movie_options.frameTimeValue;
				}
				if(this.movie_options.kb_controls.arrows_time_shift && keyCode == 39 && shiftKey){
					// console.log(" FORWARD 5sec, id: "+id);
					this.media.currentTime += 5;
				}else if(keyCode == 39){
					// console.log(" FORWARD 1frame, id: "+id);
					this.media.currentTime += this.movie_options.frameTimeValue;
				}
				// si on a un peaks initialisé, c'est qu'on a un lecteur avec affichage audiowaveform, 
				// celui ci necessite l'émission de l'event user_seek pour maintenir l'ensemble de ses composants à jour par rapport à la position courante de la vidéo 
				if(typeof this.peaks != 'undefined'){
					this.peaks.emit('user_seek', this.media.currentTime );
				}
			}				
		}
	}.bind(this);
	document.addEventListener('keydown',key_controls,false);
	
	// Fonction de traitement de la scrollwheel sur le média
	try{
		wheel_control = function(evt){
			if(this.playerHasFocus && document.hasFocus && !$j(document.activeElement).is('input[type="text"],textarea')){
				var _delta = evt.wheelDelta || -evt.detail || evt.originalEvent.wheelDelta || -evt.originalEvent.detail;
				evt.preventDefault();
				evt.stopPropagation();
			
				if(_delta>0){
					this.media.currentTime -= 1;
				}else if (_delta<0){
					this.media.currentTime += 1;
				}
				
				// console.log("_delta "+_delta);
			}
			return false;
		}.bind(this);
	
		if(typeof $j(this.media).on != 'undefined'){
			$j(this.media).on('mousewheel DOMMouseScroll',wheel_control.bind(this));	
		}else{
			$j(this.media).bind('mousewheel DOMMouseScroll',wheel_control.bind(this));	
		}
	}catch(e){
		console.log("OWH_player - controlwheel - crash init mousewheel handler",e);
	}
	
}

OWH_player.prototype.initRatioSelect= function(){
	select_force_ratio_container = $j(this.container).find("#select_force_ratio_container") ; 
	select_force_ratio = $j(this.container).find("#select_force_ratio") ; 
	if( this.movie_options.activateRatioSelect == true 
		&& select_force_ratio.length > 0 
		&& this.mediaType == 'video'
		&& typeof this.container.style.objectFit != 'undefined'){ // On ajoute un contrôle sur l'existence de la règle CSS object-fit sur un objet de la page (quel qu'il soit, ici on test sur la div capsulevideo meme si on la regle sera appliquée sur l'élément vidéo direct), cette règle est la condition sine qua non de cette feature.
		select_force_ratio_container.addClass('active');
		select_force_ratio.on('change',function(evt){
			elt = evt.target ; 
			if($j(elt).val() == 'auto'){
				this.movie_options.force_ratio_ref = null ;
				$j(this.capsulevideo).removeClass('video_fill');
			}else{ 
				$j(this.capsulevideo).addClass('video_fill');
				if ($j(elt).val() == '16_9'){
					this.movie_options.force_ratio_ref = 16/9 ;
				}else if ($j(elt).val() == '4_3'){
					this.movie_options.force_ratio_ref = 4/3 ;
				}
			}
			this.resizeVideoKeepRatio(); 
			if (this.movie_options.infos.visu_type.match(new RegExp(/(stream_)/)) == null){
				this.adjustTimeline();
			}
		}.bind(this));
	}else if(select_force_ratio_container.length>0){
		select_force_ratio_container.css('display','none');
		select_force_ratio.css('display','none');
	}else if (select_force_ratio.length>0){
		select_force_ratio.css('display','none');			
	}
}

OWH_player.prototype.makeMergedMediaSubtitleSelector= function(){
	options = [];
	if( Object.prototype.toString.call(this.movie_options.urls) === '[object Array]' && this.movie_options.urls.length > 0) {
		// génération de l'objet contenant les diverses options possibles (ici combinaisons des options media & des options sous titres)
		for (key in this.movie_options.quality_names) {
			if (this.movie_options.quality_names.hasOwnProperty(key) && this.movie_options.urls.hasOwnProperty(key)){
				if (typeof str_lang !="undefined" && typeof str_lang[this.movie_options.quality_names[key]]=="string"){
					quality_label = str_lang[this.movie_options.quality_names[key]];
				} else {
					quality_label = this.movie_options.quality_names[key];
				}
				
				options.push({
					label : quality_label,
					url : this.movie_options.urls[key],
					typeMime : this.movie_options.typesMime[key],
					label_st : null ,
					url_st : null
				});
				if(typeof this.subtitle_tracks != 'undefined'){
					for (i = 0 ; i < this.subtitle_tracks.length ; i++){
						if(typeof this.subtitle_tracks[i].label != 'undefined'){
							label_st = this.subtitle_tracks[i].label ; 
						}else{
							label_st = this.subtitle_tracks[i].srclang ; 
						}
						srclang_st = this.subtitle_tracks[i].srclang ; 
						url_st = this.subtitle_tracks[i].src ; 
						
						options.push({
							label : quality_label+" ST "+label_st,
							url : this.movie_options.urls[key],
							typeMime : this.movie_options.typesMime[key],
							'label_st' : label_st,
							'url_st' : url_st,
							'srclang_st' : srclang_st
						});
						
					}
				}
			}
		}
	}else if (this.movie_options.url != '') {
		options.push({
			label : "Sans",
			url : this.movie_options.url,
			typeMime : this.movie_options["typeMime"],
			label_st : null ,
			url_st : null
		});
		if(typeof this.subtitle_tracks != 'undefined'){
			for (i = 0 ; i < this.subtitle_tracks.length ; i++){
				if(typeof this.subtitle_tracks[i].label != 'undefined'){
					label_st = this.subtitle_tracks[i].label ; 
				}else{
					label_st = this.subtitle_tracks[i].srclang ; 
				}
				srclang_st = this.subtitle_tracks[i].srclang ; 
				url_st = this.subtitle_tracks[i].src ; 
				
				options.push({
					label : "ST "+label_st,
					url : this.movie_options.url,
					typeMime : this.movie_options["typeMime"],
					'label_st' : label_st,
					'url_st' : url_st,
					'srclang_st' : srclang_st
				});
				
			}
		}
	}
	this.merged_media_st_options = options ; 
	// console.log(this.merged_media_st_options );
	
	if(this.merged_media_st_options.length >1){
		
		// fonction de switch entre deux configurations
		switch_media_st = function(elt){
			idx = $j(elt).attr('data-index');
			load_opts = this.merged_media_st_options[idx];
			// console.log("load_opts");
			// console.log(load_opts);
			$j('.subtitles-menu.merged_media_st .subtitles-button').attr('data-state','inactive');
			$j(elt).attr('data-state','active');
			if(this.media.textTracks.length>0){
				for (j = 0 ; j< this.media.textTracks.length  ; j++){
					if(load_opts['url_st'] != null && this.media.textTracks[j].language == load_opts['srclang_st']){
						this.media.textTracks[j].mode = "showing";
					}else{
						this.media.textTracks[j].mode = "hidden";							
					}
				}
			}
			//console.log(this.media.src  +" == "+ load_opts['url']+"? "+(this.media.url != load_opts['url']));
			if(this.media.src != load_opts['url']){
				this.SwitchMovie(load_opts['url'], load_opts['typeMime']);
			}
		}.bind(this);
		
		
		// generation du menu, sur le modele du menu sous titres existant 
		var subtitlesMenu = document.createElement('ul');
		subtitlesMenu.className = 'subtitles-menu merged_media_st';
		for (i = 0 ; i < this.merged_media_st_options.length; i ++ ){
			listItem = document.createElement('li');
			button_media_st = document.createElement('button');
			button_media_st.className = 'subtitles-button';
			if(i == 0){
				button_media_st.setAttribute('data-state', 'active');
			}else{
				button_media_st.setAttribute('data-state', 'inactive');
			}
			button_media_st.setAttribute('data-index', i);
			
			$j(button_media_st).text(this.merged_media_st_options[i].label);
			$j(button_media_st).click(function(evt){
				return switch_media_st(evt.currentTarget)
			});
			listItem.appendChild(button_media_st);
			subtitlesMenu.appendChild(listItem);
		}
		ow_bouton_srt = $j("#ow_bouton_srt").get(0);
		ow_bouton_srt.appendChild(subtitlesMenu);
		ow_bouton_srt.title = "Version";
		$j(ow_bouton_srt).show();
		$j(ow_bouton_srt).click(function(e){
			$j(e.currentTarget).find('ul.subtitles-menu').toggle();
			
		});
	}
		
		
		
		/*
		// VERSION SELECTEUR NATIF - DEPRECATED
		
		code = '<select class="ow_merged_media_st_options">';
		for (i = 0 ; i < this.merged_media_st_options.length; i ++ ){
			code+='<option value="'+i+'">'+this.merged_media_st_options[i].label+'</option>';
		}
		code+="</select>";
		
		// console.log("merged_media_st_opts : ");
		// console.log(this.merged_media_st_options);
		$j('#ow_quality_switch').html(code);
		$j('#ow_quality_switch').show();
		$j("#ow_quality_switch select.ow_merged_media_st_options").change(function(evt){
			load_opts = this.merged_media_st_options[$j(evt.currentTarget).val()];
			if(this.media.textTracks.length>0){
				for (j = 0 ; j< this.media.textTracks.length  ; j++){
					if(load_opts['url_st'] != null && this.media.textTracks[j].language == load_opts['srclang_st']){
						this.media.textTracks[j].mode = "showing";
					}else{
						this.media.textTracks[j].mode = "hidden";							
					}
				}
			}
			
			this.SwitchMovie(load_opts['url'], load_opts['typeMime']);
		}.bind(this));*/
}

OWH_player.prototype.initSubtitleMenu = function(){
	//afficher le mnu de sous titres avec action si la balise track est présente
	//on travaille avec une copie de l'objet
	// var copieMedia=this.media;

	if(this.playlist){
		max = 0 ; 
		max_tracks = 0 ; 
		for(i= 0 ; i < this.track_media.length ; i++){
			if($j(this.track_media[i]).find("track[kind='subtitles']").length > max_tracks ) {
				max = i ; 
				max_tracks = $j(this.track_media[i]).find("track[kind='subtitles']").length ;
			}
		}
		media_ref_subtitle = this.track_media[max];
	}else{
		media_ref_subtitle = this.media ; 
	}

	//menu pour sous titre avec action
	var subtitles = document.getElementById('ow_bouton_srt');
	// Turn off all subtitles
	if (this.movie_options.merge_media_subtitles_selectors == false && typeof media_ref_subtitle.textTracks !='undefined' && subtitles != null  ) {		
		for (var i = 0; i < media_ref_subtitle.textTracks.length; i++) {
			media_ref_subtitle.textTracks[i].mode = 'hidden';
		}

		init_menu = function() {
			var subtitleMenuButtons = [];
			var createMenuItem = function(id, lang, label) {
				var listItem = document.createElement('li');
				var button = listItem.appendChild(document.createElement('button'));
				button.setAttribute('id', id);
				button.className = 'subtitles-button';
				if (lang.length > 0) button.setAttribute('lang', lang);
				button.value = label;
				button.setAttribute('data-state', 'inactive');
				button.appendChild(document.createTextNode(label));
				button.addEventListener('click', function(e) {
					subtitleMenuButtons.map(function(v, i, a) {
					subtitleMenuButtons[i].setAttribute('data-state', 'inactive');
					});
					if(typeof e.srcElement != 'undefined'){
						current_btn = e.srcElement ; 
					}else if(typeof e.target != 'undefined'){
						current_btn = e.target ; 
					}
					var lang = current_btn.getAttribute('lang');
					if(this.playlist){
						for (idx_media = 0 ; idx_media < this.track_media.length ; idx_media++){
							for (var i = 0; i < this.track_media[idx_media].textTracks.length; i++) {
								if(typeof this.track_media[idx_media].textTracks[i] != 'undefined' && this.track_media[idx_media].textTracks[i] != null){
									if (this.track_media[idx_media].textTracks[i].language == lang) {
										this.track_media[idx_media].textTracks[i].mode = 'showing';
										subtitleMenuButtons[i].setAttribute('data-state', 'active');
									}else{
										this.track_media[idx_media].textTracks[i].mode = 'hidden';
									}
								}
							}
						}
					}else{
						for (var i = 0; i < media_ref_subtitle.textTracks.length; i++) {
							if (media_ref_subtitle.textTracks[i].language == lang) {
								media_ref_subtitle.textTracks[i].mode = 'showing';
								subtitleMenuButtons[i].setAttribute('data-state', 'active');
							}else{
								media_ref_subtitle.textTracks[i].mode = 'hidden';
							}
						}
					}
				}.bind(this));
				subtitleMenuButtons.push(button);
				return listItem;
			}.bind(this)

			var subtitlesMenu;
			if (media_ref_subtitle.textTracks) {
				var df = document.createDocumentFragment();
				var subtitlesMenu = df.appendChild(document.createElement('ul'));
				subtitlesMenu.className = 'subtitles-menu';
				//console.log(media_ref_subtitle.textTracks);
				for (var i = 0; i < media_ref_subtitle.textTracks.length; i++) {
					//console.log("menu html");
					subtitlesMenu.appendChild(createMenuItem('subtitles-' + media_ref_subtitle.textTracks[i].language, media_ref_subtitle.textTracks[i].language, media_ref_subtitle.textTracks[i].label));
				}
				subtitlesMenu.appendChild(createMenuItem('subtitles-off', '', 'Off'));
				ow_bouton_srt.appendChild(subtitlesMenu);
			}
			subtitles.addEventListener('click', function(e) {
				if (subtitlesMenu) {
					subtitlesMenu.style.display = (subtitlesMenu.style.display == 'block' ? 'none' : 'block');
				}
			});

			// on recopie tout dans l'objet media
			// this.media=copieMedia;
		}.bind(this);

		if(this.playlist){
			init_menu() ; 
		}else{
			// Attendre que les metadata soit entierement chargé avant de continuer .. ( bug firefox )
			media_ref_subtitle.addEventListener('loadeddata',init_menu , false);
		}
	}else{
		if(document.getElementById('ow_bouton_srt') != null){
			document.getElementById('ow_bouton_srt').style.display='none';
		}
	}
	if(this.movie_options.merge_media_subtitles_selectors == true){
		this.makeMergedMediaSubtitleSelector();
		
	}
}


OWH_player.prototype.parseSequences = function(){
	if(this.movie_options['xml_sequences']){
		this.sequences = new Array();
		if(this.movie_options.diapo_type_seq != null){
			this.diapos = new Array();
		}
		if (typeof(DOMParser)!='undefined')
		{
			parser=new DOMParser();
			xmlseq=parser.parseFromString(this.movie_options["xml_sequences"],"text/xml");
		}
		else
		{
			xmlseq = new ActiveXObject("Microsoft.XMLDOM");
			xmlseq.async=false;
			xmlseq.loadXML(this.movie_options["xml_sequences"]);
		}
		if (xmlseq.responseXML==null)
			elements_seq=xmlseq.getElementsByTagName('seq');
		else
			elements_seq=xmlseq.responseXML.getElementsByTagName('seq');

		// console.log(elements_seq );
		for(i=0;i<elements_seq.length;i++)
		{
			if (elements_seq[i].getElementsByTagName('type_seq')[0]!=null){
				type_seq = elements_seq[i].getElementsByTagName('type_seq')[0].textContent; 
			}else{
				type_seq = null ; 
			}
			
			if (elements_seq[i].getElementsByTagName('tcin')[0]!=null){
				tcin = elements_seq[i].getElementsByTagName('tcin')[0].textContent; 
			}else{
				tcin = null ; 
			}
			
			if (elements_seq[i].getElementsByTagName('tcout')[0]!=null){
				tcout = elements_seq[i].getElementsByTagName('tcout')[0].textContent; 
			}else{
				tcout = null ; 
			}
			
			if (elements_seq[i].getElementsByTagName('title')[0]!=null){
				title = elements_seq[i].getElementsByTagName('title')[0].textContent;
			}else{
				title = null ; 
			}
			if (elements_seq[i].getElementsByTagName('img')[0]!=null){
				img = elements_seq[i].getElementsByTagName('img')[0].textContent;
			}else{
				img = null ; 
			}
			
			item = {
					'type_seq': type_seq ,
					'tcin': tcin ,
					'tcout': tcout ,
					'title': title ,
					'img' : img,
					'tcin_offset' : (tcin != null)?(this.timecodeToSecs(tcin) - this.timecodeToSecs(this.movie_options.timecode_lag)):null,
					'tcout_offset' : (tcout != null)?(this.timecodeToSecs(tcout)  - this.timecodeToSecs(this.movie_options.timecode_lag)):null
			} ;
			//console.log(this.movie_options.diapo_type_seq,this.movie_options.chapter_type_seq);
			// Si diapo_type_seq est défini, et que l'item a le type_seq correspondant, alors on l'ajoute à la liste des diapos qui sont traitées séparément
			if(this.movie_options.diapo_type_seq != null 
			&& item['type_seq'] == this.movie_options.diapo_type_seq){
				this.diapos.push(item);
			}
			
			// si chapter_type_seq 'nest pas défini, ou qu'il est défini et que le type_seq de l'item correspond, alors on ajoute la séquence à la liste des séquences à afficher dans la timeline 
			if(this.movie_options.chapter_type_seq == null 
			|| item['type_seq'] == this.movie_options.chapter_type_seq){
				this.sequences.push(item);
			}
		}
	}else{
		$j("#ow_bouton_prev_seq, #ow_bouton_next_seq").addClass('disabled');
	}
}
OWH_player.prototype.buildRichMedia = function(){
	this.rich_media_flag = true ; 
	$j(this.container).addClass('rich_media');
	
	this.rich_media_menu = $j('<div id="ow_rich_media_menu" class="hidden">'
	+ '<div id="ow_rich_media_menu_header"></div>'
	+ '<div id="ow_rich_media_menu_main"></div>'
	+ '</div>');
	$j(this.container).append(this.rich_media_menu);
	
	ref_btn_to_add = null ; 
	if($j(this.container).find("#ow_boutons_right").length > 0 ){
		ref_btn_to_add = $j(this.container).find("#ow_boutons_right");
	}else if ($j(this.container).find("#ow_first_row_controller").length > 0 ){
		ref_btn_to_add = $j(this.container).find("#ow_first_row_controller");
	}
	
	
	if(this.sequences){
		// ajout onglet chapitre au menu
		$j("#ow_rich_media_menu_header").append('<div class="rich_media_mode_select" data-mode="chapitres">Chapitres</div>');
		// ajout zone selection chapitres
		chapitres_html = '<div class="rich_media_mode" data-mode="chapitres">';
		chapitres_html += '<ul id="rich_media_chapitres">';
		for (i = 0 ; i < this.sequences.length ; i ++){
			chapitres_html += '<li class="rich_media_chapitre rich_media_jump" data-tcin="'+this.sequences[i]['tcin']+'">'+this.sequences[i]['tcin']+' - '+this.sequences[i]['title']+'</li>';
		}
		chapitres_html += '</ul>';
		$j("#ow_rich_media_menu_main").append(chapitres_html);
	}
	
	
	if(this.diapos){
		// build wrapper diapos
		this.diapos_wrapper= document.createElement('div');
		this.diapos_wrapper.id = "ow_diapos_wrapper";

		this.diapo_img= document.createElement('img');
		this.diapo_img.id = "ow_diapo";
		this.diapo_img.className = "no_source";
		
		this.diapos_wrapper.appendChild(this.diapo_img);
		this.capsulevideo.appendChild(this.diapos_wrapper);
		
		// ajout au menu
		$j("#ow_rich_media_menu_header").append('<div class="rich_media_mode_select" data-mode="diapos">Diapositives</div>');
		// ajout zone selection diapos
		diapos_html = '<div class="rich_media_mode" data-mode="diapos">';
		diapos_html += '<ul id="rich_media_diapos">';
		for (i = 0 ; i < this.diapos.length ; i ++){
			diapos_html += '<li class="rich_media_diapo rich_media_jump" data-tcin="'+this.diapos[i]['tcin']+'">'
			+ '<img src="makeVignette.php?image='+this.diapos[i]['img']+'&kr=1&h=60"/>'
			+this.diapos[i]['tcin']+' - '+this.diapos[i]['title']+'</li>';
		}
		diapos_html += '</ul>';
		$j("#ow_rich_media_menu_main").append(diapos_html);
	}
	
	// build selecteur de ratio des diapos vs média
	if(this.diapos && this.sequences){
		$j(ref_btn_to_add).append(
			'<select id="ow_rich_media_selector">'
		+	'<option value="videoonly" >video seule</option>'
		+	'<option value="videomode">video agrandie</option>'
		+	'<option selected value="">video et diapo</option>'
		+	'<option value="diapomode">diapo agrandie</option>'
		+	'<option value="diapoonly">diapo seule</option>'
		+'</select>');
		$j("#ow_rich_media_selector").change(function(elt){
			$j(this.container).removeClass('videoonly videomode diapomode diapoonly');
			$j(this.container).addClass($j("#ow_rich_media_selector").val());
		}.bind(this));
	}
	$j(ref_btn_to_add).append('<div id="ow_btn_toggle_rich_media_menu">Menu</div>');
	
	$j("#ow_btn_toggle_rich_media_menu").click(function(){
		this.toggleRichMediaMenu();
	}.bind(this));
	$j("#ow_rich_media_menu .rich_media_jump").click(function(evt){
		this.GoToLongTimeCode($j(evt.currentTarget).attr('data-tcin'));
	}.bind(this))
	$j("#ow_rich_media_menu .rich_media_mode_select").click(function(evt){
		$j("#ow_rich_media_menu .rich_media_mode_select, #ow_rich_media_menu .rich_media_mode").removeClass('active');
		$j(evt.currentTarget).addClass('active');
		$j("#ow_rich_media_menu .rich_media_mode[data-mode='"+$j(evt.currentTarget).attr('data-mode')+"']").addClass('active');
	}.bind(this));
	// initialisation premier onglet
	$j("#ow_rich_media_menu .rich_media_mode_select").eq(0).click() ; 
	
}

OWH_player.prototype.toggleRichMediaMenu = function(){
	$j("#ow_rich_media_menu").toggleClass('hidden')
}
OWH_player.prototype.initSequencesWrapper = function(){
	if(typeof this.sequences_wrapper != 'undefined'){
		for (i = 0 ; i < this.sequences.length ; i ++){
			seq = document.createElement('span');
			seq.className='ow_seq';
			seq.id='ow_seq_'+i;

			if(this.movie_options.show_seq_nums){
				seq_span = document.createElement('span');
				seq_span.className='ow_seq_span';
				seq_span.id='ow_seq_span_'+i;

				seq.appendChild(seq_span);	
			}
			
			seq_span_timeline = document.createElement('span');
			seq_span_timeline.className='ow_seq_timeline';
			seq_span_timeline.id='ow_seq_timeline_'+i;
			
			seq.appendChild(seq_span_timeline);	
			
			if(this.movie_options.show_seq_nums){
				seq_num = document.createElement('span');
				seq_num.className='ow_seq_num';
				$j(seq_num).attr("data-seq_num",i);
				$j(seq_num).html(i + 1 );
				seq_num.id='ow_seq_num_'+i;
				$j(seq_num).click({param_idx : i},function(evt){
					this.GoToLongTimeCode(this.sequences[evt.data.param_idx]['tcin']);
				}.bind(this));
				seq_span.appendChild(seq_num);
			}
			
			
			$j(seq).attr('data-tcin',this.sequences[i]['tcin']);
			$j(seq).attr('data-tcout',this.sequences[i]['tcout']);
			$j(seq).attr('data-title',this.sequences[i]['title']);
			$j(seq).attr('data-img',this.sequences[i]['img']);
			$j(seq).attr('data-type',this.sequences[i]['type_seq']);
			$j(this.sequences_wrapper).append(seq);
		}
		
		if(this.movie_options.show_seq_encadre){
			encadre_seq = document.createElement('div');
			encadre_seq.id = "ow_prevSeqContainer";
			encadre_seq.style.display = "none" ; 
			
			$j(encadre_seq).html('<div id="ow_img_prevSeqContainer"></div>'
			+'<div id="ow_lab_prevSeqContainer"></div>'
			+'<div id="ow_text_prevSeqContainer">'
			+'<div id="ow_title_prevSeqContainer"></div>'
			+'<div id="ow_truetitle_prevSeqContainer"></div>'
			+'<div id="ow_link_prevSeqContainer">Visionner la séquence</div>'
			+'</div>');
			this.sequences_wrapper.appendChild(encadre_seq);
			this.encadre_seq = encadre_seq ; 
		}
	}
}


OWH_player.prototype.drawSequences = function(){
	if(typeof this.sequences_wrapper != 'undefined'){
		handle_width = $j("#"+this.container.id).find('#progress_handle').width() ; 
		
		if(this.movie_options.show_seq_encadre){
			if((this.media.videoWidth/this.media.videoHeight)>1.5 ){
				this.encadre_seq.className="form_16_9";
			}else{
				this.encadre_seq.className="form_4_3";
			}
		}
		
		$j(this.sequences_wrapper).find('.ow_seq').each(function(idx,elt){
			try{
				tcin_offset = this.timeline.translateToPx(this.timecodeToSecs($j(elt).attr('data-tcin')) - this.timecodeToSecs(this.movie_options.timecode_lag));
				tcout_offset = this.timeline.translateToPx(this.timecodeToSecs($j(elt).attr('data-tcout'))  - this.timecodeToSecs(this.movie_options.timecode_lag) );
				seq_width = parseInt(tcout_offset,10) - parseInt(tcin_offset,10) + (handle_width / 2 );
				if (seq_width <= 10) seq_width = 11;
				$j(elt).css('left',tcin_offset );
				$j(elt).css('width',(seq_width)+"px");
				//console.log("")
			}catch(e){
				console.log(e);
			}
		}.bind(this));
	}
}

OWH_player.prototype.inSequenceTest = function(value,mode){
	current_pos = false ; 
	if(typeof mode == 'undefined'){
		mode = "sequences"
	}
	
	switch(mode){
		case "sequences" : 
			work_arr = this.sequences ;
		break ; 
		case "diapos" : 
			work_arr = this.diapos;
		break ; 
	}
	
	
	for(i = 0 ;  i < work_arr.length ; i ++ ) {
		calc_tcin = (work_arr[i]['tcin_offset'] -  work_arr[i]['tcin_offset']%0.04); 
		calc_tcout = (work_arr[i]['tcout_offset'] -  work_arr[i]['tcout_offset']%0.04) ; 
		if ( calc_tcin <= value && value < calc_tcout ){
			current_pos = ""+i ; 
		}
	}
	return current_pos ; 
}

OWH_player.prototype.showEncadreSeq = function (curr_seq,value,value_pic){

	if(typeof this.sequences[curr_seq] != 'undefined'){
		seq = this.sequences[curr_seq] ; 
		
		$j(".ow_seq").removeClass('current_seq_hover');
		$j("#ow_seq_"+curr_seq).addClass('current_seq_hover');
		
		if(this.movie_options.show_seq_encadre){
			center_seq = (parseInt($j("#ow_seq_"+curr_seq).css('left'),10) +  parseInt($j("#ow_seq_"+curr_seq).css('width'),10)/2) - parseInt($j("#ow_prevSeqContainer").width(),10)/2 - parseInt($j("#ow_prevSeqContainer").css('margin-left') , 10);
			
			center_seq = Math.max( - parseInt($j("#ow_prevSeqContainer").css('margin-left') , 10),center_seq);
			center_seq = Math.min(center_seq , parseInt($j("#ow_id_progress_slider").width(),10) - parseInt($j("#ow_prevSeqContainer").width(),10) - parseInt($j("#ow_prevSeqContainer").css('margin-left') , 10));
			
			$j(this.encadre_seq).css('left',center_seq+"px");
			
			if(this.movie_options.seq_use_seq_vignette){
				$j(this.encadre_seq).find("#ow_img_prevSeqContainer").css('background-image','url(makeVignette.php?image=/storyboard/'+seq['img']+')');
			
				if(this.movie_options.timecode_display_format =='hh:mm:ss:ff'){
					str_tc= seq['tcin'];
				}else{
					var str_tc ;
					tc = seq['tcin'].split(':');
					if(this.movie_options.timecode_display_format=='hh:mm:ss'){
						str_tc = tc[0]+':'+tc[1]+':'+tc[2];
					}else if(this.movie_options.timecode_display_format=='mm:ss'){
						m = (parseInt(tc[0])*60+parseInt(tc[1]));
						if(m<10){ m= '0'+m ; }
						str_tc = m+':'+tc[2];
					}
				}
				$j(this.encadre_seq).find("#ow_lab_prevSeqContainer").html(str_tc);

			}else{
				if(this.movie_options.mosaic_use_index){
					this.getPreviewPicFromIndex(value_pic,"ow_img_prevSeqContainer","ow_lab_prevSeqContainer");
				}else{
					this.getPreviewPic(value_pic,"ow_img_prevSeqContainer","ow_lab_prevSeqContainer");
				}
				
				if(!this.movie_options.tcMosaicExact){
					document.getElementById("ow_lab_prevSeqContainer").innerHTML = this.DisplayTimeCode(value);
				}
				
			}
			
			
			
			display_num = ( parseInt(curr_seq,10) + 1) ; 
			$j(this.encadre_seq).find("#ow_title_prevSeqContainer").html("Séquence n°"+display_num);
			$j(this.encadre_seq).find("#ow_truetitle_prevSeqContainer").html(seq['title']);
			$j(this.encadre_seq).find("#ow_link_prevSeqContainer").unbind('click');
			$j(this.encadre_seq).find("#ow_link_prevSeqContainer").click(function(){
				this.GoToLongTimeCode(seq['tcin']);
			}.bind(this));
			this.encadre_seq.style.display = "" ; 
			
			if(typeof this.hide_encadre_handler == 'undefined'){
				this.hide_encadre_handler = function(evt){
					not_to_seq = (typeof evt.toElement != 'undefined' && $j(evt.toElement).parents('.ow_seq').length == 0 && !$j(evt.toElement).is('.ow_seq') && !$j(evt.toElement).is('#ow_prevSeqContainer') && $j(evt.toElement).parents('#ow_prevSeqContainer').length ==  0  && !$j(evt.toElement).is('#progress_handle')) 
					|| (typeof evt.relatedTarget != 'undefined' && $j(evt.relatedTarget).parents('.ow_seq').length == 0 && !$j(evt.relatedTarget).is('.ow_seq') && !$j(evt.relatedTarget).is('#ow_prevSeqContainer') && $j(evt.relatedTarget).parents('#ow_prevSeqContainer').length ==  0 && !$j(evt.relatedTarget).is('#progress_handle'));
					// console.log("not_to_seq : "+not_to_seq);
					if(not_to_seq){
						// console.log("call hide_encadre_handler");
						this.hideEncadreSeq();
					}
					return false ;
				}.bind(this); 
			}
			
			
			$j('.ow_seq_span').css('pointer-events','auto');
			$j("#ow_prevSeqContainer, .ow_seq_span, #ow_id_progress_slider").unbind('mouseout',this.hide_encadre_handler);
			$j("#ow_prevSeqContainer, .ow_seq_span, #ow_id_progress_slider").bind('mouseout',this.hide_encadre_handler);
		}
	}
}

OWH_player.prototype.hideEncadreSeq = function (){
	if(typeof this.encadre_seq != 'undefined'){
		this.encadre_seq.style.display = "none" ; 
		$j('.ow_seq_span').css('pointer-events','none');
		$j(".ow_seq").removeClass('current_seq_hover');
	}
}



OWH_player.prototype.GoToClosestSeq = function(sens){
	if(typeof this.sequences == 'undefined'){
		return false ; 
	}
	if (typeof sens == 'undefined') {
		sens = 1 ; 
	}
	closest_seq = this.GetClosestSeq(sens);
	if(closest_seq != null && typeof this.sequences != 'undefined' ){
		this.GoToLongTimeCode(this.sequences[closest_seq]['tcin']);
	}
}

OWH_player.prototype.GetClosestSeq = function(sens){
	current_time = this.GetCurrentTime();
	delta_sec = null ; 
	curr_seq = null ; 
	
	if (typeof sens == 'undefined'){
		sens = 1 ; 
	}
	
	for(i = 0 ;  i < this.sequences.length ; i ++ ){
		tcin_offset = this.timecodeToSecs(this.sequences[i]['tcin']) - this.timecodeToSecs(this.movie_options.timecode_lag);
		if(sens == 1 ){
			if((delta_sec == null || (tcin_offset - current_time) < delta_sec ) && (tcin_offset - current_time)>0 ){
				delta_sec = tcin_offset - current_time ; 
				curr_seq = i ; 
			}
		}else if(sens == 0 ){
			// afin d'éviter de coincer sur le cas ou on réalise plusieurs "prev" pendant que la vidéo est en lecture, le code est légèrement modifié pour laisser une tolérance de 0.3 sec 
			//	c a d que si le tcin_offset est à moins de 0.5sec du temps courant, on ne considère pas la séquence aussi proche comme une option valable pour le prev. 		
			if((delta_sec == null || (current_time - tcin_offset) < delta_sec ) && (current_time - tcin_offset)>0.5 ){
				delta_sec = current_time - tcin_offset; 
				curr_seq = i ; 
			}
		}
	}
	return curr_seq ; 
}



OWH_player.prototype.initSimpleDurationTracker = function(){
	this.simple_duration_tracker = {
		time : 0,
		last_pos : 0,
		time_watched : 0,
		media_duration : 0
	}
	this.simple_duration_tracker.media_duration = this.timecodeToSecs(this.movie_options.timecode_OUT) - this.timecodeToSecs(this.movie_options.timecode_IN) ; 	
	
	if(this.movie_options.auto_send_tracked_duration){
		this.unload_func = function(){
			this.sendLogDurationVisio();
		}.bind(this);
		window.addEventListener("beforeunload",this.unload_func,false);
	}
	
}

OWH_player.prototype.sendLogDurationVisio= function(){
	$j.ajax({
		url : "empty.php?urlaction=logDurationV",
		method : 'POST' , 
		data : {
			'id' 	:	this.movie_options['infos']['id'],
			'visu_type' : 	this.movie_options['infos']['visu_type'],
			'time' : this.getSimpleDurationTrackerTime(), 
			'async'  : false
		}
		,
		complete : function(){
			console.log("onbeforeunload send DURV");
		}
	});
}


OWH_player.prototype.getSimpleDurationTrackerTime = function(){
	if (typeof this.simple_duration_tracker != 'undefined'){
		return Math.min(this.simple_duration_tracker.time_watched,this.simple_duration_tracker.media_duration);
	}
}


// HELP BOX 
OWH_player.prototype.makeHelpbox = function(){
	this.helpbox = document.createElement('div');
	this.helpbox.id = "ow_helpbox";
	this.helpbox.className = "hidden";
	this.helpbox.innerHTML = '<div class="ow_helpbox_title"></div>'
	+'<div class="ow_helpbox_close"></div>'
	+'<div class="ow_helpbox_text"></div>';
	$j(this.helpbox).find('.ow_helpbox_close').on('click',function(){
		this.clearHelpbox() ; 
		this.hideHelpbox() ; 
	}.bind(this));
	this.container.append(this.helpbox);
}

OWH_player.prototype.showHelpbox = function(){
	$j(this.helpbox).removeClass('hidden').addClass('visible');
}
OWH_player.prototype.hideHelpbox = function(){
	$j(this.helpbox).removeClass('visible').addClass('hidden');
}
OWH_player.prototype.clearHelpbox = function(){
	$j(this.helpbox).find('*').each(function(idx,elt){
		$j(elt).html('');
	});
}
OWH_player.prototype.openHelp = function(type){
	if(typeof this.helpbox == 'undefined'){
		this.makeHelpbox();
	}
	switch(type){
		case 'help_shortcuts': 
			$j(this.helpbox).find('.ow_helpbox_title').html(this.tooltips['ow_shortcuts_help']);
			html = "" ; 
			for ( key in this.movie_options.kb_controls){
				if(this.movie_options.kb_controls[key] == true && typeof this.tooltips[key] != 'undefined'){
					html+='<div class="kb_controls_help_tooltip">'+this.tooltips[key]+'</div>';
				}
			}
			$j(this.helpbox).find('.ow_helpbox_text').html(html);
			this.showHelpbox();
		break ; 
		default : 
		return false ; 
	}
	
}

// WAVEFORM 
OWH_player.prototype.initWaveForm = function(){
	$j(this.container).find("#ow_audiovignette").addClass('hidden');
	$j(this.container).find('#peaks-container').css('min-width','100%');
	
	// on complète les options waveform avec les variables indispensables au fonctionnement de waveform
	this.movie_options.waveform_peaks_opts.container = $j(this.container).find('#peaks-container').get(0);
	// console.log("test container",this.movie_options.waveform_peaks_opts.container);
	this.movie_options.waveform_peaks_opts.mediaElement = this.media;
	this.movie_options.waveform_peaks_opts.dataUri={
		arraybuffer : this.movie_options.waveform_path // URI to waveform data file in binary or JSON 
	};

	requirejs(['oraoweb/peaks/peaks'],function(Peaks){
		this.peaks = Peaks.init(this.movie_options.waveform_peaks_opts);
		if(this.peaks){
			try{
				waveform_mousewheel_handler = function(evt){
					var _delta = evt.wheelDelta || -evt.detail || evt.originalEvent.wheelDelta || -evt.originalEvent.detail;
					evt.preventDefault();
					evt.stopPropagation();
				
					if(_delta>0){
						this.peaks.zoom.zoomIn();
						// console.log("scrollwheel peaks zoomIn");
					}else if (_delta<0){
						this.peaks.zoom.zoomOut();
						// console.log("scrollwheel peaks zoomOut");
					}
					
					return false;
				}.bind(this);
			
				if(typeof $j(this.container).find('#peaks-container').on != 'undefined'){
					$j(this.container).find('#peaks-container').on('mousewheel DOMMouseScroll',waveform_mousewheel_handler.bind(this));	
				}else{
					$j(this.container).find('#peaks-container').bind('mousewheel DOMMouseScroll',waveform_mousewheel_handler.bind(this));	
				}
			}catch(e){
				console.log("OWH_player - crash init waveform_mousewheel_handler",e);
			}
		}
		}.bind(this)
	);
}