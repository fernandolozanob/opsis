<?php
require_once(libDir."class_cherche.php");


class RechercheFest extends Recherche {

	function __construct() {
	  	$this->entity='FEST';
     	$this->name=kFestival;
     	$this->prefix='F';
     	$this->sessVar='recherche_FEST';
		$this->tab_recherche=array();
		$this->useSession=true;
	}


    function prepareSQL(){
		global $db;
		$this->sql = "select F.*,DA.DA_FICHIER,DA.DA_CHEMIN from t_festival F
			left join t_doc_acc DA on (DA.ID_DOC_ACC=F.FEST_ID_IMAGE and DA.ID_LANG=".$db->Quote($_SESSION['langue']).")
			WHERE F.ID_LANG=".$db->Quote($_SESSION['langue']);
    }

    /** Recherche de personnes par libellé
     *  IN : tableau critère, préfixe
     *  OUT : sql + arrHighlight
     * 	Tableau de critère : structure
     * 		VALEUR : nom de la ou des personnes (plusieurs valeurs permises, la chaine sera analysée)
     * 		FIELD : type DESC (GEN, PRD, etc)
     * 		VALEUR2 (optionnel) : type de personne (PM ou PP)
     * 	NOTE : l'analyse de chaine permet d'extraire une ou plusieurs personnes (selon la présence
     *   de guillemets, ou de wildcards).
     * 	NOTE 2 : comme la recherche porte sur le nom ET le prénom, les deux bénéficient du highlight.
     *
     */
    protected function cherchePers($tabCrit,$prefix="", $intersect = false) {
    	global $db;
    	$prefix=($prefix!=""?$prefix.".":"");

		$arrWords=$this->analyzeString($tabCrit['VALEUR'],$op);
		foreach ($arrWords as $i=>$word) if (strlen($word)<=2) unset ($arrWords[$i]); //strip des mots <2 lettres qui empèchent le FT
		foreach ($arrWords as $idx=>$word) {
			$strVals.=" ".$this->FToperator."(".$word.") ";
		}

		  $arrHL=array(0=>@implode(' ',$arrWords)); //Tableau des noms (libellé)
		  //$sql2 = "select DISTINCT ID_PERS from t_personne where (PERS_NOM || ' ' || PERS_PRENOM) @@ (".$db->Quote($strVals).")";
		  $sql2 = "select DISTINCT ID_PERS from t_personne where ".$db->getFullTextSQL(array($strVals), array("PERS_NOM", "PERS_PRENOM"));
		  if (!empty($tabCrit['VALEUR2'])) $sql2.=" AND PERS_ID_TYPE_LEX=".$db->Quote($tabCrit['VALEUR2']);

		  $arrIDs=$db->GetAll($sql2);

		   if (!empty($arrIDs)) foreach ($arrIDs as $pers) $arrLex['ID'][]=$pers['ID_PERS'];  // Tableau des personnes (ID)
		   else $arrLex['ID']=array('-1');
       	$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."ID_FEST IN (select ID_FEST from t_doc_lex where ID_PERS ";

	   	$this->sqlRecherche.= $this->sql_op(@implode(",",$arrLex['ID']));
	   	if (!empty($tabCrit['FIELD'])) $this->sqlRecherche.= " AND ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD']);

		$this->sqlRecherche.=" ))";

	     $this->tabHighlight["pers_nom"]=array_values($arrHL);
	     $this->tabHighlight["pers_prenom"]=array_values($arrHL);

	     $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
    }

    protected function chercheLex($tabCrit,$prefix="",$extLexiquePre=true,$extLexiquePost=true){
        if(!empty($tabCrit['VALEUR'])) {
            global $db;
           	$prefix=($prefix!=""?$prefix.".":"");

			if ($extLexiquePre) { // Extension lexicale PRE : analyse de la chaine pour trouver les ID_LEX
		 		$arrLex=$this->extLexique($tabCrit);
			}


			if ($arrLex) { // Si extension demandée ET existe
					$strVals=$arrLex['VALEURS'];
					$arrHL=$arrLex['HIGHLIGHT'];

			}

			else { //Pas d'extension PRE, on se contente de retrouver les ID_LEX par fulltext
				   $strVals=str_replace(array(",","'"),array(" "," ' "),$tabCrit['VALEUR']);
				  //by LD 06/06/08 => ajout spaceQuote (bug cnrs pour recherche thésaurus avec quotes)
				  $arrHL=array(0=>$strVals);
		 		  $sql2 = "select DISTINCT ID_LEX from t_lexique where (LEX_TERME) against (".$db->Quote($strVals).")";

			  $arrIDs=$db->GetAll($sql2);
			  if (!empty($arrIDs)) foreach ($arrIDs as $lx) $arrLex['ID'][]=$lx['ID_LEX'];
			  else $arrLex['ID']=array('-1');

			}

			//Extension lexique Post Recherche : un ou plusieurs ID ont été trouvés, on les étend => comme dans chercheIdLex
			if ($extLexiquePost=='true') {
	     		$extHier=$this->extLexiqueHier($arrLex,true);
	    		if (!empty($extHier)) $arrLex['ID']=array_merge($arrLex['ID'],$extHier);

	    		$extSyno=$this->extLexiqueSyno($arrLex);
		 		if (!empty($extSyno)) $arrLex['ID']=array_merge($arrLex['ID'],$extSyno);
	 			$this->getTermesFromLexique($arrLex,true);
			}

			//debug($arrLex,"red");
           	$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."ID_FEST in (select ID_FEST from t_doc_lex where ID_LEX ";
		   	$this->sqlRecherche.= $this->sql_op(implode(",",$arrLex['ID']));


            if($tabCrit['VALEUR2']){ // Un rôle est spécifié
                    $this->sqlRecherche.= " AND ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD'])." AND DLEX_ID_ROLE=".$db->Quote($tabCrit['VALEUR2']);
                    if (empty($tabCrit['LIB']))
                    	$tabCrit['LIB']=$db->GetOne("select ROLE from t_role where ID_ROLE=".$db->Quote($tabCrit['VALEUR2'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
            } else {
                    $this->sqlRecherche.= " AND ID_TYPE_DESC='".$tabCrit['FIELD']."' ";
                    if (empty($tabCrit['LIB']))
                    	$tabCrit['LIB']=$db->GetOne("select TYPE_DESC from t_type_desc where ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
            }


            $this->sqlRecherche.="))";



        	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight["LEX_TERME"][]=array_values($arrHL);

            $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
        }
    }

}
?>
