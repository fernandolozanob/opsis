<?php
require_once(libDir."class_chercheDoc.php");

class RechercheSolr extends RechercheDoc
{
	public $solr_query;
	public $str_recherche;

	private $champs_affichage;
	private $champs_highlight;

	private $database;

	function __construct()
	{
		// error_reporting(E_ALL);
		$this->entity='DOC';
		$this->name=kDocument;
		$this->prefix=null;
		$this->sessVar='recherche_DOC_Solr';
		$this->tab_recherche=array();
		$this->useSession=true;
		$this->champs_noHls = array();
		$this->sqlRecherche='';
		$this->str_recherche='';
		$this->str_recherche_fq='';
		$this->etape='';
		$this->parsed_etape=null;

		if (defined("gParamsRechercheDoc"))
			$this->params=unserialize(gParamsRechercheDoc);

		if ($this->params['insideFieldBooleanOp']){
			$this->insideFieldBooleanOp = $this->params['insideFieldBooleanOp'];
		}else{
			$this->insideFieldBooleanOp = 'OR';
		}
		$this->solr_query=new SolrQuery();
		$this->solr_query->setHighlight(true);
		$this->solr_query->setHighlightSimplePre('<highlight>');
		$this->solr_query->setHighlightSimplePost('</highlight>');
		$this->solr_query->setHighlightRequireFieldMatch(FALSE);
		$this->solr_query->setHighlightHighlightMultiTerm(TRUE);


		$this->champs_highlight=array();
		if (defined('gOverwriteChampsRechercheSolr') && gOverwriteChampsRechercheSolr != '' ){
			$this->champs_affichage = array() ; 
			$arr_champs = explode(',',gOverwriteChampsRechercheSolr);
			foreach ($arr_champs as $champ)
				$this->champs_affichage[]=trim($champ);
		}else{
			$this->champs_affichage=array('id','id_doc','id_lang','im_chemin','im_fichier','da_chemin','da_fichier','doc_titre','doc_id_media','doc_cote','doc_num','doc_date_prod','doc_date_pv_debut','doc_date_diff','doc_id_fonds','fonds','fonds_parent','type_doc','doc_duree','doc_id_fonds','mat_type','mat_nb_pages','mat_width','mat_height','cnt_doc_lies_src','cnt_doc_lies_dst','doc_acces');
		}
		//champs a ajouter a la recherche
		if (defined('gAjoutChampsRechercheSolr') && gAjoutChampsRechercheSolr != '' )
		{
			$arr_champs=explode(',',gAjoutChampsRechercheSolr);
			foreach ($arr_champs as $champ)
				$this->champs_affichage[]=$champ;

			$this->champs_affichage = array_unique($this->champs_affichage);
		}

	}

	public function prepareSQL($most_viewed=false)
	{
		$usr=User::getInstance();
		$this->str_recherche='';
		$this->str_filter_query='';

		$this->initArrFieldsToSaveWithIdLang();


		// si la quantite de donnees presentes dans un des champs (doc_transcript par exemple) on atteint la limite de memoire de php
		foreach($this->champs_affichage as $field){
			if(in_array($field,$this->arrFieldsToSaveWithIdLang) && $field!='doc_acces'){
				$field = $field.'_'.strtolower($_SESSION['langue']);
			}
			$this->solr_query->addField($field);
		}

		// MS 06.05.14 report règle doc_acces=1 si usager <= kLoggedNorm (en théorie devrait etre doc_acces>=1 mais l'ajout d'un range [1 TO *] semble etre interprété par le highlight comme une date ... à résoudre)
		if((defined("gSrchDoc_UserTypeWithoutFullAccess") && $usr->getTypeLog() <= gSrchDoc_UserTypeWithoutFullAccess) || (!defined("gSrchDoc_UserTypeWithoutFullAccess") && $usr->getTypeLog() <= kLoggedNorm)){
			$this->str_recherche.=" doc_acces:1 ";
		}

		//$this->solr_query->addParam('q.op','AND');
        if(defined("gSolrDefType") && gSolrDefType=="synonym_edismax"){
            $this->solr_query->addParam("defType","synonym_edismax");
            $this->solr_query->addParam("synonyms","true");
        }

		// LE PROBLEME EST QUE PREPARESQL NEST PAS APPELE A CHAQUE FOIS, IL FAUT DONC STOCKER EN SESSION / RECUPERER DEPUIS LA SESSION 
		if(!isset($this->solr_facet_file)){
			$this->solr_facet_file = "/xml/solr_facet.xml" ; 
		}
		
		if(is_file(getSiteFile("formDir",$this->solr_facet_file)) && basename(getSiteFile("formDir",$this->solr_facet_file))!='index.php'){
			$this->solr_facet_file_fullpath = getSiteFile("formDir",$this->solr_facet_file) ; 
			$xml_facets = xml2tab(file_get_contents($this->solr_facet_file_fullpath));

			if(isset($xml_facets["FACETS"][0]['FACET']) && !empty($xml_facets["FACETS"][0]['FACET'])){
				foreach($xml_facets["FACETS"][0]['FACET'] as $idx => $facet_arr){
					$this->addFacet($facet_arr);
				}
			}
		}
	}
	
	// MS 01.04.16 - Apparement n'est plus utilisé pour l'instant (on passe direct via solr_query->addSortField dans docListe.php (le peu de traitement de cette fonction est déjà fait par getSort)
	function ajouterTri($champ,$ordre='')
	{
		if (!empty($ordre))
		{
			if ($ordre=='DESC')
				$query_order=SolrQuery::ORDER_DESC;
			else
				$query_order=SolrQuery::ORDER_ASC;
		}
		else
			$query_order=SolrQuery::ORDER_ASC;

		$champ = $this->checkFieldVersionDependant($champ);
		$this->solr_query->addSortField($champ,$query_order);
	}

	function analyzeFields ($myFields,$myVals,$myTypes,$myOps=array(),$myLibs=array(),$myVals2=array(),$myNOHL=array(),$myNOHist=array(),$myOptions=array())
	{

		if (!empty($myFields))
		{
			foreach ($myFields as $idx=>$field)
			{
				if (($field!='' && $myVals[$idx]!='' && $myTypes[$idx]!='') || $myTypes[$idx]=='FACET' || $myTypes[$idx]=='FACET_DATE' || $myTypes[$idx]=='FACET_MULTI')
				{
					if (empty($myOps[$idx]))
						$myOps[$idx]="AND";

					if (empty($myLibs[$idx]))
						$myLibs[$idx]=kCritere;
					//si plusieurs valeurs en tableau, on les concatène (cas des checkbox)
					//if (is_array($myVals[$idx])) $myVals[$idx]=implode(',',$myVals[$idx]);

					// les obligatoires
                    // VP 17/9/14 : passage de FIELD en minuscule
					$this->tab_recherche[$idx]['FIELD']=strtolower($field);
					$this->tab_recherche[$idx]['VALEUR']=$this->removeWeirdCharacters($myVals[$idx]);
					$this->tab_recherche[$idx]['OP']=$myOps[$idx];
					$this->tab_recherche[$idx]['TYPE']=$myTypes[$idx];
					$this->tab_recherche[$idx]['LIB']=$myLibs[$idx];

					// les optionnels
					if (isset($myVals2[$idx]))
						$this->tab_recherche[$idx]['VALEUR2']=$myVals2[$idx];

					if (isset($myNOHL[$idx]))
						$this->tab_recherche[$idx]['NOHIGHLIGHT']=$myNOHL[$idx];

					if (isset($myNOHist[$idx]))
						$this->tab_recherche[$idx]['NOHIST']=$myNOHist[$idx];

					if (isset($myOptions[$idx]))
						$this->tab_recherche[$idx]['OPTIONS']=$myOptions[$idx];
				}
			}
		}
		
		$this->makeSQL(); //Lancement du moteur
     }

	public function ChooseType($tabCrit,$prefix)
	{
		$suffixe="";
		if(isset($tabCrit['VALEUR']) && is_string($tabCrit['VALEUR'])){
			$tabCrit['VALEUR'] = stripUnmatchedParentheses($tabCrit['VALEUR']);
		}

		//VG : il FAUT garder la compatibilité avec les anciens opérateurs logiques afin de faciliter les mises à niveau
		//COMPATIBLITE ANCIENNES VERSIONS
		if ($tabCrit['OP']=='AND NOT') {
			$tabCrit['OP'] = 'NOT';
		}


        if(is_array($tabCrit['VALEUR'])) $_valeurs=implode(', ',$tabCrit['VALEUR']);
        else $_valeurs=$tabCrit['VALEUR'];
		$arr_field=$this->analyzeString2($_valeurs,$arr_operators);

        if($tabCrit['TYPE']=='CFQ') {
			// dans l'implémentation courante, rien à faire ici, tout est traité dans chercheChampFQ, laissé tel quel pr éviter d'avoir à le filtrer sur les autres cas
        }  else if (!empty($this->str_recherche) && $tabCrit['TYPE']=='FT' && !empty($arr_field)) {
			if ($tabCrit['OP']=='OR')
				$this->str_recherche.=' OR '; // OR
			else if ($tabCrit['OP']=='NOT'){
				$this->str_recherche.=' AND (*:* NOT '; // NOT
				$suffixe=' ) ';
				}
			else
				$this->str_recherche.=' AND '; // AND
		}
		else if ($tabCrit['TYPE']!='FT' && isset($tabCrit['VALEUR']) && !empty($this->str_recherche))
		{
			// voir avec operateur
			if ($tabCrit['OP']=='OR')
				$this->str_recherche.=' OR '; // OR
			else if ($tabCrit['OP']=='NOT'){
				$this->str_recherche.=' AND (*:* NOT '; // NOT
				$suffixe=' ) ';
				}
			else
				$this->str_recherche.=' AND '; // AND
		}
		else if (empty($this->str_recherche) && $tabCrit['OP']=='NOT')
		{
			$this->str_recherche.='(*:* NOT '; // NOT
			$suffixe=' ) ';
		}

		switch ($tabCrit['TYPE'])
		{
			case 'C':
				$this->chercheChampListe($tabCrit,$prefix);
				break;
			case 'CE':
			case 'NCE':
				$this->chercheChamp($tabCrit,$prefix,true);
				break;
			case 'CR': // Cherche rŽfŽrence
				$arr_fields=explode(',',$tabCrit['FIELD']);
				if (in_array('id_doc',$arr_fields))
				{
					if (!is_numeric($tabCrit['VALEUR']))
					{
						$key=array_search('id_doc',$arr_fields);
						unset($arr_fields[$key]);
						$tabCrit['FIELD']=implode(',',$arr_fields);
					}
				}

				$this->chercheChamp($tabCrit,$prefix,true,true);
				break;
			case 'CFQ':
				$this->chercheChampFQ($tabCrit,$prefix,true);
				break;
			case 'CI':
				$this->chercheIdChamp($tabCrit,$prefix);
				break;
			case 'FT':
				$this->chercheTexteAmongFields($tabCrit,$prefix);
				break;
            case 'FTE': //FullText exact
                $tabCrit['VALEUR']='"'.trim(str_replace('"',' ',$tabCrit['VALEUR'])).'"';
                $this->chercheTexteAmongFields($tabCrit,$prefix,true);
                break;
			case 'MAT':
				$this->chercheMat($tabCrit,$prefix);
				break;
			case 'FEST':
				$this->chercheFestival($tabCrit,$prefix);
				break;
            case 'DA' : // Recherche DocAcc
                $this->chercheDocAcc($tabCrit,$prefix);
                break;
            case 'DAF' : // Recherche DocAcc en fulltext
                $this->chercheDocAccFT($tabCrit,$prefix);
                break;
			case 'D':
				$this->chercheDate($tabCrit,$prefix);
				break;
			case 'CDLT':
				$this->chercheDate($tabCrit,$prefix);
				break;
			case 'CDGT':
				$this->chercheDate($tabCrit,$prefix);
				break;
			case 'H':
			case 'CH':
				$this->chercheDuree($tabCrit,$prefix);
				break;
			case 'CHGT':
				$this->chercheDuree($tabCrit,$prefix);
				break;
			case 'CHLT':
				$this->chercheDuree($tabCrit,$prefix);
				break;
			case "CV" : //comparaison de valeur =
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVGT" : //comparaison de valeur >
				$tabCrit['VALEUR2']='>';
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVLT" : //comparaison de valeur <
				$tabCrit['VALEUR2']='<';
				$this->compareValeur($tabCrit,$prefix);
				break;
			case 'PAN':
				$this->cherchePanier($tabCrit,$prefix);
				break;
            case 'L' :
                $this->chercheLexique($tabCrit,$prefix);
                break;
			case 'CAT' :
                $this->chercheCategorie($tabCrit,$prefix);
                break;
			case 'HIER' :
				$this->chercheHierachic($tabCrit,$prefix);
				break;
			case 'FP' :
			case 'F' :
				$this->chercheFonds($tabCrit,$prefix);
				break;
			case 'V':
			case 'CB':
			case 'P' :
			case 'CT' :
			case "FI": //Fonds par ID
            case "FPI": //Fonds parent par ID
				$this->chercheValeur($tabCrit,$prefix);
				break;
			case 'U':
				$this->chercheUsager($tabCrit,$prefix);
				break;
		}

		if(!empty($suffixe)){
			$this->str_recherche.=$suffixe;
		}
	}

	protected function chercheChamp($tabCrit,$prefix=null,$keepIntact=false,$onlyAlpha=false)
	{
        if($tabCrit['VALEUR']=='*') $this->ajouterChampHighlight($tabCrit['FIELD'],1);
        else $this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);

		/*if ($keepIntact===true)
			$this->str_recherche.=$tabCrit['FIELD'].':"'.$tabCrit['VALEUR'].'"';
		else
			$this->str_recherche.=$tabCrit['FIELD'].':('.$this->filtreOperateursRequete($tabCrit['VALEUR']).')';*/
		if (!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0){
			$this->str_recherche.= " (";
			if(is_array($tabCrit['FIELD'])){
				foreach($tabCrit['FIELD'] as $idx=>$fld){
					if($idx!=0){$this->str_recherche.=" OR ";}
					$this->str_recherche.=" ( *:* NOT ".$fld.":* )";
				}
			}else{
				$this->str_recherche.=" *:* NOT ".$tabCrit['FIELD'].":* ";
			}
			$this->str_recherche.=")";

			if(empty($tabCrit['NOHIST'])){
				$this->addEtape($tabCrit['OP'],$tabCrit['LIB']." : ".(defined("kVide")?kVide:"vide"));
			}

		} else {
	        $fields=explode(',',$tabCrit['FIELD']);
			$criteres=array();
			if (empty($tabCrit['VALEUR2'])){
				foreach($fields as $fld)
					$criteres[]=$this->sql_opSolr($fld,$this->filtreOperateursRequete($tabCrit['VALEUR']),$keepIntact,$onlyAlpha);
			} else {
				foreach($fields as $fld) {
					$criteres[]=$fld.":[".$tabCrit['VALEUR']." TO ".$tabCrit['VALEUR2']."]";
				}

			}

			$this->str_recherche.='('.implode(' OR ',$criteres).')';

	        if(empty($tabCrit['NOHIST'])){
	            if(is_array($tabCrit['VALEUR'])) $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.implode(', ',$tabCrit['VALEUR']));
	            else $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
	        }
		}
	}

	protected function chercheChampFQ($tabCrit,$prefix=null,$keepIntact=false,$onlyAlpha=false)
	{
        if($tabCrit['VALEUR']=='*') $this->ajouterChampHighlight($tabCrit['FIELD'],1);
        else $this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);
		
        $fields=explode(',',$tabCrit['FIELD']);
		$criteres=array();

		foreach($fields as $fld){
			$criteres[]=$this->sql_opSolr($fld,$this->filtreOperateursRequete($tabCrit['VALEUR']),$keepIntact,$onlyAlpha);
		}
		
		$str_fq ='('.implode('OR',$criteres).')';
		// Les fqs sont des filtres, et ne devraient donc pas être des champs "OU", le cas AND est redondant, car les fq devraient par défaut etre appliquées avec AND, seul le cas AND NOT doit être traité différement. 
		if(isset($tabCrit['OP']) && ($tabCrit['OP'] == 'NOT' || $tabCrit['OP'] == 'AND NOT')){
			$str_fq = '*:* AND NOT '.$str_fq;
		}
		
		if(isset($tabCrit['OPTIONS']) && !empty($tabCrit['OPTIONS'])){
			$str_fq.="{!".$tabCrit['OPTIONS']."}";
		}else{
			$str_fq .="{!form_crit=1}";
		}
		// les filterquery sont maintenant ajoutés au fur et a mesure du traitement du formulaire, et plus dans finaliserequete uniquement. 
		$this->solr_query->addFilterQuery($str_fq);
		$this->str_recherche_fq .= "&fq=".$str_fq;
		
        if(empty($tabCrit['NOHIST'])){
            if(is_array($tabCrit['VALEUR'])) $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.implode(', ',$tabCrit['VALEUR']));
            else $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
        }
	}
	protected function chercheChampListe($tabCrit,$prefix=null,$keepIntact=false)
	{
        if($tabCrit['VALEUR']=='*') $this->ajouterChampHighlight($tabCrit['FIELD'],1);
        else $this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);
		
		if(!is_array($tabCrit['VALEUR'])){
			$valeurs = explode(',',$tabCrit['VALEUR']);
		}else{
			$valeurs = $tabCrit['VALEUR'];
		}
		$this->str_recherche.='(';

		foreach ($valeurs as $idx=>$val)
		{
			if ($idx>0)
				$this->str_recherche.=' '.$this->insideFieldBooleanOp.' ';

			$this->str_recherche.=' '.$this->checkFieldVersionDependant($tabCrit['FIELD']).':('.$this->filtreOperateursRequete($val).')';
		}
		$this->str_recherche.=' )';

        if(empty($tabCrit['NOHIST'])){
            if(is_array($tabCrit['VALEUR'])) $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.implode(', ',$tabCrit['VALEUR']));
            else $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
        }
	}

	protected function chercheIdChamp($tabCrit,$prefix="",$includeFather=false)
	{
		$champ=strtolower($this->entity.'_id_'.$tabCrit['FIELD']);

		$this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);

		$this->str_recherche.='(';
		if (is_array($tabCrit['VALEUR']))
		{
			foreach ($tabCrit['VALEUR'] as $idx=>$valeur)
			{
				if ($idx>0)
					$this->str_recherche.=' '.$this->insideFieldBooleanOp.' ';

				$this->str_recherche.=$this->checkFieldVersionDependant($champ).':"'.$valeur.'" ';
			}
		}
		else
		{
			$this->str_recherche.=$this->checkFieldVersionDependant($champ).':"'.$tabCrit['VALEUR'].'" ';
		}
		$this->str_recherche.=')';

		try{
			$table="t_".strtolower($tabCrit['FIELD']);
			if (is_array($tabCrit['VALEUR'])) { //// correction Warning: Invalid argument supplied
			    foreach ($tabCrit['VALEUR'] as $idx) {
					$val=GetRefValue($table,$idx,$_SESSION['langue']);
					if ($val) $arrWords[]=$val." ";
					else $arrWords[]=$idx;/*elseif ($missing==false) $missing=true;*/
				}
			}
		}catch(Exception $e){
			$arrWords = $tabCrit['VALEUR'];
		}
		if (!empty($arrWords)) { // correction Warning: Invalid argument supplied
			$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.implode(', ',$arrWords));
		}
	}

	protected function chercheValeur($tabCrit,$prefix="")
	{
		if($tabCrit['TYPE']== 'P' && isset($tabCrit['OPTIONS']) && !empty($tabCrit['OPTIONS'])) {
			$champ=strtolower($tabCrit['FIELD']).'__'.strtolower($tabCrit['OPTIONS']);
		} else {
			$champ=strtolower($tabCrit['FIELD']);
		}

		if (isset($tabCrit['OPTIONS']) && !empty($tabCrit['OPTIONS']) && strpos($tabCrit['OPTIONS'], "table:") === 0) {
			$table = substr($tabCrit['OPTIONS'], 6);
		}
		
		// VP 8/9/2017 : correction bug recherche sur plusieurs valeurs
		//$tabCrit['VALEUR']=str_replace('", "','","',$tabCrit['VALEUR']);
		if (is_array($tabCrit['VALEUR']))
			$valeurs=$tabCrit['VALEUR'];
		else{
			$valeurs=explode(',',$tabCrit['VALEUR']);
		}

		if(!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0) {
			$this->str_recherche.='( *:* NOT '.$tabCrit['FIELD'].':* )';
			$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.(defined("kVide")?kVide:"vide"));
			return;
		}

        if($tabCrit['VALEUR']=='*') $this->ajouterChampHighlight($tabCrit['FIELD'],1);
        else $this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);

		$this->str_recherche.='(';
		$missing=false;
		$fields=explode(',',$champ);
		foreach ($fields as $idc=>$field) {
			foreach ($valeurs as $idx=>$valeur) {
				$valeur = trim($valeur);
				if ($idx>0){
					$clause_ft.=' '.$this->insideFieldBooleanOp.' ';
				}else {
					$clause_ft = '';
				}

				if(preg_match('|^".*?"$|', $valeur) === 1) {
					$clause_ft.='('.$valeur.')';
				} elseif(preg_match('|^".*|', $valeur) === 1) {
					$clause_ft.='('.$valeur.'")';
				} elseif(preg_match('|.*?"$|', $valeur) === 1) {
					$clause_ft.='("'.$valeur.')';
				} else {
					$clause_ft.='("'.$valeur.'")';
				}

			}
			
			if ($idc>0) $this->str_recherche.=' OR ';
			$this->str_recherche.=$this->checkFieldVersionDependant($field).':('.$clause_ft.')';
			
			if (isset($table) && !empty($table)) {
				$val=GetRefValue($table,$valeur,$_SESSION['langue']);
				if ($val) $arrWords[]=$val." "; elseif ($missing==false) $missing=true;
			}
		}

		$this->str_recherche.=')';
		
		$valEtape = $tabCrit['VALEUR'];
		if (isset($table) && !empty($table) && !empty($arrWords) && !$missing) {
			$arrWords=array_unique($arrWords);
            $valEtape = implode(' / ',$arrWords)." ";
		} elseif (is_array($valEtape)) {
			$valEtape = implode(", ", $valEtape);
		}
		$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$valEtape);

	}

    protected function chercheLexique($tabCrit,$prefix="")
    {

        if($tabCrit['TYPE']== 'P' && isset($tabCrit['OPTIONS']) && !empty($tabCrit['OPTIONS'])) {
            $champ=strtolower($tabCrit['FIELD']).'__'.strtolower($tabCrit['OPTIONS']);
        } else {
            $champ=strtolower($tabCrit['FIELD']);
        }
		
		if(strpos($tabCrit['VALEUR'], '"')>0){
			$tabCrit['VALEUR']=str_replace('", "','","',$tabCrit['VALEUR']);
			$valeurs=explode('","',$tabCrit['VALEUR']);
		}else{
			$valeurs=explode(',',$tabCrit['VALEUR']);
		}

		$valeurs = array_map("addDoubleQuote",$valeurs);
		
		   
        if(!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0) {
        	$this->str_recherche.='( *:* NOT '.$tabCrit['FIELD'].':* )';
        	$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.(defined("kVide")?kVide:"vide"));
        	return;
        }

        if($tabCrit['VALEUR']=='*') $this->ajouterChampHighlight($tabCrit['FIELD'],1);
        else $this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);

        $this->str_recherche.='(';
        foreach ($valeurs as $idx=>$valeur)
        {
            $arrWords=array($valeur);

            if ($idx>0)
                $this->str_recherche.=' '.$this->insideFieldBooleanOp.' ';

            $fields=explode(',',$champ);
            foreach ($fields as $idc=>$field) {

                if(strpos($field,'l_')===0){
                    $field_solr = explode('_',$field);
                    if(count($field_solr) >3){
                        $restrict_to_type = strtoupper($field_solr[3]);
                    }else{
                        $restrict_to_type = null ;
                    }
                    $synonymes=$this->extLexique($arrWords,null,false,$restrict_to_type);
                }else{
                    $synonymes=$this->extLexique($arrWords);
                }
                if (empty($synonymes[0]['HIGHLIGHT']))
                    $clause_ft='('.$valeur.')';
                else
                    $clause_ft='('.implode(' OR ',$synonymes[0]['HIGHLIGHT']).')';

                if ($idc>0) $this->str_recherche.='OR ';
                $this->str_recherche.=' '.$this->checkFieldVersionDependant($field).':'.$clause_ft;
            }
        }

        $this->str_recherche.=')';

        $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
    }


	protected function chercheCategorie($tabCrit,$prefix="",$keepIntact=true)
    {
        $champ=strtolower($tabCrit['FIELD']);

        $tabCrit['VALEUR']=str_replace('", "','","',$tabCrit['VALEUR']);
        $valeurs=explode('","',$tabCrit['VALEUR']);

        if($tabCrit['VALEUR']=='*') $this->ajouterChampHighlight($tabCrit['FIELD'],1);
        else $this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);

        $this->str_recherche.='(';
        foreach ($valeurs as $idx=>$valeur)
        {
            $arrWords=array($valeur);

            if ($idx>0)
                $this->str_recherche.=' '.$this->insideFieldBooleanOp.' ';

            $fields=explode(',',$champ);
            foreach ($fields as $idc=>$field) {

                if(strpos($field,'ct_')===0){
                    $field_solr = explode('_',$field);
                    if(count($field_solr) >1){
                        $restrict_to_type = $field_solr[1];
                    }else{
                        $restrict_to_type = null ;
                    }
                    $synonymes=$this->extCategorie($arrWords,null,$restrict_to_type);
                }else{
                    $synonymes=$this->extCategorie($arrWords);
                }

                if (empty($synonymes[0]['HIGHLIGHT']))
                    $clause_ft='('.$valeur.')';
                else
                    $clause_ft='('.implode(' OR ',$synonymes[0]['HIGHLIGHT']).')';

                if ($idc>0) $this->str_recherche.='OR ';
                $this->str_recherche.=' '.$this->checkFieldVersionDependant($field).':'.$clause_ft;
            }
        }

        $this->str_recherche.=')';

        $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
    }

	protected function chercheHierachic($tabCrit,$prefix="",$keepIntact=true)
	{
		$champ=strtolower($tabCrit['FIELD']);
		
		$tabCrit['VALEUR']=str_replace('"','',$tabCrit['VALEUR']);
		$valeurs=explode(',',$tabCrit['VALEUR']);
		
		if($tabCrit['VALEUR']=='*') $this->ajouterChampHighlight($tabCrit['FIELD'],1);
		else $this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);

		
		$this->str_recherche.='(';
		foreach ($valeurs as $idx=>$valeur)
		{
			$cntChevron=explode(">",$valeur);$cntChevron=count($cntChevron);
			$cntSlash=explode("/",$valeur);$cntSlash=count($cntSlash);
			$cntMax=max($cntChevron,$cntSlash);
			switch($cntMax){
				case $cntChevron:$separator=">";break;
				case $cntSlash:$separator="/";break;
			}
			if ($idx>0)
				$this->str_recherche.=' '.$this->insideFieldBooleanOp.' ';
			
			$fields=explode(',',$champ);
			foreach ($fields as $idc=>$field) {
				
				$_chunks = explode($separator, trim($valeur));
				
				$valeur = count($_chunks)-1;
				foreach($_chunks as $chunk){
					$valeur .= "/".trim(str_replace($separator,"/",$chunk));
				}
				
				$clause_ft='("'.$valeur.'")';
				
				if ($idc>0) $this->str_recherche.='OR ';
				$this->str_recherche.=' '.$this->checkFieldVersionDependant($field).':'.$clause_ft;
			}
		}
		
		$this->str_recherche.=')';
		
		$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
	}


	protected function chercheTexteAmongFields($tabCrit,$prefix="")
	{
		$string_recherche_fulltext='';

		if (count(explode(",",$tabCrit['FIELD'])) == 1 && strcmp(trim($tabCrit['VALEUR']), '#') === 0) {
			$arrChamps=explode(",",$tabCrit['FIELD']);
			$fld = $prefix.$arrChamps[0];
			$this->str_recherche.= " ( *:* NOT ".$fld.":* )";
			if(empty($tabCrit['NOHIST'])) $this->addEtape($tabCrit['OP'],$tabCrit['LIB']." : ".(defined("kVide")?kVide:"vide"),$tabCrit['FIELD']);
			$this->ajouterChampHighlight($field,1);

			return;
		}

		foreach (explode(',',$tabCrit['FIELD']) as $idx=>$field){
			
			if(strpos($field,'^') !== false){
				$fld_tmp = explode('^',$field);
				$field = $fld_tmp[0];
				$ponderation = $fld_tmp[1];
				unset($fld_tmp);
			}else{
				$ponderation = null ;
			}
			
			if(preg_match("/^\[E\]/i",$field)){
				// on a le modifier [E] devant le nom du champ, ce qui permet de passer du mode FT au mode FTE pour un champ parmis une liste par ex
				$field = substr($field,3);
				$valeur_tmp = '"'.trim(str_replace('"',' ',$tabCrit['VALEUR'])).'"';
			}else{
				$valeur_tmp = $tabCrit['VALEUR'];
			}
			
			
			if($valeur_tmp=='*')
				$this->ajouterChampHighlight($field,1);
            else 
				$this->ajouterChampHighlight($field,$tabCrit['NOHIGHLIGHT']);
			
			$parse_str_to_lex = true ; 
			if(isset($tabCrit['VALEUR2']) && $tabCrit['VALEUR2']=='1'){
				$extLexFlag = true ;
			}elseif(isset($tabCrit['VALEUR2']) && $tabCrit['VALEUR2']=='2'){
				$onlySyno = true ;
				$extLexFlag = true ;
			}else{
				if(isset($tabCrit['VALEUR2']) && $tabCrit['VALEUR2']=='-1'){
					$parse_str_to_lex = false ; 
				}
				$onlySyno = false ;
				$extLexFlag = false ;
			}
			$params_ext_lex = array(
				"parse_str_to_lex"=>$parse_str_to_lex,
				"extLexFlag"=>$extLexFlag,
				"onlySyno"=>$onlySyno
			);
			
			

			$arr_field=$this->analyzeString2(stripUnmatchedParentheses($valeur_tmp),$arr_operators,true);
			$clause_ft='';
			
			if(!empty($arr_field)){
				// dans le cas où le champ sur lequel on recherche est un champ lexique (ou hl_l, ou facet_l, ou thes_l) on récupère le type du lexique depuis la clé solr du champ
				if(preg_match('/^(?:hl_l_|facet_l_|l_)([\S]+)/',$field,$match)){
					
					if(isset($match[1])){
						$field_solr = explode('_',$match[1]);
						if(count($field_solr) == 3){
							$restrict_to_type = $field_solr[2];
						}
					}
				}else if (preg_match('/^(?:thes_)([\S]+)/',$field,$match)){
					if(isset($match[1])){
						$restrict_to_type = $match[1];
					}
				}
				
				if(!isset($restrict_to_type)){
					$restrict_to_type = null ; 
				}
				$params_ext_lex["restrict_to_type"]=$restrict_to_type;
				
				$clause_ft = $this->smartKeywordsToSearchString($arr_field,$params_ext_lex);
			}
			if ($idx>0)
				$string_recherche_fulltext.=' OR ';
			$string_recherche_fulltext.=' '.$this->checkFieldVersionDependant($field).':('.$clause_ft.')'.((isset($ponderation) && !empty($ponderation))?"^".$ponderation:'');
		}
		
		
		if (!empty($string_recherche_fulltext)){
			$this->str_recherche.='('.$string_recherche_fulltext.' )';

            if(empty($tabCrit['NOHIST'])){
                $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR'],$tabCrit['FIELD'],$clause_ft);
            }
		}
	}

	function smartKeywordsToSearchString($smartKeywords,$params_ext_lex = array("parse_str_to_lex"=>true,"onlySyno"=>false,"extLexFlag" => false,"restrict_to_type" => null)){
		$str_recherche = "";
		$str_recherche = $this->recurs_toSearchString($smartKeywords,$str_recherche,$params_ext_lex);
		return $str_recherche ;
	}

	function recurs_toSearchString($node,$str_recherche,$params_ext_lex = array("parse_str_to_lex"=>true,"onlySyno"=>false,"extLexFlag" => false,"restrict_to_type" => null)){
		//trace("recurs_toSearchString");
		if(is_array($node)){
			foreach($node as $idx=>$childnode){
				$add_end_parenthese = false ;

				if(isset($childnode['OP']) && !empty($childnode['OP'])){
					if($childnode['OP'] != 'NOT' && $idx!= 0){
						$str_recherche.=" ".strtoupper($childnode['OP'])." ";
					}
					if($childnode['OP'] == 'NOT'){
						if($idx != 0 ){ $str_recherche.=" AND ";}
						$str_recherche.=" (*:* ".strtoupper($childnode['OP'])." ";
						$add_end_parenthese = true ;
					}
				}
				if(isset($childnode['SUBQUERY']) && !empty($childnode['SUBQUERY'])){
					if(is_array($childnode['SUBQUERY'])){
						$str_recherche.="(";
						$str_recherche.=$this->recurs_toSearchString($childnode['SUBQUERY'],'',$params_ext_lex);
						$str_recherche.=")";
					}else{
						if(defined("gSolrDefType") && gSolrDefType=="synonym_edismax"){
							// Pas d'extension lexicale
							$str_recherche='('.$childnode['SUBQUERY'].')';
						}
						else
						{
							// extension lexicale en appelant al fonction récursive smartExtLexique
							$transformed_str=$this->smartExtLexique($childnode['TOKENS'],$params_ext_lex);

							// normalization de la chaîne subquery :
							$childnode['SUBQUERY'] = trim(preg_replace( "/\s+|\(\)/", " ", $childnode['SUBQUERY']));

							// ajout à la chaine globale de recherche
							if(is_string($transformed_str) && !empty($transformed_str)){
								if(empty($str_recherche)) {
									$str_recherche .=$transformed_str;
								} else {
									$str_recherche .="(".$transformed_str.")";						 // (".str_replace(' ',' AND ',trim($childnode['SUBQUERY'])).") OR
								}
							}else{
								$str_recherche .=str_replace(' ',' AND ',trim($childnode['SUBQUERY']));
							}
						}
					}
				}
				if($add_end_parenthese){
					$str_recherche.=") ";
				}
			}
		}
		return $str_recherche;
	}

// 	MS 12/2014, algo DC,VP
   // Fonction récursive d'extension lexicale.
   // L'idée est de tester l'existence de termes complexes dans le thésaurus :
   // 	pr une recherche "A B C", on va chercher des lexiques en FT dans le thésaurus successivement  "A B C ";  "A B";  "A";
	//	La fonction getLexTermeFromThes gère la recherche FT vers postgre & l'extension lexicale proprement dite.
	// 	si on a trouvé des termes equivalents à "A", on alimente la chaine de recherche avec ces termes étendus, sinon juste avec le terme seul
	//	on continue récursivement la recherche sur "B C", puis "B" ; <appel recurs> "C" etc ...
    protected function smartExtLexique($arrWords,$params_ext_lex = array("parse_str_to_lex"=>true,"onlySyno"=>false,"extLexFlag" => false,"restrict_to_type" => null)) {
		global $db;
		$to_search = array() ;
		$j = count($arrWords)-1;
		$found = false ;
		$str_recherche='';
		if(is_array($params_ext_lex)){
			foreach($params_ext_lex as $name=>$value){
				try{
					eval('$'.$name.'=$value;');
				}catch(Exception $e){
					trace("smartExtLexique error - params_ext_lex failed eval $name => $value");
				}
			}
		}
		
		// si mots $arrWords vides, retourne array vide
		if(empty($arrWords)){
			return array();
		}

		// parcours de la chaine de recherche depuis la fin
		while($j>=0 && !$found){
			$tmp_arrWords = $arrWords;
			// récupération du terme candidat (ex : "A B C", "A B", "A")
			$candidat=implode(' ',array_splice($tmp_arrWords,0,$j+1));
			//  verification thesaurus et ext lexicale
			$tmp = array(); 
			if($parse_str_to_lex){
				$tmp = $this->getLexTermeFromThes($candidat,$onlySyno,$restrict_to_type,$extLexFlag);
				$tmp2 = $this->getCatFromThes($candidat,$extLexFlag,$restrict_to_type);
				$tmp = array_merge($tmp,$tmp2);
			}
			// si on trouve on alimente l'array "to_search" des termes à rechercher avec : les termes equivalents trouvés, les termes trouvés par ext lexicale et les termes candidats utilisés
			if(is_array($tmp) && !empty($tmp)){
				$found = true ;
				// $to_search = array_merge($to_search,$tmp,array(trim($candidat,'"')));
				$to_search = array_merge($to_search,$tmp,array(array('type'=>'candidat','value'=>$candidat)));
			}else{
				// sinon on continue le parcours
				$j--;
			}
		}
		// on supprime les termes doublons si il y en a
		$to_search = array_values(array_unique($to_search));

		// si des termes ont été trouvé
		if(!empty($to_search)){
			$str_recherche.= '(';
			foreach($to_search as $idx=>$component){
				if($idx!=0){
					$str_recherche.=" OR ";
				}
				// on alimente la chaine de recherche avec la syntaxe d'adjacence de Solr (~).
				// Il est à noté qu'il s'agit plus d'un nombre de permutation autorisé que d'une distance entre les mots. La permutation "A B" <-> "B A" coute 1, alors que la distance est tjs 0.
				
				// MS - 11.12.17 - dans le cas où l'un des termes (en général le candidat) est de la forme d'un seul mot contenant ou se terminant par un wildcard, 
				// on ne met pas de doubles quotes pour utiliser l'interpretation native du wildcard, 
				// Dans le cas d'un 'terme' de plusieurs mots, entre quotes, contenant un wildcard, il faut s'assurer de l'existence du queryparser dans solrconfig.xml pour qu'il soit correctement évalué et de son activation sur le requestHandler "/select" en mettant le param defType=<nom_du_query_parser>: 
				// 		- le plugin ComplexPhraseQParserPlugin pour solr < 4.8.0
				//		- le ComplexPhraseQueryParser natif pour solr >= 4.8.0
				if(is_array($component) && $component['type'] == 'candidat'){
					$component['out_value'] = trim(preg_replace( "/\s+|\(\)/", " ", $component['value']));
					if($component['out_value'][0] == '"' && $component['out_value'][strlen($component['out_value'])-1] == '"' ){
						$str_recherche.='('.$component['out_value'].')';
					}else{
						$str_recherche.='('.str_replace(' ',' AND ',$component['out_value']).')';
					}
				}else{
					// on alimente la chaine de recherche avec la syntaxe d'adjacence de Solr (~).
					// Il est à noté qu'il s'agit plus d'un nombre de permutation autorisé que d'une distance entre les mots. La permutation "A B" <-> "B A" coute 1, alors que la distance est tjs 0.
					$str_recherche.='"'.$component.'"~'.intval(count(explode(' ',$component))-1)." ";
				}
			}
			$str_recherche.=")";
		}
		unset($tmp_arrWords);

		// Si des termes ont été trouvés et qu'on etait pas sur le premier mot de la chaine candidate de départ
		if($found && $j != -1 ){
			$tmp_arrWords = $arrWords;
			// appel récursif sur le reste de la chaine
			$tmp_str = $this->smartExtLexique(array_splice($tmp_arrWords,$j+1), $restrict_to_type,$params_ext_lex);
			if(!empty($tmp_str)){
				if(!empty($str_recherche)){
					$str_recherche.=' AND ';
				}
				$str_recherche.= $tmp_str;
			}
		}
		unset($tmp_arrWords);

		// si aucun terme n'a été trouvé, on ajoute le premier mot à la chaine de recherche tel quel, et on continue le process sur le reste de la chaine
		if($found == false && $j == -1 && !empty($arrWords)){
			$tmp_arrWords = $arrWords;
			$str_recherche.= $tmp_arrWords[0];
			// appel récursif sur le reste de la chaine
			$tmp_str = $this->smartExtLexique(array_splice($tmp_arrWords,1), $restrict_to_type,$params_ext_lex);
			if(!empty($tmp_str)){
				if(!empty($str_recherche)){
					$str_recherche.=' AND ';
				}
				$str_recherche.= $tmp_str;
			}

		}
		return $str_recherche;
    }

	function getLexTermeFromThes($str,$onlySyno=false,$restrict_to_type=null,$extLexFlag = false ){
		global $db;
		//$start = microtime(true);
		//trace("getLexTermeFromThes");
		$return = array();
		if(substr($str,0,1) == '"' && substr($str,-1)=='"'){
			$terme_exact = true ;
			$str = trim($str,'"');
		}else{
		$terme_exact = false ;
		}

		$tmp_arr = explode(' ',$str);
		$str2="";
		foreach($tmp_arr as $word){
			if($str2 != ''){
			$str2.= " & ";
			}
			$str2.= $word;
		}
		if(trim($str2) === ''){
			return array();
		}
		// on a éventuellement une restriction à un type de lexique
		if(isset($restrict_to_type) && !empty($restrict_to_type)){
			$where_cond = $db->getFullTextSQL(array($str2), array("LEX_TERME")).' AND LEX_ID_TYPE_LEX = '.$db->Quote(strtoupper($restrict_to_type));
		}else{
			$where_cond = $db->getFullTextSQL(array($str2), array("LEX_TERME"));
		}
		$langue=(isset($_SESSION['langue'])?$db->Quote($_SESSION['langue']):"'FR'");

        //trace(print_r($this->params, true));
		// recherche fulltext des termes equivalents à la chaine candidate $str
		//$sql = "select DISTINCT ID_LEX,LEX_TERME from t_lexique where ID_LANG=".$langue." and ".$where_cond;
        $sql = "select DISTINCT ID_LEX,LEX_TERME from t_lexique where ".$where_cond;
        if ($this->params['RechercheLexiqueRestrictionLangage']==true) $sql.=" AND ID_LANG=".$langue;
		$result = $db->Execute($sql);
		$ids = array();
		//trace("fin getLexTermeFromThes : ".floatval(microtime(true) - $start ) ); 
		if (!$result) return array();
		while($list=$result->FetchRow()){
			// MS  - 09.03.16 - les mots vides solr sont filtrés lors de la génération du mapping de la requete, on doit donc aussi les filtrer dans les retours postgre avant de comparer la longueur des chaines
			// On récupère donc la meme liste de mots vides et on les enleve des resultats du fulltext postgre.
			// => cf test sur gpa avec le terme "TOUR DE FRANCE"
			if(defined("gMotsVidesAnalyzeStringSolr")  && is_array(unserialize(gMotsVidesAnalyzeStringSolr))){
				$arr_mots_vides = unserialize(gMotsVidesAnalyzeStringSolr);
				$tmp_arr = explode(' ',str_replace(array('-',"'"),array(' ',''),$list["LEX_TERME"]));
				$terme_explode = array();
				foreach($tmp_arr as $idx=>$word){
					if(!in_array(strtolower(trim($word)),$arr_mots_vides)){
						$terme_explode [] = $word ;
					}
				}
			}

			// pour chaque terme trouvé, on exclut deja les termes trouvés qui n'ont pas le même nombre de mots
			if((isset($terme_explode) && !empty($terme_explode) && count($terme_explode) == count(explode(' & ',trim($str2))))
				|| (count(explode(' ',str_replace(array('-',"'"),array(' ',''),$list["LEX_TERME"]))) == count(explode(' & ',trim($str2))))
				|| ($terme_exact && count(explode(' ',$list["LEX_TERME"])) == count(explode(' & ',trim($str2))))){
				$ids['ID'][] = $list["ID_LEX"];
				$return[] = $list["LEX_TERME"];
			}
		}
		// si on a toujours des termes, on passe par le processus d'extension lexicale classique utilisé jusqu'à maintenant
		if(!empty($ids)){
			$ext_array = $ids['ID'];
			if($extLexFlag){
				$extSyno1=$this->extLexiqueSyno($ids);
				if (!empty($extSyno1))$ext_array=array_merge($ext_array,$extSyno1);
			}
			if(!$onlySyno && $extLexFlag){
				// MS - 09.03.16 - on applique l'extension hierarchique sur le terme recherché ET ses synonymes (=> appel avec $ext_array comme paramètre à la place de $ids['ID'])
				$extHier=$this->extLexiqueHier(array('ID'=>$ext_array),true);
				if (!empty($extHier)) {
					$ext_array=array_merge($ext_array,$extHier);
					$extSyno=$this->extLexiqueSyno(array('ID'=>$extHier));
					if (!empty($extSyno)) $ext_array=array_merge($ext_array,$extSyno);
				}
			}

			$ext_array = array_unique($ext_array);
			if(defined('kMaxExtensionLexicaleSolr')) {
				$ext_array = array_splice($ext_array,0,kMaxExtensionLexicaleSolr);
			}
			// récupération des termes à partir des IDs
			if(!empty($ext_array)){
				//$sql2 = "select DISTINCT ID_LEX,LEX_TERME from t_lexique where ID_LANG=".$langue." and ID_LEX in (".implode(',',$ext_array).");";
                $sql2 = "select DISTINCT ID_LEX,LEX_TERME from t_lexique where ID_LEX in (".implode(',',$ext_array).")";
                if ($this->params['RechercheLexiqueRestrictionLangage']==true) $sql2.=" AND ID_LANG=".$langue;
				$result = $db->Execute($sql2);
				while($list=$result->FetchRow()){
					$return[] = $list["LEX_TERME"];
				}
			}
		}
		//trace("fin getLexTermeFromThes : ".floatval(microtime(true) - $start ) ); 
		return $return;
	}

	function getCatFromThes($str,$extLexFlag = false,$restrict_to_type=null){
		//$start = microtime(true);
		//trace("getCatFromThes");
		global $db;
		$return = array();
		if(substr($str,0,1) == '"' && substr($str,-1)=='"'){
			$terme_exact = true ;
			$str = trim($str,'"');
		}else{
			$terme_exact = false ;
		}

		$tmp_arr = explode(' ',$str);
		$str2="";
		foreach($tmp_arr as $word){
			if($str2 != ''){
			$str2.= " & ";
			}
			$str2.= $word;
		}
		if(trim($str2) === ''){
			return array();
		}
		// on a éventuellement une restriction à un type de lexique
		if(isset($restrict_to_type) && !empty($restrict_to_type)){
			$where_cond = $db->getFullTextSQL(array($str2,$restrict_to_type), array("CAT_NOM","CAT_ID_TYPE_CAT"));
		}else{
			$where_cond = $db->getFullTextSQL(array($str2), array("CAT_NOM"));
		}
		$langue=(isset($_SESSION['langue'])?$db->Quote($_SESSION['langue']):"'FR'");

		// recherche fulltext des termes equivalents à la chaine candidate $str
		$sql = "select DISTINCT ID_CAT,CAT_NOM from t_categorie where ID_LANG=".$langue." and ".$where_cond;
		$result = $db->Execute($sql);
		$ids = array();
		//trace("fin getCatFromThes : ".floatval(microtime(true) - $start ) ); 
		if (!$result) return array();
		while($list=$result->FetchRow()){
			// MS  - 09.03.16 - les mots vides solr sont filtrés lors de la génération du mapping de la requete, on doit donc aussi les filtrer dans les retours postgre avant de comparer la longueur des chaines
			// On récupère donc la meme liste de mots vides et on les enleve des resultats du fulltext postgre.
			// => cf test sur gpa avec le terme "TOUR DE FRANCE"
			if(defined("gMotsVidesAnalyzeStringSolr")  && is_array(unserialize(gMotsVidesAnalyzeStringSolr))){
				$arr_mots_vides = unserialize(gMotsVidesAnalyzeStringSolr);
				$tmp_arr = explode(' ',str_replace(array('-',"'"),array(' ',''),$list["LEX_TERME"]));
				$terme_explode = array();
				foreach($tmp_arr as $idx=>$word){
					if(!in_array(strtolower(trim($word)),$arr_mots_vides)){
						$terme_explode [] = $word ;
					}
				}
			}

			// pour chaque terme trouvé, on exclut deja les termes trouvés qui n'ont pas le même nombre de mots
			if((count(explode(' ',str_replace(array('-',"'"),array(' ',''),$list["CAT_NOM"]))) == count(explode(' & ',trim($str2))))
				||($terme_exact && count(explode(' ',$list["CAT_NOM"])) == count(explode(' & ',trim($str2))))){
				$ids['ID'][] = $list["ID_CAT"];
				$return[] = $list["CAT_NOM"];
			}
		}
		// si on a toujours des termes, on passe par le processus d'extension lexicale classique utilisé jusqu'à maintenant
		if(!empty($ids)){
			$ext_array = $ids['ID'];

			if($extLexFlag){
				$extHier=$this->extCatHier($ids,true);
				if (!empty($extHier)) {
					$ext_array=array_merge($ext_array,$extHier);
				}
			}

			$ext_array = array_unique($ext_array);
			if(defined('kMaxExtensionLexicaleSolr')) {
				$ext_array = array_splice($ext_array,0,kMaxExtensionLexicaleSolr);
			}

			// récupération des termes à partir des IDs
			if(!empty($ext_array)){
				$sql2 = "select DISTINCT ID_CAT,CAT_NOM from t_categorie where ID_LANG=".$langue." and ID_CAT in (".implode(',',$ext_array).");";
				$result = $db->Execute($sql2);
				while($list=$result->FetchRow()){
					$return[] = $list["CAT_NOM"];
				}
			}
		}
		//trace("fin getCatFromThes : ".floatval(microtime(true) - $start ) ); 
		return $return;
	}



	protected function chercheDate($tabCrit,$prefix="")
	{
		//formats yyyy-mm-dd, yyyy-mm, yyyy
		//$tabCrit['VALEUR2']
		$champs_type_date=array('doc_date_diff_solr','doc_date_prod_solr','doc_date_solr');

		if (defined('gAjoutChampsDateSolr'))
			$champs_type_date=array_merge($champs_type_date,explode(',',gAjoutChampsDateSolr));

		if (isset($tabCrit['VALEUR2']) && !empty($tabCrit['VALEUR2']))
		{
			if (in_array($tabCrit['FIELD'],$champs_type_date))
			{
				$date_debut=convToFirstDate(convDate($tabCrit['VALEUR'])).'T00:00:00Z';
				$date_fin=convToLastDate(convDate($tabCrit['VALEUR2'])).'T23:59:59Z';
			}
			else
			{
				$date_debut=convToFirstDate(convDate($tabCrit['VALEUR']));
				$date_fin=convToLastDate(convDate($tabCrit['VALEUR2']));
			}
			//$date_debut=$tabCrit['VALEUR'];
			//$date_fin=$tabCrit['VALEUR2'];

			if(empty($tabCrit['NOHIST']))
				$this->addEtape($tabCrit['OP'],$tabCrit['LIB']." ".kEntre." ".$tabCrit['VALEUR']." ".kEt." ".$tabCrit['VALEUR2']."");

			$tabCrit['VALEUR']='['.$date_debut.' TO '.$date_fin.']';

		}
		else
		{
			
			if (!is_array($tabCrit['VALEUR']) && strcmp(trim($tabCrit['VALEUR']), "#") == 0) {
				$this->str_recherche.='( *:* NOT ' . $tabCrit['FIELD'] . ':* )';
				if (empty($tabCrit['NOHIST'])) {
				$this->addEtape($tabCrit['OP'], $tabCrit['LIB'] . ' : ' . (defined("kVide") ? kVide : "vide"));
				}
				return;
			}
			
			if(empty($tabCrit['NOHIST']))
				$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' '.$tabCrit['VALEUR']);

			$tabCrit['VALEUR']=convDate($tabCrit['VALEUR']);
			if (in_array($tabCrit['FIELD'],$champs_type_date))
			{
				//MS - 09.11.16 - les valeurs horaires ont été ajustées pour être cohérent avec un comportement de comparaison inférieure ou supérieure stricte
				// CDLT - strictement en dessous de la valeur, c'est pourquoi l'heure limite est set à minuit, 
				// CDGT - strictement au dessus de la valeur, c'est pourquoi on set l'heure limite à la valeur T23:59:59Z
				if ($tabCrit['TYPE']=='CDLT')
					$tabCrit['VALEUR']='[* TO '.convToFirstDate($tabCrit['VALEUR']).'T00:00:00Z]';
				else if ($tabCrit['TYPE']=='CDGT')
					$tabCrit['VALEUR']='['.convToLastDate($tabCrit['VALEUR']).'T23:59:59Z TO *]';
				else
				{
					/*if(strlen($tabCrit['VALEUR'])<=10 && strpos($tabCrit['VALEUR'],'*')===false)
						$tabCrit['VALEUR'].='*';*/
					$tabCrit['VALEUR']='['.convToFirstDate($tabCrit['VALEUR']).'T00:00:00Z TO '.convToLastDate($tabCrit['VALEUR']).'T23:59:59Z]';
				}
			}
			else
			{
				// MS - 17.06.14 - Ajout de limites à la recherche par date
				// Les champs dates sont stockés comme des chaines, chercher 1930 TO * valide tout les champs possibles ou presque.
				// On ajoute donc des limites qui rameneront des valeurs plus cohérentes, meme si ce n'est pas parfait :
				// Limite haute : fin de l'année courante,
				// Limite basse : '1000' => evite de faire remonter les durées tout en laissant une large marge
				if ($tabCrit['TYPE']=='CDLT')
					$tabCrit['VALEUR']='[1000 TO '.$tabCrit['VALEUR'].']';
				else if ($tabCrit['TYPE']=='CDGT')
					$tabCrit['VALEUR']='['.$tabCrit['VALEUR'].' TO '.date('Y').'-12-31]';
				else
				{
					if(strlen($tabCrit['VALEUR'])<=10 && strpos($tabCrit['VALEUR'],'*')===false)
						$tabCrit['VALEUR'].='*';
					$tabCrit['VALEUR']=''.$tabCrit['VALEUR'].'';
				}
			}
		}

		$this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);

		if (strstr($tabCrit['FIELD'],',')!=false)// si on a plusieurs champs séparés par des virgules
		{
			$this->str_recherche.='( ';

			foreach (explode(',',$tabCrit['FIELD']) as $idx=>$field)
			{
				if ($idx!=0)
					$this->str_recherche.='OR ';
				$this->str_recherche.=$this->checkFieldVersionDependant(trim($field)).':'.$tabCrit['VALEUR'].' ';
			}

			$this->str_recherche.=')';
		}
		else
			$this->str_recherche.=$this->checkFieldVersionDependant($tabCrit['FIELD']).':'.$tabCrit['VALEUR'].'';
	}

	protected function chercheDuree($tabCrit,$prefix="")
	{
		$valeur_orig=$tabCrit['VALEUR'];

		if ($tabCrit['FIELD']=='doc_duree')
		{
			$tabCrit['FIELD']='duree_sec';
			$tabCrit['VALEUR']=timeToSec($tabCrit['VALEUR']);

            if (isset($tabCrit['VALEUR2']) && !empty($tabCrit['VALEUR2'])){
                $valeur_orig2 = $tabCrit['VALEUR2'];
				$tabCrit['VALEUR2']=timeToSec($tabCrit['VALEUR2']);
            }
		}

        if (isset($tabCrit['VALEUR2']) && !empty($tabCrit['VALEUR2'])){
			$tabCrit['VALEUR']='["'.$tabCrit['VALEUR'].'" TO "'.$tabCrit['VALEUR2'].'"]';
            if(empty($tabCrit['NOHIST']))
                $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$valeur_orig.' '.kA.' '.$valeur_orig2);
        } else {

			// MS - 17.06.14 - Ajout de limites à la recherche par duree
			// Le champ durée est stocké comme une chaine, chercher 00:01:00:00 TO * valide tout les champs possibles ou presque.
			// On ajoute donc des limites qui rameneront des valeurs plus cohérentes, meme si ce n'est pas parfait :
			// Limite haute :99,
			// Limite basse : 00

			if ($tabCrit['TYPE']=='CHGT')
			{
				if(empty($tabCrit['NOHIST']))
					$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' > '.$valeur_orig);

				$tabCrit['VALEUR']='["'.$tabCrit['VALEUR'].'" TO 86400]'; // 24 heures
			}
			else if ($tabCrit['TYPE']=='CHLT')
			{
				if(empty($tabCrit['NOHIST']))
					$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' < '.$valeur_orig);

				$tabCrit['VALEUR']='[00 TO "'.$tabCrit['VALEUR'].'"]';
			}
			else
			{
				if(empty($tabCrit['NOHIST']))
					$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' = '.$valeur_orig);

				$tabCrit['VALEUR']='"'.$tabCrit['VALEUR'].'"';
			}
		}

		$this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);
		$this->str_recherche.=$this->checkFieldVersionDependant($tabCrit['FIELD']).':'.$tabCrit['VALEUR'].'';
	}

	protected function compareValeur($tabCrit,$prefix="") {
		$valeur_orig=intval($tabCrit['VALEUR']);
   		if ($tabCrit['TYPE']=='CVGT')
		{
			if(empty($tabCrit['NOHIST']))
				$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' > '.$valeur_orig);

			$tabCrit['VALEUR']='['.$valeur_orig.' TO *]';
		}
		else if ($tabCrit['TYPE']=='CVLT')
		{
			if(empty($tabCrit['NOHIST']))
				$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' < '.$valeur_orig);

			$tabCrit['VALEUR']='[00 TO '.$valeur_orig.']';
		}
		else
		{
			if(empty($tabCrit['NOHIST']))
				$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' '.$valeur_orig);

			$tabCrit['VALEUR']=$valeur_orig;
		}

		$this->ajouterChampHighlight($tabCrit['FIELD'],$tabCrit['NOHIGHLIGHT']);
		$this->str_recherche.=$tabCrit['FIELD'].':'.$tabCrit['VALEUR'].'';
   }

	protected function chercheMat($tabCrit,$prefix="")
	{
		global $db;
		//trace('SELECT * FROM t_mat WHERE '.$tabCrit['FIELD'].' LIKE \'%'.$tabCrit['VALEUR'].'%\'');
		//$result=$db->Execute('SELECT * FROM t_mat WHERE '.$tabCrit['FIELD'].' LIKE \'%'.$tabCrit['VALEUR'].'%\'')->getRows();
//		trace('SELECT id_doc FROM t_mat LEFT JOIN t_doc_mat ON t_mat.id_mat=t_doc_mat.id_mat WHERE '.$tabCrit['FIELD'].' LIKE \'%'.$tabCrit['VALEUR'].'%\'');
//		$result=$db->Execute('SELECT ID_DOC FROM t_mat LEFT JOIN t_doc_mat ON t_mat.id_mat=t_doc_mat.id_mat WHERE '.$tabCrit['FIELD'].' LIKE \'%'.$tabCrit['VALEUR'].'%\'')->getRows();

        // trace('SELECT ID_DOC FROM t_mat INNER JOIN t_doc_mat ON t_mat.id_mat=t_doc_mat.id_mat WHERE '.$tabCrit['FIELD'].$this->sql_op($tabCrit['VALEUR'],true));
		$result=$db->Execute('SELECT ID_DOC FROM t_mat INNER JOIN t_doc_mat ON t_mat.id_mat=t_doc_mat.id_mat WHERE '.$tabCrit['FIELD'].$this->sql_op($tabCrit['VALEUR'],true))->getRows();


		$sql_doc='';

		foreach ($result as $idx=>$res)
		{
			if ($idx>0)
				$sql_doc.=' OR ';

			if (!empty($res['ID_DOC']))
				$sql_doc.=' id_doc:'.$res['ID_DOC'];
		}

		if(!empty($sql_doc)){
            $sql_doc='('.$sql_doc.' )';
            $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
            $this->str_recherche.=$sql_doc.'';
        }else{
            $this->str_recherche.=' id_doc:0';
        }
	}

	protected function chercheFestival($tabCrit,$prefix="")
	{
		global $db;
		// trace("SELECT df.ID_DOC from t_festival f,t_doc_fest df WHERE f.ID_FEST=df.ID_FEST and f.".$tabCrit['FIELD'].$this->sql_op($tabCrit['VALEUR'],true)." AND ID_LANG=".$db->quote($_SESSION['langue']));
		$result=$db->Execute("SELECT df.ID_DOC from t_festival f,t_doc_fest df WHERE f.ID_FEST=df.ID_FEST and f.".$tabCrit['FIELD'].$this->sql_op($tabCrit['VALEUR'],true)." AND ID_LANG=".$db->quote($_SESSION['langue']))->getRows();

		$sql_doc='';

		foreach ($result as $idx=>$res)
		{
			if ($idx>0)
				$sql_doc.=' OR ';

			if (!empty($res['ID_DOC']))
				$sql_doc.=' id_doc:'.$res['ID_DOC'];
		}

		if(!empty($sql_doc)){
            $sql_doc='('.$sql_doc.' )';
            $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
            $this->str_recherche.=$sql_doc.'';
        }else{
            $this->str_recherche.=' id_doc:0';
        }
	}

	protected function cherchePanier($tabCrit,$prefix="")
	{
		global $db;
		// trace("SELECT pd.ID_DOC from t_panier p JOIN t_panier_doc pd ON p.ID_PANIER=pd.ID_PANIER WHERE p.".$tabCrit['FIELD'].$this->sql_op($tabCrit['VALEUR'],true));

		$result=$db->Execute("SELECT pd.ID_DOC from t_panier p JOIN t_panier_doc pd ON p.ID_PANIER=pd.ID_PANIER WHERE p.".$tabCrit['FIELD'].$this->sql_op($tabCrit['VALEUR'],true))->getRows();

		$sql_doc='';

		foreach ($result as $idx=>$res)
		{
			if ($idx>0)
				$sql_doc.=' OR ';

			if (!empty($res['ID_DOC']))
				$sql_doc.=' id_doc:'.$res['ID_DOC'];
		}

		if(!empty($sql_doc)){
            $sql_doc='('.$sql_doc.' )';
            $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
            $this->str_recherche.=$sql_doc.'';
        }else{
            $this->str_recherche.=' id_doc:0';
        }
	}

	protected function chercheUsager($tabCrit,$prefix="")
	{
		global $db;
		$vals_recherche=$tabCrit['VALEUR'];
 		$arrWords=$this->analyzeString2($tabCrit['VALEUR'],$op);
		foreach ($arrWords as $i=>$word) if (strlen(trim($word))<=2) unset ($arrWords[$i]); //strip des mots <2 lettres qui empèchent le FT
		$sql = "select DISTINCT ID_USAGER from t_usager where ".$db->getFullTextSQL($arrWords, array("US_NOM", "US_PRENOM", "US_LOGIN"), "", array(), $op);
		$arrIDs=$db->GetAll($sql);
		$value="";
		if(!empty($arrIDs)){
			foreach ($arrIDs as $us)
			{
				if(!empty($value)){
					$value.=' OR ';
				}
				$value.='"'.$us['ID_USAGER'].'"';
			}
		}else{
			$value='"-1"';
		}
		
		$tabCrit['VALEUR']=trim($value);


		if(!empty($tabCrit['VALEUR'])){
            $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$vals_recherche);
            $this->str_recherche.=$this->checkFieldVersionDependant($tabCrit['FIELD']).':('.$tabCrit['VALEUR'].')';
		}
	}

    /** Recherche sur les doc acc liés
     * 	IN : tabCrit, prefix
     * 	OUT : sql
     * 	NOTE : pas de highlight !
     */
    // VP 26/05/13 : création fonction chercheDocAcc
    protected function chercheDocAcc($tabCrit,$prefix) {
        global $db;

        $result=$db->Execute("SELECT ID_DOC from t_doc_acc WHERE  ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true))->getRows();
                         
        $sql_doc='';

        foreach ($result as $idx=>$res)
        {
            if ($idx>0)
                $sql_doc.=' OR ';

            if (!empty($res['ID_DOC']))
                $sql_doc.=' id_doc:'.$res['ID_DOC'];
        }

        if(!empty($sql_doc)){
            $sql_doc='('.$sql_doc.' )';
            $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
            $this->str_recherche.=$sql_doc.'';
        }else{
            $this->str_recherche.=' id_doc:0';
        }
    }
    
    /** Recherche sur les doc acc liés
     * 	IN : tabCrit, prefix
     * 	OUT : sql
     * 	NOTE : pas de highlight !
     */
    // VP 26/05/13 : création fonction chercheDocAcc
    protected function chercheDocAccFT($tabCrit,$prefix) {
        global $db;
        
        $prefix=(!is_null($prefix)?$prefix.".":"");
        $this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN (SELECT ID_DOC from t_doc_acc WHERE ".$db->getFullTextSQL(array($tabCrit['VALEUR']), array($tabCrit['FIELD']));
        
        $result=$db->Execute("SELECT ID_DOC from t_doc_acc WHERE ".$db->getFullTextSQL(array($tabCrit['VALEUR']), array($tabCrit['FIELD'])))->getRows();
                                                  
        $sql_doc='';
        
        foreach ($result as $idx=>$res)
        {
            if ($idx>0)
                $sql_doc.=' OR ';
            
            if (!empty($res['ID_DOC']))
                $sql_doc.=' id_doc:'.$res['ID_DOC'];
        }
        
        if(!empty($sql_doc)){
            $sql_doc='('.$sql_doc.' )';
            $this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
            $this->str_recherche.=$sql_doc.'';
        }else{
            $this->str_recherche.=' id_doc:0';
        }
    }
    
	/** Recherche textuelle sur des fonds
	 * 	IN : tabCrit, prefix , ext_children
	 * 	OUT : sql
	 */
	protected function chercheFonds($tabCrit,$prefix,$ext_children=true) {
		global $db;
		$fld=($tabCrit['FIELD']!=''?$tabCrit['FIELD']:'FONDS');
		
		if (is_array($tabCrit['VALEUR'])) $myVals=implode(",",$tabCrit['VALEUR']);
		else $myVals=$tabCrit['VALEUR'];
		if (empty($myVals)) $myVals='';
		
		if($ext_children){
			// MS - correction de la récupération des fonds fils pour permettre la gestion d'une hierarchie de fonds sur plus de 2 niveaux
			$tmp_arr = explode(',',$myVals);
			$tmp_fonds = $db->GetCol("SELECT $fld from t_fonds where ID_FONDS_GEN in (select id_fonds from t_fonds where $fld ".$this->sql_op($myVals,true).") and ID_LANG=".$db->Quote($_SESSION['langue']));
			
			while(!empty($tmp_fonds)){
				// trace("==== pass chercheFonds debug ====");
				// trace("tmp_fonds ".print_r($tmp_fonds,1));
				// trace("tmp_arr ".print_r($tmp_arr,1));
				$tmp_fonds = array_diff($tmp_fonds,$tmp_arr);
				foreach($tmp_fonds as $f){
					if(!in_array($f,$tmp_arr)){
						array_push($tmp_arr,$f);
						$myVals.=",".$f;
					}
				}
				$tmp_fonds = $db->GetCol("SELECT $fld from t_fonds where ID_FONDS_GEN in (select id_fonds from t_fonds where $fld ".$this->sql_op($tmp_fonds,true).") and ID_LANG=".$db->Quote($_SESSION['langue']));
				
				// trace("SELECT $fld from t_fonds where ID_FONDS_GEN in (select id_fonds from t_fonds where $fld ".$this->sql_op($tmp_fonds,true).") and ID_LANG=".$db->Quote($_SESSION['langue']).' '.print_r($tmp_fonds,1));
			}
			unset($tmp_arr);
			unset($tmp_fonds);
		}
		
		$arrVals = explode(',',$myVals);
		$sql_fonds="";
		foreach ($arrVals as $idx=>$val){
			if ($idx>0)
				$sql_fonds.=' OR ';
			
			$sql_fonds.=' fonds:"'.trim($val).'"';
		}
		if(!empty($sql_fonds)){
		   $this->str_recherche.='('.$sql_fonds.')';
		}
		$this->addEtape($tabCrit['OP'],$tabCrit['LIB'].' : '.$tabCrit['VALEUR']);
		
	}

    private function addEtape($operateur,$str,$fld='',$value=null,$value2=null)
	{
		$this->parsed_etape[] = array(
			'OP'=>$operateur,
			'FIELD'=>$fld,
			'VALEUR'=>$value,
			'VALEUR2'=>$value2,
			'etape'=>$str
		);

		if (!empty($this->etape))
			$this->etape.=' '.$this->tab_libelles[$operateur].' ';

		$this->etape.=$str;
	}

	private function filtreOperateursRequete($str)
	{
		if(is_string($str)){
			$filtre_op=array(' and ',' or ',' not ');
			$replacement=array(' AND ',' OR ',' NOT ');
			$str=str_replace($filtre_op,$replacement,$str);
		}
		return $str;
	}

	public function addFacet($tabCrit,$prefix="")
	{
		$this->solr_query->setFacet(true);
		$this->solr_query->setFacetMinCount(1);

		if(!isset($tabCrit['FIELD']) || empty($tabCrit['FIELD']) || !isset($tabCrit['TYPE']) || empty($tabCrit['TYPE']) ){
			return false ;
		}
		
		// MS - 16.11.16 - possibilité de filtrer sur la base cible, y compris sous forme de constante php
		if(isset($tabCrit['BASE']) && !empty($tabCrit['BASE'])){
			if($this->database != (defined($tabCrit['BASE'])?constant($tabCrit['BASE']):$tabCrit['BASE'])){
				return false ; 
			}
		}


		if (isset($tabCrit['MISSING_AS'])&& !empty($tabCrit['MISSING_AS']))
		{
			$this->solr_query->setFacetMissing(true,$tabCrit['FIELD']);
		}


// MS - je cherchais a pouvoir établir des intervalles spécifiques (pour la durée)
// malheureusement ce fonctionnement n'existera que pour Solr version 4.10 (actuellement en 4.7.2)
		/*if(isset($tabCrit['INTERVAL']) && $tabCrit['INTERVAL'] != ''){
			$intervals = explode('$',$tabCrit['INTERVAL']);

			$facet_query = "&facet=true&facet.interval=".$tabCrit['FIELD'];
			foreach($intervals as $interval){
				$facet_query .= "&f.".$tabCrit['FIELD'].".facet.interval.set=".($interval);
			}
		}*/

		if($tabCrit['TYPE'] == 'FACET_RANGE'){
			//trace(print_r($tabCrit,true));
			// MS implémentation via addParam => seul moyen de définir ces params, le wrapper php d'interaction avec solr ne propose pas de fonction pour ce cas
			if(isset($tabCrit['FIELD']) && !empty($tabCrit['FIELD'])){
				$this->solr_query->addParam('facet.range',"".$tabCrit['FIELD']);
			}
			if(isset($tabCrit['RANGE_START']) ){
				$this->solr_query->addParam('f.'.$tabCrit['FIELD'].'.facet.range.start',"".$tabCrit['RANGE_START']);
			}
			if(isset($tabCrit['RANGE_END']) && !empty($tabCrit['RANGE_END'])){
				$this->solr_query->addParam('f.'.$tabCrit['FIELD'].'.facet.range.end',"".$tabCrit['RANGE_END']);
			}
			if(isset($tabCrit['RANGE_GAP']) && !empty($tabCrit['RANGE_GAP'])){
				$this->solr_query->addParam('f.'.$tabCrit['FIELD'].'.facet.range.gap',"".$tabCrit['RANGE_GAP']);
			}
			if(isset($tabCrit['RANGE_INCLUDE']) && !empty($tabCrit['RANGE_INCLUDE'])){
				$this->solr_query->addParam('f.'.$tabCrit['FIELD'].'.facet.range.include',"".$tabCrit['RANGE_INCLUDE']);
			}
		}else if ($tabCrit['TYPE']=='FACET_HIER'){
			//définition de la facette depuis solr_facet
			$hier_facet = array();
			// si on a un prefix défini, on l'utilise pour déterminer le "key_level" => la profondeur de la facette dans l'arbre hierarchique
			$key_level = 0;
			if(isset($tabCrit['PREFIX'])){
				if(strpos($tabCrit['PREFIX'],'/') !== false){
					$key_level = intval(substr($tabCrit['PREFIX'],0,strpos($tabCrit['PREFIX'],'/')));
				}else if (is_numeric($tabCrit['PREFIX'])){
					$key_level = intval($tabCrit['PREFIX']);
				}
			}

			// définitions des paramètres spécifiques
			// dans le cadre des facettes hierarchiques on est amené à définir de nombreuses facettes avec des critères légèrement variables
			// => on utilise donc les "localParams" solr: via la définition d'une "key", on peut créer de nombreux facet.fields sur un même champ d'un docSolr
			$arr_facet_prms = array('key'=>$tabCrit['FIELD']."-".$key_level);
			if(isset($tabCrit['PREFIX']) ){
				$arr_facet_prms['facet.prefix'] = '"'.str_replace('"','\"',$tabCrit['PREFIX']).'"' ;
			}
			if(isset($tabCrit['REFINE_CRIT'])){
				if(is_numeric($tabCrit['REFINE_CRIT'])){
					$arr_facet_prms['refine_crit'] = $tabCrit['REFINE_CRIT'];
					$arr_facet_prms['key'] = $arr_facet_prms['key']."-ref-".$tabCrit['REFINE_CRIT'] ;
				}else{
					$arr_facet_prms['refine_crit'] = $tabCrit['REFINE_CRIT'];
				}
			}
			$hier_facet[] = $arr_facet_prms;

			//Ajout des paramètres définis précédements sous la forme de localParams
			// ex : &facet.field={!key=hier_ct_thm-0 facet.prefix="0" }hier_ct_thm
			// dans le tableau de retour des facettes depuis Solr, le count des documents satisfaisants ces conditions sera associé au champ "hier_ct_thm-0", bien qu'il soit issu du champ hier_ct_thm
			foreach($hier_facet as $fld=>$params){
				$str_facet_hier = '{!';
				foreach($params as $key=>$val){
					$str_facet_hier.=$key."=".$val." ";
				}
				$str_facet_hier .='}'.$tabCrit['FIELD'];
				$this->solr_query->addParam('facet.field',$str_facet_hier);
			}

			// Ajout paramètre permettant de lancer le addfacet sur un niveau + des sous niveaux,
			// ces inclusions de sous level sont fait en rappelant addfacet en modifiant le paramètre PREFIX
			// ces inclusions peuvent se faire sur plusieurs niveau en utilisant la même technique.
			// => Dans le cas de facettes hierarchiques, on devrait avoir ce paramètre renseigné à 1 dans le solr_facet.xml
			// car cela permet d'avoir un "buffer" : quand un utilisateur choisi de déplier un niveau de hierarchie, on l'a déjà préchargé, et le temps qu'il étudie l'arbre, on a le temps de faire les requêtes ajax remontant les facettes pour le niveau du dessous, pas encore visible pour l'utilisateur.
			if(isset($tabCrit['INCLUDE_SUB_LEVEL']) && isset($tabCrit['PREFIX'])){
				$tabCrit_sub = $tabCrit;
				$tabCrit_sub['PREFIX'] = ($key_level+1).substr($tabCrit['PREFIX'],strlen($key_level));
				if(is_numeric($tabCrit_sub['INCLUDE_SUB_LEVEL']) && intval($tabCrit_sub['INCLUDE_SUB_LEVEL']) > 1){
					$tabCrit_sub['INCLUDE_SUB_LEVEL'] = intval($tabCrit_sub['INCLUDE_SUB_LEVEL']) - 1 ;
				}else{
					unset($tabCrit_sub['INCLUDE_SUB_LEVEL']);
				}
				$this->addFacet($tabCrit_sub);
			}



		}else if ($tabCrit['TYPE']=='FACET_DATE'){
			$this->solr_query->addFacetDateField($tabCrit['FIELD']);
			$this->solr_query->setFacetDateStart('1800-01-01T00:00:00Z',$tabCrit['FIELD']);
			$this->solr_query->setFacetDateEnd(''.(date('Y')+1).'-01-01T00:00:00Z',$tabCrit['FIELD']);
			
			if (isset($tabCrit['OPTIONS']) && !empty($tabCrit['OPTIONS']))
				$facet_gap='+'.$tabCrit['OPTIONS'].'YEAR';
			else
				$facet_gap='+1YEAR';

            $this->solr_query->setFacetDateGap($facet_gap,$tabCrit['FIELD']);
            // VP 9/05/14 : pour contrer le double affichage dû à 00:00:00 (1930-01-01 00:00:00 = 1929-12-31 minuit)
            $this->solr_query->setParam('facet.date.include', 'lower');
		}else{
			$this->solr_query->addFacetField($tabCrit['FIELD']);
            // VP 26/01/2017 : facet.limit à 100 par défaut dans solr, passé à 500 en dûr ici
            $this->solr_query->setFacetLimit(500);
            if(isset($tabCrit['PREFIX']) ){
                $this->solr_query->setFacetPrefix($tabCrit['PREFIX'],$tabCrit['FIELD']);
            }
		}
		// options & paramètres pouvant être appliqués à l'ensemble des facettes
		if (isset($tabCrit['MINCOUNT'])){
			$this->solr_query->addParam('f.'.$tabCrit['FIELD'].".facet.mincount",$tabCrit['MINCOUNT']);
		}
		if (isset($tabCrit['SORT'])){
			$this->solr_query->addParam('f.'.$tabCrit['FIELD'].".facet.sort",$tabCrit['SORT']);
		}
	}

    /**
     * Détermine la paramètre refine
     * IN : refine, reset
     * OUT : maj var refine
	// MS - report info solr.js sur les valeurs de reset_refine: 
	// 0 -> on applique le refine, mais on ne recompose pas la requete SQL (pas de prepareSQL)
	// 1 -> on recompose la requete SQL (prepareSQL) mais on n'applique pas le refine
	// 2 -> on recompose la requete SQL ET on applique le refine 
        
     */
    function setRefine($str_refine='', $reset_refine='',$refine_facet =null ) {
		// ajout nouvelle valeur reset_refine == 2 qui agit ici comme un bypass => reset_refine à 2 passe donc par prepareSQL mais maintien le refine
        if($reset_refine == 1){
            $this->str_refine = '';
        } elseif(!empty($str_refine)) {
			// dans le cas où on a un paramètre str_refine dans l'appel, 
			// c'est qu'on a un refine en paramètre de la recherche, et qu'on n'est pas dans le cas "reset_refine" 
			// => donc il y a une probable modification du nombre de résultats et donc du nombre de pages
			// => donc on repasse en page 1 (comme lorsqu'on change le nombre de résultats par pages)
			$this->setPage(1);
            $this->str_refine = $str_refine;
        } else{
			if($str_refine === null ){
				$this->prevent_call_refine = true ; 
			}
			if(isset($_SESSION[$this->sessVar]['refine']) && !empty($_SESSION[$this->sessVar]['refine'])) {
				$this->str_refine = $_SESSION[$this->sessVar]['refine'];
			} else {
				$this->str_refine = '';
			}
		}
		if($refine_facet != null){
			$this->refine_facet = $refine_facet;
		}
     }

    function refine ($string='' )
	{
        //trace("chercheSolr refine : ".$string);
		if(empty($string))
            $string=$this->str_refine;
        
		if(isset($this->refine_facet ) && !empty($this->refine_facet )){
			$facet_prms = json_decode($this->refine_facet ,true); 
			if(is_array($facet_prms) && isset($facet_prms['FIELD']) && isset($facet_prms['TYPE'])){
				$this->addFacet($facet_prms);
			}
		}
		
		if (!empty($string) && (!isset($this->prevent_call_refine) || !$this->prevent_call_refine))
		{
			$obj_refine = json_decode($string); // valeur envoyées dans POST[refine]
			$arr_facet_to_add = array();

			// trace("refine : ".print_r($obj_refine,true));

			$filterQueries =$this->solr_query->getFilterQueries();
			if(!empty($filterQueries)){
				foreach($filterQueries as $idx=>$fq){
					if(strpos($fq,'keepalive=1') === false  && strpos($fq,'form_crit=1') === false  ) {
						$this->solr_query->removeFilterQuery($fq);
					}
				}
			}
			$facets =$this->solr_query->getFacetFields();
			// trace("fn refine removeFacetFields => clear facets");
			if(!empty($facets)){
				foreach($facets as $idx=>$facet){
					if(strpos($facet,'refine_crit=')!==false){
						// trace("refine removefacets : ".print_r($facet,true));
						$this->solr_query->removeFacetField($facet);
					}
				}
			}
			
			
			foreach($obj_refine as $idx=>$refine_unit){
				if(strpos($refine_unit->type,'date')!==false){
					if(strpos($refine_unit->type,',')!==false){
						$exp = explode(',',$refine_unit->type);
						$str = "";
						foreach($exp as $f){
							if($str!=''){$str.=' OR ';}
							$str.=$f.":".$refine_unit->value;
						}
						$this->solr_query->addFilterQuery($str);
					}else{
						$this->solr_query->addFilterQuery($refine_unit->type.":".$refine_unit->value);
					}
				}else if ($refine_unit->value == '_undefined_property_name'){
					if(strpos($refine_unit->type,',')!==false){
						$exp = explode(',',$refine_unit->type);
						$str = "";
						foreach($exp as $f){
							if($str!=''){$str.=' OR ';}
							// $str.=$f.":\"".$refine_unit->value."\"";
							$str.=" NOT (".$f.":*)";
						}
						$this->solr_query->addFilterQuery($str);
					}else{
						// $this->solr_query->addFilterQuery($refine_unit->type.":\"".$refine_unit->value."\"");
						$this->solr_query->addFilterQuery(' NOT ('.$refine_unit->type.":*)");
					}
				}elseif(strpos($refine_unit->type,'hier_')!==false){
				// cas refine hierarchique
				// la partie "refine" est relativement standard, ex : hier_ct_thm : 2/Nature/Flore/Fleurs
				// On doit cependant aussi ajouter des facettes à la requete (cf addFacet)
				//=>  doit permettre d'ajouter la facette du niveau "recherché"
				// + les facettes du niveau "+1" par rapport au terme recherché (pour faciliter la navigation dans l'arbre des facettes et ne pas avoir a attendre trop ....)
				// + toutes les facettes parentes jusqu'au niveau 0 (root) de la hierarchie

					//Partie facettes :
					$hier_path = explode('/',$refine_unit->value);
					$num_prefix = $hier_path[0];
					for ($i = 0 ; $i<=$num_prefix+1 ; $i++){
						$str = $i;
						for($j = 1 ;$j<=($i+1) && $j <count($hier_path);$j++){
							$str.="/".$hier_path[$j];
						}
						// on génère un tableau de valeurs pouvant être passé à la fonction addFacet
						$mock_tabCrit = array('FIELD'=>$refine_unit->type,'TYPE'=>'FACET_HIER','PREFIX'=>$str,'REFINE_CRIT'=>$idx);
						// Etant donné la nature du facettage hierarchique, on peut se retrouver avec des facettes équivalentes pour différents critères de refine, on va donc vérifier qu'il n'y a pas déjà de facette équivalente dans $arr_facet_to_add
						$found = false ;
						foreach($arr_facet_to_add as $facet){
							// Si on a pas les bons paramètres, ou si on est pas de type hierarchique, ça ne sert à rien de continuer à tester dessus
							if(!isset($facet['FIELD']) || !isset($facet['TYPE']) || $facet['TYPE'] != 'FACET_HIER'|| !isset($facet['PREFIX'])){
								continue;
							}
							// Si on trouve une facette équivalente, on change le flag "found" et on sort du foreach
							if($facet['FIELD'] == $mock_tabCrit['FIELD'] && $facet['PREFIX'] == $mock_tabCrit['PREFIX'] ){
								$found = true ;
								break ;
							}
						}
						// Seules les facettes pour lesquelles on n'a pas d'équivalent sont ajoutées à la recherche
						if(!$found){
							$arr_facet_to_add[] = $mock_tabCrit;
						}
					}

					$this->solr_query->addFilterQuery($refine_unit->type.":\"".$refine_unit->value."\"");

				}else{
					if(strpos($refine_unit->type,',')!==false){
						$exp = explode(',',$refine_unit->type);
						$str = "";
						foreach($exp as $f){
							if($str!=''){$str.=' OR ';}
							$str.=$f.":\"".$refine_unit->value."\"";
						}
						$this->solr_query->addFilterQuery($str);
                        /* Si la valeur est encadré par [], à priori il s'agit d'un range de valeur ex: durée en secondes */
					}elseif(preg_match('/^\[.+\]$/', $refine_unit->value)){
						$this->solr_query->addFilterQuery($refine_unit->type.":".$refine_unit->value);
					}else{
						$this->solr_query->addFilterQuery($refine_unit->type.":\"".$refine_unit->value."\"");
					}
				}
			}
			if($filterQueries == $this->solr_query->getFilterQueries()){
				$omit_reset_page = true ;
			}else{
				$omit_reset_page = false ;
			}
			// ajout des facettes necessaires à la requête(pour l'instant uniquement pr les refine hierarchique)
			foreach($arr_facet_to_add as $tab_facet){
				$this->addFacet($tab_facet);
			}

			if($this->useSession){
				$this->putSearchInSession("",$omit_reset_page);
			}
		}
		else
			return null;
	}


	public function appliqueDroits()
	{
		$usr=User::getInstance();

		$str_droits='';
		if(!empty($usr->Groupes)){
			$first=true;
			foreach($usr->Groupes as $value)
			{
				if ($value["ID_PRIV"]!=0)
				{
					if ($first==false)
						$str_droits.=' OR ';

					$str_droits.='doc_id_fonds:'.$value["ID_FONDS"];
					$first=false;
				}
			}
		}
		if(!empty($str_droits)){
            if (!empty($this->str_recherche)){
                $this->str_recherche='('.$this->str_recherche.') AND ('.$str_droits.')';
            }else{
                $this->str_recherche='('.$str_droits.')';
            }
        }

		if(function_exists("appliqueDroitsCustom")){
            $this->str_recherche.=appliqueDroitsCustom($this->str_recherche,$this);
        }
	}

	function setDatabase($db)
	{
		$this->database=$db;
	}

    function getSearchFromSession() {
        if(isset($_SESSION[$this->sessVar]['searchObj'])){
			// récupération de l'objet RechercheSolr depuis la session, cf RechercheDocSolr::putSearchInSession pour voir comment on réalise le stockage
			foreach (get_object_vars($_SESSION[$this->sessVar]['searchObj']) as $key => $value) {
				try{
					if(is_object($value)){	
						$this->$key = unserialize(serialize($value));
					}else{
						$this->$key = $value;
					}
				}catch(Exception $e){
					trace("crash - chercheSolr getSearchFromSession ".$e->getMessage());
					$this->$key = $value ;
				}
			}
		}
		
		// MS workaround correction d'un bug lié au tri : 
		// il faut mettre à clair la notion de chargement / stockage dans la session pour les objets de la class_chercheSolr, car actuellement le searchObj n'est modifié que lorsqu'on fait une réelle sauvegarde d'une nouvelle recherche, donc lorsqu'il s'agit d'une nouvelle recherche complete (pas pour les tri / refine), mais on se base ensuite sur ce meme objet stocké en session pour charger l'état courante de la recherche avant d'envoyer les paramètres dans les params4XSL, ce qui faisait que dans certains cas (apres de multiples manipulations des tris), on pouvait se retrouver avec un tabOrder n'ayant pas la meme valeur dans l'execution du controller et dans l'execution de la vue. 
		if(isset($_SESSION[$this->sessVar]['tabOrder'])){
			$this->tabOrder = $_SESSION[$this->sessVar]['tabOrder'];
		}
		/*
		//Commenté mais conservé pour backward compat
		else if(isset($_SESSION[$this->sessVar])){
			$this->etape=$_SESSION[$this->sessVar]["etape"];
			$this->solr_query=$_SESSION[$this->sessVar]["sql"] ;
			$this->tab_recherche=$_SESSION[$this->sessVar]["form"] ;
			$this->tabHighlight=$_SESSION[$this->sessVar]["highlight"];
			$this->rows = $_SESSION[$this->sessVar]["rows"]; //@update VG 08/04/2010
        }*/
    }

    function putSearchInSession($id="",$omit_reset_page = false)
	{
		// On veut stocker l'objet mySearch à l'instant T pour le stocker dans la session
		// On doit donc réaliser une copie de l'objet et la stocker en session, on ne peut pas juste passer la référence car l'objet en session continuerait d'être modifié en même temps que $this, 
		// Par ailleurs, dans l'objet solr, l'object SolrClient permettant d'établir la connexion n'est pas serializable et donc pas stockable en session
		$copy_this = new RechercheSolr() ; 
		foreach (get_object_vars($this) as $key => $value) {
			// Pour une raison inconnue l'objet SolrClient php natif (stocké dans la variable this->solr_client) n'est pas serializable et cause des erreurs si on tente de le conserver. 
			// Etant donné la nature de l'objet SolrClient qui établi uniquement la connexion avec la base, il n'est pas dramatique de l'omettre de la session. 
			if($key != 'solr_client'){ 
				$copy_this->$key = $value;
			}
		}
		$_SESSION[$this->sessVar]["searchObj"] = $copy_this;
		
		$_SESSION[$this->sessVar]["etape"] = $this->etape;
		$_SESSION[$this->sessVar]["parsed_etape"] = $this->parsed_etape;
		$_SESSION[$this->sessVar]["sql"] = $this->solr_query;
		$_SESSION[$this->sessVar]["form"] = $this->tab_recherche;
		$_SESSION[$this->sessVar]["hist"] = $id;
		if(!$omit_reset_page){
			$_SESSION[$this->sessVar]["page"] = "1";
		}
		$_SESSION[$this->sessVar]["rows"] = $this->rows;
	}

	public function saveSearchInDB($requete)
	{
		$myQuery = New Requete();

		$myQuery->t_requete['REQ_SQL']=$this->str_recherche.str_replace('&fq=','&amp;fq=',$this->str_recherche_fq);
		$myQuery->t_requete['REQ_ENGINE']='Solr';
		$myQuery->t_requete['REQ_LIBRE']=$this->etape;
		//var_dump(mb_detect_encoding($this->str_recherche));
		$exist=$myQuery->checkExist();
		if ($exist) {

			$myQuery->t_requete['ID_REQ']=$exist;
			$myQuery->getRequete();
		}

		$myQuery->t_requete['REQ_NB_DOC']=$this->found_rows;
		$myQuery->t_requete['REQ_LIBRE']=$this->etape;
		$myQuery->t_requete['REQ_PARAMS']=$this->tab_recherche;
		$myQuery->t_requete['REQ_DATE_EXEC']=date('Y-m-d');

		if (!empty($requete['SAUVE']))
		{
			if(!empty($requete['REQUETE']))
			{
				$myQuery->t_requete['REQ_ALERTE']=$requete['REQ_ALERTE'];
				$myQuery->t_requete['REQ_SAVED']=1;
				$myQuery->t_requete['REQUETE']=$requete['REQUETE'];
			}
			else
			{
				$myQuery->t_requete['REQ_ALERTE']=0;
				$myQuery->t_requete['REQ_SAVED']=0;
			}
		}

		//$myQuery->t_requete['REQ_DB']=;
		//$this->requete['REQUETE']=$myQuery->t_requete['REQUETE'];
		//$this->requete['REQ_ALERTE']=$myQuery->t_requete['REQ_ALERTE'];
		//var_dump($myQuery);
		$myQuery->save(false);
	}

	public function getSearchFromDB($id)
	{
		$this->prepareSQL();
		$myQuery=new Requete();
		$myQuery->t_requete['ID_REQ']=intval($id);
		$myQuery->getRequete();

		$this->rows = $myQuery->t_requete['REQ_NB_DOC'];
		$this->etape=$myQuery->t_requete['REQ_LIBRE'];
		// MS - 30.03.17 - suite à l'ajout de removescripttags, les caractères spéciaux sont échappés dans REQ_SQL, on doit donc les retransformer lors d'une récupération depuis la base
		$this->str_recherche = htmlspecialchars_decode($myQuery->t_requete['REQ_SQL'],ENT_NOQUOTES | ENT_HTML401);
		
		$tokens_recherche = preg_split('/(?:&q=|&fq=|&amp;fq)/',$this->str_recherche);
		
		//trace("getSearchFromDB tokens_recherche ".print_r($tokens_recherche,1));
		if(is_array($tokens_recherche)){
			$this->str_recherche = $tokens_recherche[0];
			unset($tokens_recherche[0]);
			if(!empty($tokens_recherche)){
				$filter_queries = $tokens_recherche;
			}
		}
		
		$this->tab_recherche=$myQuery->t_requete['REQ_PARAMS'];

		// B.RAVI 2016-05-06 Eviter Warning: Invalid argument supplied for foreach()
		if (is_object($this->tab_recherche) || is_array($this->tab_recherche)){
			foreach ($this->tab_recherche as $critere)
			{
				$fields_hl=explode(',',$critere['FIELD']);
				foreach ($fields_hl as $fl)
					$this->ajouterChampHighlight($fl,$critere['NOHIGHLIGHT']);

				if (strstr($critere['TYPE'],'FACET'))
					$this->ChooseType($critere,"");
			}
		}
		// MS - les highlights ne sont pas stockés en base, pour tenter d'obtenir un highlight à minima, on active pour l'instant hl.fl sur tous les champs .. 
		$this->ajouterChampHighlight('*');
		
		//$this->requete['REQUETE']=$myQuery->t_requete['REQUETE'];
		//$this->requete['REQ_ALERTE']=$myQuery->t_requete['REQ_ALERTE'];
		$this->finaliseRequete();
		
		if(!empty($filter_queries)){
			foreach($filter_queries as $fq){
				$this->solr_query->addFilterQuery($fq);
				$this->str_recherche_fq .= '&fq='.$fq;
			}
		}
		
		//trace(print_r('getSearchFromDB solr_query : '.$this->solr_query,true));
		$this->putSearchInSession($id);
	}

	public function ajouterChampHighlight($field,$no_highlight='')
	{
		if(isset($no_highlight) && !empty($no_highlight)){
			$this->champs_noHls[]=$field;
		}

		if (!in_array($field,$this->champs_highlight) && empty($no_highlight))
		{
            // VP 19/12/2014 : gSolrFieldsToHighlight peut maintenant être un tableau à 2 dimensions pour préciser plusieurs index multi-champs
			$fields=array();
            if(defined("gSolrFieldsToHighlight") && is_array(unserialize(gSolrFieldsToHighlight))){
				$fieldsToHighlight = unserialize(gSolrFieldsToHighlight);
                $keys=array_keys($fieldsToHighlight);
                if(is_array($fieldsToHighlight[$keys[0]])){
                    if(in_array($field, $keys )){
                        $fields=$fieldsToHighlight[$field];
                    }
                }elseif($field == "text" || $field == "text_exact"){
                    $fields=$fieldsToHighlight;
                }
            }
            if(count($fields)>0){
				foreach($fields as $fld){
					$fld = $this->checkFieldVersionDependant($fld); // MS123
					$this->solr_query->addHighlightField($fld);
					$this->champs_highlight[]=$fld;
				}
//			if($field == "text" && defined("gSolrFieldsToHighlight") && is_array(unserialize(gSolrFieldsToHighlight))){
//				$fields = unserialize(gSolrFieldsToHighlight);
//				foreach($fields as $fld){
//					$this->solr_query->addHighlightField($fld);
//					$this->champs_highlight[]=$fld;
//				}
			}else{
				$field = $this->checkFieldVersionDependant($field);
				$this->solr_query->addHighlightField($field);
				$this->champs_highlight[]=$field;
			}
		}

	}

	public function finaliseRequete()
	{
		//MS 09.07.14 - Si aucun champ n'a été défini comme highlightable, alors on désactive le highlight
		$arr_test_highlight = $this->solr_query->getHighlightFields();
		if(empty($arr_test_highlight)){
			$trace_hl = "highlight : false";
			$this->solr_query->setHighlight(false);
		}else{
			$trace_hl = "highlight : true, fields :".implode(',',$arr_test_highlight);
		}
		unset($arr_test_highlight);

		if (defined('gUseIdLangFieldSolr') && gUseIdLangFieldSolr==false)
		{
			$trimStrRecherche = trim($this->str_recherche,'() \t\n\r\0\x0B');
			if (!empty( $trimStrRecherche) ){
                $this->str_recherche=trim($this->str_recherche);
                if (strpos($this->str_recherche,"AND")===0){ //retrait du premier opérateur (évite le AND...)
                    $this->str_recherche=substr($this->str_recherche,3);
                }else if (strpos($this->str_recherche,"OR")===0){ //retrait du premier opérateur OR 
                    $this->str_recherche=substr($this->str_recherche,2);
                }
				$this->solr_query->setQuery('('.$this->str_recherche.')');
			}else
				$this->solr_query->setQuery('*:*');

            trace('finaliseRequete :: '.$this->str_recherche);
		}
		else
		{
			if (!empty($this->str_recherche))
				$this->solr_query->setQuery('('.$this->str_recherche.') AND id_lang:"'.$_SESSION['langue'].'"');
			else
				$this->solr_query->setQuery('id_lang:"'.$_SESSION['langue'].'"');

            trace('finaliseRequete ::('.$this->str_recherche.') AND id_lang:"'.$_SESSION['langue'].'"');
		}


		if ($this->useSession){
			$this->putSearchInSession();
		}
	}


    /**
     // VP (9/04/14) : reprise fonction sql_op
     * ComplŽtion "intelligente" selon les critres : si XX* -> LIKE, si array -> IN(,,) sinon =
     * IN : valeur (string comprenant 1 ou plusieurs vals), $keepIntact=>pas d'analyse de la chaine (utilisŽ pr DOC_COTE par ex)
     * OUT : string SQL
     */
    protected function sql_opSolr($field,$valeur,$keepIntact=false,$onlyAlpha=false) {
        global $db;
        if (is_array($valeur)) $valeur=implode(',',$valeur);
        if (is_numeric($valeur) || $keepIntact) $arrWords=explode(",",$valeur);//une seule valeur, de type ID (numŽrique)
        else $arrWords=$this->analyzeString2($valeur,$op); //DŽcoupage intelligent des mots.

		$field = $this->checkFieldVersionDependant($field);

        $expr=$field.':(';
        foreach($arrWords as $idx => $word){
            if($onlyAlpha){
                $word=preg_replace('(\s)','_',trim($word));
            }
            if($idx>0)
                $expr.=" OR ";
            if(strpos($valeur,"*")===false && $keepIntact)
                $expr.='"'.trim($word).'"';
            else
                $expr.=trim($word);
        }
        $expr.=')';

        return $expr;
    }

	 static function analyzeString2($valeur_champ,&$arrOps,$smartParsing=false) {
		// 24-04-14 MS surcharge de la fonction analyzeString2
		// met en place la possibilité de bloquer les traitements liés au spaceQuotes (stockage des single quote encadrés par 2 espaces en base)
		// Actuellement, dans postgres, on ne devrait pas avoir d'espacement des quotes,
		// l'export vers solr génère donc des lexiques sans espaces, exemple : "l'europe" => un seul terme stocké dans solr => doit être cherché comme tel, et non comme (l) and (europe)
		// à revoir ...

		// version non récursive OK :
		// $smart_keywords = preg_split("/[\s]*(\\([\s\S]+?\\))|[\s]*(\\\"[^\\\"]+\\\")|[\s]*?(OR)[\s]+?([\s\S]*?)|[\s]*(AND)[\s]+?([\s\S]*?)/",
		 // $valeur_champ, 0, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY  );

	 	if (is_array($valeur_champ)) $valeur_champ=implode(',',$valeur_champ);

		if($smartParsing){
			if(!function_exists('recurs_split')){
				function recurs_split($str){
					if(defined("gMotsVidesAnalyzeStringSolr")  && is_array(unserialize(gMotsVidesAnalyzeStringSolr))){
						$arr_mots_vides = unserialize(gMotsVidesAnalyzeStringSolr);
					}

					$str = trim(preg_replace( "/\s+|\(\)/", " ", $str));

					$arr = preg_split("/[\s]*(\\\"[^\"]+\\\")|[\s]*([(][\S\s^)]+[)])|([\s]+?(?:or|ou|oder|OR|OU|ODER|and|et|und|AND|ET|UND|not|sauf|nicht|NOT|SAUF|NICHT)[\s]+?|\s*(?:!|,)+\s*|(?:\s|^)+(?:-)+\s*)([\s\S]*?)/",
					$str, 0, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY  );
					$arr2 = array();
					$curr_op;

					foreach($arr as $idx=>&$chunk){
						if(in_array(trim($chunk),array('OU','ou','OR','or','ODER','oder',','))){
							$curr_op = 'OR';
						}else if (in_array(trim($chunk),array('ET','et','AND','and','UND','und'))){
							$curr_op = 'AND';
						}else if (in_array(trim($chunk),array('SAUF','sauf','NOT','not','NICHT','nicht','!','-'))){
							$curr_op = 'NOT';
						}else{
							$arr_new = array();
							if(isset($curr_op) && !empty($curr_op)){
								$arr_new['OP'] = $curr_op;
							}else{
								$arr_new['OP'] = 'AND';
							}
							unset($curr_op);
							$arr_new['SUBQUERY'] = $chunk;
							// MS - 10.11.16 - La simple quote a été supprimée des valeurs du preg_split, l'élision est maintenant gérée par Solr (filtre ElisionFilterFactory)
							$tmp_tokens = preg_split( "/[\s]*(\\\"[^\\\"]+\\\")([\s,]*)|([\s,!-]+)/",
							$chunk, 0, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY );

							foreach($tmp_tokens as $idx=>$token){
								if(trim($token)!='' && trim($token) != "'"&& trim($token) != "-" &&  trim($token) != ":"){
									if(!isset($arr_mots_vides) || empty($arr_mots_vides) || !in_array(strtolower($token),($arr_mots_vides))){
										$arr_new['TOKENS'][]=$token;
									}
								}
							}
							$arr2[] = $arr_new;
							unset($arr_new);
						}
					}
					unset($idx);
					unset($chunk);
					unset($arr);
					foreach($arr2 as $idx=>&$chunk){
						if(substr($chunk['SUBQUERY'],0,1) == '(' && substr($chunk['SUBQUERY'],-1)==')'){
							unset($chunk['TOKENS']);
							$chunk['SUBQUERY'] = recurs_split(substr($chunk['SUBQUERY'],1,-1));
						}
					}
					unset($idx);
					unset($chunk);
					return $arr2;
				}
			}

			$smart_keywords = recurs_split($valeur_champ);

			return $smart_keywords;

		}else{
			if (defined("gQueryAddSpaceQuotes") && gQueryAddSpaceQuotes  == false ){
				$keywords = preg_split( "/[\s]*(\\\"[^\\\"]+\\\")([\s,]*)|([\s,!]+)/",
				 $valeur_champ, 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY );
			}else{
				$keywords = preg_split( "/[\s]*(\\\"[^\\\"]+\\\")([\s,]*)|([\s,!']+)/",
				 $valeur_champ, 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY );

			}


			//En sortie d'analyse ici, on a un tableau contenant tous les mots, et les séparateurs (, espace ' )
			//Les expressions entre guillemets sont préservées ainsi que les mots-composés
			//Les caractères spéciaux comme @ sont gérés comme des caractères normaux
			//Les chaînes < 2 caractères sont gardées ici, elle seront ôtées par ailleurs

			$keywords=array_values($keywords);

			if(!empty($keywords))
			foreach ($keywords as $idx=>$wd) {
				//update VG 18/07/2011 : on ne retourne plus les mots ne contenants que "*"
				if (trim($wd)=='' || trim($wd)=="'" || trim($wd)=="*" || trim($wd)=="!" || trim($wd)=='AND') continue;//on vire les ' seuls
				//virgule seule ? on place les 2 termes attenants en opérateur + (ou)
				if (trim($wd)==',' || trim($wd)=='OR' ) {$arrOps[$idx-1]='+';$arrOps[$idx+1]='+';}
				elseif (trim($wd)=='-' || trim($wd)=='NOT') {$arrOps[$idx+1]='-';} //tiret seul ? on place le terme suivant en opérateur -
				//tiret au début ? on met l'opérateur - et on retire le - du mot
				elseif ($wd[0]=='-') {$arrOps[$idx]='-';$myWords[$idx]=str_replace("'"," ' ",trim($wd,'-'));}
				// VP (26/8/09) + au début ? on met l'opérateur + et on retire le + du mot
				elseif ($wd[0]=='+') {$arrOps[$idx]='+';$myWords[$idx]=str_replace("'"," ' ",trim($wd,'+'));}
				else if (defined("gQueryAddSpaceQuotes") && gQueryAddSpaceQuotes  == false ) $myWords[$idx]=$wd; //mot ou expr entre "" normale
				else  $myWords[$idx]=str_replace("'"," ' ",$wd); //mot ou expr entre "" normale
			}

			if( !empty($myWords) && defined("gMotsVidesAnalyzeStringSolr")  && is_array(unserialize(gMotsVidesAnalyzeStringSolr))){

				$filtered_words = array();
				$arr_mots_vides = unserialize(gMotsVidesAnalyzeStringSolr);
				foreach($myWords as $idx=>$word){
					if(!in_array(strtolower($word),($arr_mots_vides))){
						$filtered_words[$idx] = $word;
					}
				}
				$idx = 0 ; 
				$myWords = array() ; 
				$tmp_arrOps = array() ; 
				foreach($filtered_words as $f_idx =>$f_word){
					$myWords[$idx] = $f_word;
					if(isset($arrOps[$f_idx]) && !empty($arrOps[$f_idx])){
						$tmp_arrOps[$idx] = $arrOps[$f_idx];
					}
					$idx++;
				}
				$arrOps = $tmp_arrOps;
				unset($tmp_arrOps);
				unset($filtered_words);
			}
		}
		//debug($myWords,'yellow');
		//debug($arrOps,'pink');


		return $myWords;
	}

	function initArrFieldsToSaveWithIdLang(){
		if(!isset($this->arrFieldsToSaveWithIdLang) ){
			require_once(modelDir.'model_doc.php');
			$tmpDoc=  new Doc() ;
			$this->arrFieldsToSaveWithIdLang = $tmpDoc->getArrFieldsToSaveWithIdLang();
			$this->arrFieldsToSaveWithIdLang = array_map('strtolower',$this->arrFieldsToSaveWithIdLang);
			$this->arrFieldsToSaveWithIdLang[] ='text';
			unset($tmpDoc);
		}
	}

	function checkFieldVersionDependant($field){
		$this->initArrFieldsToSaveWithIdLang();
		// MS 01.04.16 - Ajout reconnaissance des champs sort_<champ_texte> (ex sort_doc_titre(champ solr de type string non parsé) pour trier les doc_titre) afin de le remplacer par sort_<champ_texte>_<langue>
		if((in_array(strtolower($field),$this->arrFieldsToSaveWithIdLang) || in_array(strtolower(preg_replace("/^sort_/",'',$field)),$this->arrFieldsToSaveWithIdLang))
		&& preg_match('/_'.strtolower($_SESSION['langue']).'$/',$field) === 0 && strtolower($field) != 'doc_acces'){
			$field = $field."_".strtolower($_SESSION['langue']);
		}
		return $field ;

	}
	
	public function browseIndex($field,$criteres='',$nbLigne=100,$alphanum_sort=false){				
		//création requete solr
		$solr_query=new SolrQuery();
		$solr_query->setTerms(true);
		$solr_query->setTermsLimit(-1);
		$solr_query->addParam("terms.fl",strtolower($field));
		if(!empty($criteres)){
			$solr_query->set("terms.regex",$criteres);
			$solr_query->set("terms.regex.flag","case_insensitive");
		}
		$solr_query->setTermsLimit($nbLigne);
		if($alphanum_sort){
			$solr_query->addParam('terms.sort','index');
		}
		
		// creation client solr
		$solr_opts=unserialize(kSolrOptions);
		if (!empty($this->database))
		{
			$solr_opts['path']=$this->database;
		}
        if (!empty($_SESSION['DB']))
        {
            $solr_opts['path']=$_SESSION['DB'];
        }
		$solr_client=new SolrClient($solr_opts);
		
		
		// execution de la requete
		$result=$solr_client->query($solr_query);
	
		if(!empty($result)){
			$results_tmp = $result->getResponse()->terms->offsetGet(strtolower($field)); 
			foreach($results_tmp as $key=>$res){
				$final_results[$key] = $res;
			}
			
			return $final_results;
		}else{
			return false ; 
		}
		
	}
	
	public static function browseIndexByFacet($field,$tabCrit='',$nbLigne=100){
		//création requete solr
		$start = microtime(true);
		
		if(empty($nbLigne)){
			$nbLigne = 100 ;  
		}
		
		if(is_array($tabCrit)){
			$tmp = reset($tabCrit);
			if(!is_array($tmp) && !empty($tmp)){
				$tabCrit = array($tabCrit);
			}
		}
		
		
		$srch = new RechercheSolr() ; 
		$srch->useSession = false ; 
		$srch->prepareSQL() ; 
		if(!empty($tabCrit)){
			foreach($tabCrit as $crit){
				if(isset($crit['FIELD']) && !empty($crit['FIELD'])){
					$crit['FIELD'] = strtolower($crit['FIELD']);
				}
				if(!isset($crit['TYPE'])){
					$crit['TYPE'] = 'CE';
				}
				$srch->ChooseType($crit,'');
			}
		}
		
		$crit_facet = array(
			'TYPE' => 'FACET',
			'FIELD' => strtolower($field),
			'SORT' => 'index'
		);
		
		$srch->addFacet($crit_facet);
		$srch->finaliseRequete() ; 
		
		$srch->execute($nbLigne,0, false);
		
		$arrIndex = array() ;
		foreach($srch->arr_facet as $facet){
			if($facet['field'] == strtolower($field)){
				$arrIndex[] = array('VALEUR'=>$facet['key'],'NB'=>$facet['valeur']);
			}
		}
	
		trace("browseIndexByFacet temps d'exec : ".(microtime(true) - $start));
		return $arrIndex;
	}
	
	
	public static function quickSearch($tabCrit='',$nbLigne=100,$offset=0,$returnObject=false){
		//création requete solr
		$start = microtime(true);
		
		if(empty($nbLigne)){
			$nbLigne = 100 ;  
		}
		
		if(is_array($tabCrit)){
			$tmp = reset($tabCrit);
			if(!is_array($tmp) && !empty($tmp)){
				$tabCrit = array($tabCrit);
			}
		}
		
		
		$srch = new RechercheSolr() ; 
		$srch->useSession = false ; 
		$srch->start_offset = $offset ; 
		$srch->prepareSQL() ; 
		if(!empty($tabCrit)){
			foreach($tabCrit as $crit){
				if(!isset($crit['TYPE'])){
					$crit['TYPE'] = 'CE';
				}
				$srch->ChooseType($crit,'');
			}
		}
		
		$srch->finaliseRequete() ; 
		$srch->execute($nbLigne,0, false);
	
		trace("quickSearch temps d'exec : ".(microtime(true) - $start));
		if($returnObject){
			return $srch;
		}else if (isset($srch->result)){
			return $srch->result;
		}else{
			return false ; 
		}
	}
	
	
	
	
	
	

    /**
     * Execution requête
     * IN : solr query, max_rows (nb de lignes max), $secstocache (compatibilité adodb), $highlight (flag)
     * OUT : var de classe : Result (resultat de requete "tronqué"),
     * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales)
	 
	 * Pour rappel & report des corrections : code principalement issu de PageSolr:InitPager en version opsis 3.0.X
     */
    function execute($max_rows = 10, $secstocache=0, $highlight=true)
    {
        
        if(empty($this->solr_query))
            return;
       
        
        if (!isset($this->nbLignes))
            $this->max_rows = $max_rows;
        else
            $this->max_rows=$this->nbLignes;
        
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) )
            $this->page=1;

        // array contenant le contenu d'une requete solr
        
        if ($this->max_rows!="all")
        {
            if(isset($this->start_offset) ){
                $this->solr_query->setStart(($this->start_offset));
            }else{
                $this->solr_query->setStart(($this->page-1)*$this->max_rows);
            }
            $this->solr_query->setRows($this->max_rows);
            
			// limites sur les termes
			// MS - 16.12.16 - Restauration code supprimé, la recherche solr peut effectivement être utilisé avec une requete solr de recherche sur les termscomponents. Ajout d'un test vérifiant si la solr_query est bien relative à des terms solr
			if($this->solr_query->getTerms()){
				$start_term=($this->page-1)*$this->max_rows;
				$this->solr_query->setTermsLimit($this->page*$this->max_rows);
			}
        }
        
        $solr_opts=unserialize(kSolrOptions);
        
        if (!empty($this->database))
        {
            $solr_opts['path']=$this->database;
        }
        if (!empty($_SESSION['DB']))
        {
            $solr_opts['path']=$_SESSION['DB'];
        }
		if(is_array($solr_opts) && !empty($solr_opts)){
			$this->solr_client=new SolrClient($solr_opts);
		}else{
			$this->error_msg = "Problem initalizing SolrClient";
			return false ; 
			
		}
        if($this->solr_query->getHighlight() === false){
            $highlight = false ;
        }
        
        // MS - 24/11/14 - ajout handling exception solr, affichage "problem with request" lorsque le getQuery fail... on ne plante plus l'intÈgralitÈ du site...
		try{
			trace("solrquery : \n=======\n".str_replace('&',"\n&",(string)$this->solr_query)."\n=======");
            $response=$this->solr_client->query($this->solr_query);
        }catch(Exception $e){
            $this->rows=0;
            $this->found_rows=0;
            $this->page=1;
            $this->PagerLink="";
            $this->result=array();
            $this->error_msg="Problem with Solr request";
			// permet de catcher le message spécial de pb de connexion au serveur 
			// NE PERMET PAS de catch les message de parsing de la requête etc. Mais d'après moi on ne veut pas necessairement les indiquer au client ... 
			preg_match("/(?:Solr HTTP Error [0-9]+: '([\S\s]+)')|/",$e->getMessage(),$ex_matches);
			if(isset($ex_matches[1]) && !empty($ex_matches[1])){
				$this->error_msg.=' : '.$ex_matches[1];
			}
			if(strpos($e->getMessage(),'Cannot parse') !== false){
				$this->error_msg.=' : error while parsing query';
			}
            $this->error_msg.=".";
            trace("solr query crash : ".$e->getMessage());
            return false;
        }
        
        
        if ($highlight==true && $this->solr_query->getHighlight()===true)
            $this->solr_highlight=$response->getResponse()->highlighting;
        
        if ($this->solr_query->getFacet()===true)
            $this->solr_facet=$response->getResponse()->facet_counts;
        
        // dans le cas d'un requete
        $this->found_rows=$response->getResponse()->response->numFound;
        
        $this->result=array();
        if (!empty($response->getResponse()->response->docs))
        {
            foreach($response->getResponse()->response->docs as $doc)
            {
                $arr_doc=array();
                
                foreach ($doc->getPropertyNames() as $property)
                {
                    if (is_array($doc->offsetGet($property)))
                        // $arr_doc[strtoupper(trim($property))]=implode(',',$doc->offsetGet($property));
                        $arr_doc[strtoupper($this->removeLangFieldVersionDependant(trim($property)))]=implode(',',$doc->offsetGet($property));
                    else
                        // $arr_doc[strtoupper(trim($property))]=$doc->offsetGet($property);
                        $arr_doc[strtoupper($this->removeLangFieldVersionDependant(trim($property)))]=$doc->offsetGet($property);
                }
                
                $this->result[]=$arr_doc;
            }
			$this->ajoutPrivResult() ;
        }
        else if (!empty($response->getResponse()->terms))
        {
            $nb_terms=0;
            foreach($response->getResponse()->terms as $field)
            {
				// MS - 16.12.16 - si on ne trouve pas de résultats à la page indiquée, on repart depuis la première page, et on reset donc start_term
				if($this->solr_query->getTerms() && $this->max_rows != 'all' && count($field->getPropertyNames()) < $this->page * $this->max_rows){
					$start_term = 0 ; 
					$this->page = 1 ; 
				}
                foreach ($field->getPropertyNames() as $valeur)
                {
                    if ($nb_terms>=$start_term)
                    {
                        $valeur=trim($valeur);
                        $arr_term=array();
                        $arr_term['VALEUR']=$valeur;
                        
                        $this->result[]=$arr_term;
                    }
                    $nb_terms++;
                }
            }
        }
        
        // facettes
        $this->arr_facet=array();
        
        if (!empty($this->solr_facet->facet_fields))
        {
            foreach ($this->solr_facet->facet_fields->getPropertyNames() as $fields)
            {
                $fields=trim($fields);
                foreach ($this->solr_facet->facet_fields->offsetGet($fields) as $key=>$valeur) {
                    $key=trim($key);
                    $valeur=trim($valeur);
                    
                    $arr_facet_tmp=array();
                    
                    $arr_facet_tmp['field']=$fields;
                    $arr_facet_tmp['key']=$key;
                    $arr_facet_tmp['valeur']=$valeur;
                    
                    if(strpos($arr_facet_tmp['field'],'-ref') !== false){
						$arr_facet_tmp['field'] = substr($arr_facet_tmp['field'],0,strpos($arr_facet_tmp['field'],'-ref'));
						$found = false ; 
						foreach($this->arr_facet as $facet){
							if($facet['field'] ==  $arr_facet_tmp['field'] && $facet['key'] == $arr_facet_tmp['key']){
								$found = true ; 
								break ; 
							}
						}
						if(!$found){
							$this->arr_facet[]=$arr_facet_tmp;
						}
					}else{
						$this->arr_facet[]=$arr_facet_tmp;
					}
                }
            }
        }
        
        if (!empty($this->solr_facet->facet_dates))
        {
            foreach ($this->solr_facet->facet_dates->getPropertyNames() as $fields)
            {
                $fields=trim($fields);
                foreach ($this->solr_facet->facet_dates->offsetGet(trim($fields))->getPropertyNames() as $key)
                {
                    $key=trim($key);
                    if ($key!='start' && $key!='end' && $key!='gap')
                    {
                        $valeur=$this->solr_facet->facet_dates->offsetGet(trim($fields))->offsetGet($key);
                        
                        $arr_facet_tmp=array();
                        
                        $arr_facet_tmp['field']=$fields;
                        $arr_facet_tmp['key']=$key;
                        $arr_facet_tmp['valeur']=$valeur;
                        
                        $gap=str_replace('+','',$this->solr_facet->facet_dates->offsetGet(trim($fields))->offsetGet('gap')); //+xYEAR
                        $gap=str_replace('YEAR','',$gap);
                        $annee=explode('-',$key);
                        $annee=$annee[0]+$gap-1;
                        $arr_facet_tmp['key2']=$annee.'-12-31T23:59:59Z';
                        $arr_facet_tmp['gap']=$this->solr_facet->facet_dates->offsetGet(trim($fields))->offsetGet('gap');
                        
                        $this->arr_facet[]=$arr_facet_tmp;
                    }
                }
            }
        }
        if (!empty($this->solr_facet->facet_ranges))
        { 
            foreach ($this->solr_facet->facet_ranges->getPropertyNames() as $fields)
            {
                $fields=trim($fields);
                foreach ($this->solr_facet->facet_ranges->offsetGet(trim($fields))->getPropertyNames() as $key)
                {
                    foreach ($this->solr_facet->facet_ranges->offsetGet(trim($fields))->offsetGet('counts') as $key_range=>$range)
                    {
                        
                        $key=trim($key);
                        if ($key!='start' && $key!='end' && $key!='gap')
                        {
                            try{
                                $valeur = $range;
                                $arr_facet_tmp=array();
                                
                                $arr_facet_tmp['field']=$fields;
                                $arr_facet_tmp['key']=$key_range;
                                $arr_facet_tmp['valeur']=$valeur;
                                $arr_facet_tmp['gap']=$this->solr_facet->facet_ranges->offsetGet(trim($fields))->offsetGet('gap');
                                $arr_facet_tmp['key2']=(string)((int)$key_range+(int)$arr_facet_tmp['gap']);
                                
                                $this->arr_facet[]=$arr_facet_tmp;
                            }catch(Exception $e){
                                trace("class_pageSolr: crash récupération des facet_ranges :".$e);
                            }
                        }
                    }
                }
            }
        }
        //$this->result=$response->getResponse()->response->docs;
        
        // contextes de highlight
        $this->highlight_context=array();
        if ($highlight==true && $this->solr_query->getHighlight()===true)
        {
            foreach ($this->solr_highlight as $id=>$highlight)
            {
                $tmp_arr_ctx=array();
                
                $tmp_arr_ctx['id']=$id;
                foreach ($highlight->getPropertyNames() as $fields)
                {
                    //$tmp_arr_ctx[trim($fields)]=$highlight->offsetGet($fields);
                    $txt_highlight='';
                    
                    foreach ($highlight->offsetGet($fields) as $txt_highlight)
                    $txt_highlight=str_replace(array('<highlight>','</highlight>'),array('<span class="highlight1">','</span>'),$txt_highlight);
                    
                    $tmp_arr_ctx[trim($fields)]=$txt_highlight;
                }
                
                $this->highlight_context[]=$tmp_arr_ctx;
                
            }
            
            
        }
        
        return true;
    }

    /**
     * Application tri
     * IN : paramètre tri , tableau de champ
     * OUT : solr query avec tri et màj des variables order (col), triInvert (sens passé à l'url)
     */
    function addDeftOrder($tabOrder) {
       // Suppression tri
	   // trace(print_r(debug_backtrace(),true));
        $sort_fields = $this->solr_query->getSortFields();
        if (!empty($sort_fields)){
            foreach($sort_fields as $fld){
                $this->solr_query->removeSortField(trim(str_replace(array('desc','asc'),'',$fld)));
            }
        }
        
        $this->tabOrder = $tabOrder;
        if (!empty($tabOrder)) {
            if($tabOrder['COL'][0]!='none'){
                foreach ($tabOrder['COL'] as $idx=>$key) {
                    if ($tabOrder['DIRECTION'][$idx] && strtoupper($tabOrder['DIRECTION'][$idx]) == 'DESC') {
                        $order = SolrQuery::ORDER_DESC;
                    }else{
                        $order = SolrQuery::ORDER_ASC;
                    }
                    $key = strtolower(str_replace(' ',',',trim($key)));
                    $aFld = explode(',',$key);
                    foreach ($aFld as $fld) {
						$fld = $this->checkFieldVersionDependant($fld);
                        $this->solr_query->addSortField($fld, $order);
                    }
                }
                $this->order=$tabOrder['COL'][0]; //du coup dans le tableau, on trie sur la première colonne
                $this->triInvert=($tabOrder['DIRECTION'][0]=='DESC'?0:1);
                
            }
        }
    }

    function getPageSolrHighlight()
    {
        return $this->solr_highlight;
    }
    
    function getArrayHighlightContext()
    {
        return $this->highlight_context;
    }
    
    function getPageSolrFacet()
    {
        return $this->solr_facet;
    }
    
    
    function getArrayFacets()
    {
        return $this->arr_facet;
    }
    
    /*function setOffset($n){
        if(isset($n) && !empty($n) && is_numeric($n))
            $this->start_offset = $n;
    }*/

   	// MS 26/08/14
    // Fonction de merge des SolrObjects (utilisÈ pour l'isntant uniquement pour merge les objets highlight solr )
    function mergeSolrObjects($obj1,$obj2){
        if(isset($obj1) && !empty($obj1) && isset($obj2) && !empty($obj2)){
            foreach($obj2 as $idx=>$val){
                if(!is_array($obj1->offsetGet($idx))){
                    $obj1->offsetSet($idx,$val);
                }else{
                }
            }
            return $obj1;
        }else if(isset($obj1) && !empty($obj1) && (!isset($obj2) || empty($obj2)) ){
            return $obj1;
        }else{
            return false ; 
        }
    }
 
    function removeLangFieldVersionDependant($field){
        if(!isset($this->arrFieldsToSaveWithIdLang) ){
            require_once(modelDir.'model_doc.php');
            $tmpDoc=  new Doc() ;
            $this->arrFieldsToSaveWithIdLang = $tmpDoc->getArrFieldsToSaveWithIdLang();
            $this->arrFieldsToSaveWithIdLang = array_map('strtolower',$this->arrFieldsToSaveWithIdLang);
            unset($tmpDoc);
        }
        
        if(preg_match('/_'.strtolower($_SESSION['langue']).'$/',$field) && in_array(str_replace('_'.strtolower($_SESSION['langue']),'',$field),$this->arrFieldsToSaveWithIdLang )){
            $field = str_replace('_'.strtolower($_SESSION['langue']),'',$field);
        }
        return $field ; 
    }
	/**
	 * fonction ajoutPrivResult 
	 * Alimente le tableau de résultats avec le niveau de privilège de l'utilisateur pour chaque résultat.
     */
	function ajoutPrivResult(){
		$usr=User::getInstance();
		// VP 2/3/09 : prise en compte paramètre useMySQL
		$fld="DOC_ID_FONDS";
		
		for ($i=0;$i<count($this->result);$i++){
			// VP 21/4/10 : modif valeur privilege admin
			$this->result[$i]["ID_PRIV"]=5;
			if ($usr->Type!=kLoggedAdmin){
				$this->result[$i]["ID_PRIV"]=0; //par défaut
				foreach($usr->Groupes as $id=>$value){
					if(isset($this->result[$i][$fld])){
						if(is_numeric($id) && $value["ID_FONDS"]==$this->result[$i][$fld]){
							$this->result[$i]["ID_PRIV"]=$value["ID_PRIV"];
						}
					}
				}
			}
		}
		// MS - à optimiser / patcher, je n'aime pas trop l'idée d'avoir une fonction externe pouvant faire n'importe quoi sur le tableau. 
		// Conservé pour rétrocompatibilité.
		if(function_exists("ajoutPrivResultCustom")){
			$this->result=ajoutPrivResultCustom($this->result);
		}
		
		return true;
	}
	
	/**
	 * function escapeSpecialCharsForSolr
	 * Echappe certains caractères spéciaux pour Solr, afin de permettre la suppression des guillemets, guillements transformant une recherche fulltexte en recherche exacte
	 * @param string $str
	 * @return string
	 */
	function escapeSpecialCharsForSolr($str) {
		$sSpecialsChars = preg_quote('+ - && || ! ( ) { } [ ] ^ " ~ ? :'); //Caractères spéciaux à échapper
		//Transformation de la liste en paramètre utilisable
		$aSpecialChars = explode(' ', trim(preg_replace('|([^ ]+) *|', '#(\1)# ', $sSpecialsChars)) );
		
		$pattern = $aSpecialChars;
		$replacement = '\\\\$1'; //4 \ pour pouvoir en écrire 1 seul. grrr
		$subject = $str;
		$strWithoutSpecialChars = preg_replace($pattern, $replacement, $subject);
		
		return $strWithoutSpecialChars;
	}
	
}

?>
