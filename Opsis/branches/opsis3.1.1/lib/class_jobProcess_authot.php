<?php
require_once('class_jobProcess.php');
require_once('class_matInfo.php');
require_once('interface_Process.php');
require_once('exception/exception_uploadException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');

class JobProcess_authot extends JobProcess implements Process
{

	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);

		$this->setModuleName('authot');

		$this->required_xml_params=array
		(
		'url'=>'string',
		'token'=>'string'		
		);
		
		$this->optional_xml_params=array
		(
        'folder_in'=>'string',
		'champ_parol'=>'string',
		'file_xml'=>'string',
		'file_full_xml'=>'string',
		'lang'=>'string',
		'correction'=>'string',
		'file_srt'=>'string',
		'file_txt'=>'string',
		'xsl_post'=>'string',
		'xsl_keep_transcript_suffix'=>'string' 
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
		$this->loading=0;
		$this->etat_avancement=0;
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		$path_in=$this->getFileInPath();
		$name_fichier=basename($this->getFileOutPath());


		///////////////////////////////////////////////
		/////////////////// UPLOAD ///////////////////
		//////////////////////////////////////////////
	

	$path_out=dirname($this->getFileOutPath());
	
	//$params_job['lang'] utilise la syntaxe d'opsis (dmat_id_lang, dmat_version suivant les sites) TANDIS que $langue correspond à ce qu'attend authot (syntaxe des langues suivant authot)
	$params_job['lang'] = trim(strtoupper($params_job['lang']));

	if ($params_job['lang'] == 'EN') {
		$langue = 'en';
	} else if ($params_job['lang'] == 'ES' || $params_job['lang'] == 'ESP') {
		$langue = 'es-ES';
	} else {
		$langue = 'fr';
	}

		//rajouter le parametre de correction
	if($params_job['correction']=='true')
		$correction='true';
	else
		$correction='false';
	
	$token=$params_job['token'];	
	$url=$params_job['url'];
	$file=$this->getFileInPath();
	$curl= 'curl -X POST -H "Access-Token:'.$token.'" -F  "correction='.$correction.'"	-F data="@'.$file.'" "https://app.xn--autht-9ta.com/api/sounds/new?lang='.$langue.'"';
	$this->writeJobLog($curl);
	$output=shell_exec($curl);
	$result = json_decode($output,true);
	if(!empty($result['state']) && $result['state']=='Error'){
		throw new Exception('Erreur: '.$result['error_message']);
	}
	$id=$result['id'];
	if(empty($id)){
		throw new Exception('Erreur: id authot vide');
	}
	$curl='curl -H "Access-Token:'.$token.'"	"https://app.xn--autht-9ta.com/api/sounds/'.$id.'"';
	$this->writeJobLog($curl);
	$output=shell_exec($curl);
	$result = json_decode($output,true);
	// VP 31/03/2017 : prise en compte état 110 = correction finie
	do
	{	
		if(!empty($id)){		
			sleep(30);			
			$this->updateProgress($token, $id, $correction);
		}
	}
	while($this->etat_avancement !==10 && $this->etat_avancement !==110);
	
	if($this->etat_avancement ===10 || $this->etat_avancement ===110) {
		$this->loading=100;
	}
	//temporise avant de récupérer les fichiers 
	sleep(10);
	//resultat
	if($this->loading == 100){
		if($params_job['file_txt']==1){
			$abs_path_out = $path_out.'/'.$name_fichier.'.txt';
			$curl='curl -H "Access-Token:'.$token.'" -o '.$abs_path_out.' "https://app.xn--autht-9ta.com/api/sounds/'.$id.'?export_format=txt"';
			$this->writeJobLog($curl);
			$output=shell_exec($curl);
		}		
		// ESSAYEZ L EXTENSION .VTT
		if($params_job['file_vtt']==1){
			$abs_path_out = $path_out.'/'.$name_fichier.'.vtt';
			$curl='curl -H "Access-Token:'.$token.'" -o '.$abs_path_out.' "https://app.xn--autht-9ta.com/api/sounds/'.$id.'?export_format=webvtt"';
			$this->writeJobLog($curl);
			$output=shell_exec($curl);
		}	
		if($params_job['file_srt']==1){
			$abs_path_out = $path_out.'/'.$name_fichier.'.srt';
			$curl='curl -H "Access-Token:'.$token.'" -o '.$abs_path_out.' "https://app.xn--autht-9ta.com/api/sounds/'.$id.'?export_format=srt"';
			$this->writeJobLog($curl);
			$output=shell_exec($curl);
		}	
		if($params_job['file_xml']==1){
			$abs_path_out = $path_out.'/'.$name_fichier.'.xml';
			$curl='curl -H "Access-Token:'.$token.'" -o '.$abs_path_out.' "https://app.xn--autht-9ta.com/api/sounds/'.$id.'?export_format=xml"';
			$this->writeJobLog($curl);
			$output=shell_exec($curl);
		}
		if($params_job['file_full_xml']==1){
			$abs_path_out = $path_out.'/'.$name_fichier.'.xml';
			$curl='curl -H "Access-Token:'.$token.'" -o '.$abs_path_out.' "https://app.xn--autht-9ta.com/api/sounds/'.$id.'?export_format=full_xml"';
			$this->writeJobLog($curl);
			$output=shell_exec($curl);
		}
		if (!is_file($abs_path_out))
			throw new FileNotFoundException("File authot not found : ".$abs_path_out);
			
		if(isset($params_job['xsl_post']) && file_exists(jobDir.'/etc/'.$params_job['xsl_post'].".xsl")){
			//$this->writeJobLog("class_jobProcess_authot test traitement xsl : ".$params_job['xsl_post']." ".$params_job['xsl_keep_transcript_suffix']);
			require_once("fonctionsXML.php");
			$authot_trans = file_get_contents($abs_path_out);
			$authot_trans = str_replace('<br>','<br/>',$authot_trans);
			$post_traitement = TraitementXSLT($authot_trans,jobDir.'/etc/'.$params_job['xsl_post'].".xsl");
			//$this->writeJobLog("class_jobProcess_authot post_traitement :".$post_traitement );
			if(isset($params_job['xsl_keep_transcript_suffix']) && !empty($params_job['xsl_keep_transcript_suffix'])){
				rename($abs_path_out,stripExtension($abs_path_out).$params_job['xsl_keep_transcript_suffix'].'.'.getExtension($abs_path_out));
				//$this->writeJobLog("class_jobProcess_authot rename  :".stripExtension($abs_path_out).$params_job['xsl_keep_transcript_suffix'].'.'.getExtension($abs_path_out));
			}
			file_put_contents($abs_path_out,$post_traitement);
			//$this->writeJobLog("class_jobProcess_authot remplacement de  :".$abs_path_out);
		}
		
		//TEMPORAIRE POUR PB AUTHOT
		$authot_trans = file_get_contents($abs_path_out);
		if(strpos($authot_trans, '500 Internal Server Error') !== false) {
			mail('support@opsomai.com', 'Authot : erreur 500 : '.basename($abs_path_out),'Probleme generation transcription. Chamin du fichier de transcription  : '.$abs_path_out.' Envoye depuis opsis3.1.0/class_jobProcess_authot.php');
		}
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	
	}
	public function finishJob()
	{
		if (file_exists($this->tmp_xml_file))
		{
			if (unlink($this->tmp_xml_file)===false)
				throw new FileException('failed to delete XML file ('.$this->tmp_xml_file.')');
		}
		
		parent::finishJob();
	}
	
	public function updateProgress($token='',$id='',$correction=false)
	{
		$curl='curl -H 	"Access-Token:'.$token.'"	"https://app.xn--autht-9ta.com/api/sounds/'.$id.'"';
		$output=shell_exec($curl);
		$result = json_decode($output,true);
		$this->writeOutLog($curl." \n transcription=".$result['transcription']." state=".$result['state']." status=".$result['status']);		
		if($result['state']<0){
			throw new Exception($result['status']);
		}
		$this->loading=round($result['transcription']);
		$this->etat_avancement=$result['state'];
		if($correction && $result['state']!==10 && $result['state']!==110) {
			$this->setProgression(0.8 * $this->loading);
		} else {
			$this->setProgression($this->loading);
		}
	}
	

	
	public function updateUploadProgress($download_size, $downloaded, $upload_size, $uploaded)
	{
		$this->updateProgress();
	}

	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}


?>
