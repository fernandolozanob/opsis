<?php

/**
 *  Classe Page
 * Version : 1.0 (PHP4)
 * Author : L. Desjardins 02/08/2006
 */
include_once(libDir."class_page.php");
require_once(libDir.'class_Basis.php');

class PageBasis extends Page {


private static $instance;
var $urlaction;
var $page;
var $template;
var $langue;
var $titre;
var $includePath;

var $prmAction, $prmPage, $prmSort, $prmNbLignes; // Noms des params GET comprenant l'action, la page de résultat, le tri

// Variables pour le Pager
var $max_rows; // max rows by page ($this->max_rows)
var $rows; // number of rows in pages ($this->rows)
var $fleches= array("prev"=>"&lt;","next"=>"&gt;","first"=>"&lt;&lt;","last"=>"&gt;&gt;"); //flèches par défaut : tableau
var $tab_options = array(5=>"5",10 => "10", 20 => "20", 50 => "50", 100 => "100", "all" => kTous);
var $found_rows;
var $result=array(); // Tableau de résultat de la requête
var $PagerLink;
var $nbLignes;
var $error_msg;

// Variables pour le tri
var $order; //tri récupéré depuis l'url
var $triInvert; //sens inverse (passé aux url du tableau pour la bascule ASC / DESC
var $col; //colonne de tri
var $params4XSL=array(); //Paramètres de base passés aux feuilles XSL.
	protected $database;
	
	function __construct ()
	{
		$this->prmAction="urlaction";
		$this->prmPage="page";
		$this->prmSort="tri";
		$this->prmNbLignes="nbLignes";
		$this->page=1;
		$this->titre=""; // titre par d�faut
		$this->nomEntite=""; // titre par d�faut
		$this->titreSite=gSite; // titre par d�faut
		$this->initParams4XSL();
		$this->includePath=includeDir;
		$this->database=kBaseBasis;
	}

	/** Singleton : une seule classe PageSinequa  en même temps !*/
   public static function getInstance()
   {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
   }
   
	// ----------------------
	// methode permmettant de choisir la base dans laquelle la recherche est affectuée
	// ----------------------
	public function setDatabase($db)
	{
		$this->database=$db;
	}

	/**
	 * Initialisation du Pager.
	 * IN : SQL, new_max (opt, nb de lignes max),
	 * 		$altVar = autre mode d'initialisation de la page en cours (ex : session),
	 * 			NOTE: cette variable n'est utile que si le sélecteur du nb de lignes est mis dans la XSL.
	 * 			Si ce sélecteur est appelé dans la page PHP, la var de session est déjà initialisée
	 * 		$addGETVars = variables additionnelles à passer aux liens du Pager
	 * OUT : var de classe : Result (resultat de requete "tronqué"),
	 * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales), PagerLink (url passée de page en page)
	 */
    function initPager($sql, $new_max = 10,$altVar=null,$addGETVars=null,$secstocache=0,$highlight=true)
    {

        global $db;
		$deftNbLignes=defined("gNbLignesDocListeDefaut")?gNbLignesDocListeDefaut:10;
		
		if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) )
			$this->page=1;

		if (!isset($this->nbLignes))
			$this->max_rows = $new_max;
		else
			$this->max_rows=$this->nbLignes;
        
		/*echo 'SQL='.$sql.'<br />';
		echo 'nb_lignes='.$this->nbLignes.'<br />';
		echo 'max_rows='.$this->max_rows.'<br />';
		echo 'page='.$this->page.'<br />';*/
		
		
		// VP 1/02/2012 : test paramtre nb_pages
        // VP 8/03/2012 : limite ˆ 5000 si max (sinon le composant limite ˆ 10)
		if(!empty($this->max_rows)&& ($this->max_rows!='all'))
			$nb_lignes=$this->max_rows;
        else
			$nb_lignes=5000;
		
		trace('requete basis : '.$sql);
		
		$sql_basis=new Basis($this->database);
		
		try
		{
			$data=$sql_basis->Execute($sql,$nb_lignes,$this->page,$highlight);
		}
		catch (BasisException $e)
		{
			trace('Erreur SQL Basis : '.$e->getMessage());
			echo 'Erreur requête';
		}
		//$data=curl_exec($curl_basis);
		//$data=utf8_encode($data);
		
		$data=xml2tab($data);
		$data=$data['DATAS'][0]['DATA'][0];
		/*echo '<pre>';
		print_r($sql);
		echo '</pre>';*/
		$this->found_rows=$data['NB_LIGNES'];
		$data=$data['ROW'];
		/*echo '<pre>';
		print_r($info);
		echo '</pre>';*/
		
		// échappement de certaines valeurs highlightées qui perdent l'espace qui les precedent... (descripteurs / genre)
		foreach($data as $idx=>$row){
			foreach($row as $field=>$value){
				if(strpos($value,'&lt;span class="highlight') != false){
					if(substr($value,strpos($value,'&lt;span class="highlight')-1,1) != " "){
						$data[$idx][$field] = str_replace('&lt;span class="highlight',' &lt;span class="highlight',$value);
					}
				}
			}
		}
				
		
		$this->result=$data;

        $this->PagerLink=$this->getName()."?urlaction=".$this->getActionFromUrl().$this->addUrlParams($addGETVars)."&page=";
        if ($this->page>$this->num_pages()) $this->page=$this->num_pages();
        $this->params4XSL['nb_pages']=$this->num_pages();
        $this->params4XSL['nb_rows']=$this->found_rows;
    	
    	//debug($this,'gold',true);
    
    	
    
    }
	
	
	function initPagerFromArray($arr,$found_rows=null,$ignore_page = null,$addGETVars=null) {
		if (!is_array($arr)) {
 			$this->params4XSL['nb_pages']=0;
        	$this->params4XSL['nb_rows']=0;
			return false;
		}
		
		if(!isset($found_rows)){
			$this->found_rows=count($arr);
		}else{
			$this->found_rows= $found_rows;
		}

		if (!isset($this->nbLignes) || $this->nbLignes=='all') $this->max_rows = count($arr); else $this->max_rows=$this->nbLignes;

		
		if(!isset($ignore_page) || !$ignore_page){
			if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) ) $this->page=1;
		}
		
		$num_page=$this->num_pages();

		if($this->page > $num_page) $this->page = $num_page;
		if ($this->nbLignes!='all' && (!isset($ignore_page) || !$ignore_page)) $this->result=array_slice($arr,($this->page-1)*$this->max_rows,$this->nbLignes);
		else $this->result=$arr;


		$this->rows=$this->found_rows;

        $this->PagerLink=$this->getName()."?urlaction=".$this->getActionFromUrl().$this->addUrlParams($addGETVars)."&page=";
        $this->params4XSL['nb_pages']=$num_page;
        $this->params4XSL['nb_rows']=$this->found_rows;

	}


	function getSortFromSession($order_session){
		$ordre=substr($order_session,-1,1);
		$this->tri =  $order_session;
		
		if (!is_numeric($ordre)) {
			$ordre=0;
		}
		if ($ordre==0) {
			$this->triInvert="1";
		}else{
			$this->triInvert="0";
		}
		$this->params4XSL['ordre'] = $this->triInvert;
		$this->params4XSL['tri'] = $this->order;
	}
	




} // fin de classe PAGE
?>
