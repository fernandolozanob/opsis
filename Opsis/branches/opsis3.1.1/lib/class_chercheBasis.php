<?

require_once(libDir."class_chercheDoc.php");
require_once(libDir.'class_Basis.php');

class RechercheBasis extends RechercheDoc 
{
	private $database;
	// VP 5/10/09 : prise en compte paramètre NOHIST dans les différents modes de recherche
    
    function __construct() {
	  	$this->entity='DOC';
     	$this->name=kDocument;
     	$this->prefix=null;
     	$this->sessVar='recherche_DOC';
		$this->tab_recherche=array();
		$this->useSession=true;
		
		$this->sqlRecherche='';
        
		if (defined("gParamsRecherche")) $this->params=unserialize(gParamsRecherche);
	}
    
    
    /**
     * Initialise le SQL de recherche (DOC)
     *
     */
    function prepareSQL(){
		$usr=User::getInstance();
		
		$this->sql='SELECT '.kChampsRechercheBasis.' FROM '.kVueBasis.' ';
    }
    
    /**
     * En fonction des types de recherche, appelle les fonctions de traitement dédiée.
     * Cette fonction peut se trouver surchargée si une classe introduit des recherches spécifiques
     */
    protected function ChooseType($tabCrit,$prefix)
	{
        // I.2.A.1 Tests des champs et appel des fonctions de recherche ad?quates :
		// VP 13/12/09 : ajout mode FTE recherche exacte (Vietnamien en particulier)
		
		debug($tabCrit, "#FF80C0", true);
		switch($tabCrit['TYPE'])
		{
			case "CE_B":   // workaround cas spécifiques CE pour les select / checkbox 
			case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
				//$this->chercheChamp($tabCrit,$prefix,true);
				$this->chercheChamp($tabCrit);
				break;
			case "D":   // Recherche sur date
            case "CDLT" : 
            case "CDGT" : 
            case "CDLE" : 
            case "CDGE" : 
				$this->chercheDate($tabCrit,$prefix);
                break;
			case "H":   // Recherche sur durées(s)
				$this->chercheDuree($tabCrit,$prefix);
				break;
			case "PH_INC":   // Recherche sur durées(s)
				$this->cherchePH($tabCrit,'INC');
				break;
			case "PH_IS":   // Recherche sur durées(s)
				$this->cherchePH($tabCrit,'IS');
				break;
			case "PH_LIKE":   // Recherche sur durées(s)
				$this->cherchePH($tabCrit,'LIKE');
				break;
			case "PH_ANY":   // Recherche sur durées(s)
				$this->cherchePH($tabCrit,'ANY');
				break;
			case "PH_WORDS":   // Recherche sur durées(s)
				$this->cherchePH($tabCrit,'WORDS');
				break;
		}
    }
	
	/**
     * Recherche sur un champ de la table avec un ou plusieurs champs
     * Les paramètres prefix_not_used & keepintact_not_used sont définis pour être compatible avec la classe parente, mais n'ont pas de fonction pour l'instant 
     */
    protected function chercheChamp($tabCrit,$prefix_not_used=null,$keepintact_not_used=null) // Cherche VALS dans 1 CHAMP de la table PRINC
	{
        global $db;
		
		//$valeur=str_replace("'","''",$tabCrit['VALEUR']);
		
		$this->sqlRecherche.=" ".$tabCrit['OP'];
		
		$this->sqlRecherche.=" (".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		if(empty($tabCrit['NOHIST'])){
			$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".(is_array($tabCrit['VALEUR'])?implode(',',$tabCrit['VALEUR']):$tabCrit['VALEUR'])." ";
		}
    }
	/**
     * Recherche sur un champ de la table avec un ou plusieurs champs
     *
     */
    protected function cherchePH($tabCrit,$type) // Cherche VALS dans 1 CHAMP de la table PRINC
	{
        global $db;
		
		//$fields=explode(',',$tabCrit['FIELD']);
		
		$this->sqlRecherche.=' '.$tabCrit['OP'].' (';
	
		// comme demandé à la date du 29/06/16 - Si le terme recherché avec un type PH_WORDS est entouré de doubles quotes, alors on génère la requête avec un type PH_LIKE
		if(!empty($tabCrit['VALEUR']) && substr(trim($tabCrit['VALEUR']),0,1) == '"' && substr(trim($tabCrit['VALEUR']),-1) == '"' && $type == 'WORDS'){
			$type = 'LIKE';
		}
        // Syntaxe FT : NATIVE('field PH LIKE ''terme1'',''terme2'',''l''''exemple''')
		// but de ces transformations est d'obtenir les échappements suivants : 
		// si on a une apostrophe dans un terme, alors il est échappé tq : ' => ''''
		// Pour les autres apostrophes éventuels séparant des termes, tq : <mot1>','<mot2> => <mot1>'',''<mot2>
		$valeur = trim(trim($tabCrit['VALEUR']),"'");
		$valeur = preg_replace('/([A-Za-z0-9]+[\s]*)(\')([\s]*[A-Za-z0-9]+)/',"$1''''$3",$valeur);
		if(isset($tabCrit['OPTIONS'])){
			$replace = $tabCrit['OPTIONS'] ;  
		}else{
			$replace = "$1";
		}
		$valeur = preg_replace('/\'[\s]*([,&]{1})[\s]*\'/',"''".$replace."''",$valeur);
				
		switch ($type)
		{
			case 'INC':
				$this->sqlRecherche.="NATIVE('".$tabCrit['FIELD']." INC ''".$valeur."''')";
				break;
			case 'IS':
				$this->sqlRecherche.="NATIVE('".$tabCrit['FIELD']." PH IS ''".$valeur."''')";
				break;
			case 'LIKE':
				$this->sqlRecherche.="NATIVE('".$tabCrit['FIELD']." PH LIKE ''".$valeur."''')";;
				break;
			case 'ANY':
				$this->sqlRecherche.="NATIVE('".$tabCrit['FIELD']." PH ANY ''".$valeur."''')";
				break;
			case 'WORDS':
				$this->sqlRecherche.="NATIVE('".$tabCrit['FIELD']." PH WORDS ''".$valeur."''')";
				break;
		}
		/*$first=true;
         foreach ($fields as $field)
         {
         if ($first)
         $this->sqlRecherche.='NATIVE (\''.$field.' '.$type.' \'\''.$tabCrit['VALEUR'].'\'\'\')';
         else
         $this->sqlRecherche.='OR NATIVE (\''.$field.' '.$type.' \'\''.$tabCrit['VALEUR'].'\'\'\')';
         
         $first=false;
         
         }*/
        
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
		
		$this->sqlRecherche.=') ';
    }
    
	/**
	 * Recherche sur un champ de type date
     * IN : tableau critre, prefixe table (opt)
     * OUT : sql + etape (pas de highlight)
     * NOTE : structure du tableau de critres
     * 		FIELD : champ (un seul)
     * 		TYPE :  opŽrateur)
     * 		VALEUR : date (une seule)
     * 		VALEUR2 : date de fin
	 */
	 
	
	protected function convDateBasis($sign,&$field1,&$field2){
		if(isset($field1) && !empty($field1) && isset($field2) && !empty($field2)){
			$date1 = explode('.',$field1);
			$date2 = explode('.',$field2);
			
			if(count($date1) == 1 && strlen($date1[0])==4){
				$field1 = "01.01.".$date1[0];
			}else if (count($date1) == 2 && strlen($date1[0]) == 2 &&  strlen($date1[1]) == 4){
				$field1 = "01.".implode('.',$date1);
			}
			
			if(count($date2) == 1 && strlen($date2[0])==4){
				$field2 = date("t.m.Y",strtotime("01.12.".$date2[0]));
			}else if (count($date2) == 2 && strlen($date2[0]) == 2 &&  strlen($date2[1]) == 4){
				$field2 = date("t.m.Y",strtotime("01.".implode('.',$date2)));
			}
		
		}else if(empty($field2)){
			$date1 = explode('.',$field1);
			switch($sign){
				case "D" :
					if(count($date1) == 1 && strlen($date1[0])==4){
						$field1 = "01.01.".$date1[0];
						$field2 = date("t.m.Y",strtotime("01.12.".$date1[0]));
					}else if(count($date1) == 2 && strlen($date1[0]) == 2 &&  strlen($date1[1]) == 4){
						$field1 = "01.".implode('.',$date1);
						$field2 = date("t.m.Y",strtotime("01.".implode('.',$date1)));
					}
				break;
				
				case "CDLT" : 
					if(count($date1) == 1 && strlen($date1[0])==4){
						$field1 = "01.01.".$date1[0];
					}else if(count($date1) == 2 && strlen($date1[0]) == 2 &&  strlen($date1[1]) == 4){
						$field1 = "01.".implode('.',$date1);
					}
				break;
				
				case "CDGT" : 
					if(count($date1) == 1 && strlen($date1[0])==4){
						$field1 = date("t.m.Y",strtotime("01.12.".$date1[0]));
					}else if(count($date1) == 2 && strlen($date1[0]) == 2 &&  strlen($date1[0]) == 4){
						$field1 = date("t.m.Y",strtotime("01.".implode('.',$date1)));
					}
				break ;
			
			}
		}
	}
	 
    protected function chercheDate($tabCrit,$type="") // Cherche VALS dans 1 CHAMP de la table PRINC
	{
        global $db;
		
		$this->sqlRecherche.=" ".$tabCrit['OP'];
		
		$valeur=$tabCrit['VALEUR'];
		$valeur2=$tabCrit['VALEUR2'];
		
		$this->convDateBasis($tabCrit['TYPE'], $valeur, $valeur2);
		
		if(!empty($tabCrit['VALEUR2']) || !empty($valeur2)){
            // Recherche par intervalle
            $this->sqlRecherche.=" (".$tabCrit['FIELD']." between '".$valeur."' and '".$valeur2."')";
		}else{
            // Recherche sur date seule
            switch ($tabCrit['TYPE'])
            {
                case "D":
                    $sens="=";
                    break;
                case "CDGT":
                    $sens=">";
                    break;
                case "CDLT":
                    $sens="<";
                    break;
                case "CDGE":
                    $sens=">=";
                    break;
                case "CDLE":
                    $sens="<=";
                    break;
            }
            if (empty($sens)) $sens="=";
            $this->sqlRecherche.=" (".$tabCrit['FIELD']." $sens '".$valeur."')";
        }
		
		if( empty($tabCrit['NOHIST'])){
			if(!empty($tabCrit['VALEUR2'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".kEntre." (".$tabCrit['VALEUR']." ".kEt." ".$tabCrit['VALEUR2'].")  ";
			else $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." $sens ".$tabCrit['VALEUR']." ";
		}
		
    }
    
	
	/**
     * Recherche sur la duree
     */
    protected function chercheDuree($tabCrit,$prefix="")
	{
		global $db;
		if(strlen($tabCrit['VALEUR'])>0)
		{
			$prefix=($prefix!=""?$prefix.".":"");
			if(strlen($tabCrit['VALEUR2'])>0)
			{
				$this->sqlRecherche.= " ".$tabCrit['OP'];
				
				$this->sqlRecherche.= " (".$prefix.$tabCrit['FIELD']." between ".$db->Quote(fDuree($tabCrit['VALEUR']))." AND ".$db->Quote(fDuree($tabCrit['VALEUR2'])).")";
				
				
				if(empty($tabCrit['NOHIST']))
					$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".kEntre." (".$tabCrit['VALEUR']." ".kEt." ".$tabCrit['VALEUR2'].") ";
			}
			else
			{
				$this->sqlRecherche.= " ".$tabCrit['OP'];
			
				$this->sqlRecherche.= " (".$prefix.$tabCrit['FIELD']." = ".$db->Quote(fDuree($tabCrit['VALEUR'])).")";
				
				
				if(empty($tabCrit['NOHIST']))
					$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";

				$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);
				if (empty($tabCrit['NOHIGHLIGHT']))
					$this->tabHighlight=$this->tabHighlight+$highlight;
			}
		}
    }
	
	
    /**
     * ComplŽtion "intelligente" selon les critres : si XX* -> LIKE, si array -> IN(,,) sinon =
     * IN : valeur (string comprenant 1 ou plusieurs vals), $keepIntact=>pas d'analyse de la chaine (utilisŽ pr DOC_COTE par ex)
     * OUT : string SQL
     */
    protected function sql_op($valeur,$keepIntact_not_used=false) {
        global $db;
        if (is_array($valeur)) $valeur=implode(',',$valeur);
        
        $arrWords=explode(",",$valeur);
        
		if(count($arrWords)>1) {
            
			$valeur="";
        	for($i = 0; $i <= (count($arrWords) - 1); $i++){
                // suppression caract?res parasites et quotes
				$valeur.=",'".str_replace("'","''",preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),trim($arrWords[$i])))."'";
			}
			$valeur=" IN (".substr($valeur,1).")";
		} else if((strpos($valeur,"%")!==false)||(strpos($valeur,"*")!==false)) {
			// suppression caract?res parasites et quotes
			$valeur=trim($arrWords[0]); //suppression des espaces
			$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
			$valeur=" LIKE '".str_replace(array("*","'"),array("%","'"),$valeur)."'";
		} else {
			// suppression caract?res parasites
			$valeur=trim($arrWords[0]); //suppression des espaces
			$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
			$valeur=" = '".str_replace("'","''",$valeur)."'";
		}
		return $valeur;
    }
    
    
    /** Applique les restrictions de droits sur la recherche.
     // i.e. : Sp?cifie que la liste des docs r?sultats doit faire partie d'une liste de fonds/collection auquelle l'utilisateur a acc?s.
     // Ajoute donc un "AND DOC_ID_FONDS in ('fonds1', 'fonds5', 'coll3',...) ? la requette sql
     // NB. : On ne prend pas en compte les fonds qui ont des privil?ges ? 0.
     **/
    function appliqueDroits(){
        //    	//by LD 04/06/08 => utilisation d'un tableau et non d'une chaine pour éviter les id_fonds in ('',.....)
        //		$prefix=(!is_null($this->prefix)?$this->prefix.".":"");
        //        $usr=User::getInstance();
        //        //$liste_id="''";
        //        foreach($usr->Groupes as $value){
        //           // if ($value["ID_PRIV"]!=0){ $liste_id.=", '".$value["ID_FONDS"]."'"; }
        //           if ($value["ID_PRIV"]!=0) $liste_id[]=$value["ID_FONDS"];
        //        }
        //
        //        //if(strlen($liste_id)>0) {
        //        if (count($liste_id)>0) {
        //            $this->sqlRecherche.= " AND ".$prefix."DOC_ID_FONDS ";
        //            $this->sqlRecherche.= " in ('".implode("','",$liste_id)."')";
        //        }
        //        
        //        //PC 02/12/10 : Ajout de la possiblité d'importer un fichier afin de définir des droits supplémentaires 
        //        if (file_exists(designDir . '/include/addDroits.inc.php')) {
        //			include(getSiteFile("designDir", 'include/addDroits.inc.php'));
        //		}
    }
    
   	/*fausse fonction refine, surclassée dans chercheSinequa */

	function refine ($string) {return '';}
	
	function setDatabase($db)
	{
		$this->database=$db;
	}
	
	function getDatabase()
	{
		return $this->database;
	}
	
	function saveSearchInDB ($requete) 
	{
		global $db;
		require_once(modelDir.'model_requete.php');

        $myQuery = New Requete();
		
		$myQuery->t_requete['REQ_SQL']=$this->sql;
		$myQuery->t_requete['REQ_LIBRE']=$this->etape;
		$myQuery->t_requete['REQ_ENGINE']='Basis';
		$myQuery->t_requete['REQ_DB']=$this->database;
        
		debug('saveSearch','cyan');
		$exist=$myQuery->checkExist();
		if ($exist)
		{
			$myQuery->t_requete['ID_REQ']=$exist;
			$myQuery->getRequete();
		}
        
		$myQuery->t_requete['REQ_NB_DOC']=$this->found_rows;
		$myQuery->t_requete['REQ_LIBRE']=$this->etape;
		$myQuery->t_requete['REQ_PARAMS']=$this->tab_recherche;
		$myQuery->t_requete['REQ_DATE_EXEC']=str_replace("'","",$db->DBTimeStamp(time()));
		$myQuery->t_requete['REQ_ENGINE']='Basis';
		$myQuery->t_requete['REQ_DB']=$this->database;
        
		if ($requete['SAUVE']) 
		{ //on sauve "sciemment" la requête ?
			$myQuery->t_requete['REQUETE']=$requete['REQUETE'];
			if (!empty($requete['REQUETE']))
			{
				$myQuery->t_requete['REQ_SAVED']=1;
				$myQuery->t_requete['REQ_ALERTE']=$requete['REQ_ALERTE']; 
			}
			else 
			{
				$myQuery->t_requete['REQ_ALERTE']=0; //... ou on la met juste dans l'histo...
				$myQuery->t_requete['REQ_SAVED']=0; 
			}
		}
        
		$myQuery->save(false); //ly ld 21 11 08 sauv sans chk car fait au dessus
		$this->requete['REQUETE']=$myQuery->t_requete['REQUETE'];
		$this->requete['REQ_ALERTE']=$myQuery->t_requete['REQ_ALERTE'];
		unset($myQuery);
	}
	
	function getSearchFromDB ($id)
	{
		require_once(modelDir.'model_requete.php');
		$myQuery = New Requete();
		$myQuery->t_requete['ID_REQ']=$id;
		$myQuery->getRequete();
		$this->etape = $myQuery->t_requete['REQ_LIBRE'];
		// MS - 30.03.17 - suite à l'ajout de removescripttags, les caractères spéciaux sont échappés dans REQ_SQL, on doit donc les retransformer lors d'une récupération depuis la base
		$this->sql = htmlspecialchars_decode($myQuery->t_requete['REQ_SQL'],ENT_NOQUOTES | ENT_HTML401);
		$this->tab_recherche = $myQuery->t_requete['REQ_PARAMS'];
		$this->tabHighlight=$myQuery->t_requete['REQ_HIGHLIGHT'];
		$this->requete['REQUETE']=$myQuery->t_requete['REQUETE'];
		$this->requete['REQ_ALERTE']=$myQuery->t_requete['REQ_ALERTE'];
		
		$this->database=$myQuery->t_requete['REQ_DB'];

		$this->rows = $myQuery->t_requete['REQ_NB_DOC'];//@update VG 08/04/2010 : ajout du nombre de lignes

		$this->putSearchInSession($id);
		unset($myQuery);
	}
    
	function finaliseRequete(){
		
		// VP 3/12/10 : prise en compte langue pour troncature étape (coupe au premier espace)
        $this->sqlRecherche=trim($this->sqlRecherche);
	
        if (strpos($this->sqlRecherche,"AND NOT")===0){
			// Dans ce cas, on fait une recherche en NOT
			
           $this->sqlRecherche=substr($this->sqlRecherche,7);
            $this->sqlRecherche=" NOT ( ".$this->sqlRecherche." ) ";
            $this->etape=kTousDocuments." ".$this->etape;
        }else if (strpos($this->sqlRecherche,"AND")===0){ //retrait du premier opérateur (évite le WHERE AND...)
            $this->sqlRecherche=substr($this->sqlRecherche,3);
            $this->etape=substr($this->etape,strpos($this->etape,' '));
		} else if (strpos($this->sqlRecherche,"OR")===0){
            $this->sqlRecherche=substr($this->sqlRecherche,2);
			$this->etape=substr($this->etape,strpos($this->etape,' '));
        }
		
		$this->sql.=" ".$this->sqlWhere;
        if($this->sqlRecherche!=""){
			$this->sql.=" WHERE ".$this->sqlRecherche;
        }
		
		
        $this->sql.=$this->sqlSuffixe;
    	if (empty($this->etape)) $this->etape=$this->name;
		
        // VP 14/03/2014 : suppression htmlentities, ˆ faire dans pages XXXListe
		//$this->etape=htmlentities($this->etape,ENT_COMPAT,'UTF-8');
    	if ($this->useSession) $this->putSearchInSession(); // On sauve la recherche en session
    }
    /**
     * Execution requ�te
     * IN : SQL, new_max (opt, nb de lignes max),
     * OUT : var de classe : Result (resultat de requete "tronqu�"),
     * 		 Rows (nb de lignes "tronqu�"), Found_Rows (nb lignes totales), PagerLink (url pass�e de page en page)
     */
    function execute( $max_rows = 10, $secstocache=0, $highlight=true)
    {
        global $db;

        if(empty($this->sql))
            return;
        
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) )
            $this->page=1;
        
        if (!isset($this->nbLignes))
            $this->max_rows = $max_rows;
        else
            $this->max_rows=$this->nbLignes;
        
        // VP 1/02/2012 : test param�tre nb_pages
        // VP 8/03/2012 : limite � 5000 si max (sinon le composant limite � 10)
        if(!empty($this->max_rows)&& ($this->max_rows!='all'))
            $nb_lignes=$this->max_rows;
        else
            $nb_lignes=5000;
        
        trace('requete basis : '.$sql);
        
        $sql_basis=new Basis($this->database);
        
        try
        {
            $data=$sql_basis->Execute($sql,$nb_lignes,$this->page,$highlight);
        }
        catch (BasisException $e)
        {
            trace('Erreur SQL Basis : '.$e->getMessage());
            echo 'Erreur requete';
        }
        //$data=curl_exec($curl_basis);
        //$data=utf8_encode($data);
        
        $data=xml2tab($data);
        $data=$data['DATAS'][0]['DATA'][0];
        $this->found_rows=$data['NB_LIGNES'];
        $data=$data['ROW'];
        
        $this->result=$data;
        $this->rows =count($this->result);
        
        return true;
        
    }
	
}//Fin de la classe ChercheBasis

?>