<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_networkException.php');
require_once('exception/exception_engineException.php');

class JobProcess_zip extends JobProcess implements Process
{	
	private $app_token;
	private $upload_key;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('zip');
		$this->required_xml_params=array
		(
		);
		
		$this->optional_xml_params=array
		(
			'files_in' => 'string',
			'delete_files' => 'int',
			'zip_to_print' => 'int',
			'regex_filter_file'=>'string'
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		if(!defined("kBinZip")){
			throw new FileNotFoundException('kBinZip undefined');
		}
		$params_job=$this->getXmlParams();
		
		if(isset($params_job['zip_to_print']) && $params_job['zip_to_print']){
			$this->exec_to_pipe = true ; 
		}
		
		// Fichiers temporaires dans répertoire tmp : zip -b /tmp stuff.zip *
		if(isset($this->exec_to_pipe) && $this->exec_to_pipe){
			$cmd="-j1X -b ".$this->getTmpDirPath()." - ";
		}else{
			$cmd="-j1X -b ".$this->getTmpDirPath()." ".escapeQuoteShell($this->getFileOutPath());
		}
		$cmd_files='';
		if (is_dir($this->getFileInPath()) && $arr_files = scandir($this->getFileInPath())){
			foreach($arr_files as $file) {
				if (is_file($this->getFileInPath().'/'.$file) && strpos(strtolower($file), '.zip')===false && $file[0]!='.'
				&& (!isset($params_job['regex_filter_file']) || empty($params_job['regex_filter_file']) || preg_match($params_job['regex_filter_file'],$file))
				) {
					$cmd_files.=' '.escapeQuoteShell($this->getFileInPath().'/'.$file);
				}
			}
		} elseif(is_file($this->getFileInPath())) {
			$cmd_files.=' '.escapeQuoteShell($this->getFileInPath());
		}
		
		if(!empty($params_job['files_in'])) {
			$aFilesIn = explode(';',$params_job['files_in']);
			foreach($aFilesIn as $file) {
				$cmd_files.=' '.escapeQuoteShell($file);
			}
		}
		
		if(empty($cmd_files)) {
			throw new FileNotFoundException('no file to zip');
		}
		
		if(isset($params_job['zip_to_print']) && $params_job['zip_to_print']){
			$this->shellExecute(kBinZip,$cmd." ".$cmd_files,false);
			$bufsize = 65535;
			$buff = '';
			$i = 0 ; 
			while(!feof($this->exec_pipes[1]) ) { 
			  echo stream_get_contents($this->exec_pipes[1],$bufsize);
			}
		}else{
			$this->shellExecute(kBinZip,$cmd." ".$cmd_files);
		}
		
		if($params_job['delete_files'] == '1') {
			if(is_file($this->getFileInPath()) && !is_dir($this->getFileInPath()) && dirname($this->getFileOutPath()) == dirname($this->getFileInPath())) {
				$this->shellExecute('rm '.escapeQuoteShell($this->getFileInPath()));
			}
		}
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	

	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>