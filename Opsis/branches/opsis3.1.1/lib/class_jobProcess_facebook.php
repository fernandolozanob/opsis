<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_uploadException.php');

require_once (libDir . 'class_facebook.php');

class JobProcess_facebook extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{

		parent::__construct($syncJobProcess);

		$this->setModuleName('facebook');

		$this->required_xml_params = array
			(
			'id_application' => 'int', //on garde id_application et api_key comme noms, pour r�tro-compatibilit� avec les precedentes publication facebook
			'api_key' => 'string',
			'access_token' => 'string', //allez sur https://developers.facebook.com/tools/debug/accesstoken/
		);

		$this->optional_xml_params = array
			(
			'id_page' => 'int',
			'publish_as_pageid' => 'int',
			'donnees' => 'array',
			'target' => 'int'
		);

		$this->setInput($input_job);
		$this->setEngine($engine);
	}

	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
	}
	

	public function doProcess() {
		$this->writeOutLog('Debut traitement');

		$params_job = $this->getXmlParams();

		$app_id = trim($params_job['id_application']);
		$app_secret = trim($params_job['api_key']);
		$user_access_token = trim($params_job['access_token']);
		
		$target=trim($params_job['donnees'][0]['target']);
		if(empty($target)) {
			$target=trim($params_job['target']);
		}
		if(empty($target)) {
			$target = "me";
		}
		
		$publish_as_pageid = $params_job['publish_as_pageid'];
		if(!empty($params_job['donnees'][0]['publish_as_pageid'])) {
			$publish_as_pageid = trim($params_job['donnees'][0]['publish_as_pageid']);
		}
	
		$link = trim($params_job['donnees'][0]['link']);
		$message = trim($params_job['donnees'][0]['message']);
		$title = trim($params_job['donnees'][0]['title']);
		$description = trim($params_job['donnees'][0]['description']);
		$filePath = trim($params_job['donnees'][0]['filepath']);
		if(empty($filePath)) {
            $filePath = $this->create_public_path();
		}

		$action = 'PUBLISH_LINK';
		if (!empty($params_job['donnees'][0]['nom_action'])) {
			$action = strtoupper(trim($params_job['donnees'][0]['nom_action']));
		}


		$fb = new Opsis\Facebook(array('app_id' => $app_id, 'app_secret' => $app_secret, 'user_access_token' => $user_access_token));
		$fb->connect();

		$this->setProgression(10);

// Important, la fonction unpublish est appel� sans job, vu que c'est rapide (@see class_facebook)

		if ($action == 'PUBLISH_LINK') {

			if (empty($link)) {
				throw new Exception('Erreur : Param�tre link  manquant dans le xml');
			}

			if (empty($message)) {
				throw new Exception('Erreur : Param�tre message  manquant dans le xml');
			}


			$publish_link = $fb->publish_link(array('link' => $link, 'message' => $message), $target);

			if (array_key_exists('KO', $publish_link)) {
				throw new Exception($publish_link['KO']);
			} else {
				$this->writeOutLog('video_id:<id>' . $publish_link['OK'] . '</id>');
				$this->writeJobLog('video_id:' . $publish_link['OK']);
			}
		} elseif ($action == 'PUBLISH_VIDEO') {

			if (empty($title)) {
				throw new Exception('Erreur : Param�tre title  manquant dans le xml');
			}

//			if (empty($description)) {
//				throw new Exception('Erreur : Param�tre description  manquant dans le xml');
//			}

			if (!file_exists($filePath)) {
				throw new Exception('Erreur : Fichier introuvable : '.$filePath);
			}

			$publish_video = $fb->publish_video($filePath, array('title' => $title, 'description' => $description), $target, $publish_as_pageid);

			if (array_key_exists('KO', $publish_video)) {
				throw new Exception($publish_video['KO']);
			} else {
				$this->writeOutLog('video_id:<id>' . $publish_video['OK'] . '</id>');
				$this->writeJobLog('video_id:' . $publish_video['OK']);
			}
		} else {
			throw new Exception('Erreur : ' . $action . ' non implemente');
		}


		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function create_public_path() {
		$FileInPath=$this->getFileInPath();
			
		$TmpDirPath = $this->getTmpDirPath();
		$prefixe = '/FB';
		if(substr($TmpDirPath, -1) === '/') { $prefixe = 'FB'; }
		$link_file_path = $TmpDirPath.$prefixe.basename($FileInPath);
		if(is_file($FileInPath)){
			if(is_link($link_file_path)) {
				unlink($link_file_path);
			}
			if(symlink($FileInPath,$link_file_path)===false){
				$this->writeJobLog('Error linking file ('.$FileInPath.' -> '.$link_file_path.')');
				throw new FileNotFoundException('Error linking file ('.$FileInPath.' -> '.$link_file_path.')');
			} else {
				$this->writeJobLog('Linking file ('.$FileInPath.' -> '.$link_file_path.')');
			}
		} else {
			$this->writeJobLog('Error target file ('.$FileInPath.' -> '.$link_file_path.')');
			throw new FileNotFoundException('Error target file ('.$FileInPath.' -> '.$link_file_path.')');
		}
		
		return $link_file_path;
	}

	public function updateProgress() {
		
	}

	public static function kill($module, $xml_file) {
		JobProcess::kill($module, $xml_file);
	}

}

?>
