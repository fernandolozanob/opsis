<?
/**
* Classe d'affichage de formulaires
 */
require_once(libDir."class_form.php");

class FormCherche extends Form {

	var $countElt=0;		// Compteur utilisé pour les formulaires de recherche
	var $chercheObj;	// Objet recherche / ex : $myCherche
	var $useIdxCrit=false; // Flag pour restreindre l'affectation de valeur par getValueForField à l'index du champ dans le formulaire, à mettre à true si on le souhaite


	//VG 31/05/2011 : transfert de class_form vers class_formCherche
	function displayElt($attr){

		// VP 26/11/09 : ajout attribut PROFIL_MIN
		if(isset($attr['PROFIL_MIN'])){
			$myUsr=User::getInstance();
            $profil_min = defined($attr['PROFIL_MIN'])?constant($attr['PROFIL_MIN']):$attr['PROFIL_MIN'];
			if($myUsr->getTypeLog() < $profil_min ) {return;}
		}
		if(isset($attr['PROFIL_MAX'])){
			$myUsr=User::getInstance();
            $profil_max = defined($attr['PROFIL_MAX'])?constant($attr['PROFIL_MAX']):$attr['PROFIL_MAX'];
			if($myUsr->getTypeLog() > $profil_max ) {return;}
		}
		if(isset($attr['PROFIL_IDS'])){
			$myUsr=User::getInstance();
			$ids = explode(",", $attr['PROFIL_IDS']);
            array_walk($ids, create_function('&$v', '$v = defined($v)?constant($v):$v;'));
			if(!in_array($myUsr->getTypeLog(), $ids)) {return;}
		}
		if($attr['TYPE']=="multifield"){
            $this->displayEltMulti($attr);
            return;
        }
        if (isset($attr['FIRST_FIELD_ID']) && !empty($attr['FIRST_FIELD_ID']))
            $this->countElt=$attr['FIRST_FIELD_ID']-1;
		else $this->countElt++;

		// LABEL
		if(isset($attr['LABEL'])) $label=defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'];
		else if(isset($attr['CHLIBS'])) $label=$attr['CHLIBS'];

		if(isset($label) && defined($label)) $label=constant($label);
		//VP 4/06/09 : reaffectation valeur label décodée
		if (isset($label))
			$attr['LABEL']=$label;
		else
			$attr['LABEL']='';

		// VP 20/09/10 : ajout attr DIVTAGS
		if(isset($attr['TRTAGS'])) {
			if($attr['TRTAGS']=="start" || $attr['TRTAGS']=="both") print "<tr>\n";
			if(isset($label)) {
				// VP 3/12/09 : ajout * au label pour les champs obligatoires
				print "<td class='".(isset($attr['CLASSLABEL'])?$attr['CLASSLABEL']:$this->classLabel)."' valign='".(isset($attr['VALIGN'])?$attr['VALIGN']:'top')."' ".(isset($attr['LABEL_WIDTH'])?" WIDTH=".$attr['LABEL_WIDTH']:"").">";
				print $this->displayOperator($attr);
				
				print "<label for='chValues[".$this->countElt."]' >";
				print $label;
				print "</label>";

				if (isset($attr['MANDATORY']) && !empty($attr['MANDATORY']))
					echo "<em>*</em>";

				echo "</td>\n";
			}
			// VP (24/2/09) : ajout valign='top' à la cellule d'affichage du champ
			print "<td class='".(isset($attr['CLASSVALUE'])?$attr['CLASSVALUE']:$this->classValue)."' valign='".(isset($attr['VALIGN'])?$attr['VALIGN']:'top')."' ".(isset($attr['COLSPAN'])?" COLSPAN=".$attr['COLSPAN']:"").(isset($attr['WIDTH'])?" WIDTH=".$attr['WIDTH']:"").">\n";
		}elseif(isset($attr['DIVTAGS'])) {
			if($attr['DIVTAGS']=="start" || $attr['DIVTAGS']=="both")
				print "<div class='".(isset($attr['CLASSVALUE'])?$attr['CLASSVALUE']:$this->classLabel)."'>\n";
			print $this->displayOperator($attr);
			if(isset($label))
				print "<label class='".(isset($attr['CLASSLABEL'])?$attr['CLASSLABEL']:$this->classLabel)."' >".$label.($attr['MANDATORY']?"<em>*</em>":"")."</label>\n";

		}

		// CHAMP(S) EFFECTIF(S)
		// VP 22/1/09 : ajout valeur par défaut depuis $_REQUEST
		if(!isset($attr['VALUE']) && isset($attr['NAME']))
			$attr['VALUE']=(isset($_REQUEST[$attr['NAME']]))?$_REQUEST[$attr['NAME']]:null;

		if (!isset($attr['NAME']))
			$attr['NAME']=null;

		if (!isset($attr['VALUE']))
			$attr['VALUE']=null;

		//Titre
		if(isset($attr['TITLE']) && defined($attr['TITLE'])) $attr['TITLE']=constant($attr['TITLE']);

		$this->displayField($attr['NAME'], $attr['VALUE'], $attr);

		// AIDE (optionel)
        if(isset($attr['HELP'])) {
            print "<span class='".(isset($attr['CLASSHELP'])?$attr['CLASSHELP']:$this->classHelp)."' ".(isset($attr['HELP_WIDTH'])?" WIDTH=".$attr['HELP_WIDTH']:"")." ".(isset($attr['HELP_JS'])?" onclick=\"".$attr['HELP_JS']."\"":"").">".(defined($attr['HELP'])?constant($attr['HELP']):$attr['HELP'])."</span>\n";
        }else if(isset($attr['HELP_IMG']) && !empty($attr['HELP_IMG'])){
            print "<span class='".(isset($attr['CLASSHELP'])?$attr['CLASSHELP']:$this->classHelp)."' ".(isset($attr['HELP_WIDTH'])?" WIDTH=".$attr['HELP_WIDTH']:"")." ".(isset($attr['HELP_JS'])?" onclick=\"".$attr['HELP_JS']."\"":"")."><img src=\"".$attr['HELP_IMG']."\" /></span>\n";
        }
		if(isset($attr['TRTAGS'])) {
            print "</td>\n";
			if($attr['TRTAGS']=="end" || $attr['TRTAGS']=="both") print "</tr>";
		}elseif(isset($attr['DIVTAGS'])) {

			if($attr['DIVTAGS']=="end" || $attr['DIVTAGS']=="both") print "</div>\n";
		}
	}

	//VG 31/05/2011 : ajout de la fonction
	//Affiche un select avec les opérateurs booléens réutilisés dans la requête SQL
	function displayOperator($attr){
		if(!isset($attr['CHOPS'])) $attr['CHOPS']="AND";
		//update VG 01/04/11 : ajout d'une gestion de libell√©s
		if(isset($attr['CHOPS_LIB'])){

			$chOp=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'OP'));
			$tabChops=explode(",",$attr['CHOPS']);
			$tabChopsLib=explode(",",$attr['CHOPS_LIB']);
			//update VG 23/05/11 : correction chTypes => chOps
			$select="<select name='chOps[".$this->countElt."]'>";
			foreach($tabChops as $i=>$x) {
				$select.= "<option value=\"".$tabChops[$i]."\"".($tabChops[$i]==$chOp?" selected":"").">".(defined($tabChopsLib[$i])?constant($tabChopsLib[$i]):$tabChopsLib[$i])."</option>";
			}
			$select.= "</select>";
			return $select;
		}
	}
	/**
	* Affiche d'un élément
	 * IN : tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	function displayField($name, $chValue, $attr){
		global $db;
		if($this->useIdxCrit){
			$idxCrit = $this->countElt;
		}
		if(isset($attr['CHFIELDS'])){
			$name="chValues[".$this->countElt."]";
			//PC 19/03/13 : ajout possibilité de faire un select sur plusieurs champs
			if (!empty($attr['MULTIFIELD'])) {
				$tmp = array("NOALL" => "1", "SELECT_OPTION" => $attr["CHFIELDS"], "SELECT_LIB" => $attr["MULTIFIELD"]);

				if (!isset($_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["FIELD"]) || empty($_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["FIELD"]))
					$_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["FIELD"]='';

				if (!isset($_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["TYPE"]) || empty($_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["TYPE"]))
					$_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["TYPE"]='';

				$chValue=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["FIELD"],'TYPE'=>$_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["TYPE"]),'VALEUR', $idxCrit));
				$this->addSelect("", "chFields[".$this->countElt."]", $_SESSION[$this->chercheObj->sessVar]["form"][$this->countElt]["FIELD"], $tmp);
			}
			else $this->addInput("hidden", "chFields[".$this->countElt."]",$attr['CHFIELDS']);
			// Cas de types multiples
			if(isset($attr['CHTYPES_LIB'])){
//				if (!isset($chValue) || empty($chValue))
//					$chValue=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'])));
//				$chValue2=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'VALEUR2'));

				$chType=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'TYPE'));
				$tabTypes=explode(",",$attr['CHTYPES']);
				$tabTypesLib=explode(",",$attr['CHTYPES_LIB']);
				print "<select name='chTypes[".$this->countElt."]'>";
				foreach($tabTypes as $i=>$x) {
					if (!isset($chValue) || empty($chValue))
                        $chValue=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'],'TYPE'=>$tabTypes[$i]),'VALEUR', $idxCrit));
                    if (!isset($chValue2) || empty($chValue2))
                        $chValue2=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'],'TYPE'=>$tabTypes[$i]),'VALEUR2', $idxCrit));
					print "<option value=\"".$tabTypes[$i]."\"".($tabTypes[$i]==$chType?" selected":"").">".(defined($tabTypesLib[$i])?constant($tabTypesLib[$i]):$tabTypesLib[$i])."</option>";
				}
				print "</select>";
			} else {
				$this->addInput("hidden", "chTypes[".$this->countElt."]",$attr['CHTYPES']);
				//by ld 28/11/08 : ajout VALUES2 pour pouvoir différencier des ROLES différents dans le cas des input indexés
				// VP 26/03/10 : test sur CHVALUES2 pour trouver la valeur cherchée

				//PC 15/10/10 : si role est défini, on met sa valeur dans chValues2
				if (isset($attr['ROLE']) && empty($attr['CHVALUES2']))
					$attr['CHVALUES2'] = $attr['ROLE'];
				if(isset($attr['CHVALUES2'])) $chValue=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'],'TYPE'=>$attr['CHTYPES'],'VALEUR2'=>$attr['CHVALUES2']),'VALEUR', $idxCrit));
				else $chValue=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'],'TYPE'=>$attr['CHTYPES']),'VALEUR', $idxCrit));
				if(isset($attr['CHVALUES2'])) $chValue2=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'],'TYPE'=>$attr['CHTYPES'],'VALEUR2'=>$attr['CHVALUES2']),'VALEUR2', $idxCrit));
				else $chValue2=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'],'TYPE'=>$attr['CHTYPES']),'VALEUR2', $idxCrit));
			}
			// VP 29/06/09 : possibilité de définir chValues
			if(isset($attr['CHVALUES'])){
				$chValue=$attr['CHVALUES'];
			}
			// Dans le cas où chValues2 est défini par défaut
			if(isset($attr['CHVALUES2'])){
				if(!isset($attr['CHVALUES2_TYPE'])) $attr['CHVALUES2_TYPE']="hidden";
				$chValue2=$attr['CHVALUES2'];
			}
			// Options de recherche (STATEGY, les autres à ajouter si nécessaire)
			if(isset($attr['CHOPTIONS_STRATEGY'])){
				if(isset($attr['CHOPTIONS_STRATEGY_LIB'])){
					print "<select name='chOptions[".$this->countElt."][STRATEGY]'>";
					$tabOptions=explode(",",$attr['CHOPTIONS_STRATEGY']);
					$tabOptionsLib=explode(",",$attr['CHOPTIONS_STRATEGY_LIB']);
					$t=$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'OPTIONS', $idxCrit);
					$chOption=str_replace('"','&quot;',$t['STRATEGY']);
					foreach($tabOptions as $i=>$x) {
						print "<option value=\"".$tabOptions[$i]."\"".($tabOptions[$i]==$chOption?" selected":"").">".(defined($tabOptionsLib[$i])?constant($tabOptionsLib[$i]):$tabOptionsLib[$i])."</option>";
					}
					print "</select>";
				}
			}
			//PC 15/03/2011 : Ajout du paramètre chOptions_role pour affiner la recherche sur personne avec leur role
			if(isset($attr['CHOPTIONS_ROLE'])){ //NB 24 04 2015 : Ajout gestion solr de chOptions_role
				if (defined('useSolr') && useSolr==true && $this->entity == 'DOC') { //NB 28042015 Solr ne gère que les docs, tout autres type de recherche doit suivre l'ancien comportement (Martin)
					$type = explode('_', $attr['CHFIELDS']);
					if (isset($type[1])) {
						$type = strtoupper($type[1]);
					}
				} else {
					$type = $attr['CHFIELDS'];
				}
				$option_role=$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'OPTIONS', $idxCrit);


				if($attr['CHOPTIONS_ROLE']=="SELECT"){
					print'<select name="chOptions['.$this->countElt.']">';
						print "<option value=''>".kTous."</option>";
						$sql = "SELECT ID_ROLE as ID,ROLE as VAL from t_role WHERE id_lang='FR' AND id_type_desc =".$db->Quote($type)."ORDER BY ROLE ASC;";
						print listOptions($sql,Array("ID","VAL"),$option_role);
					print'</select>';

				}else{
					$this->addInput("hidden", "chOptions[".$this->countElt."]",$attr['CHOPTIONS_ROLE']);
				}
			}


//			if(!isset($attr['CHOPS'])) $attr['CHOPS']="AND";
//			//update VG 01/04/11 : ajout d'une gestion de libell√©s
//			if(isset($attr['CHOPS_LIB'])){
//				$chValue=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS'])));
//				$chValue2=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'VALEUR2'));
//
//				$chOp=str_replace('"','&quot;',$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'TYPE'));
//				$tabChops=explode(",",$attr['CHOPS']);
//				$tabChopsLib=explode(",",$attr['CHOPS_LIB']);
//				//update VG 23/05/11 : correction chTypes => chOps
//				print "<select name='chOps[".$this->countElt."]'>";
//				foreach($tabChops as $i=>$x) {
//					print "<option value=\"".$tabChops[$i]."\"".($tabChops[$i]==$chOp?" selected":"").">".(defined($tabChopsLib[$i])?constant($tabChopsLib[$i]):$tabChopsLib[$i])."</option>";
//				}
//				print "</select>";
//			} else {
//				$this->addInput("hidden", "chOps[".$this->countElt."]",$attr['CHOPS']);
//			}
			if(!isset($attr['CHOPS_LIB'])){
				if(!isset($attr['CHOPS'])) $attr['CHOPS']="AND";
				$this->addInput("hidden", "chOps[".$this->countElt."]",$attr['CHOPS']);
			}		
	
			if(!empty($attr['CHOPTIONS_DEEP'])){
				$this->addInput("hidden", "chOptions[".$this->countElt."][DEEP]",$attr['CHOPTIONS_DEEP']);
			}
			// MS - 08.09.17  Ajout de l'affichage par défaut du CHOPTIONS, 
			// Piste d'amélioration : transformer automatiquement CHOPTIONS_<ABC> en input chOptions[countElt][<ABC>] , tout en évitant de traiter les cas déjà existants CHOPTIONS_TRONCATURE et CHOPTIONS_ROLE
			if(!empty($attr['CHOPTIONS'])){
				$this->addInput("hidden", "chOptions[".$this->countElt."]",$attr['CHOPTIONS']);
			}
			
			$this->addInput("hidden", "chLibs[".$this->countElt."]",(defined($attr['CHLIBS'])?constant($attr['CHLIBS']):$attr['CHLIBS']));
			if(isset($attr['CHNOHLS'])) $this->addInput("hidden", "chNoHLs[".$this->countElt."]",$attr['CHNOHLS']);
			// VP (7/10/08) : prise encompte attribut CHNOHIST
			if(isset($attr['CHNOHIST'])) $this->addInput("hidden", "chNoHist[".$this->countElt."]",$attr['CHNOHIST']);
		}
		if(isset($attr['NAME']) && !isset($attr['FIXED_VALUE']) && isset($this->chercheObj->sessVar) && isset($_SESSION[$this->chercheObj->sessVar][$attr['NAME']])){
			if(defined('useSolr') && useSolr && $attr['NAME'] == 'tri' && !empty($_SESSION[$this->chercheObj->sessVar][$attr['NAME']])){
				// la gestion du tri sur solr est gérée sous forme de tableau, il faut donc reconstruire la forme texte
				$chValue ="" ; 
				/**
				 * @author B.RAVI 08/02/17 15:33 
				 * éviter Warning: Invalid argument supplied for foreach()
				 */
				if (is_object($_SESSION[$this->chercheObj->sessVar][$attr['NAME']]) || is_array($_SESSION[$this->chercheObj->sessVar][$attr['NAME']])) {
					foreach ($_SESSION[$this->chercheObj->sessVar][$attr['NAME']] as $tri) {
						if ($chValue != '') {
							$chValue.=",";
						}
						$chValue.=implode('', $tri);
					}
				}
			}else{
				$chValue = $_SESSION[$this->chercheObj->sessVar][$attr['NAME']];
			}
		}
		// Affichage champ principal
		// VP 2/06/09 : ajout cas "buttonspan"
		// VP 24/06/09 : ajout cas "span"
		switch(strtolower($attr['TYPE'])){
			case "textarea":
				$this->addTextarea($name, $chValue, $attr);
				break;

			case "select":
				if (!isset($attr['SELECT_TYPE']))
					$attr['SELECT_TYPE']='';
				$this->addSelect($attr['SELECT_TYPE'], $name, $chValue, $attr);
				break;

			case "radio":
				if (!isset($attr['RADIO_TYPE']))
					$attr['RADIO_TYPE']='';
				$this->addRadio($attr['RADIO_TYPE'], $name, $chValue,$attr);
				break;

			case "checkbox":
				if (!isset($attr['CHECKBOX_TYPE']))
					$attr['CHECKBOX_TYPE']='';
				$this->addCheckbox($attr['CHECKBOX_TYPE'], $name, $chValue, $attr);
				break;

			case "buttonspan":
				if (!isset($attr['BUTTON_TYPE']))
					$attr['BUTTON_TYPE']='button';
				$this->addButton($attr['BUTTON_TYPE'], $name, $chValue, $attr);
				break;

			case "span":
				$this->addSpan($name, $chValue, $attr);
				break;

			case "image":
				$this->addImage($attr['TYPE'], $name, $chValue, $attr);
				break;

			case "custom":
				$this->addCustom($name, $chValue, $attr);
				break;

			default:
				$this->addInput($attr['TYPE'], $name, $chValue, $attr);
				break;
		}



		// Affichage index
		if(isset($attr['INDEX'])) {
			if(!isset($attr['INDEX_NAME'])) $attr['INDEX_NAME']="bIndex".$this->countElt;
			if(!isset($attr['INDEX_CHOOSE'])) $attr['INDEX_CHOOSE']="document.getElementById(\"".$name."\")";
			$this->addIndexBtn($attr['INDEX_NAME'], $attr['INDEX_CHOOSE'], $attr);
		}

		// Affichage chvalue2 (spécifique recherche)
		//update VG 01/04/11 : ajout gestion des classes pour le label
		if(isset($attr['CHVALUES2_TYPE'])) {
			if(isset($attr['CHVALUES2_LABEL'])) {
				if (!isset($attr['CLASSLABEL2']))
					$attr['CLASSLABEL2']='';

				print "<span class='".$attr['CLASSLABEL2']."'>";
				print (defined($attr['CHVALUES2_LABEL'])?constant($attr['CHVALUES2_LABEL']):$attr['CHVALUES2_LABEL']); // ."&nbsp;" on concaténait nbsp, j'imagine pour éviter un retour à la ligne, mais cela decale parfois le texte, à remettre si necessaire
				print "</span>";
			}
            switch($attr['CHVALUES2_TYPE']){
                case 'checkbox':
                    $this->addCheckbox($attr['CHVALUES2_TYPE'], "chValues2[".$this->countElt."]", $chValue2, $attr);
                    break;
				
				case "textarea":
					$this->addTextarea("chValues2[".$this->countElt."]", $chValue2, $attr);
					break;
                    
				case "textarea":
					$this->addTextarea("chValues2[".$this->countElt."]", $chValue2, $attr);
					break;
					
                default:
                    $this->addInput($attr['CHVALUES2_TYPE'], "chValues2[".$this->countElt."]", $chValue2, $attr);
                    break;
            }
		}
		// VP 25/9/09 : Affichage Option troncature (spécifique recherche)
		if(isset($attr['CHOPTIONS_TRONCATURE'])) {
			$attrOptions['VALUE']=$attr['CHOPTIONS_TRONCATURE'];
			$attrOptions['NAME']="chOptions[".$this->countElt."][TRONCATURE]";
			if(!isset($attr['CHOPTIONS_TRONCATURE_TYPE'])) $attr['CHOPTIONS_TRONCATURE_TYPE']="hidden";
			if(empty($this->chercheObj->sql) || $attr['CHOPTIONS_TRONCATURE_TYPE']=="hidden") {
				$chOption=$attr['CHOPTIONS_TRONCATURE_DEFAUT'];
			}else{
				$tabChercheOptions=$this->chercheObj->getValueForField(array('FIELD'=>$attr['CHFIELDS']),'OPTIONS', $idxCrit);
				$chOption=str_replace('"','&quot;',$tabChercheOptions['TRONCATURE']);
			}
			if($attr['CHOPTIONS_TRONCATURE_TYPE']=="checkbox"){
				// VP 26/03/10 : Option tips pour case à cocher troncature (CHOPTIONS_TRONCATURE_TIPS)
				if(isset($attr['CHOPTIONS_TRONCATURE_TIPS'])) $attrOptions['TITLE']=(defined($attr['CHOPTIONS_TRONCATURE_TIPS'])?constant($attr['CHOPTIONS_TRONCATURE_TIPS']):$attr['CHOPTIONS_TRONCATURE_TIPS']);
				$this->addCheckbox("checkbox",$attrOptions['NAME'] , $chOption,$attrOptions);
			}else{
				$this->addInput($attr['CHOPTIONS_TRONCATURE_TYPE'], "chOptions[".$this->countElt."][TRONCATURE]", $chOption,$attrOptions);
			}
			if(!empty($attr['CHOPTIONS_TRONCATURE_LABEL'])) print (defined($attr['CHOPTIONS_TRONCATURE_LABEL'])?constant($attr['CHOPTIONS_TRONCATURE_LABEL']):$attr['CHOPTIONS_TRONCATURE_LABEL'])."&nbsp;";
			unset($attrOptions);
		}

	}
	/**
     * Affiche un champ de recherche sélectionnable
	 * IN : nom, valeur par défaut, tableau de paramètres
	 * OUT : affichage du champ
	 **/
	// VP 04/07/12 : Création fonction addMultifield()
	function displayEltMulti($attr=null){
        global $db;

		if (isset($attr['FIRST_FIELD_ID']) && !empty($attr['FIRST_FIELD_ID']))
			$this->countElt=$attr['FIRST_FIELD_ID']-1;
        $first=1 + $this->countElt;
        // Création champs
        $attr['NB_FIELDS']=(empty($attr['NB_FIELDS'])?1:$attr['NB_FIELDS']);

		if(isset($attr['VAR_SUFFIXE']) && !empty($attr['VAR_SUFFIXE'])){
			$suffixe= $attr['VAR_SUFFIXE'];
		}else{
			$suffixe = "";
		}

		for ($i=1;$i<=$attr['NB_FIELDS'];$i++)
		{
			$this->countElt++;
			$k=$this->countElt;
			echo '<tr>
			<td class="'.(isset($attr['CLASSLABEL'])?$attr['CLASSLABEL']:$this->classLabel).'" valign="top">';

			if (isset($attr['CHOPS']) && !empty($attr['CHOPS']) && isset($attr['CHOPS_LIB']) && !empty($attr['CHOPS_LIB']))
			{
				$operateurs=explode(',',$attr['CHOPS']);
				$libelles=explode(',',$attr['CHOPS_LIB']);

				echo '<select id="chOps['.$k.']" name="chOps['.$k.']">';

				for ($j=0; $j<count($operateurs); $j++)
				{
					/*if (1)
						echo '<option value="'.$operateurs[$j].'">'.$libelles[$j].'</option>';
					else*/
						echo '<option value="'.$operateurs[$j].'">'.(defined($libelles[$j])?constant($libelles[$j]):$libelles[$j]).'</option>';

				}
				echo '</select>';
			}
			else
				echo '<input type="hidden" id="chOps['.$k.']" name="chOps['.$k.']" value="AND" />';

			echo '<span id="type_'.$k.'"></span>
			<input type="hidden" id="chFields['.$k.']" name="chFields['.$k.']" value="" />
			</td>
			<td class="'.(isset($attr['CLASSVALUE'])?$attr['CLASSVALUE']:$this->classValue).'" valign="top">
			<span id="typeRech['.$k.']"></span>
			<input type="text" id="chValues['.$k.']" name="chValues['.$k.']" value="" class="'.$attr['CLASS'].'"/>
			<input type="hidden" id="chValues2['.$k.']" name="chValues2['.$k.']" value="" />
			<input type="hidden" id="chLibs['.$k.']" name="chLibs['.$k.']" value="" />
			<input type="hidden" id="chNoHLs['.$k.']" name="chNoHLs['.$k.']" value="" />
			<input type="hidden" id="chOptions['.$k.']" name="chOptions['.$k.']" value="" />
			<input type="button" style="display:none;" name="btnIndex_'.$k.'" id="btnIndex_'.$k.'" value="'.(!empty($attr['INDEX'])?$attr['INDEX']:'Index').'" label="">
			</td>
			</tr>';

		}
        echo "<script type=\"text/javascript\">\n var tabCrit".$suffixe." = new Array();\n";
        if ($this->chercheObj && isset($_SESSION[$this->chercheObj->sessVar]['form'])) {
			// VP 5/04/2017 : filtrage du tableau form par le premier indice du champ multi
            //$compdTab=array_values($_SESSION[$this->chercheObj->sessVar]['form']);
            
			$compdTab=$_SESSION[$this->chercheObj->sessVar]['form'];
			$crit_idx=$first;
            foreach($compdTab as $idx=>$crit) {
				if($idx>=$first && $idx<$first+$attr['NB_FIELDS']){
					echo "tabCrit".$suffixe."[".($crit_idx)."] = new Array();\n";
					foreach ($crit as $fld=>$val) {
						if (empty($val)){
							echo "tabCrit".$suffixe."[".($crit_idx)."]['".$fld."']='';\n";
						}elseif(is_array($val)){
							echo "tabCrit".$suffixe."[".($crit_idx)."]['".$fld."']=".quoteField(implode(',',$val)).";\n";
						}else{
							echo "tabCrit".$suffixe."[".($crit_idx)."]['".$fld."']=".quoteField($val).";\n";
						}
					}
					$crit_idx++;
				}
            }
        }
        /*
		if(defined('useSolr') && useSolr==true)
		{
			if (isset($_SESSION['recherche_DOC_Solr']['form']))
			{
				// $compdTab=array_values($_SESSION['recherche_DOC_Solr']['form']);
				foreach($_SESSION['recherche_DOC_Solr']['form'] as $idx=>$crit) {
					echo "tabCrit[".($idx)."] = new Array();\n";
					foreach ($crit as $fld=>$val) {
                        if(is_array($val)){
                            echo "tabCrit[".($idx)."]['".$fld."']=".quoteField(implode(',',$val)).";\n";
                        }else{
                            echo "tabCrit[".($idx)."]['".$fld."']=".quoteField($val).";\n";
                        }
					}
				}
			}
		}
		else if (isset($_SESSION['recherche_DOC']['form'])) {
			$compdTab=array_values($_SESSION['recherche_DOC']['form']);
			foreach($compdTab as $idx=>$crit) {
                echo "tabCrit[".($idx+1)."] = new Array();\n";
                foreach ($crit as $fld=>$val) {
					if (empty($val)){
						echo "tabCrit[".($idx+1)."]['".$fld."']='';\n";
					}elseif(is_array($val)){
						echo "tabCrit[".($idx+1)."]['".$fld."']=".quoteField(implode(',',$val)).";\n";
					}else{
						echo "tabCrit[".($idx+1)."]['".$fld."']=".quoteField($val).";\n";
					}
                }
			}
		}
         */


        echo "var tabRech".$suffixe."=new Array();
        i=0;";
		
		// MS - 19.06.15 - Ajout possibilité d'avoir des datepicker dans les éléments multi (recherche experte);
		// Pour l'instant, on défini des options datepicker communes à l'ensemble des innerFields, puis on active au cas par cas en répétant le tag datepicker à 1 pour chaque champ sur lequel on veut activer le calendrier
		if (isset($attr['DATEPICKER']) && !empty($attr['DATEPICKER']))
		{
			$config_dp='';
			//seul image fait appel a datepicker
			if (isset($attr['DATEPICKER_ONLY_IMG']) && !empty($attr['DATEPICKER_ONLY_IMG'])){
			$config_dp.= 'buttonImage: "'.designUrl.'/images/calendar.gif",
						  buttonImageOnly: true,
						  showOn : true,';
			}	
			// MS - ajout changeYear : true , permet de changer l'année par le biais d'un select
			$config_dp.='dateFormat: "yy-mm-dd",
						dayNamesMin: ["'.kDimancheShort.'", "'.kLundiShort.'", "'.kMardiShort.'", "'.kMercrediShort.'", "'.kJeudiShort.'", "'.kVendrediShort.'", "'.kSamediShort.'"],
						monthNames: ["'.kJanvier.'","'.kFevrier.'","'.kMars.'","'.kAvril.'","'.kMai.'","'.kJuin.'","'.kJuillet.'","'.kAout.'","'.kSeptembre.'","'.kOctobre.'","'.kNovembre.'","'.kDecembre.'"],
						monthNamesShort: ["'.kJanvierShort.'","'.kFevrierShort.'","'.kMarsShort.'","'.kAvrilShort.'","'.kMaiShort.'","'.kJuinShort.'","'.kJuilletShort.'","'.kAoutShort.'","'.kSeptembreShort.'","'.kOctobreShort.'","'.kNovembreShort.'","'.kDecembreShort.'"],
						changeYear : true,
						changeMonth : true';
						
		
			
			// MS ajout possibilité de configurer le range d'années selectionnables par l'option changeYear 
			// voir documentation datepicker jquery pour la syntaxe 
			if(isset($attr['DATEPICKER_YEAR_RANGE']) && !empty($attr['DATEPICKER_YEAR_RANGE'])){
				if(!preg_match('/^(["\']).*\1$/m', $attr['DATEPICKER_YEAR_RANGE'])){
					$attr['DATEPICKER_YEAR_RANGE'] = '"'.$attr['DATEPICKER_YEAR_RANGE'].'"';
				}
				$config_dp .=', yearRange : '.$attr['DATEPICKER_YEAR_RANGE'];
			}
			
			if (isset($attr['DATEPICKER_IMG']) && !empty($attr['DATEPICKER_IMG']))
				$datepicker_img=$attr['DATEPICKER_IMG'];
			else
				$datepicker_img='calendar.gif';
			
				echo '	var dp_config=
					{
						'.$config_dp.'
					};
					';
		}
        if(isset($attr['INNERFIELDS'])){
            $tabElt=$attr['INNERFIELDS'][0]['ELEMENT'];
            //Tri des champs
            //$tabElt=sortArrayByField ( $tabElt, "CHLIBS");
            foreach($tabElt as $idx=>$attrElt){
                
                if(isset($attrElt['PROFIL_MIN'])){
					$myUsr=User::getInstance();
                    $profil_min = defined($attrElt['PROFIL_MIN'])?constant($attrElt['PROFIL_MIN']):$attrElt['PROFIL_MIN'];
					if($myUsr->getTypeLog() < $profil_min ) {continue;}
				}
                if(isset($attrElt['PROFIL_IDS'])){
					$myUsr=User::getInstance();
					$ids = explode(",", $attrElt['PROFIL_IDS']);
                    array_walk($ids, create_function('&$v', '$v = defined($v)?constant($v):$v;'));
					if(!in_array($myUsr->getTypeLog(), $ids)) {continue;}
				}
                
				echo "tabRech".$suffixe."[".$idx."]=new Array();\n";
				
				echo "tabRech".$suffixe."[".$idx."]['FIELDS']='".((isset($attrElt['CHFIELDS']))?$attrElt['CHFIELDS']:'')."';\n";
                echo "tabRech".$suffixe."[".$idx."]['TYPES']='".((isset($attrElt['CHTYPES']))?$attrElt['CHTYPES']:'')."';\n";
                /*
                 * @author B.RAVI 2015-month-day
                 * Pouvoir appeler une fct spécifique quand on choisit une value dans la palette
                 */
                echo "tabRech".$suffixe."[".$idx."]['RTN']='".((isset($attrElt['INDEX_RTN']))?$attrElt['INDEX_RTN']:'')."';\n";
                // @author B.RAVI 2016-02-12 Pouvoir appeler &affich_nb=1 dans la palette (chercheIndex2.php)
                echo "tabRech".$suffixe."[".$idx."]['INDEX_NB']='".((isset($attrElt['INDEX_NB']))?$attrElt['INDEX_NB']:'')."';\n";
                if(isset($attrElt['CHOPTIONS_ROLE'])){
                   echo "tabRech".$suffixe."[".$idx."]['OPTIONS']=".quoteField($attrElt['CHOPTIONS_ROLE']).";\n";
                }
                if(isset($attrElt['CHTYPES_LIB'])){
                   //B.RAVI 2015-09-15 pouvoir utiliser des constantes dans <CHTYPES_LIB>
                    $tab_CHTYPES_LIB=explode(',',$attrElt['CHTYPES_LIB']);
                    $tab_dynamicCHTYPES_LIB=array();
                    foreach ($tab_CHTYPES_LIB as $v){
                        if (defined($v)) {
                       $tab_dynamicCHTYPES_LIB[]=constant($v);
                        } 
                        else {$tab_dynamicCHTYPES_LIB[]=$v;
                       }
                    }
                   echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']=".quoteField(implode(',',$tab_dynamicCHTYPES_LIB)).";\n";
                }else {
					if (isset($attrElt['CHTYPES']) && !empty($attrElt['CHTYPES']))
					{
						switch($attrElt['CHTYPES']){
							case "FT":
								echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='".kRechercheTypeRechFullText."';\n";
								break;
							case "L":
							case "V":
							case "P":
								echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='".kRechercheTypeRechSelect."';\n";
								break;
							default:
								echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='".kRechercheTypeRechExact."';\n";
								break;
						}
					}
					else
						echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='';\n";
                }
				
				$arr_fields_copied = array() ; 
				
				foreach($attrElt as $key=>$val){
					switch ($key){
						// echo "//".$key." ".$val."\n";
						case "CHFIELDS" : 
						case "CHTYPES" : 
						break ; 
						
						case "INDEX_RTN" : 
							/*
							 * @author B.RAVI 2015-month-day
							 * Pouvoir appeler une fct spécifique quand on choisit une value dans la palette
							 */
							echo "tabRech".$suffixe."[".$idx."]['RTN']='".((isset($attrElt['INDEX_RTN']))?$attrElt['INDEX_RTN']:'')."';\n";
						break ; 
						
						
						case "CHOPTIONS_ROLE" : 
							echo "tabRech".$suffixe."[".$idx."]['OPTIONS']=".quoteField($attrElt['CHOPTIONS_ROLE']).";\n";
						break ;

						case "DATEPICKER" : 
							echo "tabRech".$suffixe."[".$idx."]['DATEPICKER']=1;\n";
						break ; 
						
						case "CHLIBS" : 
							echo "tabRech".$suffixe."[".$idx."]['LIB']=".quoteField(defined($attrElt['CHLIBS'])?constant($attrElt['CHLIBS']):$attrElt['CHLIBS']).";\n";
							
						break ; 
						
						case 'CHTYPES_LIB':
							   //B.RAVI 2015-09-15 pouvoir utiliser des constantes dans <CHTYPES_LIB>
								$tab_CHTYPES_LIB=explode(',',$attrElt['CHTYPES_LIB']);
								$tab_dynamicCHTYPES_LIB=array();
								foreach ($tab_CHTYPES_LIB as $v){
									if (defined($v)) {
								   $tab_dynamicCHTYPES_LIB[]=constant($v);
									} 
									else {$tab_dynamicCHTYPES_LIB[]=$v;
								   }
								}
							   echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']=".quoteField(implode(',',$tab_dynamicCHTYPES_LIB)).";\n";

						break ; 
						
						case "CHNOHLS" : 
							echo "tabRech".$suffixe."[".$idx."]['NOHL']='".$attrElt['CHNOHLS']."';\n";
						break ;

						case "CHVALUES2" : 
							echo "tabRech".$suffixe."[".$idx."]['VALUES2']=".quoteField($attrElt['CHVALUES2']).";\n";
						break ; 
						
						case "HIDDEN" : 
							/* quand la liste déroulante de chType n'a qu'un seul choix, on peut choisir de ne pas l'afficher en rajoutant <hidden>1</hidden> dans le xml
							 * le fait que la liste déroulante soit caché n'empeche que sa valeur soit bien prise en compte dans le processus de recherche 
							 */
							echo "tabRech".$suffixe."[".$idx."]['HIDDEN']=1;\n";
						break ; 
						
						case "SELECT_SQL": 
						case "SELECT_SOLR_INDEX": 
							// MS - 28.07.16 - Si on a un select_sql dans un innerfields, on va alimenter l'objet SELECT_VALS de la partie tabRech associé au champ. 
							if(isset($attrElt['SELECT_SQL']) && strpos(trim(strtoupper($attrElt['SELECT_SQL'])),'SELECT')===0){
								$values = $db->GetAll($attrElt['SELECT_SQL']);
								echo "tabRech".$suffixe."[".$idx."]['SELECT_VALS'] = {} ;\n";
								if(!empty($values)){
									foreach($values as $id=>$val){
										if(isset($val['ID']) && isset($val['VAL'])){
											echo "tabRech".$suffixe."[".$idx."]['SELECT_VALS'][".quoteField($val['ID'])."]=".quoteField($val['VAL']).";\n";
										}
									}
								}
							}else if(isset($attrElt['SELECT_SOLR_INDEX']) && !empty($attrElt['SELECT_SOLR_INDEX'])){
								$values = $this->chercheObj->browseIndex($attrElt['SELECT_SOLR_INDEX'],'',1000,((isset($attrElt['SELECT_SOLR_INDEX_ALPHASORT']) && !empty($attrElt['SELECT_SOLR_INDEX_ALPHASORT']))?true:false));
								echo "tabRech".$suffixe."[".$idx."]['SELECT_VALS'] = {} ;\n";
								if(!empty($values)){
									foreach($values as $valeur=>$nb){
										if(!empty($valeur)){
											echo "tabRech".$suffixe."[".$idx."]['SELECT_VALS'][".quoteField($valeur)."]=".quoteField($valeur).";\n";
										}
									}
								}
							}
						break ;
						
						case   "AUTOCOMPLETE" : 
						case   "AUTOCOMPLETE_FUNC" : 
						case   "AUTOCOMPLETE_QUOTE" : 
							if($key == 'AUTOCOMPLETE') {
								$funcAutocomplete = (!empty($attrElt['AUTOCOMPLETE_FUNC'])?htmlentities($attrElt['AUTOCOMPLETE_FUNC']):"initAutoComplete");
								$quoteSuggestions = (!empty($attrElt['AUTOCOMPLETE_QUOTE'])?"".$attrElt['AUTOCOMPLETE_QUOTE']:"false");
								$autocomplete_urlaction = (!empty($attrElt['AUTOCOMPLETE_URLACTION'])?"".$attrElt['AUTOCOMPLETE_URLACTION']:"chercheMotsSolr");
								echo "tabRech".$suffixe."[".$idx."]['AUTOCOMPLETE']=".quoteField($attrElt['AUTOCOMPLETE']).";\n";
								echo "tabRech".$suffixe."[".$idx."]['AUTOCOMPLETE_FUNC']=".quoteField($funcAutocomplete).";\n";
								echo "tabRech".$suffixe."[".$idx."]['AUTOCOMPLETE_QUOTE']=".quoteField($quoteSuggestions).";\n";
								echo "tabRech".$suffixe."[".$idx."]['AUTOCOMPLETE_URLACTION']=".quoteField($autocomplete_urlaction).";\n";
							}
						break ; 
						
						case "INDEX_CHAMP" : 
						case "INDEX_JS_FUNC" : 
						case "INDEX_IMG" : 
						case "INDEX_LEX_AFF" : 
							if($key == 'INDEX_CHAMP'){
								echo "tabRech".$suffixe."[".$idx."]['SELECTOR']='".$attrElt['INDEX_CHAMP']."';\n";
								if(isset($attrElt['INDEX_JS_FUNC'])) echo "tabRech".$suffixe."[".$idx."]['JS_FUNC']='".$attrElt['INDEX_JS_FUNC']."';\n";
								if(isset($attrElt['INDEX_IMG'])) echo "tabRech".$suffixe."[".$idx."]['INDEX_IMG']='".$attrElt['INDEX_IMG']."';\n";
								if(isset($attrElt['INDEX_LEX_AFF'])) echo "tabRech".$suffixe."[".$idx."]['INDEX_LEX_AFF']='".$attrElt['INDEX_LEX_AFF']."';\n";
							}
						break ; 
						
						default : 
							echo "tabRech".$suffixe."[".$idx."]['".$key."']=".quoteField($attrElt[$key]).";\n";
						break ; 	
					}
					// MS - certains champs sont considérés comme toujours présents dans la fonction actualize. Jusqu'a ce que cela soit changé, on vérifie l'existence de ces paramètres de façon à les set à vide si ils n'existent pas
					if(!isset($attrElt['CHLIBS']) || empty($attrElt['CHLIBS'])){
						echo "tabRech".$suffixe."[".$idx."]['LIB']='';\n";
					}
					if(!isset($attrElt['INDEX_CHAMP'])){
						echo "tabRech".$suffixe."[".$idx."]['SELECTOR']='';\n";
					}
					
					
					if(!isset($attrElt['CHTYPES_LIB']) || empty($attrElt['CHTYPES_LIB'])) {
						if (isset($attrElt['CHTYPES']) && !empty($attrElt['CHTYPES'])) {
							switch($attrElt['CHTYPES']){
								case "FT":
									echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='".kRechercheTypeRechFullText."';\n";
									break;
								case "L":
								case "V":
								case "P":
								case "F":
									echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='".kRechercheTypeRechSelect."';\n";
									break;
								default:
									echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='".kRechercheTypeRechExact."';\n";
									break;
							}
						}
						else
							echo "tabRech".$suffixe."[".$idx."]['TYPES_LIB']='';\n";
					}
				}
			}
        }
		// MS - 30.06.15 - le param "end" de initCriteres (second paramètre) est $first + attr[NB_FIELDS], et non pas $attr[NB_FIELDS]-1
        echo "\$j(document).ready(initCriteres(".$first.",".($first+$attr['NB_FIELDS']-1).",tabRech".$suffixe.",tabCrit".$suffixe."));";
        echo "</script>\n";

	}

}
?>
