<?php

class Stats
{
	public $sql;
	public $sqlSelect;
	public $sqlFrom;
	public $sqlWhere;
	public $sqlGroup;
	public $sqlOrder;
	public $sqlEnd = "";
	
	public $arrSql;
	public $arrLib;
	public $arrSearch = array();
	public $titre;
	public $arrEmptyLegend;
	
	var $filDistrib;
	var $filPrecis;
	var $coreSql;
	var $error_msg;
	
	function __construct()
	{
		$this->arrSql=array();
		$this->setEntity("action");
	}
	
	function init($arrStat) {
		foreach ($arrStat as $id=>$stat)
			if (isset($stat['SELECT']) && isset($stat['WHERE'])) {
				$this->arrSql[$id] = $stat;
			}
			elseif ($id == "LIB")
				$this->arrLib = $stat;
	}
	
	function initEmptyLegend($arrEmptyLegend) {
		$this->arrEmptyLegend = $arrEmptyLegend;
	}
	
	function setParamForSearch($arrData) {
		foreach ($arrData as $key=>$data)
			if (in_array(strtolower($key),array(
				"stat_date_deb",
				"stat_date_fin",
				"stat_auth",
				"stat_usager",
				"stat_us_societe",
				"stat_groupe",
				"stat_media",
				"stat_type_doc",
				"stat_etat_doc",
				"stat_acces",
				"stat_doc_titre",
				"stat_doc_titre_col",
				"stat_fonds",
				"stat_base",
				"stat_type_commande",
				"stat_panxml",
				"exclude_admins"
			)) && ($data!="")  ){
				$this->arrSearch[$key] = $data;
			}
	}
	
	private function constructSqlWhere() {
		global $db;
		
		foreach ($this->arrSearch as $field=>$val)
			switch ($field) {
				case "stat_date_deb" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					//$this->sqlWhere .= $db->SQLDate('Y-m-d',"a.ACT_DATE").">='".fDate($val)."' "; // Pb de performances dus à TO_CHAR(a.ACT_DATE,'YYYY-MM-DD')
                    $this->sqlWhere .= $this->stat_date." >='".fDate($val)."' ";
					break;
				case "stat_date_fin" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
                    //$this->sqlWhere .= $db->SQLDate('Y-m-d',"a.ACT_DATE")."<='".fDate($val)."' "; // Pb de performances dus à TO_CHAR(a.ACT_DATE,'YYYY-MM-DD')
                    $this->sqlWhere .= $this->stat_date." <='".fDate($val)."' ";
					break;
				case "stat_auth" :
					if($val == '0'){
						if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
						$this->sqlWhere .= " (u.ID_USAGER = 0 or u.ID_USAGER IS NULL) ";
					}else if ($val == '1'){
						if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
						$this->sqlWhere .= " (u.ID_USAGER != 0 AND u.ID_USAGER IS NOT NULL) ";
					}
					break ; 
				case "stat_usager" :
					$aStatsUsager = explode (',', $val);
					$allUsager = '';
					foreach ($aStatsUsager as $usager) $allUsagerSQL .= "'".trim(trim($usager,'"'))."',";
					$allUsagerSQL = substr($allUsagerSQL, 0, -1);
					
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "u.ID_USAGER in (select ID_USAGER from t_usager where ".$db->Concat('US_NOM',"' '",'US_PRENOM')." in (".$allUsagerSQL.") or US_NOM in (".$allUsagerSQL."))";
					break;
				case "stat_us_societe" :
					$socs = explode(',',$val) ;
					trace(print_r($socs,1));
					if(!empty($socs)){
						if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
						$this->sqlWhere.='(';
						foreach($socs as $idx => $soc){
							if($idx>0){
								$this->sqlWhere .= " OR ";
							}
							$this->sqlWhere .= "u.US_SOCIETE ".$db->Like($soc);
						}
						$this->sqlWhere.=')';
					}
					break;
				case "stat_groupe" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "g.ID_GROUPE = ".intval($val);
					break;
				case "exclude_admins" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "(u.US_ID_TYPE_USAGER is null or u.US_ID_TYPE_USAGER < ".kLoggedDoc.")";
					break;
				case "stat_media" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "DOC_ID_MEDIA = '".$val."'";
					break;	
				case "stat_type_doc" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "DOC_ID_TYPE_DOC = '".$val."'";
					break;
				case "stat_etat_doc" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "DOC_ID_ETAT_DOC = '".$val."'";
					break;
				case "stat_acces" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "DOC_ACCES = '".$val."'";
					break;
				case "stat_fonds" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					if(is_array($val)){
						$this->sqlWhere .= "DOC_ID_FONDS in (0,".implode(",", $val).")";
					}else if (strpos($val,',') !== false){
						$this->sqlWhere .= "DOC_ID_FONDS in (".($val).")";
					}else{
						$this->sqlWhere .= "DOC_ID_FONDS = ".intval($val);
					}
					break;
				case "stat_type_commande" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "PAN_ID_TYPE_COMMANDE = '".$val."'";
					break;
				case "stat_panxml" :
					if(is_array($val)){
						$sql_panxml="";
						foreach($val as $k=>$v){
							if(!empty($v)){
								if(!empty($sql_panxml)) $sql_panxml .=" AND ";
								$sql_panxml .= "PAN_XML ".$db->like("%<".$k.">".str_replace("*","%",$v)."</".$k.">%");
							}
						}
						if(!empty($sql_panxml)){
							if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
							$this->sqlWhere .= "(".$sql_panxml.")";
						}
					}else{
						if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
						$this->sqlWhere .= "PAN_XML ".$db->like("%".str_replace("*","%",$val)."%");
					}
					break;
				case "stat_base" :
					if(!empty($this->stat_base)){
						if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
						if(is_array($val)){
							$this->sqlWhere .= $this->stat_base." in ('".implode("','", $val)."')";
						}else{
							$this->sqlWhere .= $this->stat_base." = '".$val."'";
						}
					}
					break;
				case "exclude_admins" :
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= "(u.US_ID_TYPE_USAGER is null or u.US_ID_TYPE_USAGER < ".kLoggedDoc.")";
					break;
				case "stat_doc_titre":
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= '(';
					$n=0;
					foreach(explode(',',$val) as $v){
						if ($n) {
							$this->sqlWhere .=" OR ";
						}
						$this->sqlWhere .= "d.doc_titre ILIKE ".$db->Quote('%'.$v.'%');	
						$n++;
					}
					$this->sqlWhere .= ')';
					break;

					
				case "stat_doc_titre_col":
					if(!empty($this->sqlWhere)) $this->sqlWhere.= " AND "; else $this->sqlWhere="WHERE ";
					$this->sqlWhere .= '(';
					$n=0;
					foreach(explode(',',$val) as $v){
						if ($n) {
							$this->sqlWhere .=" OR ";
						}
						$this->sqlWhere .= "d.doc_titre_col ILIKE ".$db->Quote('%'.$v.'%');	
						$n++;
					}
					$this->sqlWhere .= ')';	

					break;
					
					
				default :
					break;
			}
	}
	
	function setParamForFilter($distribution, $precision) {
		$this->filDistrib = $distribution;
		$this->filPrecis = $precision;
	}
	
	function constructSql($type_stat, $presentation) {
		if (isset($this->arrSql[$type_stat])) {
			$this->arrSql = $this->arrSql[$type_stat];
			if(isset($this->arrSql['ENTITY'])){
				$this->setEntity($this->arrSql['ENTITY']);
			}
			$this->sqlSelect = "SELECT ".$this->arrSql['SELECT'];
			$this->sqlFrom = $this->coreSql." ".$this->arrSql['JOIN'];
			$this->sqlWhere = "WHERE ".$this->arrSql['WHERE'];
			if (!empty($this->arrSql['GROUP'])) $this->sqlGroup = "GROUP BY ".$this->arrSql['GROUP'];
			
			if (is_array($this->arrSql['LIB'])) $this->arrLib = array_merge($this->arrLib, $this->arrSql['LIB']);
			
			switch ($presentation) {
				case "FIL":
					$this->constructSqlFiltered();
					$this->genereTitleFil((strpos($this->sqlSelect, "PRECIS")!==false?"PRECIS":""), (strpos($this->sqlSelect, "DISTRIB")!==false?"DISTRIB":""), "NB");
					break;
				case "TOP":
					$this->constructSqlTop();
					$this->genereTitleTop();
					break;
				default:
					$this->constructSqlBrut();
					break;
			}
			$this->constructSqlWhere();
		}
	}
	
	function setEntity($entity) {
		switch ($entity) {
			case "doc" :
				$this->coreSql="FROM t_doc d
				LEFT OUTER JOIN t_usager u on u.ID_USAGER = d.DOC_ID_USAGER_CREA
				LEFT OUTER JOIN t_usager_groupe ug ON ug.ID_USAGER = u.ID_USAGER
				LEFT OUTER JOIN t_groupe g ON g.ID_GROUPE = ug.ID_GROUPE";
				$this->stat_date="DOC_DATE_CREA";
				$this->stat_pays="";
				$this->stat_base="";
				break;
			case "panier" :
				$this->coreSql="FROM t_panier p
				LEFT OUTER JOIN t_usager u on u.ID_USAGER = p.PAN_ID_USAGER
				LEFT OUTER JOIN t_usager_groupe ug ON ug.ID_USAGER = u.ID_USAGER
				LEFT OUTER JOIN t_groupe g ON g.ID_GROUPE = ug.ID_GROUPE";
				$this->stat_date="PAN_DATE_COM";
				$this->stat_pays="";
				$this->stat_base="";
				break;
			case "action" :
			default :
				$this->coreSql="FROM t_action a
				LEFT OUTER JOIN t_usager u on u.ID_USAGER = a.ID_USAGER
				LEFT OUTER JOIN t_usager_groupe ug ON ug.ID_USAGER = u.ID_USAGER
				LEFT OUTER JOIN t_groupe g ON g.ID_GROUPE = ug.ID_GROUPE";
				$this->stat_date="ACT_DATE";
				$this->stat_pays="ACT_PAYS";
				$this->stat_base="ACT_BASE";
				break;
		}
	}

	private function constructSqlFiltered() {
		global $db;
        if (!empty($this->arrSql['COUNT'])){
            $this->sqlSelect = "SELECT ".$this->arrSql['COUNT']." as NB";
        } else {
            $this->sqlSelect = "SELECT count(1) as NB";
        }
		
		if (isset($this->filDistrib) && !empty($this->filDistrib)) {
			$arr = explode("/", $this->filDistrib);
			if (count($arr) >= 2) {
				$dist_grp = $arr[0];
				$dist_sel = $arr[1];
			} else {
				$dist_grp = $this->filDistrib;
				$dist_sel = $this->filDistrib;
			}
			$this->sqlSelect = str_replace("SELECT ", "SELECT $dist_sel as DISTRIB, ", $this->sqlSelect);
			if (empty($this->sqlGroup)) $this->sqlGroup = "GROUP BY"; else $this->sqlGroup .= ",";
			$this->sqlGroup .= " $dist_grp";
		}
		
		if (isset($this->filPrecis) && intval($this->filPrecis) > 0) {
			
			//XB utilise la class connection pour differencier mysql et postgre
			//MYSQL : $this->sqlSelect = str_replace("SELECT ", "SELECT SUBSTRING(a.ACT_DATE,1,".$this->filPrecis.") as PRECIS, ", $this->sqlSelect);
			// POSTGRE : $this->sqlSelect = str_replace("SELECT ", "SELECT SUBSTRING(a.ACT_DATE::text,1,".$this->filPrecis.") as PRECIS, ", $this->sqlSelect);
			$this->sqlSelect = str_replace("SELECT ", "SELECT SUBSTRING(".$db->cast_to_string($this->stat_date).",1,".$this->filPrecis.") as PRECIS, ", $this->sqlSelect);
		
			if (empty($this->sqlGroup)) $this->sqlGroup = "GROUP BY"; else $this->sqlGroup .= ",";
			$this->sqlGroup .= " PRECIS";
			if (empty($this->sqlOrder)) $this->sqlOrder = "ORDER BY"; else $this->sqlOrder .= ",";
			$this->sqlOrder .= " PRECIS DESC";
		}else {
			if (empty($this->sqlOrder)) $this->sqlOrder = "ORDER BY"; else $this->sqlOrder .= ",";
			$this->sqlOrder .= " NB DESC";
		}
	}
	
	private function constructSqlTop() {
		if (preg_match("|SELECT (.*)|",$this->sqlSelect,$matches)) {
			$fields = $matches[1];
			while (preg_match("|AS (.*),|i",$fields,$matchess)) $fields = str_replace($matchess[0], ",", $fields);
			if (preg_match("|AS (.*)$|i",$fields,$matchess)) $fields = str_replace($matchess[0], "", $fields);
			if ( preg_match("|GROUPE,|i",$fields,$matchess) ) {
				$fields = str_replace($matchess[0], "", $fields);
			} elseif ( preg_match("|GROUPE$|i",$fields,$matchess) ) {
				$fields = str_replace($matchess[0], "", $fields);
			}
			
			if (empty($this->sqlGroup)) $this->sqlGroup = "GROUP BY"; else $this->sqlGroup .= ",";
			$this->sqlGroup .= " ".$fields;
		}
		//$this->sqlSelect = str_replace("SELECT ", "SELECT TOP 25 ", $this->sqlSelect);
        if (!empty($this->arrSql['COUNT'])){
            $this->sqlSelect .= ", ".$this->arrSql['COUNT']." as NB";
        } else {
            $this->sqlSelect .= ", count(1) as NB";
        }
		$this->sqlOrder = "ORDER BY NB DESC";
		$this->sqlEnd = "LIMIT 25";
		
	}
	
	private function constructSqlBrut() {
		global $db;
		
		if (!empty($this->arrSql['SELECTBRUT']))
			$this->sqlSelect .= ", ".$this->arrSql['SELECTBRUT'];
		$this->sqlSelect = str_replace("SELECT ", "SELECT ".$this->stat_date." as DATE, ", $this->sqlSelect);
		if (strpos($this->sqlSelect, " AS USAGER") === false)
			$this->sqlSelect .= ", ".$db->Concat("u.US_NOM", "' '", "u.US_PRENOM")." AS USAGER";
		if(!empty($this->stat_pays))
			$this->sqlSelect .= ", ".$this->stat_pays." as PAYS";
		if(!empty($this->stat_base))
			$this->sqlSelect .= ", ".$this->stat_base." as BASE";
		
		$this->sqlOrder = "ORDER BY DATE DESC";
		
		if (!empty($this->sqlGroup)) {
			$this->sqlGroup .= ", u.US_PRENOM, u.US_NOM";
			$this->sqlSelect = str_replace("SELECT ".$this->stat_date." as DATE, ", "SELECT min(".$this->stat_date.") as DATE, ", $this->sqlSelect);
		} else {
			
			$fields = '';
			if(preg_match("|SELECT (.*)|",$this->sqlSelect,$matches))  {
				$fields = $matches[1];
			}
			while ( preg_match("|AS (.+?),|i",$fields,$matchess) ) $fields = str_replace($matchess[0], ",", $fields);
			if ( preg_match("|AS (.*)$|i",$fields,$matchess) ) $fields = str_replace($matchess[0], "", $fields);
			if ( preg_match("|GROUPE,|i",$fields,$matchess) ) {
				$fields = str_replace($matchess[0], "", $fields);
			} elseif ( preg_match("|GROUPE$|i",$fields,$matchess) ) {
				$fields = str_replace($matchess[0], "", $fields);
			} 
			
			$this->sqlGroup = "GROUP BY ".$fields. ", u.US_PRENOM, u.US_NOM";
			$this->sqlSelect = str_replace("SELECT ".$this->stat_date." as DATE, ", "SELECT min(".$this->stat_date.") as DATE, ", $this->sqlSelect);
			$this->sqlSelect .= ", ".$db->stringAggregate('distinct g.GROUPE', ' - ')." AS GROUPE";
		}
		if(!empty($this->stat_pays))
			$this->sqlGroup .= ", ".$this->stat_pays;
		if(!empty($this->stat_base))
			$this->sqlGroup .= ", ".$this->stat_base;
	}
	
	function getSql() {
		$this->sql = $this->sqlSelect." ".$this->sqlFrom." ".$this->sqlWhere." ".$this->sqlGroup." ";
		if(!empty($this->sqlOrder) && !isset($_SESSION["stat_tri"])) $this->sql.= $this->sqlOrder;
		elseif(isset($_SESSION["stat_tri"])) $this->sql.= $_SESSION["stat_tri"];
		else $this->sql .= "ORDER BY ".$this->stat_date." desc";
		
		return $this->sql;
	}
	
	function getEndSql() {
		return " ".$this->sqlEnd;
	}
	
	function dropError($errLib) {
		$this->error_msg.=$errLib."<br/>";
	}
	
	private function genereTitleFil($xAxe, $yAxe, $valueAxe) {
		$title = $this->getFieldLabel($valueAxe);
		if (!empty($xAxe)) { 
			$title .= " ".kPar." ".strtolower($this->getFieldLabel($xAxe));
			if (!empty($yAxe)) $title .= " ".strtolower(kEt);
		}
		if (!empty($yAxe)) $title .= strtolower(" ".kPar." ".$this->getFieldLabel($yAxe));
		
		$this->titre = $title;
	}
	
	private function genereTitleTop() {
		$title = "Top 25 : ".$this->getFieldLabel("NB");
		$this->titre = $title;
	}
	
	function getFieldLabel($value) {
		return (isset($this->arrLib[$value])?$this->arrLib[$value]:$value);
	}
	
	function getEmptyLegend($value) {
		return (isset($this->arrEmptyLegend[$value])?$this->arrEmptyLegend[$value]:"");
	}
	
	function getTitle() {
		return $this->titre;
	}
	
	function getGraphParam($presentation) {
		switch ($presentation) {
			case "FIL" :	return "PRECIS/DISTRIB/NB"; break;
			case "TOP" :	return "/".$this->getFirstCol()."/NB/".$this->getSecondCol(); break;
			default :		return ""; break;
		}
	}
	
	function getFirstCol() {
		$col = "";
		if (preg_match("|AS (.*),|",$this->sqlSelect,$matches)) $col = $matches[1];
		elseif (preg_match("|SELECT (.*?),|",$this->sqlSelect,$matches)) $col = $matches[1];
		elseif (preg_match("|AS (.*)$|",$this->sqlSelect,$matches)) $col = $matches[1];
		elseif (preg_match("|SELECT (.*)$|",$this->sqlSelect,$matches)) $col = $matches[1];
		if (strpos($col, ".") !== false) $col = substr($col, strpos($col, ".")+1);
		return $col;
	}
        
        
        function getSecondCol() {
            $col = "";
            //si c'est entouré par 2 virgules, alors on considère que c'est ni le 1er ni le dernier (donc le 2ème)
            if (preg_match("/,.*AS (.*),/", $this->sqlSelect, $matches))
                $col = $matches[1];
            elseif (preg_match("/,(.*),/", $this->sqlSelect, $matches))
                $col = $matches[1];

            if (strpos($col, ".") !== false)
                $col = substr($col, strpos($col, ".") + 1);
            return $col;
	}
        
	
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=array(),$search_sup=array(),$replace_sup=array()){
		global $db;
		
		$limit="";
		if ($nb!="all"){
			
			//on gère le limit différement selon postgre ou mysql (class_connection)
			//  mysql $limit=" LIMIT $i_min,".$nb." ";
			// postgre $limit=" LIMIT ".$nb." OFFSET $i_min";
			// $limit= $db->limit($i_min,$nb);
			$limit= $db->limit($nb_offset,$nb);
		}
		
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		$xml_file_path = kServerTempDir."/export_opsis_doc_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
		$results = $db->Execute($sql.$limit);
		$return = false;
		
		foreach($results as $res){
			$return = true;
			$xml .= "\t<t_stat>\n";
			foreach($res as $fld=>$val){
				 $xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
			}
			$xml .= "\t</t_stat>\n";
			fwrite($xml_file,$xml);
			$xml = "";
		}
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
		flock($xml_file, LOCK_UN);
		fclose($xml_file);
	
		if ($return) return $xml_file_path;
		else return false;
	}
	
	function initSession () {
		$mySearch->initSession();
	}
}

?>
