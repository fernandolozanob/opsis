<?php
require_once(libDir."class_cherche.php");



class RecherchePanier extends Recherche {

	function __construct() {
	  	$this->entity='PAN';
     	$this->name=kPanier;
     	$this->prefix=null;// doit être null et non une chaîne vide => si chaîne vide, alors prefix '.' ajouté ...
     	$this->sessVar='recherche_PAN';
		$this->tab_recherche=array();
		$this->useSession=true;
        // VP 18/03/2014 : optimisation recherche par group by
		//$this->sqlSuffixe=" GROUP BY t_panier.id_panier, t_panier.pan_id_gen, t_panier.pan_dossier, t_panier.pan_public, pan_date_mod, pan_id_usager_mod, pan_id_etat, pan_id_usager, pan_titre, pan_objet, pan_xml, pan_date_com, pan_date_crea, pan_id_type_commande, pan_total_ht, pan_frais_ht, pan_id_trans, pan_tva, pan_id_doc_acc, type_commande, etat_pan, t_usager.US_NOM, t_usager.US_PRENOM,t_usager.US_SOCIETE ";
		$this->sqlSuffixe=" GROUP BY t_panier.id_panier, type_commande, etat_pan, t_usager.ID_USAGER ,t_type_usager.TYPE_USAGER ";


		//param par défaut pour affichage hiérarchique
		$this->onlyDossier=false;
		$this->includePublic=0; //0:sans dossiers publics, 1:avec dossiers publics,2:que dossiers publics
		$this->excludePanier=0; //0:avec panier courant, 1:sans panier courant
		$this->excludeId=0; //Id panier à exclure
		$this->noAjax=true;
		$this->useCookies=true;
		$this->imgBase='imgfolder.gif';
		$this->imgNode;

	}


    function prepareSQL(){
		global $db;
		// By VP (17/9/08) : recup de toutes les colonnes de t_panier
		// VP (4/12/08) : ajout calcul nb lignes du panier
            $sql="SELECT  t_panier.*,t_type_commande.TYPE_COMMANDE,t_type_usager.TYPE_USAGER as pan_profil, ".$db->Concat("t_usager.US_NOM", "' '", "t_usager.US_PRENOM", "' '", "t_usager.US_SOCIETE")." as pan_usager";
            $sql.=",count(distinct t_panier_doc.ID_LIGNE_PANIER) as NB,count(distinct t_panier_doc.ID_DOC) as NBDOCUMENTS,sum(t_panier_doc.PDOC_EXTRAIT) as NBEXTRAITS";
            $sql.=", t_etat_pan.ETAT_PAN from t_panier
            		LEFT OUTER JOIN t_usager ON t_usager.ID_USAGER = t_panier.PAN_ID_USAGER
            		LEFT OUTER JOIN t_usager_groupe ON t_usager_groupe.ID_USAGER = t_panier.PAN_ID_USAGER
            		LEFT OUTER JOIN t_type_usager ON t_type_usager.ID_TYPE_USAGER = t_usager.US_ID_TYPE_USAGER
            		LEFT JOIN t_panier_doc ON t_panier.ID_PANIER=t_panier_doc.ID_PANIER";
            $sql.=" LEFT JOIN t_etat_pan ON t_panier.PAN_ID_ETAT=t_etat_pan.ID_ETAT_PAN and t_etat_pan.ID_LANG='".$_SESSION['langue']."'";
            $sql.=" LEFT JOIN t_type_commande ON t_panier.PAN_ID_TYPE_COMMANDE=t_type_commande.ID_TYPE_COMMANDE and t_type_commande.ID_LANG='".$_SESSION['langue']."'";
            $sql.=" WHERE 1=1 ";
			if ($this->onlyDossier) $sql.=" AND PAN_DOSSIER=1";
			if ($this->includePublic) $sql.=" AND PAN_PUBLIC=1";
			$this->sql = $sql;
    }

    /**
     * CONTRUCTION ARBRE HIERARCHIQUE
     * La partie ci-dessous est exclusivement consacrée à la construction d'un arbre hiérarchique (appelée par DTREE)
     * Récupère les fils d'un noeud id_panier.
     */

	// VP (16/03/09) : reprise fonction getChildren sans passer par appels successif à getPanier()

	function getChildren($id_panier,&$arrNodes) {
    	global $db;

		require_once(modelDir.'model_panier.php');
		$folders=Panier::getFolders();
		foreach ($folders as $folder) {

			if ($folder['PAN_PUBLIC']==1 && $this->includePublic==0) continue; //dossier public mais on ne veut pas
			if ($folder['PAN_PUBLIC']==0 && $this->includePublic==2) continue; //privé mais on ne veut que public
			 // VP 27/8/09 : nouveaux paramètres affichage hiérarchique des paniers (excludePanier et excludeId)
			if ($folder['PAN_ID_ETAT']=='1' && $this->excludePanier==1) continue; //panier mais on n'en veut pas
			// VP 29/01/10 : exclusion de plusieurs ID de panier
			$excludeIds=explode(",",$this->excludeId);
			//if ($folder['ID_PANIER']==$this->excludeId) continue; // si on ne veut pas d'un ID panier particulier
			if (in_array($folder['ID_PANIER'],$excludeIds)) continue; // si on ne veut pas d'un ID panier particulier


	 		if ($folder['PAN_ID_GEN']!='0') {
	 			//si on est en mode "dossiers seuls" on ne compte que les sous-dossiers, sinon on compte tout
	 			//ceci évite d'avoir un bouton + qui ne déplie rien
	 			if (($this->onlyDossier && $folder['PAN_DOSSIER']==1) || !$this->onlyDossier)
	 				$arrNodes['elt_'.$folder['PAN_ID_GEN']]['nb_children']++;

	 			//Dans tous les cas, on incrémente le nombre pour l'afficher à coté du nom entre ()
	 			$arrNodes['elt_'.$folder['PAN_ID_GEN']]['extra']++;
	 		}

			if ($this->onlyDossier && $folder['PAN_DOSSIER']==0) continue;

			$arrNodes['elt_'.$folder['ID_PANIER']]['id']=$folder['ID_PANIER'];
			$arrNodes['elt_'.$folder['ID_PANIER']]['id_pere']=$folder['PAN_ID_GEN'];
			$arrNodes['elt_'.$folder['ID_PANIER']]['order']=$folder['PAN_ORDRE'];
	    	$arrNodes['elt_'.$folder['ID_PANIER']]['terme']=$folder['PAN_ID_ETAT']==1?kPanier:str_replace (" ' ", "'",trim($folder['PAN_TITRE']));
	    	$arrNodes['elt_'.$folder['ID_PANIER']]['valide']=true;
	    	$arrNodes['elt_'.$folder['ID_PANIER']]['context']=false;
			// VP 25/8/09 : ajout test avant initialisation nb_children et extra
	    	if(!isset($arrNodes['elt_'.$folder['ID_PANIER']]['nb_children'])) $arrNodes['elt_'.$folder['ID_PANIER']]['nb_children']=0;
	    	// MS - 10/04/13 Si "extra" est déja défini, alors on ajoute NB au lieu de remplacer
			if(!isset($arrNodes['elt_'.$folder['ID_PANIER']]['extra'])) $arrNodes['elt_'.$folder['ID_PANIER']]['extra']=$folder['NB'];
			else  $arrNodes['elt_'.$folder['ID_PANIER']]['extra']+=$folder['NB'];
			$arrNodes['elt_'.$folder['ID_PANIER']]['nb_asso']=0;

			if(!empty($this->imgNode)){
				$arrNodes['elt_'.$folder['ID_PANIER']]['icone']=$this->imgNode;
			}else if ($folder['PAN_DOSSIER']==1)
				$arrNodes['elt_'.$folder['ID_PANIER']]['icone']='folder.gif';

	 		$arrNodes['elt_'.$folder['ID_PANIER']]['open']=0;

		}
                
                //B.RAVI 2015-11-30 tri des t_paniers pour l'arbre hierarchique tafelTree par pan_ordre
                function cmp_by_pan_ordre($a, $b) {
                    return $a["order"] - $b["order"];
                }

                uasort($arrNodes, "cmp_by_pan_ordre");
                
    	return $arrNodes;
    }

    function getFather($id_panier,$recursif=false,&$arrNodes) {
    	global $db;

    	$sql=" SELECT ID_PANIER,PAN_TITRE,PAN_ID_GEN
				from t_panier where id_panier
			in (select pan_id_gen from t_panier
			where id_panier=".intval($id_panier).")";
		$rows=$db->GetAll($sql);

    	foreach($rows as $row) {

		    	$arrNodes['elt_'.$row['ID_PANIER']]['id']=$row['ID_PANIER'];
		    	$arrNodes['elt_'.$row['ID_PANIER']]['id_pere']=$row['PAN_ID_GEN'];
		    	$arrNodes['elt_'.$row['ID_PANIER']]['terme']=str_replace (" ' ", "'",trim($row['PAN_TITRE']));

		    	$arrNodes['elt_'.$row['ID_PANIER']]['valide']=true;
		    	$arrNodes['elt_'.$row['ID_PANIER']]['context']=false;
				$arrNodes['elt_'.$row['ID_PANIER']]['open']=true;

    			if ($recursif && $row['PAN_ID_GEN']!=0) $this->getFather($row['ID_PANIER'],$recursif,$arrNodes);

    			if (!isset($arrNodes['elt_'.$row['ID_PANIER']]['nb_children']))  $arrNodes['elt_'.$row['ID_PANIER']]['nb_children']=1;
   				//if (!isset($arrNodes['elt_'.$row['ID_LEX']]['nb_asso']))  $arrNodes['elt_'.$row['ID_LEX']]['nb_asso']=1;
    	}
    	return $arrNodes;
    }


    // VP 7/06/2012 : ajout paramètre $withBrother pour compatibilité avec class_tafelTree
    function getNode($id_panier,&$arrNodes,$style='highlight',$withBrother=true) {
		// VP 1/07/09 : test PAN_ID_ETAT pour nommer le panier
    	global $db;
    	$this->getFather($id_panier,true,$arrNodes);
    	$sql="select * from t_panier where id_panier='".$id_panier."'";
     	$rows=$db->GetAll($sql);
     	foreach($rows as $row) {
		    	$arrNodes['elt_'.$row['ID_PANIER']]['id']=$row['ID_PANIER'];
		    	$arrNodes['elt_'.$row['ID_PANIER']]['id_pere']=$row['PAN_ID_GEN'];
		    	$arrNodes['elt_'.$row['ID_PANIER']]['terme']=$row['PAN_ID_ETAT']==1&&$row['PAN_PUBLIC']!="1"?kPanier:str_replace (" ' ", "'",trim($row['PAN_TITRE']));
				$arrNodes['elt_'.$row['ID_PANIER']]['open']=true;
		    	$arrNodes['elt_'.$row['ID_PANIER']]['valide']=true;
		    	$arrNodes['elt_'.$row['ID_PANIER']]['context']=false;
		    	$arrNodes['elt_'.$row['ID_PANIER']]['style']=$style;
     	}

     	//$this->getChildren($id_panier,$arrNodes); //ld 18 12 08 : inutile car on charge déjà tt l'arbre
    	return $arrNodes;
    }

    function getRootName() {
    	if( $this->includePublic > 0 && defined('kPublicFolders')) {
    		return kPublicFolders;

    	} elseif ( defined( 'kPrivateFolders' ) ) {
    		return kPrivateFolders;

    	} else {
    		return kFolders;

    	}

    }
	// By VP (17/9/08) : ajout de méthodes chercheDoc et chercheMat, donc recpie de ChooseType depuis class_cherche

	/**
	* En fonction des types de recherche, appelle les fonctions de traitement dédiée.
	* Cette fonction peut se trouver surchargée si une classe introduit des recherches spécifiques
	*/
    protected function ChooseType($tabCrit,$prefix) {
		// I.2.A.1 Tests des champs et appel des fonctions de recherche ad�quates :
		switch($tabCrit['TYPE'])
		{
			case "FT": //FullText
				$this->chercheTexteAmongFields($tabCrit,$prefix);
				break;
			case "FTS": //FullText avec découpage de chaque mot
				$this->FToperator='+';
				$this->chercheTexteAmongFields($tabCrit,$prefix);
				break;
			case "C":   // Recherche sur un champ précis
				$this->chercheChamp($tabCrit,$prefix);
				break;
			case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
				$this->chercheChamp($tabCrit,$prefix,true);
				break;
			case "CI":    // Recherche sur un champ d'une table annexe par ID
				$this->chercheIdChamp($tabCrit,$prefix);
				break;
			case "CT": // Recherche sur un champ par texte exact dans une table liée (fonds, genre,...)
				$this->chercheChampTable($tabCrit,$prefix);
				break;
			case "D":   // Recherche sur date(s)
				$this->chercheDate($tabCrit,$prefix);
				break;
			case "H":   // Recherche sur durées(s)
				$this->chercheDuree($tabCrit,$prefix);
				break;
			case "V":  // Recherche sur valeur
				$this->chercheVal($tabCrit,$prefix);
				break;
			case "VI":    // Recherche sur valeur par ID (récursif)
				$this->chercheIdVal($tabCrit,$prefix,true);
				break;
			case "CD" : //comparaison de date
				$this->compareDate($tabCrit,$prefix);
				break;
			case "CH" : //comparaison de date
				$this->compareDuree($tabCrit,$prefix);
				break;
            case "CDLT" : //comparaison de date
            	$tabCrit['VALEUR2']='<=';
            	$this->compareDate($tabCrit,$prefix);
            	break;
            case "CDGT" : //comparaison de date
            	$tabCrit['VALEUR2']='>=';
            	$this->compareDate($tabCrit,$prefix);
            	break;
			case "CHLT" : //comparaison de date
				$tabCrit['VALEUR2']='<=';
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CHGT" : //comparaison de date
				$tabCrit['VALEUR2']='>=';
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CV" : //comparaison de valeur =
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVGT" : //comparaison de valeur >
				$tabCrit['VALEUR2']='>';
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVLT" : //comparaison de valeur <
				$tabCrit['VALEUR2']='<';
				$this->compareValeur($tabCrit,$prefix);
				break;

			case "LI" : // Recherche sur ID LEX unique (récursif)
				$this->chercheIdLex($tabCrit,$prefix,true);
				break;
			case "L" : // Recherche sur termes lexique (av. extension lex PRE)
				$this->chercheLex($tabCrit,$prefix,true,false);
				break;
			case "LP" : // Recherche sur termes lexique (av. extension lex PRE et POST)
				$this->chercheLex($tabCrit,$prefix,true,true);
				break;

			case "M" :
				$this->chercheMat($tabCrit,$prefix);
            	break;

			case "DOC" :
				$this->chercheDoc($tabCrit,$prefix);
            	break;

			case "P" :
				$this->cherchePers($tabCrit,$prefix);
				break;
			case "PI" :
				$this->chercheIdPers($tabCrit,$prefix);
				break;
			case "U" :
				$this->chercheUsager($tabCrit,$prefix);
				break;

			case "XML" :
				$this->chercheXML($tabCrit,$prefix);
				break;

			case "ALL" :
				$this->chercheAll($tabCrit,$prefix);
				break;

default : break;
		}
    }

	/** Recherche sur les usagers
	* 	IN : tabCrit, prefix
	* 	OUT : sql
	* 	FIELD => champ de t_usager ou t_usager_groupe
	* 	VALEUR => valeur recherchée
	* 	NOTE : pas de highlight
	*/
	protected function chercheUsager($tabCrit,$prefix="") {
		global $db;

		$prefix=(!empty($prefix)?$prefix.".":"");
		$this->sqlRecherche.= " ".$tabCrit['OP']." ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true);
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kMateriel;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
	}

	/** Recherche sur les documents liés
	* 	IN : tabCrit, prefix
	* 	OUT : sql
	* 	FIELD => champ de t_doc
	* 	VALEUR => valeur recherchée
	* 	NOTE : pas de highlight
	*/
	protected function chercheDoc($tabCrit,$prefix) {
		global $db;

		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche.= " ".$tabCrit['OP']." t_panier.ID_PANIER IN
(SELECT distinct ID_PANIER from t_panier_doc,t_doc WHERE t_panier_doc.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG='".$_SESSION['langue']."' and t_doc.".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kDocument;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
	}

	/** Recherche sur les matériels liés
	* 	IN : tabCrit, prefix
	* 	OUT : sql
	* 	FIELD => champ de t_mat
	* 	VALEUR => valeur recherchée
	* 	NOTE : pas de highlight
	*/
	protected function chercheMat($tabCrit,$prefix) {
		global $db;

		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche.= " ".$tabCrit['OP']." t_panier.ID_PANIER IN
(SELECT distinct ID_PANIER from t_panier_doc,t_mat WHERE t_panier_doc.PDOC_ID_MAT=t_mat.ID_MAT and t_mat.".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kMateriel;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
	}

	   /**
    * Recherche sur une balise d'un champ XML de la table avec un ou plusieurs valeurs
		* IN : tab de Critères, prefixe table (opt)
		* OUT : sql + etape + highlight
		* NOTE : structure du tab de Critères
		* 	FIELD : un seul champ
		* 	VALEUR : un ou plusieurs valeurs
		* 	VALEUR2 : balise XML
		* NOTE2 : cette recherche accepte les critères en HAVING (=restriction APRES un group by)
		*
		*/
    protected function chercheXML($tabCrit,$prefix){ // Cherche VALS dans 1 CHAMP de la table PRINC

        global $db;
		$prefix=(!is_null($prefix)?$prefix.".":"");

		$valeur=trim($tabCrit['VALEUR']); //suppression des espaces
		$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
		$valeur=str_replace("*","%",$valeur);

		$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." ".$db->like("%<".$tabCrit['VALEUR2'].">".$valeur."</".$tabCrit['VALEUR2'].">%").")";
		//$this->sqlRecherche.= " ".$tabCrit['OP']." (".$prefix.$tabCrit['FIELD']." like ".$db->Quote("%<".$tabCrit['VALEUR2'].">".$valeur."%</".$tabCrit['VALEUR2'].">%").")";

		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
		//$highlight[$tabCrit['FIELD']]=array(str_replace(array(' +',' -','"'),' ',$tabCrit['VALEUR']));
		//By LD 04/06/08 => pas de remplacement dans les highlight
		$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);
		if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
    }

	   /** VP 4/2/09 : création fonction chercheAll()
    * Recherche sur plusieurs champs
		* IN : tab de Critères
		* OUT : sql + etape
		* NOTE : structure du tab de Critères
		* 	FIELD : plusieurs champs avec prefixe table (t_panier, t_panier_doc, t_doc et t_mat)
		* 	VALEUR : un ou plusieurs valeurs
		*
		*/
	protected function chercheAll($tabCrit,$prefix) {
		global $db;

		$arrChamps=split(",",$tabCrit['FIELD']);
		$sql="";
		$tmp_arr = explode(' ',$tabCrit['VALEUR']);
		$sValeurs="";
		foreach($tmp_arr as $word){
			if($sValeurs != ''){
			$sValeurs.= " & ";
			}
			$sValeurs.= $word;
		}
		foreach ($arrChamps as $chmp){
			switch(trim(strtok($chmp,"."))){
				case "t_doc":
					$sql.= " OR t_panier.ID_PANIER IN (SELECT distinct ID_PANIER from t_panier_doc,t_doc
							WHERE t_panier_doc.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG='".$_SESSION['langue']."' and ".$db->getFullTextSQL(array($sValeurs), array($chmp))." )";
					break;
				case "t_mat":
					$sql.= " OR t_panier.ID_PANIER IN (SELECT distinct ID_PANIER from t_panier_doc,t_mat WHERE t_panier_doc.PDOC_ID_MAT=t_mat.ID_MAT and ".$chmp." ".$this->sql_op($tabCrit['VALEUR'],true).")";
					break;
				case "t_panier":
					$sql.= " OR ".$db->getFullTextSQL(array($sValeurs), array($chmp));
					break;
				case "t_panier_doc":
					$sql.= " OR ".$db->getFullTextSQL(array($sValeurs), array($chmp));
					break;
			}
		}
		$this->sqlRecherche.=" ".$tabCrit['OP']."(".substr($sql,3).")";
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";


	}

}
?>
