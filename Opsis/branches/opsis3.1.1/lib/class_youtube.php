<?php 


// class toolbox pour communication avec l'API V3 de youtube
Class Youtube{
	var $etape_ref;
	var $type_val_pl;
	var $type_val_status_yt;
	var $client_id ;
	var $client_secret;
	var $refresh_token;
	var $connected ; 
	var $client;
	var $youtube ; 
	var $champ_retour ;
	var $error_msg;

	function __construct(){
		$this->token_url = "https://accounts.google.com/o/oauth2/token";
		$this->etape_ref = null;
		$this->error_msg="";	
		$this->connected=false;	
		$this->client = null;
		$this->youtube = null;
		$this->doc_xsl = null;
		$this->champ_retour = null;
		$this->lang = $_SESSION['langue'];
	}
	
	function setDocXsl($doc_xsl){
		if(!file_exists($doc_xsl)){
			$this->dropError("Erreur - fichier doc_xsl non trouvé");
			return false ; 
		}else{
			$this->doc_xsl = $doc_xsl;
			
			return true ; 
		}	
	}
	
	function setTypeValPl($type_val_pl){
		$this->type_val_pl = $type_val_pl;
	}
	
	function setTypeValStatus($type_val_status_yt){
		$this->type_val_status_yt = $type_val_status_yt;
	}
	
	
	// récupération des paramètres de login depuis une etape
	function getLogParamsFromEtape($etape_ref=null){
		global $db ;
		if(isset($etape_ref) && !empty($etape_ref) ){
			$id_etp = intval($etape_ref);
		}elseif(isset($this->etape_ref) && !empty($this->etape_ref)){
			$id_etp = $this->etape_ref;
		}
		
		$etape_arr = $db->GetRow("SELECT * from t_etape where id_etape=".intval($id_etp));
		
		if(!isset($etape_arr) || empty($etape_arr) ||   !isset($etape_arr['ETAPE_PARAM'])){
			$this->dropError("Erreur recupération identifiants depuis etape");
			return false ; 
		}
		$tab_xml=xml2tab($etape_arr['ETAPE_PARAM']);
		$params_job = $tab_xml['PARAM'][0];
		$this->client_id      = $params_job['ID_CLIENT'];
		$this->client_secret  = $params_job['API_SECRET'];
		$this->refresh_token  = $params_job['TOKEN'];
		$this->champ_retour  = $params_job['CHAMP_RETOUR'];
		
		if(empty($this->doc_xsl)){
			$this->setDocXsl(getSiteFile("designDir","print/".$params_job['DOC_XSL']));
		}
		
		
		if(!empty($this->client_id) && !empty($this->client_secret) && !empty($this->refresh_token)){
			return true ;		
		}else{
			$this->dropError("Error - La recuperation des params de connexion a echouee");
			return false ; 
		}
	}
	
	// connexion youtube obligatoire avant n'importe quelle autre action
	// necessite que les informations client_id & client_secret soit définis (voir getLogParamsFromEtape, ou implémenter une autre méthode)
	function connectYt(){
		require_once libDir.'google-api-php-client/src/Google_Client.php';
		require_once libDir.'google-api-php-client/src/contrib/Google_YouTubeService.php';
		
		if(!isset($this->client_id) || empty($this->client_id) || !isset($this->client_secret) || empty($this->client_secret)){
			$this->dropError("Erreur connexion youtube - paramètres manquants");
			
			return false ; 
		}
		
		$client = new Google_Client();
		$client->setClientId($this->client_id);
		$client->setClientSecret($this->client_secret);
		$client->setScopes("https://www.googleapis.com/auth/youtube");
		$youtube = new Google_YoutubeService($client);
		
		/* get token */
		try { 
			$post_data = array(
								'client_secret' =>   $this->client_secret,
								'grant_type'    =>   'refresh_token',
								'refresh_token' =>   $this->refresh_token,
								'client_id'     =>   $this->client_id
								);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->token_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			$token_object = json_decode($result);
			$access_token = $token_object->access_token;

			$result = array("access_token" => $access_token,
							"token_type" => "Bearer",
							"expires_in" => 3600,
							"refresh_token" => $refresh_token,
							"created" => time());
			$result = json_encode($result);
			$client->setAccessToken($result);
			if($client->getAccessToken()){
				$this->connected = true ;
				$this->client = $client;
				$this->youtube = $youtube;
			}else{
				$this->connected = false ; 
			}
			return true ; 
		}
		catch (Exception $e)
		{
			$this->dropError('Authentification error : '.$e->getMessage());
			// throw new Exception('Authentification error : '.$e->getMessage());
			return false ; 
		}
	}
	
	function import_playlist_yt_to_val($type_v_pl_yt=null){
		global $db;
	
		if(!empty($type_v_pl_yt)){
			$this->type_val_pl = $type_v_pl_yt;
		}
		
		if($this->connected && !empty($this->youtube) && !empty($this->type_val_pl)){
			// ici on récupère les playlists (créées par l'utilisateur, pas les playlists internes de YT)
			// et on alimente un ensemble de valeurs 
			// 24.03 - on ne peut récupérer que 50 playlists max en un appel
			$PLResponse = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50));
			
			// Activation 
			$PLResponse_page = $PLResponse;
			// Si la réponse de la première requete contenait un "nextPageToken", on a + de 50 résultats, on va donc appeler les résultats suivants en relancant l'appel avec pageToken=nextPageToken
			while(isset($PLResponse_page['nextPageToken'])){
				$PLResponse_page = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50,'pageToken'=>$PLResponse_page['nextPageToken']));
				$PLResponse['items'] = array_merge($PLResponse['items'],$PLResponse_page['items']);
			}
			
			if(empty($PLResponse) || !$PLResponse){
				$this->dropError("Erreur récupération des playlists");
				return false ; 
			}
			
			$existing_vals = $db->GetArray("SELECT * from t_val where VAL_ID_TYPE_VAL=".$db->Quote($this->type_val_pl)." AND ID_LANG=".$db->Quote($this->lang));
			
			// comparaison playlists récupérées depuis YT avec l'existant, gère l'update & le delete 
			foreach($existing_vals as $idx=>$e_val){
				foreach($PLResponse['items'] as $idx_pl=>$pl){
					if($e_val['VAL_CODE']==$pl['id']){
						// les playlists trouvées de cette manière sont flag 
						$PLResponse['items'][$idx_pl]['found']=1;
						$existing_vals[$idx]['found'] = 1;
						// ID trouvé dans les valeurs existantes
						// vérification de modifications du titre de la playlist
						if($e_val['VALEUR'] != $pl['snippet']['title']){
							$existing_vals[$idx]['VALEUR'] = $pl['snippet']['title'];
							$existing_vals[$idx]['action'] = "update" ; 
						}
						break ; 
					}
				}
				if(!isset($existing_vals[$idx]['found'])){
					// une valeur Opsis n'a plus de playlist associée sur YT 
					// => preparation de la suppression de la valeur
					$existing_vals[$idx]['action'] = "delete";
				}
			}

			// les éléments récupérés depuis YT qui ne sont pas flag "found" doivent être créées dans les valeurs OPSIS
			foreach($PLResponse['items'] as $idx=>$pl){
				if(isset($pl['found']) && $pl['found']){
					continue ; 
				}else{
					$existing_vals[] = array('action'=>'create','VALEUR'=>$pl['snippet']['title'],'VAL_CODE'=>$pl['id'],'VAL_ID_TYPE_VAL'=>$this->type_val_pl);
				}
			}
			$cnt_crea = 0 ;
			$cnt_del= 0 ;
			
			require_once(modelDir."model_val.php");
			// Lancement de l'ensemble des modifications : 
			foreach ($existing_vals as $e_val){
				if(!isset($e_val['action'])){
					continue ; 
				}
				$tmp_val = new Valeur() ; 
				switch ($e_val['action']){
					case "update" :
						$tmp_val->t_val['ID_VAL'] = $e_val['ID_VAL'];
						$tmp_val->getVal();
					case "create" :
						$tmp_val->t_val['VALEUR'] = $e_val['VALEUR'];
						$tmp_val->t_val['VAL_CODE'] = $e_val['VAL_CODE'];
						$tmp_val->t_val['VAL_ID_TYPE_VAL'] = $e_val['VAL_ID_TYPE_VAL'];
						$tmp_val->t_val['ID_LANG'] = $_SESSION['langue'];
						$cnt_crea++;
						$tmp_val->save(); 
					break;
					case "delete" : 
						$tmp_val->t_val['ID_VAL'] = $e_val['ID_VAL'];
						$tmp_val->delete();
						$cnt_del++;
					break ; 
				
				}
			}
			trace("import_playlist_yt_to_val etape ".$this->etape_ref." val playlist créées / updatées : ".$cnt_crea." val playlist supprimées : ".$cnt_del);
			return true;
		}else{
			$this->dropError("Erreur - non connecté ou params manquants");
			return false ; 
		}
	}
	
	function export_val_to_playlist_yt($id_val = null,$type_v_pl_yt=null){
		global $db;
		
		if(!empty($type_v_pl_yt)){
			$this->type_val_pl = $type_v_pl_yt;
		}
		$update_target_val = false ; 
		if($this->connected && !empty($this->youtube) && !empty($this->type_val_pl)){
		
			if(isset($id_val) && !empty($id_val)){
				$update_target_val = true ; 
				$update_one = true; 
				$existing_vals = $db->GetArray("SELECT * from t_val where ID_VAL=".intval($id_val)." AND VAL_ID_TYPE_VAL=".$db->Quote($this->type_val_pl)." AND ID_LANG=".$db->Quote($this->lang));
				if($existing_vals[0]['VAL_CODE']){
					$id_pl = $existing_vals[0]['VAL_CODE'];
					$PLResponse = $this->youtube->playlists->listPlaylists('id,snippet,status',array('id'=>$id_pl));
				}else{
					// $PLResponse = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true'));
					// 24.03 - on ne peut récupérer que 50 playlists max en un appel
					$PLResponse = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50));
					
					// Activation 
					$PLResponse_page = $PLResponse;
					// Si la réponse de la première requete contenait un "nextPageToken", on a + de 50 résultats, on va donc appeler les résultats suivants en relancant l'appel avec pageToken=nextPageToken
					while(isset($PLResponse_page['nextPageToken'])){
						$PLResponse_page = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50,'pageToken'=>$PLResponse_page['nextPageToken']));
						$PLResponse['items'] = array_merge($PLResponse['items'],$PLResponse_page['items']);
					}
				}
				if(empty($PLResponse) || !$PLResponse){
					$this->dropError("Erreur récupération des playlists");
					return false ;
				}
			}else{
					//$PLResponse = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true'));
					// 24.03 - on ne peut récupérer que 50 playlists max en un appel
					$PLResponse = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50));
					// Activation 
					$PLResponse_page = $PLResponse;
					// Si la réponse de la première requete contenait un "nextPageToken", on a + de 50 résultats, on va donc appeler les résultats suivants en relancant l'appel avec pageToken=nextPageToken
					while(isset($PLResponse_page['nextPageToken'])){
						$PLResponse_page = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50,'pageToken'=>$PLResponse_page['nextPageToken']));
						$PLResponse['items'] = array_merge($PLResponse['items'],$PLResponse_page['items']);
					}
					if(empty($PLResponse) || !$PLResponse){
					$this->dropError("Erreur récupération des playlists");
					return false ;
				}
				$existing_vals = $db->GetArray("SELECT * from t_val where VAL_ID_TYPE_VAL=".$db->Quote($this->type_val_pl)." AND ID_LANG=".$db->Quote($this->lang));
			}
			// comparaison playlists récupérées depuis YT avec l'existant, gère l'update & le delete 
			foreach($existing_vals as $idx=>$e_val){
				foreach($PLResponse['items'] as $idx_pl=>$pl){
					if($e_val['VAL_CODE']==$pl['id']){
						// les playlists trouvées de cette manière sont flag 
						$PLResponse['items'][$idx_pl]['found']=1;
						$existing_vals[$idx]['found'] = 1;
						// ID trouvé dans les valeurs existantes
						// vérification de modifications du titre de la playlist
						if($e_val['VALEUR'] != $pl['snippet']['title']){
							$existing_vals[$idx]['action'] = "update" ; 
							$existing_vals[$idx]['snippet'] = $pl['snippet'];
						}
						break ; 
					}
				}
				if(!isset($existing_vals[$idx]['found'])){
					// une valeur Opsis n'a plus de playlist associée sur YT 
					// => preparation de la suppression de la valeur
					$existing_vals[$idx]['action'] = "create";
				}
			}
			
			// les éléments récupérés depuis YT qui ne sont pas flag "found" (trouvé dans les valeurs OPSIS) doivent être supprimés de youtube
			foreach($PLResponse['items'] as $idx=>$pl){
				if(isset($pl['found']) && $pl['found']){
					continue ; 
				}else{
					$existing_vals[] = array('action'=>'delete','VALEUR'=>$pl['snippet']['title'],'VAL_CODE'=>$pl['id']);
				}
			}
			
			require_once(modelDir."model_val.php");
			// Lancement de l'ensemble des modifications : 
			foreach ($existing_vals as $e_val){
				if(!isset($e_val['action']) || ($update_target_val && $e_val['ID_VAL'] != $id_val)){
					continue ; 
				}
				$tmp_val = new Valeur() ; 
				switch ($e_val['action']){
					case "update" :
						try {
							$updateSnippet = $e_val['snippet'];
							
							$playlistSnippet = new Google_PlaylistSnippet();
							$youTubePlaylist = new Google_Playlist();
							
							$playlistSnippet->setTitle($e_val['VALEUR']);
							$youTubePlaylist->setId($e_val['VAL_CODE']);
							$youTubePlaylist->setSnippet($playlistSnippet);
							
							$this->youtube->playlists->update('id,snippet',$youTubePlaylist);
						}catch(Exception $e){
							$this->dropError("Erreur - playlist refresh - fail on update val_code:".$e_val['VAL_CODE']."\n".$e);
							trace("processYoutube - playlist refresh - fail on update val_code:".$e_val['VAL_CODE']."\n".$e);
						}
					break;
					case "create" :
						try{
							$playlistSnippet = new Google_PlaylistSnippet();
							$playlistSnippet->setTitle($e_val['VALEUR']);
							// Pour l'instant on ne peut pas set la description ou le statut depuis la valeur , pr plus tard:
							// $playlistSnippet->setDescription('');
							$playlistStatus = new Google_PlaylistStatus();
							$playlistStatus->setPrivacyStatus('public');
							
							
							$youTubePlaylist = new Google_Playlist();
							$youTubePlaylist->setSnippet($playlistSnippet);
							$youTubePlaylist->setStatus($playlistStatus);
							
							$playlistResponse = $this->youtube->playlists->insert('snippet,status',$youTubePlaylist, array());
							$playlistId = $playlistResponse['id'];
							
							$tmp_val->t_val['ID_VAL'] = $e_val['ID_VAL'];
							$tmp_val->getVal();
							$tmp_val->t_val['VAL_CODE'] = $playlistId;
							$tmp_val->save();
							
						}catch(Exception $e){
							$this->dropError("Erreur -  playlist refresh - fail on create valeur:".$e_val['VALEUR']."\n".$e);
							trace("processYoutube - playlist refresh -fail on create valeur:".$e_val['VALEUR']."\n".$e);
						}
					break;
					case "delete" : 
						try{
							$this->youtube->playlists->delete($e_val['VAL_CODE']);
						}catch(Exception $e){
							$this->dropError("Erreur - playlist refresh - fail on delete val_code:".$e_val['VAL_CODE']."\n".$e);
							trace("processYoutube - playlist refresh - fail on delete val_code:".$e_val['VAL_CODE']."\n".$e);
						}
					break ; 
				}
			}
			return true ; 
		}else{
			$this->dropError("Erreur - non connecté ou params manquants");
			return false ; 
		}
	}
	
	function get_status_video_xml($id_doc=null,$id_yt_ex=null){
		global $db ; 
		
		if((!isset($id_doc) || empty($id_doc)) && (!isset($id_yt_ex) || empty($id_yt_ex))){
			$this->dropError("Erreur get_status_video_yt, pas d'id_doc défini ");
			return false ; 
		}
		
		if($this->connected && !empty($this->youtube) && !empty($id_doc)){
		
			if(isset($id_yt_ex) && !empty($id_yt_ex)){
				$id_yt = $id_yt_ex;
			}else{
				require_once(modelDir.'model_doc.php');
				$myDoc = new Doc();
				$myDoc->t_doc['ID_DOC'] = $id_doc;
				$myDoc->getDoc();
				$myDoc->getDocPublication();
				
				if(empty($myDoc->t_doc_publication)){
					$this->dropError("Erreur, pas de publication détectée pour la vidéo n:".$id_doc);
					return false ;
				}
				foreach($myDoc->t_doc_publication as $dpub){
					if($dpub['PUB_CIBLE'] == 'youtube'){
						$id_yt = $dpub['PUB_VALUE'];
						break ; 
					}
				}
			}
			if(!isset($id_yt) || empty($id_yt)){
				$this->dropError("Erreur get_status_video_yt, pas d'id_doc défini ");
				return false ; 
			}
			
			try
			{
				$listResponse = $this->youtube->videos->listVideos("status,processingDetails", array('id' => $id_yt));
				if(!empty($listResponse) && $listResponse){
					$xml = tab2xml($listResponse);
					return $xml ; 
				}
				return false ;
			}catch(Exception $e ){
				$this->dropError("Erreur get_status_video_xml :".$e);
				return false ; 
			}
		}else{
			return false ; 
		}
	}
	
	function publish_video_yt_job($path_file_to_upload=null,$params_job=null){
		global $db;
		
		if(empty($params_job)){
			throw new Exception('paramètres jobs vides');
		}
		if(empty($path_file_to_upload) ||!file_exists($path_file_to_upload)){
			throw new Exception('Fichier non trouvé');
		}
				
		if($this->connected && !empty($this->youtube) && !empty($params_job) && !empty($path_file_to_upload)){
			try
			{

				// Get the Mimetype of your video
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$mime_type = finfo_file($finfo, $path_file_to_upload);
		 
				// Build the Needed Video Information
				$snippet = new Google_VideoSnippet();
				$snippet->setTitle($params_job['donnees'][0]['title']);
				$snippet->setDescription(str_replace(array('\n','<','>'),array("\n",'',''),$params_job['donnees'][0]['description']));
				
                $tags=array();
                if(isset($params_job['donnees'][0]['tags']) && !empty($params_job['donnees'][0]['tags']))
                {
                    if(!is_array($params_job['donnees'][0]['tags'])) {
                        $tags=explode(',',$params_job['donnees'][0]['tags']);
                    } else {
                        $tags = $params_job['donnees'][0]['tags'];
                    }
                }
				$snippet->setTags($tags);
				if (!empty($params_job['donnees'][0]['channel_id']))
					$snippet->setCategoryId($params_job['donnees'][0]['channel_id']);
				else $snippet->setCategoryId(1);
		 
				// Build the Needed video Status
				$status = new Google_VideoStatus();
				if (!empty($params_job['donnees'][0]['private']) && ($params_job['donnees'][0]['private']=='1' || $params_job['donnees'][0]['private']=='true')){
					$status->setPrivacyStatus('private');
				}else if(isset($params_job['donnees'][0]['private']) && !empty($params_job['donnees'][0]['private']) && $params_job['donnees'][0]['private'] == 'unlisted'){
					$status->setPrivacyStatus('unlisted');
				}else $status->setPrivacyStatus('public'); 
		 
				// Set the Video Info and Status in the Main Tag
				$video = new Google_Video();
				$video->setSnippet($snippet);
				$video->setStatus($status);
	 
				// Send the video to the Google Youtube API
				//$created_file = $this->youtube->videos->insert('snippet,status', $video, array('data' => file_get_contents($path_file_to_upload), 'mimeType' => $mime_type,));
				$chunkSizeBytes = 1 * 1024 * 1024;
				$media = new Google_MediaFileUpload('video/*', null, true, $chunkSizeBytes);
				$media->setFileSize(filesize($path_file_to_upload));
				$insertResponse = $this->youtube->videos->insert("status,snippet", $video, array('mediaUpload' => $media));
				$uploadStatus = false;
				$handle = fopen($path_file_to_upload, "rb");
				while (!$uploadStatus && !feof($handle)) {
					$chunk = fread($handle, $chunkSizeBytes);
					$uploadStatus = $media->nextChunk($insertResponse, $chunk);
				}
				fclose($handle);

				if (isset($uploadStatus['id'])) {
					$video_id = $uploadStatus['id'];
					
					//Vignette
					$thumb = new Google_MediaFileUpload('image/*', null, true, $chunkSizeBytes);
					$thumb->setFileSize(filesize($imagePath));
					$setResponse = $this->youtube->thumbnails->set($video_id, array('mediaUpload' => $thumb));
					$uploadStatus = false;
					$handle = fopen($imagePath, "rb");
					

					while (!$uploadStatus && !feof($handle)) {
						$chunk = fread($handle, $chunkSizeBytes);
						$uploadStatus = $media->nextChunk($setResponse, $chunk);
					}
					fclose($handle);
					
					// playlists
					if(isset($params_job['donnees'][0]['playlists_ids']) && !empty($params_job['donnees'][0]['playlists_ids']) ){
						if(strpos($params_job['donnees'][0]['playlists_ids'],',')!==false){
							$arr_pls = explode(',',$params_job['donnees'][0]['playlists_ids']);
						}else{
							$arr_pls = array($params_job['donnees'][0]['playlists_ids']);
						}
						foreach($arr_pls as $pl){
							try{
								$new_link = new Google_PlaylistItem();
								$new_link_snippet = new Google_PlaylistItemSnippet();
								$new_ressource = new Google_ResourceId();
								$new_ressource->setVideoId($video_id);
								$new_ressource->setKind('youtube#video');
								$new_link_snippet->setResourceId($new_ressource);
								$new_link_snippet->setPlaylistId($pl);
								$new_link->setSnippet($new_link_snippet);
								$this->youtube->playlistItems->insert('snippet',$new_link,array());
								JobProcess::writeJobLog('publish_video_yt_job link video '.$video_id."-> pl ".$pl);
								trace('publish_video_yt_job link video '.$video_id."-> pl ".$pl);
							}catch(Exception $e){
								JobProcess::writeJobLog('publish_video_yt_job, crash creation lien video '.$video_id."-> pl ".$pl);
								trace('publish_video_yt_job, crash creation lien video '.$video_id."-> pl ".$pl);
							}
						}
					}
			
					
					//Vidéo star
					if (!empty($params_job['video_star']) && (empty($params_job['donnees'][0]['private']) || ($params_job['donnees'][0]['private']!='1' && $params_job['donnees'][0]['private']!='true'))) {
						$channelsResponse = $this->youtube->channels->listChannels('id,brandingSettings', array('mine' => 'true'));
						if (isset($channelsResponse["items"][0])) {
							$chan = $channelsResponse["items"][0];
							$channelId = $chan["id"];
							
							$bran = $chan["brandingSettings"];
							
							$setG = new Google_ChannelSettings();
							$setG->setUnsubscribedTrailer($video_id);
							$setG->setTitle($bran["channel"]["title"]);
							$setG->setProfileColor($bran["channel"]["profileColor"]);
							
							$brandG = new Google_ChannelBrandingSettings();
							$brandG->setChannel($setG);
							
							$chanG = new Google_Channel();
							$chanG->setId($channelId);
							$chanG->setBrandingSettings($brandG);
							
							$res = $this->youtube->channels->update('id,brandingSettings', $chanG, array());
						}
					}

					return $video_id;
				}
				else{
					throw new Exception('video rejected');
				}
			}
			catch (Exception $ex)
			{
					echo ($ex);
					throw $ex;
			}
		}else{	
		throw new Exception('Erreur de connexion à YT / vérifiez les paramètres');
	}
}

	
	function update_video_yt($id_doc=null,$sync_infos_flag=1,$sync_pl_flag = 0){
		global $db;
		
		if(!isset($id_doc) || empty($id_doc)){
			$this->dropError("Erreur update_video_yt, pas d'id_doc défini ");
			return false ; 
		}
		
		if($this->connected && !empty($this->youtube) && !empty($id_doc)){
			require_once(modelDir.'model_doc.php');
			$myDoc = new Doc();
			$myDoc->t_doc['ID_DOC'] = $id_doc;
			$myDoc->t_doc['ID_LANG'] = $this->lang;
			$myDoc->getDoc();
			$myDoc->getValeurs();
			$myDoc->getLexique();
			$myDoc->getDocPublication();
			
			if(empty($myDoc->t_doc_publication)){
				$this->dropError("Erreur, pas de publication détectée pour la vidéo n:".$id_doc);
				return false ;
			}
			
			foreach($myDoc->t_doc_publication as $dpub){
				if($dpub['PUB_CIBLE'] == 'youtube'){
					$id_yt = $dpub['PUB_VALUE'];
					break ; 
				}
			}
			if( !isset($id_yt) || empty($id_yt)){
				$this->dropError("Erreur récupération de l'id youtube de la video");
				return false ; 
			}
			
			if(isset($sync_infos_flag) && $sync_infos_flag==1 && !empty($this->doc_xsl)){
				try
				{
					$listResponse = $this->youtube->videos->listVideos("snippet,status", array('id' => $id_yt));
					$videoList = $listResponse['items'];
					if (!empty($videoList)) {
						$video = $videoList[0];
						$videoSnippet = $video['snippet'];
						$videoStatus = $video['status'];
						$updateSnippet = new Google_VideoSnippet($videoSnippet);
						
						/*------------- update données depuis Opsis ---------*/
						$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$myDoc->xml_export()."</EXPORT_OPSIS>";
						
						$xml_params = TraitementXSLT($xml,$this->doc_xsl,null,0,"",null,false);
						$xml_tab = xml2tab($xml_params);
					
				
					
						$yt_donnees = $xml_tab['DONNEES'][0];
						$update = array() ; 
						
						
						if(isset($yt_donnees['TITLE']) && (trim($yt_donnees['TITLE'])!=trim($updateSnippet->getTitle()))){
							if(!in_array('snippet',$update))$update[] = 'snippet' ; 
							$yt_title = $yt_donnees['TITLE'];
							$updateSnippet->setTitle($yt_title);
						}
					
						if(isset($yt_donnees['DESCRIPTION']) && (trim($yt_donnees['DESCRIPTION'])!=trim($updateSnippet->getDescription()))){
							if(!in_array('snippet',$update))$update[] = 'snippet' ; 
							$yt_description = $yt_donnees['DESCRIPTION'];
							$updateSnippet->setDescription($yt_description);
						}
						if(isset($yt_donnees['TAGS']) && ($yt_donnees['TAGS']!=$updateSnippet->getTags())){
							if(!in_array('snippet',$update))$update[] = 'snippet' ; 
							$yt_tags= explode(',',$yt_donnees['TAGS']);
							$updateSnippet->setTags($yt_tags);
						}
						if(isset($yt_donnees['CHANNEL_ID']) && !empty($yt_donnees['CHANNEL_ID']) && ($yt_donnees['CHANNEL_ID']!=$updateSnippet->getCategoryId())){
							if(!in_array('snippet',$update))$update[] = 'snippet' ; 
							$yt_categ_id= $yt_donnees['CHANNEL_ID'];
							$updateSnippet->setCategoryId($yt_categ_id);
						}
												
						if(isset($yt_donnees['PRIVATE']) && !empty($yt_donnees['PRIVATE']) && ($yt_donnees['PRIVATE'] == '1' || $yt_donnees['PRIVATE'] =='true') ){
							$status_flag = 'private';
						}else if(isset($yt_donnees['PRIVATE']) && !empty($yt_donnees['PRIVATE']) && $yt_donnees['PRIVATE'] == 'unlisted'){
							$status_flag = 'unlisted';
						}else{
							$status_flag = 'public'; 
						}
						
						$status = new Google_VideoStatus();
						if($status_flag != $videoStatus['privacyStatus']){
							if(!in_array('status',$update))$update[] = 'status' ; 
							$status->setPrivacyStatus($status_flag); 
						}
						
						/* --- */
						if(!empty($update)){
							$updateVideo = new Google_Video($video);
							if(in_array('snippet',$update)){
								$updateVideo->setSnippet($updateSnippet);	
							}
							if(in_array('status',$update)){
								$updateVideo->setStatus($status);
							}							
							$updateResponse = $this->youtube->videos->update("status,snippet", $updateVideo);
							 echo kSuccesMiseAJourYT;
						}
					}
					
				}
				catch (Exception $e)
				{
					echo 'Exception : '.$e->getMessage();
					return false ;
				}
			}
			
			if(isset($sync_pl_flag ) && $sync_pl_flag  == 1 ){
			
				if(!isset($this->type_val_pl) || empty($this->type_val_pl)){
					$this->dropError("Erreur type_val des playlists youtube n'est pas défini");
					return false; 
				}
			
				$arr_valeurs_pl = array();
				$myDoc->getValeurs();
				if(!empty($myDoc->t_doc_val)){
					foreach($myDoc->t_doc_val as $dval){
						if($dval['VAL_ID_TYPE_VAL'] == $this->type_val_pl){
							$arr_valeurs_pl[] = $dval;
						}
					}
				}
				
				// $myPlaylists = $this->youtube->playlists->listPlaylists('id,snippet',array('mine'=>'true'));
				// 24.03 - on ne peut récupérer que 50 playlists max en un appel
				$myPlaylists = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50));
				// Activation 
				$PLResponse_page = $myPlaylists;
				// Si la réponse de la première requete contenait un "nextPageToken", on a + de 50 résultats, on va donc appeler les résultats suivants en relancant l'appel avec pageToken=nextPageToken
				while(isset($PLResponse_page['nextPageToken'])){
					$PLResponse_page = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50,'pageToken'=>$PLResponse_page['nextPageToken']));
					$myPlaylists['items'] = array_merge($myPlaylists['items'],$PLResponse_page['items']);
				}
				$arr_ids=array();
				foreach($myPlaylists['items'] as $pl){
					$arr_ids[] = $pl['id'];
				}
				$current_pls = array();
				foreach($arr_ids as $id_pl_yt){
					if(isset($tmp_listitems)){
						unset($tmp_listitems);
					}
					//$tmp_listitems = $this->youtube->playlistItems->listPlaylistItems('id,snippet,status',array("playlistId"=>$id_pl_yt,"videoId"=>$id_yt));
					$tmp_listitems = $this->youtube->playlistItems->listPlaylistItems('id,snippet,status',array("playlistId"=>$id_pl_yt));
					
					if(!empty($tmp_listitems['items'])) {
						$LIResponse_page = $tmp_listitems;
						while(isset($LIResponse_page['nextPageToken'])){
							$LIResponse_page = $this->youtube->playlistItems->listPlaylistItems('id,snippet,status',array("playlistId"=>$id_pl_yt,'pageToken'=>$LIResponse_page['nextPageToken']));
							$tmp_listitems['items'] = array_merge($tmp_listitems['items'],$LIResponse_page['items']);
						}

					
						foreach($tmp_listitems['items'] as $item){
							if(isset( $item['snippet']['resourceId']['videoId'] ) && !empty($item['snippet']['resourceId']['videoId']) && $item['snippet']['resourceId']['videoId'] == $id_yt){
								$current_pls[] = array('id'=>$item['id'],"playlistId"=>$item['snippet']['playlistId']);
							}
						}
					}
				}
				// comparaison array
				foreach($arr_valeurs_pl as $i=>$v_pl){
					foreach($current_pls as $j=>$pl){
						// identification cas correct "Le lien entre la vidéo et la playlist est représenté dans les valeurs OPSIS et existe sur youtube" 
						if($v_pl['VAL_CODE'] == $pl['playlistId']){
							$current_pls[$j]['found'] = 1;
							$arr_valeurs_pl[$i]['found']=1;
						}
					}
					// identification des liens video<=>playlist qui n'existent que pour 
					if(!isset($arr_valeurs_pl[$i]['found']) || !$arr_valeurs_pl[$i]['found']){
						$arr_valeurs_pl[$i]['action'] = 'create_link';
					}
				}
				foreach($current_pls as $j=>$pl){
					if(!isset($pl['found']) || !$pl['found']){
						$arr_valeurs_pl[] = array_merge($current_pls[$j],array('action'=>'delete_link'));
					}
				}
				
				foreach($arr_valeurs_pl as $val_pl){
					if(!isset($val_pl['action'])){
						continue ; 
					}
					switch ($val_pl['action']){
						case "create_link":
							$new_link = new Google_PlaylistItem();
							$new_link_snippet = new Google_PlaylistItemSnippet();
							$new_ressource = new Google_ResourceId();
							$new_ressource->setVideoId($id_yt);
							$new_ressource->setKind('youtube#video');
							$new_link_snippet->setResourceId($new_ressource);
							if(!isset($val_pl['VAL_CODE']) || empty($val_pl['VAL_CODE'])){
								$ok = $this->export_val_to_playlist_yt($val_pl['ID_VAL']);
								if($ok == true ){
									$tmpVal = new Valeur();
									$tmpVal->t_val['ID_VAL'] = $val_pl['ID_VAL'];
									$tmpVal->getVal();
									$val_pl['VAL_CODE'] = $tmpVal->t_val['VAL_CODE'];
									unset($tmpVal);
								}
								
							}
							$new_link_snippet->setPlaylistId($val_pl['VAL_CODE']);
							$new_link->setSnippet($new_link_snippet);
							$this->youtube->playlistItems->insert('snippet',$new_link,array());
							break ; 
						case "delete_link" :
							$this->youtube->playlistItems->delete($val_pl['id']);
							break ; 
					}
				}
			}
			return true;
		}else{
			$this->dropError("Erreur - non connecté ou params manquants");
			return false ; 
		}
	}
	
	
	function delete_from_yt($id_doc,$id_etape=null){
		global $db;
		if(!isset($id_doc) || empty($id_doc)){
			$this->dropError("Erreur delete_from_yt, pas d'id_doc défini ");
			return false ; 
		}
		if($this->connected && !empty($this->youtube) && !empty($id_doc)){
			require_once(modelDir.'model_doc.php');
			$myDoc = new Doc() ; 
			$myDoc->t_doc['ID_DOC'] = $id_doc; 
			$myDoc->getDoc();
			$myDoc->getDocPublication();
			if(!empty($myDoc->t_doc_publication)){
				foreach($myDoc->t_doc_publication as $pub){
					if(!empty($id_etape)){
						if($pub['PUB_CIBLE'] == 'youtube' && $pub['PUB_ID_ETAPE'] == $id_etape){
						$id_video = $pub['PUB_VALUE'];
						break ; 
						}
					}	
					else
					{
					if($pub['PUB_CIBLE'] == 'youtube'){
						$id_video = $pub['PUB_VALUE'];
						break ; 
						}
					}
				}
			}
			if(!isset($id_video) || empty($id_video)){
				$this->dropError("Erreur delete_from_yt, id video youtube non trouvé");
				return false ; 
			}
			
			try {
				$ok = $this->youtube->videos->delete($id_video);
			}  catch (Google_Service_Exception $e) {
				if($e->getCode() != '404') {
					throw $e;
				}
			} catch (Google_Exception $e) {
				if($e->getCode() != '404') {
					throw $e;
				}
			}	
			trace("retour delete video ".$id_video.": ".print_r($ok,true));
			return true ; 
		}else{
			$this->dropError("Erreur - non connecté ou params manquants");
			return false ; 
		}
	}
	
	
	
	
	
	
	function import_links_playlist_video($field_id_yt =''){
		global $db;
		if(empty($this->champ_retour) && isset($field_id_yt) && !empty($field_id_yt)){
			$this->champ_retour = $field_id_yt;
		}
		
		if($this->connected && !empty($this->youtube) &&( (isset($this->champ_retour) && !empty($this->champ_retour)))&& !empty($this->type_val_pl)){
			// $myPlaylists =$this->youtube->playlists->listPlaylists('id,snippet',array('mine'=>'true'));
			// 24.03 - on ne peut récupérer que 50 playlists max en un appel
			$myPlaylists = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50));
			// Activation 
			$PLResponse_page = $myPlaylists;
			// Si la réponse de la première requete contenait un "nextPageToken", on a + de 50 résultats, on va donc appeler les résultats suivants en relancant l'appel avec pageToken=nextPageToken
			while(isset($PLResponse_page['nextPageToken'])){
				$PLResponse_page = $this->youtube->playlists->listPlaylists('id,snippet,status',array('mine'=>'true','maxResults'=>50,'pageToken'=>$PLResponse_page['nextPageToken']));
				$myPlaylists['items'] = array_merge($myPlaylists['items'],$PLResponse_page['items']);
			}
			
			$arr_ids=array();
			foreach($myPlaylists['items'] as $pl){
				$arr_ids[] = $pl['id'];
			}
			$cnt = 0;
			foreach($arr_ids as $id_pl_yt){
				if(isset($tmp_listitems)){
					unset($tmp_listitems);
				}
				// recup des éléments attachés aux playlist
				$tmp_listitems =$this->youtube->playlistItems->listPlaylistItems('id,snippet,status',array('maxResults'=>50,"playlistId"=>$id_pl_yt));
				$cnt_it = 0;
				// Si la réponse de la première requete contenait un "nextPageToken", on a + de 50 résultats, on va donc appeler les résultats suivants en relancant l'appel avec pageToken=nextPageToken
				// on garde tout de même une limite de 10 appel successifs pour éviter de partir en loop si des problèmes surviennent ... 
				// On ne peut donc récupérer "que" 500 playlistItems.
				$tmp_listitems_page = $tmp_listitems;
				while(isset($tmp_listitems_page['nextPageToken']) && $cnt_it<10){
					$tmp_listitems_page = $this->youtube->playlistItems->listPlaylistItems('id,snippet,status',array('maxResults'=>50,'pageToken'=>$tmp_listitems_page['nextPageToken'],"playlistId"=>$id_pl_yt));
					$tmp_listitems['items'] = array_merge($tmp_listitems['items'],$tmp_listitems_page['items']);
					$cnt_it++;
				}
				// echo "nb items de la playlist ".$id_pl_yt." : ".count($tmp_listitems['items'])."<br />";
				$list_elem_yt = array () ; 
				if(isset($tmp_listitems['items']) && !empty($tmp_listitems['items'])){
					foreach($tmp_listitems['items'] as $item){
						if(isset($item['snippet']['resourceId']['videoId'])){
							$list_elem_yt[] = $item['snippet']['resourceId']['videoId'];
						}else{
							continue;
						}
					}
					if(empty($list_elem_yt)){
						$this->dropError("Pas d'id YT récupéré");
						continue ; 
					}
					
					$sql = "SELECT distinct t_doc.ID_DOC,".$this->champ_retour." from t_doc where ".$this->champ_retour." is not null AND ".$this->champ_retour." in ('".implode("','",$list_elem_yt)."') order by ID_DOC ASC";
					$res = $db->GetArray($sql);
					if(!empty($res)){
						require_once(modelDir.'model_doc.php');
						foreach ($res as $doc){
							$myDoc = new Doc(); 
							$myDoc->t_doc['ID_DOC'] = $doc['ID_DOC'];
							$myDoc->t_doc['ID_LANG'] = $_SESSION['langue'];
							$myDoc->getValeurs() ; 
							$found = false ; 
							foreach($myDoc->t_doc_val as $dval){
								if($dval['VAL_CODE'] === $id_pl_yt){
									$found=true;
									break ; 
								}
							}
							if(!$found){
								$val_array = $db->GetRow("SELECT ID_VAL,VAL_CODE,VAL_ID_TYPE_VAL, VAL_ID_TYPE_VAL AS ID_TYPE_VAL,VAL_ID_GEN,VALEUR
								from t_val where VAL_ID_TYPE_VAL=".$db->Quote($this->type_val_pl)." AND VAL_CODE=".$db->Quote($id_pl_yt)." AND ID_LANG=".$db->Quote($this->lang));
								$myDoc->t_doc_val[] = $val_array; 
								$cnt++;
								$ok = $myDoc->saveDocVal() ; 
							}
						}
					}
					
				}
			}
			trace("import_links_playlist_video, etape ".$this->etape_ref." nb de liens créés : ".$cnt);
			return true;  
		}else{
			$this->dropError("Erreur - non connecté ou params manquants");
			return false ; 
		}
	}
	
     
     function get_videos_yt(){
        $videos = array();
        try {
             // Call the search.list method to retrieve results matching the specified
             // query term.
             $searchResponse = $this->youtube->search->listSearch('id,snippet', array(
                                                                                'type' => 'video',
                                                                                'forMine' => 'true',
                                                                                'maxResults' => 50,
                                                                                ));
             
             # Merge video ids
             foreach ($searchResponse['items'] as $searchResult) {
             array_push($videos, $searchResult['id']['videoId']);
             }
                                                         
           } catch (Google_Service_Exception $e) {
             echo( 'A service error occurred: '.$e->getMessage());
            $this->dropError( 'A service error occurred: '.$e->getMessage());
         } catch (Google_Exception $e) {
            echo( 'An client error occurred: '.$e->getMessage());
            $this->dropError( 'An client error occurred: '.$e->getMessage());
         }
         return $videos;
     }
     
    function delete_video_from_yt($id_video){
        if(!isset($id_video) || empty($id_video)){
            $this->dropError("Erreur delete_from_yt, id video youtube non trouvé");
            return false ;
        }
            	 
		try {
			$ok = $this->youtube->videos->delete($id_video);
		} catch (Google_Service_Exception $e) {
			if($e->getCode() != '404') {
				throw $e;
			}
		} catch (Google_Exception $e) {
			if($e->getCode() != '404') {
				throw $e;
			}
		}			
		
         trace("retour delete video ".$id_video.": ".print_r($ok,true));
    }
                                                         
	
	
	
	function dropError($errLib) { 
		$this->error_msg.=$errLib."<br/>";
	}
	
	
	
	

}


?>