<?php 
Class BC{
	var $account_id;
	var $client_id;
	var $client_secret;
	var $api;
	var $id_video;
	var $url_oauth="https://oauth.brightcove.com/v3/access_token?grant_type=client_credentials";
	var $url_cms="https://cms.api.brightcove.com/v1/accounts/";
	var $url_di="https://ingest.api.brightcove.com/v1/accounts/";


	function __construct(){

	}
	
	function setDocXsl($doc_xsl){
		if(!file_exists($doc_xsl)){
			$this->dropError("Erreur - fichier doc_xsl non trouvé");
			return false ; 
		}else{
			$this->doc_xsl = $doc_xsl;
			
			return true ; 
		}	
	}
	

	
	// récupération des paramètres de login depuis une etape
	function getLogParamsFromEtape($etape_ref=null){}
	

	//nettoyer réponse curl
	 function bit32Clean($response){
        $response = preg_replace('/(?:((?:":\s*)(?:\[\s*)?|(?:\[\s*)|(?:\,\s*))+(\d{10,}))/', '\1"\2"', $response);       
        return $response;
    }

    //appel curl
	function curlRequest($options){
		trace('appel curl brightcove');
      	$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $options['url']);
        if (isset($options['headers'])) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $options['headers']);
        }
        if ($options['method'] === 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            if (isset($options['data'])) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $options['data']);
            }
            if (isset($options['user_pwd'])) {
                curl_setopt($curl, CURLOPT_USERPWD, $options['user_pwd']);
            }
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);

        $curl_error = null;
        if (curl_errno($curl)) {
            $curl_error = curl_error($curl);
        }
        curl_close($curl);

        if ($curl_error !== null) {
            // TODO
        }
        trace('réponse de curl');
        trace(print_r($this->bit32clean($response),1));
        return $this->bit32clean($response);
    }


    //génère acess token
	function get_access_token(){
		$auth_string = $this->client_id.':'.$this->client_secret;
		$options['url'] = $this->url_oauth;
		$options['method'] = 'POST';
		$options['headers'] = array('Content-type: application/x-www-form-urlencoded');
		$options['user_pwd'] = $auth_string;
		$access_token_response = $this->curlRequest($options);
		$access_token = json_decode($access_token_response);
		$access_token = $access_token->access_token;
		return $access_token;
	}
	

	
	//obtenir id BC à partir de référence id
	function get_video_id($reference_id){
		$access_token=$this->get_access_token();
		$options['url'] = $this->url_di.$this->account_id.'/videos/ref:'.$reference_id;
		$options['method'] = 'GET';
		$access_token=$this->get_access_token();
		$options['headers'] = array(
		    'Content-type: application/json',
		    'Authorization: Bearer '.$access_token,
		);
		$response = $this->curlRequest($options);
		 $response = json_decode($response);
		return $response->id;
	}

	// créer les metadonnées 
	function create_video($params_job=null){

		//traitement des données
		$search = array("\t", "\n", "\r");
		$title = str_replace($search, "", $params_job['donnees'][0]['title']);
		$description = str_replace($search, "", $params_job['donnees'][0]['description']);
		$long_description = str_replace($search, "", $params_job['donnees'][0]['long-description']);

		if(isset($params_job['donnees'][0]['tags']) && !empty($params_job['donnees'][0]['tags']))
		{
			$tags=explode(',',$params_job['donnees'][0]['tags']);
		}
		else
			$tags=array();

		$schedule = new StdClass();
		$schedule->ends_at =  $params_job['donnees'][0]['ends_date'];

		$video_metadata=array(
			"name"=>$title,
			"description"=>$description,
			"long_description"=>$long_description,
			"tags"=>$tags,
			"schedule"=> $schedule,
			"reference_id"=>$params_job['donnees'][0]['reference_id']
			);

		$video_metadata=json_encode($video_metadata);

		//envoie des données sur BC
		$access_token=$this->get_access_token();
		$options['url'] = $this->url_cms.$this->account_id.'/videos';
	    $options['method'] = 'POST';
	    $options['headers'] = array(
	                    'Content-type: application/json',
	                    'Authorization: Bearer '.$access_token,
	        			);
	    $options['data']= $video_metadata;
	    $video = $this->curlRequest($options);
	    $video = json_decode($video);
		$video_id = $video->id;
	    return $video_id;
	}

	//envoie de la vidéo
	function ingest_video($params_job=NULL,$video_id){

		//gestion des données ingestion
		$path = new StdClass();
		$path->url =  $params_job['donnees'][0]['path'];
		if(isset($params_job['donnees'][0]['callbacks']) && !empty($params_job['donnees'][0]['callbacks']))
		{
			$callbacks=explode(',',$params_job['donnees'][0]['callbacks']);
		}
		else
			$callbacks=array();

		$ingest_options=array(
			"master"=>$path
			);
		if(!empty($callbacks)){
			$ingest_options["callbacks"]=$callbacks;
		}
		if(!empty($params_job['donnees'][0]['profile'])){
			$ingest_options["profile"]=$params_job['donnees'][0]['profile'];
		}
		$ingest_options=json_encode($ingest_options);
		//ingestion
		$access_token=$this->get_access_token();
		$options['url'] = $this->url_di.$this->account_id.'/videos/'.$video_id.'/ingest-requests';
	    $options['method'] = 'POST';
	    $options['headers'] = array(
	        'Content-type: application/json',
	        'Authorization: Bearer '.$access_token,
	    );
	    $options['data']= $ingest_options;
	    $video = $this->curlRequest($options);
	}

	function update($id_doc=null,$etape=null){}	
	function delete($id_doc,$etape=null){}
	
	
	function dropError($errLib) { 
		$this->error_msg.=$errLib."<br/>";
	}
	
	
	
	

}


?>
