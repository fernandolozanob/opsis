<?php

interface Process
{
	public function doProcess();
	public function updateProgress();
	public static function kill($module,$xml_file);
}

?>