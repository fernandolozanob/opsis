<?php 

	require_once(libDir."aws/aws-autoloader.php");
	
	use Aws\S3\S3Client;
	use Aws\Exception\AwsException;
	use Aws\S3\Exception\S3Exception;
	
	// class toolbox pour communication avec l'API AWS
Class AwsTool{
	var $params;

	function __construct($params=NULL){
		if($params){
			$this->params =$params;
		}
	}
	
	// récupération des paramètres de login depuis une etape
	function getLogParamsFromEtape($etape_ref=null){
		global $db ;
		if(isset($etape_ref) && !empty($etape_ref) ){
			$id_etp = intval($etape_ref);
		}elseif(isset($this->etape_ref) && !empty($this->etape_ref)){
			$id_etp = $this->etape_ref;
		}
		
		if(!empty($id_etp)){
			$etape_arr = $db->GetRow("SELECT * from t_etape where id_etape=".intval($id_etp));
		}else{
			$etape_arr = $db->GetRow("SELECT * from t_etape, t_module where etape_id_module=id_module and module_nom='aws'");
		}
		
		if(!isset($etape_arr) || empty($etape_arr) ||   !isset($etape_arr['ETAPE_PARAM'])){
			$this->dropError("Erreur recupération identifiants depuis etape");
			return false ;
		}
		$tab_xml=xml2tab($etape_arr['ETAPE_PARAM']);
		
		if(isset($tab_xml['PARAM'][0])){
			foreach($tab_xml['PARAM'][0] as $k=>$v){
				$this->params[strtolower($k)] = $v;
			}
			return true ;
		}else{
			$this->dropError("Error - La recuperation des params de connexion a echouee");
			return false ;
		}
	}

	public function connectS3()
	{
		try{
			if(!$this->s3Client){
				if(isset($this->params['s3_version'])) $s3_version = $this->params['s3_version'];
				else $s3_version = 'latest';
				
				if(isset($this->params['s3_region'])) $s3_region = $this->params['s3_region'];
				else $s3_region = 'eu-west-1';
				
				$this->s3Client = new S3Client([
											   'version'     => $s3_version,
											   'region'      => $s3_region,
											   'credentials' => [
											   'key'    => $this->params['s3_key'],
											   'secret' => $this->params['s3_secret'],
											   ],
											   ]);
			}
		} catch (S3Exception $e) {
			$this->dropError('Erreur connectS3 :'.$e->getMessage());
		}
		return true;
	}

	
	public function putObjectS3($bucket_name, $file_path, $file_key)
	{
		$file_in_s3 = false;
		try{
			if(!$this->s3Client){
				$this->connectS3();
			}
			$result = $this->s3Client->putObject([
										   'Bucket'     => $bucket_name,
										   'Key'        => $file_key,
										   'SourceFile' => $file_path,
										   ]);
			$file_in_s3 = true;
		} catch (S3Exception $e) {
			$file_in_s3=false;
			$this->dropError('Erreur putObjectS3 :'.$e->getMessage());
		}
		return $file_in_s3;
	}
	

	public function fileExistS3($bucket_name, $file_key)
	{
		try{
			if(!$this->s3Client){
				$this->connectS3();
			}
			$result = $this->s3Client->headObject([
										   'Bucket'     => $bucket_name,
										   'Key'        => $file_key,
										   ]);
			return $result;
		} catch (S3Exception $e) {
			$this->dropError('Erreur S3 :'.$e->getMessage());
			return false;
		}
	}
	
	function dropError($error_msg) {
		$this->error_msg = $error_msg;
	}
	
	
	
	

}


?>
