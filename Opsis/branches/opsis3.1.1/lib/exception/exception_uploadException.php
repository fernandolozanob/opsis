<?php

require_once ('exception_networkException.php');

class UploadException extends NetworkException
{
	function __construct($message,$code=0)
	{
		parent::__construct($message,$code);
	}
}

?>