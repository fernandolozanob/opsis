<?php

require_once('exception_fileNotFoundException.php');

class MediaNotFoundException extends FileNotFoundException
{
	function __construct($message,$code=0)
	{
		parent::__construct('Media file not found : '.$message,$code);
	}
}

?>