<?php

class ParamTypeException extends Exception
{
	function __construct($message,$code=0)
	{
		parent::__construct('Input XML file not found : '.$message,$code);
	}
}

?>