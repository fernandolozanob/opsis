<?php

require_once('exception_fileNotFoundException.php');

class XmlInNotFountException extends FileNotFoundException
{
	function __construct($message,$code=0)
	{
		parent::__construct('Input XML file not found : '.$message,$code);
	}
}

?>