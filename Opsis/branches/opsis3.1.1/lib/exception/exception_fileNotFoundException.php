<?php

require_once('exception_fileException.php');

class FileNotFoundException extends FileException
{
	function __construct($message,$code=0)
	{
		parent::__construct($message,$code);
	}
}

?>