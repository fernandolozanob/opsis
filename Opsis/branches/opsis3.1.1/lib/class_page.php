<?php
/**
 *  Classe Page
 * Version : 1.0 (PHP4)
 * Author : L. Desjardins 02/08/2006
 */
include_once(libDir."class_user.php");
include_once(coreDir."/view/view.php");


class Page {

private static $instance;
var $urlaction;
var $page;
var $template;
var $langue;
var $titre;
var $includePath;
var $id;
private $start_offset;

var $prmAction, $prmPage, $prmSort, $prmNbLignes; // Noms des params GET comprenant l'action, la page de résultat, le tri

// Variables pour le Pager
var $max_rows; // max rows by page ($this->max_rows)
var $rows; // number of rows in pages ($this->rows)
var $fleches= array("prev"=>"&lt;","next"=>"&gt;","first"=>"&lt;&lt;","last"=>"&gt;&gt;"); //flèches par défaut : tableau
var $tab_options = array(5=>"5",10 => "10", 20 => "20", 50 => "50", 100 => "100", "all" => kTous);
var $found_rows;
var $result=array(); // Tableau de résultat de la requête
var $PagerLink;
var $nbLignes;
var $error_msg;

// Variables pour le tri
var $order; //tri récupéré depuis l'url
var $triInvert; //sens inverse (passé aux url du tableau pour la bascule ASC / DESC
var $col; //colonne de tri
var $params4XSL=array(); //Paramètres de base passés aux feuilles XSL.

// Variables url rewriting : 
var $rw_pattern ; 
var $arr_rw_pattern ; 
var $rw_param; 



	function Page () { //contructeur
		$this->prmAction="urlaction";
		$this->prmPage="page";
		$this->prmSort="tri";
		$this->prmNbLignes="nbLignes";
		$this->page=1;
		$this->titre=""; // titre par défaut
		$this->nomEntite=""; // titre par défaut
		$this->titreSite=gSite; // titre par défaut
		$this->initParams4XSL();
		$this->includePath=includeDir;
		$this->langue=strtolower($_SESSION['langue']);
		if(defined('gModRewrite') && gModRewrite){
			if(defined("gModRewrite_prm_pattern") && is_string(gModRewrite_prm_pattern)){
				$this->rw_pattern = gModRewrite_prm_pattern;
			}else{
				$this->rw_pattern = "%langue%/%urlaction%/%id%";
			}
			$arr_rw_pattern = explode('/',$this->rw_pattern);
			
			foreach($arr_rw_pattern as $idx=>$vars){
				if(substr($vars,0,1) == '%' && substr($vars,-1) == '%'){
					 $this->arr_rw_pattern[$idx] = trim($vars,'%');
				}
			}
			
		}
	}

	/**
	 * Singleton : une seule classe Page en même temps !
	 *
	 * @return Page
	 */
   public static function getInstance()
   {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
   }

	public function getTitrePage($withSite = true,$withEntite = false ){
		$str = "" ;
		if($withSite){
			$str .= $this->titreSite;
		}
		if($withEntite){
			$str .= $this->nomEntite;
		}
		if(!empty($str) && !empty($this->titre)){
			$str.=" : ";
		}
		$str.= $this->titre;
		return $str;
	}

	/** Assigne le fichier de design à la page**/
	function setDesign ($xslFile) {
		// En fonction des constantes "gate", on limite l'accès au site en redirigeant vers le template "locked.html"
		// On applique pas ce comportement pour les appels à empty.php (pr interaction ajax)
		// ATTENTION LE TEST DOIT RESTER DANS CET ORDRE
		if(strpos($xslFile,'empty.html')===false && $this->isLockedByGateBehavior()){
			$this->template = templateDir."locked.html";
		}else if (file_exists($xslFile)){
			$this->template=$xslFile; 
		}else{
			print('Le design de la page n existe pas');
		}
	}
	
	function isLockedByGateBehavior(){
		require_once(libDir.'class_user.php');
		$user = new User() ; 
		$user->getFromSession();
		
		// Si la page a le flag guestPartageAccess défini (normalement lorsqu'on passe depuis viewer.php)
		if(isset($this->guestPartageAccess) && $this->guestPartageAccess){
			$this->locked = false ; 
			return $this->locked;
		}
		
		// captcha gate 
		// l'accès au site necessite une première étape de validation anti-bots via captcha
		if(defined("gCaptchaGate")){
			$this->locked = "captchaGate";
			if(isset($_COOKIE['ops_verif']) && md5('Y_'.date('Ymd')) == $_COOKIE['ops_verif']){
				$_SESSION['AreUHuman'] = 'Y';
				$this->locked = false ; 
			}

			//suppression test !$user->loggedIn() sur la première partie du test => n'avait pas grand sens et causait un bug si on switchait d'une logGate à captchaGate
			if((!isset($_SESSION['AreUHuman'])) || (!isset($_COOKIE['ops_verif']) && $_SESSION['AreUHuman'] == 'Y')){
				$_SESSION['AreUHuman'] = 'test';
			}else if(isset($_SESSION['AreUHuman'])){
				if($_SESSION['AreUHuman'] == 'test'){
					if(isset($_POST['cap_valid_human']) && !empty($_POST['cap_valid_human'])){
						require_once(libDir.'/captcha/securimage.php');
						$captcha=new Securimage();
						if ($captcha->check($_POST['cap_valid_human'])){
							$captcha_valid=true;
						}else{
							$captcha_valid=false;
						}
						if($captcha_valid){
							$_SESSION['AreUHuman'] = 'Y';
							setCookie('ops_verif',md5('Y_'.date('Ymd')),time()+3600*8);
							$this->locked = false;
							// Ajout possibilité de rediriger vers la page cible initiale après validation captcha
							if (isset($_POST['refer']) && !empty($_POST['refer'])){
								Page::getInstance()->redirect($_POST['refer']);
							}
						}
					}
				}
			}
		}
	
		// logged gate 
		// l'accès au site necessite une connexion à un compte opsis
		if(defined("gLoggedOnlyGate") && !$this->locked){	
			if(isset($user) && !$user->loggedIn()){
				$this->locked = "loggedOnlyGate";
			}else{
				$this->locked = false;
			}
		}
		return $this->locked;
	}
	
	function autoLog($addToParams=''){
		try{
			$user = new User() ; 
			$user->getFromSession();
			$autoLogArr = unserialize(gAutoLogAs);
			if(!is_array($autoLogArr) || !isset($autoLogArr['login']) || !isset($autoLogArr['password'])){
				trace("autolog failed : autoLogArr mal formé");
				return false;
			}
			
			// on autolog pas si on est déja connecté (principalement utile si sur un autre compte ...)
			// on autolog pas si on est "deconnected" => seul moyen d'avoir l'état déconnecté sur un site en autoconnect
			// on autolog pas si on a "resetPassword" ou "loginValidate" => ce sont des opérations d'inscription /récupération de password autant éviter d'avoir le compte autolog actif 
			// 		pendant qu'on fait ces opérations sur un autre compte 
			// on autolog pas si une tentative de login manuel est en cours (c a d si on a un POST[login] && un POST['password']),  pour laisser la tentative de login se résoudre
			if (!$user->loggedIn() 
			&& !isset($_GET['disconnected']) 
			&& !isset($_GET['loginValidate']) && !isset($_GET['resetPassword'])
			&& !isset($_POST['login']) && !isset($_POST['password'])){
				// autolog demo
				$user->Login($autoLogArr['login'], $autoLogArr['password'], $addToParams);
			}
			elseif (!$user->loggedIn()){
				// si déconnecté => on affiche dans le formulaire doc/demo
				$_POST["login"] = $autoLogArr['login'];
				$_POST["password"] = $autoLogArr['password'];
			}
			
			
		}catch(Exception $e){
			trace("autolog failed : ".$e->getMessage());
		}
	}
	
	

	/** Calcule et renvoie le HTML d'une page
	 * Analyse du template : extraction et traitement des balises spéciales
	 * IN : template (init par méthode SetDesign)
	 * OUT : HTML
	*/
	function render() {
		try {
            $this->view = new View($this->template);
            
            // Detection balise content
            $src=file_get_contents($this->template);
            $motifContent='`<include[\s]*id="content"[\s]*/>`siu';
            @preg_match_all($motifContent,$src,$includeContent,PREG_SET_ORDER); // Pour les balises include
            if(!empty($includeContent)){
                $this->route();
            }
            $this->view->render();
		}catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}



	function getActionFromURL() {
		if(!empty($_REQUEST[$this->prmAction])) $this->urlaction=$_REQUEST[$this->prmAction]; //XXX : changé par LD le 10/05/07 pour AJAX type POST
        if(strpos($this->urlaction, '../')!==false) $this->urlaction="";
		return $this->urlaction;
	}

	function getPageFromURL($altPage = 0) {
		if(!empty($_REQUEST[$this->prmPage])){
			$this->page=$_REQUEST[$this->prmPage];
		}else if (!empty($altPage)){
			$this->page=$altPage;
		}
		return $this->page;
	}

	function getSortFromURL() {

		if (!isset($_REQUEST[$this->prmSort]) || preg_match('/[a-zA-Z0-9]*?/',$_REQUEST[$this->prmSort])==0) return ''; //pour eviter l'injection SQL
		$this->order=$_REQUEST[$this->prmSort];
		return $this->order;
	}
	/**
	 * addUrlParams : recrée la chaîne des paramètres GET en ôtant les paramètres url_action et page
	 * 	et en en ajoutant d'autres.
	 * IN : addParams => tableau de paramètres à ajouter (opt.), encodeAmpersand => encodage ou non du & (opt. true par deft)
	 * OUT : chaine de type QUERYSTRING contenant tous les paramètres à ajouter à un lien.
	 */
	function addUrlParams($addParams=null,$encodeAmpersand=true) {
		$arrParams=$_GET;
		$str="";

		unset($arrParams[$this->prmAction]);
		unset($arrParams[$this->prmPage]);
		if (is_array($addParams)) $arrParams+=$addParams;
		foreach ($arrParams as $k=>$v) {
			$str.=($encodeAmpersand==true?"&amp;":"&").$k."=".urlencode($v);
		}
		return $str;
	}

	/** Récupère des valeurs d'un fichier XML, pas utilisé pour le moment
	 *  pourrait être utile en cas de création de page ou d'éléments de page dynamique (menus, etc.)
	 */
	function getNavigationTree($file="navigation.xml") {
		$xmlTree=SimpleXML_load_file(confDir.$file);
		var_dump($xmlTree);
		// Code à créer ici
	}

	function getPOSTvars() {
		return ($_POST);
	}

	function getName() {
		$parts=explode("/",$_SERVER['SCRIPT_NAME']);
		$url=$parts[count($parts)-1];
		
		// MS - 21.02.17 - getName est massivement utilisé pour créer des liens de façon dynamique sur une page opsis, 
		// Dans le cadre du partage on a besoin de pouvoir rediriger les liens vers un autre fichier php que celui par lequel on arrive, on permet donc de définir une variable permettant d'altérer la valeur retournée par getName
		if(isset($this->redirect_name) && !empty($this->redirect_name)){
			$url = $this->redirect_name ;
		}
		
		return ($url);
	}

	private function route () {
        //si url rewritting
		if(defined('gModRewrite') && gModRewrite==1 ){
			$this->parseUri();
			//$this->makeCanonical();
			
			if($this->urlaction=='index'){
				 $this->urlaction='';
			}
			
			if(empty($this->urlaction)) {
				$this->getActionFromURL();
			}
		}
		else
		{
			$this->getActionFromURL();
		}

        
        // Choix controller
		if(isset($this->locked) && $this->locked !== false ){
            $ctrl = "Controller";
            $methode = "do_lock";
            $args['locked'] = $this->locked;

		}else{
            // Retrocompatbilité
            if(isset($_GET["html"]) && !empty($_GET["html"])){
               $this->urlaction = "html";
               $this->id = intval($_GET["html"]);
            }
            
            switch ($this->urlaction){
                case "conso":
                case "consoListe":
                    $ctrl = "Controller_conso";
                    break;
				case "doc":
                case "docListe":
                case "docSaisie":
                case "docSolr":
                    $ctrl = "Controller_doc";
                    break;

                case "html":
                case "htmlListe":
                case "htmlSaisie":
                    $ctrl = "Controller_html";
                    break;

                case "mat":
                case "matListe":
                case "matSaisie":
                    $ctrl = "Controller_mat";
                    break;

                case "valListe":
                case "valSaisie":
				case "valExists":
                    $ctrl = "Controller_val";
                    break;

                case "personne":
                case "personneListe":
                case "personneSaisie":
                case "persSaisie":
                    $ctrl = "Controller_personne";
                    break;

                case "panier":
                case "panierListe":
                case "panierPersoListe":
                    $ctrl = "Controller_panier";
                    break;

				case "requeteListe":
					$ctrl = "Controller_requete";
					break;

                default:
                    $ctrl = "Controller";
                    $methode = "do_action";
                    $args['includePath'] = $this->includePath;
                    $args['params4XSL'] = $this->params4XSL;
                    break;
            }
        }
        // Tableau d'arguments pour controller
        $args['urlaction'] = $this->urlaction;
        $args['id'] = $this->id;
		if(isset($this->guestPartageAccess) && !empty($this->guestPartageAccess)){
			$args['guestPartageAccess'] = $this->guestPartageAccess;
			
		}
       
        // Méthode par défaut
        if(empty($methode) && !empty($this->urlaction)){
            $methode = "do_".$this->urlaction;
        }

        // Instanciation controller et exécution méthode
        $ctrl_file =  coreDir."/controller/".strtolower($ctrl).".php";
        if (file_exists($ctrl_file) && !empty($methode)) {
            require_once($ctrl_file);
            $this->controller = new $ctrl();
            $this->controller->execute($methode, $args);
            $this->view->setParams($this->controller->params);
            if(isset($this->controller->params['title'])){
                $this->titre = $this->controller->params['title'];
            }
			if(isset($this->controller->params['setReferrer'])){
				$queue_flag = (isset($this->controller->params['setReferrer']['queue'])?intval($this->controller->params['setReferrer']['queue']):false);
				$this->setReferrer($queue_flag);
			}
        }
        else {
            throw new Exception("Controller '$ctrl' not found");
        }

	}



	/** Redirection de page
	 * IN : url
	 * OUT : header.
	 */
	public function redirect($url) {
		// AJOUTER UN TEST DE PERTINENCE URL
		//trace ("REDIRECTION ".$url);
		//trace (dirname($_SERVER['PHP_SELF'])."    ".$_SERVER['HTTP_HOST']);

		if (strpos($url,'http://')===0 || strpos($url,'https://')===0)
			header("Location: ".$url);
		else{
			$dir=str_replace('\\','/',dirname($_SERVER['PHP_SELF']));
			header("Location: http". ($_SERVER["HTTPS"]=='on'?"s":"") ."://" . $_SERVER['HTTP_HOST']
                     . (strpos($url,$dir)===false?$dir.(substr($dir,-1,1)=="/"?"":"/"):"")
                     . $url);
        }
        exit;
	}

	/**
	 * Préparation d'un ordre SQL à partir de l'analyse du paramètre sort dans l'url.
	 * IN : paramètre tri dans le GET, récup ordre par défaut true/false
	 * OUT : SQL (ORDER BY..., direct) et màj des variables order (col), triInvert (sens passé à l'url)
	 * 		et params4XSL (params de base passés aux XSL)
	 */
	function getSort ($withDefault=true,&$default=true,$default_field='') {
		global $db;
        $tri=$this->getSortFromURL();

        if($tri!=""){ //Calcul de la colonne de tri, cette valeur sera envoyée à la XSL pour afficher les flèches
            $default=false;
            $ordre=substr($tri,-1,1);
            $col = substr($tri,0,-1);
			if (!is_numeric($ordre)) {
				$ordre=0;
				$this->order=$tri;
				$col=$tri;
			}
			if ($ordre==0) {
				$this->triInvert="1";
				$this->col=$col;
			}else{
				$this->triInvert="0";
				$this->col=$col;
			}

			$this->initParams4XSL();

			//Deuxième partie, calcul du SQL de tri qui peut comporter plusieurs colonnes
			$_hasbrackets=preg_match("/^\[(.*)\](.*)$/i",$tri,$matches); //Présence de crochet autour des champs => concaténation
			if ($_hasbrackets) {
				$tri=$matches[1].$matches[2]; //on se débarrasse des crochets (mais on garde le sens)
				$sqlTri=str_replace(';',',',$tri);
				if (substr($sqlTri,-1,1)=='1') {
						$_cols=strtoupper(substr($sqlTri,0,-1));
						$_sens=" DESC "; }
				elseif (substr($sqlTri,-1,1)=='0') {
						$_cols=strtoupper(substr($sqlTri,0,-1));
						$_sens=" ASC "; }
				elseif (!is_numeric(substr($sqlTri,-1,1))) {
						$_cols=strtoupper($sqlTri);
						$_sens=" ASC ";
				}
				if (!empty($_cols)){
					return " ORDER BY ".$db->Concat($_cols).$_sens;
				}
			} else {
				// VP 19/1/09 : ajout tri multiple
				$sqlTri="";
				if(stripos($tri,'ORDER BY') !== false){
					$tri = trim(preg_replace('/ORDER BY/i','',$tri));
				}
				$fields=explode(",",$tri);
				foreach ($fields as $field) {

					$field=trim($field);
					$ordre=substr($field,-1,1);
					// ajout gestion si $tri embarque deja une direction => évite un bug constaté sur démo / jobListe.php
					if(stripos($field,'ASC') !== false || stripos($field,'DESC') !== false){
						preg_match('/DESC|ASC/i',$field,$matches);
						if(!empty($matches)){
							$fld = trim(str_replace($matches[0],'',$field));
							$direction = $matches[0];
						}else{
							$fld = $field;
							$direction ="";

						}
					}elseif (is_numeric($ordre)){
						if($ordre==1){$direction="DESC";}else{$direction="ASC";}
						$fld=substr($field,0,-1);
					}
					else {
						$direction="ASC";
						$fld=$field;
					}

					//update VG 16/02/12 : si il y a un nom de table dans le champ, celui-ci doit rester en minuscule
					//Gestion de la casse
					if(strpos($fld,'.') === false) {
						$fld = strtoupper($fld);
					} else {
						$aFld = explode(".",$fld);
						$fld = strtolower($aFld[0]).".".strtoupper($aFld[1]);
					}

					$sqlTri.=",".$fld." ".$direction;
				}

				if(!empty($sqlTri)){
					return " ORDER BY ".substr($sqlTri,1);
				}
			}
        } elseif ($withDefault) {
		if (isset($default_field) && !empty($default_field))
        		$_SESSION['sqlorderbydeft'][$this->urlaction]=$this->addDeftOrder($default_field);
        	//by ld 22/12/08: introduction de urlaction pour gérer des ordres par deft différents suivant la page
        	//puisqu'on n'a pas l'objet recherche ici...
			$default=true;
			if (isset($_SESSION['sqlorderbydeft'][$this->urlaction]))
				return $_SESSION['sqlorderbydeft'][$this->urlaction];
        }
	}


	/**
	 * Ajoute des clés de tri par défaut
	 * IN : tableau de clés et ordre de tri
	 * OUT : var classe sqlOrderByDeft contient la chaine SQL de tri
	 * NOTE : structure du tab d'ordre
	 * 	[COL][i]=> colonne,
	 * 	[DIRECTION][i]=> ASC/DESC (optionnel, ASC par déft)
	 *  [PREFIX][i]=> prefixe (ajouté devant la colonne,optionnel);
	 */
	function addDeftOrder($tabOrder) {
		if (empty($tabOrder)) return '';
		$sqlOrderByDeft=" ORDER BY ";
		foreach ($tabOrder['COL'] as $idx=>$key) {

			$sqlOrderByDeft.=" ".(!empty($tabOrder['PREFIX'][$idx])?$tabOrder['PREFIX'][$idx].".":"").($key);
			if ($tabOrder['DIRECTION'][$idx] && in_array(strtoupper($tabOrder['DIRECTION'][$idx]),array('ASC','DESC',''))) $sqlOrderByDeft.=" ".$tabOrder['DIRECTION'][$idx]." ";
			if ($idx<count($tabOrder['COL'])-1) $sqlOrderByDeft.=",";
		}
		$this->order=$tabOrder['COL'][0]; //du coup dans le tableau, on trie sur la première colonne
		$this->triInvert=($tabOrder['DIRECTION'][0]=='DESC'?0:1);

		return $sqlOrderByDeft;
	}

	/**
	 * Détermine le nombre le résultats par page à afficher
	 * A utiliser AVANT initPager et/ou afficheNbParPage
	 * IN : sessvar (byRef), variable de session dans laquelle le nombre est stocké
	 * 		Nb de lignes passé en REQUEST.
	 * OUT : sessvar initialisé et var de classe nbLignes aussi
	 */
	function setNbParPage(& $sessvar) {

			if($_REQUEST[$this->prmNbLignes] != '')
            {   $this->nbLignes = $_POST[$this->prmNbLignes];
                $sessvar= $this->nbLignes; }
            elseif(!isset($sessvar)) {
                $sessvar = 10;
            	$this->nbLignes=10;
            	}
            else $this->nbLignes=$sessvar;
	}


	/**
	 * Pour initialiser un pager depuis un tableau de résultats.
	 * Contrairement à Initpager qui execute puis analyse une requête SQL.
	 * IN: array de résultats.
	 * OUT : var de classe Result, Rows, Found_Rows, PagerLink
	 * 		False si pas array passé en param.
	 */
	function initPagerFromArray($arr,$addGETVars=null) {
		if (!is_array($arr)) {
 			$this->params4XSL['nb_pages']=0;
        	$this->params4XSL['nb_rows']=0;
			return false;
		}
		if (!isset($this->nbLignes) || $this->nbLignes=='all') $this->max_rows = count($arr); else $this->max_rows=$this->nbLignes;
		if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) ) $this->page=1;



		$this->found_rows=count($arr);

		$num_page=$this->num_pages();

		if($this->page > $num_page) $this->page = $num_page;
		if ($this->nbLignes!='all') $this->result=array_slice($arr,($this->page-1)*$this->max_rows,$this->nbLignes);
		else $this->result=$arr;


		$this->rows=$this->found_rows;

        $this->PagerLink=$this->getName()."?urlaction=".$this->getActionFromUrl().$this->addUrlParams($addGETVars)."&page=";
        $this->params4XSL['nb_pages']=$num_page;
        $this->params4XSL['nb_rows']=$this->found_rows;

	}

	/**
	 * Initialisation du Pager.
	 * IN : SQL, new_max (opt, nb de lignes max),
	 * 		$altVar = autre mode d'initialisation de la page en cours (ex : session),
	 * 			NOTE: cette variable n'est utile que si le sélecteur du nb de lignes est mis dans la XSL.
	 * 			Si ce sélecteur est appelé dans la page PHP, la var de session est déjà initialisée
	 * 		$addGETVars = variables additionnelles à passer aux liens du Pager
	 * OUT : var de classe : Result (resultat de requete "tronqué"),
	 * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales), PagerLink (url passée de page en page)
	 */
    function initPager($sql, $new_max = 10,$altVar=null,$addGETVars=null,$secstocache=0)
    {
        global $db;

        if (!isset($this->nbLignes)) $this->max_rows = $new_max; else $this->max_rows=$this->nbLignes;

       	$this->getPageFromURL();
        if ($this->page=="" && $altVar!=null) $this->page=$altVar;
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) ) $this->page=1;

        if($this->max_rows == "all"){
        	$i=0;
            if ($secstocache==0) $rsfull= $db->Execute($sql); else $rsfull=$db->CacheExecute($secstocache,$sql);

            if (!$rsfull) { //0 résultats retournés !
            	$this->rows=0;
            	$this->found_rows=0;
            	$this->page=1;
            	$this->PagerLink="";
            	$this->result=array();
            	$this->error_msg="Problem with request.";
            	return false;
            	}

            $this->max_rows = $rsfull->RecordCount();

            if ($this->max_rows>kMaxRows) $this->max_rows=kMaxRows; // précaution : si trop de lignes, on revient à un mode paginé pour ne pas surcharger le serveur
            else {
            $this->result=$rsfull->GetArray();
            $this->rows =$this->max_rows;
            $this->found_rows=$this->max_rows;
            $this->page=1;
            $rsfull->Close();
            }
        }

		if ($this->max_rows!="all")
		{

	        $i=0;
			if(isset($this->start_offset) && !empty($this->start_offset)){
				$rs_found_rows=$db->Execute($sql.$db->limit($this->start_offset,($this->max_rows*$this->page)),false);
				if ($rs_found_rows) {
					$this->result=$rs_found_rows->GetArray();
					$this->rows =count($this->result);
					$this->found_rows = $this->rows ; // Attention : propriété "cachée" de ADODB
					$rs_found_rows->Close();

				} else {$this->found_rows=0;$this->page=1;$this->error_msg="Problem with request.";return false;}
				
			}else{
				$rs_found_rows=$db->PageExecute($sql,$this->max_rows,$this->page,false,$secstocache);
				if ($rs_found_rows) {
					$this->found_rows = $db->_maxRecordCount; // Attention : propriété "cachée" de ADODB
					$i_min = ($this->page - 1) * $this->max_rows;
					if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
					$this->result=$rs_found_rows->GetArray();
					$this->rows =count($this->result);
					$rs_found_rows->Close();

				} else {$this->found_rows=0;$this->page=1;$this->error_msg="Problem with request.";return false;}
			}
		}
        $this->PagerLink=$this->getName()."?urlaction=".$this->getActionFromUrl().$this->addUrlParams($addGETVars)."&page=";
		// dans le cas ou on est en train d'acceder à la recherche avec un offset, on ne veut pas de cette reprise automatique 
        if ($this->page > $this->num_pages() && (!isset($this->start_offset) || empty($this->start_offset))) {
			$this->page = $this->num_pages();
			//PC 04/12/12 : Rappel de PageExecute quand la page courante est supérieur au nombre de pages totales
			$rs_found_rows=$db->PageExecute($sql,$this->max_rows,$this->page,false,$secstocache);
		    if ($rs_found_rows) {
		        $this->found_rows = $db->_maxRecordCount;
		        $i_min = ($this->page - 1) * $this->max_rows;
		        if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
		       	$this->result=$rs_found_rows->GetArray();
		        $this->rows =count($this->result);
				$rs_found_rows->Close();
		    }
		}
        $this->params4XSL['nb_pages']=$this->num_pages();
        $this->params4XSL['nb_rows']=$this->found_rows;

        return true;
    }

	/** Calcule le nombre de pages à afficher
	 *  IN : var classe Found_rows et max_rows
	 *  OUT : nombre de pages (int)
	 */
    function num_pages(){
    	if ($this->max_rows==0) return 0;
        $num = round($this->found_rows / $this->max_rows);
        if($num < ($this->found_rows / $this->max_rows)) $num++;
        return $num;
    }

	/** Rendu automatique du pager (selection de page)
	 *  IN : includeFirst, includeLast => liens direct vers la première / dernière page ? (0/1)
	 *  IN : includeInput => champ texte de choix de page, nbPageDisplayed => nb de numéros dans le cas d'un affichage par numéro (1|2|3...)
	 *  IN : class => classe appliquée
	 *  IN : vars de classe page
	 *  OUT : code html à insérer
	 *  NOTE : Cette fonction est devenue obsolète, on préfèrera utiliser les possibilités plus souples
	 *  offertes par les feuilles de style.
	 */

	function renderPager($includeFirst=1,$includeLast=1,$includeInput=0,$nbPageDisplayed=10,$class="resultsMenu") {

	    $menu = '<!-- Navigation Menu -->'; // Debug line for HTML
        $num_page = $this->num_pages();
		if ($num_page==0) return false; // Si aucun résultat, pas de pager !

        if($this->page < 1) $this->page = 1;
        if($this->page > $num_page) $this->page = $num_page;

        $pagemin = (integer) (($this->page-1) / $nbPageDisplayed);
        $pagemin = $pagemin*$nbPageDisplayed+1;
        $pagemax = $pagemin + $nbPageDisplayed;

        if($this->page > $nbPageDisplayed){
        //Pour acc�der au d�but directement
            if ($includeFirst==1) {
            if($this->page == 1)
                $menu.='<b>'.$this->fleches["first"].'&nbsp;</b> ';
            else
                $menu.='<a href="'.$this->PagerLink.(1).'">'.$this->fleches["first"].'&nbsp;</a> ';
        	}

            $menu.='<a href="'.$this->PagerLink.($pagemin-1).'">'.$this->fleches["prev"].'</a>&nbsp;&nbsp;';

        }

       if ($num_page>1) { // Sélecteur uniquement si  nb pages >1
	       if ($includeInput!=1) {
		        for($i = $pagemin; $i <= min($num_page,$pagemax-1) ; $i++){
		            if($this->page == $i){
		                $menu.='<b>'.$i.'</b>';
		            }else{
		                $menu.='<a href="'.$this->PagerLink.$i.'">'.$i.'</a>';
		            }
		            if($i != min($num_page,$pagemax-1))
		                $menu.=' | ';
		        }
			}
	        else $menu.="<input type='text' name='rang' value='".$this->page."' size='5' onBlur='javascript:location.href=\"".$this->PagerLink."\"+eval(this.value)' />&nbsp;&nbsp;/&nbsp;&nbsp;".$this->num_pages();
       }

      if($pagemax <= $num_page){ // a surveiller
            $menu.='<a href="'.$this->PagerLink.($pagemax).'">&nbsp;&nbsp;'.$this->fleches["next"].'</a>';
            //Pour acc�der � la fin directement
            if ($includeLast==1) {
	            if($this->page == $num_page)
	                $menu.='<b>&nbsp;&nbsp;'.$this->fleches["last"].'</b>';
	            else
	                $menu.='<a href="'.$this->PagerLink.($num_page).'">&nbsp;&nbsp;'.$this->fleches["last"].'</a>';
            }
        }/*else
            $menu.='&gt;'; */


        // Affichage nombre de resultats et de pages

        $menu = "<table width=\"95%\" border=\"0\" class=\"".$class."\"><tr><td align=\"left\" width=\"33%\">".$this->found_rows." ".kResultats."</td>
        		<td align=\"center\" width=\"33%\">".$menu."</td>
        		<td align=\"right\" width=\"33%\">".$num_page." ".kPages."</td></tr></table>";
        return $menu;
	}


	/**
	 * Ajoute de nouveaux paramètres aux paramètres par défaut passés aux XSL
	 * IN : array de paramètres
	 * OUT : tableau params4XSL mis à jour et dédoublonné
	 */
	function addParamsToXSL($arr) {
		if (!is_array($arr)) return false;
		//$this->params4XSL=$this->params4XSL+$arr;
        $this->params4XSL=array_merge($this->params4XSL, $arr);
	}

	/**
	 * Initialise les paramètres par défaut passés aux XSL de liste
	 * Ordre (sens du tri), Tri (colonne triée), ScriptUrl (page en cours), Profil (profil utilisateur)
	 * NOTE : en cas de recherche (initPager) => ajout des params nb_pages et nb_rows
	 * @update VG 07/04/2010 : ajout du paramètre "referrer"
	 */

	protected function initParams4XSL() {
		$myUsr = User::getInstance();
		$this->params4XSL=Array("ordre"=>$this->triInvert, "tri"=>$this->order,"scripturl"=>$this->getName(),"profil"=>$myUsr->getTypeLog(), "referrer"=>$this->getReferrer(), "userId"=>$myUsr->UserID);
	}


	/**
	$nomelement : nom de l'�lement dans le xml
	$nomFichierXsl : nom du fichier xsl de transformation
	$options : 1 user, 2 doc, 3 admin
	$page : num�ro de la page

	$ajoutPrivilegeDoc : Faut-il ajouter aux r�sultats le statuts de leurs privil�ges (seulement utile pour la gestion des documents). N.b. Ceci pourrait �tre g�rer de fa�on plus propre dans une classe Doc
	*/
	function afficherListe($nomelement,$nomFichierXsl,$ajoutPrivilegeDoc=FALSE,$arrHighlight=null,$xml2merge='',$eval=false)
	{

	        if ($ajoutPrivilegeDoc){
	            $this->result = ajoutPrivResult($this->result);
                if(function_exists("ajoutPrivResultCustom")){
                    $this->result=ajoutPrivResultCustom($this->result);
                }
	        }

	       // if ($this->result!=null)
	       // {
	           $xml = TableauVersXML($this->result,$nomelement,4,"select",1,$xml2merge); //classique



	           $log = new Logger("data_$nomelement.xml");
	           $log->Log($xml);
	           foreach($this->params4XSL as $ind => $data) $tab[$ind] = $data;
	           $html=TraitementXSLT($xml,$nomFichierXsl,$tab,0,$xml,$arrHighlight);
	           if(!empty($eval) && $eval=true){
	           	$html = eval("?".chr(62).$html.chr(60)."?");
	           }
	           echo $html;
	      //  }
	      //  else print kAucunResultat;
	}

	/**
	 * Affiche le selecteur de nb de résultats par page
	 * IN  : links (bool :affichage de type lien ou select), variable de session, libellé (opt)
	 * 		 Les autres paramètres (optionnels) ne sont utilisés qu'en mode Select :
	 * 		 Form_nom, form_method (pref. POST), select_name
	 * OUT : print du HTML généré
	 */
	function afficherNbParPage($links=false,$label,$addGETVars=null,$form_nom="form_aff",$form_method="POST"){

		if (!$links) {
            $param_aff.="<form name='$form_nom' id='$form_nom' action='".$this->getName()."?urlaction=".$this->urlaction.$this->addUrlParams($addGETVars)."' method=\"$form_method\" style=\"margin:0px\">$label";
            $param_aff.="<select name='".$this->prmNbLignes."' onChange='javascript:document.$form_nom.submit()'>";

            foreach($this->tab_options as $tab_options=>$valeur) {
                $param_aff .= "<option value=\"$tab_options\"";
                if(strcmp($tab_options,$this->nbLignes) == 0)
                    $param_aff .= " selected=\"selected\"";
                $param_aff .= ">".$valeur."</option>\n";
            }
            $param_aff.="</select></form>";
		}
		else {
			$param_aff= "<div class='nbSelecteur'>";
			foreach ($this->tab_options as $tab_options=>$valeur) {
				$param_aff.="<a href='".$this->getName()."?urlaction=".$this->urlaction."&amp;".$this->prmNbLignes."=".$tab_options."'>$valeur</a>&nbsp;&nbsp;";
			}
			$param_aff.="</div>";
		}
   			return $param_aff;
	}


	/**
	 * Stocke la page courante, paramètres GET compris, dans une variable de session.
	 * Cette variable est "circonstanciée" par le ficher "root" en cours. (ex: index.php, iframe.php)
	 * pour éviter des confusions entre les différents types de navigation (pleine page, popup, palette, etc.)
	 * Cette variable sera ensuite utilisée pour les liens de "retour" présents sur les pages de détail et d'édition.
	 * Ajout de la variable canonical dans le cas de réecriture url pour faciliter l indexation google.
	 *
	 */
	function setReferrer($queue = false,$str_label="",$overwrite_last_ref_same_urlaction = false) {
		/* Si on a des params page et tri uniquement en POST, on les ajoute à l'url stockée
		 * Ceci permet de revenir directement avec le bon tri et la bonne page */
		static $prm = '';
		if (!isset($_GET['tri']) && empty($_GET['tri']) && isset($_POST['tri']) && !empty($_POST['tri'])) $prm.="&tri=".$_POST['tri'];
		if (!isset($_GET['page']) && empty($_GET['page']) && isset($_POST['page']) && !empty($_POST['page'])) $prm.="&page=".$_POST['page'];

		$url = str_replace(array('&amp;'),array('&'),((isset($_SERVER['HTTPS'])&&!empty($_SERVER['HTTPS']))?'https://':'http://').$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].$prm);
		$arr_params = array() ; 
		if(strpos($url,'?')!==false){
			$script_name = substr($url,0,strpos($url,'?'));
			$url_params = substr($url,strpos($url,'?')+1);
			parse_str($url_params,$arr_params);
		}else{
			$script_name  = $url;
		}
		
		if($queue){
			// MS - 18.09.15 - nouveau paramètre $queue ajouté, qui permet de stocker des tableaux de referer au lieu d'une seule url de reference
			if(!is_array($_SESSION[$_SERVER['SCRIPT_NAME']])){
				if(strpos($_SESSION[$_SERVER['SCRIPT_NAME']],'?')!==false){
					$existing_script_name = substr($_SESSION[$_SERVER['SCRIPT_NAME']],0,strpos($_SESSION[$_SERVER['SCRIPT_NAME']],'?'));
					$existing_url_params = substr($_SESSION[$_SERVER['SCRIPT_NAME']],strpos($_SESSION[$_SERVER['SCRIPT_NAME']],'?')+1);
					parse_str($existing_url_params,$existing_arr_params);
				}else{
					$existing_script_name  = $_SESSION[$_SERVER['SCRIPT_NAME']];
				}
				// transtypage referer string contenant une URL  => array d'URLs
				$_SESSION[$_SERVER['SCRIPT_NAME']] = array(array('url'=>$_SESSION[$_SERVER['SCRIPT_NAME']],'str'=>'','titre'=>$this->getTitrePage(false,true),'script_name'=>$existing_script_name,'arr_params'=>$existing_arr_params));
			}
			$arr_urls = array() ;
			foreach($_SESSION[$_SERVER['SCRIPT_NAME']] as $idx=>$val){
				$arr_urls[] =$val['url'];
			}
			// if(defined('gModRewrite') && gModRewrite==1){
			// 	$this->canonical .= "/".$this->getTitrePage(false,false);
			// }
			if($url == end($arr_urls) || (strpos($url,'&page=')!==false && ((isset($_POST['page']) && !is_numeric($_POST['page'])) || (isset($_GET['page']) &&  !is_numeric($_GET['page']))))){
				// dans le cas d'un reload de la page, on altère pas le tableau referer
				// OU dans le cas d'un accès à une url contenant un paramètre "page" => il s'agit d'une sauvegarde+redirection, on ne la stocke pas en tant que page puisque l'utilisateur ne voit pas cette page passer
				return false ;
			}else if(in_array($_SERVER['HTTP_REFERER'],$arr_urls) && in_array($url,$arr_urls) ){
				// si le HTTP_REFERER & la page courante ($url) sont déjà dans la liste des referers, alors c'est qu'on revient d'une page en bout de navigation vers une déja naviguée
				// on tronque donc l'array des referer après $url pour récupérer un tableau de referer semblable à celui qu'on avait lors du premier passage sur cette page
				$offset = array_search($url,$arr_urls)+1;
				$length = count($arr_urls);
				array_splice($_SESSION[$_SERVER['SCRIPT_NAME']],$offset,$length);
			}else{
				if ($overwrite_last_ref_same_urlaction == true && count($_SESSION[$_SERVER['SCRIPT_NAME']])>0){
					$lastRef = end($_SESSION[$_SERVER['SCRIPT_NAME']]);
					// si flag $overwrite_last_ref_same_urlaction est à true et que le last referer dans l'array des referer est une page ayant la meme urlaction (sans necessairement etre la meme url, ce cas est traité plus haut)
					// alors on supprime le dernier referer et on le remplace par l'url qu'on est en train de set => permet d'éviter de flood le breadcrumb quand on saute entre plusieurs occurences docs ou catégories par ex
					if(isset($lastRef['arr_params']['urlaction']) && $lastRef['arr_params']['urlaction'] == $arr_params['urlaction']){
						array_splice($_SESSION[$_SERVER['SCRIPT_NAME']],count($_SESSION[$_SERVER['SCRIPT_NAME']])-1);
					}
				}
				
				// cas default => on ajoute l'url courante au tableau des referer
				$_SESSION[$_SERVER['SCRIPT_NAME']][] = array('url'=>$url,'str'=>$str_label,'titre'=>$this->getTitrePage(false,true),'script_name'=>$script_name,'arr_params'=>$arr_params);
			}
		}else{
			// comportement inchangé si $queue == false
			// $_SESSION[$_SERVER['SCRIPT_NAME']]=$url;
			$_SESSION[$_SERVER['SCRIPT_NAME']] = array(0=>array('url'=>$url,'str'=>$str_label,'titre'=>$this->getTitrePage(false,true),'script_name'=>$script_name,'arr_params'=>$arr_params));
		}
		$_SESSION['breadcrumb'][$_SERVER['SCRIPT_NAME']] = $this->refererTab2BreadCrumb();
	}

	static function refererTab2BreadCrumb($arr_input=null){
		if(is_array($arr_input) && !empty($arr_input)){
			$arr_referer = $arr_input;
		}else{
			$arr_referer = $_SESSION[$_SERVER['SCRIPT_NAME']];
		}
		if(!isset($arr_referer) || empty($arr_referer)){
			return false ;
		}

		if(!is_array($arr_referer)){
			// transtypage referer string contenant une URL  => array d'URLs
			$arr_referer = array($arr_referer);
		}

		$arr_bc = array() ;
		$idx_bc = 0 ;
		foreach($arr_referer as $idx=>$step){
			// récupération des divers paramètres
			$arr_params = array() ; 
			if(isset($step['script_name']) && !empty($step['script_name'])){
				$script_name = $step['script_name'] ;
			}else{
				$script_name = $step['url'] ; 
			}
			if(isset($step['arr_params']) && !empty($step['arr_params'])){
				$arr_params = $step['arr_params'] ;
			}
			
			if(!empty($arr_params['urlaction'])){
				$arr_etape = array_merge($step,array('script_name'=>$script_name),$arr_params);
				$cle_etape = str_replace(array('View','Saisie'),'',$arr_params['urlaction']);
				unset($cle_id);
				if(array_key_exists('id_'.$cle_etape,$arr_params)){
					$cle_id = $arr_params['id_'.$cle_etape];
					$cle_concat = $cle_etape."_".$cle_id;
				}else{
					$cle_concat = $cle_etape;
				}

				if(empty($arr_bc)){
					// si arr breadcrumb n'existe pas, on la crée
					$arr_bc[$idx_bc] = array('key'=>$cle_concat,'instances'=>array($arr_etape));
					$idx_bc++;
				}else{
					// possibilité de définir une clé à laquelle se "raccrocher" => c a d qu'on supprime toutes les étapes intermédiaires, utile pour la navigation dans l'administration par ex. 
					if(isset($arr_params['bc_prec']) && !empty($arr_params['bc_prec'])){
						foreach($arr_bc as $i => $bc){
							if($bc['key'] == $arr_params['bc_prec'] && isset($arr_bc[$i + 1])){
								array_splice($arr_bc,$i+1);
								$idx_bc = $i+1 ;
								break ; 
							}
						}
					}
					$last_bc = &$arr_bc[count($arr_bc)-1];
					$last_bc_splitkey = explode('_',$last_bc['key']);
					// posait des problèmes pour les sauts d'un média reportage => élément du reportage, & sur les documents liés
					//(isset($last_bc_splitkey[1]) && isset($cle_id) && $last_bc_splitkey[1] !=  $cle_id) ||  // (si on a des ids définis , sont ils différents? OU
					if($last_bc_splitkey[0] == $cle_etape && // on est sur la même entité ? ET
							((!isset($cle_id) && isset($arr_params['rang'])))){ 		// l'élément précédent avait un id, ce qu'on ajoute a uniquement un rang, => on doit être dans le cas d'une navigation inter notices
							// si on est sur la même entité, mais qu'on a pas le même id, ou si on a pas d'id et qu'on navigue par rang, on remplace la dernière étape breadcrumb (cf navigation doc inter-notices)
							$arr_bc[$idx_bc-1] = array('key'=>$cle_concat,'instances'=>array($arr_etape));
					}else if($cle_concat == $last_bc['key'] // si même key (<entité>_<id>) entre l'élement précédent et l'actuel, => même objet
						|| (isset($cle_id) && isset($arr_params['rang']) && isset($last_bc['instances'][count($last_bc['instances'])-1]['rang']) && $arr_params['rang']==$last_bc['instances'][count($last_bc['instances'])-1]['rang'] )){// si l'élément précédent avait un rang mais pas de clé, et que l'actuel a une clé et le même rang => même objet
						// => les différentes urls sont regroupées dans les instances de l'étape breadcrumb
						$last_bc['instances'][] = $arr_etape;
					}else{
						// sinon on ajoute crée une nouvelle étape breadcrumb à partir du referer
						$arr_bc[$idx_bc] = array('key'=>$cle_concat,'instances'=>array($arr_etape));
						$idx_bc++;
					}
				}
			}
		}
		return $arr_bc;
	}

	function getReferrer() {
		if (isset($_SERVER['SCRIPT_NAME']) && !empty($_SERVER['SCRIPT_NAME']) && isset($_SESSION[$_SERVER['SCRIPT_NAME']]) && !empty($_SESSION[$_SERVER['SCRIPT_NAME']])){
			$curr_url = str_replace(array('&amp;'),array('&'),((isset($_SERVER['HTTPS'])&&!empty($_SERVER['HTTPS']))?'https://':'http://').$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
				
			if(isset($_SESSION['breadcrumb'][$_SERVER['SCRIPT_NAME']])){
				$arr_breadcrumb = array_reverse($_SESSION['breadcrumb'][$_SERVER['SCRIPT_NAME']]);

				foreach($arr_breadcrumb as $idx=> $entity){
					$found = false ;
					foreach($entity['instances'] as $inst){
						if($inst['url'] == $curr_url){
							$found = true ;
						}
					}
					if($found==false){
						$lastInstance = end($entity['instances']);
					}
					if(isset($lastInstance) && $lastInstance != false){
						break;
					}
				}
				// Si on a pas trouvé de lastInstance, c'est que l'url courante est la seule dans le tableau breadcrumb
				// on renvoi donc vers la page d'accueil
				if(!isset($lastInstance)){
					$lastInstance['url'] = 'index.php';
				}
				$referer = $lastInstance['url'];
			}else{
				$referer = $_SESSION[$_SERVER['SCRIPT_NAME']];
				if(is_array($referer)){
					$ref = end($referer);
					while($ref && $curr_url == $ref['url']){
						$ref = prev($referer);
					}
					$referer = $ref['url'];
				}
			}
			return $referer;
		}else{
			return 'index.php';
		}
	}

	function getArrReferrer(){
		if(isset($_SESSION[$_SERVER['SCRIPT_NAME']])){
			return $_SESSION[$_SERVER['SCRIPT_NAME']];
		}else{
			return array();
		}
	}

	function initReferrer() {
		$_SESSION[$_SERVER['SCRIPT_NAME']]='';
		$_SESSION['breadcrumb'][$_SERVER['SCRIPT_NAME']] = $this->refererTab2BreadCrumb();
	}

	function hasReferrer() {
		if (empty($_SESSION[$_SERVER['SCRIPT_NAME']])) return false; else return true;
	}


	/** Affiche le lien de retour vers la liste.
	 *  IN :  caption (libellé du lien), style (chaine de style css ou nom de classe complet ex : "class='maclasse'" )
	 */
	function renderReferrer($caption='',$style='', $suffix = '') {

		if (empty($caption)) $caption=kRetour;

		if ($this->hasReferrer()) {
			$referer = $this->getReferrer();
			if (!empty($suffix))
				$referer .= $suffix;
			echo "<a ".$style." href='".$referer."' >".$caption."</a>";
		}
	}



/*  Analyse de l’url pour extraire les paramètres urlaction, lang et id.  et initier les variables de classe correspondantes ($urlaction, $id et $lang).*/
	function parseUri(){
		$path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
		//extrait le premier slash apres http:// ou https://
		$pos=strpos(kCheminHttp,"/",8);
		// si pos existe pas > $path ne contient pas de sous repertoire /www/ ou /int/, path contient directement les parametres /fr/doc/4059/
		if(!empty($pos)){
			$var = substr(kCheminHttp,$pos); 
			$suffixe = explode($var,$path);
			$long_url=$suffixe[1];
		}else{
			$long_url=$path;
		}
		
		// On supprime le tout premier slash pour éviter d'avoir le premier paramètre toujours égal à une chaine vide après explode sur '/'
		if(substr($long_url,0,1) === '/'){
			$long_url = substr($long_url,1) ; 
		}
		
		$url_params = explode("/", $long_url);
		
		// trace("class_Page - parseURI - params : ".print_r($url_params,true)." test pattern XXX.php".preg_match('/([a-zA-Z-_]+\.php)/',$url_params[0]));
		// Le premier paramètre reconnu est une référence à un fichier php, on est donc pas dans le cas d'une URL réécrite, on ne va donc pas tenter d'interpreter les paramètres en fonction du rewrite pattern
		// On sort donc de la fonction, et la fonction classique getActionFromURL sera appelée à la place 
		// On conserve un flag dans l'objet permettant d'identifier qu'on est dans un cas où l'url n'a pas été interprétée en fonction du rewrite pattern
		if(isset($url_params[0]) && !empty($url_params[0]) && preg_match('/([a-zA-Z-_]+\.php)/',$url_params[0])){
			$this->bypass_url_rewrite = true ;  
			return false ; 
		}
		
		
		foreach($url_params as $idx=>$param){
			if(isset($this->arr_rw_pattern[$idx]) && !empty($this->arr_rw_pattern[$idx]) && is_string($this->arr_rw_pattern[$idx])){
				$var_name = $this->arr_rw_pattern[$idx]; 
				$this->rw_param[$var_name] = html_entity_decode(urldecode($param)) ;
				if(in_array($var_name,array('id','urlaction','langue'))){
					$this->$var_name = html_entity_decode(urldecode($param)) ; 
				}
			}
		}
	}

	function getUrlFromAction($urlaction, $id,$titre,$params_str){
	// test a changer car possible mode mixte urlaction doit etre rempli
		if(defined('gModRewrite') && gModRewrite==1){
			$titre=$this->urlEncodeTitle($titre);
			$this->langue=strtolower($_SESSION['langue']);
			$out_url =array(kCheminHttp) ;
			
			parse_str($params_str,$params);
			foreach($this->arr_rw_pattern as $idx=>$var_name){	
				if($var_name == 'urlaction' && isset($urlaction) && !empty($urlaction)){
					$out_url[] = $urlaction;
				}else if($var_name == 'langue' && isset($this->langue) && !empty($this->langue)){
					$out_url[] = $this->langue;					
				}else if($var_name == 'id' && isset($id) && !empty($id)){
					$out_url[] = $id;
				}else if($var_name == 'titre' && isset($titre) && !empty($titre)){
					$out_url[] = $titre;
				}else if (isset($params[$var_name]) && !empty($params[$var_name]) && is_scalar($params[$var_name])){
					$out_url[] = $params[$var_name];
				}else if (isset($this->rw_param[$var_name]) && !empty($this->rw_param[$var_name])){
					$out_url[] = $this->rw_param[$var_name] ; 
				}else{
					$out_url[] = "" ; 
				}
			}
			
			$out_url = implode('/',$out_url);
			if(!empty($params_str)){
				$first_char = substr($params_str,0,1); 
				if($first_char =='?' ){
					$out_url.=$params_str;
				}else if ($first_char =='&' ){
					$out_url.='?'.substr($params_str,1);
				}else {
					$out_url.='?'.$params_str;
				}
			}
			
			return $out_url;
		}
		else return 'index.php?urlaction='.$urlaction.'&id='.$id.'&'.$params;

	}
// function getUrlFromAction($urlaction, $id,$titre,$params){
// 		if(defined('gModRewrite') && gModRewrite==1 ){
// 			$titre=$this->urlEncodeTitle($titre);
// 			$this->langue=strtolower($_SESSION['langue']);
// 			if(!empty($params)){
// 				return kCheminHttp.'/'.$this->langue.'/'.$urlaction.'/'.$id.'/'.$titre.'?'.$params;
// 				}
// 			else{
// 				return kCheminHttp.'/'.$this->langue.'/'.$urlaction.'/'.$id.'/'.$titre;
// 				}
// 		}
// 		else return 'index.php?urlaction='.$urlaction.'&id='.$id.'&'.$params;

// 	}

	function urlEncodeTitle($titre){
		$name=str_replace(' ','+',$titre);
        $name=explode("/",str_replace("\\","/",$name));
        $name=$name[count($name)-1];
		//on veut garder le +
		$name = replaceAccents($name);
         $name=str_replace(array(' ','\'','"','$','%','?',';',':','!','#','|','>','<','*','\\','&',"'",'~','(',')','=',',','`',"\xC2\xA0","«","»","\xB0"),'_',$name);
	  return $name;

	}

	function setOffset($n){
		if(isset($n) && !empty($n) && is_numeric($n))
			$this->start_offset = $n;
	}
	
	function makeCanonical(){
		// VP 9/06/2017 : appel urlEncodeTitle dans boucle sur arr_rw_pattern pour ne pas modifier la variable titre
		$canonical = array() ; 
		//$this->titre=$this->urlEncodeTitle($this->titre);
		foreach($this->arr_rw_pattern as $idx=>$var_name){
			$canonical[$idx] = (isset($this->rw_param[$var_name]) && !empty($this->rw_param[$var_name]) && is_scalar($this->rw_param[$var_name]))?$this->rw_param[$var_name]:$this->$var_name;
			if($var_name == 'titre'){
				$canonical[$idx] = $this->urlEncodeTitle($canonical[$idx]);
			}
		}
		$this->canonical = implode('/',$canonical);
		return $this->canonical;
	}
} // fin de classe PAGE
?>
