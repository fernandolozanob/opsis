<?php
/*
	* This program is part of "upload with curl" code sample. This is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	* 
	* Created by Kewego - 2009  - See more details on <http://developers.kewego.com/>
	* 
	* Version 1.0.0
*/	


// GENERAL SETTINGS
// Debug 'on' or 'off'
define('DEBUG', 'off');

// SPECIFIC CLIENT SETTINGS

define ('DESTINATION_PATH', '<temporary_path>'); //ex : 'C:/wamp/tmp/'
define('APPLICATION_KEY', 'c1a79b135493b78aad0ad6b804cebaba');
define('USERNAME', urlencode("vprost@opsomai.com"));
define('PASSWORD', urlencode("opsomai"));

?>