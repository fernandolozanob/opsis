<?php


include_once(libDir."class_page.php");

class PageSolr extends Page
{
	private static $instance;
	
	private $solr_client;
	private $solr_highlight;
	private $highlight_context;
	private $solr_facet;
	private $arr_facet;
	private $start_offset;
	
	protected $database;
	
	/*function __construct ()
	{
	}*/

	public static function getInstance()
	{	
		// MS - si l'instance PageSolr n'existe pas, mais qu'on a une instance Page, 
		// on utilise un hack pour caster l'instance parente page comme instance pour pageSolr
		try{
			$parent_instance = parent::getInstance() ; 
		}catch(Exception $e){
			$parent_instance = null ; 
		}
		if(!isset(self::$instance) && !empty($parent_instance )){
			$c = __CLASS__;
			self::$instance = new $c;
			$cast_serialize_parent = str_replace('O:4:"Page"','O:8:"PageSolr"',serialize(parent::getInstance()));
			self::$instance = unserialize($cast_serialize_parent);
		}else if (!isset(self::$instance))
		{
			$c = __CLASS__;
			self::$instance = new $c;
		}
		
		// MS - 13/05/14 report du max execution time de solr dans le max_execution_time de php si le timeout php d�fini est inf�rieur
		if(defined("kSolrOptions")){
			$solr_opts = unserialize(kSolrOptions);
			if(isset($solr_opts['timeout']) && !empty($solr_opts['timeout']) && ini_get('max_execution_time')< $solr_opts['timeout'] && ini_get('max_execution_time')!='0'){
				ini_set('max_execution_time',$solr_opts['timeout']);
			}
		}
				
		return self::$instance;
	}
	
	public function setDatabase($db)
	{
		$this->database=$db;
	}
	
    function initPager($requete_solr, $new_max = 10,$altVar=null,$addGETVars=null,$secstocache=0,$highlight=true)
    {
		$deftNbLignes=defined("gNbLignesDocListeDefaut")?gNbLignesDocListeDefaut:10;
		// MS 01.06.15 - ajout getPageFromUrl (comme dans initPager de la class_page)
		$this->getPageFromURL($altVar);
		
		if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) )
			$this->page=1;
		
		// nb de resultat gere par solrQuery
		if (!isset($this->nbLignes))
			$this->max_rows = $new_max;
		else
			$this->max_rows=$this->nbLignes;
		
		// array contenant le contenu d'une requete solr
		
		if ($this->max_rows!="all")
		{
			if(isset($this->start_offset) ){
				$requete_solr->setStart(($this->start_offset));
			}else{
				$requete_solr->setStart(($this->page-1)*$this->max_rows);
			}
			$requete_solr->setRows($this->max_rows);
			
			// limites sur les termes
			// MS - 16.12.16 - Restauration code supprim�, initpager peut effectivement �tre utilis� avec une requete solr de recherche sur les termscomponents. Ajout d'un test v�rifiant si la requete_solr est bien relative � des terms solr
			if($requete_solr->getTerms()){
				$start_term=($this->page-1)*$this->max_rows;
				$requete_solr->setTermsLimit($this->page*$this->max_rows);
			}
		}	
		
		$solr_opts=unserialize(kSolrOptions);
		
		if (!empty($this->database))
		{
			$solr_opts['path']=$this->database;
		}
        if (!empty($_SESSION['DB']))
        {
            $solr_opts['path']=$_SESSION['DB'];
        }
		
		$this->solr_client=new SolrClient($solr_opts);
		
		
		if($requete_solr->getHighlight() === false){
			$highlight = false ; 
		}
		
		//trace("\n\nrequete_solr : ".$requete_solr."\n\n");
		// MS - 24/11/14 - ajout handling exception solr, affichage "problem with request" lorsque le getQuery fail... on ne plante plus l'int�gralit� du site...
		try{
			//trace("solrquery : \n=======\n".str_replace('&',"\n&",(string)$requete_solr)."\n=======");
			$response=$this->solr_client->query($requete_solr);
		}catch(Exception $e){
			$this->rows=0;
			$this->found_rows=0;
			$this->page=1;
			$this->PagerLink="";
			$this->result=array();
			$this->error_msg="Problem with request.";
			trace("solr query crash : ".$e->getMessage());
			return false;
		}
		
		
		if ($highlight==true && $requete_solr->getHighlight()===true)
			$this->solr_highlight=$response->getResponse()->highlighting;
		
		if ($requete_solr->getFacet()===true)
			$this->solr_facet=$response->getResponse()->facet_counts;
	
		// dans le cas d'un requete 
		$this->found_rows=$response->getResponse()->response->numFound;
		
		$this->result=array();
		if (!empty($response->getResponse()->response->docs))
		{
			foreach($response->getResponse()->response->docs as $doc)
			{
				$arr_doc=array();
				
//				if($highlight)
//					$highlight_doc=$response->getResponse()->highlighting->offsetGet($doc->id);
				
				foreach ($doc->getPropertyNames() as $property)
				{
//					if ($highlight==true && $requete_solr->getHighlight()===true && ($highlight_doc->offsetExists($property) || $highlight_doc->offsetExists("hl_".$property) ))
//					{
//						// remplacement pour highlighting
//						// if (is_array($doc->offsetGet($property)))
//							// $texte_final=implode(',',$doc->offsetGet($property));
//						// else
//							// $texte_final=$doc->offsetGet($property);
//						
//						// if($highlight_doc->offsetExists("hl_".$property) ){
//							// foreach ($highlight_doc->offsetGet("hl_".$property) as $txt_highlight)
//							// {
//								// $txt_highlight=str_replace(array('<highlight>','</highlight>'),array('<span class="highlight1">','</span>'),$txt_highlight);
//								// $texte_final=str_replace(strip_tags($txt_highlight),$txt_highlight,$texte_final);
//							// }
//						// }else{
//							// foreach ($highlight_doc->offsetGet($property) as $txt_highlight)
//							// {
//								// $txt_highlight=str_replace(array('<highlight>','</highlight>'),array('<span class="highlight1">','</span>'),$txt_highlight);
//								// $texte_final=str_replace(strip_tags($txt_highlight),$txt_highlight,$texte_final);
//							// }
//						// }
//						if(is_array($doc->offsetGet($property))){
//							$texte_final=implode(',',$doc->offsetGet($property));
//						}else{
//							$texte_final=$doc->offsetGet($property);
//						}
//						$arr_doc[strtoupper(trim($property))]=$texte_final;
//					}
//					else
//					{
//                    if (is_array($doc->offsetGet($property)))
//                        // $arr_doc[strtoupper(trim($property))]=implode(',',$doc->offsetGet($property));
//                        $arr_doc[strtoupper($this->removeLangFieldVersionDependant(trim($property)))]=implode(',',$doc->offsetGet($property));
//                    else
//                        // $arr_doc[strtoupper(trim($property))]=$doc->offsetGet($property);
//                        $arr_doc[strtoupper($this->removeLangFieldVersionDependant(trim($property)))]=$doc->offsetGet($property);
//					}
                    if (is_array($doc->offsetGet($property)))
                        // $arr_doc[strtoupper(trim($property))]=implode(',',$doc->offsetGet($property));
                        $arr_doc[strtoupper($this->removeLangFieldVersionDependant(trim($property)))]=implode(',',$doc->offsetGet($property));
                    else
                        // $arr_doc[strtoupper(trim($property))]=$doc->offsetGet($property);
                        $arr_doc[strtoupper($this->removeLangFieldVersionDependant(trim($property)))]=$doc->offsetGet($property);
				}
				
				$this->result[]=$arr_doc;
			}
		}
		else if (!empty($response->getResponse()->terms))
		{
			$nb_terms=0;
			foreach($response->getResponse()->terms as $field)
			{
				// MS - 16.12.16 - si on ne trouve pas de r�sultats � la page indiqu�e, on repart depuis la premi�re page, et on reset donc start_term
				if($requete_solr->getTerms() && $this->max_rows != 'all' && $this->page > 1 && count($field->getPropertyNames()) < ($this->page-1) * $this->max_rows){
					$start_term = 0 ; 
					$this->page = 1 ; 
				}
				foreach ($field->getPropertyNames() as $valeur)
				{
					if ($nb_terms>=$start_term)
					{
						$valeur=trim($valeur);
						$arr_term=array();
						$arr_term['VALEUR']=$valeur;
						$arr_term['NB']=$field->offsetGet($valeur);
						
						$this->result[]=$arr_term;
					}
					$nb_terms++;
				}
				// VP 19/6/2018 : on ne peut pas connaitre le nombre de page total sauf pour la derni�re page car on a moins de r�sultats que pr�vu
				if($this->max_rows != 'all' && count($this->result)<$this->max_rows){
					$this->found_rows = count($field->getPropertyNames());
				}
			}
		}
		
		// facettes
		$this->arr_facet=array();
		
		if (!empty($this->solr_facet->facet_fields))
		{
			foreach ($this->solr_facet->facet_fields->getPropertyNames() as $fields)
			{
				$fields=trim($fields);
				foreach ($this->solr_facet->facet_fields->offsetGet($fields) as $key=>$valeur) {
					$key=trim($key);
					$valeur=trim($valeur);
					
					$arr_facet_tmp=array();
					
					$arr_facet_tmp['field']=$fields;
					$arr_facet_tmp['key']=$key;
					$arr_facet_tmp['valeur']=$valeur;
					
					if(strpos($arr_facet_tmp['field'],'-ref') !== false){
						$arr_facet_tmp['field'] = substr($arr_facet_tmp['field'],0,strpos($arr_facet_tmp['field'],'-ref'));
						$found = false ; 
						foreach($this->arr_facet as $facet){
							if($facet['field'] ==  $arr_facet_tmp['field'] && $facet['key'] == $arr_facet_tmp['key']){
								$found = true ; 
								break ; 
							}
						}
						if(!$found){
							$this->arr_facet[]=$arr_facet_tmp;
						}
					}else{
						$this->arr_facet[]=$arr_facet_tmp;
					}
				}
			}
		}
		
		if (!empty($this->solr_facet->facet_dates))
		{
			foreach ($this->solr_facet->facet_dates->getPropertyNames() as $fields)
			{
				$fields=trim($fields);
				foreach ($this->solr_facet->facet_dates->offsetGet(trim($fields))->getPropertyNames() as $key)
				{
					$key=trim($key);
					if ($key!='start' && $key!='end' && $key!='gap')
					{
						$valeur=$this->solr_facet->facet_dates->offsetGet(trim($fields))->offsetGet($key);
						
						$arr_facet_tmp=array();
						
						$arr_facet_tmp['field']=$fields;
						$arr_facet_tmp['key']=$key;
						$arr_facet_tmp['valeur']=$valeur;
						
						$gap=str_replace('+','',$this->solr_facet->facet_dates->offsetGet(trim($fields))->offsetGet('gap')); //+xYEAR
						$gap=str_replace('YEAR','',$gap);
						$annee=explode('-',$key);
						$annee=$annee[0]+$gap-1;
						$arr_facet_tmp['key2']=$annee.'-12-31T23:59:59Z';
						$arr_facet_tmp['gap']=$this->solr_facet->facet_dates->offsetGet(trim($fields))->offsetGet('gap');

						$this->arr_facet[]=$arr_facet_tmp;
					}
				}
			}
		}
		if (!empty($this->solr_facet->facet_ranges))
		{ 
			foreach ($this->solr_facet->facet_ranges->getPropertyNames() as $fields)
			{
				$fields=trim($fields);
				foreach ($this->solr_facet->facet_ranges->offsetGet(trim($fields))->getPropertyNames() as $key)
				{
					foreach ($this->solr_facet->facet_ranges->offsetGet(trim($fields))->offsetGet('counts') as $key_range=>$range)
					{

						$key=trim($key);
						if ($key!='start' && $key!='end' && $key!='gap')
						{
							try{
							$valeur = $range;
							$arr_facet_tmp=array();
							
							$arr_facet_tmp['field']=$fields;
							$arr_facet_tmp['key']=$key_range;
							$arr_facet_tmp['valeur']=$valeur;
							$arr_facet_tmp['gap']=$this->solr_facet->facet_ranges->offsetGet(trim($fields))->offsetGet('gap');
							$arr_facet_tmp['key2']=(string)((int)$key_range+(int)$arr_facet_tmp['gap']);
							
							$this->arr_facet[]=$arr_facet_tmp;
							}catch(Exception $e){
								trace("class_pageSolr: crash r�cup�ration des facet_ranges :".$e);
							}
						}
					}
				}
			}
		}
		//$this->result=$response->getResponse()->response->docs;
		
		// contextes de highlight
		$this->highlight_context=array();
		if ($highlight==true && $requete_solr->getHighlight()===true)
		{
			foreach ($this->solr_highlight as $id=>$highlight)
			{
				$tmp_arr_ctx=array();
				
				$tmp_arr_ctx['id']=$id;
				foreach ($highlight->getPropertyNames() as $fields)
				{
					//$tmp_arr_ctx[trim($fields)]=$highlight->offsetGet($fields);
					$txt_highlight='';
					
					foreach ($highlight->offsetGet($fields) as $txt_highlight)
						$txt_highlight=str_replace(array('<highlight>','</highlight>'),array('<span class="highlight1">','</span>'),$txt_highlight);
					
					$tmp_arr_ctx[trim($fields)]=$txt_highlight;
				}
				
				$this->highlight_context[]=$tmp_arr_ctx;
				
			}
			
			
		}
		
		$this->params4XSL['nb_pages']=$this->num_pages();
        $this->params4XSL['nb_rows']=$this->found_rows;
		
		return true;
    }
	
	function getPageSolrHighlight()
	{
		return $this->solr_highlight;
	}
	
	function getArrayHighlightContext()
	{
		return $this->highlight_context;
	}
	
	function getPageSolrFacet()
	{
		return $this->solr_facet;
	}
	
	
	function getArrayFacets()
	{
		return $this->arr_facet;
	}
	
	function setSolrOffset($n){
		if(isset($n) && !empty($n) && is_numeric($n))
			$this->start_offset = $n;
	}
	
	/*function initPagerFromArray($arr,$found_rows=null,$ignore_page = null,$addGETVars=null)
	{
	}


	function getSortFromSession($order_session)
	{
	}*/
	
	
	// MS 26/08/14 
	// Fonction de merge des SolrObjects (utilis� pour l'isntant uniquement pour merge les objets highlight solr )
	function mergeSolrObjects($obj1,$obj2){
		if(isset($obj1) && !empty($obj1) && isset($obj2) && !empty($obj2)){
			foreach($obj2 as $idx=>$val){
				if(!is_array($obj1->offsetGet($idx))){
					$obj1->offsetSet($idx,$val);
				}else{
				}
			}
			return $obj1;
		}else if(isset($obj1) && !empty($obj1) && (!isset($obj2) || empty($obj2)) ){
			return $obj1;
		}else{
			return false ; 
		}
	}
	
	function getSort($withDefault=true,&$default=true,$default_field='')
	{
		global $db;
        $tri=$this->getSortFromURL();

		if($tri!=""){ //Calcul de la colonne de tri, cette valeur sera envoy�e � la XSL pour afficher les fl�ches
			$default=false;
			$ordre=substr($tri,-1,1);
			$col = substr($tri,0,-1);
			if (!is_numeric($ordre)) {
				$ordre=0;
				$this->order=$tri;
				$col=$tri;
			}
			if ($ordre==0) {
				$this->triInvert="1";
				$this->col=$col;
			}else{
				$this->triInvert="0";
				$this->col=$col;
			}

			// $this->initParams4XSL();

			//Deuxi�me partie, calcul du SQL de tri qui peut comporter plusieurs colonnes
			$_hasbrackets=preg_match("/^\[(.*)\](.*)$/i",$tri,$matches); //Pr�sence de crochet autour des champs => concat�nation
			if ($_hasbrackets) {
				$tri=$matches[1].$matches[2]; //on se d�barrasse des crochets (mais on garde le sens)
				$sqlTri=str_replace(';',',',$tri);
				if (substr($sqlTri,-1,1)=='1') {
					$_cols=strtoupper(substr($sqlTri,0,-1));
					$_sens=SolrQuery::ORDER_DESC; }
				elseif (substr($sqlTri,-1,1)=='0') {
					$_cols=strtoupper(substr($sqlTri,0,-1));
					$_sens=SolrQuery::ORDER_ASC; }
				elseif (!is_numeric(substr($sqlTri,-1,1))) {
					$_cols=strtoupper($sqlTri);
					$_sens=SolrQuery::ORDER_ASC;
				}
				if (!empty($_cols))
				{
					$return_sort=array();
					$arr_cols=explode(',',strtolower($_cols));
					
					foreach ($arr_cols as $col)
					{
						$return_sort[]=array($col,$_sens);
					}
					// MS ajout init valeur de $this->order � partir de la valeur de "$_sens"
					//	=> permet de correctement conserver les tris lors des changements de pages, ajout de filtre etc ... 
					if($_sens == SolrQuery::ORDER_ASC ){
						$this->order = $col."0" ; 
						$this->triInvert ="1";
					}else if ($_sens == SolrQuery::ORDER_DESC){
						$this->order = $col."1" ; 
						$this->triInvert ="0";
					}
					
					
					return $return_sort;
				}
			} else {
				$this->order = ""; 
			// VP 19/1/09 : ajout tri multiple
				$sqlTri=array();
				
				//preg_match_all("/(?:sum[(])[^)]+\)[01]|(query\([\S\s]\)|[^(,\s:]+/",$tri,$matches);
				//VG : correction problèmes de parenthèses + pattern query
				preg_match_all("/(?:sum[\(])[^)]+\)[01]|(query\([\S\s]+\)[01]{0,1})|[^(,\s:]+/",$tri,$matches);
				
				if(isset($matches[0]) && !empty($matches[0])){
					$fields=$matches[0];
				}else{
					$fields=explode(",",$tri);
				}
				// trace("getSort test : fields : ".print_r($fields,true)." matches : ".print_r($matches,true));
				foreach ($fields as $field) {
					$field=trim($field);
					$ordre=substr($field,-1,1);
					if (is_numeric($ordre)){
						if($ordre==1){$direction=SolrQuery::ORDER_DESC;}else{$direction=SolrQuery::ORDER_ASC;}
							$fld=substr($field,0,-1);
					}
					else {
						$direction=SolrQuery::ORDER_ASC;
						$fld=$field;
					}

					//update VG 16/02/12 : si il y a un nom de table dans le champ, celui-ci doit rester en minuscule
					//Gestion de la casse
					if(strpos($fld,'query') !== false) {
						// do nothing
					}else  if(strpos($fld,'.') === false) {
						$fld = strtolower($fld);
					} else {
						$aFld = explode(".",$fld);
						$fld = strtolower($aFld[0]).".".strtoupper($aFld[1]);
					}
					// MS ajout init valeur de $this->order � partir de la valeur de "$_sens"
					//	=> permet de correctement conserver les tris lors des changements de pages, ajout de filtre etc ... 
					if(!empty($this->order)){
						$this->order.=",";
					}
					if($direction == SolrQuery::ORDER_ASC ){
						$this->order .= $fld."0" ; 
					}else if ($direction == SolrQuery::ORDER_DESC){
						$this->order .= $fld."1" ; 
					}
					
					$sqlTri[]=array($fld,$direction);
				}


				if(!empty($sqlTri)) return $sqlTri;
			}
		} elseif ($withDefault) {
			if (isset($default_field) && !empty($default_field))
			{			
				$default_strorder=trim($this->addDeftOrder($default_field));
				$default_strorder=trim(str_replace('ORDER BY','',$default_strorder));
				
				$default_orders=explode(',',$default_strorder);
				$this->order = "" ; 
				$_SESSION['sqlorderbydeft'][$this->urlaction]=array();
				foreach ($default_orders as $dft_order) {
					$default_order=explode(' ',trim($dft_order));
					$champ = strtolower($default_order[0]);
					if (trim($default_order[1])=='DESC')
						$ordre=SolrQuery::ORDER_DESC;
					else
						$ordre=SolrQuery::ORDER_ASC;
					
					
					if (strstr($champ,'.')!=false)
					{
						$arr_champ=explode('.',$champ);
						$_SESSION['sqlorderbydeft'][$this->urlaction][]=array($arr_champ[1],$ordre);
						if(!isset($fld) || $fld !=$arr_champ[1]) {
							$fld = $arr_champ[1];
						}
					}
					else{
						$_SESSION['sqlorderbydeft'][$this->urlaction][]=array($champ,$ordre);
						if(!isset($fld) || $fld !=$arr_champ[1]) {
							$fld = $champ; 
						}
					}
					if(!empty($this->order)){
						$this->order.=",";
					}
					if($ordre == SolrQuery::ORDER_ASC ){
						$this->order .=  $fld."0" ; 
					}else if ($ordre == SolrQuery::ORDER_DESC){
						$this->order .=  $fld."1" ; 
					}
				}
				// MS ajout init valeur de $this->order � partir de la valeur de "$_sens"
				// => permet de correctement conserver les tris lors des changements de pages, ajout de filtre etc ... 
				
				
				
				//$_SESSION['sqlorderbydeft'][$this->urlaction]=$default_order;
			}//by ld 22/12/08: introduction de urlaction pour g�rer des ordres par deft diff�rents suivant la page
			//puisqu'on n'a pas l'objet recherche ici...
			$default=true;

			
			if (isset($_SESSION['sqlorderbydeft'][$this->urlaction]))
			return $_SESSION['sqlorderbydeft'][$this->urlaction];
		}
	}
	
	function addUrlParams($addParams=null,$encodeAmpersand=true) {
		$arrParams=$_GET;
		$str="";

		unset($arrParams[$this->prmAction]);
		unset($arrParams[$this->prmSort]);
		unset($arrParams[$this->prmPage]);
		if (is_array($addParams)) $arrParams+=$addParams;
		foreach ($arrParams as $k=>$v) {
			$str.=($encodeAmpersand==true?"&amp;":"&").$k."=".urlencode($v);
		}
		return $str;
	}
	
	function removeLangFieldVersionDependant($field){
		if(!isset($this->arrFieldsToSaveWithIdLang) ){
			require_once(modelDir.'model_doc.php');
			$tmpDoc=  new Doc() ; 
			$this->arrFieldsToSaveWithIdLang = $tmpDoc->getArrFieldsToSaveWithIdLang();
			$this->arrFieldsToSaveWithIdLang = array_map('strtolower',$this->arrFieldsToSaveWithIdLang);
			unset($tmpDoc);
		}
				
		if(preg_match('/_'.strtolower($_SESSION['langue']).'$/',$field) && in_array(str_replace('_'.strtolower($_SESSION['langue']),'',$field),$this->arrFieldsToSaveWithIdLang )){
			$field = str_replace('_'.strtolower($_SESSION['langue']),'',$field);
		}
		return $field ; 
	}
	
}
?>
