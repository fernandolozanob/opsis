<?

require_once(libDir."class_cherche.php");

class RechercheDoc extends Recherche {
	// VP 5/10/09 : prise en compte paramètre NOHIST dans les différents modes de recherche

    function __construct() {
	  	$this->entity='DOC';
     	$this->name=kDocument;
     	$this->prefix='t1';
     	$this->sessVar='recherche_DOC';
		$this->tab_recherche=array();
		$this->useSession=true;
		
		if (defined("gParamsRecherche")) $this->params=unserialize(gParamsRecherche);
	}


    /**
     * Initialise le SQL de recherche (DOC)
     *
     */
    function prepareSQL($most_viewed=false){
		$usr=User::getInstance();
        //by LD 30/10/08 ajout DOC_TITREORI et DOC_XML
		//VP 2/03/09 : ajout DOC_DATE
		//VP 9/07/09 : ajout DOC_ORI_FORMAT
		//VP 16/02/10 : ajout DOC_ID_ETAT_DOC
		//VG 06/08/10 : ajout DOC_DATE_PV_DEBUT
		//VG 16/02/12 : ajout DOC_DATE_CREATION
		//MS 02/05/12 : ajout DOC_NB_EPISODES
		//MS 16/01/13 : ajout DOC_CAT 
        $this->sql = "select t1.ID_DOC, t1.ID_LANG, REPLACE(t1.DOC_RES,'\r\n','') as DOC_RES, t1.DOC_ID_FONDS, t1.DOC_ID_TYPE_DOC, t1.DOC_NB_EPISODES, t1.DOC_ID_ETAT_DOC, t1.DOC_COTE,t1.DOC_ID_MEDIA,t1.DOC_NUM,t3.IM_CHEMIN, t3.IM_FICHIER";
        $this->sql .=",t4.DOC_TITRE as TITRE_PARENT,t1.DOC_TITRE, t1.DOC_TITRE_COL, t1.DOC_AUTRE_TITRE, t1.DOC_SOUSTITRE, t1.DOC_TITREORI, t1.DOC_ACCES, t1.DOC_ORI_FORMAT, t6.DA_FICHIER, t6.DA_CHEMIN";
        $this->sql .=",t1.DOC_DATE_DIFF, t1.DOC_DATE_PV_DEBUT, t1.DOC_DATE_PV_FIN, t1.DOC_DATE_CREA, t1.DOC_VAL, t1.DOC_XML,t5.FONDS_COL as FONDS, t5.FONDS as FONDS2, t2.TYPE_DOC, t1.DOC_DUREE, t1.DOC_LEX, t1.DOC_CAT,t1.DOC_DATE_PROD,t1.DOC_DATE,t1.DOC_COPYRIGHT";
        if(defined("gRechercheDocSelectFields") && (gRechercheDocSelectFields)){
			$this->sql.=','.gRechercheDocSelectFields;
		}
		
		
		if ($most_viewed==true)
			$this->sql.=',COUNT(t_action.ID_ACTION) AS NB_VIEW';
		
		$this->sql .=" from t_doc as t1 left join t_type_doc as t2 on t1.DOC_ID_TYPE_DOC=t2.ID_TYPE_DOC AND t2.ID_LANG='".$_SESSION["langue"]."'";
        $this->sql.=" left join t_image as t3 on t1.DOC_ID_IMAGE=t3.ID_IMAGE AND t3.ID_LANG='".$_SESSION["langue"]."'";
        $this->sql.=" left join t_doc_acc as t6 on (t1.DOC_ID_DOC_ACC=t6.ID_DOC_ACC AND t6.ID_LANG='".$_SESSION['langue']."' ) ";
        $this->sql.=" left join t_doc t4 on t1.DOC_ID_GEN=t4.ID_DOC AND t4.ID_LANG='".$_SESSION["langue"]."'";
        $this->sql.=" left join t_fonds t5 on (t1.DOC_ID_FONDS=t5.ID_FONDS AND t1.ID_LANG=t5.ID_LANG)";
		
		if ($most_viewed==true)
		{
			$this->sql.=' LEFT JOIN t_action ON t_action.ID_DOC=t1.ID_DOC AND t_action.ACT_TYPE=\'DOC\'';
			$this->sqlSuffixe=' GROUP BY t1.ID_DOC, t1.ID_LANG,t2.ID_TYPE_DOC, t2.ID_LANG,t3.ID_IMAGE, t3.ID_LANG, t6.ID_DOC_ACC, t6.ID_LANG, t4.ID_DOC, t4.ID_LANG, t5.ID_FONDS, t5.ID_LANG';
		}
        $prefix=(!is_null($this->prefix)?$this->prefix.".":"");
        $this->sqlWhere=" WHERE ".$prefix."ID_LANG='".$_SESSION["langue"]."' ";
        //by VP/LD 14/05/08 utilisation sqlWhere au lieu de sql, LD 26/05/08, utilisation var $prefix
		// Modification du critere de recherche sur DOC_ACCES
        // MS ajout possibilité de régler le seuil de type d'usager à partir duquel on ajoute le DOC_ACCES
		
		if((defined("gSrchDoc_UserTypeWithoutFullAccess") && $usr->getTypeLog() <= gSrchDoc_UserTypeWithoutFullAccess) || (!defined("gSrchDoc_UserTypeWithoutFullAccess") && $usr->getTypeLog() <= kLoggedNorm)) $this->sqlWhere.=" AND ".$prefix."DOC_ACCES>='1' ";
        // Historique
        $this->etape="";
    }


	protected function chercheTouteDate($tabCrit,$prefix) {
		global $db;
        $prefix=($prefix!=""?$prefix.".":"");
		$valeur_date1=convDate($tabCrit['VALEUR']);
        $valeur_date2=convDate($tabCrit['VALEUR2']);

         if(strlen($valeur_date1)>0) {
            if(strlen($valeur_date2)==0) {
                // VP 11/09/12 : changement OR en AND dans intervalle DOC_DATE_PV
	        	$this->sqlRecherche.= $tabCrit['OP']."(".$prefix."DOC_DATE_PROD='".$valeur_date1."' OR
										".$prefix."DOC_DATE_DIFF='".$valeur_date1."' OR
										".$prefix."DOC_DATE_PV_DEBUT='".$valeur_date1."' OR
										((".$prefix."DOC_DATE_PV_DEBUT <='".$valeur_date1."' AND ".$prefix."DOC_DATE_PV_FIN>='".$valeur_date1."') AND ".$prefix."DOC_DATE_PV_DEBUT != '0000-00-00')) ";
				if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".kRechercheTouteDate." : ".$valeur_date1."  ";
			}else {
                // VP 7/11/12 : ajout parentèses englobantes
				$this->sqlRecherche.= $tabCrit['OP']." ((".$prefix."DOC_DATE_PROD>='".$valeur_date1."' AND ".$prefix."DOC_DATE_PROD<='".$valeur_date2."') OR (".$prefix."DOC_DATE_DIFF>='".$valeur_date1."' AND ".$prefix."DOC_DATE_DIFF<='".$valeur_date2."' ) OR (t1.DOC_DATE_PV_DEBUT>='".$valeur_date1."' AND t1.DOC_DATE_PV_DEBUT<='".$valeur_date2."'))";
				if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".kRechercheToutesDates." : ".$valeur_date1." ".$valeur_date2."  ";
		    }
        }
	}
        
        
    protected function chercheTouteDateRedif($tabCrit,$prefix=""){
        global $db;
        $prefix=($prefix!=""?$prefix.".":"");
            
        $sql="";
            
        foreach ( explode(',',$tabCrit['FIELD']) as $i=>$myField) {
           if (preg_match('/^doc/i',$myField)){
               $listFieldsDoc[]=$myField;
           }else if (preg_match('/^redif/i',$myField)){
               $listFieldsRedif[]=$myField;
           }
        }      
            
        $valeur_date1=convDate($tabCrit['VALEUR']);
        $valeur_date2=convDate($tabCrit['VALEUR2']);
        
        if (empty($valeur_date1))
            return;
        
        if (count($listFieldsRedif)){
            foreach ($listFieldsRedif as $i=>$myFieldRedif) {

                $highlight[$tabCrit['FIELD']]=array($valeur_date1);
                if($i>0)
                    $sql.=" OR ";
                
                if(strlen($valeur_date2)==0){
                    $sql.=" ".$prefix."ID_DOC IN (SELECT distinct ID_DOC from t_doc_redif WHERE ".$myFieldRedif." like ".$db->Quote($valeur_date1.'%').")";
                } else {
                    $sql.=" ".$prefix."ID_DOC IN (SELECT distinct ID_DOC from t_doc_redif WHERE ".$myFieldRedif." >= ".$valeur_date1." AND ".$myFieldRedif." <= '".$valeur_date2.")";
                }

                if(empty($tabCrit['NOHIGHLIGHT']))
                    $this->tabHighlight=$this->tabHighlight+$highlight;
            }
        }
          
        if (count($listFieldsRedif) && count($listFieldsDoc)){
            $sql.=" OR ";
        }

        if (count($listFieldsDoc)){
            foreach ($listFieldsDoc as $i=>$myFieldDoc) {

                $highlight[$tabCrit['FIELD']]=array($valeur_date1);
                if($i>0)
                    $sql.=" OR ";
                if(strlen($valeur_date2)==0){
                    $sql.=" ".$prefix.$myFieldDoc." like ".$db->Quote($valeur_date1.'%');
                } else {
                    $sql.=" (".$prefix.$myFieldDoc." >= ".$db->Quote($valeur_date1)." AND ".$prefix.$myFieldDoc." <= ".$db->Quote($valeur_date2).")";
                }

                if(empty($tabCrit['NOHIGHLIGHT']))
                    $this->tabHighlight=$this->tabHighlight+$highlight;
            }
        }

        if(empty($tabCrit['NOHIST'])){
            if(strlen($valeur_date2)==0)
                $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".kRechercheToutesDiffusions." : ".$valeur_date1."  ";
            else
                $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".kRechercheToutesDiffusions." : ".$valeur_date1." ".$valeur_date2."  ";
        }

        if(!empty($sql)){
            $this->sqlRecherche.=" ".$tabCrit['OP']." (".$sql.") ";
        }
        
   }
        
        
        
        

	/** Recherche sur les doc acc liés
     * 	IN : tabCrit, prefix
     * 	OUT : sql
     * 	NOTE : pas de highlight !
     */
	// VP 26/05/13 : création fonction chercheDocAcc
	protected function chercheDocAcc($tabCrit,$prefix) {
		global $db;
        
		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN
        (SELECT ID_DOC from t_doc_acc WHERE  ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kDocumentAcc;
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
	}
    
    /** Recherche sur les doc acc liés
     * 	IN : tabCrit, prefix
     * 	OUT : sql
     * 	NOTE : pas de highlight !
     */
	// VP 26/05/13 : création fonction chercheDocAcc
	protected function chercheDocAccFT($tabCrit,$prefix) {
		global $db;
        
		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN (SELECT ID_DOC from t_doc_acc WHERE ".$db->getFullTextSQL(array($tabCrit['VALEUR']), array($tabCrit['FIELD']));
        
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kDocumentAcc;
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
	}
    
    
/** Recherche dans les fonds et sous-fonds en se basant sur un ou plusieurs ID
 *   La recherche de fonds est systématiquement étendue aux sous-fonds
 *  IN : tabCrit, prefix, ext_children
 *  OUT : sql + tableau highlight
 *  NOTE : structure de tabCrit
 * 		FIELD => champ à afficher, FONDS ou FONDS_COL (FONDS par déft)
 * 		VALEUR => un ou plusieurs ID (array)
 * 		VALEUR2 => intitulé de la recherche : ce champ est renseigné par chercheFonds et contient la recherche textuelle originale
 *		ext_children => flag d'extension de la recherche aux fonds fils
 *  NOTE2 : on passe dans cette recherche après une recherche en chercheFonds
 */
	protected function chercheFondsId($tabCrit,$prefix,$ext_children=true) {
		global $db;
		if (is_array($tabCrit['VALEUR'])) $myVals=implode(",",$tabCrit['VALEUR']);
		else $myVals=$tabCrit['VALEUR'];
		if (empty($myVals)) $myVals='0';
		
		if($ext_children){
			// MS - correction de la récupération des fonds fils pour permettre la gestion d'une hierarchie de fonds sur plus de 2 niveaux
			$tmp_arr = explode(',',$myVals);
			$tmp_id_fond = $myVals;
			$tmp_ids = $db->GetCol('SELECT t_fonds.ID_FONDS from t_fonds where ID_FONDS_GEN in ('.$tmp_id_fond.') GROUP BY ID_FONDS');
			
			while(!empty($tmp_ids)){
				foreach($tmp_ids as $id){
					// ajout des ids récupérés à la valeur myVals avec controle de doublons
					if(!in_array($id,$tmp_arr)){
						array_push($tmp_arr,$id);
						$myVals.=",".$id;
					}
				}
				$tmp_id_fond = implode(',',$tmp_ids);
				$tmp_ids = $db->GetCol('SELECT t_fonds.ID_FONDS from t_fonds where ID_FONDS_GEN in ('.$tmp_id_fond.') GROUP BY ID_FONDS');
			}
			unset($tmp_arr);
			unset($tmp_ids);
			unset($tmp_id_fond);
		}
		$prefix=(!is_null($prefix)?$prefix.".":"");
		//Création requete SQL
		$this->sqlRecherche.= $tabCrit['OP']." ".$prefix."DOC_ID_FONDS IN (".$myVals.") ";

		$fld=($tabCrit['FIELD']!=''?$tabCrit['FIELD']:'FONDS');
		//Récupération des intitulés si on n'a pas de VALEUR2
		if (empty($tabCrit['VALEUR2'])) {
			// MS - 18.06.15 - ajout prise en compte de la langue lors de l'alimentation du texte de l'"étape", évite de récupérer les diverses versions du nom du FONDS quand on recherche par id
			$sql3="SELECT DISTINCT ID_FONDS,".$fld." from t_fonds WHERE ID_FONDS IN(".$myVals.") and ID_LANG='".$_SESSION['langue']."'";
			$libs=$db->GetAll($sql3);
			foreach ($libs as $lib) {
				$highlight[]=$lib[$fld]; //liste des fonds pour le highlight
				// Le libellé correspond à une valeur d'origine (hors extension)
				// MS 26/06/2012 : update du test depuis : if(in_array($lib['ID_FONDS'],(array)$tabCrit['VALEUR']))
				if(in_array(intval($lib['ID_FONDS']),explode(',',$tabCrit['VALEUR']))) $orgLibs[]=$lib[$fld];
			}	
		} else { //On a déjà un intitulé ou une valeur dans VALEUR2 => c notamment le cas quand on est passé par chercheFonds
			$orgLibs=(array)$tabCrit['VALEUR2'];
			$highlight=$orgLibs;
		}
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kFonds;
		if (empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$orgLibs)." ";
		if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=array_merge($this->tabHighlight, $highlight);
	}

/** Recherche textuelle sur des fonds (typiquement depuis rech experte et/ou avancée)
 * 	IN : tabCrit, prefix , ext_children
 * 	OUT : redirection vers chercheFondsId pour terminer la recherche
 * 	NOTE : structure de tabCrit
 * 		FIELD => champ à afficher (FONDS ou FONDS_COL, FONDS par déft)
 * 		VALEUR => noms de fonds qui sera analysé
 *		ext_children => flag d'extension de la recherche aux fonds fils		
 *  NOTE2 : cette recherche ne ramène rien en soi, elle prépare la recherche par chercheFondsId
 * 	 en allant simplement chercher les ID correspondant à la valeur saisie
 */
	protected function chercheFonds($tabCrit,$prefix,$ext_children=true) {
		global $db;
		$fld=($tabCrit['FIELD']!=''?$tabCrit['FIELD']:'FONDS');
		//Récupération des ID_FONDS par analyse "intelligente" du contenu du champ
		$sql2="SELECT ID_FONDS from t_fonds WHERE ".$fld.$this->sql_op($tabCrit['VALEUR'],true);
		$ids=$db->GetAll($sql2);
	
		foreach ($ids as $id) $arrIDs[]=$id['ID_FONDS'];
		//redirection vers recherche par ID
		$tabCrit['VALEUR2']=$tabCrit['VALEUR'];
		$tabCrit['VALEUR']=$arrIDs;
		$this->chercheFondsId($tabCrit,$prefix,$ext_children); //appel de la recherche par ID
	}

/** Recherche dans les festivals en se basant sur un champ
 *   La recherche de fonds est systématiquement étendue aux sous-fonds
 *  IN : tabCrit, prefix
 *  OUT : sql + tableau highlight
 *  NOTE : structure de tabCrit
 * 		FIELD => champ à chercher (FEST_DATE,FEST_LIBELLE etc) FEST_LIBELLE par défaut
 * 		VALEUR => une valeur (textuelle)
 * 		VALEUR2 =>
 *
 */
	protected function chercheFestival($tabCrit,$prefix) {
		//by ld 171108 créa function
		global $db;
		if (is_array($tabCrit['VALEUR'])) $myVals=implode(",",$tabCrit['VALEUR']);
		else $myVals=$tabCrit['VALEUR'];
		if (empty($myVals)) return false;

		$prefix=(!is_null($prefix)?$prefix.".":"");
		//Création requete SQL
		$fld=($tabCrit['FIELD']!=''?$tabCrit['FIELD']:'FEST_LIBELLE');
		$this->sqlRecherche.= $tabCrit['OP']." ".$prefix."ID_DOC IN (SELECT df.ID_DOC from t_festival f,t_doc_fest df
				 WHERE f.ID_ID_FEST=df.ID_FEST and f.".$fld." like '%".$tabCrit['VALEUR']."%'
				 AND ID_LANG=".$_SESSION['langue'];

		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kFestival;
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
		if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight[$tabCrit['FIELD']]=$tabCrit['VALEUR'];
	}

/** Recherche sur les matériels liés (doc_mat)
 * 	IN : tabCrit, prefix
 * 	OUT : sql, highlight
 * 	NOTE : structure de tabCrit
 * 		VALEUR => chaine faite des ID_MAT
 * 	NOTE : pas de highlight pour les ID_MAT !
 */
	// VP 27/07/09 : fonction chercheMat renommée en chercheDocMat
	protected function chercheDocMat($tabCrit,$prefix) {
		global $db;

		$prefix=(!is_null($prefix)?$prefix.".":"");
		//by LD 26/6/8 : possté utilisation d'un autre champ via FIELD
		//by LD 26/6/8 : réintroduction du keepIntact=true
		$this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN
					(SELECT ID_DOC from t_doc_mat WHERE  ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kMateriel;
		//$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
	}

	/** Recherche sur les matériels liés
	* 	IN : tabCrit, prefix
	* 	OUT : sql, highlight
	* 	NOTE : structure de tabCrit
	* 		VALEUR => chaine faite des ID_MAT
	* 	NOTE : pas de highlight pour les ID_MAT !
	*/
	// VP 27/07/09 : création fonction chercheMat
	protected function chercheMat($tabCrit,$prefix) {
		global $db;
		
		$prefix=(!is_null($prefix)?$prefix.".":"");
		// MS 28-11-13 ajout possibilité de rechercher par fulltext sur un champ du matériel (en vue de recherhe sur les infos récupérés par OCR pour les documents )
		if($tabCrit['TYPE'] == 'FTMAT'){
			$arrWords = explode(" ",$tabCrit['VALEUR']);
			$this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN (select distinct ID_DOC from t_doc_mat,t_mat where t_mat.ID_MAT=t_doc_mat.ID_MAT AND ".$db->getFullTextSQL($arrWords, array($tabCrit['FIELD'])).") ";
		}else{
			$this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN (select distinct ID_DOC from t_doc_mat,t_mat where t_mat.ID_MAT=t_doc_mat.ID_MAT
				and t_mat.".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		}
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kMateriel;
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
	}

	//XB:  recherche sur les docs_mat_val
	protected function chercheDocMatVal($tabCrit,$prefix) {
		global $db;
		
		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN (select distinct ID_DOC from t_doc_mat,t_doc_mat_val,t_val where t_val.ID_VAL=t_doc_mat_val.ID_VAL AND  t_doc_mat_val.ID_DOC_MAT=t_doc_mat.ID_DOC_MAT and t_doc_mat_val.ID_TYPE_VAL='".$tabCrit['FIELD']."' AND t_val.VALEUR ".$this->sql_op($tabCrit['VALEUR'],true).")";
		
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kMateriel;
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
	}

	/** Recherche sur les paniers
	* 	IN : tabCrit, prefix
	* 	OUT : sql
	* 	FIELD => champ de t_doc
	* 	VALEUR => valeur recherchée
	* 	NOTE : pas de highlight
	*/
	protected function cherchePanier($tabCrit,$prefix) {
		global $db;

		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche.=$tabCrit['OP']." ".$prefix."ID_DOC IN (SELECT distinct ID_DOC from t_panier_doc WHERE ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true).")";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kPanier;
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR2']." ";
	}
	
	protected function chercheRightVal($tabCrit,$prefix="",$keepIntact=true){

            global $db;
            $prefix=($prefix!=""?$prefix.".":"");
            $this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC ";

			$this->sqlRecherche.= " in (select ID_DOC from t_panier_doc where ID_PANIER ";
			$this->sqlRecherche.= " in (select ID_PANIER from t_right where ID_RIGHT ";
			$this->sqlRecherche.= " in (select ID_RIGHT from t_right_val where ID_TYPE_VAL=".$db->Quote($tabCrit['FIELD'])." AND ID_VAL ";
            $this->sqlRecherche.= " in (select ID_VAL from t_val where VALEUR ".$this->sql_op($tabCrit['VALEUR'],$keepIntact)."))))";
            if (!$tabCrit['LIB']) $tabCrit['LIB']=GetRefValue('t_type_val',$tabCrit['FIELD'],$_SESSION['langue']);

        	$highlight["VALEUR"]=array(str_replace(array(' +',' -','"',','),' ',$tabCrit['VALEUR']));
        	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;

            if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
    }

	protected function chercheRightDate($tabCrit,$prefix="",$keepIntact=true){
        
		global $db;
		$prefix=($prefix!=""?$prefix.".":"");
		$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC ";
        
		$this->sqlRecherche.= " in (select ID_DOC from t_panier_doc where ID_PANIER ";
		$this->sqlRecherche.= " in (select ID_PANIER from t_right r1 where 1=1 ";
		$prefix = 'r1.';
		
		if (!is_array($tabCrit['VALEUR'])) $tabCrit['VALEUR']=(array)$tabCrit['VALEUR'];
        
       	if (empty($tabCrit['VALEUR'])) return;
        
		$sql="";
       	foreach ($tabCrit['VALEUR'] as $myDate) {
			$myDate=str_replace('*','',$myDate);
	        $valeur_date1=convDate($myDate);
	        $valeur_date2=convDate($tabCrit['VALEUR2']);
			if (empty($valeur_date1)) break;
            
			$arrFldDate=explode(',',$tabCrit['FIELD']);
            
			if(strlen($valeur_date2)>0) {
				//VP 18/04/13 : ajout fonction convToFirstDate
				$valeur_date1 = convToFirstDate($valeur_date1);
				$valeur_date2 = convToLastDate($valeur_date2);
				foreach ($arrFldDate as $i=>$date) { //boucle sur les champs
					if($i>0) $sql.=" OR ";
					$sql.="(".$prefix.$date." between ".$db->Quote($valeur_date1)." AND ".$db->Quote($valeur_date2).")";
				}
                
				if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".kEntre." (".$valeur_date1." ".kEt." ".$valeur_date2.")  ";
			}
			else{
                
				foreach ($arrFldDate as $i=>$date) {
					$highlight[$date]=array($valeur_date1);
					if($i>0) $sql.=" OR ";
					$sql.="(".$prefix.$date." like '".$valeur_date1."%')";
				}
                
				if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$myDate." ";
				if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
			}
	    }
		if(!empty($sql)){
			$this->sqlRecherche.=" ".$tabCrit['OP']." (".$sql.") ))";
		}
    }
    
    protected function compareRightDate($tabCrit,$prefix="") {
   		global $db;
   		
   		$prefix=($prefix!=""?$prefix.".":"");
   		$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC ";
		$this->sqlRecherche.= " in (select ID_DOC from t_panier_doc where ID_PANIER ";
		$this->sqlRecherche.= " in (select ID_PANIER from t_right r1 where 1=1 ";
		$prefix = 'r1.';
   		
   		$date=convDate($tabCrit['VALEUR']);
        
   		if ($date=="") return false;
   		$sens=$tabCrit['VALEUR2'];
        
   		if (!in_array($sens,array(">","<",">=","<="))) return false;
        
   		$sql='';
        
   		$arrFldDate=explode(',',$tabCrit['FIELD']);
		foreach ($arrFldDate as $i=>$dateField) {
			if($i>0) $sql.=" OR ";
			$sql.="(".$prefix.$dateField.$sens.$db->Quote($date).")";
		}
   		$this->sqlRecherche.=" AND (".$sql."))) ";
   		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".$sens." ".$tabCrit['VALEUR']." ";
    }

	protected function chercheIdCategorie($tabCrit,$prefix="",$keepIntact=true)
	{
		global $db;
		
		$prefix=($prefix!=""?$prefix.".":"");
		$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC ";
		$this->sqlRecherche.= " IN (SELECT ID_DOC FROM t_doc_cat WHERE ID_CAT='".$tabCrit['VALEUR']."')";
		
		$highlight["VALEUR"]=array(str_replace(array(' +',' -','"',','),' ',$tabCrit['VALEUR']));
		if (empty($tabCrit['NOHIGHLIGHT']))
			$this->tabHighlight=$this->tabHighlight+$highlight;
		

		if(empty($tabCrit['NOHIST']))
		{
			$result=$db->Execute('SELECT CAT_NOM FROM t_categorie WHERE ID_CAT='.intval($tabCrit['VALEUR']).' AND ID_LANG=\''.$_SESSION['langue'].'\'')->GetRows();
			$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$result[0]['CAT_NOM']." ";
		}
	}
	
	
	protected function chercheCategorie($tabCrit,$prefix="",$keepIntact=true)
	{
		global $db;
		
		$prefix=($prefix!=""?$prefix.".":"");
		$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC ";
		$this->sqlRecherche.= " IN (SELECT ID_DOC FROM t_doc_cat left join t_categorie on t_doc_cat.id_cat=t_categorie.id_cat WHERE ".$tabCrit['FIELD'].$this->sql_op($tabCrit['VALEUR']).")";
		
		$highlight["VALEUR"]=array(str_replace(array(' +',' -','"',','),' ',$tabCrit['VALEUR']));
		if (empty($tabCrit['NOHIGHLIGHT']))
			$this->tabHighlight=$this->tabHighlight+$highlight;
		

		if(empty($tabCrit['NOHIST']))
		{
			$result=$db->Execute('SELECT CAT_NOM FROM t_categorie WHERE ID_CAT='.intval($tabCrit['VALEUR']).' AND ID_LANG=\''.$_SESSION['langue'].'\'')->GetRows();
			$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$result[0]['CAT_NOM']." ";
		}
	}

	protected function chercheRediffusion($tabCrit,$prefix=null,$keepIntact=false){

        global $db;
		$prefix=(!is_null($prefix)?$prefix.".":"");
			
		$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC IN (SELECT distinct ID_DOC from t_doc_redif WHERE ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],$keepIntact).")";
			
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
		$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);

		if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
    }
	
	protected function cherchePublication($tabCrit,$prefix=null,$keepIntact=false){

        global $db;
		$prefix=(!is_null($prefix)?$prefix.".":"");
			
		$this->sqlRecherche.= " ".$tabCrit['OP']." ".$prefix."ID_DOC IN (SELECT distinct ID_DOC from t_doc_publication WHERE ".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],$keepIntact).")";
			
		if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
		$highlight[$tabCrit['FIELD']]=array($tabCrit['VALEUR']);

		if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
    }
	
	protected function chercheDateRedif($tabCrit,$prefix=""){
        global $db;
		$prefix=($prefix!=""?$prefix.".":"");
       	if (!is_array($tabCrit['VALEUR'])) $tabCrit['VALEUR']=(array)$tabCrit['VALEUR'];

       	if (empty($tabCrit['VALEUR'])) return;

		$sql="";
       	foreach ($tabCrit['VALEUR'] as $i=>$myDate) {
			$myDate=str_replace('*','',$myDate);
	        $valeur_date1=convDate($myDate);
			if (empty($valeur_date1)) break;

			$arrFldDate=explode(',',$tabCrit['FIELD']);

			$highlight[$tabCrit['FIELD']]=array($valeur_date1);
			if($i>0) $sql.=" OR ";
			$sql.=" ".$prefix."ID_DOC IN (SELECT distinct ID_DOC from t_doc_redif WHERE ".$tabCrit['FIELD']." like '".$valeur_date1."%')";

			if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$myDate." ";
			if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
	    }
		
		if(!empty($sql)){
			$this->sqlRecherche.=" ".$tabCrit['OP']." (".$sql.") ";
		}
   }

/**
 * En fonction des types de recherche, appelle les fonctions de traitement dédiée.
 * Cette fonction peut se trouver surchargée si une classe introduit des recherches spécifiques
 */
    protected function ChooseType($tabCrit,$prefix) {
            // I.2.A.1 Tests des champs et appel des fonctions de recherche ad�quates :
		// VP 13/12/09 : ajout mode FTE recherche exacte (Vietnamien en particulier)
           switch($tabCrit['TYPE'])
            {
            	case "DA" : // Recherche DocAcc
            		$this->chercheDocAcc($tabCrit,$prefix);
                    break;
                    
            	case "DAF" : // Recherche DocAcc en fulltext
            		$this->chercheDocAccFT($tabCrit,$prefix);
                    break;
                    
            	case "M" :
            		$this->chercheDocMat($tabCrit,$prefix);
            	break;

            	case "MAT" :
            		$this->chercheMat($tabCrit,$prefix);
					break;
				case "FTMAT" :
            		$this->chercheMat($tabCrit,$prefix);
					break;	
				//recheeche sur DocMatVal		
				case "DMV" :
            		$this->chercheDocMatVal($tabCrit,$prefix);
					break;

            	case "F": //Fonds par saisie de texte
            		$this->chercheFonds($tabCrit,$prefix);
            	break;
				
				case "FE": //Fonds exact par saisie de texte, pas d'extension de la recherche aux fils 
            		$this->chercheFonds($tabCrit,$prefix,false);
            	break;

            	case "FI": //Fonds par ID
            	case "FPI": //Fonds parent par ID
            		$this->chercheFondsId($tabCrit,$prefix);
            	break;
				
				case "FIE": //Fonds exact par ID, pas d'extension de la recherche aux fils 
            		$this->chercheFondsId($tabCrit,$prefix,false);
            	break;

            	case "DX": //Toutes dates
            		$this->chercheTouteDate($tabCrit,$prefix);
            	break;

                case "FT": //FullText
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    break;
                case "FTS": //FullText avec découpage de chaque mot
                	$this->FToperator='+';
                	$this->chercheTexteAmongFields($tabCrit,$prefix);
                	break;
                case "FTE": //FullText exact
					if(strpos("\"",$tabCrit['VALEUR'])===false) $tabCrit['VALEUR']="\"".$tabCrit['VALEUR']."\"";
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    break;
                case "C":   // Recherche sur un champ précis
                    $this->chercheChamp($tabCrit,$prefix);
                    break;
                case "CB":   // VP 24/07/09 : Recherche sur un champ booléen
					$this->chercheChampBooleen($tabCrit,$prefix);
                    break;
                case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
                    $this->chercheChamp($tabCrit,$prefix,true);
                    break;
                case "CS":   // Recherche sur un champ strict
                    $this->chercheChampStrict($tabCrit,$prefix,true);
                    break;
                //PC 12/07/11 : ajout d'un critère pour gérer les apostrophes en recherche
                case "CA":   // Recherche sur un champ précis sans analyse de valeur avec traitement des apostrophes
                	$tabCrit['VALEUR'] = str_replace("'", " ' ", $tabCrit['VALEUR']);
                    $this->chercheChamp($tabCrit,$prefix,true);
                case "CI":    // Recherche sur un champ d'une table annexe par ID
                    $this->chercheIdChamp($tabCrit,$prefix);
                    break;
                case "CT": // Recherche sur un champ par texte exact dans une table liée (fonds, genre,...)
                    $this->chercheChampTable($tabCrit,$prefix);
                    break;
                case "D":   // Recherche sur date(s)
                    $this->chercheDate($tabCrit,$prefix);
                    break;
                case "H":   // Recherche sur durées(s)
                    $this->chercheDuree($tabCrit,$prefix);
                    break;
                case "V":  // Recherche sur valeur
                case "VF":
                    $this->chercheVal($tabCrit,$prefix);
                    break;
                case "VE":  // VP 2/3/09 : Recherche exacte sur valeur
                    $this->chercheVal($tabCrit,$prefix,true);
                    break;
                case "VI":    // Recherche sur valeur par ID (récursif)
                    $this->chercheIdVal($tabCrit,$prefix,true);
                    break;
                case "DV":  // Recherche sur valeur des droits
                    $this->chercheRightVal($tabCrit,$prefix);
                    break;
                    //PC 07/09/12 : ajout des recherches sur les dates de droits
                case "DD":  // Recherche sur date des droits
                    $this->chercheRightDate($tabCrit,$prefix);
                    break;
                case "DCDLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareRightDate($tabCrit,$prefix);
                	break;
                case "DCDGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareRightDate($tabCrit,$prefix);
                	break;
                case "CD" : //comparaison de date
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CDN" : //comparaison de date ou date null
                    $this->compareDate($tabCrit,$prefix,true);
                    break;
                case "CDLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CDGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CH" : //comparaison de date
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CV" : //comparaison de valeur =
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVGT" : //comparaison de valeur >
                	$tabCrit['VALEUR2']='>';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVLT" : //comparaison de valeur <
                	$tabCrit['VALEUR2']='<';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
				case "CVGTE" : //comparaison de valeur >=
                	$tabCrit['VALEUR2']='>=';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
				case "CVLTE" : //comparaison de valeur <=
                	$tabCrit['VALEUR2']='<=';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "LI" : // Recherche sur ID LEX unique (récursif)
                	$this->chercheIdLex($tabCrit,$prefix,true);
                	break;
					
					//XB sans aucune extension lex
					 case "LIE" : // Recherche sur ID LEX unique (non récursif)
                	$this->chercheIdLex($tabCrit,$prefix,false);
                	break;
					
                case "L" : // Recherche sur termes lexique (av. extension lex PRE)
                	$this->chercheLex($tabCrit,$prefix,true,false);
                	break;
                //@update VG 21/06/2010
                //@update VG 29/07/2010 : ajout du 4ème et du 5ème paramètre
                case "LE" : //Recherche exacte sur lexique
                	$this->chercheLexExacte($tabCrit,$prefix,true, true, false);
                	break;
                case "LP" : // Recherche sur termes lexique (av. extension lex PRE et POST)
                	$this->chercheLex($tabCrit,$prefix,true,true);
                	break;
		
                case "P" :
                	$this->cherchePers($tabCrit,$prefix);
                	break;
				// recherche personne, intersection entre chacun des termes
				case "PX" :
                	$this->cherchePers($tabCrit,$prefix,true);
                	break;
                case "PI" :
                	$this->chercheIdPers($tabCrit,$prefix);
                	break;
				case "U" :
					$this->chercheUsager($tabCrit,$prefix);
					break;
				case "FEST" :
					$this->chercheFestival($tabCrit,$prefix);
					break;

				case "PAN" :
					$this->cherchePanier($tabCrit,$prefix);
					break;
				
				case 'CATID':
					$this->chercheIdCategorie($tabCrit,$prefix);
					break;
				
				case 'CAT':
					$this->chercheCategorie($tabCrit,$prefix);
					break;

				case 'RDIF':
					$this->chercheRediffusion($tabCrit,$prefix);
					break;

				case 'DDIF':
					$this->chercheDateRedif($tabCrit,$prefix);
					break;
					
                case 'DRX':
                    $this->chercheTouteDateRedif($tabCrit,$prefix);
                    break;
                    
				case 'PUB':
					$this->cherchePublication($tabCrit,$prefix);
					break;

                default : break;
            }
    }





    /** Applique les restrictions de droits sur la recherche.
    // i.e. : Sp�cifie que la liste des docs r�sultats doit faire partie d'une liste de fonds/collection auquelle l'utilisateur a acc�s.
    // Ajoute donc un "AND DOC_ID_FONDS in ('fonds1', 'fonds5', 'coll3',...) � la requette sql
    // NB. : On ne prend pas en compte les fonds qui ont des privil�ges � 0.
    **/
    function appliqueDroits(){
    	//by LD 04/06/08 => utilisation d'un tableau et non d'une chaine pour éviter les id_fonds in ('',.....)
		$prefix=(!is_null($this->prefix)?$this->prefix.".":"");
        $usr=User::getInstance();
        //$liste_id="''";
        // VP 8/02/13 : par défaut l'utilisateur n'a accès à rien
        $liste_id=array('0');
        foreach($usr->Groupes as $value){
           // if ($value["ID_PRIV"]!=0){ $liste_id.=", '".$value["ID_FONDS"]."'"; }
           if (isset($value["ID_PRIV"]) && $value["ID_PRIV"]!=0 && !empty($value["ID_FONDS"])) $liste_id[]=$value["ID_FONDS"];
        }

        //if(strlen($liste_id)>0) {
        if (count($liste_id)>0) {
            $this->sqlRecherche.= " AND ".$prefix."DOC_ID_FONDS ";
            $this->sqlRecherche.= " in ('".implode("','",$liste_id)."')";
        }
        
        //PC 02/12/10 : Ajout de la possiblité d'importer un fichier afin de définir des droits supplémentaires 
//        if (file_exists(designDir . '/include/addDroits.inc.php')) {
//			include(getSiteFile("designDir", 'include/addDroits.inc.php'));
//		}
		// VP 9/11/2017 : utilisation fonction plutôt que include addDroits
		if(function_exists("appliqueDroitsCustomDoc")){
			$this->sqlRecherche.= appliqueDroitsCustomDoc($this->sqlRecherche);
		}
    }

   	/*  refine pour compatibiloté autres moteurs */
    function refine ($string='') {
        return '';
    }

    function setRefine($str_refine='', $reset_refine='',$refine_facet ='') {
        $this->str_refine = '';
    }
	
	/**
     * Execution requête 
     * IN : SQL, max_rows (nb de lignes max), $secstocache, $highlight (compatibilité solr)
     * OUT : var de classe : Result (resultat de requete "tronqué"),
     * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales)
     */
    function execute( $max_rows = 10, $secstocache=0, $highlight=false)
    {	
		$return = parent::execute($max_rows,$secstocache, $highlight);
		if($return){
			return $return && $this->ajoutPrivResult();
		}
	}
	
	/**
	 * fonction ajoutPrivResult 
	 * Alimente le tableau de résultats avec le niveau de privilège de l'utilisateur pour chaque résultat.
     */
	function ajoutPrivResult(){
		$usr=User::getInstance();
		// VP 2/3/09 : prise en compte paramètre useMySQL
		$fld="DOC_ID_FONDS";
		
		for ($i=0;$i<count($this->result);$i++){
			// VP 21/4/10 : modif valeur privilege admin
			$this->result[$i]["ID_PRIV"]=5;
			if ($usr->Type!=kLoggedAdmin){
			$this->result[$i]["ID_PRIV"]=0; //par défaut

				foreach($usr->Groupes as $value){
					if(isset($this->result[$i][$fld])){
						if($value["ID_FONDS"]==$this->result[$i][$fld]){
							$this->result[$i]["ID_PRIV"]=$value["ID_PRIV"];
						}
					}
				}
			}
		}
		// MS - à optimiser / patcher, je n'aime pas trop l'idée d'avoir une fonction externe pouvant faire n'importe quoi sur le tableau. 
		// Conservé pour rétrocompatibilité.
		if(function_exists("ajoutPrivResultCustom")){
			$this->result=ajoutPrivResultCustom($this->result);
		}
		
		return true;
	}
	
}//Fin de la classe ChercheDoc

?>
