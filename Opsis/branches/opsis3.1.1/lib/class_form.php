<?
/**
* Classe d'affichage de formulaires
*/
class Form{

	var $entity;		// Entité concernée : DOC, MAT, DOCMAT, etc...
	var $classLabel;	// Class des labels
	var $classValue;	// Class des valeurs

/**
* Affiche un formulaire décrit par des données XML
* IN : données XML
* OUT : affichage du formulaire
**/
	function display($xmlForm){
		$tabForm=xml2tab($xmlForm);
		//print_r($tabForm);
		$this->displayTab($tabForm);
	}

/**
* Affiche un formulaire décrit par un tableau hiérarchique
* IN : tableau hiérarchique issu d'une transformation XML
* OUT : affichage du formulaire
**/

	function displayTab($tab,$nature="",$index=""){
		if(($nature!="ELEMENT")&&is_numeric($index)){
			// Ouverture tags
			if (isset($tab['TYPE_MEDIA']))  {
				$arrType = explode(",", $tab['TYPE_MEDIA']);
				if(isset($this->saisieObj->t_doc['DOC_ID_MEDIA'])){
					$typeMedia = $this->saisieObj->t_doc['DOC_ID_MEDIA'];
				}else if (isset($this->saisieObj->t_mat['MAT_ID_MEDIA'])){
					$typeMedia = $this->saisieObj->t_mat['MAT_ID_MEDIA'];
				}else if (isset($this->saisieObj->t_panier_doc['DOC_ID_MEDIA'])){
					$typeMedia = $this->saisieObj->t_panier_doc['DOC_ID_MEDIA'];
				}
				if (count($arrType) > 0 && (empty($typeMedia) || !in_array($typeMedia, $arrType)))
					return;
			}
			
			switch($nature){
				case "FORM":
					$this->beginForm($tab);
					break;

				case "TABLE":
				case "FIELDSET":
				case "DIV":
					$this->beginTagTable($nature, $tab);
					break;

				case "INCLUDE":
					if(empty($tab['FOLDER'])) $fieldRefPath = $tab['NAME'].".xml";
					else $fieldRefPath = $tab['FOLDER']."/".$tab['NAME'].".xml";
					$xmlFieldRef = file_get_contents(getSiteFile("designDir", $fieldRefPath));
					$tabFieldRef = xml2tab($xmlFieldRef);
					$this->displayTab($tabFieldRef);
					break;
					
				default:
					$this->beginTag($nature, $tab);
					break;
			}
		}
		// Parse des tableaux imbriqués
		$nat=$nature;
		foreach($tab as $idx=>$elt){
			if(is_array($elt) && $idx !== "INNERFIELDS") {
				if(!(is_numeric($idx))) {
					$nat=$idx;
				}
				$this->displayTab($elt,$nat,$idx);
			} else{
				$attr[$idx]=$elt;
			}
		}
		if(($nature=="ELEMENT") && isset($attr) && is_array($attr)){
			// Affichage élément de formulaire
			$this->displayElt($attr);
		}else if(is_numeric($index)){
			// Fermeture tags
			switch($nature){
				case "FORM":
					$this->endForm($attr);
					break;

				case "TABLE":
				case "FIELDSET":
				case "DIV":
					$this->endTagTable($nature, $attr);
					break;

				default:
					$this->endTag($nature);
					break;
			}
		}
	}

	/**
	* Affiche début formulaire
	 * IN : tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	function beginForm($attr){
		// VP 10/11/10 ajout attribut ENCTYPE
		print "<div id='chooser' class='iframechoose' style='position:absolute;display:none;'></div>\n";
		print "<script>makeDraggable(document.getElementById('chooser'));</script>\n";
		print "<form method='post' ";
		if(isset($attr['NAME'])) print " NAME='".$attr['NAME']."'";
		if(isset($attr['ID'])) print " ID='".$attr['ID']."'";
		if(isset($attr['ACTION'])) print " ACTION='".$attr['ACTION']."'";
		if(isset($attr['ONSUBMIT'])) print " ONSUBMIT='".$attr['ONSUBMIT']."'";
		if(isset($attr['TARGET'])) print " TARGET='".$attr['TARGET']."'";
		if(isset($attr['CLASS'])) print " CLASS='".$attr['CLASS']."'";
		if(isset($attr['ENCTYPE'])) print " ENCTYPE='".$attr['ENCTYPE']."'";

		print " >\n";

		// VP 23/1/09 : ajout tag SCRIPT
		if(isset($attr['SCRIPT'])) print " <script type='text/javascript'>\n".$attr['SCRIPT']."\n</script>";
                // B.RAVI 20150303 correction bug, class="nomClasse" et non plus class=nomClasse
		if(!isset($attr['NO_TABLE'])) print "<table border='0' cellspacing='0' cellpadding='0' ".(isset($attr['CLASS'])?" CLASS=\"".$attr['CLASS']."\"":"").">\n";

	}
	/**
	* Affiche fin formulaire
	 * OUT : affichage de l'élément
	 **/
	function endForm($attr=''){
		if(!isset($attr['NO_TABLE'])) print "</table>\n</form>\n";
		else print "</form>\n";
		
		// MS - 22.02.16 - voir fonction addinput (attribut PREVENT_CACHE)
		if(isset($this->prevent_cache_events) && $this->prevent_cache_events){
			echo '<script type="text/javascript">
				try{
				window.addEventListener(\'pageshow\', function(event) {
					if(event.persisted){
						$j("input[data-prevent-cache]").each(function(idx,elt){
							try{
								$j(elt).val($j(elt).attr("data-prevent-cache"));';
			if(defined('useSolr') && useSolr){
				echo '			if($j(elt).attr("name") == "refine"){
									if(typeof form.refine != \'undefined\' && form.refine.value!=""){
										obj_refine_solr = JSON.parse(form.refine.value);
									}else{
										obj_refine_solr=new Array();
									}
								}';
				}
			echo'			}catch(e){console.log("crash prevent-cache:"+e);}
						});
					}
				});
				}catch(e){console.log(e);}
			</script>';
		}
	}

	/**
		* Affiche début tag avec tableau
	 * IN : nature, tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	function beginTagTable($nature, $attr){
		// VP 5/03/09 : ajout attribut ID
		// VP 4/09/09 : ajout attributs CLASSLABEL
		// VP 2/12/09 : ajout attributs TOGGLE_DISPLAY
		if(!isset($attr['NO_TABLE'])) print "</table>";
		if(isset($attr['TOGGLE_DISPLAY'])){
			if($attr['TOGGLE_DISPLAY']=="none") {
				$arrow="arrow_right.gif";
			}else {
				$arrow="arrow_down.gif";
			}
			$attr['DISPLAY']=$attr['TOGGLE_DISPLAY'];

			print "<div class='".(isset($attr['CLASSLABEL'])?$attr['CLASSLABEL']:$this->classLabel)."'>
<a href=\"javascript:toggleVisibility(document.getElementById('".$attr['NAME']."'),document.getElementById('arrow".$attr['NAME']."'))\" >
<img src='design/images/".$arrow."' align='absmiddle' border='0' id='arrow".$attr['NAME']."' />".(defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'])."</a>
</div>";
		}
		print "<".strtolower($nature);
		if(isset($attr['NAME'])) print " NAME='".$attr['NAME']."' ID='".$attr['NAME']."'";
		if(isset($attr['ID'])) print " ID='".$attr['ID']."'";
		if(isset($attr['CLASS'])) print " CLASS='".$attr['CLASS']."'";
		if(isset($attr['DISPLAY'])) print " style='display:".$attr['DISPLAY']."'";
		print ">\n";
		// VP 23/1/09 : ajout tag SCRIPT
		if(isset($attr['SCRIPT'])) print " <script type='text/javascript'>\n".$attr['SCRIPT']."\n</script>";
		if(isset($attr['LEGEND'])) {
			$legend=$attr['LEGEND'];
			print "<legend>".(defined($legend)?constant($legend):$legend)."</legend>";
		}
		//update VG 01/04/11 : ajout gestion de classes CSS
		if(!isset($attr['NO_TABLE'])) print "<table border='0' cellspacing='0' cellpadding='0' ".(isset($attr['CLASSTABLE'])?" CLASS='".$attr['CLASSTABLE']."'":"").">\n";
	}
	/**
		* Affiche début tag avec tableau
	 * IN : nature
	 * OUT : affichage de l'élément
	 **/
	function endTagTable($nature, $attr=array()){
		if(!isset($attr['NO_TABLE'])) {
			print "</table>\n</".strtolower($nature).">\n";
			print "<table border='0' cellspacing='0' cellpadding='0'>\n";
		}
		else print "</".strtolower($nature).">\n";
	}
	/**
		* Affiche début tag
	 * IN : nature, tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	function beginTag($nature, $attr){
		// VP 5/03/09 : ajout attribut ID
		print "<".strtolower($nature);
		if(isset($attr['NAME'])) print " NAME='".$attr['NAME']."'";
		if(isset($attr['ID'])) print " ID='".$attr['ID']."'";
		if(isset($attr['CLASS'])) print " CLASS='".$attr['CLASS']."'";
		print " >\n";
	}
	/**
		* Affiche fin tag
	 * IN : nature
	 * OUT : affichage de l'élément
	 **/
	function endTag($nature){
		print "</".strtolower($nature).">\n";
	}
	/**
		* Affiche d'un élément
	 * IN : tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	// VP 27/07/09 : ajout attributs CLASSLABEL, CLASSVALUE et CLASSHELP
	function displayElt($attr){
		//PC 05/07/13 : test sur le type de document
		// MS - 30.08.18 - Si on a le flag "BLANK_LINE", c'est qu'on est en train de générer des éléments appartenant à la ligne vide "blank"
		// dans le cadre de la génération d'un input de type "multi", cf FormSaisie:addMulti
		// dans ce cas on bypass les tests car il faut que la ligne vide contienne le formulaire entier, indépendamment des limites TYPE_MEDIA ou TYPE_DOC
		if (isset($attr['TYPE_DOC'])&& !isset($attr['BLANK_LINE']))  {
			$arrType = explode(",", $attr['TYPE_DOC']);
			$typeDoc = $this->saisieObj->t_doc['DOC_ID_TYPE_DOC'];
			if (count($arrType) > 0 && (empty($typeDoc) || !in_array($typeDoc, $arrType)))
				return;
		}
		//MS 29/08/14 : test sur le type de media
		// MS - 30.08.18 - Si on a le flag "BLANK_LINE", c'est qu'on est en train de générer des éléments appartenant à la ligne vide "blank"
		// dans le cadre de la génération d'un input de type "multi", cf FormSaisie:addMulti
		// dans ce cas on bypass les tests car il faut que la ligne vide contienne le formulaire entier, indépendamment des limites TYPE_MEDIA ou TYPE_DOC
		if (isset($attr['TYPE_MEDIA']) && !isset($attr['BLANK_LINE']))  {
			$arrType = explode(",", $attr['TYPE_MEDIA']);
			if(isset($this->saisieObj->t_doc['DOC_ID_MEDIA'])){
				$typeMedia = $this->saisieObj->t_doc['DOC_ID_MEDIA'];
			}else if (isset($this->saisieObj->t_mat['MAT_ID_MEDIA'])){
				$typeMedia = $this->saisieObj->t_mat['MAT_ID_MEDIA'];
			}else if (isset($this->saisieObj->t_panier_doc['DOC_ID_MEDIA'])){
				$typeMedia = $this->saisieObj->t_panier_doc['DOC_ID_MEDIA'];
			}
			if (count($arrType) > 0 && (empty($typeMedia) || !in_array($typeMedia, $arrType)))
				return;
		}
		//MS 13/11/14 : test sur le type de desc personne / lex
		if (isset($attr['TYPE_LEX']))  {
			$arrType = explode(",", $attr['TYPE_LEX']);
			if(isset($this->saisieObj->t_personne['PERS_ID_TYPE_LEX'])){
				$typeDesc = $this->saisieObj->t_personne['PERS_ID_TYPE_LEX'];
			}else if(isset($this->saisieObj->t_lex['LEX_ID_TYPE_LEX'])){
				$typeDesc = $this->saisieObj->t_lex['LEX_ID_TYPE_LEX'];
			}
			if (count($arrType) > 0 && (empty($typeDesc) || !in_array($typeDesc, $arrType)))
				return;
		}

		//MS 19/12/14 : test sur les fonds
		// accepte des array  de valeurs obligatoire : 1,3 => champ affiché si doc_id_fonds == 1 ou doc_id_fonds == 3
		// accepte des valeurs interdites : !1,3 champ affiché si doc_id_fonds != 1 et si doc_id_fonds==3
		if (isset($attr['FONDS_COND']))  {
			//
			$arrFonds = explode(",", $attr['FONDS_COND']);
			if(isset($this->saisieObj->t_doc['DOC_ID_FONDS'])){
				//recup doc_id_fonds
				$fonds = $this->saisieObj->t_doc['DOC_ID_FONDS'];
			}
			if(isset($this->saisieObj->t_mat['MAT_ID_FONDS'])){
				$fonds = $this->saisieObj->t_mat['MAT_ID_FONDS'];
			}
			if (count($arrFonds) > 0 && (empty($fonds))){
				return ;
			}
			$arr_fonds_in = array() ;
			// parcours arrFonds
			foreach($arrFonds as $valid_fond){
				if(substr($valid_fond,0,1) == '!' && $fonds == substr($valid_fond,1)){
					// si on trouve une valeur interdite == doc_id_fonds, on sort
					return ;
				}else if (substr($valid_fond,0,1) != '!'){
					// sinon on ajoute à la liste des valeurs "obligatoires"
					$arr_fonds_in[] = $valid_fond;
				}
			}
			// si on a des valeurs obligatoires et que le fonds n'est pas contenu dans ces valeurs, alors on sort
			if(!empty($arr_fonds_in)){
				if(!in_array($fonds,$arr_fonds_in)){
					return ;
				}
			}
		}



		// VP 26/11/09 : ajout attribut PROFIL_MIN
		if(isset($attr['PROFIL_MIN'])){
			$myUsr=User::getInstance();
            $profil_min = defined($attr['PROFIL_MIN'])?constant($attr['PROFIL_MIN']):$attr['PROFIL_MIN'];
			if($myUsr->getTypeLog() < $profil_min ) {return;}
		}
		if(isset($attr['PROFIL_MAX'])){
			$myUsr=User::getInstance();
            $profil_max = defined($attr['PROFIL_MAX'])?constant($attr['PROFIL_MAX']):$attr['PROFIL_MAX'];
			if($myUsr->getTypeLog() > $profil_max ) {return;}
		}
		// VP 26/10/17 : ajout attribut PROFIL_IDS
		if(isset($attr['PROFIL_IDS'])){
			$myUsr=User::getInstance();
			$ids = explode(",", $attr['PROFIL_IDS']);
			array_walk($ids, create_function('&$v', '$v = defined($v)?constant($v):$v;'));
			if(!in_array($myUsr->getTypeLog(), $ids)) {return;}
		}
										
		// VP 26/10/17 : ajout attribut PRIV_IDS
		if(isset($attr['PRIV_IDS']) && method_exists($this->saisieObj, "checkAccess")){
			$ids = explode(",", $attr['PRIV_IDS']);
			$access = false;
			foreach($ids as $id){
				if($this->saisieObj->checkAccess($id)){
					$access=true;
				}
			}
			if(!$access) {return;}
		}
										
		// VP 22/12/17 : ajout attribut READONLY_PRIV_IDS
		if(isset($attr['READONLY_PRIV_IDS']) && method_exists($this->saisieObj, "getPrivileges")){
			$arrPriv = $this->saisieObj->getPrivileges();
			if(isset($this->variables['input_arrPriv']) && is_array($this->variables['input_arrPriv'])){
				$arrPriv = array_merge($arrPriv,$this->variables['input_arrPriv']);
			}
			$readonly = false;
			$ids = explode(",", $attr['READONLY_PRIV_IDS']);
			foreach($arrPriv as $id_priv){
				if(in_array($id_priv, $ids)){
				   $readonly = true;
				}else{
				   $readonly = false;
				   break;
				}
			}
			if($readonly){
				$attr['READONLY']=true;
				unset($attr['MANDATORY']);
			}
		}
										
		// LABEL
		if(isset($attr['LABEL'])) $label=defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'];
		else if(isset($attr['CHLIBS'])) $label=$attr['CHLIBS'];
		if(isset($label) && defined($label)) $label=constant($label);
		//VP 4/06/09 : reaffectation valeur label décodée
		if (isset($label))
			$attr['LABEL']=$label;

		// VP 20/09/10 : ajout attr DIVTAGS
		if(isset($attr['TRTAGS'])) {
			if($attr['TRTAGS']=="start" || $attr['TRTAGS']=="both") print "<tr class='".(isset($attr['CLASSROW'])?$attr['CLASSROW']:"")."'>\n";
			if(isset($label)) {
				// VP 3/12/09 : ajout * au label pour les champs obligatoires
				print "<td class='".(isset($attr['CLASSLABEL'])?$attr['CLASSLABEL']:$this->classLabel)."' valign='".(isset($attr['VALIGN'])?$attr['VALIGN']:"top")."' ".(isset($attr['LABEL_WIDTH'])?" WIDTH=".$attr['LABEL_WIDTH']:"").(isset($attr['ROWSPAN'])?" ROWSPAN='".$attr['ROWSPAN']."'":"").">";
				print $label.((isset($attr['MANDATORY']) && $attr['MANDATORY'])?"<em>*</em>":"")."</td>\n";
			}
			// VP (24/2/09) : ajout valign='top' à la cellule d'affichage du champ
			print "<td class='".(isset($attr['CLASSVALUE'])?$attr['CLASSVALUE']:$this->classValue)."' valign='".(isset($attr['VALIGN'])?$attr['VALIGN']:"top")."' ".(isset($attr['COLSPAN'])?" COLSPAN=".$attr['COLSPAN']:"").(isset($attr['ROWSPAN'])?" ROWSPAN='".$attr['ROWSPAN']."'":"").(isset($attr['WIDTH'])?" WIDTH=".$attr['WIDTH']:"").">\n";
		}elseif(isset($attr['DIVTAGS'])) {
			if($attr['DIVTAGS']=="start" || $attr['DIVTAGS']=="both")
				// MS - 13.08.18 - correction de l'affichage de classValue à la place de classLabel comme valeur par défaut si aucun classvalue n'est défini dans l'élément meme ... 
				// étant donné qu'il s'agit d'une valeur par défaut cela va peut être perturber des affichages mais on ne peut pas laisser cette valeur par défaut illogique. 
				
				print "<div class='".(isset($attr['CLASSVALUE'])?$attr['CLASSVALUE']:$this->classValue)."'>\n";
			if(isset($label))
			{
				print "<label class='".(isset($attr['CLASSLABEL'])?$attr['CLASSLABEL']:$this->classLabel)."' >";
				echo $label;
				echo ((isset($attr['MANDATORY']) && $attr['MANDATORY'])?"<em>*</em>":"");
				echo "</label>\n";
			}
		}

		// CHAMP(S) EFFECTIF(S)
		// VP 22/1/09 : ajout valeur par défaut depuis $_REQUEST
		if(!isset($attr['VALUE']) && isset($attr['NAME']) && isset($_REQUEST[$attr['NAME']])) $attr['VALUE']=$_REQUEST[$attr['NAME']];

		if (!isset($attr['VALUE']))
			$attr['VALUE']='';

		if (!isset($attr['NAME']))
			$attr['NAME']=null;

		$this->displayField($attr['NAME'], $attr['VALUE'], $attr);

		// AIDE (optionel)
		if(isset($attr['TRTAGS'])) {
			print "</td>\n";
			if(isset($attr['HELP'])) {
				print "<td class='".(isset($attr['CLASSHELP'])?$attr['CLASSHELP']:$this->classHelp)."' ".(isset($attr['HELP_WIDTH'])?" WIDTH=".$attr['HELP_WIDTH']:"").">".(defined($attr['HELP'])?constant($attr['HELP']):$attr['HELP'])."</td>\n";
			}
			if($attr['TRTAGS']=="end" || $attr['TRTAGS']=="both") print "</tr>";
		}
		elseif(isset($attr['HELP'])) {
				print "<span class='".(isset($attr['CLASSHELP'])?$attr['CLASSHELP']:$this->classHelp)."' ".(isset($attr['HELP_WIDTH'])?" WIDTH=".$attr['HELP_WIDTH']:"")." ".(isset($attr['HELP_JS'])?" onclick=\"".$attr['HELP_JS']."\"":"").">".(defined($attr['HELP'])?constant($attr['HELP']):$attr['HELP'])."</span>\n";
		}
		else if(isset($attr['HELP_IMG']) && !empty($attr['HELP_IMG'])){
				print "<span class='".(isset($attr['CLASSHELP'])?$attr['CLASSHELP']:$this->classHelp)."' ".(isset($attr['HELP_WIDTH'])?" WIDTH=".$attr['HELP_WIDTH']:"")." ".(isset($attr['HELP_JS'])?" onclick=\"".$attr['HELP_JS']."\"":"")." ".(isset($attr['HELP_JS_MOUSE_OVER'])?" onMouseOver =\"".$attr['HELP_JS_MOUSE_OVER']."\"":"")." ".(isset($attr['HELP_JS_MOUSE_OUT'])?" onMouseOut=\"".$attr['HELP_JS_MOUSE_OUT']."\"":"")."><img src=\"".$attr['HELP_IMG']."\" /></span>\n";
		}

		if(isset($attr['DIVTAGS'])) {
			if($attr['DIVTAGS']=="end" || $attr['DIVTAGS']=="both") print "</div>\n";
		}
	}




	/**
	* Affiche d'un élément
	 * IN : tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	function displayField($name, $chValue, $attr){

		// VP 2/06/09 : ajout cas "buttonspan"
		// VP 24/06/09 : ajout cas "span"
		// VP 27/11/09 : ajout RTEditor (fckEditor)
		switch(strtolower($attr['TYPE'])){
			case "textarea":
				$this->addTextarea($name, $chValue, $attr);
				break;

			case "rte":
				$this->addRTEditor($name, $chValue, $attr);
				break;

			case "select":
				$this->addSelect($attr['SELECT_TYPE'], $name, $chValue, $attr);
				break;

			case "radio":
				$this->addRadio($attr['RADIO_TYPE'], $name, $chValue,$attr);
				break;

			case "checkbox":
				$this->addCheckbox($attr['CHECKBOX_TYPE'], $name, $chValue,$attr);
				break;

			case "buttonspan":
				$this->addButton($attr['BUTTON_TYPE'], $name, $chValue, $attr);
				break;

			case "span":
				$this->addSpan($name, $chValue, $attr);
				break;

			case "image":
				$this->addImage($attr['TYPE'], $name, $chValue, $attr);
				break;
			
			case "custom":
				$this->addCustom($name, $chValue, $attr);
				break;
										
			default:
				$this->addInput($attr['TYPE'], $name, $chValue, $attr);
				break;
		}
	}

	/**
	* Affiche un champ input
	* IN : type, nom, valeur par défaut, tableau de paramètres
	* OUT : affichage du champ
	**/

	function addInput($type, $nom, $val, $attr=null){
		$attr['TYPE']=$type;
		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		$attr['VALUE']=(is_string($val) && defined($val)?constant($val):$val);
		//Ajout format
		if (isset($attr['FORMAT']) && !empty($attr['FORMAT']))
		{
			switch($attr['FORMAT']){
				case "TC":
					//@update VG : ajout de la condition pour l'attribut onchange
					if(get_class($this)=='FormSaisie') $attr["ONCHANGE"].=";checkTC(this)";
					$attr["ONKEYPRESS"].=";formatTC(this)";
					$attr["ONKEYUP"].=";formatTC(this)";
					if(empty($attr["SIZE"])) $attr["SIZE"]="11";
					break;
				// VP 30/09/10 : ajout Time et Date pour attribut FORMAT
				case "Time":
					if(get_class($this)=='FormSaisie') $attr["ONCHANGE"].=";checkTime(this)";
					$attr["ONBLUR"].=";checkTime(this)";
					$attr["ONKEYPRESS"].=";formatTime(this)";
					$attr["ONKEYUP"].=";formatTime(this)";
					if(empty($attr["SIZE"])) $attr["SIZE"]="8";
					break;

				case "Date":
					if(empty($attr["FORMATFR"])){
						if(get_class($this)=='FormSaisie') $attr["ONCHANGE"].=";checkDate(this)";
						$attr["ONBLUR"].=";checkDate(this)";
						$attr["ONKEYPRESS"].=";formatDate(this)";
						$attr["ONKEYUP"].=";formatDate(this)";
					}
					else
					{
						if(get_class($this)=='FormSaisie') $attr["ONCHANGE"].=";checkDate(this,'jj/mm/aaaa')";
						$attr["ONBLUR"].=";checkDate(this,'jj/mm/aaaa')";
						$attr["ONKEYPRESS"].=";formatDate(this,'jj/mm/aaaa')";
						$attr["ONKEYUP"].=";formatDate(this,'jj/mm/aaaa')";
					}
					if(empty($attr["SIZE"])) $attr["SIZE"]="10";
					break;
			}
		}

		if (isset($attr['LABEL']) && !empty($attr['LABEL']))
			$attr['LABEL']=defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'];
		// VP 14/9/09 : ajout paramètre AUTOCOMPLETE
		if(isset($attr['AUTOCOMPLETE'])) {
			$autocomplete=$attr['AUTOCOMPLETE'];
			$attr['AUTOCOMPLETE']="off";
		}

		print "<input ";
		foreach($attr as $i=>$x) {
//			if (!in_array(strtolower($i),array('chtypes','label','chvalues','chvalues2','chlibs','chfields','chvalues2_type','chops','chvalues2_label','chtypes_lib','trtags','select_sql')))
            // VP 2/05/13 : consrvaton de label et chfields pour fonctiosn JS de saisie et de validation de formulaire
			if( $i== 'PLACEHOLDER' && defined($attr[$i])){
				print " ".strtolower($i)."=\"".str_replace('"',"&quot;",constant($x))."\"";
			}else if (!in_array(strtolower($i),array('chtypes','chvalues','chvalues2','chlibs','chvalues2_type','chops','chvalues2_label','chtypes_lib','trtags','select_sql','chnohls','classvalue','classlabel','checkbox_type')))
                                //2015-05-04 B.RAVI Pouvoir utiliser des constantes PHP dans les attributs  ex: <alt>kRechercher</alt>
				print " ".strtolower($i)."=\"".str_replace('"',"&quot;",(is_string($x) && defined($x)?constant($x):$x))."\"";
		}

		// MS - 22.02.16 - méthodes pour éviter les problèmes de cache lors des click sur les boutons back / forward
		// Pour les navigateurs utilisant un BFcache (FFox/Safari), on ajoute des données data-prevent-cache, que l'on charge via un event handler sur pageshow (voir fonction endForm)
		// Certains navigateurs utilisent un BFcache mais ont des bugs dans la gestion de l'évenement pageshow :
		// - Chrome => event.persisted est tjs false, cf : https://bugs.chromium.org/p/chromium/issues/detail?id=344507#) 
		//			=> on utilise l'attribut autocomplete=off qui permet de recharger la page telle qu'elle a été chargée, plutot que telle qu'on l'a quitté 
		//			=> comportement OK pas de cache
		// - IE8->IE10 => event pageshow n'existe pas, ne semble pas avoir de BFcache OU recharge la page telle qu'elle a été load plutot que telle qu'elle a été quittée 
		//		  => comportement OK pas de cache, 
		// - IE11 => event pageshow existe, persisted est bugé => tjs false (cf Chrome), ne semble pas avoir de BFcache OU recharge la page telle qu'elle a été load plutot que telle qu'elle a été quittée 
		//        => comportement OK pas de cache, 
		if(isset($attr['PREVENT_CACHE']) && $attr['PREVENT_CACHE'] != 'false'){
			$this->prevent_cache_events = true;
			echo " autocomplete=\"off\" data-prevent-cache=\"".str_replace('"',"&quot;",(defined($attr['VALUE'])?constant($attr['VALUE']):$attr['VALUE']))."\" ";  
		}
		print "/>\n";

		
		// affichage de l'icone a cote du champ
		if (isset($attr['ICONE_CHAMP2']) && !empty($attr['ICONE_CHAMP2']) && $attr['TYPE']=='text' && is_int(strpos($attr['NAME'],'chValues2[')))
			echo '<img src="'.$attr['ICONE_CHAMP2'].'" alt="icone" name="chImage" style="vertical-align:bottom;" />';
		else if (isset($attr['ICONE_CHAMP']) && !empty($attr['ICONE_CHAMP']) && $attr['TYPE']=='text' &&  is_int(strpos($attr['NAME'],'chValues[')))
			echo '<img src="'.$attr['ICONE_CHAMP'].'" alt="icone" name="chImage" style="vertical-align:bottom;"/>';

		if (isset($attr['DATEPICKER']) && !empty($attr['DATEPICKER']))
		{
			$config_dp='';
			//seul image fait appel a datepicker
			if (isset($attr['DATEPICKER_ONLY_IMG']) && !empty($attr['DATEPICKER_ONLY_IMG'])){
			$config_dp.= 'buttonImage: "'.designUrl.'/images/calendar.gif",
						  buttonImageOnly: true,
						  showOn : true,';
			}
			// MS - ajout changeYear : true , permet de changer l'année par le biais d'un select
            $config_dp='dateFormat: "'.((isset($attr['DATEPICKER_DATE_FMT']) && !empty($attr['DATEPICKER_DATE_FMT']))?$attr['DATEPICKER_DATE_FMT']:"yy-mm-dd").'",
						dayNamesMin: ["'.kDimancheShort.'", "'.kLundiShort.'", "'.kMardiShort.'", "'.kMercrediShort.'", "'.kJeudiShort.'", "'.kVendrediShort.'", "'.kSamediShort.'"],
						monthNames: ["'.kJanvier.'","'.kFevrier.'","'.kMars.'","'.kAvril.'","'.kMai.'","'.kJuin.'","'.kJuillet.'","'.kAout.'","'.kSeptembre.'","'.kOctobre.'","'.kNovembre.'","'.kDecembre.'"],
						firstDay: "'.((isset($attr['DATEPICKER_FIRST_DAY']) && !empty($attr['DATEPICKER_FIRST_DAY']))?$attr['DATEPICKER_FIRST_DAY']:"").'",
						changeYear : true';



			// MS ajout possibilité de configurer le range d'années selectionnables par l'option changeYear
			// voir documentation datepicker jquery pour la syntaxe
			if(isset($attr['DATEPICKER_YEAR_RANGE']) && !empty($attr['DATEPICKER_YEAR_RANGE'])){
				if(!preg_match('/^(["\']).*\1$/m', $attr['DATEPICKER_YEAR_RANGE'])){
					$attr['DATEPICKER_YEAR_RANGE'] = '"'.$attr['DATEPICKER_YEAR_RANGE'].'"';
				}
				$config_dp .=', yearRange : '.$attr['DATEPICKER_YEAR_RANGE'];
			}
			if(isset($attr['DATEPICKER_SHOWON']) && !empty($attr['DATEPICKER_SHOWON'])){
				if(!preg_match('/^(["\']).*\1$/m', $attr['DATEPICKER_SHOWON'])){
					$attr['DATEPICKER_SHOWON'] = '"'.$attr['DATEPICKER_SHOWON'].'"';
				}
				$config_dp .=', showOn : '.$attr['DATEPICKER_SHOWON'];
			}
			
			if ( !empty( $attr['DATEPICKER_IMG'] ) &&  strcasecmp($attr['DATEPICKER_IMG'], 'none') === 0)
				$datepicker_img = '';
			elseif (!empty($attr['DATEPICKER_IMG']))
				$datepicker_img=$attr['DATEPICKER_IMG'];
			else
				$datepicker_img='calendar.gif';

			if(!empty($datepicker_img)) {
				echo '<a class="datepicker_img" href="javascript:void(0);" onclick="$j(\'#'.str_replace(array("[","]"),array("\\\\[","\\\\]"),$attr['ID']).'\').datepicker(\'show\');"><img src="'.designUrl.'/images/'.$datepicker_img.'" alt="'.kCalendrier.'" /></a>';
			}
			//datepicker nécessite require.js désormais
			echo '<script type="text/javascript">
			$j(document).ready
			(
				function ()
				{	
					requirejs([\'jquery.ui.widget\'],function(){
					var dp_config=
					{
						'.$config_dp.'
					};
					$j("#'.str_replace(array("[","]"),array("\\\\[","\\\\]"),$attr['ID']).'").datepicker(dp_config);
					});
				}
			)
			</script>
			';
		}

		if(!empty($autocomplete) && $autocomplete!="off") {
			//update VG 09/06/2011 : personnalisation de la fonction d'autocompletion
			$funcAutocomplete = (!empty($attr['AUTOCOMPLETE_FUNC'])?htmlentities($attr['AUTOCOMPLETE_FUNC']):"initAutoComplete");
			$quoteSuggestions = (!empty($attr['AUTOCOMPLETE_QUOTE'])?"".$attr['AUTOCOMPLETE_QUOTE']:"false");
			$autocomplete_urlaction = (!empty($attr['AUTOCOMPLETE_URLACTION'])?"".$attr['AUTOCOMPLETE_URLACTION']:"chercheMotsSolr");
			$add_str = "" ; 
			if(isset($attr['SET_ID_FONDS']) && !empty($attr['SET_ID_FONDS'])){
				$add_str.="&id_fonds=".$this->variables[$attr['SET_ID_FONDS']];
			}
			
			//if ($this->entity=='DOC' && $this->chercheObj && defined('useSolr') && useSolr==true)
			if (get_class($this->chercheObj) == "RechercheSolr")
			{
				print "<script type='text/javascript'>
				document.getElementById('".$attr['ID']."').onfocus=function () {".$funcAutocomplete."(this.form, document.getElementById('".$attr['ID']."'),null,'blank.php?urlaction=".$autocomplete_urlaction."&type=".$autocomplete.$add_str."',null,".$quoteSuggestions.")}
				</script>";
			}
			else
			{
				if($autocomplete == "chercheAdresse"){
					print "<script type='text/javascript'>
					document.getElementById('".$attr['ID']."').onfocus=function () {".$funcAutocomplete."(this.form, document.getElementById('".$attr['ID']."'),null,'blank.php?urlaction=chercheAdresse',null,".$quoteSuggestions.")}
					</script>";
				}else{
					print "<script type='text/javascript'>
					document.getElementById('".$attr['ID']."').onfocus=function () {".$funcAutocomplete."(this.form, document.getElementById('".$attr['ID']."'),null,'blank.php?urlaction=chercheMots&type=".$autocomplete.(isset($attr['CHAR_MIN'])?"&char_min=".$attr['CHAR_MIN']:"")."',null,".$quoteSuggestions.")}
					</script>";
				}
										
			}
		}
	}

	//LD 09 12 2008 => ajout d'une image (utilisé pour les boutons Index)
	function addImage($type, $nom, $val, $attr=null){
		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		if($val) $attr['VALUE']=(defined($val)?constant($val):$val);

		if((!isset($attr['SRC']) || empty($attr['SRC'])) && isset($attr['VALUE']) && !empty($attr['VALUE'])){
			$attr['SRC'] = $attr['VALUE'] ; 
		}
		if(isset($attr['SRC']) && !empty($attr['SRC'])&&isset($attr['SRC_PREFIX']) && !empty($attr['SRC_PREFIX'])){
			$attr['SRC'] = $attr['SRC_PREFIX'].$attr['SRC'] ; 
		}
		
		print "<img ";
		foreach($attr as $i=>$x) {
			print " $i=\"".str_replace('"',"'",$x)."\"";
		}
		print "/>\n";
	}



	/**
	* Affiche un champ textarea
	 * IN : nom, valeur par défaut, tableau de paramètres
	 * OUT : affichage du champ
	 **/

	function addTextarea($nom, $val, $attr=null){
		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		if($val) $val=(defined($val)?constant($val):$val);
		unset($attr['VALUE']);
		unset($attr['TYPE']);

		// VP 7/04/11 : ajout autocomplete pour textarea avec séparateur
		if(isset($attr['AUTOCOMPLETE'])) {
			$autocomplete=$attr['AUTOCOMPLETE'];
			$attr['AUTOCOMPLETE']="off";
		}

		print "<textarea ";
		foreach($attr as $i=>$x) {
			if( $i== 'PLACEHOLDER' && defined($attr[$i]))
				print " ".strtolower($i)."=\"".str_replace('"',"&quot;",constant($x))."\"";
			else
				print " $i=\"$x\"";
		}

		print ">".$val."</textarea>\n";
		if(!empty($autocomplete) && $autocomplete!="off") {
			$funcAutocomplete = (!empty($attr['AUTOCOMPLETE_FUNC'])?htmlentities($attr['AUTOCOMPLETE_FUNC']):"initAutoComplete");
			$quoteSuggestions = (!empty($attr['AUTOCOMPLETE_QUOTE'])?"".$attr['AUTOCOMPLETE_QUOTE']:"false");
			$add_str = "" ; 
			if(isset($attr['SET_ID_FONDS']) && !empty($attr['SET_ID_FONDS'])){
				$add_str.="&id_fonds=".$this->variables[$attr['SET_ID_FONDS']];
			}
			
			//if ($this->entity=='DOC' && defined('useSolr') && useSolr==true)
			if (get_class($this->chercheObj)=="RechercheSolr" )
			{
				print "<script type='text/javascript'>
					document.getElementById('".$attr['ID']."').onfocus=function () {".$funcAutocomplete."(this.form, document.getElementById('".$attr['ID']."'),null,'blank.php?urlaction=chercheMotsSolr&type=".$autocomplete.$add_str."',null,".$quoteSuggestions.")}
					</script>";
			}
			else
			{
				if($autocomplete == "chercheAdresse"){
					print "<script type='text/javascript'>
					document.getElementById('".$attr['ID']."').onfocus=function () {initAutoComplete(this.form, document.getElementById('".$attr['ID']."'),null,'blank.php?urlaction=chercheAdresse&sep=".urlencode(',')."')}
					</script>";
				}else{
					print "<script type='text/javascript'>
					document.getElementById('".$attr['ID']."').onfocus=function () {".$funcAutocomplete."(this.form, document.getElementById('".$attr['ID']."'),null,'blank.php?urlaction=chercheMots&sep=".urlencode(',')."&type=".$autocomplete.$add_str."',null,".$quoteSuggestions.")}
					</script>";
				}
			}
		}
	}

	/**
	 * Affiche un champ Rich Text Editor (fckEditor)
	 * IN : nom, valeur par défaut, tableau de paramètres
	 * OUT : affichage du champ
	 **/
// VP 27/11/09 : ajout RTEditor (fckEditor)
	function addRTEditor($nom, $val, $attr=null){
		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		if($val) $val=(defined($val)?constant($val):$val);
		unset($attr['VALUE']);
		unset($attr['TYPE']);

		include_once (gFCKEditorPath."fckeditor.php");

		$oFCKeditor = new FCKeditor($nom) ;
		$oFCKeditor->BasePath	= gFCKEditorPath;

		if(isset($attr['HEIGHT'])) $oFCKeditor->Height	= $attr['HEIGHT']  ;
		if(isset($attr['WIDTH'])) $oFCKeditor->Width	= $attr['WIDTH']  ;
		if(isset($attr['FCK_TOOLBARSET'])) $oFCKeditor->ToolbarSet = $attr['FCK_TOOLBARSET']; // 'Basic', 'Default', 'Custom' créé spécifiquement
		$oFCKeditor->Value		= $val ;
		$oFCKeditor->Config['DefaultLanguage']=strtolower($_SESSION["langue"]);
		$oFCKeditor->Create() ;
		unset($oFCKeditor);
	}

	/**
	* Affiche un bouton index
	 * IN : nom, elt HTML de positionnement, tableau de paramètres
	 * OUT : affichage du champ
	 **/

	function addIndexBtn($indexName, $indexChoose, $attr){
		//by LD 30/10/08 : ajout test sur defined INDEX + nv param INDEX_CLASS
		// +nv param index_img (cas image) + nv param index_type (button par déft, mais peut-être image ou submit)
		//by LD nv param INDEX_LIB pour le libellé de la palette
		//VP 27/07/09 ajout param INDEX_W et INDEX_H pour la taille de la palette
		$label=defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'];
		if ($label=='') $label=defined($attr['INDEX_LIB'])?constant($attr['INDEX_LIB']):$attr['INDEX_LIB'];
		if ($label=='') $label=defined($attr['CHLIBS'])?constant($attr['CHLIBS']):$attr['CHLIBS'];
		// VP 13/04/10 : prise en compte paramètre dspAsso
		// VP 28/06/11 : prise en compte paramètre INDEX_TRI
		// VP 07/06/12 : prise en compte paramètre INDEX_TYPE_DOC
		// VP 28/09/12 : prise en compte paramètre INDEX_VAL_ID_GEN
		// MS 13/02/13 : prise en compte paramètre INDEX_TYPE_PRIV (privilège)
		// MS 29/08/14 : prise en compte paramètre INDEX_LEX_MODE (rempli le champ 'mode' lorsqu'on ne passe pas par la définition 'desc_champ_appelant' combinée, voir chercheIndex2)
		if(!isset($attr['INDEX_TYPE_LEX']) && isset($attr['ID_TYPE_LEX']))
			$attr['INDEX_TYPE_LEX']=$attr['ID_TYPE_LEX'];
		$params="\"titre_index=".urlencode($label)."&valeur=&champ=".$attr['INDEX_CHAMP']
		.(isset($attr['ID_LANG'])?"&id_lang=".$attr['ID_LANG']:"&id_lang=".$_SESSION['langue'])
		.(isset($attr['INDEX_TYPE_LEX'])?"&type_lex=".$attr['INDEX_TYPE_LEX']:"")
		.(isset($attr['INDEX_TYPE_CAT'])?"&type_cat=".$attr['INDEX_TYPE_CAT']:"")
		.(isset($attr['INDEX_TYPE_DOC'])?"&type_doc=".$attr['INDEX_TYPE_DOC']:"")
		.(isset($attr['INDEX_TYPE_PRIV'])?"&type_priv=".$attr['INDEX_TYPE_PRIV']:"")
		.(isset($attr['INDEX_TYPE_US_MIN'])?"&type_us_min=".$attr['INDEX_TYPE_US_MIN']:"")
		.(isset($attr['INDEX_LEX_AFF'])?"&lex_aff=".$attr['INDEX_LEX_AFF']:"")
		.(isset($attr['INDEX_VAL_ID_GEN'])?"&val_id_gen=".$attr['INDEX_VAL_ID_GEN']:"")
		.(isset($attr['INDEX_DSPASSO'])?"&dspAsso=".$attr['INDEX_DSPASSO']:"")
		.(isset($attr['INDEX_XSL'])?"&xsl=".$attr['INDEX_XSL']:"")
		.(isset($attr['INDEX_NB'])?"&affich_nb=".$attr['INDEX_NB']:"")
		.(isset($attr['INDEX_TBL'])?"&tbl=".$attr['INDEX_TBL']:"")
		.(isset($attr['INDEX_TRI'])?"&tri=".$attr['INDEX_TRI']:"").""
		.(isset($attr['INDEX_RTN'])?"&rtn=".$attr['INDEX_RTN']:"").""
		.(isset($attr['INDEX_FILTER'])?"&filter=".$attr['INDEX_FILTER']:"").""
		.(isset($attr['INDEX_LEX_MODE'])?"&mode=".$attr['INDEX_LEX_MODE']:"").""
		.(isset($attr['INDEX_APPLYDROIT'])?"&applydroit=".$attr['INDEX_APPLYDROIT']:"")."\"";
		if(!isset($indexChoose)) $indexChoose="this";

		if(isset($attr['INDEX_JS_FUNC']))
			$indexJsFunc=htmlentities($attr['INDEX_JS_FUNC']);
		else
			$indexJsFunc="choose";

		if (!isset($attr['INDEX_W']))
			$attr['INDEX_W']=0;

		if (!isset($attr['INDEX_H']))
			$attr['INDEX_H']=0;

		$tabIndexParam['ONCLICK']=$indexJsFunc."(".$indexChoose.",".$params.",'',".(0+$attr['INDEX_W']).",".(0+$attr['INDEX_H']).");return false;"; //by LD 30/10/08 ajout return false pour ne pas soumettre le formulaire (btn image)
		if (isset($attr['INDEX_CLASS']) && $attr['INDEX_CLASS']) $tabIndexParam['CLASS']=$attr['INDEX_CLASS'];
		if (isset($attr['INDEX_IMG']) && $attr['INDEX_IMG']) $tabIndexParam['SRC']=imgUrl.$attr['INDEX_IMG'];
		if (!isset($attr['INDEX_TYPE']) || !$attr['INDEX_TYPE']) $attr['INDEX_TYPE']='button'; //sécurité

		$tabIndexParam['STYLE']="cursor:pointer;";
		if (isset($attr['INDEX_STYLE']))
			$tabIndexParam['STYLE'].=$attr['INDEX_STYLE'];

		//by ld 09 12 08 : addInput remplacé par nv fonction addImage si type image
		//car les input type=image induisent de nbx problèmes (soumission si entrée, ou appel incongru de palette)
		if ($attr['INDEX_TYPE']=='image') $this->addImage($attr['INDEX_TYPE'], $indexName, defined($attr['INDEX'])?constant($attr['INDEX']):$attr['INDEX'], $tabIndexParam);
		else
		$this->addInput($attr['INDEX_TYPE'], $indexName, defined($attr['INDEX'])?constant($attr['INDEX']):$attr['INDEX'], $tabIndexParam);



	}
/**
* Affiche des checkbox
* IN : type checkbox , nom, tableau de paramètres
* OUT : affichage du champ
**/
// VP 3/07/09 : ajout paramètre pour fixer le nb de checkbox par ligne dans listCheckbox()
// VP 27/07/09 : ajout cas FI
// MS 23/02/2012 : transfert de l'attribut "toinsert" en tant que code a inserer à la fonction listCheckBox
	function addCheckbox($type, $nom, $val, $attr){
		$nom.="[]";
		if(empty($type)) $type=$attr['CHTYPES'];
		switch($type){
			case "VI":
				// PC 03/04/12 : ajout du parametre de tri
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				if(isset($attr['TYPE_VAL'])) $type_val=$attr['TYPE_VAL'];
				else $type_val=$attr['CHFIELDS'];
				listCheckbox($nom,"SELECT ID_VAL,VALEUR FROM t_val where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='". $_SESSION["langue"]."' order by $tri",
							 array("ID_VAL","VALEUR"), $val,1,$attr['TOINSERT'],$attr['NB_CHECKBOX']);
				break;
			case "V":
				// PC 03/04/12 : ajout du parametre de tri
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				if(isset($attr['TYPE_VAL'])) $type_val=$attr['TYPE_VAL'];
				else $type_val=$attr['CHFIELDS'];
				listCheckbox($nom,"SELECT ID_VAL,VALEUR FROM t_val where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='". $_SESSION["langue"]."' order by $tri",
							 array("VALEUR","VALEUR"), $val,1,$attr['TOINSERT'],$attr['NB_CHECKBOX']);
				break;
			case "FI":
				// VP 19/05/10 : utilisation tableau pour liste_id
				foreach($_SESSION["USER"]["US_GROUPES"] as $v){
					if ($v["ID_PRIV"]!=0 && !empty($v["ID_FONDS"])) $liste_id[]=$v["ID_FONDS"];
				}
				if(count($liste_id)>0){
					$tri=(isset($attr['TRI'])?$attr['TRI']:"FONDS_COL");
					listCheckbox($nom,"select DISTINCT ID_FONDS, FONDS_COL from t_fonds WHERE ID_FONDS in ('".implode("','",$liste_id)."') or ID_FONDS_GEN in ('".implode("','",$liste_id)."')  order by FONDS_COL",
							 Array("ID_FONDS","FONDS_COL"),$val,1,$attr['TOINSERT'],$attr['NB_CHECKBOX']);
				}
				break;

			case "CE":
			case "CS":
			case "C":
			default:
				if(strtoupper(substr($nom,0,6))=="PANXML" || strtoupper(substr($nom,0,5))=="USXML" ){
					$nom.="[]";
				}
				//$attr['CHECKBOX_SQL'] : "select ID_XX as ID, XX as VAL ...
				// langue à substituer éventuellement
				// VP 4/02/10 : ajout cas CHECKBOX_OPTION/CHECKBOX_LIB
				if(isset($attr['CHECKBOX_SQL'])) {
					$sql=$attr['CHECKBOX_SQL'];
					if((strpos($sql,"ID_LANG='FR'")>0) && strtoupper($_SESSION['langue'])!='FR') $sql=str_replace("ID_LANG='FR'","ID_LANG='".$_SESSION['langue']."'",$sql);
					print listCheckbox($nom,$sql,Array("ID","VAL"),$val,1,$attr['TOINSERT'],$attr['NB_CHECKBOX'],$attr['SPAN_DISPLAY']);
				}elseif(isset($attr['CHECKBOX_OPTION']) && isset($attr['NB_CHECKBOX'])) {
						$cols=$attr['NB_CHECKBOX'];
						$pourcent= intval(100 / $cols);
						$tabOptions=explode(",",$attr['CHECKBOX_OPTION']);
						$tabLibs=explode(",",$attr['CHECKBOX_LIB']);
						print '<table id="tabCheckbox'.$attr['CHFIELDS'].'" width="100%" border="0">';
						$k=0;
						foreach($tabOptions as $i=>$x) {
							if(($k % $cols) == 0) print "<tr>";
							$k++;
							print "<td width='".$pourcent."%' valign='".(isset($attr['VALIGN'])?$attr['VALIGN']:"top")."'>";
							$checked="";
							if((is_array($val) && in_array($tabOptions[$i],$val)) || ($val==$tabOptions[$i])) $checked=" checked";
							print "<input type=\"checkbox\" name=\"".$nom."\" value=\"".$tabOptions[$i]."\"".$checked." ".((isset($attr['ONCHANGE'])&&!empty($attr['ONCHANGE']))?'onchange="'.$attr['ONCHANGE'].'"':'')."/>";
							print "<label>".(defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i])."</label>&nbsp;&nbsp;&nbsp;";
							print "</td>";
							if(($k % $cols) == 0) print "</tr>";
						}
						print "</table>";
				}elseif(isset($attr['CHECKBOX_OPTION'])) {
						$tabOptions=explode(",",$attr['CHECKBOX_OPTION']);
						$tabLibs=explode(",",$attr['CHECKBOX_LIB']);
						if(!function_exists('interpret_constant')){
							function interpret_constant($n){
								return (defined($n)?constant($n):$n);
							}
						}
						$interpreted_tabOptions = array_map('interpret_constant',$tabOptions);
						foreach($tabOptions as $i=>$x) {
                            // VP 10/05/12 : ajout eval() si CHECKBOX_OPTION contient return
                            if(strpos($tabOptions[$i],"return")!==false) {
                                $tabOptions[$i]=eval($tabOptions[$i]);
                            }
							$checked="";
 							if((is_array($val) && (in_array($tabOptions[$i],$val)  || in_array($interpreted_tabOptions[$i],$val))) || ($val==$tabOptions[$i] || $val == $interpreted_tabOptions[$i])) $checked=" checked";
							print '<span>';
							print "<input id=\"".$nom.$i."\" type=\"checkbox\" name=\"".$nom."\" value=\"".(defined($tabOptions[$i])?constant($tabOptions[$i]):$tabOptions[$i])."\"".$checked." ".((isset($attr['ONCHANGE'])&&!empty($attr['ONCHANGE']))?'onchange="'.$attr['ONCHANGE'].'"':'')."/>";
							// MS - on ajoute un noeud label autour du label => permet d'éviter des problemes de CSS entre divers navigateurs => les noeuds textes ne sont pas tjs interprétés pareils ex : Safari / Chrome flexbox
							$label_str = (defined($tabLibs[$i])?"<label for=\"".$nom.$i."\">".constant($tabLibs[$i])."</label>":"<label>".$tabLibs[$i]."</label>");
							print $label_str;
							print '</span>';
						}
                }elseif($this->entity=="MAT"){
                        print listCheckbox($nom,"select distinct ".$attr['CHFIELDS']." from t_mat where ".$attr['CHFIELDS']."!='' order by 1",$attr['CHFIELDS'],$val, $tabId,1,$attr['TOINSERT'],$attr['NB_CHECKBOX']);
                }else if($this->entity=="DOC"){
                        print listCheckbox($nom,"select distinct ".$attr['CHFIELDS']." from t_doc where ".$attr['CHFIELDS']."!='' and ID_LANG='".$_SESSION['langue']."' order by 1",$champ,$val,1,$attr['TOINSERT'],$attr['NB_CHECKBOX']);
                }else if($this->entity=="DOCMAT"){
                        print listCheckbox($nom,"select distinct ".$attr['CHFIELDS']." from t_doc_mat where ".$attr['CHFIELDS']."!='' order by 1",$champ,$val,1,$attr['TOINSERT'],$attr['NB_CHECKBOX']);
				}else{
					//VP (17/10/08) ajout cas checkbox de base + LD 04/03/09 statut checked
					// VP 25/09/09 : cas checked=false

					//B.RAVI  2015-09-17 si on a fait 1 recherche avec la checkbox coché, il override par défault <checked>false</checked> du xml
					if ( $val==$attr['VALUE'] || in_array($attr['VALUE'],$val)){
						$attr['CHECKED']=true;
					}elseif($attr['CHECKED']=="false"){
						unset($attr['CHECKED']);
					}

                                        //B.RAVI bug sur la generation html de la checkbox qui n'avait pas l'attribut name="chValue[x]"
                                        if (trim($attr['NAME'])!='') $nom=$attr['NAME'];
					$this->addInput("checkbox", $nom, $attr['VALUE'], $attr);
				}
				break;
		}
	}
/**
* Affiche des boutons radio
 * IN : type radio , nom, tableau de paramètres
 * OUT : affichage du champ
 **/

	function addRadio($type, $nom, $val, $attr){
		if(!isset($type)) $type=$attr['CHTYPES'];
		switch($type){
			case "VI":
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				listRadio($nom,"SELECT ID_VAL,VALEUR FROM t_val where VAL_ID_TYPE_VAL='".$attr['CHFIELDS']."' and ID_LANG='". $_SESSION["langue"]."' order by VAL_CODE,VALEUR",
						  array("ID_VAL","VALEUR"), $val, 1, $attr['NB_RADIO']);
				break;

			default:
				//$attr['RADIO_SQL'] : "select ID_XX as ID, XX as VAL ...
				// langue à substituer éventuellement
				if(isset($attr['RADIO_SQL'])) {
					$sql=$attr['RADIO_SQL'];
					if((strpos($sql,"ID_LANG='FR'")>0) && strtoupper($_SESSION['langue'])!='FR') $sql=str_replace("ID_LANG='FR'","ID_LANG='".$_SESSION['langue']."'",$sql);
					// VP 17/03/10 : correction bug sur appel listRadio
					print listRadio($nom,$sql,Array("ID","VAL"),$val);
				}
				// VP 6/01/09 : ajout cas SELECT_OPTION/SELECT_LIB
				// VG 19/04/11 : ajout de la possibilité d'utiliser une constante pour la valeur
				if(isset($attr['RADIO_OPTION'])) {
					$tabOptions=explode(",",$attr['RADIO_OPTION']);
					$tabLibs=explode(",",$attr['RADIO_LIB']);

					if(isset($attr['NB_RADIO'])) {
						$cols=$attr['NB_RADIO'];
						$pourcent= intval(100 / $cols);
						print '<table id="tabRadio'.$attr['CHFIELDS'].'" width="100%" border="0">';
					}
					//update VG 19/04/11
					$k=0;
					if(isset($attr["RADIO_ALL"]) && $attr["RADIO_ALL"] == '1') {
						if(isset($cols) && ($k % $cols) == 0) print "<tr>";
						$k++;
						if(isset($cols))
							print "<td width='".$pourcent."%' valign='".(isset($attr['VALIGN'])?$attr['VALIGN']:"top")."'>";
						$allOptions = "";
						foreach ($tabOptions as $j => $optionVal) {
							if(!empty($allOptions))
								$allOptions.= ",";
							$allOptions .= (defined($tabOptions[$j])?constant($tabOptions[$j]):$tabOptions[$j]);
						}
						print "<input type=\"radio\" name=\"".$nom."\" id=\"".$nom.$allOptions."\" value=\"".$allOptions."\"".(($allOptions==$val || empty($val))?" checked":"")."/><label for='".$nom.$allOptions."'>".kTous."</label>";
						if(isset($cols))
							print "</td>";
						if(isset($cols) && ($k % $cols) == 0) print "</tr>";
					}
					foreach($tabOptions as $i=>$x) {
						if(isset($cols) && ($k % $cols) == 0) print "<tr>";
						$k++;
						if(isset($cols))
							print "<td width='".$pourcent."%' valign='".(isset($attr['VALIGN'])?$attr['VALIGN']:"top")."'>";
						//PC 21/03/13 : sélection par défaut du premier élément si le paramêtre RADIO_DEFAULT est renseigné
						$value = defined($tabOptions[$i])?constant($tabOptions[$i]):$tabOptions[$i];
						print "<input type=\"radio\"  name=\"".$nom."\" id=\"".$nom.$value."\" value=\"".(defined($tabOptions[$i])?constant($tabOptions[$i]):$tabOptions[$i])."\"".(($tabOptions[$i]==$val)||(empty($val)&&$i==0&&$attr["RADIO_DEFAULT"])?" checked":"")."/><label for='".$nom.$value."'>".(defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i])."</label>&nbsp;&nbsp;&nbsp;";
						if(isset($cols))
							print "</td>";
						if(isset($cols) && ($k % $cols) == 0) print "</tr>";
					}
					if(isset($cols))
						print '</table>';
				}
				break;
		}
	}

/**
* Affiche un champ select
 * IN : type select , nom, tableau de paramètres
 * OUT : affichage du champ
 **/
	function addSelect($type, $nom, $val, $attr){
		// VP 25/06/09 : traitement attribut TYPE_VAL pour le cas VAL_ID_TYPE_VAL != ID_TYPE_VAL
		print "<select name='".$nom;
		if (isset($attr['MULTIPLE']) && $attr['MULTIPLE']) print"[]"; //by LD 29/10/08 : cas des select multiples
		//update VG 22/02/2012 : on set l'ID par $attr['ID'] mis à $nom par défaut plutot que $nom directement
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		print"' id='".$attr['ID']."'";
		//if(!isset($attr['ID'])) $attr['ID']=$nom;
		$attr['LABEL']=defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'];
		foreach($attr as $i=>$x) {
			if (!in_array(strtolower($i),array('select_sql','chfields','select_type','chops','type','chlibs','chtypes','trtags','label','value','like_sql_title','pan_sql')))
				print " $i=\"$x\"";
		}
		//if(isset($attr['CLASS'])) print " CLASS='".$attr['CLASS']."'";
		print ">\n";
		// VP 16/09/10 : ajout attribut NOALL
		if (!isset($attr['NOALL']) || !$attr['NOALL']) {
			print "<option value=''>".kTous."</option>";
		}
		// MS 21/10/13 - ajout de '|| empty($type)' au test car si $type est vide, il est quand meme set => on ne récupère donc pas le CHTYPES comme type
		if(!isset($type) || empty($type)) $type=$attr['CHTYPES'];
		// VP 30/11/09 : ajout paramètre TRI
		switch($type){
			case "V":
				if(isset($attr['TYPE_VAL'])) $type_val=$attr['TYPE_VAL'];
				else $type_val=$attr['CHFIELDS'];
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				print listOptions("select VALEUR from t_val where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='".$_SESSION['langue']."' order by $tri","VALEUR",$val);
				break;

			case "VI":
				require_once(libDir."class_user.php");
				$myUsr=User::getInstance();
				if(isset($attr['TYPE_VAL'])) $type_val=$attr['TYPE_VAL'];
				else $type_val=$attr['CHFIELDS'];
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"v.VAL_CODE,v.VALEUR");
				//PC 18/01/11 : ajout possibilité d'ajouter les droits au listes déroulantes
				//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
				if (isset($attr['APPLYDROIT']) &&
				(	(defined('gSeuilAppliqueDroits') && $myUsr->getTypeLog() < intval(gSeuilAppliqueDroits))
					||(!defined('gSeuilAppliqueDroits') && $myUsr->getTypeLog() < kLoggedDoc))){
	            	require_once(libDir."class_chercheDoc.php");
				    $mySearchDoc=new RechercheDoc();
				    $mySearchDoc->appliqueDroits();
				    $sql_droit =  $mySearchDoc->sqlRecherche;
				    unset($mySearchDoc);
				    listOptions("select v.ID_VAL,v.VALEUR
				    				from t_val v
				    				inner join t_doc_val dv ON dv.ID_VAL = v.ID_VAL
				    				inner join t_doc t1 on t1.ID_DOC = dv.ID_DOC
				    				where v.VAL_ID_TYPE_VAL='".$type_val."' $sql_droit and v.ID_LANG='".$_SESSION['langue']."' group by v.ID_VAL,v.VALEUR,v.VAL_CODE order by $tri",Array("ID_VAL","VALEUR"),$val);
				}
				else
					print listOptions("select ID_VAL,VALEUR from t_val v where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='".$_SESSION['langue']."' order by $tri",Array("ID_VAL","VALEUR"),$val);
				break;

			case "F":
				require_once(libDir."class_user.php");
				$myUsr=User::getInstance();
				// VP 20/11/08 : modif liste_id
				// VP 19/05/10 : utilisation tableau pour liste_id
				//XB 6/02/14 : si admin on voit tout
				//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
				if((defined('gSeuilAppliqueDroits') && $myUsr->Type >= intval(gSeuilAppliqueDroits) )
				||(!defined('gSeuilAppliqueDroits') && $myUsr->Type >= kLoggedDoc )){
					$tri=(isset($attr['TRI'])?$attr['TRI']:"FONDS_COL");
					print listOptions("select DISTINCT FONDS,FONDS_COL from t_fonds where ID_LANG='".$_SESSION['langue']."' order by $tri",Array("FONDS","FONDS_COL"),$val);

				}
				//XB
				else{
				foreach($_SESSION["USER"]["US_GROUPES"] as $v){
					if ($v["ID_PRIV"]!=0 && !empty($v["ID_FONDS"])) $liste_id[]=$v["ID_FONDS"];
				}
				if(count($liste_id)>0){
					$tri=(isset($attr['TRI'])?$attr['TRI']:"FONDS_COL");
					print listOptions("select DISTINCT FONDS,FONDS_COL from t_fonds WHERE (ID_FONDS in ('".implode("','",$liste_id)."') or ID_FONDS_GEN in ('".implode("','",$liste_id)."')) and ID_LANG='".$_SESSION['langue']."' order by $tri",Array("FONDS","FONDS_COL"),$val);
				}
			}
				break;

			case "FI":
				$fonds_champ = (isset($attr['FONDS_CHAMP'])?$attr['FONDS_CHAMP']:"FONDS_COL");
				require_once(libDir."class_user.php");
				$myUsr=User::getInstance();
				// VP 20/11/08 : modif liste_id
				// VP 19/05/10 : utilisation tableau pour liste_id
				//XB 6/02/14 : si profil >= kLoggedDoc on voit tout
				//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
				if((defined('gSeuilAppliqueDroits') && $myUsr->Type >= intval(gSeuilAppliqueDroits) )
				||(!defined('gSeuilAppliqueDroits') && $myUsr->Type >= kLoggedDoc )){
					$tri=(isset($attr['TRI'])?$attr['TRI']:$fonds_champ);
					print listOptions("select DISTINCT ID_FONDS, $fonds_champ from t_fonds  where ID_LANG='".$_SESSION["langue"]."' order by $tri",Array("ID_FONDS",$fonds_champ),$val);
				
				}
				else{
				//XB
				foreach($_SESSION["USER"]["US_GROUPES"] as $v){
					if ($v["ID_PRIV"]!=0 && !empty($v["ID_FONDS"])) $liste_id[]=$v["ID_FONDS"];
				}
				if(count($liste_id)>0){
					$tri=(isset($attr['TRI'])?$attr['TRI']:$fonds_champ);
					//PC 28/03/11 : Ajout du filtre par langue
					print listOptions("select DISTINCT ID_FONDS, $fonds_champ from t_fonds WHERE (ID_FONDS in ('".implode("','",$liste_id)."') or ID_FONDS_GEN in ('".implode("','",$liste_id)."')) and ID_LANG='".$_SESSION["langue"]."' order by $tri",Array("ID_FONDS",$fonds_champ),$val);
				}
				}
				break;

			case "FP":
				require_once(libDir."class_user.php");
				$myUsr=User::getInstance();
				//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
				if((defined('gSeuilAppliqueDroits') && $myUsr->Type >=  intval(gSeuilAppliqueDroits))
				||(!defined('gSeuilAppliqueDroits') && $myUsr->Type >= kLoggedDoc )){
					$tri=(isset($attr['TRI'])?$attr['TRI']:"FONDS");
					print listOptions("select DISTINCT ID_FONDS,FONDS from t_fonds where ID_FONDS_GEN = 0 AND ID_LANG='".$_SESSION["langue"]."' order by $tri",Array("FONDS","FONDS"),$val);
				}
				else{
					foreach($_SESSION["USER"]["US_GROUPES"] as $v){
						if ($v["ID_PRIV"]!=0 && !empty($v["ID_FONDS"])) $liste_id[]=$v["ID_FONDS"];
					}
					if(count($liste_id)>0){
						$tri=(isset($attr['TRI'])?$attr['TRI']:"FONDS");
						//PC 28/03/11 : Ajout du filtre par langue
						print listOptions("select DISTINCT FONDS from t_fonds WHERE (ID_FONDS in ('".implode("','",$liste_id)."') and ID_FONDS_GEN = 0) and ID_LANG='".$_SESSION["langue"]."' order by $tri",Array("FONDS","FONDS"),$val);
					}
				}
				break;

			case "C":
			case "CE":
			case "CS":
				if(isset($attr['SELECT_SOLR_INDEX'])){
					listOptionsSolr($this->chercheObj,$attr['SELECT_SOLR_INDEX'],$val,1,((isset($attr['SELECT_SOLR_INDEX_ALPHASORT']) && !empty($attr['SELECT_SOLR_INDEX_ALPHASORT']))?true:false));
					
				}else if(isset($attr['SELECT_SQL'])) {
					$sql=$attr['SELECT_SQL'];
					if((strpos($sql,"ID_LANG='FR'")>0) && strtoupper($_SESSION['langue'])!='FR') $sql=str_replace("ID_LANG='FR'","ID_LANG='".$_SESSION['langue']."'",$sql);
					print listOptions($sql,Array("ID","VAL"),$val);
				}				
				elseif(isset($attr['SELECT_OPTION'])) {
					$tabOptions=explode(",",$attr['SELECT_OPTION']);
					$tabLibs=explode(",",$attr['SELECT_LIB']);
					foreach($tabOptions as $i=>$x) {
						//PC 19/03/13 : ajout eval() si SELECT_OPTION contient return
						if(strpos($tabOptions[$i],"return")!==false) {
							$tabOptions[$i]=eval($tabOptions[$i]);
						} 
						print "<option value=\"".$tabOptions[$i]."\"".($tabOptions[$i]==$val?" selected":"").">".(defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i])."</option>";
					}
				}			
				elseif($this->entity=="MAT")
					print listOptions("select distinct ".$attr['CHFIELDS']." from t_mat order by 1",strtoupper($attr['CHFIELDS']),$val);
				else if($this->entity=="DOC")
					print listOptions("select distinct ".$attr['CHFIELDS']." from t_doc where ID_LANG='".$_SESSION['langue']."' order by 1",strtoupper($attr['CHFIELDS']),$val);
				else if($this->entity=="DOCMAT")
					print listOptions("select distinct ".$attr['CHFIELDS']." from t_doc_mat where ID_LANG='".$_SESSION['langue']."' order by 1",strtoupper($attr['CHFIELDS']),$val);
					break;

			case "PAN":
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"pan_titre,id_panier");
                                $sql="select id_panier, pan_titre, pan_dossier FROM t_panier WHERE pan_titre<>'' ";
                                if(isset($attr['LIKE_SQL_TITLE'])){
                                    $sql.=" AND pan_titre LIKE '%".$attr['LIKE_SQL_TITLE']."%'";
                                }

                                /* B.RAVI 2015-11-30 Traitement spécial nous avons besoin d'avoir le DERNIER enfant dans une arborescence de t_panier*/
                                if(isset($attr['PAN_ID_GEN'])){
                                    if (defined($attr['PAN_ID_GEN'])) {
                                        $pan_id_gen=constant ($attr['PAN_ID_GEN']);
                                    }else{
                                        $pan_id_gen=$attr['PAN_ID_GEN'];
                                    }
                                    $sql.=" AND pan_id_gen ='".$pan_id_gen."'";
                                    $sql.=" ORDER BY $tri";
                                    global $db;
                                    $arrPan = $db->CacheGetAll(300, $sql);

//                                    echo 'sql '.$sql;


//                                    echo '<div style="position:absolute;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_BR $arrPan<pre>';
//                                    var_dump($arrPan);
//                                    echo '</pre></div><br />';
                                    $arrPanFinal=array();
                                    foreach($arrPan as $v){
                                        //Si dossier => on recherche les 1ers enfants //TODO faire fct recursive pour trouver les derniers enfants
                                        if ($v['PAN_DOSSIER']){
                                            $sql2="select id_panier, pan_titre, pan_dossier FROM t_panier WHERE PAN_DOSSIER=0 AND pan_id_gen=".(int)$v["ID_PANIER"];                 $arrPan2 = $db->CacheGetAll(300, $sql2);
                                            if ($arrPan2){
                                                foreach ($arrPan2 as $v2){
                                                    $arrPanFinal[$v2["ID_PANIER"]]= $v2['PAN_TITRE'];
                                                }
                                            }

                                        }else{
                                            $arrPanFinal[$v["ID_PANIER"]]=$v['PAN_TITRE'];
                                        }
                                    }

//                                    echo $sql;
//                                    echo '<div style="position:absolute;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_BR $arrPanFinal<pre>';
//                                    var_dump($arrPanFinal);
//                                    echo '</pre></div><br />';
                                     print listOptionsFromArray($arrPanFinal,$val);
                                }
                                else{


                                    $sql.=" ORDER BY $tri";
									
									//besoin sur gdf, pour selectionner uniquement le panier + selections de l'utilisateur  par ex
									if (isset($attr['PAN_SQL'])) {
										$sql = $attr['PAN_SQL'];
										if ((strpos($sql, "PAN_ID_USAGER=1") > 0) && $_SESSION["USER"]["ID_USAGER"] != '1') {
											$sql = str_replace("PAN_ID_USAGER=1", "PAN_ID_USAGER=" . (int) $_SESSION["USER"]["ID_USAGER"], $sql);
										}
									}
									
//                                    echo $sql;
                                    print listOptions($sql,Array("ID_PANIER","PAN_TITRE"),$val);
                                }
				break;

			case "UF":
				require_once(libDir."class_user.php");
				$myUsr=User::getInstance();
				$tri=(isset($attr['TRI'])?$attr['TRI']:"US_NOM");
				if((defined('gSeuilAppliqueDroits') && $myUsr->Type >=  intval(gSeuilAppliqueDroits))
				||(!defined('gSeuilAppliqueDroits') && $myUsr->Type >= kLoggedDoc )){
					$tri=(isset($attr['TRI'])?$attr['TRI']:"US_NOM");
					print listOptions("select DISTINCT ID_USAGER,US_NOM from t_usager order by $tri",Array("US_NOM"),$val);
				}
				else{
				foreach($_SESSION["USER"]["US_GROUPES"] as $v){
					if ($v["ID_PRIV"]!=0 && !empty($v["ID_FONDS"])) $liste_id[]=$v["ID_FONDS"];
				}
					if(count($liste_id)>0){
						//PC 28/03/11 : Ajout du filtre par langue
						print listOptions("select DISTINCT u.ID_USAGER, US_NOM
											from t_usager u
											inner join t_usager_groupe ug ON ug.id_usager= u.id_usager
											inner join t_groupe_fonds gf ON gf.id_groupe= ug.id_groupe
											inner join t_fonds tf ON gf.id_fonds = tf.id_fonds
											WHERE (tf.ID_FONDS in ('".implode("','",$liste_id)."') or tf.ID_FONDS_GEN in ('".implode("','",$liste_id)."')) and ID_LANG='".$_SESSION["langue"]."' order by $tri",Array("US_NOM"),$val);
					}
				}


			break;

			default:
				//$attr['SELECT_SQL'] : "select ID_XX as ID, XX as VAL ...
				// langue à substituer éventuellement
				if(isset($attr['SELECT_SQL'])) {
					$sql=$attr['SELECT_SQL'];
					if((strpos($sql,"ID_LANG='FR'")>0) && strtoupper($_SESSION['langue'])!='FR') $sql=str_replace("ID_LANG='FR'","ID_LANG='".$_SESSION['langue']."'",$sql);
					print listOptions($sql,Array("ID","VAL"),$val);
				}

				// VP 6/01/09 : ajout cas SELECT_OPTION/SELECT_LIB

				if(isset($attr['SELECT_OPTION'])) {
					$tabOptions=explode(",",$attr['SELECT_OPTION']);
					$tabLibs=explode(",",$attr['SELECT_LIB']);
					foreach($tabOptions as $i=>$x) {
						//PC 19/03/13 : ajout eval() si SELECT_OPTION contient return
						if(strpos($tabOptions[$i],"return")!==false) {
							$tabOptions[$i]=eval($tabOptions[$i]);
						}
						print "<option value=\"".$tabOptions[$i]."\"".(html_entity_decode($tabOptions[$i])==html_entity_decode($val)?" selected":"").">".(defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i])."</option>";
					}
				}
				break;
		}

		print "</select>\n";
	}

	/**
		* Affiche un champ bouton (cf jquery)
	 * IN : type, nom, valeur par défaut, tableau de paramètres
	 * OUT : affichage du champ
	 **/
	// VP 2/06/09 : Création fonction addButton()

	function addButton($type, $nom, $val, $attr=null){
		$attr['TYPE']=$type;
		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		if($val) $attr['VALUE']=(defined($val)?constant($val):$val);
		if(!isset($attr['BUTTON_TYPE']) || empty($attr['BUTTON_TYPE'])) $attr['BUTTON_TYPE']="button";

		if (isset($attr['LABEL']))
			$attr['LABEL']=defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'];
		else
			$attr['LABEL']='';

		if (!isset($attr['ONCLICK'])){
			$attr['ONCLICK']='';
                }

                // B.RAVI 20150304 sous ie7 on est obligé d'avoir un span pour pouvoir agir dessus via css et corriger le bug graphique
                if (isset($attr['ADD_SPAN_IN_BUTTON'])){
                    $label_in_span='<span class="'.$attr['ADD_SPAN_IN_BUTTON'].'">'.$attr['VALUE'].'</span>';
                }else{
                    $label_in_span=$attr['VALUE'];
                }

		print "<button id=\"".$attr['ID']."\" class=\"".$attr['CLASS']."\" type=\"".$attr['BUTTON_TYPE']."\"  name=\"".$attr['NAME']."\" onclick=\"".$attr['ONCLICK']."\">
<span class=\"".$attr['SPAN_CLASS']."\"></span>".$label_in_span."
</button>\n";
	}

	/**
		* Affiche une balise span
	 * IN : nom, valeur par défaut, tableau de paramètres
	 * OUT : affichage du champ
	 **/
	// VP 24/06/09 : Création fonction addSpan()
	function addSpan($nom, $val, $attr=null){
		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		if($val) $val=(defined($val)?constant($val):$val);
		unset($attr['VALUE']);
		unset($attr['TYPE']);
		print "<span ";
		foreach($attr as $i=>$x) {
			print " $i=\"$x\"";
		}

		print ">".$val."</span>\n";
	}

	  /**
	   * Enregistre une liste
	   * IN : nom, tableau
	   * OUT : affichage du champ
	   **/
	function setListOptions($list_name, $tab){
		$this->listOptions[$list_name] = $tab;
	}

	/**
	* Enregistre une variable
	* IN : nom, tableau
	* OUT : affichage du champ
	**/
	function setVariable($name, $val){
		$this->variables[$name] = $val;
	}

	/**
	* Affiche un champ défini par une fonction custom
	* IN : nom, valeur par défaut, tableau de paramètres
	* OUT : affichage du champ
	**/
	function addCustom($nom, $val, $attr=null){
		$func = $attr['FUNCTION'];
		if(function_exists($func)){
			unset($attr['TYPE']);
			$func($this, $nom, $val, $attr);
		}
	}

}
?>
