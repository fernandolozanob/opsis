<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- Creation : 27/01/2016                                                       ---*/
/*--- Author : Vincent Prost                                                      ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/



class Logger {
	var $fh;
    var $filepath;
	

	function __construct($nomfic,$mode = "wt")
	{
        if(basename($nomfic)==$nomfic){
            if(defined("logDir") && logDir!="") {
                if(!is_dir(logDir)) mkdir ( logDir ,  0755 , true);
                $this->filepath = logDir."/".$nomfic;
            }
            else $this->filepath = baseDir."/log/".$nomfic;
        }
        if(isset($this->filepath)){
           $this->fh = fopen( $this->filepath, $mode);
        }
	}

	function Log($texte)
	{
	    fwrite($this->fh,$texte);
	    fflush($this->fh);
	}
	
	function Close()
	{
		fclose($this->fh);
	}
    
    function Read($maxLength)
    {
        $fsize = filesize($this->filepath);
        if($maxLength > $fsize) $maxLength = $fsize;
        $ret = fseek($this->fh, -$maxLength , SEEK_END);
        $data = fread($this->fh, $maxLength);
        return $data;
    }
}

global $trace_log;
$trace_log = new Logger("trace.log","a+t");

function trace($t)
{
	global $trace_log;
	$trace_log->Log(date("D d/m H:i:s")." :  ");
	$trace_log->Log($t."\n");
}



openlog("Opsis ",LOG_PID | LOG_CONS,LOG_USER);



?>