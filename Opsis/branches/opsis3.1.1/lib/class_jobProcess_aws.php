<?php
require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');

require_once(libDir."class_awsTool.php");

class JobProcess_aws extends JobProcess implements Process
{

	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('aws');
		
		$this->required_xml_params=array
		(
			's3_key'=>'string',
			's3_secret'=>'string',
			's3_bucket'=>'string'
		);
		
		$this->optional_xml_params=array
		(
			's3_version'=>'string',
			's3_region'=>'string',
			's3_watchfolder'=>'string',
			's3_timeout'=>'int',
			
		 );
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
		
		if(isset($params_job['s3_watchfolder']) && !is_dir($params_job['s3_watchfolder'])) {
			throw new InvalidXmlParamException('Directory do not exist ('.$params_job['s3_watchfolder'].')');
		}
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		$file_path = $this->getFileInPath();
		$file_key="";
		if(!empty($params_job['s3_root'])){
			$file_key = $params_job['s3_root']."/";
		}
		if(!empty($params_job['s3_path'])){
			$file_key .= $params_job['s3_path']."/".basename($file_path);
		}else{
			$file_key .= basename($file_path);
		}
		$file_key = trim(str_replace("//", "/", $file_key), "/");
		$file_size = filesize($file_path);
		
		$bucket_name = $params_job['s3_bucket'];
		
		// Connect S3
		$this->aws = new AwsTool($params_job);
		$this->aws->connectS3();
		
		// Envoi du fichier sur S3 si non présent
		$file_in_s3 = $this->aws->fileExistS3($bucket_name, $file_key);
		if(!$file_in_s3){
			// Envoi par watchfolder ou direct
			if(isset($params_job['s3_watchfolder'])){
				$this->setProgression(5);
				// Copy dans Watchfolder synchronisé avec S3
				$dir_copy=dirname($params_job['s3_watchfolder']."/".$file_key);
				if(!is_dir($dir_copy)){
					mkdir($dir_copy, 0777, true);
				}
				copy($file_path, $params_job['s3_watchfolder']."/".$file_key);
				if(!is_file($params_job['s3_watchfolder']."/".$file_key)){
					throw new Exception('Erreur watchfoler : missing file');
				}
				
				$this->setProgression(50);
				
				// Attente présence fichier sur S3
				if(isset($params_job['s3_timeout'])) $timeout = $params_job['s3_timeout'];
				else $timeout = 3600;
				
				$time_end = time() + $timeout;
				$file_in_s3=false;
				$file_size_s3=0;
				$last_file_size_s3=0;
				while(!$file_in_s3 && time()<$time_end){
					$file_in_s3 = $this->aws->fileExistS3($bucket_name, $file_key);
					/*
					// Calcul progression basé sur taill reçue, mais cela ne fonctionne pas
					$file_size_s3 = intval($file_in_s3['ContentLength']);
					if($file_size_s3 > $last_file_size_s3){
						$last_file_size_s3=$file_size_s3;
						$time_end = time() + 300;
					}
					$this->setProgression(50 + intval(50 * $file_size_s3/$file_size));
					 */
					sleep(5);
				}
				if(!$file_in_s3){
					if(time()>=$time_end) {
						throw new Exception('Erreur sync watchfolder S3 (timeout)');
					}else{
						throw new Exception('Erreur sync watchfolder S3');
					}
				}
			}else{
				// Envoi direct
				$file_in_s3 = $this->aws->putObjectS3($bucket_name, $file_path, $file_key);
			}
		}
		if(!$file_in_s3){
			throw new Exception('Erreur S3 : missing file');
		}
		
		$this->writeOutLog('AWS video_id:<id>'.$file_key.'</id>');
		JobProcess::writeJobLog('AWS video_id:'.$file_key);

		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
		
	}
	
	
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}
?>
