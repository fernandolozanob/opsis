<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once(modelDir.'model_materiel.php');
require_once(libDir."class_matInfo.php");

require_once('exception/exception_fileNotFoundException.php');

class JobProcess_episode extends JobProcess implements Process
{
	//private $process_running;
	//private $
	
	private $nb_frames; // nombre theorique de frames
	private $nb_frames_encodees;

	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('episode');
		
		$this->required_xml_params=array
		(
		);
		
		$this->optional_xml_params=array
		(
            'setting'=>'string',
			'tcin'=>'string',
			'tcout'=>'string'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
		
		//$this->process_running=Mutex::create();
	}
	
	function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		
		$tmp_rep=$this->getTmpDirPath();
	
		$params_job=$this->getXmlParams();
		$this->writeJobLog(print_r($params_job,true));
		/// ====================GESTION DES SETTINGS========
		if(!empty($params_job['setting']) && !empty($params_job['tcin']) && !empty($params_job['tcout']) &&  $params_job['tcout']!='00:00:00:00')
		{
			// on traite la XSL en fonction du tcin et tcout
			//on traite le XML ave la XSL pour episode
			$fichier_config=$tmp_rep.'/config.epitask';
			$xml_setting=file_get_contents(jobSettingDir."/".$params_job['setting']);
			$this->writeJobLog("settings epitask : ".$xml_setting);
			//calcul du nombre de seconde pour le tcin
			$vals_tc_in=explode(':',$params_job['tcin']);
			$tcin=$vals_tc_in[0]*3600+$vals_tc_in[1]*60+$vals_tc_in[2]+$vals_tc_in[3]/25;
			//calcul du nombre de secondes pour tcout
			$vals_tc_out=explode(':',$params_job['tcout']);
			$tcout=$vals_tc_out[0]*3600+$vals_tc_out[1]*60+$vals_tc_out[2]+$vals_tc_out[3]/25;

			// on ajoute le tc in et out dans importer
			$data=preg_split('/(<importer(.*)media="movie"(.*)>)|(<\/importer>)/' ,$xml_setting);

			preg_match('/<importer(.*)media="movie"(.*)>/',$xml_setting,$results);
			$balise_decoupage='<option name="start-time" value="'.$tcin.'"></option><option name="end-time" value="'.$tcout.'"></option>';
			$xml_setting=$data[0].$results[0].$data[1].$balise_decoupage.'</importer>'.$data[2];
			
			// on decoupe l'audio et on y ajoute un filtre
			preg_match('/<encoder(.*)media="audio"(.*)>/',$xml_setting,$balise_encoder);
			$data=preg_split('/(<encoder(.*)media="audio"(.*)>)|(<\/encoder>)/' ,$xml_setting ,3);
			preg_match('/<in id="(.*)">/',$data[1],$results);// l'id de in sera remplace plus tard
			$id_out_audio=explode('_',$results[1]);
			$id_out_audio='audio_'.($id_out_audio[1]+1).'_ftim_filter';
			//creation de la partie du XML qui gere le decoupage audio
			$xml_decoupage_audio ='<filter type="ftim" media="any">';
			$xml_decoupage_audio.='<in id="'.$results[1].'"></in>';
			$xml_decoupage_audio.='<out id="'.$id_out_audio.'"></out>';
			$xml_decoupage_audio.='<option name="start-time" value="'.$tcin.'"></option>';
			$xml_decoupage_audio.='<option name="end-time" value="'.$tcout.'"></option>';
			$xml_decoupage_audio.='</filter>'."\n";
			//on replace l'id audio par l'id de sortie du filtre
			$data[1]=preg_replace('/<in id="(.*)">/','<in id="'.$id_out_audio.'">',$data[1]);
			$xml_setting=$data[0].$xml_decoupage_audio.$balise_encoder[0].$data[1].'</encoder>'.$data[2];
			
			// on decoupe la video et on y ajoute un filtre
			preg_match('/<encoder(.*)media="video"(.*)>/',$xml_setting,$balise_encoder);
			$tmp_data=preg_split('/<encoder(.*)media="video"(.*)>/' ,$xml_setting);
			$data[0]=$tmp_data[0];
			$tmp_data=preg_split('/<\/encoder>/' ,$tmp_data[1]);
			$data[1]=$tmp_data[0];
			$data[2]=$tmp_data[1];
			//var_dump($data);
			preg_match('/<in id="(.*)">/',$data[1],$results);// l'id de in sera remplace plus tard
			$id_out_video=explode('_',$results[1]);
			$id_out_video='video_'.($id_out_video[1]+1).'_ftim_filter';
			//creation de la partie du XML qui gere le decoupage video
			$xml_decoupage_video ='<filter type="ftim" media="any">';
			$xml_decoupage_video.='<in id="'.$results[1].'"></in>';
			$xml_decoupage_video.='<out id="'.$id_out_video.'"></out>';
			$xml_decoupage_video.='<option name="start-time" value="'.$tcin.'"></option>';
			$xml_decoupage_video.='<option name="end-time" value="'.$tcout.'"></option>';
			$xml_decoupage_video.='</filter>'."\n";
			//on replace l'id video par l'id de sortie du filtre
			$data[1]=preg_replace('/<in id="(.*)">/','<in id="'.$id_out_video.'">',$data[1]);
			$xml_setting=$data[0].$xml_decoupage_video.$balise_encoder[0].$data[1].'</encoder>'.$data[2];

			file_put_contents($fichier_config,$xml_setting);
		}
		else if (isset($params_job['setting']) && !empty($params_job['setting']))//params_job['setting']
		{
			// utilise le parametre setting dans le fichier de config
			$fichier_config=jobSettingDir."/".$params_job['setting'];
		}
		else
		{
			//on traite le XML ave la XSL pour episode (.setting pour V5, .epitask pour V6) 
			//$fichier_config=str_replace('//','/',$tmp_rep.'/config.epitask');
			$fichier_config=$tmp_rep.'/config.setting';
			$this->writeJobLog("xml path : ".$this->getInXmlPath());
			$this->writeJobLog("gEpisodeXSL : ".gEpisodeXSL);
			$xml_res=TraitementXSLT(file_get_contents($this->getInXmlPath()),gEpisodeXSL,array());
			$fd=fopen($fichier_config,'a');
			fwrite($fd,$xml_res);
			fclose($fd);
		}
		
		/// ===========================================
		$this->writeJobLog('Utilisation du fichier de config : '.$fichier_config);

		$this->tmp_file=stripExtension($this->getFileOutPath()).'_'.time().'_'.rand(100000,999999).'.'.getExtension($this->getFileOutPath());
		
		
		// lancement de la commande
		if (kEpisodeAsynchrone==true) {
			$this->shellExecute(kBinEpisodectl, " workflow submit -f ".escapeQuoteShell($this->getFileInPath())." -e \"".$fichier_config."\" -d ".escapeQuoteShell(dirname($this->getFileOutPath())."/")." -r 0 --naming \"".stripExtension(basename($this->tmp_file))."\" --timeout 60 --ids-out");
		} else {
			$this->shellExecute(kBinEpisodectl, " workflow submit -f ".escapeQuoteShell($this->getFileInPath())." -e \"".$fichier_config."\" -d ".escapeQuoteShell(dirname($this->getFileOutPath())."/")." -r 0 --naming \"".stripExtension(basename($this->tmp_file))."\" --timeout 60 -w --ids-out");
		}
		
		
		
		// stockage sortie commande
		$std_out = file_get_contents($tmp_rep."/stdout.txt");
		$std_err = file_get_contents($tmp_rep."/stderr.txt");
		
		$this->writeJobLog($this->getPidFilePath());
		file_put_contents($this->getPidFilePath(),"\n".'workflow_id='.$std_out,FILE_APPEND);
		
		$this->writeJobLog("stdout ".$std_out);
		$this->writeJobLog("stderr ".$std_err);	
		
		// si erreur
		if(empty($std_out)) {
			// Erreur
			if(empty($std_err)) {
				throw new Exception('Erreur ID Episode vide');
			} else {
				throw new Exception($std_err);
			}
			
		}else if(kEpisodeAsynchrone==true) {
			$epi_workflow_id = $std_out;
			//tant que le statut est "en cours" ou "idle" on ne continue pas
			$cmd_opts=' status workflows '.$epi_workflow_id.' --status-out';
			$restart_status=15;
			do {
				sleep(5);
				
				$this->shellExecute(kBinEpisodectl, $cmd_opts);
				$std_err = file_get_contents($tmp_rep."/stderr.txt");
				$etat_processus = file_get_contents($tmp_rep."/stdout.txt");
				
				if(strstr($std_err, "*** Error:") !==false){
					$restart_status=15;
				} else {
					$restart_status--;
				}
				
			} while (($etat_processus == '4' || $etat_processus == '3' || $restart_status>0) && $etat_processus != '0');
			
			
			if($etat_processus == "1" || $etat_processus == "2" || $etat_processus == "" || $etat_processus === false) {
				if(empty($std_err)) {
					throw new Exception('Episode : Erreur');
				} else {
					throw new Exception($std_err);
				}
			}
		}
		//$this->writeJobLog("expected out : ".$this->tmp_file." => ".$this->getFileOutPath());
		
		if(file_exists($this->tmp_file)){
			rename($this->tmp_file,$this->getFileOutPath());
		}else{
			throw new Exception('Fichier de sortie non trouv�: '.$this->tmp_file);
		}
		
		
		
		if (file_exists($tmp_rep.'/config.epitask') || file_exists($tmp_rep.'/config.setting'))
		{
			if (unlink($fichier_config)===false)
				throw new FileException('Failed to delete episode config file ('.$fichier_config.')');
		}
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
		/*$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
		// VP 8/04/13 : compatibilite Mac OS X
		$donnees_stderr=str_replace("\r","\n",$donnees_stderr);
		preg_match_all('/frame=([ 0-9]+) fps=([ 0-9]+) q=([- 0-9.]+) (Lsize|size)=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
		$this->nb_frames_encodees=intval($infos_encodage[1][count($infos_encodage[1])-1]);
        
		$this->writeOutLog($this->nb_frames_encodees.' / '.$this->nb_frames);
		$ratio=$this->nb_frames_encodees/$this->nb_frames;
		
		if ($ratio>1)
			$ratio=1;
		
		$this->setProgression($ratio*100);*/
	}
	
	private function checkIfEpisodeRunning()
	{
		/*//if ($this->nb_frames_encodees<$this->nb_frames)
		if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1)
			return true;
		else
			return false;*/
	}
	
	public static function kill($module,$xml_file)
	{
		$job_name=stripExtension($xml_file);
		$contenu=file_get_contents(jobRunDir.'/'.$module.'/'.$job_name.'.pid');

		$contenu=explode("\n",$contenu);
		$id_engine=$contenu[1];
		
		foreach ($contenu as $ligne)
		{
			$tmp_ligne=explode('=',$ligne);
			if ($tmp_ligne[0]=='workflow_id')
			{
				$engine=new Engine($id_engine);
				// annulation du workflow episode
				$cmd='ssh '.$engine->getIpAdress().' "'.kBinEpisodectl.' workflow stop '.$tmp_ligne[1].'"';
				JobProcess::writeJobLog($cmd);
				shell_exec($cmd);
				break;
			}
		}
		
		JobProcess::kill($module,$xml_file);
	}
}

?>
