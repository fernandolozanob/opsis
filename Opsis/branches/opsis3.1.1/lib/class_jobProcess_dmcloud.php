<?php
require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_uploadException.php');

require_once(libDir.'dmcloud_sdk/CloudKey.php');

class JobProcess_dmcloud extends JobProcess implements Process
{
	private $id_video_dmcloud;
	private $link_file_path;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('dmcloud');
		
		$this->required_xml_params=array
		(
			'user_id'=>'string',
			'api_key'=>'string'
		);
		
		$this->optional_xml_params=array
		(
            'action'=>'string'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
		
		$required_params=array
		(
		);
		
		$optional_params=array
		(
            'title'=>'string',
			'description'=>'string',
			'id'=>'string',
			'date'=>'string'
		);
		
		//verification des donnees presentes dans le noeud donnees
		if (!empty($required_params))
		{
			foreach($required_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
				{
					$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
				}
				else if (isset($this->params_job['donnees'][0][$param_name]) && empty($this->params_job['donnees'][0][$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($optional_params))
		{
			foreach($optional_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
					$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
			}
		}
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		$user_id = $params_job['user_id'];
		$api_key = $params_job['api_key'];
		
		$api = new CloudKey($user_id, $api_key);
		switch($params_job['action'])
		{
			case 'delete':
                $id_dmcloud="";
                if (isset($params_job['id_dmcloud'])) $id_dmcloud = $params_job['id_dmcloud'];
                elseif (isset($params_job['donnees'][0]['id_dmcloud'])) $id_dmcloud = $params_job['donnees'][0]['id_dmcloud'];
                elseif(empty($id_dmcloud))
 					throw new InvalidXmlParamException('ID DMCloud is empty');
                try
                {
                    if(strpos($id_dmcloud,'?auth')>0){
                        $id_dmcloud = strstr($id_dmcloud, '?auth',true);
                    }
                    $res = $api->media->delete(array('id' => $id_dmcloud));
                }
                catch (CloudKey_AuthenticationErrorException $e)
                {
                    throw new InvalidXmlParamException('DMCloud authentification error : '.$e->getMessage());
                }
                catch (CloudKey_NotFoundException $e)
                {
                    throw new InvalidXmlParamException('ID DMCloud not found : '.$e->getMessage());
                }
                
                $this->writeOutLog('DM Cloud delete:<id>'.$id_dmcloud.'</id>');
                $this->writeJobLog('DM Cloud delete:'.$id_dmcloud.'');
                break;
                
			case 'upload':
            default:
                
                try
                {
                    $res = $api->file->upload_file($this->getFileInPath());
                    $source_url = $res->url;
                }
                catch (CloudKey_AuthenticationErrorException $e)
                {
                    throw new InvalidXmlParamException('DMCloud authentification error : '.$e->getMessage());
                }
                catch (CloudKey_TransportException $e)
                {
                    throw new UploadException('DMCloud upload error : '.$e->getMessage());
                }
                
                $titre = stripExtension(basename($this->getFileInPath()));
                $desc="";
                $id=$titre;
                $date="";
                if (isset($params_job['donnees'][0]['title'])) $titre = stripslashes($params_job['donnees'][0]['title']);
                if (isset($params_job['donnees'][0]['description'])) $desc = stripslashes($params_job['donnees'][0]['description']);
                if (isset($params_job['donnees'][0]['id'])) $id = $params_job['donnees'][0]['id'];
                if (isset($params_job['donnees'][0]['date'])) $date = $params_job['donnees'][0]['date'];
                try
                {
                    $assets = array('mp4_h264_aac', 'mp4_h264_aac_hq', 'jpeg_thumbnail_medium', 'jpeg_thumbnail_source');
                    $meta = array('title' => $titre, 'description' => $desc, 'id' => $id, 'date' => $date);
                    $media = $api->media->create(array('assets_names' => $assets, 'meta' => $meta, 'url' => $source_url));
                    $this->id_video_dmcloud = $media->id;
                    
                 }
                catch (CloudKey_InvalidParamException $e) {
                    throw new InvalidXmlParamException('DMCloud encoding error : '.$e->getMessage());
                }

                // Sign the url
                try{
                    $protocol = 'http';
                    $asset_name = 'mp4_h264_aac';
                    $extension = '.mp4';
                    $seclevel = 0;
                    $expires = time() + 3600 * 24 * 3650 ; // 10 ans
                    
                    // Create the url
                    $url = sprintf('http://cdn.dmcloud.net/route/%s/%s/%s/%s%s', $protocol, $user_id, $media->id, $asset_name, $extension);
                    $url = sprintf('http://api.dmcloud.net/player/embed/%s/%s', $user_id, $media->id);
                    $sign_url = CloudKey_Helpers::sign_url($url, $api_key, $seclevel, null, null, null, null, null, $expires);
                    if($sign_url){
                        $auth = strstr($sign_url, '?auth');
                        $this->id_video_dmcloud.=$auth;
                    }
                    echo "/auth=".$auth;
                    
                }
                catch (InvalidArgumentException $e) {
                    echo('DMCloud error : '.$e->getMessage());
                }

                $this->writeOutLog('DM Cloud video_id:<id>'.$this->id_video_dmcloud.'</id>');
                $this->writeOutLog('DM Cloud url:<url>'.$sign_url.'</url>');
                $this->writeJobLog('DM Cloud video_id:'.$this->id_video_dmcloud.'');
                $this->writeJobLog('DM Cloud sign_url:'.$sign_url.'');
                break;
        }
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function finishJob()
	{
		parent::finishJob();
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}
?>