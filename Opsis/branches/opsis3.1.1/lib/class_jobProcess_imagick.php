<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once(modelDir.'model_materiel.php');
require_once(libDir."class_matInfo.php");

require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_uploadException.php');

class JobProcess_imagick extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('imagick');
		
		$this->required_xml_params=array
		(
			
		);
		
		$this->optional_xml_params=array
		(
			'font'=>'string',
			'pointsize'=>'int',
			'gravity'=>'string',
			'fill'=>'string',
			'text_position'=>'string',
			'text'=>'string',
			'width'=>'int',
			'height'=>'int',
			'largeur'=>'int',
			'hauteur'=>'int',
			'kr'=>'int',
			'strip_iptc'=> 'int',
            'keep_iptc'=> 'int',
            'stripimage'=> 'int',
            'encapsulage'=>'int',
			'dpi' => 'string'
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
		// MS 03/06/14 - tableau de correspondance entre les valeurs du flag d'orientation exif et les modification à appliquer à l'image
		// la première valeur des sous-tableau correspond à la rotation en degré à appliquer,
		// la seconde valeur des sous-tableau correspond aux transformations type miroir que l'on doit appliquer
		// pour exemple d'implémentation : http://beradrian.wordpress.com/2008/11/14/rotate-exif-images/
		$this->arr_rotation_transform = 
			array(
				1=>array(0,null),
				2=>array(0,'horizontal'),
				3=>array(180,null),
				4=>array(0,'vertical'),
				5=>array(90,'horizontal'),
				6=>array(90,null),
				7=>array(-90,'horizontal'),
				8=>array(-90,null)
				);
	}
	
	function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		$fileSrc=$this->getFileInPath();
		$fileout=$this->getFileOutPath();
		
		$out_folder = dirname($fileout);
		if (!is_dir($out_folder))
			mkdir($out_folder,0775,true);
		          
		if (!extension_loaded('imagick') && !defined("kConvertPath")) {throw new UploadException('Imagick error : '.kExtensionImagickNotLoaded);return false;}
		
		// MS - 18/07/14 - backward compatibility avec les paramètres en fr utilisés sur qqs vieux sites
		if(isset($params_job['hauteur']) && !empty($params_job['hauteur']) && (!isset($params_job['height'])||empty($params_job['height']))){
			$params_job['height'] = $params_job['hauteur'];
		}
		if(isset($params_job['largeur']) && !empty($params_job['largeur']) && (!isset($params_job['width'])||empty($params_job['width']))){
			$params_job['width'] = $params_job['largeur'];
		}
		
		
		// MS - 18/07/14 - report du code permettant de redimensionner l'image (après correction rotation / alpha, avant incrust logo)
		if ((isset($params_job['width']) && !empty($params_job['width'])) || (isset($params_job['height']) && !empty($params_job['height'])))
		{
			if($dim=GetImageSize($fileSrc)){
				list($src_w,$src_h) = $dim;
			}else{
				$infos=MatInfo::getMatInfo($fileSrc);
				list($src_w,$src_h)=array($infos['width'],$infos['height']);
			}
			
			
			if ((isset($params_job['width']) && !empty($params_job['width'])) && (isset($params_job['height']) && !empty($params_job['height'])))
			{
				$height=intval($params_job['height']);
				$width=intval($params_job['width']);
				if(isset($params_job['kr']) && !empty($params_job['kr'])){
					$dst_h_temp = round(($width / $src_w) * $src_h);
					$dst_w_temp = round(($height / $src_h) * $src_w);
					if($dst_h_temp > $height) $width = $dst_w_temp;
					elseif($dst_w_temp > $width) $height = $dst_h_temp;
				}
			}else if ((isset($params_job['width']) && !empty($params_job['width'])) && !(isset($params_job['height']) && !empty($params_job['height'])))
			{
				$width=intval($params_job['width']);
				$height=intval(round(($params_job['width']*$src_h)/$src_w));
			}else if (!(isset($params_job['width']) && !empty($params_job['width'])) && (isset($params_job['height']) && !empty($params_job['height'])))
			{
				$height=intval($params_job['height']);
				$width=intval(round(($params_job['height']*$src_w)/$src_h));
			}
		}
                
                
		/* ************ images RAW (issues des appareil photo numériques) (traitement spécifique) *********** */
		$imgIsRaw = MatInfo::getMatInfo($fileSrc);
		if(isset($imgIsRaw["format_calc"]))
			$format_calc=$imgIsRaw["format_calc"];
		elseif(isset($imgIsRaw["format"]))
			$format_calc=$imgIsRaw["format"];
		else
			$format_calc=stripExtension($fileSrc);
		//Attention in_array() sensible à la classe, rajouter donc les extensions en MAJUSCULES
		if (in_array(strtoupper($format_calc), array('RAW','CR2','DNG','NEF'))) {

			if (!defined('kUfrawbatch')) {
				throw new UploadException('Ufraw error : kUfrawbatch constant missing ');
				return false;
			}

			if (!empty($params_job['ufraw_conf']) && !file_exists(jobSettingDir . '/' . $params_job['ufraw_conf'])) {
				throw new UploadException('Ufraw error : conf ' . $params_job['ufraw_conf'] . ' physically missing');
				return false;
			}

			$options_raw = null;
			if (!empty($params_job['ufraw_conf'])) {
				$options_raw .= ' --conf=' . jobSettingDir . '/' . $params_job['ufraw_conf'];
			}


			/* 1er cas -- SANS logo/font */
			if (empty($params_job['logo']) && empty($params_job['font'])) {

				// même "<espace>1",  "1<espace>", "<espace>1<espace>" satisferont la condition ci-dessous
				if ($params_job['use_embed'] == 1) {
					$options_raw = ' --embedded-image'; //important (= et pas .=) car pas besoin de --conf.
					$options_raw .= ' --overwrite --output=' . escapeQuoteShell($fileout) . ' ' . escapeQuoteShell($fileSrc);

					$this->shellExecute(kUfrawbatch, $options_raw, true);
					if (!file_exists($fileout)) {
						$this->writeOutLog(' --embedded-image FAILED)');
						$options_raw = null; //important, on reinit la commande avant de poursuivre le traitement habituel
						if (!empty($params_job['ufraw_conf'])) {
							$options_raw .= ' --conf=' . jobSettingDir . '/' . $params_job['ufraw_conf'];
						}
					} else {
						$this->writeOutLog('Fin traitement (use_embed + raw image without incrustation)');
						return true; //on sort de la fonction traitement terminé
					}
				}


				$formatRaw = strtolower($params_job['format']);
				if (!empty($formatRaw)) {
					if (!in_array($formatRaw, array('ppm', 'tiff', 'tif', 'png', 'jpeg', 'jpg', 'fits'))) {
						throw new UploadException('Ufraw error :  ' . $formatRaw . ' extension not allowed');
						return false;
					}
				} else {
					$formatRaw = 'jpg';
				}

				$options_raw .= ' --out-type=' . $formatRaw;

				$compression = (int) $params_job['compression'];
				if (!empty($compression)) {
					$options_raw .= ' --compression=' . $compression;
				}


				$widthRaw = (int) $params_job['width'];
				$heightRaw = (int) $params_job['height'];
				$tailleRaw = null;
				if ($widthRaw > $heightRaw) {
					$tailleRaw = $widthRaw;
				} elseif ($heightRaw > $widthRaw) {
					$tailleRaw = $heightRaw;
				} elseif ($heightRaw == $widthRaw && !empty($heightRaw)) {
					$tailleRaw = $heightRaw;
				}


				if (!empty($tailleRaw)) {
					$options_raw .= ' --size=' . $tailleRaw;
				}

				$options_raw.=' --overwrite --output=' . escapeQuoteShell($fileout) . ' ' . escapeQuoteShell($fileSrc);

				$this->shellExecute(kUfrawbatch, $options_raw, true);
				$this->setProgression(100);
				$this->writeOutLog('Fin traitement (raw image without incrustation)');
				return true; //on sort de la fonction traitement terminé
			} else {/* 2ème cas -- AVEC logo/font */

				$tmpRawImage = $this->tmp_dir_path . 'tmp_raw_image.';



				if ($params_job['use_embed'] == 1) {
					$options_raw = ' --embedded-image'; //important (= et pas .=) car pas besoin de --conf.
					$options_raw .= ' --overwrite --output=' . $tmpRawImage . 'jpg ' . escapeQuoteShell($fileSrc);

					$this->shellExecute(kUfrawbatch, $options_raw, true);
//					if (!file_exists($tmpRawImage . 'jpg') || true) { /* debug test */
					if (!file_exists($tmpRawImage . 'jpg')) {
						$this->writeOutLog(' --embedded-TMP-image FAILED)');
						$options_raw = null; //important, on reinit la commande avant de poursuivre le traitement habituel
						if (!empty($params_job['ufraw_conf'])) {
							$options_raw .= ' --conf=' . jobSettingDir . '/' . $params_job['ufraw_conf'];
						}
					} else {
						$fileSrc = $tmpRawImage . 'jpg'; //on va poursuivre le traitement pour incruster le logo
					}
				}


				//on essaye de générer un TMP .tif dans 2 cas :
				// - use_embed=1 mais échec
				// - use_embed=0
				if ($fileSrc != $this->tmp_dir_path . 'tmp_raw_image.jpg') {
					$options_raw .= ' --out-type=tif --overwrite --output=' . $tmpRawImage . 'tif ' . escapeQuoteShell($fileSrc);
					$this->shellExecute(kUfrawbatch, $options_raw, true);

					if (file_exists($tmpRawImage . 'tif')) {
						$fileSrc = $tmpRawImage . 'tif';
					} else {
						throw new UploadException('Ufraw error : tmp image (for logo/font incrustation) missing ');
						return false;
					}
				}
			}
		}

		
		if (extension_loaded('imagick') && empty($params_job['imagick_cli'])) { //par extension
			Imagick::setResourceLimit (6, 1);
				
			$image=new Imagick();
			
			// les tiffs peuvent contenir deux images, l'image principale et une miniature
			$image->readImage($fileSrc."[0]");
			$image->setImageFileName($fileout);
			$orientation_exif = $image->getImageOrientation();
			
			
			// MS - 18/07/14 Si param strip_iptc à 1 => on vide les tags IPTC de l'image
			if(isset($params_job['strip_iptc']) && $params_job['strip_iptc'] == '1'){
				$image->profileImage('iptc',NULL);
			}
			// VP - 16/12/14 : si param stripimage ˆ 1 => on vide tous les tags (EXIF, IPTC etc...), et on remet IPTC si keep_iptc=1
			if(isset($params_job['stripimage']) && $params_job['stripimage'] == '1'){
                $profiles = $image->getImageProfiles('*', false); // Sauvegarde tags
				$image->stripImage();
                if(isset($params_job['keep_iptc']) && $params_job['keep_iptc'] == '1'){
                    $image->profileImage('iptc',$profiles['iptc']);
                }
			}

			
			// MS - 20/06/14 - Si l'image source contient un canal alpha,on ajoute un background blanc et on applati le calque de l'image sur ce background
			// => évite des bugs de transparence lorsqu'on ajoute ensuite un logo / du texte (logo apparaissant en négatif sur la zone transparente par exemple)
			if($image->getImageAlphaChannel()){
				$this->writeOutLog($fileSrc." : canal alpha detecté, ajout d'un background blanc");
				$image->setImageBackgroundColor("white");
				$image = $image->flattenImages();
			}
			
			// MS - 18/07/14 - report du code permettant de redimensionner l'image (après correction rotation / alpha, avant incrust logo)
			if ((isset($params_job['width']) && !empty($params_job['width'])) && (isset($params_job['height']) && !empty($params_job['height'])))
			{		
				$image->resizeImage($width,$height,Imagick::FILTER_LANCZOS,1);
				$image->setImagePage($width, $height, 0, 0);
			}
			
			if(isset($params_job['dpi']) && !empty($params_job['dpi']) && intval($params_job['dpi']) > 0) {
				$image->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
				$image->setImageResolution(intval($params_job['dpi']), intval($params_job['dpi']));
			}
					
			// MS - 03/06/14 - prise en compte du paramètre exif d'orientation, application des modifs avant ajout de logo / txt 
			if(isset($orientation_exif) && !empty($orientation_exif) && isset($this->arr_rotation_transform[$orientation_exif]) && !empty($this->arr_rotation_transform[$orientation_exif])){
				$rotation= $this->arr_rotation_transform[$orientation_exif][0];
				$mirror = $this->arr_rotation_transform[$orientation_exif][1];
				if(!empty($rotation)){
					$success = $image->rotateImage("#000",$rotation);
					$this->writeOutLog($fileSrc." : orientation exif detectée, rotation : ".$rotation.", ok:".intval($success));
				}
				unset($rotation);
				
				if(!empty($mirror)){
					switch($mirror){
						case 'horizontal': 
							$success = $image->flopImage();
						break;
						case 'vertical': 
							$success = $image->flipImage();
						break ;
					}
					$this->writeOutLog($fileSrc." : orientation exif detectée, miroir : ".$mirror.", ok:".intval($success));
				}
				unset($mirror);
				// on met à jour le paramètre exif d'orientation pr l'image transférée
				$image->setImageOrientation(1);
			}

			if(isset($params_job['logo']) && !empty($params_job['logo']) && file_exists(jobImageDir.$params_job['logo'])) {
                
                if (isset($params_job['logo_marge']) && !empty($params_job['logo_marge'])) {
                    $logo_marge=$params_job['logo_marge'];
                } else {
                    $logo_marge=10;
                }
                         
                $width_image = $image->getImageWidth();
                $height_image = $image->getImageHeight();
                
                $logo = new Imagick(jobImageDir.$params_job['logo']);
				
				$logo_w_ori = $logo->getImageWidth();
				$logo_h_ori = $logo->getImageHeight();
				
				if(isset($params_job['logo_scale']) && !empty($params_job['logo_scale'])){
					if(($width_image / $logo_w_ori) > ($height_image / $logo_h_ori)) {
						$resize_h_logo = ($height_image*$params_job['logo_scale']/100);
						$resize_w_logo = $resize_h_logo*$logo_w_ori/$logo_h_ori;
					} else {
						$resize_w_logo = ($params_job['logo_scale'] * $width_image /100);
						$resize_h_logo = $resize_w_logo*$logo_h_ori/$logo_w_ori;
					}
				}else{
					if(($width_image / $logo_w_ori) > ($height_image / $logo_h_ori)) {
						$resize_h_logo = $height_image - ($logo_marge * $height_image /100)*2;
						$resize_w_logo = $resize_h_logo*$logo_w_ori/$logo_h_ori;
					} else {
						$resize_w_logo = $width_image - ($logo_marge * $width_image /100)*2;
						$resize_h_logo = $resize_w_logo*$logo_h_ori/$logo_w_ori;
					}
				}
				
				$this->writeJobLog("logo_scale ".$params_job['logo_scale']." w:".$resize_w_logo." h:".$resize_h_logo);
				if (!isset($params_job['logo_noresize']))
					$logo->resizeImage($resize_w_logo, $resize_h_logo, imagick::FILTER_LANCZOS, 1, TRUE);
                
                $checkPosition = '';
                if(isset($params_job['logo_position'])) $checkPosition = $params_job['logo_position'];     
                switch($checkPosition)
                {
                    case 'top-right':
                        $x_logo = $width_image - $logo->getImageWidth() - $logo_marge;
                        $y_logo = $logo_marge;
                        break;
                    case 'bottom-right':
                        $x_logo = $width_image - $logo->getImageWidth() - $logo_marge;
                        $y_logo = $height_image - $logo->getImageHeight() - $logo_marge;
                        break;
                    case 'center':
                        $x_logo = ($width_image / 2) - ($logo->getImageWidth() / 2);
                        $y_logo = ($height_image / 2) - ($logo->getimageheight() / 2);
                        break;
                    default: //center
                        $x_logo = ($width_image / 2) - ($logo->getImageWidth() / 2);
                        $y_logo = ($height_image / 2) - ($logo->getimageheight() / 2);
                }
                $image->compositeImage($logo, imagick::COMPOSITE_DEFAULT, $x_logo, $y_logo);
                $logo->clear();
                $logo->destroy();
            }
			
            // incrustation de texte
			if
			(
				(isset($params_job['font']) && !empty($params_job['font'])) && 
				(isset($params_job['pointsize']) && !empty($params_job['pointsize'])) && 
				(isset($params_job['text_position']) && !empty($params_job['text_position'])) &&
				(isset($params_job['text']) && !empty($params_job['text']))
			)
			{
				$text_incrust=new ImagickDraw();
				$text_incrust->setFontSize($params_job['pointsize']);
				$text_incrust->setFont(jobFontDir.'/'.$params_job['font']);
				//$text_incrust->setFont($params_job['font']);
				
				$color=new ImagickPixel($params_job['fill']);
				$text_incrust->setFillColor($color);
						
				switch ($params_job['gravity'])
				{
					case 'northwest':
						$text_incrust->setGravity(Imagick::GRAVITY_NORTHWEST);
						break;
					case 'north':
						$text_incrust->setGravity(Imagick::GRAVITY_NORTH);
						break;
					case 'northeast':
						$text_incrust->setGravity(Imagick::GRAVITY_NORTHEAST);
						break;
					case 'west':
						$text_incrust->setGravity(Imagick::GRAVITY_WEST);
						break;
					case 'center':
						$text_incrust->setGravity(Imagick::GRAVITY_CENTER);
						break;
					case 'east':
						$text_incrust->setGravity(Imagick::GRAVITY_EAST);
						break;
					case 'southwest':
						$text_incrust->setGravity(Imagick::GRAVITY_SOUTHWEST);
						break;
					case 'south':
						$text_incrust->setGravity(Imagick::GRAVITY_SOUTH);
						break;
					case 'southeast':
						$text_incrust->setGravity(Imagick::GRAVITY_SOUTHEAST);
						break;
					default:
						$text_incrust->setGravity(Imagick::GRAVITY_SOUTH);
				}
				
				$position=$params_job['text_position'];
				$texte=stripslashes($params_job['text']);
				$arr_position=explode(',',$position);
				$image->annotateImage($text_incrust,$arr_position[0],$arr_position[1],0,$texte);
						
				
			}
			
			$image->writeImage();
			$image->clear();
			$image->destroy();
		} else { //par ligne de commande
			// VP 22/05/2018 : ajout [0] pour ne prendre que la pemière image du fichier (cas des TIF et des GIF)
			$options_convert=escapeQuoteShell($fileSrc."[0]");
			
			if ((isset($params_job['width']) && !empty($params_job['width'])) && (isset($params_job['height']) && !empty($params_job['height']))) {
				$options_convert.=' -filter Lanczos -resize '.$params_job['width'].'x'.$params_job['height'];	
			}
			if(isset($params_job['dpi']) && !empty($params_job['dpi']) && intval($params_job['dpi']) > 0) {
				$options_convert.=' -density '.intval($params_job['dpi']).'x'.intval($params_job['dpi']);
			}
			
			// incrustation de texte
			if
			(
				(isset($params_job['font']) && !empty($params_job['font'])) && 
				(isset($params_job['pointsize']) && !empty($params_job['pointsize'])) && 
				(isset($params_job['text_position']) && !empty($params_job['text_position'])) &&
				(isset($params_job['text']) && !empty($params_job['text']))
			)
			{
				//$params_job['text']=stripslashes($params_job['text']);
				//$params_job['draw']=str_replace('"','\"',$params_job['draw']);
				$options_convert.=' -font "'.jobFontDir.'/'.$params_job['font'].'" -pointsize '.$params_job['pointsize'];
				$options_convert.=' -draw "gravity '.$params_job['gravity'].' fill '.$params_job['fill'].' text '.$params_job['text_position'].' \''.$params_job['text'].'\'"';
			}
			


			$options_convert.=' '.escapeQuoteShell($fileout);
            if(eregi('WINNT',PHP_OS)){
                $this->writeJobLog(kConvertPath.' '.$options_convert);
                shell_exec(kConvertPath.' '.$options_convert);
            }else{
                $this->shellExecute(kConvertPath,$options_convert.'');
            }
		}
		if(isset($params_job['encapsulage']) && $params_job['encapsulage'] == '1' && !empty($params_job['enc_left']) && !empty($params_job['enc_right']) ){
				$infos=MatInfo::getMatInfo($fileSrc);

				/*
				if(!empty($params_job['width'])){
					$width_cartouche=$params_job['width'];
				}
				else{
					$width_cartouche=$infos['width'];
				}*/
				if(!empty($width)){
					$width_cartouche=$width;
				}else{
					$width_cartouche=$infos['width'];
				}
				if($width_cartouche > '300') {
					//parametre
					if(!empty($params_job['pointsize'])){
						$pointsize=$params_job['pointsize'];
						$height_cartouche=$pointsize + 6;
					}
					else{
						//seuil avant automatisation
						// if($width_cartouche <= 800){
						// 	$height_cartouche='20';
						// 	$pointsize='14';
						// }elseif($width_cartouche >= 4000){
						// 	$height_cartouche='64';
						// 	$pointsize='56';
						// }
						// else{
						// 	$height_cartouche='34';
						// 	$pointsize='28';
						// }

						//seuil calculé
						$height_cartouche = floor($width_cartouche / 60) ;
						$pointsize= $height_cartouche - 9 ;
					}
					$options_convert=escapeQuoteShell($fileSrc."[0]");
/*
					if ((isset($params_job['width']) && !empty($params_job['width'])) && (isset($params_job['height']) && !empty($params_job['height']))) {
						$options_convert.=' -filter Lanczos -resize '.$params_job['width'].'x'.$params_job['height'];
					}
 */
					if (!empty($width) && !empty($height)) {
						$options_convert.=' -filter Lanczos -resize '.$width.'x'.$height;
					}
					if(isset($params_job['dpi']) && !empty($params_job['dpi']) && intval($params_job['dpi']) > 0) {
						$options_convert.=' -density '.intval($params_job['dpi']).'x'.intval($params_job['dpi']);
					}
					
					$options_convert.= ' \( -size '.$width_cartouche.'x'.$height_cartouche.' pattern:gray100  -font "'.jobFontDir.'/'.$params_job['font'].'" -pointsize '.$pointsize.' -draw "gravity SouthWest text 10,0 \''.$params_job['enc_left'].'\'" -draw "gravity SouthEast text 10,0 \''.$params_job['enc_right'].'\'" \) ';

					$options_convert.=' -append ';
					
					$options_convert.=' '.escapeQuoteShell($fileout);
					//$options_convert.=' -append  "'.$fileout.'"';
					
					if(eregi('WINNT',PHP_OS)){
						$this->writeJobLog(kConvertPath.' '.$options_convert);
						shell_exec(kConvertPath.' '.$options_convert);
					}else{
						$this->shellExecute(kConvertPath,$options_convert.'');
					}
				}
			}
	// On execute ce code apres que l image soit générée
		if(isset($params_job['iptc_write'])){
			require_once(libDir."class_iptc.php");

			foreach ($params_job['iptc_write'] as $value)
			{
				foreach ($value as $key => $value){
					$new_key= str_replace('iptc_','', $key);
					 $iptc_val[$new_key]=$value;
				}
			}
			$iptc=new class_iptc($fileout);
			$ip=$iptc->fct_ecrireIPTC($iptc_val,$iptc->h_cheminImg);
		}

		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}

	public function finishJob()
	{
		parent::finishJob();
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
