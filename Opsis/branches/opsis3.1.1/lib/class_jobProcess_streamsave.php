<?php

require_once('class_jobProcess.php');
require_once('class_matInfo.php');
require_once('interface_Process.php');

class JobProcess_streamsave extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('streamsave');
		
		$this->required_xml_params=array
		(
		);
		
		$this->optional_xml_params=array
		(
			'duration'=> 'int',
			'time'=> 'string',
			'date_debut'=>'string',
			'date_fin'=>'string',
			'stream_name'=>'string',
			'stream_server'=>'string',
			'stream_mode'=>'string',
			'nb_retries'=>'int'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{

	/*
		// rename du fichier out de streamsave avec des infos représentatives, à déplacer un niveau au dessus, probablement dans la création de l'étape. 
		$date_info = "" ; 
		if(isset($this->params_job['date_debut']) && !empty($this->params_job['date_debut'])){
			$date_info = trim(preg_replace('/[\s]+/g','_',$this->params_job['date_debut']),'_');
		}
		if(isset($this->params_job['date_fin']) && !empty($this->params_job['date_fin'])){
			if(!empty($date_info)) $date_info.='_';
			$date_info = trim(preg_replace('/[\s]+/g','_',$this->params_job['date_fin']),'_');
		}
		if(isset($this->params_job['duration']) && !empty($this->params_job['duration'])){
			if(!empty($date_info)) $date_info.='_';
			$date_info = str_replace(':','_',secToTime($this->params_job['duration']));
		}*/
	
		$this->writeOutLog('Debut traitement');
		$this->params_job=$this->getXmlParams();

		// URL source
		if(	isset($this->params_job['stream_server']) && !empty($this->params_job['stream_server'])
			&& isset($this->params_job['stream_mode']) && strtolower($this->params_job['stream_mode']) == 'hls'
			&& isset($this->params_job['stream_name']) && !empty($this->params_job['stream_name'])){
			$url_in = $this->params_job['stream_server'].'/'.$this->params_job['stream_name'].'/playlist.m3u8' ; 
		}else{
			$url_in = $this->getFileInPath();
		}
		
		// if(get_headers($url_in))
		
		
		// MS - 04.04.18 - Dans le cas ou l'url à enregistrer est en https, on remplace https par http car ffmpeg a besoin d'être compilé avec openssl pour gérer les urls https. 
		// workaround à désactiver si on a ffmpeg avec openssl 
		if(strpos($url_in,'https') !== false){
			$url_in = str_replace('https','http',$url_in);
		}
		
		
		$options = "-i ".escapeQuoteShell($url_in)." ";
		
		// copy de la vidéo 
		$options .= " -c copy ";
		// encodage audio  
		$options .= " -bsf:a aac_adtstoasc ";
		
		if(isset($this->params_job['time']) && !empty($this->params_job['time'])
			&& (!isset($this->params_job['duration']) || !empty($this->params_job['duration']))){
			$this->params_job['duration'] = timeToSec($this->params_job['time']);
		}
		
		if(isset($this->params_job['duration']) && !empty($this->params_job['duration'])){
			$options .= " -t ".$this->params_job['duration'];
		}
		
		
		$tmp_file=stripExtension($this->getFileOutPath()).'_'.time().'_'.rand(100000,999999).'.'.getExtension($this->getFileOutPath());
		
		$options .= " ".escapeQuoteShell($tmp_file)." ";
		
		if(!empty($this->params_job['nb_retries'])) {
			$nbRetriesMax = $this->params_job['nb_retries'];
		} else {
			$nbRetriesMax = 1;
		}
		$retry = 0;
		$ok = false;
		$delay = 5 ; 
		while($ok===false && $retry<$nbRetriesMax) {
			$retry++;
			$ok=false;
			$nbEssaisRestants = $nbRetriesMax - $retry;
			if($nbRetriesMax > 1) {
				$msgEssaisRestants = $retry. " essais effectués";
			}
			$headers_url = get_headers($url_in);
			// $this->writeJobLog("headers_url : ".print_r($headers_url,1));
			if(isset($headers_url[0]) && preg_match('/(200 OK)/',$headers_url[0])){
				$ok = true ; 
			}
			if(!$ok){
				if( $retry == $nbRetriesMax) {
					throw new Exception($url_in.' not available');
				}  else {
					sleep($delay);
					$this->writeOutLog("$url_in not available. Tentative n°".($retry+1)." en attente.");
				}
			}
			
		};
			
		$this->shellExecute(kFFMPEGpath,$options,false);
		
		do
		{
			sleep(1);
			$this->updateProgress();
		}
		while($this->checkIfFfmpegRunning());
		$this->updateProgress() ; 
		
		rename($tmp_file,$this->getFileOutPath());
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function finishJob()
	{
	
		parent::finishJob();
	}
	
	public function updateProgress()
	{
		// progression avec ffmbc
		$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
		$donnees_stderr=str_replace("\r","\n",$donnees_stderr);
		$this->WriteOutLog('updateProgress donnees_stderr : '.print_r($donnees_stderr,true));
		
		// VP 8/04/13 : compatibilité Mac OS X
		preg_match_all('/frame=([ 0-9]+) fps=([ 0-9.]+) q=([- 0-9.]+) (Lsize|size)=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
		
		try{
			$time = explode('.',$infos_encodage[6][count($infos_encodage[6])-1]);
			$time = tcToSec($time[0]) + ($time[1]/100);
		}catch(Exception $e){}
		if(isset($time) && isset($this->params_job['duration']) && !empty($this->params_job['duration'])){
			$percent = $time *100 / $this->params_job['duration'] ; 
			$this->setProgression($percent);
		}else {
			$this->setProgression(-1);
		}
		
		// $this->nb_frames_encodees=intval($infos_encodage[1][count($infos_encodage[1])-1]);
		
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
	
	
	private function checkIfFfmpegRunning()
	{
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$this->getPid().'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1)
				return true;
			else
				return false;
		}
	}
}

?>
