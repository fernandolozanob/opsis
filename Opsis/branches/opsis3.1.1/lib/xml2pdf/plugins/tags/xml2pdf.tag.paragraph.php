<?php
/**
 * paragraph tag plugin file.
 *
 * @filesource
 *
 * @author guillaume l. <guillaume@geelweb.org>
 * @link http://www.geelweb.org
 * @license http://opensource.org/licenses/bsd-license.php BSD License 
 * @copyright copyright © 2006, guillaume luchet
 * @package Xml2Pdf
 * @subpackage Tag
 * @version CVS:m $Id: xml2pdf.tag.paragraph.php,v 1.3 2006/12/26 08:38:00 geelweb Exp $
 */

// dependances {{{
/**
 * parent class
 */
require_once('Xml2PdfTextTag.php');
// }}}
// doc {{{

/**
 * <paragraph> tag.
 *
 * This tag is used to wride text with options of displaying like
 * background, borders etc...
 *
 * {@example paragraph.xml}
 *
 * @author guillaume l. <guillaume@geelweb.org>
 * @link http://www.geelweb.org
 * @license http://opensource.org/licenses/bsd-license.php BSD License 
 * @copyright copyright © 2006, guillaume luchet
 * @package Xml2Pdf
 * @subpackage Tag
 * @tutorial Xml2Pdf/Xml2Pdf.Tag.paragraph.pkg
 * @version CVS $Id: xml2pdf.tag.paragraph.php,v 1.3 2006/12/26 08:38:00 geelweb Exp $
 */ // }}}
Class xml2pdf_tag_paragraph extends Xml2PdfTextTag {
    // class properties {{{
    
    /**
     * paragraph width.
     * @var float
     */
    public $width = null;
    
    /**
     * draw paragraph borders.
     * @var boolean
     */
    public $border = PDF_DEFAULT_PARAGRAPH_BORDER;
    
    /**
     * paragraph borders color.
     * @var string
     */
    public $borderColor = PDF_DEFAULT_PARAGRAPH_BORDERCOLOR;
    
    /**
     * fill the paragraph.
     * @var boolean
     */
    public $fill = PDF_DEFAULT_PARAGRAPH_FILL;
    
    /**
     * paragraph fill color.
     * @var string
     */
    public $fillColor = PDF_DEFAULT_PARAGRAPH_FILLCOLOR;
    
    /**
     * paragraph top margin.
     * @var float
     */
    public $top = 0;

    /**
     * paragraph left margin.
     * @var float
     */
    public $left = 0;

    /**
     * paragraph cursor position apres render
     * @var float
     */
    public $ln = 1;



    /**
     * type of positioning.
     * @var string
     */
    public $position = PDF_DEFAULT_PARAGRAPH_POSITION;

    /**
     * truncate
     * @var bool
     */
    public $truncate_to_fit = false;
   

   /**
     * cell ou multicell
     * @var bool
     */
    public $multi = false;

    
	 /**
     * paragraph max_height => si multi && truncate_to_fit, alors on va cacher le texte a partir de la ligne qui dépassera max_height
     * @var float
     */
	public $max_h = null;
	

    /**
     * paragraph alignment
     * @var string
     */
    public $align = PDF_DEFAULT_PARAGRAPH_ALIGN;

	
	
	

    /**
     * parent tag.
     * @var object
     */
    protected $parent = false;

    // }}}
    // xml2pdf_tag_paragraph::__construct() {{{
    
    /**
     * Constructor.
     *
     * Parse the tag attributes.
     *
     * @param array $tagProperties tag attributes
     * @param object $parent parent tag
     * @return void
     */
    public function __construct($tagProperties, $parent) {
        parent::__construct($tagProperties);
        if(is_a($parent, 'xml2pdf_tag_header') || is_a($parent, 'xml2pdf_tag_footer')) {
            $this->parent = $parent;
        }
		
        // parse properties
        if(isset($tagProperties['WIDTH'])) {
            $this->width = $tagProperties['WIDTH'];
        }
        if(isset($tagProperties['BORDER'])) {
            $this->border = $tagProperties['BORDER'];
        }
        if(isset($tagProperties['BORDERCOLOR'])) {
            $this->borderColor = $tagProperties['BORDERCOLOR'];
        }
        if(isset($tagProperties['FILL'])) {
            $this->fill = $tagProperties['FILL'];
        }
        if(isset($tagProperties['FILLCOLOR'])) {
            $this->fillColor = $tagProperties['FILLCOLOR'];
        }
        if(isset($tagProperties['TOP'])) {
            $this->top = $tagProperties['TOP'];
        }
        if(isset($tagProperties['LEFT'])) {
            $this->left = $tagProperties['LEFT'];
        }
        if(isset($tagProperties['POSITION'])) {
            $this->position = (strtolower($tagProperties['POSITION'])=='absolute')?
                'absolute':'relative';
        }
        if(isset($tagProperties['LN'])) {
            $this->ln = $tagProperties['LN'];
        }

        if(isset($tagProperties['LINEHEIGHT'])) {
            $this->lineHeight = $tagProperties['LINEHEIGHT'];
        }
		if(isset($tagProperties['TRUNCATE_TO_FIT'])) {
            $this->truncate_to_fit = $tagProperties['TRUNCATE_TO_FIT'];
        }

		if(isset($tagProperties['MULTI'])) {
            $this->multi = $tagProperties['MULTI'];
        }
		if(isset($tagProperties['MAX_H'])) {
            $this->max_h = $tagProperties['MAX_H'];
        }
		
		
		
		
        if(isset($tagProperties['ALIGN'])) {
            switch(strtoupper($tagProperties['ALIGN'])) {
                case 'L':
                case 'LEFT':
                    $this->align = 'L';
                    break;
                case 'R':
                case 'RIGHT':
                    $this->align = 'R';
                    break;
                case 'C':
                case 'CENTER':
                    $this->align = 'C';
                    break;
            }
        }
        if(isset($tagProperties['TEXTALIGN'])) {
            switch(strtoupper($tagProperties['TEXTALIGN'])) {
                case 'L':
                case 'LEFT':
                    $this->textAlign = 'L';
                    break;
                case 'R':
                case 'RIGHT':
                    $this->textAlign = 'R';
                    break;
                case 'C':
                case 'CENTER':
                    $this->textAlign = 'C';
                    break;
            }
        }
    }

    // }}}
    // xml2pdf_tag_paragraph::close() {{{
    
    /**
     * Render the paragraph or add it to the parent tag.
     * 
     * @return void
     */
    public function close() {
        if($this->parent) {
            $this->parent->elements[] = $this;
            return;
        }
        // calc the paragraph left and top using : align, position, left and top
        if($this->position=='absolute') {
            $x = $this->left;
            $y = $this->top;
        } else {
            $pageWidth = 210 - $this->pdf->lMargin - $this->pdf->rMargin;
            if($this->left) {
                $x = $this->pdf->GetX() + $this->left;
            } elseif($this->align) {
                if($this->align=='L') {
                    $x = $this->pdf->lMargin;
                } elseif($this->align=='R') {
                    $x = $pageWidth - $this->width + $this->pdf->rMargin;
                } elseif($this->align=='C') {
                    $x = ($pageWidth - $this->width) / 2 + $this->pdf->lMargin;
                }
            } else {
                $x = $this->pdf->GetX();
            }
            if($this->top) {
                $y = $this->pdf->GetY() + $this->top;
            } else {
                $y = $this->pdf->GetY();
            }
        }
        if(!$x) {
            $x = $this->pdf->lMargin;
        }
        if(!$y) {
            $y = $this->pdf->tMargin;
        }
		
		if($this->align=='A'){
			$this->pdf->SetY($y);
		}else{
			$this->pdf->SetXY($x, $y);
		}
        // set the paragraph font, fill, border and draw params
        $borderColor = Xml2Pdf::convertColor($this->borderColor);
        $this->pdf->SetDrawColor($borderColor['r'], $borderColor['g'],
            $borderColor['b']);
        $fillColor = Xml2Pdf::convertColor($this->fillColor);
        $this->pdf->SetFillColor($fillColor['r'], $fillColor['g'], 
            $fillColor['b']);        
        $fontColor = Xml2Pdf::convertColor($this->fontColor);
        $this->pdf->setTextColor($fontColor['r'], $fontColor['g'], 
            $fontColor['b']);
			

		$this->pdf->setFont($this->font, $this->fontStyle, $this->fontSize);
        // write the content


		if($this->truncate_to_fit){
			$full_content_width = $this->pdf->GetStringWidth($this->content);
			
			if($this->multi && isset($this->max_h)){
				$rows_required = ceil($full_content_width / $this->width);
				if($rows_required>1){ 
					$rows_shown =  floor($this->max_h / $this->lineHeight);
					$arr_content = explode(" ",$this->content);
					$this->content = "";
					$j = 0;
					for ($i = 0 ; $i < $rows_shown ; $i++ ){
						$true_content = ""; 
						while ($j < count($arr_content) && ($this->pdf->GetStringWidth($true_content)+$this->pdf->GetStringWidth(" ".$arr_content[$j])+0.5) < $this->width ){
							$true_content.= " ".$arr_content[$j];
							$j++;
						}
						if($this->content == "" ){
							$true_content = substr($true_content,1);
						}
						if($i == ($rows_shown-1) && $j != count($arr_content)){
							if(($this->pdf->GetStringWidth($true_content." ...")+1)>=$this->width){
								$true_content = substr($true_content,0,strlen($true_content)-5)." ...";
							}else{
								$true_content.=" ...";
							}
						}
						$this->content .=$true_content;
					}
				}				
			}else{
				if($full_content_width > $this->width){
					$this->content = substr($this->content,0,(strlen($this->content)*$this->width/$full_content_width)-5)." ...";
				}
			}
		}
		
		
		
		if($this->multi ){
			// echo "w :".$this->width." lh : ".$this->lineHeight." \n".$this->content." align : ".$this->textAlign;
			
			if($this->ln=="0" || $this->ln==0){
				$temp_x = $this->pdf->GetX();
				$temp_y = $this->pdf->GetY();
			}else if ($this->ln=="1" || $this->ln==1){
				$temp_x = $this->pdf->GetX();
			}
			
			$this->pdf->multicell($this->width, $this->lineHeight,$this->content,
                              $this->border , $this->textAlign, $this->fill);
							  
			if($this->ln=="0" || $this->ln==0){
				$this->pdf->SetX(floatval ($temp_x)+floatval($this->width));
				$this->pdf->SetY(floatval ($temp_y));
			}else if ($this->ln=="1" || $this->ln==1){
				$this->pdf->SetX(floatval ($temp_x));
			}
							  
							  
			
		// echo " ".$this->pdf->GetX().",".$this->pdf->GetY();					  
			
							  
							  
		}else{
			$this->pdf->cell($this->width, $this->lineHeight,$this->content,
                              $this->border,$this->ln , $this->textAlign, $this->fill);
		}
							  
		// if($this->ln == 0){
			// $this->pdf->SetXY($x+$this->width,$y);
		// }else if ($this->ln == 1){
			/*$this->pdf->SetX($x);*/
		// }
		// echo " ".$this->pdf->GetX().",".$this->pdf->GetY();					  
		
	}

    // }}}
}
?>
