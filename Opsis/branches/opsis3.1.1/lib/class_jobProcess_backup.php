<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_fileNotFoundException.php');

class JobProcess_backup extends JobProcess implements Process
{
	private $id_job;

	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('backup');
		
		$this->required_xml_params=array
		(
            'action'=>'string'
		);
		
		$this->optional_xml_params=array
		(
            'device'=>'string',
			'tape'=>'string',
			'drive'=>'string',
			'chemin'=>'string'
		);
		
		$donnees_xml_name=explode('_',stripextension($file_xml));
		$this->id_job=intval($donnees_xml_name[3]);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		// verification des parametres et de leurs types
		if (!empty($this->required_xml_params))
		{
			foreach($this->required_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
				{
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
				}
				else if (isset($this->params_job[$param_name]) && empty($this->params_job[$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($this->optional_xml_params))
		{
			foreach($this->optional_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
			}
		}
	}
	
	function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		
		// VP 12/05/2017 : Fin traitement écrit dans le log par shell sauf dans certains cas
		switch($params_job['action'])
		{
			case 'listing':
                if(!defined('gLtoListingPath')) {
					throw new FileNotFoundException('Binaire manquant : gLtoListingPath');
                    break;
                }
                if(!is_file($this->getOutLogPath())){
                    throw new FileNotFoundException('Fichier log manquant '.$this->getOutLogPath());
                    break;
                }
                $infoFile=kBackupInfoDir.'/'.$params_job['device'].'.xml';
                $cmd=gLtoListingPath.' '.$this->getOutLogPath().' '.$infoFile.' '.$params_job['device'];
                $this->shellExecute($cmd);

				break;
                
			case 'backup':
                if(!defined('gLtoBckFilePath')) {
					throw new FileNotFoundException('Binaire manquant : gLtoBckFilePath');
                    break;
                }
                if(!is_file($this->getOutLogPath())){
                    throw new FileNotFoundException('Fichier log manquant '.$this->getOutLogPath());
                    break;
                }
                if(empty($params_job['tape'])){
					throw new InvalidXmlParamException('tape is not set');
                    break;
                }
                $drive=$params_job['drive'];
                if(empty($drive)){
                    $drive='nst0';
                }
                // VP 18/10/2012 : ajout paramètre chemin
                $chemin=$params_job['chemin'];
                $pos=0;
                if(!empty($chemin)){
                    // traitement du chemin pour supprimer / de début ou //
                    $chemin=trim(str_replace('//','/',$chemin.'/'.basename($this->getFileInPath())),'/');
                    $pos=strpos($this->getFileInPath(),$chemin);
                }
                if($pos>0){
                    $folder=substr($this->getFileInPath(),0,$pos);
                    $fileToBck=substr($this->getFileInPath(),$pos);
                }
                else{
                    $folder=dirname($this->getFileInPath());
                    $fileToBck=basename($this->getFileInPath());
                }
                $cmd=gLtoBckFilePath.' "'.$fileToBck.'" "'.$folder.'" '.$params_job['tape'].' "'.$this->getOutLogPath().'" '.$drive.' '.$params_job['device'];
				$this->shellExecute($cmd);
				break;
                
			case 'restore':
                if(!defined('gLtoRestFilePath')) {
                    throw new FileNotFoundException('Binaire manquant : gLtoRestFilePath');
                    break;
                }
                if(!is_file($this->getOutLogPath())){
                    throw new FileNotFoundException('Fichier log manquant '.$this->getOutLogPath());
                    break;
                }
                if(empty($params_job['tape'])){
                    throw new InvalidXmlParamException('Paramètre manquant : tape');
                    break;
                }
                if(!isset($params_job['numfile'])){
                    throw new InvalidXmlParamException('Paramètre manquant : numfile');
                    break;
                }
                $drive=$params_job['drive'];
                if(empty($drive)){
                    $drive='nst0';
                }
                $chemin=$params_job['chemin'];
                $pos=0;
                if(!empty($chemin)){
                    // traitement du chemin pour supprimer / de début ou //
                    $chemin=trim(str_replace('//','/',$chemin.'/'.basename($this->getFileOutPath())),'/');
                    $pos=strpos($this->getFileOutPath(),$chemin);
                }
                if($pos>0){
                    $folder=substr($this->getFileOutPath(),0,$pos);
                    $fileToRest=substr($this->getFileOutPath(),$pos);
                }
                else{
                    $folder=dirname($this->getFileOutPath());
                    $fileToRest=basename($this->getFileOutPath());
                }
                $path=dirname($this->getFileOutPath());
                if (!is_dir($path))
                    mkdir($path,0755,true);
                
                $this->setProgression(1);
                $cmd=gLtoRestFilePath.' "'.$fileToRest.'" "'.$folder.'" '.$params_job['tape'].' '.$params_job['numfile'].' "'.$this->getOutLogPath().'" '.$drive.' '.$params_job['device'];
				$this->shellExecute($cmd);
                
                // VP 8/09/14 : extraction des fichiers tar
                if(isset($params_job['extract']) && $params_job['extract']==1 && getExtension($this->getFileOutPath())=='tar'){
                    $this->setProgression(50);
                    try {
                        $extractDir = str_replace(".tar", "_extract/", $this->getFileOutPath());
                        if (!is_dir($extractDir)){
                            mkdir($extractDir);
                            $this->shellExecute(kBinTar,'xf "'.$this->getFileOutPath().'" -C "'.$extractDir.'"');					
                        }
                    } catch (Exception $e) {
                        throw new FileNotFoundException('Can\'t open the archive : '.$e->message);
                    }
                }
                
				$this->writeOutLog('Fin traitement');
				break;
                
            case 'restoreOffline':
                if(empty($params_job['url_ws'])){
                    throw new InvalidXmlParamException('Paramètre manquant : url_ws');
                    break;
                }
                if(empty($params_job['restore_dir'])){
                    throw new InvalidXmlParamException('Paramètre manquant : restore_dir');
                    break;
                }
                if(!is_file($this->getOutLogPath())){
                    throw new FileNotFoundException('Fichier log manquant '.$this->getOutLogPath());
                    break;
                }
                if(empty($params_job['tape'])){
                    throw new InvalidXmlParamException('Paramètre manquant : tape');
                    break;
                }
                if(!isset($params_job['numfile'])){
                    throw new InvalidXmlParamException('Paramètre manquant : numfile');
                    break;
                }
                $chemin=$params_job['chemin'];
                $pos=0;
                if(!empty($chemin)){
                    // traitement du chemin pour supprimer / de début ou //
                    $chemin=trim(str_replace('//','/',$chemin.'/'.basename($this->getFileOutPath())),'/');
                    $pos=strpos($this->getFileOutPath(),$chemin);
                }
                if($pos>0){
                    $folder=substr($this->getFileOutPath(),0,$pos);
                    $fileToRest=substr($this->getFileOutPath(),$pos);
                }
                else{
                    $folder=dirname($this->getFileOutPath());
                    $fileToRest=basename($this->getFileOutPath());
                }
                $path=dirname($this->getFileOutPath());
                if (!is_dir($path))
                    mkdir($path,0755,true);
                
                $this->setProgression(1);

                // Appel web service restore
                $params_curl = $params_job;
                $params_curl['file']=$fileToRest;
                
                $curl=curl_init($params_job['url_ws']);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $params_curl);
                
                $response = curl_exec($curl);
                $info = curl_getinfo($curl);
                
                if ($response === false || $info['http_code'] != 200) {
                    $ok=false;
                } else {
                    $ok=true;
                }
                curl_close($curl);
                
                if($ok){
                    $this->setProgression(10);
                    $fileRestored = $params_job['restore_dir'].'/'.$fileToRest;
                    // Boucle d'attente
                    $nofile = true;
                    while ($nofile){
                        if (is_file($fileRestored)){
                            if( ! $this->fileOpened($fileRestored, $params_job['ftp_user'])){
                                $nofile = false;
                            }
                        }
                        sleep(10);
                    }
                    mv_rename($fileRestored, $this->getFileOutPath());
                    // Extraction des fichiers tar
                    if(isset($params_job['extract']) && $params_job['extract']==1 && getExtension($this->getFileOutPath())=='tar'){
                        $this->setProgression(50);
                        try {
                            $extractDir = str_replace(".tar", "_extract/", $this->getFileOutPath());
                            if (!is_dir($extractDir)){
                                mkdir($extractDir);
                                $this->shellExecute(kBinTar,'xf "'.$this->getFileOutPath().'" -C "'.$extractDir.'"');
                            }
                        } catch (Exception $e) {
                            throw new FileNotFoundException('Can\'t open the archive : '.$e->message);
                        }
                    }
                }
				$this->writeOutLog('Fin traitement');
                break;

            default:
				throw new InvalidXmlParamException('unknown param action ('.$params_job['action'].')');
				break;
		}
		
	}
	
    function fileOpened($file,$usager) {
        if(defined("kFtpAvailableFilePath")){
            $cmd=kFtpAvailableFilePath." ".escapeQuoteShell($file)." ".$usager;
            trace($cmd);
            $output = exec($cmd, $array = array(), $return);
            if($return===0) return false;
            else return true;
        }else{
            $cmd="/usr/sbin/lsof -l \"".$file."\" | grep -c \"".$file."\"";
            $res=shell_exec($cmd);
            trace($cmd);
            if ($res>0) return true;
            return false;
            
        }
    }

    public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
