<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_networkException.php');

class JobProcess_webservice extends JobProcess implements Process
{
	
	private $app_token;
	private $upload_key;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('webservice');
		$this->required_xml_params=array
		(
			'url'=>'string'
		);
		
		$this->optional_xml_params=array
		(
			'post'=>'array'
		 );
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		
		$ok=false;
		$retry=0;
		
		while($ok===false && $retry<3)
		{
			$curl=curl_init($params_job['url']);
			if(isset($params_job['post'])){
				$params_post=$params_job['post'][0];
			}
			foreach($params_job as $k=>$p){
				if($k!='url' && $k != 'post'){
					$params_post[$k]=$p;
				}
			}
			$params_post['file_in'] = $this->getFileInPath();
			
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($curl, CURLOPT_TIMEOUT, 12000);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params_post);

			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			
			if ($response === false || $info['http_code'] != 200)
			{
				$ok=false;
			}
			else
			{
				$ok=true;
			}
			curl_close($curl);
			
			$retry++;
		}
		
		if ($ok==false)
		{
			throw new NetworkException('http error ('.$info['http_code'].' ->'.curl_error($curl).')');
		}
		else
		{
			$this->writeOutLog("Retour callWebService ".$response);
		}
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
