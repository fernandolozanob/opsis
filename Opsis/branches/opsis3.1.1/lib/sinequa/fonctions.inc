<?	
	static $userDatabases;
	
	function checkUsers()
	{
		global $authentificationPath;
		global $userDatabases;		
		if(!isset($userDatabases))
		{
			$userDatabases=new UserDatabase($authentificationPath);			
		}
	}

	function getValue($name,$def)
	{
		if(isset($_POST[$name]))
		{
			return trim(escapeMagicQuote($_POST[$name]));
		}
		else if(isset($_GET[$name]))
		{
			return trim(escapeMagicQuote($_GET[$name]));
		}			
		else return trim(escapeMagicQuote($def));
	}
	
	function escapeMagicQuote(&$s)
	{
		$s=str_replace('\\"','"',$s);
		$s=str_replace("\\'","'",$s);
		return $s;
	}
	
	function getCookieValue($name,$def)
	{
		if(isset($_COOKIE[$name]))
		{
			return $_COOKIE[$name];
		}
		else return $def;		
	}
	
	function trimXSLResult($content)
	{		
		return ereg_replace("<html>(.+)</html>","<HTML>\\1</HTML>",$content);
	}
	
	function createPreviousForm($options)
	{
		$content="";
		$keys=array_keys($_POST);
		$r_keys=array();
					for($i=0;$i<count($keys);$i++)
					{
						if($keys[$i]!="psw" && !in_array( $keys[$i],$r_keys))
						{
							$content.="<input type=\"hidden\" id=\"".$keys[$i]."\" name=\"".$keys[$i]."\" value=\"".nettoyer($_POST[$keys[$i]])."\">\n";	
							array_push($r_keys,$keys[$i]);
						}
					}	

					if(isset($options))
					{
						for($i=0;$i<count($options);$i+=2)
						{
							if( !in_array( $options[$i],$r_keys) )
							{
								$content.="<input type=\"hidden\" id=\"".$options[$i]." name=\"".$options[$i]."\" value=\"".nettoyer($options[$i+1])."\">\n";
								array_push($r_keys,$options[$i]);
							}
						}
					}
		return $content;
	}
	
	function createHeaderTop($action,$options)
	{
		$header="<div>\n";
		$header.="<form action=\"$action\" method=\"POST\" name=\"previous-page\" id=\"previous-page\" style=\"display:none\">\n";
		$header.=createPreviousForm($options);			
		$header.="</form>\n";
		$header.="<a href=\"javascript:document.getElementById('previous-page').submit()\">Page précédente</a>\n";
		$header.="<br><br></div>\n";
		return $header;
	}
	
	function shortString($s,$max,$end)
	{
		if(strlen($s)>$max)
		{
			return substr($s,0,$max).$end;
		}else return $s;
	}
	
	function getShortArticlePath($serialization)
	{
		$grp=array();
		$r=ereg("([0-9]{4}/[0-9]{2}/[0-9]{2}/.+\\.xml)",$serialization,$grp);
		return $grp[1];
	}
		
	function getArticlePath($serialization)
	{
		global $repository_root;
		
		return $repository_root."/".getShortArticlePath($serialization);
	}
	
	function getRemoteArticlePath($serialization)
	{
		global $repository_remote_root;
		$grp=array();
		$r=ereg("([0-9]{4}/[0-9]{2}/[0-9]{2}/.+\\.xml)",$serialization,$grp);
		return $repository_remote_root."/".$grp[1];
	}
	
	function getPdfPath($serialization,$edition,$page)
	{
		global $repository_root;
		$grp=array();
		$r=ereg("([0-9]{4}/[0-9]{2}/[0-9]{2})/(.+)-.+\\.xml",$serialization,$grp);
		return $repository_root."/".$grp[1]."/".$grp[2]."_0$edition"."_"."$page.pdf";
	}
	
	function getRemotePdfPath($serialization,$edition,$page)
	{
		global $repository_remote_root;
		$grp=array();
		$r=ereg("([0-9]{4}/[0-9]{2}/[0-9]{2})/(.+)-.+\\.xml",$serialization,$grp);
		return $repository_remote_root."/".$grp[1]."/".$grp[2]."_0$edition"."_"."$page.pdf";
	}
	
	function getNextArticleSerialization($serialization)
	{
		$r=ereg("([0-9]{4}/[0-9]{2}/[0-9]{2})/(.+)-(.+)\\.xml",$serialization,$grp);
		$id= $grp[1]."/".$grp[2]."-".($grp[3]+1).".xml";
		$path="file://".getArticlePath($id);
		if( file_exists($path) )return $id;
		else return $serialization;
	}	
	
	function getPreviousArticleSerialization($serialization)
	{
		$r=ereg("([0-9]{4}/[0-9]{2}/[0-9]{2})/(.+)-(.+)\\.xml",$serialization,$grp);
		$id= $grp[1]."/".$grp[2]."-".($grp[3]-1).".xml";
		$path=getArticlePath($id);
		if( file_exists($path) )return $id;
		else return $serialization;
	}	
		
	function nettoyer($s)
	{
		$s=str_replace("\\","",$s);		
		$s=htmlspecialchars($s);
		return $s;		
	}	
	
	function parseDistribution($s)
	{
		$cpts=array();
		$tks=split(";",$s);
		for($i=0;$i<count($tks);$i++)
		{			
			if(ereg("(.+)\t(.+)",$tks[$i],$tmp))
			{
				$pv=new IPaveItem();
				$pv->label=$tmp[1];
				$pv->occurrences=$tmp[2];
				array_push($cpts,$pv);
			}
		}
		return $cpts;
		}
		
	function getArticleDatabase($year)
	{
		global $database_article_base_year;
		if($year<2003)return "huma";
		$delta= ($year-$database_article_base_year)%2;
		return "huma-".($year-$delta)."-".(($year-$delta)+1);
	}
	
	function getAllArticleDatabase()
	{
		if(true)return "*";
		global $database_article_base_year;
		$db="huma";
		$cyear=getCurrentYear()+1;
		for($i= $database_article_base_year;$i<$cyear;$i+=2)
		{
			$db.=",huma-$i-".($i+1);
		}
		return $db;
	}

	function getDatabasesByDates($startDate,$endDate)
	{				
		if(true)return "*";
		global $database_article_base_year;
		$startDate=trim($startDate);
		$endDate=trim($endDate);
		if(strlen($startDate)==0 && strlen($endDate)==0)return getAllArticleDatabase();
		$dbs="";
		$startDBDate="";
		$endDBDate="";
		$currentYear=getCurrentYear();
		
		if(strlen($startDate)>0 && verifierDateFr($startDate))
		{			
			ereg("[0-9]{2}/[0-9]{2}/(19|20[0-9]{2})",$startDate,$tmp);
			$startDBDate=$tmp[1];			
			if($startDBDate<$database_article_base_year)$startDBDate=$database_article_base_year;
		}
		if(strlen($endDate)>0 && verifierDateFr($endDate))
		{
			ereg("[0-9]{2}/[0-9]{2}/(19|20[0-9]{2})",$endDate,$tmp);
			$endDBDate=$tmp[1];
			if($endDBDate<$database_article_base_year)$endDBDate=$database_article_base_year;
			if($endDBDate>getCurrentYear())$endDBDate=$currentYear;
		}
		if($startDBDate!="" && $endDBDate=="")
		{
			$endDBDate=$currentYear;
			
		}
		if($startDBDate=="" && $endDBDate!="")
		{
			$startDBDate=$database_article_base_year;
		}	

		if($startDBDate!="" && $endDBDate!="")
		{
			if($endDBDate>$currentYear)$endDBDate=$currentYear;
			$k=0;
			for($i=$startDBDate;$i<($endDBDate+1);$i++)
			{
				if($i>$currentYear)break;				
				$db= getArticleDatabase($i);				
				if( strpos($dbs,$db)===false )
				{
					$dbs.=(strlen($dbs)>0 ? "," : "").$db;
					$k++;
				}
				if($k>40)break;				
			}
		}	
		
		return strlen($dbs)==0 ? getAllArticleDatabase() : $dbs;	
	}		
	
	function getTodayDate()
	{
		return date('Y-m-d');
	}
	
	function getCurrentMonth()
	{		
		return date('m');
	}
	
	function getCurrentDay()
	{
		return date('d');
	}
	
	function getCurrentYear()
	{
		return date('Y');
	}
	
	function saveCookie($name,$zecookie,$expireDate)
	{
		if( isset($expireDate) && $expireDate!=null) 
		{
			$r=setcookie($name,"",convertDate2Long($expireDate));
			if(!$r)return $r;			
		}
		else
		{
			$r=setcookie($name,"", time()+3600);
			if(!$r)return $r;	
		}
		$kys=array_keys($zecookie);
		for($i=0;$i<count($kys);$i++)
		{
			$k=$kys[$i];
			$r=setcookie("$name[$k]",$zecookie[$k]);
			if(!$r)return false;
		}		
		return true;
	}
	
	
	
	function getLinkTitle($row)
	{		
		$cols=array("doc_titre","doc_soustitre");
		for($i=0;$i<count($cols);$i++)
		{				
if( strlen($row[$cols[$i]])>1)return $row[$cols[$i]];
		}
		return "Aucun titre";
	}
	
	function left($str,$nb)
	{
		return substr($str,0,$nb);
	}
	
	function right($str,$nb)
	{
		return substr($str, strlen($str)-$nb);
	}
	
	function mid($str,$offet,$len)
	{
		return substr($str,$offset,$len);
	}
	
	
	function convertDate2Long($date)
	{
		$date=split("-",$date);
		return mktime(0,0,0,$date[1],$date[2],$date[0]);
	}

	/*
 	Verifie que la date est conforme au format francais
 	*/
	function verifierDateFr($date)
	{
		$rgx="([0-3][0-9])(-|/)(0[1-9]|1[0-2])(-|/)(19|20[0-9]{2})";		
		return ereg($rgx,$date) && strlen($date)==10;
	}
	
	function iDate2FrDate($idate)
	{
		if(ereg("([0-9]{4})-([0-9]{2})-([0-9]{2})",$idate,$tmp))
		{
			return $tmp[3]."/".$tmp[2]."/".$tmp[1];
		}
		else return $idate;
	}
	
	function dateFr2IDate($date)
	{
		if(ereg("([0-9]+)/([0-9]+)/([0-9]+)",$idate,$tmp))
		{
			return $tmp[3]."-".$tmp[2]."-".$tmp[1];
		}
		else return $date;
	}

	function getYear($dateString)
	{
		if(strlen( trim($dateString) )==0)return "";
		$time=strtotime($dateString);		
		if($time==-1)return "";
		$year=strftime ("%Y", $time);		
		return $year;
	}	
	
	function getFromDate($pattern)
	{
		if($pattern=="*")return date("Y-m-d", mktime(0,0,0,1,1,1900) );
		ereg("([0-9]+)(.+)",$pattern,$rs);
		$n=$rs[1];
		$type=$rs[2];
		switch( $type )
		{
			case "j" : return date("Y-m-d", mktime(0,0,0,date("m"),date("d")-$n,date("Y")) );	
			case "m" : return date("Y-m-d", mktime(0,0,0,date("m")-$n,date("d"),date("Y")) );
			case "a" : return date("Y-m-d", mktime(0,0,0,date("m"),date("d"),date("Y")-$n) );			
		}		
	}
	
	function parseSinequaLocations($s)
	{
		if( strlen($s)==0)return array();
		$s=split(";",$s);
		$t=array();
		for($i=0;$i<count($s);$i++)
		{
			$tks=split(",",$s[$i]);
			array_push($t,new SinequaLocation($tks[0],$tks[1]));
		}
		return $t;
	}
	
	class SinequaLocation
	{
		var $offset;
		var $length;
		function SinequaLocation($offset,$length)
		{
			$this->offset=$offset;
			$this->length=$length;
		}
		
		public function getSubstring($s)
		{
			return substr($s,$this->offset,$this->length);
		}
	}
	
	interface Substitutor
	{
		public function substituteBy($src, SinequaLocation $loc);		
	}
	
	class HighlightSubstitutor implements Substitutor
	{
		public function substituteBy($src,SinequaLocation $loc)
		{			
			global $highlightStyle;
			return "<span style='$highlightStyle'>".$loc->getSubstring($src)."</span>";			
		}
	}	
	
	function applyLocations( $src, Substitutor $stor, $lcs)	
	{		
		if(count($lcs)==0)return $src;					
				for($i=0;$i<count($lcs);$i++)
				{								
					$rep=$stor->substituteBy($src, $lcs[$i]) ;								
					$px=$lcs[$i]->offset+$lcs[$i]->length;
					$src=substr($src,0,$lcs[$i]->offset).$rep.substr($src,$lcs[$i]->offset+$lcs[$i]->length);			
					$diff=strlen($rep)-$lcs[$i]->length;
					if($diff!=0)
					{					
						for($j=$i+1;$j<count($lcs);$j++)
						{
							$px2=$lcs[$j]->offset+$lcs[$j]->length;
							if($lcs[$j]->offset>=$px)$lcs[$j]->offset+=$diff;
							else if($px2>=$px)$lcs[$j]->length+=$diff;
						}
					}
				}
			
		return $src;
	}
	
	function serializeUser($user)
	{
		$_SESSION["user-name"]=$user->name;
		$_SESSION["user-psw"]=$user->psw;
		$_SESSION["user-right"]=$user->right;
	}
	
	function deserializeUser()
	{
		if(isset($_SESSION["authentification"]))
		{		
			return new UserSinequa($_SESSION["user-name"],$_SESSION["user-psw"],$_SESSION["user-right"]);
		}else return null;
	}
	
	function unregisterUser()
	{
		unset($_SESSION["user-name"]);
		unset($_SESSION["user-psw"]);
		unset($_SESSION["user-right"]);
		unset($_SESSION["authentification"]);
	}
	
	class UserDatabase
	{
		var $users;
		
		function UserDatabase($path)
		{
			$subject=file_get_contents($path);
			$xml=new SimpleXMLElement( $subject );
			$this->users=array();
			foreach($xml->user as $user)
			{
				$u=new UserSinequa( (string)$user->name,(string)$user->psw,(string)$user->right );				
				array_push($this->users,$u);
			}
		}
		
		function login($userName,$userPassword)
		{			
			for($i=0;$i<count($this->users);$i++)
			{
				if($this->users[$i]->name==$userName && $this->users[$i]->psw==$userPassword)
				{				
					return $this->users[$i];
				}
			}
			throw new Exception("Cette utilisateur n'est pas enregistré.");
		}
	}
	
	class UserSinequa
	{
		var $name;
		var $psw;
		var $right;
		var $rightTokens;
		
		function UserSinequa($name,$psw,$right)
		{
			$this->name=$name;
			$this->psw=$psw;
			$this->right=$right;
			$this->rightTokens=split("\\|",$right);
		}

		function editionRight()
		{
			return in_array("all",$this->rightTokens) || in_array("edition",$this->rightTokens);
		}
	}
	

	
	class RefineCollection
	{
		var $tokens;
		
		function RefineCollection($s)
		{
			$this->parse($s);			
		}
		
		function getTokensByType($type)
		{
			$result=array();
			for($i=0;$i<count($this->tokens);$i++)
			{
				if($this->tokens[$i]->type==$type)array_push($result,$this->tokens[$i]);
			}			
			return $result;
		}
		
		function getHumanInfo()
		{
			if( count($this->tokens)>0 )
			{				
				$r="";
				$table=array();
				$table["concepts"]="le concept ";
				$table["person"]="la personnalité ";
				$table["geo"]="le lieu ";
				$table["company"]="la société ";
				for($i=0;$i<count($this->tokens);$i++)
				{
					$r.=($i>0 ? ", " : "");					
					$r.=$table[$this->tokens[$i]->type]."<b>'".$this->tokens[$i]->content."'</b>";						
				}
				return "Vous avez affiné sur $r";
			}
			return "";
		}
		
		function parse($s)
		{
			$this->tokens=array();
			if(strlen($s)>0)
			{
				$tks=split(",",$s);
				for($i=0;$i<count($tks);$i++)
				{
					$tks2=split(":",$tks[$i]);
					$t=new RefineToken($tks2[0],$tks2[1]);					
					array_push($this->tokens,$t);					
				}
			}
		}
	}
	
	class RefineToken
	{
		var $type;
		var $content;
		
		function RefineToken($type,$content)
		{
			$this->type=$type;
			$this->content=$content;			
		}
	}
	
	class Historic
	{
		function Historic($keys)
		{
			
		}
	}
	
	function parseCsvMetas($s)
	{
		$s=split("\t",$s);
		$metas=array();
		if(strlen($s)==0)return $metas;
		for($i=0;$i<count($s);$i+=2)array_push($metas,$s[$i]);
		return $metas;
	}
	
	function metasTooltip($row)
	{
		global $correlations_array;
		$s="<br><table style='font-size:10'><tr>";
		for($i=0;$i<count($correlations_array);$i+=2)
		{
			$s.="<td align='left'><b><u>".$correlations_array[$i+1]."</u></b></td>";
		}		
		$s.="</tr><tr>";
		for($i=0;$i<count($correlations_array);$i+=2)
		{			
			$metas=join("<br>",parseCsvMetas($row[$correlations_array[$i]]));
			if(!$metas)$metas="&nbsp;";
			$s.="<td align='left' valign='top'>$metas</td>";
		}
		$s.="</tr>";	
		$s.="</table>";
		return $s;
	}
	
	function escapeTooltip($tooltip)
	{
		$tooltip=str_replace("'","\\'",$tooltip);
		$tooltip=str_replace("\"","&quot;",$tooltip);
		$tooltip=str_replace("\n","",$tooltip);
		return $tooltip;
	}
	
	function parseCorrelations($s)
	{
		$s=split(";",$s);
		$tmp=array();
		if(count($s)==0)return $tmp;
		for($i=0;$i<count($s);$i++)
		{
			$t=split("\t",$s[$i]);
			if ($t[2]) $count=$t[2]; else $count=$t[1]; //selon le mode (correl/distrib), le count n'est pas au même endroit
			//array_push($tmp,new Correlation($t[0]));
			//array_push($tmp,array("label"=>$t[0]));
			array_push($tmp,array('label'=>$t[0],'count'=>$count)); //libellé + occurences
		}
		return $tmp;
	}
	
	function parseConcepts($concepts)
	{
		
		if(empty($concepts))return array();
		$concepts=split("\\|",$concepts);
		for($i=0;$i<count($concepts);$i++)
		{
			$_tmp=split("#",$concepts[$i]);
			$_tmp2=split(":",$_tmp[1]);
			$concepts[$i]=array('label'=>$_tmp[0],'count'=>$_tmp2[1]);
		}
		return $concepts;
	}
	
	class Correlation
	{
		var $label;
		function Correlation($s)
		{
			$s=split("\t",$s);
			$this->label=$s[0];
		}
	}
	
	class ClientSocket
	{
		var $serverPort;
		var $socket;
		var $reader;
		function ClientSocket($serverPort)
		{
			$this->serverPort=$serverPort;
		}
		
		function connect()
		{			
			$this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			$r=socket_connect($this->socket, '127.0.0.1', $this->serverPort);
			return $r;
		}
		
		function sendMessage($type,$message)
		{
			$message="$message\n";
			$type="$type\n";
			socket_write($this->socket,$type,strlen($type));
			socket_write($this->socket,$message,strlen($message));	
			$r=socket_read($this->socket,10000,PHP_NORMAL_READ);
			return $r;
		}
		
		function exec($cmd,$workDirectory)
		{
			$r=$this->sendMessage("cmd","[wd=$workDirectory]$cmd");
			return $r;
		}
		
		function shutdownServer()
		{
			$this->sendMessage("shutdown","");
		}
		
		function close()
		{
			socket_close($this->socket);
		}
	}
	
		
	class Article
	{			
		var $dom;
		var $id;
		var $page;
		var $taille;
		var $nb_image;
		var $publication;
		var $rubrique;
		var $modifie;
		var $date_parution;
		var $nom_jour;
		var $edition;
		var $chapo;
		var $tetiere;
		var $surtitre;
		var $titre;
		var $soustitre;		
		var $texte;
		var $signataire;
		var $path;
		
		function loadFile($path)
		{
				$this->path=$path;				
				$this->rawContent=file2String($path);
				$this->dom=new DomDocument("1.0","ISO-8859-15");
				$this->dom->load($path);				
				$doc=$this->dom->documentElement;					
				$this->id=$doc->getAttribute("id");
				$this->page=$doc->getAttribute("page");
				$this->taille=$doc->getAttribute("taille");
				$this->nb_image=$doc->getAttribute("nb_image");
				$this->publication=$doc->getAttribute("publication");
				$this->rubrique=$doc->getAttribute("rubrique");
				$this->modifie=$doc->getAttribute("modifie");
				$this->date_parution=$doc->getAttribute("date_parution");
				$this->nom_jour=$doc->getAttribute("nom_jour");
				$this->edition=$doc->getAttribute("edition");			
				$this->titre=$this->getText("titre","");
				$this->chapo=$this->getText("chapo","");
				$this->tetiere=$this->getText("tetiere","");
				$this->surtitre=$this->getText("surtitre","");
				$this->soustitre=$this->getText("soustitre","");
				$this->texte=$this->getText("texte","");
				$this->signataire=$this->getText("signataire","");
		}
		
		function saveAs($path)
		{
			$content="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
			$content.=$this->getStartElement("article",0,array(
			"id",$this->id,
			"page",$this->page,
			"taille",$this->taille,
			"nb_image",$this->nb_image,
			"publication",$this->publication,
			"rubrique",$this->rubrique,
			"modifie",$this->modifie,
			"date_parution",$this->date_parution,
			"nom_jour",$this->nom_jour,
			"edition",$this->edition			
			),null,false);
			$content.="\n";
			
			$content.=$this->getStartElement("titre",1,null,$this->titre,true);
			$content.=$this->getStartElement("chapo",1,null,$this->chapo,true);
			$content.=$this->getStartElement("tetiere",1,null,$this->tetiere,true);
			$content.=$this->getStartElement("surtitre",1,null,$this->surtitre,true);
			$content.=$this->getStartElement("soustitre",1,null,$this->soustitre,true);
			$content.=$this->getStartElement("soustitre",1,null,$this->soustitre,true);
			$content.=$this->getStartElement("texte",1,null,$this->texte,true);	
			$content.=$this->getStartElement("signataire",1,null,$this->signataire,true);						
			
			$content.=$this->getEndElement("article");
			string2File($content,$path);
			
		}
		
		function getIndent($n,$ch)
		{
			$s="";
			for($i=0;$i<$n;$i++)
			{
				$s.=$ch;
			}
			return $s;			
		}
		
		function getStartElement($eName,$n,$attrs,$text,$withText)
		{
			$s=$this->getIndent($n,"\t");
			$s.="<$eName";
			if( $attrs )
			{
				for($i=0;$i<count($attrs);$i+=2)
				{
					$s.=" ".$attrs[$i]."=\"".normalizeXmlAttr($attrs[$i+1])."\"";
				}
			}
			$s.=">";
			if($withText)
			{				
				if(!$text)
				{					
					return "";
				}
				$s.=normalizeXmlText($text);
				$s.=$this->getEndElement($eName)."\n";
			}			
			return $s;
		}
		
		function getEndElement($eName)
		{
			return "</$eName>";
		}
		
		function save()
		{
			return $this->saveAs($this->path);
		}

		function getText($tagName,$def)
		{
			$grp=array();
			$r=ereg("<$tagName>(.+)</$tagName>",$this->rawContent,$grp);
			if($r && count($grp)>1)
			{
				return $grp[1];
			}
			return $def;
		}
		
	}		
	
	function file2String($path)
	{
		$res=fopen($path,"r");		
		$content=stream_get_contents($res);
		fclose($res);
		$content=utf8_encode($content);
		return $content;
	}
	
	function string2File($s,$path)
	{
		$s=utf8_decode($s);
		file_put_contents($path,$s);		
	}
	
	function normalizeXmlText($s)
	{
		$s=str_replace("&","&amp;",$s);
		$s=str_replace(" < "," &gt; ",$s);
		$s=str_replace(" > "," &lt;",$s);
		return $s;
	}
	
	function normalizeXmlAttr($s)
	{		
		$s=str_replace("&","&amp;",$s);
		$s=str_replace("<","&gt;",$s);
		$s=str_replace(">","&lt;",$s);
		$s=str_replace("'","&apos;",$s);
		$s=str_replace("\"","&quot;",$s);
		return $s;
	}
	
	class CommandBuilder
	{
		var $zeTable=array();

		function countKeys()
		{
			return count($this->zeTable);
		}

		function addCommand($id,$cmd)
		{
			$check=isset($this->zeTable[$id]) ? $this->zeTable[$id] : null;
			$cmdObj=new Command($cmd);
			if(isset($check))
			{
				$check[count($check)]=$cmdObj;				
			}
			else
			{				
				$check=array($cmdObj);								
			}
			$this->zeTable[$id]=$check;
			return $cmdObj;
		}

		
		function setCommand($id,$cmd)
		{			
			$cmdObj=new Command($cmd);						
			$this->zeTable[$id]=array($cmdObj);
			return $cmdObj;
		}
		
		function setFormCommand($name,$type,$value)
		{
			$this->setCommand($name,"<input type='$type' name='$name' value='".$this->normalizeHtml($value)."'>");
		}
		
		function normalizeHtml($value)
		{
			$value=str_replace("\\","",$value);
			$value=str_replace("\"","&quot;",$value);
			$value=str_replace("'","&#39;",$value);
			return $value;
		}		
		

		function setURLCommand($key,$value)
		{
			$cmdTmp=$this->zeTable[$key];
			$f="";
			if($cmdTmp)
			{
				$cmd=$cmdTmp[count($cmdTmp)-1];
				if(strlen($cmd->cmd)>0 && substr($cmd->cmd,0,1)=="&")$f="&";
			}
			
			$this->zeTable[$key]=array(new Command($f.$key."=".urlencode($value)));
		}
		
		function removeKey($id)
		{
			$tk=split(",",$id);
			for($i=0;$i<count($tk);$i++)
			{
				unset($this->zeTable[$tk[$i]]);
			}
		}

		function getFormInputString($key,$cmd)
		{
			$s="<input type=\"hidden\" name=\"".$key."\"".( ($cmd!=Null && strlen($cmd)>0) ? " value=\"".$this->normalizeHtml($cmd)."\"" : "").">\n";
			return $s;
		}

		function cloneBuilder()
		{
			$cb=new CommandBuilder();			
			$keys=array_keys($this->zeTable);
			for($i=0;$i<count($keys);$i++)
			{
				$cb->zeTable[$keys[$i]]=$this->zeTable[$keys[$i]];
			}
			return $cb;			
		}		

		function toURL($page)
		{
			$keys=array_keys($this->zeTable);			
			$s=$page.(count($keys)>0 ? "?" : "");
			for($i=0;$i<count($keys);$i++)
			{
				$values=$this->zeTable[$keys[$i]];
				for($j=0;$j<count($values);$j++)
				{
					$s.=urldecode(ereg_replace("(\n|\r)","",$values[$j]->cmd));					
				}
			}
			return $s;
		}

		function toForm()
		{
			$keys=array_keys($this->zeTable);			
			$s="";
			for($i=0;$i<count($keys);$i++)
			{
				$values=$this->zeTable[$keys[$i]];
				for($j=0;$j<count($values);$j++)
				{
					$s.=$values[$j]->cmd."\n";
				}
			}
			return $s;
		}
	}
	
	function createURLCommandBuilder()
	{
		$cb=new CommandBuilder();
		$tableaux=array($_POST,$_GET);
		for($i=0;$i<count($tableaux);$i++)
		{
			$keys=array_keys($tableaux[$i]);
			for($j=0;$j<count($keys);$j++)
			{
				$values=$tableaux[$i][$keys[$j]];
				if(is_array($values))
				{
					for($k=0;$k<count($values);$k++)
					{						
						$cb->addCommand( $keys[$j],($cb->countKeys()>0 ? "&" : "").$keys[$j]."[]=".$cb->normalizeHtml($values[$k]));
					}
				}
				else
				{						
					$cb->addCommand( $keys[$j], ($cb->countKeys()>0 ? "&" : "").$keys[$j]."=".$cb->normalizeHtml($values) );			
				}
			}
		}		
		return $cb;
	}

	function createFORMCommandBuilder()
	{
		$cb=new CommandBuilder();
		$tableaux=array($_POST,$_GET);
		for($i=0;$i<count($tableaux);$i++)
		{
			$keys=array_keys($tableaux[$i]);
			for($j=0;$j<count($keys);$j++)
			{
				$values=$tableaux[$i][$keys[$j]];
				if(is_array($values))
				{
					for($k=0;$k<count($values);$k++)
					{
						$cb->addCommand( $keys[$j],$cb->getFormInputString($keys[$j]."[]",$values[$k]));
					}
				}
				else
				{
					$cb->addCommand( $keys[$j],$cb->getFormInputString($keys[$j],$values));
				}
			}
		}
		return $cb;
	}
	
	
	class Command
	{
		var $cmd="";

		function Command($_cmd)
		{
			$this->cmd=$_cmd;
		}
	}
	
	
		// General
		
		function echoForm($page,$id,$params)
		{
			$cmd=new CommandBuilder();
			for($i=0;$i<count($params);$i+=2)
			{
				$key=$params[$i];
				$val=$params[$i+1];
				$cmd->setFormCommand($key,"hidden",$val);
			}
			echo "<form action=\"$page\" id=\"$id\" method=\"POST\">\n";
			echo $cmd->toForm();
			echo "</form>";
		}
		
		function echoMainForm($page,$id,$params)
		{
			$cmd=new CommandBuilder();
			$cmd->toForm();
			for($i=0;$i<count($params);$i+=2)
			{
				$key=$params[$i];
				$val=$params[$i+1];
				$cmd->setFormCommand($key,"hidden",$val);
			}
			echo "<span style=\"display:none\">";
			echo "<form action=\$page\" id=\"$id\" method=\"POST\">\n";
			echo $cmd->toForm();
			echo "</form>";
			echo "</span>";
		}
?>