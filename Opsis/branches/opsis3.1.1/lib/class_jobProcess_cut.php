<?php

require_once('class_jobProcess.php');
require_once('class_matInfo.php');
require_once('interface_Process.php');

class JobProcess_cut extends JobProcess implements Process
{
	private $nb_frames;
	private $nb_frames_encodees;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('cut');
		
		$this->required_xml_params=array
		(
		);
		
		$this->optional_xml_params=array
		(
			'tcin'=>'timecode',
			'tcout'=>'timecode',
            'offset'=>'timecode',
			'preroll'=>'timecode',
			'postroll'=>'timecode',
			'framerate'=>'int',
			'use_link'=>'int',
            'use_ffmpeg'=>'int'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		// si pas de tcin et tcout copie du fichier
		if ((!isset($params_job['tcin']) || empty($params_job['tcin'])) || (!isset($params_job['tcout']) || empty($params_job['tcout']) ) || (tcToSecDec($params_job['tcin']) == 0 && tcToSecDec($params_job['tcout']) == 0 ))
		{
			// VP 16/02/2018 : inhibe IPTC si fichier de sortie n'est pas JPG
			if(isset($params_job['iptc_write']) && getExtension($this->getFileOutPath())!="jpg" && getExtension($this->getFileOutPath())!="jpeg"){
				unset($params_job['iptc_write']);
			}
			if(isset($params_job['logo'])){
				require_once(modelDir.'model_job.php');
				require_once('class_jobProcess_ffmbc.php');
				
				$job_sync = new Job() ;
				$job_sync->flag_runSyncJob = true;
				$params_job['vcodec']="copy";
				$params_job['acodec']="copy";
				$job_sync->full_xml_job_process = '<data><in>'.$this->getFileInPath().'</in><out>'.$this->getFileOutPath().'</out>'.tab2xml($params_job,'param').'</data>';
				$sync_jobProcess = JobProcess::getJobprocess('ffmbc',$job_sync,null,true);
				
				try{
					$sync_jobProcess->prepareProcess();
					$sync_jobProcess->doProcess();
					$sync_jobProcess->finishJob();
				}catch (Exception $e){
					throw new Exception('('.$e->getCode().') '.$e->getMessage());
				}
				
			}elseif (isset($params_job['use_link']) && !empty($params_job['use_link']) && $params_job['use_link']=='1' && !isset($params_job['iptc_write']))
			{
				// workaround hard_link sur un répertoire
				if(is_dir($this->getFileInPath())){
					// creation repertoire d'arrivee principal
					if(!is_dir($this->getFileOutPath())){
						mkdir($this->getFileOutPath());
					}
					// iteration recursive sur les fichiers  
					$it = new RecursiveDirectoryIterator($this->getFileInPath());
					foreach(new RecursiveIteratorIterator($it) as $file)
					{
						if(is_file($file)){
							// création des subdirs necessaires
							if(!is_dir(dirname($this->getFileOutPath()."/".substr($file,strlen($this->getFileInPath()))))){
								mkdir(dirname($this->getFileOutPath()."/".substr($file,strlen($this->getFileInPath()))),0755,true);
							}
							// si le fichier existe deja dans le répertoire d'arrivée, on le supprime
							if($this->getFileInPath()!=$this->getFileOutPath() && is_file($this->getFileOutPath()."/".$it."/".basename($file))){unlink($this->getFileOutPath()."/".substr($file,strlen($this->getFileInPath())));}
							// hard_link unitaire des fichiers contenus dans le dossier 
							link($file,$this->getFileOutPath()."/".substr($file,strlen($this->getFileInPath())));
							// $this->writeJobLog("link : ".$file." -> ".$this->getFileOutPath()."/".substr($file,strlen($this->getFileInPath())));
						}	
					}
				}else {
					if(is_file($this->getFileOutPath()) && $this->getFileInPath()!=$this->getFileOutPath())
						unlink($this->getFileOutPath());
					if (link($this->getFileInPath(),$this->getFileOutPath())===false){
						// VP 6/04/18 : copy si échec link
						if (copy($this->getFileInPath(),$this->getFileOutPath())===false)
							throw new FileNotFoundException('Error copying file ('.$this->getFileInPath().' -> '.$this->getFileOutPath().')');
						else
							JobProcess::writeJobLog("php : copy('".$this->getFileInPath()."','".$this->getFileOutPath()."');");
					}
					else
						JobProcess::writeJobLog("php : link('".$this->getFileInPath()."','".$this->getFileOutPath()."');");
				}
			}
			else
			{
				if (copy($this->getFileInPath(),$this->getFileOutPath())===false)
					throw new FileNotFoundException('Error copying file ('.$this->getFileInPath().' -> '.$this->getFileOutPath().')');
			}
		}
		elseif (isset($params_job['is_dpx']) && $params_job['is_dpx'] == "1") {
			if (!isset($params_job['framerate']) || empty($params_job['framerate']) || floatval($params_job['framerate']) == 0) throw new InvalidXmlParamException('framerate is missing');
			$this->setProgression(1);
			if (getExtension($this->getFileInPath()) != "tar")
				throw new FileNotFoundException('This file is not a DPX.');
			// check existence <source_tar_name>_extract/ dans <nas>/videos/TAR/ => quick way 
			$extractDir_mutual =  str_replace(".tar", "_extract/", $this->getFileInPath());
			$behavior = null;
			if(is_dir($extractDir_mutual)){
				$behavior = 1;
				$extractDir = $extractDir_mutual;
				$extractDir_out = str_replace(".tar", "_extract/", $this->getFileOutPath());
				if(!is_dir($extractDir_out)){
					mkdir($extractDir_out);
				}
			}else{
				$behavior = 2;
				try {
					// $phar = new PharData($this->getFileInPath());
					$extractDir = str_replace(".tar", "_extract/", $this->getFileOutPath());
					if (!is_dir($extractDir)){
						mkdir($extractDir);
						$this->shellExecute(kBinTar,'xf "'.$this->getFileInPath().'" -C "'.$extractDir.'"');					
					}
                    // Sauvegarde extract si pas déjà fait
                    if(!is_dir($extractDir_mutual)){
                        rename($extractDir,$extractDir_mutual);
                        // On se ramène au cas précédent
                        $behavior = 1;
                        $extractDir = $extractDir_mutual;
                        $extractDir_out = str_replace(".tar", "_extract/", $this->getFileOutPath());
                        if(!is_dir($extractDir_out)){
                            mkdir($extractDir_out);
                        }
                        
                    }
				} catch (Exception $e) {
					throw new FileNotFoundException('Can\'t open the archive : '.$e->message);
				}
			}
            $this->WriteOutLog("cut_dpx - behavior ".$behavior." - extractDir : ".$extractDir." extractDirOut ".$extractDir_out);
			
			
			$nb_fps = floatval($params_job['framerate']);
			$timein = tcToSecDec($params_job['tcin'], $nb_fps);
			$timeout = tcToSecDec($params_job['tcout'], $nb_fps);
			if(!empty($params_job['offset'])){
                $offset_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-tcToSecDec($params_job['offset'],$nb_fps);
            }else{
                $offset_in_sec=tcToSecDec($params_job['tcin'],$nb_fps);
            }
			if(!empty($params_job['preroll'])){
				$offset_in_sec=$offset_in_sec-tcToSecDec($params_job['preroll'],$nb_fps);
				if ($offset_in_sec < 0) $offset_in_sec = 0;
			}
			if(!empty($params_job['postroll']))
				$params_job['tcout'] = secDecToTc(tcToSecDec($params_job['postroll'],$nb_fps)+tcToSecDec($params_job['tcout'],$nb_fps));
			
			$this->setProgression(2);
			try {
				if ($handle = opendir($extractDir)) {
					$wavPath = "";
					while (false !== ($entry = readdir($handle)))
						if($behavior ==1 ){
							// nouveau fonctionnement, extraction mutualisée,copie du necessaire
							if (strpos($entry, "DPX") === 0 && is_dir($extractDir.$entry)) {
								$imgDpxDir = $extractDir.$entry."/";
								if ($handleImg = opendir($imgDpxDir)) {
									while (false !== ($img = readdir($handleImg))){
										if ($img == "." || $img == "..") continue;
										$ListFiles[]=$img;
									}
									closedir($handleImg);
									sort($ListFiles);
									$this->setProgression(10);
									if(!is_dir($extractDir_out.$entry)){
										mkdir($extractDir_out.$entry);
									}
									$i = 0;
									foreach ($ListFiles as $img) {
										if ($i >= $offset_in_sec * $nb_fps && $i <= $timeout * $nb_fps){
											$ok = link($imgDpxDir.$img,$extractDir_out.$entry."/".$img);
											if(!$ok) {
												copy($imgDpxDir.$img,$extractDir_out.$entry."/".$img);
												$this->WriteOutLog("cut_dpx - behavior 1 - copy ".$imgDpxDir.$img." to ".$extractDir_out.$entry."/".$img);
											} else {
												$this->WriteOutLog("cut_dpx - behavior 1 - hard link ".$imgDpxDir.$img." to ".$extractDir_out.$entry."/".$img);
											}
										}
										$i++;
									}
								}
							}
							elseif (strpos($entry, "WAV") === 0 && is_dir($extractDir.$entry)) {
								$wavDpxDir = $extractDir.$entry."/";
								if ($handleWav = opendir($wavDpxDir)) {
									while (false !== ($wav = readdir($handleWav))){
										if ($wav == "." || $wav == "..") continue;
										if (strpos($wav, ".wav") > 0) {
											$wavPath = $wavDpxDir.$wav;
											if(!is_dir($extractDir_out.$entry)){
												mkdir($extractDir_out.$entry);
											}
											
											$wavOutPath = $extractDir_out.$entry."/".$wav;
											// $tmpWavPath = $wavDpxDir."tmp.wav";
											// rename($wavPath, $tmpWavPath);
											
											$tc_in_sec=$offset_in_sec;
											$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps;
											$options_ffmbc='-y -ss '.$tc_in_sec.' -t '.$duration.' -i '.escapeQuoteShell($wavPath).' -acodec copy '.escapeQuoteShell($wavOutPath);
											$this->WriteOutLog("cut_dpx - behavior 1 - copy wav ");
											$this->shellExecute(kFFMBCpath,$options_ffmbc,false);
										}
									}
									closedir($handleWav);
									$this->setProgression(20);
								}
							}

						}else if($behavior == 2){
							// ancien fonctionnement, extraction puis suppression du superflu
							if (strpos($entry, "DPX") === 0 && is_dir($extractDir.$entry)) {
								$this->WriteOutLog("cut_dpx - behavior 2 - suppr DPX superflus ");
								$imgDpxDir = $extractDir.$entry."/";
								if ($handleImg = opendir($imgDpxDir)) {
									while (false !== ($img = readdir($handleImg))){
										if ($img == "." || $img == "..") continue;
										$ListFiles[]=$img;
									}
									closedir($handleImg);
									sort($ListFiles);
									$this->setProgression(10);
									$i = 0;
									foreach ($ListFiles as $img) {
										if ($i < $offset_in_sec * $nb_fps || $i > $timeout * $nb_fps) unlink($imgDpxDir.$img);
										$i++;
									}
								}
							}
							elseif (strpos($entry, "WAV") === 0 && is_dir($extractDir.$entry)) {
								$wavDpxDir = $extractDir.$entry."/";
								$this->WriteOutLog("cut_dpx - behavior 2 - transcod wav ");
								if ($handleWav = opendir($wavDpxDir)) {
									while (false !== ($wav = readdir($handleWav))){
										if ($wav == "." || $wav == "..") continue;
										if (strpos($wav, ".wav") > 0) {
											$wavPath = $wavDpxDir.$wav;
											$tmpWavPath = $wavDpxDir."tmp.wav";
											rename($wavPath, $tmpWavPath);
											
											$tc_in_sec=$offset_in_sec;
											$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps;
											$options_ffmbc='-y -ss '.$tc_in_sec.' -t '.$duration.' -i '.escapeQuoteShell($tmpWavPath).' -acodec copy "'.$wavPath.'"';
											$this->shellExecute(kFFMBCpath,$options_ffmbc,false);
											unlink($tmpWavPath);
										}
									}
									closedir($handleWav);
									$this->setProgression(20);
								}
							}
						}
					closedir($handle);
					$this->setProgression(30);
				}
			} catch (Exception $e) {
				throw new FileNotFoundException($e->message);
			}
			
			//$phar = new PharData($this->getFileOutPath());
			//$phar->buildFromDirectory($extractDir);
			if($behavior == 2 ){
				$this->shellExecute(kBinTar,'cf "'.$this->getFileOutPath().'" -C "'.$extractDir.'" .');
				exec("rm -rf $extractDir");
				$this->WriteOutLog("cut_dpx - behavior 2 - tar final: ".$this->getFileOutPath().", rm extractDir ".$extractDir);
			}else{
				$this->shellExecute(kBinTar,'cf "'.$this->getFileOutPath().'" -C "'.$extractDir_out.'" .');
				exec("rm -rf $extractDir_out");
				$this->WriteOutLog("cut_dpx - behavior 1 - tar final: ".$this->getFileOutPath().", rm extractDirOut ".$extractDir_out);
			}
			$this->setProgression(100);
		}
		else
		{
			$infos=MatInfo::getMatInfo($this->getFileInPath());
			//var_dump($infos);
			foreach ($infos['tracks'] as $track)
			{
				if (strtolower($track['type'])=='video')
				{
					//Présence parfois d'une piste aux informations erronées, de format motion jpeg. 
					//On évite de prendre ses infos si on en a d'autres, donc.
					if(!empty($video_format) && strtolower($track['format']) == 'mjpeg')
						continue;
					$video_format=$track['format'];
					$codec_id=$track['codec_id'];
					$version_format=$track['format_version'];
					$gop_info=$track['format_settings_gop'];
					$video_frame_rate=$track['frame_rate'];
					$video_scan_type=$track['scan_type'];
                    $nb_fps=$track['frame_rate'];
				}
				elseif (strtolower($track['type'])=='audio')
				{
					$audio_format=$track['format'];
				}
			}
			//decoupage avec ffmbc
			
			// test des time code
			/*if (tcToSecDec($params_job['tcin'],$nb_fps)<tcToSecDec($infos['tcin'],$nb_fps) || tcToSecDec($params_job['tcin'],$nb_fps)>tcToSecDec($infos['tcout'],$nb_fps))
				throw new InvalidXmlParamException('invalid tcin ('.$params_job['tcin'].')');
			
			if (tcToSecDec($params_job['tcout'],$nb_fps)<tcToSecDec($infos['tcin'],$nb_fps) || tcToSecDec($params_job['tcout'],$nb_fps)>tcToSecDec($infos['tcout'],$nb_fps))
				throw new InvalidXmlParamException('invalid tcout ('.$params_job['tcout'].')');*/
			
			if (tcToSecDec($params_job['tcin'],$nb_fps)>tcToSecDec($params_job['tcout'],$nb_fps))
				throw new InvalidXmlParamException('tcin > tcout ('.$params_job['tcin'].' > '.$params_job['tcout'].')');

            if (!empty($params_job['offset']) && tcToSecDec($params_job['offset'],$nb_fps)>tcToSecDec($params_job['tcin'],$nb_fps))
				throw new InvalidXmlParamException('offset > tcin ('.$params_job['offset'].' > '.$params_job['tcin'].')');

			
			if (isset($gop_info) && !empty($gop_info))
				$gop=$this->getGopSize($gop_info);
			else
			{
				if ($video_format=='VC-3' || $video_format=='ProRes' || $video_format=='DV')
					$gop=1;
				else if ($video_format=='Cinepack' || $video_format=='VP8')
					$gop=$this->getGopSize($gop_info);
				else if ($video_format=='VC-1' || strpos($infos['format_calc'],'MPEG2')!==false || $video_format=='AVC')
				{
					// tester presence de N ('M=3, N=12')
					if (!empty($gop_info) && strpos($gop_info,'N=')!=false)
						$gop=$this->getGopSize($gop_info);
					else
						$gop=0; // GOP != 1
						//$gop=1;
				}
				else
					$gop=1;
			}

            $use_ffmpeg=false;
            // VP 12/05/2014 : possibilité de forcer l'utilisation de ffmpeg
            if (isset($params_job['use_ffmpeg']) && !empty($params_job['use_ffmpeg']) && $params_job['use_ffmpeg']=='1') $use_ffmpeg=true;
            
			$use_partial_restore=false;
            
            // Calcul offset IN
            if(!empty($params_job['offset'])){
                $offset_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-tcToSecDec($params_job['offset'],$nb_fps);
            }else{
                $offset_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-tcToSecDec($infos['timecode'],$nb_fps);
            }
			
			$offset_in_sec=round($offset_in_sec,2);
			
			if(!empty($params_job['preroll'])){
				$offset_in_sec=$offset_in_sec-tcToSecDec($params_job['preroll'],$nb_fps);
				if ($offset_in_sec < 0) $offset_in_sec = 0;
			}
			if(!empty($params_job['postroll']))
				$params_job['tcout'] = secDecToTc(tcToSecDec($params_job['postroll'],$nb_fps)+tcToSecDec($params_job['tcout'],$nb_fps));
            
            $this->writeJobLog('gop='.$gop.' tcin='.tcToSecDec($params_job['tcin'],$nb_fps).' offset='.tcToSecDec($infos['offset'],$nb_fps).' offset_in_sec='.$offset_in_sec);
            
			$format_codec_id=MatInfo::getFormatByCodecId($codec_id);

			if (strstr($format_codec_id[0],'XDCAM')!=false && $gop!=1 && $infos['format_profile']=='QuickTime')
			{
				$use_ffmpeg=true;
				$tc_in_sec=$offset_in_sec-$gop/$nb_fps;
				$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps+(2*$gop)/$nb_fps; // OUT - IN + 1 image + 2* gop
				
				if (isset($params_job['timecode']) && !empty($params_job['timecode']))
					$timecode=secDecToTc(tcToSecDec($params_job['timecode'],$nb_fps)+$offset_in_sec-$gop/$nb_fps,$nb_fps);
				else
					$timecode=secDecToTc(tcToSecDec($params_job['tcin'],$nb_fps)-$gop/$nb_fps,$nb_fps);
				
				// ajout des options -map
				$option_map='';
				foreach ($infos['tracks'] as $idx=>$track)
				{
					if ($track['codec_id']!='tmcd')
						$option_map.='-map 0:'.$track['index'].' ';
				}
				
				
				$options_ffmbc='-ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f mov -vcodec copy -acodec copy -scodec copy '.$option_map.' -t '.$duration.' -timecode '.$timecode.' -y "'.$this->getFileOutPath().'"';
			}
            else if ($infos['format']=='MPEG-4' || $infos['format']=='QuickTime') //si (fichier mpeg4 ou mov ou avi)
            {
                // calcul des bornes et duree
                if ($gop==1)
                {
                    //$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-tcToSecDec($infos['timecode'],$nb_fps);
                    $tc_in_sec=$offset_in_sec;
                    $duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps; // OUT - IN + 1 image
                }
                else
                {
                    //$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-1/$nb_fps;
                    $tc_in_sec=$offset_in_sec-1/$nb_fps;
                    if($tc_in_sec<0) $tc_in_sec=$offset_in_sec;
                    $duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+3/$nb_fps; // OUT - IN + 3 images
                }
                
                $this->nb_frames=floor($duration*$nb_fps);
                
                $options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' ';
                
                //format
                if ($infos['type_mime']=='video/mp4')
                    $options_ffmbc.='-f mp4 ';
                else if ($infos['type_mime']=='video/quicktime')
                    $options_ffmbc.='-f mov ';
                
                $options_ffmbc.='-vcodec copy -acodec copy -t '.$duration;
                
                if($infos['type_mime']=='video/quicktime')// fichier mov avec timecode
                {
                    if ($gop==1)
                        $options_ffmbc.=' -timecode '.$params_job['tcin'];
                    else{
                        $timecodeSec=tcToSecDec($params_job['timecode'],$nb_fps)+$tc_in_sec-2/$nb_fps;
                        if ($timecodeSec>=0)
                            $timecode=secDecToTc($timecodeSec,$nb_fps);
                        else
                            $timecode="00:00:00:00";
                       
                        $options_ffmbc.=' -timecode '.$timecode;
                        //$options_ffmbc.=' -timecode '.secDecToTc(tcToSecDec($params_job['tcin'],$nb_fps)-2/$nb_fps,$nb_fps);
                    }
                }
                
                $options_ffmbc.=' "'.$this->getFileOutPath().'"';
            }
            else if ($infos['format']=='AVI')
            {
                echo 'on a un fichier .avi'."\n";
                // calcul des bornes et duree
                if ($gop==1)
                {
                    $tc_in_son=1/$nb_fps;
                    //$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-tcToSecDec($infos['timecode'],$nb_fps);
                    $tc_in_sec=$offset_in_sec-1/$nb_fps;
					
					if($tc_in_sec<0)
						$tc_in_sec=$offset_in_sec;
					
                    $duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps); // OUT - IN 
                }
                else
                {
                    $tc_in_son=1/$nb_fps;
                    //$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-1/$nb_fps;
                    $tc_in_sec=$offset_in_sec-1/$nb_fps;
					
					if($tc_in_sec<0)
						$tc_in_sec=$offset_in_sec;
					
					$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+3/$nb_fps; // OUT - IN + 3 images
				}
				
				$this->nb_frames=floor($duration*$nb_fps);
				
				$options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f avi ';
			
//				$this->shellExecute(kFFMBCpath,'-i '.escapeQuoteShell($this->getFileInPath()));
//				$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
//				$video_format
//				if (strstr($donnees_stderr,'Video: dvvideo') && strstr($donnees_stderr,'Audio: pcm_s16be'))// on regarde si on a une DVVideo avec l'audio multiplexe
                if ($video_format=='DV' && $audio_format=='PCM')// on regarde si on a une DVVideo avec l'audio multiplexe
				{
					if ($gop==1)
						$options_ffmbc.='-vcodec copy -acodec pcm_s16le -ss '.$tc_in_son.'';
					else
						$options_ffmbc.='-vcodec copy -acodec pcm_s16le';
				}
				else
				{
					if ($gop==1)
						$options_ffmbc.='-vcodec copy -acodec copy -ss '.$tc_in_son.'';
					else
						$options_ffmbc.='-vcodec copy -acodec copy ';
				}
				$options_ffmbc.=' -t '.$duration.' "'.$this->getFileOutPath().'"';
			}
			else if ($infos['format']=='MXF' && $video_format!='JPEG 2000')
			{
				//$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps);
                $tc_in_sec=$offset_in_sec;
				$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps; // OUT - IN + 1 image
				$timecode=secDecToTc(tcToSecDec($infos['timecode'],$nb_fps)+$tc_in_sec,$nb_fps);
				$this->nb_frames=floor($duration*$nb_fps);
				
				if(strpos($infos['format_calc'],'H264')!==false || strpos($infos['format_calc'],'DNxHD')!==false) //DNxHD(?) ou H264
				{
					$options_ffmbc='-y -i '.escapeQuoteShell($this->getFileInPath()).' -f mxf -vcodec copy -acodec copy -timecode '.$timecode.' -ss '.$tc_in_sec.' -t '.$duration.' "'.$this->getFileOutPath().'"';
				}
				else if (strpos($infos['format_calc'],'XDCAM HD422')!==false) //ffmpeg simple PROBLEME : une passe pour un fichier avec GOP ?
				{
					$options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f mxf -vcodec mpeg2video -target xdcamhd422 -vtag xd5c -b 50000k -minrate 50000k -maxrate 50000k -bufsize 25000k -g 12 -bf 2 -sc_threshold 40 -pix_fmt yuv422p -aspect 16:9 -r 25 -tff -acodec pcm_s24le -sample_fmt s32 -ar 48000 -ac 1 -timecode '.$timecode.' -t '.$duration.' "'.$this->getFileOutPath().'"';
				}
				else if ($gop==1) //ffmpeg simple PROBLEME : une passe pour un fichier avec GOP ?
				{
					$use_ffmpeg=true;
					
					$options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f mxf -vcodec copy -acodec copy -map 0 -timecode '.$timecode.' -t '.$duration.' "'.$this->getFileOutPath().'"';
				}
				else // ffmpeg en deux passes
				{
					$use_ffmpeg=true;
					$pre_roll=0;
					
					// 1ere passe
					do
					{
						$this->shellExecute(kFFMPEGpath,'-ss '.($tc_in_sec-($pre_roll/$nb_fps)).' -i '.escapeQuoteShell($this->getFileInPath()).' -f mxf -vcodec mpeg2video -vf "showinfo" -t 1 -y "'.$this->getTmpDirPath().'/tmp.mxf"');
						
						if (!file_exists($this->getTmpDirPath().'/tmp.mxf'))
							throw new FileException('can\'t create tmp file ('.$this->getTmpDirPath().'/tmp.mxf)');
						
						$infos_mxf=file_get_contents($this->getTmpDirPath().'/stderr.txt');
						
						//[Parsed_showinfo_0 @ 0x1cc3c20] n:0 pts:-2 pts_time:-0.08 pos:111885312 fmt:yuv422p sar:1/1 s:1920x1080 i:T iskey:1 type:I checksum:8301C4A5 plane_checksum:[BBCC9EE1 F70D1696 106D0F2E]
						
						preg_match('/\[Parsed_showinfo_0 @ 0x[0-9a-f]{7}\] n:[0-9]+ pts:(.*) pts_time:(.*)/',$infos_mxf,$matches);
						$pts=intval($matches[1]);
						
						$refaire_passe=false;
						
						$this->writeJobLog('PreRoll='.$pre_roll);
						$this->writeJobLog('pts='.$pts);
						
						if ($pre_roll-$pts>=0)
							$pre_roll=$pre_roll-$pts;
						else
						{
							$refaire_passe=true;
							$pre_roll++;
						}
                        // VP 8/06/2015 : vérification que -ss est >0
                        if(($tc_in_sec-($pre_roll/$nb_fps))<0){
 							$refaire_passe=false;
 							$pre_roll--;
                        }
					}
					while ($refaire_passe);
					
					// on recalcule la duree et le tcin et timecode
					//$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-(2/$nb_fps)+($pre_roll/$nb_fps);
                    $tc_in_sec=$offset_in_sec-($pre_roll/$nb_fps);
					$this->writeJobLog($offset_in_sec.'-('.$pre_roll.'/'.$nb_fps.')='.$tc_in_sec);
					
                    if($tc_in_sec<0)
						$tc_in_sec=$offset_in_sec;
					
                    $duration=round($duration+$gop/$nb_fps,2); // OUT - IN + longueur GOP
                    //$timecode=secDecToTc(tcToSecDec($infos['timecode'],$nb_fps)+$offset_in_sec-(2/$nb_fps)-($pre_roll/$nb_fps),$nb_fps);
                    $timecodeSec=tcToSecDec($infos['timecode'],$nb_fps)+$offset_in_sec-(2/$nb_fps)-($pre_roll/$nb_fps);
                    if ($timecodeSec>=0)
                        $timecode=secDecToTc($timecodeSec,$nb_fps);
                    else
                        $timecode="00:00:00:00";
                    
                    $options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f mxf -vcodec copy -acodec copy -timecode '.$timecode.' -map 0 -t '.$duration.' "'.$this->getFileOutPath().'"';
                }
            }
            else if (strpos($infos['format_calc'],'VOB')!==false && $infos['format']=='MPEG-PS')
            {
                //$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-(($gop+1)/$nb_fps); // IN - gop +1
                $tc_in_sec=$offset_in_sec-$gop/$nb_fps+1/$nb_fps; // IN - gop +1
				
                if ($tc_in_sec<0)
					$tc_in_sec=$offset_in_sec;
                
                $duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+(($gop*2-2)/$nb_fps); // OUT - IN + gop*2-2
                $this->nb_frames=floor($duration*$nb_fps);
                
                $options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -dn -f vob -vcodec copy -acodec copy -scodec copy -t '.$duration.' "'.$this->getFileOutPath().'"';
                
                // ajout options -subtitles en fonction du nombre de pistes sous titre
                $nb_track_subtitle=0;
                foreach($infos['tracks'] as $track)
                {
                    if (strtolower($track['type'])=='text')
                        $nb_track_subtitle++;
                }
                
                for ($i=0;$i<$nb_track_subtitle-1;$i++)
                    $options_ffmbc.=' -newsubtitle';
                
            }
            else if ($infos['format']=='MPEG-PS')
            {
                $use_ffmpeg=true;
                
                $tc_in_son=1/$nb_fps;
                //$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-tcToSecDec($infos['timecode'],$nb_fps)-($gop/$nb_fps);
                $tc_in_sec=$offset_in_sec-($gop/$nb_fps);
                
				if($tc_in_sec<0)
					$tc_in_sec=$offset_in_sec;
				
				$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+(($gop*2)/$nb_fps); // OUT - IN + gop*2
				$this->nb_frames=floor($duration*$nb_fps);
				
				$options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -dn -f vob -vcodec copy -acodec copy -map 0 -ss '.$tc_in_son.' -t '.$duration.' "'.$this->getFileOutPath().'"';
			}
			else if ($infos['format']=='MPEG-TS')
			{
				$use_ffmpeg=true;
				
				if ($gop==0)
				{
					if ($infos['display_width']>=1920) // HD a corriger
					{
						if ($video_frame_rate==25)
							$gop=50;
						else
							$gop=60;
					}
					else // SD
					{
						if ($video_frame_rate==25)
							$gop=12;
						else
							$gop=15;
					}
				}
				
				$this->writeJobLog('GOP : '.$gop);
				
				$tc_in_son=1/$nb_fps;
				//$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps)-($gop/$nb_fps);
				$tc_in_sec=$offset_in_sec-($gop/$nb_fps);
                if($tc_in_sec<0) $tc_in_sec=$offset_in_sec;
				$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+(($gop*2)/$nb_fps); // OUT - IN + gop*2
				$this->nb_frames=floor($duration*$nb_fps);
				
				$options_ffmbc='-y -ss '.$tc_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f mpegts -vcodec copy -acodec copy -scodec copy -map 0 -ss '.$tc_in_son.' -t '.$duration.' "'.$this->getFileOutPath().'"';
			}
			else if ($infos['format']=='MXF' && $video_format=='JPEG 2000')
			{
				$use_partial_restore=true;
				
				if (isset($params_job['offset']) && !empty($params_job['offset']))
					$timecode_in=secDecToTc(tcToSecDec($params_job['tcin'],$nb_fps)-tcToSecDec($params_job['offset'],$nb_fps)+tcToSecDec($infos['timecode'],$nb_fps),$nb_fps);
				else
					$timecode_in=$params_job['tcin'];
				
				if ($video_scan_type=='progressive')
				{
					$tc_in=$timecode_in;
					$duration=secDecToTc(tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)-1/$nb_fps,$nb_fps);
					$this->nb_frames=floor($duration*$nb_fps);
				}
				else
				{
					// on double le nombre d'images
					$tab_tc=explode(':',$timecode_in);
					$tc_in=$tab_tc[0].':'.$tab_tc[1].':'.$tab_tc[2].':'.sprintf('%02d',$tab_tc[3]*2);
					
					$duration=secDecToTc(tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps),$nb_fps);
					$tab_tc=explode(':',$duration);
					$duration=$tab_tc[0].':'.$tab_tc[1].':'.$tab_tc[2].':'.sprintf('%02d',$tab_tc[3]*2);
					
					$this->nb_frames=floor($duration*$nb_fps*2);
				}
				//partial_restore --in=00:31:25:00 --duration=00:00:09:24 JP2K/P-120220-TUP-004-J2K-HD-LL-Frame-25psf.mxf P-120220-TUP-004-J2K-HD-LL-Frame-25psf_TC00312500_TC00313500.mxf
				$option_partial_restore='--in='.$tc_in.' --duration='.$duration.' '.escapeQuoteShell($this->getFileInPath()).'" "'.$this->getFileOutPath().'"';
				
				
			}
			else if(strpos($infos['format_calc'],'WMV')!==false && $video_format=='VC-1')
			{
				$use_ffmpeg=true;
				
				$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps; // OUT - IN + 1 image
				$this->nb_frames=floor($duration*$nb_fps);
				
				//ffmpeg-1.2 -ss 00:00:48.08 -i animation_satellite_SVOM_2007.wmv -f asf  -vcodec copy -acodec copy -map 0 -t 00:00:19.68 -y animation_satellite_SVOM_2007_00004802_00010718.wmv
				$options_ffmbc='-ss '.$offset_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f asf -vcodec copy -acodec copy -map 0 -t '.$duration.' -y "'.$this->getFileOutPath().'"';
			}
			else if($infos['format']=='WebM' || $infos['format']=='Matroska' || $infos['format']=='DivX')
			{
				$use_ffmpeg=true;
				
				if ($infos['format']=='WebM')
					$format='webm';
				else if ($infos['format']=='Matroska')
					$format='matroska';
				else
					$format='avi';
				
				$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps; // OUT - IN + 1 image
				$this->nb_frames=floor($duration*$nb_fps);
				
				$options_ffmbc='-ss '.$offset_in_sec.' -i '.escapeQuoteShell($this->getFileInPath()).' -f '.$format.' -vcodec copy -acodec copy -map 0 -t '.$duration.' -y "'.$this->getFileOutPath().'"';
			}
			else
			{
				//$tc_in_sec=tcToSecDec($params_job['tcin'],$nb_fps);
				$tc_in_sec=$offset_in_sec;
				$duration=tcToSecDec($params_job['tcout'],$nb_fps)-tcToSecDec($params_job['tcin'],$nb_fps)+1/$nb_fps; // OUT - IN + 1 image
				$this->nb_frames=floor($duration*$nb_fps);
				$options_ffmbc='-y -ss '.$tc_in_sec.' -t '.$duration.' -i '.escapeQuoteShell($this->getFileInPath()).' -vcodec copy -acodec copy "'.$this->getFileOutPath().'"';
			}
			
			// si on utilise les outils opencube pour le decoupage
			if ($use_partial_restore===true)
			{
				$this->shellExecute('export','MXF_HOME="'.kMxftkHome.'"; '.kPartialRestorePath.' '.$option_partial_restore);
			}
			else
			{
				if ($use_ffmpeg===true)
					$this->shellExecute(kFFMPEGpath,$options_ffmbc,false);
				else
				{
					// calcul du nombre de pistes audio pour la conservation des pistes audio
					$nb_pistes_audio=0;
					
					foreach ($infos['tracks'] as $track)
					{
						if (strtolower($track['type'])=='audio')
							$nb_pistes_audio++;
					}
					
					if ($nb_pistes_audio>1)
					{
						if ($infos['format']=='MXF' && strpos($infos['format_calc'],'XDCAM')!==false){
							for ($i=0;$i<$nb_pistes_audio-1;$i++)
							{
								$options_ffmbc.=' -acodec pcm_s24le -sample_fmt s32 -ar 48000 -ac 1 -newaudio';
							}
						} else {
							for ($i=0;$i<$nb_pistes_audio-1;$i++)
							{
								$options_ffmbc.=' -newaudio';
							}
						
						}
					}
					
					$this->shellExecute(kFFMBCpath,$options_ffmbc,false);
				}
				
				do
				{
					$this->updateProgress();
				}
				while($this->checkIfFfmbcRunning());
				
				$this->updateProgress();
				
				if ($this->nb_frames_encodees<($this->nb_frames-50))
				{
					throw new Exception('Erreur encodage ffmbc (nb frames : '.$this->nb_frames_encodees.' < '.$this->nb_frames.')');
				}
			}
		}
			// On execute ce code apres que l image soit générée
		if(isset($params_job['iptc_write'])){
			require_once(libDir."class_iptc.php");
			foreach ($params_job['iptc_write'] as $value)
			{
				foreach ($value as $key => $value){
					$new_key= str_replace('iptc_','', $key);
					 $iptc_val[$new_key]=$value;
				}
			}
			$iptc=new class_iptc($this->getFileOutPath());
			$ip=$iptc->fct_ecrireIPTC($iptc_val,$iptc->h_cheminImg);
		}

		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function finishJob()
	{
		//supression du log avidemux
		if (file_exists($this->getTmpDirPath().'/avidemux.log'))
		{
			if (unlink($this->getTmpDirPath().'/avidemux.log')===false)
				throw new FileException('Failed to delete file ('.$this->getTmpDirPath().'/avidemux.log)');
		}
		
		if (file_exists($this->getTmpDirPath().'/avidemux.log.logdemux'))
		{
			if (unlink($this->getTmpDirPath().'/avidemux.log.logdemux')===false)
				throw new FileException('Failed to delete file ('.$this->getTmpDirPath().'/avidemux.log.logdemux)');
		}
		
		// suppression du tmp mxf dans le cas d'un decoupage d'un mxf avec un gop
		if (file_exists($this->getTmpDirPath().'/tmp.mxf'))
		{
			if (unlink($this->getTmpDirPath().'/tmp.mxf')===false)
				throw new FileException('Failed to delete file ('.$this->getTmpDirPath().'/tmp.mxf)');
		}
		
		parent::finishJob();
	}
	
	public function updateProgress()
	{
		// progression avec ffmbc
		$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
		// VP 8/04/13 : compatibilité Mac OS X
		$donnees_stderr=str_replace("\r","\n",$donnees_stderr);
		preg_match_all("/frame=([ 0-9]+) fps=(.*)/", $donnees_stderr, $infos_encodage);
		$this->nb_frames_encodees=intval($infos_encodage[1][count($infos_encodage[1])-1]);
		
		$this->writeOutLog($this->nb_frames_encodees.' / '.$this->nb_frames);
		$ratio=$this->nb_frames_encodees/$this->nb_frames;
		
		if ($ratio>1)
			$ratio=1;
		
		$this->setProgression($ratio*100);
	}
	
	private function checkIfFfmbcRunning()
	{
		//if ($this->nb_frames_encodees<$this->nb_frames)
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$this->getPid().'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1)
				return true;
			else
				return false;
		}
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
	
	private function getGopSize($gop_params)
	{
		$donnees_gop=explode(',',$gop_params);
		foreach($donnees_gop as $dat_gop)
		{
			if (strpos($dat_gop,'N=')!=false)
			{
				$valeur_gop=explode('=',trim($donnees_gop[1]));
				return intval($valeur_gop[1]);
			}
		}
		
		return 1;
	}
}

?>
