<?php
require_once(libDir."class_cherche.php");

	// VP 15/09/10 : suppression utilisation du cache adodb

class RechercheLex extends Recherche {

	var $treeParams;
	var $affichage; //@update VG 07/04/2010: ajout de cette propriété, qui sauvegarde le mode d'affichage ('HIERARCHIQUE', 'LISTE'...)
	var $entity;	//@update VG 21/06/2011 : ajout de cette variable en "public"
	
	var $highlightedFieldInHierarchy;

	function __construct() {
	  	$this->entity='LEX';
     	$this->name=kLexique;
     	$this->prefix='L';
     	$this->sessVar='recherche_LEX';
		$this->tab_recherche=array();
		$this->useSession=true;
		$this->sqlSuffixe=" GROUP BY L.ID_LEX, L.ID_LANG, T3.LEX_TERME, T2.LEX_TERME, EL.ETAT_LEX  HAVING 1=1 ";
		$this->highlightedFieldInHierarchy='LEX_TERME';
        $this->lang=$_SESSION['langue'];
	}


    function prepareSQL(){
		global $db;
		// VP 8/07/09 : optimisation requête par ajout clause ID_LEX!=0
        // VP 15/7/14 : ajout distinct aux count()
		$this->sql = "select L.ID_LEX,L.LEX_ID_TYPE_LEX,L.LEX_TERME, L.LEX_ID_ETAT_LEX, L.LEX_COOR,
        			  L.LEX_ID_SYN,T3.LEX_TERME as LEX_SYN,T2.LEX_TERME as LEX_GEN, L.ID_LANG,
					  count(distinct NULLIF(D.ID_DOC,0)) as NB_DOC,count(distinct NULLIF(DL.ID_PERS,0)) as NB_PERS,L.LEX_ID_GEN, EL.ETAT_LEX 
        			  from t_lexique L
					  left join t_lexique T2 on (L.LEX_ID_GEN=T2.ID_LEX and T2.ID_LANG='".$this->lang."' )
					  left join t_lexique T3 on (L.LEX_ID_SYN=T3.ID_LEX and T3.ID_LANG='".$this->lang."')
					  left join t_etat_lex EL on (L.LEX_ID_ETAT_LEX=EL.ID_ETAT_LEX and EL.ID_LANG=L.ID_LANG)
					  left join t_doc_lex DL on (DL.ID_LEX=L.ID_LEX and DL.ID_LEX!=0)
					  left JOIN t_doc D ON D.id_doc= DL.id_doc
			          where L.ID_LANG='".$this->lang."' ";
    }



    /**
     * CONTRUCTION ARBRE HIERARCHIQUE
     * La partie ci-dessous est exclusivement consacrée à la construction d'un arbre hiérarchique (appelée par DTREE)
     * Récupère les fils d'un noeud id_lex.
     */
    function getChildren($id_lex,&$arrNodes,$fullTree = false) {
    	global $db;

		// VP 23/10/09 : ajout critère onlyDocs
		// VP 8/11/12 : utilisation de sql_op pour ID_TYPE_LEX 
        // VP 6/09/13 : changement nb_children en NB_CHILDREN et nb_asso en NB_ASSO pour compatibilité POSTGRE
		$sql="SELECT l1. * , count( l2.ID_LEX ) AS NB_CHILDREN, count(la.ID_LEX) as NB_ASSO FROM t_lexique l1
        LEFT JOIN t_lexique l2 ON ( l2.LEX_ID_GEN = l1.ID_LEX AND l2.LEX_ID_TYPE_LEX = l1.LEX_ID_TYPE_LEX
                                   AND l2.LEX_ID_ETAT_LEX >=2 AND l2.LEX_ID_SYN =0 AND l2.ID_LANG = '".$this->lang."' )
        LEFT JOIN t_lexique_asso la ON (la.ID_LEX=l1.ID_LEX)
        WHERE l1.ID_LANG = '".$this->lang."'
        AND l1.LEX_ID_GEN = ".intval($id_lex)."
        AND l1.LEX_ID_TYPE_LEX ".$this->sql_op($this->treeParams['ID_TYPE_LEX'])." AND l1.LEX_ID_SYN =0 AND l1.LEX_ID_ETAT_LEX >=2";
		if($this->treeParams['onlyDocs']=="1") $sql.=" and l1.LEX_NB_DOCS>0";
		// VP 28/06/2011 : prise en compte paramètre tri
		$sql.=" GROUP BY l1.ID_LEX, l1.lex_id_type_lex, l1.lex_terme, l1.lex_id_etat_lex, l1.lex_id_syn, l1.id_lang, l1.lex_id_gen, l1.lex_date_crea, l1.lex_note, l1.lex_date_mod, l1.lex_id_usager_mod, l1.lex_nb_docs, l1.lex_path ";
		if(empty($this->treeParams['tri'])) $sql.=" ORDER BY l1.LEX_ID_GEN, l1.LEX_TERME";
		else $sql.=" ORDER BY l1.LEX_ID_GEN, l1.".$this->treeParams['tri'];
    	
		//$rows=$db->CacheGetAll(120,$sql);
    	$rows=$db->GetAll($sql);
    	foreach($rows as $row) {

		    	$arrNodes['elt_'.$row['ID_LEX']]['id']=$row['ID_LEX'];
		    	$arrNodes['elt_'.$row['ID_LEX']]['id_pere']=$row['LEX_ID_GEN'];
		    	$arrNodes['elt_'.$row['ID_LEX']]['terme']=str_replace (" ' ", "'",trim($row['LEX_TERME']));
		    	$arrNodes['elt_'.$row['ID_LEX']]['valide']=true;
		    	$arrNodes['elt_'.$row['ID_LEX']]['context']=false;
		    	$arrNodes['elt_'.$row['ID_LEX']]['nb_children']=$row['NB_CHILDREN'];
				$arrNodes['elt_'.$row['ID_LEX']]['nb_asso']=$row['NB_ASSO'];
				$arrNodes['elt_'.$row['ID_LEX']]['extra']="";
				$_rowIDs[]=$row['ID_LEX'];
				if($fullTree){
					$arrNodes['elt_'.$row['ID_LEX']]['openAtLoad']=true;
					$this->getChildren($row['ID_LEX'],$arrNodes,$fullTree);
				}
		}

     	//Recherche des synonymes
    	if (!empty($_rowIDs)) {
            // VP 8/11/12 : utilisation de sql_op pour ID_TYPE_LEX 
	    	$sql="SELECT  LEX_ID_SYN,LEX_TERME from t_lexique WHERE  ID_LANG='".$this->lang."'
            AND LEX_ID_TYPE_LEX ".$this->sql_op($this->treeParams['ID_TYPE_LEX'])." AND LEX_ID_ETAT_LEX>=2
            AND LEX_ID_SYN IN (".implode(",",$_rowIDs).")";
	    	//$rs=$db->CacheExecute(120,$sql);
	    	$rs=$db->Execute($sql);


	    	while ($syno=$rs->FetchRow()) {
	    		// $arrNodes['elt_'.$syno['LEX_ID_SYN']]['extra']=str_replace (" ' ", "'",trim($syno['LEX_TERME']));
				if ((trim($arrNodes['elt_'.$syno['LEX_ID_SYN']]['extra'])) == "")
					$arrNodes['elt_'.$syno['LEX_ID_SYN']]['extra'] = str_replace (" ' ", "'",trim($syno['LEX_TERME']));
				else
					$arrNodes['elt_'.$syno['LEX_ID_SYN']]['extra'] .= ', ' . str_replace (" ' ", "'",trim($syno['LEX_TERME']));
	    	}
	    	$rs->Close();
    	}

    	//Recherche des associations
		// VP 13/04/10 : prise en compte paramètre dspAsso
        if(isset($id_lex) && !empty($id_lex)) {
		if($this->treeParams['dspAsso']!="false"){
            // VP 8/11/12 : utilisation de sql_op pour ID_TYPE_LEX 
			$sql="SELECT  l1.ID_LEX,l1.LEX_TERME,la.ID_LEX as ID_LEX_ASSO from t_lexique_asso la, t_lexique l1
            WHERE la.ID_LEX_ASSO=l1.ID_LEX
            and l1.ID_LANG='".$this->lang."'
            and la.ID_LEX in (".$id_lex.")
            AND l1.LEX_ID_ETAT_LEX != 0 AND l1.LEX_ID_TYPE_LEX ".$this->sql_op($this->treeParams['ID_TYPE_LEX']);

			//trace('ASSO : '.$sql);
			//$rs=$db->CacheExecute(120,$sql);
	    	$rs=$db->Execute($sql);

			while ($asso = $rs->FetchRow()) {
						   $arrNodes['elt_'.$asso["ID_LEX"]."_".$asso["ID_LEX_ASSO"]."_asso"]=
							array('id'=>$asso["ID_LEX"]."_".$asso["ID_LEX_ASSO"]."_asso",
								  'id_pere'=>$asso["ID_LEX_ASSO"],
								  'terme'=>str_replace (" ' ", '&rsquo;',$asso['LEX_TERME']),
								 // 'icone'=>imgUrl."dtree/ta.gif",
								  'valide'=>true,
								  'context'=>true,
								  'id_orig'=>$asso["ID_LEX"]);
						   $arrNodes['elt_'.$asso["ID_LEX"]."_".$asso["ID_LEX_ASSO"]."_asso"]['asso']=$asso["ID_LEX_ASSO"];
						   //$arrNodes['elt_'.$asso['ID_LEX']]['nb_asso']++;
			}
			$rs->Close();
		}
        }
        
    	return $arrNodes;
    }

    // VP 7/06/2012 : ajout paramètre $withChildren pour récupérer les enfants
    function getFather($id_lex,$recursif=false,&$arrNodes,$withChildren=false) {
    	global $db;
    	$sql=" SELECT ID_LEX,LEX_TERME,LEX_ID_GEN from t_lexique where ID_LANG='".$this->lang."' and ID_LEX
        in (select LEX_ID_GEN from t_lexique where ID_LEX=".intval($id_lex).") and LEX_ID_SYN=0 and LEX_ID_ETAT_LEX>=2";
		//$rows=$db->CacheGetAll(120,$sql);
		$rows=$db->GetAll($sql);
    	foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_LEX']]['id']=$row['ID_LEX'];
            $arrNodes['elt_'.$row['ID_LEX']]['id_pere']=$row['LEX_ID_GEN'];
            $arrNodes['elt_'.$row['ID_LEX']]['terme']=str_replace (" ' ", "'",trim($row['LEX_TERME']));
            
            $arrNodes['elt_'.$row['ID_LEX']]['valide']=true;
            $arrNodes['elt_'.$row['ID_LEX']]['context']=false;
            $arrNodes['elt_'.$row['ID_LEX']]['open']=true;
            
            if ($recursif && $row['LEX_ID_GEN']!=0 && $row['LEX_ID_GEN']!=$row['ID_LEX']) $this->getFather($row['ID_LEX'],$recursif,$arrNodes,$withChildren);
            
            if (!isset($arrNodes['elt_'.$row['ID_LEX']]['nb_children']))  $arrNodes['elt_'.$row['ID_LEX']]['nb_children']=1;
            //if (!isset($arrNodes['elt_'.$row['ID_LEX']]['nb_asso']))  $arrNodes['elt_'.$row['ID_LEX']]['nb_asso']=1;
            
            if($withChildren) $this->getChildren($row['ID_LEX'],$arrNodes);
    	}
    	return $arrNodes;
    }
    
    // VP 7/06/2012 : ajout paramètre $withBrother pour récupérer les frères
    function getNode($id_lex,&$arrNodes,$style='highlight',$withBrother=false) {
    	global $db;
    	$this->getFather($id_lex,true,$arrNodes,$withBrother);
		// MS - récupération du lexique avec la notion de langue ... 
    	$sql="select * from t_lexique where ID_LEX=".intval($id_lex)." AND ID_LANG=".$db->Quote($this->lang);
     	//$rows=$db->CacheGetAll(120,$sql);
		$rows=$db->GetAll($sql);
     	foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_LEX']]['id']=$row['ID_LEX'];
            $arrNodes['elt_'.$row['ID_LEX']]['id_pere']=$row['LEX_ID_GEN'];
            $arrNodes['elt_'.$row['ID_LEX']]['terme']=str_replace (" ' ", "'",trim($row['LEX_TERME']));
            
            $arrNodes['elt_'.$row['ID_LEX']]['valide']=true;
            $arrNodes['elt_'.$row['ID_LEX']]['context']=false;
            $arrNodes['elt_'.$row['ID_LEX']]['style']=$style;
			$arrNodes['elt_'.$row['ID_LEX']]['extra']="";
     	}
		// VP 8/11/12 : utilisation de sql_op pour ID_TYPE_LEX
	    $sql="SELECT  LEX_ID_SYN,LEX_TERME from t_lexique WHERE  ID_LANG='".$this->lang."'
        AND LEX_ID_TYPE_LEX ".$this->sql_op($this->treeParams['ID_TYPE_LEX'])." AND LEX_ID_ETAT_LEX>=2
        AND LEX_ID_SYN IN (".intval($id_lex).")";
	    //$rs=$db->CacheExecute(120,$sql);
	    $rs=$db->Execute($sql);
        while ($syno=$rs->FetchRow()) {
            //PC 30/01/11 : correction du bug qui n'affichait que le dernier synonyme dans l'affichage hiérarchique
            if ((trim($arrNodes['elt_'.$syno['LEX_ID_SYN']]['extra'])) == "")
                $arrNodes['elt_'.$syno['LEX_ID_SYN']]['extra'] = str_replace (" ' ", "'",trim($syno['LEX_TERME']));
            else
                $arrNodes['elt_'.$syno['LEX_ID_SYN']]['extra'] .= ', ' . str_replace (" ' ", "'",trim($syno['LEX_TERME']));
        }
        $rs->Close();
        
     	$this->getChildren($id_lex,$arrNodes);
    	return $arrNodes;
        
    }

    function complementNodes(&$arrNodes,$_rowIDs,$fatherID) { //ajouts complémentaires

    	global $db;
    	//debug($_rowIDs,'beige');




		/*
     	$sql="SELECT  l1.ID_LEX,l1.LEX_TERME,la.ID_LEX_ASSO as ID_LEX_ASSO from t_lexique_asso la, t_lexique l1
			   WHERE la.id_lex=l1.id_lex
			   and l1.ID_LANG='".$this->lang."'
			   AND l1.LEX_ID_ETAT_LEX AND l1.LEX_ID_TYPE_LEX=".$db->Quote($this->treeParams['ID_TYPE_LEX']);

    	$rs=$db->Execute($sql);

    	while ($asso=$rs->FetchRow()) {
                       $arrNodes['elt_'.$asso["ID_LEX"]."_asso"]=array(
							'id'=>$asso["ID_LEX_ASSO"]."_asso",
                       		'id_pere'=>$asso["id_lex"],
                       		'terme'=>str_replace (" ' ", '&rsquo;',$asso['LEX_TERME']),
                       		'icone'=>imgUrl."dtree/page2.gif",
                       		'valide'=>true,
                       		'context'=>true,
							'id_orig'=>$asso["ID_LEX"]);
    	}
		$rs->Close();
		*/

    }


    function getRootName() {
    	return GetRefValue('t_type_lex',$this->treeParams['ID_TYPE_LEX'],$this->lang);

    }


	//@update VG 07/04/2010 : surcharge de la méthode putSearchInSession de la classe parente avec la sauvegarde en session
	//du paramètre 'affichage'
    function putSearchInSession($id='') {
   		parent::putSearchInSession();
   		$_SESSION[$this->sessVar]["affichage"] = $this->affichage;
    }


	function appliqueDroits() {
		if(function_exists("appliqueDroitsCustomLex")){
			$this->sqlRecherche.= appliqueDroitsCustomLex($this->sqlRecherche);
		}
	}


}
?>
