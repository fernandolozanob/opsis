<?php
require_once(libDir."class_cherche.php");


class RechercheTapeSet extends Recherche {

	function __construct() {
	  	$this->entity='TS';
     	$this->name=kJeuCartouche;
     	$this->prefix='';
     	$this->sessVar='recherche_TAPE_SET';
		$this->tab_recherche=array();
		$this->useSession=true;
		$this->sqlSuffixe=" GROUP BY t_tape_set.ID_TAPE_SET, t_groupe_mat.GROUPE_MAT";
	}


    function prepareSQL(){
		global $db;
		$this->sql = "select t_tape_set.*, t_groupe_mat.GROUPE_MAT,count(t_tape.ID_TAPE) as NB from t_tape_set 
						left outer join t_tape on t_tape_set.ID_TAPE_SET=t_tape.ID_TAPE_SET 
						left outer join t_groupe_mat on t_groupe_mat.ID_GROUPE_MAT = t_tape_set.TS_ID_GROUPE_MAT
						WHERE 1=1 "; 
    }
      
}    
?>