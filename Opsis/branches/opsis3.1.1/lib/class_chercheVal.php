<?php
require_once(libDir."class_cherche.php");


class RechercheVal extends Recherche {

    var $treeParams;
    var $affichage;
    var $entity;
    var $highlightedFieldInHierarchy;
    
	function __construct() {
	  	$this->entity='VAL';
		
		if (defined('kPlace'))
			$this->name=kPlace;
		
     	$this->prefix='v';
     	$this->sessVar='recherche_VAL';
		$this->tab_recherche=array();
		$this->useSession=true;
        $this->highlightedFieldInHierarchy='VALEUR';
        $this->lang=$_SESSION['langue'];
	}

	// MS 15/04/2013 - Ajout jointure t_doc_val pour avoir le nombre de docs li�s � chaque valeur. (NB_DOC)
    function prepareSQL($groupby=false){
		global $db;
		if($groupby){
			$this->sql = "
			SELECT v.*,tv.TYPE_VAL,prt.VALEUR as PRT_VALEUR, prt.ID_VAL as PRT_ID_VAL , f.FONDS, COUNT(t_doc_val.ID_DOC) as NB_DOC
				FROM t_val v
					INNER JOIN t_type_val tv ON (v.VAL_ID_TYPE_VAL=tv.ID_TYPE_VAL and v.ID_LANG=tv.ID_LANG)
					LEFT JOIN t_val prt ON (v.VAL_ID_GEN=prt.ID_VAL and prt.ID_LANG=v.ID_LANG)
					LEFT JOIN t_doc_val ON  t_doc_val.ID_VAL = v.ID_VAL
					LEFT JOIN t_fonds f ON (ID_FONDS = v.VAL_ID_FONDS and f.ID_LANG=".$db->Quote($_SESSION['langue']).")
			WHERE v.ID_LANG=".$db->Quote($_SESSION['langue']);
			$this->sqlSuffixe = " GROUP BY v.id_val, v.id_lang, tv.type_val, prt.valeur, prt.id_val, f.FONDS ";
		}else{
			$this->sql = "
			SELECT v.*,tv.TYPE_VAL,prt.VALEUR as PRT_VALEUR, prt.ID_VAL as PRT_ID_VAL, f.FONDS
			FROM t_val v
			INNER JOIN t_type_val tv ON (v.VAL_ID_TYPE_VAL=tv.ID_TYPE_VAL and v.ID_LANG=tv.ID_LANG)
			LEFT JOIN t_val prt ON (v.VAL_ID_GEN=prt.ID_VAL and prt.ID_LANG=v.ID_LANG)
			LEFT JOIN t_fonds f ON (ID_FONDS = v.VAL_ID_FONDS and f.ID_LANG=".$db->Quote($_SESSION['langue']).")
			WHERE v.ID_LANG=".$db->Quote($_SESSION['langue']);
			$this->sqlSuffixe="";
		}
		$this->etape="";
    }

    /**
     * CONTRUCTION ARBRE HIERARCHIQUE
     * La partie ci-dessous est exclusivement consacr�e � la construction d'un arbre hi�rarchique (appel�e par DTREE)
     * R�cup�re les fils d'un noeud id_val.
     */
    function getChildren($id_val,&$arrNodes,$fullTree = false) {
        global $db;
        
        $sql="SELECT l1. * , count( l2.ID_VAL ) AS NB_CHILDREN FROM t_val l1
        LEFT JOIN t_val l2 ON ( l2.VAL_ID_GEN = l1.ID_VAL AND l2.VAL_ID_TYPE_VAL = l1.VAL_ID_TYPE_VAL
                                   AND l2.ID_LANG = '".$this->lang."' )
        WHERE l1.ID_LANG = '".$this->lang."'
        AND l1.VAL_ID_GEN = ".intval($id_val)."
        AND l1.VAL_ID_TYPE_VAL ".$this->sql_op($this->treeParams['ID_TYPE_VAL']);
        $sql.=" GROUP BY l1.ID_VAL, l1.ID_LANG";
        if(empty($this->treeParams['tri'])) $sql.=" ORDER BY l1.VAL_ID_GEN, l1.VALEUR";
        else $sql.=" ORDER BY l1.VAL_ID_GEN, l1.".$this->treeParams['tri'];

        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_VAL']]['id']=$row['ID_VAL'];
            $arrNodes['elt_'.$row['ID_VAL']]['id_pere']=$row['VAL_ID_GEN'];
            $arrNodes['elt_'.$row['ID_VAL']]['terme']=str_replace (" ' ", "'",trim($row['VALEUR']));
            $arrNodes['elt_'.$row['ID_VAL']]['valide']=true;
            $arrNodes['elt_'.$row['ID_VAL']]['context']=false;
            $arrNodes['elt_'.$row['ID_VAL']]['nb_children']=$row['NB_CHILDREN'];
            $arrNodes['elt_'.$row['ID_VAL']]['nb_asso']=$row['NB_ASSO'];
            $_rowIDs[]=$row['ID_VAL'];
            if($fullTree){
                $arrNodes['elt_'.$row['ID_VAL']]['openAtLoad']=true;
                $this->getChildren($row['ID_VAL'],$arrNodes,$fullTree);
            }
        }
        
        return $arrNodes;
    }
    
    function getFather($id_val,$recursif=false,&$arrNodes,$withChildren=false) {
        global $db;
        $sql=" SELECT ID_VAL,VALEUR,VAL_ID_GEN from t_val where ID_LANG='".$this->lang."' and ID_VAL
        in (select VAL_ID_GEN from t_val where ID_VAL=".intval($id_val).")";
        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_VAL']]['id']=$row['ID_VAL'];
            $arrNodes['elt_'.$row['ID_VAL']]['id_pere']=$row['VAL_ID_GEN'];
            $arrNodes['elt_'.$row['ID_VAL']]['terme']=str_replace (" ' ", "'",trim($row['VALEUR']));
            
            $arrNodes['elt_'.$row['ID_VAL']]['valide']=true;
            $arrNodes['elt_'.$row['ID_VAL']]['context']=false;
            $arrNodes['elt_'.$row['ID_VAL']]['open']=true;
            
            if ($recursif && $row['VAL_ID_GEN']!=0 && $row['VAL_ID_GEN']!=$row['ID_VAL']) $this->getFather($row['ID_VAL'],$recursif,$arrNodes,$withChildren);
            
            if (!isset($arrNodes['elt_'.$row['ID_VAL']]['nb_children']))  $arrNodes['elt_'.$row['ID_VAL']]['nb_children']=1;
            
            if($withChildren) $this->getChildren($row['ID_VAL'],$arrNodes);
        }
        return $arrNodes;
    }
    
    function getNode($id_val,&$arrNodes,$style='highlight',$withBrother=false) {
        global $db;
        $this->getFather($id_val,true,$arrNodes,$withBrother);
        $sql="select * from t_val where ID_VAL=".intval($id_val)." AND ID_LANG=".$db->Quote($this->lang);
        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_VAL']]['id']=$row['ID_VAL'];
            $arrNodes['elt_'.$row['ID_VAL']]['id_pere']=$row['VAL_ID_GEN'];
            $arrNodes['elt_'.$row['ID_VAL']]['terme']=str_replace (" ' ", "'",trim($row['VALEUR']));
            
            $arrNodes['elt_'.$row['ID_VAL']]['valide']=true;
            $arrNodes['elt_'.$row['ID_VAL']]['context']=false;
            $arrNodes['elt_'.$row['ID_VAL']]['style']=$style;
        }
        
        $this->getChildren($id_val,$arrNodes);
        return $arrNodes;
        
    }
    
    function getRootName() {
        return GetRefValue('t_type_val',$this->treeParams['ID_TYPE_VAL'],$this->lang);
        
    }
    
    
    // Surcharge de la m�thode putSearchInSession de la classe parente avec la sauvegarde en session du param�tre 'affichage'
    function putSearchInSession($id='') {
        parent::putSearchInSession();
        $_SESSION[$this->sessVar]["affichage"] = $this->affichage;
    }

	function appliqueDroits() {
		if(function_exists("appliqueDroitsCustomVal")){
			$this->sqlRecherche.= appliqueDroitsCustomVal($this->sqlRecherche);
		}
	}
}
?>
