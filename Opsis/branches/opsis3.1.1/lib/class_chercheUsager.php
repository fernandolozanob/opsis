<?php
require_once(libDir."class_cherche.php");


class RechercheUsager extends Recherche {

	function __construct() {
	  	$this->entity='US';
     	$this->name=kUsager;
     	$this->prefix='u';
     	$this->sessVar='recherche_USR';
		$this->tab_recherche=array();
		$this->useSession=true;
		$this->sqlSuffixe=" GROUP BY u.id_usager,tu.TYPE_USAGER,eu.ETAT_USAGER ";
	}


    function prepareSQL(){
		global $db;
		// VP 20/1/09 : modification requête de base

		$this->sql = "
			SELECT u.*,tu.TYPE_USAGER,eu.ETAT_USAGER, ".$db->stringAggregate('g.groupe', ', ')." groupes
				FROM t_usager u
					LEFT JOIN t_etat_usager eu on (u.US_ID_ETAT_USAGER=eu.ID_ETAT_USAGER) AND eu.ID_LANG=".$db->Quote($_SESSION['langue'])."
					LEFT JOIN t_type_usager tu on (u.US_ID_TYPE_USAGER=tu.ID_TYPE_USAGER)
					LEFT JOIN t_usager_groupe ug on (ug.ID_USAGER=u.ID_USAGER)
					LEFT JOIN t_groupe g on (g.ID_GROUPE=ug.ID_GROUPE)
				WHERE 1=1
			 	";
        $this->etape="";
    }
	
	
	
/**
 * En fonction des types de recherche, appelle les fonctions de traitement dédiée.
 * Cette fonction peut se trouver surchargée si une classe introduit des recherches spécifiques
 */
    protected function ChooseType($tabCrit,$prefix) {
            // I.2.A.1 Tests des champs et appel des fonctions de recherche ad?quates :
            switch($tabCrit['TYPE'])
            {
                case "FT": //FullText
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    break;
                case "FTS": //FullText avec découpage de chaque mot
                	$this->FToperator='+';
                	$this->chercheTexteAmongFields($tabCrit,$prefix);
                	break;
                case "C":   // Recherche sur un champ précis
                    $this->chercheChamp($tabCrit,$prefix);
                    break;
                case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
                    $this->chercheChamp($tabCrit,$prefix,true);
                    break;
                case "CB":   // VP 24/07/09 : Recherche sur un champ booléen
                    $this->chercheChampBooleen($tabCrit,$prefix);
                    break;
                case "CI":    // Recherche sur un champ d'une table annexe par ID
                    $this->chercheIdChamp($tabCrit,$prefix);
                    break;
                case "CT": // Recherche sur un champ par texte exact dans une table liée (fonds, genre,...)
                    $this->chercheChampTable($tabCrit,$prefix);
                    break;
                case "D":   // Recherche sur date(s)
                    $this->chercheDate($tabCrit,$prefix);
                    break;
                case "H":   // Recherche sur durées(s)
                    $this->chercheDuree($tabCrit,$prefix);
                    break;
					// VP 23/12/08 : ajout recherche VF
                case "V":  // Recherche sur valeur
                case "VF":  // Recherche sur valeur en FT
                    $this->chercheVal($tabCrit,$prefix);
                    break;
                case "VI":    // Recherche sur valeur par ID (récursif)
                    $this->chercheIdVal($tabCrit,$prefix,true);
                    break;
                case "CD" : //comparaison de date
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CDLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CDGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CH" : //comparaison de date
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CV" : //comparaison de valeur =
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVGT" : //comparaison de valeur >
                	$tabCrit['VALEUR2']='>';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
				case "CVGTE" : //comparaison de valeur >=
                	$tabCrit['VALEUR2']='>=';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVLT" : //comparaison de valeur <
                	$tabCrit['VALEUR2']='<';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
				case "CVLTE" : //comparaison de valeur <=
                	$tabCrit['VALEUR2']='<=';
                	$this->compareValeur($tabCrit,$prefix);
                	break;

                case "LI" : // Recherche sur ID LEX unique (récursif)
                	$this->chercheIdLex($tabCrit,$prefix,true);
                	break;
                case "L" : // Recherche sur termes lexique (av. extension lex PRE)
                	$this->chercheLex($tabCrit,$prefix,true,false);
                	break;
                case "LP" : // Recherche sur termes lexique (av. extension lex PRE et POST)
                	$this->chercheLex($tabCrit,$prefix,true,true);
                	break;

                case "P" :
                	$this->cherchePers($tabCrit,$prefix);
                	break;
                case "PI" :
                	$this->chercheIdPers($tabCrit,$prefix);
                	break;
				case "U" :
					$this->chercheUsager($tabCrit,$prefix);
					break;
				case "UGR" :
					$this->chercheUsagerGroupe($tabCrit,$prefix);
					break;
				case "XML" :
					$this->chercheXML($tabCrit,$prefix);
				break;
                default : break;
            }
            
    }
	
	/** Recherche par utilisateur **/
    protected function chercheUsagerGroupe($tabCrit,$prefix="") {

    	global $db;
		$prefix=($prefix!=""?$prefix.".":"");
		
		$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."ID_USAGER IN (select ID_USAGER from t_usager_groupe where ".$tabCrit['FIELD']." ";

	   	$this->sqlRecherche.= $this->sql_op($tabCrit['VALEUR'])."))";
	}

	function appliqueDroits() {
		if(function_exists("appliqueDroitsCustomUsager")){
			$this->sqlRecherche.= appliqueDroitsCustomUsager($this->sqlRecherche);
		}
	}

}
?>
