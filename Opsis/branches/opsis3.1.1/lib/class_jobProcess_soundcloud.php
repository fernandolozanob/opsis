<?php
require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_uploadException.php');
require_once(libDir.'class_soundcloud.php');
require_once(libDir.'soundcloud_sdk/Soundcloud.php');

class JobProcess_soundcloud extends JobProcess implements Process
{

	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('soundcloud');
		
		$this->required_xml_params=array
		(
			'user'=>'string',
			'pass'=>'string',
			'api_secret'=>'string',
			'api_key'=>'string',
			'donnees'=>'array'
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
		
		$required_params=array
		(
			'title'=>'string'		
		);
		
		$optional_params=array
		(

		);
		
		//verification des donnees presentes dans le noeud donnees
		if (!empty($required_params))
		{
			foreach($required_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
				{
					//$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
				}
				else if (isset($this->params_job['donnees'][0][$param_name]) && empty($this->params_job['donnees'][0][$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($optional_params))
		{
			foreach($optional_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
                {
					//$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
                }
			}
		}
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		$sc = new SC();
		$sc->username=$params_job['user'];
		$sc->password=$params_job['pass'];
		$sc->api_key=$params_job['api_key'];
		$sc->api_secret=$params_job['api_secret'];
		$sc->private=$params_job['private'];

		try{
			$accessToken=$sc->connectSC();
		}

		catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) {
		    exit($e->getMessage());
		}
		
		//upload
		$this->setProgression(50); 
		$path_to_track_to_upload = $this->getFileInPath();
		$upload = $sc->publish_track($path_to_track_to_upload,$params_job);
		if(!empty($sc->id_audio_soundcloud)){
			$this->writeOutLog('SC video_id:<id>'.$sc->id_audio_soundcloud.'</id>');
		}		
		if(!empty($sc->private_id)){
			$this->writeOutLog('SC private_id:<id>'.$sc->private_id.'</id>');
		}
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');

	}
	
	public function finishJob()
	{
		if (file_exists($this->link_file_path))
		{
			if (unlink($this->link_file_path)===false) {
				throw new FileException('Failed to delete link ('.$this->link_file_path.')');
            }
		}
		
		parent::finishJob();
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}
?>