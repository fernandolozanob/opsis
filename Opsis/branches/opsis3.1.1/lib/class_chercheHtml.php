<?php
require_once(libDir."class_cherche.php");


class RechercheHtml extends Recherche {

    var $treeParams;
    var $affichage;
    var $entity;
    var $highlightedFieldInHierarchy;
    
	function __construct() {
	  	$this->entity='HTML';
     	$this->prefix='h';
     	$this->sessVar='recherche_HTML';
		$this->tab_recherche=array();
		$this->useSession=true;
        $this->highlightedFieldInHierarchy='HTML_TITRE';
        $this->lang=$_SESSION['langue'];
	}

    function prepareSQL(){
		global $db;
		$this->sql = "
			SELECT h.*,prt.HTML_TITRE as PRT_HTML_TITRE, prt.ID_HTML as PRT_ID_HTML
				FROM t_html h
					LEFT JOIN t_html prt ON (h.HTML_ID_GEN=prt.ID_HTML and prt.ID_LANG=".$db->Quote($_SESSION['langue']).")
		WHERE 1=1"; 
		//h.ID_LANG=".$db->Quote($_SESSION['langue']);
		$this->sqlSuffixe = "";
        $this->etape="";
    }

    /**
     * CONTRUCTION ARBRE HIERARCHIQUE
     * La partie ci-dessous est exclusivement consacr�e � la construction d'un arbre hi�rarchique (appel�e par DTREE)
     * R�cup�re les fils d'un noeud id_html.
     */
    function getChildren($id_html,&$arrNodes,$fullTree = false) {
        global $db;
        
        $sql="SELECT l1. * , count( l2.ID_HTML ) AS NB_CHILDREN FROM t_html l1
        LEFT JOIN t_html l2 ON ( l2.HTML_ID_GEN = l1.ID_HTML AND l2.ID_LANG = '".$this->lang."' )
        WHERE l1.ID_LANG = '".$this->lang."'
        AND l1.HTML_ID_GEN = ".intval($id_html)."
        GROUP BY l1.ID_HTML, l1.ID_LANG";
        if(empty($this->treeParams['tri'])) $sql.=" ORDER BY l1.HTML_ID_GEN, l1.HTML_TITRE";
        else $sql.=" ORDER BY l1.HTML_ID_GEN, l1.".$this->treeParams['tri'];

        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_HTML']]['id']=$row['ID_HTML'];
            $arrNodes['elt_'.$row['ID_HTML']]['id_pere']=$row['HTML_ID_GEN'];
            $arrNodes['elt_'.$row['ID_HTML']]['terme']=str_replace (" ' ", "'",trim($row['HTML_TITRE']));
            $arrNodes['elt_'.$row['ID_HTML']]['valide']=true;
            $arrNodes['elt_'.$row['ID_HTML']]['context']=false;
            $arrNodes['elt_'.$row['ID_HTML']]['nb_children']=$row['NB_CHILDREN'];
            $arrNodes['elt_'.$row['ID_HTML']]['nb_asso']=$row['NB_ASSO'];
            $_rowIDs[]=$row['ID_HTML'];
            if($fullTree){
                $arrNodes['elt_'.$row['ID_HTML']]['openAtLoad']=true;
                $this->getChildren($row['ID_HTML'],$arrNodes,$fullTree);
            }
        }
        
        return $arrNodes;
    }
    
    function getFather($id_html,$recursif=false,&$arrNodes,$withChildren=false) {
        global $db;
        $sql=" SELECT ID_HTML,HTML_TITRE,HTML_ID_GEN from t_html where ID_LANG='".$this->lang."' and ID_HTML
        in (select HTML_ID_GEN from t_html where ID_HTML=".intval($id_html).")";
        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_HTML']]['id']=$row['ID_HTML'];
            $arrNodes['elt_'.$row['ID_HTML']]['id_pere']=$row['HTML_ID_GEN'];
            $arrNodes['elt_'.$row['ID_HTML']]['terme']=str_replace (" ' ", "'",trim($row['HTML_TITRE']));
            
            $arrNodes['elt_'.$row['ID_HTML']]['valide']=true;
            $arrNodes['elt_'.$row['ID_HTML']]['context']=false;
            $arrNodes['elt_'.$row['ID_HTML']]['open']=true;
            
            if ($recursif && $row['HTML_ID_GEN']!=0 && $row['HTML_ID_GEN']!=$row['ID_HTML']) $this->getFather($row['ID_HTML'],$recursif,$arrNodes,$withChildren);
            
            if (!isset($arrNodes['elt_'.$row['ID_HTML']]['nb_children']))  $arrNodes['elt_'.$row['ID_HTML']]['nb_children']=1;
            
            if($withChildren) $this->getChildren($row['ID_HTML'],$arrNodes);
        }
        return $arrNodes;
    }
    
    function getNode($id_html,&$arrNodes,$style='highlight',$withBrother=false) {
        global $db;
        $this->getFather($id_html,true,$arrNodes,$withBrother);
        $sql="select * from t_html where ID_HTML=".intval($id_html)." AND ID_LANG=".$db->Quote($this->lang);
        $rows=$db->GetAll($sql);
        foreach($rows as $row) {
            
            $arrNodes['elt_'.$row['ID_HTML']]['id']=$row['ID_HTML'];
            $arrNodes['elt_'.$row['ID_HTML']]['id_pere']=$row['HTML_ID_GEN'];
            $arrNodes['elt_'.$row['ID_HTML']]['terme']=str_replace (" ' ", "'",trim($row['HTML_TITRE']));
            
            $arrNodes['elt_'.$row['ID_HTML']]['valide']=true;
            $arrNodes['elt_'.$row['ID_HTML']]['context']=false;
            $arrNodes['elt_'.$row['ID_HTML']]['style']=$style;
        }
        
        $this->getChildren($id_html,$arrNodes);
        return $arrNodes;
        
    }
    
    function getRootName() {
        return "";
        
    }
    
    
    // Surcharge de la m�thode putSearchInSession de la classe parente avec la sauvegarde en session du param�tre 'affichage'
    function putSearchInSession($id='') {
        parent::putSearchInSession();
        $_SESSION[$this->sessVar]["affichage"] = $this->affichage;
    }


}
?>
