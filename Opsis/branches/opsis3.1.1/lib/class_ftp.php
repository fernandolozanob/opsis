<?php

class Ftp
{
	private $login;
	private $mot_passe;
	private $home;
	private $uid;
	private $gid;
	private $directory_exists;
	
	const COPY=0;
	const LINK=1;
	const SYM_LINK=2;
	
	function __construct()
	{
		$this->login='';
		$this->mot_passe='';
		$this->home='';
		$this->uid=0;
		$this->gid=0;
		$this->directory_exists=false;
		
	}
	
	/* setters */
	function setLogin($val)
	{
		$this->login=$val;
	}
	
	function setPassword($val)
	{
		$this->mot_passe=$val;
	}
	
	function setHome($val,$create_dir=true)
	{
		$this->home=$val;
		
		if ($create_dir==true) {
			$ok = $this->createDir();
			if(!$ok) {
				trace("Echec Creation Home FTP : ".$val);
			}
		}
	}
	
	function setUid($val)
	{
		$this->uid=$val;
	}
	
	function setGid($val)
	{
		$this->gid=$val;
	}
	
	/*getters*/
	function getLogin()
	{
		return $this->login;
	}
	
	function getPassword()
	{
		return $this->mot_passe;
	}

	
	function getHome()
	{
		return $this->home;
	}
	
	function createDir()
	{
		if (!empty($this->home))
		{
			if (file_exists($this->home))
			{
				$this->directory_exists=true;
				return true;
			}
			else
			{
				if (mkdir($this->home))
				{
					$this->directory_exists=true;
					return true;
				}
				else
					return false;
			}
		}
		else
			return false;
	}
	
	function addFile($file,$type=Ftp::COPY, $new_name='')
	{
		if (empty($new_name))
			$new_name=basename($file);
		
		switch ($type)
		{
			case Ftp::COPY:
				return copy($file,$this->home.$new_name);
				break;
			case Ftp::LINK:
				return link($file,$this->home.$new_name);
				break;
			case Ftp::SYM_LINK:
				return symlink($file,$this->home.$new_name);
				break;
			default:
				return false;
		}
	}
	
	function createCompte()
	{
		if (!empty($this->login) && !empty($this->mot_passe) && !empty($this->home) && $this->directory_exists==true) {
			
            $commande = gFtpAccessPath." insert ".$this->login." ".$this->mot_passe." ".$this->home;
            $output = exec($commande, $array = array(), $return);
            trace("$commande -> output=$output return=$return");
			return $return;
			
		} else {
			trace("Echec Creation FTP : $commande -> output=$output return=$return; params : login : ".$this->login.", password : ".$this->mot_passe.", home : ".$this->home);
			return false;
		}
	}
	

}

?>