<?php


class tafelTree {

	var $arrNodes; //Tableau des noeuds. Contient la structure, type de noeuds, etc.

	var $objSearch; //Objet de type recherche

	var $output;

	var $JSlink; //Fonction JS ou URL appelée sur les liens de chaque mot
	var $btnEdit; //Valeur du bouton edit
	var $id_input_result;
	var $id_input_form;

	function __construct($searchobject,$formName,$arr_values=array()) {
		$this->objSearch=$searchobject;
		$this->output='';
		$this->id_input_form=$formName;
		$this->id_div_tree='myTree';
		$this->fullTree = false ; 
		$this->checkboxTree = false ; 
		$this->openAtLoad = true ; 
		$this->type_field = '' ; 
		$this->arr_values= $arr_values ; 
		$this->styleRootName = 'font-size:14px;font-weight:bold;z-index:99;';
	}

	function setObject($searchobject) {
		$this->objSearch=$searchobject;
	}

	function makeRoot() { //Constitue la racine et les têtes de rubriques (niveau 1)

		$arrChildren=$this->objSearch->getChildren(0,$this->arrNodes,$this->fullTree); //récupération niveau 1
		//Ce noeud primaire n'est utilisé que lorsque l'on a displayRoot=true
		$this->arrNodes['elt_0']=array("id"=>0,
							"id_pere"=>-1,
							"terme"=>$this->objSearch->getRootName(),
							"open"=>1,
							"icone"=>'',
							"valide"=>false,
							"context"=>false,
							"checkbox"=>false,
							"nb_children"=>count($arrChildren),
							 "loaded"=>true,
							 'generateBigTree' => false
							);
	}

    
    // VP 7/06/2012 : ajout paramètre $withBrother pour récupérer les frères
	function revealNode($id,$style='highlight',$withBrother=false) {
		$this->objSearch->getNode($id,$this->arrNodes,$style,$withBrother);
		//$this->objSearch->getChildren($id,$this->arrNodes);
		//$this->objSearch->getFather($id,true,$this->arrNodes);
	}

	function complementNodes() {
		$this->objSearch->complementNodes($this->arrNodes);
	}

	/** Affiche l'arbre des termes.
	 * IN : $id_input_result=champ mis à jour qd on clique sur un terme, $print=affiché direct/ou renvoyé, $url_edit=lien href edition
	 * NOTES : cette routine est très consommatrice en CPU et mémoire.
	 * Elle a donc fait l'objet d'optimisations DONT :
	 * - parcours du tableau en byRef
	 * - utilisation du buffer (ob_start, etc...) pour éviter la concaténation.
	 * - 2 parcours de tableaux :
	 * 		1. pour créer les items (folder & document)
	 * 		2. pour créer les liens entre items
	 * Pour info, ces optimisations ont permis de passer de 33s à 2,5s lors de la génération
	 * d'un thésaurus de 8000 termes (synos non comptés)
	 */

	function renderOutput($id_input_result,$print=true,$url_edit='',$all_open=0,$withDeclaration=true) {

			//debug($this->arrNodes,'yellow',true);

			$this->btnEdit=$url_edit;
			static $terme;
			static $url_noeud;
			ob_start();

            if ($withDeclaration) {
				// VP 26/11/09 : ajout paramètre JSRootlink (lien sur root)
				if($this->JSRootlink) $root="<a href='#'' onclick='".$this->JSRootlink."'>".$this->objSearch->getRootName()."</a>";
				else $root=$this->objSearch->getRootName();
            echo "\n<div align='left' id='".$this->id_div_tree."'></div>\n";
            echo "<script type=\"text/javascript\">\n";
			echo "var struct1 = [\n
				{
				'id':'root1',
				'txt':\"<span style='".$this->styleRootName."' >".$root."</span>\",
				'img':'".($this->imgBase?$this->imgBase:'base.gif')."',
				'canhavechildren':true,";
			echo"'items':";
			}
			echo "[";

            /// II.A.7.a. Cr�ation des noeuds

			$this->id_input_result=$id_input_result;
			$this->makeTree($this->includeRoot?-1:0); //Doit-on également include la racine. Oui pour les paniers

		   //fin structure
		   if ($withDeclaration) echo "]
				}";
			echo"];";



			$serializedPrms=serialize($this->objSearch->treeParams);

		// VP 26/11/09 : ajout paramètre imgBaseUrl pour spécifier dossier images tafelTree
		if (!$this->imgBaseUrl && $this->checkboxTree) $this->imgBaseUrl=libUrl."/webComponents/tafelTree/form_imgs/";
		if (!$this->imgBaseUrl) {
			if (defined("treeImgBaseUrl"))
				$this->imgBaseUrl=treeImgBaseUrl;
			else
				$this->imgBaseUrl=libUrl."/webComponents/tafelTree/imgs/";
		}

		// VP 5/01/10 : ajout variable onDrop pour autoriser le drag'n'drop interne

		if ($withDeclaration) echo "
				function myOpenPopulate (branch, response) {
					//alert(response);
					return response;
				}

				function openNode(id) {
					//alert(id);
					myForm=document.".$this->id_input_form.";
					if (myForm.id_asso) {
						myForm.id_asso.value=id;
						myForm.submit();
					} else {alert('manque id_asso au formulaire');}
				}

				function goEdit(branch,checked) {
					id=branch.getId();
					window.location.href='".$this->btnEdit."'+id;
					return false;
				}
				
				function editInputs(branch,checked) {
					checkGenBox = ".($this->objSearch->treeParams['checkGenBox']?"'".$this->objSearch->treeParams['checkGenBox']."'":"false").";
				
					id=branch.getId();
					//console.log('id:'+id);
					if(checked){ // passage 0 -> 1 
						new_elem=\$j(\"div.".$this->type_field."_tree_inputs_wrapper div.tree_inputs_empty\").clone();
						\$j(new_elem).find('input.tree_id_item').val(id);
						\$j(new_elem).removeClass('tree_inputs_empty');
						\$j(new_elem).addClass('id_'+id);
						\$j(\"div.".$this->type_field."_tree_inputs_wrapper\").append(new_elem);
						if(checkGenBox && typeof branch.parent != 'undefined' && branch.parent != '' && (typeof branch.parent.isRoot == 'undefined' || !branch.parent.isRoot) && !branch.parent.isChecked()){
							// on est obligé de passer par un click direct sur la checkbox car un appel indirect à editInputs peut ajouter la valeur dans le wrapper d'un autre tree checkbox
							\$j(branch.parent.checkbox).click();
						}
					}else{ //passage 1 -> 0
						\$j(\"div.".$this->type_field."_tree_inputs_wrapper  div.tree_inputs.id_\"+id+\"\").remove();
					}
					
					
					return true;
				}

				function myMouseOver(branch) {
					branch.addClass('mouseover');
				}
				function myMouseOut(branch) {
					branch.removeClass('mouseover');
				}
				function funcDrop (move, drop, response, finished) {
                                    
					if (!finished && response) {
						var obj = eval(response);
						if (!obj.ok) {
						return false;
						}
					}
                                        
                                        if (finished) {";
                                       //B.RAVI 2015-11-30 on rafraîchit l'iframe car sinon quand on drag&drop à l'INTERIEUR d'1 dossier (PAN_DOSSIER=1 ou dans un dossier ayant des enfants), cela n'est pas affiché correctement
                                        echo "window.location.reload();
                                        }
					return true;
				}
				var tree = new TafelTree('".$this->id_div_tree."', struct1, 						{
										'generateBigTree' : true,
										'imgBase' : '".$this->imgBaseUrl."',
										'width' : 0, // valeur par défaut : 100%
										'height':'100%',
										'openAtLoad' : ".($this->openAtLoad?'true':'false').",
										'cookies' : ".($this->useCookies?'true':'false').",
										".(!empty($this->onDropAjax)?"'onDropAjax' : ".$this->onDropAjax.",":"")."
										".(!empty($this->behaviourDrop)?"'behaviourDrop' : ".$this->behaviourDrop.",":"")."										
										'multiline' : false,
										".(!empty($this->onDrop)?"'onDrop' : ".$this->onDrop.",":"")."
										'checkboxes' : ".(( empty($this->btnEdit)&& !($this->checkboxTree))?'false':'true')."
										}
				);"; //Note:si on a un btnEdit, alors on affiche les checkboxes (ie le bouton Edit)
				//by LD 16 12 08 : passage de 800px (forcage ascenseur) à 0(=100%)

				//pour les termes avec style (résultat de recherche, rebond) on récupère les coords du noeud
				foreach ($this->arrNodes as $nd) {
					if ($nd['style']) echo "
					var branch = tree.getBranchById(".$nd['id'].");
                                        if (!(typeof branch === 'undefined' || branch === null)) {
                                            if (typeof(branch)=='object') {var pos = branch.getAbsoluteOffset();}
                                        }
					";
				}
				//et puis on scrolle la fenêtre sur le dernier résultat de la liste
				//(rebond étant prioritaire puisque généré à la fin)
				//Note : on scrolle 100px plus haut pour voir un peu la hiérarchie supérieure
				echo "
				if (typeof(pos)!='undefined') window.scroll(0,pos[1]-100);
				";
				echo "</script>
		";

        $output=ob_get_contents();

        ob_end_clean();

		
		if ($print) echo $output; else return $output;

}

function makeTree($rootid=0) { //analyse du tableau des résultats et création de l'arbre hiérarchique

	//debug($this->arrNodes,'tan');
	//trace(print_r($this->arrNodes,true));
	$tree = Array();
	foreach ($this->arrNodes as $idx=>$item) {
	$id = $item['id'];
	if (!isset($tree[$id])) {
	    /* Il a pu ?tre d?j? cr?? par un de ses enfants,
	     * sinon on le cr?e ici */
	   $tree[$id] = Array();
	}
	$tree[$id]['id'] = $id;
	$tree[$id]['terme'] = $item['terme'];
	$parent = $item['id_pere'];
	if (!isset($tree[$parent])) {
	    /* Il a pu ?tre d?j? cr?? pour lui-m?me, ou par un
	     * autre de ses enfants, sinon on le cr?e ici */
	    $tree[$parent] = Array();
	}
	$tree[$parent][$id] =& $tree[$id];
	}
	foreach ($tree as $idx=>$itm) if ($idx!=$rootid) unset($tree[$idx]);

	//debug($tree,'pink');
	
	$this->createJSON($tree[$rootid]);
}

function createJSON($tree) {
	//trace(print_r($tree,true));
	$serializedPrms=serialize($this->objSearch->treeParams);
	if (!is_array($tree)) return;
	$virgule=false;
	foreach ($tree as $idx=>$branch) {
		//if (!is_numeric($idx)) continue;
		if (!is_array($branch)) return; //by LD 23/07/08, remplacement du return par continue pour aff hiér panier
										  //pour pouvoir continuer si des entités 'nom' (terme, etc) sont placées devant
		$children=false;unset($arrSons);
		foreach ($branch as $u=>$fld) {
			if (is_array($fld)) {$arrSons[$u]=$fld;}
			//debug($arrSons,'plum');
		}
		$element=$this->arrNodes['elt_'.$idx];
		$id=$element["id"];
		// Si le noeud est valide, on lui associe son url et sa checkbox
		if($element["context"]==false){
			//Par défaut, cliquer sur un terme appelle MaJChamp...
			if (empty($this->JSlink)) $this->JSlink = "javascript:void(null)";
			$url_noeud = sprintf($this->JSlink,$this->id_input_result,str_replace('"',"&quot;",$element['terme']),$element['id']);
		} else {
			$url_noeud = "javascript:openNode(".$element['id_orig'].")";
		}
		$terme=$element["terme"];
		// VP 23/02/10 : limitation taille affichage des termes (paramètre termeSize)
		if (!empty($this->termeSize)){
			if(strlen($terme)>$this->termeSize) $terme=substr($terme,0,$this->termeSize)."...";
		}

		if (isset($element['extra'])) $terme.=" (<i>".$element['extra']."</i>)"; //syno
		/*$terme=str_replace (" ' ", '&rsquo;',trim($terme));*/
		$terme=$this->highlight($terme);

		if ($element['asso']) $terme.="<a href='javascript:openNode(".$element['id_orig'].")' ><img align='absmiddle' title='".str_replace("'","\'",kLexiqueHierarchiqueTooltipTermeAssocie)."' border=0 src='".libUrl."/webComponents/tafelTree/imgs/loupe.gif'/></a>";
		//if (!empty($this->btnEdit)) {
		//	$terme="<a href='javascript:goEdit(".$element['id'].")'><img align='absmiddle' border=0 src='".imgUrl."button_modif.gif'/></a>".$terme;
		//	$event="onclick";
		//} else
		$event="onclick";
		$begin = microtime(true);
		//@update VG 24/05/10 : ajout de str_replace
		if (empty($element['nb_children']) && (empty($element['nb_asso']) || $element['nb_asso']<1)) { //pas d'enfant et 0/1 terme asso
			if ($virgule) echo ",";
			echo "{
					'id':'".$id."',
				   'txt':\"".str_replace('"',"&quot;",$terme)."\",
				'onmouseover':myMouseOver,
				'onmouseout':myMouseOut,";
				if($this->checkboxTree){echo "'onbeforecheck':editInputs,";}
				else{ echo "	'onbeforecheck':goEdit,";}	
				if(!$this->checkboxTree){echo "'img':'".($element['icone']?$element['icone']:'page.gif')."',"; }
				echo "	'".$event."':function () {".$url_noeud."}
				";
			if ($element['style']) echo ",'style':'".$element['style']."'";
			if ($this->checkboxTree && in_array($id,$this->arr_values)){ 
				echo ",'check':1";
			}
			if ($arrSons) {
				echo ",'items':[\n";
					$this->createJSON($arrSons);
				echo "\n]";
			}
			echo "}
		"; //noeud simple
			//echo 'fld'.$element["id"].'=gLnk("S","'.$terme.'","'.str_replace('"',"\'",$url_noeud).'");
//';
			//if ($element['icone']) echo 'fld'.$element["id"].'.iconSrc="'.$element["icone"].'"; ';
		}

		 else {
			 if ($virgule) echo ",";
			echo "{
				'id':'".$id."',
				'txt':\"".($terme)."\",
				'open':".($element['open']?'true':'false').",
				'onmouseover':myMouseOver,
				'onmouseout':myMouseOut,";
				if ($this->checkboxTree && in_array($id,$this->arr_values)){
					echo "'check':1,";
				}
				if($this->checkboxTree){echo "'onbeforecheck':editInputs,";}
				else{	echo "	'onbeforecheck':goEdit,";}	
			if (!$this->noAjax) {
			echo " 'onopenpopulate':myOpenPopulate,
				  'openlink':'empty.php?urlaction=ttreeOpenNode&id_input_result=".$this->id_input_result."&jsLink=".urlencode($this->JSlink)."&form_name=".$this->id_input_form."&entity=".$this->objSearch->entity."&treeParams=".$serializedPrms."',";
			}
			echo "	'canhavechildren':true,
				'".$event."':function () {".$url_noeud."}";
			if ($element['style']) echo ",'style':'".$element['style']."'";
			// VP 7/06/2012 : changement notreallyopened.jpg en notreallyopened.gif
			if (!$this->noAjax) echo "
					,'img':'notreallyopened.gif'
					,'notReallyOpened':true";
			else if(!$this->checkboxTree) echo ",'img':'".($element['icone']?$element['icone']:'folder.gif')."'";
			if ($arrSons) {


				echo"	,'items':[\n";
				ob_start();
				$this->createJSON($arrSons);
				$str = ob_get_contents();
				ob_end_clean();
				echo $str;
				echo "\n]";
				if (strpos($str,"'open':true")!==false || strpos($str,"'check':1")!==false){
					echo ",'open':true";
				}
			}
			echo "}";
		   // echo 'fld'.$element["id"].'=gFld("'.$terme.'","'.$url_noeud.'");
//';
		}
		unset($terme);
		$virgule=true;
	}
}


function highlight($str) {
	$key=$this->objSearch->tabHighlight[$this->objSearch->highlightedFieldInHierarchy];
    if (!$key) return $str;
	$key="`(.*?)(".str_replace('%','',$key[0]).")(.*?)`siu"; //on cherche tout en virant le % s'il existe)
	$sortie="$1<b>$2</b>$3";
	$hled=@preg_replace($key,$sortie,$str);
	return $hled;

}

}
?>