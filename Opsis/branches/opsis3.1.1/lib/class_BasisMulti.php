<?php
    
    require_once(libDir.'fonctionsGeneral.php');
    require_once(libDir.'class_Basis.php');
    
    class MultiBasis extends Basis
    {
        //private $server;
        
        // si l'url est vide utiliser une constante ?
        public function __construct($nom_db='')
        {
            parent::__construct($nom_db);
            $this->sql=array();
        }
        
        /*
         arguments Fonctionnement servlet Jdbc : 
         page
         nb_lignes -> fixe a 10 ca m'embete
         highlight
         host
         query
         db
         
         => ajouter getters et setters sur ces parametres (peut etre pas pour tous)
         */
        
        public function select($base,$champs,$where='',$group_by='',$order_by='',$nb_ligne=10,$page=1,$highlight=false)
        {
            if (!empty($base) && !empty($champs) && is_array($champs))
            {
                $i=0;
                
                foreach ($this->database as $base_de_donnees)
                {
                    $this->sql[$i]='SELECT '.implode(',',$champs).' FROM '.trim($base_de_donnees).'.'.trim($base);
                    
                    if (!empty($this->nb_lignes))
                        $this->nb_lignes=intval($nb_ligne);
                    else
                        $this->nb_lignes=10;
					
                    if (!empty($this->page))
                        $this->page=intval($page);
                    else
                        $this->page=1;
                    
                    if (!empty($highlight) && $highlight===true)
                        $this->highlight=true;
                    else
                        $this->highlight=false;
                    
                    
                    if (!empty($where))
                    {
                        $this->sql[$i].=' WHERE '.$where;
                    }
                    
                    if (!empty($group_by))
                    {
                        $this->sql[$i].=' GROUP BY '.$group_by;
                    }
                    
                    if (!empty($order_by))
                    {
                        $this->sql[$i].=' ORDER BY '.$order_by;
                    }
                    
                    $i++;
                }
                
                $this->Execute();
                $this->parseResults();
                return $this->result_sql;
                
            }
            else
                throw new BasisException('base et champs vides');
        }
        
        public function browseIndex($index,$from='',$nb_ligne=10,$page=1,$highlight=false)
        {
            if (!empty($index))
            {
                $i=0;
                foreach($this->database as $base_de_donnees)
                {
                    $this->sql[$i]='BROWSE INDEX OF '.trim($base_de_donnees).'.'.trim($index);
                    
                    if (!empty($from))
                        $this->sql[$i].=' FROM '.$from;
                    
                    if (!empty($this->nb_lignes))
                        $this->nb_lignes=intval($nb_ligne);
                    else
                        $this->nb_lignes=10;
					
                    if (!empty($this->page))
                        $this->page=intval($page);
                    else
                        $this->page=-1;
                    
                    if (!empty($highlight) && $highlight===true)
                        $this->highlight=true;
                    else
                        $this->highlight=false;
                    
                    $i++;
                }
                $this->Execute();
                $this->parseResults();
                return $this->result_sql;
            }
            else
                throw new BasisException('base et champs vides');
        }
        
        
        public function Execute()
        {
            // utilisation du prgm java
            if ($this->highlight)
                $str_highlight='true';
            else
                $str_highlight='false';
            
            $nb_lignes=$this->nb_lignes;
            $num_page=$this->page;
            
            $sql='';
            
            foreach ($this->sql as $requete)
			$sql.=' query="'.str_replace('"','\"',$requete).'"';
            
            if (!defined('kJavaBasis'))
                throw new BasisException('kJavaBasis non defini');
            
            if (!defined('kConfigJavaBasis'))
                throw new BasisException('kConfigJavaBasis non defini');
            
            // on verifie qu'on a bien choisi une base de donnees
            if (empty($this->database))
                throw new BasisException('pas de base donnees selectionee');
            
            //java "-Dfile.encoding=UTF-8" -classpath basisjdbc.jar
            $this->xml_retour_requete='';
            
            $descriptors=array
            (
             0=>array('pipe','r'),
             1=>array('pipe','w'),
             2=>array('pipe','w')
             );
            $pipes=array();
            
			$locale = 'fr_FR.UTF-8';
			setlocale(LC_ALL, $locale);
			putenv('LC_ALL='.$locale);
			
            $cmd=kJavaBasis.' ConnecteurJdbc conf='.kConfigJavaBasis.' db='.implode(',',$this->database).' '.$sql.' nblignes='.$nb_lignes.' page='.$num_page.' highlight='.$str_highlight.'';
            trace('MultiBasis : '.$cmd);
            $proc=proc_open($cmd,$descriptors,$pipes);
            
            $this->xml_retour_requete=stream_get_contents($pipes[1]);
            $stderr=trim(stream_get_contents($pipes[2]));
            fclose($pipes[1]);
            fclose($pipes[2]);
            proc_close($proc);
            
            if (!empty($stderr))
                throw new BasisException('Erreur SQL : '.$stderr);
            //$this->result_sql=''.$this->sql;
            
            return $this->xml_retour_requete;
        }
        
        private function parseResults()
        {
            // parser le XML en array
            $tab_sql=xml2tab($this->xml_retour_requete);
            $tab_sql=$tab_sql['DATAS'][0]['DATA'];
            
            $this->nb_lignes_sql=array();
            $this->result_sql=array();
            
            for ($i=0;$i<count($tab_sql);$i++)
            {
                $this->result_sql[$this->database[$i]]=$tab_sql[$i]['ROW'];
                $this->nb_lignes_sql[$this->database[$i]]=$tab_sql[$i]['NB_LIGNES'];
            }
            /*$this->nb_lignes_sql=$this->result_sql['DATAS'][0]['DATA'][0]['NB_LIGNES'];
             $this->result_sql=$this->result_sql['DATAS'][0]['DATA'][0]['ROW'];*/
        }
    }
    
    ?>