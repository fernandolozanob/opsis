<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once(libDir."class_matInfo.php");

require_once('exception/exception_fileNotFoundException.php');

class JobProcess_ffmpeg extends JobProcess implements Process
{
	//private $process_running;
	//private $
	
	private $nb_frames; // nombre theorique de frames
	private $nb_frames_encodees;

	private $nb_fps_fichier;

	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('ffmpeg');
		$this->nb_frames_encodees=0;
		$this->required_xml_params=array
		(
			'acodec'=>'string'
			
		);
		
		$this->optional_xml_params=array
		(
			'ac'=>'int',
			'ab'=>'string',
			'ar'=>'int',
			'f'=>'string',
			'movflags'=>'string',
			//'faststart'=>'string',
			'aspect'=>'string',
			'aspect_ratio'=>'string',
			'aspect_mode'=>'string',
			'pix_fmt'=>'string',
			'profile'=>'string',
			'vprofile'=> 'string',
			'level'=>'string',
			'g'=>'int',
			'keyint_min'=>'int',
			'vb'=>'string',
			'minrate'=>'string',
			'maxrate'=>'string',
			'bufsize'=>'string',
			'vcodec'=>'string',
			'vf'=>'string',
			'sample_fmt'=>'string',
			'tcin'=>'string',
			'tcout'=>'string',
			'preroll'=>'string',
			'postroll'=>'string',
			'bypass_logo'=>'int',
			'logo'=>'string',
			'logo_dir'=>'string',
			'logo_marge'=>'int',
			'logo_marge_scaled'=>'int',
			'logo_position'=>'string',
			'logo_scale'=>'int',
			's'=>'int',
			's_h'=>'int',
			's_w'=>'int',
			'width'=>'int',
			'height'=>'int',
			'threads'=>'int',
			'async'=>'int',
			'incrust_tc'=>'int',
			'police_tc'=>'string',
			'incrust_tc_in'=>'string',
			'marge_tc'=>'string',
			
			'incrust_text'=>'string',
			'police_text'=>'string',
			'size_text'=>'int',
			
			'final_resize'=>'int',
			'map'=>'string',
			'map_channel'=>'string',
			'type_mapping' => 'string',
			'montage' => 'array',
			'preset'=> 'string'
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
		
		//$this->process_running=Mutex::create();
	}
	
	function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		
		
		//MS est on dans le cas DCP ? 
		if(isset($params_job['dcp']) && !empty($params_job['dcp'])){
			// les 'reels' sont les bobines, ce xml est reconstitué par les fonctions buildDCPMap & buildDPXMap de la class_materiel
			// contient diverses informations, mais principalement les noms des fichiers à associer pour générer le chunk complet (VIDEO + SOUND (+ SUBTITLE))
			$this->reels = $params_job['dcp'][0]['xml'][0]['reel'];
			
			if(isset($params_job['dcp'][0]['xml'][0]['framerate'])){
				// framerate_dcp : ne devrait être disponible que dans le cas des dcp unwrap
				$this->framerate_dcp = $params_job['dcp'][0]['xml'][0]['framerate'];
			}
			if(isset($params_job['dcp'][0]['xml'][0]['diff_path'])){
				//diff_path : path de décalage par rapport au repertoire racine du dcp
				// ne devrait exister que dans le cas des DCP partiels;
				$this->diff_path_dcp= $params_job['dcp'][0]['xml'][0]['diff_path'];
			}
			if(isset($params_job['dcp'][0]['xml'][0]['type_dcp'])){
				$this->type_dcp = $params_job['dcp'][0]['xml'][0]['type_dcp'];
			}
		}
		if(isset($params_job['dpx']) && !empty($params_job['dpx'])){
			$this->dpx_infos = $params_job['dpx'][0]['xml'][0];
		}
		
		if(isset($params_job['hls']) && !empty($params_job['hls'])){
			$this->generation_hls = true ; 
			$this->params_hls = array() ; 
			if(!isset($params_job['main_playlist'])){
				throw new Exception('Erreur encodage HLS : paramètre main_playlist manquant');
			}
			
			// récupération des params d'encodages communs à toutes les qualités
			$common_params = array() ; 
			foreach($params_job as $key=>$val){
				if(!is_array($val)){
					$common_params[$key] = $val;
				}
			}
			// merge des params communs & specifiques pour avoir des listes de params cohérentes pour le reste du process
			foreach($params_job['hls'][0]['qual'] as $quality_params){
				$this->params_hls[] =  array_merge($common_params,$quality_params);
			}
			
			$this->current_hls = 0;
			$this->nb_quals = count($this->params_hls);
			
			$this->tmp_HLS_out_dir=$this->getTmpDirPath()."/".stripExtension(basename($this->getFileOutPath())).'_'.time().'_'.rand(100000,999999); //.getExtension($this->getFileOutPath());
			mkdir($this->tmp_HLS_out_dir,0755);
		}
		if(isset($params_job['montage']) && !empty($params_job['montage'])){
			$this->generation_montage = true ;
			$this->montage_map = $params_job['montage'][0]['clip'];
			// $this->writeJobLog("first montage_map : ".print_r($this->montage_map ,true));
		}
		
		// MS - rétrocompatibilité anciens paramètres  : 
		if(isset($params_job['s_w']) && !isset($params_job['width'])){
			$params_job['width'] = $params_job['s_w'];
			unset($params_job['s_w']);
		}
		if(isset($params_job['s_h']) && !isset($params_job['height'])){
			$params_job['height'] = $params_job['s_h'];
			unset($params_job['s_h']);
		}
		if(isset($params_job['aspect']) && !isset($params_job['aspect_ratio'])){
			$params_job['aspect_ratio'] = $params_job['aspect'];
			unset($params_job['aspect']);
		}
		
		$options_ffmpeg='-y';

		
		// récupération infos fichier in 
		$infos=MatInfo::getMatInfo($this->getFileInPath());
		foreach ($infos['tracks'] as $track)
		{
			if (strtolower($track['type'])=='video' && !isset($video_format) && strtolower($track['format'])!= 'mjpeg')
			{
				$video_format=$track['format'];
				$codec_id=$track['codec_id'];
				$version_format=$track['format_version'];
				$gop_info=$track['format_settings_gop'];
				$video_frame_rate=$track['frame_rate'];
				$video_scan_type=$track['scan_type'];
				$nb_fps=$track['frame_rate'];
				$this->infile_has_videostream = true ;
			}else if (strtolower($track['type']) == 'audio' ){
				$this->infile_has_audiostream = true ; 
			}
		}
		// Cas DPX
		if(isset($this->dpx_infos)){
			// test existence du fichier son
			if(isset($this->dpx_infos['wav_files'][0])){
				$wav_file_path  = ($this->getFileInPath())."/".$this->dpx_infos['wav_files'][0]['rel_path']."/".$this->dpx_infos['wav_files'][0]['file'];
				$this->writeJobLog("wav_file_path : ".$wav_file_path);
				if(file_exists($wav_file_path)){
					$infos_wav=MatInfo::getMatInfo($wav_file_path);
					$duration_dpx = $infos_wav['duration'];
				}
			}
			// recup compte dpx
			if(isset($this->dpx_infos['dpx_files'][0]['cpt_dpx'])){
				$cpt_dpx = $this->dpx_infos['dpx_files'][0]['cpt_dpx'];
			}
			// si on a durée && compte
			if(isset($duration_dpx) && isset($cpt_dpx)){
				// => déduction du framerate 
				$nb_fps_input = intval($cpt_dpx/$duration_dpx);
				$this->writeJobLog("cpt dpx :".$cpt_dpx." duration_dpx : ".$duration_dpx." nb_fps_input ".$nb_fps_input);
			}else if (isset($cpt_dpx)){
				// sinon, default framerate pour dpx => 24, puis on déduit la durée = compte_dpx / 24
				$nb_fps_input = 24 ;
				$duration_dpx = $cpt_dpx/$nb_fps_input; 
			}
			
			
			// path des fichiers dpx 
			$dpx_path  = ($this->getFileInPath())."/".$this->dpx_infos['dpx_files'][0]['rel_path']."/";
			// nom du premier fichier
			$first_dpx_filename = $this->dpx_infos['dpx_files'][0]['first'];
			// extension des fichiers image composant le DPX
			$dpx_extension = getExtension($first_dpx_filename);
			// racine des fichiers images
			$dpx_name = substr(stripExtension($first_dpx_filename),0,strrpos($first_dpx_filename,'_')+1);
			// recupération du pattern de numérotation
			$dpx_pattern = substr(stripExtension($first_dpx_filename),strrpos($first_dpx_filename,'_')+1);
			// récupération du point de départ de la numérotation
			$dpx_start = intval($dpx_pattern);
			// nombre de caractères utilisés pour la numérotation
			$dpx_pattern = strlen($dpx_pattern);
			// $this->writeJobLog($first_dpx_filename." name: ".$dpx_name. "num_pattern : ".$dpx_pattern." start : ".$dpx_start." duration_dpx : ".$duration_dpx." framerate dpx: ".$nb_fps);
			//récupération taille de la première image DPX pour déterminer le ratio à utiliser pour le fichier de visionnage 
			try{
				$dpx_infos = MatInfo::getMatInfo($dpx_path."/".$first_dpx_filename);
				$infos['display_width'] = $dpx_infos['width'];
				$infos['display_height'] = $dpx_infos['height'];
				$infos['display_aspect_ratio'] = $dpx_infos['display_aspect_ratio'];
			}catch(Exception $e){
				$this->writeJobLog("jobProcess FFMPEG - echec récupération taille & ratio dpx ");
			}
		}
		// DCP simples & unwrap => récupération de la taille des fichiers sources pour déduire l'aspect ratio du format de visionnage
		if(isset($this->reels) && !empty($this->reels)){
			if(isset($this->diff_path_dcp) && !empty($this->diff_path_dcp) && $this->diff_path_dcp !='/' ){
				$dir_input = str_replace($this->diff_path_dcp,'',dirname($this->getFileInPath()).'/');
			}else{
				$dir_input = dirname($this->getFileInPath());
			}
			
			foreach($this->reels as $reel){
				if($this->type_dcp == 'CPL_unwrap'){
					if(is_array($reel['video']) && isset($reel['video'][0]['path'])){
						// path des fichiers dcp_unwrp 
						$dcp_unwrp_path  = $dir_input."/".dirname($reel['video'][0]['path'])."/";
						// nom du premier fichier
						$first_dcp_unwrp_filename = basename($reel['video'][0]['path']);
						try{
							$dcp_unwrp_infos = MatInfo::getMatInfo($dcp_unwrp_path."/".$first_dcp_unwrp_filename);
							$infos['display_width'] = $dcp_unwrp_infos['width'];
							$infos['display_height'] = $dcp_unwrp_infos['height'];
							$infos['display_aspect_ratio'] = $dcp_unwrp_infos['display_aspect_ratio'];
						}catch(Exception $e){
							$this->writeJobLog("jobProcess FFMPEG - echec récupération taille & ratio dcp unwrap ");
						}
						break ; 
					}
				}else{
					try{
						$dcp_infos = MatInfo::getMatInfo($dir_input."/".$reel['video']);
						$infos['display_width'] = $dcp_infos['width'];
						$infos['display_height'] = $dcp_infos['height'];
						$infos['display_aspect_ratio'] = $dcp_infos['display_aspect_ratio'];
					}catch(Exception $e){
						$this->writeJobLog("jobProcess FFMPEG - echec récupération taille & ratio dcp ");
					}
					break ; 
				}
			}
		}
		
		// Cas montage 
		// premiere passe sur la montage_map, on va générer les cartons, vérifier l'existence de streams audios 
		if (isset($this->generation_montage)){
			foreach($this->montage_map as &$clip){
				//$this->writeJobLog('clip : '.print_r($clip,true));
				if($clip['type'] == 'carton' || ( $clip['type'] == 'doc' && $clip['mediatype'] == 'picture' )){
					$this->genCarton($clip);
					$clip['has_audio_stream'] = 1;
				}else if ($clip['type'] == 'doc'){
					$clip['infos'] = MatInfo::getMatInfo($clip['file']);
					$has_audio = false ; 
					foreach($clip['infos']['tracks'] as $track){
						if(isset($track['type']) && trim(strtolower($track['type'])) == 'audio'){
							$has_audio = true ; 
							break ; 
						}
					}
					unset($clip['infos']);
					$clip['has_audio_stream'] = $has_audio;
				}
			}
		}
		unset($clip);
		
		if(!isset($nb_fps)) $nb_fps=25;
		if(isset($duration_dpx)){
			$duree_max= $duration_dpx;
		}else{
			$duree_max=$infos['duration'];
		}
		
		
		$gop=$this->getGopSize($gop_info, $video_format, $infos['format_calc']);


		// HLS => appel avec les opts de chaque qual indépendamment
		if(isset($this->generation_hls) && $this->generation_hls){
			foreach($this->params_hls as $idx=>$params){
				$options_sst = $this->calc_TimeOpts_Fps($params,$duree_max,$nb_fps);
				$this->params_hls[$idx]['options_sst'] = $options_sst;
				$this->params_hls[$idx]['nb_frames'] = $this->nb_frames;
				unset($options_sst);
			}
			$this->nb_frames = $this->params_hls[0]['nb_frames'];
		}else{
			$options_sst = $this->calc_TimeOpts_Fps($params_job,$duree_max,$nb_fps);
		}
		
		if(isset($params_job['threads']) && !empty($params_job['threads'])){
			$str_threads1 = " -threads 1 ";
			$str_threads2 = " -threads ".(intval($params_job['threads'])-1)." ";
		}else{
			$str_threads1 = "" ; 
			$str_threads2 = "" ; 
		}
		
		if(isset($params_job['dcp']) && isset($this->reels) && !empty($this->reels)){
		//DCP
			$options_ffmpeg.=$str_threads1.' <dcp_file_input> '; 
		}else if(isset($params_job['dpx']) && isset($first_dpx_filename) && !empty($first_dpx_filename) ){
		//DPX
			$options_ffmpeg.=$str_threads1.' '.(in_array(strtolower($dpx_extension),array('jpg','j2c'))?'-c:v libopenjpeg':'').' -r '.$nb_fps_input." -start_number ".$dpx_start." -i ".$dpx_path.$dpx_name."%".sprintf("%02s", $dpx_pattern)."d.".$dpx_extension; 
			if(isset($wav_file_path) && file_exists($wav_file_path)){
				$options_ffmpeg.=' -i '.$wav_file_path;
			}
			// $options_ffmpeg.=$options_sst;
			
		}else if (isset($this->generation_hls) && $this->generation_hls){
			$tmp_opts_ffmpg = $options_ffmpeg;
			foreach($this->params_hls as $idx=>$params){
				if($gop == 1 ){
					$this->params_hls[$idx]['options_ffmpeg'] = $str_threads1.$tmp_opts_ffmpg." ".$this->params_hls[$idx]['options_sst']." -i ".escapeQuoteShell($this->getFileInPath());
				}else{
					$this->params_hls[$idx]['options_ffmpeg'] = $tmp_opts_ffmpg.$str_threads1."  -i ".escapeQuoteShell($this->getFileInPath())." ".$this->params_hls[$idx]['options_sst'];
				}
			}
		}else if(isset($this->generation_montage) && $this->generation_montage){
			$options_ffmpeg.=$str_threads1 ; 
		}else if($gop==1)
			$options_ffmpeg.=$str_threads1.$options_sst.' -i '.escapeQuoteShell($this->getFileInPath());
		else
			$options_ffmpeg.=$str_threads1.' -i '.escapeQuoteShell($this->getFileInPath()).$options_sst;

			
			
		// HLS => appel avec les opts de chaque qual indépendamment
		if(isset($this->generation_hls) && $this->generation_hls){
			foreach($this->params_hls as $idx=>$params){
				$chaine_vfilters = $this->calc_vfilters($params,$infos);
				$this->params_hls[$idx]['vf'] = $chaine_vfilters;
				$this->writejoblog("hls ".$idx." vf : ".$chaine_vfilters);
				unset($chaine_vfilters);
			}
		}else{
			$chaine_vfilters = $this->calc_vfilters($params_job,$infos);
			if(!empty($chaine_vfilters))
				$params_job['vf'] = $chaine_vfilters;
		}
		
		
		// VP 4/09/2014 : paramètre threads pour libx264 avec x264opts
		// MS - 17.07.17 - réactivation code spécifique x264opts 
		// + retrait du "unset" sur le param threads général, la précision des threads dans les x264opts est ajoutée, mais ne remplace pas le paramètre original. 
		// A terme il faudrait éventuellement ajouter trois types de param threads pour du fine tuning  + un systeme de fallback entre ces options. 
		if($params_job['vcodec']=='libx264' && isset($params_job['threads'])){
			$params_job['x264opts']='threads='.$params_job['threads'];
		}

		// VP 10/04/2013 : ajout param?tres 'bf' et 'b_strategy'
		$ffmpeg_param=array
		(
			'f',
			'movflags',
			'vcodec',
			'x264opts',
			'profile',
			'level',
			'g',
			'keyint_min',
			'vprofile',
			'preset',
			'vb',
			'minrate',
			'maxrate',
			'bufsize',
			'r',
			'acodec',
			'ac',
			'ab',
			'ar',
			'async',
			'vpre',
			'croptop',
			'cropbottom',
			'cropleft',
			'cropright',
			'aspect',
			'bt',
			'strict',
			'fpre',
			'vf',
			'filter_complex',
			'pix_fmt',
			'sample_fmt',
			'bf',
			'b_strategy',
			'map'
		);
		
		
		
		if(isset($params_job['fpre']) && !empty($params_job['fpre']))
			$params_job['fpre']=kFFMPEGPresetPath."/".$params_job['fpre'];
		
		if(isset($this->reels) && !empty($this->reels)){
			$options_ffmpeg_ori = $options_ffmpeg;
			// nb segments => pour l'instant un encodage par chunk + une concaténation
			$segments = count($this->reels)+1;
			$this->global_nb_frames = 0 ; 
			$this->global_frames_encodees = 0 ; 
		}else if (isset($this->generation_hls) && $this->generation_hls){
			$segments = count($this->params_hls)*2;
			$this->global_nb_frames = 0 ; 
			$this->global_frames_encodees = 0 ; 
		}
		
		$idx_dcp = 0;
		$idx_hls = 0;
		$this->writeJobLog(print_r($this->type_dcp,true));
		// ================================ BOUCLE PRINCIPALE ============================ // 
		do{
			if(isset($this->reels) && !empty($this->reels)){
				// *************** SPECIFIQUE ENCODAGE DCP ( SIMPLES / PARTIELS / UNWRAP )************* //
				$options_ffmpeg = $options_ffmpeg_ori;
				$tmp_file=$this->getTmpDirPath()."/".$idx_dcp."_".stripExtension(basename($this->getFileOutPath())).'_'.time().'_'.rand(100000,999999).'.'.getExtension($this->getFileOutPath());
				$this->reels[$idx_dcp]['tmp_file'] = $tmp_file;
				$file_input = "";
				$subtitle_file="";
				if(isset($this->diff_path_dcp) && !empty($this->diff_path_dcp) && $this->diff_path_dcp !='/' ){
					$dir_input = str_replace($this->diff_path_dcp,'',dirname($this->getFileInPath()).'/');
				}else{
					$dir_input = dirname($this->getFileInPath());
				}
				if(isset($this->reels[$idx_dcp]) && isset($this->reels[$idx_dcp]['video'])){
					if($this->type_dcp == 'CPL_unwrap'){
						if(is_array($this->reels[$idx_dcp]['video']) && isset($this->reels[$idx_dcp]['video'][0]['path'])){
							// path des fichiers dcp_unwrp 
							$dcp_unwrp_path  = $dir_input."/".dirname($this->reels[$idx_dcp]['video'][0]['path'])."/";
							// nom du premier fichier
							$first_dcp_unwrp_filename = basename($this->reels[$idx_dcp]['video'][0]['path']);
							// extension des fichiers image composant le dcp_unwrp
							$dcp_unwrp_extension = getExtension($first_dcp_unwrp_filename);
							// racine des fichiers images
							$dcp_unwrp_name = substr(stripExtension($first_dcp_unwrp_filename),0,strrpos($first_dcp_unwrp_filename,'_')+1);
							// recupération du pattern de numérotation
							$dcp_unwrp_pattern = substr(stripExtension($first_dcp_unwrp_filename),strrpos($first_dcp_unwrp_filename,'_')+1);
							// récupération du point de départ de la numérotation
							$dcp_unwrp_start = intval($dcp_unwrp_pattern);
							// nombre de caractères utilisés pour la numérotation
							$dcp_unwrp_pattern = strlen($dcp_unwrp_pattern);
							// framerate : 
							$nb_fps_input = $this->framerate_dcp;
							$file_input .=' '.(in_array(strtolower($dcp_unwrp_extension),array('jpg','j2c'))?'-c:v libopenjpeg':'').' -r '.$nb_fps_input." -start_number ".$dcp_unwrp_start." -i ".$dcp_unwrp_path.$dcp_unwrp_name."%".sprintf("%02s", $dcp_unwrp_pattern)."d.".$dcp_unwrp_extension; 
							
							$duree_max =  $this->reels[$idx_dcp]['video'][0]['cpt'] / $nb_fps_input;
							
								$this->writeJobLog("nb_fps_input : ".$nb_fps_input." nb_fps_final : ".$this->nb_fps_fichier." duree max : ".$duree_max);
							if (isset($params_job['t']) && !empty($params_job['t']))
							{
								if ((timeToSec($params_job['ss'])+$params_job['t'])>$duree_max){
									$this->nb_frames=floor(($duree_max-timeToSec($params_job['ss']))*$this->nb_fps_fichier);
								}else{
									$this->nb_frames=floor($params_job['t']*$this->nb_fps_fichier);
								}
							}
							else{
								$this->nb_frames=floor($duree_max*$this->nb_fps_fichier);
							}
							$this->global_nb_frames +=$this->nb_frames;
							
						
						}
					}else{
						// if($gop == 1 ){$file_input.=" ".$options_sst;}
						//VP 19/11/2014 : ajout -c:v libopenjpeg
						//$file_input .=" -i ".escapeQuoteShell($dir_input.'/'.$this->reels[$idx_dcp]['video']);
						$file_input .=" -c:v libopenjpeg -i ".escapeQuoteShell($dir_input.'/'.$this->reels[$idx_dcp]['video']);
						// if($gop != 1 ){$file_input.=" ".$options_sst;}
						
						$this->nb_frames_encodees=0;
						$infos_chunk=MatInfo::getMatInfo($dir_input.'/'.$this->reels[$idx_dcp]['video']);
						$duree_max = $infos_chunk['duration'];
						// $this->writeJobLog("chunk ".$idx_dcp." duree : ".$duree_max);
						unset($infos_chunk);
						if (isset($params_job['t']) && !empty($params_job['t']))
						{
							if ((timeToSec($params_job['ss'])+$params_job['t'])>$duree_max){
								$this->nb_frames=floor(($duree_max-timeToSec($params_job['ss']))*$this->nb_fps_fichier);
							}else{
								$this->nb_frames=floor($params_job['t']*$this->nb_fps_fichier);
							}
						}
						else{
							$this->nb_frames=floor($duree_max*$this->nb_fps_fichier);
						}
						$this->global_nb_frames +=$this->nb_frames;
					}
					// $this->writeJobLog("chunk ".$idx_dcp." nb frmaes".$this->nb_frames);
				}
				// si fichier son défini dans $this->reels
				if(isset($this->reels[$idx_dcp]) && isset($this->reels[$idx_dcp]['sound'])){
					if(is_array($this->reels[$idx_dcp]['sound'])){
						$f_complex_audio = '"' ; 
						foreach($this->reels[$idx_dcp]['sound'] as $idx=>$sound){
							// $file_input.=" ".$options_sst;
							$file_input .=" -i ".escapeQuoteShell($dir_input.'/'.$sound['filename']);
							$f_complex_audio .= "[".intval($idx+1).":a]";
						}
						$f_complex_audio.='amerge=inputs='.count($this->reels[$idx_dcp]['sound']).',volume=2[aout]" -map "[aout]"';
					}else{
						// if($gop == 1 ){$file_input.=" ".$options_sst;}
						$file_input .=" -i ".escapeQuoteShell($dir_input.'/'.$this->reels[$idx_dcp]['sound']);
						// if($gop != 1 ){$file_input.=" ".$options_sst;}
					}
				}
				// si fichier sous titre dans $this->reels
				if(isset($this->reels[$idx_dcp]) && isset($this->reels[$idx_dcp]['subtitle'])){
					$subtitle_file =$dir_input.'/'.$this->reels[$idx_dcp]['subtitle'];
					$subtitle_srt = $this->getTmpDirPath()."/".stripExtension(basename($subtitle_file))."_".rand(100000,999999).".srt";
					$ok = DCSub2srt($subtitle_file,$subtitle_srt);
				}
				
				// cas dcp unwrap;
				if(is_array($this->reels[$idx_dcp]['video'])){
					$file_input.=' -map 0:0 ';
				}
				$options_ffmpeg = str_replace('<dcp_file_input>',$file_input,$options_ffmpeg);
				
				
				foreach($ffmpeg_param as $param)
				{
					if (isset($params_job[$param]) && !empty($params_job[$param]))
					{
						if (($param=='b' || $param=='ab') && strpos($params_job[$param],'k') === false){
							$options_ffmpeg.=' -'.$param.' '.$params_job[$param].'k';
						}else if ($param=='vf'){
							if(!empty($subtitle_srt)){
								$options_ffmpeg.=' -'.$param.' "'.$params_job[$param].',subtitles='.$subtitle_srt.'"';
							}else if (empty($subtitle_srt)){
								$options_ffmpeg.=' -'.$param.' "'.$params_job[$param].'"';
							}
							if(isset($f_complex_audio) && !empty($f_complex_audio)){
								$options_ffmpeg.= ' -filter_complex '.$f_complex_audio;
							}
						}else if ($param=='acodec' && $params_job[$param]=='none'){
							$options_ffmpeg.=' -an';
						}else{
							$options_ffmpeg.=' -'.$param.' '.$params_job[$param];
						}
					}
				}
				$options_ffmpeg.=$str_threads2.' '.escapeQuoteShell($tmp_file).'';				
			}else if (isset($this->generation_hls) && $this->generation_hls){
				// *************** SPECIFIQUE ENCODAGE HLS************* //
				$this->params_hls[$idx_hls]['qual_dirpath'] = $this->tmp_HLS_out_dir."/Q".($idx_hls+1)."/";
				$this->params_hls[$idx_hls]['qual_filename'] = "Q".($idx_hls+1).".mp4";
				mkdir($this->params_hls[$idx_hls]['qual_dirpath'],0755);
				$this->writeJobLog("hls ".$idx_hls." ".$this->params_hls[$idx_hls]['qual_dirpath']);
				
				$this->nb_frames = $this->params_hls[$idx_hls]['nb_frames'];
				
				// HLS -> on modifie options_ffmpeg en fonction de la qualité courante
				foreach($ffmpeg_param as $param)
				{
					if (isset($this->params_hls[$idx_hls][$param]) && !empty($this->params_hls[$idx_hls][$param]))
					{
						if ($param=='vb' || $param=='ab')
							$this->params_hls[$idx_hls]['options_ffmpeg'].=' -'.$param.' '.$this->params_hls[$idx_hls][$param].'k';
						else if ($param=='vf')
							$this->params_hls[$idx_hls]['options_ffmpeg'].=' -'.$param.' "'.$this->params_hls[$idx_hls][$param].'"';
						else
							$this->params_hls[$idx_hls]['options_ffmpeg'].=' -'.$param.' '.$this->params_hls[$idx_hls][$param];
					}
					$options_ffmpeg = $this->params_hls[$idx_hls]['options_ffmpeg'];
					// mise en place d'un nom temporaire
					// le fichier de sortie est un fichier temporaire. le fichhier est renomme apres traitement.
					// le fichier temporaire est supprimé en cas d'annulation ou de suppression.
					$options_ffmpeg.=$str_threads2.' '.escapeQuoteShell($this->params_hls[$idx_hls]['qual_dirpath']."/".$this->params_hls[$idx_hls]['qual_filename']);
					file_put_contents($this->getPidFilePath(),"\n".'qual_dirpath '.($idx_hls+1).'='.$this->params_hls[$idx_hls]['qual_dirpath'],FILE_APPEND);
				}
			}else if (isset($this->generation_montage ) && $this->generation_montage == true){
				// *************** SPECIFIQUE ENCODAGE MONTAGE(PIVOT) ************* //
				$input_files = "" ;
				$filter_complex = "" ;
				$end_map_v = "" ;
				$end_map_a= "" ;
				$index_input = 0 ; 
				// MS - 26.09.16 - Construction de la chaine de montage avec le paramètre filter_complex, 
				// chaque stream video est ramené à une résolution commune (via la chaine vfilter/vf habituelle), reçoit de nouvelles valeurs SAR/DAR en fonction du paramètre d'aspect ratio(setsar,setdar), et est découpée si on a besoin d'une partie d'un fichier et non du fichier entier (trim)
				// les streams audios sont également tronqués si on a besoin d'une partie du fichier (atrim), si une vidéo n'a pas de piste audio on en crée une vide à la place (anullsrc). 
				// Les pistes sont ensuites concaténées via le filtre concat
				foreach($this->montage_map as $idx=>$clip){

					if(isset($clip['file']) && !empty($clip['file'])){
						$input_files.= "-i ".escapeQuoteShell($clip['file'])." ";
						
						$filter_complex .= '['.$index_input.':v]trim='.tcToSecDec($clip['file_tcin']).':'.tcToSecDec($clip['file_tcout']).',setpts=PTS-STARTPTS';
						// MS - A ce jour l'encodage pivot pour la concaténation des clips du montage est l'encodage ProResHQ. Après discussion avec vincent, pour cet encodage on peut se permettre de set le DAR et le SAR à la même valeur, soit à la valeur du parametre aspect_ratio 
						$filter_complex .= ','.$params_job['vf'].',setsar=sar='.str_replace(':','/',$params_job['aspect_ratio']).',setdar=dar='.str_replace(':','/',$params_job['aspect_ratio']);
						if($clip['logo']){
							$chaine_logo = $this->calc_logo($clip, "mov");
							if(!empty($chaine_logo)){
								$filter_complex .= ' [mov],'.$chaine_logo;
							}
						}
						$filter_complex .= '[v'.$index_input.'];';
						$end_map.='[v'.$index_input.'] ';
						
						if($clip['has_audio_stream']){
							$filter_complex .= '['.$index_input.':a]atrim='.tcToSecDec($clip['file_tcin']).':'.tcToSecDec($clip['file_tcout']).',asetpts=PTS-STARTPTS';
							$filter_complex .= '[a'.$index_input.'];';
						}else{
							$filter_complex .= 'anullsrc,atrim=duration=0.04';
							$filter_complex .= '[a'.$index_input.'];';
						}
						$end_map.='[a'.$index_input.'] ';
						
						$index_input++ ; 
					}
				}
				$options_ffmpeg .= " ".$input_files." -filter_complex '".$filter_complex." ".$end_map." concat=n=".($index_input).":v=1:a=1 [outv][outa]' -map '[outv]' -map '[outa]' ";
				
				
				foreach($ffmpeg_param as $param)
				{
					if (isset($params_job[$param]) && !empty($params_job[$param]))
					{
						if (($param=='b' || $param=='vb' || $param=='ab') && strpos($params_job[$param],'k') === false){
							$options_ffmpeg.=' -'.$param.' '.$params_job[$param].'k';
						}else if ($param == 'map' || $param=='vf' || $param=='filter_complex'){
							continue;
						}else if ($param=='acodec' && $params_job[$param]=='none'){
							$options_ffmpeg.=' -an';
						}else{
							$options_ffmpeg.=' -'.$param.' '.$params_job[$param];
						}					
					}
				}
				
				// mise en place d'un nom temporaire
				// le fichier de sortie est un fichier temporaire. le fichier est renomme apres traitement.
				// le fichier temporaire est supprimé en cas d'annulation ou de suppression.
				$tmp_file=stripExtension($this->getFileOutPath()).'_'.time().'_'.rand(100000,999999).'.'.getExtension($this->getFileOutPath());
				$options_ffmpeg.=$str_threads2.' '.escapeQuoteShell($tmp_file).'';
				file_put_contents($this->getPidFilePath(),"\n".'tmp_file='.$tmp_file,FILE_APPEND);
			
			
			}else{
				
				// *************** ENCODAGE STANDARD ******************* //
				$chaine_filter_complex = $this->calc_filter_complex($params_job,$infos);
				if(!empty($chaine_filter_complex))
					$params_job['filter_complex'] = $chaine_filter_complex;
				
				$chaine_map_channel = $this->calc_map_channel($params_job,$infos);
				if(!empty($chaine_map_channel)) {
					$options_ffmpeg .= $chaine_map_channel;
					unset($params_job['map_channel']);
				}
				
				foreach($ffmpeg_param as $param)
				{
					if (isset($params_job[$param]) && !empty($params_job[$param]))
					{
						if (($param=='b' || $param=='vb' || $param=='ab') && strpos($params_job[$param],'k') === false){
							$options_ffmpeg.=' -'.$param.' '.$params_job[$param].'k';
						}else if ($param=='vf' || $param=='filter_complex'){
							$options_ffmpeg.=' -'.$param.' "'.$params_job[$param].'"';
						}else if ($param=='acodec' && $params_job[$param]=='none'){
							$options_ffmpeg.=' -an';
						}else{
							$options_ffmpeg.=' -'.$param.' '.$params_job[$param];
						}					
					}
				}
				
				
				// mise en place d'un nom temporaire
				// le fichier de sortie est un fichier temporaire. le fichhier est renomme apres traitement.
				// le fichier temporaire est supprimé en cas d'annulation ou de suppression.
				$tmp_file=stripExtension($this->getFileOutPath()).'_'.time().'_'.rand(100000,999999).'.'.getExtension($this->getFileOutPath());
				$options_ffmpeg.=$str_threads2.' '.escapeQuoteShell($tmp_file).'';
				file_put_contents($this->getPidFilePath(),"\n".'tmp_file='.$tmp_file,FILE_APPEND);
			}
			
			
			
			$this->shellExecute(kFFMPEGpath,$options_ffmpeg,false);
			
			do
			{
				sleep(5);
				if(isset($segments) && isset($idx_dcp) && isset($this->reels)){
					$this->updateProgress($segments,$idx_dcp);
				}if(isset($segments) && isset($idx_hls) && isset($this->params_hls)){
					$this->updateProgress($segments,$idx_hls);
				}else{
					$this->updateProgress();
				}
			}
			while($this->checkIfFfmpegRunning());
			if(isset($segments)&& isset($idx_dcp) && isset($this->reels)){
				$this->updateProgress($segments,$idx_dcp);
				$this->global_frames_encodees+=$this->nb_frames;
			}elseif(isset($segments)&& isset($idx_hls) && isset($this->params_hls)){
				$this->updateProgress($segments,$idx_hls);
				$this->global_frames_encodees+=$this->nb_frames;
			}else{
				$this->updateProgress();
			}
			if(isset($subtitle_srt) && !empty($subtitle_srt)){
				unlink($subtitle_srt);
				unset($subtitle_srt);
			}
			if (((isset($this->reels) && !empty($this->reels))|| 
				(isset($this->generation_hls) && !empty($this->params_hls)) ) && $this->nb_frames_encodees<($this->nb_frames-250))
			{
				throw new Exception('Erreur encodage chunk '.$idx_dcp.'/'.count($this->reels).' ffmpeg (nb frame encodees :'.$this->nb_frames_encodees.', nb frames attendues :  '.$this->nb_frames.')');
			}
				
			
			$idx_dcp++;
			$idx_hls++;
		}while((isset($this->reels) && !empty($this->reels) && is_array($this->reels) && $idx_dcp < count($this->reels))
		|| (isset($this->generation_hls) && !empty($this->params_hls) && is_array($this->params_hls) && $idx_hls < count($this->params_hls)));
		
		
		// =====================POST TRAITEMENT ===================================
		// ICI, si DCP => concaténation  
		if(isset($this->reels) && !empty($this->reels)){
			if(count($this->reels)>1){
				$this->writeJobLog("concaténation");
				$file_inputs = '';
				$mapping = '';
				foreach($this->reels as $idx=>$reel){
					if(!isset($reel['tmp_file']) || !file_exists($reel['tmp_file'])){
						throw new Exception('Erreur encodage chunks '.(isset($reel['tmp_file'])?', fichier '.$reel['tmp_file']." n'existe pas":''));
					}else{
						$file_inputs .= " -i ".$reel['tmp_file'];
						$mapping .= "[".$idx.":0][".$idx.":1]";
					}
				}
				
				$this->nb_frames = $this->global_nb_frames;
				$this->writeJobLog("concaténation , nb frmes : ".$this->nb_frames);
				$opts_concat = $file_inputs." -filter_complex '".$mapping." concat=n=".count($this->reels).":v=1:a=1 [v][a]' -map '[v]' -map '[a]' ";
				
				foreach($ffmpeg_param as $param)
				{
					if (isset($params_job[$param]) && !empty($params_job[$param]) && !in_array($param,array('vf','t','ss')))
					{
						if ($param=='vb' || $param=='ab')
							$opts_concat.=' -'.$param.' '.$params_job[$param].'k';
						else
							$opts_concat.=' -'.$param.' '.$params_job[$param];
					}
				}
				
				$tmp_file=$this->getTmpDirPath().'/'.stripExtension(basename($this->getFileOutPath())).'_'.time().'_'.rand(100000,999999).'.'.getExtension(basename($this->getFileOutPath()));
				$opts_concat.=" ".$tmp_file;
				$this->shellExecute(kFFMPEGpath,$opts_concat,false);
				do
				{
					sleep(5);
					$this->updateProgress($segments,intval($segments-1));
				}
				while($this->checkIfFfmpegRunning());
				$this->updateProgress($segments,intval($segments-1));
				// clear tmp_file
				foreach($this->reels as $idx=>$reel){
					unlink($reel['tmp_file']);
				}
			}
		}
		
		// ICI, si HLS => découpage de chaque qualité QN.mp4 en chunk .ts
		if(isset($this->generation_hls) && !empty($this->params_hls)){
			foreach($this->params_hls as $idx=>$param_qual){
				if(!is_dir($param_qual['qual_dirpath'])){
					$this->writeJobLog("Erreur segmentation HLS ".$idx." le répertoire qual_dirpath n'existe pas (".$param_qual['qual_dirpath'].")");
					throw new Exception("Erreur segmentation HLS ".$idx." le répertoire temporaire Q".$idx." n'existe pas");
				}
				// chdir($param_qual['qual_dirpath']);
				// $ret = shell_exec('pwd');
				// $this->shellExecute(,false);
				$options_seg = " -i ./".$param_qual['qual_filename']." -max_delay 50000 -map 0:v ".(isset($this->infile_has_audiostream) && $this->infile_has_audiostream?"-map 0:a":"")." -c copy -flags:v +global_header -bsf:v h264_mp4toannexb -f segment -segment_time 9  -segment_list prog_index.m3u8 -segment_format mpegts segment_%05d.ts";
				// MS 27.05.15 - Si on passe par un serveur de charge, et donc par des connexions ssh, on ne peut pas faire le change dir + la commande ffmpeg dans un même temps, on doit forcément le faire en une commande, 
				// d'ou la commande suivante, peu élégante : 
				$this->shellExecute("cd",$param_qual['qual_dirpath'].";".kFFMPEGpath." ".$options_seg,false);
				do
				{
					sleep(5);
					// $this->updateProgress($segments,$idx_hls);
				}
				while($this->checkIfFfmpegRunning());
				
				if(!is_file($param_qual['qual_dirpath']."/prog_index.m3u8")){
					$this->writeJobLog("Erreur segmentation HLS Q".$idx." : le fichier prog_index.m3u8 n'a pas été généré (".$param_qual['qual_dirpath'].")");
					throw new Exception("Erreur segmentation HLS Q".$idx." : le fichier prog_index.m3u8 n'a pas été généré");
				}
				$this->setProgression(($idx_hls*(100/$segments))+(100)/$segments);
				$this->writejoblog("seg progression : ".(($idx_hls*(100/$segments))+(100)/$segments));
				unlink($param_qual['qual_dirpath']."/".$param_qual['qual_filename']);
				$idx_hls++;
			}
			
			if(!is_file(jobSettingDir."/".$params_job['main_playlist'])){
				$this->writeJobLog("Erreur segmentation HLS  : le fichier playlist principale n'a pas été trouvé (".jobSettingDir."/".$this->params_job['main_playlist'].")");
				throw new Exception("Erreur segmentation HLS  : le fichier playlist principale n'a pas été trouvé");
			}
			copy(jobSettingDir."/".$params_job['main_playlist'],$this->tmp_HLS_out_dir."/".basename($this->getFileOutPath()).".m3u8");
			
			$tmp_file = $this->tmp_HLS_out_dir;
		}
		
		
		if ($this->nb_frames_encodees<($this->nb_frames-1500))
		{
			$data_ffprobe = MatInfo::getFFprobe($tmp_file);
			$duree_out = $data_ffprobe['duration'];
			$nb_frames_out = $duree_out*$nb_fps_final;
			if($nb_frames_out<($this->nb_frames-1500)){
				//throw new Exception('Erreur encodage ffmpeg (commande : '.kFFMPEGpath.' '.$options_ffmpeg.')');
				throw new Exception('Erreur encodage ffmpeg (nb frame encodees :'.$this->nb_frames_encodees.', nb frames attendues :  '.$this->nb_frames.')');
			}
		}

		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
		mv_rename($tmp_file,$this->getFileOutPath());
		$this->writeJobLog("rename final : ".$tmp_file." =>".$this->getFileOutPath());
	}
	
	public function updateProgress($segment=1,$current=0)
	{
		$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
		$donnees_stderr=str_replace("\r","\n",$donnees_stderr);
		
		$params_job=$this->getXmlParams();
		
		if (isset($params_job['vcodec']) && !empty($params_job['vcodec']))
		{
			preg_match_all('/frame=([ 0-9]+) fps=([ 0-9.]+) q=([- 0-9.]+) (Lsize|size)=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
			$this->nb_frames_encodees=intval($infos_encodage[1][count($infos_encodage[1])-1]);
		}
		else
		{
			// CAS fichier audio (pas de codec video)
			// pour les fichiers audio la sortie est : size=	5420kB time=00:03:18.17 bitrate= 224.0kbits/s
			preg_match_all('/size=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
			$nb_fps_encod=timeToSec($infos_encodage[2][count($infos_encodage[2])-1])*$this->nb_fps_fichier;
			$this->nb_frames_encodees=intval($nb_fps_encod);
		}
		//$this->writeJobLog("updateProgress - nb_frames_encodees : ".$this->nb_frames_encodees."/".$this->nb_frames);

		$this->writeOutLog($this->nb_frames_encodees.' / '.$this->nb_frames);
		$ratio=$this->nb_frames_encodees/$this->nb_frames;
		
		if ($ratio>1)
			$ratio=1;
		
		if($segment>1){
			$this->setProgression(($current*(100/$segment))+($ratio*100)/$segment);
		}else{
			$this->setProgression($ratio*100);
		}
	}
	
	
	function calc_TimeOpts_Fps(&$params_job,$duree_max,$nb_fps){
		if (isset($params_job['r']) && !empty($params_job['r']))
			$nb_fps_final=$params_job['r'];
		else
			$nb_fps_final=$nb_fps;
		
		$this->nb_fps_fichier = $nb_fps_final;
		//nombre d'images total
		// utilisation de la floor a voir pour un replacement avec round(?)
		

		if (isset($params_job['t']) && !empty($params_job['t']))
		{
			if ((timeToSec($params_job['ss'])+$params_job['t'])>$duree_max){
				$this->nb_frames=floor(($duree_max-timeToSec($params_job['ss']))*$this->nb_fps_fichier);
			}else{
				$this->nb_frames=floor($params_job['t']*$this->nb_fps_fichier);
			}
		}else if (isset($this->generation_montage) && !empty($this->generation_montage)){
			$nb_frames_tmp = 0 ;  
			// pour l'instant dans le cas de la génération d'un montage on sort forcément un fichier à 25 fps, ce qui simplifie le calcul du nombre de frames attendues 
			foreach($this->montage_map as $clip){
				if($clip['type'] == 'doc' && isset($clip['file_tcin']) && $clip['file_tcout']){
					$nb_frames_tmp += (tcToSecDec($clip['file_tcout']) -  tcToSecDec($clip['file_tcin']))*$params_job['r']; 
				}else if ($clip['type'] == 'carton'){
					// pour l'instant la durée d'un carton est fixe
					$nb_frames_tmp += 5*$params_job['r'] ; 
				}
			}
			// $this->writeJobLog("compte nb frames finale montage : ".$nb_frames_tmp);
			$this->nb_frames=$nb_frames_tmp;
		}else{
			$this->nb_frames=floor($duree_max*$this->nb_fps_fichier);
		}
		
		if (isset($params_job['tcin']) && !empty($params_job['tcin']) && isset($params_job['tcout']) && !empty($params_job['tcout']))
		{
			$tcin_sec=tcToSecDec($params_job['tcin'],$this->nb_fps_fichier);
			$tcout_sec=tcToSecDec($params_job['tcout'],$this->nb_fps_fichier);
			if(!empty($params_job['preroll'])){
				$tcin_sec=$tcin_sec-tcToSecDec($params_job['preroll'],$this->nb_fps_fichier);
				if ($tcin_sec < 0) $tcin_sec = 0;
			}
			if(!empty($params_job['postroll'])){
				$tcout_sec = tcToSecDec($params_job['postroll'],$this->nb_fps_fichier)+tcToSecDec($params_job['tcout'],$this->nb_fps_fichier);
			}
			
			$params_job['ss']=$tcin_sec;
			$params_job['t']=$tcout_sec-$tcin_sec;
			
			$this->nb_frames=$params_job['t']*$this->nb_fps_fichier;
		}
		
		// VP 4:09/2014 : paramètres -ss et -t avant -i si gop=1
		$options_sst='';
		if (isset($params_job['ss']) && !empty($params_job['ss']))
			$options_sst.=' -ss '.$params_job['ss'];
		if (isset($params_job['t']) && !empty($params_job['t']))
			$options_sst.=' -t '.$params_job['t'];
			
		return $options_sst;
	}
	
	function calc_vfilters(&$params_job,$infos){
		$chaine_vfilters.='';
		
		// filtre de desentrelacement
		if ( isset( $params_job['yadif'] ) && !empty( $params_job['yadif'] ) )
			$chaine_vfilters.='yadif='.$params_job['yadif'];
		else if (isset($params_job['deinterlace']) && !empty($params_job['deinterlace']))
			$chaine_vfilters.='yadif=0:-1:1';
		// si aspect_ratio auto => detection auto de l'aspect ratio cible,
		if(isset($params_job['aspect_ratio']) && $params_job['aspect_ratio']=="auto")
		{
			if(strpos($infos['display_aspect_ratio'],':')>0){
				$tmp_aspect = explode(':',$infos['display_aspect_ratio']);
				$aspect_ratio=floatval($tmp_aspect[0])/floatval($tmp_aspect[1]);
			}else{
				$aspect_ratio=floatval($infos['display_aspect_ratio']);
			}
			if($aspect_ratio < 1.5){
				$params_job['aspect_ratio'] = "4:3";
			}else if ($aspect_ratio>=1.5){
				$params_job['aspect_ratio'] = "16:9";
			}
			
			$aspect_cible = $params_job['aspect_ratio'];
			unset($tmp_aspect);
		// si aspect_ratio défini => aspect_ratio cible
		}
		else if (isset($params_job['aspect_ratio']) && !empty($params_job['aspect_ratio']))
			$aspect_cible = $params_job['aspect_ratio'];
		else
			$aspect_cible = $infos['display_aspect_ratio'];
		
		
		
		$largeur=$params_job['width'];
		$hauteur=$params_job['height'];
		
		$src_w=$infos['display_width'];
		$src_h=$infos['display_height'];
		
		if(empty($src_w) || empty($src_h))
		{
			//largeur et hauteur par défaut
			$src_w=320;
			$src_h=240;
		}
		
		if(isset($aspect_cible))
		{
			$tmp_aspect = explode(':',$aspect_cible);
			$aspect_value = floatval($tmp_aspect[0])/floatval($tmp_aspect[1]);
			unset($tmp_aspect);				
		}
		
		if (!empty($largeur) && empty($hauteur) && isset($aspect_value))
			$hauteur = round($largeur *(1/$aspect_value)); //largeur sans hauteur
		
		if (empty($largeur) && !empty($hauteur)&& isset($aspect_value))
			$largeur = round($hauteur*$aspect_value); //hauteur sans largeur
		
		if (empty($largeur) && empty($hauteur)) //pas de size : on garde la taille originale
		{
			$hauteur=$src_h;
			$largeur=$src_w;
		}
		
		$largeur+=$largeur%2;
		$hauteur+=$hauteur%2;
		
		$params_job['aspect'] = $params_job['aspect_ratio'];
		

		
		$largeur_scale='min('.$largeur.'\,2*trunc(('.$hauteur.'*dar+1)/2))';
		$hauteur_scale='min('.$hauteur.'\,2*trunc(('.$largeur.'/dar+1)/2))';
		
		unset($tmp_aspect);
		
		if(isset($params_job['aspect_mode']) && $params_job['aspect_mode']=='letterbox')
		{
			if(!empty($chaine_vfilters))
				$chaine_vfilters.=",";
			
			$chaine_vfilters .= "scale=".$largeur_scale.":".$hauteur_scale."";
			$chaine_padding=",pad=".$largeur.":".$hauteur.":max(0\,(".$largeur."-iw)/2):max(0\,(".$hauteur."-ih)/2):black";
		}
		else if(isset($params_job['aspect_mode']) && $params_job['aspect_mode'] == 'fill')
		{
			if(!empty($chaine_vfilters))
				$chaine_vfilters.=",";
			
			if (isset($params_job['width']) && !empty($params_job['width']) && !isset($params_job['height'])) // hauteur variable
				$chaine_vfilters.='scale='.$largeur.':2*trunc(('.$largeur.'/dar+1)/2)';
			else
				$chaine_vfilters .= "scale=".$largeur.":".$hauteur;
			$chaine_padding="";
		}
		
		// ajout du logo dans vf
		if (!empty($params_job['logo']))
		{
			if (isset($chaine_vfilters) && !empty($chaine_vfilters)){
				$chaine_logo = $this->calc_logo($params_job, "mov");
				if(!empty($chaine_logo)){
					$chaine_vfilters.=' [mov] ,'.$chaine_logo;
				}
			}else{
				$chaine_logo = $this->calc_logo($params_job) ;
				if(!empty($chaine_logo)){
					$chaine_vfilters=$chaine_logo;
				}
			}
		}
		
		
		//ajout du logo avant le padding et apres le scale
		$chaine_vfilters=$chaine_vfilters.$chaine_padding;
		
		if(isset($params_job['vf']) && !empty($params_job['vf'])){
			if(!empty($chaine_vfilters))
				$chaine_vfilters.=",";
			
			$chaine_vfilters.=$params_job['vf'];
			unset($params_job['vf']);
		}
		
		
		// incrustation du timecode
		if (isset($params_job['incrust_tc']) && !empty($params_job['incrust_tc']))
		{
			// police_tc
			if (isset($params_job['police_tc']) && !empty($params_job['police_tc']))
			{
				if (file_exists($params_job['police_tc'])) // retrocompatibilite avec les anciennes etapes frontal
					$police_tc=$params_job['police_tc'];
				else
					$police_tc=jobFontDir.$params_job['police_tc'];
			} else {
				//$police_tc=jobFontDir.'Tahoma.ttf';
				$police_tc=jobFontDir.'AurulentSansMono-Regular.otf';
			}
			
			// incrust_tc_in
			if (isset($params_job['incrust_tc_in']) && !empty($params_job['incrust_tc_in'])) {
				$incrust_tc_in=str_replace(':','\:',$params_job['incrust_tc_in']);
				
			} else {
				if (isset($infos['timecode']) && !empty($infos['timecode'])) {
					if(strpos($infos['timecode'], ";") === false) {
						$tc = $infos['timecode'];
					} else {
						$tc = secDecToTc(tcToSecDec($infos['timecode']));
					}
					
					
				} else {
					$tc='00:00:00:00';
				}
				
				//cas de l'extrait. //TODO : rendre compatible avec preroll ( mettre à jour tcin après prise en compte preroll dans la méthode calc_TimeOpts_Fps ?
				if (isset($params_job['tcin']) && !empty($params_job['tcin'])) {
					$tc = secDecToTc ( tcToSecDec($tc) + tcToSecDec($params_job['tcin']) );
				}
				
				$incrust_tc_in=str_replace(':','\:',$tc);
			}
			
			$filter_tc='';
			
			if ($largeur==640)
			{
				$pos_fond_tc_x=200;
				$largeur_tc=240;
				$hauteur_tc=34;
				$incrust_font_size=27;
				
				if ($largeur/$hauteur>=1.5)
				{
					$pos_fond_tc_y=322;
					$y_font=328;
				}
				else
				{
					$pos_fond_tc_y=442;
					$y_font=448;
				}
			}
			else if ($largeur==720)
			{
				$pos_fond_tc_x=253;
				$largeur_tc=210;
				$hauteur_tc=34;
				$incrust_font_size=27;
				
				if ($largeur/$hauteur>=1.5)
				{
					$pos_fond_tc_y=364;
					$y_font=371;
				}
				else
				{
					$pos_fond_tc_y=499;
					$y_font=506;
				}
			}
			else if ($largeur==960)
			{
				$pos_fond_tc_x=300;
				$largeur_tc=360;
				$hauteur_tc=44;
				$incrust_font_size=40;
				
				if ($largeur/$hauteur>=1.5)
				{
					$pos_fond_tc_y=486;
					$y_font=494;
				}
				else
				{
					$pos_fond_tc_y=666;
					$y_font=674;
				}
			}
			else if ($largeur==1024)
			{
				$pos_fond_tc_x=332;
				$largeur_tc=360;
				$hauteur_tc=44;
				$incrust_font_size=40;
				
				$pos_fond_tc_y=400;
				$y_font=406;

			}
			else if ($largeur==1280)
			{
				$pos_fond_tc_x=460;
				$largeur_tc=360;
				$hauteur_tc=44;
				$incrust_font_size=40;
					
				$pos_fond_tc_y=666;
				$y_font=674;
			}
			else if ($largeur==1920)
			{
				$pos_fond_tc_x=600;
				$largeur_tc=720;
				$hauteur_tc=90;
				$incrust_font_size=80;
				
				if ($largeur/$hauteur>=1.5)
				{
					$pos_fond_tc_y=970;
					$y_font=985;
				}
				else
				{
					$pos_fond_tc_y=1330;
					$y_font=1345;
				}
				
			}
			
			
			if (isset($params_job['marge_tc']) && !empty($params_job['marge_tc']))
			{
				$this->writeJobLog('marge='.$hauteur.'*'.$params_job['marge_tc']);
				$this->writeJobLog('y_font='.$y_font);
				$this->writeJobLog('pos_fond_tc_y='.$pos_fond_tc_y);
				$marge=intval(floor($hauteur*floatval($params_job['marge_tc'])));
				$y_font=$y_font-$marge;
				$pos_fond_tc_y=$pos_fond_tc_y-$marge;
			}
			
            // VP 25/08/2016 : paramètre text_tc ('TC:' par défaut)
            $text_tc = 'TC\:';
            if (isset($params_job['text_tc']) && !empty($params_job['text_tc']))
            {
                if($params_job['text_tc'] == 'filename') $text_tc = stripExtension(basename($this->getFileInPath())).'\\ ';
                else $text_tc = $params_job['text_tc'];
                if (strlen($text_tc) > 3) {
                    $largeur_text = intval(floor( ($largeur_tc/12)*(strlen($text_tc) - 3) ));
                    $largeur_tc += $largeur_text;
                    $pos_fond_tc_x = $pos_fond_tc_x - $largeur_text/2;
                    if ($largeur_tc>$largeur) {
                        $largeur_tc = $largeur;
                    }
                    if ($pos_fond_tc_x <0) {
                        $pos_fond_tc_x = 0;
                    }
                }
            }
            
               // VP 15/07/2015 : remplacement t=max par t='.$hauteur_tc.' (compatibilité ffmpeg2.0.1)
			//$filter_tc.='drawbox=x='.$pos_fond_tc_x.':y='.$pos_fond_tc_y.':width='.$largeur_tc.':height='.$hauteur_tc.':color=black@0.3:t=max,fps='.$this->nb_fps_fichier.',';
			$filter_tc.='drawbox=x='.$pos_fond_tc_x.':y='.$pos_fond_tc_y.':width='.$largeur_tc.':height='.$hauteur_tc.':color=black@0.3:t='.$hauteur_tc.',fps='.$this->nb_fps_fichier.',';
			$filter_tc.='drawtext=fontfile='.$police_tc.':fontsize='.$incrust_font_size.':fontcolor=white@0.8:ft_load_flags=render+force_autohint:x=(main_w-text_w)/2:y='.$y_font.':text=\''.$text_tc.'\':timecode=\''.$incrust_tc_in.'\':r='.$this->nb_fps_fichier.'';
			
			
			if (isset($chaine_vfilters) && !empty($chaine_vfilters))
				$chaine_vfilters.=' ,'.$filter_tc;
			else
				$chaine_vfilters=$filter_tc;
		}
		
		// incrustation texte
		if (isset($params_job['incrust_text']) && !empty($params_job['incrust_text']))
		{
			// police_text
			if (isset($params_job['police_text']) && !empty($params_job['police_text']))
			{
				if (file_exists($params_job['police_text']))
					$police_text=$params_job['police_text'];
				else
					$police_text=jobFontDir.$params_job['police_text'];
			}
			else
				$police_text=jobFontDir.'Tahoma.ttf';
				
			// size_text
			if (isset($params_job['size_text']) && !empty($params_job['size_text']))
			{
				$incrust_font_size=intval($params_job['size_text']);
			}
			else
				$incrust_font_size=20;
			
			$incrust_text="";
			if (isset($params_job['incrust_text']) && !empty($params_job['incrust_text']))
			{
				$incrust_text=$params_job['incrust_text'];
			}
			
            // VP 25/08/2016 : paramètre incrust_text_y (entier ou 'middle')
            $text_y=4;
            if (isset($params_job['incrust_text_y']) && !empty($params_job['incrust_text_y']))
            {
                if($params_job['incrust_text_y']=='middle') $text_y='(main_h-text_h)/2'; // texte centré
                else $text_y=intval($params_job['incrust_text_y']);
            }
            
            $filter_tc='drawtext=fontfile='.$police_text.':fontsize='.$incrust_font_size.':text=\''.$incrust_text.'\':fontcolor=white@0.5:x=(main_w-text_w)/2:y='.$text_y;
		
			if (isset($chaine_vfilters) && !empty($chaine_vfilters))
				$chaine_vfilters.=' ,'.$filter_tc;
			else
				$chaine_vfilters=$filter_tc;
		}
		
		if (isset($params_job['aspect_mode']) && $params_job['aspect_mode'] == 'fill' && $largeur==640 && !isset($params_job['height'])) // hauteur variable
		{
			if(!empty($chaine_vfilters))
				$chaine_vfilters.=',';
			
			$chaine_vfilters.='setsar=1/1';
		}
		
		if (isset($params_job['final_resize']) && $params_job['final_resize'] == '1' && $largeur==1024) // hauteur variable
		{
			if(!empty($chaine_vfilters))
				$chaine_vfilters.=',';
			
			$chaine_vfilters.='scale=720:576';
		}
		
		
		return $chaine_vfilters;
	}

	
	function calc_filter_complex(&$params_job,$infos){
		$chaine_filter_complex='';
		
		if(isset($params_job['type_mapping']) && !empty($params_job['type_mapping'])){
			$src_audio_streams = array();
			if (isset($infos) && !empty($infos)){
				foreach($infos['tracks'] as $idx=>$track){
					if(strtolower($track['type']) == 'audio' && isset($track["channels"])){
						$src_audio_streams[] = array("idx"=>$idx,"nb_chan"=>$track["channels"]);
					}
				}
			}

			if(count($src_audio_streams) == 1 && $src_audio_streams[0]['nb_chan'] == 4 ){
				// CAS 1 piste x 4 canaux
				switch ($params_job['type_mapping']){
					case "stereo_multiples" :
						//$options_ffmbc.=" ".$newaudio_string." -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":2:0:2:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":3:0:2:1 ";
						break ;
						
					case "stereo_unique" :
						$chaine_filter_complex.="pan=stereo|c0<c0|c1<c1";
						break ;
						
					case "1_piste_4_canaux" :
						// ffmbc : le mapping est automatique, pas besoin d'ajouter un map_audio_channel
						break;
						
						
					case "stereo_downmix" :
						$chaine_filter_complex.="pan=stereo|c0<c0+c2|c1<c1+c3";
						break ;
				}
				
			}else if(count($src_audio_streams) == 1 && $src_audio_streams[0]['nb_chan'] == 8 ){
				// CAS 1 piste x 8 canaux
				switch ($params_job['type_mapping']){
					case "stereo_unique" :
						$chaine_filter_complex.="pan=stereo|c0<c0|c1<c1";
						break ;
						
					case "stereo_downmix" :
						// Bug dans ffmpeg2.0.1, corrig� en ffmpeg2.4
						//$chaine_filter_complex.="pan=stereo|c0<c0+c2+c4+c6|c1<c1+c3+c5+c7";
						$chaine_filter_complex.="pan=stereo|c0<c0+c2|c1<c1+c3";
						break ;
				}
			}else if(count($src_audio_streams) == 2 ){
				$is_mono = true ;
				foreach($src_audio_streams as $stream){
					if($stream['nb_chan'] != 1){
						$is_mono = false ; break ;
					}
				}
				
				$is_stereo = true ;
				foreach($src_audio_streams as $stream){
					if($stream['nb_chan'] != 2){
						$is_stereo = false ; break ;
					}
				}
				if($is_mono){
					// CAS 2 pistes  x MONO
					switch ($params_job['type_mapping']){
						case "stereo_multiples" :
						case "1_piste_4_canaux" :
						case "stereo_unique" :
						case "stereo_downmix" :
							$chaine_filter_complex.="[0:".$src_audio_streams[0]["idx"]."] [0:".$src_audio_streams[1]["idx"]."] amerge";
							break ;
							
					}
				}
				
				if($is_stereo){
					// CAS 2 pistes x STEREO
					switch ($params_job['type_mapping']){
						case "stereo_multiples" :
						case "stereo_unique" :
						case "1_piste_4_canaux" :
							break ;
						case "stereo_downmix" :
							$chaine_filter_complex.="[0:".$src_audio_streams[0]["idx"]."] [0:".$src_audio_streams[1]["idx"]."] amerge,pan=stereo|c0<c0+c2|c1<c1+c3";
							break ;
					}
				}
			}else if (count($src_audio_streams) == 4){
				$is_mono = true ;
				foreach($src_audio_streams as $stream){
					if($stream['nb_chan'] != 1){
						$is_mono = false ;
						break ;
					}
				}
				if($is_mono){
					// CAS 4 pistes  x MONO
					switch ($params_job['type_mapping']){
						case "stereo_multiples" :
							break ;
							
						case "stereo_unique" :
							$chaine_filter_complex.="[0:".$src_audio_streams[0]["idx"]."] [0:".$src_audio_streams[1]["idx"]."] amerge";
							break ;
							
						case "1_piste_4_canaux" : 
						case "1_piste_n_canaux" :
							$params_job['ac']=4;
							$chaine_filter_complex.="[0:".$src_audio_streams[0]["idx"]."] [0:".$src_audio_streams[1]["idx"]."] [0:".$src_audio_streams[2]["idx"]."] [0:".$src_audio_streams[3]["idx"]."] amerge=inputs=4";
							break ;
							
						case "stereo_downmix" :
							$chaine_filter_complex.="[0:".$src_audio_streams[0]["idx"]."] [0:".$src_audio_streams[1]["idx"]."] [0:".$src_audio_streams[2]["idx"]."] [0:".$src_audio_streams[3]["idx"]."]  amerge=inputs=4,pan=stereo|c0<c0+c2|c1<c1+c3";
							break ;
					}
				}
			} else if (count($src_audio_streams) == 8){
				switch ($params_job['type_mapping']){
					case "1_piste_8_canaux" : 
					case "1_piste_n_canaux" :
						break;
						
					case "stereo_unique" :
						$chaine_filter_complex.="[0:".$src_audio_streams[0]["idx"]."] [0:".$src_audio_streams[1]["idx"]."] amerge";
						break ;

					case "stereo_downmix" :
						$chaine_filter_complex.="[0:".$src_audio_streams[0]["idx"]."] [0:".$src_audio_streams[1]["idx"]."] [0:".$src_audio_streams[2]["idx"]."] [0:".$src_audio_streams[3]["idx"]."] [0:".$src_audio_streams[4]["idx"]."] [0:".$src_audio_streams[5]["idx"]."] [0:".$src_audio_streams[6]["idx"]."] [0:".$src_audio_streams[7]["idx"]."] amerge=inputs=8,pan=stereo|c0<c0+c2+c4+c6|c1<c1+c3+c5+c7";
						break ;
				}
			}
		}
		return $chaine_filter_complex;
	}
	
	
	function calc_map_channel(&$params_job,$infos) {
		$chaine_map_channel='';
		// trace(print_r($params_job,1));
		if(empty($params_job['map_channel'])) {
			return '';
		}
		if($params_job['map_channel'] == "auto"){
			if(isset($params_job['type_mapping']) && !empty($params_job['type_mapping'])){
				$src_audio_streams = array();
				if (isset($infos) && !empty($infos)){
					foreach($infos['tracks'] as $idx=>$track){
						if(strtolower($track['type']) == 'audio' && isset($track["channels"])){
							$src_audio_streams[] = array("idx"=>$idx,"nb_chan"=>$track["channels"]);
						}
					}
				}
				
				if(count($src_audio_streams) == 1 && $src_audio_streams[0]['nb_chan'] == 2 ){
					// CAS 1 piste stéréo
					switch ($params_job['type_mapping']){
						case "stereo_multiples" :
							break ;
								
						case "mono_multiples" :
							$chaine_map_channel.=" -map 0:v -map 0:a -map 0:a -map_channel 0.".$src_audio_streams[0]["idx"].".0:0.1 -map_channel 0.".$src_audio_streams[0]["idx"].".1:0.2 ";
							break;
				
						case "stereo_unique" :
							break ;
				
						case "1_piste_4_canaux" :
							break;
					}
						
				} elseif(count($src_audio_streams) == 1 && $src_audio_streams[0]['nb_chan'] == 4 ){
					// CAS 1 piste x 4 canaux
					switch ($params_job['type_mapping']){
						case "stereo_multiples" : 
						break ;
					
						case "mono_multiples" : 
							$chaine_map_channel.=" -map 0:v -map 0:a -map 0:a -map 0:a -map 0:a -map_channel 0.".$src_audio_streams[0]["idx"].".0:0.1 -map_channel 0.".$src_audio_streams[0]["idx"].".1:0.2 -map_channel 0.".$src_audio_streams[0]["idx"].".2:0.3 -map_channel 0.".$src_audio_streams[0]["idx"].".3:0.4 ";
						break;
						
						case "stereo_unique" : 
						break ; 
						
						case "1_piste_4_canaux" : 
						break;
					}
					
				}else if(count($src_audio_streams) == 2 ){
					$is_mono = true ; 
					foreach($src_audio_streams as $stream){
						if($stream['nb_chan'] != 1){
							$is_mono = false ; break ; 
						}
					}
					
					$is_stereo = true ; 
					foreach($src_audio_streams as $stream){
						if($stream['nb_chan'] != 2){
							$is_stereo = false ; break ; 
						}
					}
					if($is_mono){
						// CAS 2 pistes  x MONO 
						switch ($params_job['type_mapping']){
							case "stereo_multiples" : 
							case "1_piste_4_canaux" : 
							case "stereo_unique" : 
							break ;
						
							case "mono_multiples" : 
								$chaine_map_channel.=" -map 0:v -map 0:a ";
							break;
						}
					}
					
					if($is_stereo){
						// CAS 2 pistes x STEREO 
						switch ($params_job['type_mapping']){
							case "stereo_multiples" : 
							break ;
						
							case "mono_multiples" : 
								$chaine_map_channel.=" -map 0:v -map 0:a:0 -map 0:a:0 -map 0:a:1 -map 0:a:1 -map_channel 0.".$src_audio_streams[0]["idx"].".0:0.1 -map_channel 0.".$src_audio_streams[0]["idx"].".1:0.2 -map_channel 0.".$src_audio_streams[1]["idx"].".0:0.3 -map_channel 0.".$src_audio_streams[1]["idx"].".1:0.4 ";
							break;
							
							case "stereo_unique" : 
							break ; 
							
							case "1_piste_4_canaux" : 
							break ;
							
						}
					}
				}else if (count($src_audio_streams) == 4){
					$is_mono = true ; 
					foreach($src_audio_streams as $stream){
						if($stream['nb_chan'] != 1){
							$is_mono = false ; 
							break ; 
						}
					}
					if($is_mono){
						// CAS 4 pistes  x MONO 
						switch ($params_job['type_mapping']){
							case "stereo_multiples" : 
							break ;
						
							case "mono_multiples" : 
								$chaine_map_channel.=" -map 0:v -map 0:a ";
							break;
							
							case "stereo_unique" : 
							break ; 
							
							case "1_piste_4_canaux" : 
							case "1_piste_n_canaux" :
							break ;
							
						}
					}
				} else if (count($src_audio_streams) == 8){
					switch ($params_job['type_mapping']){
						case "1_piste_8_canaux" : 
						case "1_piste_n_canaux" :
						break;
						case "mono_multiples" :
							$chaine_map_channel.=" -map 0:v -map 0:a -map 0:a -map 0:a -map 0:a -map 0:a -map 0:a -map 0:a -map 0:a ";
							$chaine_map_channel.=" -map_channel 0.".$src_audio_streams[0]["idx"].".0:0.1 -map_channel 0.".$src_audio_streams[0]["idx"].".1:0.2 -map_channel 0.".$src_audio_streams[0]["idx"].".2:0.3 -map_channel 0.".$src_audio_streams[0]["idx"].".3:0.4 ";
							$chaine_map_channel.=" -map_channel 0.".$src_audio_streams[0]["idx"].".4:0.5 -map_channel 0.".$src_audio_streams[0]["idx"].".5:0.6 -map_channel 0.".$src_audio_streams[0]["idx"].".6:0.7 -map_channel 0.".$src_audio_streams[0]["idx"].".7:0.8 ";
						break;
					}
				}
			}
		}else{
			$chunks=explode(';', $params_job['map_channel']);
			foreach($chunks as $chunk){
				$chaine_map_channel.=" -map_channel ".trim($chunk);
			}
		}
		
		return $chaine_map_channel;
	}

	function calc_logo($params_job, $stream_in="in"){
		// ajout du logo dans vf
		if(!empty($params_job['logo']) 
		&& (!isset($params_job['bypass_logo']) || $params_job['bypass_logo']!=1)){
			
			if(isset($params_job['logo_dir']) && !empty($params_job['logo_dir'])){
				$logo_path = resolve_path($params_job['logo_dir'].'/'.$params_job['logo']);
			}else {
				$logo_path = resolve_path(jobImageDir.'/'.$params_job['logo']);
			}
			
			if(!file_exists($logo_path))
				throw new FileNotFoundException('logo file not found');
			else{
				if(isset($params_job['logo_marge_scaled']) && !empty($params_job['logo_marge_scaled'])){
					if($largeur > $hauteur) {
						$marge_logo = ($params_job['logo_marge_scaled'] * $hauteur /100);
					} else {
						$marge_logo = ($params_job['logo_marge_scaled'] * $largeur /100);
					}
				}else if (isset($params_job['logo_marge']) && !empty($params_job['logo_marge']))
					$marge_logo=$params_job['logo_marge'];
				else
					$marge_logo=10;
				
					
				if(isset($params_job['logo_scale']) && !empty($params_job['logo_scale'])){
					$logo = new Imagick($logo_path);
					$logo_w_ori = $logo->getImageWidth();
					$logo_h_ori = $logo->getImageHeight();
					
					if($largeur > $hauteur) {
						// $resize_w_logo = $hauteur - ($marge_logo * $hauteur /100)*2;
						$resize_h_logo = ($hauteur*$params_job['logo_scale']/100);
						$resize_w_logo = $resize_h_logo*$logo_w_ori/$logo_h_ori;
					} else {
						// $resize_h_logo = $largeur - ($marge_logo * $largeur /100)*2;
						$resize_w_logo = ($params_job['logo_scale'] * $largeur /100);
						$resize_h_logo = $resize_w_logo*$logo_h_ori/$logo_w_ori;
					}
				}
				
				switch($params_job['logo_position'])
				{
					case 'center':
						$x_logo="main_w/2-overlay_w/2";
						$y_logo="main_h/2-overlay_h/2";
						break;
					case 'top-center':
						$x_logo="main_w/2-overlay_w/2";
						$y_logo=$marge_logo;
						break;
					case 'top-left':
						$x_logo=$marge_logo;
						$y_logo=$marge_logo;
						break;
					case 'top-right':
						$x_logo="main_w-overlay_w-".$marge_logo;
						$y_logo=$marge_logo;
						break;
					case 'bottom-left':
						$x_logo=$marge_logo;
						$y_logo="main_h-overlay_h-".$marge_logo;
						break;
					case 'bottom-center':
						$x_logo="main_w/2-overlay_w/2";
						$y_logo="main_h-overlay_h-".$marge_logo;
						break;
					default: //bottom-right
						$x_logo="main_w-overlay_w-".$marge_logo;
						$y_logo="main_h-overlay_h-".$marge_logo;
				}
				
				if(isset($params_job['logo_scale']) && !empty($params_job['logo_scale']) && $params_job['logo_scale']){
					$chaine_logo='movie='.$logo_path.' [logo];[logo] scale='.$resize_w_logo.':'.$resize_h_logo.' [logo2];['.$stream_in.'][logo2] overlay='.($x_logo).':'.($y_logo).'';
				}else{
					$chaine_logo='movie='.$logo_path.' [logo];['.$stream_in.'][logo] overlay='.($x_logo).':'.($y_logo).'';
				}
			}
		}
		
		return $chaine_logo;
	}
	
	// MS - 26.09.16 - fonction de generation du fichier temporaire "carton" pour livraison du montage
	function genCarton(&$clip){
		if(defined('kFFMPEGpath') ){
			//$this->writeJobLog("call genCarton");
			// $filename = $clip['titre'];
			
			if(isset($clip['clip_params'][0])){
				$node_params = $clip['clip_params'][0];
			}else{
				$node_params = array(
					'fontfamily'=>'Vera',
					'alignment'=>'middle'
				);
			}
			
			//$this->writeJobLog("genCarton node_params : ".print_r($node_params,true));
			if($clip['type'] == 'doc' && $clip['mediatype'] == 'picture'){
				// cas d'un carton depuis un doc image
				$id_doc = $clip['id'] ; 
				$filename = 'pic_'.$id_doc.'_'.microtime(true);
				try{
					$imgPath = $clip['file'];
				}catch(Exception $e){
					$this->writeJobLog("JobProcess_ffmpeg::genCarton - crash lors de la récupération du mat de visionnage du doc ".$id_doc);
				}
			}else{
				// cas carton black standard 
				 $filename = $clip['titre'];
			}
			//$this->writeJobLog("imgPath : ".$imgPath);
			if(!isset($imgPath) || empty($imgPath) || !file_exists($imgPath)){
				$imgPath = jobImageDir."/PixelNoir.png";
				if(!file_exists($imgPath)){
					$this->makePixelNoir();
				}
			}
			
			if(isset($node_params['print_text'])){
				$text = str_replace('\n',"\n",$node_params['print_text']);
			}else if (isset($clip['texte']) && !empty($clip['texte'])){
				$text = $clip['texte']; // devrait exister uniquement dans le cas d'un carton, et pas d'un doc image
			}else{
				$text="" ; 
			}
			
			if(!empty($text)){
				require_once(libDir.'/fonctionsGeneral.php');
				
				// récupération du fichier de police à utiliser en fonction de la police et du style
				$fontfile = getFontFilename('gAvailableFontMap',
					(isset($node_params['fontfamily'])?$node_params['fontfamily']:null),
					(isset($node_params['fontweight'])?$node_params['fontweight']:null),
					(isset($node_params['fontitalic'])?$node_params['fontitalic']:null)
				);

				// gestion des cas d'erreurs / fallbacks vers les anciens comportements 
				if(!isset($fontfile) || !$fontfile || !file_exists(jobFontDir.'/'.$fontfile)){
					$fontfile = "Vera.ttf";
				}
				
				// gestion de l'alignement du texte : 
				$margin = 60 ; 
				switch($node_params['alignment']){
					case 'left' :
						$str_align = (string)$margin ; 
						break ; 
					case 'right' :
						$str_align ="main_w-(text_w + ".$margin .")"; 
						break ; 
					case 'middle' : 
					default : 
						$str_align ="(main_w-text_w)/2"; 
						break ; 
				
				}

				// taille de la police
				switch((int)$node_params['fontsize']){
					case 1 :
						// très petite
						$fontsize = 16 ; 
						$line_max_length = 80;
						$interligne = 8 ; 
						$line_h = 15;
						break ;
					case 2 :
						// petite
						$fontsize = 32 ; 
						$line_max_length = 45;
						$interligne = 10 ; 
						$line_h = 24;
						break ;
					case 3 :
						// moyenne
						$fontsize = 48 ; 
						$line_max_length = 26;
						$interligne = 12 ; 
						$line_h = 38;
						break ;
					case 4 :
						// grande
						$fontsize = 64 ; 
						$line_max_length = 21;
						$interligne = 14 ;
						$line_h = 56;					
						break ;
					case 5 :
						// très grande
						$fontsize = 80 ; 
						$line_max_length = 17;
						$interligne = 16 ; 
						$line_h = 64;
						break ; 
					 
					default : 
						// default => même valeurs que "grande"
						$fontsize = 64 ; 
						$line_max_length = 21;
						$interligne = 14 ;
						$line_h = 56;					
						break ; 
				
				}
				
				
				// permet principalement de récupérer la virgule, qui doit être échappée en tant que &#44; 
				//car la virgule est le délimiteur des ID qu'on appelle avec prepareVisu est
				if(strpos($text,'\n') !== false) {
					$lines = explode('\n',html_entity_decode($text)); 
				} else {
					$lines = explode("\n",html_entity_decode($text)); 
				}
				
				$lines2 = array();
				// traitement de la chaine à incruster : pour l'instant 25char/lignes, 4lignes max
				foreach($lines as $idx => $line){
					// traitement des chaines de + de $line_max_length char
					if(strlen($line) > $line_max_length ){
						$tmp = $line;
						while (strlen($tmp)>$line_max_length){
							$space_pos = strrpos($tmp,' ',($line_max_length-strlen($tmp)));
							//On coupe sur l'espace s'il y en a un, sinon il faut tronquer là où on peut...
							if($space_pos !== false ) {
								$tmp_line = substr($tmp,0,$space_pos);
							} else {
								//Recherche d'une "limite de mot" où tronquer
								preg_match('/^.{1,'.$line_max_length.'}\b/', $tmp, $matches, PREG_OFFSET_CAPTURE);
								if(!empty($matches) && !empty($matches[0][1])) {
									$posForSplit = $matches[0][1];
								} else {
									//Autrement on tronque au milieu d'un mot
									$posForSplit = $line_max_length;
								}
								$tmp_line = substr($tmp,0,$posForSplit);
							}
							array_push($lines2,$tmp_line);
							$tmp = substr($tmp,strlen($tmp_line));
						}
						array_push($lines2,$tmp);
						unset($tmp);
						unset($tmp_line);
					}else{
						array_push($lines2,$line);
					}
				}
				// suppression des lignes au dela de la 4e
				$lines = $lines2;
				unset($lines2);
				$count_lines = count($lines);
				$constant_offset = "-$line_h/2";
				
				$anchors_h = array() ; 
				if($count_lines%2 == 0 ){
					$constant_middle = 0.5*$interligne;
				}else if ($count_lines%2 != 0 ){
					$constant_middle = "($line_h/2 + ".$interligne.")" ; 
				}
				
				for($i = - intval($count_lines/2); $i <= intval($count_lines/2) ; $i++ ){
					if($count_lines%2 == 0 && $i == 0 ){
						continue ; 
					}
					$sign = (($i>=0)?"+":"-") ;
					$j = abs($i);

					 if($i == 0 ){
						$anchors_h[] = "main_h/2 ".$constant_offset;
					}else{
						$anchors_h[] = "main_h/2 ".$sign.$constant_middle." ".$sign."($line_h/2) ".$sign.($j-1)."*($line_h + ".$interligne.") ".$constant_offset;
					}
				}
				
			}
			
			$cmd =" -f image2 -framerate 25 -loop 1 -y -i ".$imgPath." -f lavfi -i anullsrc=r=44100:cl=stereo -f mp4 -vcodec libx264 -vprofile baseline -aspect 16:9 -r 25 -pix_fmt yuv420p -vb 1000k -vf \"";		
			$cmd.= "scale=min(960\,2*trunc((540*dar+1)/2)):min(540\,2*trunc((960/dar+1)/2)),pad=960:540:max(0\,(960 - iw)/2):max(0\,(540-ih)/2):black";
				// ajout d'un filtre par ligne de texte à afficher.
			
			if(isset($lines) && !empty($lines)){
				foreach($lines as $idx=> $line){
					$escaped_line = str_replace("'","’",$line);
					$escaped_line = preg_replace('/([^[:alnum:]| ])/','\\\\$1',$escaped_line);
					$cmd .=",drawtext=fontfile=".jobFontDir."/".$fontfile.":fontsize=".$fontsize.":text='".$escaped_line."':x=".$str_align.":y=".$anchors_h[$idx].":fontcolor=white";	
				}
			}
			$filename = $filename.'.mp4';
			
			$cmd .= ",fade=in:0:25,fade=out:100:25\" -t 5 ".$this->getTmpDirPath()."/".$filename;

			$ret_code = $this->shellExecute(kFFMPEGpath,$cmd);
			
			if($ret_code!=0){
				throw new Exception(kErrorGenCarton);
				return false ; 
			}
			$clip['file_tcin'] = secToTc(0) ; 
			$clip['file_tcout'] = secToTc(5) ; 
			$clip['file']  = $this->getTmpDirPath()."/".$filename; ; 
			
			// $this->writeJobLog('genCarton fin - clip : '.print_r($clip,true));

			return true;
		}else{
			$this->dropError(kError);
			return false; 
		}
	}
	
	private function makePixelNoir(){
		if(!file_exists(jobImageDir."/PixelNoir.png")){
			if(!defined('kConvertPath')){
				throw new Exception ("makePixelNoir - kConvertPath not defined");
			}
			$cmd = "-size 10x10 xc:black ".jobImageDir."/PixelNoir.png";
			
			$return = $this->shellExecute(kConvertPath,$cmd);
			// $this->writeJobLog("makePixelNoir return cmd : ".print_r($cmd,true));
		}	
		return jobImageDir."/PixelNoir.png";
	}
	
	
	private function checkIfFfmpegRunning()
	{
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$this->getPid().'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1)
				return true;
			else
				return false;
		}
	}
	
	private function getGopSize($gop_info, $video_format, $format_calc)
	{
		if(!empty($gop_info)){
			$donnees_gop=explode(',',$gop_info);
			foreach($donnees_gop as $dat_gop)
			{
				if (strpos($dat_gop,'N=')!=false)
				{
					$valeur_gop=explode('=',trim($donnees_gop[1]));
					return intval($valeur_gop[1]);
				}
			}
		} else {
			if ($video_format=='VC-3' || $video_format=='ProRes' || $video_format=='DV')
				$gop=1;
			else if ($video_format=='VC-1' || strpos($format_calc,'MPEG2')!==false || $video_format=='AVC')
				$gop=0; // GOP != 1
			else
				$gop=1;
		}
		return $gop;
	}

	public static function kill($module,$xml_file)
	{
		$job_name=stripExtension($xml_file);
		$contenu=file_get_contents(jobRunDir.'/'.$module.'/'.$job_name.'.pid');

		$contenu=explode("\n",$contenu);
		
		foreach ($contenu as $ligne)
		{
			$tmp_ligne=explode('=',$ligne);
			if ($tmp_ligne[0]=='tmp_file')
			{
				// unlink($tmp_ligne[1]);
				break;
			}
		}
		
		JobProcess::kill($module,$xml_file);
	}
}

?>
