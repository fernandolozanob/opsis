<?php
require_once(libDir."class_cherche.php");


class RechercheRequete extends Recherche {

	private $requestsType;
	
	function __construct() {
	  	$this->entity='REQUETE';
	  	$this->name=kRequete;
     	$this->prefix='req';
     	$this->sessVar='recherche_REQUETE';
		$this->tab_recherche=array();
		$this->useSession=false;
		unset($_SESSION[$this->sessVar]);
	}

    function prepareSQL(){
		global $db;
		$this->sql = "SELECT * 
				FROM t_requete as req
				where req.ID_USAGER=".intval(User::getInstance()->UserID)." and req.REQ_SQL<>''";
		$this->addSQL_requestsType();
		
		$this->sqlSuffixe = "";
        $this->etape="";
    }

    
    /**
     * Fixe les types de recherche (session en cours, ou sauvegardées) à lister
     * IN : array $modeAff, valeurs possible des cellules : "session" ou "saved"
     * 
     * @param array $modeAff
     */
    function set_requestsType( $modeAff ) {
    	$this->requestsType = $modeAff;
    }

    function get_requestsType() {
    	return $this->requestsType;
    }
    
    function addSQL_requestsType() {
    	global $db;
    	
    	$this->sql .= " AND ( ";
    	$tmpSql = '';
    	foreach($this->requestsType as $type) {
    		if(!empty($tmpSql))
    			$tmpSql.= ' OR';
    		switch ($type) {
    			case 'session':
    				$tmpSql .= " SID=".$db->Quote(session_id());
    				break;
    	
    			case 'saved':
    				$tmpSql .= " REQ_SAVED='1'";
    				break;
    	
    		}
    	}
    	$this->sql .= $tmpSql ." ) ";
    	unset($tmpSql);
    	
    }
    
    function execute($max_rows = 10, $secstocache=0, $highlight=false) {
    	parent::execute($max_rows, $secstocache, $highlight);
    	foreach ($this->result as $key => $row)
    	{
    		$this->result[$key]["REQ_PARAMS"]=unserialize($this->result[$key]["REQ_PARAMS"]);
    	}
    }

}
?>
