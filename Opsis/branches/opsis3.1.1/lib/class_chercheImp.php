<?php
require_once(libDir."class_cherche.php");


class RechercheImp extends Recherche {

	function __construct() {
	  	$this->entity='IMP';
		$this->name=kImport;
		
     	$this->prefix='';
     	$this->sessVar='recherche_IMP';
		$this->tab_recherche=array();
		$this->useSession=true;
	}

    function prepareSQL(){
		global $db;
		$this->sql = "SELECT imp.* ,t_job.job_id_etat as ETAT_JOB,t_job.job_progression AS PROGRESS_JOB, count(distinct t1.id_doc) as NB_DOCS, count(distinct t_mat.id_mat) as NB_MATS,t_usager.US_NOM || ' ' || t_usager.US_PRENOM as IMP_USAGER,timp.type_import
		FROM t_import as imp
		LEFT  JOIN t_doc t1 ON imp.ID_IMPORT=t1.DOC_ID_IMPORT and t1.ID_LANG=".$db->Quote($_SESSION['langue'])."
		LEFT  JOIN t_mat ON imp.ID_IMPORT=t_mat.MAT_ID_IMPORT
		LEFT JOIN t_job ON imp.IMP_ID_JOB= t_job.ID_JOB
		LEFT JOIN t_usager ON imp.IMP_ID_USAGER=t_usager.ID_USAGER
		LEFT JOIN t_type_import as timp ON (imp.IMP_TYPE=timp.ID_TYPE_IMPORT and timp.id_lang=".$db->Quote($_SESSION['langue']).") 
		WHERE 1=1";
	
	
		$this->sqlSuffixe = " GROUP BY imp.id_import,t_job.id_job, t_usager.id_usager,timp.id_type_import, timp.id_lang";
        $this->etape="";
    }
	
	    function appliqueDroits(){
    	//by LD 04/06/08 => utilisation d'un tableau et non d'une chaine pour éviter les id_fonds in ('',.....)
        $usr=User::getInstance();
        //$liste_id="''";
        // VP 8/02/13 : par défaut l'utilisateur n'a accès à rien
        $liste_id=array('0');
        foreach($usr->Groupes as $value){
           // if ($value["ID_PRIV"]!=0){ $liste_id.=", '".$value["ID_FONDS"]."'"; }
           if (isset($value["ID_PRIV"]) && $value["ID_PRIV"]!=0 && !empty($value["ID_FONDS"])) $liste_id[]=$value["ID_FONDS"];
        }

        //if(strlen($liste_id)>0) {
        if (count($liste_id)>0) {
            $this->sqlRecherche.= " AND DOC_ID_FONDS ";
            $this->sqlRecherche.= " in ('".implode("','",$liste_id)."')";
        }
        
        //PC 02/12/10 : Ajout de la possiblité d'importer un fichier afin de définir des droits supplémentaires 
        if (file_exists(designDir . '/include/addDroits.inc.php')) {
			include(getSiteFile("designDir", 'include/addDroits.inc.php'));
		}
    }

	function getLastImports($nb_imps=10){
		global $db ; 
		$myUser=User::getInstance();
		if(empty($nb_imps) || 0==intval($nb_imps)){
			$nb_imps = 10;
		}else{
			$nb_imps = intval($nb_imps);
		}
		
		
		if (empty($this->sql)){
			$this->prepareSQL();
			    	if ( (defined('gSeuilAppliqueDroits') && $myUser->Type < intval(gSeuilAppliqueDroits) ) 
			||(!defined('gSeuilAppliqueDroits') && $myUser->Type<kLoggedDoc ) ){
			 $this->appliqueDroits();
		}
		}
		$rsfull= $db->Execute($this->sql.$this->sqlRecherche.$this->sqlSuffixe." ORDER BY ID_IMPORT DESC LIMIT ".$nb_imps." ");
		if($rsfull){
			$xml = TableauVersXML($rsfull->GetArray(),"t_import",4,"last_imps",0);
		}
		return $xml;
	}



}
?>
