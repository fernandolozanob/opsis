<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_fileException.php');
require_once(libDir."class_matInfo.php");

class JobProcess_dvd extends JobProcess implements Process
{
	private $list_params_xml_video;
	
	private $nom_videos;
	private $list_video;
	private $video_menu;
	
	private $file_name;
	private $highlight_name;
	private $select_name;

	private $fichier_menu_final;
	private $video_m2v;
	
	private $fichier_son_menu;
	private $mpeg_menu_tmp;
	private $fichier_xml_spumux;
	private $fichier_xml_dvdauthor;
	
	private $tmp_dir_dvdauthor;
	
	private $font;
	private $font_size;
	
	private $standard;
	private $ratio;
	
	private $nb_pages_menu;
	private $image_fond_menu;
	private $image_logo_menu;
	private $logo_margin_left;
	private $logo_margin_top;
	
	private $largeur_image;
	private $hauteur_image;
	
	private $marge_vert;
	private $marge_hrz;
	
	private $largeur_image_finale;
	private $hauteur_image_finale;
	
	private $x_dpi;
	private $y_dpi;
	
	private $nb_bouton_page;
	
	private $couleur_texte;
	
	
	private $nb_etapes_encodage_dvd;
	private $nb_frames_fichiers;
	private $nb_fichiers_encodes;
    
    private $loop=0;
	
	private $arr_outfiles;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('dvd');
		
		$this->required_xml_params=array
		(
			'standard'=>'string',
			'aspect_ratio'=>'string'
		);
		
		$this->optional_xml_params=array
		(
			'couleur_texte'=>'string',
            'fond'=>'string',
            'font'=>'string',
            'logo'=>'string',
            'logo_margin_left'=>'int',
            'logo_margin_top'=>'int',
            'marge_vert'=>'int',
            'marge_hrz'=>'int',
            'no_menu'=>'int',
            'opening_file'=>'string',
            'video'=>'array',
			'videos'=>'array',
            'folder_out'=>'string'
		);
		
		$this->list_params_xml_video=array
		(
			'file'=>'file',
			'name'=>'string',
			'tcin'=>'string',
			'tcout'=>'string'
		);
		
		$this->nb_bouton_page=5;
		$this->setInput($input_job);
		$this->setEngine($engine);
		
		$this->nb_frames_fichiers=array();
		$this->arr_outfiles = array();
		$this->nb_fichiers_encodes=0;
	}
	
	protected function inXmlCheck()
	{
		// pour le job DVD il y a soit un fichier d'entr�e (in) soit un param�tre video
		
		// verification des parametres et de leurs types
		if (!empty($this->required_xml_params))
		{
			foreach($this->required_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
				{
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
				}
				else if (isset($this->params_job[$param_name]) && empty($this->params_job[$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		// verification des parametres et de leurs types
		if (!empty($this->optional_xml_params))
		{
			foreach($this->optional_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
			}
		}
		
		// verification des parametre video
		if (isset($this->params_job['video']) && !empty($this->params_job['video']))
		{
			foreach($this->params_job['video'] as $key=>$donnees_video)
			{
				foreach($this->list_params_xml_video as $param_name=>$type)
				{
					if (isset($donnees_video[$param_name]) && !empty($donnees_video[$param_name])){
						$this->params_job['video'][$key][$param_name]=$this->checkParamType($donnees_video[$param_name],$param_name,$type);
					}
				}
			}
		} elseif(isset($this->params_job['videos']) && !empty($this->params_job['videos'])) {
			$this->params_job['video'] = array();
			foreach($this->params_job['videos']['0'] as $key=>$donnees_video)
			{
				foreach($this->list_params_xml_video as $param_name=>$type)
				{
					if (isset($donnees_video[0][$param_name]) && !empty($donnees_video[0][$param_name])){
						$aTmp = explode('_', $key);
						$this->params_job['video'][$aTmp[1]][$param_name]=$this->checkParamType($donnees_video[0][$param_name],$param_name,$type);
						unset($aTmp);
					}
				}
			}
		}
		else
        {
            // VP 27/10/2015 : dvd sur fichier in si param�tre video vide
            $this->params_job['video'][0]['file']=$this->getFileInPath();
            $this->params_job['video'][0]['name']=basename($this->getFileInPath());
            //throw new InvalidXmlParamException('video param is not set');
       }
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$param_dvd=$this->getXmlParams();
		
		$fichier_vid=array();
		$this->nom_videos=array();
		
        if (!empty($param_dvd['no_menu']))
            $this->no_menu=$param_dvd['no_menu'];
        
        if (!empty($param_dvd['opening_file']))
        {
            $opening_file = jobVideoDir.'/'.$param_dvd['opening_file'];
            if (!file_exists($opening_file))
            {
                throw new FileNotFoundException('file not found ('.$opening_file.')');
            }
            array_unshift($param_dvd['video'],array('file' => $opening_file));
        }

		foreach ($param_dvd['video'] as $video)
		{
			$fichier_vid[]=$video['file'];
			// $this->nom_videos[]=stripslashes($video['name']);
		}
		
		// nombre d'etapes (nombre de videos + menu + dvdauthor)
		$this->nb_etapes_encodage_dvd=count($fichier_vid); // +1+1
		
		//couleur du texte 
		if (!empty($param_dvd['couleur_texte']))
		{
			$this->couleur_texte=explode(',',$param_dvd['couleur_texte']);
			$this->couleur_texte[0]=intval($this->couleur_texte[0]);
			$this->couleur_texte[1]=intval($this->couleur_texte[1]);
			$this->couleur_texte[2]=intval($this->couleur_texte[2]);
		}
		else
			$this->couleur_texte=array(255,255,255);
		
		// fond
		if (!empty($param_dvd['fond']))
		{
			$this->image_fond_menu=jobImageDir.$param_dvd['fond'];
		}
		
		// logo en haut a gauche
		if (!empty($param_dvd['logo']))
		{
			$this->image_logo_menu=jobImageDir.$param_dvd['logo'];
			if (!file_exists($this->image_logo_menu))
			{
				throw new FileNotFoundException('logo image not found ('.$this->image_logo_menu.')');
			}
			
			
			if (!empty($param_dvd['logo_margin_left']))
				$this->logo_margin_left=intval($param_dvd['logo_margin_left']);
			else
				$this->logo_margin_left=0;
			
			if (!empty($param_dvd['logo_margin_top']))
				$this->logo_margin_top=intval($param_dvd['logo_margin_top']);
			else
				$this->logo_margin_top=0;
		}
		
		// loop
		if (!empty($param_dvd['loop'])){
			$this->loop=$param_dvd['loop'];
		}

		$this->ratio=$param_dvd['aspect_ratio'];
		$this->standard=$param_dvd['standard'];
//		if(is_file($param_dvd['font'])==false){
//			$param_dvd['font']=jobFontDir.$param_dvd['font'];
//		}
		//$this->font=$param_dvd['font'];
        if (!empty($param_dvd['font']))
            $this->font=jobFontDir.'/'.$param_dvd['font'];
        else
            $this->font=jobFontDir.'/Arial.ttf';
		
        if (!file_exists($this->font))
        {
            throw new FileNotFoundException('font not found ('.$this->font.')');
        }
		$this->font_size=20;
	
		$video_contenu=$this->getTmpDirPath().'vid_';
		$this->video_menu=array();
		$this->list_video=array();
		
		
		//marges
        if(!empty($param_dvd['marge_vert'])) $this->marge_vert=$param_dvd['marge_vert'];
        else $this->marge_vert=0;
        if(!empty($param_dvd['marge_hrz'])) $this->marge_hrz=$param_dvd['marge_hrz'];
        else $this->marge_hrz=0;
		
		if ($this->standard=='PAL')
		{
			if ($this->ratio=='16:9')
				$this->largeur_image=1024;
			else
				$this->largeur_image=768;
			
			$this->hauteur_image=576;
			
			$this->largeur_image_finale=720;
			$this->hauteur_image_finale=576;
			
			$this->x_dpi=75;
			$this->y_dpi=80;
			$this->nb_fps_fichier = 25 ; 
		}
		else if ($this->standard=='NTSC')
		{
			if ($this->ratio=='16:9')
			{
				$this->largeur_image=853;
				$this->hauteur_image=480;
			}
			else
			{
				$this->largeur_image=640;
				$this->hauteur_image=480;
			}
			
			$this->largeur_image_finale=720;
			$this->hauteur_image_finale=480;
			
			$this->x_dpi=81;
			$this->y_dpi=72;
			$this->nb_fps_fichier = 30000/1001 ; 
		}
		else
			throw new InvalidXmlParamException('Standard is not PAL or NTSC');
		
		$duree_vid = array();
		$cnt_img = 0;
		$duree_totale = 0 ;
		$max_length = (defined("gDvdMaxLength")?gDvdMaxLength:9000);
		$this->setProgression(0);
		
		for ($i=0;$i<count($fichier_vid);$i++)
		{
			$param_ss ="";
			$param_t="";
			
			$infos = MatInfo::getMatInfo($fichier_vid[$i]);
			
			if (isset($param_dvd['video'][$i]['tcin']) && !empty($param_dvd['video'][$i]['tcin']) && isset($param_dvd['video'][$i]['tcout']) && !empty($param_dvd['video'][$i]['tcout']))
			{
				$param_ss=tcToSecDec($param_dvd['video'][$i]['tcin']);
				$param_t=tcToSecDec($param_dvd['video'][$i]['tcout'])-$param_ss;
				$duree_vid[$i] = $param_t;
				$param_ss = " -ss ".$param_ss;
				$param_t = " -t ".$param_t;
				
			}else{
				$duree_vid[$i]=$infos['duration'];
			}
			$this->nb_frames = $duree_vid[$i] * $this->nb_fps_fichier;
		
			// MS 02/12/13 - ajout mapping audio specifique si jamais on est dans les cas suivants : sources 1piste - 4canaux, 2pistes - Mono, 2pistes - St�r�o, 4pistes - Mono
			$src_audio_streams = array();
			foreach($infos['tracks'] as $idx=>$track){
				if(strtolower($track['type']) == 'audio' && isset($track["channels"])){
					$src_audio_streams[] = array("idx"=>$idx,"nb_chan"=>$track["channels"]);
				}
			}
			$mapping_audio = "";
			if(count($src_audio_streams) == 1 && $src_audio_streams[0]['nb_chan'] == 4 ){ // CAS 1x4canaux;
				$mapping_audio=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 ";
			}else if(count($src_audio_streams) == 2 ){
				if($src_audio_streams[0]["nb_chan"] == 1 ){ // CAS 2xMONO 
					$mapping_audio = " -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:1 ";
				}else if ($src_audio_streams[0]["nb_chan"] == 2 ){// CAS 2xSTEREO
					$mapping_audio=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 ";
				}
			}else if (count($src_audio_streams[0]) == 4 && $src_audio_streams[0]["nb_chan"] == 1 ){ // CAS 4pistes x MONO
				$mapping_audio=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:1 ";
			}
			
			
			if ($this->standard=='PAL')
				$cmd='-y -i "'.$fichier_vid[$i].'" '.$param_ss.$param_t.' -target dvd -threads 3 -vcodec mpeg2video -b 5000k -maxrate 8000k -bufsize 5000k -g 12 -bf 2 -sc_threshold 40 -aspect '.$this->ratio.' -bff -r 25 -pix_fmt yuv420p -vf "fieldorder=bff,scale=min('.$this->largeur_image.'\,'.$this->hauteur_image.'*dar*sar+1)/'.$this->largeur_image.'*'.$this->largeur_image_finale.':min('.$this->hauteur_image.'\,'.$this->largeur_image.'/(dar*sar)+1),pad='.$this->largeur_image_finale.':'.$this->hauteur_image_finale.':max(0\,('.$this->largeur_image_finale.'-iw)/2):max(0\,('.$this->hauteur_image_finale.'-ih)/2):black" -acodec ac3 -sample_fmt s16 -ac 2 -channel_layout 3 -ar 48000 -ab 256k "'.$video_contenu.($i+1).'.mpg" '.$mapping_audio;
			else if ($this->standard=='NTSC')
				$cmd='-y -i "'.$fichier_vid[$i].'" '.$param_ss.$param_t.' -target dvd -threads 3 -vcodec mpeg2video -b 5000k -maxrate 8000k -bufsize 5000k -g 15 -bf 2 -sc_threshold 40 -aspect '.$this->ratio.' -bff -r 30000/1001 -pix_fmt yuv420p -vf "fieldorder=bff,scale=min('.$this->largeur_image.'\,'.$this->hauteur_image.'*dar*sar+1)/'.$this->largeur_image.'*'.$this->largeur_image_finale.':min('.$this->hauteur_image.'\,'.$this->largeur_image.'/(dar*sar)+1),pad='.$this->largeur_image_finale.':'.$this->hauteur_image_finale.':max(0\,('.$this->largeur_image_finale.'-iw)/2):max(0\,('.$this->hauteur_image_finale.'-ih)/2):black" -acodec ac3 -sample_fmt s16 -ac 2 -channel_layout 3 -ar 48000 -ab 256k "'.$video_contenu.($i+1).'.mpg"'.$mapping_audio;
			
			$this->shellExecute(kFFMBCpath,$cmd,false);
			do
			{
				sleep(3);
				$this->updateProgress($this->nb_etapes_encodage_dvd + 1,$i);
				
			}while($tmp = $this->checkIfFfmbcRunning());
			$this->updateProgress($this->nb_etapes_encodage_dvd + 1,$i);
			

			if($duree_totale + $duree_vid[$i] > $max_length){
				$cnt_img++;
				$duree_totale = 0;
			}
			
			$duree_totale+=$duree_vid[$i];
			
			$this->nom_videos[$cnt_img][] = stripslashes($param_dvd['video'][$i]['name']);
			$this->list_video[$cnt_img][]= $video_contenu.($i+1).'.mpg';
			
			$this->nb_fichiers_encodes++;
		}
		$this->nb_etapes_gen_dvd = 2*($cnt_img+1);
		
		if($this->getProgression() < 80 ){
			$this->setProgression(80);
		}
		
		for($i_ = 0 ; $i_ <= $cnt_img ; $i_++){
			//JobProcess::writeJobLog("generate image dvd n ".($i_));
			// generation du menu
            if(empty($this->no_menu))
			$this->genereMenuDvd($i_);
			// generating ISO
			$this->genereImageDvd($i_);
			//clearing tmp dir 
			$this->clearTmpDir($i_);
		}
		if($cnt_img>0){
			$this->zipFilePath = dirname($this->getFileOutPath()).'/image.zip';
			$this->zipFiles();
			$this->WriteOutLog("multiples images => DVD zipped");

		}
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	private function genereMenuDvd($index_dvd_img)
	{
		//calcul du nombre de pages pour le menu
		$this->nb_pages_menu=ceil(count($this->list_video[$index_dvd_img])/$this->nb_bouton_page);
		
		
		$base=imagecreatetruecolor($this->largeur_image,$this->hauteur_image);
		imagealphablending($base,true);
		$fond=imagecolorallocatealpha($base,0,0,0,0);
		imagefill($base,0,0,$fond);

		//incrusatation de l'image de fond
		if (!empty($this->image_fond_menu))
		{
			$fond=imagecreatefrompng($this->image_fond_menu);
			imagecopy($base,$fond,0,0,0,0,$this->largeur_image,$this->hauteur_image);
		}

		//incrustation du logo
		if (!empty($this->image_logo_menu))
		{
			// incrustation du logo en haut a gauche
			$logo=imagecreatefrompng($this->image_logo_menu);
			$size=getimagesize($this->image_logo_menu);
			imagecopy($base,$logo,$this->logo_margin_left,$this->logo_margin_top,0,0,$size[0],$size[1]);
		}
	
		// on genere les images du menu
		$this->file_name=$this->getTmpDirPath().'page.png';
		$this->highlight_name=$this->getTmpDirPath().'highlight.png';
		$this->select_name=$this->getTmpDirPath().'select.png';

		$this->fichier_menu_final=$this->getTmpDirPath().'menu_final_';
		$this->video_m2v=$this->getTmpDirPath().'menu.mov';
		
		$this->fichier_son_menu=$this->getTmpDirPath().'menu_son.wav';
		$this->mpeg_menu_tmp=$this->getTmpDirPath().'menu_tmp.mpg';
		$this->fichier_xml_spumux=$this->getTmpDirPath().'spumux_config.xml';
		
		$this->shellExecute(kFFMBCpath,'-ar 48000 -t 60 -f s16le -acodec pcm_s16le -i /dev/zero -f wav -acodec copy -y '.$this->fichier_son_menu.'');
		
		// JobProcess::writeJobLog('nombre de pages : '.$this->nb_pages_menu);
		$this->video_menu = array();
		for ($i=0;$i<$this->nb_pages_menu;$i++)
		{
			$position_text=150;
			$noir=imagecolorallocate($base,0,0,0);
			$image_page=imagecreatetruecolor($this->largeur_image,$this->hauteur_image);
			
			
			$image_highlight=imagecreatetruecolor($this->largeur_image,$this->hauteur_image);
			imagecolortransparent($image_highlight,$noir);
			
			$image_selected=imagecreatetruecolor($this->largeur_image,$this->hauteur_image);
			imagecolortransparent($image_selected,$noir);
			
			imagecopy($image_page,$base,0,0,0,0,$this->largeur_image,$this->hauteur_image);
			
			$blanc_text=imagecolorallocate($image_page,$this->couleur_texte[0],$this->couleur_texte[1],$this->couleur_texte[2]);
			
			for ($j=0;$j<$this->nb_bouton_page && $i*$this->nb_bouton_page+$j<count($this->list_video[$index_dvd_img]);$j++)
			{
				$id_video=$i*$this->nb_bouton_page+$j;
				imagettftext($image_page,$this->font_size,0,$this->marge_hrz,$position_text,$blanc_text,$this->font,$this->nom_videos[$index_dvd_img][$id_video]);
				
				// dessin du rectangle de highlight
				$vert=imagecolorallocate($image_highlight,255,255,255);
				imagerectangle($image_highlight,$this->marge_hrz-5,$position_text-$this->font_size-5,$this->largeur_image-$this->marge_hrz+5,$position_text+5,$vert);
				imagerectangle($image_highlight,$this->marge_hrz-5-1,$position_text-$this->font_size-5-1,$this->largeur_image-$this->marge_hrz+5+1,$position_text+5+1,$vert);
				// dessin du rectangle de selection
				$bleu=imagecolorallocate($image_selected,255,255,255);
				imagerectangle($image_selected,$this->marge_hrz-5,$position_text-$this->font_size-5,$this->largeur_image-$this->marge_hrz+5,$position_text+5,$bleu);
				imagerectangle($image_selected,$this->marge_hrz-5-1,$position_text-$this->font_size-5-1,$this->largeur_image-$this->marge_hrz+5+1,$position_text+5+1,$bleu);
				
				$position_text+=50;
			}
			
			imagettftext($image_page,$this->font_size,0,$this->largeur_image/2-96/2,$this->hauteur_image-150,$blanc_text,$this->font,'Lire tout');
			
			// dessin du rectangle de highlight
			$vert=imagecolorallocate($image_highlight,255,255,255);
			imagerectangle($image_highlight,$this->largeur_image/2-(106/2),$this->hauteur_image-150-$this->font_size-5,$this->largeur_image/2+(106/2),$this->hauteur_image-150+5,$vert);
			imagerectangle($image_highlight,$this->largeur_image/2-(106/2)-1,$this->hauteur_image-150-$this->font_size-5-1,$this->largeur_image/2+(106/2)+1,$this->hauteur_image-150+5+1,$vert);
			
			// dessin du rectangle de selection
			$bleu=imagecolorallocate($image_selected,255,255,255);
			imagerectangle($image_selected,$this->largeur_image/2-(106/2),$this->hauteur_image-150-$this->font_size-5,$this->largeur_image/2+(106/2),$this->hauteur_image-150+5,$bleu);
			imagerectangle($image_selected,$this->largeur_image/2-(106/2)-1,$this->hauteur_image-150-$this->font_size-5-1,$this->largeur_image/2+(106/2)+1,$this->hauteur_image-150+5+1,$bleu);
			
			if ($this->nb_pages_menu>1)
			{
				if ($i!=0) // pas a la premiere page
				{
					//affichage d'un fleche vers la gauche
					$array_points=array(0,22,22,0,22,10,50,10,50,35,22,35,22,44);
					for ($k=0;$k<14;$k=$k+2)
					{
						$array_points[$k]=$array_points[$k]+$this->marge_hrz;
						$array_points[$k+1]=$array_points[$k+1]+$this->hauteur_image-45-$this->marge_vert;
					}
					imagefilledpolygon($image_page,$array_points,7,$blanc_text);
					
					imagerectangle($image_highlight,$this->marge_hrz-5,$this->hauteur_image-45-$this->marge_vert-5,$this->marge_hrz+50+5,$this->hauteur_image-$this->marge_vert+5,$vert);
					imagerectangle($image_highlight,$this->marge_hrz-5-1,$this->hauteur_image-45-$this->marge_vert-5-1,$this->marge_hrz+50+5+1,$this->hauteur_image-$this->marge_vert+5+1,$vert);
					
					imagerectangle($image_selected,$this->marge_hrz-5,$this->hauteur_image-45-$this->marge_vert-5,$this->marge_hrz+50+5,$this->hauteur_image-$this->marge_vert+5,$bleu);
					imagerectangle($image_selected,$this->marge_hrz-5-1,$this->hauteur_image-45-$this->marge_vert-5-1,$this->marge_hrz+50+5+1,$this->hauteur_image-$this->marge_vert+5+1,$bleu);
					
				}
				
				if ($i<($this->nb_pages_menu-1)) // pas a la derniere page
				{
					//affichage d'un fleche droite
					$array_points=array(50,22,28,0,28,10,0,10,0,35,28,35,28,44);
					for ($k=0;$k<14;$k=$k+2)
					{
						$array_points[$k]=$array_points[$k]+$this->largeur_image-50-$this->marge_hrz;
						$array_points[$k+1]=$array_points[$k+1]+$this->hauteur_image-45-$this->marge_vert;
					}
					imagefilledpolygon($image_page,$array_points,7,$blanc_text);
					
					imagerectangle($image_highlight,$this->largeur_image-50-$this->marge_hrz-5,$this->hauteur_image-45-$this->marge_vert-5,$this->largeur_image-$this->marge_hrz+5,$this->hauteur_image-$this->marge_vert+5,$vert);
					imagerectangle($image_highlight,$this->largeur_image-50-$this->marge_hrz-5-1,$this->hauteur_image-45-$this->marge_vert-5-1,$this->largeur_image-$this->marge_hrz+5+1,$this->hauteur_image-$this->marge_vert+5+1,$vert);
					
					imagerectangle($image_selected,$this->largeur_image-50-$this->marge_hrz-5,$this->hauteur_image-45-$this->marge_vert-5,$this->largeur_image-$this->marge_hrz+5,$this->hauteur_image-$this->marge_vert+5,$bleu);
					imagerectangle($image_selected,$this->largeur_image-50-$this->marge_hrz-5-1,$this->hauteur_image-45-$this->marge_vert-5-1,$this->largeur_image-$this->marge_hrz+5+1,$this->hauteur_image-$this->marge_vert+5+1,$bleu);
					
				}
			}
			
			imagepng($image_page,$this->file_name);
			imagepng($image_highlight,$this->highlight_name);
			imagepng($image_selected,$this->select_name);
			
			// image de base du menu
			$img=new Imagick($this->file_name);
			$img->resizeImage ($this->largeur_image_finale,$this->hauteur_image_finale,Imagick::FILTER_TRIANGLE,1);
			$img->setImageResolution($this->x_dpi,$this->y_dpi);
			$img->setImageUnits(Imagick::RESOLUTION_PIXELSPERINCH);
			$img->writeImage($this->file_name);
			// MS - 06.03.18 - Sur une install debian, les images resiz�es par Imagick deviennent vertes une fois charg�es par ffmbc,
			// pour ne pas passer des jours � debuger, on recharge & recopie les images au meme endroit, pas �l�gant mais �a fonctionne
			$tmp_image = imagecreatefrompng($this->file_name);
			imagepng($tmp_image,$this->file_name);
			
			// image pour les elements survoles
			$img=new Imagick($this->highlight_name);
			$img->resizeImage ($this->largeur_image_finale,$this->hauteur_image_finale,Imagick::FILTER_POINT,0);
			$img->setImageResolution($this->x_dpi,$this->y_dpi);
			$img->setImageUnits(Imagick::RESOLUTION_PIXELSPERINCH);
			$img->writeImage($this->highlight_name);
			

			//image pour les elements selectionnes
			$img=new Imagick($this->select_name);
			$img->resizeImage ($this->largeur_image_finale,$this->hauteur_image_finale,Imagick::FILTER_POINT,0);
			$img->setImageResolution($this->x_dpi,$this->y_dpi);
			$img->setImageUnits(Imagick::RESOLUTION_PIXELSPERINCH);
			$img->writeImage($this->select_name);
			
			
			if (!file_exists($this->file_name))
				throw new FileNotFoundException('error generating image ('.$this->file_name.')');
			
			if (!file_exists($this->highlight_name))
				throw new FileNotFoundException('error generating image ('.$this->highlight_name.')');
			
			if (!file_exists($this->select_name))
				throw new FileNotFoundException('error generating image ('.$this->select_name.')');
			
			// encodage de la video a partir du menu
			if ($this->standard=='PAL')
				$cmd='-f image2 -framerate 25 -loop 1 -i  '.$this->file_name.' -f mov -threads 3 -vcodec prores -profile hq -aspect '.$this->ratio.' -r 25 -pix_fmt yuv422p10le -t 60 -y '.$this->video_m2v.'';
			else if ($this->standard=='NTSC')
				$cmd='-f image2 -framerate 30000/1001 -loop 1 -i  '.$this->file_name.' -f mov -threads 3 -vcodec prores -profile hq -aspect '.$this->ratio.' -r 30000/1001 -pix_fmt yuv422p10le -t 60 -y '.$this->video_m2v.'';
			
			$this->shellExecute(kFFMBCpath,$cmd);
			
			if (!file_exists($this->video_m2v))
				throw new FileNotFoundException('error generating menu video ('.$this->video_m2v.')');
			
			// encodage du menu avec la video et le son
			if ($this->standard=='PAL')
				$cmd=' -y -i '.$this->video_m2v.' -i '.$this->fichier_son_menu.' -target dvd -threads 3 -vcodec mpeg2video -b 5000k -maxrate 8000k -bufsize 5000k -g 12 -bf 2 -sc_threshold 40 -aspect '.$this->ratio.' -bff -r 25 -pix_fmt yuv420p -vf "fieldorder=bff,scale=min('.$this->largeur_image.'\,'.$this->hauteur_image.'*dar*sar+1)/'.$this->largeur_image.'*'.$this->largeur_image_finale.':min('.$this->hauteur_image.'\,'.$this->largeur_image.'/(dar*sar)+1),pad='.$this->largeur_image_finale.':'.$this->hauteur_image_finale.':max(0\,('.$this->largeur_image_finale.'-iw)/2):max(0\,('.$this->hauteur_image_finale.'-ih)/2):black" -acodec ac3 -sample_fmt s16 -ac 2 -channel_layout 3 -ar 48000 -ab 256k '.$this->mpeg_menu_tmp.'';
			else if ($this->standard=='NTSC')
				$cmd=' -y -i '.$this->video_m2v.' -i '.$this->fichier_son_menu.' -target dvd -threads 3 -vcodec mpeg2video -b 5000k -maxrate 8000k -bufsize 5000k -g 15 -bf 2 -sc_threshold 40 -aspect '.$this->ratio.' -bff -r 30000/1001 -pix_fmt yuv420p -vf "fieldorder=bff,scale=min('.$this->largeur_image.'\,'.$this->hauteur_image.'*dar*sar+1)/'.$this->largeur_image.'*'.$this->largeur_image_finale.':min('.$this->hauteur_image.'\,'.$this->largeur_image.'/(dar*sar)+1),pad='.$this->largeur_image_finale.':'.$this->hauteur_image_finale.':max(0\,('.$this->largeur_image_finale.'-iw)/2):max(0\,('.$this->hauteur_image_finale.'-ih)/2):black" -acodec ac3 -sample_fmt s16 -ac 2 -channel_layout 3 -ar 48000 -ab 256k '.$this->mpeg_menu_tmp.'';
			
			$this->shellExecute(kFFMBCpath,$cmd);
			
			if (!file_exists($this->mpeg_menu_tmp))
				throw new FileNotFoundException('error generating menu (ffmbc - '.$this->mpeg_menu_tmp.')');
			
			// creation des boutons a partir des images highlight et selected
			// Voir pour remplacer les fichier highlight par les balises button dans le XML de spumux
			// cf. : $ man spumux
			$xml_spumux='<subpictures>
				<stream>
					<spu start="00:00:00.0" end="00:00:00.0"
					highlight="'.$this->highlight_name.'" 
					select="'.$this->select_name.'"
					autooutline="infer"
					autoorder="rows" />
				</stream>
			</subpictures>';
			
			file_put_contents($this->fichier_xml_spumux,$xml_spumux);
			
			$cmd=$this->fichier_xml_spumux.' < '.$this->mpeg_menu_tmp.' > '.$this->fichier_menu_final.$i.'.mpg';
			
			$this->shellExecute(kSpumuxPath,$cmd);
			
			if (!file_exists($this->fichier_menu_final.$i.'.mpg'))
				throw new FileNotFoundException('error generating menu (spumux - '.$this->fichier_menu_final.$i.'.mpg)');
			
			$this->video_menu[]=$this->fichier_menu_final.$i.'.mpg';
		}
		$this->setProgression($this->getProgression()+(20/$this->nb_etapes_gen_dvd),'generating menu');
		
	}
	
	private function genereImageDvd($index_dvd_img)
	{
		
		
		$this->fichier_xml_dvdauthor=$this->getTmpDirPath().'config_dvd_author.xml';
		$this->tmp_dir_dvdauthor=$this->getTmpDirPath().'DVD/';
		
        if(!empty($this->no_menu)){
            $xml_dvdauthor = '<dvdauthor>
                <vmgm />
                <titleset>
                <titles>
                <pgc>';
            
            foreach ($this->list_video[$index_dvd_img] as $vid) {
                $xml_dvdauthor.= '<vob file="'.$vid.'" />';
            }
            
            $xml_dvdauthor .='</pgc>
                </titles>
                </titleset>
                </dvdauthor>';
            
        }else{
            
		$xml_dvdauthor = '<dvdauthor>';
		$xml_dvdauthor.= '<vmgm>
							<menus lang="FR">
								<video format="'.strtolower($this->standard).'"/>
								<audio lang="FR" />';

		for ($i=0;$i<count($this->video_menu);$i++)
			$xml_dvdauthor.= '<pgc><post>jump titleset '.($i+1).' menu;</post></pgc>';

		$xml_dvdauthor.= '</menus>
						</vmgm>';
		$xml_dvdauthor.= '<titleset>';

		$xml_dvdauthor.= '<menus>';

		$xml_dvdauthor.= '<video format="'.strtolower($this->standard).'" />';
		$xml_dvdauthor.= '<audio lang="FR" />';

		for ($i=0;$i<count($this->video_menu);$i++)
		{
			$xml_dvdauthor.= '<pgc>';
			
			for ($j=0;$j<$this->nb_bouton_page && $i*$this->nb_bouton_page+$j<count($this->list_video[$index_dvd_img]);$j++)
			{
				$xml_dvdauthor.= '<button>jump title '.($i*$this->nb_bouton_page+$j+1).';</button>';
			}
			
			$xml_dvdauthor.= '<button>jump title '.(count($this->list_video[$index_dvd_img])+1).';</button>';
			
			if ($this->nb_pages_menu>1)
			{
				if ($i!=0) // fleche de gauche
				{
					$xml_dvdauthor.= '<button>jump menu '.$i.';</button>';
				}
				
				if ($i<($this->nb_pages_menu-1)) // fleche de droite
				{
					$xml_dvdauthor.= '<button>jump menu '.($i+2).';</button>';
				}
			}
			
			$xml_dvdauthor.= '<vob file="'.$this->video_menu[$i].'" />';
			$xml_dvdauthor.= '<post>jump cell 1;</post>';
			$xml_dvdauthor.= '</pgc>';
		}
	
		$xml_dvdauthor.= '</menus>';

		$xml_dvdauthor.= '<titles>';

		$xml_dvdauthor.= '<video format="'.strtolower($this->standard).'" />';
		$xml_dvdauthor.= '<audio lang="FR" />';

		// liste des videos pour la lecture unique
		foreach ($this->list_video[$index_dvd_img] as $idx => $vid)
		{
			$xml_dvdauthor.= '<pgc>';
			$xml_dvdauthor.= '<vob file="'.$vid.'" />';
            if($this->loop){
                $xml_dvdauthor.= '<post>jump title '.($idx+1).';</post>';
            }else{
                $xml_dvdauthor.= '<post>call menu;</post>';
            }
			$xml_dvdauthor.= '</pgc>';
		}
		
		// liste de videos pour la fonction lire tout
		$xml_dvdauthor.= '<pgc>';
		foreach ($this->list_video[$index_dvd_img] as $vid){
			$xml_dvdauthor.= '<vob file="'.$vid.'" />';
        }
        if($this->loop){
            $xml_dvdauthor.= '<post>jump title '.(count($this->list_video[$index_dvd_img])+1).';</post>';
        }else{
            $xml_dvdauthor.= '<post>call menu;</post>';
        }
		$xml_dvdauthor.= '</pgc>';


		$xml_dvdauthor.= '</titles>';
		$xml_dvdauthor.= '</titleset>';
		$xml_dvdauthor.= '</dvdauthor>';
        }
	
		if (file_put_contents($this->fichier_xml_dvdauthor,$xml_dvdauthor)===false)
			throw new FileNotFoundException('');
		
		$this->shellExecute(kDvdauthorPath,'-o '.$this->tmp_dir_dvdauthor.' -x '.$this->fichier_xml_dvdauthor);
		if($index_dvd_img>0){
			$out_file = str_replace(basename($this->getFileOutPath()),stripExtension(basename($this->getFileOutPath()))."_".$index_dvd_img.".".getExtension(basename($this->getFileOutPath())), $this->getFileOutPath());
		}else{
			$out_file = $this->getFileOutPath();
		}
		// JobProcess::writeJobLog("generating ".$out_file);
		$this->arr_outfiles[] = $out_file;
		// creation de l'image ISO
		$this->shellExecute(kMkisofsPath,'-dvd-video -o '.$out_file.' '.$this->tmp_dir_dvdauthor.'');
		// $this->shellExecute(kMkisofsPath,'-dvd-video -o '.$this->getFileOutPath().' '.$this->tmp_dir_dvdauthor.'');
		
		// if (!file_exists($this->getFileOutPath()))
			// throw new FileNotFoundException('ISO file not found ('.$this->getFileOutPath().')');
		if (!file_exists($out_file))
			throw new FileNotFoundException('ISO file not found ('.$out_file.')');
		
		$this->setProgression($this->getProgression()+(20/$this->nb_etapes_gen_dvd),'generating DVD');
	}
	
	public function finishJob()
	{
		$this->clearTmpDir(null);
		parent::finishJob();
	}
	
	function clearTmpDir($index_dvd_img){
		//suppression des fichier temporaires
		if(isset($index_dvd_img)  && $index_dvd_img !== null){
			foreach($this->list_video[$index_dvd_img] as $vid_dvd)
			{
				if (file_exists($vid_dvd) && unlink($vid_dvd)===false)
					throw new FileException('failed to delete video file ('.$vid_dvd.')');
			}
		}else if ($index_dvd_img == null){
			foreach($this->list_video as $list_vid){
				foreach($list_vid as $vid_dvd)
				{
					if (file_exists($vid_dvd) && unlink($vid_dvd)===false)
						throw new FileException('failed to delete video file ('.$vid_dvd.')');
				}
			}
		}
		
		//images du menu
		if (file_exists($this->file_name)  && unlink($this->file_name)===false)
			throw new FileException('failed to delete menu image file ('.$this->file_name.')');
		
		if (file_exists($this->highlight_name)  && unlink($this->highlight_name)===false)
			throw new FileException('failed to delete menu image file ('.$this->highlight_name.')');
		
		if (file_exists($this->select_name)  && unlink($this->select_name)===false)
			throw new FileException('failed to delete menu image file ('.$this->select_name.')');
		
		// fichier prores 
		if (file_exists($this->video_m2v)  && unlink($this->video_m2v)===false)
			throw new FileException('failed to delete menu video file ('.$this->video_m2v.')');
		
		//son menu
		if (file_exists($this->fichier_son_menu)  && unlink($this->fichier_son_menu)===false)
			throw new FileException('failed to delete menu audio file ('.$this->fichier_son_menu.')');
		
		// fichier mpeg avec son
		if (file_exists($this->mpeg_menu_tmp)  && unlink($this->mpeg_menu_tmp)===false)
			throw new FileException('failed to delete menu video file ('.$this->mpeg_menu_tmp.')');
		
		//videos du menu
		foreach($this->video_menu as $vid_menu)
		{
			if (file_exists($vid_menu)  && unlink($vid_menu)===false)
				throw new FileException('failed to delete menu video file ('.$vid_menu.')');
		}
		
		// XML pour dvdauthor et spumux
		if (file_exists($this->fichier_xml_spumux)  && unlink($this->fichier_xml_spumux)===false)
			throw new FileException('failed to delete spumux XML file ('.$this->fichier_xml_spumux.')');
			
		if (file_exists($this->fichier_xml_dvdauthor)  && unlink($this->fichier_xml_dvdauthor)===false)
			throw new FileException('failed to delete dvdauthor XML file ('.$this->fichier_xml_dvdauthor.')');
		
		// suppression du repertoire DVD
		JobProcess::supRep($this->tmp_dir_dvdauthor);
	}
	
	public function updateProgress($segment=1,$current=0)
	{
		$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
		// VP 8/04/13 : compatibilite Mac OS X
		$donnees_stderr=str_replace("\r","\n",$donnees_stderr);
		$params_job=$this->getXmlParams();
		//$this->writeJobLog($donnees_stderr);
		
		if (isset($params_job['vcodec']) && !empty($params_job['vcodec']))
		{	
			preg_match_all('/frame=([ 0-9]+) fps=([ 0-9.]+) (?:q=([- 0-9.]+) ){0,1}(Lsize|size)=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
			$this->nb_frames_encodees=intval($infos_encodage[1][count($infos_encodage[1])-1]);
		}
		else
		{
			// CAS fichier audio (pas de codec video)
			// pour les fichiers audio la sortie est : size=    5420kB time=00:03:18.17 bitrate= 224.0kbits/s
			preg_match_all('/size=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
			$nb_fps_encod=timeToSec($infos_encodage[2][count($infos_encodage[2])-1])*$this->nb_fps_fichier;
			$this->nb_frames_encodees=intval($nb_fps_encod);
		}
		
		//$this->writeJobLog("updateProgress ".$segment." ".$current);
		$this->writeOutLog($this->nb_frames_encodees.' / '.$this->nb_frames);
		$ratio=$this->nb_frames_encodees/$this->nb_frames;
		if ($ratio>1)
			$ratio=1;
		
		if($segment>1){
			$this->setProgression(($current*(100/$segment))+($ratio*100)/$segment);
		}else{
			$this->setProgression($ratio*100);
		}
		
		// $this->setProgression($ratio*100);
	}
	private function checkIfFfmbcRunning()
	{
		//$this->writeJobLog("checkIfFfmbcRunning");
		//if ($this->nb_frames_encodees<$this->nb_frames)
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$this->getPid().'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1)
				return true;
			else
				return false;
		}
	}
	
	
	private function zipFiles(){
		if(defined("kBinZip")){
			$cmd="-j1X ".escapeQuoteShell($this->zipFilePath);
			$cmd_files='';
			foreach ($this->arr_outfiles as $file)
			{
				if(file_exists($file))
					$cmd_files.=" ".escapeQuoteShell($file);
			}
			$this->shellExecute(kBinZip,$cmd." ".$cmd_files);
			if(!file_exists($this->zipFilePath) || (filesize($this->zipFilePath)<=1000)){
				throw new FileNotFoundException('file not found : '.$this->zipFilePath);
			}
			
		}else{
			throw new FileNotFoundException('kBinZip undefined');
		}
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
