<?php

require_once('class_jobProcess.php');
require_once(libDir.'class_matInfo.php');
require_once(libDir.'class_youtube.php');
require_once('interface_Process.php');

require_once('exception/exception_invalidXmlParamException.php');

class JobProcess_youtube extends JobProcess implements Process
{
	private $isApiV3 = false;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('youtube');
		
		$this->required_xml_params=array();
		$this->optional_xml_params=array();	
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		if (!empty($this->params_job['token'])) {
			$this->isApiV3 = true;
			$this->required_xml_params=array
			(
				'id_client'=>'string',
				'api_secret'=>'string',
				'token'=>'string',
				'donnees'=>'array'
			);
			
			$this->optional_xml_params=array
			(
				'video_star'=>'string'
			);
		}
		else {
			$this->required_xml_params=array
			(
				'user'=>'string',
				'pass'=>'string',
				'source'=>'string',
				'id_application'=>'string',
				'id_client'=>'string',
				'api_key'=>'string',
				'donnees'=>'array'
			);
			
			$this->optional_xml_params=array
			(
				'video_star'=>'string',
				'api_secret'=>'string'
			);
		}
		parent::inXmlCheck(false);
		
		$required_params=array
		(
			'title'=>'string',
			'private'=>'string'
		);
		
		$optional_params=array
		(
			'description'=>'string',
			'tags'=>'string',
			'channel_id'=>'string',
			'channel'=>'string',
			'location'=>'string'
		);
		
		//verification des donnees presentes dans le noeud donnees
		if (!empty($required_params))
		{
			foreach($required_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
				{
					$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type,false);
				}
				else if (isset($this->params_job['donnees'][0][$param_name]) && empty($this->params_job['donnees'][0][$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($optional_params))
		{
			foreach($optional_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
					$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type,false);
			}
		}
	}
	
	public function doProcess() {
		if ($this->isApiV3) $this->doProcess_v3();
		else $this->doProcess_v2();
	}
	
	public function doProcess_v3()
	{
		$this->writeOutLog('Debut traitement');
		
		//recuperation des parametres
		$params_job=$this->getXmlParams();
		
		
		$yt_obj = new Youtube();
		
		$yt_obj->client_id      = $params_job['id_client'];
		$yt_obj->client_secret  = $params_job['api_secret'];
		$yt_obj->refresh_token  = $params_job['token'];
		$yt_obj->champ_retour   = $params_job['champ_retour'];
		
		$yt_obj->connectYt();
		
		$this->setProgression(50);
		$path_to_video_to_upload = $this->getFileInPath();
		
		$video_id = $yt_obj->publish_video_yt_job($path_to_video_to_upload,$params_job);
		$this->writeOutLog('test vivien');
		if(!empty($video_id)){
			$this->writeOutLog('Youtube video_id:<id>'.$video_id.'</id>');
		}
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function doProcess_v2()
	{
		$this->writeOutLog('Debut traitement');
		
		//recuperation des parametres
		$params_job=$this->getXmlParams();
		
		$include_path_defaut=ini_get('include_path');
		ini_set('include_path',libDir.':.');
		require_once('Zend/Loader.php');
		Zend_Loader::loadClass('Zend_Gdata_YouTube');
		Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
		Zend_Loader::loadClass('Zend_Gdata_App_Exception');
		
		$http_client=null;
		$username=$params_job['user'];
		$password=$params_job['pass'];
		$service='youtube';
		$client=null;
		$source=$params_job['source'];
		$login_token=null;
		$login_captcha=null;
		$authentication_url= 'https://www.google.com/accounts/ClientLogin';
		$url_upload='http://uploads.gdata.youtube.com/feeds/users/default/uploads';
		
		try
		{
			$http_client = Zend_Gdata_ClientLogin::getHttpClient($username,$password,$service,$client,$source,$login_token,$login_captcha,$authentication_url);
		}
		catch (Zend_Gdata_App_AuthException $e)
		{
			throw new InvalidXmlParamException('Youtube authentification error : '.$e->getMessage());
		}
		catch (Zend_Gdata_App_HttpException $e)
		{
			throw new Exception('Http error : '.$e->getMessage());
		}
		catch (Zend_Gdata_App_CaptchaRequiredException $e)
		{
			throw new Exception('Captcha error : '.$e->getMessage());
		}
		
		$id_application=$params_job['id_application'];
		$id_client=$params_job['id_client'];
		$api_key=$params_job['api_key'];
		
		try
		{
			$yt = new Zend_Gdata_YouTube($http_client,$id_application,null,$api_key);
		}
		catch (Zend_Gdata_App_Exception $e)
		{
			throw new InvalidXmlParamException('Youtube Zend_Gdata_YouTube error : '.$e->getMessage());
		}
		
		//on charge la video
		$fichier_video=$yt->newMediaFileSource($this->getFileInPath());
		$fichier_video->setContentType(MatInfo::getTypeMimeFromFile($this->getFileInPath()));
		$fichier_video->setSlug(basename($this->getFileInPath()));
		
		$my_video_entry = new Zend_Gdata_YouTube_VideoEntry();
		$my_video_entry->setMediaSource($fichier_video);
		
		$my_video_entry->setVideoTitle($params_job['donnees'][0]['title']);
		$my_video_entry->setVideoDescription(str_replace(array('\n','<','>'),array("\n",'',''),$params_job['donnees'][0]['description']));
		
		trim($params_job['donnees'][0]['channel']);
		
		if (!empty($params_job['donnees'][0]['channel']))
			$my_video_entry->setVideoCategory($params_job['donnees'][0]['channel']);
		
		$my_video_entry->setVideoTags($params_job['donnees'][0]['tags']);
		
		if (isset($params_job['donnees'][0]['location']) && !empty($params_job['donnees'][0]['location']))
		{
			$yt->registerPackage('Zend_Gdata_Geo');
			$yt->registerPackage('Zend_Gdata_Geo_Extension');
			$where = $yt->newGeoRssWhere();
			$position = $yt->newGmlPos($params_job['donnees'][0]['location']);
			$where->point = $yt->newGmlPoint($position);
			$my_video_entry->setWhere($where);
		}
		
		if (!empty($params_job['donnees'][0]['private']) && ($params_job['donnees'][0]['private']=='1' || $params_job['donnees'][0]['private']=='true')){
			// VP 12/07/2011 : lgne comment�e car vid"o syst�matiquement priv�e
			$my_video_entry->setVideoPrivate();
		}
		
		try 
		{
			$this->setProgression(50);
			$new_entry=$yt->insertEntry($my_video_entry, $url_upload, 'Zend_Gdata_YouTube_VideoEntry');
			$video_id=$new_entry->getVideoId();

			$state=$new_entry->getVideoState();

			while ($state!=null && $state->getName()=='processing')
			{
				JobProcess::writeJobLog($state->getName());
				sleep(10);
				$new_entry=$yt->getVideoEntry($video_id);
				$state=$new_entry->getVideoState();
			}
			
			
			//$new_entry=$yt->getVideoEntry($video_id);
			if ($new_entry->getVideoState()!=null && $new_entry->getVideoState()->getName()=='rejected')
			{
				$nom_etat=$new_entry->getVideoState()->getName();
				$texte_etat=$new_entry->getVideoState()->getText();
				$new_entry=$yt->getVideoEntry($video_id,null,true);
				$yt->delete($new_entry);
				throw new Exception('video rejected : '.$nom_etat.' '.$texte_etat);
			}
			else
			{
				$this->writeOutLog('Youtube video_id:<id>'.$video_id.'</id>');
				JobProcess::writeJobLog('Youtube video_id:'.$video_id);
				//echo 'Nouvelle Video : '.$new_entry."\n";
			}
		}
		catch (Zend_Gdata_App_HttpException $e)
		{
			throw new Exception('Exception http : '.$e->getMessage());
		}
		catch (Zend_Gdata_App_Exception $e)
		{
			throw new Exception('Exception Zend_Gdata_App_Exception : '.$e->getMessage());
		}
		catch (Exception $e)
		{
			throw new Exception('Exception : '.$e->getMessage());
			/*$msg_xml=explode("\n",$e->getMessage());
			$msg_xml=$msg_xml[1];
			$retour_erreur=xml2array($msg_xml);
			$this->dropError('Exception : '.$retour_erreur['errors'][0]['error']['location']." ".$retour_erreur['errors'][0]['error']['code']."\n");*/
		}
		
		ini_set('include_path',$include_path_defaut);
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}
?>