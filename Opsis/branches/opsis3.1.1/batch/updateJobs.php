<?php
	require_once(modelDir.'model_job.php');
	global $db;
    
    // VP 10/05/12 : ajout paramètre plateforme pour gérer plusieurs frontaux
    if(defined("kJobPlateforme")){
        $sql_plateforme=" and j.JOB_PLATEFORME=".$db->Quote(kJobPlateforme);
    }else{
        $sql_plateforme="";
    }
		
	// jobs en demande d'annulation ou de suppression
	$result=$db->Execute('SELECT * FROM t_job 
	LEFT OUTER JOIN t_module ON t_job.JOB_ID_MODULE=t_module.ID_MODULE 
	WHERE JOB_ID_MODULE!=0 
	AND 
	(
		JOB_ID_ETAT='.intval(jobEnDemandeAnnulation).' OR 
		JOB_ID_ETAT='.intval(jobEnDemandeSuppression).' OR 
		JOB_ID_ETAT='.intval(jobEnCoursSuppression).' OR 
		JOB_ID_ETAT='.intval(jobEnCoursAnnulation).'
	)')->getRows();
	
	foreach($result as $job_db)
	{
		$job=new Job();
		$job->t_job['ID_JOB']=$job_db['ID_JOB'];
		$job->getJob();
		switch($job->t_job['JOB_ID_ETAT'])
		{
			case jobEnDemandeAnnulation:
				//si le job est en cours d'annulation ou l'annulation est terminée
				if (file_exists(jobCancellingDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml')) // en cours d'annulation
					$job->t_job['JOB_ID_ETAT']=jobEnCoursAnnulation;
				else if (file_exists(jobCanceledDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml'))
					$job->t_job['JOB_ID_ETAT']=jobAnnule;
				else if
				(
					!file_exists(jobCancelDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml') && 
					!file_exists(jobCancellingDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml') && 
					!file_exists(jobCanceledDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml')
				) // si dans aucun de ce repertoire ==> creation du fichier XML d'annulation
				{
					$job->makeCancelXml();
				}
				
				$job->save();
				break;
			case jobEnDemandeSuppression:
				if (file_exists(jobDeletingDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml')) // en cours d'annulation
				{
					$job->t_job['JOB_ID_ETAT']=jobEnCoursSuppression;
					$job->save();
				}
				else if (file_exists(jobDeletedDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml'))
				{
					//$job->t_job['JOB_ID_ETAT']=jobAnnule;
					//pas d'etat a supprime : suppression du job en base
					$job->delete();
				}
				else if 
				(
					!file_exists(jobDeleteDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml') && 
					!file_exists(jobDeletingDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml') && 
					!file_exists(jobDeletedDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml')
				)
				{
					$job->makeDeleteXml();
				}
				break;
			case jobEnCoursAnnulation:
				if (file_exists(jobCanceledDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml'))
				{
					$job->t_job['JOB_ID_ETAT']=jobAnnule;
					$job->save();
				}
				break;
			case jobEnCoursSuppression:
				// on regarde si le XML de suppression est encore present dans le repertoire jobDeletingDir
				if (file_exists(jobDeletedDir.'/'.$job->t_module['MODULE_NOM'].'/P'.$job->t_job['JOB_PRIORITE'].'_'.str_replace(array('-',' ',':'),'',$job->t_job['JOB_DATE_CREA']).'_'.kDatabaseName.'_'.sprintf('%08d',$job->t_job['ID_JOB']).'.xml'))
				{
					$job->delete();
				}
				break;
		}
	}
    $arrId=array();
                         
    $tab1=$db->GetAll("select j.ID_JOB,j.JOB_ID_ETAT FROM t_job j left join t_job jp on jp.ID_JOB= j.ID_JOB_PREC left join t_job jv on jv.ID_JOB=j.ID_JOB_VALID
        WHERE j.JOB_ID_MODULE!=0 and ((j.JOB_ID_ETAT=0
        and (j.ID_JOB_PREC=0 or (j.ID_JOB_PREC!=0 and jp.JOB_ID_ETAT='".jobFini."'))
        and (j.ID_JOB_VALID =0 or (j.ID_JOB_VALID!=0 and jv.JOB_ID_ETAT='".jobFini."'))))
        $sql_plateforme order by j.JOB_PRIORITE,j.ID_JOB limit 1000");
	//$tab2=$db->GetAll("select j.ID_JOB,j.JOB_ID_ETAT FROM t_job j WHERE j.JOB_ID_MODULE!=0 and j.JOB_ID_ETAT=".intval(jobEnCours).$sql_plateforme." order by j.JOB_PRIORITE,j.ID_JOB limit 1000");
	//$tab3=$db->GetAll("select j.ID_JOB,j.JOB_ID_ETAT FROM t_job j WHERE j.JOB_ID_MODULE!=0 and j.JOB_ID_ETAT=".intval(jobAttente).$sql_plateforme." order by j.JOB_PRIORITE,j.ID_JOB limit 1000");
	
	$modules = $db->GetCol("select id_module from t_module");
	if(!isset($modules ) || empty($modules)){
		trace("updateJobs - crash recup modules");
	}
	
	$tab2 = array();
	$tab3 = array();
	
	foreach($modules as $id_module){
		$arr_en_cours=$db->GetAll("select j.ID_JOB,j.JOB_ID_ETAT FROM t_job j WHERE j.JOB_ID_MODULE=".$id_module." and j.JOB_ID_ETAT=".intval(jobEnCours).$sql_plateforme." order by j.JOB_PRIORITE,j.ID_JOB limit 1000");
		$arr_en_att=$db->GetAll("select j.ID_JOB,j.JOB_ID_ETAT FROM t_job j WHERE j.JOB_ID_MODULE=".$id_module." and j.JOB_ID_ETAT=".intval(jobAttente).$sql_plateforme." order by j.JOB_PRIORITE,j.ID_JOB limit 1000");
		if(!empty($arr_en_cours)){
			$tab2 = array_merge($tab2,$arr_en_cours);
		}
		if(!empty($arr_en_att)){
			$tab3 = array_merge($tab3,$arr_en_att);
		}
		
	}

    foreach ($tab1 as $idx=>&$job) $arrId[]=$job['ID_JOB'];
	unset($job);
	foreach ($tab2 as $idx=>&$job) $arrId[]=$job['ID_JOB'];
	unset($job);
	foreach ($tab3 as $idx=>&$job) $arrId[]=$job['ID_JOB'];
	unset($job);
    unset($tab1);
	unset($tab2);
	unset($tab3);
	// VP 19/7/11 : ajout ID issus des fichiers log
//	$logList=list_directory(jobOutDir,array("log"));
//	foreach($logList as $logFile){
//		$nom_fichier=explode('_',stripExtension(basename($logFile)));
//		if($nom_fichier[0] == kDatabaseName){
//			$id_job=0+$nom_fichier[1];
//			if(!in_array($id_job,$arrId)) {
//				print "Ajout job par fichier : ".$id_job."\n";
//				$arrId[]=$id_job;
//			}
//		}
//	}
	//echo "post ArrID / pre select detail jobs :".microtime(true)."\n";
	if(count($arrId)>0){
		// S�lection des objets correspondants
		$dumb=new Job;
		$arrJob=$dumb->getJob($arrId);
		//echo "post select detail jobs / pre boucle princ : ".microtime(true)."\n";
		unset($arrId);
		unset($dumb);
		unset($tab);
		//debug($arrJob);
		foreach ($arrJob as $idx=>$jobObj) {
			if(isset($jobObj->flag_runSyncJob) && $jobObj->flag_runSyncJob){
				// trace("flag_runSyncJob detecté, non traité par batch/updateJobs.php ".$jobObj->t_job['ID_JOB']);
				continue ; 
			}
			$jobObj->updateJob($arrJob) ; 
		}
	}
	

?>
