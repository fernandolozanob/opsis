<?

require_once(confDir."initialisationGeneral.inc.php");

require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
require_once(modelDir.'model_doc.php');
require_once(modelDir.'model_imageur.php');


	ini_set('memory_limit','256M');
	ini_set('max_execution_time',0);
	
	if(!defined("gDecouperSequence")){
		echo "constante gDecouperSequence non définie dans l'initialisation générale";
		exit ;
	}


	define("gForceRegenVisSequence",true);

	if(defined("gTypeDocSequence")) $doc_id_type_doc = gTypeDocSequence;
	else $doc_id_type_doc = 3;

	$sql="select DOC_ID_GEN from t_doc where doc_id_type_doc=$doc_id_type_doc AND DOC_ID_GEN!=0 GROUP BY DOC_ID_GEN ORDER BY DOC_ID_GEN";
	$rs=$db->Execute($sql);
	$tab=$rs->GetRows();

	$nl="\n";
	
	echo count($tab)." documents ont des séquences à traiter".$nl.$nl;
	
	$array_root_doc = array();
	foreach($tab as $res){
		echo "lancement génération des séquences rattachées au document ".$res['DOC_ID_GEN'].$nl;
		$myDoc = new Doc();
		$myDoc->t_doc['ID_DOC'] = $res['DOC_ID_GEN'];
		$myDoc->getDoc();
		$myDoc->getSequences();
		$myDoc->getMats();
		foreach($myDoc->t_doc_mat as $idx=>$dmat){
			if($dmat['MAT']->t_mat['MAT_TYPE'] == 'VISIO'){
				$mat_visio = $dmat['MAT'];
				break;
			}
		}
		
		if(!empty($mat_visio) && file_exists($mat_visio->getlieu().$mat_visio->t_mat['MAT_NOM'])){
			$myDoc->saveSequences();
		}else{
			echo "!!! matériel ou fichier de visionnage non trouvé !!! ".$nl;
		}
		
		unset ($myDoc);
		unset ($mat_visio);
	}
	
	
?>
