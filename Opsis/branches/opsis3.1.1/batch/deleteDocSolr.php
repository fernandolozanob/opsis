<?php

/*
 Suppression des documents dans Solr qui ne sont pas présents dans la base postgre/mysql
*/
require_once(modelDir.'model_doc.php'); 	

global $db;

header('Content-type: text/plain');

ini_set('max_execution_time','0');

$timestart=microtime(true);

if (defined("useSolr") && useSolr==true){
    $solr_opts = unserialize(kSolrOptions);
    $solr_opts['timeout']=300;
    $client_solr=new SolrClient($solr_opts);
	$query_solr=new SolrQuery();
    $query_solr->setQuery('*:*');
    $query_solr->addField('id');
    $query_solr->addField('id_doc');
    $query_solr->addField('id_lang');
    
    $nb_doc_groupe=10000;
    $end=true;
    $i=0;
    $objDoc=new Doc();
    $arrDocToDelete=array();
    while($end)
    {
        $query_solr->setRows($nb_doc_groupe);
        $query_solr->setStart($nb_doc_groupe*$i);
        $response=$client_solr->query($query_solr);
        if($i==0) {
            $numFound=$response->getResponse()->response->numFound;
            echo($numFound." trouvés \n");
        };
        echo "start=".$nb_doc_groupe*$i." count=".count($response->getResponse()->response->docs)." docs \n";
        foreach ($response->getResponse()->response->docs as $res){
            $objDoc->t_doc['ID_DOC']=intval($res['id_doc']);
            $objDoc->t_doc['ID_LANG']=$res['id_lang'];
            if(!$objDoc->checkExistId()){
                $arrDocToDelete[]=$res['id'];
            }
        }
        $i++;
        if($i*$nb_doc_groupe>$numFound) $end=false;
    }
    echo count($arrDocToDelete)." docs solr à supprimer";
    
    if(count($arrDocToDelete)>10000){
        echo "Trop de documents à supprimer : ".count($arrDocToDelete)." : vérifier...\n";
    }elseif(count($arrDocToDelete)>0){
        $ids=implode(" OR ",$arrDocToDelete);
        $url = "http://".$solr_opts['hostname'].":".$solr_opts['port']."/".$solr_opts['path']."/update?stream.body=<delete><query>id:(".$ids.")</query></delete>&commit=true";
        echo("curl delete : ".$url."\n");
        trace("curl delete : ".$url);
//    $curl = curl_init($url);
//    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//    $return = curl_exec($curl);
//    if($return){
//        trace("curl delete OK");
//    }else{
//        trace("curl delete failed");
//    }
//    
//    curl_close($curl);
    }

}



echo "Script execute en " . (microtime(true)-$timestart) . " sec";

?>