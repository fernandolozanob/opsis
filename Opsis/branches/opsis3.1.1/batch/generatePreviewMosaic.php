<?php

// require_once(confDir.'/conf.inc.php');
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
require_once(modelDir.'model_materiel.php');
require_once(modelDir.'model_imageur.php');

global $db;

	ini_set('memory_limit','256M');
	ini_set('max_execution_time',0);

    if(isset($argv[2]) && !empty($argv[2])) {
        $sqlCritere = $argv[2];
    }else{
        $sqlCritere = "";
    }

	$sql="SELECT distinct ID_MAT FROM t_mat WHERE MAT_ID_MEDIA='V' AND MAT_TYPE='VISIO' AND MAT_ID_IMAGEUR IS NOT NULL $sqlCritere ORDER BY ID_MAT";
	$rs=$db->Execute($sql);
	$tab=$rs->GetRows();
	
	$nl="\n";
	
	echo count($tab)." matériel(s) à traiter".$nl;

	foreach($tab as $id_mat){
		$start = microtime(true);
		echo "id_mat : ".$id_mat['ID_MAT']."\t=>\t";
		$tmpMat = new Materiel();
		$tmpMat->t_mat['ID_MAT']=$id_mat['ID_MAT'];
		$tmpMat->getMat();
		$tmpImageur=new Imageur();
		$tmpImageur->t_imageur['ID_IMAGEUR'] = $tmpMat->t_mat['MAT_ID_IMAGEUR'];
		$tmpImageur->generateMosaicFromMat($tmpMat);
		$msg=$tmpImageur->error_msg;
		if($msg!=''){
			echo "erreur : ".$msg;
		}else{
			echo "traitement terminé ";
		}
		unset($tmpMat);
		unset($tmpImageur);
		unset($msg);
		$exec_time = microtime(true)-$start;
		echo ", ".$exec_time." secs".$nl;
	}
	echo "Fin batch generatePreviewMosaic".$nl;

