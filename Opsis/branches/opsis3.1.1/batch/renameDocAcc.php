<?
require_once(modelDir.'model_docAcc.php');

header('Content-type: text/plain');

global $db;
$result = $db->Execute("SELECT ID_DOC_ACC, DA_FICHIER from t_doc_acc ORDER BY DA_FICHIER ASC");
echo "***Rangement des documents d'accompagnement***\n\n";


while ($acc = $result->FetchRow()){
    $id = intval($acc['ID_DOC_ACC']);
    $folder = DocAcc::getFolderName($id);
	if (is_file(kDocumentDir.$acc['DA_FICHIER'])) {
		rename(kDocumentDir.$acc['DA_FICHIER'], kDocumentDir.$folder.$acc['DA_FICHIER']);
		$db->execute("UPDATE t_doc_acc SET DA_CHEMIN = '$folder' WHERE ID_DOC_ACC = $id");
		echo $acc['DA_FICHIER']." moved to $folder\n";
	}elseif (is_file(kDocumentDir.$folder.$acc['DA_FICHIER'])){
		$db->execute("UPDATE t_doc_acc SET DA_CHEMIN = '$folder' WHERE ID_DOC_ACC = $id");
    } else {
    	$rsDocAcc = $db->GetAll("SELECT ID_DOC_ACC, DA_FICHIER, DA_CHEMIN from t_doc_acc WHERE DA_FICHIER like ".$db->Quote($acc['DA_FICHIER'])." and (DA_CHEMIN != '' and DA_CHEMIN IS NOT NULL)");
    	$folder = $rsDocAcc[0]['DA_CHEMIN'];
    	unset($rsDocAcc);
    	$db->execute("UPDATE t_doc_acc SET DA_CHEMIN = '$folder' WHERE ID_DOC_ACC = $id");
    }
}

echo "\n\n***END***\n";
exit;

?>