<?php

// MS - Batch permettant la mise à jour d'une base avec les passwords en clair vers une base avec les mots de passes cryptés.

require_once(modelDir.'model_usager.php');

echo ("batch updatePasswordToHash"."\n");

global $db;

// récupération de la liste des usagers
$sql_get_usagers = "SELECT ID_USAGER,US_LOGIN,US_PASSWORD,US_SALT from t_usager ORDER BY ID_USAGER ASC";
$rs = $db->Execute($sql_get_usagers);
echo ("nb users : ".$rs->RecordCount()."\n");

$cnt_upd = 0 ;
$cnt_not = 0 ;
if(!empty($rs)){
	while($row = $rs->FetchRow()){
		// pour chaque usager, si on a pas encore de salt, c'est a priori qu'on a pas encore de password hashed
		if(empty($row['US_SALT'])){
			$usr = new Usager() ;
			$usr->t_usager['ID_USAGER'] = $row['ID_USAGER'];
			// 01 04 2016 - on redéfini uniqueFields pour éviter le control checkExist (dans save ci-dessous) sur us_soc_mail pour gérer le cas de plusieurs comptes avec la meme adresse mail. le batch sert a mettre a jour les mdp et on ne souhaite pas qu'il échoue sur des cas particuliers comme plusieurs utilisateurs ayant la meme adresse.
			$usr->uniqueFields = array("US_LOGIN");
			$usr->getUsager();

			// génération du salt
			$strong = false ;
			while (!$strong && strlen($usr->t_usager['US_SALT']) != 64 ){
				$usr->t_usager['US_SALT'] = base64_encode(openssl_random_pseudo_bytes(48,$strong));
			}

			// la génération du hash se fait automatiquement à partir du password courant & du salt dès lors qu'on set le premier argument de save, $new_passw, à true
			$test=$usr->save(true);
			$cnt_upd++ ;
			// $old_pw = $usr->t_usager['US_PASSWORD'] ;
			// $usr->t_usager['US_PASSWORD'] =  getPasswordHash($usr->t_usager['US_PASSWORD'],$usr->t_usager['US_SALT']);
			// echo "user ".$row['ID_USAGER']." old pw : ".$old_pw." new pw ".$usr->t_usager['US_PASSWORD']." new salt ".$usr->t_usager['US_SALT']."\n\n";
		}else{
			$cnt_not++ ;
			// echo "user ".$row['ID_USAGER']." deja hashed (salt présent)"."\n\n";
		}
	}
	echo ("fin updatePasswordToHash ".$cnt_upd." usr updaté, ".$cnt_not." usr inchangés\n");
	trace("fin updatePasswordToHash ".$cnt_upd." usr updaté, ".$cnt_not." usr inchangés");

}




?>