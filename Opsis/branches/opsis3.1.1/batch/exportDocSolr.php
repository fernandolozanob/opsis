<?php

// require_once('../conf/conf.inc.php');
// require_once(libDir."session.php");
// require_once(libDir."fonctionsGeneral.php"); 	
require_once(modelDir.'model_doc.php'); 	

global $db;

header('Content-type: text/plain');

ini_set('max_execution_time','0');

$timestart=microtime(true);

if(!empty($argv[2])) {
	$sqlCritere =  " where ".$argv[2];
} else {
	$sqlCritere = "";
}
$result=$db->Execute('SELECT COUNT(*) AS COMPTE FROM t_doc '.$sqlCritere)->GetRows();
$total_nb_doc=$result[0]['COMPTE'];

// import des documents par groupe de 10000
$nb_doc_groupe=10000;

for ($i=0;$i<ceil($total_nb_doc/$nb_doc_groupe);$i++)
{
	echo ($i*$nb_doc_groupe).' - '.(($i+1)*$nb_doc_groupe).' => ';
	$result=$db->Execute('SELECT ID_DOC,ID_LANG FROM t_doc '.$sqlCritere.' ORDER BY ID_DOC LIMIT '.$nb_doc_groupe.' OFFSET '.($i*$nb_doc_groupe))->GetRows();
	echo count($result).' document a importer.'."\n";
	
	foreach ($result as $idx=>$res)
	{
		$doc=new Doc();
		$doc->t_doc['ID_DOC']=intval($res['ID_DOC']);
		$doc->t_doc['ID_LANG']=$res['ID_LANG'];
		$doc->getDoc();
		$doc->saveSolr();
		echo $doc->t_doc['ID_DOC'].','.$doc->t_doc['ID_LANG'].' => '.$doc->t_doc['DOC_TITRE']."\n";
	}

}


$timeend=microtime(true);
$time=$timeend-$timestart;

echo "Script execute en " . $time . " sec";

?>
