<?php

require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
require_once(modelDir.'model_materiel.php');

global $db;

if (!empty($argv[2]) && is_numeric($argv[2]) ) {
	$id_mats = array(array("ID_MAT" => $argv[2]));
} else {
	if(!empty($argv[2])) {
		$sqlCritere =  $argv[2];
	} else {
		$sqlCritere = "";
	}
	$id_mats=$db->Execute('SELECT ID_MAT FROM t_mat WHERE 1=1 '.$sqlCritere)->getRows();
}


foreach ($id_mats as $id_mat)
{
	$mat=new Materiel();
	$mat->t_mat['ID_MAT']=intval($id_mat['ID_MAT']);
	$mat->getMat();
	$mat->id_mat_ref = $mat->t_mat['MAT_NOM'];
	
	if(file_exists($mat->getLieu().$mat->t_mat['MAT_NOM'])){
		
		$mat->updateMatInfo($infos) ; 
		$mat->createFormatMat() ; 
		$type_mime=explode('/',$mat->t_mat['MAT_TYPE_MIME']);
		$mat->save();
		
		//Mise a jour des TC dans t_doc_mat 
		$rs = $db->Execute("SELECT * FROM t_doc_mat WHERE (DMAT_TCOUT is null OR DMAT_TCOUT like '00:00:00%') AND ID_MAT=".intval($mat->t_mat['ID_MAT']));
		$sql = $db->GetUpdateSQL($rs, array("DMAT_TCIN" => $mat->t_mat['MAT_TCIN'], "DMAT_TCOUT" => $mat->t_mat['MAT_TCOUT']));
		if (!empty($sql)) $ok = $db->Execute($sql);
		
		if ($type_mime[0]=='video')
		{
			$mat->getMatTrack();
			$mat->saveMatTrack();
		}
		echo "OK : ".$mat->t_mat['ID_MAT']." ".$mat->t_mat['MAT_NOM']." infos media updatées<br />\n";
	}else{
		echo "NOK : ".$mat->t_mat['ID_MAT']." ".$mat->t_mat['MAT_NOM']." infos inchangées, fichier introuvable (".$mat->getLieu().$mat->t_mat['MAT_NOM'].")<br />\n";
	}
	$mat->updateEtatArch();
	unset($mat);

}

?>