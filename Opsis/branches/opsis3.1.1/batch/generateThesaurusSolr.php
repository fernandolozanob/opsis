<?php

// require_once('../conf/conf.inc.php');
// require_once(libDir."session.php");
// require_once(libDir."fonctionsGeneral.php");

require_once(modelDir.'model_lexique.php');

ini_set('memory_limit','4096M');
ini_set('max_execution_time','0');
header('Content-type: text/plain');

global $db;

function getTermesEnfants($lex,$indent=0)
{
	$lex->getChildren();
	
	$indent++;
	
	$liste='';
	
	foreach ($lex->arrChildren as $child)
	{
		if ($child['ID_LEX']!=null && $child['ID_LEX']!=0)
		{
			$enfant=new Lexique();
			$enfant->t_lexique['ID_LEX']=$child['ID_LEX'];
			$enfant->t_lexique['ID_LANG']=$lex->t_lexique['ID_LANG'];
			$enfant->getLexique();
			
			$str_enfants=getTermesEnfants($enfant,$indent);
			
			if (!empty($liste))
				$liste.=', ';
			
			if (empty($str_enfants))
				$liste.=$enfant->t_lexique['LEX_TERME'];
			else
				$liste.=$enfant->t_lexique['LEX_TERME'].', '.$str_enfants;
		}
	}
	
	return $liste;
}


// gestion des synonymes
$result=$db->Execute('SELECT t_lexique.id_lex,t_lexique.id_lang,t_lexique.lex_terme,
		lexique_syn.id_lex as id_syn,lexique_syn.id_lang as lang_syn,lexique_syn.lex_terme as terme_syn
FROM t_lexique 
	LEFT JOIN t_lexique AS lexique_syn ON t_lexique.lex_id_syn=lexique_syn.id_lex AND t_lexique.id_lang=lexique_syn.id_lang
WHERE t_lexique.lex_id_syn IS NOT NULL 
	AND t_lexique.lex_id_syn<>0')->getRows();

foreach ($result as $synonyme)
{
	if ($synonyme['ID_SYN']!=null)
		echo $synonyme['LEX_TERME'].', '.$synonyme['TERME_SYN'].' => '.$synonyme['TERME_SYN']."\n";
}
/*
echo "# Thesaurus hierarchique\n";
// ajouter gestion synonymes dans les champs de type general FAIT
// idem dans les champs string A FAIRE
// thesaurus hierarchique
$result=$db->Execute('SELECT id_lex,id_lang FROM t_lexique')->getRows();
foreach($result as $arr_lex)
{
	$parent=new Lexique();
	$parent->t_lexique['ID_LEX']=$arr_lex['ID_LEX'];
	$parent->getLexique(null,$arr_lex['ID_LANG']);
	$enfants=getTermesEnfants($parent);
	
	if (!empty($enfants))
        {
        // Limitation à 500 termes
        $arrChildren=explode( ',', $enfants);
        if(count($arrChildren)>500)
                     $enfants=implode(',',array_slice($arrChildren,0,500));
		echo ''.$parent->t_lexique['LEX_TERME'].' => '.$enfants."\n";
        }
}*/
?>