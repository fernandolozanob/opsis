<?
/**
 *  Classe Controller_val
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(libDir."class_chercheVal.php");
require_once(modelDir."model_val.php");
require_once(libDir."class_page.php");

class Controller_val extends Controller {
    
    /** Liste valeurs
     * IN : GET, POST
     * OUT : objet cherche
     */
    function do_valListe($args=null) {
		$mySearch=new RechercheVal();
        
        $myPage=Page::getInstance();
        
		$mySearch->affichage = "LISTE";
		if (isset($_REQUEST['affichage'])) {
			$mySearch->affichage = $_REQUEST['affichage'];
		} elseif (isset($_SESSION[$mySearch->sessVar]['affichage'])) {
			$mySearch->affichage = $_SESSION[$mySearch->sessVar]['affichage'];
		}
        
        if (!isset($_SESSION[$mySearch->sessVar])) {
            $mySearch->tab_recherche[2]=array('FIELD'=>'TYPE_VAL',
                                              'VALEUR'=>'REL',
                                              'TYPE'=>'CI',
                                              'OP'=>'AND',
                                              'LIB'=>kType);
        }
		$this->init_list($mySearch, "F_val_form", kGestionValeurs, "val");
        
        if($mySearch->affichage=='HIERARCHIQUE'){
            // Préparation affichage hiérachique
            require_once(libDir."class_tafelTree.php");
            $mySearch->treeParams=array("ID_TYPE_VAL"=>$mySearch->getValueForField(array('FIELD'=>'VAL_ID_TYPE_VAL','TYPE'=>'CE')));
            $btnEdit=$myPage->getName()."?urlaction=valSaisie&id_val=";
            $myAjaxTree=new tafelTree($mySearch,'form1');
            $myAjaxTree->JSReturnFunction=$jsFunction;
            
            $myAjaxTree->makeRoot();
            $myAjaxTree->JSlink="window.location.href='".$myPage->getName()."?urlaction=valSaisie&id_val=%3\$s'"; //Le lien sur les terme ouvrira l'édition
            //Récupération du type de recherche
            $title=GetRefValue('t_type_val',$mySearch->getValueForField(array('FIELD'=>'VAL_ID_TYPE_VAL','TYPE'=>'CE')),$_SESSION['id_lang']);
            
            
            if ($mySearch->getValueForField(array('FIELD'=>'VALEUR','TYPE'=>'FT'))!=''
                || $mySearch->getValueForField(array('FIELD'=>'VALEUR','TYPE'=>'C'))!='' ) { //En plus, on a une recherche sur un terme
                if (!empty($mySearch->result)) {
                    foreach ($mySearch->result as $row) { //Pour chaque résultat...
                        $myAjaxTree->revealNode($row['ID_VAL']);
                    }
                }
                $this->params['nb_results'] = $mySearch->found_rows;
            }
            $this->params['ajaxTree'] = $myAjaxTree;
        }
        
    }
    
    
    /** Verif valeur
     * IN : GET, POST, objet val
     * OUT : objet val
     */
    function do_valExists($args=null) {
        
        $mode=$_REQUEST['mode'];
        $from=$_REQUEST['from'];
        
        $myVal=new Valeur();
        $myVal->t_val['ID_VAL'] = $_REQUEST['id_val'];
        $myVal->t_val['ID_LANG'] = ($_REQUEST['id_lang']?$_REQUEST['id_lang']:$_SESSION['langue']);
        $myVal->t_val['VAL_ID_TYPE_VAL'] = $_REQUEST['val_id_type_val'];
		$myVal->t_val['VAL_ID_FONDS'] = $_REQUEST['val_id_fonds'];
        $myVal->t_val['VALEUR'] = $_REQUEST['value'];
        
        $idExists = $myVal->checkExist();
        if (!$idExists)
            $str='<ok/>';
        else
            $str ="<t_val>\n\t<ID_VAL>".$idExists."</ID_VAL>\n\t<FROM>".$from."</FROM>\n\t<MODE>".$mode."</MODE>\n\t<VALEUR>".$myVal->t_val['VALEUR']."</VALEUR>\n</t_val>";
        
        $this->params['content'] = $str;
        $this->params['contentTemplate'] = getSiteFile("designDir","templates/content/xml.html");
    }
    
    
    /** Saisie valeur
     * IN : GET, POST, objet val
     * OUT : objet val
     */
    function do_valSaisie($args=null) {

        $myPage=Page::getInstance();
        $myPage->nomEntite = kValeur;

        // update VG 11/06/10 : pour l'affichage de la liste des documents liés
        $docLiesUseMysql = '1';

        $myVal=new Valeur();

        if(!empty($_REQUEST["id_val"])){
            $myVal->t_val['ID_VAL'] = intval($_REQUEST["id_val"]);
        }elseif(!empty($myPage->id)){
            $myVal->t_val['ID_VAL'] = intval($myPage->id);
        }

        if (!isset($_REQUEST['commande']))
            $_REQUEST['commande']='';

        switch ($_REQUEST['commande']) {
            case "SAVE":  // Sauvegarde après modif infos

                $myVal->updateFromArray($_POST);
                
                $ok=$myVal->save(); // renvoie 'crea' si on crée, rien si on update
                
				if ($ok){
					// VP 9/11/2017 : création version à la volée si nécessaire
					if(isset($_POST['version'])){
						$valVersions = $_POST['version'];
					} else {
						$valVersions = array();
						$aLang = getOtherLanguages();
						if(count($aLang)>0){
							foreach ($aLang as $lang) {
								$valVersions[$lang] = $myVal->t_val['VALEUR'];
							}
						}
					}
					$myVal->saveVersions($valVersions);
					
					//VP 30/6/18 : déplacement appel updateDocChampsXML depuis méthode save
					$myVal->updateDocChampsXML(true);
				}
				// YT_flag
				// ce flag identifie que la valeur est du type "playlist youtube" et indique qu'on doit mettre à jour la playlist youtube 
				if(isset($_POST['update_pl_yt'])){
					$infos =json_decode($_POST['update_pl_yt'],true);
					if(isset($infos['id'])){
						$id_val = $infos['id'];
					}else{
						$id_val = $myVal->t_val['ID_VAL'];
					}
				
					if(isset($infos['etape_log'])  && isset($infos['type_val']) ){
						try{
							require_once(libDir."class_youtube.php");
							$youtube = new Youtube() ; 
							$youtube->getLogParamsFromEtape($infos['etape_log']);
							$youtube->setTypeValPl($infos['type_val']);
							$youtube->connectYt();
							$youtube->export_val_to_playlist_yt($id_val);
						}catch(exception $e ){
							trace("fail update playlist youtube id_val : ".$id_val.", exception:".$e);
						}
					}
				}

				//by LD 03/010/08 pour ajouter le nom à la redirection => du coup le retour vers la liste filtre automatiquement sur le terme sauvé
				if ($_REQUEST['fldToPassToRedirection']) $additPrm="&".$_REQUEST['fldToPassToRedirection']."=".urlencode($myVal->t_val["VALEUR"]);			
				if ($myPage->hasReferrer() && $ok) $myPage->redirect($myPage->getReferrer().$additPrm);
				

            
            break;
            
            case "SUP": // Supression
            case "SUPPR": // Retrocompatibilité
                // VP 10/3/10 : Suppression par commande SUP au lieu de SUPPR
                $myVal->updateFromArray($_POST);
                
                // YT_flag
                // ce flag identifie que la valeur est du type "playlist youtube" et indique qu'on doit mettre à jour la playlist youtube 
                if(isset($_POST['update_pl_yt'])){
                    $infos =json_decode($_POST['update_pl_yt'],true);
                    if(isset($infos['id'])){
                        $id_val = $infos['id'];
                    }else{
                        $id_val = $myVal->t_val['ID_VAL'];
                    }
                    $val_code = $myVal->t_val['VAL_CODE'];
                    
                    if(isset($infos['etape_log'])  && isset($infos['type_val']) && isset($val_code) ){
                        try{
                            require_once(libDir."class_youtube.php");
                            $youtube = new Youtube() ; 
                            $youtube->getLogParamsFromEtape($infos['etape_log']);
                            $youtube->setTypeValPl($infos['type_val']);
                            $youtube->connectYt();
                            $youtube->youtube->playlists->delete($val_code);
                        }catch(exception $e){
                            trace("fail suppression playlist youtube id : ".$val_code.", exception:".$e);
                        }
                    }
                }
                //update VG 12/10/11 : màj du chap DOC_XML
                $myVal->getDocs(false);
                if ($myVal->delete() && $myVal->detachChildren()) {
                    $myVal->updateDocChampsXML(true);
                    if($myPage->hasReferrer())
                        $myPage->redirect($myPage->getReferrer());
                }
                
            break;
            
            case "DETACH_FILS": //Détachage des valeurs filles attachées
                $myVal->updateFromArray($_POST);
                $myVal->detachChildren();
            break;
            
            case "DETACH_DOC" : //Rupture du lien avec doc, pers et mat
            case "DETACH_PERS" :
            case "DETACH_MAT":
                $myVal->updateFromArray($_POST);
                $entity=split('_',$_REQUEST['commande']);
                $myVal->detachLink($entity[1]);
            break;
            
            case "FUSION" :
                $myVal->updateFromArray($_POST);
                $myVal->fusion($_POST['fusionVal']); // fusion : ce terme remplace l'ancien (note : fonction inverse / v1)
            break;
            
            case "": // pas d'action, on préinitialise le type avec celui issu du get (cas de création d'une nv val)
            
                $myVal->updateFromArray($_POST);
                $myVal->t_val['VAL_ID_TYPE_VAL']=$_GET['val_id_type_val'];
            break;

        }


        if(!empty($myVal->t_val['ID_VAL'])){
            $myVal->getVal();
            $myVal->getVersions();
            $myVal->getDocs();
            $myVal->getPers();
            $myVal->getMats();	
            $myVal->getChildren();
        }


        $myVal->type_val=GetRefValue("t_type_val",$myVal->t_val["VAL_ID_TYPE_VAL"]);
        
		$this->init_editParam($myVal, (isset($myVal->t_val['VALEUR']) && !empty($myVal->t_val['VALEUR'])?$myVal->t_val['VALEUR']:''));
    }
}

?>
