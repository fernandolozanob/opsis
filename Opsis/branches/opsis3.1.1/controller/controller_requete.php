<?
/**
 *  Classe Controller_html
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(libDir."class_chercheRequete.php");
require_once(modelDir.'model_requete.php');
require_once(libDir."class_page.php");

class Controller_requete extends Controller {

	/** Liste des recherches effectu�es
	* IN : GET, POST
	* OUT : objet cherche
	* @param string $args
	*/
	function do_requeteListe($args=null) {
		
		$entity = 'requete';
		$myPage=Page::getInstance();
		$this->params['setReferrer']= true ; 

		//TODO : déplacer cela dans une autre action
		// -------
		// supression d'une requete de l'historique
		// -------
		if (isset($_GET['suppr']) && !empty($_GET['suppr']))
		{
			if($_GET['suppr']=="all"){
				Requete::delete_all_histo();
			}else{
				$id_suppr=intval($_GET['suppr']);
		
				if (!empty($id_suppr) && $id_suppr!=0)
				{
					$requete=new Requete();
					$requete->t_requete['ID_REQ']=$id_suppr;
					$requete->deleteRequete();
				}
			}
		}
		
		//si fusion des requetes
		if (isset($_POST['valider_form_croisement']) && !empty($_POST['valider_form_croisement']))
		{
			//trace('passage controller_requete : header valider_form_croisement');
			header('Location: index.php?urlaction=docListe&croiserRequete='.serialize($_POST['checkbox_requete']).'&operateur='.$_POST['operateur'].(isset($_REQUEST['tri'])?'&tri='.$_REQUEST['tri']:''));
		}
		

		//TODO : déplacer cela dans une autre action
		// -------
		// sauvegarde d'une requete de l'historique
		// -------
		if (isset($_POST['REQUETE']) && $_POST['REQUETE']) {
			trace('passage controller_requete : $_POST["REQUETE"]');
			$myQuery=new Requete;
			$myQuery->t_requete['ID_REQ']=$_POST['REQUETE']['ID_REQ'];
			$myQuery->getRequete();
			$myQuery->t_requete['REQUETE']=$_POST['REQUETE']['REQUETE'];
			$myQuery->t_requete['REQ_ALERTE']=$_POST['REQUETE']['REQ_ALERTE'];
			$myQuery->t_requete['REQ_SAVED']=1;
			$myQuery->save();
		
		}
		
		$mySearch=new RechercheRequete();
		
		if(!empty($_REQUEST['type_requests']))
			$type_requests = explode('_', $_REQUEST['type_requests']); //valeurs possibles (et concaténées par "_" ) : session saved
		else 
			$type_requests = array( 'session' , 'saved');
		
		$mySearch->set_requestsType($type_requests);
		if(empty($_REQUEST['tri'])) {
			$_REQUEST['tri'] = 'ID_REQ1';
		}
		$this->params['params4XSL']['type_requests'] = implode('_', $type_requests);
		$this->init_list($mySearch, "F_requete_form", kHistorique, "requete");
		
	}
}

?>
