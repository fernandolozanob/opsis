<?
/**
 *  Classe Controller
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */

class Controller {
    var $params;
    var $page;
    var $nbLignes;
    
    function __construct() {
    }


    function execute($methode, $args) {
		$urlaction = $args['urlaction'];

        if (empty($urlaction) || $this->getAccess(User::getInstance()->getTypeLog(),$urlaction) || ($args['guestPartageAccess'] && $this->getAccess('guestPartage',$urlaction)) || $methode == 'do_lock') {
            if (method_exists($this, $methode)) {
                $this->{$methode}($args);
            }
        } else {
        	// Si pas de droit d'accès, message.
        	$this->dropError("<h3 class='error'>".kAccesLogin."</h3>");
        }
    }

    function do_action($args) {
        $urlaction = $args['urlaction'];
        $includePath = $args['includePath'];
        $params4XSL = $args['params4XSL'];
        
        // Compatibilité dossiers include, frame, etc
        if (substr($urlaction,-4)==".php") {
            $urlaction=substr($urlaction,0,-4);
            trace("Attention : nom de page PHP dans l'url :".$urlaction.".php");
        }
        
        if ( !empty($urlaction) ) {
            if (file_exists($includePath.$urlaction.".php")) {
                
                // Si pas de droit d'accès, message.
                if ($this->getAccess(User::getInstance()->getTypeLog(),$urlaction) || ($args['guestPartageAccess'] && $this->getAccess('guestPartage',$urlaction))) {
                    $incfile=$includePath.$urlaction.".php";
                    $error="";
                }
                else {
                	$error="<h3 class='error'>".kAccesLogin."</h3>";$incfile="";
                }
            }
            else { //tentative d'accès à une page qui n'existe pas
                $error="<h3 class='error'>".kErreurPageNotFound."</h3>"; $incfile="";
            }
        } else { //pas d'action, ou pas de user : on affiche la page par déft : design/index.php
            $incfile=getSiteFile("designDir","index.php");
        }
        
        if (!empty($error)) {
            $this->dropError($error);
        } elseif (!empty($incfile)) {
            $this->includeFile($incfile);
        }
        
    }
    
    function do_lock($args) {
        switch ($args['locked']){
            case 'captchaGate' :
                $incfile = getSiteFile('designDir','validHuman.php');
                break ;
            case 'loggedOnlyGate' :
                $incfile = getSiteFile('designDir','loginModal.php');
                if(!empty($args['urlaction'])) {
                	$this->dropError("<h3 class='error'>".kAccesLogin."</h3>");
                }
                break ;
        }
        $this->includeFile($incfile);
    }
    
    function do_edit() {
    }
    
    function do_list() {
    }

    function do_show() {
    }
	
    function init_list($mySearch, $nom_post_form, $titre, $entity, $tabDeftOrder=array(),$prefix=null) {
		global $db;
        
        $myPage=Page::getInstance();
		
		if(!isset($this->params['setReferrer'])){ // ce if permet d'autoriser ce paramétrage à arriver en dehors de cette fonction. 
			$this->params['setReferrer']['queue'] = true ; 
		}
		if (function_exists($myPage->urlaction.'_custom')){
			$func_customSearch = $myPage->urlaction.'_custom' ; 
		}
        
        if (!isset($_SESSION[$mySearch->sessVar])) {
            $mySearch->prepareSQL();
            $mySearch->makeSQL();
			if(isset($func_customSearch)){
				$func_customSearch($mySearch);
			}
            $mySearch->finaliseRequete();
        }
        
        if(isset($_POST[$nom_post_form])) {
            //Recherche par formulaire
            $mySearch->prepareSQL();
            $mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
			$mySearch->appliqueDroits();
			if(isset($func_customSearch)){
				$func_customSearch($mySearch);
			}
            $mySearch->finaliseRequete(); //fin et mise en session
            
        } elseif(isset($_GET["init"])) {
            
            $mySearch->initSession();
            
        } else {
            $mySearch->getSearchFromSession();
        }
        
        // Paramètres affichage
        $nbLignesDefaut = (defined("gNbLignesListeDefaut")?gNbLignesListeDefaut:10);
        $mySearch->setNbLignes($_REQUEST['nbLignes'], $nbLignesDefaut);
        $mySearch->setPage($_REQUEST['page']);
		if (isset($_REQUEST['orderbydeft']) && $_REQUEST['orderbydeft']) {
            $deft_field = $_REQUEST['orderbydeft'];
        }
        $mySearch->setOrderBy($_REQUEST['tri'],(isset($deft_field)?$deft_field:array()));
        $mySearch->setAffichage($_REQUEST['affichage']);
        
        // Exécution requête
        $mySearch->execute();
        //trace(print_r($mySearch, true));
        
        // Stockage session
        $_SESSION[$mySearch->sessVar]['page'] = $mySearch->page;
        $_SESSION[$mySearch->sessVar]['nbLignes'] = $mySearch->nbLignes;
        $_SESSION[$mySearch->sessVar]['tabOrder'] = $mySearch->tabOrder;
        
        // Paramètres pour view
        $this->params['searchObj'] = $mySearch;
        $this->params['title'] = $titre;
        $this->params['entity'] = $entity;
        $this->params['nomElement'] = "t_".$entity;

        if(empty($prefix))
            $prefix=$entity;

        if (file_exists(designDir."templates/content/".$prefix."Liste.html"))
            $this->params['contentTemplate'] = designDir."templates/content/".$prefix."Liste.html";
        else
            $this->params['contentTemplate'] = getSiteFile("designDir","templates/content/deftListe.html");
		$xslFile = getSiteFile("listeDir",$prefix."Liste.xsl");
        if (file_exists($xslFile) && basename($xslFile) != 'index.php')
            $this->params['xslListFile'] = getSiteFile("listeDir",$prefix."Liste.xsl");
        else
            $this->params['xslListFile'] = getSiteFile("listeDir","deftListe.xsl");
        $this->params['xmlFormFile'] = getSiteFile("designDir","form/xml/cherche".ucfirst($prefix).".xml");
        $this->params['xmlListFile'] = getSiteFile("listeDir","xml/".$prefix."Liste.xml");
    }
	
	function init_baseParam($obj,$titre="") {
		$this->params['modelObj'] = $obj;
        $this->params['entity'] = $obj->entity_code;
        $this->params['title'] = $titre;
        $this->params['id'] = $obj->{$obj->table}[$obj->key];
	}

    function init_editParam($obj,$titre="") {
		$this->init_baseParam($obj,$titre);
		$entity_filename_format = $obj->getEntityFilenamePart();
        $this->params['contentTemplate'] = getSiteFile("designDir","templates/content/".$entity_filename_format."Saisie.html");
        $this->params['xmlFormFile'] = getSiteFile("designDir","form/xml/".$entity_filename_format."Saisie.xml");
	}
	
    function init_showParam($obj,$titre="", $content="") {
		$this->init_baseParam($obj,$titre);
		if (!empty($content))
			$this->params['content'] = $content;
		else {
			$entity_filename_format = $obj->getEntityFilenamePart();
			$this->params['contentTemplate'] = getSiteFile("designDir","templates/content/".$entity_filename_format."Aff.html");
			if (preg_match('#index.php$#',$this->params['contentTemplate'])!=0){
				$this->params['contentTemplate'] = getSiteFile("designDir","templates/content/deftAff.html");
			}
            if ($filexsl=getSiteFile("designDir",$entity_filename_format."Aff.xsl") && file_exists($filexsl) && basename($filexsl) != 'index.php'){
				$this->params['xslFile'] = $filexsl;
            }else{
				$this->params['xslFile'] = getSiteFile("designDir","detail/".$entity_filename_format."Aff.xsl");
			}
			if (preg_match('#index.php$#',$this->params['xslFile'])!=0){
				$this->params['xslFile'] = getSiteFile("designDir","detail/deftAff.xsl");
			}
            $this->params['xmlFile'] = getSiteFile("designDir","detail/xml/".$entity_filename_format."Aff.xml");
		}
	}
	
	function dropError($error) {
        $this->params['content'] = $error;
        $this->params['contentNoHtml'] = strip_tags($error);
    }
    
    function getAccess($type, $urlaction) {
        if(empty($urlaction)) {
            return false;
        }
		if(function_exists("checkAccessCustom")){
			return checkAccessCustom($type, $urlaction);
		}
        global $adminPages,$docPages,$usagerPages,$loggedPages,$usagerAdvPages,$tabPages;
        
        switch ($type) {
            case kLoggedAdmin : $pages=$adminPages;break;
            case kLoggedDoc : $pages= $docPages;break;
            case kLoggedUsagerAdv : $pages = $usagerAdvPages;break;
            case kLoggedNorm : $pages = $usagerPages;break;
            default :
                if(isset($tabPages[$type])) $pages = $tabPages[$type];
                else $pages = $loggedPages;
        }
        //On cherche la page demandée parmi les url interdites (extension php pour être backward compatible avec la v1)
        if(!is_array($pages)) {
            $aPages = explode(",",$pages);
        } else {
            $aPages = $pages;
        }
        $aPages = array_map("trim", $aPages);
        $aPages = array_map("strtolower", $aPages);
        
        if(!in_array(strtolower($urlaction.".php"),$aPages)) {
            return true;
        } else {
            return false;
        }
    }

    
    function includeFile($incfile) {
        if (!empty($incfile) && is_file($incfile)) {
            ob_start();
            require($incfile);
            $this->params['content'] = ob_get_clean();
        }
    }

    function transformArrayMulti($arrForm, $keys, $type=null){
		$arrTmp = array();
		foreach ($arrForm as $idx=>$item) {
            foreach($item as $tag=>$val){
                if(in_array ($tag, $keys)){
                    $lastID=$idx;
				}
                if(isset($lastID) && !isset($arrTmp[$lastID][$tag])){
					$arrTmp[$lastID][$tag]=$val;
				}
            }
        }
		
		$arrTmp = array_values($arrTmp);
		if(!empty($type)){
			for($idx=count($arrTmp);$idx>=0;$idx--){
				if(isset($arrTmp[$idx])){
					if(empty($arrTmp[$idx][strtoupper($type)])) $arrTmp[$idx][strtoupper($type)]=$lastType;
					else $lastType=$arrTmp[$idx][strtoupper($type)];
				}
			}
		}
        return $arrTmp;
    }
}

?>
