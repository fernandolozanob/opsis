<?
/**
 *  Classe Controller_personne
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(libDir."class_cherchePers.php");
require_once(modelDir.'model_personne.php');
require_once(libDir."class_page.php");

class Controller_personne extends Controller {
    
    function do_personne($args=null) {
		global $db;
        
        $myPage=Page::getInstance();
        $myPage->nomEntite = kPersonne;
        $this->params['setReferrer']['queue'] = true ; 
        
		$myPers=new Personne();
        
        if(isset($_GET["id_pers"])||isset($_GET["rang"]))
        {
            if(isset($_GET["rang"])) $rang=intval($_GET["rang"]);
            
            if(isset($_GET["id_pers"])) {
                $id_pers=intval($_GET["id_pers"]);
            } else {
                if($rang==0) $rang=1;
                $sql=$_SESSION["recherche_PERS"]["sql"];
                if(isset($_SESSION["recherche_PERS"]["tri"])) $sql.= $_SESSION["recherche_PERS"]["tri"];
                if (!empty($sql)) {
                    $result=$db->Execute($sql." limit 1 OFFSET ".($rang-1));
                    if($result && $row=$result->FetchRow()){
                        $id_pers=$row["ID_PERS"];
                    }
                    $result->Close();
                } else {$myPage->redirect($myPage->getName()."?urlaction=personneListe");}
            }
            
            $myPers->t_personne['ID_PERS']=$id_pers;
            
            $myPers->getPersonne(array(), true);
			$myPers->getLexique();
			$myPers->getValeurs();
			$myPers->getDoc();
			$myPers->getDocAcc();
            
			$nom = (isset($myPers->t_personne['PERS_NOM']) && !empty($myPers->t_personne['PERS_NOM'])?$myPers->t_personne['PERS_NOM']:'');
			$prenom = (isset($myPers->t_personne['PERS_PRENOM']) && !empty($myPers->t_personne['PERS_PRENOM'])?$myPers->t_personne['PERS_PRENOM']:'');
			$this->init_showParam($myPers, $nom." ".$prenom);
		}
	}
	
    /** Liste personnes
     * IN : GET, POST
     * OUT : objet cherche
     */
    function do_personneListe($args=null) {
        $mySearch=new RecherchePersonne();
		$this->init_list($mySearch, "F_pers_form", kGestionPersonnes, "personne");
	
        if($mySearch->affichage=='HIERARCHIQUE'){
            // Préparation affichage hiérachique
            require_once(libDir."class_tafelTree.php");
	        $mySearch->treeParams=array("ID_TYPE_LEX"=>$mySearch->getValueForField(array('FIELD'=>'PERS_ID_TYPE_LEX','TYPE'=>'CE')), "ID_TYPE_DESC"=>'',"ID_ROLE"=>'',"MODE"=>'');
	        $btnEdit=$myPage->getName()."?urlaction=personneSaisie&id_pers=";
            $myAjaxTree=new tafelTree($mySearch,'form1');
            $myAjaxTree->JSReturnFunction=$jsFunction;
            
            $myAjaxTree->makeRoot();
	        $myAjaxTree->JSlink="window.location.href='".Page::getName()."?urlaction=personneSaisie&id_pers=%3\$s'"; //Le lien sur les terme ouvrira l'édition
            //Récupération du type de recherche
	        $title=GetRefValue('t_type_lex',$mySearch->getValueForField(array('FIELD'=>'PERS_ID_TYPE_LEX','TYPE'=>'CE')),$_SESSION['id_lang']);
            
            
	        if ($mySearch->getValueForField(array('FIELD'=>'PERS_NOM','TYPE'=>'FT'))!=''
	        	|| $mySearch->getValueForField(array('FIELD'=>'PERS_NOM','TYPE'=>'C'))!='' ) { //En plus, on a une recherche sur un terme
                if (!empty($mySearch->result)) {
                    foreach ($mySearch->result as $row) { //Pour chaque résultat...
                        $myAjaxTree->revealNode($row['ID_PERS']);
                    }
                }
                $this->params['nb_results'] = $mySearch->found_rows;
            }
            $this->params['ajaxTree'] = $myAjaxTree;
        }
        
    }
	
	 /** Saisie valeur
     * IN : GET, POST, objet val
     * OUT : objet val
     */
    function do_personneSaisie($args=null) {

        $myPage=Page::getInstance();
        $myPage->nomEntite = kPersonne;
		
		$myPers=new Personne();
		$myPers->updateFromArray($_REQUEST);

        if(empty($myPers->t_personne['ID_PERS'] ) && !empty($myPage->id)){
            $myPers->t_personne['ID_PERS'] = intval($myPage->id);
        }
		
		 if (!isset($_REQUEST['commande']))
            $_REQUEST['commande']='';

        switch ($_REQUEST['commande']) {
            case "SAVE":
            case "DUP":
				$FORM=$_POST;
				$myPers->pers_trad=$_POST['pers_trad'];

				if (!empty($FORM['t_pers_val']))
					foreach ($FORM['t_pers_val'] as $idx=>$val) {
						if (isset($val['ID_VAL'])) {$lastID=$idx;$FORM['t_pers_val'][$idx]['ID_PERS']=$myPers->t_personne['ID_PERS'];}
						if (isset($val['ID_TYPE_VAL'])) {$FORM['t_pers_val'][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];unset($FORM['t_pers_val'][$idx]);}
					}

				if (!empty($FORM['t_pers_lex'])) {
					$lastID=-1;
					foreach ($FORM['t_pers_lex'] as $idx=>$lex) {
							if (isset($lex['ID_LEX']) || isset($lex['ID_PERS'])) {
								$lastID=$idx;
								$FORM['t_pers_lex'][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
								if(isset($lex_terme)) {$FORM['t_pers_lex'][$idx]['LEX_TERME']=$lex_terme;$FORM['t_pers_lex'][$idx]['ID_LEX']=0;unset($lex_terme);}
								if(isset($pers_nom)) {$FORM['t_pers_lex'][$idx]['PERS_NOM']=$pers_nom;$FORM['t_pers_lex'][$idx]['ID_PERS']=0;unset($pers_nom);}
							}
							if (isset($lex['LEX_TERME'])) {$lex_terme=$lex['LEX_TERME'];unset($FORM['t_pers_lex'][$idx]);}
							if (isset($lex['PERS_NOM'])) {$pers_nom=$lex['PERS_NOM'];unset($FORM['t_pers_lex'][$idx]);}
							if (isset($lex['DLEX_ID_ROLE'])) {$FORM['t_pers_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM['t_pers_lex'][$idx]);}
							if (isset($lex['ID_TYPE_LEX'])) {$FORM['t_pers_lex'][$lastID]['ID_TYPE_LEX']=$lex['ID_TYPE_LEX'];unset($FORM['t_pers_lex'][$idx]);}
							if (isset($lex['DLEX_REVERSEMENT'])) {$FORM['t_pers_lex'][$lastID]['DLEX_REVERSEMENT']=$lex['DLEX_REVERSEMENT'];unset($FORM['t_pers_lex'][$idx]);}
							if (isset($lex['LEX_PRECISION'])) {$FORM['t_pers_lex'][$lastID]['LEX_PRECISION']=$lex['LEX_PRECISION'];unset($FORM['t_pers_lex'][$idx]);}
							if (isset($lex['ID_TYPE_DESC'])) {$FORM['t_pers_lex'][$lastID]['ID_TYPE_DESC']=$lex['ID_TYPE_DESC'];unset($FORM['t_pers_lex'][$idx]);}
					}
				}
				$myPers->t_pers_lex=(empty($FORM['t_pers_lex'])?array(array('1')):$FORM['t_pers_lex']);
				$myPers->t_pers_val=$FORM['t_pers_val'];

				$ok=$myPers->save();
				$myPers->savePersLex();
				$myPers->savePersVal();

				//VP 6/07/18 : appel updateDocChampsXML pour mettre à jour t_doc et solr
				$myPers->updateDocChampsXML(true);
				
				if(!empty($_POST["page"]))  {
					$myPage->redirect($_POST["page"]);;
				}

				if (!empty($_POST['fusion'])) {
					$myPers->fusion($_POST['fusion']);
				}

				if ($_REQUEST['fldToPassToRedirection']) $additPrm="&".$_REQUEST['fldToPassToRedirection']."=".urlencode($myPers->t_personne["PERS_NOM"]." ".$myPers->t_personne["PERS_PRENOM"]);
				if ($myPage->hasReferrer() && $ok) {
					$myPage->redirect($myPage->getReferrer().$additPrm);
				}

				$myPers->getPersonne();
				$myPers->getLexique();
				$myPers->getValeurs();
				$myPers->getDoc();
				$myPers->getDocAcc();

				if (strcmp($_REQUEST['commande'],"DUP")==0){
					$newPers=$myPers->duplicate();
					$myPers=$newPers;

				}
            break;
			
            case "SUP":
				// VP 6/07/2018 : appel getDocs puis updateDocChampsXML pour mettre à jour t_doc et Solr
				$myPers->getDocs(false);
				$myPers->delete($_POST["version2suppr"]);
				$myPers->updateDocChampsXML(true);
				if($myPage->hasReferrer()) {
					$myPage->redirect($myPage->getReferrer());
				}elseif(!empty($_POST["page"]) && empty($_POST["version2suppr"])){
					$myPage->redirect($_POST["page"]); //XXX : a fixer
				}

				$myPers->getPersonne();
				$myPers->getLexique();
				$myPers->getValeurs();
				$myPers->getDoc();
            break;
            
            case "": // pas d'action, on préinitialise le type avec celui issu du get (cas de création d'une nv val)
                $myPers->updateFromArray($_POST);
            break;
		}
		
		if(!empty($myPers->t_personne['ID_PERS'])){
			$myPers->getPersonne(array(), true);
			$myPers->getLexique();
			$myPers->getValeurs();
			$myPers->getDoc();
			$myPers->getDocAcc();

		}
		$myPers->getVersions();
        
        // Paramètres pour view
		$nom = (isset($myPers->t_personne['PERS_NOM']) && !empty($myPers->t_personne['PERS_NOM'])?$myPers->t_personne['PERS_NOM']:'');
		$prenom = (isset($myPers->t_personne['PERS_PRENOM']) && !empty($myPers->t_personne['PERS_PRENOM'])?$myPers->t_personne['PERS_PRENOM']:'');
		$this->init_editParam($myPers, $nom." ".$prenom);
	}
    
}

?>
