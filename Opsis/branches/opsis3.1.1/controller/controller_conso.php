<?
/**
 *  Classe Controller_val
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(libDir."class_chercheConso.php");
require_once(modelDir."model_conso.php");
require_once(libDir."class_page.php");

class Controller_conso extends Controller {
    
    /** Liste valeurs
     * IN : GET, POST
     * OUT : objet cherche
     */
    function do_consoListe($args=null) {
		$mySearch=new RechercheConso();
        
        $myPage=Page::getInstance();
		
		$this->init_list($mySearch, "F_conso_form", kSuiviConso, "conso");
	}
    
	function do_conso($args=null) {
        
        $myConso=new Conso();
        if(!empty($_REQUEST["id_conso"])){
            $myConso->t_conso['ID_CONSO'] = intval($_REQUEST["id_conso"]);
        }elseif(!empty($myPage->id)){
            $myConso->t_conso['ID_CONSO'] = intval($myPage->id);
        }elseif(isset($_GET["conso"]) && !empty($_GET["conso"])){
            $myConso->t_conso['ID_CONSO'] = intval($_GET["conso"]);
        }
        $myConso->getConso();
        
        $nomFichierXsl=getSiteFile('designDir','consoAff.xsl');
	
		$myConso->t_conso['CONSO_OBJET']=str_replace('\'','&#39;',$myConso->t_conso['CONSO_OBJET']);
		$xml=$myConso->xml_export(1);
		$params4XSL=array();
		foreach($params4XSL as $ind => $data) $tab[$ind] = $data;
		$conso=TraitementXSLT($xml,$nomFichierXsl,$tab,0,$xml,$arrHighlight);
		
        $this->init_showParam($myConso, (isset($myConso->t_conso['CONSO_OBJET']) && !empty($myConso->t_conso['CONSO_OBJET'])?$myConso->t_conso['CONSO_OBJET']:''), $conso);
    }
    
    
}

?>
