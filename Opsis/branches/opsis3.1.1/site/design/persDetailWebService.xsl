<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->

<xsl:param name="scripturl"/>
<xsl:param name="id_lang"/>

<xsl:key name="preg" match="t_doc" use="ID_DOC"/>
<xsl:template match='/t_personne'>

<data>

   <personne id_pers="{ID_PERS}">
  
   
      <id_pers><xsl:value-of select="ID_PERS"/></id_pers>
      <id_lang><xsl:value-of select="ID_LANG"/></id_lang>
      <prenom><xsl:value-of select="PERS_PRENOM"/></prenom>
      <particule><xsl:value-of select="PERS_PARTICULE"/></particule>
      <pays><xsl:value-of select="PERS_ORGANISATION"/></pays>
      <telephone><xsl:value-of select="PERS_TEL_PROF"/></telephone>
      <fax><xsl:value-of select="PERS_FAX"/></fax>
      <mail><xsl:value-of select="PERS_MAIL_PROF"/></mail>
      <adresse><xsl:value-of select="PERS_ADRESSE_PROF"/></adresse>
      <biographie><xsl:value-of select="PERS_BIOGRAPHIE"/></biographie>
      <commentaire><xsl:value-of select="PERS_NOTES"/></commentaire>
      <site_web><xsl:value-of select="PERS_DIFFUSION"/></site_web>
      
      <fonctions>
      	<xsl:for-each select="t_pers_val/TYPE_VAL[@VAL_ID_TYPE_VAL='FONC']/t_val">
      		<fonction><xsl:value-of select="VALEUR"/>
      		</fonction>
      	</xsl:for-each>
      </fonctions>
      <festivals>
      	<xsl:for-each select="t_fest/FEST">
      		<festival>
      			<id_fest><xsl:value-of select="ID_FEST"/></id_fest>
      			<annee><xsl:value-of select="substring-before(FEST_ANNEE,'-')"/></annee>
      			<description><xsl:value-of select="FEST_DESC"/></description>
      			<role><xsl:value-of select="ROLE"/></role>
      			<precision><xsl:value-of select="LEX_PRECISION"/></precision>
      			<lien>service.php?urlaction=detail&amp;amp;entite=festival&amp;amp;id=<xsl:value-of select="ID_FEST"/></lien>
      		</festival>
      	</xsl:for-each>
      </festivals>
      
      <films>
      	<xsl:for-each select="t_pers_doc/t_doc[count(. | key('preg', ID_DOC)[1]) = 1]">
      	<film>
      		<id_doc><xsl:value-of select="ID_DOC"/></id_doc>
      		<id_lang><xsl:value-of select="ID_LANG"/></id_lang>
      		<xsl:variable name="titretraduit">
                <xsl:choose>
                <xsl:when test="$id_lang='fr' or $id_lang='FR'">
                	<xsl:value-of select="DOC_TITRE"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="DOC_AUTRE_TITRE" /></xsl:otherwise>
                </xsl:choose>
                </xsl:variable>
                <xsl:variable name="articletraduit"><xsl:choose><xsl:when test="$id_lang='fr' or $id_lang='FR'"><xsl:value-of select="normalize-space(DOC_TITRE_COL)"/></xsl:when>
    	            <xsl:otherwise><xsl:value-of select="normalize-space(DOC_COTE_ANC)" /></xsl:otherwise></xsl:choose>
   			   </xsl:variable>
	        <titre_original><xsl:if test="DOC_SOUSTITRE!=''">(<xsl:value-of select="DOC_SOUSTITRE"/>) </xsl:if>
				<xsl:value-of select="DOC_TITREORI"/>
	        </titre_original>
	        <titre_traduit>
	      		<xsl:if test="normalize-space($articletraduit)!=''"><xsl:value-of select="$articletraduit"/>&#160;</xsl:if><xsl:value-of select="$titretraduit"/>
	        </titre_traduit>
	        <duree><xsl:value-of select="DOC_DUREE"/></duree>
	        <annee><xsl:value-of select="substring-before(DOC_DATE_PROD,'-')"/></annee>
	        <resume>
	           <xsl:choose>
	                <xsl:when test="$id_lang='fr' or $id_lang='FR'">
	                	<xsl:value-of select="DOC_RES"/>
	                </xsl:when>
	                <xsl:otherwise><xsl:value-of select="DOC_RES_CAT" /></xsl:otherwise>
	           </xsl:choose>     	
	        </resume>
	        <roles>
					<xsl:for-each select="key('preg', ID_DOC)">
						<xsl:sort select="ROLE" />
						<role><xsl:value-of select="ROLE" /></role>
						
					</xsl:for-each>
			</roles>
	               
	        
      	</film>
      	</xsl:for-each>
      </films>
      
 
	  <pieces_jointes>
	  	<xsl:for-each select="t_doc_acc/t_doc_acc">
	  	<piece_jointe>
	  		<id_doc_acc><xsl:value-of select="ID_DOC_ACC"/></id_doc_acc>
	  		<titre><xsl:value-of select="DA_TITRE"/></titre>
	  		<fichier>service.php?urlaction=getfile&amp;amp;type=doc_acc&amp;amp;filename=<xsl:value-of select="DA_CHEMIN"/><xsl:value-of select="DA_FICHIER"/></fichier>
	  		<type><xsl:value-of select="t_doc_acc_val/t_val/t_val/VALEUR"/></type>
	  	</piece_jointe>
	  	</xsl:for-each>
	  </pieces_jointes>


    </personne>
</data>


</xsl:template>


<xsl:template name="internal-quote-replace">
	  <xsl:param name="stream" />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>


</xsl:stylesheet>
