<?php
	$myUsr=User::getInstance();
	$myPage->titre = str_replace(chr(34),'',$myExp->t_exploitation['EXP_TITRE']);
?>
<div id="tabs">
	<div id="pageTitre"><?= $myExp->t_exploitation['EXP_TITRE'] ?></div>
	<ul>
		<li><a href='#tabs-1' onclick="javascript:saveIfAndGo('<?=$myPage->getName()?>?urlaction=expSaisie&id_exp=<?= $myExp->t_exploitation['ID_EXP']?>')"><?= kExploitation ?></a></li>
		<li><a href='#tabs-2' onclick="javascript:saveIfAndGo('<?=$myPage->getName()?>?urlaction=expSaisie&form=expProdSaisie&id_exp=<?= $myExp->t_exploitation['ID_EXP']?>')"><?= kListeProduction ?></a></li>
		<? if ($myExp->t_exploitation['EXP_ID_TYPE_EXP'] === "COF") { ?>
		<li><a href='#tabs-3' onclick="javascript:saveIfAndGo('<?=$myPage->getName()?>?urlaction=expSaisie&form=expExpSaisie&id_exp=<?= $myExp->t_exploitation['ID_EXP']?>')"><?= kListeExploitations ?></a></li>
		<? } ?>
	</ul>
	<div id="tabs-1" style="display:none"></div>
	<div id="tabs-2" style="display:none"></div>
	<div id="tabs-3" style="display:none"></div>
</div>

<script type="text/javascript">
formChanged=false;
</script>

<table width="100%" border="0" class="resultsMenu">
	<tr>
		<td align="left"></td>
		<td align="center"></td>
		<td align="right">
			<a href="javascript:popupPrint('print.php?<?= $_SERVER['QUERY_STRING']?>','main')"><img src="design/images/famfamfam_mini_icons/action_print.gif" border="0" />&nbsp;<?= kImprimer ?></a>
		</td>
	</tr>
</table>

