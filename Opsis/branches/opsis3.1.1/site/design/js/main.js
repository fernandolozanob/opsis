function displayFacettes(elt)
{
	if ($j(elt).prev().is(':visible')){
		// $j(elt).prev().hide('fast');
		$j(elt).prev().css('display','none');
		$j(elt).parent('.block_facet.open').removeClass("openfull");
		if(typeof str_lang !="undefined" && str_lang.kAfficherTout)
			$j(elt).html(str_lang.kAfficherTout);
	}else{
		$j(elt).parent('.block_facet.open').addClass("openfull");
		$j(elt).prev().show('fast');
		if(typeof str_lang !="undefined" && str_lang.kAfficherTopResults)
			$j(elt).html(str_lang.kAfficherTopResults);
	}
}







// fonction d'affichage du formulaire de nommage rapide d'une nouvelle sélection (menuGauche)
function displayNewPanLeft(id,ctrl,type) {
	
    myDiv=document.getElementById('newPanLeft');
    myDiv.style.display='block';
    document.formCreaSelection.cartName.focus();
    document.formCreaSelection.cartIdItem.value = id;
    document.formCreaSelection.cartTypeItem.value = type;
    document.getElementById('link_new_selection').style.display='none';
}

// Fonction de rafraichissement de l'affichage hierarchique des paniers (menuGauche)
function refreshFolders(id) {
	
    var lang = getCookie('cook_lang').toUpperCase();
	if (id) {
		fromlink=document.getElementById('folder_button'+id);
		if (fromlink.className=='collapseFolder') {

			fromlink.className='expandFolder';
			fromlink.innerHTML="<img src='design/images/mini_arrow_right.gif' />";
			action='collapse';
			if (document.getElementById('folderDetail'+id)) document.getElementById('folderDetail'+id).style.display='none';
		} else {

			fromlink.className='collapseFolder';
			fromlink.innerHTML="<img src=''design/images/mini_arrow_down.gif' />";
			action='expand';
		}
	} else {action='';}
	return !sendData('GET','blank.php','urlaction=loadPanier&action='+action+'&id='+id+'&id_lang='+lang+'&display=none','dspFolders');
}


// fonction d'affichage hierarchique des paniers sous forme de folder.
// + créa du formulaire de nommage pour la création d'une nouvelle sélection (menuGauche)
function dspFolders(str) {
	var lang = getCookie('cook_lang').toLowerCase();
	var jsNom = {'en':'Name','fr':'Nom'};
    var jsCreerSelection = {'en':'New cart', 'fr':'Nouveau panier'};
    var jsPanier = {'en':'Cart', 'fr':'Panier'};

	xml = importXML(str);
	
	var carts = xml.getElementsByTagName('cart');
	output = '';
	output_2 = '';
	for( i=0; i < carts.length ; i ++){
		try{
			id = carts[i].getElementsByTagName('ID_PANIER')[0].firstChild.nodeValue;
		}catch(e){id='';}
		try{
		etat = carts[i].getElementsByTagName('PAN_ID_ETAT')[0].firstChild.nodeValue;
		}catch(e){etat='';}
		try{
		titre = carts[i].getElementsByTagName('PAN_TITRE')[0].firstChild.nodeValue;
		}catch(e){titre='';}
		// console.log("id: "+id+" etat: "+etat+" titre: "+titre);
	
		if(id!=''){
			output += '<li id="panier'+id+'" onclick="loadPanier(\''+id+'\',this)">';
			if(etat=='1' && titre == ''){
				output += jsPanier[lang]; 
			}else{
				output+=titre;
			}
			output+="</li>";
			
			output_2 += '<li class="add_panier'+id+'" onclick="addCheckedToCart(this,\''+id+'\')">';
			if(etat=='1' && titre == ''){
				output_2 += jsPanier[lang]; 
			}else{
				output_2+=titre;
			}
			output_2+="</li>";
		}
		
	}
	
    var newsel = '<li id="link_new_selection" onclick="displayNewPanLeft(0, this)" >'+jsCreerSelection[lang]+'</li>'+
								'<li id="newPanLeft" class="resultsMenu" style="clear:both;display:none;">' +
	 							'<form name="formCreaSelection" onsubmit="add2folder(-1,this.cartIdItem.value,this.cartTypeItem.value,this.cartName.value);return false;">' + 
								'<input name="cartTypeItem" id="cartTypeItem" type="hidden" />'+
								'<input name="cartIdItem" id="cartIdItem" type="hidden" />'+
								jsNom[lang] +'&#160;<input name="cartName" type="text" style="width:100px" />' + 
					 		    '<input type="submit" value="OK"  />' + 
					 		    '</form>' + 
					 		    '</li>'  
	 							
	if (id != '_session') document.querySelector('ul.panListe').innerHTML=output+newsel;
	else document.querySelector('ul.panListe').innerHTML=output;
	
	if(typeof id_current_panier != 'undefined' && id_current_panier != ''){
		loadPanier(id_current_panier,$j('ul.panListe #panier'+id_current_panier));
	}
	
	$j('.toolbar .add .drop_down').html("<ul>"+output_2+"</ul>");
	if (refreshDroppables)
		refreshDroppables();

	
}


function addCheckedToCart(elt,idCart){
	if(typeof idCart == 'undefined' || idCart == null){
		return false ; 
	}
	items2add="";
	type = "";
	
	// console.log($j(elt).parents('#panBlock').length);
	if($j(elt).parents('#panBlock').length>0){
		type = 'pdoc';
		$j("#panFrame input[type='checkbox']:checked()").each(function(idx,elt){
			items2add += $j(elt).val()+'$';
		});
	}else{
		type = 'doc';
		if(typeof current_id_doc != 'undefined' && current_id_doc != null ){
			items2add+=current_id_doc+'$';
		}
		$j("#resultats input[type='checkbox']:checked()").each(function(idx,elt){
			items2add += $j(elt).val()+'$';
		});
	}
	if(idCart == id_current_panier){
		flag_reload_current_pan = true ; 
	}
	
	
	// console.log(items2add +" "+type);
	if(items2add!='' && type != ''){
		return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&commande=add&items='+items2add+"&typeItem="+type,'updateCarts');
	}
}



function toggleMe(tab_id,panel) {
	if (myVideo && myVideo.UnSetSelection) {
		myVideo.UnSetSelection();
	}
	
	$j('.panelSelWrapper .panelSel').removeClass('selected');
	$j('.panelSelWrapper #sel_'+tab_id).addClass('selected');
	
	
	if (myPanel && myPanel._hasChanged) {ok=confirm(str_lang.kConfirmerAbandon); if (!ok) return;}
	if (myPanel) myPanel._hasChanged=false;
	
	//Le sidecommande qui comprend rightPanel doit suivre l onglet selectionne.
	sidecommandeObj=document.getElementById('sidecommande').cloneNode(true); //clonage du sidecommande
	
	//suppression du sidecommande original
	document.getElementById('sidecommande').parentNode.removeChild(document.getElementById('sidecommande')); 

	//suppression du icon_bar original
	if(document.getElementById('icon_bar')) document.getElementById('icon_bar').parentNode.removeChild(document.getElementById('icon_bar')); 

	//re création du sidecommande dans l onglet sélectionné
	tab=document.getElementById(tab_id);
	tab.appendChild(sidecommandeObj); 

	if(panel)
		showPanel(panel);
	else
		hidePanel(); 
	
	showTab(tab_id);
	//movePanelIconBar();
	
	toggleSelection(tab_id)
}

/**************** FONCTION RELATIVE RECHERCHE / AFFICHAGE DE RESULTATS *******************/

// fonction qui permet de 'remplir' la mosaique => évite que la dernière ligne soit incomplète si il existe d'autres documents
function fillMosaic(){
	current_cnt = $j("#main_block .resultsMos").length;
	obj_cnt = calculateNbLignes();
	
	// console.log("currentCnt : "+current_cnt+" , nbmos :"+nb_mos_w);
		
	if(typeof obj_cnt == "number" && typeof current_cnt == "number" && (current_cnt%nb_mos_w) != 0){
		// console.log(nb_mos_w+" par ligne,  depasse de : "+(current_cnt%nb_mos_w)+", manque :"+(nb_mos_w - (current_cnt%nb_mos_w)));
		$j.ajax({
			url : "empty.php?urlaction=docListe&where=include&from_offset="+current_cnt+"&nbLignes="+(nb_mos_w - (current_cnt%nb_mos_w))+"&fromAjax=1&cherche=0",
			success: function(data){
				$j("#resultats div.pusher").before(data);
				$j("#quick_search input#nbLignes").val(current_cnt+(current_cnt%nb_mos_w));
				updateDraggables();
			},
			complete : function(){
				if($j('#quick_search input#nbLignes').val() >= totalNbResults ){
					$j("#loadMoreRes").css("display","none");
				}
			}
		});
	}
	
	
}


// fonction qui permet de gérer le chargement de resultats supplémentaires
var f_loading = false ; 
function loadMoreResults(){
	if(!f_loading && !$j("#loadMoreResults").hasClass("disabled")){
		// flag de blocage empechant plusieurs loading parallèle, 
		f_loading = true ; 
		
		// calculs du nb d'élément à récupérer
		current_cnt = $j("#main_block .resultsMos").length;
		nbLignes = calculateNbLignes();
		nbLignes = nbLignes*2;
		
		//feedback utilisateur : chargement & impossible de clicker à nouveau sur le bouton
		$j("#loadMoreRes").addClass("disabled");
		
		$j.ajax({
			url : "empty.php?urlaction=docListe&where=include&from_offset="+current_cnt+"&nbLignes="+(nbLignes)+"&fromAjax=1&cherche=0",
			success: function(data){
				$j("#resultats div.pusher").before(data);
				$j("#quick_search input#nbLignes").val(current_cnt+(nbLignes));
				// console.log("current count : "+current_cnt+" nbLignes "+nbLignes+", new val input#nbLignes "+$j("#quick_search input#nbLignes").val()+" total rows : "+totalNbResults);
				updateDraggables();
			},
			complete : function(){
				$j("#loadMoreRes").removeClass("disabled");
				// console.log("call complete");
				if($j('#quick_search input#nbLignes').val() >= totalNbResults ){
					$j("#loadMoreRes").css("display","none");
				}
				$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").unbind('mouseover');
				$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").unbind('mouseleave');
				$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").hover(callHoverPlayer);
				$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").mouseleave(hideHoverPlayer);
				f_loading = false ; 
			}
		});
		updateDraggables();
		
		
	}
}

// fonction de centrage de la mosaique dans son container (pour corriger le comportement du float left)
function adjustMosaique(){
	$j("#resultats.mos, #panFrame .mosWrapper").css("margin-left","auto");
	w = $j("#main_block").width();
	if(w == null && $j("#panBlock div.mosWrapper").length>0){
		w = $j("#panBlock div.mosWrapper").width();
	}
	w = w%mos_unit_w;
	w = w/2;
	$j("#resultats.mos, #panFrame .mosWrapper").css("margin-left",w+"px");
	$j("#resultats.mos, #panFrame .mosWrapper").css("visibility","visible");
}	



		
$j(document).ready(function(){	
	// fonctions de gestion de la mosaïque 
	if($j("#resultats").hasClass("mos")){
		fillMosaic();
		adjustMosaique();
		$j(window).resize(function(){
			fillMosaic();
			adjustMosaique();
		});
		if($j("#main_block .resultsMos").length>0){
			loadPrevDoc($j("#main_block  .resultsMos").eq(0).attr('id').replace('doc_',''),$j("#main_block .resultsMos").eq(0),0);
		}
	}else{
		if($j("tr .resultsCorps input[type='checkbox']").length>0){
			loadPrevDoc($j("tr .resultsCorps input[type='checkbox']").eq(0).val().replace('doc_',''),$j("tr .resultsCorps").eq(0).parent());
			$j("#resultats.liste tr").each(function(idx,elt){
				if(!$j(elt).hasClass("separator") && !$j(elt).hasClass("resultsHead_row") ){
					if($j(elt).find(".resultsCorps input[type='checkbox']").length>0){
						$j(elt).click(function(){
							chck = $j(elt).find(".resultsCorps input[type='checkbox']");
							loadPrevDoc(chck.val().replace("doc_",''),elt);
						});
					}
				}
			});
		
		}
	}
	
	// MS mise en place redirection par doubleclick sur l'image : 
	// Ok pour recherche & categs, 
	// voir panier.xsl pour la gestion du panier
	// mosaique, 
	$j("#resultats.mos div.resultsMos").dblclick(function(evt){
		if($j(evt.target).parents('.resultsMos').find('a.consult').length >0){
			window.location.href = $j(evt.target).parents('.resultsMos').find('a.consult').attr('href');
		}
		
	});
	// liste, 
	$j("#resultats.liste div.resultsMosRow .bgVignetteMos").dblclick(function(evt){
		if($j(evt.target).parents('.resultsMosRow').find('a.consult').length >0){
			window.location.href = $j(evt.target).parents('.resultsMosRow').find('a.consult').attr('href');
		}
	});
	
	
	
	// déploiement premier menu de facettes , si il existe, 
	if($j("#menuGauche:not(.collapsed) .type_facet").length>0){
		setTimeout(function(){$j("#menuGauche .type_facet").eq(0).click();},200);
	}
	
});

// le changement de la présentation change aussi le nombre de ligne affichée
function presentation_updateLignes(xsl,alt_pagerForm) {
	if(typeof alt_pagerForm == 'undefined'){
		alt_pagerForm = pagerForm ;
	}
	alt_pagerForm.style.value=xsl;
	if(xsl == "docListe2.xsl"){ // cas liste => on repart sur le nombre de lignes par défaut (transposé plus haut  php => JS)
		$j(alt_pagerForm.nbLignes).val(nb_lignes_deft);
	}else{ 						// cas mosaique => on recalcule l'affichage optimal
		nbLignes = calculateNbLignes();
		if(nbLignes){
			nbLignes = nbLignes*2;
		}else{
			nbLignes = nb_lignes_deft;
		}
		alt_pagerForm.nbLignes = nbLignes;
	}
	alt_pagerForm.submit();
}



function loadPrevDoc(id_doc,elt,rang){
	if($j("#menuDroite").length>0){
		if(typeof elt == "undefined"){
			elt = null ; 
			type =null;
		}else{
			$j("#resultats *.selected,#panFrame *.selected").removeClass("selected");
			$j(elt).addClass("selected");

			if($j(elt).parents('#panBlock').length>0 && $j(elt).attr('id').substr(0,5)=='pdoc_'){
				type = "pdoc";
			}else{
				type = 'doc';
			}
			
			// type = $j(elt).attr('id').split('_')[0];
			
			if(type == 'pdoc'){
				if($j(elt).hasClass('resultsMos') && $j(elt).attr('id')){
					id_ligne_panier = $j(elt).attr('id').split('_')[1];
				}else{
					id_ligne_panier = $j(elt).find("input[type='checkbox']").val();
				}
				id_panier =id_current_panier;
			}
		}
		

		if(rang== null&& typeof $j(elt).find("a[href*='id_doc="+id_doc+"&rang=']").attr('href') == 'string' ){
			
			rang = $j(elt).find("a[href*='id_doc="+id_doc+"']").attr('href').split('&rang=') ;
			rang=parseInt(rang[1],10);
			// console.log(rang);
		}
		
		
		
		$j("#menuDroite").addClass("loading");
		$j.ajax({
			url : "empty.php?urlaction=loadDoc&id_doc="+id_doc,
			success : function(data){
				
				try{
					titre= $j('<span>').append($j.parseHTML(data.getElementsByTagName('DOC_TITRE')[0].childNodes[0].nodeValue)).get(0).innerHTML;
				}catch(e){
					titre="";
				}
				try{
					resume = $j('<span>').append($j.parseHTML(data.getElementsByTagName('DOC_RES')[0].childNodes[0].nodeValue)).get(0).innerHTML;
				}catch(e){
					resume="";
				}
				try{
					vignette = $j('<span>').append($j.parseHTML(data.getElementsByTagName('VIGNETTE')[0].childNodes[0].nodeValue)).get(0).innerHTML;
				}catch(e){
					vignette="";
				}
				try{
					type_media = $j('<span>').append($j.parseHTML(data.getElementsByTagName('DOC_ID_MEDIA')[0].childNodes[0].nodeValue)).get(0).innerHTML;
				}catch(e){
					type_media="";
				}
					
				$j("#prevTitre").html(titre);
				$j("#prevInfos").html(resume);
				$j("#prevMedia").get(0).className = '';
				$j("#prevMedia").css('min-height',"0px");
				if(vignette != ""){
					if((type_media == 'V' || type_media=='A') && type == 'pdoc' && typeof(id_ligne_panier) != 'undefined'){
						$j("#prevMedia").html('<img class="prev_vignette" onclick="loadHoverPlayer('+id_ligne_panier+',\'prevMedia\',\''+type+'\');" src="makeVignette.php?image='+vignette+'&kr=1&w=286&h=215&ol=player"/>');
					}else if(type_media == 'V' || type_media=='A'){
						$j("#prevMedia").html('<img class="prev_vignette" onclick="loadHoverPlayer('+id_doc+',\'prevMedia\',\'doc\');" src="makeVignette.php?image='+vignette+'&kr=1&w=286&h=215&ol=player"/>');
					}else{
						$j("#prevMedia").html('<img class="prev_vignette"  src="makeVignette.php?image='+vignette+'&kr=1&w=286&h=215"/>');
					}
				}else{
					$j("#prevMedia").html("");
				}
				
				$j('#prevLink button').css('visibility','visible');
				
				$j("#prevLink button").get(0).onclick = function(){
					link_url="index.php?urlaction=doc&id_doc="+id_doc;
					if(typeof rang =='number'){
						link_url+="&rang="+rang;
					}
					if(typeof id_panier != 'undefined'){
						link_url+='&id_panier='+id_panier;
					}
					
					window.location.href = link_url;
				}
				
				setTimeout(function(){
					$j("#menuDroite").removeClass("loading");
					$j("#prevMedia").css('min-height',$j('#prevMedia').height()+"px");
				}, 150);
			}
		});
	}
}

function smartCheckAll(elt_tool,container){
	if(!$j(elt_tool).hasClass('checked')){
		$j(elt_tool).addClass('checked');
		$j(container).find('input[type="checkbox"]').prop('checked',true);
	}else{
		$j(elt_tool).removeClass('checked');
		$j(container).find('input[type="checkbox"]').prop('checked',false);
	}
}



/***************************** preview player **************************/


var trigger_preview = null ; 

function callHoverPlayer(event){
	if(sessLayout.prev_pop_in && !showRightMenu){
		// console.log(event);
		id = $j(event.currentTarget).find('input[type="checkbox"]').val();
		if($j(event.currentTarget).attr('id')){
			type = $j(event.currentTarget).attr('id').replace('_'+id,'');
		}else{
			if($j(event.currentTarget).find('input[name^="t_panier_doc"]').length>0){
				type='pdoc';
			}else{
				type='doc';
			}
		}
		if(trigger_preview !=null ){
			clearTimeout(trigger_preview);
			trigger_preview = null ; 
		}
		
		trigger_preview = setTimeout(function(){
			setPositionPrevPlayer(event);
			loadHoverPlayer(id,'previewHoverWrapper',type);
		},400);
	}
}



function setPositionPrevPlayer(evt){
	if(typeof evt.currentTarget!='undefined'){
		t = evt.currentTarget;
		modifs = {};
		
		if($j(t).hasClass('resultsMos')){
			o = $j(t).offset();
			if(o.left-410<0){
				modifs.left = o.left + $j(t).width() +10;
			}else{
				modifs.left = o.left-410;
			}
			if(o.top+400>$j(window).height()){
				modifs.top = o.top - (400 - $j(t).height());
			}else{
				modifs.top = o.top;
			}
			
				
		}else{
			o = $j(t).offset();
			if((o.top + $j(t).height() + 420 )> $j(window).height()){
				modifs.top = o.top - 420
			}else{
				modifs.top =o.top + $j(t).height() + 20 ;
			}
			
			modifs.left = o.left;
			
		}
		$j("#previewHoverWrapper").css(modifs);
	}
}

function loadHoverPlayer(id, target,type){
	if(typeof player_options != 'undefined'){
		delete player_options['image'];
		delete player_options['width'];
		delete player_options['height'];
		delete player_options['url'];
		delete player_options['controls'];
	}
	
	
	
	if(typeof target == 'undefined' || target == null){
		target = 'previewHoverWrapper';
	}
	
	if(!$j('#'+target).hasClass("id_"+id)){
		trigger_preview = null ; 
		$j.ajax({
			url : 'previewPlayer.php?id='+id+"&type="+type+"&target="+target,
			success : function(data){
				if(target == 'previewHoverWrapper'){
					try{
						$j('#previewHoverWrapper').css('visibility','hidden');
						$j('#previewHoverWrapper').css('display','list-item');
						$j('#previewContainer').html(data);
						$j('#previewHoverWrapper').css('min-height',$j("#previewContainer").get(0).offsetHeight);
						$j('#previewHoverWrapper').css('visibility','visible');
						
						$j('#previewHoverWrapper').addClass('id_'+id);
					}catch(e){
						// console.log('crash');
					}
				}else if (target == 'prevMedia'){
					try{
						$j('#prevMedia').html(data);
						$j('#prevMedia').find('img.prev_vignette').css('display','none');
						$j('#prevMedia').addClass('id_'+id);
					}catch(e){
						// console.log('crash');
					}
				}
			},
			complete : function(data){
				// console.log('complete tst');
				// console.log(data);
			}
		});
	}
}

function hideHoverPlayer(){
	// return false ; 
	if(sessLayout.prev_pop_in && !showRightMenu){
		$j("#prevMedia").get(0).className='';
		$j("#previewHoverWrapper").get(0).className='';
		$j('#previewHoverWrapper').css('display','none');
		if(typeof myVideo != 'undefined' && myVideo!=null){
			myVideo = null ; 
		}else if (typeof player != 'undefined' && player!=null){
			player = null ; 
		}else if (typeof my_player != 'undefined' && my_player!=null){
			if(my_player.thread_affichage){
				my_player.thread_affichage.stop();
			}
			my_player = null ; 
		}
		
		$j("#previewContainer").html("");
		
		if(trigger_preview !=null ){
			clearTimeout(trigger_preview);
			trigger_preview = null ; 
		}
	}
}

function saveAndGo(url) {
	var form1 = document.getElementById("form1");
	// Le formulaire doit avoir un champ save_action qui détermine le type de sauvegarde
	// à effectuer (ex : doc, sequence, matériel, etc...)
	if (!form1.save_action) commande="SAVE";
	else commande=form1.save_action.value;
    form1.commande.value=commande;
    form1.page.value=url;
    form1.submit();
}

function removeItem(url, form) {
	if (typeof(form)=='undefined') {
		form=document.getElementsByName("form1")[0];
	}
    if (confirm(str_lang.kConfirmerSuppression)) {
    	form.commande.value="SUP";
    	form.page.value=url;
        form.submit();
    }
}