<?php

	error_reporting(0);

    require_once("conf/conf.inc.php");
    require_once(libDir."session.php");
    require_once(libDir."class_formSaisie.php");

	global $db ; 
	
	// génération des flags de changement de langue dans la variable $langue_menu pour insertion dans l'entete un peu plus bas
	$res = $db->Execute("select ID_LANG,LANG from t_lang"); 
	$langue_menu = "" ; 
	while($row = $res->FetchRow()){
		if($row['ID_LANG'] == $_SESSION['langue']){
			$current_langue = $row['LANG'];
		}
		
		$current_query_string = (isset($_GET['resetPassword'])?'&resetPassword='.$_GET['resetPassword']:'');
		$langue_menu.="<a href=\"".$_SERVER['SCRIPT_NAME']."?langue=".$row["ID_LANG"].$current_query_string."\" ><img class='flag_chooser ".(($_SESSION['langue'] == $row['ID_LANG'])?'current':'')."' border='0' alt='".$row["LANG"]."'  title='".$row["LANG"]."' src='".designUrl."images/flag_".$row["ID_LANG"].".png'/></a>";
		
	}
	$res->Close();	

	echo '<div id="login_bg" class="gate_bg">';
	echo '<div id="login_popin" class="gate_popin">';
	echo '<div class="gate_popin_header">';
		echo '<span class="gate_popin_logo"><a href="'.kCheminHttp.'">';
		echo '<img alt="logo opsomai" id="logo_opsomai" src="design/images/'.gSiteLogoFilename.'"  border="0" style="height:58px; border:none;"/>';
		echo '<img alt="logo opsomai" id="logo_opsomai_small" src="design/images/'.gSiteLogoFilenameSmall.'" border="0" style="height:67px; border:none;" />';
		echo '</a></span>';
		echo '<span class="gate_popin_title">'.kGatePopinTitle.'</span>';
		echo '<div class="gate_popin_lang">'.$langue_menu.'</div>';
	echo '</div>';
	echo '<div class="gate_popin_corps loading">';
	echo '<div class="main">';
	echo '<div class="msg">'.kIdentificationMsg.'</div>';
	$myUsr = User::getInstance();
	if(!empty($myUsr->message)){
		echo '<p class="error">'.$myUsr->message.'</p>';
	}
	
	// si on est pas en train d'appeler index.php avec un token loginValidate ou un token resetPassword
	if((isset($_GET['urlaction']) &&  !empty($_GET['urlaction'])) && !isset($_GET['loginValidate']) && !isset($_GET['resetPassword'])){
		$refer_url = "http".(isset($_SERVER['HTTPS']) ? 's' : '')."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	}else{
		$refer_url= "" ; 
	}
	
	$logForm = new Form();
	$logForm->entity = 'USAGER';
	$logForm->beginForm(array('NAME'=>'connexion','ID'=>'connect_form','ACTION'=>'index.php'));
	$logForm->displayElt(array('CHLIBS'=>kIdentifiant,'CLASSLABEL'=>'label_champs_form','CLASSVALUE'=>'val_champs_form','TYPE'=>'TEXT','ID'=>'login','NAME'=>'login','DIVTAGS'=>'both'));
	$logForm->displayElt(array('CHLIBS'=>kMotDePasse,'CLASSLABEL'=>'label_champs_form','CLASSVALUE'=>'val_champs_form','TYPE'=>'password','ID'=>'password','NAME'=>'password','DIVTAGS'=>'both'));
	$logForm->displayElt(array('TYPE'=>'hidden','ID'=>'refer','NAME'=>'refer','VALUE'=>$refer_url));
	$logForm->displayElt(array('TYPE'=>'BUTTONSPAN','BUTTON_TYPE'=>'submit','NAME'=>'bsubmitLogin','CLASS'=>'std_button_style','VALUE'=>'kValider'));
	echo '<div class="log_links">';
	echo '<a onclick="loadLoginDemande();" >'.kMotDePasseOublie.'</a>';
	echo '<span class="log_links_separator"></span>';
	echo '<a onclick="loadInscription();" >'.kInscription.'</a>';
	echo '</div>';
	$logForm->endForm();
	echo '</div>';
	echo '<div class="ajax"></div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
?>
<script>
<?if(isset($_GET['resetPassword'])){
	echo "loadResetPassword();\n";
}else if (isset($_GET['loginValidate']) || (isset($_GET['urlaction']) && $_GET['urlaction'] == 'loginValidate' && isset($_GET['id']))){
	if(isset($_GET['loginValidate']) && !isset($_GET['id']))
		$_GET['id']=$_GET['loginValidate'];
	echo "loadLoginValidate();\n";
}else{
	echo "\$j('.gate_popin_corps').removeClass('loading');\n";
}?>

var conf_initSubmitToAjax = {
	loading_parent : 'div.gate_popin_corps',
	result_element : '#login_popin div.gate_popin_corps .ajax'
};

function loadLoginDemande(){
	$j("div.gate_popin_corps").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=loginDemande&where=include&fromAjax',
		success : function(data){
			$j("div.gate_popin_corps .main").css('display','none');
			$j("div.gate_popin_corps .ajax").html(data);
			$j("div.gate_popin_corps").removeClass('loading');
			initSubmitToAjax("form#loginDemande_form",chkFormReinitPw,conf_initSubmitToAjax);
		}
	});
}



function loadInscription(){
	$j("div.gate_popin_corps").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=inscription&where=include',
		success : function(data){
			$j("div.gate_popin_corps .main").css('display','none');
			$j("div.gate_popin_corps .ajax").html(data);
			$j("div.gate_popin_corps").removeClass('loading');
			initSubmitToAjax("form#inscription_form",chkFormInscription,conf_initSubmitToAjax);
		}
	});
}
function loadResetPassword(){
	$j("div.gate_popin_corps").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=inscription&where=include&fromAjax&resetPassword=<?=$_GET['resetPassword']?>',
		success : function(data){
			$j("div.gate_popin_corps .main").css('display','none');
			$j("div.gate_popin_corps .ajax").html(data);
			$j("div.gate_popin_corps").removeClass('loading');
			initSubmitToAjax("form#resetPassword_form",checkPwFields ,conf_initSubmitToAjax);
		}
	});
}

function loadLoginValidate(){
	$j("div.gate_popin_corps").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=loginValidate&where=include&id=<?=$_GET['id']?>',
		success : function(data){
			$j("div.gate_popin_corps .main").css('display','none');
			$j("div.gate_popin_corps .ajax").html(data);
			$j("div.gate_popin_corps").removeClass('loading');
			initSubmitToAjax("form#loginDemande_form",chkFormFields,conf_initSubmitToAjax);
		}
	});
}


function returnFromAjaxPopin(){
	$j("div.gate_popin_corps").addClass('loading');
	$j("div.gate_popin_corps .ajax").html("");
	$j("div.gate_popin_corps .main").css('display','');
	$j("div.gate_popin_corps .main p.error").html('');
	$j("div.gate_popin_corps").removeClass('loading');
}

</script>
