<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->

<xsl:param name="scripturl"/>
<xsl:param name="id_lang"/>
<xsl:param name="identifier"/>

<xsl:template match='/t_doc'>

<metadata>
	<oai_dc:dc 
          xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" 
          xmlns:dc="http://purl.org/dc/elements/1.1/" 
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
          xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ 
          http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
		<dc:title><xsl:value-of select="DOC_TITRE"/></dc:title>
		<xsl:if test="DOC_SOUSTITRE!=''"><dc:title><xsl:value-of select="DOC_SOUSTITRE"/></dc:title></xsl:if>
		<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='ANI']/t_val/VALEUR!=''"><dc:title><xsl:value-of select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='CC']/t_val/VALEUR"/></dc:title></xsl:if>
		<xsl:if test="DOC_DUREE!='00:00:00'"><dc:format><xsl:value-of select="substring(DOC_DUREE,1,5)"/></dc:format></xsl:if>
		<dc:date><xsl:value-of select="DOC_DATE_PROD"/></dc:date>
		<xsl:if test="DOC_DATE_DIFF!='' and DOC_DATE_DIFF!='0000-00-00'"><dc:date><xsl:value-of select="DOC_DATE_DIFF"/></dc:date></xsl:if>
		<dc:language><xsl:call-template name="lower"><xsl:with-param name="chaine" select="ID_LANG"/></xsl:call-template></dc:language>
		<dc:description><xsl:value-of select="DOC_RES"/></dc:description>
		<xsl:if test="DOC_XML/XML/DOC/TYPE_DOC!=''"><dc:description><xsl:value-of select="DOC_XML/XML/DOC/TYPE_DOC"/></dc:description></xsl:if>
		<xsl:if test="DOC_NOTES!=''"><dc:description><xsl:value-of select="DOC_NOTES"/></dc:description></xsl:if>
		<xsl:if test="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='INT']">
			<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='INT']">
				<xsl:for-each select="t_personne">
					<xsl:sort select="PERS_NOM"/>
					<dc:creator><xsl:value-of select="PERS_NOM"/> <xsl:value-of select="PERS_PRENOM"/></dc:creator>
				</xsl:for-each>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='ANI']">
			<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='ANI']">
				<xsl:for-each select="t_personne">
					<xsl:sort select="PERS_NOM"/>
					<dc:creator><xsl:value-of select="PERS_NOM"/> <xsl:value-of select="PERS_PRENOM"/> (<xsl:value-of select="ROLE"/>)</dc:creator>
				</xsl:for-each>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='CSC']">
			<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='CSC']">
				<xsl:for-each select="t_personne">
					<xsl:sort select="PERS_NOM"/>
					<dc:creator><xsl:value-of select="PERS_NOM"/> <xsl:value-of select="PERS_PRENOM"/> (<xsl:value-of select="ROLE"/>)</dc:creator>
				</xsl:for-each>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="DOC_ACC!=''"><dc:contributor><xsl:value-of select="DOC_ACC"/></dc:contributor></xsl:if>
		<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='PUB']/t_val/VALEUR!=''"><dc:audience><xsl:value-of select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='PUB']/t_val/VALEUR"/></dc:audience></xsl:if>
		<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='THEM']/t_val/VALEUR">
			<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='THEM']/t_val">
					<dc:subject><xsl:value-of select="VALEUR"/></dc:subject>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='AU']/t_rol/t_lex/LEX_TERME">
			<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='AU']/t_rol/t_lex">
					<dc:subject><xsl:value-of select="LEX_TERME"/></dc:subject>
			</xsl:for-each>
		</xsl:if>
		<dc:publisher>Bibliothèque publique d'information</dc:publisher>
		<dc:rights>
			<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='DIF']/t_val/VALEUR!=''">diffusion audio : <xsl:value-of select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='DIF']/t_val/VALEUR"/> ; </xsl:if>
			<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='CC']/t_val/VALEUR!=''">creative Common : <xsl:value-of select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='CC']/t_val/VALEUR"/> ; </xsl:if>
			<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='CONS']/t_val/VALEUR!=''">type consultation : <xsl:value-of select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='CONS']/t_val/VALEUR"/></xsl:if>
		</dc:rights>
		<dc:identifier><xsl:value-of select="concat('&lt;?= kCheminHttp?&gt;/index.php?urlaction=doc&amp;amp;id_doc=',ID_DOC)"/></dc:identifier>
		<xsl:for-each select="t_doc_mat/DOC_MAT">
			<xsl:sort select="t_mat/MAT_FORMAT"/>
			<xsl:sort select="MAT_NOM"/>
			<xsl:if test="contains(MAT_NOM,'.MP3') or contains(MAT_NOM,'.mp3')">
				<dc:type>Audio</dc:type>
				<dc:relation><xsl:value-of select="concat('&lt;?= kCheminHttpMedia?&gt;/audio/',MAT_NOM)"/></dc:relation>
			</xsl:if>
			<xsl:if test="contains(MAT_NOM,'.MP4') or contains(MAT_NOM,'.mp4')">
				<dc:type>Video</dc:type>
				<dc:relation><xsl:value-of select="concat('&lt;?= kCheminHttpMedia?&gt;/video/',MAT_NOM)"/></dc:relation>
			</xsl:if>
		</xsl:for-each>       
		<xsl:for-each select="t_doc_acc/t_doc_acc">
			<xsl:variable name="datype">
				<xsl:choose>
				<xsl:when test="t_doc_acc_val/TYPE_VAL[@ID_TYPE_VAL='DA']/t_val/VALEUR='Image'">Vignette</xsl:when>
				<xsl:when test="not(contains(DA_FICHIER,'http:')) and t_doc_acc_val/TYPE_VAL[@ID_TYPE_VAL='DA']/t_val/VALEUR!=''"><xsl:value-of select="t_doc_acc_val/TYPE_VAL[@ID_TYPE_VAL='DA']/t_val/VALEUR"/></xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="$datype!=''">
				<dc:type>
				<xsl:value-of select="$datype"/>
				</dc:type>
				<dc:relation><xsl:value-of select="concat('&lt;?= kDocumentUrl?&gt;',DA_CHEMIN,DA_FICHIER)"/></dc:relation>
			</xsl:if>
		</xsl:for-each>       
		<xsl:for-each select="t_doc_lies_src/t_doc">
				<dc:type><xsl:value-of select="TYPE_DOC"/></dc:type>
				<dc:relation><xsl:value-of select="concat('&lt;?= kCheminHttp?&gt;/index.php?urlaction=doc&amp;amp;id_doc=',ID_DOC)"/></dc:relation>
		</xsl:for-each>       
		<xsl:for-each select="t_doc_lies_dst/t_doc">
				<dc:type><xsl:value-of select="TYPE_DOC"/></dc:type>
				<dc:relation><xsl:value-of select="concat('&lt;?= kCheminHttp?&gt;/index.php?urlaction=doc&amp;amp;id_doc=',ID_DOC)"/></dc:relation>
		</xsl:for-each>       
		<dc:source>Base des archives sonores de la Bpi</dc:source>
		<xsl:if test="DOC_LIEU_PV!=''"><dc:source><xsl:value-of select="DOC_LIEU_PV"/></dc:source></xsl:if>
		<dc:coverage></dc:coverage>
	</oai_dc:dc>
</metadata>


</xsl:template>

<xsl:template name="lower">
	  <xsl:param name="chaine" />
	  <xsl:value-of select="translate($chaine,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')"/>
</xsl:template>


<xsl:template name="internal-quote-replace">
	  <xsl:param name="stream" />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>


</xsl:stylesheet>
