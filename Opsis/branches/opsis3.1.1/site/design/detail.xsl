<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

	<xsl:template name='displayView'>
		<xsl:param name="xmllist"/>
		
		<xsl:variable name="entity" select="$xmllist/*[name()='view' or name()='data']/entity"/>
            <xsl:for-each select="child::*[name()=$entity]">
                <xsl:variable name="row" select="child::*"/>
					
				<xsl:for-each select="$xmllist/*[name()='view' or name()='data']/fieldset">
                    <xsl:if test="(not(type_media) or type_media='' or ($row[name()='DOC_ID_MEDIA']!='' and contains(type_media,$row[name()='DOC_ID_MEDIA'])) or ($row[name()='MAT_ID_MEDIA']!='' and contains(type_media,$row[name()='MAT_ID_MEDIA']))) and (not(profil_min!='') or (profil_min!='' and $profil &gt;= profil_min)) and (not(profil_max!='') or (profil_max!='' and $profil &lt;= profil_max))">	
					<fieldset name="{name}" id="{name}" class="{class}">
					<xsl:variable name="lbl">
						<xsl:choose>
							<xsl:when test="legend!=''"><xsl:value-of select="legend"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="label"/></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<legend><xsl:processing-instruction name="php">print <xsl:value-of select="$lbl"/>;</xsl:processing-instruction></legend>
				<table>
					<xsl:for-each select="element[not(type_doc) or (contains(type_doc,$row[name()='DOC_ID_TYPE_DOC']))]">
						
						<xsl:variable name="selector" select="substring-before(opsfield,'_')"/>
						<xsl:variable name="selector2" select="substring-before(substring-after(opsfield,'_'),'_')"/>
						<xsl:variable name="selector3" select="substring-after(substring-after(opsfield,'_'),'_')"/>
						<xsl:variable name="type">
							<xsl:choose>
								<xsl:when test="$selector='CAT' and show_fullpath=1">DOC_CAT</xsl:when>
								<xsl:when test="$selector='CAT'">CAT</xsl:when>
								<xsl:when test="$selector='L'">LEX</xsl:when>
								<xsl:when test="$selector='P'">PERS</xsl:when>
								<xsl:when test="$selector='V'">VAL</xsl:when>
								<xsl:when test="opsfield='FEST'">FEST</xsl:when>
								<xsl:when test="$selector='DOC' and $selector2='ID' and $row[name()=$selector3]!=''">DOC_XML</xsl:when>
								<xsl:when test="chTypes != ''"><xsl:value-of select="chTypes" /></xsl:when>
								<xsl:when test="type!='select' and type!='text' and type!='textarea' and type!='multi'"><xsl:value-of select="type" /></xsl:when>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="field">
							<xsl:choose>
								<xsl:when test="$type='DOC_XML'"><xsl:value-of select="substring-after(substring-after(opsfield,'_'),'_')"/></xsl:when>
								<xsl:when test="(not(field_keep_intact) or field_keep_intact != '1') and $type!='' and substring-before(substring-after(opsfield,'_'),'_')"><xsl:value-of select="substring-before(substring-after(opsfield,'_'),'_')"/></xsl:when>
								<xsl:when test="(not(field_keep_intact) or field_keep_intact != '1') and $type!='' and substring-after(opsfield,'_')"><xsl:value-of select="substring-after(opsfield,'_')"/></xsl:when>								<xsl:when test="opsfield"><xsl:value-of select="opsfield"/></xsl:when>
								<xsl:when test="chFields"><xsl:value-of select="chFields"/></xsl:when>
							</xsl:choose>
						</xsl:variable>
						
						<xsl:variable name="dlex_id_role">
							<xsl:choose>
								<xsl:when test="$selector3!=''">
									<xsl:value-of select="$selector3" />
								</xsl:when>
								<xsl:when test="role!=''">
									<xsl:value-of select="role" />
								</xsl:when>
							</xsl:choose>
						</xsl:variable>
						
						<xsl:variable name="label" select="label"/>
						<xsl:variable name="id" select="link_id"/>
						<xsl:if test="(not(type_media) or type_media='' or ($row[name()='DOC_ID_MEDIA']!='' and contains(type_media,$row[name()='DOC_ID_MEDIA'])) or ($row[name()='MAT_ID_MEDIA']!='' and contains(type_media,$row[name()='MAT_ID_MEDIA']))) and (not(profil_min!='') or (profil_min!='' and $profil &gt;= profil_min)) and (not(profil_max!='') or (profil_max!='' and $profil &lt;= profil_max))">
							<xsl:choose>
								<xsl:when test="$type='DOC_LEX'">
									<xsl:if test="$row[name()='DOC_LEX']/XML/LEX[@TYPE=$field]!=''">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											<xsl:for-each select="$row[name()='DOC_LEX']/XML/LEX[@TYPE=$field]">
												<xsl:variable name="id_role" select="@ROLE"/>
												<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX"/></xsl:variable>
												<xsl:variable name="key_solr"><xsl:value-of select="./@KEY_SOLR"/></xsl:variable>
												<xsl:choose>
													<xsl:when test="$id_role!=''">
														<xsl:variable name="role">&lt;? global $db; echo $db->getOne("SELECT role FROM t_role WHERE id_role ILIKE '<xsl:value-of select="$id_role"/>' AND id_lang like '".$_SESSION['langue']."'"); ?&gt;</xsl:variable>
														<!--xsl:variable name="role"><xsl:value-of select="$id_role"/></xsl:variable-->
														<!--a href="#" onclick="rebondSolr('{TERME}','{$key_solr}','&lt;?= {$label} ?&gt;','V')"-->
															<xsl:value-of select="concat(TERME, ' (', $role, ')')"/>
														<!--/a-->
													</xsl:when>
													<xsl:when test="normalize-space(PRECISION)!=''">
														<!--a href="#" onclick="rebondSolr('{TERME}','{$key_solr}','&lt;?= {$label} ?&gt;','V')"-->
															<xsl:value-of select="concat(TERME, ' (', normalize-space(PRECISION), ')')"/>
														<!--/a-->
													</xsl:when>
													<xsl:otherwise>
														<!--a href="#" onclick="rebondSolr('{TERME}','{$key_solr}','&lt;?= {$label} ?&gt;','V')"-->
															<xsl:value-of select="TERME"/>
														<!--/a-->
													</xsl:otherwise>
												</xsl:choose>
												<!--xsl:value-of select="."/-->
												<xsl:if test="position()!=last()">, </xsl:if>
											</xsl:for-each>
										</td>
									</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='DOC_VAL'">
									<xsl:if test="$row[name()='DOC_VAL']/XML/*[name()=$field]!=''">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											<xsl:for-each select="$row[name()='DOC_VAL']/XML/*[name()=$field]">
												<xsl:value-of select="."/>
												<xsl:if test="position()!=last()">, </xsl:if>
											</xsl:for-each>
										</td>
									</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='DOC_CAT'">
									<xsl:if test="$row[name()='DOC_CAT']/XML/CAT[@TYPE=$field]!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='DOC_CAT']/XML/CAT[@TYPE=$field]">
													<xsl:if test="CAT_PATH != ''">
														<xsl:call-template name="replace">
															<xsl:with-param name="stream" select="CAT_PATH"/>
															<xsl:with-param name="old"><xsl:text>	</xsl:text></xsl:with-param>
															<xsl:with-param name="new"><xsl:text>/</xsl:text></xsl:with-param>
														</xsl:call-template>
														<xsl:text>/</xsl:text>
													</xsl:if>
													<xsl:value-of select="CAT_NOM"/>
													<xsl:if test="position()!=last()"><br/></xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='DOC_XML'">
									<xsl:if test="$row[name()='DOC_XML']/XML/DOC/*[name()=$field]!=''">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
												<xsl:value-of select="$row[name()='DOC_XML']/XML/DOC/*[name()=$field]"/>
										</td>
									</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='MAT_INFO'">
									<xsl:if test="$row[name()='MAT_INFO']/xml/file/*[name()=$field]!=''">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
												<xsl:value-of select="$row[name()='MAT_INFO']/xml/file/*[name()=$field]"/>
										</td>
									</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='MAT'">
									<xsl:if test="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[name()=concat('MAT_',$field)] != ''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
													<xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[name()=concat('MAT_',$field)]"/>&#160;
													<!--xsl:processing-instruction name="php">print kPages;</xsl:processing-instruction-->
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='IMG_DIM'">
									<xsl:if test="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[name()='MAT_WIDTH']!='' and $row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[name()='MAT_HEIGHT']!='' ">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
												<xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[name()='MAT_WIDTH']"/>&#160;x&#160;<xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[name()='MAT_HEIGHT']"/>px
										</td>
									</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='LEX' and $dlex_id_role!=''">
									<xsl:if test="$row[name()='t_doc_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$dlex_id_role]/t_lex/LEX_TERME!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_doc_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$dlex_id_role]">
													<xsl:variable name="key_solr"><xsl:value-of select="./@KEY_SOLR"/></xsl:variable>
													<xsl:for-each select="t_lex">
															<xsl:variable name="lex_terme"><xsl:call-template name="internal-quote-replace"><xsl:with-param name="stream" ><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="LEX_TERME"/></xsl:call-template></xsl:with-param></xsl:call-template></xsl:variable>
															<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX"/></xsl:variable>
															<a href="#" onclick="rebondSolr('{$lex_terme}','{$key_solr}','&lt;?= {$label} ?&gt;','V')"><xsl:value-of select="LEX_TERME"/></a>
															<xsl:if test="LEX_PRECISION!=''" >
																<xsl:text>(</xsl:text><xsl:value-of select="LEX_PRECISION"/><xsl:text>)</xsl:text>
															</xsl:if>
														<xsl:choose>
															<xsl:when test="position()!=last()"><xsl:text>; </xsl:text></xsl:when>
															<xsl:otherwise><br/></xsl:otherwise>
														</xsl:choose>
													</xsl:for-each>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='LEX'">
									<xsl:if test="$row[name()='t_doc_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol/t_lex/LEX_TERME!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_doc_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE='']">
													<xsl:variable name="key_solr"><xsl:value-of select="./@KEY_SOLR"/></xsl:variable>
													<xsl:for-each select="t_lex">
															<xsl:variable name="lex_terme"><xsl:call-template name="internal-quote-replace"><xsl:with-param name="stream" ><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="LEX_TERME"/></xsl:call-template></xsl:with-param></xsl:call-template></xsl:variable>
															<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX"/></xsl:variable>
															<a href="#" onclick="rebondSolr('{$lex_terme}','{$key_solr}','&lt;?= {$label} ?&gt;')"><xsl:value-of select="LEX_TERME"/></a>
															<xsl:if test="LEX_PRECISION!=''" >
																<xsl:text>(</xsl:text><xsl:value-of select="LEX_PRECISION"/><xsl:text>)</xsl:text>
															</xsl:if>
														<xsl:choose>
															<xsl:when test="position()!=last()"><xsl:text>; </xsl:text></xsl:when>
															<xsl:otherwise><br/></xsl:otherwise>
														</xsl:choose>
													</xsl:for-each>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
                                <xsl:when test="$type='PERS' and $dlex_id_role!=''">
                                    <xsl:if test="$row[name()='t_pers_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$dlex_id_role]/t_personne/PERS_TERME!=''">
                                        <tr>
                                            <td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
                                            <td class="val_champs_form">
                                               <xsl:for-each select="$row[name()='t_pers_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$dlex_id_role]/t_personne">
                                                    <xsl:variable name="id_pers"><xsl:value-of select="ID_PERS"/></xsl:variable>
                                                    <xsl:value-of select="PERS_TERME"/>
                                                    <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
                                                </xsl:for-each>
                                            </td>
                                        </tr>
                                    </xsl:if>
                                    <xsl:if test="$row[name()='t_mat_pers']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$dlex_id_role]/t_personne/PERS_NOM!=''">
                                        <tr>
                                            <td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
                                            <td class="val_champs_form">
                                                <xsl:for-each select="$row[name()='t_mat_pers']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$dlex_id_role]/t_personne">
                                                    <xsl:variable name="id_pers"><xsl:value-of select="ID_PERS"/></xsl:variable>
                                                    <xsl:value-of select="PERS_NOM"/>&amp;nbsp;<xsl:value-of select="PERS_PRENOM"/>
                                                    <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
                                                </xsl:for-each>
                                            </td>
                                        </tr>
                                    </xsl:if>
                                </xsl:when>
                                <xsl:when test="$type='PERS'">
									<xsl:if test="$row[name()='t_pers_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol/t_personne/PERS_TERME!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_pers_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@KEY_SOLR!=concat('P_',$field)]">
													<xsl:variable name="id_role"><xsl:value-of select="@DLEX_ID_ROLE"/></xsl:variable>
													<xsl:if test="$id_role!=''">
														<xsl:variable name="role">&lt;? global $db; echo $db->getOne("SELECT role FROM t_role WHERE id_role ILIKE '<xsl:value-of select="@DLEX_ID_ROLE"/>' AND id_lang like '".$_SESSION['langue']."'"); ?&gt;</xsl:variable>
														<xsl:value-of select="$role"/><xsl:text> : </xsl:text> 
													</xsl:if>
													<xsl:for-each select="t_personne">
														<xsl:variable name="id_pers"><xsl:value-of select="ID_PERS"/></xsl:variable>
														<xsl:value-of select="PERS_TERME"/>
														<xsl:choose>
															<xsl:when test="position()!=last()"><xsl:text>, </xsl:text></xsl:when>
															<xsl:otherwise><br/></xsl:otherwise>
														</xsl:choose>
													</xsl:for-each>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="$row[name()='t_mat_pers']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol/t_personne/PERS_NOM!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_mat_pers']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol">
													<xsl:value-of select="t_personne/ROLE"/><xsl:text> : </xsl:text> 
													<xsl:for-each select="t_personne">
														<xsl:variable name="id_pers"><xsl:value-of select="ID_PERS"/></xsl:variable>
														<xsl:value-of select="PERS_NOM"/>&amp;nbsp;<xsl:value-of select="PERS_PRENOM"/>
														<xsl:choose>
															<xsl:when test="position()!=last()"><xsl:text>, </xsl:text></xsl:when>
															<xsl:otherwise><br/></xsl:otherwise>
														</xsl:choose>
													</xsl:for-each>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='FEST'">
									<xsl:if test="$row[name()='t_doc_fest']!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_doc_fest']/FEST">
													<xsl:copy-of select="FEST_LIBELLE"/>
													<xsl:if test="position()!=last()">
														<xsl:text>, </xsl:text>
													</xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='CAT'">
									<xsl:if test="$row[name()='t_doc_cat']/t_categorie[CAT_ID_TYPE_CAT=$field]!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_doc_cat']/t_categorie[CAT_ID_TYPE_CAT=$field and not(@id_cat)]">
													<xsl:value-of select="CAT_NOM"/>
													<xsl:if test="position()!=last()">
														<xsl:text>, </xsl:text>
													</xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when >
								<xsl:when test="$type='VAL'">
									<xsl:if test="$row[name()='t_mat_val']/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val/VALEUR!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_mat_val']/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val">
													<xsl:variable name="key_solr"><xsl:value-of select="KEY_SOLR"/></xsl:variable>
													<xsl:variable name="val_terme"><xsl:call-template name="internal-quote-replace"><xsl:with-param name="stream" ><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="VALEUR"/></xsl:call-template></xsl:with-param></xsl:call-template></xsl:variable>
													<xsl:value-of select="VALEUR"/>
													<xsl:if test="position()!=last()">
														<xsl:text>, </xsl:text>
													</xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="$row[name()='t_pers_val']/TYPE_VAL[@VAL_ID_TYPE_VAL=$field]/t_val/VALEUR!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_pers_val']/TYPE_VAL[@VAL_ID_TYPE_VAL=$field]/t_val">
													<xsl:sort select="VALEUR"/>
													<xsl:value-of select="VALEUR"/>
													<xsl:if test="position()!=last()">
														<xsl:text>, </xsl:text>
													</xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="$row[name()='t_doc_val']/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val/VALEUR!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_doc_val']/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val">
													<xsl:variable name="key_solr"><xsl:value-of select="KEY_SOLR"/></xsl:variable>
													<xsl:variable name="val_terme"><xsl:call-template name="internal-quote-replace"><xsl:with-param name="stream" ><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="VALEUR"/></xsl:call-template></xsl:with-param></xsl:call-template></xsl:variable>
													<a href="#" onclick="rebondSolr('{$val_terme}','{$key_solr}','&lt;?= {$label} ?&gt;')"><xsl:value-of select="VALEUR"/></a>
													<xsl:if test="position()!=last()">
														<xsl:text>, </xsl:text>
													</xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="$row[name()='t_pers_val']/TYPE_VAL[@VAL_ID_TYPE_VAL=$field]/t_val/VALEUR!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_pers_val']/TYPE_VAL[@VAL_ID_TYPE_VAL=$field]/t_val">
													<xsl:value-of select="VALEUR"/>
													<xsl:if test="position()!=last()">
														<xsl:text>, </xsl:text>
													</xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$field='t_personne_father'">
									<xsl:if test="$row[name()='t_personne_father']/t_personne!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_personne_father']/t_personne">
													<xsl:value-of select="PERS_NOM"/>&amp;nbsp;<xsl:value-of select="PERS_PRENOM"/>
													<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$field='t_personne_syno'">
									<xsl:if test="$row[name()='t_personne_syno']/t_personne!=''">
										<tr>
											<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
											<td class="val_champs_form">
												<xsl:for-each select="$row[name()='t_personne_syno']/t_personne">
													<xsl:value-of select="PERS_NOM"/>&amp;nbsp;<xsl:value-of select="PERS_PRENOM"/>
													<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
												</xsl:for-each>
											</td>
										</tr>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$field='DOC_ID_MEDIA'">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											<xsl:choose>
											<xsl:when test="$row[name()=$field]='V'"><xsl:processing-instruction name="php">print kVideo</xsl:processing-instruction></xsl:when>
											<xsl:when test="$row[name()=$field]='A'"><xsl:processing-instruction name="php">print kAudio</xsl:processing-instruction></xsl:when>
											<xsl:when test="$row[name()=$field]='P'"><xsl:processing-instruction name="php">print kPhoto</xsl:processing-instruction></xsl:when>
											<xsl:when test="$row[name()=$field]='D'"><xsl:processing-instruction name="php">print kDocument</xsl:processing-instruction></xsl:when>
											<xsl:when test="$row[name()=$field]='R'"><xsl:processing-instruction name="php">print kReportage</xsl:processing-instruction></xsl:when>
											</xsl:choose>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$field='DOC_ID_ETAT_DOC'">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form"><xsl:value-of select="$row[name()='DOC_XML']/XML/DOC/ETAT_DOC"/></td>
									</tr>
								</xsl:when>
								<xsl:when test="$field='PERS_ID_TYPE_LEX'">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form"><xsl:value-of select="$row[name()='TYPE_LEX']"/></td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='tracks'">
									<xsl:variable name="chlabel" select="label"/>
									<xsl:variable name="tablabel" select="innerFields/element"/>
									<xsl:if test="number($row[name()=$field]/@nb)>0">
										<xsl:for-each select="$row[name()=$field]/child::*">
											<xsl:variable name="tabval" select="self::*"/>
											<xsl:variable name="j"><xsl:number/></xsl:variable>
											<tr>
												<td class="label_champs_form" valign="top">
													<a href="javascript:toggleVisibility(document.getElementById('track_{$j}'),document.getElementById('arrowident_{$j}'))" >
														<img src='design/images/arrow_right.gif' align='absmiddle' border='0' id='arrowident_{$j}' /><xsl:processing-instruction name="php">print <xsl:value-of select="$chlabel"/>;</xsl:processing-instruction><xsl:value-of select="concat(' ',($j - 1),' - ',MT_TYPE)"/></a><!-- xsl:number commence la numÃ©rotation Ã  1, on affiche donc l'index -1 -->
												</td>
												<td class="val_champs_form">
													<table id="track_{$j}" border="0" cellspacing="0" style="display: none; ">
														<xsl:for-each select="$tablabel">
															<xsl:variable name="fieldname"><xsl:value-of select="chFields"/></xsl:variable>
															<xsl:variable name="fieldnamevalue">
																<xsl:for-each select="$tabval">
																	<xsl:for-each select="child::*">
																		<xsl:if test="$fieldname=name()">
																			<xsl:value-of select="self::*"/>
																		</xsl:if>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:variable>
															<xsl:if test="$fieldnamevalue!='' and ($fieldnamevalue!='0' or $fieldname = 'MT_INDEX')">
																<tr>
																	<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
																	<td class="val_champs_form">
																		<xsl:value-of select="$fieldnamevalue"/>
																	</td>
																</tr>
															</xsl:if>
														</xsl:for-each>
													</table>
												</td>
											</tr>
										</xsl:for-each>
									</xsl:if>
								</xsl:when>
								<xsl:when test="$type='booleen'">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											<xsl:choose>
											<xsl:when test="contains($row[name()=$field],'1')"><xsl:processing-instruction name="php">print kOui</xsl:processing-instruction></xsl:when>
											<xsl:otherwise><xsl:processing-instruction name="php">print kNon</xsl:processing-instruction></xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$field = 'DOC_ACCES'">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											<xsl:choose>
											<xsl:when test="contains($row[name()=$field],'1')"><xsl:processing-instruction name="php">print kEnLigne</xsl:processing-instruction></xsl:when>
											<xsl:otherwise><xsl:processing-instruction name="php">print kAccesInterdit</xsl:processing-instruction></xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="(contains($field,'DOC_DATE') or contains($field,'MAT_DATE') or $type='date') and $row[name()=$field] != '' and not(prevent_date_format)">
									<tr>
										<td class="label_champs_form"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											<xsl:call-template name="format_date">
												<xsl:with-param name="chaine_date" select="$row[name()=$field]"/>
											</xsl:call-template>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='link' and img!=''">
									<tr>
										<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form"><xsl:value-of select="concat('img=',img)"/>
									<a href="#{$row[name()=$id]}" onclick="javascript:{link_popup}('{concat(link_url,$row[name()=$id])}','main','{$row[name()=$id]}')"><img src="{img}" border="0"/></a>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='img' and $row[name()=$field] != ''">
									<div class="val_champs_form">
										<img border='0' src='{$row[name()=$field]}' width='200' title='' alt='' style="float:right" />
									</div>
								</xsl:when>
								<xsl:when test="$type='link'">
									<tr>
										<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form"><xsl:value-of select="concat('img=',img)"/>
									<a href="#{$row[name()=$id]}" onclick="javascript:{link_popup}('{concat(link_url,$row[name()=$id])}','main','{$row[name()=$id]}')"><xsl:value-of select="$row[name()=$field]"/></a>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='ADD_XML' and $row/../../ADD_XML/*[name()=$field] != '' ">
									<tr>
										<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											<xsl:if test="$row/../../ADD_XML/*[name()=$field] != '' ">
												<xsl:value-of select="$row/../../ADD_XML/*[name()=$field]"/>
											</xsl:if>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='code_embed_player_export'">
									<xsl:variable name="short_url">&lt;?= kCheminHttp?&gt;/player_export.php?token=<xsl:value-of select="$row[name()=$field]"/></xsl:variable>
									<tr>
										<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
											&amp;lt;iframe src="<xsl:value-of select="$short_url" />&amp;display=simple&amp;width=720&amp;height=405"&amp;gt;&amp;lt;/iframe&amp;gt;
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$row[name()=$field]!='' and $row[name()=$field]!='0000-00-00' and $row[name()=$field]!='00:00:00 '">
									<tr>
										<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></td>
										<td class="val_champs_form">
									<xsl:call-template name="nl2br"><xsl:with-param name="stream" select="$row[name()=$field]"/></xsl:call-template>		
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='facebook'">
									<tr>
										<td class="label_champs_form">
											<xsl:processing-instruction name="php">print kFacebook;</xsl:processing-instruction>
										</td>
										<td class="val_champs_form">
											<xsl:variable name="safe_titre">
												<xsl:call-template name="internal-quote-replace">
													<xsl:with-param name="stream" select="$row[name()='DOC_TITRE']" />
												</xsl:call-template>
											</xsl:variable>
											<!--<a href="http://www.facebook.com/sharer.php?u={$url_reseaux_sociaux}&amp;t={$doc_titre}" target="_blank">-->
											<a onclick="shareFacebook('&lt;?php echo kCheminHttp; ?&gt;',{$row[name()='ID_DOC']},'{$safe_titre}'); return false;" href="javascript:void(0);">
												<img alt="Facebook" src="design/images/share_facebook.png" />
											</a>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='twitter'">
									<xsl:variable name="doc_res">
										<xsl:call-template name="internal-quote-replace">
											<xsl:with-param name="stream" select="t_doc/DOC_RES"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="safe_titre">
										<xsl:call-template name="internal-quote-replace">
											<xsl:with-param name="stream" select="$row[name()='DOC_TITRE']" />
										</xsl:call-template>
									</xsl:variable>
									<tr>
										<td class="label_champs_form">
											<xsl:processing-instruction name="php">print kTwitter;</xsl:processing-instruction>
										</td>
										<td class="val_champs_form">
											<a onclick="shareTwitter('&lt;?php echo kCheminHttp; ?&gt;',{$row[name()='ID_DOC']},'{$safe_titre}'); return false;" href="javascript:void(0);" target="_blank">
												<img alt="Twitter" src="design/images/share_twitter.png" />
											</a>
										</td>
									</tr>
								</xsl:when>
								<xsl:when test="$type='linkedin'">
									<xsl:variable name="doc_res">
										<xsl:call-template name="internal-quote-replace">
											<xsl:with-param name="stream" select="t_doc/DOC_RES"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="safe_titre">
										<xsl:call-template name="internal-quote-replace">
											<xsl:with-param name="stream" select="$row[name()='DOC_TITRE']" />
										</xsl:call-template>
									</xsl:variable>
									<tr>
										<td class="label_champs_form">
											<xsl:processing-instruction name="php">print kLinkedIn;</xsl:processing-instruction>
										</td>
										<td class="val_champs_form">
											<a onclick="shareLinkedIn('&lt;?php echo kCheminHttp; ?&gt;',{$row[name()='ID_DOC']},'{$safe_titre}'); return false;" href="javascript:void(0);" target="_blank">
												<img alt="Linked in" src="design/images/share_linkedin.png" />
											</a>
										</td>
									</tr>
								</xsl:when>
								<!--xsl:otherwise><tr><td>_<xsl:value-of select="$selector" /></td></tr></xsl:otherwise-->
							</xsl:choose>
						</xsl:if>
					</xsl:for-each>
						<tr><td>&#160;</td></tr>
				</table>
				</fieldset>
                    </xsl:if>
					</xsl:for-each>
            </xsl:for-each>
    
    </xsl:template>
    
</xsl:stylesheet>
