<div id="fiche_info" class="<?=($entity_has_media)?'':'no_media'; ?>" > 
	<? 
	global $db;
	
	if(!defined("gSitePublications")){ // default : define("gSitePublications",serialize(array("youtube" => "Youtube", "dailymotion" => "Daily Motion")));
		echo "Constante de configuration manquante : gSitePublications";
	}
	
	$arrCible =unserialize(gSitePublications);
	if(isset($_POST['bPub']) && isset($_POST['target'])){
		$target=$_POST['target'];
	}
	if (!empty($target) && isset($arrCible[$target]) && $_POST["action"][$target] == "PUB") {
		
		if(isset($_POST['id_etape'][$target])){
			$id_etape = $_POST["id_etape"][$target];
		}
		
		if ($_POST["is_star"][$target] == "1") {
			$myPrms["job_param"] = "<video_star>1</video_star>";
		}
		
		//Insertion t_doc_publication
		if(!isset($myDoc->t_doc_publication[$target]) && isset($id_etape)){
			$myDoc->t_doc_publication[$target] = array();
			$myDoc->t_doc_publication[$target]["PUB_ID_ETAPE"]=$id_etape;
		} elseif(isset($myDoc->t_doc_publication[$target])) {
			$id_etape = $myDoc->t_doc_publication[$target]["PUB_ID_ETAPE"];
		}
		
		$myDoc->t_doc_publication[$target]["PUB_CIBLE"]= $target;
		$myDoc->t_doc_publication[$target]["PUB_DATE"] = date("Y-m-d H:i:s");
		
		//Creation jobs
		$myPrms["type"] = "doc";
		foreach ($myDoc->t_doc_mat as $dm)
			if (strpos($dm["MAT"]->t_mat['MAT_NOM'], "_ex.mp4") > 0) {
				$mats[0]["ID_MAT"] = $dm["MAT"]->t_mat['ID_MAT'];
				$myPrms["id_etape"] = $id_etape;
			}
		if (!isset($mats[0]["ID_MAT"]) || !Materiel::checkFileExistsById($mats[0]["ID_MAT"])) {
			$jobP = $db->getRow("SELECT ID_JOB, JOB_ID_MAT FROM t_job j INNER JOIN t_module m ON m.ID_MODULE = j.JOB_ID_MODULE WHERE j.JOB_PARAM like '%<ID_DOC>".$myDoc->t_doc['ID_DOC']."</ID_DOC>%' AND m.MODULE_TYPE like 'ENCOD' AND j.JOB_ID_ETAT in (".jobAttente.", ".jobEnCours.") AND JOB_OUT like '%_ex.mp4';");
			if ($jobP) {
				$mats[0]["ID_MAT"] = $jobP["JOB_ID_MAT"];
				$myPrms["id_etape"] = $id_etape;
				$myPrms["id_job_prec"] = $jobP["ID_JOB"];
			}
		}
		if (!isset($mats[0]["ID_MAT"]) || !Materiel::checkFileExistsById($mats[0]["ID_MAT"])) {
			$mats=$db->GetAll("select T1.ID_MAT,T2.MAT_COTE_ORI,T1.DMAT_TCIN as TCIN,T1.DMAT_TCOUT as TCOUT from t_doc_mat T1 left outer join t_mat T2 on T1.ID_MAT=T2.ID_MAT where T2.MAT_TYPE='MASTER' and T1.ID_DOC=".intval($myDoc->t_doc['ID_DOC']));
			if (empty($mats) || !Materiel::checkFileExistsById($mats[0]["ID_MAT"])) $mats=$db->GetAll("select T1.ID_MAT,T2.MAT_COTE_ORI,T1.DMAT_TCIN as TCIN,T1.DMAT_TCOUT as TCOUT from t_doc_mat T1 left outer join t_mat T2 on T1.ID_MAT=T2.ID_MAT where (T2.MAT_NOM like '%.mov' or T2.MAT_NOM like '%.mpg' or (T2.MAT_NOM like '%.mp4' AND T2.MAT_NOM not like '%_vis.mp4')) and T1.ID_DOC=".intval($myDoc->t_doc['ID_DOC']));
			if (empty($mats) || !Materiel::checkFileExistsById($mats[0]["ID_MAT"])) $mats=$db->GetAll("select T1.ID_MAT,T2.MAT_COTE_ORI,T1.DMAT_TCIN as TCIN,T1.DMAT_TCOUT as TCOUT from t_doc_mat T1 left outer join t_mat T2 on T1.ID_MAT=T2.ID_MAT where T2.MAT_TYPE='VISIO' and T1.ID_DOC=".intval($myDoc->t_doc['ID_DOC']));
			if (empty($mats) || !Materiel::checkFileExistsById($mats[0]["ID_MAT"])) $mats=$db->GetAll("select T1.ID_MAT,T2.MAT_COTE_ORI,T1.DMAT_TCIN as TCIN,T1.DMAT_TCOUT as TCOUT from t_doc_mat T1 left outer join t_mat T2 on T1.ID_MAT=T2.ID_MAT left outer join t_format_mat T3 on T2.MAT_FORMAT=T3.FORMAT_MAT where T3.FORMAT_ONLINE='1' and T1.ID_DOC=".intval($myDoc->t_doc['ID_DOC']));
						
			$myPrms["id_proc"] = $db->getOne("SELECT p.ID_PROC FROM t_proc p INNER JOIN t_proc_etape pe ON pe.ID_PROC = p.ID_PROC WHERE pe.ID_ETAPE = ".intval($id_etape));
			if (!$myPrms["id_proc"] || empty($myPrms["id_proc"])) $myPrms["id_etape"] = $id_etape;
		}
		$mats[0]["ID_DOC"]=$myDoc->t_doc['ID_DOC'];
		$myDoc->saveDocPublication();
			
		$myPrms["lancement"] = "proc";
		$myPrms["noDspProcs"] = true;
		include_once(includeDir."jobListe.php");
		if(isset($jobObj)){
			$myDoc->t_doc_publication[$target]["PUB_ID_JOB"] = $jobObj;
			$myDoc->saveDocPublication();
			$myDoc->getDocPublication();
		}
	}
	elseif (!empty($target) && isset($arrCible[$target]) && $_POST["action"][$target] == "UPDATE") {
		
		if ($target == "youtube"){
			/* MAJ INFO YT */
			require_once libDir.'google-api-php-client/src/Google_Client.php';
			require_once libDir.'google-api-php-client/src/contrib/Google_YouTubeService.php';
			
			$id_etape = $myDoc->t_doc_publication[$target]['PUB_ID_ETAPE'];
			$donnees=$db->Execute('SELECT ETAPE_PARAM FROM t_etape WHERE ID_ETAPE='.intval($id_etape));
			$donnees=$donnees->GetArray();
			$donnees=$donnees[0]['ETAPE_PARAM'];
			$donnees=xml2array($donnees);
			$param=$donnees['param'];
			
			$client_id      =   $param['id_client'];
			$client_secret  =   $param['api_secret'];
			$refresh_token  =   $param['token'];
			$token_url 		= 	"https://accounts.google.com/o/oauth2/token";
			$id_video 		= 	$myDoc->t_doc_publication[$target]['PUB_VALUE'];
			
			$client = new Google_Client();
			$client->setClientId($client_id);
			$client->setClientSecret($client_secret);
			$client->setScopes("https://www.googleapis.com/auth/youtube");
			$youtube = new Google_YoutubeService($client);
			
			try { 
				$post_data = array('client_secret' =>   $client_secret,
									'grant_type'    =>   'refresh_token',
									'refresh_token' =>   $refresh_token,
									'client_id'     =>   $client_id
									);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $token_url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$result = curl_exec($ch);
				$token_object = json_decode($result);
				$access_token = $token_object->access_token;

				$result = array("access_token" => $access_token,
								"token_type" => "Bearer",
								"expires_in" => 3600,
								"refresh_token" => $refresh_token,
								"created" => time());
				$result = json_encode($result);
				$client->setAccessToken($result);
			}
			catch (Exception $e)
			{
				echo 'Authentification error : '.$e->getMessage();
			}
			
			if ($client->getAccessToken()) {
				try
				{
					$listResponse = $youtube->videos->listVideos("snippet", array('id' => $id_video));
					$videoList = $listResponse['items'];
					if (!empty($videoList)) {
						$video = $videoList[0];
						$videoSnippet = $video['snippet'];
						$updateSnippet = new Google_VideoSnippet($videoSnippet);
						
						/*------------- update données depuis Opsis ---------*/
						$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$myDoc->xml_export()."</EXPORT_OPSIS>";
						$xml_params = TraitementXSLT($xml,getSiteFile("designDir","print/".$param["doc_xsl"]),null,0,"",null,false);
						$xml_tab = xml2tab($xml_params);
					
						$yt_donnees = $xml_tab['DONNEES'][0];
						
						if(isset($yt_donnees['TITLE'])){
							$yt_title = $yt_donnees['TITLE'];
							$updateSnippet->setTitle($yt_title);
						}
						if(isset($yt_donnees['DESCRIPTION'])){
							$yt_description = $yt_donnees['DESCRIPTION'];
							$updateSnippet->setDescription($yt_description);
						}
						if(isset($yt_donnees['TAGS'])){
							$yt_tags= explode(',',$yt_donnees['TAGS']);
							$updateSnippet->setTags($yt_tags);
						}
						if(isset($yt_donnees['CHANNEL_ID']) && !empty($yt_donnees['CHANNEL_ID'])){
							$yt_categ_id= $yt_donnees['CHANNEL_ID'];
							$updateSnippet->setCategoryId($yt_categ_id);
						}
						$status = new Google_VideoStatus();
						if(isset($yt_donnees['PRIVATE']) && !empty($yt_donnees['PRIVATE']) && ($yt_donnees['PRIVATE'] == '1' || $yt_donnees['PRIVATE'] =='true')){
							$status->setPrivacyStatus('private');
						}else{
							$status->setPrivacyStatus('public'); 
						}
						/* --- */
						
						
						$updateVideo = new Google_Video($video);
						$updateVideo->setSnippet($updateSnippet);	
						$updateVideo->setStatus($status);
						$updateResponse = $youtube->videos->update("status,snippet", $updateVideo);
						echo kSuccesMiseAJourYT;
					}
				}
				catch (Exception $e)
				{
					echo 'Exception : '.$e->getMessage();
				}
			}
		}else if ($target == "dailymotion"){
			/* MAJ INFO DM */
			require_once(libDir.'dailymotion_sdk/Dailymotion.php');

			
			$id_etape = $myDoc->t_doc_publication[$target]['PUB_ID_ETAPE'];
			$donnees=$db->Execute('SELECT ETAPE_PARAM FROM t_etape WHERE ID_ETAPE='.intval($id_etape));
			$donnees=$donnees->GetArray();
			$donnees=$donnees[0]['ETAPE_PARAM'];
			$donnees=xml2array($donnees);
			$param=$donnees['param'];
			
			// var_dump($param);
			
			$api_key=$param['api_key'];
			$api_secret=$param['api_secret'];
			$user_login = array('username'=>$param['user'], 'password' =>$param['pass'],'scope'=>'read write');
			$id_video =	$myDoc->t_doc_publication[$target]['PUB_VALUE'];
			
			
			/*connexion au compte DM*/
			$api = new Dailymotion();
			$api->setGrantType(Dailymotion::GRANT_TYPE_PASSWORD, $api_key, $api_secret,array("manage_videos","write","delete","manage_playlists"),$user_login);
			try
			{
			
				/*------------- update données depuis Opsis ---------*/
				$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$myDoc->xml_export()."</EXPORT_OPSIS>";
				$xml_params = TraitementXSLT($xml,getSiteFile("designDir","print/".$param["doc_xsl"]),null,0,"",null,false);
				$xml_tab = xml2tab($xml_params);
				$dm_donnees = $xml_tab['DONNEES'][0];
				
				$donnees_video=array
				(
					'id'=>$id_video,
				);
				
				
				if(isset($dm_donnees['TITLE'])){
					$donnees_video['title'] = $dm_donnees['TITLE'];
				}
				if(isset($dm_donnees['DESCRIPTION']) ){
					$donnees_video['description'] = $dm_donnees['DESCRIPTION'];
				}
				if(isset($dm_donnees['CHANNEL']) ){
					$donnees_video['channel'] = $dm_donnees['CHANNEL'];
				}
				if(isset($dm_donnees['TAGS'])){
					$donnees_video['tags'] = explode(',',$dm_donnees['TAGS']);
				}
				if(isset($dm_donnees['OFFICIAL']) ){
					$donnees_video['official'] = $dm_donnees['OFFICIAL'];
				}
				if(isset($dm_donnees['CREATIVE']) ){
					$donnees_video['creative'] = $dm_donnees['CREATIVE'];
				}
				
				if(isset($dm_donnees['PUBLISHED']) && !empty($dm_donnees['PUBLISHED']) && ($dm_donnees['PUBLISHED'] == '1' || $dm_donnees['PUBLISHED'] =='true')){
					$donnees_video['published'] = true ; 
				}else{
					$donnees_video['published'] = false;
				}
				if(isset($dm_donnees['PRIVATE']) && !empty($dm_donnees['PRIVATE']) && ($dm_donnees['PRIVATE'] == '1' || $dm_donnees['PRIVATE'] =='true')){
					$donnees_video['private'] = true ; 
				}else{
					$donnees_video['private'] = false;
				}
				
				if(isset($dm_donnees['ALLOW_COMMENTS']) && !empty($dm_donnees['ALLOW_COMMENTS']) && ($dm_donnees['ALLOW_COMMENTS'] == '1' || $dm_donnees['ALLOW_COMMENTS'] =='true')){
					$donnees_video['allow_comments'] = true ; 
				}else{
					$donnees_video['allow_comments'] = false;
				}

				$result=$api->call('video.edit',$donnees_video);
				
				echo kSuccesMiseAJourDM;
				
				// MS - 09/01/14 - Gestion des playlist & de la vidéo star non reportées ici, voir cadremploi mais devra probablement être spécifique à un site donné.
		
			}
			catch (DailymotionAuthRequiredException $e)
			{
					echo 'DailymotionAuthRequiredException '.$e->getMessage();
			}
			catch (DailymotionApiException $e)
			{
					echo 'DailymotionApiException '.$e->getMessage();
			}
		}
	}elseif (!empty($target) && isset($arrCible[$target]) && $_POST["action"][$target] == "DEPUB") {
		$id_etape = $myDoc->t_doc_publication[$target]['PUB_ID_ETAPE'];
		$donnees=$db->Execute('SELECT ETAPE_PARAM FROM t_etape WHERE ID_ETAPE='.intval($id_etape));
		$donnees=$donnees->GetArray();
		$donnees=$donnees[0]['ETAPE_PARAM'];
		$donnees=xml2array($donnees);
		$param=$donnees['param'];
	
		if ($target == "youtube" && isset($param["token"])) { //API v3
			require_once libDir.'google-api-php-client/src/Google_Client.php';
			require_once libDir.'google-api-php-client/src/contrib/Google_YouTubeService.php';
	
			$client_id      =   $param['id_client'];
			$client_secret  =   $param['api_secret'];
			$refresh_token  =   $param['token'];
			$token_url 		= 	"https://accounts.google.com/o/oauth2/token";
			$id_video 		= 	$myDoc->t_doc_publication[$target]['PUB_VALUE'];
			
			$client = new Google_Client();
			$client->setClientId($client_id);
			$client->setClientSecret($client_secret);
			$client->setScopes("https://www.googleapis.com/auth/youtube");
			$youtube = new Google_YoutubeService($client);
			
			/* get token */
			$post_data = array(
								'client_secret' =>   $client_secret,
								'grant_type'    =>   'refresh_token',
								'refresh_token' =>   $refresh_token,
								'client_id'     =>   $client_id
								);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $token_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			$token_object = json_decode($result);
			$access_token = $token_object->access_token;

			$result = array("access_token" => $access_token,
							"token_type" => "Bearer",
							"expires_in" => 3600,
							"refresh_token" => $refresh_token,
							"created" => time());
			$result = json_encode($result);
			$client->setAccessToken($result);
			
			if ($client->getAccessToken()) {
				try
				{
					$youtube->videos->delete($id_video);
				}
				catch (Exception $ex)
				{
					$myDoc->dropError('Exception : '.$ex->getMessage());
				}
				unset($myDoc->t_doc_publication[$target]);
				$myDoc->saveDocPublication();
			}
		}
		elseif ($target == "youtube") {
			$include_path_defaut=ini_get('include_path');
			ini_set('include_path',libDir.':.');
			require_once(libDir.'Zend/Loader.php');
			Zend_Loader::loadClass('Zend_Gdata_YouTube');
			Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
			Zend_Loader::loadClass('Zend_Gdata_App_Exception');
			
			$http_client=null;

			$username=$param['user'];
			$password=$param['pass'];
			$service='youtube';
			$client=null;
			$source=$param['source'];
			$login_token=null;
			$login_captcha=null;
			$authentication_url= 'https://www.google.com/accounts/ClientLogin';
			try
			{
				$http_client = Zend_Gdata_ClientLogin::getHttpClient($username,$password,$service,$client,$source,$login_token,$login_captcha,$authentication_url);
			}
			catch (Exception $e)
			{
				$myDoc->dropError('Exception : '.$e->getMessage());
			}

			$id_application=$param['id_application'];
			$id_client=$param['id_client'];
			$api_key=$param['api_key'];

			try
			{
				$yt = new Zend_Gdata_YouTube($http_client,$id_application,null,$api_key);
			}
			catch (Zend_Gdata_App_Exception $e)
			{
				$myDoc->dropError('Zend_Gdata_YouTube=>Zend_Gdata_App_Exception : '.$e->getMessage());
			}
			
			$video_entry = $yt->getVideoEntry($myDoc->t_doc_publication[$target]['PUB_VALUE'],null,true);
			$yt->delete($video_entry);
			
			ini_set('include_path',$include_path_defaut);
			unset($myDoc->t_doc_publication[$target]);
			$myDoc->saveDocPublication();
		}
		elseif ($target == "dailymotion") {
			require_once(libDir.'dailymotion_sdk/Dailymotion.php');
			$api_key=$param['api_key'];
			$api_secret=$param['api_secret'];
			$user_login = array('username'=>$param['user'], 'password' =>$param['pass'],'scope'=>'read write delete');

			$api = new Dailymotion();
			$api->setGrantType(Dailymotion::GRANT_TYPE_PASSWORD, $api_key, $api_secret,null,$user_login);
			$result=$api->call('video.delete',array('id'=>$myDoc->t_doc_publication[$target]['PUB_VALUE']));
			
			unset($myDoc->t_doc_publication[$target]);
			$myDoc->saveDocPublication();
		}
	}elseif (!empty($target) && isset($arrCible[$target]) && $_POST["action"][$target] == "STAR") {
		$id_etape = $myDoc->t_doc_publication[$target]['PUB_ID_ETAPE'];
		$donnees=$db->Execute('SELECT ETAPE_PARAM FROM t_etape WHERE ID_ETAPE='.intval($id_etape));
		$donnees=$donnees->GetArray();
		$donnees=$donnees[0]['ETAPE_PARAM'];
		$donnees=xml2array($donnees);
		$param=$donnees['param'];
		$id_video = $myDoc->t_doc_publication[$target]['PUB_VALUE'];
	
		if ($target == "youtube" && isset($param["token"])) { //API v3
			require_once libDir.'google-api-php-client/src/Google_Client.php';
			require_once libDir.'google-api-php-client/src/contrib/Google_YouTubeService.php';
	
			$client_id      =   $param['id_client'];
			$client_secret  =   $param['api_secret'];
			$refresh_token  =   $param['token'];
			$token_url 		= 	"https://accounts.google.com/o/oauth2/token";
			
			$client = new Google_Client();
			$client->setClientId($client_id);
			$client->setClientSecret($client_secret);
			$client->setScopes("https://www.googleapis.com/auth/youtube");
			$youtube = new Google_YoutubeService($client);
			
			/* get token */
			$post_data = array(
								'client_secret' =>   $client_secret,
								'grant_type'    =>   'refresh_token',
								'refresh_token' =>   $refresh_token,
								'client_id'     =>   $client_id
								);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $token_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			$token_object = json_decode($result);
			$access_token = $token_object->access_token;

			$result = array("access_token" => $access_token,
							"token_type" => "Bearer",
							"expires_in" => 3600,
							"refresh_token" => $refresh_token,
							"created" => time());
			$result = json_encode($result);
			$client->setAccessToken($result);
			
			if ($client->getAccessToken()) {
				try
				{
					$channelsResponse = $youtube->channels->listChannels('id,brandingSettings', array('mine' => 'true',));
					if (isset($channelsResponse["items"][0])) {
						$chan = $channelsResponse["items"][0];
						$channelId = $chan["id"];
						
						$bran = $chan["brandingSettings"];
						
						$setG = new Google_ChannelSettings();
						$setG->setUnsubscribedTrailer($id_video);
						$setG->setTitle($bran["channel"]["title"]);
						$setG->setProfileColor($bran["channel"]["profileColor"]);
						
						$brandG = new Google_ChannelBrandingSettings();
						$brandG->setChannel($setG);
						
						$chanG = new Google_Channel();
						$chanG->setId($channelId);
						$chanG->setBrandingSettings($brandG);
						
						$res = $youtube->channels->update('id,brandingSettings', $chanG, array());
						echo kSuccesMiseEnAvant;
					}
				}
				catch (Exception $ex)
				{
					$myDoc->dropError('Exception : '.$ex->getMessage());
				}
			}
		}
		elseif ($target == "youtube") {
			//TODO
		}
		elseif ($target == "dailymotion") {
			require_once(libDir.'dailymotion_sdk/Dailymotion.php');
			$api_key=$param['api_key'];
			$api_secret=$param['api_secret'];
			$user_login = array('username'=>$param['user'], 'password' =>$param['pass'],'scope'=>'read write delete');

			$api = new Dailymotion();
			$api->setGrantType(Dailymotion::GRANT_TYPE_PASSWORD, $api_key, $api_secret,null,$user_login);
			
			$id_user = $api->call('GET /video/'.$id_video,array('fields'=>'owner.id'));
			$id_user = $id_user['owner.id'];
			$api->call('POST /user/'.$id_user,array('videostar'=>$id_video));
			
			echo kSuccesMiseEnAvant;
		}
	}
	
	if (true) { 
		?>
			<form name="formPublication" method="POST" action="">
				<input type="hidden" name="target" value="" />
				<table cellpadding="0" cellspacing="0" class="tableResults" style="margin:auto;min-width:50%">		
					<tr>
						<td class="resultsHead">Cible</td>
						<td class="resultsHead">Traitement</td>
						<td class="resultsHead">Action</td>
					</tr>
					<? foreach ($arrCible as $idc=>$cible) { ?>
						<tr>
							<td class="resultsCorps"><?=$cible;?></td>
							<td class="resultsCorps"><?
								if (isset($myDoc->t_doc_publication[$idc])) {
									$sql = "SELECT ETAPE_NOM from t_etape where ID_ETAPE = ".intval($myDoc->t_doc_publication[$idc]['PUB_ID_ETAPE']);
									echo $db->getOne($sql);
								}
								else {
									getEtapes($idc);
								}
							?></td>
							<td class="resultsCorps"><?
								if (isset($myDoc->t_doc_publication[$idc]) && !empty($myDoc->t_doc_publication[$idc]['PUB_VALUE'])) {
									?>
									<input type="hidden" id="action_<?=$idc?>" name='action[<?= $idc ?>]' />
									<input name="bPub" type="submit" value="<?= kMettreAJour ?>" onClick="document.getElementById('action_<?=$idc?>').value='UPDATE';document.formPublication.target.value='<?= $idc ?>'" />
									<input name="bPub" type="submit" value="<?= kDepublier ?>" onClick="document.getElementById('action_<?=$idc?>').value='DEPUB';document.formPublication.target.value='<?= $idc ?>'" />
									<input name="bPub" type="submit" value="<?= kMettreEnAvant ?>" onClick="document.getElementById('action_<?=$idc?>').value='STAR';document.formPublication.target.value='<?= $idc ?>'" /><?
								}
								elseif (isset($myDoc->t_doc_publication[$idc]) && empty($myDoc->t_doc_publication[$idc]['PUB_VALUE']) && ($myDoc->t_doc_publication[$idc]['ETAT_PUB'] != jobErreur) ) {
									echo kEnCoursDeTraitement;
								}
								else {
									if($myDoc->t_doc_publication[$idc]['ETAT_PUB'] == jobErreur){
										echo kErreurPublication." ".$myDoc->t_doc_publication[$idc]['PUB_ID_JOB'];
									}
									?><input type="hidden" name='action[<?= $idc ?>]' value="PUB" />
									<input name="bPub" type="submit" value="<?= kPublier ?>" onClick="document.formPublication.target.value='<?= $idc ?>'" />
									<input type="checkbox" name='is_star[<?= $idc ?>]' value="1"><?= kMettreEnAvant ?></input><?
								}
							?></td>
						</tr>
					<? } ?>
				</table>
			<form>
		<?
	}

	function getEtapes($cible) {
		global $db;
		$sql = "SELECT ID_ETAPE, ETAPE_NOM FROM t_etape e INNER JOIN t_module m ON m.ID_MODULE = e.ETAPE_ID_MODULE WHERE MODULE_TYPE like 'DIFF' AND MODULE_NOM like '$cible';";
		$res = $db->CacheExecute(360,$sql);
		if($res->RecordCount()>1){
			echo "<select name='id_etape[$cible]'>";
				listOptions($sql,Array("ID_ETAPE","ETAPE_NOM"),array(),1);
			echo "</select>";
		}else if($res->RecordCount() == 1){
			$etape = $res->FetchRow();
			echo '<input type="hidden" name="id_etape['.$cible.']" value="'.$etape['ID_ETAPE'].'" />'.$etape['ETAPE_NOM'];
		}
	}
	?>
</div>