<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--<xsl:include href="../fonctions.xsl"/>
	<xsl:include href="../detail.xsl"/>-->

	<xsl:output
		method="xml"
		encoding="utf-8"
		omit-xml-declaration="yes"
		indent="yes"
	/>

	<!-- recuperation de parametres PHP -->
	<xsl:param name="gestPers"/>
	<xsl:param name="profil" />
	<xsl:param name="id_doc" />
	<xsl:param name="id_mat" />

	<xsl:template match='subtitles'>
        
		<!--        <xsl:variable name="active_id_mat">
			<xsl:value-of select="active_id_mat" />
		</xsl:variable>-->
		
		
		<script>
			
			$j(document).ready(function () {


			/* 						La cliente veut que la modification soit prise en compte même SANS avoir besoin de cliquer sur le bouton modifier
			Attention, on est obligé de mettre ce code js ici car cela ne marche pas dans la page mère docSousTitres.inc.php dans le document.ready (car l'element n'est pas encore dispo car c'est du ajax) */
			$j("tr[id^='subTitleLine']").focusout(function () {
			//alert('OUT of ' + $j(this).attr('id'));
			$j(this).find(".modifLineSubtitle").click();
			});
			
			$j("#currentSubtitle .detectClick").click(function () {
			alertDisabled($j(this));
			});
			
			
				
			
			});
			
		</script>
        
		<table>
			<tr id="subTitleLineSTART">
				<th>
					<xsl:processing-instruction name="php">echo kDebut</xsl:processing-instruction>
				</th>
                
				<th>
					<xsl:processing-instruction name="php">echo kFin</xsl:processing-instruction>
				</th>
				<th>
					<xsl:processing-instruction name="php">echo kSousTitre</xsl:processing-instruction>
				</th>
				<th>&#160;</th>
				<th> 
					<img onclick="addHTMLSubtitle(this)"   src="&lt;?php echo designUrl ?&gt;/images/btnAdd.png" title="&lt;?php echo kAjouter ?&gt;" alt="&lt;?php echo kAjouter ?&gt;"/>
				</th>
				<th>&#160;</th>
			</tr>
            
			<xsl:for-each select="subtitle">
				<tr id="subTitleLine{tcin}" onclick="goToTimeCodeInPlayer('{tcin}',this)" >
					<td>
						<img  src="&lt;?php echo designUrl ?&gt;/images/b_definir_debut_petit.gif" title="&lt;?php echo kUtiliserPositionCommeDebut ?&gt;" alt="&lt;?php echo kUtiliserPositionCommeDebut ?&gt;" onClick="piocheTC(this,'in',true)"/>
					</td>
					<td class="detectClick"><!-- detectClick car un élement en disabled ne provoque pas l'event onclick -->
						<input type="text" name="tcin" value="{tcin}" size="12" maxlength="12"/>
					</td>
					<td>
						<img id="piocheTCout" src="&lt;?php echo designUrl ?&gt;/images/b_definir_fin_petit.gif" title="&lt;?php echo kUtiliserPositionCommeFin ?&gt;" alt="&lt;?php echo kUtiliserPositionCommeFin ?&gt;" onClick="piocheTC(this,'out',true)"/>
					</td>
					<td class="detectClick">
						<input type="text" name="tcout" value="{tcout}" size="12" maxlength="12" />
					</td>
					<td class="detectClick">
						<!--<input type="text" name="text" value="{text}"/>-->
						<textarea name="text" rows="2" cols="70">
							<xsl:value-of select="text" />
						</textarea>
					</td>
                    
                
					<td>
						<img onclick="actionSubtitle({$id_mat},{$id_doc},'modif','{tcin}',this)"   src="&lt;?php echo designUrl ?&gt;/images/eye.png" title="&lt;?php echo kModifier ?&gt;" alt="&lt;?php echo kModifier ?&gt;" class="modifLineSubtitle" />
                    
					</td>
                
                
					<td>
						<img onclick="addHTMLSubtitle(this,'{tcin}')"   src="&lt;?php echo designUrl ?&gt;/images/btnAdd.png" title="&lt;?php echo kAjouter ?&gt;" alt="&lt;?php echo kAjouter ?&gt;" class="addLineSubtitle"/>
                    
					</td>
                
                
                
					<td>
						<img onclick="actionSubtitle({$id_mat},{$id_doc},'delete','{tcin}')"  class="miniTrash deleteLineSubtitle" src="&lt;?php echo designUrl ?&gt;/images/button_drop.gif" title="&lt;?php echo kSupprimer ?&gt;" alt="&lt;?php echo kSupprimer ?&gt;" />
                    
					</td>
				</tr>
                
			</xsl:for-each>
                
		</table>
	</xsl:template>
</xsl:stylesheet>
