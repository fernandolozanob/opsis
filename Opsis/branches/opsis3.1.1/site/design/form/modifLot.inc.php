<?
if(!empty($_POST)){
	echo '<script>formSend=true;</script>';
}else{
	echo '<script>formSend=false;</script>';
	
}


if (!empty($myMod->error_msg)) {
	echo "<div class='error' style='font-size:large'>".$myMod->error_msg."</div>";
} elseif (!empty($_GET['ajax']) &&  $_GET['ajax']==1 && !empty($myMod->error_msg)){
	echo "<div class='error' style='font-size:large'>".count($myMod->resultSet).' '.$myMod->entite_label.kSuccessModifLot."</div>";
	echo '<script>_hasChanged=false;</script>';
}
else{
?>
<div id='chooser' class="iframechoose" name='chooser' style='position:absolute;display:none;'> </div>
<div class="frame_menu_actions title_bar"><?=kModificationLot?> (<?=count($myMod->resultSet)?> <?=defined($myMod->entite_label)?constant($myMod->entite_label):$myMod->entite_label?>)<div id="close_export_menu" title="Fermer" onclick="hideMenuActionsModifLot()" style="float:right;"></div></div>
<!-- MS pour l'instant un certains nombres de fonctions de modifLot se trouvent dans cherche.js => peut être necessité de séparer-->
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/cherche.js"></script>
<script type="text/javascript" src="<?=libUrl?>webComponents/jquery/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?=libUrl?>webComponents/jquery/js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/fonctionsJavascript.js"></script>
<link type="text/css" href="<?=designUrl?>css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<link rel="stylesheet" href="<?=designUrl?>css/n_general.css" type="text/css" />
<link rel="stylesheet" href="<?=designUrl?>css/modifLot.css" type="text/css" />
<script type="text/javascript">window.name='main'; var $j=jQuery.noConflict();</script>
<script type="text/javascript">
window.onbeforeunload=confirmExit;
function confirmExit() {if (_hasChanged) return '';}

_hasChanged=false; //par défaut
	
function hideMenuActionsModifLot(){
	$j("#popupModal").css('display','none');
	// MS - on ne recharge la page que si une modification a été appliquée
	// corriger le comportement de _hasChanged si le problème de reload non necessaires persiste
	if(_hasChanged)
		window.location.reload();
}

function applySaiseLot(formLot){
	$j('#waiter').show();
	// console.log('applySaiseLot');
	//si retour ajax on ne veut pas d'affichage à part les erreurs ou le nombre de résultats 
	url_modif_lot =$j(formLot).attr('action')+'&ajax=1';
	formData = $j(formLot).serialize();
	// console.log(url_modif_lot);
	// console.log(formData);
	$j.ajax({
		url : url_modif_lot,
		type: 'POST', 
		data : formData, 
		success : function(data){
			$j('#waiter').hide();
			$j('.msg').html(data);
			$j('.alert').show();
			$j('#saisie_lot').animate({scrollTop:0}, 'fast');
		},
		error :  function(xhr, status) {  
          $j('#waiter').hide();
          alert('Unknown error ajax ' + status); 
        }    
	});
}
/*
* Mise à jour d un champ valeur suivant sa nature (champ entite doc,mat,docmat ou champ val)
*/
function refreshInput(elmt,select,btnIndex) {
	
	myInput=document.getElementById(elmt);	//champ input text
	mySelect=select.options[select.selectedIndex].value; //champ sélectionné
	myIndex=document.getElementById(btnIndex);
	for (i=0;i<arrFields.items.length;i++) {

        if (arrFields.items[i].CHFIELDS)
			fldName = arrFields.items[i].CHFIELDS;
        else
			fldName = arrFields.items[i].NAME;
        if (mySelect==fldName ) {
			try{
				$j("#all_params").val(JSON.stringify(arrFields.items[i]));
			}catch(e){
				console.log("failed to stringify ",e);
			}
			
            // INDEX et INDEX_CHAMP par défaut
            _fld=mySelect.split("_");
            if (_fld[0]=='L' || _fld[0]=='P' || _fld[0]=='V') {
                if(!arrFields.items[i].INDEX)
                    arrFields.items[i].INDEX = "Index";
                if(!arrFields.items[i].INDEX_CHAMP)
                    arrFields.items[i].INDEX_CHAMP = mySelect;
            }
            
			if (arrFields.items[i].INDEX && myInput.disabled==false) { //on a un bouton Index à afficher
				myIndex.disabled=false;
				myIndex.style.display='inline';
				//génération du javascript sur le bouton Index
				if (myInput.getAttribute('callback')) {
					_add="&rtn="+myInput.getAttribute('callback');
				} else {
					_add='';
				}
                                //update VG 28/05/2010 : Ajout de la possibilit� de red�finir une fonction de callback particuli�re pour le champ
				if(arrFields.items[i].CALLBACK) {
					_add='&rtn='+arrFields.items[i].CALLBACK;
				}
				
				//B.RAVI 2016-10-06 besoin sur scnf pour pouvoir filtrer dans la modif par lot, des valeurs spécifique de la table t_type_doc
				//on ne peut pas utiliser &valeur (car la valeur du champ 'valeur' peut etre réécraser lors d'1 recherche dans la palette)
				if (!(typeof arrFields.items[i].INDEX_FILTER === 'undefined' || arrFields.items[i].INDEX_FILTER===null)) {
					_add='&index_filter='+arrFields.items[i].INDEX_FILTER;
	
				}
				if (!(typeof arrFields.items[i].INDEX_JS_FUNC === 'undefined' || arrFields.items[i].INDEX_JS_FUNC===null)) {
					js_func = arrFields.items[i].INDEX_JS_FUNC
				}else{
					js_func= "choose";
				}
				
				str="myIndex.onclick=function () {" + js_func + "(document.getElementById('"+elmt+"'),'titre_index="+Quote(arrFields.items[i].LABEL)+"&id_lang=<?=$_SESSION['langue']?>&valeur=&affich_nb=1&champ="+arrFields.items[i].INDEX_CHAMP+'&type_lex='+arrFields.items[i].ID_TYPE_LEX+_add+"');}";
				eval(str);

			} else { //on désactive le bouton
				myIndex.disabled=true;
				myIndex.style.display='none';
			}
			break ; 
		}
	}

	if ($j('#form1').find('input[name="commande"]').val() =='EDIT' && elmt=='value') { //mode edit, on récupère la valeur en AJAX
		$j('#form1').find('input[name="commande"]').val('... wait ...');
		// console.log('4');
		_hasChanged=false;
		<? switch (strtolower($myMod->entite)) {
			case 'doc':
			case 'panier_doc':
				$urlact='updateDocument';
			break;
			case 'mat':
			case 'panier_mat'://update VG 15/04/2010
				$urlact='updateMateriel';
			break;
			case 'docmat': $urlact='updateDocMat';break;
		}
		?>
		//update VG 15/06/2010 : modification de la variable search
		return !sendData('GET','blank.php','xmlhttp=1&urlaction=<?=$urlact?>&search=<?=ModifLot::rechercheName?>&rang='+$j('#form1').find('input[name="rang"]').val()+'&commande=get<?=ucfirst($myMod->entite)?>','updateValue');
	}
}

/*
* Fonction de retour Ajax dans le cas de l édition particulière
*/
function updateValue(str) {

	myDom=importXML(str);
	myField=$j('#field').val().toUpperCase();
	_fld=myField.split("_");
	if (_fld[0]=='V') { //cas d une valeur : on va concaténer toutes les valeurs du type demandé
		type_val=_fld[1];
		valuestr='';
		myBalise=myDom.getElementsByTagName('TYPE_VAL');
		for (i=0;i<myBalise.length;i++) {
			if (myBalise[i].getAttribute('ID_TYPE_VAL')==type_val) {
				tmp=myBalise[i].getElementsByTagName('VALEUR');
				//update VG 02/06/2010 : il peut y avoir plusieurs valeurs de même type : on les prends toutes
				for (j=0;j<tmp.length;j++) {
					valuestr=valuestr+'"'+tmp[j].firstChild.nodeValue+'",';
				}
			}
		}
		 $j('#form1').find('input[name="value"]').val(valuestr);

	}else if (_fld[0]=='L') { //cas d un terme lexique : on va concaténer toutes les valeurs du type demandé
			type_desc=_fld[1];
			valuestr='';
			myBalise=myDom.getElementsByTagName('TYPE_DESC');
			for (i=0;i<myBalise.length;i++) {
				if (myBalise[i].getAttribute('ID_TYPE_DESC')==type_desc) {
					tmp=myBalise[i].getElementsByTagName('LEX_TERME');
					//update VG 02/06/2010 : il peut y avoir plusieurs valeurs de même type : on les prends toutes
					for (j=0;j<tmp.length;j++) {
						valuestr=valuestr+'"'+tmp[j].firstChild.nodeValue+'",';
					}
				}
			}
			$j('#form1').find('input[name="value"]').val(valuestr);
			
	}else if (_fld[0]=='P') { //cas d un terme lexique : on va concaténer toutes les valeurs du type demandé
			type_desc=_fld[1];
			valuestr='';
			myBalise=myDom.getElementsByTagName('TYPE_DESC');
			for (i=0;i<myBalise.length;i++) {
				if (myBalise[i].getAttribute('ID_TYPE_DESC')==type_desc) {
					tmp=myBalise[i].getElementsByTagName('PERS_NOM');

					for (j=0;j<tmp.length;j++) {
						valuestr=valuestr+'"'+tmp[j].firstChild.nodeValue+'",';
					}
				}
			}
			$j('#form1').find('input[name="value"]').val(valuestr);

	//update VG 31/05/2010
	}else if (_fld[0]=='F') {
		//On calcule le nom du champ à partir du tableau, excepté la première case
		var aFld = _fld;
		aFld.shift();
		var fldName = aFld.join('_');
		myBalise=myDom.getElementsByTagName(fldName);
		if (myBalise[0] && myBalise[0].firstChild) $j('#form1').find('input[name="value"]').val(myBalise[0].firstChild.nodeValue); //on met la valeur du champ sÃ©lectionnÃ©
		else $j('#form1').find('input[name="value"]').val('');

	} else if (_fld[0]=='CT') { //cas d un champ dans l entite (doc, mat, etc)	: on met la valeur en direct
		var aFld = _fld;
		aFld.shift();
		var fldName = aFld.join('_');
		myBalise=myDom.getElementsByTagName(fldName);
		if (myBalise[0] && myBalise[0].firstChild) $j('#form1').find('input[name="value"]').val(myBalise[0].firstChild.nodeValue); //on met la valeur du champ sélectionné
		else $j('#form1').find('input[name="value"]').val('');
		
	} else { //cas d un champ dans l entite (doc, mat, etc)	: on met la valeur en direct
		myBalise=myDom.getElementsByTagName(myField);
		if (myBalise[0] && myBalise[0].firstChild) $j('#form1').find('input[name="value"]').val(myBalise[0].firstChild.nodeValue); //on met la valeur du champ sélectionné
		else $j('#form1').find('input[name="value"]').val('');
	}

	//Affichage des informations de l entité, optionnel
	infos='';
	myBalise=myDom.getElementsByTagName('ID_DOC');
	if (myBalise[0] && myBalise[0].firstChild) infos=infos+myBalise[0].firstChild.nodeValue;

	myBalise=myDom.getElementsByTagName('DOC_COTE');
	if (myBalise[0] && myBalise[0].firstChild && myBalise[0].firstChild.nodeValue != '') infos=infos+', '+myBalise[0].firstChild.nodeValue;
		
	myBalise=myDom.getElementsByTagName('ID_MAT');
	if (myBalise[0] && myBalise[0].firstChild) infos=infos+myBalise[0].firstChild.nodeValue;

	myBalise=myDom.getElementsByTagName('DOC_TITRE');
	if (myBalise[0] && myBalise[0].firstChild) infos=infos+' : '+myBalise[0].firstChild.nodeValue;


	infos=infos+'  (<?=($rang+1)."/".count($myMod->resultSet)?>)'; //on edite le numéro combien sur le total ?
	document.getElementById('info_entite').innerHTML=infos;
}


<?php
// if (isset($_POST['commande']) && !empty($_POST['commande']) && (!isset($_REQUEST['noRefresh']) || $_REQUEST['noRefresh'] != "1"))
// {
// 	echo 'window.onload=function ()';
// 	echo '{';
// 	if (isset($_POST['id_import']) && !empty($_POST['id_import']))
// 		echo 'window.opener.location.href="index.php?urlaction=importView&id_import='.$_POST['id_import'].'";';
// 	else
// 		echo 'window.opener.location.href="index.php?urlaction=docListe";';
// 	echo '}';
// }
?>
</script>


<div id='waiter'><br/><br/>
<?=kWaiterMessage?>
</div>

<? 
//fabrique l objet JS >> a quoi sert il ?
$myMod->renderFields(); 
?>
<!-- update VG 15/06/2010: ajout de la variable entite dans l'url -->
<form name="form1" id="form1"   method="POST" action="modifLot.php?modif_lot=1&action=modifLot&entite=<?= $myMod->entite?><?=(!empty($_GET['tIds'])?'&tIds='.$_GET['tIds']:'')?>">

<? 
//affiche les bouton sous forme de div
	echo "<div id='action_bar'>";
	$myMod->renderActions();
	echo "</div>";
	echo "<div id='action_comment' style='display:none;'>&nbsp;</div>";

	if (empty($myMod->resultSet)) {
		echo "<div class='error' style='font-size:large'>".kMsgModifLotNoPriv."</div>";
		
		exit();
	}
	
	?>

<div class="alert" class="ui-state-error" style="display:none">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
  <div class="msg"></div>
</div>

<?if (!empty($myMod->error_msg)) echo '<div class="errormsg">'.$myMod->error_msg.'</div>';?>
<input type="hidden" name="commande" />
<input type="hidden" name="entite" value="<?=$myMod->entite?>" />
<input type="hidden" name="rang" value="<?=$rang?>" />
<input type="hidden" name="step" value="0" />
<? if (isset($_GET['noRefresh'])) { ?>
	<input type="hidden" name="noRefresh" value="1" />
<? } ?>
<? if (isset($_GET['id_import'])) { ?>
	<input type="hidden" name="id_import" value="<?=$_GET['id_import'];?>" />
<? } ?>

<fieldset id='input_container'>
<label for='field' id='lblfield'></label>
<select id="field" name="field" onchange="refreshInput('value',this,'index');refreshInput('value2',this,'index2');" >
	<option>---</option>
	<?
	//Tri des champs
	$myMod->arrFields=sortArrayByField ( $myMod->arrFields, "LABEL");
	foreach ($myMod->arrFields as $fld) {
        if(!empty($fld['CHFIELDS'])) $fldName = $fld['CHFIELDS'];
        else $fldName = $fld['NAME'];
		if(isset($fld['PROFIL_MIN'])){
            $profil_min = defined($fld['PROFIL_MIN'])?constant($fld['PROFIL_MIN']):$fld['PROFIL_MIN'];
			if($myUsr->getTypeLog() < $profil_min ) {
				continue;
			}
		}
		if(isset($fld['PROFIL_MAX'])){
            $profil_max = defined($fld['PROFIL_MAX'])?constant($fld['PROFIL_MAX']):$fld['PROFIL_MAX'];
			if($myUsr->getTypeLog() > $profil_max ) {
				continue;
			}
		}
		echo "<option value=\"".$fldName."\" ".($fldName==$_POST['field']?"selected=\"true\"":"")." >".(defined($fld['LABEL'])?constant($fld['LABEL']):$fld['LABEL'])."</option>";
	}
	?>
</select>
<br/>
<label for='field2' id='lblfield2'></label>
<select id="field2" name="field2" onchange="refreshInput('value2',this,'index2')" >
	<option>---</option>
	<?
	foreach ($myMod->arrFields as $fld) {
        if(!empty($fld['CHFIELDS'])) $fldName = $fld['CHFIELDS'];
        else $fldName = $fld['NAME'];
		if(isset($fld['PROFIL_MIN'])){
            $profil_min = defined($fld['PROFIL_MIN'])?constant($fld['PROFIL_MIN']):$fld['PROFIL_MIN'];
			if($myUsr->getTypeLog() < $profil_min ) {
				continue;
			}
		}
		if(isset($fld['PROFIL_MAX'])){
            $profil_max = defined($fld['PROFIL_MAX'])?constant($fld['PROFIL_MAX']):$fld['PROFIL_MAX'];
			if($myUsr->getTypeLog() > $profil_max ) {
				continue;
			}
		}
		echo "<option value=\"".$fldName."\" ".($fldName==$_POST['field2']?"selected=\"true\"":"")." >".(defined($fld['LABEL'])?constant($fld['LABEL']):$fld['LABEL'])."</option>";
	}
	?>
</select>
<div id='info_entite'>&nbsp;</div>
<?  if ($rang>0) {?>
<input id="btnPrev" class='button' name="btnPrev" alt="<?=kPrecedent?>" title="<?=kPrecedent?>" type="button" value="&lt;" onclick="jQuery('#form1').find('input[name=&quot;rang&quot;]').val(-1);jQuery('#form1').submit();" />&nbsp;
<? } ?>
<label id='lblvalue' for='value'></label>
<input id="value" name="value" onchange="_hasChanged=true" size="70"/>
<!-- btn index pour les valeurs -->
<input id="index" class='button' name="btnIndex" type="button" value="Index" alt="<?=kIndex?>" title="<?=kIndex?>" disabled="true"/>

<? if ($rang+1<count($myMod->resultSet)) {?>
&nbsp;<input id="btnNext" class='button' alt="<?=kSuivant?>" title="<?=kSuivant?>" name="btnNext" type="button" value="&gt;" onclick="jQuery('#form1').find('input[name=&quot;rang&quot;]').val(1);jQuery('#form1').submit();" />
<?}?>
<br/>
<label for='value' id='lblvalue2'></label>
<input id="value2" name="value2" onchange="_hasChanged=true" size="70"/>
<input id="all_params" type="hidden" name="all_params"/>
<input  class='button' id="index2" name="btnIndex2" alt="<?=knIdex?>" title="<?=kIndex?>" type="button" value="Index" disabled="true"/>
<br/>

<button  type="button" onclick="applySaiseLot('#form1');" name="btnAppliquer" alt="<?=kAppliquer?>" title="<?=kAppliquer?>" value="ok" ><?=kAppliquer?></button>
<!-- <button name="btnAnnuler" id='bAnnuler' alt="<?=kAnnuler?>" title="<?=kAnnuler?>" onclick="cancel();return false;"><img alt="" src='<?=imgUrl?>famfamfam_mini_icons/action_stop.gif'/><?=kAnnuler?></button> -->

</fieldset>
</form>
<style>
html,body { height:95%; }
body center { height :100%; }
</style>
<?
//-------------------------------------//
	//		  AFFICHAGE DE LA LISTE        //
	//-------------------------------------//
	if(gDisplayListeModifLot==1){
		//Fichiers de traitement et d'affichage
		$xslAffListe = "lot.xsl";//XXXXXXXXXX------REMPLACER------XXXXXXXXXX//
		$xmlDataListe=getSiteFile("listeDir","xml/lot_".strtolower($entiteForm).".xml");

		//Mise de Paramètres en session
		//$_SESSION['recherche_'.strtoupper($entite)]['docListeXSL'] = $xslAffListe;
		//Sauvegarde du nombre de lignes de recherche � afficher :

		$myPage->nbLignes = (defined(gNbLignesSaisieLotDefaut))?gNbLignesSaisieLotDefaut:"100";
		$myPage->page = 1;
		//Prépare les données
		$myPage->initPagerFromArray($result); // init splitter object
		trace(print_r($result,1));
		$param["nbLigne"] = $myPage->nbLignes;
		$param["page"] = $myPage->page;
		$param["nbRows"]=$myPage->found_rows;
		$param['xmlfile'] = $xmlDataListe;
		$param["urlparams"]=$myPage->addUrlParams();
		$param["offset"] = "0";

		$myPage->addParamsToXSL($param);
		$myPage->afficherListe(strtolower(!empty($entiteForm)?$entiteForm:$entite),getSiteFile("listeDir",$xslAffListe),false,null,'',true);

	}
// XB a la plce de iframe saisieLot.inc
include(libDir."class_formSaisie.php");

if (empty($myMod->resultSet)) {
	echo "<div class='error' style='font-size:large'>".$myMod->error_msg."</div>";
} else {
?>
<!--  <iframe id="frameModif" name="frameModif" style="display:none;"></iframe>-->
<script language="JavaScript" type="text/javascript">
$j('#btnSAISIE').click();
		//by LD : pour ajouter une valeur dynamiquement � un tableau avec DOM.
	function addValue(id_champ,valeur,idx,role) {
		formChanged=true;
		if(id_champ.indexOf('$')== -1){
			fld=document.getElementById(id_champ);
			if (fld.value!='') fld.value+=", ";
			fld.value+='"'+valeur+'"';
		} else {
			arrField=id_champ.split('$');
			type=arrField[0];
			cntFields=0;
			if (document.getElementById(type+'$limit')) limit=document.getElementById(type+'$limit').value; else limit=250;
			allowdouble=document.getElementById(type+'$allowdouble');

			// ajouter test sur valeur existante
			prt=document.getElementById(type+'$tab');
			for(i=0;i<prt.childNodes.length;i++) {
				chld=prt.childNodes[i];

				if (chld.id && chld.id.indexOf('$DIV$')!=-1) cntFields++;
				if (cntFields==limit) {
					alert (limit+'<?=kDOMlimitvalues?>');
					return false;
					}

				if (chld.id==type+'$DIV$'+idx && !allowdouble) {
					alert('<?=kDOMnodouble?>'); return false;

				}
			}
			btnAdd=document.getElementById(type+'$add');

			newFld=document.createElement('DIV');
			newFld.id=type+'$DIV$'+idx;
			newFld.name=type+'$DIV$'+idx;
			newFld.className=prt.className;
			addRow=new Array(newFld.id,valeur,idx,role)
			str=eval('add_'+type+'(addRow)');
			newFld.innerHTML=str[1];
			if (btnAdd) {prt.insertBefore(newFld,btnAdd);} else {prt.appendChild(newFld);}
		}
	}
	
	function chkFormLotFields (myform) {
		msg='';
		var i;
		arrElts=document.form2.getElementsByTagName('*');
		for (i=0;i<arrElts.length;i++) {
			if (!arrElts[i].getAttribute('mandatory')) continue; //test si obligatoire

			if (arrElts[i].value.Trim()=='') {
				switch(arrElts[i].getAttribute('label')){
					default :
						lib=arrElts[i].getAttribute('label');
						break;
				}
				msg=msg+' '+lib+'\n';
				arrElts[i].className='errorjs';
			} else {
				arrElts[i].className='';
			}
		}
		if (msg!='') {alert(str_lang.kJSErrorOblig+msg);return false; }
		return true;
	}
</script>

<style>
.alert {
    padding: 10px;
    background-color: rgba(255, 204, 204, 0.98);
    border: black solid 1px;
}

.closebtn {
    margin-left: 15px;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
}
.msg{
	margin-left: 33%;
}
</style>

<div id='saisie_lot'>




<?php
$xmlAff=getSiteFile("formDir",'xml/saisieLot_'.strtolower($entiteForm).'.xml');
$xmlform=file_get_contents($xmlAff);

$myForm=new FormSaisie;
$myForm->saisieObj=$myObj;
$myForm->entity=strtoupper(strtolower(!empty($entiteForm)?$entiteForm:$entite));
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->classHelp="help_champs_form";
$myForm->display($xmlform);
?>
</div>
<!-- On remet pour l'instant le champ 'commande' à "INIT" -->
<script language="JavaScript" type="text/javascript">
document.form2.commande.value = 'INIT';
</script>
<?php
}
?>

<?php


} // else
?>
