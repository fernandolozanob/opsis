<?php
	global $db;
	if(isset($xmlFile)) $type="batch_fcp";
	else $type="batch_mat";
	
	if (!isset($xmldata))
		$xmldata='';
?>
<div style='font-size:15px;font-weight:bold;font-family:"Trebuchet MS";margin:10px 0 5px 45px'>
<?= ($_GET['from']=='maintenanceMat'?kGenererMateriel:kImportFinalCutPro) ?>
</div>
<form name="F1Upload"  action="indexPopupUpload.php?urlaction=finUpload&type=<?= $type ?>" method="post" >
<input type="hidden" name="xmldata" value="<?= str_replace(chr(34),'&quot;',substr($xmldata,38)) ?>" />


		<script>
			function toggleFields(myCheck,myTab) {
				state=!myCheck.checked;
				//myTab=document.getElementById(myTable);
				myTab.style.opacity=(state==true?0.5:1);
				myTab.style.filter='Alpha:opacity='+(state==true?50:100);

				inputs=myTab?myTab.getElementsByTagName('input'):null;
				for (i=0;i<inputs.length;i++) inputs[i].disabled=state;
				inputs=myTab?myTab.getElementsByTagName('select'):null;
				for (i=0;i<inputs.length;i++) inputs[i].disabled=state;
			}

			function setParams(str) {

				xml=importXML(str);
				for (i=0;i<document.F1Upload.elements.length;i++) {

					myField=document.F1Upload.elements[i];

					if (myField.name && myField.name.substr(0,6)=='trans_' && (myField.nodeName=='INPUT' || myField.nodeName=='SELECT')) { //champ transcodage ?
						xmlFieldName=myField.name.substr(6);

						_fld=xml.getElementsByTagName(xmlFieldName);

						if (_fld[0].firstChild) myField.value=_fld[0].firstChild.nodeValue; //ne PAS oublier le firstChild
					}
				}
			}
		
		need2confirm=false;

		</script>
		<style>.miniField {font-size:10px;margin:0px;padding:0px;background-color:#f5f5ff;} </style>
		<fieldset id='form1'><legend><?=kUploadParametres?></legend>
			<input type='hidden' name='importFolder' id='importFolder' value='<?= FCPWatchFolder ?>'  />
			<input type='hidden' value='1' name='getInfos' id='getInfos' />
			<input type='hidden' value='mappingDoc' name='xsl' id='xsl' />
			
			<input name="lancement" type="hidden" value="proc" id="lancement"/>
			<?php echo kProcessus; ?>
			<select name="id_proc" id="id_proc">
            <option>---</option>
			<?php
			echo listOptions("SELECT ID_PROC,PROC_NOM FROM t_proc",array("ID_PROC","PROC_NOM"),"",0);
			?>
			</select>
			<br/>
			<?php echo kPriorite; ?>
			<select name="priority" id="priority">
                <option value="1"><?=kHaute?></option>
                <option value="2"><?=kNormale?></option>
                <option value="3" selected="selected"><?=kBasse?></option>
			</select>
			<br/>
            <input name="MAT_TYPE" type="hidden" value="MASTER"/>
			<input type='checkbox' value='1' name='makedoc' id='makedoc' checked='true' /><label for='makedoc'><?=kGenererDoc?></label>
			<input type='hidden' value='V' name='DOC_ID_MEDIA' id='DOC_ID_MEDIA'/>
			<br/><?=kFonds?>&nbsp;<select id='DOC_ID_FONDS'  name='DOC_ID_FONDS' class='miniField' ><?=
			listOptions("select ID_FONDS,FONDS from t_fonds WHERE ID_LANG=".$db->Quote($_SESSION['langue']),array("ID_FONDS","FONDS"),"",0) ?>
			</select >
			&nbsp;&nbsp;<?=kType?>&nbsp;<select id='DOC_ID_TYPE_DOC'  name='DOC_ID_TYPE_DOC' class='miniField' ><?=
			listOptions("select ID_TYPE_DOC,TYPE_DOC from t_type_doc WHERE ID_LANG=".$db->Quote($_SESSION['langue']),array("ID_TYPE_DOC","TYPE_DOC"),"",0) ?>
			</select><br />
			<?php echo kAcces; ?><input type="checkbox" id="DOC_ACCES" checked="1" value="1" label="Accès" name="DOC_ACCES" />
			<?php echo kGenererAutresVersions; ?><input type='checkbox' name='doc_trad' value='1' checked="checked" />
		</fieldset>







<fieldset id="form2" >
<center><br/>
<?
if(isset($xmlfile)){
	// $xmlfile est le nom du fichier XML à importer
	// $mats est un tableau contenant les fichiers à importer
	print kFichierAImporter." ".basename($xmlfile)."<br/>";
	print count($mats)." ".kFichiers;
} else if(isset($mediaFile)){
	print count($mediaFile)." ".kFichiers."<br/>";
	foreach($mediaFile as $idx => $f){
		print "<input type=\"hidden\" name=\"file_".$idx."_path\" id=\"file_".$idx."_path\" value=\"".$f."\"  />
		<input type=\"text\" name=\"file_".$idx."\" id=\"file_".$idx."\" size=\"40\" value=\"".basename($f)."\" class=\"miniField\" />
		<input type=\"checkbox\" checked id=\"check_".$idx."\" onclick=\"javascript:document.getElementById('file_".$idx."').disabled=!this.checked\" />
		<br/>";
	}
}
            echo "<br/><script>
            function selectAll () {
                if(document.getElementById('file_0').disabled==true){
                    for (i=0;i<=".$idx.";i++) {
                        document.getElementById('file_'+i).disabled=false;
                        document.getElementById('check_'+i).checked=true;
                    }
                }else{
                    for (i=0;i<=".$idx.";i++) {
                        document.getElementById('file_'+i).disabled=true;
                        document.getElementById('check_'+i).checked=false;
                    }
                }
            }
            </script><a href='javascript:selectAll()'>".kSelectionnerTout."</a>";

?>
<br/><br/>
<input type="submit" value="<?= kLancer?>" style='cursor:pointer;' class="myForm">
<br>
</center>
</fieldset>
</form>
