	<div id="pageTitre" class="msg"> <?= kResetPasswordFormTitle ?></div>
	<div id="password_form" >
		<fieldset>
<?php if(!isset($_POST['resetPassword']) && isset($myUsager->t_usager['US_LOGIN']) && isset($myUsager->t_usager['US_PASSWORD']) && $resetPassword_time<=24){?>
		<p class="msg_box"><?=kErreurMdpNonConformeCNIL?></p>
		<?php
		include_once(libDir."class_form.php");
		$xmlform=file_get_contents(getSiteFile("designDir","form/xml/resetPassword.xml"));
		$myForm=new Form;
		$myForm->classLabel="label_champs_form";
		$myForm->classValue="val_champs_form";
		$myForm->display($xmlform);
		?>
<?}else{?>
	
	<? if($password_has_been_changed){?>
		<p class="success"><?=kResetPasswordSuccessMsg?></p>
	<?}elseif (isset($myUsager->error_msg) && !empty($myUsager->error_msg)){?>
		<p class="error"><?=$myUsager->error_msg?></p>
	<?}else{?>
		<p class="error"><?=kResetPasswordWrongToken?></p>
	<?}?>
	<div class="buttons">
		<button id="back" class="std_button_style" type="button" name="back" onclick="returnFromAjaxPopin();"><?=kRetour?></button>
	</div>
<?}?>

<script type="text/javascript">
function checkPwConfirm(id_field1,id_field2){
	// console.log($j("#"+id_field1).val()+" "+$j("#"+id_field2).val());
	if($j("#"+id_field1).val() == $j("#"+id_field2).val() ){
		$j("#"+id_field2).addClass("valid").removeClass('invalid');
		return true ; 
	}else{
		$j("#"+id_field2).addClass("invalid").removeClass('valid');
		return false ; 
	}
}

function checkPwFields(){
	valid_form = true ; 
	msg = "" ; 
	$j("#new_password,#new_password2").removeClass('errorjs');

	if(!checkPwConfirm("new_password","new_password2")){
		valid_form = false ; 
		msg +="<?=kErreurMotDePasseIdentique?>\n";
		$j("#new_password2").addClass('errorjs');
	}
	if(!check_passwd_normes_cnil($j("#new_password").val())){
		valid_form = false ; 
		if(msg != ''){
			msg+="<br />";
		}
		msg +="<?=kErreurMdpNonConformeCNIL?>\n";
		$j("#new_password").addClass('errorjs');
	}
	
	$j("#password_form p.msg_box").html(msg).addClass('error');
	
	return valid_form ; 
}
	
</script>
	</fieldset>
</div>
