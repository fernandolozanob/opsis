<?php 
// Surbrillance du menu
$HL['tapeSetSaisie']='#tabs-1';
$HL['tapeSetSaisiesetTapes']='#tabs-2';
?>
<script>

function chkFormFields (myform) {
  msg='';
  if (msg!='') {alert(msg);return false; } else return true;
}

window.onload=function dumb() {	makeDraggable(document.getElementById('chooser'));}

</script>

<div id='pageTitre'><?=kJeu?></div>

<div id="tabs">
	<ul>
		<li id="dtabs-1"><a href='#tabs-1' onclick="javascript:saveIfAndGo('<?=$myPage->getName()?>?urlaction=tapeSetSaisie&id_tape_set=<?= $myTapeSet->t_tape_set['ID_TAPE_SET']?>')"><?= kIdentification ?></a>
		<? if(!empty($myTapeSet->t_tape_set['ID_TAPE_SET']) ){ ?>
			<li id="dtabs-2"><a href='#tabs-2' onclick="javascript:saveIfAndGo('<?=$myPage->getName()?>?urlaction=tapeSetSaisie&form=setTapes&id_tape_set=<?= $myTapeSet->t_tape_set['ID_TAPE_SET'] ?>')"><?= kCartouches ?></a>
		<? } ?>
	</ul>
	
	<div id="tabs-1" style="display:none"></div>
	<div id="tabs-2" style="display:none"></div>
</div>

<div class='error'><?=$myTapeSet->error_msg?></div>

<table id="tabSetTapes" class="tableResults" width="100%" cellspacing="0" border="0" align="center">
	<tr>
		<td class="resultsHead"><?= kIdentifiant;?></td>
		<td class="resultsHead"><?= kJeu;?></td>
		<td class="resultsHead"><?= kStatut;?></td>
		<td class="resultsHead"><?= kCapaciteUtilisee;?></td>
		<td class="resultsHead"><?= kCapaciteRestante;?></td>
		<td class="resultsHead"><?= kNbFichiers;?></td>
	</tr>
	<? foreach ($myTapeSet->t_tape as $k=>$tape) { ?>
		<tr class="altern<?=($k%2);?>">
			<td class="resultsCorps">
				<a href="index.php?urlaction=tapeSaisie&id_tape=<?=$tape['ID_TAPE'];?>">
					<?=$tape['ID_TAPE'];?>
				</a>
			</td>
			<td class="resultsCorps"><?=$tape['ID_TAPE_SET'];?></td>
			<td class="resultsCorps"><?=$tape['TAPE_STATUS'];?></td>
			<td class="resultsCorps"><?=(floor(intVal($tape['TAPE_UTILISE']) / 1024)).' K';?></td>
			<td class="resultsCorps"><?=(floor((intVal($tape['TAPE_CAPACITE']) - intVal($tape['TAPE_UTILISE'])) / 1024)).' K';?></td>
			<td class="resultsCorps"><?=$tape['NB'];?></td>
		</tr>
	<? } ?>
</table>

<script>
var formChanged=false;
var $tabs = $j('#tabs').tabs();
$j('#tabs li').removeClass('ui-tabs-selected ui-state-active');
var id = '<?=$HL[$_GET['urlaction'].$_GET['form'].$_GET['display']]?>';
id = id.replace("#", "#d");
$j(id).addClass('ui-tabs-selected ui-state-active');
</script>

<?
$myPage->renderReferrer();
?>