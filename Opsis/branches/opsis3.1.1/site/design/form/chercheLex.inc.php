<div class="title_bar">
	<div id="backButton"><a class="icoBackSearch" href="index.php?urlaction=admin">
		<?=kRetourAdmin;?>
	</a></div>
	<?= kLexique ?>
	<div class="toolbar">
		<div  onclick="triggerPrint('lex')" class=" print tool">
			<span class="tool_hover_text"><?echo kImprimer;?></span>
		</div>
		<div  class="menu_actions tool"><span class="tool_hover_text"><?echo kMenuActions;?></span>
			<div class="drop_down" style="display:none;">
				<ul>
					<li onclick="displayMenuActions('export','lex');"><? print kExporter?></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/cherche.js"></script>
<form class="contentBody" name="form1" id="chercheForm" method="POST" action="<?=$myPage->getName()?>?urlaction=lexListe" >
	<fieldset class="ui-corner-all ui-widget">
		<table align="middle" cellpadding="0" cellspacing="2" border='0' width="95%">
		<tr>


			<input type="hidden" name="orderbydeft[COL][]" value="LEX_TERME" />
			<input type="hidden" name="orderbydeft[DIRECTION][]" value="ASC" />

			<input type="hidden" name="F_lex_form" value="1"/>
			<td width="60" class="label_champs_form" align="right"><?=kTexte?></td>
			<td width="300" class="val_champs_form">
				<input type="hidden" name="chFields[1]" value="LEX_TERME" />
				<input type="hidden" name="chTypes[1]" value="FT" />
				<input type="hidden" name="chOps[1]" value="AND" />
				<input type="hidden" name="chLibs[1]" value="<?=kTexte?>" />
				<input type="text" class="val_champs_form" size="35" name="chValues[1]" value="<?=str_replace('"','&quot;',$mySearch->getValueForField(array('FIELD'=>'LEX_TERME','TYPE'=>'FT')));?>" />
			</td>

			<td width="60" class="label_champs_form" align="right"><?=kStatut?></td>
			<td width="200"  class="val_champs_form">
				<input type="hidden" name="chFields[2]" value="ETAT_LEX" />
				<input type="hidden" name="chTypes[2]" value="CI" />
				<input type="hidden" name="chOps[2]" value="AND" />
				<input type="hidden" name="chLibs[2]" value="<?=kStatut?>" />
				<select  class="val_champs_form" id="chValues[2]" name="chValues[2]">
					<option value=''><?=kTous?></option>
			        <? listOptions("select ID_ETAT_LEX, ETAT_LEX FROM t_etat_lex WHERE ID_LANG=".$db->Quote($_SESSION['langue']),Array("ID_ETAT_LEX","ETAT_LEX"),$mySearch->getValueForField(array('FIELD'=>'ETAT_LEX','TYPE'=>'CI'))); ?>
				 </select>
			</td>

		</tr>
		<tr>
		<td width="60" class="label_champs_form" align="right"><?=kAffichage?></td>


		<td class='val_champs_form' ><!-- @update VG 12/04/2010 : changement de la condition pour l'affichage de la propriété 'checked'-->
		<?=kAffichAlphabetiq?>
			<input type="radio" onclick='toggleMode(this)' name="affichage" id='affLISTE' value="LISTE" <?=(empty($mySearch->affichage) || $mySearch->affichage=='LISTE'?"checked='true'":"")?>/>

		<?=kAffichHierarchie?>
			<input type="radio" onclick='toggleMode(this)' name="affichage" id='affHIERARCHIQUE' value="HIERARCHIQUE" <?=($mySearch->affichage=='HIERARCHIQUE'?"checked='true'":"")?>/>

		</td>

		<td class='label_champs_form' align="right">&nbsp;</td>
		<td class='val_champs_form' >
				<input type="hidden" name="chFields[4]" value="count(NULLIF(DL.ID_DOC,0))" />
				<input type="hidden" name="chTypes[4]" value="C" />
				<input type="hidden" name="chOps[4]" value="HAVING" />
				<input type="hidden" name="chLibs[4]" value="<?=kLexiqueTermeSansLien?>" />
				<!-- attention à la syntaxe car ici le zéro est significatif -->
				<input type="checkbox" name="chValues[4]" value="0" <?=($mySearch->getValueForField(array('FIELD'=>'count(NULLIF(DL.ID_DOC,0))','TYPE'=>'C'))==''?'':'checked=true')?>" />
				<input type="hidden" name="chValues2[4]" value="NO_PREFIX" />
				<?=kLexiqueTermeSansLien?> un <?=kDocument?>

		<br/>
				<input type="hidden" name="chFields[5]" value="count(NULLIF(DL.ID_PERS,0))" />
				<input type="hidden" name="chTypes[5]" value="C" />
				<input type="hidden" name="chOps[5]" value="HAVING" />
				<input type="hidden" name="chLibs[5]" value="<?=kLexiqueTermeSansLien?>" />
				<!-- attention à la syntaxe car ici le zéro est significatif -->
				<input type="checkbox" name="chValues[5]" value="0" <?=($mySearch->getValueForField(array('FIELD'=>'count(NULLIF(DL.ID_PERS,0))','TYPE'=>'C'))==''?'':'checked=true')?>" />
				<input type="hidden" name="chValues2[5]" value="NO_PREFIX" />
				<?=kLexiqueTermeSansLien?> une <?=kPersonne?>

		</td>
		</tr>

		<tr id='modes_affichage'>
			<td width="60" class="label_champs_form" align="right"><?=kType?></td>
			<td colspan="3" class="val_champs_form">
				<input type="hidden" name="chFields[3]" value="TYPE_LEX" />
				<input type="hidden" name="chTypes[3]" value="CI" />
				<input type="hidden" name="chOps[3]" value="AND" />
				<input type="hidden" name="chLibs[3]" value="<?=kType?>" />
				<input type="hidden" name="chValues2[3]" value="<?=$_SESSION['langue']?>" />

                <?
                    $result=$db->Execute("SELECT ID_TYPE_LEX,TYPE_LEX FROM t_type_lex WHERE PERSONNE='0' and ID_LANG='".$_SESSION['langue']."' order by 1");

                    while($list=$result->FetchRow()){
                        $id_type_lex=$list["ID_TYPE_LEX"];
                        $selectedItems=(array)$mySearch->getValueForField(array('FIELD'=>'TYPE_LEX','TYPE'=>'CI'));
                        print "<input class='val_champs_form' type=\"checkbox\" name=\"chValues[3][".$id_type_lex."]\" value='".$id_type_lex."'  id=\"".$id_type_lex."\"";
                        if(in_array($id_type_lex,$selectedItems) ) {
                            print " checked=\"checked\" ";
                        } elseif (!empty($id_pers)) {print " disabled='yes'"; }

                        print "/>".$list['TYPE_LEX']."\n<br/>";
                    }
                    $result->Close();
                ?>

			</td>
			<td>
                <?
                    $result=$db->Execute("SELECT ID_TYPE_LEX,TYPE_LEX FROM t_type_lex WHERE PERSONNE='0' and HIERARCHIQUE=1 AND ID_LANG='".$_SESSION['langue']."' order by 1");



                    print "<select class='val_champs_form' name=\"chValues[3]\" >";
                    while($list=$result->FetchRow()){
						$id_type_lex=$list["ID_TYPE_LEX"];
						$selectedItems=(array)$mySearch->getValueForField(array('FIELD'=>'TYPE_LEX','TYPE'=>'CI'));
                        print "<option value='".$id_type_lex."'";
                        if(in_array($id_type_lex,$selectedItems) ) {
                            print " selected=\"true\" ";
                        }
                        print ">".$list['TYPE_LEX']."</option>";

                    }
					print "</select>";
                    $result->Close();
                ?>
			</td>


			</tr>
		</table>
<div>
<input type="hidden" name="id_asso" value="<?=isset($_POST['id_asso'])?intval($_POST['id_asso']):''?>" /> <!-- repond thesaurus hiérarchique -->
<table border='0' cellspacing='0' cellpadding='0' class='chercheBoutons'>
<tr>
<td class='val_champs_form' valign='top' width="180">
<? $new_lex_type = $mySearch->getValueForField(array('FIELD'=>'TYPE_LEX','TYPE'=>'CI'));
	if(is_array($new_lex_type)){ $new_lex_type = reset($new_lex_type);}  ?>
<button id="bNouveau" class="ui-state-default ui-corner-all" type="button"  onclick="document.location.href='index.php?urlaction=lexSaisie&amp;lex_id_type_lex=<?=$new_lex_type?>'">
<span class="ui-icon ui-icon-plus"></span><?= kNouveauLexique ?>
</button>
</td>
<td class='val_champs_form' valign='top' width="180">
<button id="bInit" class="ui-state-default ui-corner-all" type="button"  onclick="document.location.href='index.php?urlaction=lexListe&init=1'">
<span class="ui-icon ui-icon-arrowrefresh-1-w"></span><?= kReinitialiser ?>
</button>
</td>
<td class='val_champs_form' valign='top' width="180">
<button id="bChercher" class="ui-state-default ui-corner-all" type="submit"  onclick="">
<span class="ui-icon ui-icon-search"></span><?=kChercher?>
</button>
</td>
</tr></table>
</div>
	</fieldset>
</form>

		<script type="text/javascript"> //Init Mode Affichage
				//update VG 12/04/2010 : changement de la condition
			toggleMode(document.getElementById('aff<?=(empty($mySearch->affichage)?'LISTE':$mySearch->affichage)?>'));
		</script>
