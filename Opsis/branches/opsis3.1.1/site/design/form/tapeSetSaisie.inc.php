<?php 
// Surbrillance du menu
$HL['tapeSetSaisie']='#tabs-1';
$HL['tapeSetSaisiesetTapes']='#tabs-2';
?>
<script>

function chkFormFields (myform) {
  msg='';
  if (msg!='') {alert(msg);return false; } else return true;
}

window.onload=function dumb() {	makeDraggable(document.getElementById('chooser'));}

</script>

<div id='pageTitre' class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<?=kJeu?>
</div>

<div id="tabs">
	<ul>
		<li id="dtabs-1"><a href='#tabs-1' onclick="javascript:saveIfAndGo('<?=$myPage->getName()?>?urlaction=tapeSetSaisie&id_tape_set=<?= $myTapeSet->t_tape_set['ID_TAPE_SET']?>')"><?= kIdentification ?></a>
		<? if(!empty($myTapeSet->t_tape_set['ID_TAPE_SET']) ){ ?>
			<li id="dtabs-2"><a href='#tabs-2' onclick="javascript:saveIfAndGo('<?=$myPage->getName()?>?urlaction=tapeSetSaisie&form=setTapes&id_tape_set=<?= $myTapeSet->t_tape_set['ID_TAPE_SET'] ?>')"><?= kCartouches ?></a>
		<? } ?>
	</ul>
	
	<div id="tabs-1" style="display:none"></div>
	<div id="tabs-2" style="display:none"></div>
</div>

<div class='error'><?=$myTapeSet->error_msg?></div>
	<?
	include(libDir."class_formSaisie.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/tapeSetSaisie.xml"));
	
	if (isset($myTapeSet->t_tape_set['ID_TAPE_SET']) && is_numeric($myTapeSet->t_tape_set['ID_TAPE_SET']))
		$xmlform = str_replace("[CURRENT_ID]", $myTapeSet->t_tape_set['ID_TAPE_SET'], $xmlform);
	else
		$xmlform = str_replace("WHERE ID_TAPE_SET &lt;&gt; [CURRENT_ID]", " ", $xmlform);
	
	$myForm=new FormSaisie;
	$myForm->saisieObj=$myTapeSet;
	$myForm->entity="TS";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
	
	if (!is_numeric($myTapeSet->t_tape_set['TS_NB_TAPES']))
		$myTapeSet->t_tape_set['TS_NB_TAPES'] = 0;
	?>
	<script>
	//	if(document.getElementById("user_crea")) document.getElementById("user_crea").value='<?= $myTapeSet->user_crea ?>';
	//	if(document.getElementById("user_modif")) document.getElementById("user_modif").value='<?= $myTapeSet->user_modif ?>';
	//	if(document.getElementById("date_crea")) document.getElementById("date_crea").value=convDate('<?= $myTapeSet->t_mat['MAT_DATE_CREA'] ?>','jj/mm/aaaa');
	//	if(document.getElementById("date_mod")) document.getElementById("date_mod").value=convDate('<?= $myTapeSet->t_mat['MAT_DATE_MOD'] ?>','jj/mm/aaaa');
	
	var formChanged=false;
	var $tabs = $j('#tabs').tabs();
	$j('#tabs li').removeClass('ui-tabs-selected ui-state-active');
	var id = '<?=$HL[$_GET['urlaction'].$_GET['form'].$_GET['display']]?>';
	id = id.replace("#", "#d");
	$j(id).addClass('ui-tabs-selected ui-state-active');
	
	var nb_tapes = 0<?=$myTapeSet->t_tape_set['TS_NB_TAPES'];?>;
	function removeItem(url) {
		if (nb_tapes == 0){
		    if (confirm("Confirmez-vous la suppression ?")) {
		        document.form1.commande.value="SUP";
		        document.form1.page.value=url;
		        document.form1.submit();
	        }
		}
		else
			alert("Impossible de supprimer car des cartouches sont incluses dans ce jeu.");
	}
	</script>
	<?
	$myPage->renderReferrer();
	?>
