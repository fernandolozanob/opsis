<?

if(isset($_GET['urlaction']) && $_GET['urlaction'] == 'importView'){
	$context_url = "index.php?urlaction=importView&import_type_form=folder";
	$context_action = "index.php?urlaction=finUpload&import_type_form=folder&type=batch_mat";
	$context = "inpage";
}else{
	$context = "popup";
	$context_url = "indexPopupUpload.php?urlaction=importFolder";
	$context_action = "indexPopupUpload.php?urlaction=finUpload&type=batch_mat";
}
?>

<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/upload.js"></script>
<script type='text/javascript'>
function selectAll()
{
    var all_cb_checked=true
    $j('input[type="checkbox"][name^="file_"]').each
    (
		function (idx,elt)
		{
		if (!$j(elt).is(':checked'))
		all_cb_checked=false;
		}
    );
    if (all_cb_checked){
        $j('input[type="checkbox"][name^="file_"]').removeProp('checked');
    }else{
        $j('input[type="checkbox"][name^="file_"]').prop('checked',"true");
	}
}


$j(document).ready(function(){
	createCheckboxActivation("input#addThemeCheckbox","select#selectTheme");
	$j("select,input[type='checkbox']").trigger("change");
});
</script>
<?if($context=='popup'){?>
<h1><?php echo kImporterRepertoire;?></h1>
<?}?>
<form method="post" id="form_import_dossier" action="indexPopupUpload.php?urlaction=finUpload&type=batch_mat">
	
	<?php
	if($context=="inpage"){?>
		<input type="hidden" name="redirectImportView" value="1"/>
	<?php }
	
	require_once(libDir.'class_formSaisie.php');

	$myForm=new FormSaisie();
	$myForm->entity="JOB";
	// $form->classLabel="label_champs_form";
	// $form->classValue="val_champs_form";
	  $myForm->classLabel="miniField label_miniField";
      $myForm->classValue="miniField val_miniField";
	// echo $myForm->display(file_get_contents(getSiteFile('designDir','form/xml/importFolder.xml')));
	 




	 $myForm->beginTag("FIELDSET",array("ID"=>"doc_form"));
		$myForm->displayElt(array("NAME"=>"MAT_TYPE","TYPE"=>"hidden","VALUE"=>"MASTER","DIVTAGS"=>"none"));
		$myForm->displayElt(array("NAME"=>"redirectImportView","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
		$myForm->displayElt(array("NAME"=>"checkbox_rename","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
	        $myForm->displayElt(array("NAME"=>"type_import","TYPE"=>"hidden","ID"=>"type_import","VALUE"=>"DIR"));
			
		$myForm->displayElt(array("NAME"=>"xsl","TYPE"=>"hidden","VALUE"=>"mappingDocPhoto","DIVTAGS"=>"both"));
		$myForm->displayElt(array("NAME"=>"lancement","TYPE"=>"hidden",'VALUE'=>'proc'));
        $myForm->displayElt(array("NAME"=>"id_proc","TYPE"=>"hidden",'VALUE'=>'*'));
        $myForm->displayElt(array("NAME"=>"proc_critere","TYPE"=>"hidden","VALUE"=>"{\"9\":\"&lt;MAT_FORMAT&gt;DCP-CPL&lt;/MAT_FORMAT&gt;\",\"1\":\"&lt;MAT_ID_MEDIA&gt;V&lt;/MAT_ID_MEDIA&gt;\",\"5\":\"&lt;MAT_ID_MEDIA&gt;A&lt;/MAT_ID_MEDIA&gt;\",\"3\":\"&lt;MAT_FORMAT&gt;PDF&lt;/MAT_FORMAT&gt;\",\"4\":\"&lt;MAT_ID_MEDIA&gt;D&lt;/MAT_ID_MEDIA&gt;\",\"2\":\"&lt;MAT_ID_MEDIA&gt;P&lt;/MAT_ID_MEDIA&gt;\"}"));
 
 
			// Fonds par défaut => Opsomai (5 a priori )
            $myForm->displayElt(array("NAME"=>"DOC_ID_FONDS","TYPE"=>"hidden","VALUE"=>"5"));
            // $myForm->displayElt(array("NAME"=>"id_proc","TYPE"=>"select","SELECT_SQL"=>"SELECT ID_PROC AS ID,PROC_NOM AS VAL FROM t_proc","LABEL"=>kProcessus));
            $myForm->displayElt(array("NAME"=>"makedoc","TYPE"=>"checkbox","CLASSVALUE"=>"miniField","ONCLICK"=>"toggleFields(this,document.getElementById('doc_form'));","CHECKBOX_LIB"=>kGenererDoc_long,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
			$myForm->displayElt(array("NAME"=>"doc_trad","CLASSVALUE"=>"miniField","TYPE"=>"checkbox",'CHECKBOX_LIB'=>kGenererAutresVersions_long,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
			$myForm->displayElt(array("NAME"=>"DOC_ACCES","CLASSVALUE"=>"miniField","TYPE"=>"checkbox",'CHECKBOX_LIB'=>kAcces_long,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
			$myForm->beginTag("DIV",array("CLASS"=>"miniField"));
				$myForm->displayElt(array("TYPE"=>"checkbox","DIVTAGS"=>"none", "ID"=>"addThemeCheckbox","CHECKED"=>"false")); // MS - ici il faut une fonction JS qui active / desactive le select suivant 
				$myForm->displayElt(array("TYPE"=>"span","VALUE"=>kAjouterTheme_long));
				$myForm->displayElt(array("TYPE"=>"select","NAME"=>"t_doc_val[][ID_VAL]","ID"=>"selectTheme","SELECT_SQL"=>"SELECT ID_VAL as ID, VALEUR as VAL from t_val where ID_LANG='FR' AND val_id_type_val='THM' ORDER BY VAL","NOALL"=>1)); // "name"=>"V_THM",
				$myForm->displayElt(array("NAME"=>"t_doc_val[][ID_TYPE_VAL]","TYPE"=>"hidden","VALUE"=>"THM"));

			$myForm->endTag("DIV");
			$myForm->beginTag("DIV",array("CLASS"=>"miniField"));
				$myForm->displayElt(array("TYPE"=>"checkbox","DIVTAGS"=>"none","CHECKED"=>"false","ID"=>"addReportCheckbox","onchange"=>"toggleReportage(this.checked)")); // MS - ici il faut une fonction JS qui active / desactive le select suivant 
				$myForm->displayElt(array("TYPE"=>"span","VALUE"=>kAjouterReportage_long));
				// $myForm->displayElt(array("TYPE"=>"select","SELECT_SQL"=>"SELECT ID_VAL as ID, VALEUR as VAL from t_val where ID_LANG='FR' AND val_id_type_val='THM'"));
				//Reportage
				if (isset($_GET['id_doc2link']) && intval($_GET['id_doc2link']) !== false)
					$myForm->displayElt(array("NAME"=>"id_doc2link","TYPE"=>"hidden","VALUE"=>intval($_GET['id_doc2link']),"DIVTAGS"=>"none"));
				else {
					include(modelDir.'model_docReportage.php');
					if (Reportage::isReportageEnabled()) {
						$myForm->displayElt(array("NAME"=>"id_doc2link","ID"=>"selectReport","TYPE"=>"select","SELECT_SQL"=>"(SELECT 'new' as ID, '". kNouveau ."' as VAL ORDER BY VAL ASC)
																																UNION ALL
																															(SELECT ''||ID_DOC AS ID,DOC_TITRE AS VAL 
																															FROM t_type_doc t
																															INNER JOIN t_doc d ON d.doc_id_type_doc = t.id_type_doc AND d.ID_LANG='FR'
																															WHERE t.ID_LANG='FR' AND t.ID_TYPE_DOC = ".intval(gReportagePicturesActiv)."
																															ORDER BY VAL ASC)"
																															,"DIVTAGS"=>"none",'NOALL'=>'1'
																															,"onchange" => "toggleReportName(this.value)"));
						$myForm->displayElt(array("NAME"=>"reportName",'CLASSLABEL'=>'labelNewReportName','ID'=>'newReportName',"TYPE"=>"text","PLACEHOLDER"=>kNomDuNouveauReportage,"VALUE"=>"","DIVTAGS"=>"none"));
						// MS voir si on laisse placeholder (légere incompatibilité IE), ou si on remet une vraie valeur par défaut 
					}
				}
				$myForm->displayElt(array("NAME"=>"uploadToReportage","TYPE"=>"hidden","VALUE"=>1,"DIVTAGS"=>"none"));
			$myForm->endTag("DIV");

			
			$myForm->endTag("FIELDSET");
	
	?>
	<div style="text-align : center; margin : 5px;">
		<input type="submit" class="std_button_style" value="<?php echo kEnvoyer; ?>" />
		
		<input type="hidden" id="rename" name="rename" value=""/>
		<input type="hidden" name="lancement" value="proc" />
	</div>
<table class="tableResults" width="100%">
		
		<?php
		
		
		
		echo '<tr>';
		echo '<td class="resultsHead" style="width:20px;"><input type="checkbox" name="select_all" onclick="selectAll();" /></td>';
		echo '<td class="resultsHead" style="width:20px;">'.kType.'</td>';
		echo '<td class="resultsHead" style="text-align:left;">'.kFichier.'</td>';
		echo '<td class="resultsHead" style="text-align:left;">'.kTaille.'</td>';
		echo '</tr>';
		echo '<tr class="separator">';
		echo ' <td colspan="4">&nbsp;</td>';
		echo '</tr>';
		
		if (!empty($url_path))
		{
			$rep_parent=dirname($url_path);
			
			if ($rep_parent=='.')
				$rep_parent='';
			
			echo '<tr>';
			echo '<td class="resultsCorps">&nbsp;</td>';
			echo '<td class="resultsCorps">&nbsp;</td>';
			
			if (empty($rep_parent))
				echo '<td class="resultsCorps" style="text-align:left;"><a href="'.$context_url.'&'.$url_import_folder.'">..</a></td>';
			else
				echo '<td class="resultsCorps" style="text-align:left;"><a href="'.$context_url.'&'.$url_import_folder.'currentFolder='.urlencode(dirname($url_path).'/').'">..</a></td>';
			
			echo '<td class="resultsCorps">&nbsp;</td>';
			echo '</tr>';
		}
		
		$i=0;
		
		$j=0;
		foreach ($repertoires as $rep)
		{
			$i++;
			echo '<tr>';
			echo '<td class="resultsCorps"><!-- <input type="checkbox" name="file_'.$i.'" /> --></td>';
			echo '<td class="resultsCorps"><img src="'.designUrl.'/images/folder_magnify.png" alt="repertoire" /></td>';
			echo '<td class="resultsCorps" style="text-align:left;">';
			echo '<a href="'.$context_url.'&'.$url_import_folder.'currentFolder='.urlencode($url_path.$rep.'/').'">'.$rep.'</a>';
			echo '<input type="hidden" name="file_'.$i.'_path" value="'.$repertoires_reel[$j].'" />';
			echo '<input type="hidden" name="file_'.$i.'_original" value="'.basename($repertoires_reel[$j]).'" />';
			echo '<td class="resultsCorps">&nbsp;</td>';
			echo '</td>';
			echo '</tr>';
			$j++;
		}
		
		$j=0;
		foreach ($fichiers as $fich)
		{
			$i++;
			echo '<tr>';
			echo '<td class="resultsCorps">';
			echo '<input type="checkbox" name="file_'.$i.'" />';
			echo '<input type="hidden" name="file_'.$i.'_path" value="'.$fichier_reel[$j].'" />';
			echo '<input type="hidden" name="file_'.$i.'_original" value="'.basename($fichier_reel[$j]).'" />';
			echo '</td>';
			echo '<td class="resultsCorps"><img src="'.designUrl.'/images/page_white_text.png" alt="repertoire" /></td>';
			echo '<td class="resultsCorps" style="text-align:left;">'.$fich.'</td>';
			echo '<td class="resultsCorps">'.convertFileSize(filesize($fichier_reel[$j])).'</td>';
			echo '</tr>';
			$j++;
		}
		?>
	</table>
</form>