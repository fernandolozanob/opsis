<?php
	$myPage=Page::getInstance();
	$link=$myPage->getName();
	
?>
<script type="text/javascript" language="JavaScript1.2">
var lastModif=''; //dernier champ texte modifié
var lastMode=''; // mode pour ce champ (terme, syno, asso)
var lastLangue=''; //langue pour ce champ
var doSubmit=false; //Bool pour lancer un submit en retour d'aJAX ou non

	function chkFields() {
		doSubmit=false;
		//alert (lastModif+'   '+lastMode);
		msg='';
		if (!formCheckNonEmpty(document.getElementById('LEX_TERME').value)) msg+='<?=kJSErrorValeurOblig?>';
		if (msg!='') {alert(msg);return false; }
		if (lastModif!='') {
			doSubmit=true; //Il faudra lancer un submit ensuite
			checkExists(lastMode,document.getElementById(lastModif).value,document.getElementById(lastModif).id,lastLangue);
			return false;
			} //on ne valide pas directement, on le fera coté AJAX
		//alert('submit now chkfields');
			
	}

	function launchSearch(page) {
		alert('Pas encore développé');
	}
	
	
	// Ajout d'un champ input vide pour saisir une valeur inédite
function addBlank(id_champ) {
	
	cntSyno++;
	arrField=id_champ.split('$');
	type=arrField[0];
	
	// VP 16/7/18 : suppression test ci-dessous pour permettre l'ajout de plusieurs champs vides
	//if (document.getElementById(type+'$DIV$new')) return false;
	
	// ajouter test sur valeur existante
	prt=document.getElementById(type+'$tab');
	
	btnAdd=document.getElementById(type+'$add');
	
	newFld=document.createElement('DIV');
	newFld.className='miniTab';
	newFld.id=type+'$DIV$new';
	//newFld.innerHTML="<input type='text' style='display:block;float:left;width:180px;font-size:10px;' onKeyPress=\"lastModif=this.id;lastMode="+type+";lastLangue='<?=$_SESSION['langue']?>'\" onBlur='checkExists(\""+type+"\",this.value,\""+type+"$DIV$new\",\"<?=$_SESSION['langue']?>\")' length='25' id="+type+" name='"+type+"[new]' /><img style='cursor:pointer;' id='delVal' onClick='removeValue(\""+newFld.id+"\")' src='design/images/button_drop.gif' />";
	newFld.innerHTML="<input type='text' style='width:180px;' onKeyPress=\"lastModif=this.id;lastMode="+type+";lastLangue='<?=$_SESSION['langue']?>'\" onBlur='checkExists(\""+type+"\",this.value,\""+type+"$DIV$new\",\"<?=$_SESSION['langue']?>\")' length='25' id="+type+" name='"+type+"["+cntSyno+"][LEX_TERME]' />";
	newFld.innerHTML+="<img style='cursor:pointer;' id='delVal' onClick='removeValue(\""+newFld.id+"\")' src='design/images/button_drop.gif' />";
	prt.appendChild(newFld);
	//if (btnAdd) {prt.insertBefore(newFld,btnAdd);} else {prt.appendChild(newFld);}

	}
	
	
//by LD : pour ajouter une valeur dynamiquement à un tableau avec DOM.
	function addValue(id_champ,valeur,idx) {

		//alert(id_champ+" val:"+valeur + " idx:"+idx);
		arrField=id_champ.split('$');
		type=arrField[0];
		cntFields=0;
		
		if (document.getElementById(type+'$limit')) limit=document.getElementById(type+'$limit').value; else limit=20;
		
		// ajouter test sur valeur existante
		prt=document.getElementById(type+'$tab');

		
		for(i=0;i<prt.childNodes.length;i++) {
			chld=prt.childNodes[i];
			
			if (chld.id && chld.id.indexOf('$DIV$')!=-1) cntFields++;
			if (cntFields==limit) {
				alert (limit+'<?=kDOMlimitvalues?>');
				return false;
				}
			
			if (chld.id==type+'$DIV$'+idx) {
				alert('<?=kDOMnodouble?>');
				return false;
			}
		}
		btnAdd=document.getElementById(type+'$add');
		
		newFld=document.createElement('DIV');
		newFld.id=type+'$DIV$'+idx;
		newFld.name=type+'$DIV$'+idx;
		newFld.className='miniTab';
		addRow=new Array(newFld.id,valeur,idx)
		str=eval('add_'+type+'(addRow)');
		newFld.innerHTML=str[1];
		prt.appendChild(newFld);
	}
	

	window.onload=function () {

		makeDraggable(document.getElementById('chooser'));



		document.getElementById('LEX_TERME').onblur=function() {
			<?	if (!empty($myLex->arrVersions))
				foreach ($myLex->arrVersions as $ver)
				echo "if (document.getElementById('version_".$ver['ID_LANG']."').value=='') document.getElementById('version_".$ver['ID_LANG']."').value=document.getElementById('LEX_TERME').value\n";
			?>
			checkExists('terme',document.getElementById('LEX_TERME').value,document.getElementById('LEX_TERME').id,'<?=$_SESSION['langue']?>');
			}
		//document.getElementById('LEX_TERME').onfocus=function () {document.form1.save.disabled=true;}
		document.getElementById('LEX_TERME').onkeypress=function () {lastModif='LEX_TERME';lastMode='terme';lastLangue='<?=$_SESSION['langue']?>'}
	}

	// Appel AJAX
	function checkExists (mode,val,from,id_lang) {
		return !sendData('GET','blank.php','xmlhttp=1&urlaction=lexExists&mode='+mode+'&value='+encodeURIComponent(val)+'&lex_id_type_lex='+document.getElementById('LEX_ID_TYPE_LEX').value+"&from="+from+"&id_lex=<?=$myLex->t_lexique['ID_LEX']?>&id_lang="+id_lang);
	}
	
	function doFusion(id) { //lancement de la fusion => preparation du formulaire puis submit
		document.getElementById('fusionLex').value=id;
		document.getElementById('commande').value='FUSION';
		document.getElementById('input_form_lex').submit();
	}
	
	
	// Traitement retour AJAX : Fonction unique pour tous les retours AJAX de page
	function dspResult(str) {
	
		//document.form1.save.disabled=false;
		

		//alert(str);
		if (str=='') { // chaine vide
			if (doSubmit==true) document.getElementById('input_form_lex').submit();
			return false;
		}
		else myDom=importXML(str); //transo chaine en XML
		pasdeDoublon=myDom.getElementsByTagName('ok');
		if (pasdeDoublon[0]) {	//pas de contenu, donc pas de doublon => OK
			if (doSubmit==true) document.getElementById('input_form_lex').submit(); //s'il faut faire un submit, on y va
			return false; // si on ne soumet pas, alors on continue normalement
		}
		
		//A partir d'ici, cela signifie que l'on a trouvé un doublon

		doSubmit=false; // désactivation du submit
		myBalise=myDom.getElementsByTagName('ID_LEX');
		
		
		var id=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
		
		myBalise=myDom.getElementsByTagName('FROM');
		var from=myBalise[0].firstChild.nodeValue; 
		
		myBalise=myDom.getElementsByTagName('MODE');
		var mode=myBalise[0].firstChild.nodeValue; 

		
		myBalise=myDom.getElementsByTagName('LEX_TERME');
		var lexTerme=myBalise[0].firstChild.nodeValue; 

		myBalise=myDom.getElementsByTagName('LEX_NOTE');
		if (myBalise[0].firstChild) 
		var lexNote="\n"+myBalise[0].firstChild.nodeValue; 		
		else 
		var lexNote='';
		
		//Mode syno/asso => si on doublon existe, alors ALERT puis on retire la ligne fraichement saisie
		if (mode=='arrSyno' || mode=='arrAsso') {alert('<?=kErrorLexiqueExisteDeja?>');removeValue(from);}
		
		//Mode terme (=> LEX_TERME + versions) => on lance le dialogue de fusion
		if (mode=='terme') {
			yes=confirm ('<?=kErrorLexiqueExisteDeja?> : \n'+lexTerme+lexNote+'\n\n <?=kConfirmLexiqueFusion?>');
			if (yes) doFusion(id);
		}
	}

	// Retrait d une valeur
	function removeValue(id_div) {
		fld=document.getElementById(id_div);
		fld.parentNode.removeChild(fld);
	}
	
	// Vidage des listes de terme (si on change le type)
	function emptyLists() {
		
		allDivs=document.getElementsByTagName('DIV');
		for (i=0;i<allDivs.length;i++) {
			if (allDivs[i].className=='miniTab') {
				allDivs[i].parentNode.removeChild(allDivs[i]);
				i--; //le fait de retirer un element modifie la liste des DIV donc on rembobine d un cran 
				}
		}
		//document.getElementById('chooser').style.display='none';
	}


</script>
<script type="text/javascript" language="JavaScript1.2">
function confirmDetach() {
	if (!confirm("Confirmer-vous le détachement ?")) return;
	document.getElementById('input_form_lex').commande.value='DETACH_DOC';
	document.getElementById('input_form_lex').submit();
}
</script>

<div class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<div id="pageTitre"><?= kLexique ?>&nbsp;</div>
	<div class="toolbar">
		<div  onclick="triggerPrint('lex')" class="print tool"><span class="tool_hover_text"><?=kImprimer?></span>&#160;</div>
		<? if(isset($myLex) && isset($myLex->t_lexique['ID_LEX'])){?>
			<div class="menu_actions tool" ><span class="tool_hover_text"><?echo kMenuActions;?></span>
				<div class="drop_down" style="display:none;">
					<ul>
						<li onclick="displayMenuActions('export','lex','<?=$myLex->t_lexique['ID_LEX'];?>');"><? echo kExporter;?></li>
					</ul>
				</div>&#160;
			</div>
		<?}?>
	</div>
</div>
<div class="contentBody">
<div class="scrollableDiv">
	<div id='chooser' class="iframechoose" name='chooser' style='position:absolute;display:none;'></div>

	<div id='fusionbox' class="iframechoose" name='fusionbox' style='position:absolute;display:none;'></div>
	<? 
	
	$myLex->getPossibleSolrFields();

	if(isset($_GET['type_lex']) && $myLex->t_lexique['LEX_ID_TYPE_LEX']=='') $myLex->t_lexique['LEX_ID_TYPE_LEX']=htmlentities($_GET['type_lex']);
	if(isset($_GET['lex_id_type_lex']) && $myLex->t_lexique['LEX_ID_TYPE_LEX']=='') $myLex->t_lexique['LEX_ID_TYPE_LEX']=htmlentities($_GET['lex_id_type_lex']);
	if($myLex->t_lexique['LEX_ID_ETAT_LEX']=='') $myLex->t_lexique['LEX_ID_ETAT_LEX']='1';
	if(isset($_REQUEST['currentId']) && $_REQUEST['currentId']!='') $myLex->t_lexique['LEX_ID_GEN']=$_REQUEST['currentId'];
	?>

	<form name="form1" id="input_form_lex" class="input_form"  method="POST">

		<input type="hidden" name="id_lex" value="<?=$myLex->t_lexique['ID_LEX']?>"/>
		<input type="hidden" id="commande" name="commande" value="" />
		<input type="hidden" id="page" name="page" value="" />
		<input type="hidden" id="fusionLex" name="fusionLex" value="" />
		<input name="LEX_ID_SYN" id="LEX_ID_SYN" type="hidden" value="<?=$myLex->t_lexique['LEX_ID_SYN']?>" />
		<input name="fldToPassToRedirection" value="valeur" type="hidden" />
		<button onClick="return false;" style='display:none'> </button>
				
		<fieldset name="ident" id="ident" class="ui-widget">
		<!--legend ><?=kLexique?></legend--> 
		<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td class="label_champs_form" >
				<?=kIdentifiant?>
			</td>
			<td class="val_champs_form" >
				<?=$myLex->t_lexique['ID_LEX']?>
			</td>
		</tr>
		
		<tr>
			<td class="label_champs_form" >
				<?=kType?>
			</td>		
			<td class="val_champs_form" colspan="3">
			<? if (count($myLex->arrFather)+count($myLex->arrChildren)+count($myLex->arrDoc)+count($myLex->arrAsso)+count($myLex->arrSyno)==0) {	?>
				<select class="val_champs_form" id="LEX_ID_TYPE_LEX" name="LEX_ID_TYPE_LEX" onChange="emptyLists()" > 
				<?listOptions("SELECT * FROM t_type_lex WHERE PERSONNE='0' and ID_LANG='".$_SESSION['langue']."' ORDER BY TYPE_LEX",Array("ID_TYPE_LEX","TYPE_LEX"),$myLex->t_lexique['LEX_ID_TYPE_LEX']); ?>
				</select>
			<? } else { ?>
			<input type="hidden" id="LEX_ID_TYPE_LEX" name="LEX_ID_TYPE_LEX" value="<?=$myLex->t_lexique['LEX_ID_TYPE_LEX']?>" /><strong><?=$myLex->type_lex?></strong><br/>
			<small><?=kWarningTypeLexique?></small>
			<? } ?>
			</td>		
		</tr>	
		
		<tr>
			<td class="label_champs_form" valign="top">
				<?=kLexique?>
			</td>		
			<td class="val_champs_form" colspan="3" valign="top">
				<?=$myLex->t_lexique['LEX_TERME']?><br/>
				<input id='LEX_TERME' style='font-size:14px;' type="text" size="40" name="LEX_TERME" value="<?=str_replace('"','\"',$myLex->t_lexique['LEX_TERME'])?>" />
	<? foreach ($myLex->arrVersions as $idx=>$version) { ?>
	<br/><?=$version['ID_LANG']?>&nbsp;<input type="text" id="version_<?=$version['ID_LANG']?>" onBlur="checkExists('terme',this.value,this.id,'<?=$version['ID_LANG']?>')" size="40" name="version[<?=$version['ID_LANG']?>][LEX_TERME]" value="<?=str_replace('"','\"',$version['LEX_TERME'])?>" 
	onKeyPress="lastModif='version_<?=$version['ID_LANG']?>';lastMode='terme';lastLangue='<?=$version['ID_LANG']?>'" />
	<? } ?>
	</td>		
		</tr>	

		<tr>
			<td class="label_champs_form" valign="top">
				<?=kNotes?>
			</td>		
			<td class="val_champs_form" colspan="3" valign="top">
				<textarea cols='50' rows='2' id='LEX_NOTE' type="text" name="LEX_NOTE" ><?=trim($myLex->t_lexique['LEX_NOTE'])?></textarea>
	<?
	foreach ($myLex->arrVersions as $idx=>$version) { ?>
		<br/><?=$version['ID_LANG']?><br/><textarea cols='50' rows='2' type="text" name="version[<?=$version['ID_LANG']?>][LEX_NOTE]" ><?=trim($version['LEX_NOTE'])?></textarea>
	<?}?>
			</td>		
		</tr>
		


		<tr>
			<td class="label_champs_form" >
				<?=kEtat ?>
			</td>		
			<td class="val_champs_form" colspan="3">
				<select class="val_champs_form" id="LEX_ID_ETAT_LEX" name="LEX_ID_ETAT_LEX" > 
				<? listOptions("SELECT * FROM t_etat_lex WHERE ID_LANG='".$_SESSION['langue']."' ORDER BY ETAT_LEX",Array("ID_ETAT_LEX","ETAT_LEX"),$myLex->t_lexique['LEX_ID_ETAT_LEX']); ?>
				</select>
			</td>		
		</tr>	
		<?if (!empty($myLex->arrSynoPref)) {
			echo "<tr><td/><td colspan='3' class='val_champs_form'>";
			echo "<div class='error'>".kWarningLexiqueSynonymePreferentiel."</div>";
			echo "<br>";
			echo "<a href='".$myPage->getName()."?urlaction=lexSaisie&id_lex=".$myLex->arrSynoPref[0]['ID_LEX']."&w=600&h=550'>";
			echo $myLex->arrSynoPref[0]['LEX_TERME'];
			echo "</a>";
			echo "</td></tr>";
		
		} else {
		?>
		
		
		<tr>
			<td class="label_champs_form" >
				<?=kTermeGenerique?>
			</td>		
			<td class="val_champs_form" colspan="3">
			<input type="hidden" id="LEX_ID_GEN" name="LEX_ID_GEN" value="0" />
			<input type="hidden" id="arrFather$limit" name="arrFather$limit" value="1" />
			<div id="arrFather$tab" border='2' class='lineDesc' >
					<? 	
					
						$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"");
						$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>"arrFather[][ID_LEX]");
						$arrFields[]=array("ID"=>"","TYPE"=>"TRASH");
						$js="<script>divs='';\n";
						echo makeFormJSFunction2('arrFather',$arrFields,'miniTab');
					
						if (is_array($myLex->arrFather))
							foreach ($myLex->arrFather as $idx=>$father){ 
						
									$js.="arrRow=new Array('arrFather\$DIV$".$father['ID_LEX']."',".quoteField($father['LEX_TERME']).",'".$father['ID_LEX']."');";
									$js.="out=add_arrFather(arrRow);\n document.write(out[0]);";					
						
							}
						$js.="document.write(divs);\n</script>";echo $js;	
						unset($arrFields);
					?>
			
		   </div>
				<button  class='miniButton' id='arrFather$add' style="cursor:pointer" onClick="choose(this,'titre_index=<?=urlencode(kTermeGenerique)?>&id_lang=<?=$_SESSION['langue']?>&valeur=&champ=LFC___GENE&type_lex='+document.getElementById('input_form_lex').LEX_ID_TYPE_LEX.value+'&currentId=<?=$myLex->t_lexique['ID_LEX']?>');return false;">
				<img src='design/images/btnAdd.png' border='0' alt='<?=kAjouter?>' title='<?=kAjouter?>' />
				</button>
			</td>		
		</tr>	
		
		<tr>
			<td class="label_champs_form" >
				<?=kTermesSpecifiques?>
			</td>
			<td class="val_champs_form" colspan="3">
			<div id="arrChildren$tab" border='2' class='lineDesc'>
					<? 	
					
						$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"");
						$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>"arrChildren[][ID_LEX]");
						$arrFields[]=array("ID"=>"","TYPE"=>"TRASH");
						$js="<script>divs='';\n";
						echo makeFormJSFunction2('arrChildren',$arrFields,'miniTab');
						if (is_array($myLex->arrChildren))
							foreach ($myLex->arrChildren as $idx=>$child){ 
						
									$js.="arrRow=new Array('arrChildren".$child['ID_LEX']."',".quoteField($child['LEX_TERME']).",'".$child['ID_LEX']."');";
									$js.="out=add_arrChildren(arrRow);\n document.write(out[0]);";					
						
							}
						$js.="document.write(divs);\n</script>";echo $js;	
						unset($arrFields);
					?>
			</div>

				<button class='miniButton' id='arrChildren$add' style="cursor:pointer" onClick="choose(this,'titre_index=<?=urlencode(kTermesSpecifiques)?>&id_lang=<?=$_SESSION['langue']?>&valeur=&champ=LFC___&type_lex='+document.getElementById('input_form_lex').LEX_ID_TYPE_LEX.value+'&currentId=<?=$myLex->t_lexique['ID_LEX']?>');return false;">
				<img src='design/images/btnAdd.png' border='0' alt='<?=kAjouter?>' title='<?=kAjouter?>' />
				</button>
			</td>
		</tr>


		
		<tr>
			<td class="label_champs_form" >
				<?=kSynonyme?>
				
			</td>
			<td class="val_champs_form" colspan="3">
			<div id="arrSyno$tab" border='2' class='lineDesc'>
					<? 	
					
						$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"");
						$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>"arrSyno[][ID_LEX]");
						$arrFields[]=array("ID"=>"","TYPE"=>"TRASH");
						$js="<script>divs='';var cntSyno=0;\n";
						echo makeFormJSFunction2('arrSyno',$arrFields,'miniTab');
						if (is_array($myLex->arrSyno))
							foreach ($myLex->arrSyno as $idx=>$syno){ 
						
									$js.="arrRow=new Array('arrSyno".$syno['ID_LEX']."',".quoteField($syno['LEX_TERME']).",'".$syno['ID_LEX']."');";
									$js.="out=add_arrSyno(arrRow);\n document.write(out[0]);cntSyno++;";					
						
							}
						$js.="document.write(divs);\n</script>";echo $js;	
						unset($arrFields);
					?>
		   </div>
				<button style='display:inline;' class='miniButton' id='arrSyno$add' style="cursor:pointer" onClick="choose(this,'titre_index=<?=urlencode(kSynonyme)?>&id_lang=<?=$_SESSION['langue']?>&valeur=&champ=LFC___SYNO&type_lex='+document.getElementById('input_form_lex').LEX_ID_TYPE_LEX.value+'&currentId=<?=$myLex->t_lexique['ID_LEX']?>');return false;">
				<img src='design/images/btnAdd.png' border='0' alt='<?=kChoisir?>' title='<?=kChoisir?>' />
				</button>
				<button style='display:inline;' class='miniButton' id='arrSyno$addblank' onClick="addBlank('arrSyno$tab');return false;">
				<img src='design/images/book_add.png' border='0' alt='<?=kAjouter?>' title='<?=kAjouter?>' />			
				</button>

			</td>	
		</tr>	

		<tr>
			<td class="label_champs_form" >
				<?=kTermesAssocies?>
				
			</td>
			<td class="val_champs_form" colspan="3">
			<div id="arrAsso$tab" border='2' class='lineDesc'>
					<? 	
					
						$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"");
						$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>"arrAsso[][ID_LEX]");
						$arrFields[]=array("ID"=>"","TYPE"=>"TRASH");
						$js="<script>divs='';\n";
						echo makeFormJSFunction2('arrAsso',$arrFields,'miniTab');
						if (is_array($myLex->arrAsso))
							foreach ($myLex->arrAsso as $idx=>$asso){ 
						
									$js.="arrRow=new Array('arrAsso".$asso['ID_LEX']."',".quoteField($asso['LEX_TERME']).",'".$asso['ID_LEX']."');";
									$js.="out=add_arrAsso(arrRow);\n document.write(out[0]);";					
						
							}
						$js.="document.write(divs);\n</script>";echo $js;	
						unset($arrFields);
					?>
				
		   </div>
				<button style='display:inline;' class='miniButton' id='arrAsso$add' style="cursor:pointer" onClick="choose(this,'titre_index=<?=urlencode(kTermesAssocies)?>&id_lang=<?=$_SESSION['langue']?>&valeur=&champ=LFC___ASSO&type_lex='+document.getElementById('input_form_lex').LEX_ID_TYPE_LEX.value+'&currentId=<?=$myLex->t_lexique['ID_LEX']?>');return false;">
				<img src='design/images/btnAdd.png' border='0' alt='<?=kChoisir?>' title='<?=kChoisir?>' />
				</button>
				<button style='display:inline;' class='miniButton' id='arrSyno$addblank' onClick="addBlank('arrAsso$tab');return false;">
				<img src='design/images/book_add.png' border='0' alt='<?=kAjouter?>' title='<?=kAjouter?>' />			
				</button>
			</td>	
		</tr>	
		<? } ?>
		
		<tr>
			<td class="label_champs_form" >
				<?=kDocumentsLies?>
			</td>
			<td class="val_champs_form" colspan="3">
				<?=count($myLex->arrDoc)?>
				<? if (count($myLex->arrDoc)!=0 && $link=='index.php') {
						echo "<input type='button' value='Détacher' onClick=\"confirmDetach()\" name='DETACH_DOC'/>";
						if(defined("useSolr") && useSolr){
							echo "<input type='button' value='Voir' onClick=\"rebondSolr(".quoteField($myLex->t_lexique['LEX_TERME']).",'".implode(',',$myLex->solr_fields)."','".(isset($myLex->type_lex)?$myLex->type_lex:'')."');\" name='VIEW'/>";
						}else{
							echo "<input type='button' value='Voir' onClick=\"cherche(".$myLex->t_lexique['ID_LEX'].",'".$myLex->t_lexique['LEX_ID_TYPE_LEX']."','".(isset($myLex->t_lexique['TYPE_LEX'])?$myLex->t_lexique['TYPE_LEX']:'')."','LI');\" name='VIEW'/>";
						}
				}
 ?>
			</td>
		</tr>
	</table>

		</fieldset>
		<div  class="button_fieldset" align="center">
			<button class='actionBouton ui-state-default ui-corner-all' id='bSup' name="suppr" type="button" onclick="removeItem();"><span class="ui-icon ui-icon-trash">&nbsp;</span><?= kSupprimer ?></button>
			<button class='actionBouton ui-state-default ui-corner-all' id='bOk' name="bOk" type="submit" onclick="document.getElementById('input_form_lex').commande.value='SAVE';" ><span class="ui-icon ui-icon-check">&nbsp;</span><?= kEnregistrer ?></button>
		</div>
	</form>


<?
if( $link=='index.php' && count($myLex->arrDocLex) > 0) {
        ?>
        <fieldset style="width: 80%" class="ui-widget ui-widget-content ui-corner-all">
        <legend><?=kListeDocuments?></legend>
        <table>
        <tr><td class="resultsHead"><?=kReference?></td><td class="resultsHead"><?=kTitre?></td></tr>
        <?
        foreach ($myLex->arrDocLex as $doc) {
            echo "<tr><td><a href='index.php?urlaction=doc&id_doc=".$doc['ID_DOC']."'>".$doc['DOC_COTE']."</a></td><td><a href='index.php?urlaction=doc&id_doc=".$doc['ID_DOC']."'>".$doc['DOC_TITRE']."</a></td></tr>";
        }
        ?>
        </table>
        </fieldset>
<? } ?>
    
    </div>
</div>
