
<script>
  var _hasChanged;
  _hasChanged=false;
  var arrEtapes; //tableau des étapes
  
  
  function chkFormFields () {
    msg='';

  arrInputs=document.form1.getElementsByTagName('input');
  for(v=0;v<arrInputs.length;v++) {
    if (arrInputs[v].getAttribute('mandatory')&& arrInputs[v].value.Trim()=='') {
        msg+='<?=kJSErrorValeurOblig?>';
           arrInputs[v].focus();
           arrInputs[v].className='errorjs';
           new Effect.Pulsate(arrInputs[v]);
      
      } else  {
        arrInputs[v].className=null;
      }
    }
    
    arrInputs=document.form1.getElementsByTagName('select');
  	for(v=0;v<arrInputs.length;v++) {
    if (arrInputs[v].getAttribute('mandatory') 
    		&& arrInputs[v].id=='ID_ETAPE'
    		&& arrInputs[v].options[arrInputs[v].selectedIndex].value=='-1' ) {
            msg+='<?=kJSErrorValeurOblig?>';
           arrInputs[v].focus();
           arrInputs[v].className='errorjs';
           new Effect.Pulsate(arrInputs[v]);     	
      
      } else {
      	 arrInputs[v].className=null;
      }
    
  	}

    if (msg!='') {alert(msg);return false;}
    _hasChanged=false; //pour ne pas déclencher le message
 

    
  }

  function confirmExit() {if (_hasChanged) return '';}
  window.onbeforeunload=confirmExit;


function updateArrEtapes(champ,position) {

      etapeLabel=champ.options[champ.selectedIndex].text.replace("'","\'");
      etapeId=champ.options[champ.selectedIndex].value;
  	  eval("arrEtapes["+position+"]={'value':"+etapeId+",'label':'"+(position+1)+'. '+etapeLabel+"'}");
}

function updateEtapePrec(champ,position) {
	//clean du champ
	for (u=0;u<champ.options.length;u++) champ.options[u]=null; //Note: la méthode .remove() n existe pas sur IE

	opt=new Option("------",'');
	champ.options[0]=opt;
	
	for (u=0;u<position;u++) {
	  	if (arrEtapes[u] && arrEtapes[u].value!='-1') {
		  	opt=new Option(arrEtapes[u].label,arrEtapes[u].value);
		  	champ.options[u+1]=opt;
		  	opt=null;
	  	}
	}
}

function addEtape(etape,position) {
  var _i;
  //Partie 1 : création de l étape par clonage de la section vierge
  blankEtape=document.getElementById('etape$blank');
  myNewEtape=blankEtape.cloneNode(true);
  myNewEtape.id='etape$cloned'; //chgt id car il ne faut qu un seul section$blank
  document.getElementById('etapes').appendChild(myNewEtape);
  myNewEtape.style.display='block'; //on rend visible le clone

  if (!etape) { //création d une etape vierge ? 
  	position=document.getElementById('etapes').getElementsByTagName('LI').length-1;
  	etape= new Object();
  	etape.ID_ETAPE='-1';
  	etape.PE_ID_ETAPE_PREC='-1';
  }

  //Partie 2 : affectation des valeurs aux champs
  arrFlds= myNewEtape.getElementsByTagName('*');
  for (_i=0;_i<arrFlds.length;_i++) {
    if (arrFlds[_i].id=='PE_ORDRE') {
      arrFlds[_i].value=position+1;
    }
    if (arrFlds[_i].id=='ID_ETAPE' && etape.ID_ETAPE ) {
      idEtape=etape.ID_ETAPE;
      if (idEtape!='-1') setValue(idEtape,arrFlds[_i],false); //affectation de l étape
      //Mise à jour du tableau des étapes existantes
      if (idEtape!='-1') updateArrEtapes(arrFlds[_i],position);

    }
     if (arrFlds[_i].id=='PE_ID_ETAPE_PREC') {
      updateEtapePrec(arrFlds[_i],position);
      idEtapePrec=etape.PE_ID_ETAPE_PREC;
      if (idEtapePrec!='-1') setValue(idEtapePrec,arrFlds[_i],false); //affectation de l étape
  	}  
    
    if (arrFlds[_i].id=='btnEdit' && etape.ID_ETAPE!='') {
      arrFlds[_i].onclick=function() {location.href=eval("\'<?=$myPage->getName()?>?urlaction=etapeSaisie&id_etape="+etape.ID_ETAPE+"\'")};
    }
  
  }
  arrFlds=null;
  myNewEtape=null;
	
	
	Sortable.create('etapes',{scroll:window,onChange:function(){updateAllEtapes()} });

}

function updateAllEtapes(champ) {
	
	arrElts=document.getElementById('etapes').getElementsByTagName('LI');
	
	//Pour tous les élements
	for (v=0;v<arrElts.length;v++) {
		myEtape=arrElts[v];
		arrFlds= myEtape.getElementsByTagName('*');
		  for (_i=0;_i<arrFlds.length;_i++) { //boucle sur les champs
		  
		    if (arrFlds[_i].id=='PE_ORDRE') { //Màj position
		      arrFlds[_i].value=v+1;
		    }
		    
		    if (arrFlds[_i].id=='ID_ETAPE') { 
		    	updateArrEtapes(arrFlds[_i],v); //Màj du tableau des étapes
			}
		    
		    if (arrFlds[_i].id=='PE_ID_ETAPE_PREC') {
		      currentId=arrFlds[_i].options[arrFlds[_i].selectedIndex].value;
		      updateEtapePrec(arrFlds[_i],v);
		      setValue(currentId,arrFlds[_i],false); //affectation de l étape
		  	}
		    
		}
	}
}

function duplicate(url) {
     if (confirm("<?=kConfirmJSDocDuplication?>")) {
        document.form1.commande.value="DUP_PROC";
        if (url!='') document.form1.page.value=url;
        document.form1.submit();
     }	
}

</script>

<div id='pageTitre' class="title_bar">
<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
<?=kProcessus?></div>
<div class='error'><?=$myProc->error_msg?></div>

<form name="form1" id="form1" method="post" action="" onsubmit="return chkFormFields(this);">
<input type="hidden" name="commande" value="SAVE" />
<input type="hidden" name="page" value="" />

    <table width="400"  border="0">
        <tr>
<td class="label_champs_form"><?= kIdentifiant ?>&nbsp;:</td>
            <td class="val_champs_form">
            <?=$myProc->t_proc['ID_PROC'] ?>
            <input name="id_proc" id='id_proc' type="hidden" value="<?=$myProc->t_proc['ID_PROC'] ?>"/></td>
        </tr>
		<tr>
			<td class="label_champs_form"><?= kNom ?>&nbsp;:</td>
			<td class="val_champs_form">
					<input name="proc_nom" id='proc_nom' mandatory="true" type="text" size="60" 
            		maxlength="100" value="<?=$myProc->t_proc['PROC_NOM'] ?>"/>
			</td>
		</tr>
	</table>


<fieldset>
<legend><?=kEtape?>(s)</legend>
<br/>
<span id='etape§add'
    class='btnAjouter'
    onClick="addEtape()">
    <img src='design/images/brick_add.png' /><?=kProcessusAjouterEtape?></span>
<ul id='etapes'> </ul>
</fieldset>

<br/>


<fieldset name='buttons' id='buttons' class='button_fieldset' >
<table border='0' cellspacing='0' cellpadding='0'>
	<tr>
	<td class='val_champs_form' valign='top' width='150'>&nbsp;</td>
	<td class='val_champs_form' valign='top' width='150' >
	<button id="bSup" class="ui-state-default ui-corner-all" type="button"  onclick="removeItem('index.php?urlaction=procListe')">
	<span class="ui-icon ui-icon-trash"></span><?= kSupprimer ?>
	</button>
	</td>
	<td class='val_champs_form' valign='top' width='150'>
	<button id="bDup" class="ui-state-default ui-corner-all" type="button"  onclick="duplicate('')">
	<span class="ui-icon ui-icon-copy"></span><?= kDupliquer ?>
	</button>
	</td>
	<td class='val_champs_form' valign='top' width='150'>
	<button id="bOk" class="ui-state-default ui-corner-all" type="submit"  onclick="">
	<span class="ui-icon ui-icon-check"></span><?= kEnregistrer ?>
	</button>
	</td>
</tr>
</table>
</fieldset>
</form>


<style>
  #etape {margin-left:10px;}
  .etape {clear:both;border:1px solid #006600;display:block;margin:2px;background-color:white;font-weight:normal;}
  .etape table td {font-size:11px;}
  #PE_ORDRE {font-weight:bold;border:none;font-size:16px;background-color:transparent;}
  #trash {float:right;vertical-align:center;cursor:pointer;margin:3px 3px 0px 0px;}
  #btnEdit {margin-right:30px;}
  .btnAjouter {cursor:pointer;border:1px solid black;background-color:#ddffdd;
        font-size:11px;font-family:"Trebuchet MS",Helvetica,sans-serif;
        padding:2px;margin:5px 2px 5px 12px;font-weight:bold;}
  .btnAjouter img {vertical-align:bottom;padding-right:3px;}  
</style>

<!-- Entité etape -->
<li id='etape$blank' class='etape' style='display:none;'>
    <img src="<?=imgUrl?>brick_delete.png" id='trash'
      onclick="_hasChanged=true;this.parentNode.parentNode.removeChild(this.parentNode);updateAllEtapes();" />
  <table>
    <tr>
    <td width='30'>
    	<input name='t_proc_etape[][PE_ORDRE]' type='text' readonly='true' size='2' id='PE_ORDRE' />
    </td>
 
    <td ><label><?=kEtape?>
    <select id='ID_ETAPE' mandatory="true" mystyle='list' onChange='_hasChanged=true;updateAllEtapes(this)' name="t_proc_etape[][ID_ETAPE]" >
      <?=listOptions("select '-1' as ID_ETAPE,'------' as ETAPE_NOM from t_etape union select distinct ID_ETAPE, ETAPE_NOM from t_etape order by etape_nom",array("ID_ETAPE","ETAPE_NOM"),"ID_ETAPE")?>
    </select></label>    
    </td>
    <td><img src='<?=imgUrl?>brick_edit.png' id='btnEdit' style='cursor:pointer' title="<?=kModifier." ".kEtape?>" alt="<?=kModifier." ".kEtape?>" onclick='' />
  	</td>
    <td><label><?=kEtapePrecedente?>
    <select id='PE_ID_ETAPE_PREC' mystyle='list' name="t_proc_etape[][PE_ID_ETAPE_PREC]" >
    	<option value=''>--------</option>
    </select></label>
    </td>    
    
  </tr>
  </table>
</li>

<script>
//chargement et remplissage des entites. A laisser à la fin
arrEtapes=<?=array2json($myProc->t_proc_etape)?>;

for (cnt=0;cnt<arrEtapes.length;cnt++) {
 addEtape(arrEtapes[cnt],cnt);
}

</script>




<?
$myPage->renderReferrer();
?>
