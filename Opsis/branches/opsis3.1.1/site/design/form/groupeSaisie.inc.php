<? require_once(libDir."/class_chercheUsager.php");
?>
<script>
var currentPage = 1; // indice page pour liste pager usager 
var tri ="ID_USAGER1";
var id_groupe="<?=(isset($myGroupe->t_groupe['ID_GROUPE'])?$myGroupe->t_groupe['ID_GROUPE']:$_GET['id_groupe']);?>";
var refine;
var nbLignes=20;
var error_val_already_exists="<?=kJSErrorValeurExisteDeja?>";




function addUser(field,val,id) {
  if (document.getElementById('t_usager_groupe$'+id)) {
      alert(error_val_already_exists);
      return false;}  
	 terme = document.getElementById('refine_liste_usager').value;
	params = 'urlaction=processUsagerGroupe&xmlhttp=1&id_groupe='+id_groupe+'&page='+currentPage+'&add_item='+id+"&refine="+terme;
	params+="&tri="+tri;
	
	// MS on soumet aussi le formulaire de la palette pour refresh la liste des users hors groupe
	var ifrm = document.getElementById('frmchoose');
	var innerDoc = ifrm.contentDocument? ifrm.contentDocument: ifrm.contentWindow.document;
	innerDoc.form1.submit();
	//window.frames['frmchoose'].document.form1.submit();
  return !sendData('GET','blank.php',params,"displayUsagerList");
}


function addFonds(field,val,id) {
  if (document.getElementById('t_groupe_fonds$'+id)) {
      alert(error_val_already_exists);
      return false;}
  return !sendData('GET','blank.php','urlaction=getLines&type=fonds&xmlhttp=1&id='+id,'addFondsLine');
}

function addUserLine(str) {
  addRow(str,document.getElementById('usagerListe'));
}

function addFondsLine(str) {
  addRow(str,document.getElementById('fondsListe'));
}

function addRow(str,myTable) {
  if(str.indexOf('?>')!=-1) str = str.substring(1+str.indexOf('>')); //on vire l'entete php / xml
  x=document.createElement('tr');
  x.className='altern'+(myTable.rows.count % 2)
  myTable.appendChild(x);
  _tmpstr=str;
  _idx=0;
  _cellIdx=0;
  while (_tmpstr.indexOf('<td')!=-1) {
    _idx=_tmpstr.indexOf('<td');
    _tmpstr=_tmpstr.substr(_idx+3);
    _argus=_tmpstr.indexOf('>');
    _closetag=_tmpstr.indexOf('</td>');
    myStr=_tmpstr.substring(_argus+1,_closetag);
    _cell=x.insertCell(_cellIdx);
    _cell.innerHTML=myStr;
    _cell.className='resultsCorps';
    _cellIdx++;
  }
}


function removeLineFondsGroupe(elt) { //ote une ligne d'un tableau via DOM
  _myTR=elt.parentNode.parentNode;
  if (_myTR.nodeName!='TR') return false;
  
    if(_myTR.firstChild.firstChild.id.split('$')[0]=="t_usager_groupe"){
		terme = document.getElementById('refine_liste_usager').value;
		params='urlaction=processUsagerGroupe&xmlhttp=1&id_groupe='+id_groupe+'&page='+currentPage+'&delete_item='+_myTR.firstChild.firstChild.value+"&refine="+terme;
		params+="&tri="+tri;	
		
		return !sendData('GET','blank.php',params,"displayUsagerList");
	}
	
	_myTR.parentNode.removeChild(_myTR);
}

function refineUsagerListe(e,change){
	if(e.keyCode ==13 || change==true){
		terme = document.getElementById('refine_liste_usager').value;
		params = 'urlaction=processUsagerGroupe&xmlhttp=1&id_groupe='+id_groupe+'&page='+currentPage+'&refine='+terme;
		params+="&tri="+tri;
		
		return !sendData('GET','blank.php',params,"displayUsagerList");

	}
}

function triUsagerListe(new_tri){
		tri = new_tri;
		terme = document.getElementById('refine_liste_usager').value;
		params = 'urlaction=processUsagerGroupe&xmlhttp=1&id_groupe='+id_groupe+'&page='+currentPage+'&refine='+terme;
		params+="&tri="+tri;
		return !sendData('GET','blank.php',params,"displayUsagerList");


}


function switchPageUsagerListe(page){
	if(page<0 || Math.round(page)!=page ){
		return false; 
	}
	currentPage = parseInt(page,10);
	terme = document.getElementById('refine_liste_usager').value;
	params = 'urlaction=processUsagerGroupe&xmlhttp=1&id_groupe='+id_groupe+'&page='+currentPage+"&refine="+terme;
	params+="&tri="+tri;	
	
	return !sendData('GET','blank.php',params,'displayUsagerList');
}

function displayUsagerList(str){
	document.getElementById("liste_pager_usager").innerHTML = str;
}

function chkFormFields (myform) {
	if(document.activeElement.id == "refine" || document.activeElement.className.indexOf('preventSubmit')!="-1"){return false;}
  msg=''; 
  if (!formCheckNonEmpty(document.getElementById('groupe').value)) {
    msg+=error_val_already_exists;
    document.getElementById('groupe').parentNode.className='errormsg';}
  else {
    document.getElementById('groupe').parentNode.className='val_champs_form';}
  if (msg!='') {alert(msg);return false; } else return true;
}



window.onload=function dumb() {	
	makeDraggable(document.getElementById('chooser'));
	sendData('GET','blank.php','urlaction=processUsagerGroupe&xmlhttp=1&id_groupe=<?=(isset($myGroupe->t_groupe['ID_GROUPE'])?$myGroupe->t_groupe['ID_GROUPE']:$_GET['id_groupe'])?>&page=1&tri='+tri,'displayUsagerList');

}

</script>
<div class='error'><?=$myGroupe->error_msg?></div>
<div class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<div id="pageTitre" ><?= kGroupe ?>&nbsp;</div>
</div>

<div id='chooser' class='iframechoose' style='position:absolute;display:none;'></div>
<script>makeDraggable(document.getElementById('chooser'));</script>

<form name="form1" id="form1" method="post" action="" onsubmit="return chkFormFields(this);">
    <input name="commande" id="commande" type="hidden" value=""/>
    <input name="type" id="type" type="hidden" value=""/>
    <input name="page" id="page" type="hidden" value="" />
    <input name="ligne" id="ligne" type="hidden" value="" />
    <input name="listeUsagerAjax" id="listeUsagerAjax" type="hidden" value="1" />
	<fieldset name="ident" id="ident" class="ui-widget ui-widget-content ui-corner-all">
		<legend><?=kGroupeSaisie?></legend>
    <input name="id_groupe" type="hidden" size="50" maxlength="50" value="<?=str_replace('"','\"',$myGroupe->t_groupe['ID_GROUPE']) ?>" />
    <table width="400"  border="0" class="feature">
        <tr>
            <td><?= kGroupe ?> :  </td>
            <td><input name="groupe" id='groupe' type="text" size="20" maxlength="100" class="preventSubmit" tabindex="2" value="<?=$myGroupe->t_groupe['GROUPE'] ?>"/></td>
        </tr>
    </table>
    <h3><?= kListeUsagers ?></h3>
	<div style="margin-left	:2.6%;"><?= kRecherche?>&#160;&#160;<input name="refine" id="refine_liste_usager" type="text" size="20" maxlength="100" class="preventSubmit" onkeydown="refineUsagerListe(event);" onchange="refineListeUsager(event,true);" /></div>
    <div id="liste_pager_usager"><div align="center"><img src='design/images/wait30trans.gif'></div>&#160;</div>
	<?	
	echo "<button class='miniButton ui-state-default ui-corner-all' type='button' id='utilisateur\$add' style='margin:5px auto;' onclick=\"choose(this,'titre_index=".kUsager."&id_lang=".$_SESSION['langue']."&valeur=&champ=USR&rtn=addUser&groupFilter=".$myGroupe->t_groupe['ID_GROUPE']."');return false;\"><span class='ui-icon ui-icon-plus'></span>&nbsp;".kAjouter."</button>";
    ?>
	<br />
    <h3><?= kListeFondsPrivileges ?></h3>
	<br />
    <?

        // Affichage des fonds et des privil�ges associ�s au groupe
 //                       $params = Array("id_groupe" => $mon_objet->id_groupe,"groupe" => $mon_objet->groupe, "ordre" => $ordre, "tri"=>$tri);
			$params['entete']='1';
            $params['tri']=strtoupper(Page::getInstance()->col);
            $params['ordre']=Page::getInstance()->triInvert;
            tabToXSL($myGroupe->t_groupe_fonds,"t_fonds",getSiteFile("listeDir","fondsGroupeListe.xsl"),$params);
	echo "<button class='miniButton ui-state-default ui-corner-all' type='button' id='fonds\$add' style='margin:5px auto;' onclick=\"choose(this,'titre_index=".kFonds."&id_lang=".$_SESSION['langue']."&valeur=&champ=FON&rtn=addFonds');return false;\"><span class='ui-icon ui-icon-plus'></span>&nbsp;".kAjouter."</button>";
	?>
	</fieldset>
	<div class="button_fieldset" align="center">
		<button class='actionBouton ui-state-default ui-corner-all ' id='bSup' name="bSup" type="button"  onclick="removeItem('index.php?urlaction=groupeListe')"><span class="ui-icon ui-icon-trash"></span><?= kSupprimer ?></button>
		<button class='actionBouton ui-state-default ui-corner-all' name="bOk" type="submit" id="bOk"  tabindex="3" ><span class="ui-icon ui-icon-check"></span><?= kEnregistrer ?></button>
	</div>
</form>
