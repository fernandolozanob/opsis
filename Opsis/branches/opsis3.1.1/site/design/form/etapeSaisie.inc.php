<?
$mods=$db->CacheGetAll(3600,"SELECT * from t_module order by 1");
$codecs=$db->CacheGetAll(3600,"SELECT FORMAT_FILE_EXT,FORMAT_VCODEC,FORMAT_ACODEC from t_format_mat where format_id_media='V' and format_file_ext!=''");
?>
<script type="text/javascript" >
  var formChanged;
  formChanged=false;
var mods=<?=array2json($mods);?> //version json de la table t_module
var codecs=<?=array2json($codecs);?>  //version json de la table t_format_mat (codecs)
var myBlock; //block en cours de sélection

//N'affiche que le formulaire correspondant au type du module
function updateModule(elmt) {
	var form1 = $j("form[name='form1']").get(0);
	if (!elmt) elmt=form1.etape_id_module;
	//Récupération du type suivant le module sélectionné
	for (i=0;i<mods.length;i++) {
		if (mods[i].ID_MODULE==elmt.value) currentType=mods[i].MODULE_TYPE;
	}	
	
	fldSets=form1.getElementsByTagName('fieldset');
	
	for (i=0;i<fldSets.length;i++) {
				
		if (fldSets[i].id==currentType) { //on affiche le type en cours
			myBlock=fldSets[i];
			fldSets[i].style.display='block';
			
		} else if (fldSets[i].id=='ident' || fldSets[i].id=='buttons') { //on affiche ident & boutons
			fldSets[i].style.display='block';
		}
		else {fldSets[i].style.display='none';}	// on cache le reste
	}
		
}


// Met à jour les champs avec les paramètres présents dans le xml de ETAPE_PARAMS
function putParams() {
	var form1 = $j("form[name='form1']").get(0);
	if (!myBlock) return;
	missing='';
	if (form1.etape_param.value.Trim()=='') return;
	
	myXML=importXML(form1.etape_param.value);
	var nodes=myXML.getElementsByTagName('*');
	for (u=0;u<nodes.length;u++) {
		if (!nodes[u].firstChild) continue;
		//1er cas, simple, balise XML & champ unique
		field=document.getElementById(myBlock.id.toLowerCase()+'_'+nodes[u].nodeName); 
		if (field ) { //le champ existe dans le formulaire
			
			if (field.type=='text') field.value=nodes[u].firstChild.nodeValue;
		
			if (field.type=='select-one') {
				setValue(nodes[u].firstChild.nodeValue,field,false); //note : case insensitive
			}
			if ((field.type=='checkbox' || field.type=='radio') && field.value==nodes[u].firstChild.nodeValue) 
				field.checked=true;			
		
		} 
		else { //2ème cas : champs multiples, car la balise XML est multiple, ex: filter 
			//NOTE : dans ce cas, on veillera qu'il existe autant de champs dans le formulaire que 
			//de noeud jumeaux dans le XML !
			
			field=form1.elements[myBlock.id.toLowerCase()+'_'+nodes[u].nodeName+'[]'];
			if (field && field.length) { //les champs existent dans le formulaire, field contient alors un tableau
				for (z=0;z<field.length;z++) {
					//Affectation de la valeur du noeud XML au premier champ vide de la collection
					if (field[z].value=='') {field[z].value=nodes[u].firstChild.nodeValue;break;}
				}	
			}
		}
		
		if (!field && nodes[u].nodeName!='param') {
			//champ manquant dans le formulaire (sauf balise encadrante param)
			//missing+=" "+nodes[u].nodeName;
			newDiv=document.createElement('DIV');
			newLabel=document.createElement('LABEL');
			newLabel.innerHTML=nodes[u].nodeName;
			newInput=document.createElement('INPUT');
			newInput.name=myBlock.id.toLowerCase()+'_'+nodes[u].nodeName;
			newInput.id=myBlock.id.toLowerCase()+'_'+nodes[u].nodeName;
			newInput.value=nodes[u].firstChild.nodeValue;
			newDiv.appendChild(newLabel);
			newDiv.appendChild(newInput);
			myBlock.appendChild(newDiv);
			
		}
	}
	
	if (missing!='') alert('<?=kEtapeChampManquant?> : '+missing); //Notification des champs manquants
	
}

//Met à jour le champ xml ETAPE_PARAMS avec les paramètres du type en cours
function getParams() {
	var form1 = $j("form[name='form1']").get(0);
	
	if (!myBlock) return;
	str='';
	myFields=myBlock.getElementsByTagName('input');
	for (u=0;u<myFields.length;u++) {
		_fld=myFields[u];
		balise=_fld.id.replace(myBlock.id.toLowerCase()+'_','');
		balise=balise.replace('[]',''); //et d'éventuel crochets en cas de champ multiple
		if (_fld.type=='checkbox' && !_fld.checked) continue;
		str+="<"+balise+">"+_fld.value+"</"+balise+">\n";
	}
	
	myFields=myBlock.getElementsByTagName('select');
	for (u=0;u<myFields.length;u++) {
		_fld=myFields[u];
		balise=_fld.id.replace(myBlock.id.toLowerCase()+'_',''); //on ote la partie TYPE_ de la balise
	
		str+="<"+balise+">"+_fld.options[_fld.selectedIndex].value+"</"+balise+">\n";
	}	
	
	str="<param>"+str+"</param>";
	form1.etape_param.value=str;
	return true;
}

//Met à jour les codecs audio et video selon le format choisi (dans le cas encodage)
function updateCodecs(elmt) {
	var form1 = $j("form[name='form1']").get(0);
	if (!elmt) elmt=form1.encod_format;
	for (v=0;v<codecs.length;v++) {
		if (elmt.options[elmt.selectedIndex].value==codecs[v].FORMAT_FILE_EXT) {
			form1.encod_vcodec.value=codecs[v].FORMAT_VCODEC;
			form1.encod_acodec.value=codecs[v].FORMAT_ACODEC;
		}
	}	
}

//Check avant formulaire 
// Contrôle des champs obligatoires + Màj du champ caché Etape_param
function chkFormFields () {
    msg='';
	if (!myBlock) return false;

  arrInputs=myBlock.getElementsByTagName('input'); //Inputs du TYPE en cours
  for(v=0;v<arrInputs.length;v++) {
    if (arrInputs[v].getAttribute('mandatory')&& arrInputs[v].value.Trim()=='') {
        msg+=arrInputs[v].getAttribute('label')+"\n";
           arrInputs[v].focus();
           arrInputs[v].className='errorjs';
           new Effect.Pulsate(arrInputs[v]);
      
      } else  {
        arrInputs[v].className=null;
      }
    }
  
  arrInputs=document.getElementById('ident').getElementsByTagName('input'); //Inputs de la partie IDENT commune
  for(v=0;v<arrInputs.length;v++) {
    if (arrInputs[v].getAttribute('mandatory')&& arrInputs[v].value.Trim()=='') {
         msg+=arrInputs[v].getAttribute('label')+"\n";
           arrInputs[v].focus();
           arrInputs[v].className='errorjs';
           new Effect.Pulsate(arrInputs[v]);
      
      } else  {
        arrInputs[v].className=null;
      }
    }
  
    
    arrInputs=myBlock.getElementsByTagName('select'); //Select du TYPE en cours
  	for(v=0;v<arrInputs.length;v++) {
    if (arrInputs[v].getAttribute('mandatory') 
    		&& arrInputs[v].options[arrInputs[v].selectedIndex].value=='-1' ) {
             msg+=arrInputs[v].getAttribute('label')+"\n";
            
           arrInputs[v].focus();
           arrInputs[v].className='errorjs';
           new Effect.Pulsate(arrInputs[v]);     	
      
      } else {
      	 arrInputs[v].className=null;
      }
    
  	}
  	
  	

    if (msg!='') {alert('<?=kJSErrorValeurOblig?>'+msg);return false;}
    if (!getParams()) return false; //màj du champ ETAPE_PARAM
    
    formChanged=false; //pour ne pas déclencher le message
     
}


function duplicate(url) {
	var form1 = $j("form[name='form1']").get(0);
     if (confirm("<?=kConfirmJSDocDuplication?>")) {
        form1.commande.value="DUP_ETAPE";
        if (url!='') form1.page.value=url;
        form1.submit();
     }	
}


</script>

<style>
.miniHelp {text-align:left;font-family:Arial;font-size:11px;}
.label_champs_form a {display:none;} <!-- pour cacher les flèches -->
</style>
<!--
<a href="javascript:void(null)" onclick="getParams()">test params</a>
-->
<div id='pageTitre' class="title_bar">
<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
<?=kEtape?></div>
<? 
if ($myEtape->error_msg){ ?><div class='error' id='error_msg'><?=$myEtape->error_msg?></div><? } 

include(libDir."class_formSaisie.php");
$xmlform=file_get_contents(getSiteFile("designDir","form/xml/etapeSaisie.xml"));
$myForm=new FormSaisie;
$myForm->saisieObj=$myEtape;
$myForm->entity="ETAPE";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->classHelp="miniHelp";
$myForm->display($xmlform);

if ($myPage->hasReferrer()) $myPage->renderReferrer(); 
?>

<script type="text/javascript">
//chargement de page => affichage du module sélectionné et màj des champs dans ce module
updateModule();
putParams();
updateCodecs();

  function confirmExit() {if (formChanged) return '';}
  window.onbeforeunload=confirmExit;

</script>
