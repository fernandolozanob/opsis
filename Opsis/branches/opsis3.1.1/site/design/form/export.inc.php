<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- D�veloppeurs : Vincent Prost, Fran�ois Duran, J�rome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

if(!isset($type) || empty($type)){
	$type = "doc";
}
$protocol = "http";
if (isset($_SERVER["HTTPS"]))
	$protocol = "https";
$curr_url = "$protocol://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// récupération des infos de la recherche en cours en fonction du type d'export demandé

$id_lang = (isset($_GET['id_lang']) && !empty($_GET['id_lang']) && in_array($_GET['id_lang'],$_SESSION['arrLangues']))?$_GET['id_lang']:$_SESSION['langue'];

switch ($type){
	case "doc":
	case "doc_reportage" :
	case "panierdoc" :
		if($type=='doc' || $type=='doc_reportage'){
			$nbLignesTotal = (isset($_SESSION['recherche_DOC_Solr']['rows'])?$_SESSION['recherche_DOC_Solr']['rows']:(isset($_SESSION['recherche_DOC']['rows'])?$_SESSION['recherche_DOC']['rows']:''));
		}else if ($type=="panierdoc" && isset($_GET['nbLignesTotal'])){
			$nbLignesTotal = $_GET['nbLignesTotal'];
		}
		$nbLignes=   (isset($_SESSION['recherche_DOC_Solr']['nbLignes'])?$_SESSION['recherche_DOC_Solr']['nbLignes']:(isset($_SESSION['recherche_DOC']['nbLignes'])?$_SESSION['recherche_DOC']['nbLignes']:gNbLignesDocListeDefaut));
		if(isset($nbLignes) && isset($nbLignesTotal)){
			$nbLignes = min($nbLignes,$nbLignesTotal);
		}
		$page=   (isset($_SESSION['recherche_DOC_Solr']['page'])?$_SESSION['recherche_DOC_Solr']['page']:(isset($_SESSION['recherche_DOC']['page'])?$_SESSION['recherche_DOC']['page']:1));
		$deft_fieldRefXML = "fieldRef_doc.xml";
	break ;
	case "materiel" :
	case "mat" :
		$nbLignes=   (isset($_SESSION['recherche_MAT']['nbLignes'])?$_SESSION['recherche_MAT']['nbLignes']:gNbLignesMatListeDefaut);
		$nbLignesTotal=   (isset($_SESSION['recherche_MAT']['val_rows'])?$_SESSION['recherche_MAT']['val_rows']:'');
		$page=   (isset($_SESSION['recherche_MAT']['page'])?$_SESSION['recherche_MAT']['page']:1);
		$deft_fieldRefXML = "fieldRef_mat.xml";
	break ;
	case "val" :
		$nbLignes=   (isset($_SESSION['recherche_VAL']['nbLignes'])?$_SESSION['recherche_VAL']['nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=   (isset($_SESSION['recherche_VAL']['val_rows'])?$_SESSION['recherche_VAL']['val_rows']:'');
		$page=   (isset($_SESSION['recherche_VAL']['page'])?$_SESSION['recherche_VAL']['page']:1);
		$deft_fieldRefXML = "fieldRef_val.xml";
	break ;
	case "lex" :
		$nbLignes= (isset($_SESSION['recherche_LEX']['nbLignes'])?$_SESSION['recherche_LEX']['nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=  (isset($_SESSION['recherche_LEX']['val_rows'])?$_SESSION['recherche_LEX']['val_rows']:'');
		$page=   (isset($_SESSION['recherche_LEX']['page'])?$_SESSION['recherche_LEX']['page']:1);
		$deft_fieldRefXML = "fieldRef_lex.xml";
	break ;
	case "tape" :
		$nbLignes=  (isset($_SESSION['recherche_TAPE']['nbLignes'])?$_SESSION['recherche_TAPE']['nbLignes']:gNbLignesListeDefaut);
		$page=   (isset($_SESSION['recherche_TAPE']['page'])?$_SESSION['recherche_TAPE']['page']:1);
		$deft_fieldRefXML = "fieldRef_tape.xml";
	break ;
	case "usager" :
		$nbLignes=   (isset($_SESSION['recherche_USR']['nbLignes'])?$_SESSION['recherche_USR']['nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=  (isset($_SESSION['recherche_USR']['val_rows'])?$_SESSION['recherche_USR']['val_rows']:'');
		$page=   (isset($_SESSION['recherche_USR']['page'])?$_SESSION['recherche_USR']['page']:1);
		$deft_fieldRefXML = "fieldRef_usager.xml";
	break ;
    case "exp" :
        $nbLignes=   (isset($_SESSION['recherche_EXP']['nbLignes'])?$_SESSION['recherche_EXP']['nbLignes']:gNbLignesListeDefaut);
        $nbLignesTotal=  (isset($_SESSION['recherche_EXP']['val_rows'])?$_SESSION['recherche_EXP']['val_rows']:'');
        $page=   (isset($_SESSION['recherche_EXP']['page'])?$_SESSION['recherche_EXP']['page']:1);
        $deft_fieldRefXML = "fieldRef_exp.xml";
        break ;
    case "fonds" :
		$nbLignes=   (isset($_SESSION['recherche_FON']['nbLignes'])?$_SESSION['recherche_FON']['nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=  (isset($_SESSION['recherche_FON']['val_rows'])?$_SESSION['recherche_FON']['val_rows']:'');
		$page=   (isset($_SESSION['recherche_FON']['page'])?$_SESSION['recherche_FON']['page']:1);
		$deft_fieldRefXML = "fieldRef_fonds.xml";
	break ;
	case "groupe" :
		$nbLignes=   (isset($_SESSION['recherche_GRP']['nbLignes'])?$_SESSION['recherche_GRP']['nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=  (isset($_SESSION['recherche_GRP']['val_rows'])?$_SESSION['recherche_GRP']['val_rows']:'');
		$page=   (isset($_SESSION['recherche_GRP']['page'])?$_SESSION['recherche_GRP']['page']:1);
		$deft_fieldRefXML = "fieldRef_groupe.xml";
	break ;
	case "personne" :
	case "pers" :
		$nbLignes=  (isset($_SESSION['recherche_PERS']['nbLignes'])?$_SESSION['recherche_PERS']['nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=  (isset($_SESSION['recherche_PERS']['val_rows'])?$_SESSION['recherche_PERS']['val_rows']:'');
		$page=  (isset($_SESSION['recherche_PERS']['page'])?$_SESSION['recherche_PERS']['page']:1);
		$deft_fieldRefXML = "fieldRef_pers.xml";
	break ;
	case "stat" :
		$nbLignes=  (isset($_SESSION['stat_nbLignes'])?$_SESSION['stat_nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=  (isset($_SESSION['stat_sql']['val_rows'])?$_SESSION['stat_sql']['val_rows']:'');
		$page=  (isset($_SESSION['stat_page'])?$_SESSION['stat_page']:1);
		$deft_fieldRefXML = "";
		break ;
	case "conso" : 
		$nbLignes=  (isset($_SESSION['recherche_CONSO']['nbLignes'])?$_SESSION['recherche_CONSO']['nbLignes']:gNbLignesListeDefaut);
		$nbLignesTotal=  (isset($_SESSION['recherche_CONSO']['val_rows'])?$_SESSION['recherche_CONSO']['val_rows']:'');
		$page=  (isset($_SESSION['recherche_CONSO']['page'])?$_SESSION['recherche_CONSO']['page']:1);
		$deft_fieldRefXML = "fieldRef_conso.xml";
	break ; 
	default :
		$deft_fieldRefXML = "fieldRef_".removeTrickyChars($type).".xml";
	break;
}
	// on laisse l'option d'un appel à un XML custom de référence des champs
	if(isset($_REQUEST['fieldRefXML']) && !empty($_REQUEST['fieldRefXML'])){
		$fieldRefXML = removeTrickyChars($_REQUEST['fieldRefXML']);
	}else{
		$fieldRefXML = $deft_fieldRefXML;
	}
?>
	<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/export.js"></script>
	<iframe id="frameExport" name="frameExport" style="display:none;"></iframe>
	<form id="formExport" name="form1" target="frameExport" method="post" action="<?=$curr_url?>">
		<input name="type" id="type" type="hidden" value="<?= $type ?>" />
		<input name="id" id="id" type="hidden" value="<?= $id_objet ?>" />
		<input name="id_lang" id="id_lang" type="hidden" value="<?= $id_lang ?>" />
		<input name="expr_lang" id="expr_lang" type="hidden" value="<?= $id_lang ?>" />
		<input name="page" id="page" type="hidden" value="<?= $page ?>" />
		<input name="encodage" id="encodage" type="hidden" value="UTF-8" />
		<input name="tIds" id="exp_ids" type="hidden"  />


		<input name="prm_xsl[name_site]" id="name_site" type="hidden" value="<?= gSite ?>" />
		<input name="prm_xsl[logo_site]" id="logo_site" type="hidden" value="Logo_Opsomai_2012_small.jpg" />
		<input name="prm_xsl[date_fr]" id="date_fr" type="hidden" value="<?= date('d-m-Y') ?>" />
        <input name="prm_xsl[layout]" id="layout" type="hidden" />
        <input name="prm_xsl[type_mosaique]" id="type_mosaique" type="hidden" />
        <input name="prm_xsl[xmlfile]" id="xmlfile" type="hidden" />

		<div class="title_bar"><?=kExporter?><div id="close_export_menu" title="<?=kFermer?>" onclick="hideMenuActions()" style="float:right;"></div></div>
		<div id="exportBody">
			<?if(!isset($_GET['exportItem']) || empty($_GET['exportItem'])){?>
			<div class="fld_export">
				<?=kType?> <select name="nb" class="nb exportQuery" onchange="checkActivateIds()">
					<option class="nb_selection" disabled="true"><?=kSelection?></option>
					<?php if($type != 'panierdoc'){?><option class="nb_current_page" value="<?= $nbLignes ?>"><?=kPageCourante?> (<?=$nbLignes?>)</option><?php }?>
					<option value="all"><?=kComplet?><? if(isset($nbLignesTotal) && !empty($nbLignesTotal)){echo ' ('.$nbLignesTotal.')';}?></option>
				</select>
			</div>
			<?}?>
			<div class="std_export_form fld_export">
				<ul class="type_export">
					<? // Version originale de l'export XML => exporte littéralement la structure XML classique des entités d'opsis ?>
					<?/*?>
					<li>
						<input type="radio" name="export" onchange="updateHiddenFields(this);"  value="0"/> <?=kExportLabelXml?> opsis
						<input name="fichier" class="hiddenExportPrms" attr="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
					</li>
					<?*/?>
					<? // Version customizable / flat => permet d'avoir une représentation plus plate des données, avec eventuellement des tags spécifiques etc ..
						// pourrait fonctionner avec les exports customisables
						// peut être plutot orienté pour des APIs que nous serions amenés à fournir ...?>
					<li>
						<input type="radio" name="export" onchange="updateHiddenFields(this);" value="exportXMLFlat_xml" id="export_XML" /> <label for="export_XML"><span class="icn-export icn-export_xml"></span> <?=kExportLabelXml?></label>
						<input name="fichier" class="hiddenExportPrms" attr="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
						<? if(!empty($fieldRefXML)){ ?>
						<input name="prm_xsl[xmlfile]" id="xmlfile" class="hiddenExportPrms"   disabled="disabled" type="hidden" value="designDir$<?=$fieldRefXML?>"/>
						<? } ?>
					</li>

				   <? if ($type=="doc" || $type=="panierdoc" || $type=="doc_reportage") { ?>
						<li>
							<input type="radio" name="export" onchange="updateHiddenFields(this);" checked="true" value="exportTab_xls" id="export_CSV"/><label for="export_CSV"><span class="icn-export icn-export_csv"></span> <?=kExportLabelExcel?></label>
							<input name="fichier" class="hiddenExportPrms"  type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
							<input name="encodage" class="hiddenExportPrms"  type="hidden" value="ISO-8859-1" />
							<input name="prm_xsl[xmlfile]" id="xmlfile" class="hiddenExportPrms"   disabled="disabled" type="hidden" value="designDir$<?=$fieldRefXML?>"/>
						</li>
						<li>
						<input type="radio" name="export" onchange="updateHiddenFields(this);" value="exportTxt_doc" id="export_DOC"/><label for="export_DOC"><span class="icn-export icn-export_csv"></span> <?=kExportLabelText?></label>
							<input name="fichier" class="hiddenExportPrms"  type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
							<input name="encodage" class="hiddenExportPrms"  type="hidden" value="ISO-8859-1" />
							<input name="prm_xsl[xmlfile]" id="xmlfile" class="hiddenExportPrms"   disabled="disabled" type="hidden" value="designDir$<?=$fieldRefXML?>"/>
						</li>

						<li>
							<input type="radio" name="export" onchange="updateHiddenFields(this);"  value="exportPDFviaFop_PDF" id="export_CATA_PDF"/><label for="export_CATA_PDF"><span class="icn-export icn-export_catalog"></span> <?=kExportLabelCatalogue?> (PDF)</label><!-- Fiches docs full -->
							<input name="impression" class="hiddenExportPrms" disabled="disabled" type="hidden" value="download" />
							<input name="fichier" class="hiddenExportPrms"  disabled="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
							 <input name="prm_xsl[xmlfile]" id="xmlfile" class="hiddenExportPrms"   disabled="disabled" type="hidden" value="designDir$<?=$fieldRefXML?>"/>
						</li>
						 <li>
							<input type="radio" name="export" onchange="updateHiddenFields(this);"  value="exportPDFviaFop_PDF" id="export_MOS2_PDF"/><label for="export_MOS2_PDF"><span class="icn-export icn-export_mos2x2"></span> <?=kExportLabelPlanche?> 2 x 2 (PDF)</label><!-- Mosaique 2 x 2 -->
							<input name="impression" class="hiddenExportPrms" disabled="disabled"  type="hidden" value="download" />
							<input name="fichier" class="hiddenExportPrms"  disabled="disabled"  type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
							<input name="prm_xsl[layout]" class="hiddenExportPrms" disabled="disabled"  value="mosaique" type="hidden" />
							<input name="prm_xsl[nb_by_line]" class="hiddenExportPrms" disabled="disabled" value="2" type="hidden" />
							<input name="prm_xsl[img_block_shape]" class="hiddenExportPrms" disabled="disabled" value="square" id="img_block_shape" type="hidden" />
						</li>
						<li>
							<input type="radio" name="export" onchange="updateHiddenFields(this);"  value="exportPDFviaFop_PDF"  id="export_MOS3_PDF"/><label for="export_MOS3_PDF"><span class="icn-export icn-export_mos3x3"></span> <?=kExportLabelPlanche?> 3 x 3 (PDF)</label><!-- Mosaique 3 x 3 -->
							<input name="impression" class="hiddenExportPrms" disabled="disabled" type="hidden" value="download" />
							<input name="fichier" class="hiddenExportPrms"  disabled="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
							<input name="prm_xsl[layout]" class="hiddenExportPrms"  disabled="disabled" value="mosaique" type="hidden" />
							<input name="prm_xsl[nb_by_line]" class="hiddenExportPrms" disabled="disabled" value="3" type="hidden" />
							<input name="prm_xsl[img_block_shape]" class="hiddenExportPrms" disabled="disabled" value="square" id="img_block_shape" type="hidden" />
						</li>
						<li>
							<input type="radio" name="export" onchange="updateHiddenFields(this);" value="exportPDFviaFop_PDF" id="export_MOS4_PDF"/><label for="export_MOS4_PDF"> <span class="icn-export icn-export_mos4x6"></span> <?=kExportLabelPlanche?> 4 x 6 (PDF)</label><!-- Mosaique 4 x 6 -->
							<input name="impression" class="hiddenExportPrms" disabled="disabled" type="hidden" value="download" />
							<input name="fichier" class="hiddenExportPrms"  disabled="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
							<input name="prm_xsl[layout]" class="hiddenExportPrms"  disabled="disabled" value="mosaique" type="hidden" />
							<input name="prm_xsl[nb_by_line]" class="hiddenExportPrms" disabled="disabled" value="4" type="hidden" />
							<input name="prm_xsl[img_block_shape]" class="hiddenExportPrms" disabled="disabled" value="4_3" id="img_block_shape" type="hidden" />
						</li>
						<li>
							<input type="radio" name="export" onchange="updateHiddenFields(this);"  value="custom_export" id="custom_export"/><label for="custom_export"><span class="icn-export icn-export_custom"></span> <?=kExportLabelPersonnalise?></label>
						</li>
					<? } else if (in_array($type,array('materiel','mat','val','lex','pers','personne','usager','tape','fonds','groupe','exp','conso')) || !empty($fieldRefXML)) {  
					//MS - 06.04.18 - ajout du cas conso, mais le seul vrai test ici est de savoir si $fieldRefXML est défini, si le fieldRefXML est défini en entete de cette meme page & n'est pas empty, on part du principe qu'il existe .... ?>
						<li>
							<input type="radio" name="export" checked="true" onchange="updateHiddenFields(this);" value="exportTab_xls"/> <span class="icn-export icn-export_csv"></span> <?=kExportLabelExcel?>
							<input name="prm_xsl[xmlfile]" id="xmlfile" class="hiddenExportPrms"   disabled="disabled" type="hidden" value="designDir$<?=$fieldRefXML?>"/>
							<input name="fichier" type="hidden" class="hiddenExportPrms"  disabled="disabled" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
						</li>
						<li>
							<input type="radio" name="export" onchange="updateHiddenFields(this);"  value="custom_export"/> <span class="icn-export icn-export_custom"></span> <?=kExportLabelPersonnalise?>
						</li>
					<? } else if ($type=="panier") { ?>
						<li>
							<input type="radio" name="export" checked="true" onchange="updateHiddenFields(this);" value="exportTab_xls"/> <span class="icn-export icn-export_csv"></span> <?=kExportLabelExcel?>
							<input name="fichier" type="hidden" class="hiddenExportPrms"  disabled="disabled" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
						</li>
					<? } else { ?>
						<li>
							<input type="radio" name="export" checked="true" onchange="updateHiddenFields(this);" value="exportTab_xls"/> <span class="icn-export icn-export_xml"></span>  <?=kExportLabelExcel?>
							<input name="fichier" type="hidden" class="hiddenExportPrms"  disabled="disabled" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
						</li>
				   <? } ?>
				</ul>
			</div>
			<!-- DEBUT EXPORT CUSTOM -->
			<div class="fld_export custom_export_form" style="display:none">
				<div class="customField cust_format">
					<?= kExportLabelFormat?>
					<select name="export" class="cust_export_sel" disabled="disabled" onchange="updateCustomHiddenFields(this)">
						<option class="custom_xml" value="exportXMLFlat_xml"><?=kExportLabelXml?></option><!-- Pour l'instant, l'export XML ne renvoi que la structure opsis, il faut modifier pour pouvoir sortir un xml dépendant d'un xmlfile, et normalisé (profondeur : racine > éléments docs > tout les champs demandés / defaut au même niveau) -->
						<option class="custom_txt" selected="true" value="exportTxt_doc"><?=kExportLabelText?></option>
						<option class="custom_xls" selected="true" value="exportTab_xls"><?=kExportLabelExcel?></option>
						<? if ($type=="doc" || $type=="panierdoc" ){?>
							<option class="custom_foppdf_catalogue" value="exportPDFviaFop_PDF"><?=kExportLabelCatalogue?></option>
							<?if(!isset($_GET['exportItem']) || empty($_GET['exportItem'])){?>
								<option class="custom_foppdf_planche" value="exportPDFviaFop_PDF"><?=kExportLabelPlanche?></option>
							<?}?>
						<?}?>
					</select>
				</div>
				<div style="display:none;">
					<input name="fichier" class="hiddenExportPrms custom_xml custom_xls" disabled="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
					<input name="fichier" class="hiddenExportPrms custom_txt custom_txt" disabled="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
					<input name="fichier" class="hiddenExportPrms custom_foppdf_catalogue custom_foppdf_planche"  disabled="disabled" type="hidden" value="<?= (isset($_REQUEST['fichier'])?$_REQUEST['fichier']:'opsis_export_'.$type); ?>" />
					<input name="encodage" class="hiddenExportPrms custom_xls"  type="hidden" disabled="disabled" value="ISO-8859-1" />
					<input name="encodage" class="hiddenExportPrms custom_txt"  type="hidden" disabled="disabled" value="ISO-8859-1" />
					<input name="impression" class="hiddenExportPrms custom_foppdf_catalogue custom_foppdf_planche" disabled="disabled" type="hidden" value="download" />
					 <input name="prm_xsl[xmlfile]" id="xmlfile" class="hiddenExportPrms custom_xml"   disabled="disabled" type="hidden" value="designDir$<?=$fieldRefXML?>"/>
					 <input name="prm_xsl[layout]" class="hiddenExportPrms custom_foppdf_planche" disabled="disabled"  value="mosaique" type="hidden" />
				</div>
				<!-- export=exportPDFviaFop_PDF -->
				<div class="fopPdfPrms_panel hidden">
					<div>
					<label><?=kExportLabelItemByLine?> : </label><br /><select  name="prm_xsl[nb_by_line]" class="hiddenExportPrms" disabled="disabled" id="nb_by_line">
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
					</div>
					<div>
					<label><?=kExportLabelDimensionsImage?> : </label><br /><select  name="prm_xsl[img_block_shape]" class="hiddenExportPrms" disabled="disabled" id="img_block_shape">
						<option value="square">1:1</option>
						<option value="4_3">4:3</option>
						<option value="16_9">16:9</option>
					</select>
					</div>
				</div>
						<?
						if(!empty($fieldRefXML)) {
							$str_const_dir = 'designDir';
							if(basename($fieldRefXML) == $fieldRefXML && $str_const_dir == 'designDir'){
								$print_path = getSiteFile($str_const_dir,'/print/xml/'.$fieldRefXML);
								$fieldref_path = getSiteFile($str_const_dir,'/fieldref/'.$fieldRefXML);
								if(strpos($print_path,baseDir) !== false && dirname($print_path ) != 'index.php'){
									$path = $print_path;
								}else if (strpos($fieldref_path,baseDir) !== false && dirname($print_path ) != 'index.php'){
									$path = $fieldref_path;
								}else if (dirname($print_path) != 'index.php'){
									$path = $print_path;
								}else{
									$path = $fieldref_path;
								}
							}else{
								$path = getSiteFile($str_const_dir,$fieldRefXML);
							}
			
							if(strrpos($path,'index.php') !== 0 ){
								$xml_ref_field = file_get_contents($path);
							}

							if(!isset($xml_ref_field) && in_array($type,array('doc','panierdoc'))){
								// on récupère la liste des champs / noms des champs (ici uniquement pour le cas document)
								// pour l'instant est récupéré depuis docAff, on pourrait éventuellement pointer vers un autre fichier comme référence des champs / labels dispos (via une constante ? )
								$xml_ref_field = file_get_contents(getSiteFile("designDir","detail/xml/docAff.xml"));
							}

							$xml_elt_ref_field = new SimpleXMLElement($xml_ref_field);
							$result_ref_fields = $xml_elt_ref_field->xpath("//element");

							$nb_fields = 3 ;

							// tri alphanumérique des champs proposés
							function sortLabels($a,$b){
								return strnatcmp  ((defined($a->label)?constant($a->label):$a->label),(defined($b->label)?constant($b->label):$b->label));
							}
							usort($result_ref_fields,sortLabels);

							if($type == 'panierdoc'){
								$type_form = "doc";
							}else{
								$type_form = $type ;
							}

							$arr_zone_items = array();
							foreach($result_ref_fields as $elt){
								if(!isset($elt->veto_fieldref_export) || empty($elt->veto_fieldref_export)){
									$zone_item = '<div class="customField2">';
									$zone_item .= '<div class="cust_field_row">';
									$zone_item .= '<input class="cust_field_checker" type="checkbox" checked="checked"/>';
									$zone_item .= '<span class="cust_field_text">'.(defined($elt->label)?constant($elt->label):$elt->label);
									if(isset($elt->sufLabel)){
										$zone_item .=' '.(defined($elt->sufLabel)?constant($elt->sufLabel):$elt->sufLabel);
									}
									$zone_item .= '</span>';
									$zone_item .= '<span class="sortable_icon"></span>';
									$zone_item.="</div>";
									$zone_item.='<span class="cust_field_arrow_action" onclick="toggleCustomField(this);"></span>';
									$zone_item.='<div class="data_customField" style="display:none ; ">';
									$zone_item.='<input type="hidden" disabled class="cust_field_val" name="custom_fields[VALUE][]" value="'.(isset($elt->opsfield)?$elt->opsfield:$elt->chFields).'"/>';
									$zone_item.='<input type="hidden" disabled class="cust_field_xml" name="custom_fields[XML][]" value="<![CDATA['.$elt->asXML().']]>"/>';
									$zone_item.="</div>";

									$zone_item.="</div>";
									array_push($arr_zone_items,$zone_item);
								}
							}
						}
						?>

				<div id="customFieldsWrapper2">
					<div id="customFieldsTitle"><?=kExportTitleAreas?></div>
					<div class="customFields2">
						<div class="cust_check_all_wrapper"><input class="cust_check_all" type="checkbox" checked onclick="exportCheckAllFields('.inactive',this)"/><?=kToutSelectionner?></div>
						<div class="cust_field_area inactive">
						<?
							foreach($arr_zone_items as $zone_item){
								echo $zone_item;
							}
						?>
						</div>
					</div>
					<div id="customFieldsArrows">
						<div class="main_arrow to_right" onclick="switchAllChecked('inactive');"></div>
						<div class="main_arrow to_left" onclick="switchAllChecked('active');"></div>
					</div>
					<div class="customFields2">
						<div class="cust_check_all_wrapper"><input class="cust_check_all" type="checkbox" checked onclick="exportCheckAllFields('.active',this)"/><?=kToutSelectionner?></div>
						<div class="cust_field_area active"></div>
					</div>
				</div>
				<?

				echo '<script type="text/javascript">'."\n";
				echo 'var tabExportXML = new Array() ;'."\n";

				/** Il est possible de recharger un formulaire déja configuré :
					A terme, un modèle de custom pourra être sauvegardé  & rechargé,
					En attendant on va recharger le dernier modèle utilisé **/
				if(isset($_SESSION['export'][$type_form]['lastCustomFields']) && !empty($_SESSION['export'][$type_form]['lastCustomFields'])){
					$xml_pattern_fields = new SimpleXMLElement($_SESSION['export'][$type_form]['lastCustomFields']);
					$result = $xml_pattern_fields->xpath('//element');
					if(!empty($result)){
						foreach($result  as $elt){
							if(isset($elt->opsfield) && !empty($elt->opsfield)){
								echo "tabExportXML.push(\"".str_replace("\n",'\n',$elt->asXML())."\");";
							}else if(isset($elt->chFields) && !empty($elt->chFields)){
								echo "tabExportXML.push(\"".str_replace("\n",'\n',$elt->asXML())."\");";
							}
						}
						echo 'loadTemplateExport(tabExportXML);'."\n";
					}
				}else if(isset($xml_ref_field) && !empty($xml_ref_field)){
					// Par défaut, si on aucun export custom en cours on charge le xml de définition des champs (fieldRef)
					// ce qui permet de reconstruire le meme export que l'export par défaut dans le custom.
					// => A terme il faudra éventuellement modifier ce chargement pour ne pas charger l'ensemble des champs
					// => on pourrait utiliser un tag XML dans les fichiers fieldref qui permettrait de savoir si un champ (tag élément) devrait être chargé par défaut
					$xml_elt_ref_field = new SimpleXMLElement($xml_ref_field);
					$result = $xml_elt_ref_field->xpath("//element");
					if(!empty($result)){
						foreach($result  as $elt){
							if(isset($elt->opsfield) && !empty($elt->opsfield)){
								echo "tabExportXML.push(\"".str_replace("\n",'\n',$elt->asXML())."\");";
							}else if(isset($elt->chFields) && !empty($elt->chFields)){
								echo "tabExportXML.push(\"".str_replace("\n",'\n',$elt->asXML())."\");";
							}
						}
						echo 'loadTemplateExport(tabExportXML);'."\n";
					}
				}


				echo "makeCustomFieldsSortable();\n";
				echo "hideCustomExport();\n";
				echo '</script>'."\n";
				?>
			</div>
			<div class="fld_export" id="fieldSubmit">
				<div id="progress_export"></div>
				<input type="submit" name="bChercher" onclick="beginExportReport();" class="std_button_style" value="<?= kTelecharger ?>" />
			</div>
			<div class="fld_export" id="continueToCustomExport" style="display:none;">
				<input type="button" name="bCustomExport" onclick="showCustomExport();" class="std_button_style" value="<?= kContinuer ?>" />
			</div>
			<div id="msgExportDone" ><?=kExportMsgDone?></div>
			<div class="fld_export" id="closeExportDone">
				<input type="button" name="bCustomExport" onclick="hideMenuActions();" class="std_button_style" value="<?= kFermer ?>" />
			</div>
		</div>
	</form>

