<? 
include(getSiteFile("designDir","menuExp.inc.php")); 
?>

<div id="fiche_info">
	<script type="text/javascript">
		function addValue(id_champ,valeur,idx,autre_valeur) {
			arrField=id_champ.split('$');
			type=arrField[0];
			cntFields=0;
	
			if (document.getElementById(type+'$limit')) limit=document.getElementById(type+'$limit').value; else limit=20;
			allowdouble=document.getElementById(type+'$allowdouble');
	
			// ajouter test sur valeur existante
			myTab=document.getElementById(type+'$tab');
	
			for(i=0;i<myTab.childNodes.length;i++) {
				chld=myTab.childNodes[i];
	
				if (chld.id && chld.id.indexOf('$DIV$')!=-1) cntFields++;
				if (cntFields==limit) {
					alert (limit+'<?=kDOMlimitvalues?>');
					return false;
					}
	
				if (chld.id==type+'$DIV$'+idx && !allowdouble) {
					alert('<?=kDOMnodouble?>'); return false;
	
				}
			}
	
			newFld=document.createElement('DIV');
			newFld.id=type+'$DIV$'+idx;
			newFld.name=type+'$DIV$'+idx;
			newFld.className=myTab.className;
			addRow=new Array(newFld.id,idx,valeur,autre_valeur);
			str=eval('add_'+type+'(addRow)');
			newFld.innerHTML=str[1];
	
			myBtn=document.getElementById(type+'$add');
			if (myBtn) {myTab.insertBefore(newFld,myBtn);} else {myTab.appendChild(newFld);}
		}
	
		window.onload=function () { makeDraggable(document.getElementById('chooser'));}
	
	</script>

	<div id='chooser' class='iframechoose' name='chooser' style='position:absolute;display:none;'></div>

	<?
	if ($myDoc->error_msg) echo "<div class='error' id='error_msg'>".$myDoc->error_msg."</div>";

	$myUsr=User::getInstance();

	?>
	<form name="form1" id="form1" method="post" action="<?=$myPage->getName()?>?urlaction=expSaisie&form=expProdSaisie&id_exp=<?= $myExp->t_exploitation['ID_EXP'] ?>">
		<input name="commande" type="hidden" value="SAVE_PROD" />
		<input name="page" type="hidden" value="" />
		<input name="id_exp" type="hidden" value="<?= $myExp->t_exploitation['ID_EXP'] ?>" />
		<input name="save_action" id="save_action" type="hidden" value="SAVE_PROD" />
		<input name="ligne" type="hidden" value="" />
		<table width=100% border=0>
			<tr>
				<td>
					<table>
						<tr>
							<td style="width : 210px">
	        					<?=kCote ?>
	        				</td>
	        				<td>
	        					<?=kTitre ?>
	        				</td>
	        			</tr>
	        		</table>
	        	</td>
	        </tr>
	        <tr>
	        	<td>
	  				<div id="t_exp_doc$tab" class='lineDoc' border='2' align="left" style="width:590px;" >
						<?
						$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>"t_exp_doc[][ID_DOC]");
						$arrFields[]=array("ID"=>"","TYPE"=>"TEXT", "NAME"=>"","ATTR"=>"readonly='1' size='30' style='width:200px'","LIB"=>"&nbsp;");
						$arrFields[]=array("ID"=>"","TYPE"=>"TEXT", "NAME"=>"","ATTR"=>"readonly='1' size='60'","LIB"=>"&nbsp;");
						$arrFields[]=array("ID"=>"","TYPE"=>"EDIT","ATTR"=>" alt='".kModifier."' title='".kModifier."' ");
						$arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
			
						$js="<script type='text/javascript'>divs='';\n";
						echo makeFormJSFunction2('t_exp_doc',$arrFields,'lineDoc');
						foreach ($myExp->t_exp_doc as $idx=>$dlsrc){
			
							$js.="arrRow=new Array('t_exp_doc$DIV$".$dlsrc['ID_DOC']."',".quoteField($dlsrc['ID_DOC']).",'".$dlsrc['DOC_COTE']."',".quoteField($dlsrc['DOC_TITRE']).",'".$myPage->getName()."?urlaction=docSaisie&id_doc='+encodeURIComponent('".$dlsrc['ID_DOC']."'));";
							$js.="out=add_t_exp_doc(arrRow);\n document.write(out[0]);\n";
			
						}
						$js.="document.write(divs);\n</script>";echo $js;
						unset($arrFields);
						?>
	
						<button id="t_exp_doc$add" class="ui-state-default ui-corner-all" type="button"  onclick="choose(this,'titre_index=<?=kProductions?>&id_lang=FR&valeur=&champ=MDOC_DOC_COTE&autreChamps=DOC_TITRE&autre_libelle=kTitre&libelle=kCote&xsl=paletteMdoc')">
							<span class="ui-icon ui-icon-plus"></span><?= kChoisir ?>
						</button>
						<button id="bOK" class="ui-state-default ui-corner-all" type="button"  onclick="document.form1.submit();">
							<span class="ui-icon ui-icon-check"></span><?= kEnregistrer ?>
						</button>
					</div>
	                </td>
			</tr>
			<tr>
				<td>
				</td>
			</tr>
		</table>
	
	</form>
	<?$myPage->renderReferrer();?>
</div>