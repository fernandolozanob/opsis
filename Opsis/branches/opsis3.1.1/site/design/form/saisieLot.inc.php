<?php
/*
 * Created on 26 avr. 10
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include(libDir."class_formSaisie.php");

if (empty($myMod->resultSet)) {
	echo "<div class='error' style='font-size:large'>".$myMod->error_msg."</div>";
} else {
?>
<!--  <iframe id="frameModif" name="frameModif" style="display:none;"></iframe>-->
<script language="JavaScript" type="text/javascript">

		//by LD : pour ajouter une valeur dynamiquement � un tableau avec DOM.
	function addValue(id_champ,valeur,idx,role) {
		formChanged=true;
		if(id_champ.indexOf('$')== -1){
			fld=document.getElementById(id_champ);
			if (fld.value!='') fld.value+=", ";
			fld.value+='"'+valeur+'"';
		} else {
			arrField=id_champ.split('$');
			type=arrField[0];
			cntFields=0;
			if (document.getElementById(type+'$limit')) limit=document.getElementById(type+'$limit').value; else limit=250;
			allowdouble=document.getElementById(type+'$allowdouble');

			// ajouter test sur valeur existante
			prt=document.getElementById(type+'$tab');
			for(i=0;i<prt.childNodes.length;i++) {
				chld=prt.childNodes[i];

				if (chld.id && chld.id.indexOf('$DIV$')!=-1) cntFields++;
				if (cntFields==limit) {
					alert (limit+'<?=kDOMlimitvalues?>');
					return false;
					}

				if (chld.id==type+'$DIV$'+idx && !allowdouble) {
					alert('<?=kDOMnodouble?>'); return false;

				}
			}
			btnAdd=document.getElementById(type+'$add');

			newFld=document.createElement('DIV');
			newFld.id=type+'$DIV$'+idx;
			newFld.name=type+'$DIV$'+idx;
			newFld.className=prt.className;
			addRow=new Array(newFld.id,valeur,idx,role)
			str=eval('add_'+type+'(addRow)');
			newFld.innerHTML=str[1];
			if (btnAdd) {prt.insertBefore(newFld,btnAdd);} else {prt.appendChild(newFld);}
		}
	}
	
	function chkFormLotFields (myform) {
		msg='';
		var i;
		arrElts=document.form2.getElementsByTagName('*');
		for (i=0;i<arrElts.length;i++) {
			if (!arrElts[i].getAttribute('mandatory')) continue; //test si obligatoire

			if (arrElts[i].value.Trim()=='') {
				switch(arrElts[i].getAttribute('label')){
					default :
						lib=arrElts[i].getAttribute('label');
						break;
				}
				msg=msg+' '+lib+'\n';
				arrElts[i].className='errorjs';
			} else {
				arrElts[i].className='';
			}
		}
		if (msg!='') {alert(str_lang.kJSErrorOblig+msg);return false; }
		return true;
	}
</script>
<div class="frame_menu_actions title_bar"><?=kModifierLot?></div>
<?php
$xmlform=file_get_contents($xmlAff);

$myForm=new FormSaisie;
$myForm->saisieObj=$myObj;
$myForm->entity=strtoupper(strtolower(!empty($entiteForm)?$entiteForm:$entite));
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->classHelp="help_champs_form";
$myForm->display($xmlform);
?>
<script language="JavaScript" type="text/javascript">
<!-- On remet pour l'instant le champ 'commande' à "INIT" -->
document.form2.commande.value = 'INIT';
</script>
<?php
}
?>
