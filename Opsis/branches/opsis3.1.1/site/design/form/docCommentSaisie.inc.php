<?
//Ajout commentaire en bdd
$action=$_POST["commande"];
if (isset($action) && $action = "ADD_COMMENT") {
	$myDoc->getDocComments();
	$comment = array();
	foreach ($_POST as $key=>$val)
		if (strpos($key, "dc_") === 0)
			$comment[strtoupper($key)] = $val;
	$comment["ID_DOC"] = $myDoc->t_doc['ID_DOC'];
	$comment['DC_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
	$comment['DC_DATE_MOD']=$comment['DC_DATE_CREA'];
	if ($myUser->UserID) { 
		$comment['DC_ID_USAGER_CREA'] = $myUser->UserID;
		$comment['DC_AUTEUR']=$myUser->Prenom." ".$myUser->Nom;
	}
	else $comment['DC_ID_USAGER_CREA'] = 0;
	$comment['DC_ID_USAGER_MOD']=$comment['DC_ID_USAGER_CREA'];
	
	$myDoc->t_doc_comment[] = $comment;
	$myDoc->saveDocComments();
	?><script>
		var newLocation = ""+window.opener.location;
		if (newLocation.indexOf("&display=comments") == -1)
			newLocation += "&display=comments";
			
		window.opener.location = newLocation;
		window.close();
	</script><?
}
?>
<div id="fiche_info" class="<?=($entity_has_media)?'':'no_media'; ?>"> 
	<?
	if ($myDoc->error_msg) echo "<div class='error' id='error_msg'>".$myDoc->error_msg."</div>";  

	include(libDir."class_formSaisie.php");

	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/docCommentSaisie.xml"));
	$myForm=new FormSaisie;
	$myForm->saisieObj=$myDoc;
	$myForm->entity="DOC";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->classHelp="help_champs_form";
	$myForm->display($xmlform);
	?>
</div>

<script>
	<? if ($myUser) { ?>
		$j("#dc_auteur").parent("td").parent("tr").css("display", "none");
	<? } ?>
	function closePopup() {
		window.close();
	}
	
	function chkFormFields(elem) {
		saisieForm.commande.value = "ADD_COMMENT";
	}
</script>