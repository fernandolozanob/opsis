<iframe id="framePartage" name="framePartage"></iframe>
<div class="title_bar"><?=kPartager?><div id="close_export_menu" title="<?=kFermer?>" onclick="hideMenuActions()" style="float:right;"></div></div>
<div id="menuBody">
	<form  action="empty.php?urlaction=partage&where=include"  method="post" id="formPartage" class="formMenu formPartage" name="formPartage" target="framePartage">
		<div class="returnMsg"></div>
		<input type="hidden" name="action"/>
		<input type="hidden" name="xsl" value="<?=$_GET['xsl']?>"/>
		<input type="hidden" name="logo_url" value="design/images/<?= defined('gSiteLogoFilename')?gSiteLogoFilename:'logo_opso_alpha_2.png' ?>"/>
		<input type="hidden" name="partage_type" value="MAIL"/>
		<input type="hidden" name="partage_titre" value="<?=$_GET['titre']?>"/>
		<input type="hidden" name="partage_type_entite" value="<?=$_GET['type_partage']?>"/>
		<input type="hidden" name="partage_id_entite" value="<?=$_GET['id_item']?>"/>
		<table width="80%" align="center">
			<tr>
				<td class="label_champs_form"><?=kDestinataire?></td>
				<td class="val_champs_form"><input type="text" required="required" style="width:99%" cols="60" name="partage_mail_dest"/></td>
			</tr>
			<tr>
				<td class="label_champs_form"><?=kObjet?></td>
				<td class="val_champs_form"><input type="text" required="required" style="width:99%"  name="partage_objet"/></td>
			</tr>
			<tr>
				<td class="label_champs_form"><?=kCorps?></td>
				<td class="val_champs_form"><textarea cols="60" rows="6" style="resize:vertical;max-height:600px;" name="partage_corps"></textarea></td>
			</tr>
			<tr>
				<td class="label_champs_form"><?=kExpediteurCopie ?></td>
				<td class="val_champs_form"><input type="checkbox" name="partage_expediteur_cc" value="1"/></td>
			</tr>
			<tr>
				<td class="label_champs_form"><?=kDureePartage ?></td>
				<td class="val_champs_form">
					<select name="partage_date_limit">
						<option value="<?=mktime(date("H"),date("i"),date("s"),date("n"),date("j")+7,date("Y"))?>">1 <?= kSemaine?></option>
						<option value="<?=mktime(date("H"),date("i"),date("s"),date("n"),date("j")+14,date("Y"))?>">2 <?= kSemaines?></option>
						<option value="<?=mktime(date("H"),date("i"),date("s"),date("n")+1,date("j"),date("Y"))?>">1 <?= kMois?></option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<button class="std_button_style" onclick="trigger_partage()"><?=kEnvoyer?></button>
				</td>
			</tr>
		</table>
	</form>
</div>
<script>
function trigger_partage(){
	returnMsg_elt = $j("form#formPartage div.returnMsg");
	// reset du champ de feedback
	returnMsg_elt.removeClass("error");
	returnMsg_elt.html("");
	
	// controle adresse mail
	if($j("input[name='partage_mail_dest']").val()=='' || !formCheckMultiEmail($j("input[name='partage_mail_dest']").val())){
		returnMsg_elt.addClass("error");
		returnMsg_elt.html("Une adresse email valide est requise.");
	}
	// controle sujet mail
	if($j("input[name='partage_objet']").val()==''){
		returnMsg_elt.addClass("error");
		returnMsg_elt.html((returnMsg_elt.html()!=''?returnMsg_elt.html()+"<br/>":"")+"Le champ \"Objet\" est obligatoire.");
	}
	// blocage si erreur
	if(returnMsg_elt.hasClass("error")){
		return false;  
	}
	
	$j("form#formPartage").addClass("loading");
	$j("form.formPartage input[name='action']").val("create");
	$j("#framePartage").unbind('load');
	$j("#framePartage").load(function(){
		$j("form#formPartage").removeClass("loading");
		try{
			$j("div.returnMsg").html($j("iframe#framePartage").contents().find("body").html());
		}catch(e){
			console.log("crash load content iframe retour partage.php");
		}
	});
	$j("form.formPartage").submit();
}

</script>
