<?
	// Nb Connexions
	$tStat["Nb Connexions"]="select to_char(t_action.ACT_DATE,'mm-dd-YYYY') as DATE,count(*) as NB from t_action group by DATE order by DATE desc";
	// Connexions
	$tStat["Connexions"]="select t_action.ACT_DATE as DATE,t_usager.US_NOM from t_action
							 left join t_usager on t_action.ID_USAGER=t_usager.ID_USAGER
							 where t_action.ACT_TYPE='IN' group by DATE, t_usager.US_NOM order by DATE desc";
	// Recherches
	$tStat["Recherches"]="select t_action.ACT_DATE as DATE,t_usager.US_NOM,t_action.ACT_REQ as RECHERCHE,max(t_requete.REQ_NB_DOC) as NB from t_action
							 left join t_usager on t_action.ID_USAGER=t_usager.ID_USAGER
							 left join t_requete on t_action.ACT_REQ=t_requete.REQ_LIBRE and t_action.ID_USAGER=t_requete.ID_USAGER
							 where t_action.ACT_TYPE='SQL' group by DATE, t_usager.US_NOM,RECHERCHE order by DATE desc";
	// Notices
	$tStat["Notices"]="select t_action.ACT_DATE as DATE,t_usager.US_NOM,t_doc.DOC_COTE,t_doc.DOC_TITRE from t_action
							 left join t_doc on t_action.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG='".strtoupper($_SESSION["langue"])."'
							 left join t_usager on t_action.ID_USAGER=t_usager.ID_USAGER
							 where t_action.ACT_TYPE='DOC' and t_action.ID_DOC!=0  group by DATE, t_usager.US_NOM,t_doc.DOC_COTE,t_doc.DOC_TITRE order by DATE desc";
	// Visionnages
	$tStat["Visionnages"]="select t_action.ACT_DATE as DATE,t_usager.US_NOM,t_doc.DOC_COTE,t_doc.DOC_TITRE from t_action
							 left join t_doc on t_action.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG='".strtoupper($_SESSION["langue"])."'
							 left join t_usager on t_action.ID_USAGER=t_usager.ID_USAGER
							 where t_action.ACT_TYPE='VIS' and t_action.ID_DOC!=0 group by DATE, t_usager.US_NOM,t_doc.DOC_COTE,t_doc.DOC_TITRE order by DATE desc";
	// Nb Notices
	$tStat["Nb Notices"]="select to_char(t_action.ACT_DATE,'mm-dd-YYYY') as DATE,count(*) as NB from t_action where t_action.ACT_TYPE='DOC' group by DATE order by DATE desc";
	// Top notices
	$tStat["Top notices"]="select t_doc.DOC_COTE,t_doc.DOC_TITRE,count(*) as NB from t_action
							 left join t_doc on t_action.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG='".strtoupper($_SESSION["langue"])."'
							 where t_action.ACT_TYPE='DOC' and t_action.ID_DOC!=0  group by t_action.ID_DOC,t_doc.DOC_COTE,t_doc.DOC_TITRE order by NB desc";
	// Nb Visionnages
	$tStat["Nb Visionnages"]="select to_char(t_action.ACT_DATE,'mm-dd-YYYY') as DATE,count(*) as NB from t_action where t_action.ACT_TYPE='VIS' group by DATE order by DATE desc";
	// Top visionnage
	$tStat["Top visionnages"]="select t_doc.DOC_COTE,t_doc.DOC_TITRE,count(*) as NB from t_action
							 left join t_doc on t_action.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG='".strtoupper($_SESSION["langue"])."'
							 where t_action.ACT_TYPE='VIS' and t_action.ID_DOC!=0 group by t_action.ID_DOC,t_doc.DOC_COTE,t_doc.DOC_TITRE order by NB desc";
	// Fonctions usagers
	$tStat["Usagers"]="select t_fonction_usager.FONCTION_USAGER,t_pays.PAYS,count(distinct t_action.ID_USAGER) as NB from t_action inner join t_usager  ON t_action.ID_USAGER=t_usager.ID_USAGER inner join t_fonction_usager ON t_fonction_usager.ID_FONCTION_USAGER=t_usager.US_ID_FONCTION_USAGER ";
	$tStat["Usagers"].=" left outer join t_pays on t_pays.ID_PAYS=t_usager.US_ID_PAYS";
	$tStat["Usagers"].=" where t_action.ACT_TYPE='IN'";
	$tStat["Usagers"].=" group by t_fonction_usager.FONCTION_USAGER,t_pays.PAYS";
	// MÃ©thodes
	$tStat["Formulaire recherche"]="select t_action.ACT_FORM as FORMULAIRE_RECHERCHE,count(*) as NB from t_action where t_action.ACT_TYPE='SQL' group by FORMULAIRE_RECHERCHE";
?>
	<form name="form1" id="form1" method="post" action="index.php?urlaction=stats">
	    <div align="center">
	      <fieldset style='width:500px;'>
	      <legend><?=kStatistiques?></legend>
	        <table width="80%" border="0" cellpadding="0" cellspacing="0" >
	        <tr>
	            <td class='label_champs_form'><?= kType ?></td>
	            <td class='val_champs_form'>
	                <select name="stat_num">
	                <?php
						// Affichage des valeurs
						foreach($tStat as $index => $val){
	                        print "<option value='$index'";
	                        if($stat_num==$index) print " selected ";
	                        print ">".$index."</option>";
	                    }
	                ?>
	                </select>
	            </td>
	        </tr>
	        <tr>
				<td class='label_champs_form'><?=kDateDebut?></td>
	            <td class='val_champs_form'>
	                <input name="stat_date_deb" id="stat_date_deb" value="<?= isset($stat_date_deb)?$stat_date_deb:'' ?>" onFocus="formatDate(this)" onKeyPress="formatDate(this)" onKeyUp="formatDate(this)" onBlur="checkDate(this)" onChange="checkDate(this)"/>
	            </td>
	        </tr>
	        <tr>
	            <td class='label_champs_form'><?=kDateFin?></td>
	            <td class='val_champs_form'>
	                <input name="stat_date_fin" id="stat_date_fin" value="<?= isset($stat_date_fin)?$stat_date_fin:'' ?>" onFocus="formatDate(this)" onKeyPress="formatDate(this)" onKeyUp="formatDate(this)" onBlur="checkDate(this)" onChange="checkDate(this)"/>
	            </td>
	        </tr>
	        <tr>
	            <td class='label_champs_form'><?=kNoUserAndDoc?></td>
	            <td class='val_champs_form'>
	                <input name="stat_type_usager" id="stat_type_usager" type="checkbox" value="1" <?php print ($stat_type_usager=="1" ? "checked":""); ?> />
	            </td>
	        </tr>
	        <tr>
	            <td>&nbsp;</td>
	            <td>
	                <input name="bOK" id="bChercher" type="submit" value="<?= kChercher ?>"/>
	            </td>
	        </tr>
	        </table>
	        </fieldset>
	    </div>
	</form>