<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/cherche.js"></script>

<div id="block_form_cherche" style="display : none ; ">
<div id='pageTitre'><?= kRecherche?></div>
<? 


// MS - 30.06.15 Une fois arrivé ici, si on n'a pas de recherche en session, c'est qu'on accède à docListe sans paramètres de recherche (on ne vient pas d'un submit quick_search), on redirige donc vers la même url avec precherche=1
if(defined("useSolr") && useSolr){
	$myPage = PageSolr::getInstance();
}else{
	$myPage = Page::getInstance();
}
$myPage->getActionFromURL();
if(isset($myPage->urlaction) && $myPage->urlaction == 'docListe'){
	if(!isset($_SESSION['recherche_DOC_Solr']['sql'])  && !isset($_SESSION['recherche_DOC']['sql'])){
		header("Location: http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&precherche=1");
		exit;
	}
}
	
	

require_once(libDir."webComponents/oraoweb/orao_player.php");?>

<script type="text/javascript" src='<?=libUrl?>/webComponents/oraoweb/slideshow/OW_slideshow.js'></script>
<link type="text/css" href="<?=libUrl?>/webComponents/oraoweb/css/ow-slideshow.css" rel="stylesheet" />	

<?
// MS - 30.06.15 on ne charge plus le formulaire de recherche ici pour l'instant (voir template general.html ... )
?>
</div>

<div id="previewHoverWrapper"><div id="previewContainer"></div></div>
<script type="text/javascript">
toggleAdvForm();
$j(document).ready(function(){
	$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").hover(callHoverPlayer);
	$j("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row)").mouseleave(hideHoverPlayer);
	// MS - 25.03.16 - A cause du drag and drop (et du comportement "cloning" qui fait apparaitre un double de l'élément qu'on drag and drop),
	//	il était possible de déplacer sa souris hors de l'élément déclenchant l'affichage preview sans toutefois déclenché d'evenement mouseleave.
	// ici on rajoute donc un test sur le mousemove permettant eventuellement de corriger cette erreur 
	$j(document).mousemove(function(event){
		// si preview visible
		if($j("#previewHoverWrapper").is(':visible')){
			// et que la cible n'est pas .resultsMos ou 
			if(!$j(event.target).is("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row),#panFrame .resultsMos, #panFrame tr:not(.separator, .resultsHead_row)") 
			&& !$j(event.target).parents("#resultats.mos .resultsMos, #resultats.liste tr:not(.separator, .resultsHead_row),#panFrame .resultsMos, #panFrame tr:not(.separator, .resultsHead_row)").length>0){
				hideHoverPlayer();
			}
		}
	});
});
</script>
