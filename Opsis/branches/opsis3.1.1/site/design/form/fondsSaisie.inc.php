
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/saisie.js"></script>
<script type="text/javascript">

window.onload=function () {	makeDraggable(document.getElementById('chooser'));}

</script>
<!--div id="pageTitre"><?=kFondsSaisie?></div-->
<?php
if (isset($myGroupe))
{
	echo '<div class="error">';
	echo $myGroupe->error_msg;
	echo '</div>';
}
?>

<div class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<div id="pageTitre"><?= kFonds ?>&nbsp;</div>
</div>

<div id='chooser' class="iframechoose" style='position:absolute; display:none;'> </div>

<form name="form1" id="form1" method="post" action="" onsubmit="return chkFormFields(this);">
    <input name="commande" id="commande" type="hidden" value=""/>
    <input name="type" id="type" type="hidden" value=""/>
    <input name="page" id="page" type="hidden" value="" />
    <input name="ligne" id="ligne" type="hidden" value="" />
    <input name="id_fonds" type="hidden" size="50" maxlength="50" value="<?=$myFonds->t_fonds['ID_FONDS'] ?>" />
    <input name="id_lang" type="hidden" size="50" maxlength="50" value="<?=$myFonds->t_fonds['ID_LANG'] ?>" />

	<fieldset name="ident" id="ident" class="ui-widget ui-widget-content ui-corner-all">
		<legend><?=kFondsSaisie?> </legend>
    <table  border="0" >
        <tr>
            <td class='label_champs_form'><?= kFonds ?> : </td>
            <td class='val_champs_form' ><input name="fonds" id='fonds' mandatory="true" type="text" size="20" maxlength="100" value="<?=str_replace('"','\"',$myFonds->t_fonds['FONDS']) ?>"/></td>
        </tr>
        <tr>
            <td class='label_champs_form'><?= kFondsPere ?> :</td>
            <td class='val_champs_form' >
            	<select name="id_fonds_gen" id='id_fonds_gen' class='val_champs_form' >
            	<option value=''>---</option>
            	<?=listOptions("SELECT * FROM t_fonds WHERE ID_LANG=".$db->Quote($_SESSION['langue'])." AND ID_FONDS<>".intval($myFonds->t_fonds['ID_FONDS']),array('ID_FONDS','FONDS'),$myFonds->t_fonds['ID_FONDS_GEN']) ?>
            	</select>
            </td>

        </tr>
        <tr>
        	<td class='label_champs_form'><?=kGenealogie?> :</td>
        	<td class='val_champs_form' ><input name="fonds_col" id='fonds_col' type='text' size="20" maxlength="200" value="<?=str_replace('"','\"',$myFonds->t_fonds['FONDS_COL']) ?>" /></td>
        </tr>
		<tr><td colspan='2'><hr/></td></tr>
		<tr>
			<td class="label_champs_form" >
				<?=kAutresVersions?> :
			</td>
			<td class="val_champs_form" colspan="3">
			<?
			foreach ($myFonds->arrVersions as $idx=>$version) {
				echo strtoupper($version['LANG'])." : ".kFonds."<input type='text' id='version_".$version['ID_LANG']."'
	 			size='30' maxlength='30' name='version[".$version['ID_LANG']."][FONDS]' value='".$version['FONDS']."' />
				".kGenealogie."<input size='30' maxlength='100' name='version[".$version['ID_LANG']."][FONDS_COL]' value='".$version['FONDS_COL']."'/>";
			}
			?>
			</td>
		</tr>

    </table>
	<br />
    <h5><?= kListeGroupes ?></h5>
    <?

           $params['entete']='1';
            $params['tri']=strtoupper(Page::getInstance()->col);
            $params['ordre']=Page::getInstance()->triInvert;
            tabToXSL($myFonds->t_groupe_fonds,"t_fonds",getSiteFile("listeDir","groupeFondsListe.xsl"),$params);

echo "<button class='miniButton ui-state-default ui-corner-all' style='margin:5px auto;'  id='groupe§add' onclick=\"choose(this,'titre_index=".kGroupe."&amp;id_lang=".$_SESSION['langue']."&amp;valeur=&amp;champ=GRP&amp;rtn=addGroupe&amp;filter=".(isset($_filter)?$_filter:'')."');return false;\"><img src='design/images/group_add.png' border='0' alt=\"".kAjouter."\"/>&nbsp;".kAjouter."</button>";

    ?>
		<br/>
		
		</fieldset>
		<div  class="button_fieldset" align="center">
		<button class='actionBouton ui-state-default ui-corner-all' id='bSup' name="bSup" type="button" onclick="removeItem('index.php?urlaction=fondsListe')"><span class="ui-icon ui-icon-trash">&nbsp;</span><?= kSupprimer ?></button>
		<button class='actionBouton ui-state-default ui-corner-all' id='bOk' name="bOk" type="submit" ><span class="ui-icon ui-icon-check">&nbsp;</span><?= kEnregistrer ?></button>
		</div>
    </form>
<a href="index.php?urlaction=fondsListe"><?= kRetour ?></a>
