<?
//-----------------------------------------------------------------------------------*/
//---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
//---                                                                             ---*/
//--- 2004/2005                                                                   ---*/
//--- Developpeurs : Vincent Prost, Francois Duran, Jerome Chauvin, Xavier Sirven ---*/
//-----------------------------------------------------------------------------------*/
//-----------------------------------------------------------------------------------*/
// XS : 26/10/2005 : Ajout de l'onglet Mes recherches
// XS : 16/09/2005 : Ajout du traitement de l'identification dans cette page plutot que dans "loginVerification.php"

require_once(modelDir.'model_panier.php');

?>

<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/panier.js"></script>

<dl class="left-nav">
<dt><?= kAuthentification ?></dt>


</dl>
<dl class="left-nav">
	<dt><?= kRecherche ?></dt>
	<dd class="left-nav"><a href="<?=$link?>?urlaction=docListe&amp;cherche=1"><?= kRechercheSimple ?></a></dd>
	<!--dd class="left-nav"><a href="<?=$link?>?urlaction=cherche&amp;sf=2"><?= kRechercheAvancee ?></a></dd-->
	<dd class="left-nav"><a href="<?=$link?>?urlaction=cherche&amp;sf=3"><?= kRechercheExperte ?></a></dd>
	<dd class="left-nav"><a href="<?=$link?>?urlaction=historique"><?= kHistorique ?></a></dd>
	<? if($myUsr->loggedIn()){ // Loggé ! ?>
		<dd class="left-nav"><a href="<?=$link?>?urlaction=recherche"><?= kMesRecherches ?></a></dd>
	<? } ?>
</dl>

<? if($myUsr->loggedIn()){ // Loggé ! ?>
	
	<dl class="left-nav">
	 	<dt><?=kPanierSelections?></dt>
	 	<dd class="left-panier">
                    <div id='folders'>&nbsp;</div>
                    <script type="text/javascript">refreshFolders();</script>
                </dd>
	</dl>

 	<dl class="left-nav">
		<dt><?=kMonCompte?></dt>
		<dd class="left-nav"><a href="<?=$link?>?urlaction=inscription&amp;id_usager=<?=$myUsr->UserID?>&amp;edit=1"><?= kProfil ?></a></dd>
		<dd class="left-nav"><a href="<?=$link?>?urlaction=panierListe"><?= kMesCommandes ?></a></dd>
	</dl>
	
	<? if($myUsr->getTypeLog() >= kLoggedUsagerAdv) { ?>
	<dl class="left-nav">
		<dt>Actions</dt>
		<!--dd class="left-nav"><a href="javascript:popupDoc('indexPopup.php?urlaction=docSaisie&doc_id_media=V','Main','0')"><?= kCreerNoticeVideo ?></a></dd-->
		<dd class="left-nav"><a href="javascript:popupWindow('indexPopupUpload.php?urlaction=simpleUploadJS&amp;type=batch_mat&amp;media_type=v','Upload_mat','height=500,width=800,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main')"><?= kImportVideo ?></a></dd>
		<!--dd class="left-nav"><a href="javascript:popupUpload('indexPopupUpload.php?urlaction=simpleUploadJava&type=batch_mat','main','mat')"><?= kImportVideo ?></a></dd-->
		<dd class="left-nav"><a href="javascript:popupWindow('indexPopupUpload.php?urlaction=simpleUploadJS&amp;type=batch_mat&amp;media_type=a','Upload_mat','height=500,width=800,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main')"><?= kImportAudio ?></a></dd>
		<dd class="left-nav"><a href="javascript:popupWindow('indexPopupUpload.php?urlaction=simpleUploadJS&amp;type=batch_mat&amp;media_type=p','Upload_mat','height=500,width=800,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main')"><?= kImportImage ?></a></dd>
		<dd class="left-nav"><a href="javascript:popupWindow('indexPopupUpload.php?urlaction=simpleUploadJS&amp;type=batch_mat&amp;media_type=d','Upload_mat','height=500,width=800,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main')"><?= kImportDocument ?></a></dd>
		<dd class="left-nav"><a href="javascript:popupWindow('simple.php?urlaction=jobListe','jobs','height=500,width=800,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main')"><?= kSuiviTraitements ?></a></dd>
		<? if($myUsr->getTypeLog() == kLoggedAdmin || $myUsr->getTypeLog() == kLoggedDoc) { ?>
			<dd class="left-nav"><a href="<?=$link?>?urlaction=admin"><?= kAdministration ?></a></dd>
		<? } ?>
	</dl>
	<? } ?>

<? } else {  // Pas loggé ! ?>

	<script type="text/javascript">refreshFolders();</script>
	<dl class="left-nav">
		<dt><?=kPanier?></dt>
	 	<dd class="left-panier">
                    <div id='folders'>&nbsp;</div>
                    <script type="text/javascript">refreshFolders();</script>
                </dd>
	</dl>
<? } ?>

