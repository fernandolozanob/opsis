<?xml version="1.0" encoding="utf-8"?>
<!-- trashListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<!-- recuperation de parametres PHP -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />

    <xsl:template match='/select'>

<form name="documentSelection" method="post" action="{$scripturl}?urlaction=trashListe{$urlparams}">
	 <input type="hidden" value="{$page}" name="page" />
<br/>
 <table width="95%" border="0" class="resultsMenu" align="center">
    <tr>
        <td align="left" width="33%">
 			<xsl:value-of select="$nb_rows" /> <xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>
        </td>
        <td align="center" width="33%">
 			<xsl:if test="$nb_pages>1">
				<xsl:if test="$page>1"><a href="#" onclick='javascript:document.documentSelection.page.value={$page - 1};document.documentSelection.submit();'><img src="design/images/fleche-gauche.gif" alt="" border="0" width="11" height="12" /></a> </xsl:if>
				<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onChange='javascript:document.documentSelection.page.value=eval(this.value);document.documentSelection.submit();' />
				<xsl:value-of select="concat(' / ',$nb_pages)"/>
				<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:document.documentSelection.page.value={$page + 1};document.documentSelection.submit();'><img src="design/images/fleche-droite.gif" alt="" border="0" width="11" height="12" /></a></xsl:if>
			</xsl:if>
        </td>
        <td align="right" width="33%">

                <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
                <select class='val_champs_form' name="nbLignes" onchange="javascript:document.documentSelection.submit()">
                    <option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
                    <option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
                    <option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
                    <option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
                    <option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
                </select>

		</td>
    </tr>
</table>

        <table width="80%" border="0" cellspacing="0"  align="center" class="tableResults">
            <tr>
                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kNumero;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>
            </tr>
            <xsl:for-each select="trash">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">
                    <td class="resultsCorps"><a href="{$scripturl}?urlaction=trash&amp;id_trash={ID_TRASH}"><xsl:value-of select="ID_TRASH"/></a></td>
                    <td class="resultsCorps"><a href="{$scripturl}?urlaction=trash&amp;id_trash={ID_TRASH}"><xsl:value-of select="TRA_TITRE"/></a></td>
                    <td class="resultsCorps"><a href="{$scripturl}?urlaction=trash&amp;id_trash={ID_TRASH}"><xsl:value-of select="TRA_ENTITE"/></a></td>
                    <td class="resultsCorps"><a href="{$scripturl}?urlaction=trash&amp;id_trash={ID_TRASH}"><xsl:value-of select="TRA_DATE_CREA"/></a></td>
                 </tr>
            </xsl:for-each>
        </table>



     </form>



    </xsl:template>
</xsl:stylesheet>
