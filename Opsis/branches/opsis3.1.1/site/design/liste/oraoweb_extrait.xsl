<?xml version="1.0" encoding="utf-8"?>
<!-- minipanier : utilisé pour afficher le contenu d'un folder en format très réduit (ex: frame de gauche, etc. ) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<xsl:param name="currentIdDoc"/>

<xsl:template match='/select'>
    <form name='form1' id='form1' action='blank.php?urlaction=processPanier&amp;commande=saveExtraits&amp;id_panier={t_panier/ID_PANIER}' method='POST' target='iframeSauve' >
		<input id="id_panier" type="hidden" value="{t_panier/ID_PANIER}" />
		<input id="pan_titre" type="hidden" value="{t_panier/PAN_TITRE}" />
	    <xsl:for-each select="t_panier_doc">
	      	<xsl:if test="PDOC_EXTRAIT='1' and DOC/t_doc/ID_DOC=$currentIdDoc" >
				<div id='row$' class='row_extrait'>
					<div id='handle$' class='ext_handler' onClick='myPanel.selectExtrait(this.parentNode)' >&#160;</div>

					<xsl:variable name="titre" ><xsl:call-template name="internal-dbquote-replace">
                	<xsl:with-param	name="stream" select="PDOC_EXT_TITRE"/>
                	</xsl:call-template>
               		</xsl:variable>
					<input id='ext_titre$' type='text' size='20' name='PDOC_EXT_TITRE[]' class='ext_titre' value="{$titre}" 
					onKeyPress='myPanel.hasChanged(this.parentNode);' onBlur="myPanel.checkLine(this.parentNode);" />
					<input type='hidden' name='ID_LIGNE_PANIER[]' value='{ID_LIGNE_PANIER}' />
					<input id='action$' type='hidden' name='ligne_action[]' value='edit' />
					<input type='hidden' name='ID_DOC[]' value='{$currentIdDoc}' />
					<input id='tcin$' type='text' name='PDOC_EXT_TCIN[]' size='11' maxlength='11' class='ext_TC' 
						value='{PDOC_EXT_TCIN}' onfocus="myPanel.setCurr(this.parentNode);" onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);'/>
					<input id='tcout$' type='text' name='PDOC_EXT_TCOUT[]' size='11' maxlength='11' class='ext_TC' 
						value='{PDOC_EXT_TCOUT}' onfocus="myPanel.setCurr(this.parentNode);" onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);' style='margin-left:0px'/>				
					<input id='duration$' type='text' name='duration' size='8' maxlength='8' readonly='1' class='ext_TC' value='{EXT_DUREE}' style='width:70px;background-color:#ccc;'/>				
					<img id='trash$' src='design/images/button_drop.gif' class='trash' 
						onclick='myPanel.removeExtrait(this.parentNode)' />
				</div>
			</xsl:if>			
	    </xsl:for-each> 
	    <div id='separateur'>&#160;</div>  
    </form>	    
    	
    <div id='row$blank' class='row_extrait' style='display:none;'>
			<div id='handle$' class='ext_handler' onClick="myPanel.selectExtrait(this.parentNode)" >&#160;</div>
			<input id='ext_titre$' type='text' size='20' name='PDOC_EXT_TITRE[]' class='ext_titre' value="" 
			onKeyPress='myPanel.hasChanged(this.parentNode);' onBlur="myPanel.checkLine(this.parentNode);"  />
			<input type='hidden' name='ID_LIGNE_PANIER[]' value='' />
			<input id='action$' type='hidden' name='ligne_action[]' value='edit' />
			<input type='hidden' name='ID_DOC[]' value='{$currentIdDoc}' />
			<input id='tcin$' type='text' name='PDOC_EXT_TCIN[]' size='11' maxlength='11' class='ext_TC' 
				value=""  onfocus="myPanel.setCurr(this.parentNode);" onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);' />
			<input id='tcout$' type='text' name='PDOC_EXT_TCOUT[]' size='11' maxlength='11' class='ext_TC' 
				value=""  onfocus="myPanel.setCurr(this.parentNode);" onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);' style='margin-left:0px'/>				
			<input id='duration$' type='text' name='duration' size='8' maxlength='8' readonly='1' class='ext_TC' style='width:70px;background-color:#ccc;'/>				
			<img id='trash$' src='design/images/button_drop.gif' class='trash' 
				onclick='myPanel.removeExtrait(this.parentNode)' />
	</div>
</xsl:template>

	<xsl:template name="internal-quote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">'</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
		name="internal-quote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
	</xsl:template>

<xsl:template name="internal-dbquote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">"</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>&quot;<xsl:call-template
		name="internal-dbquote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
</xsl:template>


</xsl:stylesheet>