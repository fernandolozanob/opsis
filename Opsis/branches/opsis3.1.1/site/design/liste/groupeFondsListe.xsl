<?xml version="1.0" encoding="utf-8"?>
<!--  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="no"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="entete" />

    <xsl:template match='/select'>
	<xsl:variable name='sens'>
	<xsl:choose>
		<xsl:when test='$ordre=1'>descending</xsl:when>
		<xsl:otherwise>ascending</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

    <xsl:choose>
    <xsl:when test='$entete=1'>
		<xsl:variable name='id_fonds'><xsl:value-of select='t_fonds/ID_FONDS' /></xsl:variable>

        <table width="95%" id='groupeListe' align="center" border="0" cellspacing="0"  class="tableResults">
            <tr>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'GROUPE')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=fondsSaisie&amp;amp;id_fonds={$id_fonds}&amp;amp;tri_t=groupe&amp;amp;tri=groupe{$ordre}"><xsl:processing-instruction name="php">print kGroupe</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                   <xsl:processing-instruction name="php">print kPrivilege;</xsl:processing-instruction>
                </td>
                <td class="resultsHead" width="20px">&#160;</td>
            </tr>
            <xsl:for-each select="t_fonds/GROUPE/t_groupe">
                <xsl:sort select="*[name(.)=$tri]" datatype="text" order="{$sens}"/>
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">
					<xsl:call-template name='row'>
						<xsl:with-param name='id_priv'><xsl:value-of select="../../ID_PRIV" /></xsl:with-param>
					</xsl:call-template>
                </tr>
            </xsl:for-each>
        </table>

	</xsl:when>
	<xsl:otherwise>
            <xsl:for-each select="t_groupe">
				<xsl:call-template name='row'>
					<xsl:with-param name='id_priv' />
				</xsl:call-template>
            </xsl:for-each>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name='row'>
        <xsl:param name="id_priv" />
        <xsl:variable name="id_groupe"><xsl:value-of select="ID_GROUPE"/></xsl:variable>
            <td class="resultsCorps"><input type='hidden' name='t_groupe_fonds[ID_GROUPE][]' id='t_groupe_fonds§{$id_groupe}' value='{$id_groupe}' />
            <a href='index.php?urlaction=groupeSaisie&amp;amp;id_groupe={$id_groupe}'><xsl:value-of select="GROUPE"/></a></td>
            <td class="resultsCorps">
                <select class="resultsCorps" name="t_groupe_fonds[ID_PRIV][]" id="id_ligne_priv{$id_groupe}" >
                    <option>---</option>
                    &lt;?listOptions("select ID_PRIV, PRIV from t_privilege  where ID_LANG='".$_SESSION["langue"]."' order by ID_PRIV",array("ID_PRIV","PRIV"),'<xsl:value-of select='$id_priv' />');?&gt;
                </select>
            </td>
            <td class="resultsCorps">
                <img name="btSupF{$id_groupe}" src="design/images/group_delete.png" style='cursor:pointer' onclick="removeRow(this)" title="&lt;?php echo kSupprimer; ?&gt;" alt="&lt;?php echo kSupprimer; ?&gt;"/>
            </td>
</xsl:template>

</xsl:stylesheet>
