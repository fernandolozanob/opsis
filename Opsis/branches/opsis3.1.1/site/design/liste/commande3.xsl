<?xml version="1.0" encoding="utf-8"?>
<!-- commande2.xsl (Achats) -->
<!-- VP : 12/12/2005 : Création du fichier -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="no"
/> 


<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="imgurl"/>

    <xsl:template name='commande3'>


		<script type="text/javascript">
	
		function refresh() {
			document.form1.target='frameCommande';
			document.form1.action='blank.php?urlaction=iframeCommande';
			document.form1.submit();
			document.form1.target='';
			document.form1.action='<xsl:value-of select="scripturl" />?urlaction=commande';
					
		}
				
		
		function chkFields (myform,redirectToPay,optFields) {
			var emptyFld=0;
			var cntChk, grp;
			
			for (var i=0;i&lt;myform.elements.length;i++) {
				myelmt=myform.elements[i];
				myelmt.className='val_champs_form';
				if(optFields.indexOf(myelmt.name)==-1){
					switch (myelmt.type) {
						case 'text':
						case 'textarea': 
						case 'select-one': 
							myelmt.value=myelmt.value.Trim();
							if  (myelmt.value.Trim()=='') {
								if (myelmt.value&lt;0) myelmt.value='1';
								myelmt.className='errormsg';
								emptyFld++; //on a trouvé un contrôle vide, pas bon !
							}
							else {myelmt.className='val_champs_form';}
						
							break;
						case 'radio' :
							cntChk=0;
							
							grp=myform[myelmt.name];
							for (var v=0;v&lt;grp.length;v++) {
								
								if (grp[v].checked==1) cntChk++;
							}
							if (cntChk==0) {
								myelmt.parentNode.className='errormsg';
								emptyFld++;
							} else
								{
								myelmt.parentNode.className='val_champs_form';
								}		
							break;
							
						case 'checkbox' :

							
							if (myelmt.checked!=1) {
								myelmt.parentNode.className='errormsg';
								emptyFld++;
							} else {
								myelmt.parentNode.className='val_champs_form';
							}
							break;
					}
				}
				
			}
			
			if (emptyFld==0) {
				if (redirectToPay==true) { 
					//document.paybox.submit();// paiement en ligne
					//document.form1.commande.value="PAYBOX";
					//calcTarifs();
				}
				else {
					myform.commande.value='VALID'; // pas de paiement (valid classique)
					myform.submit();
				}
			} else
			{
			alert('<xsl:processing-instruction name="php">print kErreurChampsOblig;</xsl:processing-instruction>');
			}
			
		}		

		function checkNum(obj){
			rangnew=parseInt(obj.value,10);
			if(rangnew == 0) obj.value='1';
		}
	
		</script>
		
	<iframe src='' name='frameCommande' id='frameCommande' frameborder='0' scrolling='no' width='0' height='0' allowtransparency='true'> </iframe>
	<form name='form1' action='{$scripturl}?urlaction=commande' method='POST' >
		<input type='hidden' name='commande' value='' />
		<input type='hidden' name='id_panier' value='{t_panier/ID_PANIER}' />
		<input type='hidden' name='ligne' value='' />
		<input type='hidden' name='ID_PANIER' value='{t_panier/ID_PANIER}' />
         		
		<fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'> 	
		<legend><xsl:processing-instruction name="php">print kInfosCommande;</xsl:processing-instruction></legend> 

		<table width="95%" cellspacing="5" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center">
			<tr>
			<td width="100%" colspan="4"><img src="{$imgurl}pixel.gif" height="1"/></td>
			</tr>	
						
            <tr>
                <td rowspan="2" class="label_champs_form" width="20%"><xsl:processing-instruction name="php">print kTypeCommande;</xsl:processing-instruction></td>
                <td rowspan="2" class="commande_champs_form" align="left">
                	<xsl:value-of select="t_panier/TYPE_COMMANDE"/>
                </td>
                <td colspan="2" class="val_champs_form">
                	<span class="label_champs_form" ><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction> :
                    </span>
                	<xsl:value-of select="t_panier/t_usager/US_NOM"/> <xsl:value-of select="t_panier/t_usager/US_PRENOM"/>
                </td>
             </tr>
			<xsl:choose>
				<xsl:when test="t_panier/t_usager/US_SOCIETE!=''">
				<tr>
				   <td colspan="2" class="val_champs_form">
						<span class="label_champs_form" ><xsl:processing-instruction name="php">print kSociete;</xsl:processing-instruction> :
						</span>
                    <xsl:value-of select="t_panier/t_usager/US_SOCIETE"/>
					</td>
				</tr>
				</xsl:when>
				<xsl:otherwise><tr><td colspan="2"></td></tr></xsl:otherwise>
			</xsl:choose>
            <tr height="1"><td colspan="4" style="padding:0px;margin:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>


				<tr height="25">
					<td class="label_champs_form">
					<xsl:processing-instruction name="php">print kNumeroCommande;</xsl:processing-instruction></td>
					<td class="commande_champs_form" colspan="3">
					<xsl:value-of select="t_panier/ID_PANIER"/>
					</td>
				</tr>



                <tr height="60">
                <td class="val_champs_form" colspan="2">
                <span class="label_champs_form">
                <xsl:processing-instruction name="php">print kAdresseLivraison;</xsl:processing-instruction>
                </span>
                
					<textarea name="PANXML_ADRESSE_LIVRAISON" rows="6" cols="40" style="width:250px"   >
						<xsl:choose>
							<xsl:when test="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON!=''">
								<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="t_panier/t_usager/US_SOC_ADRESSE"/><xsl:text>
</xsl:text> <xsl:value-of select="t_panier/t_usager/US_SOC_ZIP"/>&#160;<xsl:value-of select="t_panier/t_usager/US_SOC_VILLE"/><xsl:text>
</xsl:text><xsl:value-of select="t_panier/t_usager/PAYS_USAGER"/>
							</xsl:otherwise>
						</xsl:choose>
					</textarea>
                </td>
                <td class="val_champs_form" colspan="2">
                <span class="label_champs_form">
                <xsl:processing-instruction name="php">print kAdresseFacturation;</xsl:processing-instruction>
                </span>
				<textarea name="PANXML_ADRESSE_FACTURATION" rows="6" cols="40" style="width:250px"   >
					<xsl:choose>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION!=''">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="t_panier/t_usager/US_SOC_ADRESSE"/><xsl:text>
</xsl:text> <xsl:value-of select="t_panier/t_usager/US_SOC_ZIP"/>&#160;<xsl:value-of select="t_panier/t_usager/US_SOC_VILLE"/><xsl:text>
</xsl:text><xsl:value-of select="t_panier/t_usager/PAYS_USAGER"/>
						</xsl:otherwise>
					</xsl:choose>
				</textarea>
                </td>
             </tr>
             <tr height="20">
                <td class="val_champs_form" colspan="2" valign="top">
	                <span class="label_champs_form">
	                <xsl:processing-instruction name="php">print kConditionExploitation;</xsl:processing-instruction>
	                </span>
					<textarea name="PANXML_CONDITION_EXPLOITATION"  cols="65" rows="3"> <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_CONDITION_EXPLOITATION" /></textarea>
                 </td>
            </tr>

			<tr height="20">
				<td colspan="4" class="small_text" valign="top">
				<xsl:processing-instruction name="php">print kRestrictionsUsageInstitutionnel;</xsl:processing-instruction>
				</td>
			</tr>

			<tr height="20">
				<td class="val_champs_form"  valign="top" colspan="2">
					<span class="label_champs_form">
					<xsl:processing-instruction name="php">print kDateEmpruntFormat;</xsl:processing-instruction></span>
					<input name="PANXML_DATE_EMPRUNT" type="text" size="10" value="{t_panier/PAN_XML/commande/PANXML_DATE_EMPRUNT}"  />
				</td>
				<td class="val_champs_form" valign="top" colspan="2">
					<span class="label_champs_form" >
					<xsl:processing-instruction name="php">print kDateRetourFormat;</xsl:processing-instruction>
					</span>
					<input name="PANXML_DATE_RETOUR" type="text" size="10" value="{t_panier/PAN_XML/commande/PANXML_DATE_RETOUR}"  />
				</td>
			</tr>
			<tr height="20">
				<td colspan="4" class="small_text" valign="top">
				<xsl:processing-instruction name="php">print kRestrictionsPret;</xsl:processing-instruction>
				</td>
			</tr>
 
        </table>		 
		
		</fieldset>

		<fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'> 	
		<legend><xsl:processing-instruction name="php">print kContenuCommande;</xsl:processing-instruction></legend> 
  
		<table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
			<tr>
                <td class="resultsHead" width="50"><xsl:processing-instruction name="php">print kCote;</xsl:processing-instruction></td>
                <td class="resultsHead" width="100"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kFonds;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></td>
                <td class="resultsHead" width="150"><xsl:processing-instruction name="php">print kExtrait;</xsl:processing-instruction></td>
                <td class="resultsHead" width="60"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kSupportLivraison;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kNb;</xsl:processing-instruction></td>
               <!-- 
               <td class="resultsHead"><xsl:processing-instruction name="php">print kVersion;</xsl:processing-instruction></td>
				-->

                <td class="resultsHead"><xsl:processing-instruction name="php">print kPrixHT;</xsl:processing-instruction></td>

                <td class="resultsHead" align="center"><input type="image" name="btSupAll" src="design/images/button_drop_a.gif" onclick="removeAll()" title="&lt;?php echo kSupprimerTout; ?&gt;"/> </td>
            </tr>
            <xsl:for-each select="t_panier_doc">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <xsl:variable name="pdoc_support_liv"><xsl:value-of select="PDOC_SUPPORT_LIV"/></xsl:variable>
                <tr class="resultsCorps" id="{concat('altern',$j mod 2)}">
                    <td class="resultsCorps" width="50">
						<input type="hidden" name="t_panier_doc[][ID_LIGNE_PANIER]" value="{ID_LIGNE_PANIER}" />
						<input type="hidden" name="t_panier_doc[][ID_DOC]" value="{ID_DOC}" />
					<xsl:value-of select="DOC/t_doc/DOC_COTE"/>
					</td>

                    <td class="resultsCorps" width="100">
						<a href="{$scripturl}?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></a>
					</td>
                    <td class="resultsCorps" width="80">
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="DOC/t_doc/DOC_DATE_PROD"/>
						</xsl:call-template>
					</td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/DOC_XML/XML/DOC/FONDS"/></td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/TYPE_DOC"/></td>
                    <td class="resultsCorps" width="150">
						<xsl:if test="PDOC_EXTRAIT='1'">
						<xsl:value-of select="PDOC_EXT_TITRE"/><br/>
						[<xsl:value-of select="PDOC_EXT_TCIN"/> - <xsl:value-of select="PDOC_EXT_TCOUT"/>]
						</xsl:if>
						&#160;
					</td>
                    <td class="resultsCorps" width="60">
						<xsl:choose>
							<xsl:when test="PDOC_EXTRAIT='1'"><xsl:value-of select="EXT_DUREE"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:otherwise>
						</xsl:choose>
						&#160;
					</td>
                    <td class="resultsCorps">
						<xsl:choose>
							<xsl:when test="SUPPORTS/SUPPORTS!=''" >
								<select name="t_panier_doc[][PDOC_SUPPORT_LIV]" onChange='refresh()'  >
									<option></option>
									<xsl:for-each select="SUPPORTS/SUPPORTS">
										<xsl:choose>
											<xsl:when test="VALEUR=$pdoc_support_liv">
												<option value="{VALEUR}" selected="" ><xsl:value-of select="VALEUR" /></option>
											</xsl:when>
											<xsl:otherwise>
												<option value="{VALEUR}" ><xsl:value-of select="VALEUR" /></option>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>

								</select>
							</xsl:when>
							<xsl:otherwise>
							N.A.
							</xsl:otherwise>
						</xsl:choose>
                    </td>
                    <td class="resultsCorps">
						<input name="t_panier_doc[][PDOC_NB_SUP]" type="text" size="3" maxlength="3" onKeyUp='checkNum(this);refresh()' value="{PDOC_NB_SUP}" />
                    </td>
                   <!-- 
                    <td class="resultsCorps">
						<select name="t_panier_doc[][PDOC_VERSION]" onChange='refresh()'>
							<xsl:for-each select="DOC/t_doc/DOC_VAL/XML/VERS">
									<xsl:choose>
										<xsl:when test=".=$pdoc_version" >
											<option value="{.}" selected="true"><xsl:value-of select="."  /></option>
										</xsl:when>
										<xsl:otherwise>
											<option value="{.}"><xsl:value-of select="." /></option>
										</xsl:otherwise>
									</xsl:choose>
							</xsl:for-each>
						</select>
                    </td> -->

					<td class="resultsCorps">
						<div id="pdoc_prix_calc{ID_LIGNE_PANIER}">
	                      <xsl:choose>
		                      <xsl:when test="normalize-space(PDOC_PRIX_CALC)!=''"><xsl:value-of select="PDOC_PRIX_CALC"/> €</xsl:when>
		                      <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
	                      </xsl:choose>
					   </div>
						<input type="hidden" name="t_panier_doc[][PDOC_PRIX_CALC]" id="pdoc_prix_calc{ID_LIGNE_PANIER}" value="{PDOC_PRIX_CALC}" />
					</td>

					<td class="resultsCorps" align="center">
						<img name="btSup{$j}" style="cursor:pointer" src="design/images/button_drop.gif" onclick="removeLine({ID_LIGNE_PANIER})" title="&lt;?php echo kSupprimer; ?&gt;"/>
					</td>
                </tr>
            </xsl:for-each>
		</table>
  
		 <xsl:variable name="total_ht"><xsl:value-of select="t_panier/TOTAL_HT" /></xsl:variable>
		 <xsl:variable name="frais_ht"><xsl:value-of select="t_panier/FRAIS_HT" /></xsl:variable>
		 <xsl:variable name="total_ttc"><xsl:value-of select="t_panier/TOTAL_TTC" /></xsl:variable>
		 <input type="hidden" name="PAN_TVA" value="{t_panier/PAN_TVA}" />

			 <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
			   <!--
			   <tr class="resultsCorps" id="altern0">
			     <td align="right" class="resultsCorps" style="font-size:13px">
			     	<xsl:processing-instruction name="php">print kPanierTotalProduits;</xsl:processing-instruction>
			     </td>
			     <td  align="right" class="resultsCorps" style="font-size:13px">
			       	<div id="total_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/TOTAL_HT" /> €</xsl:if> 
				</div>
			     </td>
			     <td  class="resultsCorps">   </td>
			   </tr>
			   
			   <tr class="resultsCorps" id="altern1">
			   	<td align="right" class="resultsCorps">
			     	<xsl:processing-instruction name="php">print kPanierFrais;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsCorps">
			       <div id="frais_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/FRAIS_HT" /> €</xsl:if> 
				</div>
			     </td>
			     <td  class="resultsCorps">   </td>
			   </tr>
			   -->

			   <tr class="resultsHead" id="altern1">
			     <td align="right" class="resultsHead">
			     	<xsl:processing-instruction name="php">print kPanierTotalHT;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsHead">
			        <div id="total_frais_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="$total_ht+$frais_ht" /> €</xsl:if> 
				</div>
			     </td>
			      <td class="resultsHead"></td>
			   </tr>
			   <tr class="resultsHead" id="altern1">
				 <td align="right" class="resultsHead">
					<xsl:processing-instruction name="php">print kTotalTVA;</xsl:processing-instruction>
				 </td>
				 <td align="right" class="resultsHead">
					<div id="total_tva">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="format-number($total_ttc - $total_ht - $frais_ht, '0.00')" /> €</xsl:if> 
				</div>
				 </td>
				  <td class="resultsHead">   </td>
			   </tr>

			   <tr class="resultsHead" id="altern1">
				 <td align="right" class="resultsHead">
					<xsl:processing-instruction name="php">print kPanierTotalTTC;</xsl:processing-instruction>
				 </td>
				 <td align="right" class="resultsHead">
					<div id="total_ttc">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/TOTAL_TTC" /> €</xsl:if> 
				</div>
				 </td>
				  <td class="resultsHead">   </td>
			   </tr>
			 </table> 
  		</fieldset>
	
	     <br/>
	     
	     <div align="center"> 
                <table width="95%" border="0" cellpadding="0" cellspacing="0">
                  <tr align="left" valign="top"> 
						<td width="33%" align="center" class="resultsBar">
							<a href="#" onclick="chkFields(document.form1,false,'')"><xsl:processing-instruction name="php">print kValider;</xsl:processing-instruction></a>
						</td>
                 </tr>
                </table>
            </div>

	
		</form>

    </xsl:template>
    
    <xsl:template name="break">
	 <xsl:param name="text" select="."/>
	 <xsl:choose>
	   <xsl:when test="contains($text, '&#xA;')">
	     <xsl:value-of select="substring-before($text, '&#xA;')"/>
	     <br/>
	     <xsl:call-template name="break">
	       <xsl:with-param name="text" select="substring-after($text,'&#xA;')"/>
	     </xsl:call-template>
	   </xsl:when>
	   <xsl:otherwise>
	           <xsl:value-of select="$text"/>
	   </xsl:otherwise>
	 </xsl:choose>
	</xsl:template>

    
</xsl:stylesheet>

