<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 


<xsl:param name="playerVideo"/>
<xsl:param name="imgurl"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />


    <xsl:template name='commandeRecap1'>
		
		
		<div id='pageTitre' ><xsl:processing-instruction name="php">print kCommandeRecap;</xsl:processing-instruction></div>

        <table width='95%' border='0' align="center" class="resultsMenu">
            <tr>
                <td align="right" width="33%">
					<a href="javascript:popupPrint('export.php?impression=1&amp;export=commandeImp1&amp;id={t_panier/ID_PANIER}&amp;type=panier&amp;tri={$tri}','main')">
					<xsl:processing-instruction name="php">print kImprimer;</xsl:processing-instruction></a>
				</td>
			</tr>
		</table>
		
	    <fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'> 	
    	  <legend>Infos commande</legend> 

          <table width="95%" cellspacing="5" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center">
            <tr>
                <td class="label_champs_form" width="20%"><xsl:processing-instruction name="php">print kTypeCommande;</xsl:processing-instruction></td>
                <td class="commande_champs_form" align="left">
                	<xsl:value-of select="t_panier/TYPE_COMMANDE"/>
                </td>
				
                <td class="label_champs_form"><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction> :</td>
                <td class="val_champs_form">
                	<xsl:value-of select="t_panier/t_usager/US_NOM"/> <xsl:value-of select="t_panier/t_usager/US_PRENOM"/>
                </td>
             </tr>
			 
             <tr>
                <td class="label_champs_form" width="20%"><xsl:processing-instruction name="php">print kDateCommande;</xsl:processing-instruction></td>
                <td class="val_champs_form" align="left">
					<xsl:call-template name="format_date">
						<xsl:with-param name="chaine_date" select="t_panier/PAN_DATE_COM"/>
					</xsl:call-template>
                </td>
				
				<xsl:if test="t_panier/t_usager/US_SOCIETE!=''">
					<td class="label_champs_form"><xsl:processing-instruction name="php">print kSociete;</xsl:processing-instruction> :</td>
					<td class="val_champs_form">
						<xsl:value-of select="t_panier/t_usager/US_SOCIETE"/>
					</td>
				</xsl:if>
            </tr>
			
            <tr height="1"><td colspan="4" style="padding:0px;spacing:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>
			
			<tr height="25">
				<td class="label_champs_form"><xsl:processing-instruction name="php">print kNumeroCommande;</xsl:processing-instruction></td>
				<td class="val_champs_form" colspan="3">
					<xsl:value-of select="t_panier/ID_PANIER"/>
				</td>
			</tr>
			<xsl:if test="t_panier/PAN_ID_TRANS!=''">
				<tr height="25">
					<td class="label_champs_form"><xsl:processing-instruction name="php">print kNumeroTransaction;</xsl:processing-instruction></td>
					<td class="val_champs_form" colspan="3">
						<xsl:value-of select="t_panier/PAN_ID_TRANS" />
					</td>
				</tr>
			</xsl:if>
			
			<tr height="60">
				<xsl:if test="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kAdresseLivraison;</xsl:processing-instruction></td>
					<td class="val_champs_form" valign="top">
						<xsl:call-template name="break" >
						   <xsl:with-param name="text">
								<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON"/>
						   </xsl:with-param>
						 </xsl:call-template>
					</td>
				</xsl:if>
					
				<xsl:if test="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kAdresseFacturation;</xsl:processing-instruction></td>
					<td class="val_champs_form" valign="top">
						<xsl:call-template name="break" >
						   <xsl:with-param name="text">
								<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION"/>
						   </xsl:with-param>
						 </xsl:call-template>
					</td>
				</xsl:if>
			</tr>
			 
			<tr height="20">
				<xsl:if test="t_panier/PAN_XML/commande/PANXML_TVA_INTRA!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kNumeroTVA;</xsl:processing-instruction></td>
					<td class="val_champs_form">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_TVA_INTRA"/>
					 </td>
				</xsl:if>

				<xsl:if test="t_panier/PAN_XML/commande/PANXML_DESTINATION!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kDestination;</xsl:processing-instruction></td>
					<td class="val_champs_form">
						<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_DESTINATION"/>
					</td>
				</xsl:if>
            </tr>

			<xsl:if test="t_panier/PAN_XML/commande/PANXML_CONDITION_EXPLOITATION!=''">
             <tr height="20">
                <td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kConditionExploitation;</xsl:processing-instruction></td>
                <td class="val_champs_form">
					<xsl:call-template name="break" >
					   <xsl:with-param name="text">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_CONDITION_EXPLOITATION"/>
					   </xsl:with-param>
					 </xsl:call-template>
                </td>
             </tr>
			</xsl:if>


            <tr height="1"><td colspan="4" style="padding:0px;spacing:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>
			<xsl:if test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT!=''">
            <tr>
                <td class="label_champs_form"><xsl:processing-instruction name="php">print kModePaiement;</xsl:processing-instruction></td>
                <td class="val_champs_form" colspan="3">
					<xsl:choose>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CHEQUE'" >
							<xsl:processing-instruction name="php">echo kModePaiementCheque</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CR'" >
							<xsl:processing-instruction name="php">echo kModePaiementCR</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CB'" >
							<xsl:processing-instruction name="php">echo kModePaiementCB</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='VISA'" >
							<xsl:processing-instruction name="php">echo kModePaiementVISA</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='EUROCARD_MASTERCARD'" >
							<xsl:processing-instruction name="php">echo kModePaiementEUROCARD_MASTERCARD</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='E_CARD'" >
							<xsl:processing-instruction name="php">echo kModePaiementE_CARD</xsl:processing-instruction>
						</xsl:when>
					</xsl:choose>
				</td>
               </tr>
			</xsl:if>

        </table>
  		</fieldset>

	  <fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'> 	
	  <legend>Contenu de la commande</legend> 
  		
        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
            <tr>
                <td class="resultsHead" width="50"><xsl:processing-instruction name="php">print kCote;</xsl:processing-instruction></td>
                <td class="resultsHead" width="100"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kFonds;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></td>
                <td class="resultsHead" width="150"><xsl:processing-instruction name="php">print kExtrait;</xsl:processing-instruction></td>
                <td class="resultsHead" width="60"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kSupportLivraison;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kNbExemplaires;</xsl:processing-instruction></td>
				<td class="resultsHead"><xsl:processing-instruction name="php">print kVersion;</xsl:processing-instruction></td>
				<td class="resultsHead"><xsl:processing-instruction name="php">print kPrixHT;</xsl:processing-instruction></td>
                       
            </tr>
            <xsl:for-each select="t_panier_doc">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="resultsCorps" id="{concat('altern'$j mod 2)}">
                    <td class="resultsCorps" width="50"><xsl:value-of select="DOC/t_doc/DOC_COTE"/></td>
                    <td class="resultsCorps" width="100"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></td>
                    <td class="resultsCorps" width="80">
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="DOC/t_doc/DOC_DATE_PROD"/>
						</xsl:call-template>
					</td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/DOC_XML/XML/DOC/FONDS"/></td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/TYPE_DOC"/></td>
                    <td class="resultsCorps" width="150">
						<xsl:if test="PDOC_EXTRAIT='1'">
						<xsl:value-of select="PDOC_EXT_TITRE"/><br/>
						[<xsl:value-of select="PDOC_EXT_TCIN"/> - <xsl:value-of select="PDOC_EXT_TCOUT"/>]
						</xsl:if>
						&#160;
					</td>
                    <td class="resultsCorps" width="60">
						<xsl:choose>
							<xsl:when test="PDOC_EXTRAIT='1'"><xsl:value-of select="EXT_DUREE"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:otherwise>
						</xsl:choose>
						&#160;
					</td>
               
                    <td class="resultsCorps"> 
	                           <xsl:value-of select="pdoc_support_liv"/>             
                    </td>
                   
                    <td class="resultsCorps">
							   <xsl:value-of select="pdoc_nb_sup"/>
                    </td>
                 
                    <td class="resultsCorps">
							<xsl:value-of select="pdoc_version"/>
                    </td>
	        
	                 <td class="resultsCorps" id="pdoc_prix_calc{id_ligne_panier}">
							<xsl:value-of select="pdoc_prix_calc"/>&#160;€
	                 </td>	        

          
                </tr>
            </xsl:for-each>
		</table>
  	</fieldset>
  
  
		<xsl:variable name="total_ht"><xsl:value-of select="t_panier/TOTAL_HT" /></xsl:variable>
		<xsl:variable name="frais_ht"><xsl:value-of select="t_panier/FRAIS_HT" /></xsl:variable>
		<xsl:variable name="tva"><xsl:value-of select="t_panier/TVA" /></xsl:variable>
		
			 <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
			   <tr class="resultsCorps" >
			     <td align="right" class="resultsCorps" style="font-size:13px" >
			     	<xsl:processing-instruction name="php">print kPanierTotalProduits;</xsl:processing-instruction>
			     </td>
			     <td  align="right" class="resultsCorps" style="font-size:13px" id="total_ht">
			       	<xsl:choose>
			       		<xsl:when test="t_panier/TOTAL_HT!=''" ><xsl:value-of select="t_panier/TOTAL_HT" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			     <td  class="resultsCorps"> &#160; </td>
			   </tr>
			   <tr class="resultsCorps" >
			   	<td align="right" class="resultsCorps">
			     	<xsl:processing-instruction name="php">print kPanierFrais;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsCorps" id="frais_ht">
			     	<xsl:choose>
			       		<xsl:when test="t_panier/FRAIS_HT!=0" ><xsl:value-of select="t_panier/FRAIS_HT" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			     <td  class="resultsCorps"> &#160; </td>
			   </tr>

			   <tr class="resultsHead" >
			     <td align="right" class="resultsHead">
			     	<xsl:processing-instruction name="php">print kPanierTotalHT;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsHead" id="total_frais_ht">
					<xsl:choose>
			       		<xsl:when test="t_panier/TOTAL_FRAIS_HT!=0" ><xsl:value-of select="t_panier/TOTAL_FRAIS_HT" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			      <td class="resultsHead"> &#160; </td>
			   </tr>

			   <tr class="resultsHead" >
			     <td align="right" class="resultsHead">
			     	<xsl:processing-instruction name="php">print kPanierTotalTTC;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsHead" id="total_ttc">
					<xsl:choose>
			       		<xsl:when test="t_panier/TOTAL_TTC!=0" ><xsl:value-of select="t_panier/TOTAL_TTC" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			      <td class="resultsHead"> &#160; </td>
			   </tr>		   
			 </table> 

	
	     <br/>
	     
	     <div align="center"> 
                <table width="95%" border="0" cellpadding="0" cellspacing="0">
                  <tr align="left" valign="top"> 
					
					<td width="33%" align="center" class="resultsBar">
								<a href="javascript:popupPrint('export.php?impression=1&amp;export=commandeImp1&amp;id={t_panier/ID_PANIER}&amp;type=panier&amp;tri={$tri}','main')">
													<xsl:processing-instruction name="php">print kImprimer;</xsl:processing-instruction></a>
					</td>
					
					<td width="33%" align="center" class="resultsBar"><a href="{$scripturl}"><xsl:processing-instruction name="php">print kRetour;</xsl:processing-instruction></a></td>
							
                   
                 </tr>
                </table>
            </div>
 
	<br/>
	

    </xsl:template>
    
    
</xsl:stylesheet>

