<?xml version="1.0" encoding="utf-8"?>
<!-- minipanier : utilisé pour afficher le contenu d'un folder en format très réduit (ex: frame de gauche, etc. ) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<xsl:param name="currentIdDoc"/>

<xsl:param name="id_doc"/>
<xsl:param name="id_mat"/>
<xsl:param name="scripturl"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="storyboardChemin" />
<xsl:param name="etat"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="imgurl" />
<xsl:param name="id_lang" />
<xsl:param name="defaultTitle" />




<xsl:template match='/select'>
<xsl:variable name="id_mat">
	<xsl:if test="t_imageur[ID_IMAGEUR!='']/ID_MAT">
		<xsl:value-of select="t_imageur[ID_IMAGEUR!='']/ID_MAT"/>
	</xsl:if>
	<xsl:if test="not(t_imageur[ID_IMAGEUR!='']/ID_MAT)">
		<xsl:value-of select="t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_TYPE='VISIO'][position()=1]/ID_MAT"/>
	</xsl:if>
</xsl:variable>

<xsl:variable name="mat_nom">
	<xsl:if test="t_imageur[ID_IMAGEUR!='']/MAT_NOM">
		<xsl:value-of select="t_imageur[ID_IMAGEUR!='']/MAT_NOM"/>
	</xsl:if>
	<xsl:if test="not(t_imageur[ID_IMAGEUR!='']/MAT_NOM)">
		<xsl:value-of select="concat(t_imageur[ID_IMAGEUR!='']/MAT_NOM,t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_TYPE='VISIO' and MAT_ID_MEDIA='V'][position()=1]/MAT_NOM)"/>
	</xsl:if>		
</xsl:variable>
<xsl:variable name="imageur"><xsl:value-of select="t_imageur[ID_IMAGEUR!='']/IMAGEUR"/></xsl:variable>
<xsl:variable name="id"><xsl:value-of select="t_imageur[ID_IMAGEUR!='']/ID_IMAGEUR"/></xsl:variable>

		<div id='planche' style='position:relative;'>
    <form name='form2' id='form2' action='blank.php?urlaction=processStoryboard&amp;id={$id}' method='POST' target='iframeSauve' >
            <input name="page" type="hidden" value="" />
			<input name="vignette" type="hidden" value="{t_doc/DOC_ID_IMAGE}" /> <!-- par deft -->
			<input name="id_doc" type="hidden" value="{t_doc/ID_DOC}" />
			<input id="imageur" name="imageur" type="hidden" value="{$imageur}" />
			<input id="id" name="id" type="hidden" value="{$id}" />
			<input id="id_mat" name="id_mat" type="hidden" value="{$id_mat}" />
			<input name="sb_number" type="hidden" value="" />
			<input name="story_mode" type="hidden" value="" />
			<input name="commande" type="hidden" value="saveStoryboard"	/>
   
   
    <table width="100%" align="center" border="0" cellspacing="0" style='font-size:10px'>
				<tr>
				<td colspan='2' nowrap="true">
					<xsl:processing-instruction name="php">print kMedia;</xsl:processing-instruction> : <xsl:value-of select="$mat_nom" /><br/>
					<xsl:processing-instruction name="php">print kStoryboard;</xsl:processing-instruction> : <xsl:value-of select="$imageur" />
				
				</td>
				<td align='right'>
				<!--	
				<xsl:processing-instruction name="php">print kVersion;</xsl:processing-instruction>
				<select name='id_lang' class='val_champs_form' onchange='myPanel.content_id_lang=this.value;myPanel.refreshContent();'>
					<xsl:for-each select='t_langues'>
					    <xsl:choose>
					    	<xsl:when test=" . = $id_lang">
								<option value='{.}' selected='true'><xsl:value-of select="." /></option>
							</xsl:when>
							<xsl:otherwise>
								<option value='{.}'><xsl:value-of select="." /></option>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:for-each>
				</select>
				-->
			</td></tr>
            <tr style='background-color:#e8e8e8'>
               <td align="left" width="25%">
					<xsl:value-of select="$nb_rows" />&#160;<xsl:processing-instruction name="php">print kImages;</xsl:processing-instruction>
                </td>
 				<!-- Changement de page, ATTENTION, car fait en repostant le formulaire (pas pas href !) -->
                <td align='center' width='25%'>
		 			<xsl:if test="$nb_pages>1">
						<xsl:if test="$page>1"><a href="javascript:void(null)" onclick='javascript:myPanel.page={$page - 1};myPanel.refreshContent();'>&lt;</a>&#160;</xsl:if>
						<input type='text' style='padding:0px;margin:0px;width:25px;' name='rang' value='{$page}' size='3' class="resultsInput" onchange='javascript:myPanel.page=eval(this.value);myPanel.refreshContent();' />
						<input type='submit' style='width:0px;height:0px;display:none;' />
						<xsl:value-of select="concat(' / ',$nb_pages)"/>
						<xsl:if test="$page &lt; $nb_pages">&#160;<a href="javascript:void(null)" onclick='javascript:myPanel.page={$page + 1};myPanel.refreshContent();'>&gt;</a></xsl:if>
					</xsl:if>
                </td>
		<td align="right" >


        <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
        <select name='nbLignes' class='val_champs_form' style='padding:0px;margin:0px;' onchange='javascript:myPanel.nbLignes=this.value;myPanel.refreshContent()'>
            <option value='9'><xsl:if test="$nbLigne=9"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>9</option>
            <option value='12'><xsl:if test="$nbLigne=12"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>12</option>
            <option value='24'><xsl:if test="$nbLigne=24"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>24</option>
            <option value='45'><xsl:if test="$nbLigne=45"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>45</option>
            <option value='all'><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
        </select>
			</td>
		</tr>
		</table>
		<!-- fin sélection / début images -->
		<hr width='100%'/>

	
		
        <xsl:for-each select="t_image">

            <xsl:variable name="imgPath2">
            	<xsl:call-template name="internal-quote-replace" >
            		<xsl:with-param name="stream" select="concat($storyboardChemin,IM_CHEMIN,'/',IM_FICHIER)" />
            	</xsl:call-template>
            </xsl:variable>
            <xsl:variable name="imgPath">
				<xsl:value-of  select="concat($storyboardChemin,IM_CHEMIN,'/',IM_FICHIER)" />
            </xsl:variable>
            <xsl:variable name="j"><xsl:number/></xsl:variable>
            <xsl:variable name="className"> <!-- Test pour voir si cette image est la vignette. Uniquement pour DOC -->
            	<xsl:choose>
            		<xsl:when test='SELECTED'>imgSelected</xsl:when>
            		<xsl:otherwise>imgNormal</xsl:otherwise>
            	</xsl:choose>
            </xsl:variable>
			<div id='row$' class='{$className}'>
				<!-- Checkbox de sélection pour le delete -->
				<!-- <label style='width:130px;font-family:"Trebuchet MS";font-weight:normal;font-size:12px;color:#333333;'>
				<input type="checkbox" name="checked[{ID_IMAGE}]" style='width:12px;' />
				#<xsl:value-of select="ID_IMAGE" /></label>-->
				
				<input id='action$' type='hidden' name='ligne_action[]' value='edit' />
	
				<xsl:if test="../t_doc/ID_DOC">
					<img src="design/images/select_vignette.gif" id='setVignette$' style="cursor:pointer;" onclick="setAsVignette({ID_IMAGE},this.parentNode);"  alt="&lt;?=kImageurSelectVignette?&gt;" titre="&lt;?=kImageurSelectVignette?&gt;" />
				</xsl:if>
				<input id='tcin$' type="text" size="11" maxlength="11" onKeyPress="formatTC(this);myPanel.hasChanged(this.parentNode);" onKeyUp="formatTC(this);myPanel.hasChanged(this.parentNode);"  
				style="text-align:center;background-color:#000000;color:#FFFFFF;font-family:arial; margin:3px;" name="IM_TC[]" value="{IM_TC}" />

				<!-- Delete de l'image -->
				<img id='trash$' src='design/images/button_drop.gif' class='trash' 
				onclick='myPanel.removeExtrait(this.parentNode)' alt="trash" />
				
				<!--img src="{$imgPath}" width="160" id='img$'  style="cursor:pointer" onclick="javascript:popupImage('{$imgPath2}',encodeURIComponent('{IM_TEXT}'))" /-->
				<img src="{$imgPath}" width="160" id='img$'  style="cursor:pointer;" onclick="if(!myPanel.video) loadVideo();myPanel.positionCursor('{IM_TC}')" />
				<!-- Formulaire pour cette image -->
				<br/>
				<xsl:variable name="text">
				<xsl:choose><xsl:when test="IM_TEXT =''"><xsl:value-of select="$defaultTitle"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="IM_TEXT" /></xsl:otherwise></xsl:choose>
				</xsl:variable>
				<input type="hidden" id='ext_titre$' style='font-size:9px;width:125px;color:#666666;padding:0px;background-color:#f5f5f5' 
					onKeyPress="myPanel.hasChanged(this.parentNode);" onKeyUp="myPanel.hasChanged(this.parentNode);"  name="IM_TEXT[]" value="{$text}" />
				<input type="hidden" name="ID_IMAGE[]" value="{ID_IMAGE}" />
				<input type="hidden" name="IM_CHEMIN[]" value="{IM_CHEMIN}" />
				<input type="hidden" name="IM_FICHIER[]" value="{IM_FICHIER}" />
				<input type="hidden" name="ID_LANG[]" value="{ID_LANG}" />
           </div>


        </xsl:for-each>


		<table width="100%" border="0" class="resultsMenu">
			<tr>
				<xsl:if test="t_imageur/ID_MAT or contains($mat_nom,'.mp4')" >
					<td align="center" class="storyboard_generation_options">
						<!--input name="bGenerer2" class="ui-state-default ui-corner-all" type="button" onmouseover="$j(this).addClass('ui-state-hover');" onmouseleave="$j(this).removeClass('ui-state-hover');" onclick="generateMosaic()" value="&lt;?=kGenererMosaique;?&gt;" /-->
						<input name="bGenerer" class="ui-state-default ui-corner-all" type="button" onmouseover="$j(this).addClass('ui-state-hover');" onmouseleave="$j(this).removeClass('ui-state-hover');" onclick="generateStoryboard()" value="&lt;?=kGenererStoryboard;?&gt;" />&#160;
						<input type="radio" checked="checked" name="story_cut_mode" value="rate"/>
						<span><xsl:processing-instruction name="php">print kIntervalle;</xsl:processing-instruction>
						<input name="sb_rate" id="sb_rate" type="text" value='&lt;?=gStoryboardIntervalleDefaut?&gt;' size='3' maxlength='4' />&#160;(sec/img)</span>
						&lt;? // pas de détection de plans si on utilise pas ffmbc pour la génération storyboards 
						if(strpos(kUtilStoryboardPath,"ffmbc")>0 ){ ?&gt;
							<input type="radio" name="story_cut_mode" value="cutdetection"/>
							<span><xsl:processing-instruction name="php">print kStoryDetectionPlan;</xsl:processing-instruction></span>
						&lt;? } ?&gt;
					</td>
				</xsl:if>
				
			</tr>
		</table>
	</form>
	
</div>
	<div id='row$blank' style='display:none;' class='imgNormal'>
				
		<input id='action$' type='hidden' name='ligne_action[]' value='edit' />
		<!-- Timecode de l'image -->
		

		<xsl:if test="t_doc/ID_DOC">
			<img id='setVignette$' src="design/images/select_vignette.gif" style="cursor:pointer" alt="&lt;?=kImageurSelectVignette?&gt;" titre="&lt;?=kImageurSelectVignette?&gt;" />
		</xsl:if>

		<input id='tcin$' type="text" size="11" maxlength="11" onKeyPress="formatTC(this);myPanel.hasChanged(this.parentNode);" 
			onKeyUp="formatTC(this);myPanel.hasChanged(this.parentNode);" 
			style='text-align:center;background-color:#000000;color:#FFFFFF;font-family:arial; margin:3px;'
			name="IM_TC[]" value="" />
		
		<!-- Delete de l'image -->
		<img id='trash$' src='design/images/button_drop.gif'  class='trash' 
		onclick='myPanel.removeExtrait(this.parentNode)' alt="trash" />
		
		<img src="design/images/wait30trans.gif" id='img$' style="cursor:pointer" />
		<!-- Formulaire pour cette image -->
		<br/>
		<input type="hidden" name="ID_IMAGE[]" value="" />
		<input type="hidden" name="IM_CHEMIN[]" value="" />
		<input type="hidden" name="IM_FICHIER[]" value="" />
		<input type="hidden" name="ID_LANG[]" value="" />
    </div>


	
	
</xsl:template>

	<xsl:template name="internal-quote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">'</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
		name="internal-quote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
	</xsl:template>

<xsl:template name="internal-dbquote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">"</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>&quot;<xsl:call-template
		name="internal-dbquote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
</xsl:template>





</xsl:stylesheet>