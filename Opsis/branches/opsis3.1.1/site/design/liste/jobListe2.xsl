<?xml version="1.0" encoding="utf-8"?>
<!-- jobListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="scripturl" />
<xsl:param name="ordre" />
<xsl:param name="tri" />
<xsl:param name="gestPers"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />
<xsl:param name="nb_pages" />
<xsl:param name="jobFini" />
<xsl:param name="jobEnCours" />

    <xsl:template match='/select'>
		<script type="text/javascript">
			$j(document).ready(function () {
				setTimeout("updateListe();",2000);
			});
			
			var arr_JobOpen = new Array();
			function updateListe() {
				$j.post("empty.php?urlaction=updateJobListe",
					{display: "&lt;? echo $_GET['display']; ?&gt;",
						job_aff: "2",
						scripturl: "<xsl:value-of select="$scripturl" />",
						arr_JobOpen: arr_JobOpen.join(',')},
					function (data) {
						updateListTab(data);
						updateListResults(data);
						updateListPage(data);
						
						var js = data.substr(data.lastIndexOf('&lt;script&gt;') + 8);
						js = js.substr(0, js.lastIndexOf('&lt;/'));
						
						data = data.substr(data.lastIndexOf('&lt;table width="80%" align="center" border="0" cellspacing="0" class="tableResults"'));
						data = data.substr(data.indexOf('&lt;tr&gt;'));
						data = data.substr(0, data.lastIndexOf('&lt;/tr&gt;') + 5);
						$j("form[name='form1'] table.tableResults").html(data);
						
						eval(js);
						
						setTimeout("updateListe();",5000);
					}
				);
			}
			
			function updateListTab(data) {
				var nb_rows = data.substr(data.indexOf('&lt;nb_rows&gt;') + 9);
				nb_rows = nb_rows.substr(0, nb_rows.indexOf('&lt;/nb_rows&gt;'));
				var tabhtml = $j("li.ui-tabs-selected a").html();
				tabhtml = tabhtml.substr(0, tabhtml.indexOf("(") + 1) + nb_rows + tabhtml.substr(tabhtml.indexOf(")"));
				$j("li.ui-tabs-selected a").html(tabhtml);
			}
			
			function updateListResults(data) {
				var nb_rows = data.substr(data.indexOf('&lt;nb_rows&gt;') + 9);
				nb_rows = nb_rows.substr(0, nb_rows.indexOf('&lt;/nb_rows&gt;'));
				$j("table.resultsMenu td[align='left']").html(nb_rows + "&amp;nbsp;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>");
			}
			
			function updateListPage(data) {
				var html = data.substr(data.lastIndexOf('&lt;td align="center" width="33%"&gt;') + 31);
				html = html.substr(0, html.indexOf('&lt;/td&gt;'));
				if (html.indexOf('input type="text" name="rang"') > 0)
					$j("table.resultsMenu td[align='center']").html(html);
			}
			
			var openSubJob = new Array();
			function displayChildren(idGen, elem) {
				$j("tr.jobGen"+idGen).toggle();
				if (elem.src.indexOf('plus.gif') >= 0) {
					elem.src = "design/images/moins.gif";
					arr_JobOpen[idGen] = 1;
				}
				else {
					elem.src = "design/images/plus.gif";
					delete arr_JobOpen[idGen];
				}
				
				$j("tr.jobGen"+idGen+" img.toggleButton").each(function (i) {
					var idJob = $j(this).attr("idJob");
					if (openSubJob[idJob] == 1 &amp;&amp; this.src.indexOf('plus.gif') &gt; 0) {
						$j(this).click();
						delete openSubJob[idJob];
					} else if (this.src.indexOf('moins.gif') &gt; 0) {
						$j(this).click();
						openSubJob[idJob] = 1;
					}
				});
			}
		</script>
		<form name="form1" method="post" action="simple.php?urlaction=jobListe{$urlparams}">
			<input name="commande" id="commande" type="hidden" value="" />
			<input name="ligne" value="" type="hidden" />
			<input type="hidden" value="{$page}" name="page" />
			
			 <table width="95%" border="0" class="resultsMenu" align="center">
				<tr>
					<td align="left" width="33%">
						<xsl:value-of select="$nb_rows" /> <xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>
					</td>
					<td align="center" width="33%">
						<xsl:if test="$nb_pages>1">
							<xsl:if test="$page>1"><a href="#" onclick='javascript:document.form1.page.value={$page - 1};document.form1.submit();'><img src="design/images/fleche-gauche.gif" alt="" border="0" width="11" height="12" /></a> </xsl:if>
							<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onChange='javascript:document.form1.page.value=eval(this.value);document.form1.submit();' />
							<xsl:value-of select="concat(' / ',$nb_pages)"/>
							<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:document.form1.page.value={$page + 1};document.form1.submit();'><img src="design/images/fleche-droite.gif" alt="" border="0" width="11" height="12" /></a></xsl:if>
						</xsl:if>
					</td>
					<td align="right" colspan="3">

							<xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
							<select class='val_champs_form' name="nbLignes" onchange="javascript:document.form1.submit()">
								<option value="5"><xsl:if test="$nbLigne=5"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>5</option>
								<option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
								<option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
								<option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
								<option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
								<option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
							</select>

					</td>
				</tr>
			</table>

			<table width="80%" align="center" border="0" cellspacing="0" class="tableResults">
			<!-- NOM DES COLONNES -->
				<tr>
					<td class="resultsHead" width="34px"> </td>
					<td class="resultsHead">
					<xsl:if test="contains($tri,'id_job')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=id_job{$ordre}"><xsl:processing-instruction name="php">print ID;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'usager')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=usager{$ordre}"><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_nom')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_nom{$ordre}"><xsl:processing-instruction name="php">print kNom;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'module_nom')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=module_nom{$ordre}"><xsl:processing-instruction name="php">print kModule;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_in')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_in{$ordre}"><xsl:processing-instruction name="php">print kFichier;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_out')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_out{$ordre}"><xsl:processing-instruction name="php">print kSortie;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_date_crea')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_date_crea{$ordre}"><xsl:processing-instruction name="php">print kDateCreation;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_date_debut')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_date_debut{$ordre}"><xsl:processing-instruction name="php">print kDateDebut;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_date_fin')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_date_fin{$ordre}"><xsl:processing-instruction name="php">print kDateFin;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'etat_job')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=etat_job{$ordre}"><xsl:processing-instruction name="php">print kEtat;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_priorite')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_priorite{$ordre}"><xsl:processing-instruction name="php">print kPriorite;</xsl:processing-instruction></a></td>
					<td class="resultsHead" width="20px"> </td>
					<td class="resultsHead" width="20px"> </td>
				</tr>
			<!-- VALEURS -->
				<xsl:for-each select="t_job[ID_JOB_GEN='0']">
					<xsl:variable name="id_job"><xsl:value-of select="ID_JOB"/></xsl:variable>
					<xsl:variable name="j"><xsl:number/></xsl:variable>
					<tr id="row_job_{$id_job}" class="altern1">
						<xsl:call-template name="displayCells">
							<xsl:with-param name="id_job" select="$id_job"/>
							<xsl:with-param name="j" select="$j"/>
							<xsl:with-param name="jobEnCours" select="$jobEnCours"/>
							<xsl:with-param name="jobFini" select="$jobFini"/>
								<xsl:with-param name="level" select="0"/>
						</xsl:call-template>
					</tr>
					
					<!-- job enfants -->
					<xsl:for-each select="/select/select/t_job_child[ID_JOB_GEN=$id_job]">
						<xsl:variable name="id_job2"><xsl:value-of select="ID_JOB"/></xsl:variable>
						<xsl:variable name="j2"><xsl:number/></xsl:variable>
						<tr id="row_job_{$id_job2}" class="altern0 jobGen{$id_job}">
							<xsl:call-template name="displayCells">
								<xsl:with-param name="id_job" select="$id_job2"/>
								<xsl:with-param name="j" select="$j2"/>
								<xsl:with-param name="jobEnCours" select="$jobEnCours"/>
								<xsl:with-param name="jobFini" select="$jobFini"/>
								<xsl:with-param name="level" select="1"/>
							</xsl:call-template>
						</tr>
						
						<!-- job petits-enfants -->
						<xsl:for-each select="/select/select/t_job_child[ID_JOB_GEN=$id_job2]">
							<xsl:variable name="id_job3"><xsl:value-of select="ID_JOB"/></xsl:variable>
							<xsl:variable name="j3"><xsl:number/></xsl:variable>
							<tr id="row_job_{$id_job3}" class="altern2 jobGen{$id_job2} jobGenParent{$id_job}">
								<xsl:call-template name="displayCells">
									<xsl:with-param name="id_job" select="$id_job3"/>
									<xsl:with-param name="j" select="$j3"/>
									<xsl:with-param name="jobEnCours" select="$jobEnCours"/>
									<xsl:with-param name="jobFini" select="$jobFini"/>
									<xsl:with-param name="level" select="2"/>
								</xsl:call-template>
							</tr>
						</xsl:for-each>
					</xsl:for-each>
					
				</xsl:for-each>
			</table>
		</form>
		<style>
			.altern0, .altern2 {
				display:none;
			}
			.altern0 .resultsCorps {
				--font-size: 9px;
			}
			.toggleButton {
				cursor: pointer;
			}
		</style>
    </xsl:template>
	
	<xsl:template name="displayCells">
		<xsl:param name="jobEnCours" />
		<xsl:param name="jobFini" />
		<xsl:param name="id_job" />
		<xsl:param name="j" />
		<xsl:param name="level" />
		
		<td class="resultsCorps">
			<xsl:choose>
				<xsl:when test="$level=1">
					&amp;nbsp;&amp;nbsp;&amp;nbsp;
				</xsl:when>
				<xsl:when test="$level=2">
					&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
				</xsl:when>
			</xsl:choose>
		
			<xsl:choose>
				<xsl:when test="count(/select/select/t_job_child[ID_JOB_GEN=$id_job])&gt;0">
					<img class="toggleButton" onclick="displayChildren({$id_job}, this)" src="design/images/plus.gif" alt="+" idJob="{$id_job}" />
				</xsl:when>
				<xsl:when test="$level&gt;=1">
					<img src="design/images/right_lot.gif" alt="->" />
				</xsl:when>
				<xsl:otherwise>
					&amp;nbsp;
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="resultsCorps"><xsl:value-of select="ID_JOB"/></td>
		<td class="resultsCorps"><xsl:value-of select="USAGER"/></td>
		<td class="resultsCorps"><xsl:value-of select="JOB_NOM"/></td>
		<td class="resultsCorps"><xsl:value-of select="MODULE_NOM"/></td>
		<td class="resultsCorps"><xsl:value-of select="JOB_IN"/></td>
		<td class="resultsCorps"><xsl:value-of select="JOB_OUT"/><xsl:text>&amp;nbsp;</xsl:text></td>
		<td class="resultsCorps"><xsl:value-of select="JOB_DATE_CREA"/></td>
		<td class="resultsCorps"><xsl:value-of select="JOB_DATE_DEBUT"/><xsl:text>&amp;nbsp;</xsl:text></td>
		<td class="resultsCorps"><xsl:value-of select="JOB_DATE_FIN"/><xsl:text>&amp;nbsp;</xsl:text></td>
		<td class="resultsCorps">
			<xsl:value-of select="ETAT_JOB"/>
			<xsl:if test="JOB_ID_ETAT=$jobEnCours">
				<div id="progressbar{ID_JOB}" style="width:50px;height:16px">
					<div class="progress-label" style="margin-left: 15%;float: left;" ><xsl:value-of select="JOB_PROGRESSION"/>%</div>
				</div>
				<script>
					$j(function() {
						$j( "#progressbar<xsl:value-of select="ID_JOB"/>" ).progressbar({value: <xsl:value-of select="JOB_PROGRESSION"/>});
					});
				</script>
				<style>
				.ui-progressbar-value {
					background : #7fea92;
					border: 1px solid #4fba62;
				}
				</style>
			</xsl:if>
			<xsl:if test="JOB_ID_ETAT!=$jobEnCours">
				<xsl:if test="string-length(normalize-space(JOB_MESSAGE))&gt;0"><br/><xsl:value-of select="JOB_MESSAGE"/></xsl:if>
			</xsl:if>
		</td>
		<td class="resultsCorps">
			<!--xsl:value-of select="JOB_PRIORITE"/-->
			<img src="design/images/star-icon.png" alt="X" />
			<xsl:if test="JOB_PRIORITE &lt; 3"><img src="design/images/star-icon.png" alt="X" /></xsl:if>
			<xsl:if test="JOB_PRIORITE &lt; 2"><img src="design/images/star-icon.png" alt="X" /></xsl:if>
		</td>
		<td class="resultsCorps">
			<xsl:if test="JOB_ID_ETAT!=$jobFini">
				<input type="image" name="btRefreshc{$j}" src="design/images/refresh.gif" onclick="updateLine('{$id_job}','INIT')" title="&lt;?php echo kRelancer; ?&gt;" alt="&lt;?php echo kRelancer; ?&gt;"/>
			</xsl:if>
			<xsl:text>&amp;nbsp;</xsl:text>
		</td>
		<td class="resultsCorps">
			<xsl:if test="JOB_ID_ETAT!=$jobFini">
				<input type="image" name="btSupc{$j}" src="design/images/button_drop.gif" onclick="removeLine('{$id_job}')" title="&lt;?php echo kSupprimer; ?&gt;" alt="&lt;?php echo kSupprimer; ?&gt;"/>
			</xsl:if>
			<xsl:text>&amp;nbsp;</xsl:text>
		</td>
	</xsl:template>
</xsl:stylesheet>
