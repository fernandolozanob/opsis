<?xml version="1.0" encoding="utf-8"?>
<!-- jobListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="scripturl" />
<xsl:param name="ordre" />
<xsl:param name="tri" />
<xsl:param name="gestPers"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />
<xsl:param name="nb_pages" />
<xsl:param name="jobFini" />
<xsl:param name="jobEnCours" />
<xsl:param name="display" />
<xsl:param name="userID" />

    <xsl:template match='/select'>
		<script type="text/javascript">
			$j(document).ready(function () {
				setTimeout("updateListe();",2000);
			});
			
			function updateListe() {
				$j.post("empty.php?urlaction=updateJobListe",
					{
						display: "&lt;? echo $_GET['display']; ?&gt;",
						job_aff: "1",
						scripturl: "<xsl:value-of select="$scripturl" />"
					},
					function (data) {
						updateListTab(data);
						updateListResults(data);
						updateListPage(data);
						
						data = data.substr(data.lastIndexOf('&lt;table width="80%" align="center" border="0" cellspacing="0" class="tableResults"'));
						data = data.substr(data.indexOf('&lt;tr&gt;'));
						data = data.substr(0, data.lastIndexOf('&lt;/tr&gt;') + 5);
						$j("form[name='form1'] table.tableResults").html(data);
						
						setTimeout("updateListe();",5000);
					}
				);
			}
			
			function updateListTab(data) {
				var nb_rows_act = data.substr(data.indexOf('&lt;nb_rows_act&gt;') + 13);
				nb_rows_act = nb_rows_act.substr(0, nb_rows_act.indexOf('&lt;/nb_rows_act&gt;'));
				var tabhtml_act = $j("a#tab_act").html();
				tabhtml_act = tabhtml_act.substr(0, tabhtml_act.indexOf("(") + 1) + nb_rows_act + tabhtml_act.substr(tabhtml_act.indexOf(")"));
				$j("a#tab_act").html(tabhtml_act);
				
				var nb_rows_err = data.substr(data.indexOf('&lt;nb_rows_err&gt;') + 13);
				nb_rows_err = nb_rows_err.substr(0, nb_rows_err.indexOf('&lt;/nb_rows_err&gt;'));
				var tabhtml_err = $j("a#tab_err").html();
				tabhtml_err = tabhtml_err.substr(0, tabhtml_err.indexOf("(") + 1) + nb_rows_err + tabhtml_err.substr(tabhtml_err.indexOf(")"));
				$j("a#tab_err").html(tabhtml_err);
				
				var nb_rows_fin = data.substr(data.indexOf('&lt;nb_rows_fin&gt;') + 13);
				nb_rows_fin = nb_rows_fin.substr(0, nb_rows_fin.indexOf('&lt;/nb_rows_fin&gt;'));
				var tabhtml_fin = $j("a#tab_fin").html();
				tabhtml_fin = tabhtml_fin.substr(0, tabhtml_fin.indexOf("(") + 1) + nb_rows_fin + tabhtml_fin.substr(tabhtml_fin.indexOf(")"));
				$j("a#tab_fin").html(tabhtml_fin);
			}
			
			function updateListResults(data) {
				var nb_rows = data.substr(data.indexOf('&lt;nb_rows&gt;') + 9);
				nb_rows = nb_rows.substr(0, nb_rows.indexOf('&lt;/nb_rows&gt;'));
				$j("table.resultsMenu td[align='left']").html(nb_rows + "&amp;nbsp;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>");
			}
			
			function updateListPage(data) {
				var html = data.substr(data.lastIndexOf('&lt;td align="center" width="33%"&gt;') + 31);
				html = html.substr(0, html.indexOf('&lt;/td&gt;'));
				if (html.indexOf('input type="text" name="rang"') > 0)
					$j("table.resultsMenu td[align='center']").html(html);
			}
		</script>
		<form name="form1" method="post" action="simple.php?urlaction=jobListe{$urlparams}">
			<input name="commande" id="commande" type="hidden" value="" />
			<input name="ligne" value="" type="hidden" />
			<input type="hidden" value="{$page}" name="page" />
			
			 <table width="95%" border="0" class="resultsMenu" align="center">
				<tr>
					<td align="left" width="33%">
						<xsl:value-of select="$nb_rows" /> <xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>
					</td>
					<td align="center" width="33%">
						<xsl:if test="$nb_pages>1">
							<xsl:if test="$page>1"><a href="#" onclick='javascript:document.form1.page.value={$page - 1};document.form1.submit();'><img src="design/images/fleche-gauche.gif" alt="" border="0" width="11" height="12" /></a> </xsl:if>
							<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onChange='javascript:document.form1.page.value=eval(this.value);document.form1.submit();' />
							<xsl:value-of select="concat(' / ',$nb_pages)"/>
							<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:document.form1.page.value={$page + 1};document.form1.submit();'><img src="design/images/fleche-droite.gif" alt="" border="0" width="11" height="12" /></a></xsl:if>
						</xsl:if>
					</td>
					<td align="right" colspan="3">

							<xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
							<select class='val_champs_form' name="nbLignes" onchange="javascript:document.form1.submit()">
								<option value="5"><xsl:if test="$nbLigne=5"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>5</option>
								<option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
								<option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
								<option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
								<option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
								<option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
							</select>

					</td>
				</tr>
			</table>

			<table width="80%" align="center" border="0" cellspacing="0" class="tableResults">
			<!-- NOM DES COLONNES -->
				<tr>
					<td class="resultsHead">
					<xsl:if test="contains($tri,'id_job')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=id_job{$ordre}"><xsl:processing-instruction name="php">print 'ID';</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'usager')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=usager{$ordre}"><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_nom')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_nom{$ordre}"><xsl:processing-instruction name="php">print kNom;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'module_nom')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=module_nom{$ordre}"><xsl:processing-instruction name="php">print kModule;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_in')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_in{$ordre}"><xsl:processing-instruction name="php">print kFichier;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_out')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_out{$ordre}"><xsl:processing-instruction name="php">print kSortie;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_date_crea')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_date_crea{$ordre}"><xsl:processing-instruction name="php">print kDateCreation;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_date_debut')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_date_debut{$ordre}"><xsl:processing-instruction name="php">print kDateDebut;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_date_fin')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_date_fin{$ordre}"><xsl:processing-instruction name="php">print kDateFin;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'etat_job')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=etat_job{$ordre}"><xsl:processing-instruction name="php">print kEtat;</xsl:processing-instruction></a></td>
					<td class="resultsHead"><xsl:if test="contains($tri,'job_priorite')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=jobListe{$urlparams}&amp;tri=job_priorite{$ordre}"><xsl:processing-instruction name="php">print kPriorite;</xsl:processing-instruction></a></td>
					<td class="resultsHead" width="20px"> </td>
					<td class="resultsHead" width="20px"> </td>
				</tr>
			<!-- VALEURS -->
				<xsl:for-each select="t_job">
					<xsl:variable name="id_job"><xsl:value-of select="ID_JOB"/></xsl:variable>
					<xsl:variable name="j"><xsl:number/></xsl:variable>
					<tr id="row_job_{$id_job}" class="{concat('altern',$j mod 2)}">
						<td class="resultsCorps cell_id_job"><xsl:value-of select="ID_JOB"/></td>
						<td class="resultsCorps cell_usager"><xsl:value-of select="USAGER"/></td>
						<td class="resultsCorps cell_job_nom"><xsl:value-of select="JOB_NOM"/></td>
						<td class="resultsCorps cell_module_nom"><xsl:value-of select="MODULE_NOM"/></td>
						<td class="resultsCorps cell_job_in"><xsl:value-of select="JOB_IN"/></td>
						<td class="resultsCorps cell_job_out"><xsl:value-of select="JOB_OUT"/><xsl:text>&amp;nbsp;</xsl:text></td>
						<td class="resultsCorps cell_job_date_crea"><xsl:value-of select="JOB_DATE_CREA"/></td>
						<td class="resultsCorps cell_job_date_debut"><xsl:value-of select="JOB_DATE_DEBUT"/><xsl:text>&amp;nbsp;</xsl:text></td>
						<td class="resultsCorps cell_job_date_fin"><xsl:value-of select="JOB_DATE_FIN"/><xsl:text>&amp;nbsp;</xsl:text></td>
						<td class="resultsCorps cell_etat_job">
							<xsl:choose>
								<xsl:when test="string-length(normalize-space(JOB_MESSAGE))&gt;0 and JOB_ID_ETAT=$jobEnCours">
									<span title="{JOB_MESSAGE}">
										<xsl:value-of select="ETAT_JOB"/>
									</span>
								</xsl:when>
								<xsl:otherwise>
										<xsl:value-of select="ETAT_JOB"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="JOB_ID_ETAT=$jobEnCours">
								<div id="progressbar{ID_JOB}" style="width:50px;height:16px">
									<div class="progress-label" style="margin-left: 15%;float: left;" ><xsl:value-of select="round(JOB_PROGRESSION)"/>%</div>
								</div>
								<script>
									$j(function() {
										$j( "#progressbar<xsl:value-of select="ID_JOB"/>" ).progressbar({value: <xsl:value-of select="round(JOB_PROGRESSION)"/>});
									});
								</script>
								<style>
								.ui-progressbar-value {
									background : #7fea92;
									border: 1px solid #4fba62;
								}
								</style>
							</xsl:if>
							<xsl:if test="JOB_ID_ETAT!=$jobEnCours">
								<xsl:if test="string-length(normalize-space(JOB_MESSAGE))&gt;0"><br/><xsl:value-of select="JOB_MESSAGE"/></xsl:if>
							</xsl:if>
						</td>
						<td class="resultsCorps cell_job_priorite">
							<xsl:choose>
								<xsl:when test="$profil = $profil_lvl_admin">
									<div class="prio_selector" id="prio_selector_{ID_JOB}" data-job-priorite="{JOB_PRIORITE}">
										<img src="design/images/star-icon.png" class="prio_stars" alt="X" onclick="setJobPriorite('{$id_job}',3)"/>
										<img src="design/images/star-icon.png" class="prio_stars" alt="X" onclick="setJobPriorite('{$id_job}',2)"/>
										<img src="design/images/star-icon.png" class="prio_stars" alt="X" onclick="setJobPriorite('{$id_job}',1)"/>	
										<img src="design/images/star-icon.png" class="prio_stars" alt="X" onclick="setJobPriorite('{$id_job}',0)"/>
									</div>
								</xsl:when>
								<xsl:otherwise>
									<img src="design/images/star-icon.png" alt="X" />
									<xsl:if test="JOB_PRIORITE &lt; 3"><img src="design/images/star-icon.png" alt="X" /></xsl:if>
									<xsl:if test="JOB_PRIORITE &lt; 2"><img src="design/images/star-icon.png" alt="X" /></xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td class="resultsCorps">
							<xsl:if test="(JOB_ID_ETAT!=$jobFini and JOB_ID_ETAT!=$jobEnCours)">
								<input type="image" name="btRefresh{$j}" src="design/images/refresh.gif" onclick="updateLine('{$id_job}','INIT')" title="&lt;?php echo kRelancer; ?&gt;" alt="&lt;?php echo kRelancer; ?&gt;"/>
							</xsl:if>
							<xsl:if test="JOB_ID_ETAT!=$jobFini and JOB_ID_ETAT=$jobEnCours">
								<input type="image" name="btRefresh{$j}" src="design/images/abandon.gif" onclick="updateLine('{$id_job}','CANCEL')" title="&lt;?php echo kAnnuler; ?&gt;" alt="&lt;?php echo kAnnuler; ?&gt;"/>
							</xsl:if>
							<xsl:text>&amp;nbsp;</xsl:text>
						</td>
						<td class="resultsCorps">
							<xsl:if test="JOB_ID_ETAT!=$jobFini">
								<input type="image" name="btSup{$j}" src="design/images/button_drop.gif" onclick="removeLine('{$id_job}')" title="&lt;?php echo kSupprimer; ?&gt;" alt="&lt;?php echo kSupprimer; ?&gt;"/>
							</xsl:if>
							<xsl:text>&amp;nbsp;</xsl:text>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</form>
    </xsl:template>
</xsl:stylesheet>
