<?xml version="1.0" encoding="utf-8"?>
<!-- XS : 26/10/2005 : rechercheListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 
<!-- recuperation de parametres PHP -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />

    <xsl:template match="EXPORT_OPSIS">
    
    	<script>
    	
    		function disableAll() {
    				myTab=document.getElementById('requetes');
    				myTRs=myTab.getElementsByTagName('tr');
    				if (myTab.off) myTab.off=false; else myTab.off=true;
    				for (j=0;j&lt;myTRs.length;j++) {
    						myTRs[j+1].off=!myTab.off;
    						disableLine(j+1);
    				}
    		
    		}
    	
	    	function disableLine(l) {
	    			myTR=document.getElementById('row'+l);
	    			if (myTR.off) {
	    				myTR.off=false;
	    				//myInputs=myTR.getElementsByTagName('input');
	    				//for (i=0;i&lt;myInputs.length;i++) myInputs[i].disabled=false;
	    				myCells=myTR.getElementsByTagName('td');
	    				for (i=0;i&lt;myCells.length;i++) myCells[i].style.textDecoration='none';	    
	    				document.getElementById('commande'+l).value='SAVE';	
	    				myTR.style.opacity=1;	
	    						
	    				
	    					    					
	    			} else
	    			{
		    			myTR.off=true;
	    				//myInputs=myTR.getElementsByTagName('input');
	    				//for (i=0;i&lt;myInputs.length;i++) if(myInputs[i].id!='action'+l) myInputs[i].disabled=true;
						myCells=myTR.getElementsByTagName('td');
	    				for (i=0;i&lt;myCells.length;i++) myCells[i].style.textDecoration='line-through';	
	    				document.getElementById('commande'+l).value='SUP';
	    				myTR.style.opacity=0.7;	
	    			}
	    	}
    	
    	</script>
    	
    	<!-- updater VG 30/05/2011 : Ajout d'ids sur la colonne et l'header "supprimer" -->
        <table width="80%" border="0" cellspacing="0" id="requetes" class="tableResults" align="center">
            <tr>
                <td class="resultsHead"> </td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kNom;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kRequete;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kAlerteMail;</xsl:processing-instruction></td>
                <td id="headDelete" class="resultsHead" width="20px"><img name="btSupAll" src="design/images/icn-trash-grey.png" style="cursor:pointer" onclick="disableAll()" title="&lt;?php echo kSupprimerTout; ?&gt;"/>&#160;</td>
            </tr>
            <xsl:for-each select="t_requete">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}" id='row{$j}' >
                    <td class="resultsCorps">
                        <input type="hidden" name="t_requete[ID_REQ][]" value="{ID_REQ}" />
                        <a href="{$scripturl}?urlaction=docListe&amp;recherche={ID_REQ}">
						<img src="design/images/refresh.gif" border="0" title="&lt;?php echo kLancer; ?&gt;" alt="&lt;?php echo kLancer; ?&gt;"/>
						</a>
                    </td>
                    <td class="resultsCorps"><input type='TEXT' value='{REQUETE}' name='t_requete[REQUETE][]' /></td>
                    <td class="resultsCorps"><xsl:value-of select="REQ_LIBRE"/></td>
                    <td class="resultsCorps"><xsl:value-of select="REQ_NB_DOC"/></td>
                    <td class="resultsCorps">
                        <input type="checkbox" name="t_requete[REQ_ALERTE][{$j -1}]" value="1">
                            <xsl:if test="REQ_ALERTE=1">
                                <xsl:attribute name = "checked" ><xsl:text>checked</xsl:text></xsl:attribute>
                            </xsl:if>
                        </input>
                    </td>
                    <td id="colDelete" class="resultsCorps">
                    	<input type="hidden" name="t_requete[commande][]" id="commande{$j}" value="SAVE" />
                        <img id="btSup{$j}" style="cursor:pointer" src="design/images/icn-trash-grey.png" onclick="disableLine({$j})" title="&lt;?php echo kSupprimer; ?&gt;" alt="&lt;?php echo kSupprimer; ?&gt;"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
	<div class="std_form_btn_wrapper">
		<input type="submit" value="&lt;?php echo kEnregistrer; ?&gt;" class="std_button_style" name="SAVE"/>
    </div>
	</xsl:template>
</xsl:stylesheet>
