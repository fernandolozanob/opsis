<?xml version="1.0" encoding="utf-8"?>
<!-- panier.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<!-- etat=0,1,2 -->
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="offset" />
<xsl:param name="fromOffset" />
<xsl:param name="affMode" />
<xsl:param name="pagination" />



<xsl:template match='/select'>

	<xsl:for-each select="t_livraison/t_livraison_doc" >
		<xsl:variable name="u"><xsl:number/></xsl:variable>
		<iframe name="frameLivraison{$u}" id="frameLivraison{$u}" frameborder="0" scrolling="no" width="0" height="0" allowtransparency="true" src="download.php?livraison={concat(../LIVRAISON_DIR,'/',FILENAME)}" >&#160;</iframe>
	</xsl:for-each>

<iframe name="framePanier" id="framePanier" style="display : none ; ">&#160;</iframe>

<form name="form1" method="post" id="formPanier"  target="framePanier" action="{$scripturl}?urlaction=panier{$urlparams}&amp;noDisplay=1" >
        <input type="hidden" name="commande"/>
        <input type="hidden" name="ligne"/>
        <input type="hidden" value="{t_panier/ID_PANIER}" name="id_panier" />
        <input type="hidden" value="{t_panier/PAN_ID_ETAT}" name="pan_id_etat" />
        <input type="hidden" value="{t_panier/PAN_ID_TYPE_COMMANDE}" name="pan_id_type_commande" />
        <input type="hidden" value="{t_panier/PAN_ID_USAGER}" name="pan_id_usager" />
        <input type="hidden" value="{t_panier/PAN_ID_GEN}" name="pan_id_gen" />
		<input type="hidden" value="{$page}" name="page" />
		<input type="hidden" name="email_to_send" />
		<input type="hidden" name="corpsMail" />
		<input type="hidden" name="objetMail" />
		<input type="hidden" name="copyForExp" />
		<input type="hidden" name="dspDest"/>
		<input type="hidden" name="DESTINATION_PANIER"/>
		<input type="hidden" name="action_panier"/>
    <div width="80">
		<xsl:attribute name="class">
			<xsl:choose>
				<xsl:when test="$affMode = 'mos'">mosWrapper</xsl:when>
				<xsl:when test="$affMode = 'liste'">resultsCorps</xsl:when>
			</xsl:choose>
		</xsl:attribute>
		<div id="panHeaderFieldsWrapper" >
			<input type="hidden" value="{count(t_panier_doc)}" name="pan_nb_elt" />
			<input class="panHeaderFields" type="text" value="{t_panier/PAN_TITRE}" placeholder="&lt;?=kTitre?&gt;" name="pan_titre" />
			<input class="panHeaderFields" type="text" value="{t_panier/PAN_OBJET}" placeholder="&lt;?=kNotes?&gt;" name="pan_objet" />
		</div>
		<xsl:if test="t_panier_doc !=''">
			<xsl:variable name="xmllist" select="document($xmlfile)"/>
			<xsl:choose>
				<xsl:when test="$affMode = 'mos'">
					<xsl:call-template name="mosDoc">
						<xsl:with-param name="context" >t_panier_doc</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$affMode = 'liste'">
					<xsl:call-template name="displayListe">
						<xsl:with-param name="context" >t_panier_doc</xsl:with-param>
						<xsl:with-param name="xmllist" select="$xmllist"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		&#160;
		<div align='center' id='livraison' >&#160;</div>
		<div class="pusher">&#160;</div>
		<div id="panBoutons">
			<button type='button' onclick="saveCurrentCart();"><xsl:processing-instruction name="php">print kEnregistrer;</xsl:processing-instruction></button>
			<xsl:if test="count(t_panier_doc) &gt; 0 ">
				<button type='button' onclick="downloadCurrentCart(2);"><xsl:processing-instruction name="php">print kTelecharger;</xsl:processing-instruction></button>
				<button type='button' onclick="goToMontage();" class="montage-btn"><xsl:processing-instruction name="php">print kMontage;</xsl:processing-instruction></button>
			</xsl:if>
			<button type='button' onclick="emptyCurrentCart();"><xsl:processing-instruction name="php">print kSupprimerDocuments;</xsl:processing-instruction></button>
			<button type='button' onclick="deleteCurrentCart();"><xsl:processing-instruction name="php">print kSupprimerPanier;</xsl:processing-instruction></button>
		</div>
	</div>
 </form>
<script>
	<xsl:if test="$affMode = 'liste'">
		$j('table#panier tbody').sortable
		(
			{
				appendTo: document.getElementById("#panFrame div.resultsCorps"),
				axis:'y',
				cancel : 'tr.resultsHead_row, tr.separator',
				forceHelperSize : true,
				placeholder : 'pan_sort_placeholder' ,
				beforeStop: function(event, ui)
				{
					if($j("#panier tbody").find('.pan_sort_placeholder').index()&lt;3){
						return false ; 
					}else{
						refreshPan();
					}
				}
			}
		);
		
		$j("#panFrame tr:not(.separator, .resultsHead_row)").each(function(idx,elt){
			if($j(elt).find(".resultsCorps input[type='checkbox']").length>0){
				$j(elt).click(function(){
					chck = $j(elt).find(".resultsCorps input[name*='ID_DOC']");
					loadPrevDoc(chck.val().replace("doc_",''),elt);
				});
			}
		});
	</xsl:if>
	// MS mise en place redirection par doubleclick sur l'image : 
	// mosaique, 
	$j("#formPanier div.mosWrapper div.resultsMos").dblclick(function(evt){
		if($j(evt.target).parents('.resultsMos').find('a.consult').length >0){
			window.location.href = $j(evt.target).parents('.resultsMos').find('a.consult').attr('href');
		}
		
	});
	// liste, 
	$j("#formPanier div.resultsCorps div.resultsMos").dblclick(function(evt){
		if($j(evt.target).parents('.resultsMosRow').find('a.consult').length >0){
			window.location.href = $j(evt.target).parents('.resultsMosRow').find('a.consult').attr('href');
		}
		
	});
</script>
</xsl:template>


</xsl:stylesheet>
