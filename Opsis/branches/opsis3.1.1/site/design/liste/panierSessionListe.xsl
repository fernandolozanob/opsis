﻿<?xml version="1.0" encoding="utf-8"?>
<!-- panier_session.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>

<xsl:template match='/select'>


<!--
<script type="text/javascript">
	var dragsort = ToolMan.dragsort()
	var junkdrawer = ToolMan.junkdrawer()

	//window.onload = function() {
	//	junkdrawer.restoreListOrder("panier_session")

	//	dragsort.makeListSortable(document.getElementById("panier_session"),
	//			verticalOnly, saveOrder)
	//}

	function verticalOnly(item) {
		item.toolManDragGroup.verticalOnly()
	}

	function speak(id, what) {
		var element = document.getElementById(id);
		element.innerHTML = 'Clicked ' + what;
	}

	function saveOrder(item) {
		var group = item.toolManDragGroup
		var list = group.element.parentNode
		var id = list.getAttribute("id")
		if (id == null) return
		group.register('dragend', function() {
			ToolMan.cookies().set("list-" + id, 
					junkdrawer.serializeList(list), 365)
		})
	}
</script>
-->
<table width="95%" border="0" class="resultsMenu" align="center">
    <tr>
        <td align="left" width="33%">
            <!--<a href="javascript:popupPrint('imprimeListe.php?sql=doc_sql&amp;type=doc&amp;tri=doc_tri&amp;titre={$titre}&amp;style_presentation=panier_session1.xsl','main')">-->
            <a href="javascript:popupPrint('export.php?impression={$titre}&amp;export=panier_session1&amp;sql=doc_sql&amp;type=doc&amp;nb={$nbLigne}&amp;page=1','main')">
            <xsl:processing-instruction name="php">print kImprimer;</xsl:processing-instruction>
            </a>
            <xsl:text> | </xsl:text>
            <a href="javascript:popupPrint('export.php?sql=doc_sql&amp;type=doc&amp;nb={$nbLigne}&amp;page=1','main')">
            <xsl:processing-instruction name="php">print kExporter;</xsl:processing-instruction>
            </a>
            <!-- A-t-on la possibilité de sauvegarder la recherche ? -->
            <xsl:if test="$sauvRequete != ''">
                <xsl:text> | </xsl:text>
                <a href="javascript:popupSave('sauvRequete.php?hist={$sauvRequete}&amp;type=1','main')">
                    <xsl:processing-instruction name="php">print kEnregistrer;</xsl:processing-instruction>
                </a>
            </xsl:if>
        </td>
        <td align="right" width="33%">
            <form name="form_aff" id="form_aff" action="{$scripturl}?urlaction=panier_session" method="post">
                <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
                <select name="nbLignes" onchange="javascript:document.form_aff.submit()">
                    <option value="5"><xsl:if test="$nbLigne=5"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>5</option>
                    <option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
                    <option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
                    <option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
                    <option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
                    <option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
                </select>
            </form>
        </td>
    </tr>
</table>

<form name="documentSelection" method="post" action="{$scripturl}?urlaction=panier_session">
	<input type='hidden' name='item2remove'/>
    <table width="95%" align="center" cellspacing="0" class="tableResults" id="panier_session" >
        
            <th class="resultsHead">
                <xsl:if test="contains($tri,'doc_cote')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                <a href="{$scripturl}?urlaction=panier_session&amp;tri=doc_cote{$ordre}"><xsl:processing-instruction name="php">print kCote;</xsl:processing-instruction></a>
            </th>
            <th class="resultsHead"><xsl:processing-instruction name="php">print kImage;</xsl:processing-instruction></th>
            <th class="resultsHead">
                <xsl:if test="contains($tri,'doc_titre')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                <a href="{$scripturl}?urlaction=panier_session&amp;tri=doc_titre{$ordre}"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></a>
            </th>
            <th class="resultsHead">
                <xsl:if test="contains($tri,'doc_date_prod')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                <a href="{$scripturl}?urlaction=panier_session&amp;tri=doc_date_prod{$ordre}"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></a>
            </th>
            <th class="resultsHead">
                <xsl:if test="contains($tri,'fonds')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                <a href="{$scripturl}?urlaction=panier_session&amp;tri=fonds{$ordre}"><xsl:processing-instruction name="php">print kFonds;</xsl:processing-instruction></a>
            </th>
            <th class="resultsHead">
                <xsl:if test="contains($tri,'type_doc')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                <a href="{$scripturl}?urlaction=panier_session&amp;tri=type_doc{$ordre}"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></a>
            </th>
            <th class="resultsHead">
                <xsl:if test="contains($tri,'doc_duree')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                <a href="{$scripturl}?urlaction=panier_session&amp;tri=doc_duree{$ordre}"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></a>
            </th>
        	<th class="resultsHead">   
        		<img src='design/images/button_drop_a.gif' onclick="document.documentSelection.item2remove.value='all';document.documentSelection.submit();" style='cursor:pointer' />
        	</th>        
 
            <xsl:for-each select="doc">
     
                <xsl:variable name="id"><xsl:value-of select="id_doc"/></xsl:variable>
                <xsl:variable name="im"><xsl:value-of select="concat('&lt;?php echo storyboardChemin ?&gt;/',imageur,'/',im_chemin)"/></xsl:variable>
                <xsl:variable name="im2"><xsl:value-of select="concat('&lt;?php echo kDocumentUrl ?&gt;/',da_chemin,da_fichier)"/></xsl:variable>
                <xsl:variable name="titre"><xsl:value-of select="doc_titre"/></xsl:variable>
                <xsl:variable name="description" ><xsl:call-template name="internal-quote-replace">
                	<xsl:with-param	name="stream" select="substring(doc_res,'1','200')"/>
                	</xsl:call-template><xsl:text>...</xsl:text>
                </xsl:variable>
                <xsl:variable name="id_priv"><xsl:value-of select="id_priv"/></xsl:variable>
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <xsl:variable name="rang" select="number(number($offset)+$j)"/>
                <tr  class="{concat('altern',$j mod 2)}" >
                    <td class="resultsCorps"><xsl:value-of select="doc_cote"/></td>
                    <td class="resultsCorps">
                        <xsl:if test="contains($im,'.')">
                            <xsl:choose>
                                <xsl:when test="$id_priv>=0"><a onMouseOver="return escape('{$description}')"  href="{$scripturl}?urlaction=doc&amp;id_doc={$id}&amp;rang={$rang}"><img src="{$im}" border="0" width="80" /></a> </xsl:when>
                                <xsl:otherwise><img src="{$im}" border="0" width="80" onMouseOver="return escape('{$description}')"  /></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                        <xsl:if test="contains($im2,'.')">
                            <xsl:choose>
                                <xsl:when test="$id_priv>=0"><a onMouseOver="return escape('{$description}')"  href="{$scripturl}?urlaction=doc&amp;id_doc={$id}&amp;rang={$rang}"><img src="{$im2}" border="0" width="80" /></a> </xsl:when>
                                <xsl:otherwise><img src="{$im2}" border="0" width="80" onMouseOver="return escape('{$description}')"  /></xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>                        
                        
                    </td>
                    <td class="resultsCorps">
                        <xsl:choose>
                            <xsl:when test="$id_priv>=0"><a onMouseOver="return escape('{$description}')"  href="{$scripturl}?urlaction=doc&amp;id_doc={$id}&amp;rang={$rang}"><xsl:value-of select="doc_titre"/></a></xsl:when>
                            <xsl:otherwise><div onMouseOver="return escape('{$description}')" ><xsl:value-of select="doc_titre"/></div></xsl:otherwise>
                        </xsl:choose>
                    </td>
                    <td class="resultsCorps"><xsl:value-of select="doc_date_prod"/></td>
                    <td class="resultsCorps"><xsl:value-of select="fonds"/></td>
                    <td class="resultsCorps"><xsl:value-of select="type_doc"/></td>
                    <td class="resultsCorps"><xsl:value-of select="doc_duree"/></td>
                    <td class="resultsCorps"><img src='design/images/button_drop.gif' onclick='document.documentSelection.item2remove.value={$id};document.documentSelection.submit();' style='cursor:pointer;' /></td>
                </tr>

            </xsl:for-each>
     </table>

</form>
</xsl:template>


<xsl:template name="internal-quote-replace">

	  <xsl:variable name="simple-quote">'</xsl:variable>
	
		<xsl:choose>
		
		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>		
		</xsl:when>
	    
		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>
	    
		</xsl:choose>
    
</xsl:template>


</xsl:stylesheet>
