<?xml version="1.0" encoding="utf-8"?>
<!-- minipanier : utilisé pour afficher le contenu d'un folder en format très réduit (ex: frame de gauche, etc. ) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- etat=0,1,2 -->
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="scripturl"/>


<!-- PANIER_COMMANDE : Affichage du panier -->
   <xsl:template match='/select'>
	

<!-- SELECT : Détail de la commande -->
        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults" id_panier="{t_panier/ID_PANIER}">
 	<tr>
		<td class="resultsHeadMini" width="20" valign="top">&#160;</td>
		<td class="resultsHeadMini" width="130" valign="top">
			<xsl:processing-instruction name="php">print kReference;</xsl:processing-instruction>
			<br/>
			<xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction>
		</td>
		<td class="resultsHeadMini" width="90" valign="top">
			<xsl:processing-instruction name="php">print kDate ;</xsl:processing-instruction>
			<br/>
			<xsl:processing-instruction name="php">print kFonds ;</xsl:processing-instruction>
		</td>
		<td class="resultsHeadMini" width="80" valign="top">
			<xsl:processing-instruction name="php">print kType;</xsl:processing-instruction>
		</td>
		<td class="resultsHeadMini" width="60" valign="top">
			<xsl:processing-instruction name="php">print kExtrait;</xsl:processing-instruction>
		</td>
		<td class="resultsHeadMini" width="60" valign="top">
			<xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction>
		</td>
		<td class="resultsHeadMini" width="60" valign="top">
			<xsl:processing-instruction name="php">print kEtat;</xsl:processing-instruction>
		</td>
	</tr>    
       
            <xsl:for-each select="t_panier_doc">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">
					<td class="resultsCorpsMini" width="20">
						<input type="checkbox" name="checkbox_{ID_LIGNE_PANIER}" value="{DOC/t_doc/ID_DOC}"/>
					</td>
					<td class="resultsCorpsMini" width="130">
						<xsl:value-of select="DOC/t_doc/DOC_COTE"/>
						<br/>
						<a href="javascript:popupDoc('index.php?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}','main','D{DOC/t_doc/ID_DOC}')"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></a>
						<xsl:if test="PDOC_EXTRAIT='1'"><br/><xsl:value-of select="PDOC_EXT_TITRE"/></xsl:if>
					</td>
					<td class="resultsCorpsMini" width="90">
						<xsl:value-of select="DOC/t_doc/DOC_DATE"/>
						<br/><xsl:value-of select="DOC/t_doc/DOC_XML/XML/DOC/FONDS"/>
					</td>
					<td class="resultsCorpsMini" width="80">
							<xsl:choose>
								<xsl:when test="PDOC_EXTRAIT='1'"><xsl:processing-instruction name="php">print kExtrait;</xsl:processing-instruction></xsl:when>
								<xsl:otherwise><xsl:value-of select="DOC/t_doc/TYPE_DOC"/></xsl:otherwise>
							</xsl:choose>
					</td>
					<td class="resultsCorpsMini" width="60"><xsl:value-of select="PDOC_EXT_TITRE"/><br/><xsl:value-of select="PDOC_EXT_TCIN"/><br/><xsl:value-of select="PDOC_EXT_TCOUT"/>&#160;</td>
					<td class="resultsCorpsMini" width="60">
						<xsl:choose>
							<xsl:when test="PDOC_EXTRAIT='1'"><xsl:value-of select="EXT_DUREE"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="resultsCorpsMini" width="90">
						<xsl:value-of select="ETAT_LIGNE"/>
					</td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    
</xsl:stylesheet>
