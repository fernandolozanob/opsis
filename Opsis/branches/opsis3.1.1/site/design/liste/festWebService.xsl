<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />

<xsl:template match='/select'>

<data>
	<nb_pages><xsl:value-of select="$nb_pages"/></nb_pages>
	<nbRows><xsl:value-of select="$nb_rows"/></nbRows>
	<recherche><xsl:value-of select="$votre_recherche"/></recherche>

<rows>
   <xsl:for-each select="festival">
  
                <xsl:variable name="id"><xsl:value-of select="ID_FEST"/></xsl:variable>
                <xsl:variable name="im2">
	            	<xsl:choose>
	                	<xsl:when test="substring(DA_FICHIER,1,4)='http'" >
	                		<xsl:value-of select="DA_FICHIER"/>
	                	</xsl:when>
	                	<xsl:otherwise>
	                		<xsl:value-of select="DA_CHEMIN"/><xsl:value-of select="DA_FICHIER"/>
	                	</xsl:otherwise>
	                </xsl:choose>
                </xsl:variable>

                
                <xsl:variable name="titre"><xsl:value-of select="DOC_TITRE"/></xsl:variable>
                <xsl:variable name="id_priv"><xsl:value-of select="ID_PRIV"/></xsl:variable>
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <xsl:variable name="rang" select="number(number($offset)+$j)"/>
    <row rang="{$rang}">
      <id_fest><xsl:value-of select="$id"/></id_fest>
      <id_lang><xsl:value-of select="ID_LANG"/></id_lang>


      <annee><xsl:value-of select="substring-before(FEST_ANNEE,'-')"/></annee>
      <description><xsl:value-of select="FEST_DESCRIPTION"/></description>
      <vignette>
        <xsl:if test="contains($im2,'.')">service.php?urlaction=getFile&amp;amp;type=doc_acc&amp;amp;filename=<xsl:value-of select="$im2"/>
        </xsl:if>
      </vignette>
      <sections>
		<xsl:for-each select="FEST_XML/XML/SECTION">
		<section>
			<id_fest><xsl:value-of select="ID_FEST"/></id_fest>
			<libelle><xsl:value-of select="FEST_LIBELLE"/></libelle>
		</section>
		</xsl:for-each>     	
      </sections>
      <lien>service.php?urlaction=detail&amp;amp;entite=festival&amp;amp;id=<xsl:value-of select="$id"/></lien>
      <palmares>
            <xsl:for-each select="FEST_XML/XML/DOC_ACC">
    		  <xsl:if test="ID_VAL=801">
                  <piece_jointe>
                    			<titre><xsl:value-of select="DA_TITRE"/></titre>
                    			<fichier>service.php?urlaction=getFile&amp;amp;type=doc_acc&amp;amp;filename=<xsl:value-of select="DA_CHEMIN"/><xsl:value-of select="DA_FICHIER"/></fichier>
                   </piece_jointe>
              </xsl:if>
            </xsl:for-each>       	
      </palmares>
    
    </row>
    </xsl:for-each>
	</rows>
</data>


</xsl:template>


<xsl:template name="internal-quote-replace">
	  <xsl:param name="stream" />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>


</xsl:stylesheet>
