<?xml version="1.0" encoding="utf-8"?>
<!-- minipanier : utilisé pour afficher le contenu d'un folder en format très réduit (ex: frame de gauche, etc. ) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<xsl:param name="currentIdDoc"/>
<xsl:param name="id_lang" />

<xsl:template match='/select'>
    <form name='form2' id='form2' action='blank.php?urlaction=processDocument&amp;commande=saveDocuments' method='POST' target='iframeSauve' >
	<input type='hidden' name='id' value="{t_mat/ID_MAT}" />
	
	<xsl:variable name="id_lang_options">
	</xsl:variable>
	
	    <xsl:for-each select="t_mat/t_doc_mat/DOC_MAT">	      	
				<div id='row$' class='row_extrait'  style='width:100%;height:90px;box-sizing:border-box;'>
					<div id='handle$' class='ext_handler' onClick='myPanel.selectExtrait(this.parentNode)' >&#160;</div>
					<xsl:choose>
						<xsl:when test="t_doc/VIGNETTE!=''">
							<img src="makeVignette.php?image={t_doc/VIGNETTE}&amp;h=70&amp;kr=1" height='70' style='margin:2px 0px 0px 4px;float:left;left:10px;'/>
						</xsl:when>
						<xsl:otherwise><img src="&lt;?=imgUrl?&gt;/nopicture.gif" height='70' style='margin:2px 0px 0px 4px;float:left;left:10px;'/></xsl:otherwise>
					</xsl:choose>
					<xsl:variable name="titre" ><xsl:call-template name="internal-dbquote-replace">
                	<xsl:with-param	name="stream" select="t_doc/DOC_TITRE"/>
                	</xsl:call-template>
               		</xsl:variable>
					
					<input id='ext_titre$' type='text' readonly='readonly' size='55' name='t_doc[DOC_TITRE][]' class='ext_titre' value="{$titre}" />
					<a href='index.php?urlaction=docSaisie&amp;id_doc={t_doc/ID_DOC}' >
						<img id='edit$' class='trash' src='design/images/icn-edit.png' border='0' />
					</a>	
					<input type='hidden' name='t_doc[ID_LANG][]' value='{$id_lang}' />
					<input type='hidden' name='ID_DOC[]' value='{@ID_DOC}' />
					<input type='hidden' name='ID_DOC_MAT[]' value='{ID_DOC_MAT}' />
					<input id='action$' type='hidden' name='ligne_action[]' value='edit' /><br/>
					<input type='hidden' name='ID_MAT[]' value="{ID_MAT}" />
					<xsl:if test="(DOC_ID_MEDIA!='' and (DOC_ID_MEDIA='V' or DOC_ID_MEDIA='A')) or (/select/t_mat/MAT_ID_MEDIA!='' and (/select/t_mat/MAT_ID_MEDIA='V' or /select/t_mat/MAT_ID_MEDIA='A'))">
					&lt;?=kTCin?&gt;<input id='tcin$' type='text' name='DMAT_TCIN[]' size='11' maxlength='11' class='ext_TC' 
						value='{DMAT_TCIN}' onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);'/>
					&lt;?=kTCout?&gt;<input id='tcout$' type='text' name='DMAT_TCOUT[]' size='11' maxlength='11' class='ext_TC' 
						value='{DMAT_TCOUT}' onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);'/>				
					<img id='trash$' src='design/images/icn-trash-grey.png' class='trash' 
						onclick='myPanel.removeExtrait(this.parentNode)' />
					</xsl:if>
				</div>		
	    </xsl:for-each> 
	    <div id='separateur'/>  
    </form>
    
    <div id='row$blank' class='row_extrait'  style='width:100%;height:80px;display:none;box-sizing:border-box;'>
			<div id='handle$' class='ext_handler' onClick="myPanel.selectExtrait(this.parentNode)" >&#160;</div>
					<img id='vignette$' src="&lt;?=imgUrl?&gt;/nopicture.gif" height='70' style='margin:2px 0px 0px 4px;float:left;left:10px;'/>
					<input id='ext_titre$' type='text'  size='55' name='t_doc[DOC_TITRE][]' class='ext_titre' value="" 
					onKeyPress='myPanel.hasChanged(this.parentNode);' />
					<input type='hidden' name='t_doc[ID_LANG][]' value='{$id_lang}' />
					<input type='hidden' id='id_doc$' name='ID_DOC[]' value='' />
					<input type='hidden' name='ID_DOC_MAT[]' value='' />
					<input type='hidden' name='t_doc[DOC_ID_MEDIA][]' value='V' />
					<input id='action$' type='hidden' name='ligne_action[]' value='edit' /><br/>
					<input type='hidden' name='ID_MAT[]' value="{t_mat/ID_MAT}" />
					<xsl:if test="(DOC_ID_MEDIA!='' and (DOC_ID_MEDIA='V' or DOC_ID_MEDIA='A')) or (/select/t_mat/MAT_ID_MEDIA!='' and (/select/t_mat/MAT_ID_MEDIA='V' or /select/t_mat/MAT_ID_MEDIA='A'))">
					&lt;?=kTCin?&gt;<input id='tcin$' type='text' name='DMAT_TCIN[]' size='11' maxlength='11' class='ext_TC' 
						value='' onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);'/>
					&lt;?=kTCout?&gt;<input id='tcout$' type='text' name='DMAT_TCOUT[]' size='11' maxlength='11' class='ext_TC' 
						value='{t_mat/MAT_TCOUT}' onBlur='checkTC(this);myPanel.checkLine(this.parentNode);' onKeyPress='myPanel.hasChanged(this.parentNode);'/>				
</xsl:if>
					<img id='trash$' src='design/images/icn-trash-grey.png' class='trash' 
						onclick='myPanel.removeExtrait(this.parentNode)' />
	</div>
</xsl:template>

<xsl:template name="internal-quote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">'</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
		name="internal-quote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
</xsl:template>

<xsl:template name="internal-dbquote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">"</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>&quot;<xsl:call-template
		name="internal-dbquote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
</xsl:template>

</xsl:stylesheet>
