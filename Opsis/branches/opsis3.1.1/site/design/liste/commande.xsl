<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="../fonctions.xsl"/>
<xsl:include href="commande2.xsl"/>
<xsl:include href="commande4.xsl"/>
<xsl:include href="commande5.xsl"/>

<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<!-- commande1.xsl (Droits) -->

<xsl:param name="playerVideo"/>
<xsl:param name="imgurl"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />


    <xsl:template match='/select'>
		<xsl:choose>
			<xsl:when test="t_panier/PAN_ID_TYPE_COMMANDE='2'"><xsl:call-template name="commande2"/></xsl:when>
			<xsl:when test="t_panier/PAN_ID_TYPE_COMMANDE='4'"><xsl:call-template name="commande4"/></xsl:when>
			<xsl:when test="t_panier/PAN_ID_TYPE_COMMANDE='5'"><xsl:call-template name="commande5"/></xsl:when>
			<xsl:otherwise><xsl:call-template name="commande2"/></xsl:otherwise>
		</xsl:choose>
    </xsl:template>
    
	<xsl:template name="displayRow_commande">
		<xsl:param name="context"/>
		<xsl:param name="j"/>
		<xsl:param name="error_class"/>
		<tr class="resultsCorps" id="{concat('altern',$j mod 2)}">
			<td class="resultsCorps" style="word-break :break-all" width="100">
				<input type="hidden" name="t_panier_doc[][ID_LIGNE_PANIER]" value="{ID_LIGNE_PANIER}" />
				<input type="hidden" name="t_panier_doc[][ID_DOC]" value="{ID_DOC}" />
				<a href="index.php?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}">
					<xsl:value-of select="DOC/t_doc/DOC_COTE"/>
				</a>
			</td>

			<td class="resultsCorps" width="10%" style="word-break :break-all;" >
				<a href="index.php?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}">
				<xsl:choose>
					<xsl:when test="PDOC_EXTRAIT = '1'">
						<xsl:value-of select="DOC/t_doc/DOC_TITRE"/> - <xsl:value-of select="PDOC_EXT_TITRE"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="DOC/t_doc/DOC_TITRE"/>
					</xsl:otherwise>
				</xsl:choose>
				</a>
				<xsl:if test="/select/t_panier/PAN_XML/commande/PANXML_MONTAGE/track/clipitem[position() = $j]/clip_params/print_text != ''">
					<br />
					<xsl:processing-instruction name="php">print kTexte." : " ; </xsl:processing-instruction>
					<xsl:call-template name="replace">
						<xsl:with-param name="old">\n</xsl:with-param>
						<xsl:with-param name="new">&lt;br /&gt;</xsl:with-param>
						<xsl:with-param name="stream" select="/select/t_panier/PAN_XML/commande/PANXML_MONTAGE/track/clipitem[position() = $j]/clip_params/print_text"/>
					</xsl:call-template>
				</xsl:if>
			</td>
			<td class="resultsCorps" width="60">
				<xsl:choose>
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='V'"><xsl:processing-instruction name="php">print kVideo</xsl:processing-instruction></xsl:when>
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='A'"><xsl:processing-instruction name="php">print kAudio</xsl:processing-instruction></xsl:when>
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='P' and $context='montage'">00:00:05 - <xsl:value-of select="MAT_WIDTH"/> x <xsl:value-of select="MAT_HEIGHT"/></xsl:when> <!-- A retoucher le jour où la durée est customisable -->
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='P'"><xsl:processing-instruction name="php">print kPhoto</xsl:processing-instruction></xsl:when>
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='D'"><xsl:processing-instruction name="php">print kDocument</xsl:processing-instruction></xsl:when>
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='R'"><xsl:processing-instruction name="php">print kReportage</xsl:processing-instruction></xsl:when>
				</xsl:choose>	
			</td>
			<td class="resultsCorps" width="80">
				<xsl:choose>
					<xsl:when test="PDOC_EXTRAIT = '1'"><xsl:processing-instruction name="php">print kExtrait</xsl:processing-instruction></xsl:when>
					<xsl:otherwise><xsl:value-of select="DOC/t_doc/TYPE_DOC"/></xsl:otherwise>
				</xsl:choose>
			</td>
			<td class="resultsCorps" width="120">
				<xsl:value-of select="MAT_FORMAT"/>
			</td>
			<td class="resultsCorps" width="80">
				<xsl:choose>
					<xsl:when test="(DOC/t_doc/DOC_ID_MEDIA='V' or DOC/t_doc/DOC_ID_MEDIA='A') and PDOC_EXTRAIT='1'"><xsl:value-of select="EXT_DUREE"/></xsl:when>
					<xsl:when test="(DOC/t_doc/DOC_ID_MEDIA='V' or DOC/t_doc/DOC_ID_MEDIA='A')"><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:when>
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='P'"><xsl:value-of select="MAT_WIDTH"/> x <xsl:value-of select="MAT_HEIGHT"/></xsl:when>
					<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='D'"><xsl:value-of select="MAT_NB_PAGES"/><xsl:processing-instruction name="php">print kPages;</xsl:processing-instruction></xsl:when>
				</xsl:choose>
				&#160;
			</td>
			 <td class="resultsCorps" width="60">
				<xsl:value-of select="ETAT_ARCH"/>
			 &#160;
			 </td>
			<xsl:if test="$context != 'montage'">
				<td>
					<xsl:choose>
						<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='V' and ( DOC/t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_TYPE='MASTER']/MAT_FORMAT='DCP' or DOC/t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_TYPE='MASTER']/MAT_FORMAT='DPX' )">
							<xsl:processing-instruction name="php">print kLivraisonFichierOrigine;</xsl:processing-instruction>
							<input type="hidden" name="t_panier_doc[][PDOC_SUPPORT_LIV]" value="100"/>
							<input type="hidden" name="id_proc_{ID_LIGNE_PANIER}" value="100"/>&#160;
						</xsl:when>
						<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='V' and $profil&lt;=2">
							<xsl:value-of select="DOC/t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_TYPE='MASTER']/MAT_FORMAT"/>
							<select id="id_proc{ID_LIGNE_PANIER}" class="id_proc_lignes media_type_V" onchange="livraison_fichier_visionnage(this);" name="t_panier_doc[][PDOC_SUPPORT_LIV]">
								&lt;? listOptions("(SELECT ID_PROC as ID, PROC_NOM as VAL from t_proc WHERE ID_PROC in (100,160) ORDER BY ID) UNION ALL (SELECT ID_PROC as ID, PROC_NOM as VAL FROM t_proc WHERE  ID_PROC &gt;= 100 AND ID_PROC&lt;190 AND ID_PROC not in (100,160) ORDER BY PROC_NOM ASC)",Array("ID","VAL"));?&gt;
							</select>
							<xsl:if test="PDOC_SUPPORT_LIV!=''  and PDOC_SUPPORT_LIV!='&#160;'">
								<script>$j('#id_proc<xsl:value-of select="ID_LIGNE_PANIER"/> option[value="<xsl:value-of select="PDOC_SUPPORT_LIV"/>"]').prop('selected', true)</script>
							</xsl:if>
							<input type="hidden" class="doc_crit_mat_val" name="crit_mat[{ID_LIGNE_PANIER}][MAT_TYPE]" value="MASTER"/>
						</xsl:when>
						<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='V'">
							<select id="id_proc{ID_LIGNE_PANIER}" class="id_proc_lignes media_type_V" onchange="copyToSupportLiv(this);livraison_fichier_visionnage(this);" name="id_proc_{ID_LIGNE_PANIER}">
								&lt;? listOptions("(SELECT ID_PROC as ID, PROC_NOM as VAL from t_proc WHERE ID_PROC in (100,160) ORDER BY ID) UNION ALL (SELECT ID_PROC as ID, PROC_NOM as VAL FROM t_proc WHERE  ID_PROC &gt;= 100 AND ID_PROC&lt;190 AND ID_PROC not in (100,160) ORDER BY PROC_NOM ASC)",Array("ID","VAL"));?&gt;
							</select>
							<input type="hidden" class="copy_pdoc_support_liv" name="t_panier_doc[][PDOC_SUPPORT_LIV]" value="{PDOC_SUPPORT_LIV}"/>
							<xsl:if test="PDOC_SUPPORT_LIV!='' and PDOC_SUPPORT_LIV!='&#160;'">
								<script>$j('#id_proc<xsl:value-of select="ID_LIGNE_PANIER"/> option[value="<xsl:value-of select="PDOC_SUPPORT_LIV"/>"]').prop('selected', true)</script>
							</xsl:if>
							<input type="hidden" class="doc_crit_mat_val" name="crit_mat[{ID_LIGNE_PANIER}][MAT_TYPE]" value="MASTER"/>
						</xsl:when>
						<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='P' and $profil &lt;=2">
							<select id="id_proc{ID_LIGNE_PANIER}" class="id_proc_lignes media_type_P" name="t_panier_doc[][PDOC_SUPPORT_LIV]">
								&lt;? listOptions("(SELECT ID_PROC as ID, PROC_NOM as VAL  from t_proc WHERE ID_PROC in (100,160) ORDER BY ID) UNION ALL (SELECT ID_PROC as ID, PROC_NOM as VAL FROM t_proc WHERE (ID_PROC&gt;=200 AND ID_PROC&lt;300) ORDER BY PROC_NOM ASC)",Array("ID","VAL"));?&gt;
							</select>
							<xsl:if test="PDOC_SUPPORT_LIV!=''  and PDOC_SUPPORT_LIV!='&#160;'">
								<script>$j('#id_proc<xsl:value-of select="ID_LIGNE_PANIER"/> option[value="<xsl:value-of select="PDOC_SUPPORT_LIV"/>"]').prop('selected', true)</script>
							</xsl:if>
						</xsl:when>
						<xsl:when test="DOC/t_doc/DOC_ID_MEDIA='P'">
							<select id="id_proc{ID_LIGNE_PANIER}" class="id_proc_lignes media_type_P" onchange="copyToSupportLiv(this)" name="id_proc_{ID_LIGNE_PANIER}">
								&lt;? listOptions("(SELECT ID_PROC as ID, PROC_NOM as VAL  from t_proc WHERE ID_PROC in (100,160) ORDER BY ID) UNION ALL (SELECT ID_PROC as ID, PROC_NOM as VAL FROM t_proc WHERE (ID_PROC&gt;=200 AND ID_PROC&lt;300) ORDER BY PROC_NOM ASC)",Array("ID","VAL"));?&gt;
							</select>
							<input type="hidden" class="copy_pdoc_support_liv" name="t_panier_doc[][PDOC_SUPPORT_LIV]" value="{PDOC_SUPPORT_LIV}"/>
							<xsl:if test="PDOC_SUPPORT_LIV!='' and PDOC_SUPPORT_LIV!='&#160;'">
								<script>$j('#id_proc<xsl:value-of select="ID_LIGNE_PANIER"/> option[value="<xsl:value-of select="PDOC_SUPPORT_LIV"/>"]').prop('selected', true)</script>
							</xsl:if>
						</xsl:when>
						<xsl:when test="DOC/t_doc/DOC_ID_MEDIA!='V' and DOC/t_doc/DOC_ID_MEDIA!='P' and $profil&lt;=2">
							<xsl:processing-instruction name="php">print kLivraisonFichierOrigine;</xsl:processing-instruction>
							<input type="hidden" name="t_panier_doc[][PDOC_SUPPORT_LIV]" value="100"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:processing-instruction name="php">print kLivraisonFichierOrigine;</xsl:processing-instruction>
							<input type="hidden" name="id_proc_{ID_LIGNE_PANIER}" value="100"/>&#160;
							<input type="hidden" name="t_panier_doc[][PDOC_SUPPORT_LIV]" value="100"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				<td class="resultsCorps" width="20" align="center">
					<img class="icoTrash" name="btSup{$j}" style="cursor:pointer" src="design/images/icn-trash-grey.png" onclick="removeLine({ID_LIGNE_PANIER},null,'SUP',this)" title="&lt;?php echo kSupprimer; ?&gt;"/>
				</td>
			</xsl:if>
		</tr>
	</xsl:template>

</xsl:stylesheet>

