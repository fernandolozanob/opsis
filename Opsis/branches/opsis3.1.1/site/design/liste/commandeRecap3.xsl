<?xml version="1.0" encoding="utf-8"?>
<!-- commande2.xsl (Achats) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>


<xsl:param name="playerVideo"/>
<xsl:param name="imgurl"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="msgcommande" />


    <xsl:template name='commandeRecap3'>

		<div id='pageTitre' ><xsl:processing-instruction name="php">print kCommandeRecap;</xsl:processing-instruction></div>

	<xsl:if test="$msgcommande!=''">
		<h3><xsl:value-of select="$msgcommande"/></h3>
	</xsl:if>

        <table width='95%' border='0' align="center" class="resultsMenu">
            <tr>
                <td align="right" width="33%">
					<a href="javascript:popupPrint('export.php?impression=1&amp;export=commandeImp3&amp;id={t_panier/ID_PANIER}&amp;type=panier&amp;tri={$tri}','main')">
										<xsl:processing-instruction name="php">print kImprimer;</xsl:processing-instruction></a>
				</td>
			</tr>
		</table>
	    <fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'>
    	  <legend><xsl:processing-instruction name="php">print kInfosCommande;</xsl:processing-instruction></legend>

          <table width="95%" cellspacing="5" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center">
			
            <tr>
                <td class="label_champs_form" width="20%"><xsl:processing-instruction name="php">print kTypeCommande;</xsl:processing-instruction></td>
                <td class="commande_champs_form" align="left">
                	<xsl:value-of select="t_panier/TYPE_COMMANDE"/>
                </td>
				
                <td class="label_champs_form"><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction> :</td>
                <td class="val_champs_form">
                	<xsl:value-of select="t_panier/t_usager/US_NOM"/> <xsl:value-of select="t_panier/t_usager/US_PRENOM"/>
                </td>
             </tr>
			 
             <tr>
                <td class="label_champs_form" width="20%"><xsl:processing-instruction name="php">print kDateCommande;</xsl:processing-instruction></td>
                <td class="val_champs_form" align="left">
					<xsl:call-template name="format_date">
						<xsl:with-param name="chaine_date" select="t_panier/PAN_DATE_COM"/>
					</xsl:call-template>
                </td>
				
				<xsl:if test="t_panier/t_usager/US_SOCIETE!=''">
					<td class="label_champs_form"><xsl:processing-instruction name="php">print kSociete;</xsl:processing-instruction> :</td>
					<td class="val_champs_form">
						<xsl:value-of select="t_panier/t_usager/US_SOCIETE"/>
					</td>
				</xsl:if>
            </tr>
			
            <tr height="1"><td colspan="4" style="padding:0px;spacing:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>
			
			<tr height="25">
				<td class="label_champs_form"><xsl:processing-instruction name="php">print kNumeroCommande;</xsl:processing-instruction></td>
				<td class="val_champs_form" colspan="3">
					<xsl:value-of select="t_panier/ID_PANIER"/>
				</td>
			</tr>
			<xsl:if test="t_panier/PAN_ID_TRANS!=''">
				<tr height="25">
					<td class="label_champs_form"><xsl:processing-instruction name="php">print kNumeroTransaction;</xsl:processing-instruction></td>
					<td class="val_champs_form" colspan="3">
						<xsl:value-of select="t_panier/PAN_ID_TRANS" />
					</td>
				</tr>
			</xsl:if>
			
			<tr height="60">
				<xsl:if test="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kAdresseLivraison;</xsl:processing-instruction></td>
					<td class="val_champs_form" valign="top">
						<xsl:call-template name="break" >
						   <xsl:with-param name="text">
								<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON"/>
						   </xsl:with-param>
						 </xsl:call-template>
					</td>
				</xsl:if>
					
				<xsl:if test="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kAdresseFacturation;</xsl:processing-instruction></td>
					<td class="val_champs_form" valign="top">
						<xsl:call-template name="break" >
						   <xsl:with-param name="text">
								<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION"/>
						   </xsl:with-param>
						 </xsl:call-template>
					</td>
				</xsl:if>
			</tr>
			 
			<tr height="20">
				<xsl:if test="t_panier/PAN_XML/commande/PANXML_TVA_INTRA!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kNumeroTVA;</xsl:processing-instruction></td>
					<td class="val_champs_form">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_TVA_INTRA"/>
					 </td>
				</xsl:if>

				<xsl:if test="t_panier/PAN_XML/commande/PANXML_DESTINATION!=''">
					<td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kDestination;</xsl:processing-instruction></td>
					<td class="val_champs_form">
						<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_DESTINATION"/>
					</td>
				</xsl:if>
            </tr>

			<xsl:if test="t_panier/PAN_XML/commande/PANXML_CONDITION_EXPLOITATION!=''">
             <tr height="20">
                <td class="label_champs_form" valign="top"><xsl:processing-instruction name="php">print kConditionExploitation;</xsl:processing-instruction></td>
                <td class="val_champs_form">
					<xsl:call-template name="break" >
					   <xsl:with-param name="text">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_CONDITION_EXPLOITATION"/>
					   </xsl:with-param>
					 </xsl:call-template>
                </td>
             </tr>
			</xsl:if>


            <tr height="1"><td colspan="4" style="padding:0px;spacing:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>
			<xsl:if test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT!=''">
            <tr>
                <td class="label_champs_form"><xsl:processing-instruction name="php">print kModePaiement;</xsl:processing-instruction></td>
                <td class="val_champs_form" colspan="3">
					<xsl:choose>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CHEQUE'" >
							<xsl:processing-instruction name="php">echo kModePaiementCheque</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CR'" >
							<xsl:processing-instruction name="php">echo kModePaiementCR</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CB'" >
							<xsl:processing-instruction name="php">echo kModePaiementCB</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='VISA'" >
							<xsl:processing-instruction name="php">echo kModePaiementVISA</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='EUROCARD_MASTERCARD'" >
							<xsl:processing-instruction name="php">echo kModePaiementEUROCARD_MASTERCARD</xsl:processing-instruction>
						</xsl:when>
						<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='E_CARD'" >
							<xsl:processing-instruction name="php">echo kModePaiementE_CARD</xsl:processing-instruction>
						</xsl:when>
					</xsl:choose>
				</td>
               </tr>
			</xsl:if>
				
        </table>
  		</fieldset>

	  <fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'>
	  <legend><xsl:processing-instruction name="php">print kContenuCommande;</xsl:processing-instruction></legend>

        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
            <tr>
                <td class="resultsHead" width="50"><xsl:processing-instruction name="php">print kCote;</xsl:processing-instruction></td>
                <td class="resultsHead" width="100"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kFonds;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></td>
                <td class="resultsHead" width="150"><xsl:processing-instruction name="php">print kExtrait;</xsl:processing-instruction></td>
                <td class="resultsHead" width="60"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kSupportLivraison;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kNb;</xsl:processing-instruction></td>
				<!--td class="resultsHead"><xsl:processing-instruction name="php">print kVersion;</xsl:processing-instruction></td-->
                <td class="resultsHead" width='61'><xsl:processing-instruction name="php">print kPrixHT;</xsl:processing-instruction></td>
            </tr>
            <xsl:for-each select="t_panier_doc">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="resultsCorps" id="{concat('altern',$j mod 2)}">
                    <td class="resultsCorps" width="50"><xsl:value-of select="DOC/t_doc/DOC_COTE"/></td>
                    <td class="resultsCorps" width="100"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></td>
                    <td class="resultsCorps" width="80">
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="DOC/t_doc/DOC_DATE_PROD"/>
						</xsl:call-template>
					</td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/DOC_XML/XML/DOC/FONDS"/></td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/TYPE_DOC"/></td>
                    <td class="resultsCorps" width="150">
						<xsl:if test="PDOC_EXTRAIT='1'">
						<xsl:value-of select="PDOC_EXT_TITRE"/><br/>
						[<xsl:value-of select="PDOC_EXT_TCIN"/> - <xsl:value-of select="PDOC_EXT_TCOUT"/>]
						</xsl:if>
						&#160;
					</td>
                    <td class="resultsCorps" width="60">
						<xsl:choose>
							<xsl:when test="PDOC_EXTRAIT='1'"><xsl:value-of select="EXT_DUREE"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:otherwise>
						</xsl:choose>
						&#160;
					</td>
                    <td class="resultsCorps"><xsl:value-of select="PDOC_SUPPORT_LIV"/></td>
                    <td class="resultsCorps"><xsl:value-of select="PDOC_NB_SUP"/></td>
                    <td class="resultsCorps"><xsl:value-of select="PDOC_VERSION"/></td>
					<td class="resultsCorps" style="text-align:right;padding-right:5px;">
					  <xsl:choose>
						  <xsl:when test="PDOC_PRIX_CALC!='&#160;'"><xsl:value-of select="PDOC_PRIX_CALC"/> €</xsl:when>
						  <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
					  </xsl:choose>
					</td>
                </tr>
            </xsl:for-each>
		</table>
		 <xsl:variable name="total_ht"><xsl:value-of select="t_panier/TOTAL_HT" /></xsl:variable>
		 <xsl:variable name="frais_ht"><xsl:value-of select="t_panier/FRAIS_HT" /></xsl:variable>
		 <xsl:variable name="pan_tva"><xsl:value-of select="t_panier/PAN_TVA" /></xsl:variable>
		 <xsl:variable name="total_tva"><xsl:value-of select="round(($total_ht + $frais_ht)* $pan_tva) * 0.01" /></xsl:variable>
		 <xsl:variable name="total_ttc"><xsl:value-of select="round(($total_ht + $frais_ht)* (100 + $pan_tva)) * 0.01" /></xsl:variable>
			 <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
			   <tr class="resultsCorps" id="altern0">
			     <td style="font-size:13px;text-align:right;">
			     	<xsl:processing-instruction name="php">print kPanierTotalProduits;</xsl:processing-instruction>
			     </td>
			     <td  style="font-size:13px;text-align:right;" width='60'>
			       	<div id="total_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/TOTAL_HT" /> €</xsl:if> 
				</div>
			     </td>
			   </tr>
			   <tr class="resultsCorps" id="altern1">
			   	<td style='text-align:right'>
			     	<xsl:processing-instruction name="php">print kPanierFrais;</xsl:processing-instruction>
			     </td>
			     <td style='text-align:right'>
			       <div id="frais_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/FRAIS_HT" /> €</xsl:if> 
				</div>
			     </td>

			   </tr>

			   <tr class="resultsHead" id="altern1">
			     <td style='text-align:right' class="resultsHead">
			     	<xsl:processing-instruction name="php">print kPanierTotalHT;</xsl:processing-instruction>
			     </td>
			     <td style='text-align:right' class="resultsHead">
			        <div id="total_frais_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="format-number($total_ht+$frais_ht,'0.00')" /> €</xsl:if> 
				</div>
			     </td>

			   </tr>
			   <tr class="resultsHead" id="altern1">
				 <td style='text-align:right' class="resultsHead">
					<xsl:processing-instruction name="php">print kTotalTVA;</xsl:processing-instruction>
				 </td>
				 <td style='text-align:right' class="resultsHead">
					<div id="total_tva">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="format-number($total_tva,'0.00')" /> €</xsl:if> 
				</div>
				 </td>
			   </tr>

			   <tr class="resultsHead" id="altern1">
				 <td style='text-align:right' class="resultsHead">
					<xsl:processing-instruction name="php">print kPanierTotalTTC;</xsl:processing-instruction>
				 </td>
				 <td style='text-align:right' class="resultsHead">
					<div id="total_ttc" >
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="format-number($total_ttc,'0.00')" /> €</xsl:if> 
				</div>
				 </td>
			   </tr>
			 </table>
  	</fieldset>

	<br/>

    </xsl:template>


</xsl:stylesheet>

