<?xml version="1.0" encoding="utf-8"?>
<!-- historiqueListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<!-- recuperation de parametres PHP -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />
<xsl:param name="type_requests" />

    <xsl:template match='/select'>

    <script>
    function displayFormSauve(id,ctrl) {
    	myDiv=document.getElementById('sauvRequete');

		$j(myDiv).find('span.save_msg').remove();
		$j("#sauvRequete").find('form').css('display','');
		
    	if (myDiv.style.display=='block') {
    		_continue=confirm ('continuer ?');
    		if (!_continue) return false;
    	}
    	myDiv.style.display='block';

    	document.getElementById('id_req').value=id;
    	document.getElementById('requete').value='';

    	y=getTop(ctrl);
    	x=getLeft(ctrl);
    	myDiv.style.top=(y-100)+"px"; // 100 arbitraire
    	myDiv.style.left=(x-253)+"px";

    }

    function cancelForm() {

    	document.getElementById('id_req').value='';
    	document.getElementById('requete').value='';
    	document.getElementById('sauvRequete').style.display='none';

    }

    function chkFields() {
    	if (document.getElementById('requete').value.Trim()=='') return false;
    	return true;

    }
	
	function sauvRequeteAjax(){
		if(chkFields()){
			form = $j("#sauvRequete").find('form');
			$j.ajax({
				url     : $j(form).attr('action'),
				method    : 'POST',
				data    : $j(form).serialize(),
				success : function( data ) {
				console.log("success : ",data)
				id_req = $j("#id_req").val();
				$j("#save"+id_req).attr('src','design/images/saved.gif');
				$j("#save"+id_req).removeAttr('onclick');
				$j("#sauvRequete").find('form').css('display','none');
				$j("#sauvRequete").find('form').before('<span class="save_msg">&lt;?= kRequeteSauvegardee ?&gt;</span>');
				},
				error   : function( xhr, err ) {
					console.log(err);
				}
			});
		}
		return false ;
	}
	
	</script>
	
	
	<div id='sauvRequete' style='opacity:0.9;filter:alpha(opacity=90);display:none;position:absolute;border:2px solid black;background-color:#FFFFFF;width:250px; padding : 5px 23px 5px 5px; '>
		<form onsubmit='return sauvRequeteAjax()' class='val_champs_form' name='form1' method='POST' action='{$scripturl}?urlaction=requeteListe'>
			<input type='hidden' id='id_req' name='REQUETE[ID_REQ]' value='' />
			<xsl:processing-instruction name="php">print kNom;</xsl:processing-instruction> :<input type='text' class='val_champs_form' id='requete' name='REQUETE[REQUETE]' value='' />
			<!--input type='image' src='design/images/save.gif' value='SAVE' /-->
			<img src='design/images/save.gif' style="cursor:pointer;" onclick="sauvRequeteAjax()" />
		</form>
		<!--input type='image' src='design/images/abandon.gif' style="position :absolute; right :4px;" onclick='cancelForm();return false;' /-->
		<img src='design/images/abandon.gif' style="position:absolute;right:4px;top:1px;cursor:pointer;" onclick='cancelForm();return false;' />
	</div>
	
	
	<form name="documentSelection" method="post" action="{$scripturl}?urlaction=requeteListe{$urlparams}">
	 <input type="hidden" name="page" value="{$page}" />
	 <input type="hidden" name="nbLignes" value="{$nbLigne}"  />
	 <input type="hidden" name="tri" value="" />
	 
	 <xsl:call-template name="pager">
		 <xsl:with-param name="form">documentSelection</xsl:with-param>
		 <xsl:with-param name="entity">requete</xsl:with-param>
	 </xsl:call-template>
	 
		<div class="feature" align="center">
			<style>
				.style_full_,.style_session_full {padding:2px 5px 0px 5px;color:#666666;font-family:"Trebuchet MS",sans-serif;font-weight:normal;border:1px solid #888888;-moz-border-radius-topleft:8px;-moz-border-radius-topright:8px;  }
				.style_full_full,.style_session_ {padding:2px 5px 0px 5px;color:#333333;font-family:"Trebuchet MS",sans-serif;font-weight:bold;border:1px solid #888888;background-color:#DDDDDD;-moz-border-radius-topleft:8px;-moz-border-radius-topright:8px;  }
		
		
			</style>

			<div id="histoScrollable">
				<div id="histoFull">
				
				<div id="filtreRequetes">
					<input type="radio" name="type_requests" value="saved" onClick="documentSelection.submit();">
						<xsl:if test="$type_requests='saved'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
					</input>
					<xsl:text>&lt;?=kRequetesSauvegardeesUniquement ?&gt;</xsl:text>
					<input type="radio" name="type_requests" value="session" onClick="documentSelection.submit();" >
						<xsl:if test="$type_requests='session'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
					</input>
					<xsl:text>&lt;?=kSessionEnCours ?&gt;</xsl:text>
					<input type="radio" name="type_requests" value="session_saved" onClick="documentSelection.submit();" >
						<xsl:if test="$type_requests='session_saved'"><xsl:attribute name="checked">1</xsl:attribute></xsl:if>
					</input>
					<xsl:text>&lt;?=kToutesMesRequetes ?&gt;</xsl:text>
				</div>
				<xsl:variable name="has_name" select="count(t_requete[normalize-space(REQUETE)!='' and REQUETE != '&#160;']) &gt; 0" />
		        <table width="80%" border="0" cellspacing="0"  align="center" class="tableResults">
		            <tr>
		                <td class="resultsHead" width="20px">&#160;</td>
						 <td class="resultsHead" width="20px">&#160;</td>
		                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>
		                <xsl:if test="$has_name"><td class="resultsHead"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td></xsl:if>
		                <td class="resultsHead"><xsl:processing-instruction name="php">print kRequete;</xsl:processing-instruction></td>
		                <td class="resultsHead" width="80px"><xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction></td>
		                <td class="resultsHead" width="20px"> </td>
		                <td class="resultsHead" width="20px"> </td>
		            </tr>
		            
		            
	            <xsl:for-each select="t_requete">
	                <xsl:variable name="j"><xsl:number/></xsl:variable>
	                <tr class="{concat('req_saved',REQ_SAVED)}">
					<td class="resultsCorps">
						<input type="checkbox" name="checkbox_requete[]" value="{ID_REQ}" onclick="checkHisto();" />
					</td>
                    <td class="resultsCorps">
					    <a href="{$scripturl}?urlaction=docListe&amp;hist={ID_REQ}"><img src="design/images/refresh.gif" border="0" title="&lt;?php echo kLancer; ?&gt;" alt="&lt;?php echo kLancer; ?&gt;"/></a>
					</td>
                    <td class="resultsCorps"><a href="{$scripturl}?urlaction=docListe&amp;hist={ID_REQ}"><xsl:value-of select="REQ_DATE_EXEC"/></a></td>
                    <xsl:if test="$has_name"> <td class="resultsCorps"><a href="{$scripturl}?urlaction=docListe&amp;hist={ID_REQ}"><xsl:value-of select="REQUETE"/></a></td></xsl:if>
                    <td class="resultsCorps"><a href="{$scripturl}?urlaction=docListe&amp;hist={ID_REQ}"><xsl:value-of select="REQ_LIBRE"/></a></td>
                    <td class="resultsCorps"><xsl:value-of select="REQ_NB_DOC"/></td>
                    <td class="resultsCorps">
						<xsl:if test="$profil>0">
                        <xsl:choose>
                            <xsl:when test="REQ_SAVED=1">
                                <img src="design/images/saved.gif" title="&lt;?php echo kEnregistrer; ?&gt;" alt="&lt;?php echo kEnregistrer; ?&gt;" border="0"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <img style='cursor:pointer' onclick="javascript:displayFormSauve({ID_REQ},this)" id="save{ID_REQ}" src="design/images/save.gif" title="&lt;?php echo kEnregistrer; ?&gt;" alt="&lt;?php echo kEnregistrer; ?&gt;" border="0"/>
                            </xsl:otherwise>
                        </xsl:choose>
  						</xsl:if>
                  </td>
				  <td>
					<a href="{$scripturl}?urlaction=requeteListe&amp;suppr={ID_REQ}"><img src="design/images/icn-trash-grey.png" alt="supprimer" /></a>
				  </td>
                </tr>
            </xsl:for-each>
	        </table>
		</div>
		</div>
    </div>
		<div id="crossSrchBtns" class="std_form_btn_wrapper" align="center">
			<select name="operateur">
				<option value="AND"><xsl:processing-instruction name="php">print kEt;</xsl:processing-instruction></option>
				<option value="OR"><xsl:processing-instruction name="php">print kOu;</xsl:processing-instruction></option>
				<option value="AND NOT"><xsl:processing-instruction name="php">print kSauf;</xsl:processing-instruction></option>
			</select>
				
			<input type="submit" value="Croiser" disabled="true" class="std_button_style"  name="valider_form_croisement" />
		</div>

     </form>


    </xsl:template>
</xsl:stylesheet>
