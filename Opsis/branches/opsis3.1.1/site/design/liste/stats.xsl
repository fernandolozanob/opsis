<?xml version="1.0" encoding="utf-8"?><!-- stats.xsl --><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output        method="xml"        encoding="utf-8"        omit-xml-declaration="yes"        indent="yes"/><!-- recuperation de parametres PHP --><xsl:param name="scripturl" />
<xsl:param name="ordre" />
<xsl:param name="tri" />
<xsl:param name="gestPers"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />
    <xsl:template match='/select'>
<form name="documentSelection" method="post" action="{$scripturl}?urlaction=stats{$urlparams}">
	 <input type="hidden" value="{$page}" name="page" />
	 <input type='submit' value='' style='width:0px;height:0px;visibility:hidden;'/>

 <table width="95%" border="0" class="resultsMenu" align="center">
    <tr>
        <td align="left" width="33%">
 			<xsl:value-of select="$nb_rows" />&#160;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>
        </td>
        <td align="center" width="33%">
 			<xsl:if test="$nb_pages>1">
				<xsl:if test="$page>1"><a href="#" onclick='javascript:document.documentSelection.page.value={$page - 1};document.documentSelection.submit();'><img src="design/images/left_pg.gif" alt="" border="0" width="11" height="12" /></a> </xsl:if>
				<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onChange='javascript:document.documentSelection.page.value=eval(this.value);document.documentSelection.submit();' />
				<xsl:value-of select="concat(' / ',$nb_pages)"/>
				<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:document.documentSelection.page.value={$page + 1};document.documentSelection.submit();'><img src="design/images/right_pg.gif" alt="" border="0" width="11" height="12" /></a></xsl:if>
			</xsl:if>
        </td>
        <td align="right" width="33%">
             <a href="javascript:popupPrint('export.php?impression=stats&amp;export=stats&amp;sql=stats_sql&amp;type=stat&amp;nb={$nbLigne}&amp;page=1','main')">
            <xsl:processing-instruction name="php">print kImprimer;</xsl:processing-instruction>
            </a>
            <xsl:text> | </xsl:text>
            <a href="javascript:popupPrint('export.php?sql=stat_sql&amp;type=stat&amp;nb={$nbLigne}&amp;page=1','main')">
            <xsl:processing-instruction name="php">print kExporter;</xsl:processing-instruction>
            </a>
        </td>
    </tr>

    <tr>

        <td align="left" colspan="3">

                <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
                <select class='val_champs_form' name="nbLignes" onchange="javascript:document.documentSelection.submit()">
                    <option value="5"><xsl:if test="$nbLigne=5"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>5</option>
                    <option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
                    <option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
                    <option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
                    <option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
                    <option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
                </select>

		</td>
    </tr>
</table>



        <table width="80%" align="center" border="0" cellspacing="0" class="tableResults">        <!-- NOM DES COLONNES -->            <tr>				<xsl:for-each select="stat[position() = 1]/child::*">					<td class="resultsHead"><xsl:value-of select="name()"/></td>				</xsl:for-each>            </tr>        <!-- VALEURS -->            <xsl:for-each select="stat">                <tr>					<xsl:for-each select="child::*">						<td class="resultsCorps"><xsl:value-of select="."/></td>					</xsl:for-each>                </tr>            </xsl:for-each>        </table>
 <table width="95%" border="0" class="resultsMenu" align="center">
    <tr>
        <td align="left" width="33%">

 			<xsl:value-of select="$nb_rows" />&#160;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>


        </td>
        <td align="center" width="33%">
 			<xsl:if test="$nb_pages>1">
				<xsl:if test="$page>1"><a href="#" onclick='javascript:document.documentSelection.page.value={$page - 1};document.documentSelection.submit();'><img src="design/images/left_pg.gif" alt="" border="0" width="11" height="12" /></a> </xsl:if>
				<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onChange='javascript:document.documentSelection.page.value=eval(this.value);document.documentSelection.submit();' />
				<xsl:value-of select="concat(' / ',$nb_pages)"/>
				<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:document.documentSelection.page.value={$page + 1};document.documentSelection.submit();'><img src="design/images/right_pg.gif" alt="" border="0" width="11" height="12" /></a></xsl:if>
			</xsl:if>
        </td>
        <td align="left" width="50%">

        </td>
    </tr>

</table>

     </form>


    </xsl:template></xsl:stylesheet>