<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
    <xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
    /> 

    <!--<xsl:param name="xmlfile" />-->
    <xsl:param name="sousTitre_extension" />

    <xsl:template match='t_doc'>
        
        
        <xsl:variable name="id_doc">
            <xsl:value-of select="ID_DOC" />
        </xsl:variable>

       <!--sousTitre_extension : <xsl:copy-of select="$sousTitre_extension" />-->
        <!--<xsl:variable name="xmllist" select="document($xmlfile)"/>-->
            
            
<!--        <xsl:if test="contains('mp1',substring-after(t_doc_mat/DOC_MAT/t_mat/MAT_NOM,'.'))">
            
            <xsl:value-of select="t_doc_mat/DOC_MAT/t_mat/MAT_NOM" />
            
        </xsl:if>-->
         
        &lt;?php  
        
        $sousTitreFileExist=false;
        
      
        
        ?&gt; 
        <xsl:for-each select="t_doc_mat/DOC_MAT/t_mat">
        
        &lt;?php       
            
            $tab_mat=explode('.',"<xsl:value-of select="MAT_NOM" />");
            $mat_ext=array_pop($tab_mat);
            
            if (strpos("<xsl:value-of select="$sousTitre_extension" />",$mat_ext)!==FALSE){
            $sousTitreFileExist=true;
            }
            
         
            ?&gt;    
        
        </xsl:for-each>
            
        &lt;?php 
        
        if ($sousTitreFileExist) {
            
        ?&gt;
        
        
        <div id="listAllSubtitledFilesLINK">
            <img src="design/images/arrow_down.gif"/>
                <xsl:processing-instruction name="php"> echo kListe . '    ' . strtolower(kSousTitres) </xsl:processing-instruction>
        </div>
        
            <div id="listAllSubtitledFiles">
				<table>
					<tr class="tr_border_bottom">
						<th>
							<xsl:processing-instruction name="php">print kFichier;</xsl:processing-instruction>
						</th>
						<th>
							<xsl:processing-instruction name="php">print kLangue;</xsl:processing-instruction>
						</th>
						<th>
							<xsl:processing-instruction name="php">print kStatut;</xsl:processing-instruction>
						</th>
						<th>
							&#160;
						</th>
					</tr>
				
				
				<xsl:for-each select="t_doc_mat/DOC_MAT">
					<xsl:sort select="t_mat/ID_MAT" data-type="number" order="descending" /><!-- frame/prepareVisu met dans la balise <track> le dernier mat sous-titre par langue, par ordre id_mat décroissant-->
					
					<xsl:if test="t_mat/MAT_FORMAT='VTT' or t_mat/MAT_FORMAT='SRT'">
						
						<xsl:variable name="classMat">&lt;?php if ($_SESSION['sousTitrageEditionVolee']['activeSubtitle'][<xsl:value-of select="$id_doc" />]['id_mat']=='<xsl:value-of select="t_mat/ID_MAT" />') { echo  'active'; }?&gt;</xsl:variable>
						
						
						<tr id="matSubtitle{t_mat/ID_MAT}" class="{$classMat}">
							<td>
							  <xsl:value-of select="t_mat/MAT_NOM" />
							  -<xsl:value-of select="t_mat/ID_MAT" />
							</td>
						
					
							<td>
								<xsl:value-of select="t_mat/t_mat_val/TYPE_VAL[@ID_TYPE_VAL='LST']/t_val/VALEUR" />
							</td>
				  
				
							<td>
								<xsl:choose>
									<xsl:when test="DMAT_INACTIF=1">
										<span class="unpublished">
											<xsl:processing-instruction name="php">print kNonPublie;</xsl:processing-instruction>
										</span>

									</xsl:when>
									<xsl:otherwise>
										<xsl:processing-instruction name="php">print kPublie;</xsl:processing-instruction>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							
							<td>
								<input type="button" class="std_button_style" onclick="askSaveThenShowOther({t_mat/ID_MAT},{$id_doc})" >
									<xsl:attribute name="value">&lt;?print kModifier;?&gt;</xsl:attribute>
								</input>
							</td>
							
						</tr>
						
					</xsl:if>
				</xsl:for-each>
				<tr ><td colspan="10"><input type="button"  class="std_button_style" id="importSubtitle" value="&lt;?php echo kImporter ?&gt;"/></td></tr>
			</table>
        </div>
        &lt;?php 
        
        }
            
        ?&gt;
            
    </xsl:template>
        
                        
        
        

</xsl:stylesheet>
