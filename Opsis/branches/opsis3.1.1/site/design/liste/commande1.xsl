<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<!-- commande1.xsl (Droits) -->

<xsl:param name="playerVideo"/>
<xsl:param name="imgurl"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />


    <xsl:template name='commande1'>


		<script type="text/javascript">

		function refresh() {
			document.form1.target='frameCommande';
			document.form1.action='blank.php?urlaction=iframeCommande';
			document.form1.submit();
			document.form1.target='';
			document.form1.action='<xsl:value-of select="scripturl" />?urlaction=commande';

		}


		function chkFields (myform,redirectToPay,optFields) {
			var emptyFld=0;
			var cntChk, grp;

			for (var i=0;i&lt;myform.elements.length;i++) {
				myelmt=myform.elements[i];
				myelmt.className='val_champs_form';

				if(optFields.indexOf(myelmt.name)==-1){
					switch (myelmt.type) {
						case 'text':
						case 'textarea':
						case 'select-one':
							myelmt.value=myelmt.value.Trim();
							if  (myelmt.value.Trim()=='') {
							if (myelmt.value&lt;0) myelmt.value='1';

							myelmt.className='errormsg';
							emptyFld++; //on a trouvé un contrôle vide, pas bon !
							}
							else {myelmt.className='val_champs_form';}

							break;
						case 'radio' :
							cntChk=0;

							grp=myform[myelmt.name];
							for (var v=0;v&lt;grp.length;v++) {

								if (grp[v].checked==1) cntChk++;
							}
							if (cntChk==0) {
								myelmt.parentNode.className='errormsg';
								emptyFld++;
							} else
								{
								myelmt.parentNode.className='val_champs_form';
								}
							break;

						case 'checkbox' :


							if (myelmt.checked!=1) {
								myelmt.parentNode.className='errormsg';
								emptyFld++;
							} else {
								myelmt.parentNode.className='val_champs_form';
							}
							break;
					}
				}

			}

			if (emptyFld==0) {
				if (redirectToPay==true) {
					//document.paybox.submit();// paiement en ligne
					//document.form1.commande.value="PAYBOX";
					//calcTarifs();
				}
				else {
					myform.commande.value='VALID'; // pas de paiement (valid classique)
					myform.submit();
				}
			} else
			{
			alert('<xsl:processing-instruction name="php">print kErreurChampsOblig;</xsl:processing-instruction>');
			}

		}

		</script>

		<iframe src='' name='frameCommande' id='frameCommande' style='display:none;'>&#160;</iframe>
         <form name='form1' action='{$scripturl}?urlaction=commande' method='POST' >
         <input type='hidden' name='commande' value='' />
         <input type='hidden' name='id_panier' value='{t_panier/ID_PANIER}' />
         <input type='hidden' name='ligne' value='' />

    	  <fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'>
    	  <legend>Infos commande</legend>

         <table width="95%" cellspacing="5" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center">
			<tr>
			<td width="120"><img src="{$imgurl}pixel.gif" height="1"/></td>
			<td width="200"><img src="{$imgurl}pixel.gif" height="1"/></td>
			<td width="200"><img src="{$imgurl}pixel.gif" height="1"/></td>
			<td width="200"><img src="{$imgurl}pixel.gif" height="1"/></td>
			<td><img src="{$imgurl}pixel.gif" height="1"/></td>
			</tr>

            <tr>
                <td class="label_champs_form"><xsl:processing-instruction name="php">print kTypeCommande;</xsl:processing-instruction></td>
                <td class="commande_champs_form" >
                	<xsl:value-of select="t_panier/TYPE_COMMANDE"/>
                </td>
                <td class="val_champs_form">
                	<span class="label_champs_form" ><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction>&#160;:
                    </span>
                	<xsl:value-of select="t_panier/t_usager/US_NOM"/>&#160;<xsl:value-of select="t_panier/t_usager/US_PRENOM"/>
                </td>
                <td class="val_champs_form" colspan="2">
                 	<span class="label_champs_form" ><xsl:processing-instruction name="php">print kSociete;</xsl:processing-instruction>&#160;:
                    </span>
                    <xsl:value-of select="t_panier/t_usager/US_SOCIETE"/>
                </td>
            </tr>

            <tr height="1"><td colspan="5" style="padding:0px;margin:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>



				<tr height="25">
					<td class="label_champs_form">
					<xsl:processing-instruction name="php">print kNumeroCommande;</xsl:processing-instruction></td>
					<td class="val_champs_form" colspan="4">
						<xsl:value-of select="t_panier/ID_PANIER"/>
					</td>
					<input type='hidden' name='ID_PANIER' value='{t_panier/ID_PANIER}' />
				</tr>


                <tr height="60">
                <td/>
                <td class="val_champs_form">
                <div class="label_champs_form">
                <xsl:processing-instruction name="php">print kAdresseLivraison;</xsl:processing-instruction>
                </div>

                    <textarea name="PANXML_ADRESSE_LIVRAISON" rows="4" cols="40" style="width:220px;font-size:10px;font-family:Arial;"  >
						<xsl:choose>
							<xsl:when test="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON!=''">
								<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="t_panier/t_usager/US_SOC_ADRESSE"/><xsl:text>&#160;</xsl:text>
								<xsl:value-of select="t_panier/t_usager/US_SOC_ZIP"/><xsl:text>&#160;</xsl:text>
								<xsl:value-of select="t_panier/t_usager/US_SOC_VILLE"/>
							</xsl:otherwise>
						</xsl:choose>
					</textarea>

                </td>

                <td class="val_champs_form">
                <div class="label_champs_form">
                <xsl:processing-instruction name="php">print kAdresseFacturation;</xsl:processing-instruction>
                </div>

                    <textarea name="PANXML_ADRESSE_FACTURATION" rows="4" cols="40" style="width:220px;font-size:10px;font-family:Arial;"  >
						<xsl:choose>
							<xsl:when test="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION!=''">
								<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION"/>
							</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="t_panier/t_usager/US_SOC_ADRESSE"/><xsl:text>&#160;</xsl:text>
						<xsl:value-of select="t_panier/t_usager/US_SOC_ZIP"/><xsl:text>&#160;</xsl:text>
						<xsl:value-of select="t_panier/t_usager/US_SOC_VILLE"/>
					</xsl:otherwise>
						</xsl:choose>
					</textarea>

                </td>
                <td colspan="2"/>
             </tr>

             <tr height="30"><td/>
                <td colspan="2" class="val_champs_form" valign="top">
	                <div class="label_champs_form">
	                <xsl:processing-instruction name="php">print kTypeUtilisation;</xsl:processing-instruction>
	                </div>
                    <xsl:choose>
                    	<xsl:when test="t_panier/t_usager/US_ID_FONCTION_USAGER>1">
                    		<input type="hidden" name="PANXML_TYPE_UTILISATION" value="2" />
                    		<xsl:processing-instruction name="php">print (kUsageInstitutionnel)</xsl:processing-instruction>
                    	</xsl:when>
                    	<xsl:otherwise>
                    		<input type="hidden" name="PANXML_TYPE_UTILISATION" value="1" />
                    		<xsl:processing-instruction name="php">print (kUsagePrive)</xsl:processing-instruction>
                    	</xsl:otherwise>
                    </xsl:choose>
                 </td>
                <td class="small_text" colspan="2">
                <xsl:processing-instruction name="php">print kRestrictionsUsageInstitutionnel;</xsl:processing-instruction>
                </td>
            </tr>

     <tr height="20">
            <td/>
            <td class="val_champs_form"  valign="top" colspan="4">
                <div class="label_champs_form">
                <xsl:processing-instruction name="php">print kDestination;</xsl:processing-instruction>
               </div>

	                	<select name="PANXML_DESTINATION" onChange='refresh()' style='font-size:10px;'>
	                	<xsl:choose>
	                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_DESTINATION='FRANCE'">

			                		<option value="FRANCE" selected="true"><xsl:processing-instruction name="php">print kDestinationFrance;</xsl:processing-instruction></option>
			                		<option value="EUROPE"><xsl:processing-instruction name="php">print kDestinationEurope;</xsl:processing-instruction></option>
			                		<option value="INTERNATIONAL"><xsl:processing-instruction name="php">print kDestinationInternational;</xsl:processing-instruction></option>

			                </xsl:when>
	                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_DESTINATION='EUROPE'">

			                		<option value="FRANCE" ><xsl:processing-instruction name="php">print kDestinationFrance;</xsl:processing-instruction></option>
			                		<option value="EUROPE" selected="true"><xsl:processing-instruction name="php">print kDestinationEurope;</xsl:processing-instruction></option>
			                		<option value="INTERNATIONAL"><xsl:processing-instruction name="php">print kDestinationInternational;</xsl:processing-instruction></option>

			                </xsl:when>
	                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_DESTINATION='INTERNATIONAL'">

			                		<option value="FRANCE" ><xsl:processing-instruction name="php">print kDestinationFrance;</xsl:processing-instruction></option>
			                		<option value="EUROPE"><xsl:processing-instruction name="php">print kDestinationEurope;</xsl:processing-instruction></option>
			                		<option value="INTERNATIONAL" selected="true"><xsl:processing-instruction name="php">print kDestinationInternational;</xsl:processing-instruction></option>

			                </xsl:when>
			                <xsl:otherwise>
			                		<option value=""/>
			                		<option value="FRANCE" ><xsl:processing-instruction name="php">print kDestinationFrance;</xsl:processing-instruction></option>
			                		<option value="EUROPE"><xsl:processing-instruction name="php">print kDestinationEurope;</xsl:processing-instruction></option>
			                		<option value="INTERNATIONAL" ><xsl:processing-instruction name="php">print kDestinationInternational;</xsl:processing-instruction></option>
			                </xsl:otherwise>
	                	</xsl:choose>
	                	</select>

                 <input type="hidden" name="PANXML_TRANSPORTEUR" value="CNRS" />
                </td>


				<td>
							<div class="label_champs_form">
							<xsl:processing-instruction name="php">print kNumeroTVA;</xsl:processing-instruction>
							</div>
									<input name="PANXML_TVA_INTRA" type="text" size="10" value="{t_panier/PAN_XML/commande/PANXML_TVA_INTRA}" onChange='calcTarifs()'  />

			 </td>


            </tr>

            <tr height="1"><td colspan="5" style="padding:0px;margin:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>
            <tr height="20">
                <td class="label_champs_form"><xsl:processing-instruction name="php">print kModePaiement;</xsl:processing-instruction></td>
                <td class="val_champs_form" colspan="4">
	               <table width="100%" >
	                <tr>

		                	<td class="val_champs_form" >
		                	<xsl:choose>
		                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CHEQUE' or t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT=null or t_panier/TOTAL_OUT_OF_RANGE=1" >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="CHEQUE" onClick='refresh()' checked="true" />
		                		</xsl:when>
		                		<xsl:otherwise >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="CHEQUE" onClick='refresh()' />
		                		</xsl:otherwise>
		                	</xsl:choose>
		                	<xsl:processing-instruction name="php">echo kModePaiementCheque</xsl:processing-instruction>
							</td>
							<td class="val_champs_form" >
		                	<xsl:choose>
		                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CR' and t_panier/TOTAL_OUT_OF_RANGE=0" >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="CR" onClick='refresh()' checked="true" />
		                		</xsl:when>
		                		<xsl:otherwise >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="CR"  onClick='refresh()' />
		                		</xsl:otherwise>
		                	</xsl:choose>
		                	<xsl:processing-instruction name="php">echo kModePaiementCR</xsl:processing-instruction>
							</td>
							<td>
		                	<xsl:choose>
		                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CB' and t_panier/TOTAL_OUT_OF_RANGE=0" >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="CB" onClick='refresh()' checked="true" />
		                		</xsl:when>
		                		<xsl:otherwise >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="CB" onClick='refresh()' />
		                		</xsl:otherwise>
		                	</xsl:choose>
		                	<img src="{$imgurl}paiement_CB.gif"/>
		                	</td>
		                	<td>
		                	<xsl:choose>
		                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='VISA' and t_panier/TOTAL_OUT_OF_RANGE=0" >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="VISA" onClick='refresh()' checked="true" />
		                		</xsl:when>
		                		<xsl:otherwise >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="VISA" onClick='refresh()' />
		                		</xsl:otherwise>
		                	</xsl:choose>
		                	<img src="{$imgurl}paiement_VISA.gif"/>
		                	</td>
		                	<td>

		                	<xsl:choose>
		                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='EUROCARD_MASTERCARD' and t_panier/TOTAL_OUT_OF_RANGE=0" >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="EUROCARD_MASTERCARD" onClick='refresh()' checked="true" />
		                		</xsl:when>
		                		<xsl:otherwise >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="EUROCARD_MASTERCARD" onClick='refresh()' />
		                		</xsl:otherwise>
		                	</xsl:choose>
		               		<img src="{$imgurl}paiement_MASTERCARD.gif"/>
		                	</td>
		                	<td>

		                	<xsl:choose>
		                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='E_CARD' and t_panier/TOTAL_OUT_OF_RANGE=0" >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="E_CARD" onClick='refresh()' checked="true" />
		                		</xsl:when>
		                		<xsl:otherwise >
		                		<input type="radio" name="PANXML_MODE_PAIEMENT" value="E_CARD" onClick='refresh()' />
		                		</xsl:otherwise>
		                	</xsl:choose>
		                	<img src="{$imgurl}paiement_ECARD.gif"/>


		                	</td>


	                </tr>
	                </table>
               </td></tr>
               <tr><td/>
                 <td class="small_text" id='msg_paiement' colspan="4">

                	<xsl:choose>
                 		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CHEQUE' or PAN_XML/commande/PANXML_MODE_PAIEMENT=null or t_panier/TOTAL_OUT_OF_RANGE=1" >
                		 <xsl:processing-instruction name="php">print kRestrictionsPaiementCheque;</xsl:processing-instruction>
                		</xsl:when>
                		<xsl:when test="t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CR'" >
                		 <xsl:processing-instruction name="php">print kRestrictionsPaiementVirement;</xsl:processing-instruction>
                		</xsl:when>
                	</xsl:choose>
                 <xsl:if test="t_panier/TOTAL_OUT_OF_RANGE=1" >
		             	<div align="left" class="errormsg"><xsl:processing-instruction name="php">print kErreurCBHorsLimites;</xsl:processing-instruction></div>
		     	</xsl:if>

                </td>
            </tr>

	            <tr><td/>
	            <td colspan="4" class="val_champs_form">
	            <input type="checkbox" name="CGV_OK" />&#160;<xsl:processing-instruction name="php">print kCommandeCGV_OK;</xsl:processing-instruction>
	            </td>
	            </tr>

        </table>
  		</fieldset>

	  <fieldset style='-moz-border-radius:8px;margin:3px;padding:3px;width:95%;margin-left:auto;margin-right:auto;'>
	  <legend>Contenu de la commande</legend>

        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
            <tr>
                <td class="resultsHead" width="50"><xsl:processing-instruction name="php">print kCote;</xsl:processing-instruction></td>
                <td class="resultsHead" width="100"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kFonds;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></td>
                <td class="resultsHead" width="150"><xsl:processing-instruction name="php">print kExtrait;</xsl:processing-instruction></td>
                <td class="resultsHead" width="60"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kSupportLivraison;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kNb;</xsl:processing-instruction></td>

          <td class="resultsHead"><xsl:processing-instruction name="php">print kPrixHT;</xsl:processing-instruction></td>

                <td class="resultsHead" align="center"><input type="image" name="btSupAll" src="design/images/button_drop_a.gif" onclick="removeAll()" title="&lt;?php echo kSupprimerTout; ?&gt;"/> </td>
            </tr>
            <xsl:for-each select="t_panier_doc">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <xsl:variable name="pdoc_support_liv"><xsl:value-of select="PDOC_SUPPORT_LIV"/></xsl:variable>
                <tr class="resultsCorps" id="{concat('altern',$j mod 2)}">
                    <td class="resultsCorps" width="50">
						<input type="hidden" name="t_panier_doc[][ID_LIGNE_PANIER]" value="{ID_LIGNE_PANIER}" />
						<input type="hidden" name="t_panier_doc[][ID_DOC]" value="{ID_DOC}" />
					<xsl:value-of select="DOC/t_doc/DOC_COTE"/>
					</td>

                    <td class="resultsCorps" width="100">
						<a href="{$scripturl}?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></a>
					</td>
                    <td class="resultsCorps" width="80">
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="DOC/t_doc/DOC_DATE_PROD"/>
						</xsl:call-template>
					</td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/DOC_XML/XML/DOC/FONDS"/></td>
                    <td class="resultsCorps" width="80"><xsl:value-of select="DOC/t_doc/TYPE_DOC"/></td>
                    <td class="resultsCorps" width="150">
						<xsl:if test="PDOC_EXTRAIT='1'">
						<xsl:value-of select="PDOC_EXT_TITRE"/><br/>
						[<xsl:value-of select="PDOC_EXT_TCIN"/> - <xsl:value-of select="PDOC_EXT_TCOUT"/>]
						</xsl:if>
						&#160;
					</td>
                    <td class="resultsCorps" width="60">
						<xsl:choose>
							<xsl:when test="PDOC_EXTRAIT='1'"><xsl:value-of select="EXT_DUREE"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:otherwise>
						</xsl:choose>
						&#160;
					</td>

                    <td class="resultsCorps">

                                <input type="hidden" name="t_panier_doc[][ID_LIGNE_PANIER]" value="{ID_LIGNE_PANIER}" />
                                <input type="hidden" name="t_panier_doc[][ID_DOC]" value="{$id_doc}" />
	                            <xsl:choose>
	                            	<xsl:when test="SUPPORTS/SUPPORTS/VALEUR!=''" >
		                                <select name="t_panier_doc[][PDOC_SUPPORT_LIV]"  onChange='refresh()' style='font-size:10px;' >
		                                    <option></option>
		                                	<xsl:for-each select="SUPPORTS/SUPPORTS">
		                                		<xsl:choose>
		                                			<xsl:when test="VALEUR=$pdoc_support_liv">
		                                				<option value="{VALEUR}" selected="true" ><xsl:value-of select="VALEUR" /></option>
		                                			</xsl:when>
		                                			<xsl:otherwise>
		                               					<option value="{VALEUR}" ><xsl:value-of select="VALEUR" /></option>
		                                			</xsl:otherwise>
		                                		</xsl:choose>
		                                	</xsl:for-each>

		                                </select>
		                            </xsl:when>
		                            <xsl:otherwise>
		                            N.A.
		                            </xsl:otherwise>
	                            </xsl:choose>

                    </td>

                    <td class="resultsCorps">
                  			<xsl:choose>
                  				<xsl:when test="SUPPORTS/SUPPORTS/VALEUR!=''" >
                                <input name="t_panier_doc[][PDOC_NB_SUP]" type="text" onChange='refresh()' style='font-size:10px;'  size="3" maxlength="3"  value="{PDOC_NB_SUP}" />
                 				</xsl:when>
                 				<xsl:otherwise>
                 				N.A.
                 				</xsl:otherwise>
                 			</xsl:choose>

                    </td>

	                    <td class="resultsCorps" id="pdoc_prix_calc{ID_LIGNE_PANIER}">

	                      <xsl:choose>
		                      <xsl:when test="normalize-space(PDOC_PRIX_CALC)!='' and normalize-space(PDOC_PRIX_CALC)!='&#160;'"><xsl:value-of select="PDOC_PRIX_CALC"/>&#160;€</xsl:when>
		                      <xsl:otherwise><xsl:text>N.A.</xsl:text></xsl:otherwise>
	                      </xsl:choose>
	                    </td>


					<td class="resultsCorps" align="center">

                            <input type="image" name="btSup" src="design/images/button_drop.gif" onclick='removeLine({ID_LIGNE_PANIER})' title="&lt;?php echo kSupprimer; ?&gt;"/>
                        </td>

                </tr>
            </xsl:for-each>
		</table>
  	</fieldset>


		<xsl:variable name="total_ht"><xsl:value-of select="t_panier/TOTAL_HT" /></xsl:variable>
		<xsl:variable name="frais_ht"><xsl:value-of select="t_panier/FRAIS_HT" /></xsl:variable>
		<xsl:variable name="tva"><xsl:value-of select="t_panier/TVA" /></xsl:variable>

			 <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
			   <tr class="resultsCorps" >
			     <td align="right" class="resultsCorps" style="font-size:13px" >
			     	<xsl:processing-instruction name="php">print kPanierTotalProduits;</xsl:processing-instruction>
			     </td>
			     <td  align="right" class="resultsCorps" style="font-size:13px" id="total_ht">
			       	<xsl:choose>
			       		<xsl:when test="t_panier/TOTAL_HT!=''" ><xsl:value-of select="t_panier/TOTAL_HT" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			     <td  class="resultsCorps"> &#160; </td>
			   </tr>
			   <tr class="resultsCorps" >
			   	<td align="right" class="resultsCorps">
			     	<xsl:processing-instruction name="php">print kPanierFrais;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsCorps" id="frais_ht">
			     	<xsl:choose>
			       		<xsl:when test="t_panier/FRAIS_HT!=0" ><xsl:value-of select="t_panier/FRAIS_HT" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			     <td  class="resultsCorps"> &#160; </td>
			   </tr>

			   <tr class="resultsHead" >
			     <td align="right" class="resultsHead">
			     	<xsl:processing-instruction name="php">print kPanierTotalHT;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsHead" id="total_frais_ht">
					<xsl:choose>
			       		<xsl:when test="t_panier/TOTAL_FRAIS_HT!=0" ><xsl:value-of select="t_panier/TOTAL_FRAIS_HT" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			      <td class="resultsHead"> &#160; </td>
			   </tr>

			   <tr class="resultsHead" >
			     <td align="right" class="resultsHead">
			     	<xsl:processing-instruction name="php">print kPanierTotalTTC;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsHead" id="total_ttc">
					<xsl:choose>
			       		<xsl:when test="t_panier/TOTAL_TTC!=0" ><xsl:value-of select="t_panier/TOTAL_TTC" />&#160;€</xsl:when>
				     	<xsl:otherwise>N.A.</xsl:otherwise>
			     	</xsl:choose>
			     </td>
			      <td class="resultsHead"> &#160; </td>
			   </tr>
			 </table>


	     <br/>

	     <div align="center">
                <table width="95%" border="0" cellpadding="0" cellspacing="0">
                  <tr align="left" valign="top">





							<td width="33%" align="center" class="resultsBar"><a href="{$scripturl}?urlaction=panier"><xsl:processing-instruction name="php">print kRetour;</xsl:processing-instruction></a></td>
							<td width="33%" id='valider' align="center" class="resultsBar">
							<xsl:choose>
								<xsl:when test="(t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='CB' or t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='VISA' or PAN_XML/commande/PANXML_MODE_PAIEMENT='EUROCARD_MASTERCARD' or t_panier/PAN_XML/commande/PANXML_MODE_PAIEMENT='E_CARD') and t_panier/TOTAL_OUT_OF_RANGE=0">
											<a href="#"  onclick="chkFields(document.form1,true,'PANXML_TVA_INTRA')" ><xsl:processing-instruction name="php">print kPayerEnLigne;</xsl:processing-instruction></a>
										</xsl:when>
										<xsl:otherwise>
											<a href="#"  onclick="chkFields(document.form1,false,'PANXML_TVA_INTRA')"><xsl:processing-instruction name="php">print kValider;</xsl:processing-instruction></a>
										</xsl:otherwise>
								</xsl:choose>
							</td>

                 </tr>
                </table>
            </div>
            </form>
	<br/>


    </xsl:template>


</xsl:stylesheet>

