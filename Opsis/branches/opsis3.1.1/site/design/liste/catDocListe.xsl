<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="xmlfile" />
<xsl:param name="fromAjax" />
<xsl:param name="fromOffset" />
<xsl:param name="solr_facet" />
<xsl:param name="pagination" />


<xsl:template match='/select'>
	<xsl:variable name="xmllist" select="document($xmlfile)"/>
	<div id="formulaire_description_cat" >	
		<script type="text/javascript" src="&lt;?=libUrl?&gt;/webComponents/opsis/liste.js">&amp;nbsp;</script>
<script>
	function expandContent(){
		if($j('#menuDroite').width() &gt; 300)
			$j('#formulaire_description_cat').css('width','98%');
			else 
		$j('#formulaire_description_cat').css('width','77%');
	}
</script>		
		<script type="text/javascript">
			/*jQuery('.title_bar').css('position','relative');
			jQuery('.title_bar').css('margin-top','0px');*/
		try{
			//calculateNbLignes();
			//adjustMosaique();
		}catch(e){
			console.log("crash first resize mos : "+e);
		}
		
		</script>
		
		<div id=""><xsl:attribute name="class">pres_1</xsl:attribute>
			<form name="documentSelection" method="post" onsubmit='updatePage()' action="{$scripturl}?urlaction=catSaisie{$urlparams}">
				<input type="hidden" name="page" value="{$page}"  />
				<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
				<input type="hidden" name="style" value=""  />
				<input type="hidden" name="stylePanier" id="stylePanier" value=""  />
				<input type="hidden" name="tri" value="{$tri}" />
				
				<xsl:choose>
					<xsl:when test="$pagination='1'">
						<xsl:call-template name="pagerListe">
							<xsl:with-param name="form">documentSelection</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<script type="text/javascript">
						var pagerForm=document.documentSelection;
						</script>
					</xsl:otherwise>
				</xsl:choose>
				
				<div id="resultats"  align="center">
					<xsl:attribute name="class">mos <xsl:if test="$pagination='1'">paginee</xsl:if></xsl:attribute> 
					<xsl:call-template name="mosDoc">
						<xsl:with-param name="context">doc_cat_list</xsl:with-param>
					</xsl:call-template>
					<div class="pusher">&#160;</div>
				</div>
				<xsl:choose>
					<xsl:when test="$pagination='1'">
						<xsl:call-template name="pagerListe">
							<xsl:with-param name="form">documentSelection</xsl:with-param>
							<xsl:with-param name="noscript">1</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$nbRows and $nbLigne &lt; $nbRows">
							<div id="loadMoreRes" onclick="loadMoreResults();"><span><xsl:processing-instruction name="php">print kLoadMoreResults;</xsl:processing-instruction></span></div>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<script type="text/javascript">
					var totalNbResults = <xsl:value-of select="$nbRows"/>;
				</script>
			</form>
		</div>
</div>
</xsl:template>

</xsl:stylesheet>