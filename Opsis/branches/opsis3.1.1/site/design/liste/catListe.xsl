<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="xmlfile" />

<xsl:template match='/select'>

	<xsl:variable name="xmllist" select="document($xmlfile)"/>


<form name="documentSelection" method="post" onsubmit='updatePage()' action="{$scripturl}?urlaction=catListe{$urlparams}">
	<input type="hidden" name="page" value="{$page}"  />
	<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
	<input type="hidden" name="style" value=""  />
	<input type="hidden" name="tri" value=""  />

<table border="0" cellPadding="3" cellSpacing="0" align="center" width="95%">
	<tr>
		<td nowrap="1"  class="val_champs_form" width="75%">
			<b><xsl:processing-instruction name="php">print kVotreRecherche;</xsl:processing-instruction></b>
			<br/>
			<xsl:processing-instruction name="php">print $_SESSION['recherche_CAT']['etape'];</xsl:processing-instruction>
		</td>
	</tr>
	</table>
	
	<div align="right"><a href="#pied" class="val_champs_form">&lt;?=kBasPage?&gt;</a></div>
	<div id="tabs">

		<xsl:call-template name="pager">
			<xsl:with-param name="form">documentSelection</xsl:with-param>
			<!--xsl:with-param name="print">print.php?&lt;?= $_SERVER['QUERY_STRING'] ?&gt;</xsl:with-param-->
		</xsl:call-template>


		<div id="tabs-1">
			<xsl:call-template name="displayListe">
				<xsl:with-param name="xmllist" select="$xmllist"/>
			</xsl:call-template>
		</div>
		
		<xsl:call-template name="pager">
			<xsl:with-param name="form">documentSelection</xsl:with-param>
			<xsl:with-param name="noscript">1</xsl:with-param>
		</xsl:call-template>

	</div>
	<div align="right"><a href="#entete" class="val_champs_form">&lt;?=kHautPage?&gt;</a></div>

</form>
</xsl:template>

</xsl:stylesheet>
