<?xml version="1.0" encoding="utf-8"?>
<!-- paletteSimple.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="scripturl"/>
<xsl:param name="affich_nb"/>
<xsl:param name="titre_index"/>
<xsl:param name="btn_edit"/>
<xsl:param name="w"/>
<xsl:param name="h"/>
<xsl:param name="jsFunction" />
<xsl:param name="id_input_result" />
<xsl:param name="get_params" />
<xsl:param name="page" />
<xsl:param name="nb_pages" />
<xsl:param name="nb_rows" />

    <xsl:template match='/select'>
    <xsl:variable name="new_url"><xsl:value-of select="concat($btn_edit,$w,$h)" /></xsl:variable>
	<script type="text/javascript" src="&lt;?= libUrl ?&gt;/webComponents/opsis/cherche.js">&amp;nbsp;</script>

       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableResults">
            <tr>
              <td width="5" class="resultsHead">&#160;</td>
              <td class="resultsHead"><xsl:value-of select="$titre_index" /></td>
            </tr>

            <xsl:for-each select="val">
                <xsl:variable name="id"><xsl:call-template name="internal-quote-replace"><xsl:with-param	name="stream" select="ID_VAL"/></xsl:call-template></xsl:variable>
                <xsl:variable name="val"><xsl:call-template name="internal-quote-replace">
                  <xsl:with-param	name="stream" select="VALEUR"/>
                  </xsl:call-template></xsl:variable>
                <xsl:variable name="edit_url"><xsl:value-of select="concat($btn_edit,$id,$w,$h)" /></xsl:variable>

                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">
                    <td class="resultsCorps" ><a href='{$edit_url}'><img src='design/images/crayon.gif' border='0'/></a></td>
                    <td class="resultsCorps" >
             <xsl:choose>
              <xsl:when test="contains($get_params,'champ=L')">
                         <a href="javascript:void(0)" onmouseover="showInfos('lex','{$id}','lexInfos')" onclick="javascript:window.parent.{$jsFunction}('{$id_input_result}','{$val}','{$id}')"><xsl:value-of select="VALEUR"/></a>
              </xsl:when>
              <xsl:otherwise>
                         <a href="javascript:void(0)" onclick="javascript:window.parent.{$jsFunction}('{$id_input_result}','{$val}','{$id}')"><xsl:value-of select="VALEUR"/></a>
              </xsl:otherwise>
            </xsl:choose>
                    </td>
                </tr>
            </xsl:for-each>


        </table>
     <table width="100%" border="0" class="resultsMenu">
    <tr>
    <td align="left" width='65'>
	<xsl:variable name="get_typelex">
	<xsl:if test="contains($get_params,'type_lex')"><xsl:value-of select="concat('&amp;type_lex',substring(substring-after($get_params,'type_lex'),1,3))" /></xsl:if>
	</xsl:variable>
	<xsl:variable name="get_valeur">
	<xsl:if test="contains($get_params,'valeur')"><xsl:value-of select="concat('&amp;valeur',substring-before(substring-after($get_params,'valeur'),'&amp;'))" /></xsl:if>
	</xsl:variable>
        <a href='{$new_url}{$get_typelex}{$get_valeur}' style='padding:0px 2px 0px 2px;color:#0066aa;text-align:center;font-size:11px;font-family:Arial,sans-serif;border:1px solid #555555'><xsl:processing-instruction name="php">print kNouveau;</xsl:processing-instruction></a>
    </td>
    <td align="left" ><xsl:value-of select='$nb_rows' />&#160;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction></td>
    <td align="center" width="55">
      <xsl:if test="$page &gt; 1"><a href="{$scripturl}?urlaction=chercheIndex2&amp;{$get_params}&amp;page={$page - 1}">&lt;</a></xsl:if>
      <input type='text' id='rang' name='rang' style='text-align:center;width:25px;font-size:9px;border:1px solid #467A94;padding:0px;' value='{$page}' size='3' onKeyPress="javascript:submitIfReturn(event)"  onchange='javascript:document.form1.page.value=eval(this.value);document.form1.submit();' />
      <xsl:if test="($page &lt; $nb_pages)"><a href="{$scripturl}?urlaction=chercheIndex2&amp;{$get_params}&amp;page={$page + 1}">&gt;</a></xsl:if>
    </td>
    <td width="60" align="right"><xsl:value-of select='$nb_pages' />&#160;<xsl:processing-instruction name="php">print kPages;</xsl:processing-instruction></td>
    </tr>
  	</table>
    <div id="infos" style="font-size:10px;font-family:Trebuchet MS;background-color:#f0f0f0;border:1px solid #e0e0e0"></div>

   </xsl:template>

<xsl:template name="internal-quote-replace">
    <xsl:param name='stream' />
    <xsl:variable name="simple-quote">'</xsl:variable>

    <xsl:choose>
    <xsl:when test="contains($stream,$simple-quote)">
  <xsl:value-of
  select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
  name="internal-quote-replace"><xsl:with-param name="stream"
  select="substring-after($stream,$simple-quote)"/></xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
	  <xsl:value-of select="$stream"/>
    </xsl:otherwise>
    </xsl:choose>

</xsl:template>
</xsl:stylesheet>
