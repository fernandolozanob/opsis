<?xml version="1.0" encoding="utf-8"?>
<!-- lot.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="xmlfile" />

<xsl:template match='/select'>

<xsl:variable name="xmllist" select="document($xmlfile)"/>
<div class="label_champs_form">
	<a href="javascript:toggleVisibility(document.getElementById('listeLot'),document.getElementById('arrowindexation'))">
		<img border="0" align="absmiddle" id="arrowindexation" src="design/images/arrow_right.gif" />&lt;?=Lot?&gt;
	</a>
</div>
<div id="listeLot" style="display: none;">
	<form name="documentSelection" method="post" onsubmit='updatePage()' action="{$scripturl}?urlaction=saisieLot{$urlparams}">
		<input type="hidden" name="page" value="{$page}"  />
		<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
		<input type="hidden" name="style" value=""  />
		<input type="hidden" name="tri" value=""  />

		<xsl:call-template name="displayListe">
			<xsl:with-param name="xmllist" select="$xmllist"/>
		</xsl:call-template>

	</form>
</div>
</xsl:template>

</xsl:stylesheet>