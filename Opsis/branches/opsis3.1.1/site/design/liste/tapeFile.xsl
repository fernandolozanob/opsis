<?xml version="1.0" encoding="utf-8"?>
<!--  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="no"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="profil"/>
<xsl:param name="source" />
<xsl:param name="nbLigne"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />
    <xsl:template match='/select'>

	<xsl:if test="$source='tape'">
		<script type="text/javascript">
		var pagerForm=document.form1;
		</script>
		<script type="text/javascript" src="&lt;?=libUrl?&gt;/webComponents/opsis/liste.js">&amp;nbsp;</script>
		<table width="100%" border="0" class="resultsMenu" align="center">
			<tr>
				<td align="left" width="33%">
					<xsl:value-of select="$nb_rows" />&#160;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>
				</td>
				<td align="center" width="33%" rowspan="2">
					<xsl:if test="$nb_pages>1">
						<xsl:if test="$page>1"><a href="#" onclick='javascript:pagerForm.page.value={$page - 1};pagerForm.submit();'><img src='design/images/left_pg.gif' border='0' /> </a> </xsl:if>
						<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onkeydown='changePage(event,{$page},${nb_page})' onchange='goPage(this,{$page},{$nb_pages})' />
						<xsl:value-of select="concat(' / ',$nb_pages)"/>
						<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:pagerForm.page.value={$page + 1};pagerForm.submit();'><img src='design/images/right_pg.gif' border='0' /></a></xsl:if>
					</xsl:if>&#160;
				</td>
				<td align="right" width="33%">
	                <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
	                <select class='val_champs_form' name="selectNbLignes" onchange="javascript:pagerForm.nbLignes.value=this.value;pagerForm.submit()">
	                    <option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
	                    <option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
	                    <option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
	                    <option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
	                    <option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
	                </select>
				</td>
			</tr>
		</table>
	</xsl:if>
		<table width="95%" align="center" border="0" cellspacing="0"  class="tableResults">
			<tbody id='tapeFileListe'>
				<tr>
                    <xsl:choose>
                        <xsl:when test="$source='tape'">
                            <td class="resultsHead"><xsl:processing-instruction name="php">print kNomFichier;</xsl:processing-instruction></td>
                        </xsl:when>
                        <xsl:when test="$source='mat'">
                            <td class="resultsHead"><xsl:processing-instruction name="php">print kCartouche;</xsl:processing-instruction></td>
                            <td class="resultsHead"><xsl:processing-instruction name="php">print kStatut;</xsl:processing-instruction></td>
                        </xsl:when>
                    </xsl:choose>
					<td class="resultsHead"><xsl:processing-instruction name="php">print kJeu;</xsl:processing-instruction></td>
					<td class="resultsHead"><xsl:processing-instruction name="php">print kNumeroFichier;</xsl:processing-instruction></td>
					<td class="resultsHead"><xsl:processing-instruction name="php">print kTailleFichier;</xsl:processing-instruction></td>
					<td class="resultsHead"><xsl:processing-instruction name="php">print kDateFichier;</xsl:processing-instruction></td>
					<td class="resultsHead"><xsl:processing-instruction name="php">print kDateSauvegarde;</xsl:processing-instruction></td>
					<td class="resultsHead" width="20px">&#160;</td>
				</tr>
				<xsl:for-each select="t_tape_file">
                    <xsl:sort select="ID_TAPE_FILE" data-type="number"/>
					<xsl:variable name="j"><xsl:number/></xsl:variable>
					<tr class="{concat('resultsCorps',$j mod 2)}">
                        <xsl:choose>
                            <xsl:when test="$source='tape'">
                                <td class="resultsCorps"><a href='index.php?urlaction=matSaisie&amp;id_mat={TF_ID_MAT}'><xsl:value-of select="TF_FICHIER"/></a></td>
                            </xsl:when>
                            <xsl:when test="$source='mat'">
                                <td class="resultsCorps"><a href='index.php?urlaction=tapeSaisie&amp;id_tape={ID_TAPE}'><xsl:value-of select="ID_TAPE"/></a></td>
                                <td class="resultsCorps">
                                    <xsl:choose>
                                        <xsl:when test="TAPE_STATUS='1'">online</xsl:when>
                                        <xsl:otherwise>offline</xsl:otherwise>
                                    </xsl:choose>
                                </td>
                            </xsl:when>
                        </xsl:choose>
						<td class="resultsCorps"><xsl:value-of select="ID_TAPE_SET"/></td>
						<td class="resultsCorps"><xsl:value-of select="TF_N_FILE"/></td>
						<td class="resultsCorps"><xsl:value-of select="concat(floor(number(TF_TAILLE_FICHIER) div 1024),' K')"/></td>
						<td class="resultsCorps"><xsl:value-of select="TF_DATE_FICHIER"/></td>
						<td class="resultsCorps"><xsl:value-of select="TF_DATE_COPIE"/></td>
						<td class="resultsCorps">
							<img name="btRestore{ID_TAPE_FILE}" src="design/images/refresh.gif" style='cursor:pointer' onclick="javascript:popupUpload('simple.php?urlaction=backupJob&amp;action=restore&amp;media=videos&amp;id_tape_file={ID_TAPE_FILE}&amp;id_tape={ID_TAPE}&amp;fichier={TF_FICHIER}','main','jobs')" title="&lt;?php echo kRestaurer; ?&gt;" alt="&lt;?php echo kRestaurer; ?&gt;"/>
						</td>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
		
		<table>
		<tr id='TFrow$blank' style='display:none;'>
				<td class="resultsCorps">
					<input type='hidden' name='t_tape_file[TF_FICHIER][]' id='input$0' value='' />
					<span id='span$0'>&#160;</span>
				</td>
				<td class="resultsCorps">
					<input type='text' name="t_tape_file[TF_DATE_COPIE][]" value=''/>
				</td>
				<td class="resultsCorps">
					<img name="btSupTF0" src="design/images/button_drop.gif" style='cursor:pointer' onclick="removeLine(this)" title="&lt;?php echo kSupprimer; ?&gt;" alt="&lt;?php echo kSupprimer; ?&gt;"/>
				</td>
		</tr>
		</table>
	</xsl:template>

</xsl:stylesheet>
