<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes" indent="yes" /> 
	
    <xsl:param name="gPrefixTape"/>
    
    <xsl:template name="aff_element">
		<div class="element_librairie">
			<div class="element_librairie_num">
				<xsl:value-of select="numero" />
			</div>
            <xsl:choose>
                <xsl:when test="id_tape!='' and starts-with(id_tape,$gPrefixTape)">
                    <div class="element_librairie_infos">
                        <div class="element_librairie_pourcentage" style="width:{pourcentage_utilise}%">&amp;nbsp;</div>
                        <p class="num_tape_set"><xsl:value-of select="id_tape_set" /></p>
                        <p><xsl:value-of select="id_tape" /></p>
                    </div>
                </xsl:when>
                <xsl:when test="id_tape!='' and not(starts-with(id_tape,$gPrefixTape))">
                    <div class="element_librairie_infos" style="background-color:#ccc">
                        <p>Full</p>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <div class="element_librairie_vide">
                        &amp;nbsp;
                    </div>
                </xsl:otherwise>
            </xsl:choose>
            </div>
	</xsl:template>

	<xsl:template match='/select'>
		
		<p>drive</p>
		<xsl:for-each select="lib_element[type='drive']">
			<xsl:call-template name="aff_element" match="." />
		</xsl:for-each>
		
		
		<p>slot</p>
		<xsl:for-each select="lib_element[type='slot']">
			<xsl:call-template name="aff_element" match="." />
		</xsl:for-each>
		
		
		<p>mail</p>
		<xsl:for-each select="lib_element[type='mail']">
			<xsl:call-template name="aff_element" match="." />
		</xsl:for-each>
		
		<form name="libAction" action="index.php?urlaction=backupLibView" method="post">
			<input type="hidden" name="commande" value="" />
            <table width="95%" border="0" class="resultsMenu" align="center">
                <tr>
                    <td align="center" width="50%">
            <input id='btnListing' type='button' value="&lt;?php print kListageCartouches; ?&gt;" onclick="document.libAction.commande.value='listing';document.libAction.submit()" />
                    </td>
                    <td align="center" width="50%">
			<input type="button" value="&lt;?php echo kExternaliser ?&gt;" name="externaliser" onclick="document.libAction.commande.value='externaliser';document.libAction.submit()"/>
                    </td>
                </tr>
            </table>
		</form>
		
	</xsl:template>
</xsl:stylesheet>