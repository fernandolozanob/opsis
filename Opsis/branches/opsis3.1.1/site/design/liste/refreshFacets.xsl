<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="xmlfile" />
<xsl:param name="fromAjax" />
<xsl:param name="fromOffset" />
<xsl:param name="solr_facet" />
<xsl:param name="pagination" />


<xsl:template match='/select'>
	<xsl:variable name="xml_solr_facet" select="document($solr_facet)"/>
	<!-- inclusion template qui genere le menu gauche -->
	<xsl:call-template name="rechercheLeftMenu" >
		<xsl:with-param name="solr_facet" select="$xml_solr_facet"/>
	</xsl:call-template>		
	
</xsl:template>

</xsl:stylesheet>