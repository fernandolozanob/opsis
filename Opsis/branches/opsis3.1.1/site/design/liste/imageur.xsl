<?xml version="1.0" encoding="utf-8"?>
<!-- imageurListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="id_doc"/>
<xsl:param name="id_mat"/>
<xsl:param name="scripturl"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="storyboardChemin" />
<xsl:param name="etat"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="imgurl" />
<xsl:param name="id_lang" />

    <xsl:template match='/select'>

	  		<script>
	  		
	  		function selectAllCheckBox(tf){
				checkedItems=document.getElementsByTagName('input');
				for (i=0;i&lt;checkedItems.length;i++) {
					if (checkedItems[i].type=='checkbox' &amp;&amp; checkedItems[i]!=tf ) {
						checkedItems[i].checked=tf.checked;
					}
				}
			}

	  		function removeAll(){
				
				checkedItems=document.getElementsByTagName('input');
				for (i=0;i&lt;checkedItems.length;i++) {
					if (checkedItems[i].name.indexOf('checkbox')==0) {
						checkedItems[i].value='1';
					}
				}
				removeItem();
			}
			
			//Retour du choix dans la palette => submit du formulaire en mode SELECT
			function addValue(id_champ,valeur,idx) {

				//alert(id_champ+'   '+valeur);
				document.getElementById(id_champ).value=valeur;
				document.form1.commande.value='SELECT';
				document.form1.submit();
			}

			//Déselection d'un imageur => submit SELECT à vide
			function razSelection () {
				document.form1.imageur.value='';
				document.form1.commande.value='SELECT';
				document.form1.submit();

			}


			</script>

	<!-- Note : utilisation d'une fausse balise pour remplir la balise DIV: en XSL, si une balise DIV ou IFRAME est vide, la xsl peut rester muette -->
 	
	<script>window.onload=function  dumb() {makeDraggable(document.getElementById('chooser'));}</script>

        <form name="form1" id="form1" method="post" enctype="multipart/form-data" action="">
            <input name="commande" type="hidden" value="" />
            <input name="page" type="hidden" value="" />
            <input name="vignette" type="hidden" value="" />
            <input name="img2del" type="hidden" value="" />
			<input name="id_imageur" type="hidden" value="{t_imageur/ID_IMAGEUR}" />
			<input name="imageur" type="hidden" value="{t_imageur/IMAGEUR}" />
  


        <table width='95%' border='0' class="resultsMenu" align="center">

		<tr><!-- MODE DOC / selection depuis plusieurs imageurs -->
		<xsl:choose>
			<xsl:when test="count(/select/t_imageur/ID_MAT)>0">
				<td>
					<xsl:processing-instruction name="php">print kMateriel;</xsl:processing-instruction> :
					<select name="DMAT_ID_MAT" style='width:120px' onChange='document.form1.submit();'>
						<option value=""><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
						<xsl:for-each select="t_imageur">
							<xsl:choose>
							<xsl:when test="IMAGEUR=DMAT_ID_MAT" >
								<option selected='true' value="{IMAGEUR}"><xsl:value-of select="ID_MAT" /></option>
							</xsl:when>
							<xsl:otherwise>
								<option value="{IMAGEUR}"><xsl:value-of select="ID_MAT" /></option>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</select>
				</td>
				<td><!--  Dans ce cas, l'imageur est vide  -->
					<xsl:processing-instruction name="php">print kOu." ".kImageur;</xsl:processing-instruction> :
					  <input class='val_champs_form' id="imageur" name="imageur" type="text" value="" />
					 <img src='{$imgurl}/index.gif' align='absmiddle' onClick="choose(document.form1.imageur,'titre_index=&lt;?=urlencode(kImageur)?&gt;&amp;id_lang={$id_lang}&amp;valeur=&amp;champ=IMG')" style="cursor:pointer"  />

					 <img src='{$imgurl}/poubelle.gif' align='absmiddle' onClick="razSelection()" style="cursor:pointer"  />

				</td>
			</xsl:when>
			<xsl:otherwise> <!-- MODE IMAGEUR UNIQUE : admin storyboard, matériel ou imageur_doc -->
				<td colspan='2'>
					<xsl:processing-instruction name="php">print kImageur;</xsl:processing-instruction> :
					  <input class='val_champs_form' id="imageur" name="imageur" type="text" value="{t_imageur/IMAGEUR}" />
					 <img src='{$imgurl}/index.gif' align='absmiddle' onClick="choose(document.form1.imageur,'titre_index=&lt;?=urlencode(kImageur)?&gt;&amp;id_lang={$id_lang}&amp;valeur=&amp;champ=IMG')" style="cursor:pointer"  />
					 <img src='{$imgurl}/poubelle.gif' align='absmiddle' onClick="razSelection()" style="cursor:pointer"  />

				</td>
			</xsl:otherwise>
		</xsl:choose>

		<td>	<xsl:processing-instruction name="php">print kVersion;</xsl:processing-instruction>

				<select name='id_lang' class='val_champs_form' onChange='save();'>
					<xsl:for-each select='t_langues'>
					    <xsl:choose>
					    	<xsl:when test=" . = $id_lang">
								<option value='{.}' selected='true'><xsl:value-of select="." /></option>
							</xsl:when>
							<xsl:otherwise>
								<option value='{.}'><xsl:value-of select="." /></option>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:for-each>
				</select>

		</td>
		<td align='right'>
			<label class='val_champs_form' style='font-weight:normal;color:#555555'>
			<xsl:processing-instruction name="php">print kSelectionnerTout;</xsl:processing-instruction>
			<input type='checkbox' name='chkAll' onClick='selectAllCheckBox(this)' />
			</label>
		</td></tr>


		<tr>
		<td colspan="2">
		<div style="position:relative;overflow:hidden">
        <xsl:for-each select="t_image">

            <xsl:variable name="imgPath2">
            	<xsl:call-template name="internal-quote-replace" >
            		<xsl:with-param name="stream" select="concat($storyboardChemin,IM_CHEMIN,'/',IM_FICHIER)" />
            	</xsl:call-template>
            </xsl:variable>
            <xsl:variable name="imgPath"><xsl:value-of  select="concat($storyboardChemin,IM_CHEMIN,'/',IM_FICHIER)" />
            </xsl:variable>
            <xsl:variable name="j"><xsl:number/></xsl:variable>
            <xsl:variable name="className"> <!-- Test pour voir si cette image est la vignette. Uniquement pour DOC -->
            	<xsl:choose>
            		<xsl:when test='SELECTED'>imgSelected</xsl:when>
            		<xsl:otherwise>imgNormal</xsl:otherwise>
            	</xsl:choose>
            </xsl:variable>

           <div class='{$className}' >
				<!-- Checkbox de sélection pour le delete -->
				<input type="checkbox" name="checked[{ID_IMAGE}]" style='width:12px;' value=""/>
				<!-- Timecode de l'image -->
				<input type="text" size="11" maxlength="11" onKeyPress="formatTC(this)" onKeyUp="formatTC(this)"  class='val_champs_form' style='width:70px;text-align:center;font-weight:bold;background-color:#000000;color:#FFFFFF;font-family:arial' name="IM_TC[{ID_IMAGE}]" value="{IM_TC}" />
				<!-- Bouton de sélection de vignette, non mis sur la vignette -->
				<xsl:if test='not(SELECTED)'>
					<img src="design/images/select_vignette.gif" align='absmiddle' style="cursor:pointer" onclick="document.form1.vignette.value='{ID_IMAGE}';document.form1.commande.value='VIGNETTE';document.form1.submit();"  alt="&lt;?=kImageurSelectVignette?&gt;" titre="&lt;?=kImageurSelectVignette?&gt;" />
				</xsl:if>
				<!-- Delete de l'image -->
				<img src="design/images/button_drop.gif" align='absmiddle' style="cursor:pointer" onclick="document.form1.img2del.value='{ID_IMAGE}';document.form1.commande.value='SUP';document.form1.submit();" />
				<!-- Image avec zoom sur le click -->
				<br/>
				<img src="{$imgPath}" width="130" style="cursor:pointer" onclick="javascript:popupImage('{$imgPath2}',encodeURIComponent('{IM_TEXT}'))" />
				<!-- Formulaire pour cette image -->
				<br/><input type="text" style='font-size:9px;width:125px;color:#666666' name="IM_TEXT[{ID_IMAGE}]" value="{IM_TEXT}" />
				<input type="hidden" name="ID_IMAGE[{ID_IMAGE}]" value="{ID_IMAGE}" />
				<input type="hidden" name="IM_CHEMIN[{ID_IMAGE}]" value="{IM_CHEMIN}" />
				<input type="hidden" name="IM_FICHIER[{ID_IMAGE}]" value="{IM_FICHIER}" />
				<input type="hidden" name="ID_LANG[{ID_IMAGE}]" value="{ID_LANG}" />
           </div>
            
        </xsl:for-each>
		
		</div>
		</td>
		</tr>
		</table>

  
    
        <table width='95%' border='0' class="resultsMenu" align="center">
            <tr>
                <td align="left" width="33%">
					<xsl:value-of select="$nb_rows" /> <xsl:processing-instruction name="php">print kImages;</xsl:processing-instruction>
 
                </td>
 				<!-- Changement de page, ATTENTION, car fait en repostant le formulaire (pas pas href !) -->
                <td align='center' width='33%'>
		 			<xsl:if test="$nb_pages>1">
						<xsl:if test="$page>1"><a href="#" onclick='javascript:document.form1.page.value={$page - 1};document.form1.submit();'><img src="design/images/fleche-gauche.gif" alt="" border="0" width="11" height="12" /></a> </xsl:if>
						<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onChange='javascript:document.form1.page.value=eval(this.value);document.form1.submit();' />
						<input type='submit' style='width:0px;height:0px;display:none;' />
						<xsl:value-of select="concat(' / ',$nb_pages)"/>
						<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:document.form1.page.value={$page + 1};document.form1.submit();'><img src="design/images/fleche-droite.gif" alt="" border="0" width="11" height="12" /></a></xsl:if>
					</xsl:if>
                </td>
                
                <td align='right' width='33%' >
                    <a href="javascript:popupPrint('export.php?type=imageur&amp;id={t_imageur/IMAGEUR}','main')">
                    <xsl:processing-instruction name="php">print kExporter;</xsl:processing-instruction>
                    </a>
                </td>

           </tr>

		<tr>
		<td align="left" width="33%">


        <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
        <select name='nbLignes' class='val_champs_form' onChange='javascript:document.form1.submit()'>
            <option value='8'><xsl:if test="$nbLigne=8"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>8</option>
            <option value='12'><xsl:if test="$nbLigne=12"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>12</option>
            <option value='24'><xsl:if test="$nbLigne=24"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>24</option>
            <option value='48'><xsl:if test="$nbLigne=48"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>48</option>
            <option value='all'><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
        </select>


		</td>
		<td align="center" width="33%">

		</td>
		<td align="right" width="33%">

		</td>
		</tr>
	</table>
    
	
	    
        <table width="100%" border="0" class="resultsMenu">
            <tr align="left" valign="top"> 
            <!-- On peut ajouter des images que si l'imageur existe réellement ET si l'on n'est pas en mode DOC mixte -->
			<xsl:if test="t_imageur/IMAGEUR!='' and count(/select/t_imageur/ID_MAT)=0 " >
              	<!--td align="center" class="resultsHead"><input name="bUpload" type="button" onclick="javascript:popupUpload('indexPopupUpload.php?urlaction=simpleUpload&amp;type=batch_img&amp;id={t_imageur/IMAGEUR}','main','jobs')" value="&lt;?=kAjoutImagesParUpload;?&gt;" /></td-->             
             	<td align="center">
             		<input name="bGenerer" type="button" onclick="document.form1.commande.value='GENERER';document.form1.submit()" value="&lt;?=kGenererStoryboard;?&gt;" /> 
             		<xsl:processing-instruction name="php">print kImageurIntervalleImage;</xsl:processing-instruction><input name="sb_rate" type="text" value='&lt;?=gStoryboardIntervalleDefaut?&gt;' size='3' maxlength='4' />
             	</td>             				
			</xsl:if>
				<td align="center"><input name="bSuppr" type="button" value="&lt;?=kSupprimerTout ?&gt;" onclick="removeAll()"/></td>
                <td align="center"><input name="bOk" type="submit" value="&lt;?=kEnregistrer?&gt;" /></td>
            </tr>
        </table>
    </form>
    <a href="&lt;?= $_SERVER['HTTP_REFERER'] ?&gt;"><xsl:processing-instruction name="php">print kRetour;</xsl:processing-instruction></a>
    </xsl:template>


</xsl:stylesheet>
