<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:include href="../detail.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 


<!-- recuperation de parametres PHP -->
<xsl:param name="loggedIn" />
<xsl:param name="tab" />
<xsl:param name="rang" />
	
	<xsl:template match='EXPORT_OPSIS'>
		<center>
			<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="message_description" style="max-width:800px;text-align:left">
				<legend><xsl:processing-instruction name="php">print kExpediteur;</xsl:processing-instruction>&amp;nbsp;:&amp;nbsp;<xsl:value-of select="t_message/USAGER_FROM" /></legend>
				<b><u><xsl:processing-instruction name="php">print kObjet;</xsl:processing-instruction> :</u>&amp;nbsp;<xsl:value-of select="t_message/MES_OBJET" /></b><br /><br />
				<i><u><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction> :</u>&amp;nbsp;<xsl:value-of select="t_message/MES_DATE_CREA" /></i>
				<p>
					<xsl:value-of disable-output-escaping="yes" select="t_message/MES_CONTENU" />
				</p>
			</fieldset>
			<form name="form1" method="POST" >
				<input type="hidden" name="commande" />
				<button id="bSup" class="ui-state-default ui-corner-all" onclick="removeMsg()" type="button"><span class="ui-icon ui-icon-trash">&amp;nbsp;</span><xsl:processing-instruction name="php">print kSupprimer;</xsl:processing-instruction></button>
			</form>
		</center>
		
		<script>
			function removeMsg() {
				if (confirm(str_lang.kConfirmJSDocSuppression)) {
					document.form1.commande.value="SUP_MSG";
					document.form1.submit();
				}
			}

			function changeRang(e,rang,doc_rows)
			{
				var keynum;
				var keychar;
				var numcheck;
				
				if(window.event) // IE
				{
					keynum = e.keyCode;
				}
				else if(e.which) // Netscape/Firefox/Opera
				{
					keynum = e.which;
				}
				if (keynum==13)
					goRang(document.getElementById('rang'),rang,doc_rows);
			}
			
			function goRang(obj,rangold,rangmax) {
				rangnew=parseInt(obj.value,10);
				if((rangnew&gt;0)&amp;&amp;(rangnew&lt;=rangmax)&amp;&amp;(rangnew!=rangold)){
					if(0){
						window.location = "index.php?urlaction=messageView&amp;amp;rang="+rangnew+"&amp;id_panier="+'';
					}	else	{
						window.location = "index.php?urlaction=messageView&amp;rang="+rangnew;
					}		
				}	else {
					obj.value=rangold;
				}
			}
		</script>
    </xsl:template>
    
</xsl:stylesheet>
