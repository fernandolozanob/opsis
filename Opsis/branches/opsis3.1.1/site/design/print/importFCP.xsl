<?xml version="1.0" encoding="utf-8"?>
<!-- 
import FCP 
VP : 13/09/2007 : Création du fichier
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

<xsl:template match='/xmeml'>
	<data>
		<xsl:for-each select="project/children/clip">
			<xsl:variable name="offset">
				<xsl:choose>
				<xsl:when test="media/video/track/clipitem/file/timecode/frame!=''">
					<xsl:value-of select="media/video/track/clipitem/file/timecode/frame"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="cote"><xsl:value-of select="name"/></xsl:variable>
			<DOC>
				<DOC_COTE><xsl:value-of select="name"/></DOC_COTE>
				<DOC_TITRE><xsl:value-of select="name"/></DOC_TITRE>
				<DOC_RES><xsl:value-of select="logginginfo/description"/></DOC_RES>
				<DOC_NOTES><xsl:value-of select="logginginfo/lognote"/></DOC_NOTES>
				<duration><xsl:value-of select="duration"/></duration>
				<in>
					<xsl:choose>
						<xsl:when test="in != -1"><xsl:value-of select="in + $offset"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="in"/></xsl:otherwise>
					</xsl:choose>
				</in>
				<out>
					<xsl:choose>
						<xsl:when test="out != -1"><xsl:value-of select="out + $offset"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="out"/></xsl:otherwise>
					</xsl:choose>
				</out>
				<xsl:for-each select="marker">
                    <xsl:variable name="j"><xsl:number format="01"/></xsl:variable>
					<t_doc_seq>
						<DOC_COTE><xsl:value-of select="concat($cote,' ',$j)"/></DOC_COTE>
						<DOC_TITRE><xsl:value-of select="name"/></DOC_TITRE>
						<DOC_RES><xsl:value-of select="comment"/></DOC_RES>
						<DOC_ID_TYPE_DOC>4</DOC_ID_TYPE_DOC>
						<in>
							<xsl:choose>
								<xsl:when test="in != -1"><xsl:value-of select="in + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="in"/></xsl:otherwise>
							</xsl:choose>
						</in>
						<out>
							<xsl:choose>
								<xsl:when test="out != -1"><xsl:value-of select="out + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="out"/></xsl:otherwise>
							</xsl:choose>
						</out>
					</t_doc_seq>
				</xsl:for-each>
				<xsl:for-each select="media/video/track/clipitem">
					<t_doc_mat>
						<pathurl><xsl:value-of select="file/pathurl"/></pathurl>
						<in>
							<xsl:choose>
								<xsl:when test="in != -1"><xsl:value-of select="in + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="in"/></xsl:otherwise>
							</xsl:choose>
						</in>
						<out>
							<xsl:choose>
								<xsl:when test="out != -1"><xsl:value-of select="out + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="out"/></xsl:otherwise>
							</xsl:choose>
						</out>
						<MAT_COTE_ORI><xsl:value-of select="file/timecode/reel/name"/></MAT_COTE_ORI>
					</t_doc_mat>
				</xsl:for-each>
			</DOC>
		</xsl:for-each>
		<xsl:for-each select="project/children/clip/media/video/track/clipitem">
			<MAT>
				<pathurl><xsl:value-of select="file/pathurl"/></pathurl>
				<in><xsl:value-of select="in"/></in>
				<out><xsl:value-of select="out"/></out>
				<offset><xsl:value-of select="file/timecode/frame"/></offset>
			</MAT>
		</xsl:for-each>

		<xsl:for-each select="clip">
			<xsl:variable name="offset">
				<xsl:choose>
				<xsl:when test="media/video/track/clipitem/file/timecode/frame!=''">
					<xsl:value-of select="media/video/track/clipitem/file/timecode/frame"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="cote"><xsl:value-of select="name"/></xsl:variable>
			<DOC>
				<DOC_COTE><xsl:value-of select="name"/></DOC_COTE>
				<DOC_TITRE><xsl:value-of select="name"/></DOC_TITRE>
				<DOC_RES><xsl:value-of select="logginginfo/description"/></DOC_RES>
				<DOC_NOTES><xsl:value-of select="logginginfo/lognote"/></DOC_NOTES>
				<duration><xsl:value-of select="duration"/></duration>
				<in>
					<xsl:choose>
						<xsl:when test="in != -1"><xsl:value-of select="in + $offset"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="in"/></xsl:otherwise>
					</xsl:choose>
				</in>
				<out>
					<xsl:choose>
						<xsl:when test="out != -1"><xsl:value-of select="out + $offset"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="out"/></xsl:otherwise>
					</xsl:choose>
				</out>
				<xsl:for-each select="marker">
                    <xsl:variable name="j"><xsl:number format="01"/></xsl:variable>
					<t_doc_seq>
						<DOC_COTE><xsl:value-of select="concat($cote,' ',$j)"/></DOC_COTE>
						<DOC_TITRE><xsl:value-of select="name"/></DOC_TITRE>
						<DOC_RES><xsl:value-of select="comment"/></DOC_RES>
						<DOC_ID_TYPE_DOC>4</DOC_ID_TYPE_DOC>
						<in>
							<xsl:choose>
								<xsl:when test="in != -1"><xsl:value-of select="in + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="in"/></xsl:otherwise>
							</xsl:choose>
						</in>
						<out>
							<xsl:choose>
								<xsl:when test="out != -1"><xsl:value-of select="out + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="out"/></xsl:otherwise>
							</xsl:choose>
						</out>
					</t_doc_seq>
				</xsl:for-each>
				<xsl:for-each select="media/video/track/clipitem">
					<t_doc_mat>
						<pathurl><xsl:value-of select="file/pathurl"/></pathurl>
						<in>
							<xsl:choose>
								<xsl:when test="in != -1"><xsl:value-of select="in + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="in"/></xsl:otherwise>
							</xsl:choose>
						</in>
						<out>
							<xsl:choose>
								<xsl:when test="out != -1"><xsl:value-of select="out + $offset"/></xsl:when>
								<xsl:otherwise><xsl:value-of select="out"/></xsl:otherwise>
							</xsl:choose>
						</out>
						<MAT_COTE_ORI><xsl:value-of select="file/timecode/reel/name"/></MAT_COTE_ORI>
					</t_doc_mat>
				</xsl:for-each>
			</DOC>
		</xsl:for-each>
		<xsl:for-each select="clip/media/video/track/clipitem">
			<MAT>
				<pathurl><xsl:value-of select="file/pathurl"/></pathurl>
				<in><xsl:value-of select="in"/></in>
				<out><xsl:value-of select="out"/></out>
				<offset><xsl:value-of select="file/timecode/frame"/></offset>
			</MAT>
		</xsl:for-each>
	</data>
</xsl:template>
</xsl:stylesheet>