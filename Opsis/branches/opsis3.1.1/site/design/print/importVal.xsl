<?xml version="1.0" encoding="utf-8"?>
<!-- 
import FCP 
VP : 13/09/2007 : Création du fichier
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

	<xsl:template match='/DATA'>
		<data>
			<xsl:for-each select="VAL">
				<VAL>
					<VAL_ID_TYPE_VAL><xsl:value-of select="id_type_val"/></VAL_ID_TYPE_VAL>
					<VALEUR><xsl:value-of select="valeur"/></VALEUR>
					<xsl:if test="valeur_EN!=''"><VALEUR_EN><xsl:value-of select="valeur_EN"/></VALEUR_EN></xsl:if>
					<xsl:if test="val_code!=''"><VAL_CODE><xsl:value-of select="val_code"/></VAL_CODE></xsl:if>
				</VAL>
			</xsl:for-each>
		</data>
	</xsl:template>

	<!-- 
	FORMAT_DUREE : Formate une dur√©e HH:MM:SS
	-->
    <xsl:template name="format_duree">
        <xsl:param name="chaine_duree"/>
            <!-- d√©coupage de la chaine -->
            <xsl:variable name="duree_hh"><xsl:text>00</xsl:text></xsl:variable>
            <xsl:variable name="duree_mm">
					<xsl:choose>
						<xsl:when test="contains($chaine_duree,'min')"><xsl:value-of select="format-number(normalize-space(substring-before($chaine_duree,'min')),'00')"/></xsl:when>
						<xsl:otherwise><xsl:text>00</xsl:text></xsl:otherwise>
					</xsl:choose>
			</xsl:variable>
            <xsl:variable name="duree_ss">
					<xsl:choose>
						<xsl:when test="contains($chaine_duree,'min')"><xsl:value-of select="format-number(normalize-space(substring-before(substring-after($chaine_duree,'min'),'sec')),'00')"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="format-number(normalize-space(substring-before($chaine_duree,'sec')),'00')"/></xsl:otherwise>
					</xsl:choose>
			</xsl:variable>

            <!-- Restitution -->
        	<xsl:value-of select="$duree_hh"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_mm"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_ss"/>
            
    </xsl:template>

<!-- 
FORMAT_DATE : Formate une date jj.mm.aaaa en aaaa-mm-jj
-->
    <xsl:template name="format_date">
        <xsl:param name="chaine_date"/>
		<!-- decoupage de la chaine -->
		<xsl:variable name="j"><xsl:value-of select="substring-before($chaine_date,'.')"/></xsl:variable>
		<xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine_date,'.'),'.')"/></xsl:variable>
		<xsl:variable name="a"><xsl:value-of select="substring-after(substring-after($chaine_date,'.'),'.')"/></xsl:variable>
        <xsl:value-of select="normalize-space($a)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($m)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($j)"/>
    </xsl:template>


</xsl:stylesheet>