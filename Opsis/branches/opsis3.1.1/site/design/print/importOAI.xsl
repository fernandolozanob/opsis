<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:oai="http://www.openarchives.org/OAI/2.0/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
    exclude-result-prefixes="oai oai_dc dc">
    <xsl:output
    method="xml"
    encoding="utf-8"
    omit-xml-declaration="no"
    indent="yes"
    />
    
    <xsl:template match='/'>
        <data>
            <xsl:for-each select="oai:OAI-PMH/oai:ListRecords/oai:record">
                <DOC>
                    <DOC_COTE><xsl:value-of select="oai:header/oai:identifier"/></DOC_COTE>
                    <DOC_TITRE><xsl:value-of select="oai:metadata/oai_dc:dc/dc:title"/></DOC_TITRE>
                    <DOC_RES><xsl:value-of select="oai:metadata/oai_dc:dc/dc:description"/></DOC_RES>
                    <DOC_DATE_PROD><xsl:value-of select="oai:metadata/oai_dc:dc/dc:date"/></DOC_DATE_PROD>
                    <xsl:for-each select="oai:metadata/oai_dc:dc/dc:subject">
                        <L_DE_MC_NC><xsl:value-of select="." /></L_DE_MC_NC>
                    </xsl:for-each>
                    <xsl:for-each select="oai:metadata/oai_dc:dc/dc:creator">
                        <P_GEN_AUT_PP><xsl:value-of select="." /></P_GEN_AUT_PP>
                    </xsl:for-each>
                </DOC>
            </xsl:for-each>
        </data>
    </xsl:template>
    
</xsl:stylesheet>

