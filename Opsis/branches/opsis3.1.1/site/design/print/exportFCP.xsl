<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 
<xsl:template match='/EXPORT_OPSIS'>

	<xmeml version="4">
		<project>
			<xsl:for-each select="t_panier">
				<name><xsl:value-of select="ID_PANIER"/></name>
			</xsl:for-each>
			<children>
				<xsl:for-each select="t_panier_doc">
					<xsl:if test="DOC/t_doc/t_doc_mat/DOC_MAT/t_mat/MAT_FORMAT='DV'">
						<xsl:variable name="offset">
							<xsl:call-template name="TC2Frame">
								<xsl:with-param name="chaine" select="DOC/t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_FORMAT='DV']/MAT_TCIN"/>
								<xsl:with-param name="offset" select="0"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="duration">
							<xsl:call-template name="TC2Frame">
								<xsl:with-param name="chaine" select="DOC/t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_FORMAT='DV']/MAT_TCOUT"/>
								<xsl:with-param name="offset" select="$offset"/>
							</xsl:call-template>
						</xsl:variable>
						<clip id="{id_ligne_panier}">
							<name>
								<xsl:choose>
									<xsl:when test="pdoc_extrait='1'"><xsl:value-of select="pdoc_ext_titre"/></xsl:when>
									<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></xsl:otherwise>
								</xsl:choose>
							</name>
							<duration><xsl:value-of select="$duration"/></duration>
							<rate>
								<ntsc>FALSE</ntsc>
								<timebase>25</timebase>
							</rate>
							<in>
								<xsl:choose>
									<xsl:when test="pdoc_extrait='1'">
										<xsl:call-template name="TC2Frame">
											<xsl:with-param name="chaine" select="pdoc_ext_tcin"/>
											<xsl:with-param name="offset" select="$offset"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="TC2Frame">
											<xsl:with-param name="chaine" select="DOC/t_doc/t_doc_mat/DOC_MAT/DMAT_TCIN"/>
											<xsl:with-param name="offset" select="$offset"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</in>
							<out>
								<xsl:choose>
									<xsl:when test="pdoc_extrait='1'">
										<xsl:call-template name="TC2Frame">
											<xsl:with-param name="chaine" select="pdoc_ext_tcout"/>
											<xsl:with-param name="offset" select="$offset"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="TC2Frame">
											<xsl:with-param name="chaine" select="DOC/t_doc/t_doc_mat/DOC_MAT/DMAT_TCOUT"/>
											<xsl:with-param name="offset" select="$offset"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</out>
							<xsl:for-each select="DOC/t_doc/t_doc_mat/DOC_MAT/t_mat[MAT_FORMAT='DV']">
								<file id="{ID_MAT}"> 
									<name><xsl:value-of select="MAT_NOM"/></name> 
									<pathurl><xsl:value-of select="concat('file://localhost/Volumes/Travaux/media/videos/',MAT_NOM)"/></pathurl>
									<duration><xsl:value-of select="$duration"/></duration>
									<timecode>
										<rate>
											<timebase>25</timebase>
										</rate>
										<string><xsl:value-of select="MAT_TCIN"/></string>
										<frame><xsl:value-of select="$offset"/></frame>
										<displayformat>NDF</displayformat>
										<source>source</source>
									</timecode>
								</file> 
							</xsl:for-each>
						</clip>
					</xsl:if>
				</xsl:for-each>
			</children>
		</project>
	</xmeml>
	
</xsl:template>
    <!-- 
    FORMAT_DUREE : Formate une durée HH:MM:SS en HH h MM m
    Appelle : 
    Appelé par : EXPORT_OPSIS
    -->
    <xsl:template name="TC2Frame">
        <xsl:param name="chaine"/>
        <xsl:param name="offset"/>
        
            <!-- découpage de la chaine -->
            <xsl:variable name="tc_hh"><xsl:value-of select="number(substring-before($chaine,':'))"/></xsl:variable>
            <xsl:variable name="tc_mm"><xsl:value-of select="number(substring-before(substring-after($chaine,':'),':'))"/></xsl:variable>
            <xsl:variable name="tc_ss"><xsl:value-of select="number(substring-before(substring-after(substring-after($chaine,':'),':'),':'))"/></xsl:variable>
            <xsl:variable name="tc_ii"><xsl:value-of select="number(substring-after(substring-after(substring-after($chaine,':'),':'),':'))"/></xsl:variable>
            
            <!-- Calcul -->
			<xsl:variable name="frames"><xsl:value-of select="((($tc_hh * 60 + $tc_mm) * 60 + $tc_ss) * 25 + $tc_ii) - $offset"/></xsl:variable>
			<xsl:choose>
				<xsl:when test="string(number($frames))= 'NaN'">0</xsl:when>
				<xsl:otherwise><xsl:value-of select="$frames"/></xsl:otherwise>
			</xsl:choose>
    </xsl:template>
	
</xsl:stylesheet>
