<?xml version="1.0" encoding="utf-8"?>
<!-- 
import FCP 
VP : 13/09/2007 : Création du fichier
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

	<xsl:template match='/DATA'>
		<data>
			<xsl:for-each select="LEX">
				<LEX>
					<LEX_ID_TYPE_LEX><xsl:value-of select="id_type_lex"/></LEX_ID_TYPE_LEX>
					<xsl:choose>
						<xsl:when test ="normalize-space(terme1)!=''"><LEX_TERME><xsl:value-of select="normalize-space(terme1)"/></LEX_TERME><level>1</level></xsl:when>
						<xsl:when test ="normalize-space(terme2)!=''"><LEX_TERME><xsl:value-of select="normalize-space(terme2)"/></LEX_TERME><level>2</level></xsl:when>
						<xsl:when test ="normalize-space(terme3)!=''"><LEX_TERME><xsl:value-of select="normalize-space(terme3)"/></LEX_TERME><level>3</level></xsl:when>
						<xsl:when test ="normalize-space(terme4)!=''"><LEX_TERME><xsl:value-of select="normalize-space(terme4)"/></LEX_TERME><level>4</level></xsl:when>
						<xsl:when test ="normalize-space(terme5)!=''"><LEX_TERME><xsl:value-of select="normalize-space(terme5)"/></LEX_TERME><level>5</level></xsl:when>
						<xsl:when test ="normalize-space(terme6)!=''"><LEX_TERME><xsl:value-of select="normalize-space(terme6)"/></LEX_TERME><level>6</level></xsl:when>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test ="normalize-space(terme2)!='' or normalize-space(terme3)!='' or normalize-space(terme4)!='' or normalize-space(terme5)!='' or normalize-space(terme6)!=''">
							<pre_father>1</pre_father>
						</xsl:when>
					</xsl:choose>
					<xsl:if test="terme_EN!=''"><LEX_TERME_EN><xsl:value-of select="terme_EN"/></LEX_TERME_EN></xsl:if>
					<xsl:if test="terme_EP!=''"><LEX_TERME_SYNO><xsl:value-of select="terme_EP"/></LEX_TERME_SYNO></xsl:if>
					<xsl:if test="terme_TA!=''"><LEX_TERME_ASSO><xsl:value-of select="terme_TA"/></LEX_TERME_ASSO></xsl:if>
					<xsl:if test="lex_note!=''"><LEX_NOTE><xsl:value-of select="lex_note"/></LEX_NOTE></xsl:if>
					<xsl:if test="notes!=''"><LEX_NOTE><xsl:value-of select="notes"/></LEX_NOTE></xsl:if>
					<xsl:if test="lex_code!=''"><LEX_CODE><xsl:value-of select="lex_code"/></LEX_CODE></xsl:if>
				</LEX>
			</xsl:for-each>
		</data>
	</xsl:template>


</xsl:stylesheet>