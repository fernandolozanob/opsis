<?xml version="1.0" encoding="utf-8"?>
<!-- export Tabulé XS : 13/10/2005 : Création du fichier -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../liste.xsl" />
	<xsl:include href="../fonctions.xsl" />
	<xsl:output method="text" encoding="utf-8" indent="yes" />
	<xsl:param name="hasheader" /><!-- défini si on a besoin de redéfinir 
		les headers & les page-master -->
	<xsl:param name="custom_fields" />
	<xsl:param name="xmlfile" />
	<!-- Pour info... tabulation = &#09; Retour à la ligne = &#x00d;&#x00a; -->

	<!-- EXPORT_OPSIS : Affiche les documents formatés Appelle : Appelé par 
		: -->
	<xsl:template match="/EXPORT_OPSIS">
		<xsl:choose>
			<xsl:when test="$custom_fields != '' or $xmlfile!=''">
				<xsl:variable name="src_xml">
					<xsl:choose>
						<xsl:when test="$custom_fields!=''">
							<xsl:value-of select="$custom_fields" />
						</xsl:when>
						<xsl:when test="$xmlfile!=''">
							<xsl:value-of select="$xmlfile" />
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="xmllist" select="document($src_xml)" />

				<xsl:call-template name="displayTabListe">
					<xsl:with-param name="xmllist" select="$xmllist" />
					<xsl:with-param name="hasheader" select="$hasheader" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
				<!-- NOM DES COLONNES -->
				<xsl:for-each select="t_doc[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="t_mat[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="t_usager[position() = 1]/child::*">
					<xsl:if test="name()!='liste_groupe'">
						<xsl:value-of select="name()" />
						<xsl:text>	</xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="t_lexique[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="t_val[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="t_imageur[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="t_panier[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="t_stat[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="t_conso[position() = 1]/child::*">
					<xsl:value-of select="name()" />
					<xsl:text>	</xsl:text>
				</xsl:for-each>
				<xsl:text>
</xsl:text>


				<!-- DOC -->
				<xsl:for-each select="t_doc">
					<xsl:for-each select="child::*">
						<xsl:choose>
							<!-- t_doc_lex -->
							<xsl:when test="name()='t_doc_lex'">
								<!-- Liste à plat: LEX_[type] [ROLE] : [LEX_TERME] (PRECISION); -->
								<xsl:if test="count(TYPE_DESC) &gt; 0">
									<xsl:for-each select="TYPE_DESC">
										<xsl:value-of select="@ID_TYPE_DESC" />
										<xsl:text>:</xsl:text>
										<xsl:for-each select="t_lex">
											<xsl:value-of select="LEX_ID_TYPE_LEX" />
											<xsl:text>!</xsl:text>
											<xsl:value-of select="LEX_TERME" />
											<xsl:if test="LEX_PRECISION!=''">
												<xsl:text> (</xsl:text>
												<xsl:value-of select="LEX_PRECISION" />
												<xsl:text>)</xsl:text>
											</xsl:if>
											<!-- Ajout d'un séparateur -->
											<xsl:if test="following-sibling::t_lex">
												<xsl:text>/</xsl:text>
											</xsl:if>
										</xsl:for-each>
										<!-- Ajout d'un séparateur -->
										<xsl:if test="following-sibling::TYPE_DESC">
											<xsl:text>;</xsl:text>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
								<xsl:text>	</xsl:text>
							</xsl:when>
							<!-- t_doc_val -->
							<xsl:when test="name()='t_doc_val'">
								<!-- Liste à plat: VAL_[type] -->
								<xsl:if test="count(TYPE_VAL) &gt; 0">
									<xsl:for-each select="TYPE_VAL">
										<xsl:value-of select="@ID_TYPE_VAL" />
										<xsl:text>:</xsl:text>
										<xsl:for-each select="t_val">
											<xsl:value-of select="VALEUR" />
											<!-- Ajout d'un séparateur -->
											<xsl:if test="following-sibling::t_val">
												<xsl:text>/</xsl:text>
											</xsl:if>
										</xsl:for-each>
										<!-- Ajout d'un séparateur -->
										<xsl:if test="following-sibling::TYPE_VAL">
											<xsl:text>;</xsl:text>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
								<xsl:text>	</xsl:text>
							</xsl:when>


							<xsl:when test="name()='t_doc_cat'">
								<!-- Liste à plat -->
								<xsl:for-each select="CAT">
									<xsl:value-of select="FEST_LIBELLE" />
									<!-- Ajout d'un séparateur -->
									<xsl:if test="following-sibling::CAT">
										<xsl:text>;</xsl:text>
									</xsl:if>
								</xsl:for-each>
								<xsl:text>	</xsl:text>
							</xsl:when>
							<!-- t_doc_mat -->
							<xsl:when test="name()='t_doc_mat'">
								<!-- Liste à plat -->
								<xsl:for-each select="DOC_MAT">
									<xsl:value-of select="MAT_NOM" />
									<xsl:text> (</xsl:text>
									<xsl:value-of select="MAT_FORMAT" />
									<xsl:text>)</xsl:text>
									<!-- Ajout d'un séparateur -->
									<xsl:if test="following-sibling::DOC_MAT">
										<xsl:text>;</xsl:text>
									</xsl:if>
								</xsl:for-each>
								<xsl:text>	</xsl:text>
							</xsl:when>
							<!-- Propriétés de t_doc -->
							<xsl:otherwise>
								<xsl:value-of select="." />
								<xsl:text>	</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_doc">
						<xsl:text>
	</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- MATERIEL -->
				<xsl:for-each select="t_mat">
					<xsl:for-each select="child::*">
						<xsl:value-of select="." />
						<xsl:text>	</xsl:text>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_mat">
						<xsl:text>
	</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- USAGER -->
				<xsl:for-each select="t_usager">
					<xsl:for-each select="child::*">
						<xsl:if test="name()!='liste_groupe'">
							<xsl:value-of select="." />
							<xsl:text>	</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_usager">
						<xsl:text>
	</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- LEXIQUE -->
				<xsl:for-each select="t_lexique">
					<xsl:for-each select="child::*">
						<xsl:choose>
							<!-- lang -->
							<xsl:when test="name()='lang'">
								<!-- Liste à plat -->
								<xsl:if test="count(ID_LANG) &gt; 0">
									<xsl:for-each select="ID_LANG">
										<xsl:value-of select="@ID_LANG" />
										<xsl:text>:</xsl:text>
										<xsl:value-of select="LEX_TERME" />
										<xsl:text>!</xsl:text>
										<xsl:if test="LEX_NOTE!=''">
											<xsl:text> (</xsl:text>
											<xsl:value-of select="LEX_NOTE" />
											<xsl:text>)</xsl:text>
										</xsl:if>
										<xsl:value-of select="LEX_GEN" />
										<xsl:text>!</xsl:text>
										<xsl:value-of select="LEX_SYN" />
										<!-- Ajout d'un séparateur -->
										<xsl:if test="following-sibling::ID_LANG">
											<xsl:text>||</xsl:text>
										</xsl:if>
									</xsl:for-each>
									<xsl:text>	</xsl:text>
								</xsl:if>
							</xsl:when>
							<!-- Propriétés de t_lexique -->
							<xsl:otherwise>
								<xsl:value-of select="." />
								<xsl:text>	</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_lexique">
						<xsl:text>
	</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- VALEUR -->
				<xsl:for-each select="t_val">
					<xsl:for-each select="child::*">
						<xsl:value-of select="." />
						<xsl:text>	</xsl:text>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_val">
						<xsl:text>
	</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- IMAGEUR -->
				<xsl:for-each select="t_imageur">
					<xsl:for-each select="child::*">
						<xsl:choose>
							<!-- t_image -->
							<xsl:when test="name()='liste_image'">
								<!-- Liste à plat -->
								<xsl:if test="@nb &gt; 0">
									<xsl:for-each select="t_image">
										<xsl:value-of select="@ID_IMAGE" />
										<xsl:text>:</xsl:text>
										<xsl:value-of select="IM_CHEMIN" />
										<xsl:text>;</xsl:text>
										<xsl:value-of select="IM_TC" />
										<xsl:if test="IM_TEXT!=''">
											<xsl:text>(</xsl:text>
											<xsl:value-of select="IM_TEXT" />
											<xsl:text>)</xsl:text>
										</xsl:if>
										<!-- Ajout d'un séparateur -->
										<xsl:if test="following-sibling::t_image">
											<xsl:text>||</xsl:text>
										</xsl:if>
									</xsl:for-each>
									<xsl:text>	</xsl:text>
								</xsl:if>
							</xsl:when>
							<!-- Propriétés de t_lexique -->
							<xsl:otherwise>
								<xsl:value-of select="." />
								<xsl:text>	</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_imageur">
						<xsl:text>
	</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- PANIER -->
				<xsl:for-each select="t_panier">
					<xsl:for-each select="child::*">
						<xsl:value-of select="." />
						<xsl:text>	</xsl:text>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_panier">
						<xsl:text>
	</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- STATS -->
				<xsl:for-each select="t_stat">
					<xsl:for-each select="child::*">
						<xsl:value-of select="." />
						<xsl:text>	</xsl:text>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_stat">
						<xsl:text>
</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- CONSO -->
				<xsl:for-each select="t_conso">
					<xsl:for-each select="child::*">
						<xsl:value-of select="." />
						<xsl:text>	</xsl:text>
					</xsl:for-each>
					<!-- Ligne de séparation entre les documents -->
					<xsl:if test="following-sibling::t_conso">
						<xsl:text>
</xsl:text>
					</xsl:if>
				</xsl:for-each>

				<!-- TODO : IL SERAIT MIEUX DE PASSER ENTITY EN PARAMETRE DE L'APPEL 
					A CE XSL ET DE PASSER PAR LE CAS PAR DEFAUT CI DESSOUS DESACTIVE POUR L'INSTANT 
					CAR CAUSE DES DOUBLONS SINON -->
				<!-- CAS GENERAL -->
				<!--xsl:for-each select="./*[starts-with(name(),'t_')]"> <xsl:variable 
					name="entity" select="name(.)"/> <xsl:for-each select="child::*"> <xsl:value-of 
					select="."/><xsl:text> </xsl:text> </xsl:for-each> <xsl:if test="following-sibling::*[name() 
					= $entity]"> <xsl:text> </xsl:text> </xsl:if> </xsl:for-each -->

			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
