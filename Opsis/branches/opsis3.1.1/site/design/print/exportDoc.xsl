<?xml version="1.0" encoding="utf-8"?>
<!--
export Texte 

VP : 06/12/2005 : Création du fichier
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="text" 
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->

	<xsl:template match='EXPORT_OPSIS'>
			<xsl:for-each select="t_doc">
				<xsl:if test="DOC_COTE!=''">
					<xsl:text>Numéro :	</xsl:text>
					<xsl:value-of select="DOC_COTE"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="US_MODIF!=''">
					<xsl:text>Documentaliste :	</xsl:text>
					<xsl:value-of select="US_MODIF"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_DATE_CREA!='' and DOC_DATE_CREA!='0000-00-00'">
					<xsl:text>Date arrivée :	</xsl:text>
					<xsl:value-of select="substring(DOC_DATE_CREA,1,10)"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_DATE_PROD!=''">
					<xsl:text>Date production :	</xsl:text>
					<xsl:value-of select="DOC_DATE_PROD"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_DATE_PV_DEBUT!='' and DOC_DATE_PV_DEBUT!='0000-00-00'">
					<xsl:text>Date tournage :	</xsl:text>
					<xsl:value-of select="DOC_DATE_PV_DEBUT"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_DUREE!=''">
					<xsl:text>Durée : </xsl:text>
					<xsl:value-of select="DOC_DUREE"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_XML/XML/DOC/FONDS!=''">
					<xsl:text>Fonds : </xsl:text>
					<xsl:value-of select="DOC_XML/XML/DOC/FONDS"/><xsl:text>
</xsl:text>
				</xsl:if>				
				<xsl:if test="TYPE_DOC!=''">
					<xsl:text>Type document : </xsl:text>
					<xsl:value-of select="TYPE_DOC"/><xsl:text>
</xsl:text>
				</xsl:if>				

				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='ORI']/t_val/VALEUR!=''">
					<xsl:text>Format : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='ORI']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='RANG']/t_val/VALEUR!=''">
					<xsl:text>Rangement : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='RANG']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_TITRE!=''">
					<xsl:text>Titre : </xsl:text>
					<xsl:value-of select="DOC_TITRE"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_AUTRE_TITRE!=''">
					<xsl:text>Titre provisoire : </xsl:text>
					<xsl:value-of select="DOC_AUTRE_TITRE"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_TITRE_COL!=''">
					<xsl:text>Titre collection : </xsl:text>
					<xsl:value-of select="DOC_TITRE_COL"/><xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_TITREORI!=''">
					<xsl:text>Titre original : </xsl:text>
					<xsl:value-of select="DOC_TITREORI"/><xsl:text>
</xsl:text>
				</xsl:if>



				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='THM']/t_val/VALEUR!=''">
					<xsl:text>Theme : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='THM']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='THEM']/t_val/VALEUR!=''">
					<xsl:text>Thématique : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='THEM']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				
				<xsl:if test="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='MT']!=''">
					<xsl:text>Matière : </xsl:text>
					
						<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='MT']">
								<xsl:for-each select="t_lex">
									<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX" /></xsl:variable>
									<xsl:value-of select="LEX_TERME"/>
									<!-- Ajout d'un séparateur -->
									<xsl:choose>
									<xsl:when test="following-sibling::t_lex/LEX_TERME"> 
										<xsl:text>; </xsl:text></xsl:when>
									<xsl:otherwise><br/></xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='MV']!=''">
					<xsl:text>Mouvement : </xsl:text>
					
						<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='MV']">
								<xsl:for-each select="t_lex">
									<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX" /></xsl:variable>
									<xsl:value-of select="LEX_TERME"/>
									<!-- Ajout d'un séparateur -->
									<xsl:choose>
									<xsl:when test="following-sibling::t_lex/LEX_TERME"> 
										<xsl:text>; </xsl:text></xsl:when>
									<xsl:otherwise><br/></xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				
				<xsl:if test="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='LG']!=''">
					<xsl:text>Lieu : </xsl:text>
					
						<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='LG']">
								<xsl:for-each select="t_lex">
									<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX" /></xsl:variable>
									<xsl:value-of select="LEX_TERME"/>
									<!-- Ajout d'un séparateur -->
									<xsl:choose>
									<xsl:when test="following-sibling::t_lex/LEX_TERME"> 
										<xsl:text>; </xsl:text></xsl:when>
									<xsl:otherwise><xsl:text>
</xsl:text></xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				
				<xsl:if test="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='PE']!=''">
					<xsl:text>Personnalité : </xsl:text>
					
						<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='PE']">
								<xsl:for-each select="t_lex">
									<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX" /></xsl:variable>
									<xsl:value-of select="LEX_TERME"/>
									<!-- Ajout d'un séparateur -->
									<xsl:choose>
									<xsl:when test="following-sibling::t_lex/LEX_TERME"> 
										<xsl:text>; </xsl:text></xsl:when>
									<xsl:otherwise><xsl:text>
</xsl:text></xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="DOC_RES_CAT!=''">
					<xsl:text>Résume diffusion : </xsl:text>
					<xsl:value-of select="DOC_RES_CAT"/><xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="DOC_RES!=''">
					<xsl:text>Résumé : </xsl:text>
					<xsl:value-of select="DOC_RES"/><xsl:text>
</xsl:text>
				</xsl:if>
				
				<xsl:if test="DOC_SEQ!=''">
					<xsl:text>Séquences : </xsl:text>
					<xsl:value-of select="translate(DOC_SEQ,'|','')"/><xsl:text>
</xsl:text>
				</xsl:if>
				
				<xsl:if test="DOC_NOTES!=''">
					<xsl:text>Notes : </xsl:text>
					<xsl:value-of select="DOC_NOTES"/><xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="DOC_RECOMP!=''">
					<xsl:text>Remerciements : </xsl:text>
					<xsl:value-of select="DOC_RECOMP"/><xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="DOC_NOTE_INTERNE!=''">
					<xsl:text>Matériel : </xsl:text>
					<xsl:value-of select="DOC_NOTE_INTERNE"/><xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='PRD']/t_val/VALEUR!=''">
					<xsl:text>Production : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='PRD']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='EXE']/t_val/VALEUR!=''">
					<xsl:text>Production exécutive : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='EXE']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>

				<xsl:if test="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='CM']!=''">
					<xsl:text>Commanditaire : </xsl:text>
					
						<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='DE']/t_rol[@DLEX_ID_ROLE='CM']">
								<xsl:for-each select="t_lex">
									<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX" /></xsl:variable>
									<xsl:value-of select="LEX_TERME"/>
									<!-- Ajout d'un séparateur -->
									<xsl:choose>
									<xsl:when test="following-sibling::t_lex/LEX_TERME"> 
										<xsl:text>; </xsl:text></xsl:when>
									<xsl:otherwise><br/></xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				
				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='AUT']/t_val/VALEUR!=''">
					<xsl:text>Auteur : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='AUT']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='REA']/t_val/VALEUR!=''">
					<xsl:text>Réalisateur : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='REA']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='CPR']/t_val/VALEUR!=''">
					<xsl:text>Chef de projet : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='CPR']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='COP']/t_val/VALEUR!=''">
					<xsl:text>Chef opérateur : </xsl:text>
					
						<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='COP']/t_val">
							<xsl:value-of select="VALEUR"/>
							<!-- Ajout d'un séparateur -->
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:text>
</xsl:text>
				</xsl:if>
				<xsl:if test="DOC_ACC!=''">
					<xsl:text>Générique : </xsl:text>
					<xsl:value-of select="DOC_ACC"/><xsl:text>
</xsl:text>
				</xsl:if>

					<!-- Séparateur de documents -->
<xsl:text>
</xsl:text>
			</xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
