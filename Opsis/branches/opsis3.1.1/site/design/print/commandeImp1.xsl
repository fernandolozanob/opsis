<?xml version="1.0" encoding="utf-8"?>
<!-- commande1.xsl (Droits)--><!-- XS : 24/10/2005 : Création du fichier --><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="imgurl"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />

<!-- PANIER_COMMANDE : Affichage du panier -->    <xsl:template match='EXPORT_OPSIS'>
	<html><head/>
	<body>
		<style type="text/css">
		 body {			font-family: Arial,sans-serif;			color: #333333;			margin: 0px;			padding: 0px;			background-color: #FFFFFF;		 }		.label_champs_form {			font-family: Arial;			font-size: 11px;
			font-weight:bold;			color: #333366;
			text-align: right;
			padding-right:3px;		}
		.val_champs_form, .val_champs_form input,  .val_champs_form select,  .val_champs_form textarea {			font-family: Arial;			font-size: 11px;
			color: #333366;			text-align:left;
			padding-left:4px;
		}
		.type {			font-family: Arial,Geneva,Helvetica,sans-serif;			font-size:16px;			color: #333366;			font-weight:bold;		}		.small_text {			font-family: Arial,Geneva,Helvetica,sans-serif;			font-size: 10px;
			color: #333366;			text-align:left;			align:top;			}		#altern0 { background-color: #f5f7f7; }		#altern1 { background-color: #FFFFFF; }		.resultsBar {			font-family: Arial;			font-size: 10pt;			font-style: normal;
			line-height: normal;
			color: #333366;
			text-decoration: none;
			font-weight: bold;			background-color: #ffffff;		}		.tableResults {			width: 95%;			color: #333366;			margin-top:2px;		}		.resultsHead {			font-family: "Trebuchet MS" ;			font-size: 12px;			color: #333366;			text-decoration: none;			font-weight: bold;			padding-left:3px;
			border-bottom: 2px solid #333366;			text-align: left;					}
		.resultsCorps {
			font-family: Arial;			font-size: 11px;
			color: #333366;			font-weight: normal;
			border-bottom: 1px solid #333366;		}
		.resultsCorps td {	border-bottom: 1px solid #333366; padding-left:4px; }				.info_entete {margin:10px;color:black;width:95%;border:1px solid #666666;background-color:#FFCC66;font-size:12px;padding:3px;font-family:"Trebuchet MS",sans-serif;}				</style>		<xsl:variable name="imgurl"><xsl:text>&lt;?php echo kCheminHttp ?&gt;/design/images/</xsl:text></xsl:variable>		
		<h3><xsl:processing-instruction name="php">print kCommande;</xsl:processing-instruction></h3>		<br/>		<p><xsl:processing-instruction name="php">print kCommandeEntete;</xsl:processing-instruction></p>        <!-- Informations sur le panier -->		<br/>        <table border="0" cellspacing="0" cellpadding="0">			<tr>			<td width="150"><img src="{$imgurl}pixel.gif" height="1"/></td>			<td width="150"><img src="{$imgurl}pixel.gif" height="1"/></td>			<td width="300"><img src="{$imgurl}pixel.gif" height="1"/></td>			</tr>							            <tr>                <td class="label_champs_form"><xsl:processing-instruction name="php">print kNumeroCommande;</xsl:processing-instruction></td>                <td class="type" >					<xsl:value-of select="t_panier/ID_PANIER"/>
                </td>                <td class="val_champs_form">                	<span class="label_champs_form" ><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction> :                    </span>                	<xsl:value-of select="t_panier/t_usager/US_NOM"/> <xsl:value-of select="t_panier/t_usager/US_PRENOM"/>                </td>
            </tr>
             <tr>               <td class="label_champs_form"><xsl:processing-instruction name="php">print kTypeCommande;</xsl:processing-instruction></td>                <td class="val_champs_form" >					<xsl:value-of select="t_panier/TYPE_COMMANDE"/>                </td>                <td class="val_champs_form" >                 	<span class="label_champs_form" ><xsl:processing-instruction name="php">print kSociete;</xsl:processing-instruction> :                    </span>                    <xsl:value-of select="t_panier/t_usager/US_SOCIETE"/>                </td>
            </tr>
            <!-- Informations spécifiques PAN_XML -->            <tr>                <td class="label_champs_form"><xsl:processing-instruction name="php">print kComObjet;</xsl:processing-instruction></td>				<td class="val_champs_form" colspan="2">                            <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_OBJET"/>                </td>
            </tr>            <tr>                <td class="label_champs_form"><xsl:processing-instruction name="php">print kAdresseLivraison;</xsl:processing-instruction></td>				<td class="val_champs_form" colspan="2">                            <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON"/>                </td>
            </tr>        </table>

        <!-- Détail de la commande -->        <p>        <table width="95%" align="center" border="1" cellspacing="0" class="tableResults">            <tr>                <td class="resultsHead" ><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></td>                <td class="resultsHead"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>                                <td class="resultsHead"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>                <td class="resultsHead"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>                <td class="resultsHead"><xsl:processing-instruction name="php">print kTCin;</xsl:processing-instruction></td>                <td class="resultsHead"><xsl:processing-instruction name="php">print kTCout;</xsl:processing-instruction></td>            </tr>
            <xsl:for-each select="t_panier_doc">
                <tr class="resultsCorps" height="50" >                    <td class="resultsCorps" >
						<xsl:choose>						<xsl:when test="pdoc_extrait='1'"><xsl:processing-instruction name="php">print kExtrait;</xsl:processing-instruction></xsl:when>						<xsl:otherwise><xsl:value-of select="DOC/t_doc/TYPE_DOC"/></xsl:otherwise>						</xsl:choose>					</td>
                    <td class="resultsCorps" ><xsl:value-of select="DOC/t_doc/DOC_TITRE"/><br/>					<xsl:value-of select="pdoc_ext_titre"/></td>                    <td class="resultsCorps" ><xsl:value-of select="DOC/t_doc/DOC_DATE_PROD"/></td>                    <td class="resultsCorps" >
						<xsl:choose>
						<xsl:when test="pdoc_extrait='1'"><xsl:value-of select="ext_duree"/></xsl:when>						<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:otherwise>						</xsl:choose>
					</td>
                    <td class="resultsCorps" ><xsl:if test="pdoc_extrait='1'"><xsl:value-of select="pdoc_ext_tcin"/></xsl:if></td>                    <td class="resultsCorps" ><xsl:if test="pdoc_extrait='1'"><xsl:value-of select="pdoc_ext_tcout"/></xsl:if></td>                </tr>
            </xsl:for-each>
        </table>        </p>
		</body>
	</html>
    </xsl:template>
</xsl:stylesheet>

