<?xml version="1.0" encoding="utf-8"?>
<!--
mail JOB 
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="text" 
        encoding="utf-8" 
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="boundary" />

<!-- 
EXPORT_OPSIS 
-->
    <xsl:template match="/EXPORT_OPSIS">
<xsl:if test="$boundary!=''" >
--<xsl:value-of select="$boundary" />
Content-Type:text/plain;charset=ISO-8859-1\r\n
</xsl:if>

            <xsl:text>Bonjour, 
 </xsl:text>
 <xsl:choose>
	<xsl:when test="JOB_IN and JOB_IN != ''">
<xsl:text>Une nouvelle vidéo a été traitée :
</xsl:text>
            <xsl:text>Titre :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_TITRE"/><xsl:text>
</xsl:text>
            <xsl:text>Référence :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_COTE"/><xsl:text>
</xsl:text>
            <xsl:text>Date de diffusion :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_DATE_PROD"/><xsl:text>
</xsl:text>
            <xsl:text>Durée :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_DUREE"/><xsl:text>

</xsl:text>
            <xsl:text>Message généré automatiquement par Opsis Media.
</xsl:text>
	</xsl:when>
	<xsl:when test="JOB_CHILDS  and JOB_CHILDS != '' and JOB_CHILDS/JOB_CHILD!=''">
		<xsl:choose>
			<xsl:when test="count(JOB_CHILDS/JOB_CHILD)&gt;1">
		<xsl:text>De nouvelles vidéos ont été traitées :
</xsl:text>
			</xsl:when>
			<xsl:otherwise>
			<xsl:text>Une nouvelle vidéo a été traitée :
</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:for-each select="JOB_CHILDS/JOB_CHILD">
			 <xsl:text>Titre :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_TITRE"/><xsl:text>
</xsl:text>
            <xsl:text>Référence :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_COTE"/><xsl:text>
</xsl:text>
            <xsl:text>Date de diffusion :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_DATE_PROD"/><xsl:text>
</xsl:text>
            <xsl:text>Durée :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_DUREE"/><xsl:text>
</xsl:text>
<xsl:text>
</xsl:text>
		</xsl:for-each>
			<xsl:text>Message généré automatiquement par Opsis Media.
</xsl:text>
	</xsl:when>
</xsl:choose>


<xsl:if test="$boundary!=''" >
--<xsl:value-of select="$boundary" />--
</xsl:if>



    </xsl:template>

</xsl:stylesheet>