<?xml version="1.0" encoding="utf-8"?>
<!-- lexListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->

    <xsl:template match="EXPORT_OPSIS">
		<fieldset>
		<xsl:for-each select="t_lexique">
            <xsl:variable name ="addValue">
                <xsl:value-of select="concat('t_doc_lexDE',LEX_ID_TYPE_LEX,'$index')"/>
            </xsl:variable>
			<xsl:value-of select="LEX_TERME"/><br/>
            <xsl:choose>
                <xsl:when test="SYNO_PREF != ''">
                    <xsl:text>Employer terme : </xsl:text>
                    <a href='#' onclick="javascript:window.parent.addValue('{$addValue}','{SYNO_PREF}','{LEX_ID_SYN}');"><xsl:value-of select="SYNO_PREF"/></a><br/>
                </xsl:when>
                <xsl:when test="t_lexique_syno_pref != ''">
                    <xsl:text>Employer terme : </xsl:text>
                    <xsl:for-each select="t_lexique_syno_pref/t_lexique">
                        <xsl:variable name="LEX_TERME"><xsl:value-of select="LEX_TERME"/></xsl:variable> 
                        <xsl:variable name="ID_LEX"><xsl:value-of select="ID_LEX"/></xsl:variable> 
                        <a href='#' onclick="javascript:window.parent.addValue('{$addValue}','{$LEX_TERME}','{$ID_LEX}');"><xsl:value-of select="LEX_TERME"/></a>
                        <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
                    </xsl:for-each>
                    <br/>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="t_lexique_syno != ''">
				<xsl:text>Employé pour : </xsl:text>
				<xsl:for-each select="t_lexique_syno/t_lexique">
					<xsl:value-of select="LEX_TERME"/>
					<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
				</xsl:for-each>
				<br/>
			</xsl:if>
            <xsl:choose>
                <xsl:when test="LEX_FATHER != ''">
                    <xsl:text>Générique : </xsl:text> <xsl:value-of select="LEX_FATHER"/><br/>
                </xsl:when>
                <xsl:when test="t_lexique_father != ''">
                    <xsl:text>Générique : </xsl:text>
                    <xsl:for-each select="t_lexique_father/t_lexique">
                        <xsl:value-of select="LEX_TERME"/>
                        <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
                    </xsl:for-each>
                    <br/>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="t_lexique_children != ''">
				<xsl:text>Spécifiques : </xsl:text>
				<xsl:for-each select="t_lexique_children/t_lexique">
					<xsl:value-of select="LEX_TERME"/>
					<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
				</xsl:for-each>
				<br/>
			</xsl:if>
			<xsl:if test="t_lexique_asso != ''">
				<xsl:text>Termes associés : </xsl:text>
				<xsl:for-each select="t_lexique_asso/t_lexique">
					<xsl:value-of select="LEX_TERME"/>
					<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
				</xsl:for-each>
				<br/>
			</xsl:if>
		</xsl:for-each>
		</fieldset>
    </xsl:template>
</xsl:stylesheet>
