<?xml version="1.0" encoding="utf-8"?>
<!-- commande1.xsl (Droits)-->
<!-- XS : 24/10/2005 : Création du fichier -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="boundary" />

<!-- PANIER_COMMANDE : Affichage du panier -->
    <xsl:template match='EXPORT_OPSIS'>
<xsl:variable name="mail"><xsl:value-of select="t_panier/t_usager/US_SOC_MAIL"/></xsl:variable>
<xsl:variable name="total_ht"><xsl:value-of select="t_panier/TOTAL_HT" /></xsl:variable>
<xsl:variable name="frais_ht"><xsl:value-of select="t_panier/FRAIS_HT" /></xsl:variable>
<xsl:variable name="pan_tva"><xsl:value-of select="t_panier/PAN_TVA" /></xsl:variable>
<xsl:variable name="total_tva"><xsl:value-of select="round(($total_ht + $frais_ht)* $pan_tva) * 0.01" /></xsl:variable>
<xsl:variable name="total_ttc"><xsl:value-of select="round(($total_ht + $frais_ht)* (100 + $pan_tva)) * 0.01" /></xsl:variable>

<xsl:if test="$boundary!=''" >
	This is a multi-part message in MIME format.
--<xsl:value-of select="$boundary" />
Content-Type: text/plain; charset="iso-8859-1"
Content-Transfer-Encoding: 8bit


<xsl:processing-instruction name="php">echo kCommandeRecap;</xsl:processing-instruction > 
<xsl:text>

</xsl:text>	
<xsl:text>CNRS Images Vidéothèque

</xsl:text >
	<xsl:choose>
	<xsl:when test="PAN_ID_TRANS!=''">
		<xsl:processing-instruction name="php">print kCommandeEnteteAchatEnLigne;</xsl:processing-instruction>
	</xsl:when>
	<xsl:otherwise>
		<xsl:processing-instruction name="php">print kCommandeEnteteAchatAutre;</xsl:processing-instruction>
	</xsl:otherwise>
	</xsl:choose>
<xsl:text>
</xsl:text>	
	<xsl:processing-instruction name="php">print kTypeCommande;</xsl:processing-instruction> : <xsl:value-of select="t_panier/TYPE_COMMANDE"/>
<xsl:text>

</xsl:text>	
	<xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction> : <xsl:value-of select="t_panier/t_usager/US_NOM"/> <xsl:value-of select="t_panier/t_usager/US_PRENOM"/>
<xsl:text>
</xsl:text>	
	<xsl:choose>
		<xsl:when test="t_panier/t_usager/US_SOCIETE!=''">
		<xsl:processing-instruction name="php">print kSociete;</xsl:processing-instruction> : <xsl:value-of select="t_panier/t_usager/US_SOCIETE"/>
		</xsl:when>
		<xsl:otherwise>&#160;</xsl:otherwise>
	</xsl:choose>
<xsl:text>
</xsl:text>	
	<xsl:processing-instruction name="php">print kMail;</xsl:processing-instruction> : <xsl:value-of select="$mail" />
<xsl:text>
</xsl:text>	
	<xsl:processing-instruction name="php">print kNumeroCommande;</xsl:processing-instruction> : <xsl:value-of select="t_panier/ID_PANIER"/>
<xsl:text>
</xsl:text>	
	<xsl:processing-instruction name="php">print kDateCommande;</xsl:processing-instruction> : <xsl:call-template name="format_date">
						<xsl:with-param name="chaine_date" select="t_panier/PAN_DATE_COM"/></xsl:call-template>
<xsl:text>
</xsl:text>	
	<xsl:if test="t_panier/PAN_ID_TRANS!=''">
		<xsl:processing-instruction name="php">print kNumeroTransaction;</xsl:processing-instruction> : <xsl:value-of select="t_panier/PAN_ID_TRANS" />
<xsl:text>
</xsl:text>	
	</xsl:if>
	<xsl:processing-instruction name="php">print kAdresseLivraison;</xsl:processing-instruction> : <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON"/>
<xsl:text>
</xsl:text>	
	<xsl:processing-instruction name="php">print kAdresseFacturation;</xsl:processing-instruction> : <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION"/>
<xsl:text>
</xsl:text>	
   <xsl:processing-instruction name="php">print kConditionExploitation;</xsl:processing-instruction> : <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_CONDITION_EXPLOITATION"/>
<xsl:text>
</xsl:text>	
	<xsl:processing-instruction name="php">print kDateEmprunt;</xsl:processing-instruction> : <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_DATE_EMPRUNT"/>
<xsl:text>
</xsl:text>	
	<xsl:processing-instruction name="php">print kDateRetour;</xsl:processing-instruction> : <xsl:value-of select="t_panier/PAN_XML/commande/PANXML_DATE_RETOUR"/>
<xsl:text>


</xsl:text>	
	<xsl:processing-instruction name="php">print kNumero;</xsl:processing-instruction><xsl:text>	</xsl:text><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction><xsl:text>					</xsl:text><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction><xsl:text>	</xsl:text><xsl:processing-instruction name="php">print kSupport;</xsl:processing-instruction><xsl:text>	</xsl:text><xsl:processing-instruction name="php">print kNombre;</xsl:processing-instruction><xsl:text>	</xsl:text><xsl:processing-instruction name="php">print kVersion;</xsl:processing-instruction><xsl:text>	</xsl:text><xsl:processing-instruction name="php">print kPrixHT;</xsl:processing-instruction>
<xsl:text>

</xsl:text>
<xsl:for-each select="t_panier_doc">
	<xsl:value-of select="DOC/t_doc/ID_DOC"/><xsl:text>	</xsl:text><xsl:call-template name="leftjustify">
    <xsl:with-param name="content"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></xsl:with-param>
    <xsl:with-param name="width">40</xsl:with-param>
</xsl:call-template><xsl:text>	</xsl:text><xsl:value-of select="DOC/t_doc/DOC_DUREE"/><xsl:text>	</xsl:text><xsl:value-of select="pdoc_support_liv"/><xsl:text>	</xsl:text><xsl:value-of select="pdoc_nb_sup"/><xsl:text>	</xsl:text><xsl:value-of select="pdoc_version"/><xsl:text>	</xsl:text><xsl:choose><xsl:when test="normalize-space(pdoc_prix_calc)!=''"><xsl:value-of select="pdoc_prix_calc"/> Euro</xsl:when><xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise></xsl:choose>
<xsl:text>
</xsl:text>	
</xsl:for-each>

<xsl:text>================================================================================================

</xsl:text>
<xsl:processing-instruction name="php">print kPanierTotalProduits;</xsl:processing-instruction> : <xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/TOTAL_HT" /> Euro</xsl:if> 
<xsl:processing-instruction name="php">print kPanierFrais;</xsl:processing-instruction> : <xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/FRAIS_HT" /> Euro</xsl:if> 
<xsl:processing-instruction name="php">print kPanierTotalHT;</xsl:processing-instruction> : <xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="$total_ht+$frais_ht" /> Euro</xsl:if> 
<xsl:processing-instruction name="php">print kTotalTVA;</xsl:processing-instruction> : <xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="format-number($total_tva, '0.00')" /> Euro</xsl:if> 
<xsl:processing-instruction name="php">print kPanierTotalTTC;</xsl:processing-instruction> : <xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="$total_ttc" /> Euro</xsl:if> 


--<xsl:value-of select="$boundary" />
Content-Type:text/html; charset="ISO-8859-1"
Content-Transfer-Encoding: 8bit
</xsl:if>
<xsl:text>
</xsl:text>
	<html><head/>
	<body>
    
    
		<style type="text/css">
		 body {
			font-family: Arial,sans-serif;
			color: #333333;
			margin: 0px;
			padding: 0px;
			background-color: #FFFFFF;
		 }
		.label_champs_form {
			font-family: Arial;
			font-size: 11px;
			font-weight:bold;
			color: #333366;
			text-align: right;
			padding-right:3px;
		}

		.val_champs_form, .val_champs_form input,  .val_champs_form select,  .val_champs_form textarea {
			font-family: Arial;
			font-size: 11px;
			color: #333366;
			text-align:left;
			padding-left:4px;
		}
		.type {
			font-family: Arial,Geneva,Helvetica,sans-serif;
			font-size:16px;
			color: #333366;
			font-weight:bold;
		}
		.small_text {
			font-family: Arial,Geneva,Helvetica,sans-serif;
			font-size: 10px;
			color: #333366;
			text-align:left;
			align:top;
			}
		#altern0 { background-color: #f5f7f7; }
		#altern1 { background-color: #FFFFFF; }

		.resultsBar {
			font-family: Arial;
			font-size: 10pt;
			font-style: normal;
			line-height: normal;
			color: #333366;
			text-decoration: none;
			font-weight: bold;
			background-color: #ffffff;
		}

		.tableResults {
			width: 95%;
			color: #333366;
			margin-top:2px;
		}

		.resultsHead {
			font-family: "Trebuchet MS" ;
			font-size: 12px;
			color: #333366;
			text-decoration: none;
			font-weight: bold;
			padding-left:3px;
			border-bottom: 2px solid #333366;
			text-align: left;
			
		}

		.resultsCorps {
			font-family: Arial;
			font-size: 11px;
			color: #333366;
			font-weight: normal;
			border-bottom: 1px solid #333366;
		}
		.resultsCorps td {	border-bottom: 1px solid #333366; padding-left:4px; }
		
		.info_entete {margin:10px;color:black;width:95%;border:1px solid #666666;background-color:#FFCC66;font-size:12px;padding:3px;font-family:"Trebuchet MS",sans-serif;}
		
		</style>
		<xsl:variable name="imgurl"><xsl:text>&lt;?php echo kCheminHttp ?&gt;/design/images/</xsl:text></xsl:variable>
		<div align='center' style='padding:10px;spacing:10px;'>		
        <table width="95%" cellspacing="5" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center">
			<tr><td><img src='{$imgurl}logo.gif'/></td>
				<td  colspan='3' class='type' align="center">
				<xsl:processing-instruction name="php">echo kCommandeRecap;</xsl:processing-instruction >
				</td>
			</tr>

			<tr height="35">
				<td colspan="4" style='font-family:Arial;font-size:14px;color:#506C93;font-weight:bold'>
					<xsl:processing-instruction name="php">print kCommandeEntetePret;</xsl:processing-instruction>
				</td>
			</tr>
						
            <tr>
                <td rowspan="3" class="label_champs_form" width="20%"><xsl:processing-instruction name="php">print kTypeCommande;</xsl:processing-instruction></td>
                <td rowspan="3" class="type" align="left">
                	<xsl:value-of select="t_panier/TYPE_COMMANDE"/>
                </td>
                <td colspan="2" class="val_champs_form">
                	<span class="label_champs_form" ><xsl:processing-instruction name="php">print kUsager;</xsl:processing-instruction> :
                    </span>
                	<xsl:value-of select="t_panier/t_usager/US_NOM"/> <xsl:value-of select="t_panier/t_usager/US_PRENOM"/>
                </td>
             </tr>
			<xsl:choose>
				<xsl:when test="t_panier/t_usager/US_SOCIETE!=''">
				<tr>
				   <td colspan="2" class="val_champs_form">
						<span class="label_champs_form" ><xsl:processing-instruction name="php">print kSociete;</xsl:processing-instruction> :
						</span>
                    <xsl:value-of select="t_panier/t_usager/US_SOCIETE"/>
					</td>
				</tr>
				</xsl:when>
				<xsl:otherwise><tr><td colspan="2"></td></tr></xsl:otherwise>
			</xsl:choose>
			<tr>
			   <td colspan="2" class="val_champs_form">
					<span class="label_champs_form" ><xsl:processing-instruction name="php">print kMail;</xsl:processing-instruction> :
					</span>
					<a href="mailto:{$mail}"><xsl:value-of select="$mail"/></a>
				</td>
			</tr>
            <tr height="1"><td colspan="4" style="padding:0px;spacing:0px;" bgcolor="#85a9ca"><img src="{$imgurl}pixel.gif" height="1"/></td></tr>
			<tr height="25">
				<td class="label_champs_form"><xsl:processing-instruction name="php">print kNumeroCommande;</xsl:processing-instruction></td>
				<td class="type">
					<xsl:value-of select="t_panier/ID_PANIER"/>
				</td>
				
				<td class="label_champs_form"><xsl:processing-instruction name="php">print kDateCommande;</xsl:processing-instruction></td>
				<td class="val_champs_form">
					<xsl:call-template name="format_date">
						<xsl:with-param name="chaine_date" select="t_panier/PAN_DATE_COM"/>
					</xsl:call-template>
				</td>
			</tr>
			<xsl:if test="t_panier/PAN_ID_TRANS!=''">
				<tr height="25">
					<td class="label_champs_form"><xsl:processing-instruction name="php">print kNumeroTransaction;</xsl:processing-instruction></td>
					<td class="type" colspan="3">
						<xsl:value-of select="t_panier/PAN_ID_TRANS" />
					</td>
				</tr>
			</xsl:if>
			<tr height="60">
                <td class="val_champs_form" colspan="2">
					<div class="label_champs_form">
					<xsl:processing-instruction name="php">print kAdresseLivraison;</xsl:processing-instruction>
					</div>
					<xsl:call-template name="break" >
					   <xsl:with-param name="text">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_LIVRAISON"/>
					   </xsl:with-param>
					 </xsl:call-template>
                </td>
                <td class="val_champs_form" colspan="2">
					<div class="label_champs_form">
					<xsl:processing-instruction name="php">print kAdresseFacturation;</xsl:processing-instruction>
					</div>
					<xsl:call-template name="break" >
					   <xsl:with-param name="text">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_ADRESSE_FACTURATION"/>
					   </xsl:with-param>
					 </xsl:call-template>
                </td>
             </tr>

             <tr height="20">
                <td colspan="4" class="val_champs_form" valign="top">
                <div class="label_champs_form">
                <xsl:processing-instruction name="php">print kConditionExploitation;</xsl:processing-instruction>
                </div>
					<xsl:call-template name="break" >
					   <xsl:with-param name="text">
							<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_CONDITION_EXPLOITATION"/>
					   </xsl:with-param>
					 </xsl:call-template>
                </td>
             </tr>

			<tr height="20">
				<td class="val_champs_form"  valign="top" colspan="2">
					<div class="label_champs_form">
					<xsl:processing-instruction name="php">print kDateEmprunt;</xsl:processing-instruction></div>
					<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_DATE_EMPRUNT"/>
				</td>
				<td class="val_champs_form" valign="top" colspan="2">
					<div class="label_champs_form" >
					<xsl:processing-instruction name="php">print kDateRetour;</xsl:processing-instruction>
					</div>
					<xsl:value-of select="t_panier/PAN_XML/commande/PANXML_DATE_RETOUR"/>
				</td>
			</tr>

        </table>

        <!-- Détail de la commande -->
        <p>
        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
            <tr>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kNumero;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>                
                <td class="resultsHead"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kSupportLivraison;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kNbSemainesProjections;</xsl:processing-instruction></td>
				<td class="resultsHead"><xsl:processing-instruction name="php">print kVersion;</xsl:processing-instruction></td>
                <td class="resultsHead"><xsl:processing-instruction name="php">print kPrixHT;</xsl:processing-instruction></td>
            </tr>
            <xsl:for-each select="t_panier_doc">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="resultsCorps" id="{concat('altern',$j mod 2)}">
                    <td class="resultsCorps"><xsl:value-of select="DOC/t_doc/ID_DOC"/></td>
                    <td class="resultsCorps"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></td>
                    <td class="resultsCorps"><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></td>
                    <td class="resultsCorps"><xsl:value-of select="pdoc_support_liv"/></td>
                    <td class="resultsCorps"><xsl:value-of select="pdoc_nb_sup"/></td>
                    <td class="resultsCorps"><xsl:value-of select="pdoc_version"/></td>
					<td class="resultsCorps">
					  <xsl:choose>
						  <xsl:when test="normalize-space(pdoc_prix_calc)!=''"><xsl:value-of select="pdoc_prix_calc"/> €</xsl:when>
						  <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
					  </xsl:choose>
					</td>
                </tr>
            </xsl:for-each>
		</table>
        </p>

			 <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
			 <!--
			   <tr class="resultsCorps" id="altern0">
			     <td align="right" class="resultsCorps" style="font-size:13px">
			     	<xsl:processing-instruction name="php">print kPanierTotalProduits;</xsl:processing-instruction>
			     </td>
			     <td  align="right" class="resultsCorps" style="font-size:13px">
			       	<div id="total_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/TOTAL_HT" /> €</xsl:if> 
				</div>
			     </td>
			     <td  class="resultsCorps">   </td>
			   </tr>
			   <tr class="resultsCorps" id="altern1">
			   	<td align="right" class="resultsCorps">
			     	<xsl:processing-instruction name="php">print kPanierFrais;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsCorps">
			       <div id="frais_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="t_panier/FRAIS_HT" /> €</xsl:if> 
				</div>
			     </td>
			     <td  class="resultsCorps">   </td>
			   </tr>
			-->
			
			   <tr class="resultsHead" id="altern1">
			     <td align="right" class="resultsHead">
			     	<xsl:processing-instruction name="php">print kPanierTotalHT;</xsl:processing-instruction>
			     </td>
			     <td align="right" class="resultsHead">
			        <div id="total_frais_ht">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="$total_ht+$frais_ht" /> €</xsl:if> 
				</div>
			     </td>

			   </tr>
			   <tr class="resultsHead" id="altern1">
				 <td align="right" class="resultsHead">
					<xsl:processing-instruction name="php">print kTotalTVA;</xsl:processing-instruction>
				 </td>
				 <td align="right" class="resultsHead">
					<div id="total_tva">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="format-number($total_tva, '0.00')" /> €</xsl:if> 
				</div>
				 </td>

			   </tr>

			   <tr class="resultsHead" id="altern1">
				 <td align="right" class="resultsHead">
					<xsl:processing-instruction name="php">print kPanierTotalTTC;</xsl:processing-instruction>
				 </td>
				 <td align="right" class="resultsHead">
					<div id="total_ttc">
				<xsl:if test="t_panier/TOTAL_HT!='' and t_panier/TOTAL_HT!=0"><xsl:value-of select="$total_ttc" /> €</xsl:if> 
				</div>
				 </td>

			   </tr>
			 </table> 

			</div>
		</body>
	</html>

<xsl:if test="$boundary!=''" >
--<xsl:value-of select="$boundary" />--
</xsl:if>

    </xsl:template>



    <xsl:template name="break">
	 <xsl:param name="text" select="."/>
	 <xsl:choose>
	   <xsl:when test="contains($text, '&#xA;')">
	     <xsl:value-of select="substring-before($text, '&#xA;')"/>
	     <br/>
	     <xsl:call-template name="break">
	       <xsl:with-param name="text" select="substring-after($text,'&#xA;')"/>
	     </xsl:call-template>
	   </xsl:when>
	   <xsl:otherwise>
	           <xsl:value-of select="$text"/>
	   </xsl:otherwise>
	 </xsl:choose>
	</xsl:template>

    <!-- 
    FORMAT_DATE : Formate une durée aaaa-mm-jj en jj/mm/aaaa
    -->
    <xsl:template name="format_date">
        <xsl:param name="chaine_date"/>
		<!-- découpage de la chaine -->
		<xsl:variable name="a"><xsl:value-of select="substring-before($chaine_date,'-')"/></xsl:variable>
		<xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine_date,'-'),'-')"/></xsl:variable>
		<xsl:variable name="j"><xsl:value-of select="substring(substring-after(substring-after($chaine_date,'-'),'-'),1,2)"/></xsl:variable>
        <xsl:value-of select="normalize-space($j)"/><xsl:text>/</xsl:text><xsl:value-of select="normalize-space($m)"/><xsl:text>/</xsl:text><xsl:value-of select="normalize-space($a)"/>
    </xsl:template>

<xsl:template name="leftjustify">
  <xsl:param name="content" />
  <xsl:param name="width" />

  <xsl:choose>
      <xsl:when test="string-length($content) > $width">
          <xsl:value-of select="substring($content,1,$width)" />
      </xsl:when>

      <xsl:otherwise>
          <xsl:value-of select="$content" />
          <xsl:call-template name="spaces">
              <xsl:with-param name="length"><xsl:value-of select="$width - string-length($content)" /></xsl:with-param>
          </xsl:call-template>
      </xsl:otherwise>

  </xsl:choose>

</xsl:template>

<xsl:template name="spaces">
  <xsl:param name="length" />
  <!-- the value of this next variable is 255 spaces.. -->
  <xsl:variable name="longstringofspaces"><xsl:text>                                                                                                                                                                                                                                                               </xsl:text></xsl:variable>
  <xsl:value-of select="substring($longstringofspaces,1,$length)" />
</xsl:template>



</xsl:stylesheet>