<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 
<xsl:template match="text() | @*" ></xsl:template>
<xsl:template match='t_mat/MAT_INFO/xml'>
	<data>
		<DOC>
			<DOC_ID_MEDIA>P</DOC_ID_MEDIA>
			<ID_LANG>FR</ID_LANG>
            
            <xsl:choose>
                <xsl:when test="IPTC/title">
                    <DOC_TITRE><xsl:value-of select="IPTC/title" /></DOC_TITRE>
                </xsl:when>
                <xsl:when test="//IFD0/ImageDescription">
                    <DOC_TITRE><xsl:value-of select="//IFD0/ImageDescription" /></DOC_TITRE>
                </xsl:when>
            </xsl:choose>
            
            <xsl:choose>
                <xsl:when test="EXIF/date!=''">
                    <DOC_DATE_PROD><xsl:call-template name="format_date"><xsl:with-param name="chaine_date" select="EXIF/date" /></xsl:call-template></DOC_DATE_PROD>
                </xsl:when>
                <xsl:when test="//IFD0/DateTime!=''">
                    <DOC_DATE_PROD><xsl:call-template name="format_date"><xsl:with-param name="chaine_date" select="//IFD0/DateTime" /></xsl:call-template></DOC_DATE_PROD>
                </xsl:when>
                <xsl:otherwise>
                    <DOC_DATE_PROD><xsl:call-template name="format_date"><xsl:with-param name="chaine_date" select="IPTC/created_date" /></xsl:call-template></DOC_DATE_PROD>
                </xsl:otherwise>
            </xsl:choose>
            <!--xsl:if test="//IFD0/Make or //IFD0/Model">
                <DOC_SOUSTITRE><xsl:value-of select="//IFD0/Make"/>&#160;<xsl:value-of select="//IFD0/Model"/></DOC_SOUSTITRE>
            </xsl:if-->
			
            <xsl:if test="IPTC/keywords">
                <DOC_RES><xsl:value-of select="IPTC/keywords" /></DOC_RES>
            </xsl:if>
			
			<xsl:if test="IPTC/caption">
				<DOC_NOTES><xsl:value-of select="IPTC/caption" /></DOC_NOTES>
			</xsl:if>
			
            <xsl:choose>
                <xsl:when test="IPTC/byline">
                    <L_GEN_PHO_PP><xsl:value-of select="IPTC/byline" /></L_GEN_PHO_PP>
                </xsl:when>
                <xsl:when test="//IFD0/Artist">
                    <L_GEN_PHO_PP><xsl:value-of select="//IFD0/Artist" /></L_GEN_PHO_PP>
                </xsl:when>
                <xsl:when test="artist">
                    <L_GEN_PHO_PP><xsl:value-of select="artist" /></L_GEN_PHO_PP>
                </xsl:when>
            </xsl:choose>
            <!--V_THM>
				<xsl:value-of select="IPTC/category" />
			</V_THM-->
			
			<xsl:if test="IPTC/country">
				<L_LG__LG><LEX_TERME><xsl:value-of select="IPTC/country" /></LEX_TERME></L_LG__LG>
			</xsl:if>
			<xsl:if test="IPTC/province_state">
				<L_LG__LG><LEX_TERME><xsl:value-of select="IPTC/province_state" /></LEX_TERME></L_LG__LG>
			</xsl:if>
			<xsl:if test="IPTC/city">
				<L_LG__LG><LEX_TERME><xsl:value-of select="IPTC/city" /></LEX_TERME></L_LG__LG>
			</xsl:if>
			<DOC_COPYRIGHT><xsl:value-of select="IPTC/copyright_string" /></DOC_COPYRIGHT>
		</DOC>
	</data>
</xsl:template>
<!-- 
FORMAT_DUREE : Formate une dur√©e HH:MM:SS
-->
    <xsl:template name="format_duree">
        <xsl:param name="chaine_duree"/>
            <!-- d√©coupage de la chaine -->
            <xsl:variable name="duree_hh"><xsl:text>00</xsl:text></xsl:variable>
            <xsl:variable name="duree_mm"><xsl:value-of select="format-number(normalize-space(substring-before($chaine_duree,'min')),'00')"/></xsl:variable>
            <xsl:variable name="duree_ss"><xsl:value-of select="format-number(normalize-space(substring-before(substring-after($chaine_duree,'min'),'sec')),'00')"/></xsl:variable>

            <!-- Restitution -->
        	<xsl:value-of select="$duree_hh"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_mm"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_ss"/>
            
    </xsl:template>

<!-- 
FORMAT_DATE : Formate une date jj.mm.aaaa en aaaa-mm-jj
-->
   <xsl:template name="format_date">
	    <xsl:param name="chaine_date"/>
		<xsl:variable name="delim">
			<xsl:choose>
				<xsl:when test="(string-length($chaine_date) - string-length(translate($chaine_date, ':', '')))&gt;=2"><xsl:text>:</xsl:text></xsl:when>
				<xsl:when test="(string-length($chaine_date) - string-length(translate($chaine_date, '/', '')))&gt;=2"><xsl:text>/</xsl:text></xsl:when>
				<xsl:when test="(string-length($chaine_date) - string-length(translate($chaine_date, '.', '')))&gt;=2"><xsl:text>.</xsl:text></xsl:when>
                <xsl:when test="(string-length($chaine_date) - string-length(translate($chaine_date, '-', '')))&gt;=2"><xsl:text>-</xsl:text></xsl:when>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="date_only">
			<xsl:choose>
				<xsl:when test="contains($chaine_date,' ')">
					<xsl:value-of select="substring-before($chaine_date,' ')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$chaine_date" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
        <xsl:choose>
            <xsl:when test="$delim!=''">
                <!-- decoupage de la chaine -->
                <xsl:variable name="a">
                    <xsl:choose>
                        <xsl:when test="string-length(substring-before($date_only,$delim)) = 4">
                            <xsl:value-of select="substring-before($date_only,$delim)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="substring-after(substring-after($date_only,$delim),$delim)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                <xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($date_only,$delim),$delim)"/></xsl:variable>
                
                <xsl:variable name="j">
                    <xsl:choose>
                        <xsl:when test="string-length(substring-before($date_only,$delim)) = 4">
                            <xsl:value-of select="substring-after(substring-after($date_only,$delim),$delim)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="substring-before($date_only,$delim)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>		
                <xsl:value-of select="normalize-space($a)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($m)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($j)"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="$date_only" /></xsl:otherwise>
        </xsl:choose>
    </xsl:template>




</xsl:stylesheet>
