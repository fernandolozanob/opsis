<?xml version="1.0" encoding="utf-8"?>
<!--
mail JOB 
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="text" 
        encoding="utf-8" 
        indent="yes"
/> 


<!-- 
EXPORT_OPSIS 
-->
    <xsl:template match="/EXPORT_OPSIS">
            <xsl:text>Une nouvelle vidéo est en attente de validation :
</xsl:text>
            <xsl:text>Titre :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_TITRE"/><xsl:text>
</xsl:text>
            <xsl:text>Référence :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_COTE"/><xsl:text>
</xsl:text>
            <xsl:text>Date de diffusion :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_DATE_PROD"/><xsl:text>
</xsl:text>
            <xsl:text>Durée :	</xsl:text><xsl:value-of select="JOB_IN/t_mat/t_doc_mat/DOC_MAT/t_doc/DOC_DUREE"/><xsl:text>

</xsl:text>
            <xsl:text>Cliquer sur le lien ci-dessous pour la valider :
</xsl:text><xsl:value-of select="concat('&lt;?php echo kCheminHttp ?&gt;/index.php?urlaction=jobValid&amp;id_job=',t_job/ID_JOB)"/><xsl:text>

</xsl:text>

            <xsl:text>Message généré automatiquement par Opsis Media.
</xsl:text>
    </xsl:template>

</xsl:stylesheet>
