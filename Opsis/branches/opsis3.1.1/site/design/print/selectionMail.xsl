<?xml version="1.0" encoding="utf-8"?>
<!-- commandeImp2 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="UTF-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="boundary" />
<xsl:param name="senderName" />
<xsl:param name="corpsTexte" />

<!-- PANIER_COMMANDE : Affichage du panier -->
    <xsl:template match='EXPORT_OPSIS'>
<xsl:variable name="mail"><xsl:value-of select="t_panier/t_usager/US_SOC_MAIL"/></xsl:variable>
<xsl:variable name="total_ht"><xsl:value-of select="t_panier/TOTAL_HT" /></xsl:variable>
<xsl:variable name="frais_ht"><xsl:value-of select="t_panier/FRAIS_HT" /></xsl:variable>
<xsl:variable name="pan_tva"><xsl:value-of select="t_panier/PAN_TVA" /></xsl:variable>
<xsl:variable name="total_tva"><xsl:value-of select="round(($total_ht + $frais_ht)* $pan_tva) * 0.01" /></xsl:variable>
<xsl:variable name="total_ttc"><xsl:value-of select="round(($total_ht + $frais_ht)* (100 + $pan_tva)) * 0.01" /></xsl:variable>

<xsl:if test="$boundary!=''" >
This is a multi-part message in MIME format.
--<xsl:value-of select="$boundary" />
Content-Type: text/plain; charset="ISO-8859-1"\r\nContent-Transfer-Encoding: 8bit

--<xsl:value-of select="$boundary" />
Content-Type:text/html; charset="ISO-8859-1"\r\nContent-Transfer-Encoding: 8bit
</xsl:if>

	<html><head>

		<style type="text/css">
		 body {
			font-family: Arial,sans-serif;
			color: #333333;
			margin: 0px;
			padding: 0px;
			background-color: #FFFFFF;
		 }
		 
		.label_champs_form {
			font-family: Arial;
			font-size: 11px;
			font-weight:bold;
			color: #333366;
			text-align: right;
			padding-right:3px;
		}
		.val_champs_form, .val_champs_form input,  .val_champs_form select,  .val_champs_form textarea {
			font-family: Arial;
			font-size: 11px;
			color: #333366;
			text-align:left;
			padding-left:4px;
		}
		.type {
			font-family: Arial,Geneva,Helvetica,sans-serif;
			font-size:16px;
			color: #333366;
			font-weight:bold;
		}
		.small_text {
			font-family: Arial,Geneva,Helvetica,sans-serif;
			font-size: 10px;
			color: #333366;
			text-align:left;
			align:top;
			}
		#altern0 { background-color: #f5f7f7; }
		#altern1 { background-color: #FFFFFF; }
		.resultsBar {
			font-family: Arial;
			font-size: 10pt;
			font-style: normal;
			line-height: normal;
			color: #333366;
			text-decoration: none;
			font-weight: bold;
			background-color: #ffffff;
		}
		.tableResults {
			width: 95%;
			color: #333366;
			margin-top:2px;
		}
		.resultsHead {
			font-family: "Trebuchet MS" ;
			font-size: 12px;
			color: #333366;
			text-decoration: none;
			font-weight: bold;
			padding-left:3px;
			border-bottom: 2px solid #333366;
			text-align: left;
			
		}

		.resultsCorps {
			font-family: Arial;
			font-size: 11px;
			color: #333366;
			font-weight: normal;
			border-bottom: 1px solid #333366;
		}
		.resultsCorps td {	border-bottom: 1px solid #333366; padding-left:4px; }
		
		.info_entete {margin:10px;color:black;width:95%;border:1px solid #666666;background-color:#FFCC66;font-size:12px;padding:3px;font-family:"Trebuchet MS",sans-serif;}
		
		p {
			font-family: Arial,Geneva,Helvetica,sans-serif;
			font-size:14px;
		}
		
		.corpsTexte {
			margin-top : 10px;
			margin-bottom : 10px;
		}
		
		</style>
		</head>
	<body>
		<xsl:variable name="imgurl"><xsl:text>&lt;?php echo kCheminHttp ?&gt;/design/images/</xsl:text></xsl:variable>
		<div align='center' style='padding:10px;spacing:10px;'>
        <table width="95%" cellspacing="5" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center">
			<tr>
				<td><img src='{$imgurl}Opsis.gif' height="40" /></td>
			</tr>
			<tr><td><p class="corpsTexte"><xsl:value-of select="$corpsTexte" /></p></td></tr>
            <tr height="1" style="line-height:1px"><td style="padding:0px;spacing:0px;line-height:1px;height:1px;" bgcolor="#85a9ca"></td></tr>
        </table>

        <!-- Détail de la commande -->
        <p class="detailCommande">
        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
            <tr>
            	<td class="resultsHead" width="50"><xsl:processing-instruction name="php">print kImage;</xsl:processing-instruction></td>
                <td class="resultsHead" width="100"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
                <td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kDate;</xsl:processing-instruction></td>
               	<xsl:if test="t_panier_doc/DOC/t_doc/DOC_LEX/XML/LEX[@TYPE='FI']/TERME != ''"><td class="resultsHead" width="80"><xsl:processing-instruction name="php">print kFiliale;</xsl:processing-instruction></td></xsl:if>
                <td class="resultsHead" width="60"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>
                <xsl:if test="t_panier_doc/DOC/t_doc/DOC_RES != ''">
                	<td class="resultsHead" width="200"><xsl:processing-instruction name="php">print kResume;</xsl:processing-instruction></td>
            	</xsl:if>
            </tr>
            <xsl:for-each select="t_panier_doc">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
				<xsl:variable name="cheminHttp"><xsl:text>&lt;?php echo kCheminHttp ?&gt;</xsl:text></xsl:variable>
                <tr class="resultsCorps" id="{concat('altern',$j mod 2)}">
                	<xsl:variable name="idmat"><xsl:value-of select="VISIOFLASH"/></xsl:variable>
                	<td class="resultsCorps">
                		<xsl:variable name="vignurl"><xsl:value-of select="URLHTTP"/>/makeVignette.php?image=<xsl:value-of select="DOC/t_doc/VIGNETTE"/></xsl:variable>
                		<xsl:if test="DOC/t_doc/VIGNETTE!=''">
                			<a href="{$cheminHttp}/index.php?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}" target="_BLANK">
                				<img border="0" width="120" src="{$vignurl}" />
                			</a>
						</xsl:if>
                		<xsl:if test="DOC/t_doc/VIGNETTE = '' and DOC/t_doc/DOC_ID_TYPE_DOC = '6'">
                			<a href="{$cheminHttp}/index.php?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}" target="_BLANK">
								<img border="0" src="&lt;?= imgUrl ?&gt;tournage.jpg" width="120" />
                			</a>
                		</xsl:if>
					&#160;</td>
                    <td class="resultsCorps" width="200"><a href="{$cheminHttp}/index.php?urlaction=doc&amp;id_doc={DOC/t_doc/ID_DOC}"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/></a></td>
                    <td class="resultsCorps" width="80">
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="DOC/t_doc/DOC_DATE_PROD"/>
						</xsl:call-template>
					&#160;</td>
					<!-- Comme pour l'en-tete, on n'affiche la colonne que si un des paniers a un document lié à des termes filiales -->
					<xsl:if test="/EXPORT_OPSIS/t_panier_doc/DOC/t_doc/DOC_LEX/XML/LEX[@TYPE='FI']/TERME != ''">
	                    <td class="resultsCorps" width="80">
	                    	<xsl:if test="DOC/t_doc/DOC_LEX/XML/LEX[@TYPE='FI']/TERME != ''">
		                    	<xsl:for-each select="DOC/t_doc/DOC_LEX/XML/LEX[@TYPE='FI']/TERME">
		                    		<xsl:value-of select="normalize-space(.)"/>
		                    		<xsl:if test="position()!=last()">, </xsl:if>
		                    	</xsl:for-each>
		                    </xsl:if>
	                    &#160;</td>
	                </xsl:if>
                    <td class="resultsCorps" width="60">
						<xsl:variable name="dureeDoc"><xsl:choose>
							<xsl:when test="pdoc_extrait='1'"><xsl:value-of select="ext_duree"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="DOC/t_doc/DOC_DUREE"/></xsl:otherwise>
						</xsl:choose></xsl:variable>
						<xsl:if test="$dureeDoc != '00:00:00'"><xsl:value-of select="$dureeDoc" /></xsl:if>
					&#160;</td>
					<xsl:if test="/EXPORT_OPSIS/t_panier_doc/DOC/t_doc/DOC_RES != ''">
						<td class="resultsCorps" >
							<xsl:if test="DOC/t_doc/DOC_RES != ''">
								<xsl:value-of select="DOC/t_doc/DOC_RES"/>
		                	</xsl:if>
	                	&#160;</td>
	                </xsl:if>
                </tr>
            </xsl:for-each>
		</table>
        </p>
        <p><xsl:processing-instruction name="php">echo kClickToConsult;</xsl:processing-instruction ></p>
			</div>
		</body>
	</html>

<xsl:if test="$boundary!=''" >
--<xsl:value-of select="$boundary" />--
</xsl:if>
   </xsl:template>

    
<xsl:template name="leftjustify">
  <xsl:param name="content" />
  <xsl:param name="width" />

  <xsl:choose>
      <xsl:when test="string-length($content) > $width">
          <xsl:value-of select="substring($content,1,$width)" />
      </xsl:when>

      <xsl:otherwise>
          <xsl:value-of select="$content" />
          <xsl:call-template name="spaces">
              <xsl:with-param name="length"><xsl:value-of select="$width - string-length($content)" /></xsl:with-param>
          </xsl:call-template>
      </xsl:otherwise>

  </xsl:choose>

</xsl:template>

<xsl:template name="spaces">
  <xsl:param name="length" />
  <!-- the value of this next variable is 255 spaces.. -->
  <xsl:variable name="longstringofspaces"><xsl:text>                                                                                                                                                                                                                                                               </xsl:text></xsl:variable>
  <xsl:value-of select="substring($longstringofspaces,1,$length)" />
</xsl:template>

</xsl:stylesheet>