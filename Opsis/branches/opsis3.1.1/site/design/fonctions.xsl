<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

    <!-- 
    replace : 
    -->
	<xsl:template name="replace">
		<xsl:param name="stream"/>
		<xsl:param name="old"/>
		<xsl:param name="new"/>
		<xsl:choose>
			<xsl:when test="contains($stream,$old)">
		<xsl:value-of select="substring-before($stream,$old)"/><xsl:value-of select="$new"/><xsl:call-template name="replace"><xsl:with-param name="stream" select="substring-after($stream,$old)"/><xsl:with-param name="old" select="$old"/><xsl:with-param name="new" select="$new"/></xsl:call-template>		
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
		</xsl:choose>
    </xsl:template>


    <!-- 
    internal-quote-replace : échappe les quotes
    -->
	<xsl:template name="internal-quote-replace">
		<xsl:param name="stream"/>
		<xsl:variable name="simple-quote">'</xsl:variable>
		<xsl:choose>
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
		name="internal-quote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>		
			</xsl:when>
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
		</xsl:choose>
    </xsl:template>

    <!-- 
    nl2br : remplace les sauts de ligne par un <br/>
    -->
	<xsl:template name="nl2br">
		<xsl:param name="stream"/>
		<xsl:variable name="simple-quote"><xsl:text>
</xsl:text></xsl:variable>
		<xsl:choose>
			<xsl:when test="contains($stream,$simple-quote)">
			<xsl:value-of
				select="substring-before($stream,$simple-quote)"/><br/>
			<xsl:call-template name="nl2br"><xsl:with-param name="stream"
				select="substring-after($stream,$simple-quote)"/></xsl:call-template>		
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$stream"/>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>

    <!--
    FORMAT_DATE : Formate une durée aaaa-mm-jj hh:mm:ss en jj/mm/aaaa hh:mm:ss
    -->
	<xsl:template name="format_date">
        <xsl:param name="chaine_date"/>
        <!-- découpage de la chaine -->
		<xsl:choose>
			<xsl:when test="contains($chaine_date, '-')">
				<xsl:variable name="a"><xsl:value-of select="substring-before($chaine_date,'-')"/></xsl:variable>
				<xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine_date,'-'),'-')"/></xsl:variable>
				<xsl:variable name="j"><xsl:value-of select="substring(substring-after(substring-after($chaine_date,'-'),'-'),1,2)"/></xsl:variable>
				<!--xsl:variable name="hms"><xsl:value-of select="substring-after($chaine_date,' ')"/></xsl:variable-->
				<xsl:if test="$a!='0000'">
					<xsl:if test="normalize-space($j)!='00' and normalize-space($j)!=''" ><xsl:value-of select="normalize-space($j)"/>
						<xsl:text>/</xsl:text></xsl:if>
					<xsl:if test="normalize-space($m)!='00' and normalize-space($m)!=''" ><xsl:value-of select="normalize-space($m)"/>
						<xsl:text>/</xsl:text></xsl:if>
					<xsl:value-of select="normalize-space($a)"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="normalize-space($chaine_date)"/>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>

			<!-- découpage de la chaine -->

    <!--xsl:template name="format_date">
        <xsl:param name="chaine_date"/>
		<xsl:variable name="a"><xsl:value-of select="substring-before($chaine_date,'-')"/></xsl:variable>
		<xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine_date,'-'),'-')"/></xsl:variable>
		<xsl:variable name="j"><xsl:value-of select="substring(substring-after(substring-after($chaine_date,'-'),'-'),1,2)"/></xsl:variable>
        <xsl:variable name="hms"><xsl:value-of select="substring-after($chaine_date,concat($a,'-',$m,'-',$j,' '))"/></xsl:variable>

        <xsl:if test="normalize-space($j)!='00' and normalize-space($j)!=''" ><xsl:value-of select="normalize-space($j)"/>
        	<xsl:text>/</xsl:text></xsl:if>
        <xsl:if test="normalize-space($m)!='00' and normalize-space($m)!=''" ><xsl:value-of select="normalize-space($m)"/>
        	<xsl:text>/</xsl:text></xsl:if>
        <xsl:value-of select="normalize-space($a)"/><xsl:text> </xsl:text><xsl:value-of select="$hms"/>
    </xsl:template-->

	<!--  comme nl2br mais autre méthode -->
    <xsl:template name="break">
	 <xsl:param name="text" select="."/>
	 <xsl:choose>
	   <xsl:when test="contains($text, '&#xA;')">
	     <xsl:value-of select="substring-before($text, '&#xA;')"/>
	     <br/>
	     <xsl:call-template name="break">
	       <xsl:with-param name="text" select="substring-after($text,'&#xA;')"/>
	     </xsl:call-template>
	   </xsl:when>
	   <xsl:otherwise>
	           <xsl:value-of select="$text"/>
	   </xsl:otherwise>
	 </xsl:choose>
	</xsl:template>


<xsl:template name="remove-duplicates">
   <xsl:param name="string" />
   <xsl:param name="newstring" />
   <xsl:param name="count" />
   
   <xsl:choose>
     <xsl:when test="$string = ''">
       <xsl:value-of select="$newstring" />
     </xsl:when>
     <xsl:otherwise>
       <xsl:if test="contains($newstring,substring-before($string, '|'))">
         <xsl:call-template name="remove-duplicates">
           <xsl:with-param name="string" select="substring-after($string, '|')" />
           <xsl:with-param name="newstring" select="$newstring" />
           <xsl:with-param name="count" select="1+$count" />
         </xsl:call-template>
       </xsl:if>
       <xsl:if test="not(contains($newstring, substring-before($string, '|')))">
         <xsl:variable name="temp">
           <xsl:if test="$newstring = ''">
             <xsl:value-of select="substring-before($string, '|')" />
             <!-- 
             <xsl:text>-</xsl:text>
             <xsl:value-of select="$count" />
           	-->
           </xsl:if>
           <xsl:if test="not($newstring = '')">
         <xsl:value-of select="concat($newstring, ',', substring-before($string, '|'))" />
             <!-- 
             <xsl:text>-</xsl:text>
             <xsl:value-of select="$count" />
           	-->
           </xsl:if>
         </xsl:variable>
         <xsl:call-template name="remove-duplicates">
           <xsl:with-param name="string" select="substring-after($string, '|')" />
           <xsl:with-param name="newstring" select="$temp" />
           <xsl:with-param name="count" select="1" />
         </xsl:call-template>
       </xsl:if>
     </xsl:otherwise>
   </xsl:choose>
   
</xsl:template>



	<xsl:template name="remove_html_tags">
		<xsl:param name="node" />
		<xsl:variable name="before" select="substring-before($node, '&lt;')"/>
		<xsl:choose>
			<xsl:when test="string-length($before) != 0 or contains($node, '&lt;')">
				<xsl:value-of select="$before"/>
				<xsl:call-template name="remove_html_tags">
					<xsl:with-param name="node" select="substring-after($node, '&gt;')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$node"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="tokenize">
        <xsl:param name="pText"/>
        <xsl:param name="delimitor"/>
		<xsl:variable name="delim">
			<xsl:choose>
				<xsl:when test="$delimitor and $delimitor !=''"><xsl:value-of select="$delimitor"/></xsl:when>
				<xsl:otherwise><xsl:text>,</xsl:text></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
        <xsl:if test="string-length($pText)">
            <tag>
				<xsl:choose>
					<xsl:when test="contains($pText,$delim)">
						<xsl:value-of select="substring-before($pText, $delim)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$pText"/>
					</xsl:otherwise>
				</xsl:choose>
            </tag>
            <xsl:call-template name="tokenize">
                <xsl:with-param name="pText" select="substring-after($pText, $delim)"/>
                <xsl:with-param name="delimitor" select="$delim"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

   <xsl:template name="lowercase">
		<xsl:param name="stream"/>
		<xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyz'"/>
		<xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:value-of select="translate($stream,$upperCase,$smallCase)"/>
	</xsl:template>

	<xsl:template name="uppercase">
		<xsl:param name="stream"/>
		<xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyz'"/>
		<xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:value-of select="translate($stream,$smallCase,$upperCase)"/>
	</xsl:template>

	<xsl:template name="split_valeur">
		<xsl:param name="stream"/>
		<xsl:param name="tag"/>
		<xsl:param name="sep"/>
		
		<xsl:variable name="sep1">
			<xsl:choose>
				<xsl:when test="$sep"><xsl:value-of select="$sep"/></xsl:when>
				<xsl:otherwise>,</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="contains($stream,$sep1)">
				<xsl:element name="{$tag}"><VALEUR><xsl:value-of select="normalize-space(substring-before($stream,$sep1))"/></VALEUR></xsl:element>
				<xsl:call-template name="split_valeur">
					<xsl:with-param name="stream" select="substring-after($stream,$sep1)"/>
					<xsl:with-param name="tag" select="$tag"/>
					<xsl:with-param name="sep" select="$sep1"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="normalize-space($stream)!=''">
					<xsl:element name="{$tag}">
						<VALEUR><xsl:value-of select="normalize-space($stream)"/></VALEUR>
					</xsl:element>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


    <!--
     CONV_DATE : Formate une date AAAA-MM-JJ
     -->
    <xsl:template name="conv_date">
        <xsl:param name="chaine"/>
        <xsl:choose>
            <xsl:when test="contains($chaine,'-')">
                <xsl:value-of select="$chaine"/>
            </xsl:when>
            <xsl:when test="contains($chaine,'/')">
                <xsl:variable name="j"><xsl:value-of select="substring-before($chaine,'/')"/></xsl:variable>
                <xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine,'/'),'/')"/></xsl:variable>
                <xsl:variable name="a"><xsl:value-of select="substring-after(substring-after($chaine,'/'),'/')"/></xsl:variable>
                <xsl:variable name="a4">
                    <xsl:choose>
                        <xsl:when test="$a &lt; 30"><xsl:value-of select="2000 + $a"/></xsl:when>
                        <xsl:when test="$a &lt; 100"><xsl:value-of select="1900 + $a"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$a"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                <xsl:value-of select="concat(format-number($a4,'0000'),'-',format-number($m,'00'),'-',format-number($j,'00'))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$chaine"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--
     CONV_DUREE : Formate une durée HH:MM:SS
     -->
    <xsl:template name="conv_duree">
        <xsl:param name="chaine"/>
        <xsl:choose>
            <xsl:when test="contains($chaine,':')">
                <xsl:value-of select="$chaine"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="duree_hh">
                    <xsl:choose>
                        <xsl:when test="contains($chaine,'h')">
                            <xsl:value-of select="normalize-space(substring-before($chaine,'h'))"/>
                        </xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="duree_mm"> <!-- séparateur min ou mn -->
                    <xsl:choose>
                        <xsl:when test="contains($chaine,'h') and contains($chaine,'m')">
                            <xsl:value-of select="normalize-space(substring-before(substring-after($chaine,'h'),'m'))"/>
                        </xsl:when>
                        <xsl:when test="contains($chaine,'m')">
                            <xsl:value-of select="normalize-space(substring-before($chaine,'m'))"/>
                        </xsl:when>
                        <xsl:when test="contains($chaine,'h')">
                            <xsl:value-of select="concat('0',normalize-space(substring-after($chaine,'h')))"/>
                        </xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="duree_ss">
                    <xsl:choose>
                        <xsl:when test="contains($chaine,'n') and contains($chaine,'s')">
                            <xsl:value-of select="normalize-space(substring-before(substring-after($chaine,'n'),'s'))"/>
                        </xsl:when>
                        <xsl:when test="contains($chaine,'n')">
                            <xsl:value-of select="concat('0',normalize-space(substring-after($chaine,'n')))"/>
                        </xsl:when>
                        <xsl:otherwise>0</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:value-of select="concat(format-number($duree_hh,'00'),':',format-number($duree_mm,'00'),':',format-number($duree_ss,'00'))"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
	
	<xsl:template name="xml2string">
		<xsl:param name="omit_root"/>
		<xsl:param name="current_node"/>
		<xsl:if test="$omit_root=''">
			<xsl:text>&lt;</xsl:text><xsl:value-of select="name($current_node)"/><xsl:text>&gt;</xsl:text>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$current_node/*">
			<xsl:for-each select="$current_node/*">
				<xsl:call-template name="xml2string">
					<xsl:with-param name="current_node" select="."/>
				</xsl:call-template>
			</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>&lt;![CDATA[</xsl:text><xsl:value-of select="text()"/><xsl:text>]]&gt;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$omit_root=''">
			<xsl:text>&lt;/</xsl:text><xsl:value-of select="name($current_node)"/><xsl:text>&gt;</xsl:text>
		</xsl:if>
	</xsl:template>

    
</xsl:stylesheet>
