<?php

// fonction de g�n�ration al�atoire du mot de passe du compte FTP de livraison
function generePass($chrs=null)
{
	if($chrs==null)
		$chrs=8;

	$list='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

	$newstring='';

	while(strlen($newstring)<$chrs)
		$newstring.=$list[rand(0, strlen($list)-1)];

	return $newstring;
}


// on initialise ces param�tres, qui passeront par le formulaire de commande pour arriver dans les param�tres du job associ� au panier. 
if($myPanier->t_panier['PAN_ID_ETAT']<gPanierFini ){
	$myPanier->t_panier['PANXML_FTP_LOGIN']='tmp_'.kDatabaseName.'_'.$user->UserID.'_'.time();
	$myPanier->t_panier['PANXML_FTP_PW']=generePass();
	$myPanier->t_panier['PANXML_FTP_HOST']=kUrlFtp;
}


?>