<?
global $db;

// $t_media=$db->getArray("SELECT ID_MEDIA FROM t_media order by 1");
$myUsr=User::getInstance();
$myPage=Page::getInstance();
?>
<div id="menuGauche" class="menuAdmin  <?=($_SESSION['USER']['layout_opts']['layout']<1 ||  $_SESSION['USER']['layout_opts']['layout']>2)?'collapsed':'';?>" >
	<div class="title_bar" style="white-space:nowrap;">
		<span class="title_label"><?= kAdministration ?></span>
		<span id="arrow_left" class="toggle_arrow" onclick="toggleLeftMenu();">&nbsp;</span>
	</div>
<?if($myUsr->Type == kLoggedAdmin || $myUsr->Type == kLoggedDoc) {?>
	<div id="admin_wrapper" class="menuGauche_wrapper">
		<div class="menuAdminTitre" onclick="displayMenuAdmin(this);"><?=kMedias?></div>
		<ul class="menuAdminListe">
			<li><a href="<?=$myPage->getName()?>?urlaction=docSaisie"><?=kCreerNotice?></a></li>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='matListe')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=matListe"><?= kGestionMateriels ?></a></li>
			<?if($myUsr->Type == kLoggedAdmin){?>
				<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='imageurListe' || $_GET['urlaction']=='imageurSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=imageurListe"><?= kGestionStoryboards ?></a></li>
				<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='tapeListe' || $_GET['urlaction']=='tapeSaisie'|| $_GET['urlaction']=='backupLibView'|| $_GET['urlaction']=='tapeSetListe'|| $_GET['urlaction']=='tapeSetSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=tapeListe"><?= kGestionSauvegardes ?></a></li>
			<?}?>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='importView')?'selected':'';?>"><a href="index.php?urlaction=importView"><?= kImportParChargement ?></a></li>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='importView')?'selected':'';?>"><a href="index.php?urlaction=importView&import_type_form=folder"><?= kImportParDossier ?></a></li>
			<?if($myUsr->Type == kLoggedAdmin){?>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='importXML')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=importXML"><?= kImportXML ?></a></li>
			<?}?>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='importListe')?'selected':'';?>"><a href="index.php?urlaction=importListe"><?= kTousImports ?></a></li>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='jobListe')?'selected':'';?>"><a href="javascript:popupWindow('simple.php?urlaction=jobListe','Upload_mat','height=500,width=900,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main')"><?= kSuiviTraitements ?></a></li>
		</ul>
		<div class="menuAdminTitre" onclick="displayMenuAdmin(this);"><?=kLexiques?></div>
		<ul class="menuAdminListe">
			<li class="<?=(!empty($_GET['urlaction']) &&  ($_GET['urlaction']=='lexListe' || $_GET['urlaction']=='lexSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=lexListe"><?= kThesaurus ?></a></li>
			<?if (gGestPers==1) echo "\n<li class=\"".((!empty($_GET['urlaction']) && ($_GET['urlaction']=='personneListe' || $_GET['urlaction']=='personneSaisie'))?'selected':'')."\"><a href=\"".$myPage->getName()."?urlaction=personneListe\">".kPersonnes."</a></li>\n";?>
			<?if (gGestCat==1) echo "\n<li class=\"".((!empty($_GET['urlaction']) && $_GET['urlaction']=='catListe')?'selected':'')."\"><a href=\"".$myPage->getName()."?urlaction=catListe\">".kCategoriesEtThemes."</a></li>\n";?>
			<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='valListe' || $_GET['urlaction']=='valSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=valListe"><?= kListeDeValeur ?></a></li>
        </ul>
		<? if($myUsr->Type == kLoggedAdmin) { ?>
		<div class="menuAdminTitre" onclick="displayMenuAdmin(this);"><?=kAdminUtilisateursDroits?></div>
		<ul class="menuAdminListe">
			<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='usagerListe' || $_GET['urlaction']=='usagerSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=usagerListe"><?= kUtilisateurs ?></a></li>
			<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='groupeListe' || $_GET['urlaction']=='groupeSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=groupeListe"><?= kGroupesUtilisateurs ?></a></li>
			<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='fondsListe' || $_GET['urlaction']=='fondsSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=fondsListe"><?= kGestionFonds ?></a></li>
			<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='comListe' || $_GET['urlaction']=='commandeSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=comListe"><?=kCommandesUtilisateurs?></a></li>
		</ul>
		<? } ?>
		<div class="menuAdminTitre" onclick="displayMenuAdmin(this);"><?=kEditorial?></div>
		<ul class="menuAdminListe">
			<li class="<?=(!empty($_GET['urlaction']) && ($_GET['urlaction']=='htmlListe' || $_GET['urlaction']=='htmlSaisie'))?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=htmlListe"><?= kAdminPagesStatiques ?></a></li>
		</ul>
		<div class="menuAdminTitre" onclick="displayMenuAdmin(this);"><?=kStatistiques?></div>
		<ul class="menuAdminListe">
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='stats_app')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=stats_app"><?=kStatistiquesOpsis?></a></li>
			<?php if(defined("gAwstatsUrl")){?>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='stats_web')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=stats_web"><?=kStatistiquesWeb?></a></li>
			<?}?>
		</ul>
		<? if($myUsr->Type == kLoggedAdmin) { ?>
		<div class="menuAdminTitre" onclick="displayMenuAdmin(this);"><?=kAdminSysteme?></div>
		<ul class="menuAdminListe">
	        <li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='procListe')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=procListe"><?=kGestionProcessus?></a></li>
	        <li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='etapeListe')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=etapeListe"><?=kGestionEtapes?></a></li>

			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='refListe')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=refListe"><?= kGestionTables ?></a></li>
			<li class="<?=(!empty($_GET['urlaction']) && $_GET['urlaction']=='maintenanceFichiers')?'selected':'';?>"><a href="<?=$myPage->getName()?>?urlaction=maintenanceFichiers"><?= kAdminMaintenanceFichierOrphelins ?></a></li>
		</ul>
		<?}?>
	</div>

<?}?>
</div>
