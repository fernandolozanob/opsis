<?xml version="1.0" encoding="utf-8"?>
<!-- paletteSimple.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />


<xsl:template match='/select'>

	<data>
		<nb_pages><xsl:value-of select="$nb_pages"/></nb_pages>
		<nbRows><xsl:value-of select="$nb_rows"/></nbRows>
		<recherche><xsl:value-of select="$votre_recherche"/></recherche>
	
	<rows>
	 	<xsl:for-each select="val">
	                <xsl:variable name="j"><xsl:number/></xsl:variable>
	                <xsl:variable name="rang" select="number(number($offset)+$j)"/>
	 		<row rang="{$rang}">
	 			<value><xsl:value-of select="VALEUR"/></value>
	 		</row>
	 	</xsl:for-each>
	</rows>
	</data>


</xsl:template>

</xsl:stylesheet>
