<?xml version="1.0" encoding="utf-8"?>
<!-- docLienSaisie.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="id_doc"/>


    <xsl:template match='/select'>
        <table width="100%"  border="0" cellspacing="0" class="tableResults">
		<tr><td colspan="3"><h2><?=kParent?></h2></td></tr>
        <tr align="left" valign="top"> 
		  <td width="10%" class="resultsHead"><?= kType?></td>
          <td width="20%" class="resultsHead"><?= kDocument ?></td>
          <td width="40%" class="resultsHead"><?= kTitre ?></td>
        </tr> 
        
            <xsl:for-each select="dmat">
                <xsl:variable name="id_doc"><xsl:value-of select="id_doc"/></xsl:variable>
                <xsl:variable name="doc_cote"><xsl:value-of select="doc_cote"/></xsl:variable>
                <xsl:variable name="doc_titre"><xsl:value-of select="doc_titre"/></xsl:variable>
                <xsl:variable name="dmat_tcin"><xsl:value-of select="dmat_tcin"/></xsl:variable>
                <xsl:variable name="dmat_tcout"><xsl:value-of select="dmat_tcout"/></xsl:variable>
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">
                    <td class="resultsCorps"><input name="id_doc{$j}" type="hidden" value="{$id_doc}" />
                    <a href="javascript:popupSelect('docLienSaisieDetail.php?id_doc={$id_doc}', 'docLienSaisie')"><img src="design/images/button_modif.gif" border="0" title="{concat('&lt;?php echo kModifier ?&gt; : ',$doc_cote)}"  alt="&lt;?php echo kModifier; ?&gt;"/></a></td>
                    <td class="resultsCorps"><a href="javascript:popupDoc('docSaisie.php?id_doc={$id_doc}','doc')"><xsl:value-of select="doc_cote"/></a></td>
				  	<td width="10%" align="left"><xsl:value-of select="type_doc"/></td>
                  	<td width="20%" align="left"><xsl:value-of select="id_doc"/></td>
                    <td width="40%" align="left"><a href="doc.php?id_doc={$id_doc}?>"><xsl:value-of select="doc_titre"/></a> </td>
                    <td class="resultsCorps"><input name="dmat_tcin{$j}" type="text" id="dmat_tcin{$j}" size="10" maxlength="11"  value="{$dmat_tcin}" onFocus="formatTC(this)" onKeyPress="formatTC(this)" onKeyUp="formatTC(this)" onchange="updateLine({$j})"/></td>
                    <td class="resultsCorps"><input name="dmat_tcout{$j}" type="text" id="dmat_tcout{$j}" size="10" maxlength="11"  value="{$dmat_tcout}" onFocus="formatTC(this)" onKeyPress="formatTC(this)" onKeyUp="formatTC(this)" onchange="updateLine({$j})"/></td>
                    <td class="resultsCorps"><a href="javascript:removeLine({$j})"><img src="design/images/button_drop.gif" width="11" height="14" border="0" title="&lt;?php echo kSupprimer; ?&gt;"/></a></td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>
