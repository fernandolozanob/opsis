<?php

	if ($myUsr->loggedIn()) {
		require_once(modelDir.'model_panier.php');
		$myPanier=new Panier;
		
		if(defined("gShareSelGrp") && gShareSelGrp && isset($_REQUEST['getGroupes']) && $_GET['getGroupes']== "1"){
			$folders=Panier::getGroupeFolders();
			$myPage->params4XSL['groupeSel']=1;
		}else{
			$folders=Panier::getFolders();
		}
	} else
	{
		require_once(modelDir.'model_panier_session.php');
		$myPanier=new PanierSession;
		$folders=PanierSession::getFolders();
	}
	
	
	$array_pan_full_width = array('doc','docSaisie','admin','docAccSaisie','mat','matSaisie');
	
	
	$css_classes=(!isset($_GET['urlaction']) || in_array($_GET['urlaction'],$array_pan_full_width))?'full_width':'';
	
	if( (isset($_GET['urlaction']) 
		&& !in_array($_GET['urlaction'],$array_pan_full_width)) 
		&& isset($_SESSION['USER']['layout_opts']['panState']) 
		&& intval($_SESSION['USER']['layout_opts']['panState']) == 2
		&& (!isset($_GET['init']) && !isset($_POST['F_doc_form']))){
		
			$css_classes.= ' full expanded';
			echo '<script>$j("#mainResultsBar .toolbar").addClass("hidden");</script>';
	}else{
		$css_classes.= ' collapsed';
	}
	
?>


<div id="panBlock" class=" pres_<?=$_SESSION['USER']['layout_opts']['layout']?>  <?=$css_classes?> ">
	<div id="panGauche" class="<?=(($_SESSION['USER']['layout_opts']['layout']<1 || $_SESSION['USER']['layout_opts']['layout']>2)?'collapsed':'')?>">
		<div class="title_bar" onclick="togglePanBlock();event.stopPropagation();"><?=kPaniers;?></div>
		<ul class="panListe"></ul>
	</div>
	<div id="mainPan">
		<div class="title_bar"  onclick="togglePanBlock();event.stopPropagation();"><span id="panTitle"><?	
			
			if(isset($_GET['xsl'])){
				$_REQUEST['xsl'] = $_GET['xsl'];
			}else if( isset($_SESSION['recherche_DOC_Solr']['docListeXSL']) && $_SESSION['recherche_DOC_Solr']['docListeXSL'] == "docListe2.xsl"){
				$_REQUEST['xsl'] = "panier2";
			}else {
				$_REQUEST['xsl'] = "panier";
			}
			
			$_REQUEST['nbLignes'] = "all";
			
			if(isset($_SESSION['USER']['layout_opts']['id_current_panier']) && !empty($_SESSION['USER']['layout_opts']['id_current_panier'])){
				foreach($folders as $f){
					if($f['ID_PANIER'] == $_SESSION['USER']['layout_opts']['id_current_panier']){
						if(intval($f['PAN_ID_ETAT']) == 1){
							echo kPanier;
						}else{
							echo $f['PAN_TITRE'];
						}
						$_REQUEST['id_panier'] = $f['ID_PANIER'];
					}
				}
			}else if(count($folders)>0 && $folders[0]['PAN_ID_ETAT'] ==0 && isset($folders[0]['PAN_TITRE']) && !empty($folders[0]['PAN_TITRE'])){
				echo $folders[0]['PAN_TITRE'];
				$_REQUEST['id_panier'] = $folders[0]['ID_PANIER'];
			}else if($folders[0]['PAN_ID_ETAT'] == 1 ){
				echo kPanier;
				$_REQUEST['id_panier'] = $folders[0]['ID_PANIER'];
			}else{
				echo "&#160;";
			}?></span>
			<div class='toolbar' onclick="event.stopPropagation();">
				<div class="check_all tool" onclick="smartCheckAll(this,$j('#panFrame'));" ><span class="tool_hover_text"><?=kToutSelectionner?></span></div>
				<div class="add tool" ><span class="tool_hover_text"><?=kAjouterA?></span><div class="drop_down" style="display:none;"></div></div>
				<div class="tri tool"><span class="tool_hover_text"><?=kTrierPar?></span>
					<div class="drop_down" style="display:none;">
						<ul>
							<li onclick="loadPanier(id_current_panier,null,'doc_titre');"><? echo kTitre;?></li>
							<li onclick="loadPanier(id_current_panier,null,'doc_date_prod');"><? echo kDate;?></li>
							<li onclick="loadPanier(id_current_panier,null,'fonds');"><? echo kFonds;?></li>
							<li onclick="loadPanier(id_current_panier,null,'doc_id_type_doc');"><? echo kType;?></li>
						</ul>
					</div>
				</div>
				<div  onclick="popupPrint('print.php?urlaction=panier&id_panier'+id_current_panier,'main')" class=" print tool"><span class="tool_hover_text"><?=kImprimer?></span></div>
				<div class="presentation">
					<span id="panListChooser" class="<?=($_REQUEST['xsl']=='panier2')?'selected':'';?>" onclick="presentation_updateLignes('docListe2.xsl')"></span>
					<span id="panMosChooser"  class="<?=($_REQUEST['xsl']=='panier')?'selected':'';?>" onclick="presentation_updateLignes('docListe.xsl')"></span>
				</div>
			</div>
			<span class='montage_back' onmousedown="retourSelFromMontage();"><?=kQuitterMontage?></span>
			<span id="panExpandFull" onclick="togglePanBlock('full');event.stopPropagation();"></span>
		</div>
		<div id="panierDropZone" ><span id="textDropZone"><?=kDropZoneText?></span></div>
			<div id="panFrame"></div>
	</div>
	
	<script>
		var id_current_panier = '';
		var ordre = 0;
		var flag_reload_current_pan = false ; 
		refreshDroppables();
		
		function loadPanier(id_pan, elt,tri,xsl,urlaction){
		
			if(typeof tri != "undefined" && tri!= null){
				flag_reload_current_pan = true;
			}
			
			if(typeof urlaction == "undefined" ||urlaction == null){
				urlaction = "panier";
			}
			
			// console.log("loadPanier , id_pan : "+id_pan+"tri "+tri+" ordre "+ordre);
			if(typeof elt != "undefined" && elt != null){
				$j("ul.panListe *").removeClass("selected");
				$j(elt).addClass("selected");
			}
			
			if(typeof xsl == 'undefined' || xsl == null){
				xsl = '<?=$_REQUEST['xsl']?>';
			}
			
			
			if(id_pan != id_current_panier || flag_reload_current_pan){
				ajax_url = "empty.php?where=include&urlaction="+urlaction+"&id_panier="+id_pan+"&nbLignes=all&xsl="+xsl+"&test="+Math.random();
				
				if(tri){
					ajax_url += "&tri="+tri+ordre;
					ordre ++ ;
					ordre = ordre%2;
				}
				$j("#panFrame").addClass('loading')	;
				
				$j.ajax({
					url : ajax_url,
					success : function(data){
						$j("#panFrame").html(data);
						if($j(".mosWrapper").length>0){
							adjustMosaique();
						}
						id_current_panier = id_pan;
						updateDraggables();
						var pan_title;
						if($j('#panFrame form input[name="pan_titre"]').val() != ''){
							// $j("#panTitle").text($j('#panFrame form input[name="pan_titre"]').val());
							pan_title = $j('#panFrame form input[name="pan_titre"]').val();
						}else if($j("#panFrame form input[name='pan_id_etat']").val()=="1" && $j("#panFrame form input[name='pan_id_type_commande']").val()=="0"){
							// $j("#panTitle").text("<?= kPanier ?>");
							pan_title = "<?= kPanier ?>";
						}else{
							// $j("#panTitle").text("");
							pan_title = "" ; 
						}
						
						$j("#panTitle").text(pan_title);
						if($j("ul.panListe #panier"+id_pan).text() != pan_title){
							$j("ul.panListe #panier"+id_pan).text(pan_title);
						}
						
						
						if($j('#panier'+id_current_panier).length>0 &&  !$j('#panier'+id_current_panier).hasClass('selected')){
							$j("ul.panListe *").removeClass("selected");
							$j('#panier'+id_current_panier).addClass("selected");							
						}
						$j("#panBlock div.toolbar .check_all").removeClass('checked');
						
						
						$j("#panFrame .resultsMos, #panFrame tr:not(.separator, .resultsHead_row)").hover(callHoverPlayer);
						$j("#panFrame .resultsMos, #panFrame tr:not(.separator, .resultsHead_row)").mouseleave(hideHoverPlayer);
						
						setTimeout(function(){
							$j("#panFrame").removeClass('loading');
						},100);
						
						updateLayoutFlags();
					}
				});
				flag_reload_current_pan = false ; 
			}
		}
		refreshFolders();
		$j(document).ready(function(){
			loadPanier('<?=$_REQUEST['id_panier']?>',$j('#panier<?=$_REQUEST['id_panier']?>'));
			
		});
		
	
	function retourSelFromMontage(){
		 if(($j("#panBlock").hasClass("montage") && panHasChanged && confirmExit)){
			var check1 = confirm(str_lang.kJSConfirmExit);
			if(check1){
				confirmExit = false ; 
				retourSel();
			}else{
				return false ; 
			}
		 }
	}
	
	function retourSel(){
		$j("#formPanier").attr('onsubmit','');
		$j("#formPanier").unbind('onsubmit');
		$j("#formPanier").unbind('submit');
		$j("#panBlock").removeClass('montage');
		$j("#montageContainer video").each(function(idx,elt){
			try{
				$j(elt).get(0).pause();
			}catch(e){}
		});
		veto_right_menu = false ; 
		flag_reload_current_pan=true;
		loadPanier(id_current_panier);
	}
	
	function goToMontage(){
		if(myVideo != null && backupLaunch_myVideo != null){
			myVideo.media = null;
			myVideo = null ; 
			$j("#media_space #container").html(backupLaunch_myVideo);
		}
	
		if($j("#menuDroite").length>0 && !$j('#menuDroite').hasClass('collapsed')){
			toggleRightPreview();
		}
		veto_right_menu = true ; 
		$j("#panBlock").addClass('montage');
		flag_reload_current_pan=true;
		loadPanier(id_current_panier,null,null,'montage');
	}
	
	function saveCurrentCart(){
		$j("#formPanier input[name='commande']").val('SAVE');
		$j("#formPanier").submit();
		flag_reload_current_pan=true;
		setTimeout(function(){loadPanier(id_current_panier,null,null);},500);
	}
	
	function emptyCurrentCart(){
		if (confirm('Voulez vous vider la sélection?')){
			$j("#formPanier input[name='commande']").val('SUP_ALL');
			$j("#formPanier").submit();
			flag_reload_current_pan=true;
			setTimeout(function(){loadPanier(id_current_panier,null,null);},500);
		}
	}
	
	function deleteCurrentCart(){
		if (confirm('Voulez vous supprimer la sélection?')){
			$j("#formPanier input[name='commande']").val('SUP');
			$j("#formPanier").submit();
			flag_reload_current_pan=true;
			setTimeout(function(){refreshFolders();$j("ul.panListe li").eq(0).click();},500);
		}
	}
	
	function downloadCurrentCart(type){
		var checkbox_checked=false;
		var array_checkbox = new Array();
		$j('#panFrame input[type="checkbox"][name^="checkbox"]').each(function (idx,elt)
		{
			if ($j(elt).prop('checked')==true){
				checkbox_checked=true;
				array_checkbox.push($j(elt).attr("name"));
			}
		});
		
		if (!checkbox_checked){
			$j('#panFrame input[type="checkbox"][name^="checkbox"]').attr('checked','checked');
			$j('#panFrame input[type="checkbox"][name^="checkbox"]').each(function (idx,elt){
				array_checkbox.push($j(elt).attr("name"));
			});
		}
		
		data_post = {
			commande : "INIT_LIGNES",
			pan_id_type_commande : type,
			conserverPanier : '1'
		};
		for(i=0 ; i < array_checkbox.length;i++){
			data_post[""+array_checkbox[i]] = "1";
		}
		
		$j.ajax({
			url : "empty.php?where=include&urlaction=commande&id_panier="+id_current_panier+"&nbLignes=all",
			method : "POST",
			data : data_post,
			success : function(data){
				$j("#panFrame").html(data);
			}
		});
	}
	
	
	function validCommandeToDownload(){
		
		id_commande = $j("#panFrame form input[name='id_panier']").val();
		
		data_post = {
			commande : "VALID",
			noSendMailAdmin : "1",
			pan_id_type_commande : $j("#panFrame form input[name='pan_id_type_commande']").val()
		};
		
		$j("#formCommande input,#formCommande textarea").each(function(idx,elt){
			if(typeof data_post[$j(elt).attr("name")] == "undefined"){
				if($j(elt).is("textarea")){
					data_post[$j(elt).attr("name")] = $j(elt).text();
				}else{
					data_post[$j(elt).attr("name")] = $j(elt).attr("value");
				}
			}
		});
		$j.ajax({
			url : "empty.php?where=include&urlaction=commande&id_panier="+id_commande+"&nbLignes=all",
			method : "POST",
			data : data_post,
			success : function(data){
				$j("#panFrame").html(data);
			}
			
		});
		// refreshCommande(id_commande,data_post);
		
	}
	
	function refreshCommande(id_commande,data_post){
		$j.ajax({
			url : "empty.php?where=include&urlaction=commande&id_panier="+id_commande+"&nbLignes=all",
			method : "POST",
			data : data_post,
			success : function(data){
				$j("#panFrame").html(data);
			}
			
		});
	}
	
	function removeLine(i,type,action) {
		event.stopPropagation();
		
		if( document.getElementById("formPanier") != null){
			formPan = document.getElementById("formPanier");
			if (confirm(str_lang.kConfirmJSLineSuppression)) {
				formPan.ligne.value=i;
				if (typeof(action)=='undefined') action="SUP";
				formPan.commande.value=action;
				if (type){
					formPan.type.value=type;
				}
				formPan.submit();
				document.getElementById("framePanier").onload = function(){
					$j(".panListe .selected").click();
				}
				flag_reload_current_pan = true ;
				
				
			}
		}else if ( document.getElementById("formCommande")!= null){
			formPan = document.getElementById("formCommande");
			if (confirm(str_lang.kConfirmJSLineSuppression)) {
				// console.log("here");
				formPan.ligne.value=i;
				if (typeof(action)=='undefined') action="SUP";
				formPan.commande.value=action;
				if (type){
					formPan.type.value=type;
				}
				formPan.submit();
				
				flag_reload_current_pan = true ;
				
				setTimeout(function(){refreshCommande($j("#formCommande input[name='id_panier']").val(),"")},500);
			}
			
			
		
		}
		

	}
	

	</script>
		
</div>