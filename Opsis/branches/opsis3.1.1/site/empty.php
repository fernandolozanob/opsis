<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- Developpeurs : Loic Desjardins											  ---*/
/*-----------------------------------------------------------------------------------*/
// NOTA : éè respecter cet ordre !!!
	error_reporting(E_PARSE | E_ERROR);
	// error_reporting(E_ALL);

    $time_start = microtime(true);
     require_once("conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php");
     require_once(libDir."class_page.php");

    $myFrame=Page::getInstance();
    
    $myFrame->setDesign(templateDir."empty.html");

	$myFrame->includePath=frameDir;
	if (isset($_GET['where']) && $_GET['where']) $myFrame->includePath=coreDir."/".$_GET['where']."/";
	if (isset($_GET['upload']) && $_GET['upload']) $myFrame->includePath=upload_scriptDir;
	$myUsr=User::getInstance();

  	$html=$myFrame->render();

	print($html);
	$time_end = microtime(true);
	$time=$time_end-$time_start;
	exit;
?>

