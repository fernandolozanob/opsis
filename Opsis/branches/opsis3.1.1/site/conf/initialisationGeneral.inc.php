<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- Developpeurs : Vincent Prost, Francois Duran, Jerome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

//------------------
// Definition site :
//------------------

define("kAnneeCopyright",2015);
define("kQuiCopyright","Opsomai");
define("gSite","Démonstration Opsis Media"); //nom du site
define("gSiteLogoFilename","logo_opso_alpha_2.png"); //nom du fichier logo 
define("gSiteLogoFilenameSmall","miniopso_alpha_center.png"); //nom du fichier logo 
define("gModRewrite",0);
define("langueDefaut","FR");


//------------------
// Type de comptes et pages interdites :
//------------------
    define("kNotLogged",0);
    define("kLoggedNorm",1);
    define("kLoggedUsagerAdv",2);
    define("kLoggedDoc",3);
    define("kLoggedAdmin",4);

// Definition des pages INTERDITES :
$allPages="alerteMail.php, majDoc.php, majMatInfo2.8.php, majMediainfo.php, renameStoryboard.php, updateFTindex.php, updateFTindexPG.php, updateJobs.php, updateMatLieu.php, updateThesNbDocs.php, chercheMotBasis.php, chercheMots.php, constantes.php, creaFileMontage.php, downloadFile.php, downloadMontageWrapper.php, extractPDF.php, getEtapeParam.php, getEtatMontage.php, getLines.php, getTranscodePreset.php, getVisioUrl.php, iframeCommande.php, lexExists.php, lexFusion.php, loadDoc.php, loadMats.php, loadPanier.php, loadPers.php, loadTreeTheme.php, makeVignette.php, paiementCommande.php, prepareVisu.php, processCategorie.php, processComment.php, processDocument.php, processJob.php, processPanier.php, processSequence.php, processStoryboard.php, processUsagerGroupe.php, readSession.php, ttreeOpenNode.php, updateDocMat.php, updateDocument.php, updateJobListe.php, updateMateriel.php, uploadTinymce.php, valExists.php, admin.php, backupJob.php, catListe.php, catSaisie.php, checkFileCreationEnd.php, cherche.php, chercheIndex2.php, chercheIndexBasis.php, comListe.php, commande.php, commandeSaisie.php, doc.php, docAcc.php, docAccSaisie.php, docBasis.php, docImageur.php, docImageurSaisie.php, docLien.php, docListe.php, docMatListe.php, docMatSaisie.php, docSaisie.php, docSeq.php, docSinequa.php, erreur.php, etapeListe.php, etapeSaisie.php, expListe.php, export.php, expSaisie.php, festListe.php, festSaisie.php, festView.php, fondsListe.php, fondsSaisie.php, generateSinequaFlatFiles.php, generateSinequaXMLFiles.php, groupeListe.php, groupeSaisie.php, histNav.php, historique.php, htmlListe.php, htmlSaisie.php, imageurListe.php, imageurSaisie.php, importVideo.php, importXML.php, inscription.php, jobListe.php, jobRecordListe.php, jobSaisie.php, jobValid.php, lexListe_olddtree.php, lexListe.php, lexSaisie.php, listeRapport.php, livraison.php, loginDemande.php, loginValidate.php, mail.php, maintenanceFichiers.php, mat.php, matImageurSaisie.php, matListe.php, matSaisie.php, messageListe.php, messageView.php, modifLot.php, panier.php, panierListe.php, personneListe.php, personneSaisie.php, personne.php, prexSaisie.php, printHLex.php, procListe.php, procSaisie.php, recherche.php, refListe.php, refSaisie.php, saisieLot.php, selectStoryboard.php, stats.php, tapeListe.php, tapeSaisie.php, tapeSetListe.php, tapeSetSaisie.php, trash.php, trashListe.php, usagerListe.php, usagerSaisie.php, valListe.php, valSaisie.php, visualisation.php, GetRecord.php, Identify.php, ListIdentifiers.php, ListMetadataFormats.php, ListRecords.php, ListSets.php, copyFile.php, finUpload.php, importFCP.php, importFolder.php, jqueryUploadHandler.php, processFiles.php, processUpload.php, putPostInSession.php, simpleUpload.php, simpleUploadJava.php, simpleUploadJS.php, detail.php, detailRang.php, getfile.php, getliste.php, listevaleurs.php, recherche.php, sitemap.php";

$adminPages= ""; // L'admin peut, logiquement, acc�der � toutes les pages
$docPages = $adminPages.", htmlSaisie.php,htmlListe.php, usagerListe.php, usagerSaisie.php, importXML.php, importVideo.php"; // Les pages auxquelles le documentaliste ne peut acc�der.
$usagerPages = $docPages.", lexListe.php,lexSaisie.php, matListe.php, matSaisie.php,comListe.php,refListe.php"; //personne loggee
$usagerAdvPages = $usagerPages; //personne loggee
$loggedPages = "alerteMail.php, majDoc.php, majMatInfo2.8.php, majMediainfo.php, renameStoryboard.php, updateFTindex.php, updateFTindexPG.php, updateJobs.php, updateMatLieu.php, updateThesNbDocs.php, chercheMotBasis.php, chercheMots.php, constantes.php, creaFileMontage.php, downloadFile.php, downloadMontageWrapper.php, extractPDF.php, getEtapeParam.php, getEtatMontage.php, getLines.php, getTranscodePreset.php, getVisioUrl.php, iframeCommande.php, lexExists.php, lexFusion.php, loadDoc.php, loadMats.php, loadPanier.php, loadPers.php, loadTreeTheme.php, makeVignette.php, paiementCommande.php, prepareVisu.php, processCategorie.php, processComment.php, processDocument.php, processJob.php, processPanier.php, processSequence.php, processStoryboard.php, processUsagerGroupe.php, readSession.php, ttreeOpenNode.php, updateDocMat.php, updateDocument.php, updateJobListe.php, updateMateriel.php, uploadTinymce.php, valExists.php, admin.php, backupJob.php, catListe.php, catSaisie.php, checkFileCreationEnd.php, comListe.php, commande.php, commandeSaisie.php, docAcc.php, docAccSaisie.php, docBasis.php, docImageur.php, docImageurSaisie.php, docLien.php, docMatListe.php, docMatSaisie.php, docSaisie.php, docSeq.php, docSinequa.php, erreur.php, etapeListe.php, etapeSaisie.php, expListe.php, export.php, expSaisie.php, festListe.php, festSaisie.php, festView.php, fondsListe.php, fondsSaisie.php, generateSinequaFlatFiles.php, generateSinequaXMLFiles.php, groupeListe.php, groupeSaisie.php, histNav.php, htmlListe.php, htmlSaisie.php, imageurListe.php, imageurSaisie.php, importVideo.php, importXML.php, jobListe.php, jobRecordListe.php, jobSaisie.php, jobValid.php, lexListe_olddtree.php, lexListe.php, lexSaisie.php, listeRapport.php, livraison.php, loginDemande.php, loginValidate.php, mail.php, maintenanceFichiers.php, mat.php, matImageurSaisie.php, matListe.php, matSaisie.php, messageListe.php, messageView.php, modifLot.php, panierListe.php, personneListe.php, personneSaisie.php, personne.php, prexSaisie.php, printHLex.php, procListe.php, procSaisie.php, recherche.php, refListe.php, refSaisie.php, saisieLot.php, selectStoryboard.php, stats.php, tapeListe.php, tapeSaisie.php, tapeSetListe.php, tapeSetSaisie.php, trash.php, trashListe.php, usagerListe.php, usagerSaisie.php, valListe.php, valSaisie.php, visualisation.php, GetRecord.php, Identify.php, ListIdentifiers.php, ListMetadataFormats.php, ListRecords.php, ListSets.php, copyFile.php, finUpload.php, importFCP.php, importFolder.php, jqueryUploadHandler.php, processFiles.php, processUpload.php, putPostInSession.php, simpleUpload.php, simpleUploadJava.php, simpleUploadJS.php, detail.php, detailRang.php, getfile.php, getliste.php, listevaleurs.php, recherche.php, sitemap.php"; //pas loggee

//------------------
// Log et erreurs
//------------------
    define("kPathInternalLog",getcwd() . "/log/");



//------------------
// Temps de connexion
//------------------


	define("kTimeNow",time()); // Tps actuel
	//TEMPS DE DUREE D'UNE CONNEXION. SI CE DELAI EST DEPASSE L'UTILISATEUR DOIT S'IDENTIFIER DE NOUVEAU.
	define("kDelaiSession",600); // 1 delai de 10 minutes

//----------------
//	Paramètres de visionnage
//----------------
	define("gDecoup",0); //Découpage à la volée de video autorisé ou non (via Java) 1=Oui.
	define("gPlayerVideo","popUpVisionnage"); // Player video : "popUpVisionnage" (capsule) ou "popupVideo" (plug-in)
    define("gQTversion","7.00");
	define("gRatioDecoupageVideo",0.5); //En dessous de quel ratio (durée extrait)/(durée totale) décide-t-on de découper une vidéo ?


/* --------------------------------
 * Paramètres applicatifs
 * ------------------------------
*/
define("gDocAccAlsoCheckUrl",false); //Vérifier également que les doc acc de type lien HTML existent réellement. ATTENTION : prend du temps !
define("gDocAccDeleteFiles",false); //Supprimer la dernière occurence d'un doc acc en base supprime aussi le fichier sur serveur ?
 //define("gDocUniqueFields",serialize(array("DOC_TITRE"))); //Tableau des champs déterminant le test d'unicité d'un doc à l'enregistrement

define("gGestPers",0); // Le site propose-t-il la gestion des personnes : 0=non, 1=oui

// Liste des types de documents d'accompagnement autorisés
define("gListeTypesDocAcc",serialize(array ("image/pjpeg","image/x-png","image/jpeg","image/png","image/gif","application/vnd.ms-excel","application/msword","application/pdf")));

define("gLivraisonTypeNom","DOC_TITRE"); // Champ utilisé pour nommer le fichier d'extrait lors d'une livraison


define("gMaterielDeleteFiles",false); //Supprimer un matériel en base supprime aussi le fichier sur serveur ? PAS ENCORE UTILISE.

define("gPutRefInSession",true); //Mettre les tables de référence en Session ? true/false. true FORTEMENT conseillé. Si false, une requete est exécutée.

define("gStoryboardIntervalleDefaut",10); //Lors de la génération auto de storyboard, intervalle par défaut entre les images (en secondes)

define("gTypeDocSequence",4); //Dans la table type_doc, valeur pour le type séquence. Utilisé par ORAO pour créer une séquence avec un type SEQUENCE

define("gUploadModeRewrite","Rename"); //Upload AJAX/JAVA : en cas de fichier déjà existant, on ajoute un incrément au fichier

// Utilisés pour la livraison et la création DOC depuis MAT
define("gAudioTypes",serialize(array("aac","ac3","aif","aiff","mp3","wav","wma","m4a")));
define("gImageTypes",serialize(array("jpg","jpeg","gif","png","tiff","bmp")));
define("gVideoTypes",serialize(array("avi","flv","m4v","mkv","mp2","mp4","mpeg","mpg","mts","mov","mxf","rm","webm","wmv","mxf","ts","vob")));
define("gDocumentTypes",serialize(array("pdf")));

define("gAlwaysUpdateDureeWithDocMat",true); // Mise à jour durée doc d'après mat
define("gReportagePicturesActiv",2); // Si present activation des reportages avec comme id_type_doc la valeur présente
//define("gReportInheritedFields",serialize(array("DOC_ACCES" => "INIT", "DOC_DATE_MOD" => "INIT", "L_TM" => "ADDVAL")));

define("gAwstatsUrl", "http://demo.opsismedia.com/stats/awstats.pl?config=demo");

/* ----------------------------------
* Paramètres liés à la recherche
* ----------------------------------
*/

define("gParamsRechercheDoc",serialize(array("RechercheLexiqueMode"=>"EXACT",
											 "RechercheLexiqueRestrictionLangage"=>true)));

define("gRechercheFTSliceWords",false); //Une recherche FT avec des mots simples est-elle découpée ?
										// Si true => l'expression est découpée en autant de mots pour être traitée en FT => (ecran) (solaire)
										// Si false => l'expression est envoyée telle quelle au moteur de recherche => (ecran solaire)
										//NOTE 13/11/07 : plus utilisé pour le highlight, juste en recherche FT
										//Tableau des substitutions d'accents (sert à construire les chaines d'highlight.)


define("gTabAccents",serialize(array("e" => array("e","é","è","ê","ë","E","È","Ê","Ë","É"),
									 "a"=>array("a","à","â","ä","A","Á","À","Ä","Â"),
									 "u"=>array("u","û","ù","ü","U","Ù","Ü","Û"),
									 "i"=>array("i","î","ì","ï","I","Ì","Ï","Î"),
									 "o"=>array("o","ô","ò","ö","O","Ô","Ö","Ò"),
									 "c"=>array("c","ç","C","Ç")
									 )));

define("kMaxExtensionLexicale",250); //Nombre de termes maxi d'extension lexicale pour le highlight. Cette limite est empirique pour éviter un plantage de preg_replace

define("kMaxRows",5000); //Nb max de lignes ramenées par un affichage "all". Si une recherche ramène plus, on repasse en affichage paginé.
						 // Cette limite permet 1. de ne pas surcharger le serveur, 2. d'éviter des pb de XML/XSLT trop long.

define("kNbCritereRechExperte",5); //Nombre de critères libres dans le formulaire de rech experte

/* ----------------------------------
 * Paramètres liés aux commandes et paniers
 * ----------------------------------
 */

// Tableau des critères requis pour calculer un prix dans le panier.
/*
 define("garrCriteresAddCalculPrix",serialize( array (
													  array(	"object"=>"panier",
																"where"=>"XML/commande/PANXML_TYPE_UTILISATION",
																"what"=>"PANXML_TYPE_UTILISATION")
													  )
											   ));
 */
define("garrCriteresAddCalculPrix",null);

// Mail de confirmation de commande.
define("gMailBody","<html><head/><body style='font-family: Arial,sans-serif;
color: #333333;
margin: 0px;
padding: 0px;
background-color: #FFFFFF;'>
<div align='center' style='padding:10px;spacing:10px;'>
'%s' </div></body></html>");

define("gModesAvecCalculPrix",serialize(array("2"))); // Type de commande (PAN_ID_TYPE_COMMANDE) déclenchant un calcul de tarifs.

// Ordre SQL destiné à vérifier si les documents d'un panier peuvent être
// commandés ou prêtés, etc... Pour échapper les % (par ex dans les like), ne
// oublier de mettre %%
define("gsqlCheckDispoDocPanier","
	select distinct t_panier_doc.ID_DOC,t_panier_doc.PDOC_SUPPORT_LIV
	from t_doc_val dvDIF,
		 t_panier_doc,
		 t_panier
	where t_panier.ID_PANIER='%s' and dvDIF.ID_DOC = t_panier_doc.ID_DOC
	and t_panier.ID_PANIER = t_panier_doc.ID_PANIER
");

//Tableau de paramètres utilisés dans la requête ci-dessus. L'ordre des params doit respecter celui des variables dans la requete
define ("gsqlCheckDispoDocPanierParams",serialize(array("ID_PANIER")
												));
//Récupération des supports commercialisables pour chaque document dans le panier
define ("gsqlGetSupportByDoc","
		select distinct VALEUR from
		t_val
		WHERE
		/* PARAM INUTILE ICI : ID_DOC '%s' */
		VAL_ID_TYPE_VAL='SUP'
		AND VALEUR in (select distinct SUPPORT_LIV FROM t_tarifs where 1=1 %s)
");




define ("kTVA",19.6); //Taux de TVA (calcul des tarifs).


//------------------
// Etat des jobs
// Il faut impérativement respecter l'ordre croissant
//------------------
define("jobAttente",1);
define("jobEnCours",2);
define("jobEnDemandeAnnulation",3);
define("jobEnCoursAnnulation",4);
define("jobEnDemandeSuppression",5);
define("jobEnCoursSuppression",6);
define("jobAnnule",7);
define("jobErreur",8);
define("jobFini",9);

//------------------
// Etat des paniers
// A synchroniser avec t_etat_pan
//------------------

define("gPanierCourant",1);
define("gPanierTransmis",2);
define("gPanierTraitement",3);
define("gPanierFini",4);

define("gCommandeRAZ",1);

define('gEtatPanierDefaut', 1);

define("gAchatMontantMin",10); // Pour les paiements par carte
define("gAchatMontantMax",1000); // Pour les paiement par carte
define('gNbLignesDocListeDefaut', 50);

define("gResolutionImageDPI",300);

//------------------
// Upload de média en ligne avec création de sélection d'import
//------------------
define("gUploadToSelection", true); // défini si on crée un panier qui recoit les nouveaux documents générés pendant l'import
define("gUploadSelectionName","import ".date("y.m.d H:i"));  //défini le pattern de nom du nouveau panier créé lors de lupload (pour l'instant si il n'est pas défini, on utilise une valeur par défaut dans finUpload qui est triés similaire Ã  cette valeur)


define('gAjoutChampsRechercheSolr','doc_id_fonds');
define("kSolrOptions",serialize(array("hostname"=>"localhost","login"=>"","password"=>'',"port"=>"8983","path"=>'solr/demo')));

?>
