<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- D?veloppeurs : Ghislain Thomas     										  ---*/
/*-----------------------------------------------------------------------------------*/
	
// NOTA : respecter cet ordre !!!
$time_start = microtime(true);
require_once("conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
require_once(libDir."class_page.php");

$myPage=Page::getInstance();
$myPage->setDesign(templateDir."vide.html");
$myPage->includePath=upload_scriptDir;

$html=$myPage->render();  

print($html);

$time_end = microtime(true);
$time=$time_end-$time_start;
//if (defined('debugMode') && debugMode==true) echo "<div style='text-align:right;font-size:9px;color:#880000'>".$time." sec.</div>";
exit;

?> 

