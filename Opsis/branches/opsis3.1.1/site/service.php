<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- Developpeurs : Loic Desjardins											  ---*/
/*-----------------------------------------------------------------------------------*/
	
// NOTA : respecter cet ordre !!!

     require_once("conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php"); 	
     require_once(libDir."class_pageWebService.php");
	
	error_reporting(E_ERROR | E_WARNING | E_PARSE);
    //error_reporting(E_ALL);
    
    $myFrame=PageWebService::getInstance();
    $myFrame->setDesign(templateDir."empty.html");

    $myFrame->includePath=webserviceDir;
    
	$myUsr=User::getInstance();

  	$html=$myFrame->render();  

	print($html);
unset($_SESSION);	

	exit;
?> 

