<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->

<xsl:param name="scripturl"/>
<xsl:param name="id_lang"/>

<xsl:template match='/t_doc'>

<data>



   <doc id_doc="{ID_DOC}">
  
   
      <id_doc><xsl:value-of select="ID_DOC"/></id_doc>
      <id_lang><xsl:value-of select="ID_LANG"/></id_lang>
      
       <xsl:variable name="titretraduit">
                <xsl:choose>
                <xsl:when test="ID_LANG='fr' or ID_LANG='FR'">
                	<xsl:value-of select="DOC_TITRE"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="DOC_AUTRE_TITRE" /></xsl:otherwise>
                </xsl:choose>
                </xsl:variable>
                <xsl:variable name="articletraduit"><xsl:choose><xsl:when test="ID_LANG='fr' or ID_LANG='FR'"><xsl:value-of select="normalize-space(DOC_TITRE_COL)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="normalize-space(DOC_COTE_ANC)" /></xsl:otherwise></xsl:choose>
      </xsl:variable>
      <titre_original><xsl:if test="DOC_SOUSTITRE!=''">(<xsl:value-of select="DOC_SOUSTITRE"/>) </xsl:if>
			<xsl:value-of select="DOC_TITREORI"/>
      </titre_original>
      <titre_traduit>
      		<xsl:if test="normalize-space($articletraduit)!=''"><xsl:value-of select="$articletraduit"/>&#160;</xsl:if><xsl:value-of select="$titretraduit"/>
      </titre_traduit>
      <duree><xsl:value-of select="DOC_DUREE"/></duree>
      <annee><xsl:value-of select="substring-before(DOC_DATE_PROD,'-')"/></annee>
      <resume>
           <xsl:choose>
                <xsl:when test="ID_LANG='fr' or ID_LANG='FR'">
                	<xsl:value-of select="DOC_RES"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="DOC_RES_CAT" /></xsl:otherwise>
           </xsl:choose>     	
      </resume>
      <theme>
      	<xsl:value-of select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='THM']/t_val/VALEUR"/>
      </theme>
       <commentaire>
           <xsl:choose>
                <xsl:when test="ID_LANG='fr' or ID_LANG='FR'">
                	<xsl:value-of select="DOC_COMMENT"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="DOC_NOTE_INTERNE" /></xsl:otherwise>
           </xsl:choose>     	
      </commentaire>     
      <vignette>
		<xsl:if test="VIGNETTE!=''">
      	service.php?urlaction=getFile&amp;amp;type=doc_acc&amp;amp;filename=<xsl:value-of select="substring-after(substring-after(VIGNETTE,'/'),'/')"/>
      	</xsl:if>
      </vignette>
   
   	  <descripteurs>
   	  	<motscles>
   	  		<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='NC']/t_rol[@DLEX_ID_ROLE='NC']/t_lex">
  				<motcle>
  				<id_lex><xsl:value-of select="ID_LEX"/></id_lex>
  				<terme><xsl:value-of select="LEX_TERME"/></terme>
  				<type><xsl:value-of select="LEX_ID_TYPE_LEX"/></type>
   	  			</motcle>
   	  		</xsl:for-each>
   	  	</motscles>
   	  	<pays>
   	  		<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='NC']/t_rol[@DLEX_ID_ROLE='PS']/t_lex">
  				<pays>
  				<id_lex><xsl:value-of select="ID_LEX"/></id_lex>
  				<terme><xsl:value-of select="LEX_TERME"/></terme>
  				<type><xsl:value-of select="LEX_ID_TYPE_LEX"/></type>
   	  			</pays>
   	  		</xsl:for-each>
   	  	</pays>
   	  	<lieux>
   	  		<xsl:for-each select="t_doc_lex/TYPE_DESC[@ID_TYPE_DESC='NC']/t_rol[@DLEX_ID_ROLE='LX']/t_lex">
  				<lieu>
  				<id_lex><xsl:value-of select="ID_LEX"/></id_lex>
  				<terme><xsl:value-of select="LEX_TERME"/></terme>
  				<type><xsl:value-of select="LEX_ID_TYPE_LEX"/></type>
  				</lieu>
   	  		</xsl:for-each>
   	  	</lieux>
   	  </descripteurs>
   	  
   	  <personnes>
   	  	 <realisateur>
   	  		<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='REA']/t_personne">
   	  			<personne>
   	  				<id_pers><xsl:value-of select="ID_PERS"/></id_pers>
   	  				<nom>
   	  					<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/>&#160;</xsl:if>
   	  					<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
   	  					<xsl:value-of select="PERS_NOM"/>
   	  				</nom>
   	  				<lien>service.php?urlaction=personne&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="ID_PERS"/></lien>
   	  			</personne>
   	  		</xsl:for-each>
   	  	 </realisateur>
   	  	 <producteur>
   	  		<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='PRD']/t_personne">
   	  			<personne>
   	  				<id_pers><xsl:value-of select="ID_PERS"/></id_pers>
   	  				<nom>
   	  					<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/>&#160;</xsl:if>
   	  					<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
   	  					<xsl:value-of select="PERS_NOM"/>
   	  				</nom>
   	  				<lien>service.php?urlaction=personne&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="ID_PERS"/></lien>
   	  			</personne>
   	  		</xsl:for-each>
   	  	 </producteur> 
    	 <distributeur>
   	  		<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='DIS']/t_personne">
   	  			<personne>
   	  				<id_pers><xsl:value-of select="ID_PERS"/></id_pers>
   	  				<nom>
   	  					<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/>&#160;</xsl:if>
   	  					<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
   	  					<xsl:value-of select="PERS_NOM"/>
   	  				</nom>
   	  				<lien>service.php?urlaction=personne&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="ID_PERS"/></lien>
   	  			</personne>
   	  		</xsl:for-each>
   	  	 </distributeur>  	  	 
    	 <montage>
   	  		<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='MON']/t_personne">
   	  			<personne>
   	  				<id_pers><xsl:value-of select="ID_PERS"/></id_pers>
   	  				<nom>
   	  					<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/>&#160;</xsl:if>
   	  					<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
   	  					<xsl:value-of select="PERS_NOM"/>
   	  				</nom>
   	  				<lien>service.php?urlaction=personne&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="ID_PERS"/></lien>
   	  			</personne>
   	  		</xsl:for-each>
   	  	 </montage>  
    	 <image>
   	  		<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='PHO']/t_personne">
   	  			<personne>
   	  				<id_pers><xsl:value-of select="ID_PERS"/></id_pers>
   	  				<nom>
   	  					<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/>&#160;</xsl:if>
   	  					<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
   	  					<xsl:value-of select="PERS_NOM"/>
   	  				</nom>
   	  				<lien>service.php?urlaction=personne&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="ID_PERS"/></lien>
   	  			</personne>
   	  		</xsl:for-each>
   	  	 </image>     	  	 
    	 <son>
   	  		<xsl:for-each select="t_pers_lex/TYPE_DESC[@ID_TYPE_DESC='GEN']/t_rol[@DLEX_ID_ROLE='SON']/t_personne">
   	  			<personne>
   	  				<id_pers><xsl:value-of select="ID_PERS"/></id_pers>
   	  				<nom>
   	  					<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/>&#160;</xsl:if>
   	  					<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
   	  					<xsl:value-of select="PERS_NOM"/>
   	  				</nom>
   	  				<lien>service.php?urlaction=personne&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="ID_PERS"/></lien>
   	  			</personne>
   	  		</xsl:for-each>
   	  	 </son>    	  	    	  	   	  	 
   	  </personnes>
   	  <sous_titres>
   	  	<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='SUB']/t_val">
   	  		<sous_titre><xsl:value-of select="VALEUR"/></sous_titre>
   	  	</xsl:for-each>
   	  </sous_titres>
      <langues>
   	  	<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='LANG']/t_val">
   	  		<langue><xsl:value-of select="VALEUR"/></langue>
   	  	</xsl:for-each>
   	  </langues>  
      <formats>
   	  	<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='FMT']/t_val">
   	  		<format><xsl:value-of select="VALEUR"/></format>
   	  	</xsl:for-each>
   	  </formats> 
      <couleurs>
   	  	<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='COUL']/t_val">
   	  		<couleur><xsl:value-of select="VALEUR"/></couleur>
   	  	</xsl:for-each>
   	  </couleurs> 
      <supports>
   	  	<xsl:for-each select="t_doc_val/TYPE_VAL[@ID_TYPE_VAL='SUP']/t_val">
   	  		<support><xsl:value-of select="VALEUR"/></support>
   	  	</xsl:for-each>
   	  </supports> 

	  <pieces_jointes>
	  	<xsl:for-each select="t_doc_acc/t_doc_acc">
	  	<piece_jointe>
	  		<id_doc_acc><xsl:value-of select="ID_DOC_ACC"/></id_doc_acc>
	  		<titre><xsl:value-of select="DA_TITRE"/></titre>
	  		<fichier>service.php?urlaction=getfile&amp;amp;type=doc_acc&amp;amp;filename=<xsl:value-of select="DA_CHEMIN"/><xsl:value-of select="DA_FICHIER"/></fichier>
	  		<type><xsl:value-of select="t_doc_acc_val/t_val/t_val/VALEUR"/></type>
	  	</piece_jointe>
	  	</xsl:for-each>
	  </pieces_jointes>

	  <festivals>
	  	<xsl:for-each select="t_doc_fest/FEST">
	  		<festival>
	  			<id_fest><xsl:value-of select="FEST_ID_GEN"/></id_fest>
	  			<id_lang><xsl:value-of select="ID_LANG"/></id_lang>
	  			<id_section><xsl:value-of select="ID_FEST"/></id_section>
	  			<libelle><xsl:value-of select="FEST_LIBELLE"/></libelle>
	  			<annee><xsl:value-of select="substring-before(FEST_ANNEE,'-')"/></annee>
	  			<description><xsl:value-of select="FEST_DESC"/></description>
	  			<prix>
	  				<xsl:for-each select="DOC_FEST_VAL">
	  					<prix><xsl:value-of select="VALEUR"/></prix>
	  				</xsl:for-each>
	  			</prix>
	  			<lien>service.php?urlaction=detail&amp;amp;entite=festival&amp;amp;id=<xsl:value-of select="FEST_ID_GEN"/></lien>	  			
	  		</festival>
	  	</xsl:for-each>
	  </festivals>
    </doc>
</data>


</xsl:template>


<xsl:template name="internal-quote-replace">
	  <xsl:param name="stream" />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>


</xsl:stylesheet>
