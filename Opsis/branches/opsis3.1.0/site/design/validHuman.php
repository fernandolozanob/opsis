<?php
	
	error_reporting(0);

    require_once("conf/conf.inc.php");
    require_once(libDir."session.php");
    require_once(libDir."class_formSaisie.php");
	
	global $db ; 
	
	// génération des flags de changement de langue dans la variable $langue_menu pour insertion dans l'entete un peu plus bas
	$res = $db->Execute("select ID_LANG,LANG from t_lang"); 
	$langue_menu = "" ; 
	while($row = $res->FetchRow()){
		if($row['ID_LANG'] == $_SESSION['langue']){
			$current_langue = $row['LANG'];
		}
		$langue_menu.="<a href=\"".$_SERVER['SCRIPT_NAME']."?langue=".$row["ID_LANG"]."\" ><img class='flag_chooser ".(($_SESSION['langue'] == $row['ID_LANG'])?'current':'')."' border='0' alt='".$row["LANG"]."'  title='".$row["LANG"]."' src='".designUrl."images/flag_".$row["ID_LANG"].".png'/></a>";
		
	}
	$res->Close();	
	echo '<div id="h_valid_bg" class="gate_bg">';
	echo '<div id="h_valid_popin" class="gate_popin">';
	echo '<div class="gate_popin_header">';
		echo '<span class="gate_popin_logo">';
		// echo '<img alt="logo opsomai" id="logo_opsomai" src="design/images/logo_opso_alpha.png"  border="0" style="height:58px; border:none;"/>' ; 
		echo '<img alt="logo opsomai" id="logo_opsomai" src="design/images/logo_opso_alpha_2.png"  border="0" style="height:58px; border:none;"/>';
		echo '<img alt="logo opsomai" id="logo_opsomai_small" src="design/images/miniopso_alpha_center.png" border="0" style="height:67px; border:none;" />';
		echo '</span>';
		echo '<span class="gate_popin_title">'.kGatePopinTitle.'</span>';
		echo '<div class="gate_popin_lang">'.$langue_menu.'</div>';
	echo '</div>';
	if(isset($_POST['cap_valid_human'])){ 
		echo '<div class="msg err">'.kInvalidCaptcha.'</div>';
	}
	echo '<div class="msg">'.kValidHumanCaptchaMsg.'</div>';
	
	// Ajout possibilité de rediriger vers la page cible initiale après validation captcha
	if(isset($_POST['refer']) && !empty($_POST['refer'])){
		$refer_url = $_POST['refer'];
	}else if(isset($_GET['urlaction'])){
		$refer_url = "http".(isset($_SERVER['HTTPS']) ? 's' : '')."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	}else{
		$refer_url = "" ; 
	}
	$hForm = new FormSaisie();
	$hForm->entity = 'USAGER';
	$hForm->beginForm(array('NAME'=>'valid_human','ID'=>'h_valid_form','ACTION'=>'index.php'));
	$hForm->displayElt(array('TYPE'=>'hidden','ID'=>'refer','NAME'=>'refer','VALUE'=>$refer_url));
	$hForm->addCaptcha(null,null,array('CHFIELDS'=>'cap_valid_human'));
	$hForm->displayElt(array('TYPE'=>'BUTTONSPAN','BUTTON_TYPE'=>'submit','CLASS'=>'std_button_style','VALUE'=>'kValider'));
	$hForm->endForm();
		
	echo '</div>';
	echo '</div>';

?>