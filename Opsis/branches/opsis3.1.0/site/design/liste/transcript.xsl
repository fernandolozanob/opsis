<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
	<xsl:output 
			method="xml" 
			encoding="utf-8" 
			omit-xml-declaration="yes"
			indent="yes"
	/> 

	<xsl:template match='/AudioDoc'>
		<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
		<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
		<xsl:variable name="withaccent" select="'áàâäéèêëíìîïóòôöúùûü'" />
		<xsl:variable name="unaccent" select="'aaaaeeeeiiiioooouuuu'" />

		<div id="speakers">
			<b>Locuteurs :</b><br/>
			<xsl:for-each select="SpeakerList/Speaker">
				<xsl:variable name="spkname">
					<xsl:choose>
						<xsl:when test="@spkname!=''">
							<xsl:value-of select="@spkname"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@spkid"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<img border="0" alt="Modifier" class="editSpkButton" src="design/images/button_modif.gif" style="float:right" onclick="toggleEditSpeakerName('{@spkid}')" />
				<p spkid="{@spkid}"><xsl:value-of select="$spkname"/></p>
				<input spkid="{@spkid}" type="textbox" value="{$spkname}" />
			</xsl:for-each>
		</div>
		
		<div id="transcript">
			<xsl:for-each select="SegmentList/SpeechSegment">
				<div stime="{@stime}" etime="{@etime}" spkid="{@spkid}" class="SpeechSegment">
					<xsl:for-each select="Word">
						<xsl:variable name="stime"><xsl:value-of select="number(@stime)"/></xsl:variable>
						<xsl:variable name="dur"><xsl:value-of select="number(@dur)"/></xsl:variable>
						<xsl:variable name="etime"><xsl:value-of select="$stime + $dur"/></xsl:variable>
						
						<span stime="{format-number($stime, '0.##')}" etime="{format-number($etime, '0.##')}" title="{translate(translate(normalize-space(.), $uppercase, $smallcase), $withaccent, $unaccent)}">
							<xsl:value-of select="."/>
						</span>
					</xsl:for-each>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>