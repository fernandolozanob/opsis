<?xml version="1.0" encoding="utf-8"?>
<!-- lexListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="id_doc"/>
<xsl:param name="id_pers_src" />
<xsl:param name="id_type_desc"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre" />
<xsl:param name="tri" />
<xsl:param name="gestPers"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />

<xsl:template match='/select'>

<form name="documentSelection" method="post" action="{$scripturl}?urlaction=lexListe{$urlparams}" class="contentBody">
	<input type="hidden" value="{$page}" name="page" />
	<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
	<input type="hidden" name="tri" value=""  />

	<xsl:call-template name="pager">
		<!--xsl:with-param name="export">export.php?sql=recherche_LEX&amp;type=lex</xsl:with-param>
		<xsl:with-param name="print">print.php?&lt;?= $_SERVER['QUERY_STRING'] ?&gt;</xsl:with-param-->
		<xsl:with-param name="form">documentSelection</xsl:with-param>
		<xsl:with-param name="entity">lexique</xsl:with-param>
	</xsl:call-template>
	<div class="scrollableDiv">
		<table class="tableResults" align="center" border="0" cellspacing="0">
			<tr>
				<td class="resultsHead" width="85">
					<xsl:if test="contains($tri,'lex_id_type_lex')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=lexListe&amp;tri=lex_id_type_lex{$ordre}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></a></td>
				<td class="resultsHead" width="200">
					<xsl:if test="contains($tri,'lex_terme')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>            	
					<a href="{$scripturl}?urlaction=lexListe&amp;tri=lex_terme{$ordre}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}"><xsl:processing-instruction name="php">print kTerme;</xsl:processing-instruction></a></td>
				<td class="resultsHead" width="85">
					<xsl:if test="contains($tri,'lex_id_etat_lex')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=lexListe&amp;tri=lex_id_etat_lex{$ordre}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}"><xsl:processing-instruction name="php">print kStatut;</xsl:processing-instruction></a></td>
				<td class="resultsHead" >
					<xsl:if test="contains($tri,'lex_syn')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>            	
					<a href="{$scripturl}?urlaction=lexListe&amp;tri=lex_syn{$ordre}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}"><xsl:processing-instruction name="php">print kSynonyme;</xsl:processing-instruction></a></td>
				<td class="resultsHead" width="200">
					<xsl:if test="contains($tri,'lex_gen')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>            	            
					<a href="{$scripturl}?urlaction=lexListe&amp;tri=lex_gen{$ordre}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}"><xsl:processing-instruction name="php">print kParent;</xsl:processing-instruction></a></td>
				<td class="resultsHead" width="65">
					<xsl:if test="contains($tri,'nb_doc')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>            	            
					<a href="{$scripturl}?urlaction=lexListe&amp;tri=nb_doc{$ordre}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}"><xsl:processing-instruction name="php">print kLexiqueNbDocsLies;</xsl:processing-instruction></a></td>
				<td class="resultsHead" width="70">
					<xsl:if test="contains($tri,'nb_pers')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>            	            
					<a href="{$scripturl}?urlaction=lexListe&amp;tri=nb_pers{$ordre}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}"><xsl:processing-instruction name="php">print kLexiqueNbPersLies;</xsl:processing-instruction></a></td>
				<td class="resultsHead" width="10">&#160;</td>
			</tr>
			<xsl:for-each select="t_lex">
				<xsl:variable name="id_lex"><xsl:value-of select="ID_LEX"/></xsl:variable>
				<xsl:variable name="id_pers"><xsl:value-of select="ID_PERS"/></xsl:variable>
				<xsl:variable name="j"><xsl:number/></xsl:variable>


				<tr class="{concat('resultsCorps',$j mod 2)}">
					<td class="resultsCorps"><xsl:value-of select="LEX_ID_TYPE_LEX"/></td>
					<td class="resultsCorps"><a href="{$scripturl}?urlaction=lexSaisie&amp;id_lex={$id_lex}&amp;id_doc={$id_doc}&amp;id_type_desc={$id_type_desc}&amp;id_pers_src={$id_pers_src}" onmouseover_="showInfos('lex','{$id_lex}','lexInfos')"><xsl:value-of select="LEX_TERME"/></a>
						<xsl:if test="LEX_COOR!='&#160;' and normalize-space(LEX_COOR)!=''">
							<img src='design/images/boussole.png' height="20" title='GPS' width='20' border='0' style="vertical-align: text-bottom;"/>
						</xsl:if>
					</td>
					<td class="resultsCorps">
						<xsl:choose>
							<xsl:when test="LEX_ID_ETAT_LEX=2">&lt;?=kValide?&gt;</xsl:when>
							<xsl:when test="LEX_ID_ETAT_LEX=1">&lt;?=kCandidat?&gt;</xsl:when>
						</xsl:choose>&#160;
					</td>
					<td class="resultsCorps"><xsl:value-of select="LEX_SYN"/></td>
					<td class="resultsCorps"><xsl:value-of select="LEX_GEN"/></td>
					<td class="resultsCorps"><xsl:value-of select="NB_DOC"/></td>
					<td class="resultsCorps"><xsl:value-of select="NB_PERS"/></td>
					<td class="resultsCorps">
						<a href="{$scripturl}?urlaction=lexSaisie&amp;id_lex={$id_lex}">
							<img src="design/images/button_modif.gif" alt="&lt;?php echo kModifier ?&gt;" />
						</a>
					</td>
				</tr>
			</xsl:for-each>
		 </table>
     </div>

     </form>
     <!--div id="infos"></div-->
</xsl:template>
</xsl:stylesheet>
