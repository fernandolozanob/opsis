<?xml version="1.0" encoding="utf-8"?>
<!-- Default liste -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->

<xsl:param name="scripturl"/>
<xsl:param name="ordre" />
<xsl:param name="tri"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />
<xsl:param name="xmlfile" />

<xsl:template match='/select'>

	<xsl:variable name="xmllist" select="document($xmlfile)"/>
	<div class="contentBody">
		<form name="documentSelection" method="post" action="{$pageurl}">
			<input type="hidden" name="page" value="{$page}"/>
			<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
			<input type="hidden" name="tri" value=""  />

			<xsl:call-template name="pager">
				<xsl:with-param name="export" select="$xmllist/list/export"/>
				<xsl:with-param name="print">print.php?&lt;?= $_SERVER['QUERY_STRING'] ?&gt;</xsl:with-param>
				<xsl:with-param name="form">documentSelection</xsl:with-param>
				<xsl:with-param name="entity" select="$xmllist/list/entity"/>
			</xsl:call-template>

			<div class="scrollableDiv">
				<xsl:call-template name="displayListe">
					<xsl:with-param name="xmllist" select="$xmllist"/>
				</xsl:call-template>
			</div>

		</form>
	</div>

</xsl:template>

</xsl:stylesheet>
