<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes" indent="yes" />

	<xsl:template match='select'>
		<table>
			<tr>
				<th>Fichier</th>
				<th>Date</th>
				<th>Chemin fichier</th>
			</tr>
			<xsl:for-each select="rapport">
				<tr>
					<td><xsl:value-of select="nom_fichier" /></td>
					<td><xsl:value-of select="mois" /> - <xsl:value-of select="annee" /></td>
					<td><xsl:value-of select="chemin_fichier" /></td>
				</tr>
			</xsl:for-each>
		</table>

	</xsl:template>
</xsl:stylesheet>