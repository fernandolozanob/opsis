<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../fonctions.xsl"/>
	<xsl:output 
			method="xml" 
			encoding="utf-8" 
			omit-xml-declaration="yes"
			indent="yes"
	/>
	
	<xsl:template match='EXPORT_OPSIS'>
		<xsl:for-each select="t_panier">
			<xsl:value-of select="PAN_TITRE" />
		</xsl:for-each>
	</xsl:template>
    
</xsl:stylesheet>