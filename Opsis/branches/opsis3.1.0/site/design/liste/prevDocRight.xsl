<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../liste.xsl"/>
	<xsl:include href="../fonctions.xsl"/>
	<xsl:output
			method="xml"
			encoding="utf-8"
			omit-xml-declaration="yes"
			indent="yes"
	/>

	<xsl:param name="rang"/>
	<xsl:param name="id_panier"/>
	<xsl:param name="id_ligne_panier"/>
	<xsl:template match='/t_doc'>
		<xsl:call-template name="rechercheRightPreview">
			<xsl:with-param name="doc" select="."/>
			<xsl:with-param name="rang" select="$rang"/>
			<xsl:with-param name="id_panier" select="$id_panier"/>
			<xsl:with-param name="id_ligne_panier" select="$id_ligne_panier"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
