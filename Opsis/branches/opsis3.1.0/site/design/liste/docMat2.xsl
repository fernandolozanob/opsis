<?xml version="1.0" encoding="utf-8"?>
<!-- docMat2.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="id_doc"/>
<xsl:param name="scripturl" />
    <xsl:template match='/select'>   
        <table width="100%"  border="0" cellspacing="0" class="tableResults">
            <tr>
                <td class="resultsHead"><a href="{$scripturl}?urlaction=docMat&amp;tri=id_mat&amp;id_doc={$id_doc}"><xsl:processing-instruction name="php">print kReferenceMateriel;</xsl:processing-instruction></a></td>
                <td class="resultsHead"><a href="{$scripturl}?urlaction=docMat&amp;tri=mat_format&amp;id_doc={$id_doc}"><xsl:processing-instruction name="php">print kFormatMateriel;</xsl:processing-instruction></a></td>
                <td class="resultsHead"><a href="{$scripturl}?urlaction=docMat&amp;tri=dmat_tcin&amp;id_doc={$id_doc}"><xsl:processing-instruction name="php">print kTCin;</xsl:processing-instruction></a></td>
                <td class="resultsHead"><a href="{$scripturl}?urlaction=docMat&amp;tri=dmat_tcout&amp;id_doc={$id_doc}"><xsl:processing-instruction name="php">print kTCout;</xsl:processing-instruction></a></td>
             </tr>
            <xsl:for-each select="dmat">
                <xsl:variable name="id_mat"><xsl:value-of select="id_mat"/></xsl:variable>
                <xsl:variable name="mat_format"><xsl:value-of select="MAT/t_mat/MAT_FORMAT"/></xsl:variable>
                <xsl:variable name="dmat_tcin"><xsl:value-of select="dmat_tcin"/></xsl:variable>
                <xsl:variable name="dmat_tcout"><xsl:value-of select="dmat_tcout"/></xsl:variable>
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">
                	<input type="hidden" name="id_doc" value="{$id_doc}"/>
                    <td class="resultsCorps"><xsl:value-of select="id_mat"/></td>
                    <td class="resultsCorps"><xsl:value-of select="MAT/t_mat/MAT_FORMAT" /></td>
                    <td class="resultsCorps"><xsl:value-of select="dmat_tcin" /></td>
                    <td class="resultsCorps"><xsl:value-of select="dmat_tcout"/></td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>
