<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />

<xsl:template match='/select'>

<data>
	<nb_pages><xsl:value-of select="$nb_pages"/></nb_pages>
	<nbRows><xsl:value-of select="$nb_rows"/></nbRows>
	<recherche><xsl:value-of select="$votre_recherche"/></recherche>

<rows>
   <xsl:for-each select="personne">
  
                <xsl:variable name="id"><xsl:value-of select="ID_PERS"/></xsl:variable>
                <xsl:variable name="im"><xsl:value-of select="concat('storyboard/',IMAGEUR,'/',IM_CHEMIN)"/></xsl:variable>
               
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <xsl:variable name="rang" select="number(number($offset)+$j)"/>
    <row rang="{$rang}">
      <id_pers><xsl:value-of select="$id"/></id_pers>
      <id_lang><xsl:value-of select="ID_LANG"/></id_lang>
      
      <nom><xsl:if test="PERS_PARTICULE!='&#160;'"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
      	<xsl:value-of select="PERS_NOM"/></nom>
      <prenom><xsl:value-of select="PERS_PRENOM"/></prenom>
      <pays><xsl:value-of select="PERS_ORGANISATION"/></pays>
      <lien>service.php?urlaction=detail&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="$id"/></lien>
    </row>
    </xsl:for-each>
	</rows>
</data>


</xsl:template>


<xsl:template name="internal-quote-replace">
	  <xsl:param name="stream" />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>


</xsl:stylesheet>
