<?xml version="1.0" encoding="utf-8"?>
<!-- minipanier : utilisé pour afficher le contenu d'un folder en format très réduit (ex: frame de gauche, etc. ) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<xsl:param name="currentIdDoc"/>

<xsl:param name="id_doc"/>
<xsl:param name="id_mat"/>
<xsl:param name="scripturl"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="storyboardChemin" />
<xsl:param name="etat"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="imgurl" />
<xsl:param name="id_lang" />



<xsl:template match='/select'>
    <form name='form2' id='form2' action='blank.php?urlaction=processStoryboard&amp;id={t_imageur/ID_IMAGEUR}' method='POST' target='iframeSauve' >
            <input name="page" type="hidden" value="" />
			<input name="vignette" type="hidden" value="{t_doc/DOC_ID_IMAGE}" /> <!-- par deft -->
			<input name="id_doc" type="hidden" value="{t_doc/ID_DOC}" />
			<input id="id_imageur" name="id" type="hidden" value="{t_imageur/ID_IMAGEUR}" />
			<input id="imageur" name="imageur" type="hidden" value="{t_imageur/IMAGEUR}" />
			<input id="id_mat" name="id_mat" type="hidden" value="{concat(t_imageur/ID_MAT,t_mat/ID_MAT,t_doc/t_doc_mat/DOC_MAT/ID_MAT)}" />
			<input name="commande" type="hidden" value="saveStoryboard"	/>
   
   
    <table width="100%" align="center" border="0" cellspacing="0" style='font-size:10px; border-collapse : collapse;'>
            <tr style='background-color:#e8e8e8; font-size : 12px; border:1px solid #3b3b3b;'>
               <td align="left" width="25%">
					<xsl:value-of select="$nb_rows" />&#160;<xsl:processing-instruction name="php">print kImages;</xsl:processing-instruction>
                </td>
 				<!-- Changement de page, ATTENTION, car fait en repostant le formulaire (pas pas href !) -->
                <td align='center' width='25%'>
		 			<xsl:if test="$nb_pages>1">
						<xsl:if test="$page>1"><a href="javascript:void(null)" onclick='javascript:myPanel.page={$page - 1};myPanel.refreshContent();'>&lt;</a>&#160;</xsl:if>
						<input type='text' style='padding:0px;margin:0px;width:25px;' name='rang' value='{$page}' size='3' class="resultsInput" onchange='javascript:myPanel.page=eval(this.value);myPanel.refreshContent();' />
						<input type='submit' style='width:0px;height:0px;display:none;' />
						<xsl:value-of select="concat(' / ',$nb_pages)"/> 
						<xsl:if test="$page &lt; $nb_pages">&#160;<a href="javascript:void(null)" onclick='javascript:myPanel.page={$page + 1};myPanel.refreshContent();'>&gt;</a></xsl:if>
					</xsl:if>
                </td>
		<td align="right" >


        <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
        <select name='nbLignes' class='val_champs_form' style='padding:0px;margin:0px;' onchange='javascript:myPanel.nbLignes=this.value;myPanel.refreshContent()'>
            <option value='9'><xsl:if test="$nbLigne=9"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>9</option>
            <option value='12'><xsl:if test="$nbLigne=12"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>12</option>
            <option value='24'><xsl:if test="$nbLigne=24"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>24</option>
            <option value='45'><xsl:if test="$nbLigne=45"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>45</option>
            <option value='all'><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
        </select>
			</td>
		</tr>
		</table>
		
		
		<!-- fin sélection / début images -->
		<hr width='100%'/>

	<div id='planche' style='position:relative;overflow:auto;width:100%;'>
	
		
        <xsl:for-each select="t_image">
			<xsl:variable name="chemin_story">
				&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,storyboardChemin); ?&gt;
			</xsl:variable>

            <xsl:variable name="imgPath2">
            	<xsl:call-template name="internal-quote-replace" >
            		<xsl:with-param name="stream" select="concat($chemin_story,IM_CHEMIN,'/',IM_FICHIER)" />
            	</xsl:call-template>
            </xsl:variable>
            <xsl:variable name="imgPath"><xsl:value-of  select="concat($chemin_story,IM_CHEMIN,'/',IM_FICHIER)" />
            </xsl:variable>
            <xsl:variable name="j"><xsl:number/></xsl:variable>
            <xsl:variable name="className"> <!-- Test pour voir si cette image est la vignette. Uniquement pour DOC -->
            	<xsl:choose>
            		<xsl:when test='SELECTED'>imgSelected</xsl:when>
            		<xsl:otherwise>imgNormal</xsl:otherwise>
            	</xsl:choose>
            </xsl:variable>
			<div id='row$' style='position:relative;float:left;display:block;border:1px solid black;text-align:center;padding-bottom:5px;margin:2px' 
				class='{$className}'>
				<!-- Checkbox de sélection pour le delete -->
				<!-- <label style='width:130px;font-family:"Trebuchet MS";font-weight:normal;font-size:12px;color:#333333;'>
				<input type="checkbox" name="checked[{ID_IMAGE}]" style='width:12px;' />
				#<xsl:value-of select="ID_IMAGE" /></label>-->
				
				<input id='action$' type='hidden' name='ligne_action[]' value='edit' />

				<span style='font-size:12px;width:70px;text-align:center;font-weight:bold;background-color:#000000;color:#FFFFFF;
							font-family:arial' ><xsl:value-of select="IM_TC"/></span>
				<!-- Image avec zoom sur le click -->
				<br/>
	
				<img src="makeVignette.php?image={$imgPath}&amp;amp;w=130" width="130" id='img$'  style="cursor:pointer" onclick="if(!myVideo) document.getElementById('poster').onclick.call();myPanel.positionCursor('{IM_TC}')" />
				<!-- Formulaire pour cette image -->
           </div>


        </xsl:for-each>
	
</div>
    


</form>
	
	
</xsl:template>

	<xsl:template name="internal-quote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">'</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
		name="internal-quote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
	</xsl:template>

<xsl:template name="internal-dbquote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">"</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>&quot;<xsl:call-template
		name="internal-dbquote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
</xsl:template>





</xsl:stylesheet>