<?xml version="1.0" encoding="utf-8"?>
<!-- paletteSimple.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="scripturl"/>
<xsl:param name="affich_nb"/>
<xsl:param name="titre_index"/>
<xsl:param name="btn_edit"/>
<xsl:param name="w"/>
<xsl:param name="h"/>
<xsl:param name="jsFunction" />
<xsl:param name="id_input_result" />
<xsl:param name="get_params" />
<xsl:param name="page" />
<xsl:param name="nb_pages" />
<xsl:param name="nb_rows" />



<xsl:template match='/select'>
	<script type="text/javascript" src="&lt;?= libUrl ?&gt;/webComponents/opsis/cherche.js">&amp;nbsp;</script>
	<script type="text/javascript">
	if(top._mouseUp){ 
		document.addEventListener('mouseup',top._mouseUp,false);
	}  
	</script>

	<div id="blocDtree">
       <table style="width:100%" border="0" cellspacing="0" cellpadding="0" class="tableResults">
            <tr>

	            <td class="resultsHead"><xsl:value-of select="$titre_index" /></td>
				<xsl:if test="$affich_nb">
					<td class="resultsHead" >Nb</td>
				</xsl:if>
			</tr>
            <xsl:for-each select="val">
                <xsl:variable name="id"><xsl:call-template name="internal-quote-replace"><xsl:with-param	name="stream" select="ID_VAL"/></xsl:call-template></xsl:variable>
                <xsl:variable name="val"><xsl:call-template name="internal-quote-replace">
                	<xsl:with-param	name="stream" select="VALEUR"/>
                	</xsl:call-template></xsl:variable>

                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">

                    <td class="resultsCorps" >
                    <a href="javascript:void(0)" onclick="javascript:window.parent.{$jsFunction}('{$id_input_result}','{$val}','{$id}')"><xsl:value-of select="VALEUR"/></a>
                    </td>
					<xsl:if test="$affich_nb">
						<td class="resultsCorps" >
							<xsl:value-of select="NB"/>
						</td>
					</xsl:if>
                </tr>
            </xsl:for-each>


        </table>
	   
		<table width="100%" border="0">
		<tr>
		<!--td align="left" width="68" nowrap="1" style='font-size:9px;'>
			<xsl:if test="$nb_rows!=''">
				<xsl:value-of select="$nb_rows"/>&#160;&lt;?=kResultats?&gt;
			</xsl:if>&#160;
		</td-->
		<td align="center">
			<xsl:if test="$nb_pages!=1">
				<xsl:if test="$page &gt; 1"><a href="{$scripturl}?urlaction=chercheIndexSolr&amp;{$get_params}&amp;page={$page - 1}">&lt;</a></xsl:if>
				<input type='text' id='rang' name='rang' style='text-align:center;width:25px;font-size:9px;border:1px solid #467A94;padding:0px;' value='{$page}' size='3' onKeyPress="javascript:submitIfReturn(event)"  onchange='javascript:document.form1.page.value=eval(this.value);document.form1.submit();' />
				<xsl:if test="$page &lt; $nb_pages or $nb_pages=0">
					<a href="{$scripturl}?urlaction=chercheIndexSolr&amp;{$get_params}&amp;page={$page + 1}">&gt;</a>
				</xsl:if>
			</xsl:if>&#160;
		</td>
		</tr>
		</table>
	</div>

    </xsl:template>


<xsl:template name="internal-quote-replace">
		<xsl:param name='stream' />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>
</xsl:stylesheet>
