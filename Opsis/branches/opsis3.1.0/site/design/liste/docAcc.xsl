<?xml version="1.0" encoding="utf-8"?><!-- XS : 28/10/2005 : rechercheListe.xsl --><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output         method="xml"         encoding="utf-8" 
        omit-xml-declaration="yes"        indent="yes"/> <!-- recuperation de parametres PHP --><xsl:param name="affichage"/>
<xsl:param name="id_doc"/>
<xsl:param name="id_lang"/>
<xsl:param name="chemin"/><xsl:param name="scripturl"/><xsl:param name="tri"/>    <xsl:template match='EXPORT_OPSIS'>            <table width="50%"  border="0" cellspacing="0" class="tableResults">            <tr>                <td class="resultsHead"><a href="{$scripturl}?urlaction=docAccSaisie&amp;tri=da_titre&amp;id_doc={$id_doc}"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></a></td>
                <td class="resultsHead"><a href="{$scripturl}?urlaction=docAccSaisie&amp;tri=da_fichier&amp;id_doc={$id_doc}"><xsl:processing-instruction name="php">print kSource;</xsl:processing-instruction></a></td>
                <td class="resultsHead"><a href="{$scripturl}?urlaction=docAccSaisie&amp;tri=id_lang&amp;id_doc={$id_doc}"><xsl:processing-instruction name="php">print kLangue;</xsl:processing-instruction></a></td>                                 <xsl:if test="$affichage=1">                    <td class="resultsHead" width="20px">&#160;</td>
                </xsl:if>            </tr>            <xsl:for-each select="t_doc_acc">                <xsl:variable name="j"><xsl:number/></xsl:variable>                <tr class="{concat('altern',$j mod 2)}">                    <td class="resultsCorps">
                        <xsl:choose>
                            <xsl:when test="$affichage=1">
                                <input name="id_doc_acc[]" type="hidden" id="id_doc_acc{$j}" value="{ID_DOC_ACC}" /><input name="da_titre[]" type="text" id="da_titre{$j}" size="30" value="{DA_TITRE}" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="DA_TITRE"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                    <td class="resultsCorps">                    <xsl:choose>                    	<xsl:when test="substring(DA_FICHIER,1,7)='http://' or substring(DA_FICHIER,1,8)='https://'" > 							<a href="{DA_FICHIER}" target="_blank"><xsl:value-of select="DA_FICHIER"/></a>                  		                    	</xsl:when>                    	<xsl:otherwise>                    		<a href="{concat($chemin,DA_CHEMIN,DA_FICHIER)}" target="_blank"><xsl:value-of select="DA_FICHIER"/></a>                    	</xsl:otherwise>                    </xsl:choose>	                   </td>                    <td class="resultsCorps"><xsl:value-of select="ID_LANG"/></td>                                        <xsl:if test="$affichage=1">                        <td class="resultsCorps"><a href="javascript:removeLine({$j})"><img src="design/images/button_drop.gif" width="11" height="14" border="0" title="&lt;?php echo kSupprimer; ?&gt;" alt="&lt;?php echo kSupprimer; ?&gt;" /></a></td>
                    </xsl:if>                </tr>            </xsl:for-each>        <!--        <tr>        	<td colspan="3">        		<input type='submit' name='btnSave' value='XXX Save' />        	</td>        </tr>        -->        </table>        </xsl:template></xsl:stylesheet>