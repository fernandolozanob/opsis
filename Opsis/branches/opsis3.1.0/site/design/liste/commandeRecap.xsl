<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="../fonctions.xsl"/>
<xsl:include href="commandeRecap2.xsl"/>
<xsl:include href="commandeRecap4.xsl"/>
<xsl:include href="commandeRecap5.xsl"/>

<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 


<xsl:param name="playerVideo"/>
<xsl:param name="imgurl"/>
<xsl:param name="scripturl" />
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="cartList"/>
<xsl:param name="nbLigne" />
<xsl:param name="page" />
<xsl:param name="pager_link" />
<xsl:param name="titre" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />


    <xsl:template match='/select'>
		<xsl:choose>
			<xsl:when test="t_panier/PAN_ID_TYPE_COMMANDE='2'"><xsl:call-template name="commandeRecap2"/></xsl:when>
			<xsl:when test="t_panier/PAN_ID_TYPE_COMMANDE='4'"><xsl:call-template name="commandeRecap4"/></xsl:when>
			<xsl:when test="t_panier/PAN_ID_TYPE_COMMANDE='5'"><xsl:call-template name="commandeRecap5"/></xsl:when>
			<xsl:otherwise><xsl:call-template name="commandeRecap2"/></xsl:otherwise>
		</xsl:choose>
    </xsl:template>
    
    
</xsl:stylesheet>

