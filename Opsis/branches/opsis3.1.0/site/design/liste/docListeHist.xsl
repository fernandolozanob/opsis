<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>

<xsl:template match='/select'>

<xsl:variable name="xmllist" select="document($xmlfile)"/>


<div id="resultats"  align="center">
	<br/>
	<xsl:call-template name="displayListe">
		<xsl:with-param name="xmllist" select="$xmllist"/>
	</xsl:call-template>
	<div class="pusher">&#160;</div>
	
	<br/><h3><a href="index.php?urlaction=historique"><xsl:text>Historique de recherche</xsl:text></a></h3>
</div>


</xsl:template>


</xsl:stylesheet>
