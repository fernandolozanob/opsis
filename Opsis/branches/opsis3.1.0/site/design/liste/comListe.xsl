<?xml version="1.0" encoding="utf-8"?>
<!-- comListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />


    <xsl:template match='/select'>

	<xsl:variable name="xmllist" select="document($xmlfile)"/>

		<script type="text/javascript">
		function togglePanier(id){
			if(document.getElementById("img_"+id)) {
				image=document.getElementById("img_"+id).src;
				if(image.indexOf("plus.gif")>0){
					document.getElementById("img_"+id).src="design/images/moins.gif";
					return !sendData('GET','blank.php','urlaction=processPanier&amp;commande=view&amp;id_panier='+id+'&amp;xsl=comListePanier','dspPanier');		
				}else{
					document.getElementById("img_"+id).src="design/images/plus.gif";
					if(document.getElementById("panier_"+id)) document.getElementById("panier_"+id).innerHTML="";
				}
			}
		}
		function dspPanier(str) {
			//alert(str);
			var exp=new RegExp("&lt;table[^&gt;]* id_panier=\"([^&lt;]+)\"&gt;");
			tab=str.match(exp);
			id=tab[1];
			
			if(document.getElementById("img_"+id)) {
				if(!document.getElementById("panier_"+id)) {
				// creation div
					tr=document.getElementById("img_"+id).parentNode.parentNode;
					var row = document.createElement("tr");
					var cell = document.createElement("td");
					cell.setAttribute("colspan","10");
					myDiv = document.createElement("div");
					myDiv.id="panier_"+id;
					cell.appendChild(myDiv);
					row.appendChild(cell);
					tbody= tr.parentNode;
					if(tr.nextSibling) tbody.insertBefore(row,tr.nextSibling);
					else tbody.appendChild(row);
				}
				document.getElementById("panier_"+id).innerHTML=str;
				document.getElementById("img_"+id).src="design/images/moins.gif";
			}
		}

		</script>

		<div class="contentBody">
			<form name="documentSelection" method="post" >
				<input type="hidden" value="{$page}" name="page" />
				<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
				<input type="hidden" value="" name="style" />
				<input type="hidden" name="tri" value="{$tri}"  />

				<xsl:call-template name="pager">
					<xsl:with-param name="export" select="$xmllist/list/export"/>
					<!--xsl:with-param name="print">print.php?&lt;?= $_SERVER['QUERY_STRING'] ?&gt;</xsl:with-param-->
					<xsl:with-param name="panier">1</xsl:with-param>
					<xsl:with-param name="form">documentSelection</xsl:with-param>
				</xsl:call-template>

				<div class="scrollableDiv">
					<xsl:call-template name="displayListe">
						<xsl:with-param name="xmllist" select="$xmllist"/>
					</xsl:call-template>
				</div>
			 </form>
		</div>
    </xsl:template>
</xsl:stylesheet>
