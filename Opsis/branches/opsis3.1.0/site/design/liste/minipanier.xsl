<?xml version="1.0" encoding="utf-8"?>
<!-- minipanier : utilisé pour afficher le contenu d'un folder en format très réduit (ex: frame de gauche, etc. ) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- etat=0,1,2 -->
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="scripturl"/>


<!-- PANIER_COMMANDE : Affichage du panier -->
   <xsl:template match='/select'>
	

<!-- SELECT : Détail de la commande -->

        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
        
            <xsl:for-each select="t_panier_doc">
    			<xsl:variable name="id_panier"><xsl:value-of select="id_panier"/></xsl:variable>
                <xsl:variable name="id_doc"><xsl:value-of select="ID_DOC"/></xsl:variable>
                <xsl:variable name="nbextraits"><xsl:value-of select="extraits"/></xsl:variable>
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="{concat('altern',$j mod 2)}">
                    <td style='font-family:arial;font-size:9px' >
					<a href="index.php?urlaction=doc&amp;id_doc={$id_doc}"><xsl:value-of select="DOC/t_doc/DOC_TITRE"/>
                   <xsl:if test="PDOC_EXTRAIT='1'"><xsl:text>/</xsl:text><xsl:value-of select="PDOC_EXT_TITRE"/></xsl:if></a></td>                
                </tr>
            </xsl:for-each>
        </table>
        
    </xsl:template>
    
    
</xsl:stylesheet>
