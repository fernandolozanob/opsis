<?xml version="1.0" encoding="utf-8"?>
<!-- panierListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="scripturl" />
<xsl:param name="ordre" />
<xsl:param name="tri" />
<xsl:param name="gestPers"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />


<xsl:template match='/select'>

	<xsl:variable name="xmllist" select="document($xmlfile)"/>

<!-- 	<div class="title_bar">
		&lt;?= kMesCommandes ?&gt;
		<div class="toolbar">
			<div onclick="javascript:popupPrint('print.php?&lt;?=$_SERVER['QUERY_STRING'];?&gt;','main')" class=" print tool">
				<span class="tool_hover_text">&lt;?echo kImprimer;?&gt;</span>
			</div>
		</div>
	</div>
 -->
	<div class="contentBody">
		<form name="documentSelection" target="framePanier	" method="post" action="{$scripturl}?urlaction=panierListePerso{$urlparams}">
			<input type="hidden" value="{$page}" name="page" />
			<input type="hidden" name="nbLignes" value="{$nbLigne}" />
			<input type="hidden" value="" name="style" />
			<input type="hidden" name="tri" value="{$tri}" />
			
			<xsl:call-template name="pager">
				<xsl:with-param name="export" select="$xmllist/list/export"/>
				<!--xsl:with-param name="print">print.php?&lt;?= $_SERVER['QUERY_STRING'] ?&gt;</xsl:with-param-->
				<xsl:with-param name="form">documentSelection</xsl:with-param>
			</xsl:call-template>

			<div class="scrollableDiv">
				<xsl:call-template name="displayListe">
					<xsl:with-param name="xmllist" select="$xmllist"/>
				</xsl:call-template>
			</div>
		 </form>
	</div>
    </xsl:template>

</xsl:stylesheet>
