<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />

<xsl:template match='/select'>
	<xsl:element name="out">
	<xsl:for-each select="doc">
		<xsl:element name="doc">
			<xsl:for-each select="child::*">
				<xsl:copy-of select='.'/>
			</xsl:for-each>
			<xsl:variable name="chemin_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,storyboardChemin); ?&gt;</xsl:variable>
			<xsl:variable name="doc_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,kDocumentUrl); ?&gt;</xsl:variable>
			
			<xsl:element name="vignette">
				<xsl:choose>
					<xsl:when test="string-length(IM_FICHIER)>3 and IM_FICHIER != '&amp;#160;'"><xsl:value-of select="concat($chemin_story,IM_CHEMIN,'/',IM_FICHIER)"/></xsl:when>
					<xsl:when test="string-length(DA_FICHIER)>3 and string-length(DA_CHEMIN)>3  and DA_CHEMIN != '&amp;#160;'"><xsl:value-of select="concat($doc_story,DA_CHEMIN,DA_FICHIER)"/></xsl:when>
					<xsl:when test="string-length(DA_FICHIER)>3 and DA_FICHIER != '&amp;#160;'"><xsl:value-of select="concat($doc_story,DA_FICHIER)"/></xsl:when>
				</xsl:choose>
			</xsl:element>
		</xsl:element>
	</xsl:for-each>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>