<?xml version="1.0" encoding="utf-8"?>
<!-- panier.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="id_panier"/>


    <xsl:template match='/select'>
        <table width="95%" align="center" border="0" cellspacing="0" class="tableResults">
            <tr>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'doc_cote')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=doc_cote{$ordre}"><xsl:processing-instruction name="php">print kNumero;</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'fonds')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=fonds{$ordre}"><xsl:processing-instruction name="php">print kFonds;</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'doc_titre')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=doc_titre{$ordre}"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'doc_date_prod')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=doc_date_prod{$ordre}"><xsl:processing-instruction name="php">print kDateProd;</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'pdoc_ext_titre')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=pdoc_ext_titre{$ordre}"><xsl:processing-instruction name="php">print kTitreExtrait;</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'doc_duree')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=doc_duree{$ordre}"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'pdoc_ext_tcin')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=pdoc_ext_tcin{$ordre}"><xsl:processing-instruction name="php">print kTCin;</xsl:processing-instruction></a>
                </td>
                <td class="resultsHead">
                    <xsl:if test="contains($tri,'pdoc_ext_tcout')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
                    <a href="index.php?urlaction=comLivraison&amp;id_panier={$id_panier}&amp;tri=pdoc_ext_tcout{$ordre}"><xsl:processing-instruction name="php">print kTCout;</xsl:processing-instruction></a>
                </td>
                <!--<td class="resultsHead"><a href="comLivraison.php?tri=id_mat"><xsl:processing-instruction name="php">print kRefMat;</xsl:processing-instruction></a></td>-->
            </tr>
            <xsl:for-each select="extrait">
                <xsl:variable name="id_doc"><xsl:value-of select="id_doc"/></xsl:variable>
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <tr class="resultsCorps" class="{concat('altern',$j mod 2)}">
                    <td><xsl:value-of select="doc_cote"/></td>
                    <td><xsl:value-of select="fonds"/></td>
                    <td><a href="javascript:popupDoc('doc.php?id_doc={$id_doc}','main')"><xsl:value-of select="doc_titre"/></a></td>
                    <td><xsl:value-of select="doc_date_prod"/></td>
                    <td><xsl:value-of select="pdoc_ext_titre"/></td>
                    <td><xsl:value-of select="doc_duree"/></td>
                    <td><xsl:value-of select="pdoc_ext_tcin"/></td>
                    <td><xsl:value-of select="pdoc_ext_tcout"/></td>
                    <!--<td><xsl:value-of select="id_mat"/></td>-->
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>
