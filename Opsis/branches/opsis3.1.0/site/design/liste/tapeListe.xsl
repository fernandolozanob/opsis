<?xml version="1.0" encoding="utf-8"?>
<!-- tapeListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="scripturl"/>
<xsl:param name="ordre" />
<xsl:param name="tri"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="pager_link" />
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="nb_rows" />
<xsl:param name="tapeSetList" />


    <xsl:template match='/select'>

<script>

function openSetForm() {
	$j("#setForm").toggle();
}

function updateSet() {
	document.documentSelection.commande.value='UpdateSet';
	document.documentSelection.submit();
}

</script>

<form name="documentSelection" method="post" action="{$scripturl}?urlaction=tapeListe{$urlparams}">
	<input type="hidden" value="{$page}" name="page" />
	<input type="hidden" value="" name="commande" />
	<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
	<input type="hidden" name="tri" value=""  />

	<xsl:call-template name="pager">
		<!--xsl:with-param name="print">print.php?&lt;?= $_SERVER['QUERY_STRING'] ?&gt;</xsl:with-param>
		<xsl:with-param name="export">export.php?type=tape&amp;sql=recherche_TAPE</xsl:with-param-->
		<xsl:with-param name="form">documentSelection</xsl:with-param>
		<xsl:with-param name="entity">tape</xsl:with-param>
	</xsl:call-template>


	<input id='btnBackup' type='button' value="&lt;?php print kModifierJeu ?&gt;" onclick="openSetForm();" />
	<table id="setForm" style="display:none;">
		<!--tr>
			<td colspan="3">
				<xsl:processing-instruction name="php">print kRepartirCartouchesMiroir;</xsl:processing-instruction>
				<input type="checkbox" name="repart_mirror" value="repart_mirror" id="repart_mirror" />
			</td>
		</tr-->
		<tr>
			<td>
				<xsl:processing-instruction name="php">print kChoisirJeu;</xsl:processing-instruction>
			</td>
			<td>
				<select name="id_tape_set">
                    <option value="0">0</option>
					<xsl:value-of select="$tapeSetList" />
				</select>
			</td>
			<td>
				<input type="button" onclick="javascript:updateSet()" value="Ok" />
			</td>
		</tr>
	</table>

        <table width="80%" align="center" border="0" cellspacing="0" class="tableResults">
            <tr>
            	<td class="resultsHead" style="width:10px">
            		<input type="checkbox" name="checkAll" value="all" onclick="selectAll(this.checked)"  />
            	</td>
                <td class="resultsHead">
					<xsl:if test="contains($tri,'id_tape')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=tapeListe&amp;tri=id_tape{$ordre}"><xsl:processing-instruction name="php">print kIdentifiant;</xsl:processing-instruction></a>
				</td>
                <td class="resultsHead">
					<xsl:if test="contains($tri,'id_tape_set')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=tapeListe&amp;tri=id_tape_set{$ordre}"><xsl:processing-instruction name="php">print kJeu;</xsl:processing-instruction></a>
				</td>
                <td class="resultsHead">
					<xsl:if test="contains($tri,'tape_status')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=tapeListe&amp;tri=tape_status{$ordre}"><xsl:processing-instruction name="php">print kStatut;</xsl:processing-instruction></a>
				</td>
				<td class="resultsHead">
					<xsl:if test="contains($tri,'tape_full')"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
					<a href="{$scripturl}?urlaction=tapeListe&amp;tri=tape_status{$ordre}"><xsl:processing-instruction name="php">print kCartouchePleine;</xsl:processing-instruction></a>
				</td>
                <td class="resultsHead">
					<xsl:processing-instruction name="php">print kCapaciteUtilisee;</xsl:processing-instruction>
				</td>
                <td class="resultsHead">
					<xsl:processing-instruction name="php">print kCapaciteRestante;</xsl:processing-instruction>
				</td>
                <td class="resultsHead">
					<xsl:processing-instruction name="php">print kNbFichiers;</xsl:processing-instruction>
				</td>
            </tr>
            <xsl:for-each select="tape">
                <xsl:variable name="j"><xsl:number/></xsl:variable>
                <xsl:variable name="row" select="child::*"/>
                <tr class="{concat('altern',$j mod 2)}">
                	<td class="resultsCorps">
	            		<input type="checkbox" name="checkboxTape[]" value="{$row[name()='ID_TAPE']}"  />
	            	</td>
                    <td class="resultsCorps" width='150'><a href="{$scripturl}?urlaction=tapeSaisie&amp;id_tape={ID_TAPE}"><xsl:value-of select="ID_TAPE"/></a></td>
                    <td class="resultsCorps" width='80'>
						<xsl:value-of select="ID_TAPE_SET"/>
					</td>
                    <td class="resultsCorps" width='80'>
						<xsl:if test="TAPE_STATUS = '1'"><xsl:processing-instruction name="php">print kOnline;</xsl:processing-instruction></xsl:if>
						<xsl:if test="TAPE_STATUS = '0'"><xsl:processing-instruction name="php">print kOffline;</xsl:processing-instruction></xsl:if>
					</td>
					<td class="resultsCorps" width='80'>
						<xsl:value-of select="TAPE_FULL"/>
					</td>
                    <td class="resultsCorps" width='80'><xsl:value-of select="concat(floor(number(TAPE_UTILISE) div 1073741824),' G')"/></td>
                    <td class="resultsCorps" width='80'><xsl:value-of select="concat(floor(number(TAPE_CAPACITE - TAPE_UTILISE) div 1073741824),' G')"/></td>
                    <td class="resultsCorps" width='80'><xsl:value-of select="NB"/></td>
                </tr>
            </xsl:for-each>
        </table>

	 <table width="95%" border="0" class="resultsMenu" align="center">
	    <tr>
	        <td align="center" width="100%">
				<input id='btnListing' type='button' value="&lt;?php print kListageCartouches; ?&gt;" onclick="document.documentSelection.commande.value='listing';document.documentSelection.submit()" />
	        </td>
	    </tr>

	</table>
	     </form>

    </xsl:template>

</xsl:stylesheet>
