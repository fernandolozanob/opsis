<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output
    method="xml"
    encoding="utf-8"
    omit-xml-declaration="yes"
    indent="yes"
    />
	
	<xsl:template match='xml'>
        <div style="padding-left: 50px;text-align:left;">
        <h1>Frontal <xsl:value-of select="date" /></h1>
        <br/>
        <xsl:for-each select="//in">
            <xsl:sort select="module"/>
            <xsl:variable name="module" select="module"/>
            
            <h2><xsl:value-of select="module" /></h2>
            <p style="padding-left: 100px;">
            <label>Nb in&#160;</label><xsl:value-of select="nb_in" />
            <br/><br/>
            <xsl:for-each select="//run[module=$module]">
                <xsl:variable name="engine" select="engine"/>
                
                <label>Engine&#160;</label><xsl:value-of select="engine" /><br/>
                <label>Nb max jobs&#160;</label><xsl:value-of select="nb_max_jobs" /><br/>
                <label>Nb run&#160;</label><xsl:value-of select="count(file[@id!=''])" /><br/>
                <xsl:if test="count(file/@id) &gt; 0">
                    <ul style="padding-left: 150px;">
                        <xsl:for-each select="//run[engine=$engine]/file[@id!='']">
                            <xsl:variable name="id" select="@id"/>
                            <li><xsl:value-of select="concat(substring-after(.,concat($engine,'/')),' (',normalize-space(concat(//run[engine=$engine]/pid[@id=$id],' ',//run[engine=$engine]/status[@id=$id])),')')" /></li>
                        </xsl:for-each>
                    </ul>
                </xsl:if>
                <br/>
            </xsl:for-each>
            </p>
        </xsl:for-each>
        </div>
        

	</xsl:template>
</xsl:stylesheet>