<?php

if (isset($_GET['xsl']) && $_GET['xsl']=='dossier')
{
	//VG 18/08/10 : transfert de l'initialisation des variables ici
	$etat= ($myPanier->t_panier["PAN_ID_ETAT"] == '')?0:$myPanier->t_panier["PAN_ID_ETAT"];
	$id_panier=$myPanier->t_panier["ID_PANIER"];
	$pan_id_gen=$myPanier->t_panier["PAN_ID_GEN"];
	$titre_parent=$myPanier->t_panier["TITRE_PARENT"];
	$id_usager=$myPanier->t_panier["ID_USAGER"];
	$pan_id_type_commande=$myPanier->t_panier["PAN_ID_TYPE_COMMANDE"];
	$pan_public=$myPanier->t_panier["PAN_PUBLIC"];
	
	
	if(empty($titre_parent) && isset($xmlPanier) && $pan_id_gen != '0'){
		$tmp_panier = new SimpleXMLElement($xmlPanier);
		$titre_parent = $tmp_panier->TITRE_PARENT[0];
		unset($tmp_panier);
	}else if ( empty($titre_parent) && $pan_id_gen =='0'){
		$titre_parent=kDossiers;
	}
	

	// MS - 10/04/13 Si création lorsqu'on a aucun panier parent pour puiser les flags,
	// il faut initialiser le panier en panier public pour pouvoir le voir apparaitre dans le panel hierarchique de gauche + dans les palettes
	if(empty($pan_public)){
		$pan_public = 1;
	}

	
	
?>
<script type="text/javascript" src="<?=libUrl?>/webComponents/prototype/dropdown.js"></script>
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/panierEdit.js"></script>
<script type="text/javascript">
var checkedLines;

function savePanierDoc(){
	document.form1.action='blank.php?urlaction=processPanier';
	document.form1.target="iframeSauve";
	document.form1.commande.value="savePanierDoc";
	submitForm();
	//Retour form1 initial
	document.form1.action="<?=$myPage->getName()?>?urlaction=panier<?=$myPage->addUrlParams()?>";
	document.form1.target="";
}


window.onload=function dumb() {
	makeDraggable(document.getElementById('chooser'));
}



</script>

<!-- menu déroulant des actions : DEBUT -->

<form name="form2" id="saisieForm2" method="post" action="index.php?urlaction=panier&xsl=dossier" style="max-width : 99%;">

	<input type='hidden' name='pan_titre' value='' />
	<input type="hidden" value="" name="pan_id_gen" />
	
	<input type="hidden" name="commande" value=""/>
	<input type="hidden" name="ligne" value=""/>
	<input type="hidden" value="<?=$id_panier?>" name="id_panier" />
	<input type="hidden" value="<?=$etat?>" name="pan_id_etat" />
	<input type="hidden" value="<?=$pan_id_type_commande?>" name="pan_id_type_commande" />
	<input type="hidden" value="" name="page" />
	<input type="hidden" value="" name="pan_id_doc_acc"/>
	<input type='hidden' value="" name="pan_dossier"/>
	<input type='hidden' value="<?=$pan_public?>" name="pan_public"/>
	<input type="hidden" value="index.php?urlaction=panier&xsl=dossier" name="refer"/>
	
<div id="dropmenu">
<? if($etat<2 && $pan_public!="1"){ ?>
<dl class="dropdown">
<!--dt id="one-ddheader" onmouseover="ddMenu('one',1,document.getElementById('chooser').style.display)" onmouseout="ddMenu('one',-1,document.getElementById('chooser').style.display)"><?=kOngletSelection?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;'/></dt>
<dd id="one-ddcontent" onmouseover="cancelHide('one')" onmouseout="ddMenu('one',-1,document.getElementById('chooser').style.display)"-->

<dt id="one-ddheader" onclick="ddToggle('one')"><?=kSelection?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;'/></dt>
<dd id="one-ddcontent">

<ul>
	<li><div>
	<img src='design/images/famfamfam_mini_icons/folder_new.gif'/>
	<?=kCreerSelection?>
	<input type='hidden' name='new_dossier_type' value='0'/>
	<input type='hidden' size='2' name='new_dossier_id' value='<?= $pan_id_gen ?>'/>
	<input type='text' name='new_dossier_titre' size="15"/>
	<br/><?=kPanierCreationParent?> <strong><span id='dossier_pere'>
	<?= ($titre_parent!=''?$titre_parent:"...") ?>
	</span></strong>&#160;
	<img src='design/images/famfamfam_mini_icons/box.gif' alt='Choisir' title='Choisir'
	onclick="choose(this.parentNode,'titre_index=Paniers&valeur=&champ=PAN&lex_aff=2&rtn=creaSelection&onlyDossier=0&excludePanier=1&includePublic=2&includeRoot=1&imgNode=folder.gif')"/>
	&#160;&#160;
	<img id='confirm_create' src='design/images/famfamfam_mini_icons/icon_accept.gif' onclick="creaSubmit();ddToggle('one')"/>
	<img id='cancel_create' src='design/images/famfamfam_mini_icons/action_stop.gif' onclick="creaSelection('','',-1);ddToggle('one')" />
	</div>
	</li>

	<li><div onclick="suppression('SUP','index.php?urlaction=panier&xsl=dossier');ddToggle('one')" border="0">
	<img src='design/images/famfamfam_mini_icons/folder_delete.gif'/>
	<?=kSelectionSupprimer?>
	</div>
	</li>

	<!--li><div onclick="choose(this,'titre_index=Paniers&valeur=&champ=PAN&lex_aff=2&rtn=setDossier&includeRoot=1&onlyDossier=1');ddToggle('one')"
	border="0">
	<img src='design/images/famfamfam_mini_icons/arrow_right.gif'/>
	<?=kSelectionDeplacer?>...
	</div>
	</li-->

	<li><div>
	<img src='design/images/famfamfam_mini_icons/icon_user.gif' alt="<?php echo kUtilisateur; ?>" />
	<?=kPanierTransmettre?><br/>
	<input type='hidden' size='2' name='new_user_id'/>
	<strong><span id='new_user_nom'>...</span></strong>&#160;
	<img src='design/images/famfamfam_mini_icons/box.gif' alt='Choisir' title='Choisir'
	onclick="choose(this.parentNode,'titre_index=Utilisateurs&valeur=<?= $id_usager ?>&champ=USR&rtn=selectUser')"/>
	&#160;&#160;
	<img style='display:none;' id='confirm_user_change' src='design/images/famfamfam_mini_icons/icon_accept.gif' onclick="changeUserSubmit();ddToggle('one')" alt="<?php echo kAccepter; ?>" />
	<img style='display:none;' id='cancel_user_change' src='design/images/famfamfam_mini_icons/action_stop.gif' onclick="selectUser('','',-1);ddToggle('one')" alt="Stop" />
	</div>
	</li>

	<li>
	<div onclick="dupliquer();ddToggle('one')" border="0">
	<img src='design/images/famfamfam_mini_icons/copy.gif' alt="<?php echo kCopier; ?>" /><?=kDupliquer?>
	</div>
	</li>

</ul>
</dd>
</dl>

<? }else if($pan_public=="1"){ ?>
			<dl class="dropdown">

				<dt id="one-ddheader" onclick="ddToggle('one')"><?=kDossier?>
				<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;' alt="menu" /></dt>
					<dd id="one-ddcontent">

					<ul>
					<li><div>
					<img src='design/images/famfamfam_mini_icons/folder_new.gif' alt="<?php echo kNouveau; ?>" />
					<?=kCreerDossier?>
					<input type='hidden' name='new_dossier_type' value='0'/>
					<input type='hidden' size='2' name='new_dossier_id' value='<?=$pan_id_gen?>'/>
					<input type='text' name='new_dossier_titre' size="15"/>
					<br/><?=kPanierCreationParent?> <strong><span id='dossier_pere'>
					<?= ($titre_parent!=''?$titre_parent:"...") ?>
					</span></strong>&#160;
				<img src='design/images/famfamfam_mini_icons/box.gif' alt='Choisir' title='Choisir'
					onclick="choose(this.parentNode,'titre_index=Paniers&valeur=&champ=PAN&lex_aff=2&rtn=creaSelection&includePublic=2&includeRoot=1&imgNode=folder.gif&imgNode=folder.gif&excludePanier=1')"/><!--&excludeId=<?=$id_panier?>-->
					&#160;&#160;
					<img id='confirm_create' src='design/images/famfamfam_mini_icons/icon_accept.gif' onclick="creaSubmit();ddToggle('one')" alt="accept" />
						<img id='cancel_create' src='design/images/famfamfam_mini_icons/action_stop.gif' onclick="creaSelection('','',-1);ddToggle('one')" alt="stop" />
						</div>
						</li>

						<li><div onclick="suppression('SUP','index.php?urlaction=panier&xsl=dossier');ddToggle('one')" border="0">
						<img src='design/images/famfamfam_mini_icons/folder_delete.gif' alt="<?php echo kSupprimerDossier; ?>" />
						<?=kSupprimerDossier?>
						</div>
						</li>

								<li>
								<div onclick="dupliquer();ddToggle('one')" border="0">
								<img src='design/images/famfamfam_mini_icons/copy.gif' alt="<?php echo kDupliquer; ?>" /><?=kDupliquer?>
								</div>
								</li>


								</ul>
								</dd>
								</dl>

		<? } ?>


<? if($etat > 1){ ?>
<dl class="dropdown">
<!--dt id="two-ddheader" onmouseover="ddMenu('two',1,document.getElementById('chooser').style.display)"
onmouseout="ddMenu('two',-1,document.getElementById('chooser').style.display)"><?=kPanier?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;'/></dt>
<dd id="two-ddcontent" onmouseover="cancelHide('two')" onmouseout="ddMenu('two',-1,document.getElementById('chooser').style.display)"-->

<dt id="two-ddheader" onclick="ddToggle('two')"><?=kPanier?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;' alt="menu" /></dt>
<dd id="two-ddcontent">

<ul>
<li><div
onclick="dupliquer();ddToggle('two')"
border="0">
<img src='design/images/famfamfam_mini_icons/copy.gif' alt="<?php echo kCopier; ?>" />
<?=kDupliquer?>
</div>
</li>

	<? if($etat == "1"){ ?>
		<li>
		<div onclick="dupliquerTravail();ddToggle('two')" border="0">
		<img src='design/images/famfamfam_mini_icons/copy.gif' alt="<?php echo kTransformerPanierTravail; ?>" /><?=kTransformerPanierTravail?>
		</div>
		</li>
	<? } ?>

	<? if($etat > 1){ ?>
		<li><div onclick="suppression('SUP','index.php?urlaction=comListe');ddToggle('two')">
		<img src='design/images/famfamfam_mini_icons/page_delete.gif' alt="<?php echo kSupprimer; ?>" />
		<?=kSupprimer?>
		</div>
		</li>
	<? } ?>
</ul>
</dd>
</dl>
<? } ?>



<dl class="dropdown">
<!--dt id="three-ddheader" onmouseover="ddMenu('three',1,document.getElementById('chooser').style.display)"
onmouseout="ddMenu('three',-1,document.getElementById('chooser').style.display)"><?=kOngletDocuments?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;'/></dt>
<dd id="three-ddcontent" onmouseover="cancelHide('three')" onmouseout="ddMenu('three',-1,document.getElementById('chooser').style.display)"-->

<dt id="three-ddheader" onclick="ddToggle('three')"><?=kDocuments?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;' alt="<?php echo kDocuments; ?>" /></dt>
<dd id="three-ddcontent">

<ul>
<li><div onclick="selectAll(true);ddToggle('three')">
<img src='design/images/famfamfam_mini_icons/page_tick.gif' alt="<?php echo kSelectionnerTout; ?>" />
<?=kSelectionnerTout?>
</div>
</li>

<li><div onclick="selectAll(false);ddToggle('three')">
<img src='design/images/famfamfam_mini_icons/page_tick.gif' alt="<?php echo kDeselectionnerTout; ?>" />
<?=kDeselectionnerTout?>
</div>
</li>

<li><div onclick="copyLines();ddToggle('three')">
<img src='design/images/famfamfam_mini_icons/copy.gif' alt="<?php echo kCopier; ?>" />
<?=kCopier?>
</div>
</li>

<li><div onclick="pasteLines();ddToggle('three')">
<img src='design/images/famfamfam_mini_icons/action_paste.gif' alt="<?php echo kColler; ?>" />
<?=kColler?>
</div>
</li>

<? if($pan_public!="1"){ ?>
<li><div>
<img src='design/images/famfamfam_mini_icons/page_right.gif' alt="<?php echo kPanierTransfertDoc; ?>" />
<?= kPanierTransfertDoc ?>
<select name='cartList' class='val_champs_form' id='cartList'>
<option value='-2'><?= kNouveauPanier ?></option>
<?
$carts=Panier::getFolders(true);
foreach ($carts as $idx=>$cart) {
	if ($cart['ID_PANIER']!=$current_panier && $cart['PAN_DOSSIER']=="0" && $cart['PAN_PUBLIC']!="1") {
		print "<option value='".$cart['ID_PANIER']."'>".($cart['PAN_ID_ETAT']==0?$cart['PAN_TITRE']:kPanier)."</option>";
	}
}
?>
>&#160;
<option value='-1'><?= kNouvelleSelection ?></option>
</select>
<img onclick="document.form1.dspDest.value='1';document.form1.commande.value='TRANSFERT';submitForm();ddToggle('three')"
src='design/images/famfamfam_mini_icons/icon_accept.gif' />
</div>
</li>
<? } ?>

<li><div onclick="suppression('SUP_LIGNES');ddToggle('three')">
<img src='design/images/famfamfam_mini_icons/page_delete.gif' alt="<?php echo kSupprimerDocumentsSelectionnes; ?>" />
<?=kSupprimerDocumentsSelectionnes?>
</div>
</li>

<li><div onclick="suppression('SUP_ALL');ddToggle('three')">
<img src='design/images/famfamfam_mini_icons/page_cross.gif' alt="<?php echo kViderPanier; ?>" />
<?= ($etat > 0 ?kViderPanier:kViderSelection) ?>
</div>
</li>

</ul>
</dd>
</dl>

<dl class="dropdown">
<!--dt id="four-ddheader" onmouseover="ddMenu('four',1,document.getElementById('chooser').style.display)"
onmouseout="ddMenu('four',-1,document.getElementById('chooser').style.display)"><?=kOngletActions?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;'/></dt>
<dd id="four-ddcontent" onmouseover="cancelHide('four')" onmouseout="ddMenu('four',-1,document.getElementById('chooser').style.display)"-->

<dt id="four-ddheader" onclick="ddToggle('four')"><?=kActions?>
<img src='design/images/arrow_down.gif' style='vertical-align:middle;margin-left:5px;' alt="<?php echo kActions; ?>" /></dt>
<dd id="four-ddcontent">

<ul>
<li>
<div onclick="document.form1.commande.value='SAVE';submitForm();ddToggle('four')" >
<img src='design/images/famfamfam_mini_icons/action_save.gif' alt="<?php echo kSauverOrdre; ?>" />
<?= kSauverOrdre ?>
</div>
</li>

<? if($pan_id_type_commande=='1'){ ?>
	<li>
	<div onclick="stockerMat();ddToggle('four')">
	<img src='design/images/famfamfam_mini_icons/icon_package_open.gif' alt="<?php echo kStockerMateriels; ?>" />
	<?=kStockerMateriels?>
	</div>
	</li>
<? } ?>

<? if($pan_id_type_commande=='2'){ ?>
	<li>
	<div onclick="sortirMat();ddToggle('four')">
	<img src='design/images/famfamfam_mini_icons/icon_package_get.gif' alt="<?php echo kSortirMateriels; ?>" />
	<?=kSortirMateriels?>
	</div>
	</li>

	<li>
	<div onclick="rangerMat();ddToggle('four')">
	<img src='design/images/famfamfam_mini_icons/icon_package.gif' alt="<?php echo kRangerMateriels; ?>" />
	<?=kRangerMateriels?>
	</div>
	</li>
<? } ?>


<li>
<div onclick="javascript:popupPrint('export.php?type=panierdoc&amp;export=exportTabDoc_xls&amp;fichier=panier_<?=$id_panier?>&amp;id=<?=$id_panier?>&amp;nb=all&amp;page=1','main');ddToggle('four')">
<img src='design/images/famfamfam_mini_icons/page_script.gif' alt="<?php echo kExporter; ?>" />
<?= kExporter ?>
</div>
</li>
</ul>
</dd>
</dl>

</div>
</form>
<!-- menu déroulant des actions : FIN -->



<div style='clear:both;font-size:0px;'>&#160;</div>

<?php
}

?>
