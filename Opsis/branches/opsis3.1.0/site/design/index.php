<?php
require_once(modelDir.'model_doc.php');

$myUsr=User::getInstance();

/*require_once(modelDir.'model_html.php');
$myHtml=new Html();
$myHtml->t_html['ID_HTML']="1";
$myHtml->getHtml(null,$_SESSION['langue']);
print $myHtml->t_html['HTML_CONTENU'];*/
global $db;


if(isset($_GET['include']) && !empty($_GET['include']))
	require(designDir."include/".str_replace('../','',$_GET['include']).".inc.php");
else
{	
	$myPage = Page::getInstance();
	$myPage->initReferrer();

	// Recherche par défaut

	$sql = "SELECT ID_DOC 
			FROM t_doc 
			INNER JOIN t_image ON (t_image.id_image = doc_id_image OR DOC_ID_MEDIA = 'A')
			WHERE t_doc.id_lang=".$db->Quote($_SESSION["langue"]);

	$id_vid = $db->getOne($sql . " AND DOC_ID_MEDIA = 'V' ORDER BY t_doc.DOC_DATE_CREA DESC");
	$id_aud = $db->getOne($sql . " AND DOC_ID_MEDIA = 'A' ORDER BY t_doc.DOC_DATE_CREA DESC");
	$id_img = $db->getOne($sql . " AND DOC_ID_MEDIA = 'P' ORDER BY t_doc.DOC_DATE_CREA DESC");
	$id_doc = $db->getOne($sql . " AND DOC_ID_MEDIA = 'D' ORDER BY t_doc.DOC_DATE_CREA DESC");

	
	function setVignette($id) {
		$doc = new Doc();
		$doc->t_doc['ID_DOC'] = $id;
		$doc->getDocFull(1,1,1,1);
		$doc->getVignette();
		$id_mat_vis=$doc->getMaterielVisu();
		$img=(empty($doc->vignette)?"design/images/player.png":"makeVignette.php?image=".$doc->vignette."&amp;h=100");
		if ($doc->t_doc['DOC_ID_MEDIA'] == "A")
			$img = "design/images/audio.png";
			
		$val = "kEveryDoc".$doc->t_doc['DOC_ID_MEDIA'];
			$titre = $doc->t_doc['DOC_TITRE'];
		?>
				<div class="vignette_accueil">
					<a class="iconvignette" href="javascript:void(0)" onclick="goToPage('index.php?urlaction=doc&amp;id_doc=<?= $id ?>');">
						<img title="<?php echo $titre; ?>" alt="<?php echo $titre; ?>" id="poster<?= $id ?>" src="<?= $img ?>" />
					</a>
				</div>
				
				<div class="texte_doc_acceuil">
					
					<p class="titre_doc_acceuil">
						<a class="iconvignette" href="javascript:goToPage('index.php?urlaction=doc&amp;id_doc=<?= $id ?>');" title="<?php echo $doc->t_doc['DOC_TITRE']; ?>">
							<?php
							if (strlen($doc->t_doc['DOC_TITRE'])>110)
								echo substr($doc->t_doc['DOC_TITRE'],0,110).'...'; 
							else
								echo $doc->t_doc['DOC_TITRE'];
							?>
						</a>
					</p>

					<?php 
					echo $doc->t_doc['DOC_DATE_PROD'];
					
					if ($doc->t_doc['DOC_ID_MEDIA'] == "A" || $doc->t_doc['DOC_ID_MEDIA'] == "V")
					{
						echo ' - '.$doc->t_doc['DOC_DUREE'];
					}
					?>
				</div>
					
				<div class="lien_liste_doc_accueil">
					<!--a href="javascript:addIdMedia_('<?=$doc->t_doc['DOC_ID_MEDIA'];?>')"-->
					<a href="javascript:rebondSolr('<?=$doc->t_doc['DOC_ID_MEDIA'];?>','doc_id_media','<?= (defined($val)?constant($val):$val); ?>');">
						<?= (defined($val)?constant($val):$val); ?>&nbsp;&nbsp;&nbsp;>>>
					</a>
				</div>

		<?php
	}

	?>

	<form id="chercheForm" action="index.php?urlaction=docListe&amp;cherche=1" name="form1" method="post" style="display:none">
		<input id="F_doc_form" type="hidden" name="F_doc_form" value="1" />
		<input id="chTypes[1]" type="hidden" name="chTypes[1]" value="CI" />
		<input id="chOps[1]" type="hidden" name="chOps[1]" value="AND" />
		<input id="chFields[1]" type="hidden" name="chFields[1]" value="MEDIA" />
		<input id="chLibs[1]" type="hidden" name="chLibs[1]" value="Media" />
		<input id="chValues[1]" type="text" name="chValues[1]" size="25" />
	</form>

	<script type="text/javascript">
		function addIdMedia_(idmedia){
			document.getElementById("chValues[7]").value=idmedia;
			document.getElementById('quick_search').submit();
		}

	<? if ($myUsr->loggedIn() && !empty($_GET["id_media"])) { ?>
		document.getElementById("chValues[7]").value="<?=$_GET["id_media"];?>";
		document.form1.submit();
	<? } ?>
	</script>
	
	<?php
	require_once(modelDir.'model_html.php');
	?>
	
	<center style="overflow:hidden; height : 100%;">
		<table id="tab_home" cellspacing="24" cellpadding="0">
				<?php
				/*$html_gauche=new Html();
				$html_gauche->t_html['ID_HTML']=5;
				$html_gauche->getHtml(null,$_SESSION['langue']);
				$texte_g=$html_gauche->t_html['HTML_CONTENU'];
				
				$html_droite=new Html();
				$html_droite->t_html['ID_HTML']=6;
				$html_droite->getHtml(null,$_SESSION['langue']);
				$texte_d=$html_droite->t_html['HTML_CONTENU'];*/
				
				if (!empty($texte_g) || !empty($texte_d))
				{
					echo '<tr>';
					echo '<td class="no_bg">';
					
					if (!empty($texte_g))
						echo $texte_g;
					else
						echo '&nbsp;';
						
					echo '</td>';
					echo '<td class="no_bg">';
					
					if (!empty($texte_d))
						echo $texte_d;
					else
						echo '&nbsp;';
					
					echo '</td>';
					echo '</tr>';
				}
				?>
			<tr>
				<td>
					<h6><?=kVideo?><img alt="ic&ocirc;ne vid&eacute;o" src="design/images/icone_video.png" /></h6>
					<?setVignette($id_vid);?>
				</td>
				<td>
					<h6><?=kAudio?><img alt="ic&ocirc;ne audio" src="design/images/icone_audio.png" /></h6>
					<?setVignette($id_aud);?>
				</td>
			</tr>
			<tr>
				<td>
					<h6><?=kImage?><img alt="ic&ocirc;ne photo"src="design/images/icone_picture.png" /></h6>
					<?setVignette($id_img);?>
				</td>
				<td>
					<h6><?=kDocument?><img alt="ic&ocirc;ne ocument" src="design/images/icone_doc.png" /></h6>
					<?setVignette($id_doc);?>
				</td>
			</tr>
		</table>
	</center>
	<?php
}
?>
