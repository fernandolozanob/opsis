<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes" indent="yes" />

	<xsl:template match='/EXPORT_OPSIS'>
		<xsl:for-each select="t_doc">
			<donnees>
				<id><xsl:value-of select="DOC_COTE" /></id>
				<title><xsl:value-of select="DOC_TITRE" /></title>
				<xsl:if test="DOC_RES!=''">
					<description>
						<xsl:value-of select="substring(DOC_RES,1,2000)" />
					</description>
				</xsl:if>
				<xsl:if test="DOC_DATE_PROD!='' and DOC_DATE_PROD!='0000-00-00'">
					<date><xsl:value-of select="DOC_DATE_PROD" /></date>
				</xsl:if>
				<xsl:if test="DOC_VISA!=''">
					<id_dmcloud><xsl:value-of select="DOC_VISA" /></id_dmcloud>
				</xsl:if>
			</donnees>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="tags">
		<xsl:param name="chaine" />
		<xsl:if test="not(contains($chaine,','))">
			<xsl:value-of select="$chaine" />
		</xsl:if>
		<xsl:if test="contains($chaine,',')">
			<xsl:value-of select="substring-before($chaine,',')" /> , <xsl:call-template name="tags">
				<xsl:with-param name="chaine">
					<xsl:value-of select="substring-after($chaine,',')" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
    
        <xsl:template name="tiret">
		<xsl:param name="chaine" />
	  <xsl:variable name="sep" select="' '" />
		<xsl:if test="not(contains($chaine,$sep))">
			<xsl:value-of select="$chaine" />
		</xsl:if>
		<xsl:if test="contains($chaine,$sep)">
			<xsl:value-of select="substring-before($chaine,$sep)" />-<xsl:call-template name="tiret">
				<xsl:with-param name="chaine">
					<xsl:value-of select="substring-after($chaine,$sep)" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>