<?xml version="1.0" encoding="utf-8"?>
<!--
export Excel 

XS : 11/10/2005 : Création du fichier
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="text" 
        indent="yes"
/> 

<!--
Pour info...

tabulation = &#09;
Retour à la ligne = &#09;
-->

<!-- 
EXPORT_OPSIS : Affiche les documents formatés
Appelle : 
Appelé par : 
-->
    <xsl:template match="/EXPORT_OPSIS">

        <!-- NOM DES COLONNES -->
           <xsl:text>Numéro	Titre	Date 	Durée 
</xsl:text>

        <!-- DOC -->
        <xsl:for-each select="t_panier_doc/DOC/t_doc">
               <xsl:value-of select="DOC_COTE"/><xsl:text>	</xsl:text>
               <xsl:value-of select="DOC_TITRE"/><xsl:text>	</xsl:text>
               <xsl:value-of select="DOC_DATE_PROD"/><xsl:text>	</xsl:text>
               <xsl:value-of select="DOC_DUREE"/><xsl:text>	</xsl:text>
            <!-- Ligne de séparation entre les documents -->
                <xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    
    
    <!-- 
    FORMAT_DUREE : Formate une durée HH:MM:SS en HH h MM m
    Appelle : 
    Appelé par : EXPORT_OPSIS
    -->
    <xsl:template name="format_duree">
        <xsl:param name="chaine_duree"/>
        
            <!-- découpage de la chaine -->
            <xsl:variable name="duree_hh"><xsl:value-of select="substring-before($chaine_duree,':')"/></xsl:variable>
            <xsl:variable name="duree_mm"><xsl:value-of select="substring-before(substring-after($chaine_duree,':'),':')"/></xsl:variable>
            <xsl:variable name="duree_ss"><xsl:value-of select="substring-after(substring-after($chaine_duree,':'),':')"/></xsl:variable>
            
            <!-- Restitution -->
            <xsl:if test="$duree_hh != '00' ">
                <xsl:value-of select="normalize-space($duree_hh)"/><xsl:text> h </xsl:text>
            </xsl:if>
            <xsl:value-of select="normalize-space($duree_mm)"/><xsl:text> min </xsl:text>
    </xsl:template>


    <!-- 
    FORMAT_PERSONNE : Formate une durée HH:MM:SS en HH h MM m
    Appelle : 
    Appelé par : EXPORT_OPSIS
    -->
    <xsl:template name="format_personne">
        <xsl:param name="chaine_personne"/>
        
            <!-- découpage de la chaine -->
            <xsl:variable name="prenom">
                 <xsl:call-template name="lastIndexOf">
                    <xsl:with-param name="chaine" select="$chaine_personne" />
                    <xsl:with-param name="pattern" select="' '" />
                 </xsl:call-template>
            </xsl:variable>
            
            <xsl:variable name="nom">
                 <xsl:call-template name="tranforme_chaine">
                    <xsl:with-param name="chaine" select="substring-before($chaine_personne,$prenom)" />
                    <xsl:with-param name="type" select="'majuscule'" />
                 </xsl:call-template>
            </xsl:variable>
            

            <!-- Restitution -->
            <xsl:value-of select="normalize-space($nom)"/><xsl:text> : </xsl:text><xsl:value-of select="normalize-space($prenom)"/>
    </xsl:template>


    <!-- 
    LASTINDEXOF : Récupère la chaine suivant la dernière occurence d'un $pattern dans la chaine $chaine
    Appelle : 
    Appelé par : EXPORT_OPSIS
    -->
    <xsl:template name="lastIndexOf">
       <!-- declare that it takes two parameters - the string and the char -->
       <xsl:param name="chaine" />
       <xsl:param name="pattern" />
       <xsl:choose>
          <xsl:when test="contains($chaine, $pattern)">
             <xsl:call-template name="lastIndexOf">
                <xsl:with-param name="chaine" select="substring-after($chaine, $pattern)" />
                <xsl:with-param name="pattern" select="$pattern" />
             </xsl:call-template>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="$chaine" /></xsl:otherwise>
       </xsl:choose>
    </xsl:template>



    <!--
    TRANSFORME_CHAINE : Permet de transformer une chaine en majuscule ou en minuscule
    Appelle : 
    Appelé par : FORMAT_PERSONNE
    -->
    <xsl:template name="tranforme_chaine">
    <xsl:param name="chaine"/>
    <xsl:param name="type"/>
    
        <xsl:choose>
            <xsl:when test="$type = 'majuscule'">
                <xsl:value-of select="translate($chaine,'abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþ','ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="translate($chaine,'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ','abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþ')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
