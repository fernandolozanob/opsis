<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

<xsl:template match='/t_doc'>
	<t_doc>
		<xsl:copy-of select="ID_DOC"/>
		<xsl:copy-of select="ID_LANG"/>
		<xsl:for-each select="child::*">
			<xsl:if test="(contains(name(),'DOC_') and child::text()!='') or name()='VIGNETTE'"><xsl:copy-of select="."/></xsl:if>
		</xsl:for-each>
		<xsl:copy-of select="DOC_XML"/>
		<xsl:for-each select="t_doc_lex/TYPE_DESC/t_rol/t_lex">
			<L>
			<xsl:for-each select="child::*">
					<xsl:if test="(contains(name(),'LEX_') or name()='ID_TYPE_DESC') and name()!='LEX_ID_GEN' and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
			</xsl:for-each>
			</L>
		</xsl:for-each>

		<xsl:for-each select="t_pers_lex/TYPE_DESC/t_rol/t_personne">
			<P>
				<xsl:for-each select="child::*">
						<xsl:if test="(contains(name(),'PERS_') or contains(name(),'DLEX_') or name()='ID_TYPE_DESC') and name()!='PERS_ID_GEN' and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
				</xsl:for-each>
			</P>
		</xsl:for-each>
		
		<xsl:for-each select="t_doc_val/TYPE_VAL">
			<V>
			<VAL_ID_TYPE_VAL><xsl:value-of select="@ID_TYPE_VAL"/></VAL_ID_TYPE_VAL>
			<xsl:for-each select="t_val/child::*">
					<xsl:if test="(contains(name(),'VAL_') or name()='VALEUR')  and name()!='VAL_ID_GEN' and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
			</xsl:for-each>
			</V>
		</xsl:for-each>

		<xsl:for-each select="t_doc_mat/DOC_MAT">
			<MAT>
				<xsl:for-each select="child::*">
					<xsl:if test="(contains(name(),'DMAT_') or name()='ID_MAT') and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
				</xsl:for-each>
				<xsl:for-each select="t_mat/child::*">
					<xsl:if test="contains(name(),'MAT_') and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
				</xsl:for-each>
			</MAT>
		</xsl:for-each>

		<!--xsl:for-each select="t_image/DOC_IMG">
			<IMG>
			<xsl:for-each select="child::*">
				<xsl:if test="(contains(name(),'IM_') or name()='IMAGEUR') and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
			</xsl:for-each>
			</IMG>
		</xsl:for-each-->

		<xsl:for-each select="t_doc_acc/t_doc_acc">
			<DA>
			<xsl:for-each select="child::*">
				<xsl:if test="contains(name(),'DA_') and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
			</xsl:for-each>
			</DA>
		</xsl:for-each>

		<xsl:for-each select="t_doc_fest/FEST">
			<FEST>
			<xsl:for-each select="child::*">
				<xsl:if test="contains(name(),'FEST_') and name()!='FEST_ID_GEN' and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
			</xsl:for-each>
			</FEST>
		</xsl:for-each>
	</t_doc>
</xsl:template>



</xsl:stylesheet>