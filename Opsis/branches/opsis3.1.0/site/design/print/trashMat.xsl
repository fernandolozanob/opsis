<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

<xsl:template match='/t_mat'>
	<t_mat>
		<xsl:copy-of select="ID_MAT"/>
		<xsl:copy-of select="ID_LANG"/>
		<xsl:for-each select="child::*">
			<xsl:if test="contains(name(),'MAT_') and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
		</xsl:for-each>
		<xsl:copy-of select="MAT_XML"/>
		<!--xsl:for-each select="t_doc_lex/TYPE_DESC/t_rol/t_lex">
			<L>
			<xsl:for-each select="child::*">
					<xsl:if test="(contains(name(),'LEX_') or name()='ID_TYPE_DESC') and name()!='LEX_ID_GEN' and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
			</xsl:for-each>
			</L>
		</xsl:for-each>

		<xsl:for-each select="t_pers_lex/TYPE_DESC/t_rol/t_personne">
			<P>
				<xsl:for-each select="child::*">
						<xsl:if test="(contains(name(),'PERS_') or contains(name(),'DLEX_') or name()='ID_TYPE_DESC') and name()!='PERS_ID_GEN' and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
				</xsl:for-each>
			</P>
		</xsl:for-each-->
		<xsl:for-each select="t_doc_mat/DOC_MAT">
			<DOC>
				<xsl:for-each select="child::*">
					<xsl:if test="(contains(name(),'DMAT_') or name()='ID_DOC') and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
				</xsl:for-each>
				<xsl:for-each select="t_doc/child::*">
					<xsl:if test="contains(name(),'DOC_') and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
				</xsl:for-each>
			</DOC>
		</xsl:for-each>
		<xsl:for-each select="t_mat_val/TYPE_VAL">
			<V>
			<VAL_ID_TYPE_VAL><xsl:value-of select="@ID_TYPE_VAL"/></VAL_ID_TYPE_VAL>
			<xsl:for-each select="t_val/child::*">
					<xsl:if test="(contains(name(),'VAL_') or name()='VALEUR' or name()='ID_VAL')  and name()!='VAL_ID_GEN' and child::text()!=''"><xsl:copy-of select="."/></xsl:if>
			</xsl:for-each>
			</V>
		</xsl:for-each>
		<xsl:for-each select="tracks/t_mat_track">
			<TRACK>
			<xsl:for-each select="./child::*">
				<xsl:copy-of select="."/>
			</xsl:for-each>
			</TRACK>
		</xsl:for-each>

	</t_mat>
</xsl:template>



</xsl:stylesheet>