<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

<xsl:template match='/data'>
	<data>
		<xsl:for-each select="file">
			<DOC>
				<DOC_ID_MEDIA>V</DOC_ID_MEDIA>
				<ID_LANG>FR</ID_LANG>
				<DOC_TITRE><xsl:value-of select="title" /></DOC_TITRE>
				<DOC_TITRE_COL><xsl:value-of select="album" /></DOC_TITRE_COL>				
				<DOC_DUREE>
						<xsl:value-of select="duration" />
				</DOC_DUREE>
				<DOC_DATE_PROD>
						<xsl:value-of select="year" />
				</DOC_DATE_PROD>
				<DOC_RES><xsl:value-of select="comment" /></DOC_RES>
				<L_PRD_EDT_PM><xsl:value-of select="artist" /></L_PRD_EDT_PM>
				<L_GEN_AUT_PP><xsl:value-of select="composer" /></L_GEN_AUT_PP>
				<V_THM>
					<xsl:value-of select="theme" />
				</V_THM>
			</DOC>
		</xsl:for-each>
	</data>
</xsl:template>

<!-- 
FORMAT_DUREE : Formate une dur√©e HH:MM:SS
-->
    <xsl:template name="format_duree">
        <xsl:param name="chaine_duree"/>
            <!-- d√©coupage de la chaine -->
            <xsl:variable name="duree_hh"><xsl:text>00</xsl:text></xsl:variable>
            <xsl:variable name="duree_mm"><xsl:value-of select="format-number(normalize-space(substring-before($chaine_duree,'min')),'00')"/></xsl:variable>
            <xsl:variable name="duree_ss"><xsl:value-of select="format-number(normalize-space(substring-before(substring-after($chaine_duree,'min'),'sec')),'00')"/></xsl:variable>

            <!-- Restitution -->
        	<xsl:value-of select="$duree_hh"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_mm"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_ss"/>
            
    </xsl:template>

<!-- 
FORMAT_DATE : Formate une date jj.mm.aaaa en aaaa-mm-jj
-->
    <xsl:template name="format_date">
        <xsl:param name="chaine_date"/>
		<!-- decoupage de la chaine -->
		<xsl:variable name="j"><xsl:value-of select="substring-before($chaine_date,'.')"/></xsl:variable>
		<xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine_date,'.'),'.')"/></xsl:variable>
		<xsl:variable name="a"><xsl:value-of select="substring-after(substring-after($chaine_date,'.'),'.')"/></xsl:variable>
        <xsl:value-of select="normalize-space($a)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($m)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($j)"/>
    </xsl:template>


</xsl:stylesheet>