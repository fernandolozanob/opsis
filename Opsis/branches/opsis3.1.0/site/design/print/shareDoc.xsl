<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:include href="../detail.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="corps"/>
<xsl:param name="date_limite"/>
<xsl:param name="chemin_http"/>
<xsl:param name="logo_url"/>
<xsl:template match='/EXPORT_OPSIS'>
<html>
	<head>
		<style>
		div.corps_mail{
			min-height : 400px; 
			margin : 6px;
		}
		div.header, div.footer{
			margin : 16px 6px ;
		}
		div.header img{
			width : 116px ; 
		}
		table td{
			border-bottom : 1px solid #CACACA;
		}
		table tr.resultsHead td{
			font-weight : bold ; 
		}
		</style>
	</head>
	<body>
		<div class="header">
			<img>
				<xsl:attribute name="src"><xsl:value-of select="concat($chemin_http,'/',$logo_url)"/></xsl:attribute>
			</img>
		</div>
		<div class="corps_mail">
			<xsl:if test="$corps != ''">
				<div>
					<xsl:value-of select="$corps"/>
				</div>
			</xsl:if>
			<br />
			<table class="resultsHead">
			<tbody>
				<tr>
					<td>Vignette</td>
					<td>Numéro</td>
					<td>Origine</td>
					<td>Série, Magazine</td>
					<td>Titre, Sous-titre</td>
					<td>Auteur, Réalisateur</td>
					<td>Type</td>
					<td>1ère Diffusion</td>
					<td>Durée</td>
					<td>Numérisé</td>
				</tr>
			<xsl:for-each select="t_partage/t_partage_objet/t_doc">
				<tr>
					<td>
						<xsl:variable name="vignette">
							<xsl:choose>
								<xsl:when test="VIGNETTE != ''">
									<xsl:value-of select="concat($chemin_http,'/','makeVignette.php?h=80&amp;w=142&amp;kr=1&amp;image=',VIGNETTE)"/>
								</xsl:when>
							</xsl:choose>
						</xsl:variable>
						<xsl:if test="$vignette != ''">
						<a style="text-decoration:none;">
							<xsl:attribute name="href"><xsl:value-of select="concat($chemin_http,'/','viewer.php?code=',../../PARTAGE_CODE)"/></xsl:attribute>
							<img src="{$vignette}"/>
						</a>
						</xsl:if>
					</td>
					<td>
						<a style="text-decoration:none;">
							<xsl:attribute name="href"><xsl:value-of select="concat($chemin_http,'/','viewer.php?code=',../../PARTAGE_CODE)"/></xsl:attribute>
							<xsl:value-of select="DOC_COTE"/>
						</a>
					</td>
					<td>
						<xsl:value-of select="DOC_XML/DOC/FONDS"/>
					</td>
					<td>
						<xsl:for-each select="DOC_VAL/XML/SER">
							<xsl:value-of select="."/>
							<xsl:if test="position()!=last()">,</xsl:if>
						</xsl:for-each>
						<br />
						<xsl:for-each select="DOC_VAL/XML/MAG">
							<xsl:value-of select="."/>
							<xsl:if test="position()!=last()">,</xsl:if>
						</xsl:for-each>
					</td>
					<td>
						<a style="text-decoration:none;">
							<xsl:attribute name="href"><xsl:value-of select="concat($chemin_http,'/','viewer.php?code=',../../PARTAGE_CODE)"/></xsl:attribute>
							<xsl:value-of select="DOC_TITRE"/><br />
							<xsl:value-of select="DOC_SOUSTITRE"/>
						</a>
					</td>
					<td>
						<xsl:for-each select="DOC_LEX/LEX[@TYPE='AUT' and @ROLE='']/TERME">
							<xsl:value-of select="PERS_NOM"/>
							<xsl:if test="position()!=last()">,</xsl:if>
						</xsl:for-each>
						<br />
						<xsl:for-each select="DOC_LEX/LEX[@TYPE='AUT' and @ROLE='REA']/TERME">
							<xsl:value-of select="PERS_NOM"/>
							<xsl:if test="position()!=last()">,</xsl:if>
						</xsl:for-each>
					</td>
					<td>
						<xsl:value-of select="TYPE_DOC"/>
					</td>
					<td>
						<xsl:value-of select="DOC_DATE_DIFF"/>
					</td>
					<td>
						<xsl:value-of select="DOC_DUREE"/>
					</td>
					<td>
						<xsl:value-of select="DOC_NUM"/>
					</td>
				</tr>
				
			</xsl:for-each>
			</tbody>
			</table>
		</div>
		<div class="footer">Ce document est accessible jusqu'au <xsl:value-of select="$date_limite"/>.</div>
	</body>
</html>
    </xsl:template>
</xsl:stylesheet>
