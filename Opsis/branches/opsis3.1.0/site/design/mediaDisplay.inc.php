<?php
require_once(libDir."webComponents/oraoweb/orao_player.php");
require_once(modelDir.'model_materiel.php');
 ?>


<?php


if(!isset($visu_type)){
	if(isset($myDoc) && !isset($myMat)){
		$visu_type = "document";
	}else if (isset($myMat) && !isset($myDoc)){
		$visu_type = "materiel";
	}
}
if(isset($myDoc) && !isset($myDoc->t_doc_mat)){
	$myDoc->getMats();
}

if($visu_type == "document" && isset($myDoc)){
	$id_visu  = $myDoc->t_doc['ID_DOC'];
	$vignette = $myDoc->vignette;
}else if($visu_type == "materiel" && isset($myMat)){
	$id_visu  = $myMat->t_mat['ID_MAT'];
	// $vignette = $myMat->vignette;
	$vignette = $myMat->getVignette();
	$vignette = $myMat->vignette;
	$id_mat_vis=$id_visu;
}

if($visu_type == "document"){
	$id_mat_vis=$myDoc->getMaterielVisu();
}
if(get_class($myDoc) == "Reportage"){
	$myDoc->getDocLiesDST();
	$myDoc->getDocLiesSRC();
}
	$entity_has_media = true ; 

?>


<script type="text/javascript">
	// Définition des options relatives au layout du site, envoyées lors de l'appel
	if(typeof(player_options) == "undefined"){
		var player_options = {};
	}
	
	player_options['resizeCustom'] = true;
	player_options['enable_streaming'] = true;
	player_options['span_time_passed'] = true;
	if(typeof player_options['fit_height_container'] == 'undefined'){
		player_options['fit_height_container'] = true;
	}
	if(typeof player_options['hauteur_track'] == 'undefined'){
		player_options['hauteur_track'] = 8;
	}
	if(typeof player_options['hauteur_bg_track'] == 'undefined'){
		player_options['hauteur_bg_track'] = 8;
	}
	if(typeof player_options['css_path'] == 'undefined'){
		player_options['css_path'] = ow_const.kCheminHttp+'/design/css/skin_player/';
	}
	if(typeof player_options['displayLoopButton'] == 'undefined'){
		player_options['displayLoopButton'] = true;
	}
	if(typeof pattern == 'undefined'){
		pattern = '_vis';
	}
	
	player_options['addParamsToPHPScript'] = { type_doc_mat_srt: "LANG", id_mat:null };

	if(typeof player_options['container_id'] != 'undefined'){
		container_id = player_options['container_id'];
	}else{
		container_id = "container";
	}
	
</script>

<!-- =================================CAS DOC VIDEO===========================================-->
<?php 

$arrFormatVisio = array("MPEG4", "H264","SD - 4:3 - MP4 - H264","SD - 16:9 - MP4 - H264", "HD - MP4 - H264");
if(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="V")
	|| ($visu_type == "materiel" && $myMat->hasfile && in_array($myMat->t_mat['MAT_FORMAT'], $arrFormatVisio))){
	
	$img=(empty($vignette)?"makeVignette.php?image=".designDir."images/vignette_blank.png&w=1280&kr=1&ol=player":"makeVignette.php?image=".$vignette."&amp;w=1280&amp;kr=1&ol=player");
	$img_print=(empty($vignette)?"makeVignette.php?image=".designDir."images/vignette_blank.png&w=1280&kr=1":"makeVignette.php?image=".$vignette."&amp;w=1280&amp;kr=1");
	?>

	<div id="media_space">
	</div>
		<script type="text/javascript">
		container_div = document.createElement('div');
		container_div.className = "video";
		container_div.id = container_id;
		container_div.innerHTML = '<?php
			if (!empty($img))
			{
				echo '<img id="poster"  alt="poster" src="'.$img.'"   style="visibility:hidden; max-height : 100%; max-width : 100%;display:block;margin : 0px auto;" onclick="loadOraoPlayer(\\\''.$id_visu.'\\\', $j(\\\'#\\\'+container_id).width(),pattern,player_options,true,\\\''.$visu_type.'\\\');" />';
				echo '<img id="poster_print"  alt="poster" src="'.$img_print.'"   style="visibility:hidden; max-height : 100%; max-width : 100%;display:block;margin : 0px auto;" />';
			}
			?>';
		$j("#media_space").append(container_div);
		</script>
	
	<script type="text/javascript">
	$j(document).ready(function(){
		if(typeof OW_env_user != 'undefined' && (OW_env_user.env_OS == 'Android' || OW_env_user.env_OS == 'iPad' || OW_env_user.env_OS == 'iPhone')){
			player_options['image_with_ol'] = true;		
			$j("#"+container_id+" #poster").click();
		}else{
			player_options['noPoster'] = true;
			$j("#"+container_id+" #poster").css("visibility","");
		}
	});
	</script>

<!-- =================================CAS DOC REPORTAGE=========================================== -->
<?}elseif ($visu_type == "document" && get_class($myDoc) == "Reportage" && count($myDoc->arrDocLiesDST) > 0){
	// POUR RAPPEL : 
	// la  constante gReportagePicturesActiv doit être définie, le document doit avoir DOC_ID_TYPE_DOC = gReportagePicturesActiv 
	// les documents doivent être liés dans t_doc_lien (l'id_doc_dst = l'id_doc du reportage)
	$id_visu = $myDoc->t_doc['ID_DOC']; ?>
	
	<div id="media_space">
		<div id="container" style="overflow:hidden;">
			<div id="mon_slide">
				<script type="text/javascript">
				function playSlideshowReport(id, type)
				{
					var options=
					{
						width:'100%',
						height:'100%',
						url:'empty.php?xmlhttp=1&urlaction=prepareVisu&method=Diaporama&action=visu&id='+id+'&id_lang=fr&pattern=vis&type='+type,
						loop:true,
						toggle_thumbs:true,
						stepping_frequency:5,
						diapo_precis : true
					};
					requirejs(['oraoweb/slideshow/OW_slideshow'],function(){
						requireCSS('<?=libUrl?>/webComponents/oraoweb/css/ow-slideshow.css',function(){
							my_player=new OW_slideshow('mon_slide',options);
						});
					});
				}
				
				playSlideshowReport('<?= $id_visu; ?>','reportage');
				</script>
			</div>
		</div>
	</div>
	
<!-- =================================CAS DOC PICTURE=========================================== -->
<?}elseif(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="P")
	|| ($visu_type == "materiel" && $myMat->hasfile &&( strpos($myMat->t_mat['MAT_FORMAT'],'GIF')!==false || strpos($myMat->t_mat['MAT_FORMAT'],'JPEG')!==false || strpos($myMat->t_mat['MAT_FORMAT'],'JPG')!==false || strpos($myMat->t_mat['MAT_FORMAT'],'PNG')!==false))){
	
	if($visu_type == "document"){
		$id_mat_vis=$myDoc->getMaterielVisu();
		$id_doc_mat=0;

		foreach ($myDoc->t_doc_mat as $mat)
		{
			if ($mat['ID_MAT']==$id_mat_vis)
				break;
			
			$id_doc_mat++;
		}
		
		// redéfinition de l'id_visu vers le materiel pour les images
		$id_visu = $myDoc->t_doc_mat[$id_doc_mat]["MAT"]->t_mat['ID_MAT'];
		$mat_visu =  $myDoc->t_doc_mat[$id_doc_mat]["MAT"];
		
	}else if ($visu_type == "materiel"){
		$mat_visu =$myMat;
		// $arrsize =  array($myMat->t_mat['MAT_WIDTH'],$myMat->t_mat['MAT_HEIGHT']);
	}

	if(isset($mat_visu->t_mat['MAT_WIDTH']) && $mat_visu->t_mat['MAT_WIDTH']!="0" 
		&& isset($mat_visu->t_mat['MAT_HEIGHT'])  && $mat_visu->t_mat['MAT_HEIGHT']!="0"){
			$arrsize = array($mat_visu->t_mat['MAT_WIDTH'],$mat_visu->t_mat['MAT_HEIGHT']);
	}else{
		require_once(libDir."class_matInfo.php");
		$infos = MatInfo::getMatInfo($mat_visu->getLieu().$mat_visu->t_mat['MAT_NOM']);
		$arrsize = array($infos["width"],$infos["height"]);
		unset($infos);
	}	
	
	$max_size = max($arrsize);
	if(intval($max_size)>2000){
		$max_size=2000;
	}
	?>
	
	<link href="<?=libUrl?>webComponents/Mapbox/css/map.css" type="text/css" rel="stylesheet" />

	<div id="media_space">
		<div id="container" class="mapwrapper img" >
			<div id="viewport" style="width:100%; height:100%; visibility : hidden;"> 
				<div id="divPart" style="width: 480px; height:360px;">
					<img src="makeVignette.php?image=<?= $id_visu ?>&type=video&w=<?=$max_size?>&h=<?=$max_size?>&kr=1" width="100%" height="100%" />
				</div> 
				<div id="divComplete" style="width: 960px; height: 720px;"> 
					<img id="imgComplete" src="makeVignette.php?image=<?= $id_visu ?>&type=video&w=<?=$max_size?>&h=<?=$max_size?>&kr=1" alt="" /> 
					<div class="mapcontent"> 
					</div> 
				</div> 
			</div> 
			<div id="control" class="zoom-control">
				<a href="#zoom" class="zoom">Zoom</a>
				<a href="#zoom_out" class="back">Back</a>
				<a href="#fullscreen" class="fullscreen">Fullscreen</a>
			</div>
		</div>
	</div>
	
	<script type ="text/javascript"> 
		
		var ratio = <?=$arrsize[1];?> / <?=$arrsize[0];?>; // var pour transmettre les éléments php > javascript pour mapboxresize
		
		requirejs(['Mapbox/js/mousewheel','Mapbox/js/mapbox'],function(){
			initMapboxResize();
			$j("#media_space #container.mapwrapper #viewport").css('visibility','visible');
		});
	</script>
	
<!-- =================================CAS DOC PDF==========================================-->
<?}elseif(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="D") 
	|| ($visu_type == "materiel" && $myMat->hasfile && strpos($myMat->t_mat['MAT_FORMAT'],'PDF')!==false)){
		// redéfinition de l'id_visu dans le cas "document"
		/*if($visu_type == "document"){
			$id_visu = $id_mat_vis;
			
		}*/
		
		$subUrl = "";
		
		if (is_array($_SESSION["recherche_DOC_Solr"]["form"])){
			foreach ($_SESSION["recherche_DOC_Solr"]["form"] as $field){
				if ($field["FIELD"] == "text") {
					$word = trim(explode(",", $field["VALEUR"])[0]);
					$subUrl = "&find=".htmlspecialchars($word);
				}
			}
		}
		?>
		
	<div id="media_space">
		<div id="container" style="overflow : hidden ; ">
		<!--div id="mon_slide">
				<script type="text/javascript">
				
				if(typeof(options) == "undefined"){
					var options={};
				}
				if(typeof(options['width']) == "undefined") options['width'] = '100%';
				if(typeof(options['height']) == "undefined") 	options['height'] = '100%';
				if(typeof(options['loop']) == "undefined") options['loop'] = true;
				if(typeof(options['toggle_thumbs']) == "undefined") options['toggle_thumbs'] = true;
				if(typeof(options['stepping_frequency']) == "undefined") options['stepping_frequency'] = 5;
				
			
				options['autoplay'] = true ; 
			
				function playSlideshow(id, type)
				{
					if(typeof(options['url']) == "undefined") options['url'] = 'empty.php?xmlhttp=1&urlaction=prepareVisu&method=Diaporama&action=visu&id='+id+'&id_lang=fr&pattern=vis&type='+type;

					requirejs(['oraoweb/slideshow/OW_slideshow'],function(){
						requireCSS('<?=libUrl?>/webComponents/oraoweb/css/ow-slideshow.css',function(){
							my_player=new OW_slideshow('mon_slide',options);
						});
					});
				}
				
				playSlideshow('<?= $id_visu; ?>',<?= ($visu_type=='materiel'?"'mat'":"'doc'")?>);
				</script>
			</div-->
		
			<iframe style="width:100%;height:100%; border:none;">
			</iframe>
			<script type="text/javascript">
			$j.ajax({
				url : 'blank.php?xmlhttp=1&urlaction=prepareVisu&method=QT&action=visu&id=<?=$id_visu;?>&pattern=pdf&type=<?= ($visu_type=='materiel'?"mat":"doc")?>',
				success : function(data){
					// $j("#container iframe").attr("src", "<?=libUrl?>/webComponents/pdf.js-gh-pages/web/viewer.php?pdf_file=" + $j(data).find("mediaurl").text() + "<?=$subUrl;?>");
					$j("#container iframe").attr("src", "empty.php?urlaction=pdfViewer&pdf_file=" + $j(data).find("mediaurl").text() + "<?=$subUrl;?>");
					// $j("#container iframe").attr("src", "pdf_viewer.php?pdf_file=" + $j(data).find("mediaurl").text() + "<?=$subUrl;?>");
				}
			});
			</script>
		</div>
	</div>

<!-- =================================CAS DOC AUDIO==========================================-->
<?}elseif(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="A") 
	|| ($visu_type == "materiel" && $myMat->hasfile && strpos($myMat->t_mat['MAT_FORMAT'],'MP3')!==false )){
	if (empty($vignette)) {?>
		<div id="media_space">
			<div id="container" class="audio_poster" >
				<img id='poster' src="design/images/audio.png"  onclick="loadOraoPlayer('<?= $id_visu ?>', 480,'.mp3',player_options,null,'<?= $visu_type ?>');" />
			</div>
		</div>
	<?} else { ?>
		<div id="media_space">
			<div id="container" class="audio_poster">
				<center>
					<img id='poster' src="<?= "makeVignette.php?image=".$vignette."&amp;w=480&amp;ol=player&amp;kr=1"; ?>" onclick="loadOraoPlayer('<?= $id_visu ?>', 480,'.mp3',player_options,null,'<?= $visu_type ?>');" />
				</center>
			</div>	
		</div>
	<?	} ?>
	<script type="text/javascript">
		player_options['show_vignette_audio'] = true ;
		player_options['res_vignette_width'] = "" ;
		image_default = "design/images/player.png" ;
		$j(document).ready(function(){
			if(typeof OW_env_user != 'undefined' && (OW_env_user.env_OS == 'Android' || OW_env_user.env_OS == 'iPad' || OW_env_user.env_OS == 'iPhone')){
				$j("#container #poster").click();
			}
		});
	</script>
<? } elseif(!empty($vignette)){?>
	<div id="media_space">
		<div id="container">
			<img id="poster" src="makeVignette.php?image=<?=$vignette?>&amp;w=480&amp;kr=1" width="100%"/>
		</div>
	</div>
<? }else{
		$entity_has_media = false;?>
		<script>
		if (document.getElementById('fiche_doc')) document.getElementById('fiche_doc').className+=" no_media";
		if (document.getElementById('fiche_info')) document.getElementById('fiche_info').className+=" no_media";
		</script>
<? } ?>
