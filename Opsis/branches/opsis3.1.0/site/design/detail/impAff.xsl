<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../detail.xsl"/>
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>


<!-- recuperation de parametres PHP -->
<xsl:param name="loggedIn" />
<xsl:param name="tab" />
<xsl:param name="docAccChemin" />
<xsl:param name="rang" />
<xsl:param name="offset" />
<xsl:param name="xmlfile" />
<xsl:param name="page"/>
<xsl:param name="tri"/>
<xsl:param name="nbLigne"/>
<xsl:param name="layout"/>
<xsl:param name="urlparams"/>
<xsl:param name="import_type_form"/>
<xsl:param name="showTypeSelector"/>
<xsl:param name="gReportagePicturesActiv"/>

<xsl:template match='/select'>

	<xsl:variable name="xmllist" select="document($xmlfile)"/>
	<xsl:variable name="docListeXml" select="document($docListeXmlFile)"/>
	<xsl:variable name="matListeXml" select="document($matListeXmlFile)"/>
		<div id="importViewBar"><!--xsl:attribute name="class">title_bar &lt;?=(($layout&lt;1 || $layout&gt;2)?'collapsed':'')?&gt;</xsl:attribute-->
		<xsl:attribute name="class">title_bar pres_<xsl:value-of select="$layout"/></xsl:attribute>
			<xsl:choose>
			<xsl:when test="not(t_import)">
				<xsl:choose>
					<xsl:when test="$showTypeSelector and $showTypeSelector=1">
						<select name="import_type_form" id="type_import_selector" onchange="changeTypeForm(this);">
							<xsl:choose>
								<xsl:when test="$import_type_form='folder'">
									<option value="http"><xsl:processing-instruction name="php">print kImportParChargement</xsl:processing-instruction></option>
									<option selected="true" value="folder"><xsl:processing-instruction name="php">print kImportParDossier</xsl:processing-instruction></option>
								</xsl:when>
								<xsl:otherwise>
									<option selected="true" value="http"><xsl:processing-instruction name="php">print kImportParChargement</xsl:processing-instruction></option>
									<option value="folder"><xsl:processing-instruction name="php">print kImportParDossier</xsl:processing-instruction></option>
								</xsl:otherwise>
							</xsl:choose>
						</select>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$import_type_form='folder'">
								<xsl:processing-instruction name="php">print kImportDossier</xsl:processing-instruction>
							</xsl:when>
							<xsl:otherwise>
								<xsl:processing-instruction name="php">print kImport</xsl:processing-instruction>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="t_import!=''">
				<span class="importTitle">
					<xsl:choose>
					<xsl:when test="$tab_shown='mat'">
						<xsl:value-of select="t_import/IMP_NOM"/><xsl:text> : </xsl:text><xsl:choose><xsl:when test="t_import/NB_MATS!=''"><xsl:value-of select="t_import/NB_MATS"/></xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose>&#160;<xsl:processing-instruction name="php">print kMateriel_s;</xsl:processing-instruction>
						<xsl:if test="t_import/IMP_USAGER!=''">&#160;<xsl:processing-instruction name="php">print kImportPar</xsl:processing-instruction>&#160;<xsl:value-of select="t_import/IMP_USAGER"/></xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="t_import/IMP_NOM"/><xsl:text> : </xsl:text><xsl:choose><xsl:when test="t_import/NB_DOCS!=''"><xsl:value-of select="t_import/NB_DOCS"/></xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose>&#160;<xsl:processing-instruction name="php">print kDocument_s</xsl:processing-instruction>
						<xsl:if test="t_import/IMP_USAGER!=''">&#160;<xsl:processing-instruction name="php">print kImportPar</xsl:processing-instruction>&#160;<xsl:value-of select="t_import/IMP_USAGER"/></xsl:if>
					</xsl:otherwise>
					</xsl:choose>
				</span>
				<script>displayHoverEllipsis('span.importTitle','ellipsis_hover ellipsis_import_title');</script>
				<xsl:call-template name="toolbar">
					<xsl:with-param name="context">importView</xsl:with-param>
					<xsl:with-param name="entity" select="$xmllist/list/entity"/>
					<xsl:with-param name="id_item" select="t_import/ID_IMPORT"/>
				</xsl:call-template>
			</xsl:when>
			</xsl:choose>
			&#160;
		</div>

	<div id="menuGauche" class="&lt;?=(($layout&lt;1 || $layout&gt;2)?'collapsed':'')?&gt;">
		<div class="title_bar last_import">
			<span class="last_import_label"><xsl:processing-instruction name="php">print kDerniersImports;</xsl:processing-instruction></span>
			<span id="arrow_left" class="toggle_arrow" onclick="toggleLeftMenu();">&#160;</span>
		</div>
		<div id="import_wrapper">
			<ul class="impListe">
			<li onclick="window.location.href='index.php?urlaction=importView'"><xsl:attribute name="class"><xsl:if test="not(t_import)">selected</xsl:if></xsl:attribute><xsl:processing-instruction name="php">print kNouvelImport</xsl:processing-instruction></li>
			<xsl:if test="count(last_imps/t_import)&gt;0">
				<xsl:for-each select="last_imps/t_import">
					<li id="imp{ID_IMPORT}" onclick="window.location.href='index.php?urlaction=importView&amp;id_import={ID_IMPORT}'">
						<xsl:if test="./ID_IMPORT = /select/t_import/ID_IMPORT"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
						<xsl:value-of select="IMP_NOM"/><xsl:text>	</xsl:text>(<xsl:value-of select="NB_DOCS"/>)
					</li>
				</xsl:for-each>
			</xsl:if>
				<li onclick="window.location.href='index.php?urlaction=importListe'" class="redirect_imp_liste"><xsl:processing-instruction name="php">print kTousImports</xsl:processing-instruction></li>
			</ul>
			&#160;
		</div>
	</div>

	<div class="main_scrollableDiv scrollableListe apply_layout pres_&lt;?=$layout;?&gt; import_section">
	<!-- t_import n'existe pas => on est pas dans le cas de visualisation d'un import => on affiche donc le formulaire d'upload-->
	<xsl:if test="not(t_import)">
		<script>
			function changeTypeForm(elt_select){
				if(elt_select.value != '<xsl:value-of select="$import_type_form"/>'){
					window.location.href='index.php?urlaction=importView&amp;import_type_form='+elt_select.value;
				}
			}
		</script>

		<xsl:choose>
			<xsl:when test="$import_type_form='folder'">
				<xsl:processing-instruction name="php">include(getSiteFile('formDir','importFolder.inc.php'));</xsl:processing-instruction>
			</xsl:when>
			<xsl:otherwise>
				<link rel="stylesheet" href="&lt;?=libUrl?&gt;webComponents/jquery/css/jquery.fileupload-ui.css" id="theme" />
				<!--script type="text/javascript" src="&lt;?=libUrl?&gt;webComponents/jquery/js/tmpl.min.js">&#160;</script>
				<script type="text/javascript" src="&lt;?=libUrl?&gt;webComponents/jquery/js/load-image.min.js">&#160;</script>

				<script type="text/javascript" src="&lt;?=libUrl?&gt;webComponents/jquery/js/jquery.iframe-transport.js">&#160;</script>
				<script type="text/javascript" src="&lt;?=libUrl?&gt;webComponents/jquery/js/jquery.fileupload.js">&#160;</script>
				<script type="text/javascript" src="&lt;?=libUrl?&gt;webComponents/jquery/js/jquery.fileupload-fp.js">&#160;</script>
				<script type="text/javascript" src="&lt;?=libUrl?&gt;webComponents/jquery/js/jquery.fileupload-ui.js">&#160;</script>
				<script type="text/javascript" src="&lt;?=libUrl?&gt;webComponents/jquery/js/jquery.fileupload-jui.js">&#160;</script-->

				<xsl:processing-instruction name="php">include(getSiteFile('formDir','importForm.inc.php'));</xsl:processing-instruction>
				<!--xsl:processing-instruction name="php">include(getSiteFile('formDir','upload.inc.php'));</xsl:processing-instruction-->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>



	<!-- Un import existe et a été xml_export avant le passage en XSL => on est dans le cas "vue d'un import existant"-->
	<xsl:if test="t_import!=''">
		<form id="import_aff_form" name="documentSelection" method="post" action="index.php?urlaction=importView{$urlparams}">
			<input type="hidden" name="page" value="{$page}"  />
			<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
			<input type="hidden" name="style" value=""  />
			<input type="hidden" name="tri" value="{$tri}"  />
			<input type="hidden" name="tab" value="{$tab_shown}"  />
		<!-- xsl:call-template name="displayView">
			<xsl:with-param name="xmllist" select="$xmllist"/>
		</xsl:call-template -->


		<ul class="panelSelWrapper import_panelSelWrapper" style="width : 97%; margin : 6px auto;  ">
			<li class="panelSel" onclick="window.location.href='index.php?urlaction=importView{$urlparams}&amp;tab=doc'">
				<xsl:attribute name="class">panelSel <xsl:if test="$tab_shown='doc'">selected</xsl:if></xsl:attribute>
				<xsl:processing-instruction name="php">print kDocuments;</xsl:processing-instruction>
			</li>
			<li class="panelSel" onclick="window.location.href='index.php?urlaction=importView{$urlparams}&amp;tab=mat'">
				<xsl:attribute name="class">panelSel <xsl:if test="$tab_shown='mat'">selected</xsl:if></xsl:attribute>
				<xsl:processing-instruction name="php">print kMateriels;</xsl:processing-instruction>
			</li>
		</ul>
		<div id="resultats">
		<xsl:if test="$tab_shown='doc'">
			<div class="tab_imp_doc">
					<xsl:call-template name="pagerListe">
						<xsl:with-param name="form">documentSelection</xsl:with-param>
					</xsl:call-template>
					<script>
						str_media_filter = '&lt;div class="imp_media_filter"&gt;&lt;label&gt;&lt;?=kMedia?&gt;&lt;/label&gt;';
						str_media_filter+= '&lt;select onchange="&#36;j(this).parents(\'form\').submit();" name="filter_media"&gt;&lt;option value=""&gt;&lt;?=kTous?&gt;&lt;/option&gt;&lt;?=listOptions("SELECT id_media as ID, media as VAL from t_media;",array("ID","VAL"),(isset($_REQUEST['filter_media'])?$_REQUEST['filter_media']:null));?&gt;&lt;/select&gt;';
						str_media_filter+= '&lt;/div&gt;';
						&#36;j(str_media_filter).insertAfter("div.resultsMenu div.res_par_pages");
					</script>

					<table width="97%" align="center" cellspacing="0" class="tableResults"  >
						<tr class="resultsHead_row">
							<xsl:for-each select="$docListeXml/list/element">
								<xsl:if test="not(profil_min!='') or (profil_min!='' and $profil &gt;= profil_min)">
									<td class="resultsHead" width="{width}">
									<xsl:choose>
									<xsl:when test="tri!=''">
										<xsl:if test="contains($tri,tri)"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
										<a href="javascript:trier('{tri}{$ordre}')"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></a>
									</xsl:when>
									<xsl:when test="type='checkbox'">
										<input type="checkbox" name="checkAll" onclick="smartCheckAll(this,'#resultats')"  />
									</xsl:when>
									<xsl:otherwise>
										<xsl:processing-instruction name="php">
											if (defined('<xsl:value-of select="label"/>'))
												print <xsl:value-of select="label"/>;
											else
												print '<xsl:value-of select="label"/>';
										</xsl:processing-instruction>
									</xsl:otherwise>
									</xsl:choose>
									</td>
								</xsl:if>
							</xsl:for-each>
						</tr>
						<tr class="separator"><td ><xsl:attribute name="colspan"><xsl:value-of select="count($docListeXml/list/element)"/></xsl:attribute>&#160;</td></tr>
						 <xsl:for-each select="/select/t_imp_docs">
							<!--xsl:sort select="./*[name()=$tri]" order="ascending"/-->
							<xsl:variable name="row" select="child::*"/>
							<xsl:variable name="j"><xsl:number/></xsl:variable>
							<!--xsl:if test="position() &lt;= ($page * $nbLigne) and position() &gt; (($page - 1)* $nbLigne)"-->
								<xsl:call-template name="displayRow">
									<xsl:with-param name="xmllist" select="$docListeXml"/>
									<xsl:with-param name="row" select="$row"/>
									<xsl:with-param name="j" select="$j"/>
									<xsl:with-param name="div" select="0"/>
								</xsl:call-template>
							<!--/xsl:if-->
						</xsl:for-each>
					 </table>
			</div>
		</xsl:if>
		<xsl:if test="$tab_shown='mat'">
			<div class="tab_imp_mat">

					<xsl:call-template name="pagerListe">
						<xsl:with-param name="form">documentSelection</xsl:with-param>
					</xsl:call-template>
					<script>
						str_media_filter = '&lt;div class="imp_media_filter"&gt;&lt;label&gt;&lt;?=kMedia?&gt;&lt;/label&gt;';
						str_media_filter+= '&lt;select onchange="&#36;j(this).parents(\'form\').submit();" name="filter_media"&gt;&lt;option value=""&gt;&lt;?=kTous?&gt;&lt;/option&gt;&lt;?=listOptions("SELECT id_media as ID, media as VAL from t_media;",array("ID","VAL"),(isset($_REQUEST['filter_media'])?$_REQUEST['filter_media']:null));?&gt;&lt;/select&gt;';
						str_media_filter+= '&lt;/div&gt;';
						&#36;j(str_media_filter).insertAfter("div.resultsMenu div.res_par_pages");
					</script>
					<table width="97%" align="center" cellspacing="0" class="tableResults"  >
						<tr class="resultsHead_row">
							<xsl:for-each select="$matListeXml/list/element">
								<xsl:if test="not(profil_min!='') or (profil_min!='' and $profil &gt;= profil_min)">
									<td class="resultsHead" width="{width}">
									<xsl:choose>
									<xsl:when test="tri!=''">
										<xsl:if test="contains($tri,tri)"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
										<a href="javascript:trier('{tri}{$ordre}')"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></a>
									</xsl:when>
									<xsl:when test="type='checkbox'">
										<input type="checkbox" name="checkAll" onclick="selectAll(this)"  />
									</xsl:when>
									<xsl:otherwise>
										<xsl:processing-instruction name="php">
											if (defined('<xsl:value-of select="label"/>'))
												print <xsl:value-of select="label"/>;
											else
												print '<xsl:value-of select="label"/>';
										</xsl:processing-instruction>
									</xsl:otherwise>
									</xsl:choose>
									</td>
								</xsl:if>
							</xsl:for-each>
						</tr>
						<tr class="separator"><td ><xsl:attribute name="colspan"><xsl:value-of select="count($matListeXml/list/element)"/></xsl:attribute>&#160;</td></tr>
						 <xsl:for-each select="/select/t_imp_mats">
							<!--xsl:sort select="./*[name()=$tri]" order="ascending"/-->
							<xsl:variable name="row" select="child::*"/>
							<xsl:variable name="j"><xsl:number/></xsl:variable>
							<!--xsl:if test="position() &lt;= ($page * $nbLigne) and position() &gt; (($page - 1)* $nbLigne)"-->
								<xsl:call-template name="displayRow">
									<xsl:with-param name="xmllist" select="$matListeXml"/>
									<xsl:with-param name="row" select="$row"/>
									<xsl:with-param name="j" select="$j"/>
									<xsl:with-param name="div" select="0"/>
								</xsl:call-template>
							<!--/xsl:if-->
						</xsl:for-each>
					 </table>
			</div>
		</xsl:if>
		</div>
        <xsl:if test="$gReportagePicturesActiv">
            <div class="formBoutons" style="text-align : center;">
                <button type="button" style="margin : 4px auto;" class="std_button_style" onclick="attachDocsToNewReport('{/select/t_import/IMP_NOM}','{/select/t_imp_docs/DOC_ID_FONDS}')"><xsl:processing-instruction name="php">print kCreerReportage</xsl:processing-instruction></button>
            </div>
        </xsl:if>
		</form>
	</xsl:if>
	</div>
	</xsl:template>

</xsl:stylesheet>
