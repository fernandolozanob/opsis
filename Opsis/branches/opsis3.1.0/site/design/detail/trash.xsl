<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:include href="../detail.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="gestPers"/>
<xsl:param name="scripturl" />
<xsl:param name="xmlfile" />
<xsl:param name="profil" />
<xsl:param name="id_priv_us_doc" />
<xsl:param name="state_notExist" />
<xsl:param name="state_inTrash" />
<xsl:param name="state_deleted" />
<xsl:param name="state_restored" />
<xsl:param name="id_doc_restored" />

	<xsl:template match='EXPORT_OPSIS'>

	<xsl:for-each select="t_trash">
		<xsl:choose>
			<xsl:when test="state_num = $state_restored">
				<br/>
				Le document a été restauré.<br/>
				<a href="indexPopup.php?urlaction=doc&amp;id_doc={$id_doc_restored}">Ouvrir le document</a><br/>
				<a href="index.php?urlaction=trashListe">Retourner dans la corbeille</a>
			</xsl:when>
			<xsl:when test="state_num = $state_notExist">
				<br/>
				Ce document a été définitivement supprimé.<br/>
				<a href="index.php?urlaction=trashListe">Retourner dans la corbeille</a>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="xml_name"><xsl:choose>
					<xsl:when test="TRA_ENTITE='mat'">matAff</xsl:when>
					<xsl:otherwise>docAff</xsl:otherwise>
				</xsl:choose></xsl:variable>
				<xsl:variable name="xmllist" select="document(concat('xml/',$xml_name,'.xml'))"/>
				<xsl:for-each select="TRA_XML">
					<script>
						var id_doc = "<xsl:value-of select="t_doc/ID_DOC" />";
						var doc_id_gen = "<xsl:value-of select="t_doc/DOC_ID_GEN" />";
					</script>
					<div id="fiche_info" class="trash">
						<div id="desc" class="trash">
							<xsl:call-template name="displayView">
								<xsl:with-param name="xmllist" select="$xmllist"/>
							</xsl:call-template>
					
						   <xsl:if test="t_doc/t_doc_mat/DOC_MAT!=''">
								<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="field_description">
								<legend><xsl:processing-instruction name="php">print kMateriels;</xsl:processing-instruction></legend>
						
							        <table width="50%"  border="0" align="center" cellspacing="0" class="tableResults">
							            <tr>
							                <td class="resultsHead"><xsl:processing-instruction name="php">print kReferenceMateriel;</xsl:processing-instruction></td>
							                <td class="resultsHead"><xsl:processing-instruction name="php">print kFormat;</xsl:processing-instruction></td>
							                <td class="resultsHead"><xsl:processing-instruction name="php">print kTCin;</xsl:processing-instruction></td>
							                <td class="resultsHead"><xsl:processing-instruction name="php">print kTCout;</xsl:processing-instruction></td>
										</tr>
							            <xsl:for-each select="t_doc/t_doc_mat/DOC_MAT">
							                <xsl:variable name="j"><xsl:number/></xsl:variable>
							                <tr class="{concat('altern',$j mod 2)}">
							                    <td class="resultsCorps"><xsl:value-of select="ID_MAT"/></td>
							                    <td class="resultsCorps"><xsl:value-of select="t_mat/MAT_FORMAT" /></td>
							                    <td class="resultsCorps"><xsl:value-of select="DMAT_TCIN"/></td>
							                    <td class="resultsCorps"><xsl:value-of select="DMAT_TCOUT"/></td>
							                 </tr>
							            </xsl:for-each>
							        </table>
								</fieldset>
							</xsl:if>
						
							<xsl:if test="count(t_doc/t_doc_acc/t_doc_acc)>0">
								<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="doc_acc">
								<legend><xsl:processing-instruction name="php">print kDocumentAcc;</xsl:processing-instruction></legend>
									<table>
									<tr>
									<td class="resultsHead"  width="100"><xsl:processing-instruction name="php">print kFichier;</xsl:processing-instruction></td>
									<td class="resultsHead"  width="100"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
									</tr>
									<xsl:for-each select="t_doc/t_doc_acc/t_doc_acc">
										<tr>
											<td class="resultsCorps"><xsl:value-of select="DA_FICHIER"/></td>
											<td class="resultsCorps"><xsl:value-of select="DA_TITRE"/></td>
										</tr>
									</xsl:for-each>
									</table>
								</fieldset>
							</xsl:if>
							
							<xsl:if test="count(t_doc_seq/t_doc)>0">
								<fieldset class="cadre_infos_video_detaille"  id="t_doc_seq">
								<legend><xsl:processing-instruction name="php">print kSequences;</xsl:processing-instruction></legend>
									<div class="contenu_table_detail_video">
										<table border="0" cellspacing="0" cellpadding="0" class="table_details_video">
											<tr class="tr_border_bottom">
												<td><span><xsl:processing-instruction name="php">print kCote;</xsl:processing-instruction></span></td>
												<td><span><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></span></td>
												<td><span><xsl:processing-instruction name="php">print kTCin;</xsl:processing-instruction></span></td>
												<td><span><xsl:processing-instruction name="php">print kTCout;</xsl:processing-instruction></span></td>
											</tr>
											<xsl:for-each select="t_doc_seq/t_doc">
												<tr class="tr_border_bottom">
													<td><xsl:value-of select="DOC_COTE"/></td>
													<td><xsl:value-of select="DOC_TITRE"/></td>
													<td><xsl:value-of select="DOC_TCIN"/></td>
													<td><xsl:value-of select="DOC_TCOUT"/></td>
												</tr>
											</xsl:for-each>
										</table>
									</div>
								</fieldset>
							</xsl:if>
						</div>
					</div>
				     <script>
						var current_form ; 
						
						if(document.getElementById("trash_form1")!=null){
							current_form = document.getElementById("trash_form");
						}else{
							current_form = document.form1;
						}
										
						var urlDeft = current_form.page.value;
						function removeTrash(url) {
						    if (confirm("Voulez-vous supprimer cet élément ?")) {
						        current_form.commande.value="SUP";
						        current_form.page.value=url;
						        current_form.submit();
						    }
						}
						
						function restoreTrash(url) {
							var msg;
							urlDeft = current_form.page.value;
							if(doc_id_gen &amp;&amp; doc_id_gen != 0) {
								current_form.page.value=url;
								urlAjax = 'commande=parentIsTrashedOrDeleted&amp;xmlhttp=1&amp;doc_id_gen='+doc_id_gen;
								sendData('GET','ajax.php',urlAjax,'restoreTrash_Valid');
							} else {
								restoreTrash_Valid('0');
							}
						}
						
						function restoreTrash_Valid(str) {
							var msg;
							if(parseInt(str) == 0) {
								msg = "Voulez-vous restaurer cet élément ?";
							    if (confirm(msg)) {
							        current_form.commande.value="RESTORE";
							        current_form.submit();
							    }
							} else {
								doc_cote="<xsl:value-of select="t_doc/DOC_XML/XML/DOC/PARENT_COTE" />";
								doc_titre="<xsl:value-of select="t_doc/DOC_XML/XML/DOC/PARENT_TITRE" />";
								msg = "Ce document est une séquence. Veuillez d'abord restaurer son parent : \nRéférence : "  + doc_cote + "\nTitre : " + doc_titre;
								alert(msg);
								current_form.page.value = urlDeft;
							}
						}
						
						
					</script>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>
