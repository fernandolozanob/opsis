<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:include href="../detail.xsl"/>
<xsl:include href="../liste.xsl"/>
<xsl:output
	method="xml"
	encoding="utf-8"
	omit-xml-declaration="yes"
	indent="yes"
/>

<!-- recuperation de parametres PHP -->

	<xsl:param name="profil" />
	<xsl:param name="storyboardChemin" />
	<xsl:param name="etat"/>
	<xsl:param name="playerVideo"/>
	<xsl:param name="ordre"/>
	<xsl:param name="tri"/>
	<xsl:param name="scripturl"/>
	<xsl:param name="cartList"/>
	<xsl:param name="nbLigne" />
	<xsl:param name="page" />
	<xsl:param name="pager_link" />
	<xsl:param name="titre" />
	<xsl:param name="urlparams" />
	<xsl:param name="frameurl" />
	<xsl:param name="nb_rows" />
	<xsl:param name="offset" />
	<xsl:param name="fromOffset" />
	<xsl:param name="affMode" />
	
	<xsl:template match='EXPORT_OPSIS'>
		<div id="docHeader" class="titre_1">
			<div id="pageTitre">
				<xsl:value-of select="t_categorie/CAT_NOM" />&amp;nbsp;
			</div>
		</div>
		<div id="docBody">
			<div class="intro_home" ><xsl:value-of select="t_categorie/CAT_DESC" />&amp;nbsp;</div>
			<xsl:if test="t_categorie/t_sub_cat!=''">
				<div id="fiche_info3" style="position: relative;overflow: hidden;min-height: 200px;">
					<div id="selmedia">
						<xsl:for-each select="t_categorie/t_sub_cat/CAT/t_categorie">
							<xsl:variable name="doc_titre">
								<xsl:call-template name="remove_html_tags">
									<xsl:with-param name="node" select="CAT_NOM" />
								</xsl:call-template>
							</xsl:variable>
							
							<div class="resultsMos bloc_image conteneur_dragable" style="-o-text-overflow: ellipsis; text-overflow: ellipsis;border: 1px solid #aaa; height: 180px;">
								<div onclick="document.location='index.php?urlaction=cat&amp;id_cat={ID_CAT}'" style="cursor:pointer" class="bgVignetteMos">
									<a href="index.php?urlaction=cat&amp;id_cat={ID_CAT}" title="{$doc_titre}" class="span_consult">
										&lt;?=kDInfo;?&gt;
									</a>
									<xsl:if test="CAT_IMAGE!=''">
										<img src="makeVignette.php?image=/doc_acc/{CAT_IMAGE}&amp;amp;w=200&amp;amp;h=150&amp;amp;kr=cover" border="0"  title="{$doc_titre}"/>
									</xsl:if>
									<xsl:if test="CAT_IMAGE=''">
										<img src="design/images/iconmedia-S.jpg" border="0" height="150"   title="{$doc_titre}" />
									</xsl:if>
								</div>
								<div class="mosTitre" style="">
									<xsl:if test="$doc_titre!=''">
										<a href="index.php?urlaction=cat&amp;id_cat={ID_CAT}" title="{$doc_titre}"><xsl:value-of select="$doc_titre"/></a>
									</xsl:if>&#160;
								</div>
							</div>
						</xsl:for-each>&amp;nbsp;
					</div>
				</div>
			</xsl:if>
			<div id="fiche_info2" style="position: relative;top: 0px;bottom: 0px;right: 0px;left:0px; width: auto; font-size: 12px; overflow: auto">
				<div class="filet"><hr /></div>
				<xsl:if test="t_categorie/t_doc_cat!=''">
					<span class="title_bar" style="padding: 8px 0px;"><xsl:value-of select="count(t_categorie/t_doc_cat)" />&#160;<xsl:processing-instruction name="php">print kDocuments;</xsl:processing-instruction></span>
					<div id="selmedia">
						<xsl:for-each select="t_categorie/t_doc_cat/DOC_CAT">
							<xsl:call-template name="mosDoc">
								<xsl:with-param name="context">t_doc</xsl:with-param>
							</xsl:call-template>
						</xsl:for-each>&amp;nbsp;
						<!--div class="separator">&amp;nbsp;</div-->
					</div>
				</xsl:if>
			</div>
		</div>
    </xsl:template>


</xsl:stylesheet>
