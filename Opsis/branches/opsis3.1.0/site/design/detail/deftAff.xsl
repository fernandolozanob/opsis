<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:include href="../detail.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 


<!-- recuperation de parametres PHP -->
<xsl:param name="loggedIn" />
<xsl:param name="tab" />
<xsl:param name="docAccChemin" />
<xsl:param name="rang" />
<xsl:param name="xmlfile" />

	<xsl:template match='EXPORT_OPSIS'>

	<xsl:variable name="xmllist" select="document($xmlfile)"/>
	
	<div id="docBody">
		<xsl:call-template name="displayView">
			<xsl:with-param name="xmllist" select="$xmllist"/>
		</xsl:call-template>
	</div>
	
    </xsl:template>
    
</xsl:stylesheet>
