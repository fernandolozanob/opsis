<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="fonctions.xsl"/>
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<xsl:param name="gestPers"/>
<xsl:param name="scripturl" />
<xsl:param name="tab" />
<xsl:param name="docAccChemin"/>
<xsl:param name="nbPers"/>
<xsl:param name="loggedIn"/>

	<xsl:template match='EXPORT_OPSIS'>
	

		
		<table align="center" cellspacing="0" class="tableResults" >
		<tr valign="top">
		<td width="90%" valign="top">
		<div id='menuDoc'>
			
			<a href='javascript:void(null);' class='selected' related_div='showDesc' id='showDescBtn' onclick="toggleDisplay(this)" ><img src="design/images/world.png" border="0" title="&lt;?php echo kDescription; ?&gt;" align="absmiddle"/><xsl:processing-instruction name="php">print kDescription;</xsl:processing-instruction></a>

			<xsl:if test="count(t_festival/t_doc_acc)!=0">
				<a href='javascript:void(null);' related_div='showDocAcc' id='showDocAccBtn' onclick="toggleDisplay(this);" ><img src="design/images/attach.png" border="0" title="&lt;?php echo kFichesScannees; ?&gt;" align="absmiddle"/><xsl:processing-instruction name="php">print kPiecesJointes</xsl:processing-instruction></a>
			</xsl:if>



		</div>

		<div id="showDesc">

<xsl:for-each select="t_festival">
<xsl:variable name="id_lang"><xsl:value-of select="ID_LANG"/></xsl:variable>

	<fieldset class="vue_fieldset" width="90%">
		<legend>&lt;?=kDescription?&gt;</legend>
		<table width="90%">

			<tr>
			<td class="label_champs_form" width="150">&lt;?=kAnnee?&gt;</td>
			<td class="val_champs_form">
				<xsl:value-of select="substring(FEST_ANNEE,1,4)" />
			</td>
			</tr>
			<xsl:if test="FEST_LIBELLE!=''">
			<tr>
			<td class="label_champs_form" width="150">&lt;?=kLibelle?&gt;</td>
			<td class="val_champs_form" style='font-size:16px'>
				<xsl:value-of select="FEST_LIBELLE" />	
			</td>
			</tr>
			</xsl:if>
			<xsl:if test="FEST_DESCRIPTION!=''">
			<tr>
			<td class="label_champs_form" width="150">&lt;?=kDescription?&gt;</td>
			<td class="val_champs_form">
				<xsl:value-of select="FEST_DESCRIPTION" />	
			</td>
			</tr>	
			</xsl:if>		
							
		</table>
		</fieldset>
		
	<fieldset class="vue_fieldset" width="90%">
		<legend>&lt;?=kJury?&gt;</legend>
		<table width="90%">
		<tr><td>
		<xsl:for-each select="t_fest_pers/TYPE_DESC[@ID_TYPE_DESC='JUR']/t_rol[@DLEX_ID_ROLE='JUR']/t_personne" >
			<xsl:sort select="LEX_PRECISION"/>
			<strong><xsl:value-of select="LEX_PRECISION"/> : </strong>
			<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/>&#160;</xsl:if>
			<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/>&#160;</xsl:if>
			<xsl:value-of select="PERS_NOM"/>&#160;
			<a href='{$scripturl}?urlaction=personne&amp;id_pers={ID_PERS}'>
			<img src='design/images/user_go.png' border='0'/></a><br/>
		</xsl:for-each>	
		</td></tr>					
		</table>	
	</fieldset>
	
	<fieldset class="vue_fieldset" width="90%">
		<legend>&lt;?=kSection?&gt;</legend>
		<ul>
		<xsl:for-each select="t_doc_acc/t_doc_acc">
		<xsl:sort select="t_doc_acc_val/t_val/t_val/VALEUR"/>
		<xsl:if test="t_doc_acc_val/t_val/t_val/ID_VAL='805'" >
		
			<xsl:variable name="im"><xsl:value-of select="DA_CHEMIN"/><xsl:value-of select="DA_FICHIER"/></xsl:variable>
			<li><a href="{concat('&lt;?php echo kDocumentUrl ?&gt;',$im)}" target="_blank">
				<xsl:if test="t_doc_acc_val/t_val/t_val/VALEUR!=''"><xsl:value-of select="t_doc_acc_val/t_val/t_val/VALEUR"/> : </xsl:if>
				<xsl:value-of select="DA_TITRE"/> (<i><xsl:value-of select="DA_FICHIER"/></i>)</a>
			</li>
		</xsl:if>
		</xsl:for-each>
		</ul>

		<xsl:for-each select="t_section/FEST">
		<a name="{ID_FEST}" style="font-size:0px">&#160;</a>
		<table width="90%" style='border:1px solid black;margin:10px;'>

			<tr>
			<td class="label_champs_form" width="90">&lt;?=kLibelle?&gt;</td>
			<td class="val_champs_form" style="font-size:16px">
				<strong><xsl:value-of select="FEST_LIBELLE" /></strong>
			</td>
			</tr>
			<xsl:if test="FEST_DESCRIPTION!=''">
			<tr>
			<td class="label_champs_form">&lt;?=kDescription?&gt;</td>
			<td class="val_champs_form">
				<xsl:value-of select="FEST_DESCRIPTION" />	
			</td>
			</tr>
			</xsl:if>
			
			<tr>
			<td class="label_champs_form">&#160;</td>
			<td class="val_champs_form">
				<table style="font-size:11px;font-family:'Trebuchet MS',Arial,sans-serif;" width="90%">	
					<tr>
					<th>&lt;?=kTitreOriginal?&gt;<br/><i>&lt;?=kTitreTraduit?&gt;</i></th>
					<th width='50'>&lt;?=kAnnee?&gt;</th>
					<th width='120'>&lt;?=kRealisateur?&gt;</th>
					<th width='150'>&lt;?=kPrix?&gt;</th>
					<th width='20'>&#160;</th>
					</tr>
				<xsl:for-each select="FESTIVAL/t_festival/t_fest_doc/DOC">
				<xsl:sort select="DOC_TITREORI"/>
					<tr>
						<td>
		
						<xsl:if test="DOC_SOUSTITRE!=''">(<xsl:value-of select="DOC_SOUSTITRE"/>)&#160;</xsl:if>
						<xsl:value-of select="DOC_TITREORI"/>												
							<xsl:if test="($id_lang ='fr' or $id_lang='FR') and DOC_TITRE!=DOC_TITREORI"><br/><i>
								<xsl:if test="DOC_TITRE_COL!=''">(<xsl:value-of select="DOC_TITRE_COL"/>)&#160;</xsl:if>
								<xsl:value-of select="DOC_TITRE"/>				
							</i></xsl:if>
							<xsl:if test="($id_lang ='en' or $id_lang='EN')and DOC_AUTRE_TITRE!=DOC_TITREORI"><br/><i>
								<xsl:if test="DOC_COTE_ANC!=''">(<xsl:value-of select="DOC_COTE_ANC"/>)&#160;</xsl:if>
								<xsl:value-of select="DOC_AUTRE_TITRE"/>				
							</i></xsl:if>								
						</td>
						<td>
							<xsl:value-of select="substring(DOC_DATE_PROD,1,4)"/>
						</td>
						<td>
							<xsl:for-each select="DOC_LEX/XML/LEX[@ROLE='REA']">
								<xsl:if test="normalize-space(TERME/PERS_PRENOM)!=''"><xsl:value-of select="normalize-space(TERME/PERS_PRENOM)"/>&#160;</xsl:if>
								<xsl:if test="normalize-space(TERME/PERS_PARTICULE)!=''"><xsl:value-of select="normalize-space(TERME/PERS_PARTICULE)"/>&#160;</xsl:if>
								<xsl:value-of select="normalize-space(TERME/PERS_NOM)"/>
								<xsl:if test="position()!=last()">, </xsl:if>
							</xsl:for-each>
						</td>
						<td>
							<xsl:for-each select="DOC_FEST_VAL">
								<xsl:if test="ID_TYPE_VAL='PRIX'"><xsl:value-of select="VALEUR"/></xsl:if>
							</xsl:for-each>
						</td>
						<td><a href="{$scripturl}?urlaction=doc&amp;id_doc={ID_DOC}"><img src='design/images/film_go.png' border="0"/></a>
						</td>				
						
					</tr>		
				</xsl:for-each>
				</table>
			
			</td>
			</tr>	
							
		</table>
		</xsl:for-each>
		
	</fieldset>		
		
		</xsl:for-each>
	</div>


<div id='showDocAcc' style="display:none">
	<ul>
	<xsl:for-each select="t_festival/t_doc_acc/t_doc_acc">
	<xsl:sort select="t_doc_acc_val/t_val/t_val/VALEUR"/>
			<xsl:variable name="im"><xsl:value-of select="DA_CHEMIN"/><xsl:value-of select="DA_FICHIER"/></xsl:variable>
		<li><a href="{concat('&lt;?php echo kDocumentUrl ?&gt;',$im)}" target="_blank">
			<xsl:if test="t_doc_acc_val/t_val/t_val/VALEUR!=''"><xsl:value-of select="t_doc_acc_val/t_val/t_val/VALEUR"/> : </xsl:if>
			<xsl:value-of select="DA_TITRE"/> (<i><xsl:value-of select="DA_FICHIER"/></i>)
		</a>
		</li>
	</xsl:for-each>
	</ul>
</div>

</td>
</tr>
</table>
	

<xsl:if test="$tab" >
<script>
	//Auto sélection d'un onglet si celui-ci a été correctement passé en paramètre tab= dans l'url
	myTab=document.getElementById('<xsl:value-of select="$tab"/>Btn');
	if (myTab) {
		toggleDisplay(myTab);
	}
</script>
</xsl:if>

    </xsl:template>
    
</xsl:stylesheet>
