<?php


    require_once(libDir.'fonctionsGeneral.php');
	global $db;
    
    $myUpload->arrParams['arrParams']['maxFileCount']=1;
    $myUpload->arrParams['arrParams']['maxTotalSize']=10000000;
    $myUpload->arrParams['arrParams']['maxFileSize']=10000000;
    $myUpload->arrParams['arrParams']['strFilesFormat']='jpg|jpeg|png|gif';
	
	$arrFormats=array_merge(unserialize(gVideoTypes),unserialize(gImageTypes),unserialize(gAudioTypes),unserialize(gDocumentTypes)); 
	
	$layout= $_SESSION['USER']['layout_opts']['layout'];
	
    ?>
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/upload.js"></script>
<script type="text/javascript">

ext_allowed_regex = <? switch(strtolower($_GET['media_type'])){
	case 'p' :
		echo "/(\.|\/)(".implode('|',unserialize(gImageTypes)).")$/i";
        break;
	case 'v' :
		echo "/(\.|\/)(".implode('|',unserialize(gVideoTypes)).")$/i";
        break;
	case 'a' :
		echo "/(\.|\/)(".implode('|',unserialize(gAudioTypes)).")$/i";
        break;
	case 'd' :
		echo "/(\.|\/)(".implode('|',unserialize(gDocumentTypes)).")$/i";
        break;
	case 'subtitle' :
		echo "/(\.|\/)(" . implode('|', unserialize(gSubtitleTypes)) . ")$/i";
		break;
	default :
		echo "/(\.|\/)(".implode('|',$arrFormats).")$/i";
        break ; 
}?>;

ext_not_allowed='';
max_upload_files='1';
max_upload_size=10 // 10Mo
descr_mode=false;
pass_required=false;
email_required=false;

</script>
<style type="text/css">
#upload_form{ 
width : 650px;
<? if($_GET['type'] != 'batch_mat' && $_GET['media_type'] != 'subtitle'){?>
	display : none
    <?}?>

}

#doc_form {width: 350px;}
</style>
<!--div id="importViewBar" class="title_bar pres_<?=$layout?>"><?=kNouvelImport?></div>
<div id="menuGauche" class="<?=(($layout<1 || $layout>2)?'collapsed':'')?>">
	<div class="title_bar"><?=kDerniersImports?><span id="arrow_left" class="toggle_arrow" onclick="toggleLeftMenu();">&#160;</span></div>
	<div id="import_wrapper">&#160;</div>
</div-->

<!--div id="import_form_wrapper" class="main_scrollableDiv apply_layout pres_<?=$layout;?>"-->

<?php if (strtolower($_GET['media_type'] == 'subtitle')) { ?>


	<script>

	$j(document).ready(function () {
	   $j("input[name='override_subtitle']").prop('checked', false);
	   $j("input[name='override_subtitle']").parent('div').hide();
	   
	   $j("select[name='t_mat_val\\[LANG\\]']").change(function () {
		   if ($j(this).val() !== '') {
		   
		   $j.get("empty.php?urlaction=subtitle&upload_isExistsSubtitleInThisLanguage&id_doc=<?php echo $_REQUEST['id_doc'] ?>&id_val=" + $j(this).val(), function (data) {
				  if (data == 'true') {
				  $j("input[name='override_subtitle']").parent('div').show();
				  } else {
				  $j("input[name='override_subtitle']").parent('div').hide();
				  }
				  
				  });
		   
		   } else {
		   $j("input[name='override_subtitle']").parent('div').hide();
		   }
		   
		   
		   });
	   
	   
					   
					   });

	</script>



	<?
	}


	$id_proc_imp_video = 1;
	$id_proc_imp_image = 2;
	$id_proc_imp_document = 6;
	$id_proc_imp_audio = 5;
	// on différencie le processus document et pdf car si pdf pas encodage, on envoie le parametre a jobliste 	
	$id_proc_imp_pdf = 4;
	
	
    if (isset($_GET['type']) && ($_GET['type']=='batch_mat' || $_GET['type']=='doc_acc' || $_GET['type']=='doc_mat'))
    {
        
        require_once(modelDir.'model_doc.php');
        $dumbDoc=new Doc;
        //ob_start();
        include(libDir."class_formSaisie.php");
        $myForm=new FormSaisie();
        $myForm->saisieObj=$dumbDoc;
        $myForm->classLabel="miniField label_miniField";
        $myForm->classValue="miniField val_miniField";
        
        
		$params_finUpload = '';
		if ($_GET['media_type'] == 'subtitle') {
			$params_finUpload.='&subtitle';
		}
		
        //$myForm->beginTag("FIELDSET",array("ID"=>"form1","ACTION"=>"indexPopupUpload.php?urlaction=finUpload&type=".$_GET['type'],"LEGEND"=>kUploadParametres));
		if (!isset($id))
			$id='';
		
        echo '<form action="indexPopupUpload.php?urlaction=finUpload&type='.$_GET['type'].$id.$params_finUpload.'"  method="post" name="form_envoi_fichiers" id="form_envoi_fichiers">';
        
        
        
        $myForm->beginTag("FIELDSET",array("ID"=>"upload_form"));
        
        if($_GET['type']!='doc_acc') {
			$myForm->displayElt(array("NAME"=>"MAT_TYPE","TYPE"=>"hidden","VALUE"=>"MASTER","DIVTAGS"=>"none"));
		}
		if($_GET['type'] == 'batch_mat' && (!isset($_GET['rtn']) || !in_array($_GET['rtn'],array('refresh_page')))){
			$myForm->displayElt(array("NAME"=>"redirectImportView","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
		}
		$myForm->displayElt(array("NAME"=>"xsl","TYPE"=>"hidden","VALUE"=>"mappingDocPhoto","DIVTAGS"=>"both"));

		
        /*if (strtolower($_GET['media_type'])=='p') {
		
			$myForm->displayElt(array("NAME"=>"MAT_TYPE","TYPE"=>"hidden","VALUE"=>"MASTER","DIVTAGS"=>"none"));
			$myForm->displayElt(array("NAME"=>"MAT_CHEMIN","TYPE"=>"hidden","VALUE"=>date("Ymd"),"DIVTAGS"=>"none"));
			$myForm->displayElt(array("NAME"=>"MAT_LIEU","TYPE"=>"hidden","VALUE"=>"images","DIVTAGS"=>"none"));
			$arrFormats=unserialize(gImageTypes);
            
        }elseif (strtolower($_GET['media_type'])=='a') {
			$myForm->displayElt(array("NAME"=>"MAT_TYPE","TYPE"=>"hidden","VALUE"=>"MASTER","DIVTAGS"=>"none"));
            $arrFormats=unserialize(gAudioTypes);
            
        }elseif (strtolower($_GET['media_type'])=='d') {
			$myForm->displayElt(array("NAME"=>"MAT_TYPE","TYPE"=>"hidden","VALUE"=>"MASTER","DIVTAGS"=>"none"));
            $arrFormats=unserialize(gDocumentTypes);
        }else{
			$myForm->displayElt(array("NAME"=>"MAT_TYPE","TYPE"=>"hidden","VALUE"=>"MASTER","DIVTAGS"=>"none"));
            $_GET['media_type'] = 'v';
            $arrFormats=unserialize(gVideoTypes); 
        }*/
		if($_GET['type']!='doc_acc') {
			$myForm->displayElt(array("NAME"=>"lancement","TYPE"=>"hidden",'VALUE'=>'proc'));
			$myForm->displayElt(array("NAME"=>"id_proc","TYPE"=>"hidden",'VALUE'=>'*'));
			$myForm->displayElt(array("NAME"=>"proc_critere","TYPE"=>"hidden","VALUE"=>"{\"9\":\"&lt;MAT_FORMAT&gt;DCP-CPL&lt;/MAT_FORMAT&gt;\",\"1\":\"&lt;MAT_ID_MEDIA&gt;V&lt;/MAT_ID_MEDIA&gt;\",\"5\":\"&lt;MAT_ID_MEDIA&gt;A&lt;/MAT_ID_MEDIA&gt;\",\"3\":\"&lt;MAT_FORMAT&gt;PDF&lt;/MAT_FORMAT&gt;\",\"4\":\"&lt;MAT_ID_MEDIA&gt;D&lt;/MAT_ID_MEDIA&gt;\",\"2\":\"&lt;MAT_ID_MEDIA&gt;P&lt;/MAT_ID_MEDIA&gt;\"}"));
        }
		// $myForm->displayElt(array("NAME"=>"id_proc_pdf","TYPE"=>"hidden","VALUE"=>$id_proc_imp_pdf));

		if (!isset($_GET['rename']))
			$_GET['rename']='';
		
		if (!isset($_GET['type']))
			$_GET['type']='';
		
		if (!isset($_GET['mode']))
			$_GET['mode']='';
		
		if (!isset($_GET['id_doc']))
			$_GET['id_doc']='';
		
		$myForm->displayElt(array("NAME"=>"checkbox_rename","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
		
        // $myForm->displayElt(array("NAME"=>"DOC_ID_MEDIA","TYPE"=>"hidden","VALUE"=>strtoupper($_GET['media_type'])));
        $myForm->displayElt(array("NAME"=>"rename","TYPE"=>"hidden","VALUE"=>$_GET['rename'],"DIVTAGS"=>"none"));
        $myForm->displayElt(array("NAME"=>"type","TYPE"=>"hidden","VALUE"=>$_GET['type'],"DIVTAGS"=>"none"));
        $myForm->displayElt(array("NAME"=>"mode","TYPE"=>"hidden","VALUE"=>$_GET['mode'],"DIVTAGS"=>"none"));
        $myForm->displayElt(array("NAME"=>"id_doc","TYPE"=>"hidden","VALUE"=>$_GET['id_doc'],"DIVTAGS"=>"none"));
        
		if(!empty($_GET['id_pers'])) {
			$myForm->displayElt(array("NAME"=>"id_pers","TYPE"=>"hidden","VALUE"=>$_GET['id_pers'],"DIVTAGS"=>"none"));
		}
        
		if ($_GET['type'] == 'doc_mat') {
			
			if (strtolower($_GET['media_type']) == 'subtitle' && defined('kSousTitrage_lang')) {
				
				$kSousTitrage_lang = unserialize(kSousTitrage_lang);
				$supported_langS = implode(',', $kSousTitrage_lang["supported"]);
				
				$myForm->displayElt(array("NAME" => "t_mat_val[" . $kSousTitrage_lang['id_type_val'] . "]", "TYPE" => "select", "LABEL" => kLangue, "SELECT_SQL" => "SELECT id_val AS ID,valeur AS VAL FROM t_val WHERE ID_LANG='FR' AND id_val in($supported_langS) ", "NOALL" => "1", "DIVTAGS" => "both"));
				
				$myForm->displayElt(array("NAME" => "override_subtitle", "TYPE" => "checkbox", "CHECKBOX_LIB" => kSousTitrage_uploadOverride, "CHECKBOX_OPTION" => "1", "VALUE" => "1", "DIVTAGS" => "both", 'CHECKED' => false));
			}
			
			$myForm->displayElt(array("NAME" => "id_doc", "TYPE" => "hidden", "VALUE" => $_GET['id_doc'], "DIVTAGS" => "none"));
			if (isset($_REQUEST['dmat_inactif'])) {
				$myForm->displayElt(array("NAME" => "DMAT_INACTIF", "TYPE" => "hidden", "VALUE" => (int) $_REQUEST['dmat_inactif'], "TRTAGS" => "none"));
			}
		}
		
        
        if($_GET['type'] == 'batch_mat'){
            
            $myForm->displayElt(array("NAME"=>"makedoc","TYPE"=>"checkbox","CLASSVALUE"=>"miniField make_doc_main","ONCLICK"=>"toggleFields(this,document.getElementById('doc_form'));","CHECKBOX_LIB"=>kGenererDoc,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
            $myForm->beginTag("FIELDSET",array("ID"=>"doc_form"));
			 $myForm->beginTag("DIV",array("ID"=>"checkbox_form","CLASS"=>"val_miniField miniField" ));
			$myForm->displayElt(array("NAME"=>"DOC_ACCES","CLASSLABEL"=>"val_miniField miniField","TYPE"=>"checkbox",'CHECKBOX_LIB'=>kAcces,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
            $myForm->displayElt(array("NAME"=>"doc_trad","CLASSLABEL"=>"val_miniField miniField","TYPE"=>"checkbox",'CHECKBOX_LIB'=>kGenererAutresVersions,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
			// if(defined("gUploadToSelection") && gUploadToSelection){
				// $myForm->displayElt(array("NAME"=>"uploadToSelection","TYPE"=>"checkbox","CHECKBOX_LIB"=>kUploadToSelection,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
            // }
			$myForm->endTag("DIV");
			$myForm->displayElt(array("NAME"=>"DOC_ID_FONDS",'LABEL'=>kFonds,"TYPE"=>"select","SELECT_SQL"=>"SELECT ID_FONDS AS ID,FONDS AS VAL FROM t_fonds WHERE ID_LANG='FR' ORDER BY FONDS ASC","DIVTAGS"=>"none",'NOALL'=>'1'));
            echo "<br />";
			$myForm->displayElt(array("NAME"=>"DOC_ID_TYPE_DOC",'LABEL'=>kType,"TYPE"=>"select","SELECT_SQL"=>"SELECT ID_TYPE_DOC AS ID,TYPE_DOC AS VAL FROM t_type_doc WHERE ID_LANG='FR'","DIVTAGS"=>"none",'NOALL'=>'0'));


			//Reportage
			if (isset($_GET['id_doc2link']) && intval($_GET['id_doc2link']) !== false){
				$myForm->displayElt(array("NAME"=>"id_doc2link","TYPE"=>"hidden","VALUE"=>intval($_GET['id_doc2link']),"DIVTAGS"=>"none"));
			}else {
				include(modelDir.'model_docReportage.php');
				if (Reportage::isReportageEnabled()) {
					$myForm->endTag("FIELDSET");
					$myForm->beginTag("FIELDSET",array("ID"=>"doc_form"));
					$myForm->displayElt(array("NAME"=>"id_doc2link",'LABEL'=>kLinkToReport." : ","TYPE"=>"select","SELECT_SQL"=>"SELECT '' as ID, '". kNon ."' as VAL
																															UNION
																														SELECT 'new' as ID, '". kNouveau ."' as VAL
																															UNION
																														SELECT ''||ID_DOC AS ID,DOC_TITRE AS VAL 
																														FROM t_type_doc t
																														INNER JOIN t_doc d ON d.doc_id_type_doc = t.id_type_doc AND d.ID_LANG='FR'
																														WHERE t.ID_LANG='FR' AND t.ID_TYPE_DOC = ".intval(gReportagePicturesActiv)."
																														ORDER BY ID"
																														,"DIVTAGS"=>"none",'NOALL'=>'1'
																														,"onchange" => "toggleReportName(this.value)"));
					$myForm->beginTag("DIV",array("ID"=>"div_reportName", "CLASS"=>"lineHidden"));
					$myForm->displayElt(array("NAME"=>"reportName",'LABEL'=>kNomReportage,"TYPE"=>"text","VALUE"=>kReportage." ".date("Y-m-d"),"DIVTAGS"=>"none"));
					$myForm->endTag("DIV");
				}
			}
			$myForm->displayElt(array("NAME"=>"uploadToReportage","TYPE"=>"hidden","VALUE"=>1,"DIVTAGS"=>"none"));
			
			$myForm->endTag("FIELDSET");
        }
		
		if (isset($_GET['rtn']))
			$myForm->displayElt(array("NAME"=>"rtn","TYPE"=>"hidden","VALUE"=>$_GET['rtn'],"DIVTAGS"=>"none"));
		
		
        $myForm->endTag("FIELDSET");
        
        // paramètres upload
        
        // gestion des processus de traitement
        
        // $myForm->displayElt(array("NAME"=>"proc_critere","TYPE"=>"hidden",'VALUE'=>'{\"1\":\"&lt;MAT_RATIO&gt;4:3&lt;/MAT_RATIO&gt;\",\"2\":\"&lt;MAT_RATIO&gt;16:9&lt;/MAT_RATIO&gt;\"}'));
       // $myForm->displayElt(array("NAME"=>"proc_critere","TYPE"=>"hidden",'VALUE'=>'{\"1\":\"&lt;MAT_ID_MEDIA&gt;V&lt;/MAT_ID_MEDIA&gt;\",\"2\":\"&lt;MAT_ID_MEDIA&gt;P&lt;/MAT_ID_MEDIA&gt;\"}'));
        
        // champs de creation du document
        //$myForm->displayElt(array("NAME"=>"makedoc","TYPE"=>"hidden","CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both","ONCLICK"=>"toggleFields(this,document.getElementById('doc_form'))"));
        //$myForm->displayElt(array("NAME"=>"xsl","TYPE"=>"hidden","VALUE"=>"mappingDoc","DIVTAGS"=>"both"));
        $myForm->displayElt(array("NAME"=>"getInfos","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
		
        $myUpload->arrParams['arrParams']["strFilesFormat"]=implode("|",$arrFormats);
        echo '</form>';
        
        //$myForm->beginTag("FIELDSET",array("ID"=>"form1","LEGEND"=>kUploadParametres));
        ?>

<div id="tabs_upload" class="type_<?=$_GET['type']?>" style="margin:auto; width:800px;">
		<ul>
			<li>
				<a href="#upload_http"><span><?= kUploadChoisirFichier ?></span></a>
			</li>
			<li <?php if ($_REQUEST['media_type'] == 'subtitle') { ?>style="display:none" <?php } ?>>
				<a href="#upload_url"><span><?= kUploadSaisirUrl ?></span></a>
			</li>
			<li <?php if ($_REQUEST['media_type'] == 'subtitle') { ?>style="display:none" <?php } ?>>
				<a href="#upload_ftp"><span><?= kUploadSaisirFTP ?></span></a>
			</li>
		</ul>
<div id="upload_http">
<?php
    if (isset($_GET['id_cat']))
    $id='&id_cat='.$_GET['id_cat'];
    
    
    
    ?>
<div id="fileupload" >
<form  action="upload.php" method="post" enctype="multipart/form-data">
<div class="fileupload-buttonbar">
<label class="fileinput-button">
<span><?=kAjouter?></span>
<input type="file" name="files[]" multiple />
</label>
<button type="submit" class="start"><?=kUploadStart?></button>
<button type="reset" class="cancel"><?=kAnnuler?></button>
<!--button type="button" class="delete"><?=kSupprimer?></button-->
</div>
</form>
<div class="fileupload-content">
<table class="files"></table>
<div class="fileupload-progressbar"></div>
</div>
</div>
</div>
<div id="upload_url">
<?php
    // champs de creation du document
    //$myForm->displayElt(array("NAME"=>"xsl","TYPE"=>"hidden","VALUE"=>"mappingDoc","DIVTAGS"=>"both"));
    $myForm->displayElt(array("NAME"=>"getInfos","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
    
    $myUpload->arrParams['arrParams']["strFilesFormat"]=implode("|",$arrFormats);
    
    echo '<p><span class="fileupload-label">URL : </span><input type="text" name="url_upload" id="url_upload" size="60" value="http://" /></p>';
    echo '<input type="button" style="display:block;margin:0px auto;" class="std_button_style" value="Envoyer" onclick="envoyerFichierURL(\''.$_GET['type'].'\');" />';	
    //	echo '</form>';
    
    
    ?>
</div>	
<div id="upload_ftp">
<?php
    // champs de creation du document
    //$myForm->displayElt(array("NAME"=>"xsl","TYPE"=>"hidden","VALUE"=>"mappingDoc","DIVTAGS"=>"both"));
    $myForm->displayElt(array("NAME"=>"getInfos","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
    
    $myUpload->arrParams['arrParams']["strFilesFormat"]=implode("|",$arrFormats);
	
    echo '<p><span class="fileupload-label">Login : </span><input type="text" name="login_ftp" id="login_ftp" size="60"  /></p>';
  
	
	 echo '<p><span class="fileupload-label">Mot de passe : </span><input type="text" name="mdp_ftp" id="mdp_ftp" size="60"  /></p>';
   
    echo '<p><span class="fileupload-label">FTP://</span><input type="text" name="ftp_upload" id="ftp_upload" size="60"  /></p>';
    echo '<input type="button" style="display:block;margin:0px auto;" class="std_button_style" value="Envoyer" onclick="envoyerFichierFTP(\''.$_GET['type'].'\');" />';	
    //	echo '</form>';
    
    
    ?>
</div>	
</div>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload ">
    <td class="preview"><span class=""></span></td>
    <td class="name"><span>{%=file.name%}</span></td>
    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
    {% if (file.error) { %}
        <td class="error" colspan="2"><span class="label label-important"><?=kErreur?>:</span> {%=translateErr(file.error)%}</td>
        {% } else if (o.files.valid && !i) { %}
        <td>
        <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
        </td>
        <td class="start" style="display:none!important;">{% if (!o.options.autoUpload) { %}
            <button class="btn btn-primary">
            <i class="icon-upload icon-white"></i>
            <span>Start</span>
            </button>
            {% } %}</td>
        {% } else { %}
        <td colspan="2"></td>
        {% } %}
    <td class="cancel">{% if (!i) { %}
        <button class="ui-button-icon-only" style="height:27px;width:29px;vertical-align:middle;">
        </button>
        {% } %}</td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    {% if (file.error) { %}
        <td></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td class="error" colspan="2"><span class="label label-important"><?=kErreur?></span> {%=file.error%}</td>
        {% } else { %}
        <td class="preview">{% if (file.thumbnail_url) { %}
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
        <td class="name">
        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
        </td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td colspan="2"></td>
        {% } %}
    <td class="delete">
    <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="icon-trash icon-white"></i>
    <span>Delete</span>
    </button>
    <input type="checkbox" name="delete" value="1">
    </td>
    </tr>
    {% } %}
</script>
<script type="text/javascript">
var file_upload_num=1;

window.onload=function ()
{
<? $maxChunkSize = (defined('kUploadJSMaxChunkSize') && is_int(intval(kUploadJSMaxChunkSize)) && intval(kUploadJSMaxChunkSize)) ?intval(kUploadJSMaxChunkSize):500000000; // 500MB ?>
 requirejs([
	'jquery.fileupload/jquery.fileupload',
	'jquery.fileupload/jquery.fileupload-fp',
	'jquery.fileupload/jquery.fileupload-ui',
	'jquery.fileupload/jquery.fileupload-jui',
	'jquery.fileupload/jquery.iframe-transport'
	],function(){


	$j('#fileupload').fileupload
	(
	 {
     url:'empty.php?urlaction=jqueryUploadHandler&upload=1&upload_type=<?=$_GET['type']?>',
     acceptFileTypes : ext_allowed_regex,
     maxFileSize:<?php echo calcMaxUploadFileSize(ini_get('upload_max_filesize'))?>,
     autoUpload : false,
     maxChunkSize: <?=$maxChunkSize?>
     }
     );
    
    $j('#fileupload').on
    (
     'fileuploaddone',
     function (event, data)
     {
     var json=data.result;
     
     $j("#form_envoi_fichiers").append('<input type="hidden" name="file_'+file_upload_num+'" value="'+json[0]["name"]+'" />');
     $j("#form_envoi_fichiers").append('<input type="hidden" name="file_'+file_upload_num+'_original" value="'+json[0]["name"]+'" />');
     
     if (file_upload_num>=$j("#fileupload table.files tr").size())
     $j("#form_envoi_fichiers").submit();
     
     file_upload_num++;
     }
     );
    
    $j('#fileupload').on
    (
     'fileuploadstop',
     function (event, data)
     {
     //$j("#form_envoi_fichiers").submit();
     }
     )
    
    $j('#upload_form > .miniField > input[name=makedoc]').on('click',function(event,data){toggleFields(this,document.getElementById('doc_form'))});
    
    $j('#tabs_upload').tabs();
    });
}

function translateErr(str_err){
    switch(str_err){
        case 'Filetype not allowed': 
            return "<?=kErreurUploadInterdit?>";
            break;
        case 'File is too big' : 
            return "<?=kFichierTropGros?>";
            break;
        case 'File is too small' : 
            return "<?=kFichierTropPetit?>";
            break;
        case 'Maximum number of files exceeded' : 
            return "<?=kNombreMaxFichierAtteint?>";
            break;
        case 'Uploaded bytes exceed file size' : 
            return "<?=kTropOctetsEnvoyes?>";
            break;
        case 'Uploaded bytes exceed file size' : 
            return "<?=kTropOctetsEnvoyes?>";
            break;
			
    }
}

function toggleReportName(value) {
	if (value == "new")
		$j("#div_reportName").show();
	else
		$j("#div_reportName").hide();
}
</script>

<!--/div-->
<?php
    }
    ?>
