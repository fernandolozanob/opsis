<? include(getSiteFile("designDir","menuDocSaisie.inc.php"));

$action=$_POST["commande"];
if (isset($action) && $action = "SAVE_COMMENTS" && isset($_POST["t_doc_comment"])) {
	$myDoc->t_doc_comment = $_POST["t_doc_comment"];
	foreach ($myDoc->t_doc_comment as &$com) {
		$com['DC_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$com['DC_ID_USAGER_MOD'] = $myUser->UserID;
	}
	$myDoc->saveDocComments();
}
?>
<div id="fiche_info" class="<?=($entity_has_media)?'':'no_media'; ?>"> 
<?
if ($myDoc->error_msg) echo "<div class='error' id='error_msg'>".$myDoc->error_msg."</div>";  

include(libDir."class_formSaisie.php");
$xmlform=file_get_contents(getSiteFile("designDir","form/xml/docComment.xml"));
$myForm=new FormSaisie;
$myForm->saisieObj=$myDoc;
$myForm->entity="DOC";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->classHelp="help_champs_form";
$myForm->display($xmlform);
?>