

<div class="pageName" id='menuDocSaisie'>
  <?	include(getSiteFile("designDir","menuFestSaisie.inc.php")); ?>
</div>

<?
if ($myDoc->error_msg) echo "<div class='error' id='error_msg'>".$myDoc->error_msg."</div>";
?>
<script type="text/javascript">
  var currentSection;
  var _hasChanged;
  _hasChanged=false;

  function chkFormFields () {
    msg='';

  arrInputs=document.form1.getElementsByTagName('input');
  for(v=0;v<arrInputs.length;v++) {
    if (arrInputs[v].getAttribute('mandatory')) {
      if (arrInputs[v].value.Trim()=='') {
        msg+='<?=kJSErrorValeurOblig?>';
           arrInputs[v].focus();
           arrInputs[v].className='errorjs';
           new Effect.Pulsate(arrInputs[v]);
      } else {
        arrInputs[v].className=null;
      }
    }
  }

    if (msg!='') {alert(msg);return false;}
    _hasChanged=false; //pour ne pas déclencher le message
    if (document.form1.FEST_ANNEE.value.length==4) {document.form1.FEST_ANNEE.value=document.form1.FEST_ANNEE.value+'-01-01';}
    
  }

  //callback de la palette d ajout d un film
  function addValue(id_champ,valeur,idx,role) {
    if (!currentSection) return;
    if (currentSection.style.display=='none') currentSection.style.display='block';
    myfilm=new Object();
    myfilm.DOC_TITREORI=valeur;
    myfilm.ID_DOC=idx;
  _hasChanged=true;
    addFilm(currentSection,myfilm);
  }

  //callback de la palette d ajout d un jure
  function addJureFromPanel(id_champ,valeur,idx,role) {
    myjure=new Object();
    myjure.PERS_NOM=valeur;
    myjure.ID_PERS=idx;
    _hasChanged=true;
    addJure(myjure);
  }


  function removeFest(url) {
      if (confirm("<?=kConfirmJSDocSuppression?>")) {
          document.form1.commande.value="SUP_FEST";
          document.form1.page.value=url;
          document.form1.submit();
          }
  }

  function duplicateFest(url) {
       if (confirm("<?=kConfirmJSDocDuplication?>")) {
          document.form1.commande.value="DUP_FEST";
          if (url!='') document.form1.page.value=url;
          document.form1.submit();
       }
  }


  function confirmExit() {if (_hasChanged) return '';}
  window.onbeforeunload=confirmExit;

</script>

<div class="resultsMenu" align="right">
   <a href="javascript:popupPrint('export.php?impression=<?=urlencode(str_replace("'"," ",$myFest->t_festival['FEST_LIBELLE'])) ?>&export=doc&id=<?= $myFest->t_festival['ID_FEST']?>&amp;id_lang=<?= $myFest->t_festival['ID_LANG']?>&amp;type=doc','main')"><?= kImprimer ?></a>
    | <a href="javascript:popupPrint('export.php?id=<?= $myFest->t_festival['ID_FEST']?>&amp;id_lang=<?= $myFest->t_festival['ID_LANG']?>&amp;type=doc','main')"><?=kExporter ?></a>
| <a href="index.php?urlaction=festView&id_fest=<?= $myFest->t_festival['ID_FEST']?>&id_lang=<?= $myFest->t_festival['ID_LANG']?>"><?=kVoir ?></a>
</div>

<?

foreach ($myFest->t_section as $i=>$section) {

  $mySect=new Festival();
  $mySect->t_festival['ID_FEST']=$section['ID_FEST'];
  $mySect->t_festival['ID_LANG']=$section['ID_LANG'];
  $mySect->getFest();
  $mySect->getDocs();
  $myFest->t_section[$i]['FEST']=$mySect;

  unset($mySect);
}

//debug($myFest->t_section,'orange',true);


?>
<style>

  .section {border:2px solid #009900;display:block;
        background-color:#ffffff;margin:10px;font-family:"Trebuchet MS";font-size:14px;font-weight:bold;
        padding:5px;}
  .section table td {font-size:11px;font-weight:normal;}
  .section input {font-size:14px;}
  .section textarea {font-size:11px;}

  #films {margin-left:10px;}
  .film {border:1px solid #006600;display:block;margin:2px;background-color:white;font-weight:normal;}
  .film table td {font-size:11px;}
  .film input,.film select {font-size:11px;font-weight:bold;}
  #DOC_TITRE {width:350px;}
  .btnAjouter {cursor:pointer;border:1px solid black;background-color:#ddffdd;
        font-size:11px;font-family:"Trebuchet MS",Helvetica,sans-serif;
        padding:2px;margin:5px 2px 5px 12px;font-weight:bold;}
  .btnAjouter img {vertical-align:bottom;padding-right:3px;}

  #jures {margin-left:10px;}
  .jure {border:1px solid #006600;display:block;margin:2px;background-color:white;font-weight:normal;}
  .jure table td {font-size:11px;}
  #PERS_NOM {width:300px;font-weight:bold;}
  #trash {float:right;vertical-align:center;cursor:pointer;margin:3px 3px 0px 0px;}

</style>

<script>
function addSection(section) {

  //Partie 1 : création de la section par clonage de la section vierge
  blankSect=document.getElementById('section$blank');
  myNewRow=blankSect.cloneNode(true);
  myNewRow.id='section$cloned'; //chgt id car il ne faut qu un seul section$blank
  document.getElementById('sections').appendChild(myNewRow);
  myNewRow.style.display='block'; //on rend visible le clone

  if (!section) return; //création d une section vierge ? on termine

  //Partie 2 : affectation des valeurs aux champs
  arrFlds=myNewRow.getElementsByTagName('input');
  for (i=0;i<arrFlds.length;i++) {
    fldName=arrFlds[i].id; //on a placé le nom du champ coté JSON dans le ID
    obj=eval("section.FEST.t_fest."+fldName);
    if (obj) {
      if (fldName=='FEST_ANNEE') obj=obj.substr(0,4); //champ année ? on ne prend que les 4 premiers chiffres
      arrFlds[i].value=obj;
    }
  }
  arrFlds=myNewRow.getElementsByTagName('textarea');
  for (i=0;i<arrFlds.length;i++) {
    fldName=arrFlds[i].id; //on a placé le nom du champ coté JSON dans le ID
    obj=eval("section.FEST.t_fest."+fldName);
    if (obj) {arrFlds[i].innerHTML=obj;}
  }

  //Partie 3 : ajout des films
  for (i=0;i<section.FEST.t_fest_doc.length;i++) {
    addFilm(myNewRow,section.FEST.t_fest_doc[i]); //ajout de chaque film au div films
  }

  arrFlds=null;
  myNewRow=null;
}

function addFilm(section,film) {
  var _i;
  secElmt=section.getElementsByTagName('div')[0]; // = div "films" dans la section

  //Partie 1 : création du film par clonage du film vierge
  blankFilm=document.getElementById('film$blank');
  myNewFilm=blankFilm.cloneNode(true);
  myNewFilm.id='film$cloned'; //chgt id car il ne faut qu un seul section$blank
  secElmt.appendChild(myNewFilm);
  myNewFilm.style.display='block'; //on rend visible le clone

  if (!film) return; //creation film vierge ? on arrete

  //Partie 2 : affectation des valeurs aux champs : boucle sur les champs du div et affection valeur du JSON
  arrFlds= myNewFilm.getElementsByTagName('*');
  for (_i=0;_i<arrFlds.length;_i++) {
    if (arrFlds[_i].id=='ID_DOC') arrFlds[_i].value=film.ID_DOC;
    if (arrFlds[_i].id=='DOC_TITRE') {
      arrFlds[_i].innerHTML=(film.DOC_SOUSTITRE?"("+film.DOC_SOUSTITRE+") ":'')+film.DOC_TITREORI;
      if (film.DOC_TITRE) arrFlds[_i].innerHTML+="&#160;&#160;&#160;&#160;<i>"+(film.DOC_TITRE_COL?"("+film.DOC_TITRE_COL+") ":'')+film.DOC_TITRE+"</i>";
    }
    //Cas de la listbox des prix
    if (arrFlds[_i].id=='PRIX' && film.DOC_FEST_VAL && film.DOC_FEST_VAL.length>0) {
      idval=film.DOC_FEST_VAL[0].ID_VAL;
      if (idval!='') setValue(idval,arrFlds[_i],false);
    }
    if (arrFlds[_i].id=='btnGo') {
      arrFlds[_i].onclick=function() {location.href=eval("\'<?=$myPage->getName()?>?urlaction=doc&id_doc="+film.ID_DOC+"\'")};
    }
    if (arrFlds[_i].id=='btnEdit') {
      arrFlds[_i].onclick=function() {location.href=eval("\'<?=$myPage->getName()?>?urlaction=docSaisie&id_doc="+film.ID_DOC+"\'")};
    }
  }
  arrFlds=null;
  myNewFilm=null;
}

function addJure(jure) {
  var _i;
  //Partie 1 : création de la personne par clonage de la section vierge
  blankJure=document.getElementById('jure$blank');
  myNewJure=blankJure.cloneNode(true);
  myNewJure.id='jure$cloned'; //chgt id car il ne faut qu un seul section$blank
  document.getElementById('jures').appendChild(myNewJure);
  myNewJure.style.display='block'; //on rend visible le clone

  if (!jure) return; //création d une section vierge ? on termine

  //Partie 2 : affectation des valeurs aux champs
  arrFlds= myNewJure.getElementsByTagName('*');
  for (_i=0;_i<arrFlds.length;_i++) {
    if (arrFlds[_i].id=='ID_PERS') {
      arrFlds[_i].value=jure.ID_PERS;
    }
    if (arrFlds[_i].id=='PERS_NOM') {
      arrFlds[_i].innerHTML=jure.PERS_NOM+(jure.PERS_PRENOM?" "+jure.PERS_PRENOM:"");
    }
    if (arrFlds[_i].id=='LEX_PRECISION' && jure.LEX_PRECISION ) {
      prec=jure.LEX_PRECISION;
      if (prec!='') setValue(prec,arrFlds[_i],false); //affectation au jury
    }
    
    if (arrFlds[_i].id=='btnGo') {
      arrFlds[_i].onclick=function() {location.href=eval("\'<?=$myPage->getName()?>?urlaction=personne&id_pers="+jure.ID_PERS+"\'")};
    }
    if (arrFlds[_i].id=='btnEdit') {
      arrFlds[_i].onclick=function() {location.href=eval("\'<?=$myPage->getName()?>?urlaction=personneSaisie&id_pers="+jure.ID_PERS+"\'")};
    }
  }
  arrFlds=null;
  myNewJure=null;
}


//Jury : change le type de input suivant que l on choisit un jury existant ou on entre un nouveau
function updateMe(fld) {
  var i;
  myStyle=fld.getAttributeNode('mystyle').value;
  myElements=fld.parentNode.childNodes;
  for (i=0;i<myElements.length;i++) {
  if (myElements[i].nodeType!=1) continue;
   if (myElements[i].getAttributeNode('mystyle').value!=myStyle) {
		myElements[i].style.display='inline';
		myElements[i].disabled=false;
	} else {
		myElements[i].style.display='none';
		myElements[i].disabled=true;
   }
  }
  _hasChanged=true;
}

//Section : switch une section on/off (les sections off sont effacées)
function toggleOnOff(mysection,bouton) {

    etat=bouton.getAttribute('etat');
    if (!etat || etat=='on') {
     new Effect.Opacity(mysection,{from:1,to:0.4,duration:0.3});
    bouton.setAttribute('etat','off');
    bouton.nextSibling.nextSibling.value='SUP';
  } else {
     new Effect.Opacity(mysection,{from:0.4,to:1,duration:0.3});
    bouton.setAttribute('etat','on');
    bouton.nextSibling.nextSibling.value='';
  }
  _hasChanged=true;
}


</script>

<? if ($myFest->error_msg) echo "<div class='errormsg'>".$myFest->error_msg."</div>"?>

<div id='section$blank' class='section' style='display:none;'>
  <input type="hidden" id="ID_FEST" name="t_section[][ID_FEST]" />
   <img src="<?=imgUrl?>/application_delete.png" id='trash'
     onclick="toggleOnOff(this.parentNode,this);return false;" >
   <input type='hidden' id='commande' name="t_section[][commande]" value=""/>
   </button>

  <table>
    <tr>
    <td><?=kLibelle?> *</td>
    <td><input type="text" size="40" mandatory="true" onchange="_hasChanged=true" id="FEST_LIBELLE" name="t_section[][FEST_LIBELLE]" /></td>
    </tr>
    <tr>
    <td><?=kDescription?></td>
    <td ><textarea cols="40" onchange="_hasChanged=true" id="FEST_DESC" rows="3" name="t_section[][FEST_DESC]"> </textarea></td>
    </tr>
  </table>
  Sélection
  <img src="design/images/arrow_down.gif" style="cursor:pointer" align="absmiddle" border="0" id="arrowIndex"
   onclick="javascript:toggleVisibility(this.nextSibling.nextSibling,this)" />
  <div id="films" style="display:block;"> </div>

  <span id='arrFather§add'
    class='btnAjouter'
    onClick="currentSection=this.parentNode;choose(this,'titre_index=<?=kDocument?>&id_lang=<?=$myFest->t_festival['ID_LANG']?>&valeur=&champ=C_DOC_TITREORI')">
    <img src='design/images/film_add.png' /><?=kFestivalAjouterFilm?></span>

</div>


<div id='film$blank' class='film' style='display:none;'>
  <input type="hidden" id="ID_DOC" name="t_section[][t_doc_fest][][ID_DOC]" />
  <img src="<?=imgUrl?>film_delete.png" id='trash'
      onclick="_hasChanged=true;this.parentNode.parentNode.removeChild(this.parentNode);" />

  <table>
    <tr>
    <td><div id='DOC_TITRE'> </div></td>
    <td><?=kPrix?></td>
    <td ><select id='PRIX' name="t_section[][t_doc_fest][][ID_VAL]" onchange="_hasChanged=true;">
    <option value="">---</option>
      <?=listOptions("select ID_VAL, VALEUR from t_val where VAL_ID_TYPE_VAL='PRIX'
            and ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY VALEUR",array("ID_VAL","VALEUR"),"ID_VAL")?>
    </select>
    <input type="hidden" name="t_section[][t_doc_fest][][ID_TYPE_VAL]" value="PRIX" />
    </td>
  <td ><img src='<?=imgUrl?>film_go.png' id='btnGo' style='cursor:pointer' title="<?=kConsulter." ".kFilm?>" alt="<?=kConsulter." ".kFilm?>" onclick='' />
    <img src='<?=imgUrl?>film_edit.png' id='btnEdit' style='cursor:pointer' title="<?=kModifier." ".kFilm?>" alt="<?=kModifier." ".kFilm?>" onclick='' />
  </td>
    </tr>
  </table>
</div>

<!-- Entité juré -->
<div id='jure$blank' class='jure' style='display:none;'>
  <input type="hidden" id="ID_PERS" name="t_fest_lex[][ID_PERS]" />
  <input type="hidden" name="t_fest_lex[][ID_TYPE_DESC]" value="JUR" />
  <input type="hidden" name="t_fest_lex[][DLEX_ID_ROLE]" value="JUR" />
    <img src="<?=imgUrl?>user_delete.png" id='trash'
      onclick="_hasChanged=true;this.parentNode.parentNode.removeChild(this.parentNode);" />
  <table>
    <tr>
    <td><div id='PERS_NOM'> </div></td>
   <td ><img src='<?=imgUrl?>user_go.png' id='btnGo' style='cursor:pointer' title="<?=kConsulter." ".kPersonne?>" alt="<?=kConsulter." ".kPersonne?>" onclick='' />
  	<img src='<?=imgUrl?>user_edit.png' id='btnEdit' style='cursor:pointer' title="<?=kModifier." ".kPersonne?>" alt="<?=kModifier." ".kPersonne?>" onclick='' />
  	</td>
 
    <td>
    <select id='LEX_PRECISION' mystyle='list' name="t_fest_lex[][LEX_PRECISION]" >
      <?=listOptions("select distinct REPLACE(LEX_PRECISION,'\"','\\\"') as ID_VAL,LEX_PRECISION as VALEUR from t_doc_lex_precision where
          ID_LANG=".$db->Quote($_SESSION['langue'])." ORDER BY LEX_PRECISION",array("ID_VAL","VALEUR"),"ID_VAL")?>
    </select>
    <input type='text' mystyle='text' style='display:none'
      size='40' name="t_fest_lex[][LEX_PRECISION]" disabled='true' value="" />
    <img src='design/images/crayon.gif' mystyle='list' style='cursor:pointer' onclick='updateMe(this)' />
    <img src='design/images/famfamfam_mini_icons/action_stop.gif' mystyle='text' onclick='updateMe(this)' style='display:none;cursor:pointer'/>
    
    </td>
    
  </tr>
  </table>
</div>



<form name="form1" id="form1" method="POST" onSubmit='return chkFormFields()' urlaction="<?=$myPage->getName()?>?urlaction=festSaisie" >
<input type="hidden" name="ID_FEST" value="<?=$myFest->t_festival['ID_FEST']?>" />
<input type="hidden" name="COMMANDE" value="SAVE" />
<input type="hidden" name="page" value="" />
<fieldset>
<legend><?=kFestival?></legend>
<br/>
<table>
  <tr>
  <td class='label_champs_form'><?=kAnneeFestival?> *</td>
  <td colspan="3"><input type="text" mandatory="true" style='font-size:16px;font-weight:bold;' id="FEST_ANNEE" name="FEST_ANNEE"
      onchange='_hasChanged=true' value="<?=substr($myFest->t_festival['FEST_ANNEE'],0,strpos($myFest->t_festival['FEST_ANNEE'],'-'))?>"/></td>
  </tr>
  <tr>
  <td class='label_champs_form'><?=kDescription?></td>
  <td><textarea name="FEST_DESC" id="FEST_DESC" cols="40" rows="3" onchange='_hasChanged=true'><?=$myFest->t_festival['FEST_DESC']?></textarea></td>
  <td class='label_champs_form' width='120' style='text-align:right'><?=kPalmares?></td>
  <td><textarea name="FEST_PALMARES" id="FEST_PALMARES" cols="50" rows="3" onchange='_hasChanged=true'><?=$myFest->t_festival['FEST_PALMARES']?></textarea></td>
  </tr>
  <tr>
  <td class='label_champs_form'><?=kIdentifiant?></td>
  <td colspan="3"><?=$myFest->t_festival['ID_FEST']?></td>
  </tr>
  <tr>
  <td class='label_champs_form'><?=kDateModification?></td>
  <td colspan="3"><input type="text" disabled="true" name="FEST_DATE_MOD" value="<?=$myFest->t_festival['FEST_DATE_MOD']?>"/>
  <?=kPar?><input type="text" disabled="true" name="user_mod" value="<?=$myFest->usager_modif?>"/></td>
  </tr>
</table>
</fieldset>

<br/>

<fieldset>
<legend><?=kJury?></legend>
<br/>
<span id='jure§add'
    class='btnAjouter'
    onClick="choose(this,'titre_index=<?=kPersonne?>&id_lang=<?=$myFest->t_festival['ID_LANG']?>&valeur=&champ=PFC&type_lex=PP&rtn=addJureFromPanel')">
    <img src='design/images/user_add.png' /><?=kFestivalAjouterJure?></span>
<div id='jures'> </div>
</fieldset>

<br/>

<fieldset>
<legend><?=kSection?> / <?=kFilm?></legend>
<br/>
<span id='section§add'
    class='btnAjouter'
    onClick="addSection()">
    <img src='design/images/btnAdd.png' /><?=kFestivalAjouterSection?></span>
<div id='sections'> </div>

</fieldset>
<script>
//chargement et remplissage des entites. A laisser à la fin
arrSections=<?=array2json($myFest->t_section)?>;
arrJures=<?=array2json($myFest->t_fest_pers)?>;

for (cnt=0;cnt<arrSections.length;cnt++) {
 addSection(arrSections[cnt]);
}
for (cnt=0;cnt<arrJures.length;cnt++) {
 addJure(arrJures[cnt]);
}
</script>

<? //debug(str_replace(array("}","["),array("}\n","\n["),array2json($myFest->t_fest_pers)),'gold') ?>
<div style='margin:10px;text-align:center;'>
<input type="submit" id="bOk" name="bOk" value="<?=kEnregistrer?>" class="BUTTON"/>
<input type="button" name="bAnnuler" id="bAnnuler" value="<?=kRetour?>" onclick="location.href='<?=$myPage->getName()?>?urlaction=festListe'" class="BUTTON"/>
<input type="button" name="bSub" id="bDelete" value="<?=kSupprimer?>" onclick="removeFest('index.php?urlaction=festListe')" class="BUTTON"/>
</div>
</form>



















<?
/*
include(libDir."class_formSaisie.php");
if(isset($_GET["mandat"])) $xmlform=file_get_contents(getSiteFile("designDir","form/xml/docSaisie2.xml"));
else $xmlform=file_get_contents(getSiteFile("designDir","form/xml/docSaisie.xml"));
$myForm=new FormSaisie;
$myForm->saisieObj=$myFest;
$myForm->entity="DOC";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->display($xmlform);

$myPage->renderReferrer();
*/
?>
<script type="text/javascript">
if(document.getElementById("user_crea")) document.getElementById("user_crea").value='<?= $myFest->user_crea ?>';
if(document.getElementById("user_modif")) document.getElementById("user_modif").value='<?= $myFest->user_modif ?>';
if(document.getElementById("date_prod")) document.getElementById('date_prod').value=convDate(document.getElementById('doc_date_prod').value,'jj/mm/aaaa');
</script>

