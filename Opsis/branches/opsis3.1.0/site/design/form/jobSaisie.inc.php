﻿<script type="text/javascript">

function toggleFields(myCheck,myTab) {
	state=!myCheck.checked;
	//myTab=document.getElementById(myTable);
	myTab.style.opacity=(state==true?0.5:1);
	myTab.style.filter='Alpha:opacity='+(state==true?50:100);
	
	inputs=myTab?myTab.getElementsByTagName('input'):null;
	for (i=0;i<inputs.length;i++) inputs[i].disabled=state;
	inputs=myTab?myTab.getElementsByTagName('select'):null;
	for (i=0;i<inputs.length;i++) inputs[i].disabled=state;
}

</script>
<?php
	
	require_once(modelDir.'model_doc.php');
	$dumbDoc=new Doc;
	
	include(libDir."class_formSaisie.php");
	$myForm=new FormSaisie();
	$myForm->saisieObj=$dumbDoc;
	$myForm->entity="DOC";
	
	//require_once(libDir."class_chercheJob.php");
	//$mySearch=new RechercheJob();
	//$myForm->chercheObj=$mySearch;
	
	$myForm->classLabel="miniField";
	$myForm->classValue="miniField";

    $myForm->classLabel="miniField";
    $myForm->classValue="miniField";
    $myForm->beginForm(array("NAME"=>"form1","ACTION"=>"simple.php?urlaction=jobSaisie"));
    
    $myForm->displayElt(array("CHFIELDS"=>"JOB_ID_SESSION","TYPE"=>"hidden","CHVALUES"=>session_id()));
    $myForm->displayElt(array("NAME"=>"F_job_form","TYPE"=>"hidden","VALUE"=>"1"));
    $myForm->displayElt(array("NAME"=>"tri","TYPE"=>"hidden","VALUE"=>"id_job1"));
    $myForm->displayElt(array("NAME"=>"lancement","TYPE"=>"hidden","VALUE"=>"proc"));
    $myForm->displayElt(array("NAME"=>"id_doc","TYPE"=>"hidden","VALUE"=>$_GET['id_doc']));
    $myForm->displayElt(array("NAME"=>"type","TYPE"=>"hidden","VALUE"=>$_GET['type']));
    $myForm->displayElt(array("NAME"=>"id_mat","TYPE"=>"hidden","VALUE"=>$_GET['id_mat']));

	$init_onclick="window.close()";
	
	switch($_GET['type']){
			
		case "panier":
			
			
			require_once(modelDir.'model_panier.php');
			$myPanier=new Panier();
			$myPanier->t_panier['ID_PANIER']=intval($_GET['id_panier']);
			$myPanier->getPanier();
			//$myPanier->getDocs();
			$myPanier->getDocsFull(1,1,1);
			if($_GET["livraison"]=="client") {
				$folder="P".$myPanier->t_panier['ID_PANIER']."_".$myPanier->t_panier['PAN_ID_USAGER']; //nom de dossier défini par id_panier et id_usager du panier
				$updatePanier='1';
				$prefixProc="liv";
				$superjob="Préparation téléchargement commande ".intval($_GET['id_panier']);
			} elseif($_GET["livraison"]=="fcp") {
				$folder="FCP".$myPanier->t_panier['ID_PANIER']; //nom de dossier défini par id_panier et id_usager connecté
				$xsl="exportFCP";
				$superjob="Export FCP commande ".intval($_GET['id_panier']);
				$prefixProc="fcp";
				$updatePanier='0';
			} else {
				$folder="P".$myPanier->t_panier['ID_PANIER']."_".$_SESSION["USER"]["ID_USAGER"]; //nom de dossier défini par id_panier et id_usager connecté
				$updatePanier='0';
				$prefixProc="liv";
			}
			
			print "<input type='hidden' name='id_panier' value='".intval($_GET['id_panier'])."' />
			<input type='hidden' name='folder' value='".$folder."' />
			<input type='hidden' name='updatePanier' value='".$updatePanier."' />
			<input type='hidden' name='xsl' value='".$xsl."' />
			<input type='hidden' name='send_mail' value='1'/>
			<input type='hidden' name='super_job' value='".$superjob."'/>";
			print "<input type='hidden' name='file_name_mask' value='DOC_COTE' />";
			
			// Choix du processus par ligne
			$myPage=Page::getInstance();
			$myPage->nbLignes="all";
			$myPage->page="1";
			$myPage->initPagerFromArray($myPanier->t_panier_doc);
			$param["prefixProc"]=$prefixProc;
			$myPage->addParamsToXSL($param);
			$xmlPanier=$myPanier->xml_export(0,0);
			$myPage->afficherListe('t_panier_doc',listeDir.'panierJob.xsl',false,null,$xmlPanier);
			unset($myPanier);
			break;
			
		default:
			
			$myForm->displayElt(array("NAME"=>"file_name_mask","TYPE"=>"hidden","VALUE"=>"DOC_COTE","TRTAGS"=>"none"));
			$myForm->displayElt(array("NAME"=>"id_proc","TYPE"=>"select","LABEL"=>kProcessus,"SELECT_SQL"=>"select ID_PROC as ID,PROC_NOM as VAL from t_proc order by 2","TRTAGS"=>"both"));
			$myForm->displayElt(array("NAME"=>"id_etape","TYPE"=>"select","LABEL"=>kOu." ".kTraitement,"SELECT_SQL"=>"select ID_ETAPE as ID,ETAPE_NOM as VAL from t_etape order by 2","TRTAGS"=>"both"));
			if(defined("kJobPlateforme")){
                $myForm->displayElt(array("NAME"=>"job_plateforme","TYPE"=>"select",'LABEL'=>kPlateforme,"SELECT_OPTION"=>"local,opsomai","SELECT_LIB"=>"local,opsomai","VALUE"=>kJobPlateforme,"NOALL"=>"1","TRTAGS"=>"both"));
            }
			$myForm->displayElt(array("NAME"=>"priority","TYPE"=>"select","LABEL"=>kPriorite,"SELECT_OPTION"=>"1,2,3","SELECT_LIB"=>"kHaute,kNormale,kBasse","VALUE"=>"2","TRTAGS"=>"both"));
			break;
	}
	$myForm->displayElt(array("NAME"=>"Cancel","TYPE"=>"buttonspan","BUTTON_TYPE"=>"button","LABEL"=>"&nbsp;","LABEL_WIDTH"=>"20","CLASS"=>"ui-state-default ui-corner-all","SPAN_CLASS"=>"ui-icon ui-icon-arrowrefresh-1-w","VALUE"=>kAnnuler,"ONCLICK"=>$init_onclick,"TRTAGS"=>"start"));
	$myForm->displayElt(array("NAME"=>"OK","TYPE"=>"buttonspan","BUTTON_TYPE"=>"submit","CLASS"=>"ui-state-default ui-corner-all","SPAN_CLASS"=>"ui-icon ui-icon-check","VALUE"=>kEnvoyer,"TRTAGS"=>"end"));
	$myForm->endForm();
	
	
	?>
