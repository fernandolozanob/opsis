<?
if(isset($_REQUEST["init"]) || isset($_REQUEST["lancement"])) {
    // Initialisation JOB_ID_SESSION
    $_SESSION['recherche_JOB']['form'][]=array ( "FIELD" => "JOB_ID_SESSION",
                                                 "TYPE" => "CE",
                                                "VALEUR" => session_id() );
}

include(libDir."class_formCherche.php");
$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheJob.xml"));
if (isset($_GET['display'])) {	//Ajout du parametre "display" dans l'url action du formulaire
	$xmlform = str_replace("<action>simple.php?urlaction=jobListe</action>", "<action>simple.php?urlaction=jobListe&amp;display=".$_GET['display']."</action>", $xmlform);
	if ($_GET['display'] == "erreur" || $_GET['display'] == "fini")
		$xmlform = str_replace("<value>JOB_PRIORITE,ID_JOB</value>", "<value>JOB_DATE_FIN1</value>", $xmlform);
}
	
$myForm=new FormCherche;
$myForm->chercheObj=$mySearch;
$myForm->entity="JOB";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->display($xmlform);


//Gestion des onglets par �tat
$HL['jobListe']='#tabs-1';
$HL['jobListeerreur']='#tabs-2';
$HL['jobListefini']='#tabs-3';

unset ($_SESSION[$mySearch->sessVar]["sqlSource"]);
if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	$_SESSION[$mySearch->sessVar]["sqlSource"] = $_SESSION[$mySearch->sessVar]["sql"];
	$sql = $_SESSION[$mySearch->sessVar]["sql"];
	
	$tabSql = array("" => "WHERE j.JOB_ID_ETAT not in (".jobErreur.",".jobFini.") AND ",
					"fini" => "WHERE j.JOB_ID_ETAT in (".jobFini.") AND ",
					"erreur" => "WHERE j.JOB_ID_ETAT in (".jobErreur.") AND ");
	
	$nbActif = $db->getOne(str_replace("WHERE ", $tabSql[""], "SELECT count(j.JOB_ID_ETAT) " . substr($sql, strpos($sql, "FROM "))));
	$nbFini = $db->getOne(str_replace("WHERE ", $tabSql["fini"], "SELECT count(j.JOB_ID_ETAT) " . substr($sql, strpos($sql, "FROM "))));
	$nbErreur = $db->getOne(str_replace("WHERE ", $tabSql["erreur"], "SELECT count(j.JOB_ID_ETAT) " . substr($sql, strpos($sql, "FROM "))));
	if (isset($_GET['display'])) {
		$param["urlparams"] = "&display=".$_GET['display'];
		$_SESSION[$mySearch->sessVar]["sql"] = str_replace("WHERE ", $tabSql[$_GET['display']], $_SESSION[$mySearch->sessVar]["sql"]);
	}
	else $_SESSION[$mySearch->sessVar]["sql"] = str_replace("WHERE ", $tabSql[""], $_SESSION[$mySearch->sessVar]["sql"]);
	?>
	<div id="tabs" style="width:95%;margin:auto">
		<ul>
			<li><a href='#tabs-1' id="tab_act" onclick="window.location='simple.php?urlaction=jobListe&tri=job_priorite,id_job'"><?= kActif." ($nbActif)" ?></a></li>
			<li><a href='#tabs-2' id="tab_err" onclick="window.location='simple.php?urlaction=jobListe&display=erreur&tri=job_date_fin1'"><?= kErreur." ($nbErreur)" ?></a></li>
			<li><a href='#tabs-3' id="tab_fin" onclick="window.location='simple.php?urlaction=jobListe&display=fini&tri=job_date_fin1'"><?= kFinis." ($nbFini)" ?></a></li>
		</ul>
	</div>
	<?
} ?>
<script type="text/javascript">
	var $tabs = $j('#tabs').tabs();
	$j('#tabs li').removeClass('ui-tabs-selected ui-state-active');
	$j("a[href='<?=$HL["jobListe".(isset($_GET['display'])?$_GET['display']:'')]?>']").parent('li').addClass('ui-tabs-selected ui-state-active');
</script>