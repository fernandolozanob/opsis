    <? if(isset($_GET['edit']) && !empty($_GET['edit']) && defined("kModificationProfil")){?>
		<div id="pageTitre" class="title_bar"><?=kModificationProfil?></div>
	<?}else{?>
		<div id="pageTitre" class="msg"><?=kInscription;?></div>
	<?}?>
    
	 <div class="feature signup_wrapper-1">
	 <div class="signup_wrapper-2">
<? 

if(!empty($myUsager->t_usager['ID_USAGER']) && empty($_GET['edit']) && $myUsager->t_usager['US_ID_ETAT_USAGER'] == Usager::getIdEtatUsrFromCode('INACTIF') && isset($_POST['captcha_inscription'])){
	if(empty($myUsager->error_msg)){?>
		<p class="success"><?=kMsgInscriptionAttente?></p>
		
		<div class="buttons">
		<a  href="javascript:boutonRetour();" >
			<button class="std_button_style"><?=kRetour?></button>
		</a>
		</div>
		<script>
			function boutonRetour(){
				if(typeof returnFromAjaxPopin == 'function'){
					return returnFromAjaxPopin();
				}else{
					window.location.href='index.php';
				}
			}
			
		</script>
<?	}
}else if (!empty($myUsager->t_usager['ID_USAGER']) && $myUsager->t_usager['US_ID_ETAT_USAGER']==Usager::getIdEtatUsrFromCode('ACTIF') && empty($_GET['edit']) ) {
	if($new){	
		echo "<br/>".kMsgInscriptionUsager."<br/>";
	}
		if ($_REQUEST['type_commande']) {
			include_once(modelDir.'model_panier.php');
			$fldrs=Panier::getFolders();
			foreach ($fldrs as $f) if ($f['PAN_ID_ETAT']==1) {$idPanier=$f['ID_PANIER'];break;}
			
			echo '<p class="error">'.$myUsager->error_msg.'</p>
			<form name="redirect2command" method="POST" action="'.Page::getInstance()->getName().'?urlaction=commande">
			<input type="hidden" name="id_panier" value="'.$idPanier.'" />
			<input type="hidden" name="commande" value="INIT" />
			<input type="hidden" name="pan_id_type_commande" value="'.$_REQUEST['type_commande'].'" />
			<input type="hidden" name="conserverPanier" value=""/>
			</form>
			
			';
			if (User::getInstance()->loggedIn()) //loggé ? on part à la commande
				echo "<span class='ancienh1'>".kRedirectionCommande."</span><br/><br/><br/>
				<script>
				function continueCommande() {document.redirect2command.submit();}
				continueCommande.delay(3);
				</script>";
			else // apparemment pas utile, on est autologgé en douce qd l'user est créé
				echo "<a href='#' onclick=\"doConnect(".quoteField($myUsager->t_usager['US_LOGIN']).",".quoteField($myUsager->t_usager['US_PASSWORD']).")\">".kContinuerCommande."</a>";
		} 
	
}elseif (empty($myUsager->t_usager['ID_USAGER']) || !empty($_GET['edit'])) {
	//Modification inscription
		
	if (isset($_REQUEST['type_commande']) && $_REQUEST['type_commande']) { 
		
		?>
		
		<div class='info_entete'><?=kInscriptionPremiereCommande?></div>
		
		<form name='connexion' id='connexion' method='post' action="">
		<br/><span class='ancienh1'><?= kMsgInscription ?></span><br />
		<fieldset>
		<legend><?=kAuthentification?></legend>
		<table width="100%"  border="0" class="feature">
		<tr><td class="label_champs_form" width="200"><?= kVotreLogin ?> : </td><td>
     	<input size='12' type='text' id='login' name='login' value="<?= $_POST['login'] ?>" /></td></tr>
		<tr><td class="label_champs_form"><?= kVotreMotDePasse?> : </td><td>
     	<input size='12' type='password' id='password' name='password' /></td></tr>
		<tr><td class="label_champs_form">&#160;</td><td>
		<input name='refer' type='hidden' value="<?=$_SERVER['REQUEST_URI']?>" />
		<input type='submit' name='bsubmitLogin' value='<?= kEnvoyer ?>' />
		</td></tr>
		</table>
		</fieldset>
		</form>
		<br/><br/>
		
		<br/>
		
		<? } ?>
	<?
	if((empty($myUsager->t_usager['ID_USAGER']) && $myUsager->error_msg != kSuccesSauveUsager."<br/>") || !empty($_GET['edit'])){
		if(strpos($myUsager->error_msg,kErrorUsagerExisteDeja)=== 0){
			$myUsager->error_msg = kMsgInscriptionUsagerExisteDeja;
		}
		if( empty($myUsager->t_usager['ID_USAGER']) && empty($myUsager->error_msg)){
			$myUsager->error_msg = kMsgInscriptionInfo;
		}
		echo '<p class="error">'.$myUsager->error_msg.'</p>';


		/*

		// Valeurs par défaut
		if(empty($myUsager->t_usager['ID_USAGER'])){
			// Valeurs par défaut
			$myUsager->t_usager['US_ID_ETAT_USAGER']=Usager::getIdEtatUsrFromCode('ACTIF');
			$myUsager->t_usager['US_ID_LANG']=$_SESSION['langue'];
		}else{
			$myUsager->getUsagerGroupe();
			foreach ($myUsager->t_usager_groupe as $idx=>$grp) {
				$idgroupe=$grp['GROUPE']->t_groupe['ID_GROUPE'];
			}
		}
		$myUsager->t_usager['ID_GROUPE']=$idgroupe;*/
		if(empty($myUsager->t_usager['ID_USAGER'])){
			// réplication US_PASSWORD => US_PASSWORD2 pour réaffichage dans le formulaire
			$myUsager->t_usager['US_PASSWORD2']=$myUsager->t_usager['US_PASSWORD'];
			$idgroupe="2";
			$myUsager->t_usager['US_NEWSLETTER']="1";
		}else{
			$myUsager->getUsagerGroupe();
			foreach ($myUsager->t_usager_groupe as $idx=>$grp) {
				$idgroupe=$grp['GROUPE']->t_groupe['ID_GROUPE'];
			}
		}

		include(libDir."class_formSaisie.php");

		if (empty($myUsager->t_usager['ID_USAGER']))
		$xmlform=file_get_contents(getSiteFile("designDir","form/xml/inscription.xml"));
		else
		$xmlform=file_get_contents(getSiteFile("designDir","form/xml/inscription_modification.xml"));

		$myForm=new FormSaisie;
		$myForm->saisieObj=$myUsager;
		$myForm->entity="USAGER";
		$myForm->classLabel="label_champs_form";
		$myForm->classValue="val_champs_form";
		$myForm->display($xmlform);
	}
} 
?>
</div>
</div>
<script type="text/javascript" >
function chkFormInscription(myForm){
	msg='';
	var i;
	arrElts=myForm.getElementsByTagName('*');
	for (i=0;i<arrElts.length;i++) {
		if (!arrElts[i].getAttribute('mandatory')) continue; //test si obligatoire
		
		if (arrElts[i].value.Trim()=='') {
			switch(arrElts[i].getAttribute('label')){
				default :
					lib=arrElts[i].getAttribute('label');
					break;
			}
			if(msg!=''){
				msg+=', ';
			}
			msg=msg+lib;
			arrElts[i].className='errorjs';
		} else {
			arrElts[i].className='';
		}
	}
	if(msg != '' ){
		msg="- <?=kJSErrorOblig?><br />"+msg+"<br />";
	}
	
	patt = new RegExp(/^[A-zÀ-ÿ -\.']+$/);
	if (myForm.us_nom && myForm.us_nom.value!=''&&!patt.test(myForm.us_nom.value)){
		$j("input[name='us_nom']").addClass('errorjs');
		if(msg != ''){
			msg+="<br />";
		}
		msg+='- <?=kJSErrorCharFieldNom?>';
	}
	if (myForm.us_prenom && myForm.us_prenom.value!=''&& !patt.test(myForm.us_prenom.value)){
		$j("input[name='us_prenom']").addClass('errorjs');
		if(msg != ''){
			msg+="<br />";
		}
		msg+='- <?=kJSErrorCharFieldPrenom?>';
	}
	patt = new RegExp(/^[A-zÀ-ÿ0-9 -\.']+$/);
	if (myForm.us_societe && myForm.us_societe.value!='' && !patt.test(myForm.us_societe.value)){
		$j("input[name='us_societe']").addClass('errorjs');
		if(msg != ''){
			msg+="<br />";
		}
		msg+='- <?=kJSErrorCharFieldSociete?>';
	}
	
	if (myForm.us_soc_mail && myForm.us_soc_mail.value!='' && !formCheckEmail(myForm.us_soc_mail.value)){
		$j("input[name='us_soc_mail']").addClass('errorjs');
		if(msg != ''){
			msg+="<br />";
		}
		msg+='- <?=kJSErrorMail?>';
	}
	
	if(myForm.us_password2){
		if (myForm.us_password.value && !formCheckSameVal(myForm.us_password.value,myForm.us_password2.value)) 
		{	
			$j("input[name='us_password'],input[name='us_password2']").addClass('errorjs');
			if(msg != ''){
				msg+="<br />";
			}
			msg+='- <?=kErreurMotDePasseIdentique?>';
		}
	}
	if(!check_passwd_normes_cnil(myForm.us_password.value)){
		$j("input[name='us_password'],input[name='us_password2']").addClass('errorjs');
		if(msg != ''){
			msg+="<br />";
		}
		msg +="- <?=kErreurMdpNonConformeCNIL?>";
	}
	
	if (msg!='') {
		$j("div.signup_wrapper-1 p.error").html(msg);
		return false;
	}
	if(typeof  document.getElementById("us_login") !='undefined' && typeof document.getElementById("us_login") != 'undefined'){
		document.getElementById("us_login").value = document.getElementById("us_soc_mail").value ;
	}
	return true;	
}
</script>