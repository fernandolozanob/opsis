<div id="pageTitre" class="msg"> <?= kDemandeLogin ?></div>
<div id="loginDemande_form" >
	<fieldset>
<?php
if (!$exists) {
?>
	<p class="error"><?=$verreurMessage?></p>
	<p><?=kDemandeLoginTexte;?></p>
	<?php

	include_once(libDir."class_form.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/loginDemande.xml"));
	$myForm=new Form;
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
}else{?>
	<p class="success"><?=$verreurMessage?></p>
	<div class="buttons">
		<button id="back" class="std_button_style" type="button" name="back" onclick="returnFromAjaxPopin()"><?=kRetour?></button>
	</div>
<?}?>
	</fieldset>
</div>
<script>
	function chkFormReinitPw(myform){
		msg='';
		var i;
		arrElts=document.form1.getElementsByTagName('*');
		for (i=0;i<arrElts.length;i++) {
			if (!arrElts[i].getAttribute('mandatory')) continue; //test si obligatoire
			
			if (arrElts[i].value.Trim()=='') {
				switch(arrElts[i].getAttribute('label')){
					default :
						lib=arrElts[i].getAttribute('label');
						break;
				}
				if(msg!=''){
					msg+=', ';
				}
				msg=msg+lib;
				arrElts[i].className='errorjs';
			} else {
				arrElts[i].className='';
			}
		}
		if(msg != '' ){
			msg="- <?=kJSErrorOblig?><br />"+msg+"<br />";
		}
		
		if (document.form1.us_soc_mail && document.form1.us_soc_mail.value!='' && !formCheckEmail(document.form1.us_soc_mail.value)){
			$j("input[name='us_soc_mail']").addClass('errorjs');
			if(msg != ''){
				msg+="<br />";
			}
			msg+='- <?=kJSErrorMail?>';
		}
		
		if (msg!='') {
			$j("#loginDemande_form p.error").html(msg);
			return false;
		}
		return true;	
	}
	
</script>