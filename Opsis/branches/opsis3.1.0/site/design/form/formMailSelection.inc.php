<!-- ENVOI DE LA SELECTION PAR MAIL -->
<?
	$user = User::getInstance();
	if(defined("kMailSujetEnvoi")) $sujetMailForm=constant("kMailSujetEnvoi").gSite;
	else $sujetMailForm="Sélection sur ".gSite;
	if(defined("kDocsSelecSentBy")) $corpsMailForm=constant("kDocsSelecSentBy").$user->Prenom . ' ' . $user->Nom.".";
?>
<div id="mailBloc" class="cadre_mail" style="">

	<div class="contenu_cadre_menu" style="clear:both; float:none">
		<form name="mailForm" class="mailForm">
			<input type="hidden" name="id_panier" value="<?= $myPanier->t_panier['ID_PANIER'] ?>">
			<input type="hidden" value="<?= $myPage->page ?>" name="page" />
			<table style="text-align:center">
				<tr>
					<td class="label_mail">Destinataire</td>
					<td style="text-align:left"><input type='text' name='email_to_send' id='email_to_send' size="50" style="margin-bottom:10px; margin-top : 10px;  border: 1px solid #B6B6B6;border-radius: 3px; font-size: 1.1em; padding: 5px;"/></td>
				</tr>
				<tr>
					<td class="label_mail">Objet</td>
					<td style="text-align:left"><input type='text' name='objetMail' id='objetMail' size="50" style="margin-bottom:10px margin-top : 10px;  border: 1px solid #B6B6B6;border-radius: 3px; font-size: 1.1em; padding: 5px;"/ value="<?= $sujetMailForm ?>"></td>
				</tr>
				<tr>
					<td class="label_mail">Corps</td>
					<td><textarea name="corpsMail" cols="50" rows="3" style="margin-bottom:10px margin-top : 10px;  border: 1px solid #B6B6B6;border-radius: 3px; font-size: 1.1em; padding: 5px;"><?= $corpsMailForm; ?></textarea></td>
				</tr>
				<tr>
					<td class="label_mail">Expéditeur en copie</td>
					<td style="text-align:left"><input type=checkbox name="copyForExp" checked></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input type="button" id='confirm_send_mail' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"   value="<?= kEnvoyer ?>" class="sub_recherche" style= "float:none" onClick="sendMailSubmit();"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<!-- /ENVOI DE LA SELECTION PAR MAIL -->