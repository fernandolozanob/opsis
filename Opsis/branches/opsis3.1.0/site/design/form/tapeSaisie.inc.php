
<script>

function chkFormFields (myform) {
  msg='';
  if (!formCheckNonEmpty(document.getElementById('id_tape').value)) {
    msg+='<?=kJSErrorValeurOblig?>';
    document.getElementById('id_tape').parentNode.className='errormsg';}
  else {
    document.getElementById('id_tape').parentNode.className='val_champs_form';}
  if (msg!='') {alert(msg);return false; } else return true;
}

function restoreAll (myform) {
	if (confirm(str_lang.kConfirmJSRestoreAll)) {
		document.form1.commande.value="RESTORE_ALL";
		document.form1.submit();
    }
    
}
window.onload=function dumb() {	makeDraggable(document.getElementById('chooser'));}

</script>

<div id='pageTitre' class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<?=kCartouche?>
</div>
<div class='error'><?=$myTape->error_msg?></div>

<form name="form1" id="form1" method="post" action="" onsubmit="return chkFormFields(this);">
    <input name="commande" id="commande" type="hidden" value=""/>
	<input name="page" id="page" type="hidden" value=""/>
	<input name="nbLignes" id="nbLignes" type="hidden" value=""/>
    <table border="0">
        <tr>
            <td class="label_champs_form"><?= kIdentifiant ?> :</td>
            <td class="val_champs_form"><input name="id_tape" id='id_tape' type="text" size="20" maxlength="100" value="<?=$myTape->t_tape['ID_TAPE'] ?>"/></td>
        </tr>
		<tr>
		<td class="label_champs_form"><?= kJeu ?> :</td>
		<td class="val_champs_form">
			<select name="id_tape_set" id='id_tape_set'>
			<?= listOptions("select ID_TAPE_SET from t_tape_set order by 1","ID_TAPE_SET",$myTape->t_tape['ID_TAPE_SET']) ?>
			</select>
		</td>
		</tr>
		<tr>
			<td class="label_champs_form"><?= kStatut ?> :</td>
			<td class="val_champs_form"><?= $myTape->status ?></td>
		</tr>
		<tr>
			<td class="label_champs_form"><?= kCapacite ?> :</td>
			<td class="val_champs_form"><?= floor($myTape->t_tape['TAPE_CAPACITE']/1073741824) ?> G</td>
		</tr>
		<tr>
			<td class="label_champs_form"><?= kCapaciteUtilisee ?> :</td>
			<td class="val_champs_form"><?= floor($myTape->t_tape['TAPE_UTILISE']/1073741824) ?> G</td>
		</tr>
		<tr>
			<td class="label_champs_form"><?= kCapaciteRestante ?> :</td>
			<td class="val_champs_form"><?= floor(($myTape->t_tape['TAPE_CAPACITE'] - $myTape->t_tape['TAPE_UTILISE'])/1073741824) ?> G</td>
		</tr>
	</table>

<? if (!empty($myTape->t_tape_file)) { ?>
	<br />
    <h3><?= kListeFichiers ?></h3>
<? 
$myPage=Page::getInstance();
$nbLignes = (!empty($_POST['nbLignes'])?intval($_POST['nbLignes']):"");
if(!empty($nbLignes)) {
	$myPage->nbLignes = $nbLignes;
} else {
	$myPage->nbLignes = 20;
}
$myPage=Page::getInstance();
$page = (!empty($_POST['page'])?intval($_POST['page']):"");
if(!empty($page)) {
	$myPage->page = $page;
} else {
	$myPage->page = 1;
}

$sql="SELECT * FROM t_tape_file WHERE ID_TAPE=".$db->Quote($myTape->t_tape['ID_TAPE']);
$myPage->initPager($sql, 10,$altVar=null,$addGETVars=null,$secstocache=0);
$param = array();
$param["nbLigne"] = $myPage->nbLignes;
$param["page"] = $myPage->page;
$param['source']='tape';
$myPage->addParamsToXSL($param);
$myPage->afficherListe("t_tape_file",getSiteFile("listeDir","tapeFile.xsl"),true,array(),array());

} ?>

<table width="400"  border="0">
		<tr>
			<td align="center"><input name="bSup" type="button"  value="<?= kSupprimer ?>" onClick="removeItem('index.php?urlaction=tapeListe')"/></td>
            <td align="center"><input name="bDup" type="button" id="bOk" value="<?= kRestaurerPurges ?>" onClick="restoreAll()" /></td>
			<td align="center"><input name="bOk" type="submit" id="bOk" value="<?= kEnregistrer ?>" /></td>
		</tr>
	</table>
</form>
<?
$myPage->renderReferrer();
?>