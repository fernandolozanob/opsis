<?php
global $db;
require_once(libDir."class_chercheCat.php");
$mySearch=new RechercheCat();
$mySearch->prepareSQL();
$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs'],$_POST['chNoHist']);
$mySearch->finaliseRequete(); //fin et mise en session

?>
<script type="text/javascript" src='<?=libUrl?>webComponents/opsis/saisie.js'></script>
<script type="text/javascript" src='<?=libUrl?>webComponents/jstree/dist/themes/default/style.css'></script>
<link rel="stylesheet" href='<?=libUrl?>webComponents/jstree/dist/themes/default/style.css' type="text/css" />
<script type="text/javascript">
function removeCat(url)
{
	if(typeof url == 'undefined'){
		url = '';
	}
	if(confirm(str_lang.kConfirmerSuppression)){
		var form=document.getElementById('saisieForm');
		form.action=url;
		form.commande.value="SUP";
		form.submit();
	}
}
function useDocAsVignette(){
	$j.ajax({
		url     : $j(this).attr('action'),
		method    : 'POST',
		data    : $j(this).serialize(),
		success : function( data ) {
			$j(conf.result_element).html(data);
			$j(conf.loading_parent).removeClass('loading');
			initSubmitToAjax(form_selector,test_function,conf);
		},
		error   : function( xhr, err ) {
			console.log(err);
		}
	});
}

function removeDocCat(elt_trash){
	elt_mos_doc = $j(elt_trash).parents("div.resultsMos");
	var id_doc=$j(elt_mos_doc).find('input.input_doc_cat.input_id_doc').val();
	var id_cat=$j("#saisieForm #id_cat").val();
	// $j("#form_doc_cat").append('<input type="hidden" name="doc_cat_suppr[]" value="'+id_doc+'" />')
	console.log("remove doc "+id_doc+" from cat "+id_cat);
	$j.ajax({
		url : "empty.php?urlaction=processCategorie&commande=remove&items="+id_doc+"&id_cat="+id_cat,
		complete:  function(){
		},
		
		success : function(){
			$j(elt_mos_doc).remove();
		}
	
	});
}

window.onload=function()
{
	var tab_cat = $j('#onglets_categorie').tabs();
	<?php
	if (!empty($_POST['form_liste_doc_cat']))
		echo 'tab_cat.tabs(\'select\',1);';
	?>
	if(document.getElementById("user_crea"))
		document.getElementById("user_crea").value='<?= $my_cat->user_crea ?>';
	
	if(document.getElementById("user_modif"))
		document.getElementById("user_modif").value='<?= $my_cat->user_modif ?>';
}

function copyDocCat2Form(){
	if($j("#saisieForm").find("#wrapper_doc_cat").length == 0 ){
		$j("#saisieForm").append('<div id="wrapper_doc_cat" style="display : none ; "></div>');
	}
	input_doc_cat  = $j("#resultats .resultsMos input.input_doc_cat").clone();
	$j("#wrapper_doc_cat").html("") ;
	$j("#wrapper_doc_cat").append(input_doc_cat);
	 
}

function refreshDocCatOrdre(){
	$j(".resultsMos").each(function(idx,elt){
		$j(elt).find("input.input_cdoc_ordre").val(idx);
	});
	copyDocCat2Form();
}


$j(document).on('dnd_stop.vakata', function (e, data) {
		try{
			if(typeof data.event.target != 'undefined'  && data.event.target.id == 'cat_image_upload_input'
			&& typeof data.element != 'undefined' && data.element.id.indexOf('doc_') === 0 ){
				id_doc = data.element.id.replace('doc_','');
				e.preventDefault?e.preventDefault():e.returnValue = false;
				e.stopPropagation?e.stopPropagation():e.cancelBubble = true;
				$j( data.event.target).parents('form').find("#cat_id_doc_image").val(id_doc);
				if($j("#cat_image_upload_img").length>0){
					src_image_helper = $j(data.helper).find(".bgVignetteMos > a > img").attr('src');
					$j("#cat_image_upload_img").attr('src',src_image_helper);
				}
			}
			if(typeof data.event.target != 'undefined' && ( data.event.target.id == 'resultats' || $j("#resultats").find($j(data.event.target)).length>0)){
				// console.log($j(closest_mos_dnd_sort).attr('id')+" "+ $j(data.element).attr('id'));
				if(typeof closest_mos_dnd_sort != 'undefined' && closest_mos_dnd_sort != null  && $j(closest_mos_dnd_sort).attr('id') != $j(data.element).attr('id')){
					// console.log("closest_mos_dnd_sort") ; 
					// console.log(closest_mos_dnd_sort) ; 
					test = $j(data.element).clone(true);
					test.insertBefore(closest_mos_dnd_sort);
					$j(data.element).remove() ; 
					refreshDocCatOrdre();
				}
			}
		}catch(e){
			console.log("crash dnd_stop"+e);
		}
	});
	
	var closest_mos_dnd_sort = null ; 
	
$j(document).on('dnd_move.vakata', function (e, data) {
	try{
		if(data.event.target.id == 'cat_image_upload_input'){
			$j(data.helper).find('.jstree-icon').eq(0).removeClass('jstree-er');
			$j(data.helper).find('.jstree-icon').eq(0).addClass('jstree-ok');
			data.bypass_evt = true ; 
		}
		if(data.event.target.id == 'resultats' || $j("#resultats").find($j(data.event.target)).length>0){
			$j(data.helper).find('.jstree-icon').eq(0).removeClass('jstree-er');
			$j(data.helper).find('.jstree-icon').eq(0).addClass('jstree-ok');
			data.bypass_evt = true ; 
			
			if(typeof placeholder_resultats == 'undefined'){
				placeholder_resultats = $j('<div "placeholder_sort"></div>');
				placeholder_resultats.attr('style','border:1px solid #DADADA ; width:'+placeholder_resultats.get(0).offsetWidth+"px;height "+placeholder_resultats.get(0).offsetHeight+"px;");
			}
			
			
			
			if($j(data.event.target).hasClass('resultsMos') || $j(data.event.target).parents('.resultsMos').length>0 ){
			// determiner l'élément sur lequel on est (=> insertion juste avant)
				if($j(data.event.target).hasClass('resutlsMos')){
					closest_mos = $j(data.event.target);
				}else{
					closest_mos = $j(data.event.target).parents('.resultsMos');
				}
				closest_mos_dnd_sort = closest_mos ; 
				// console.log(" on insere placeholder à gauche de la target");
				// console.log(closest_mos);
			}else if (data.event.target.id === 'resultats'){
					closest_mos = null ; 
					closest_prox= null ; 
					pos_event = {'left':data.event.offsetX,'top':data.event.offsetY};
					event_prox = pos_event.left + $j("#resultats").width()*(Math.floor(pos_event.top/$j(".resultsMos").eq(0).height()));
				$j(".resultsMos.draggable_doc_cat").each(function(idx,elt){
					pos_test = $j(elt).position() ; 
					pos_test.top = pos_test.top + ($j(elt).height()/2);
					pos_test.left = pos_test.left + ($j(elt).width()/2);
					
					test_prox = pos_test.left + $j("#resultats").width()*(Math.floor(pos_test.top/$j(elt).height()));
					abs_prox = Math.abs(test_prox - event_prox);
					
					
					if(closest_mos === null || abs_prox <= closest_prox){
						closest_prox = abs_prox;
						true_prox = event_prox - test_prox;
						closest_mos = $j(elt);
					}
				});
				//closest_mos représente alors la mosaique la plus proche à droite du curseur. 
				// si closest_mos est supérieur à la largeur d'une mosaique, alors on est dans le cas où on insère en fin de mosaique. 
				if(true_prox > $j(".resultsMos").eq(0).width()){
					closest_mos = $j("div.pusher");
				}
				closest_mos_dnd_sort = closest_mos ; 
				// console.log("closest_prox");
				// console.log(closest_prox);
				// console.log("true_prox");
				// console.log(true_prox);
				// console.log("closest_mos");
				// console.log(closest_mos);
				// console.log("\n\n\n");
			}
		}
	}catch(e){
		console.log("dnd_move crash : "+e);
	}
	

});

</script>
<?// MS -07.04.16 lorsqu'on accède à ce formulaire depuis la palette, on a pas besoin de menu gauche 
if(isset($_SESSION['USER']['layout_opts']['layout'])){
	$layout = $_SESSION['USER']['layout_opts']['layout'];
}else{
	$layout = '2';
}
$myPage=Page::getInstance();

if(!isset($_GET['fromPalette']) || !$_GET['fromPalette']){?>
	<div id="menuGauche" class="menuAdmin <?= ($layout<1 ||  $layout>2)?'collapsed':'';?>" >
	  <div class="title_bar" style="white-space:nowrap;">
	   <?= kCategorie ?>   
		<span id="arrow_left" class="toggle_arrow" onclick="toggleLeftMenu();">&nbsp;</span>
	  </div>
	<div id="tree_wrapper" class="menuGauche_wrapper">
	  <?
	//arbres
	include_once(libDir."class_jsTree.php");
	$res = $db->getCol("SELECT distinct ID_TYPE_CAT FROM t_type_cat");
	foreach ($res as $cat) {
		$mySearch->treeParams=array("ID_TYPE_CAT"=>$cat);
		$btnEdit=$myPage->getName()."?urlaction=catSaisie&id_cat=";
		$myTree=new jsTree($mySearch,'form1');
		$myTree->JSReturnFunction=$jsFunction;
		$myTree->treeParams=array("ID_TYPE_CAT"=>$cat,'CURR_ID_CAT'=>$my_cat->t_categorie['ID_CAT']);
		$myTree->id_div_tree='tree_'.$cat;
		$myTree->dnd_in_tree=1;
		$myTree->dnd_out_tree=1;
		$myTree->addNodeByContextMenu=1;
		$myTree->JSlink="window.location.href='".$myPage->getName()."?urlaction=catSaisie&id_cat="; //Le lien sur les terme ouvrira l'édition
		$myTree->makeRoot();
		$myTree->renderOutput('',true,$btnEdit);
		//si focus sur un noeud, on le traite en JS apres affichage du début de l arbre
		if (!empty($_GET['id_cat'])) $myTree->revealNode($_GET['id_cat']);
	}

	?>
	<div id="une" class="demo"></div>
	<div id="theme" class="demo"></div>
	  
	</div>
	</div>
	<?}?>


<div id="mainResultsBar" class="pageTitre title_bar  pres_<?=$_SESSION['USER']['layout_opts']['layout'];?>"><?=$my_cat->t_categorie['CAT_NOM']?>
	<!--div class="toolbar">
		<div class="presentation">
			<span id="listChooser" class="<?=($_REQUEST['xsl']=='panier2')?'selected':'';?>" onclick="presentation_updateLignes('docListe2.xsl')"></span>
			<span id="mosChooser"  class="<?=($_REQUEST['xsl']=='panier')?'selected':'';?>" onclick="presentation_updateLignes('docListe.xsl')"></span>
		</div>
	</div-->
</div>
<div id="main_block" class=" apply_layout pres_<?=$_SESSION['USER']['layout_opts']['layout'];?>">
<?php
	include(libDir."class_formSaisie.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/catSaisie.xml"));
	$myForm=new FormSaisie;
	$myForm->saisieObj=$my_cat;
	$myForm->entity="CAT";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->classHelp="miniHelp";
	$myForm->display($xmlform);
	if(!empty($_REQUEST['id_cat']) && (!isset($_GET['fromPalette']) || !$_GET['fromPalette'])){
		// $myPage->setReferrer(true);
		$xsl=getSiteFile("designDir","liste/catDocListe.xsl");
		
		/* IMPLEMENTATION VIA DOC_CAT*/
		$my_cat->getDocCat();
		$myPageSearch = new Page();
		
		if(isset($_REQUEST['page']) && !empty($_REQUEST['page'])){
			$myPageSearch->page = $_REQUEST['page'];
		}else{
			$myPageSearch->page = 1 ; 
		}
		if(isset($_REQUEST['nbLignes']) && !empty($_REQUEST['nbLignes'])){
			$myPageSearch->nbLignes= $_REQUEST['nbLignes'];
		}else{
			$myPageSearch->nbLignes = (defined("gNbLignesListeDefaut")?gNbLignesListeDefaut:10); 
		}
		
		$myPageSearch->initPagerFromArray($my_cat->t_doc_cat);
		
		/********************/

		$param["profil"] = User::getInstance()->getTypeLog();
		// $param["xmlfile"] = getSiteFile('listeDir','xml/docListe.xml');
		$param["nbLigne"] = $myPageSearch->nbLignes;
		$param["nbRows"] = $myPageSearch->found_rows;
		$param["page"] = $myPageSearch->page;
		$param["playerVideo"]=gPlayerVideo;
		$param["pager_link"]=$myPageSearch->PagerLink;
		$param["offset"] =  $myPageSearch->nbLignes*($myPageSearch->page-1);
		$param["cartList"]=isset($cartList)?$cartList:'';
		$param["urlparams"]=$myPageSearch->addUrlParams();
		$param["titre"]="cat";
		$param["frameurl"]=frameUrl;
		$param["pagination"]='1';
		$myPageSearch->addParamsToXSL($param);
		$myPageSearch->afficherListe("doc",$xsl,true);
	}
	?>
	</div>
	<?if(!isset($_GET['fromPalette']) || !$_GET['fromPalette']){?>
	<!-- Copie manuelle du template rechercheRightPreview => on ne pouvait pas aisément utiliser le template XSL sans créer deux listes xsl -->
	<div id="menuDroite" class="<?= ( $_SESSION['USER']['layout_opts']['layout']<2)?'collapsed':'';?>">
		<div class="title_bar"><span id="arrow_right" class="toggle_arrow" onclick="toggleRightPreview();">&#160;</span><?echo kPreview;?></div>
		<div id="previewDoc">
			<h2 id="prevTitre">&#160;</h2>
			<div id="prevMedia">&#160;</div>
			<div id="prevWrapper">
			<div id="prevInfos">&#160;</div>
			<div id="prevLink"><button type="button"><? echo kVoirFicheDoc;?></button></div>
			</div>
		</div>
	</div>
	
<a href="index.php?urlaction=catListe"><?=kRetour?></a>
<?}?>




