<div class="title_bar">
	<div id="backButton"><a class="icoBackSearch" href="index.php?urlaction=admin">
		<?=kRetourAdmin;?>
	</a></div>
	<?= kStatistiquesOpsis ?>
	<div class="toolbar">
		<div  onclick="triggerPrint('stat')" class=" print tool">
			<span class="tool_hover_text"><?echo kImprimer;?></span>
		</div>
		<div  class="menu_actions tool"><span class="tool_hover_text"><?echo kMenuActions;?></span>
			<div class="drop_down" style="display:none;">
				<ul>
					<li onclick="displayMenuActions('export','stat');"><? print kExporter?></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<? if (defined("gAwstatsUrl")){ ?>
<a href="?urlaction=stats_web" ><?=kStatistiquesWeb?></a>
<? } ?>
<div class="feature contentBody">
	<?
	//Formulaire
	$arrPrecis["-"] = "Global";
	$arrPrecis["4"] = "Année";
	$arrPrecis["7"] = "Mois";
	$arrPrecis["10"] = "Jour";

	$arrDistrib[""] = "Global";
	$arrDistrib["g.ID_GROUPE/g.GROUPE"] = "Groupe";
	$arrDistrib["u.ID_USAGER/".$db->Concat("u.US_NOM", "' '", "u.US_PRENOM")] = "Utilisateur";
	$arrDistrib["a.ACT_PAYS"] = "Pays";

	$arrEmptyLegend["USAGER"] = "Visiteur";
	if($stat_dist=="u.ID_USAGER/CONCAT(u.US_NOM,' ',u.US_PRENOM)") $arrEmptyLegend["DISTRIB"] = "Visiteur";
	if($stat_dist=="g.ID_GROUPE/g.GROUPE") $arrEmptyLegend["DISTRIB"] = "Visiteurs";
	$myStats->initEmptyLegend($arrEmptyLegend);

	//Stats
    $sql_date="";
    if(!empty($_POST['stat_date_deb'])){
        $sql_date.=" AND ACT_DATE >=".$db->Quote($_POST['stat_date_deb']);
    }
    if(!empty($_POST['stat_date_fin'])){
        $sql_date.=" AND ACT_DATE <=".$db->Quote($_POST['stat_date_fin']);
    }
    $Stat["Connexions"]['SELECT'] = $db->Concat("u.US_NOM", "' '", "u.US_PRENOM")." AS USAGER";
	//$Stat["Connexions"]['WHERE'] = "a.ACT_TYPE='IN'";
	$Stat["Connexions"]['JOIN'] = "INNER JOIN (SELECT min( a4.ID_ACTION ) ID_ACTION, a4.ID_USAGER FROM t_action a4 INNER JOIN (
										SELECT ACT_SESSKEY, max( ID_USAGER ) ID_USAGER, cast(ACT_DATE as date) SUBDATE FROM t_action WHERE 1=1 ".$sql_date." GROUP BY ACT_SESSKEY, SUBDATE
									)a3 ON a4.ACT_SESSKEY = a3.ACT_SESSKEY AND a4.ID_USAGER = a3.ID_USAGER AND a3.SUBDATE = cast(a4.ACT_DATE as date) ".$sql_date."
									GROUP BY a4.ACT_SESSKEY, a4.ID_USAGER, cast(a4.ACT_DATE as date)) a2 ON a2.ID_ACTION = a.ID_ACTION AND a2.ID_USAGER = a.ID_USAGER";
	//$Stat["Connexions"]['WHERE'] = "u.ID_USAGER is not null";
	$Stat["Connexions"]['WHERE'] = "1=1";
	$Stat["Connexions"]['LIB']['NB'] = kNbConnexions;

	$Stat["Notices"]['SELECT'] = "d.DOC_TITRE as TITRE,d.DOC_COTE as COTE";
	$Stat["Notices"]['JOIN'] = "LEFT OUTER JOIN t_doc d ON d.ID_DOC = a.ID_DOC AND d.ID_LANG = ".$db->quote($_SESSION['langue']);
	$Stat["Notices"]['WHERE'] = "a.ACT_TYPE='DOC' AND d.ID_DOC is not null AND d.ID_LANG='".$_SESSION['langue']."' ";
	$Stat["Notices"]['LIB']['NB'] = kNbNotices;
        
	$Stat["Visionnages"]['SELECT'] = "d.DOC_TITRE as TITRE,d.DOC_COTE as COTE";
	$Stat["Visionnages"]['JOIN'] = "LEFT OUTER JOIN t_doc d ON d.ID_DOC = a.ID_DOC AND d.ID_LANG = ".$db->quote($_SESSION['langue']);
	$Stat["Visionnages"]['WHERE'] = "a.ACT_TYPE='VIS' AND d.ID_DOC is not null";
	$Stat["Visionnages"]['LIB']['NB'] = kNbVisios;

	$Stat["Téléchargements"]['SELECT'] = "d.DOC_TITRE as TITRE,d.DOC_COTE as COTE";
	$Stat["Téléchargements"]['SELECTBRUT'] = "a.ACT_TYPE_MAT as MEDIA";
	$Stat["Téléchargements"]['JOIN'] = "LEFT OUTER JOIN t_doc d ON d.ID_DOC = a.ID_DOC AND d.ID_LANG = ".$db->quote($_SESSION['langue']);
	$Stat["Téléchargements"]['WHERE'] = "a.ACT_TYPE='LIV' AND d.ID_DOC is not null";
	$Stat["Téléchargements"]['LIB']['NB'] = kNbTelechargements;

	$Stat["Recherches"]['SELECT'] = "a.ACT_REQ as REQUETE, a.ACT_REQ_NB_DOC as NB_RES";
	//$Stat["Recherches"]['JOIN'] = "LEFT OUTER JOIN t_requete r on a.ACT_REQ=r.REQ_LIBRE and a.ID_USAGER=r.ID_USAGER";
	$Stat["Recherches"]['WHERE'] = "a.ACT_TYPE='SQL' AND a.ACT_REQ_NB_DOC is not null AND a.ACT_REQ not like 'Document'";
	$Stat["Recherches"]['LIB']['ACT_REQ'] = kRequete;
	$Stat["Recherches"]['LIB']['NB_RES'] = kNbResultats;
	$Stat["Recherches"]['LIB']['NB'] = kNbRequetes;

	$Stat["Imports"]['ENTITY'] = "doc";
	$Stat["Imports"]['SELECT'] = "d.DOC_TITRE as TITRE,d.DOC_COTE as COTE,t1.TYPE_DOC as TYPE";
	$Stat["Imports"]['JOIN'] = "LEFT OUTER JOIN t_type_doc t1 ON t1.ID_TYPE_DOC = d.DOC_ID_TYPE_DOC AND t1.ID_LANG = ".$db->quote($_SESSION['langue']);
	$Stat["Imports"]['WHERE'] = "1=1";
	$Stat["Imports"]['LIB']['NB'] = kNbImports;

	$Stat["Commandes"]['ENTITY'] = "panier";
	$Stat["Commandes"]['SELECT'] = "t1.TYPE_COMMANDE as TYPE,t2.ETAT_PAN as ETAT";
	$Stat["Commandes"]['JOIN'] = "LEFT OUTER JOIN t_type_commande t1 ON t1.ID_TYPE_COMMANDE = p.PAN_ID_TYPE_COMMANDE AND t1.ID_LANG = ".$db->quote($_SESSION['langue']);
	$Stat["Commandes"]['JOIN'] .= "LEFT OUTER JOIN t_etat_pan t2 ON t2.ID_ETAT_PAN = p.PAN_ID_ETAT AND t2.ID_LANG = ".$db->quote($_SESSION['langue']);
	$Stat["Commandes"]['WHERE'] = "PAN_ID_ETAT>1";
	$Stat["Commandes"]['LIB']['NB'] = kNbCommandes;


	$Stat['LIB']['USAGER'] = kUsager;
	$Stat['LIB']['GROUPE'] = kGroupe;
	$Stat['LIB']['DATE'] = kDate;
	$Stat['LIB']['COTE'] = kReference;
	$Stat['LIB']['TITRE'] = kTitre;
	$Stat['LIB']['PAYS'] = kPays;
	$Stat['LIB']['REQUETE'] = kRequete;
	$Stat['LIB']['MEDIA'] = kTypeMedia;
	$Stat['LIB']['TYPE'] = kType;
	$Stat['LIB']['ETAT'] = kEtat;

	if(isset($arrPrecis[$stat_precis])) $Stat['LIB']['PRECIS'] = $arrPrecis[$stat_precis];
	if(isset($arrDistrib[$stat_dist])) $Stat['LIB']['DISTRIB'] = $arrDistrib[$stat_dist];

	$myStats->init($Stat);
    /*
	$arrTmp = $db->getAll("SELECT DISTINCT extract(YEAR from a.ACT_DATE) as YEAR FROM t_action a ORDER BY YEAR");
	$lastVal = 0;
	foreach ($arrTmp as $year) {
		if ($lastVal != 0 && intval($year['YEAR']) - intval($lastVal) > 1) {
			$k = intval($year['YEAR']) - intval($lastVal);
			while ($k > 1) {
				$arrYear[] = $lastVal + (intval($year['YEAR']) - intval($lastVal) - $k + 1);
				$k--;
			}
		}
		$arrYear[] = $year['YEAR'];
		$lastVal = $year['YEAR'];
	}*/
    $arrTmp = $db->GetRow("SELECT extract(YEAR from min(ACT_DATE)) as MIN,extract(YEAR from max(ACT_DATE)) as MAX FROM t_action");
    if(count($arrTmp)>0){
        for($k=$arrTmp['MIN']; $k<=$arrTmp['MAX']; $k++){
            $arrYear[] = $k;
        }
    }
	?>
	<div id='chooser' class='iframechoose' name='chooser' style='position:absolute;display:none;'></div>
	<form name="form1" id="form1" method="post" action="index.php?urlaction=stats_app" onsubmit="$j(body).addClass('loading');" >
		<div align="center">
		<!--width:520px-->
		<div class='advancedForm'>
			<a href="javascript:toggleVisibility(document.getElementById('stats_search'),document.getElementById('arrowstats_search'))" >
			<img src='design/images/arrow_down.gif' align='absmiddle' border='0' id='arrowstats_search' /><?=kRecherche;?></a>
		</div>
		<fieldset style="width:660px; margin-bottom:5px;display:block;border:1px solid #aaa" id="stats_search">
			<!--legend><?=kRecherche?></legend-->
			<table width="80%" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td class='label_champs_form'><?= kType ?></td>
				<td class='val_champs_form'>
					<select id="stat_sql"  name="stat_sql" onchange="javascript:hideType();">
					<?php
						// Affichage des valeurs
						foreach($myStats->arrSql as $index => $val){
							print "<option value='$index'";
							if($stat_sql==$index) print " selected ";
							print ">".$index."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			
			<tr>
				<td class='label_champs_form' rowspan="2"><?=kPeriode?></td>
				<td class='val_champs_form'>
					<select name="stat_year" id="stat_year" value="<?= $stat_year ?>" onchange="updateStatDate()" />
						<option value=""></option>
						<?php
							foreach($arrYear as $val){
								print "<option value='$val'";
								if($val==$stat_year) print " selected ";
								print ">".$val."</option>";
							}
						?>
					</select>
					<select name="stat_month" id="stat_month" value="<?= $stat_month ?>" onchange="updateStatDate()" />
						<option value="" <? if($stat_month=="") print " selected ";?>></option>
						<option value="1" <? if($stat_month=="1") print " selected ";?>>Janvier</option>
						<option value="2" <? if($stat_month=="2") print " selected ";?>>Février</option>
						<option value="3" <? if($stat_month=="3") print " selected ";?>>Mars</option>
						<option value="4" <? if($stat_month=="4") print " selected ";?>>Avril</option>
						<option value="5" <? if($stat_month=="5") print " selected ";?>>Mai</option>
						<option value="6" <? if($stat_month=="6") print " selected ";?>>Juin</option>
						<option value="7" <? if($stat_month=="7") print " selected ";?>>Juillet</option>
						<option value="8" <? if($stat_month=="8") print " selected ";?>>Août</option>
						<option value="9" <? if($stat_month=="9") print " selected ";?>>Septembre</option>
						<option value="10" <? if($stat_month=="10") print " selected ";?>>Octobre</option>
						<option value="11" <? if($stat_month=="11") print " selected ";?>>Novembre</option>
						<option value="12" <? if($stat_month=="12") print " selected ";?>>Décembre</option>
					</select>
					<!--input type="button" onclick="updateStatDate()" value="OK" /-->
				</td>
			</tr>
			<tr>
				<td class='val_champs_form'>
					<label for="stat_date_deb"><?=kDu?></label>
					<input name="stat_date_deb" id="stat_date_deb" value="<?= $stat_date_deb ?>" onFocus="formatDate(this)" onKeyPress="formatDate(this)" onKeyUp="formatDate(this)" onBlur="checkDate(this)" onChange="checkDate(this)" size="10" />
					<label for="stat_date_fin"><?=kAu?></label>
					<input name="stat_date_fin" id="stat_date_fin" value="<?= $stat_date_fin ?>" onFocus="formatDate(this)" onKeyPress="formatDate(this)" onKeyUp="formatDate(this)" onBlur="checkDate(this)" onChange="checkDate(this)" size="10" />
				</td>
			</tr>
			<tr>
				<td class='label_champs_form'><?=kGroupe?></td>
				<td class='val_champs_form'>
					<select name="stat_groupe" id="stat_groupe" />
						<option value="" <? if($stat_groupe=="") print " selected ";?>></option>
						<? listOptions("select ID_GROUPE, GROUPE from t_groupe", array("ID_GROUPE", "GROUPE"), $stat_groupe); ?>
					</select>
				</td>
			</tr>
			
				<tr>
				<td class='label_champs_form'><?=kMedia?></td>
				<td class='val_champs_form'>
					<select name="stat_media" id="stat_media" />
						<option value="" <? if($stat_media=="") print " selected ";?>>Tous</option>
						<? listOptions("select ID_MEDIA, MEDIA from t_media", array("ID_MEDIA", "MEDIA"), $stat_media); ?>
					</select>
				</td>
			</tr>
			<tr>
				<td class='label_champs_form'><?=kUsager?></td>
				<td class='val_champs_form'>
					<input name="stat_usager" id="stat_usager" value="<?= $stat_usager ?>" /><input type="button" label="" value="Choisir..." style="cursor: pointer;" onclick="choose(document.getElementById(&quot;stat_usager&quot;),&quot;titre_index=<?=kUsager?>&amp;id_lang=FR&amp;valeur=&amp;champ=USR&amp;xsl=paletteSimple&quot;,'',0,0);return false;">
				</td>
			</tr>
		</table>
		</fieldset>
		
		<div class='advancedForm'>
			<a href="javascript:toggleVisibility(document.getElementById('stats_pres'),document.getElementById('arrowstats_pres'))" >
			<img src='design/images/arrow_down.gif' align='absmiddle' border='0' id='arrowstats_pres' /><?=kPresentation;?></a>
		</div>
		<fieldset style="width:660px; margin-bottom:5px;display:block;border:1px solid #aaa" id="stats_pres">
			<!--legend><?=kPresentation?></legend-->
			<table width="80%" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td class='label_champs_form'><?= kType ?></td>
				<td class='val_champs_form'>
					<select name="stat_pres" onchange="toggleDisplayStat(this.value)">
						<option <? if($stat_pres=="FIL") print " selected ";?> value="FIL">Données filtrées</option>
						<option <? if($stat_pres=="BRUT") print " selected ";?> value="BRUT">Données brutes</option>
						<option <? if($stat_pres=="TOP") print " selected ";?> value="TOP">Top</option>
					</select>
				</td>
			</tr>
			<tr class="rowForFiltre">
				<td class='label_champs_form'><?= kPrecision ?></td>
				<td class='val_champs_form'>
					<select name="stat_precis">
						<?
						foreach ($arrPrecis as $val=>$lib) {
							echo '<option '.($stat_precis==$val?" selected ":"").'value="'.$val.'">'.$lib."</option>";
						}
						?>
					</select>
				</td>
			</tr>
			<tr class="rowForFiltre">
				<td class='label_champs_form'><?= kDistribution ?></td>
				<td class='val_champs_form'>
					<select name="stat_dist">
						<?
						foreach ($arrDistrib as $val=>$lib) {
							echo '<option '.($stat_dist==$val?" selected ":"").'value="'.$val.'">'.$lib."</option>";
						}
						?>
					</select>
				</td>
			</tr>
			</table>
		</fieldset>
		<input name="bOK" class="std_button_style" id="bChercher" type="submit" value="<?= kChercher ?>"/>
		</div>
	</form>

	<script>
		$j(document).ready(function () {
			$j("#tabs_stat").tabs({
				active : 1 
			});
		});
		
		var dp_config =  {  
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'] ,
			monthNames: [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" ],
			firstDay: 1 
		};
		$j("#stat_date_deb").datepicker(dp_config);
		$j("#stat_date_fin").datepicker(dp_config);
		
		toggleDisplayStat("<?=$stat_pres;?>");
		
		function updateStatDate() {
			var year = $j("#stat_year").val();
			var month = $j("#stat_month").val();
			if (month == "" && year == "") updateStatDateValues("","");
			else if (month == "" && year != "") {
				updateStatDateValues(year+"-01-01",year+"-12-31");
			} else if (month != "" && year != "") {
				if (month.length == 1) month = "0"+month;
				var day = new Date(year+"-"+month+"-01");
				var dayl = new Date(DateToString(day));
				dayl.setMonth(dayl.getMonth() + 1);
				dayl.setDate(dayl.getDate() - 1);
				updateStatDateValues(DateToString(day),DateToString(dayl));
			}
		}
		
		function updateStatDateValues(debut,fin) {
			$j("#stat_date_deb").val(debut);
			$j("#stat_date_fin").val(fin);
		}
		
		function DateToString(date) {
			var y = ""+date.getFullYear();
			var m = ""+(date.getMonth()+1);
			if (m.length == 1) m = "0"+m;
			var d = ""+date.getDate();
			if (d.length == 1) d = "0"+d;
			return y+"-"+m+"-"+d;
		}
		
		function toggleDisplayStat(value) {
			if (value == "FIL" || value == "") $j(".rowForFiltre").show();
			else $j(".rowForFiltre").hide();
		}
		
		function hideType(){
			if((document.getElementById('stat_sql').value == 'Recherches') || (document.getElementById('stat_sql').value == 'Connexions') || (document.getElementById('stat_sql').value == 'Commandes')) {
				document.getElementById('stat_media').disabled = true;
			} 
			else {
				document.getElementById('stat_media').disabled = false;
			}
			
			if(document.getElementById('stat_sql').value == 'Imports'){
				$j('select').children('option[value="TOP"]').attr('disabled', true);
			}else{
				$j('select').children('option[value="TOP"]').attr('disabled', false);
			}
			if(document.getElementById('stat_sql').value == 'Commandes' || document.getElementById('stat_sql').value == 'Imports'){
				$j('select').children('option[value="a.ACT_PAYS"]').attr('disabled', true);
			}else{
				$j('select').children('option[value="a.ACT_PAYS"]').attr('disabled', false);
			}
		}
	</script>
</div>
