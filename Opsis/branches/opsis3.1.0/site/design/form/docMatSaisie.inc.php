<div class="feature"> 
 <script language="Javascript" type="text/javascript">
	function chkFormFields (myform) {
		msg='';

		// Ajouter ls tests nécessaires
		// Conversion des dates
		if(document.getElementById('dmat_date_etat_conserv')) document.getElementById('dmat_date_etat_conserv').value=convDate(document.getElementById('dmat_date_etat_conserv').value,'aaaa-mm-jj');
		if(document.getElementById('dmat_date_copie')) document.getElementById('dmat_date_copie').value=convDate(document.getElementById('dmat_date_copie').value,'aaaa-mm-jj');
		if(document.getElementById("dmat_date_crea")) document.getElementById('dmat_date_crea').value=convDate(document.getElementById('dmat_date_crea').value,'aaaa-mm-jj');
		if(document.getElementById("dmat_date_mod")) document.getElementById('dmat_date_mod').value=convDate(document.getElementById('dmat_date_mod').value,'aaaa-mm-jj');

		if (msg!='') {alert(msg);return false; } else return true;	
		
	}
		
		
		//by LD : pour ajouter une valeur dynamiquement � un tableau avec DOM.
	function addValue(id_champ,valeur,idx,role) {

		//alert(id_champ+" val:"+valeur + " idx:"+idx);
		arrField=id_champ.split('$');
		type=arrField[0];
		cntFields=0;
		
		if (document.getElementById(type+'$limit')) limit=document.getElementById(type+'$limit').value; else limit=250;
		allowdouble=document.getElementById(type+'$allowdouble'); 
		
		// ajouter test sur valeur existante
		prt=document.getElementById(type+'$tab');

		
		for(i=0;i<prt.childNodes.length;i++) {
			chld=prt.childNodes[i];
			
			if (chld.id && chld.id.indexOf('$DIV$')!=-1) cntFields++;
			if (cntFields==limit) {
				alert (limit+'<?=kDOMlimitvalues?>');
				return false;
				}
			
			if (chld.id==type+'$DIV$'+idx && !allowdouble) {
				alert('<?=kDOMnodouble?>'); return false;
				
			}
		}
		btnAdd=document.getElementById(type+'$add');
		
		newFld=document.createElement('DIV');
		newFld.id=type+'$DIV$'+idx;
		newFld.name=type+'$DIV$'+idx;
		newFld.className=prt.className;
		addRow=new Array(newFld.id,valeur,idx,role)
		str=eval('add_'+type+'(addRow)');
		newFld.innerHTML=str[1];
		if (btnAdd) {prt.insertBefore(newFld,btnAdd);} else {prt.appendChild(newFld);}
	}
	

	
	function removeDocMat(url) {
	    if (confirm("<?=kConfirmJSDocSuppression?>")) {
	        document.form1.commande.value="SUP";
	        document.form1.page.value=url;
	        document.form1.submit();
	        }
	}
	

</script>

<div id='pageTitre'><?= kSaisieSupport." S ".$myDmat->t_doc_mat["ID_DOC_MAT"] ?></div>


<div class='label_champs_form'>Identification</div>
<FIELDSET NAME='ident' ID='ident' CLASS='saisie_fieldset' style='display:block' >

<?
if(preg_match("|<FONDS>(.*)</FONDS+>|",$myDmat->t_doc['DOC']->t_doc['DOC_XML'],$matches)) $fonds=strtok($matches[1],"<"); // au cas où il y ait plusieurs <fonds>
if(preg_match("|<COUL>(.*)</COUL+>|",$myDmat->t_doc['DOC']->t_doc['DOC_VAL'],$matches)) $couleur=strtok($matches[1],"<");
$myDmat->t_mat['MAT']->getValeurs();
foreach($myDmat->t_mat['MAT']->t_mat_val as $idx=>$val){
	if($val['VAL_ID_TYPE_VAL']=="NAT") $nature=$val['VALEUR'];
}

?>

<div class="label_champs_form"><?= kDocument ?></div>
<table border='0' cellspacing='0' cellpadding='0' width='100%'>
<tr>
	<td class='resultsHead' valign='top' ><?= kNumeroDocument ?></td>
	<td class='resultsHead' valign='top' ><?= kReference ?></td>
	<td class='resultsHead' valign='top' ><?= kTitre ?></td>
	<td class='resultsHead' valign='top' ><?= kDate ?><br/><?= kCollection ?></td>
	<td class='resultsHead' valign='top' ><?= kDuree ?><br/><?= kCouleur ?></td>
</tr>
<tr>
	<td class='resultsCorps' ><a href="javascript:popupDoc('index.php?urlaction=doc&amp;id_doc=<?= $myDmat->t_doc['DOC']->t_doc['ID_DOC'] ?>','docmat','doc<?=$myDmat->t_doc['DOC']->t_doc['ID_DOC']?>')"><?= "D ".$myDmat->t_doc['DOC']->t_doc['ID_DOC'] ?></a></td>
	<td class='resultsCorps' ><?= $myDmat->t_doc['DOC']->t_doc['DOC_COTE'] ?></td>
	<td class='resultsCorps' ><?= $myDmat->t_doc['DOC']->t_doc['DOC_TITRE'] ?></td>
	<td class='resultsCorps' ><?= $myDmat->t_doc['DOC']->t_doc['DOC_DATE_PROD'] ?>&nbsp;<br/><?= $fonds ?></td>
	<td class='resultsCorps' ><?= $myDmat->t_doc['DOC']->t_doc['DOC_DUREE'] ?>&nbsp;<br/><?= $couleur ?></td>
</tr>
</table>
<div class="label_champs_form"><?= kMateriel ?></div>
<table border='0' cellspacing='0' cellpadding='0' width='100%'>
	<tr>
		<td class='resultsHead' valign='top' ><?= kNumeroMateriel ?></td>
		<td class='resultsHead' valign='top' ><?= kNumeroBande ?></td>
		<td class='resultsHead' valign='top' ><?= kFormat ?></td>
		<td class='resultsHead' valign='top' ><?= kNature ?></td>
		<td class='resultsHead' valign='top' ><?= kNoBandeOrigine ?></td>
	</tr>
	<tr>
		<td class='resultsCorps' ><a href="javascript:popupDoc('index.php?urlaction=mat&amp;id_mat=<?= $myDmat->t_mat['MAT']->t_mat['ID_MAT'] ?>','docmat','<?=str_replace(".","_",$myDmat->t_mat['MAT']->t_mat['ID_MAT'])?>')"><?= $myDmat->t_mat['MAT']->t_mat['MAT_NOM'] ?></a>&nbsp;</td>
		<td class='resultsCorps' ><?= $myDmat->t_mat['MAT']->t_mat['MAT_GROUPE'] ?>&nbsp;</td>
		<td class='resultsCorps' ><?= $myDmat->t_mat['MAT']->t_mat['MAT_FORMAT'] ?>&nbsp;</td>
		<td class='resultsCorps' ><?= $nature ?>&nbsp;</td>
		<td class='resultsCorps' ><?= $myDmat->t_mat['MAT']->t_mat['MAT_COTE_ORI'] ?>&nbsp;</td>
	</tr>
	</table>
</FIELDSET>
<?
debug($myDmat);
include(libDir."class_formSaisie.php");
$xmlform=file_get_contents(designDir."form/xml/docMatSaisie.xml");
$xmlform=sprintf($xmlform,$myDmat->t_doc_mat['ID_DOC'],$myDmat->t_doc_mat['ID_MAT']);
//debug($xmlform);
$myForm=new FormSaisie;
$myForm->saisieObj=$myDmat;
$myForm->entity="DMAT";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->display($xmlform);



?>

<script>
calcDuree();
if(document.getElementById("user_crea")) document.getElementById("user_crea").value='<?= $myDmat->user_crea ?>';
if(document.getElementById("user_modif")) document.getElementById("user_modif").value='<?= $myDmat->user_modif ?>';

if(document.getElementById("dmat_date_etat_conserv")) document.getElementById('dmat_date_etat_conserv').value=convDate(document.getElementById('dmat_date_etat_conserv').value,'jj/mm/aaaa');
if(document.getElementById("dmat_date_copie")) document.getElementById('dmat_date_copie').value=convDate(document.getElementById('dmat_date_copie').value,'jj/mm/aaaa');
if(document.getElementById("dmat_date_crea")) document.getElementById('dmat_date_crea').value=convDate(document.getElementById('dmat_date_crea').value,'jj/mm/aaaa');
if(document.getElementById("dmat_date_mod")) document.getElementById('dmat_date_mod').value=convDate(document.getElementById('dmat_date_mod').value,'jj/mm/aaaa');


function calcDuree(){
	if(document.getElementById('calcduree')) document.getElementById('calcduree').value=secToTime(tcToSec(document.getElementById('dmat_tcout').value)-tcToSec(document.getElementById('dmat_tcin').value));
}

function addMat(input,id,lib) {
	//document.form1.ID_MAT.value=id;
	document.form1.dmat_id_mat_ori.value=id;
}
function createMat(){
	mat_groupe=document.form1.mat_groupe.value;
	if(mat_groupe=="") {
		alert('<?=addslashes(kMsgNumeroMatVide)?>');
		return;
	}
	if(confirm('<?=addslashes(kConfirmCreationMat)?>'+mat_groupe+' ?')){
		// Récupératin paramètres
		params="xmlhttp=1&commande=createMat&mat_groupe="+mat_groupe;
		if(document.form1.mat_format && document.form1.mat_format.value!="") params+="&mat_format="+encodeURIComponent(document.form1.mat_format.value);
		if(document.form1.mat_tcin && document.form1.mat_tcin.value!="") params+="&mat_tcin="+document.form1.mat_tcin.value;
		if(document.form1.mat_tcout && document.form1.mat_tcout.value!="") params+="&mat_tcout="+document.form1.mat_tcout.value;
		if(document.form1.mat_val_nat && document.form1.mat_val_nat.value!="") params+="&t_mat_val[0][ID_VAL]="+document.form1.mat_val_nat.value+"&t_mat_val[0][ID_TYPE_VAL]=NAT";
		if(document.form1.mat_val_lieu && document.form1.mat_val_lieu.value!="") params+="&t_mat_val[1][ID_VAL]="+document.form1.mat_val_lieu.value+"&t_mat_val[1][ID_TYPE_VAL]=LIEU";
		params+="&liste_id_doc=<?= $myDmat->t_doc_mat['ID_DOC'] ?>&liste_tcin=<?= $myDmat->t_doc_mat['DMAT_TCIN'] ?>&liste_tcout=<?= $myDmat->t_doc_mat['DMAT_TCOUT'] ?>";
		//Creation materiel
		return !sendData('GET','ajax.php',params,'rtnMat');
	}
}

function rtnMat(str){
	//alert(str);
	myXML=importXML(str);
	_xmlFlds=myXML.getElementsByTagName('ID_MAT');
	if(_xmlFlds){
		_xmlFld=_xmlFlds[0].firstChild;
		id=_xmlFld.nodeValue;
		
		_xmlFlds=myXML.getElementsByTagName('MAT_GROUPE');
		if(_xmlFlds){
			_xmlFld=_xmlFlds[0].firstChild;
			val=_xmlFld.nodeValue;
			if(document.getElementById("dmat_id_mat_ori")){
				var sel=document.getElementById("dmat_id_mat_ori");
				sel.options[sel.length] = new Option(id+"/"+val, id);
				sel.selectedIndex=sel.length-1;
			}
		}
	}
}


function back(){
	document.location.href='<?=hex_str($_GET['back'])?>';
}
<? if ($_GET['back']) { ?>
	document.form1.page.value=<?=hex_str($_GET['back'])?>;
	if(document.form1.bRetour) document.form1.bRetour.onclick=back;
<? } else {?>
	if(document.form1.bRetour) document.form1.bRetour.style.visibility="hidden";
<? } ?>
</script>
 
<?
//$myPage->renderReferrer();
?>

