<?
  if (empty($file)) { //Formulaire si aucun fichier trouvé, ni par scan, ni par upload
 ?>
<div id="pageTitre" class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	Import de fichier
</div>
<?
if((!isset($file) || !isset($xsl)) && php_sapi_name()!="cli"){
	print "<div class='error'>Merci de sélectionner un fichier.</div>";
}
?>
<form name="form1" id="form1" action="<?=$myPage->getName()?>?urlaction=importXML" method="post" enctype="multipart/form-data" >
  
  <!--input type="hidden" name="copie" value="doc_acc,mat,image" /-->
   
  <br/><br/>
<!-- Partie 1 : formulaire d'upload -->
	<fieldset>
	<legend><?=kImport?></legend>
	<table>
		<tr>
			<td class="label_champs_form"><?=kFichier?> :</td>
			<td><input name="importFichier" type="file" size="60" /></td>
		</tr>
		<tr>
			<td class="label_champs_form"><?= kType ?> :</td>
			<td>
			<select name="xsl">
				<option value="importDoc"><?=kImport." ".kDocument?></option>
				<option value="importDMat"><?=kImport." ".kSupport?></option>
				<option value="importMat"><?=kImport." ".kMateriel?></option>				
			</select>
			</td>
		</tr>
		<!-- si on des imports CSV, le contenu est automatiquement transfo en XML MAIS il manque l'encodage.
		La ligne suivante est donc nécessaire pour proposer le choix à l'utilisateur (ou l'imposer).
		-->
		<tr>
			<td class="label_champs_form"><?= kFormat ?> :</td>
			<td>
			<select name="encodage">
				<option value="ISO-8859-1"><?=kWindows?></option>
				<option value="MAC-ROMAN"><?=kMac?></option>
				<option value="UTF-8">UTF-8</option>
			</select>
			</td>
		</tr>
		<tr>
			<td class="label_champs_form"><label for="simul"><?= kTester ?></label> :</td>
			<td>
			<input type="checkbox" name="simul" id="simul" value="1">
			</td>
		</tr>
		<tr>
			<td class="label_champs_form"><label for="simul"><?= kMettreAJourExistant ?></label> :</td>
			<td>
				<input type="checkbox" name="maj_existant" id="maj_existant" value="1">
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input name="bOK" type="submit" class='BUTTON' value="<?=kValider?>" /></td>
		</tr>
	</table>
	</fieldset>
</form>

<?  } ?>




<!-- Partie 2 : debriefing post import -->	
	<?
	debug($debrief,'beige');
	if ($debrief) {
		
		if ($errormsg!='') echo "<div class='error'>".$errormsg."</div>";
		
		
		foreach ($debrief as $i=>$line) {
			if ($line['doc']) {
				if ($line['doc']['simul']) $str.="<strong>SIMULATION</strong><br/>";
				$str.="&nbsp;&nbsp;".kFichier." : ".$line['doc']['file']."<br/>";
				$str.="&nbsp;&nbsp;".kDocument." : ".$line['doc']['titre'];
				if ($line['doc']['simul']['doc']['new']) $str.=" ".kCree;
				elseif ($line['doc']['simul']['doc']['update']) $str.=" ".kMisAJour;				
				
				$str.="<br/>";
				/*special simulation*/
				if ($line['doc']['simul']['valeur']) {
					$str.="&nbsp;&nbsp;".kValeur." : ".(int)$line['doc']['simul']['valeur']['new']." ".kNouveau." / ".(int)$line['doc']['simul']['valeur']['update']." ".kMisAJour."<br/>";			
				}
				if ($line['doc']['simul']['personne']) {
					$str.="&nbsp;&nbsp;".kPersonne." : ".(int)$line['doc']['simul']['personne']['new']." ".kNouveau." / ".(int)$line['doc']['simul']['personne']['update']." ".kMisAJour."<br/>";			
				}			
				if ($line['doc']['simul']['lexique']) {
					$str.="&nbsp;&nbsp;".kLexique." : ".(int)$line['doc']['simul']['lexique']['new']." ".kNouveau." / ".(int)$line['doc']['simul']['lexique']['update']." ".kMisAJour."<br/>";			
				}
				if ($line['doc']['simul']['materiel']) {
					$str.="&nbsp;&nbsp;".kMateriel." : ".(int)$line['doc']['simul']['materiel']['new']." ".kNouveau." / ".(int)$line['doc']['simul']['materiel']['update']." ".kMisAJour."<br/>";			
				}					
				if ($line['doc']['simul']['image']) {
					$str.="&nbsp;&nbsp;".kImage." : ".(int)$line['doc']['simul']['image']['new']." ".kNouveau." / ".(int)$line['doc']['simul']['image']['update']." ".kMisAJour."<br/>";			
				}	
				if ($line['doc']['simul']['docacc']) {
					$str.="&nbsp;&nbsp;".kDocumentAcc." : ".(int)$line['doc']['simul']['docacc']['new']." ".kNouveau." / ".(int)$line['doc']['simul']['docacc']['update']." ".kMisAJour."<br/>";			
				}	
				if ($line['doc']['simul']['doc_version']) {
					$str.="&nbsp;&nbsp;".kLangues." : ".implode(', ', array_keys($line['doc']['simul']['doc_version']))."<br/>";			
				}	
				if ($line['doc']['doc_seq']) {
					$str.="&nbsp;&nbsp;".kSequences." : ".(int)$line['doc']['doc_seq']."<br/>";			
				}	
				
				if (!$line['doc']['simul']) { //pas simulation
			
					if ($line['doc']['doc_seq']) $str.="&nbsp;&nbsp;".$line['doc']['doc_seq']." ".kSequences."<br/>";
					if ($line['doc']['doc_mat']) $str.="&nbsp;&nbsp;".$line['doc']['doc_mat']." ".kMateriels."<br/>";				
					if ($line['doc']['doc_lex']) $str.="&nbsp;&nbsp;".$line['doc']['doc_lex']." ".kLexique."(s)<br/>";	
					if ($line['doc']['doc_val']) $str.="&nbsp;&nbsp;".$line['doc']['doc_val']." ".kValeur."(s)<br/>";	
					if ($line['doc']['doc_acc']) $str.="&nbsp;&nbsp;".$line['doc']['doc_acc']." ".kDocumentAcc."(s)<br/>";	
					if ($line['doc']['image']) $str.="&nbsp;&nbsp;".$line['doc']['image']." ".kImage."(s)<br/>";	

					$str.="&nbsp;&nbsp;".kResultats." : ".$line['doc']['error_msg']."<br/>";	
				}
			}
		
			if ($line['mat']) {
			
				if ($line['mat']['simul']) $str.="<strong>SIMULATION</strong><br/>";
				$str.="&nbsp;&nbsp;".kFichier." : ".$line['mat']['file']."<br/>";
				$str.="&nbsp;&nbsp;".kMateriel." : ".$line['mat']['id_mat'];
				if ($line['mat']['simul']['mat']['new']) $str.=" ".kCree;
				elseif ($line['mat']['simul']['mat']['update']) $str.=" ".kMisAJour;		
				
				$str.="<br/>";
				/*special simulation*/
				if ($line['mat']['simul']['valeur']) {
					$str.="&nbsp;&nbsp;".kValeur." : ".(int)$line['mat']['simul']['valeur']['new']." ".kNouveau." / ".(int)$line['mat']['simul']['valeur']['update']." ".kMisAJour."<br/>";			
				}
			
				if (!$line['mat']['simul']) {	
					$str.=$line['mat']['file']." : <br/>";
					$str.="&nbsp;&nbsp;".$line['mat']['id_mat']."<br/>";
					$str.="&nbsp;&nbsp;".$line['mat']['doc_mat']." ".kDocument."(s)<br/>";				
					$str.="&nbsp;&nbsp;".$line['mat']['mat_val']." ".kValeur."(s)<br/>";	
					$str.="&nbsp;&nbsp;".kResultats." : ".$line['mat']['error_msg']."<br/>";
				}
			}
			
			
			
			if ($line['dmat']) {
			
				if ($line['dmat']['simul']) $str.="<strong>SIMULATION</strong><br/>";
				$str.="&nbsp;&nbsp;".kFichier." : ".$line['dmat']['file']."<br/>";
				$str.="&nbsp;&nbsp;".kSupport." : ".$line['dmat']['id_doc_mat'];
				if ($line['dmat']['simul']['dmat']['new']) $str.=" ".kCree;
				elseif ($line['dmat']['simul']['dmat']['update']) $str.=" ".kMisAJour;			
			
				$str.="<br/>";
				/*special simulation*/
				if ($line['dmat']['simul']['valeur']) {
					$str.="&nbsp;&nbsp;".kValeur." : ".(int)$line['dmat']['simul']['valeur']['new']." ".kNouveau." / ".(int)$line['dmat']['simul']['valeur']['update']." ".kMisAJour."<br/>";			
				}
			
				if (!$line['dmat']['simul']) {	
					$str.=$line['dmat']['file']." : <br/>";
					$str.="&nbsp;&nbsp;".$line['dmat']['id_doc_mat']."<br/>";			
					$str.="&nbsp;&nbsp;".$line['dmat']['doc_mat_val']." ".kValeur."(s)<br/>";	
					$str.="&nbsp;&nbsp;".kResultats." : ".$line['dmat']['error_msg']."<br/>";
				}			
			
			}
			
			$str.="<hr width='100%'/>";	
		
		
		}
		if ($cntDoc>0) echo $str.$debrief[0]['doc']['file']." : ".$cntDoc." ".kDocuments."<br/><br/>"; 

		if ($cntMat>0) echo $str.$debrief[0]['mat']['file']." : ".$cntMat." ".kMateriels."<br/><br/>"; 		

		if ($cntDMat>0) echo $str.$debrief[0]['dmat']['file']." : ".$cntDMat." ".kSupports."<br/><br/>"; 	
	
	}
	
	//On a copié des média : doc_acc, video, image, etc
	if ($debrief['file_copy']) {
			echo "<br/><br/><hr width='100%'/>";
			echo "<strong>".kImportCopieFichier."</strong><br/>";
			if ($debrief['file_copy']['doc_acc']) echo $debrief['file_copy']['doc_acc']." ".kDocumentAcc."(s)<br/><br/>"; 
			if ($debrief['file_copy']['image']) echo $debrief['file_copy']['image']." ".kImage."(s)<br/><br/>"; 
			if ($debrief['file_copy']['mat']) echo $debrief['file_copy']['mat']." ".kMateriel."(s)<br/><br/>"; 
	}
	
?>
