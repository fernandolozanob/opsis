<?php
    require_once(libDir.'fonctionsGeneral.php');
	global $db;

	$arrFormats=array_merge(unserialize(gVideoTypes),unserialize(gImageTypes),unserialize(gAudioTypes),unserialize(gDocumentTypes));

	$layout= $_SESSION['USER']['layout_opts']['layout'];
    ?>
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/upload.js"></script>
<script type="text/javascript">

ext_allowed_regex = <? switch($_GET['media_type']){
	case 'p' :
		echo "/(\.|\/)(".implode('|',unserialize(gImageTypes)).")$/i";
        break;
	case 'v' :
		echo "/(\.|\/)(".implode('|',unserialize(gVideoTypes)).")$/i";
        break;
	case 'a' :
		echo "/(\.|\/)(".implode('|',unserialize(gAudioTypes)).")$/i";
        break;
	case 'd' :
		echo "/(\.|\/)(".implode('|',unserialize(gDocumentTypes)).")$/i";
        break;
	default :
		echo "/(\.|\/)(".implode('|',$arrFormats).")$/i";
        break ;
}?>;

ext_not_allowed='';
max_upload_files='1';
max_upload_size=10 // 10Mo
descr_mode=false;
pass_required=false;
email_required=false;

</script>
<style type="text/css">
#upload_form{
width : 650px;
<? if($_GET['type'] != 'batch_mat'){?>
	display : none
    <?}?>

}
#doc_form {width: 350px;}
</style>

<?php
	$id_proc_imp_video = 1;
	$id_proc_imp_image = 2;
	$id_proc_imp_document = 6;
	$id_proc_imp_audio = 5;
	// on différencie le processus document et pdf car si pdf pas encodage, on envoie le parametre a jobliste
	$id_proc_imp_pdf = 4;

    if (isset($_GET['type']) && ($_GET['type']=='batch_mat' || $_GET['type']=='doc_acc' || $_GET['type']=='doc_mat'))
    {
        include(modelDir.'model_doc.php');
        $dumbDoc=new Doc;
        //ob_start();
        include(libDir."class_formSaisie.php");
        $myForm=new FormSaisie();
        $myForm->saisieObj=$dumbDoc;
        $myForm->classLabel="miniField label_miniField";
        $myForm->classValue="miniField val_miniField";

		if (!isset($id))
			$id='';

        echo '<form action="indexPopupUpload.php?urlaction=finUpload&type='.$_GET['type'].$id.'"  method="post" name="form_envoi_fichiers" id="form_envoi_fichiers">';

        $myForm->beginTag("FIELDSET",array("ID"=>"upload_form"));
        $myForm->displayElt(array("NAME"=>"type_import","TYPE"=>"hidden","ID"=>"type_import","VALUE"=>"HTTP"));

		$myForm->displayElt(array("NAME"=>"MAT_TYPE","TYPE"=>"hidden","VALUE"=>"MASTER","DIVTAGS"=>"none"));
		if($_GET['type'] == 'batch_mat'){
			$myForm->displayElt(array("NAME"=>"redirectImportView","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));
		}
		$myForm->displayElt(array("NAME"=>"xsl","TYPE"=>"hidden","VALUE"=>"mappingDocPhoto","DIVTAGS"=>"both"));
		$myForm->displayElt(array("NAME"=>"lancement","TYPE"=>"hidden",'VALUE'=>'proc'));
        $myForm->displayElt(array("NAME"=>"id_proc","TYPE"=>"hidden",'VALUE'=>'*'));
        $myForm->displayElt(array("NAME"=>"proc_critere","TYPE"=>"hidden","VALUE"=>"{\"9\":\"&lt;MAT_FORMAT&gt;DCP-CPL&lt;/MAT_FORMAT&gt;\",\"1\":\"&lt;MAT_ID_MEDIA&gt;V&lt;/MAT_ID_MEDIA&gt;\",\"5\":\"&lt;MAT_ID_MEDIA&gt;A&lt;/MAT_ID_MEDIA&gt;\",\"3\":\"&lt;MAT_FORMAT&gt;PDF&lt;/MAT_FORMAT&gt;\",\"4\":\"&lt;MAT_ID_MEDIA&gt;D&lt;/MAT_ID_MEDIA&gt;\",\"2\":\"&lt;MAT_ID_MEDIA&gt;P&lt;/MAT_ID_MEDIA&gt;\"}"));

		if (!isset($_GET['rename']))
			$_GET['rename']='';

		if (!isset($_GET['type']))
			$_GET['type']='';

		if (!isset($_GET['mode']))
			$_GET['mode']='';

		if (!isset($_GET['id_doc']))
			$_GET['id_doc']='';

		$myForm->displayElt(array("NAME"=>"checkbox_rename","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));

        $myForm->displayElt(array("NAME"=>"rename","TYPE"=>"hidden","VALUE"=>$_GET['rename'],"DIVTAGS"=>"none"));
        $myForm->displayElt(array("NAME"=>"type","TYPE"=>"hidden","VALUE"=>$_GET['type'],"DIVTAGS"=>"none"));
        $myForm->displayElt(array("NAME"=>"mode","TYPE"=>"hidden","VALUE"=>$_GET['mode'],"DIVTAGS"=>"none"));
        $myForm->displayElt(array("NAME"=>"id_doc","TYPE"=>"hidden","VALUE"=>$_GET['id_doc'],"DIVTAGS"=>"none"));





        if($_GET['type'] == 'batch_mat'){
            $myForm->beginTag("FIELDSET",array("ID"=>"doc_form"));
			// Fonds par défaut => Opsomai (5 a priori )
            $myForm->displayElt(array("NAME"=>"DOC_ID_FONDS","TYPE"=>"hidden","VALUE"=>(defined("gFondsDefault")?gFondsDefault:5)));
            $myForm->displayElt(array("NAME"=>"makedoc","TYPE"=>"checkbox","CLASSVALUE"=>"miniField","ONCLICK"=>"toggleFields(this,document.getElementById('doc_form'));","CHECKBOX_LIB"=>kGenererDoc_long,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
			$myForm->displayElt(array("NAME"=>"doc_trad","CLASSVALUE"=>"miniField","TYPE"=>"checkbox",'CHECKBOX_LIB'=>kGenererAutresVersions_long,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
			$myForm->displayElt(array("NAME"=>"DOC_ACCES","CLASSVALUE"=>"miniField","TYPE"=>"checkbox",'CHECKBOX_LIB'=>kAcces_long,"CHECKBOX_OPTION"=>"1","VALUE"=>"1","DIVTAGS"=>"both"));
			$myForm->beginTag("DIV",array("CLASS"=>"miniField"));
				$myForm->displayElt(array("TYPE"=>"checkbox","DIVTAGS"=>"none", "ID"=>"addThemeCheckbox","CHECKED"=>"false"));//"onchange"=>"toggleTheme(this.checked)" // MS - ici il faut une fonction JS qui active / desactive le select suivant
				$myForm->displayElt(array("TYPE"=>"span","VALUE"=>kAjouterTheme_long));
				$myForm->displayElt(array("TYPE"=>"select","ID"=>"selectTheme","SELECT_SQL"=>"SELECT ID_CAT as ID, CAT_NOM as VAL from t_categorie where ID_LANG='FR' AND cat_id_type_cat='THM' ORDER BY VAL","NOALL"=>1)); // "name"=>"V_THM",
				// $myForm->displayElt(array("TYPE"=>"select","ID"=>"selectTheme","SELECT_SQL"=>"SELECT ID_VAL as ID, VALEUR as VAL from t_val where ID_LANG='FR' AND val_id_type_val='THM' ORDER BY VAL","NOALL"=>1)); // "name"=>"V_THM",
			$myForm->endTag("DIV");
            if(defined("gReportagePicturesActiv")){
                $myForm->beginTag("DIV",array("CLASS"=>"miniField"));
                    $myForm->displayElt(array("TYPE"=>"checkbox","DIVTAGS"=>"none","CHECKED"=>"false","ID"=>"addReportCheckbox","onchange"=>"toggleReportage(this.checked)")); // MS - ici il faut une fonction JS qui active / desactive le select suivant
                    $myForm->displayElt(array("TYPE"=>"span","VALUE"=>kAjouterReportage_long));
                    // $myForm->displayElt(array("TYPE"=>"select","SELECT_SQL"=>"SELECT ID_VAL as ID, VALEUR as VAL from t_val where ID_LANG='FR' AND val_id_type_val='THM'"));
                    //Reportage
                    if (isset($_GET['id_doc2link']) && intval($_GET['id_doc2link']) !== false)
                        $myForm->displayElt(array("NAME"=>"id_doc2link","TYPE"=>"hidden","VALUE"=>intval($_GET['id_doc2link']),"DIVTAGS"=>"none"));
                    else {
                        include(modelDir.'model_docReportage.php');
                        if (Reportage::isReportageEnabled()) {
                            $myForm->displayElt(array("NAME"=>"id_doc2link","ID"=>"selectReport","TYPE"=>"select","SELECT_SQL"=>"(SELECT 'new' as ID, '". kNouveau ."' as VAL ORDER BY VAL ASC)
                                                                                                                                    UNION ALL
                                                                                                                                (SELECT ''||ID_DOC AS ID,DOC_TITRE AS VAL
                                                                                                                                FROM t_type_doc t
                                                                                                                                INNER JOIN t_doc d ON d.doc_id_type_doc = t.id_type_doc AND d.ID_LANG='FR'
                                                                                                                                WHERE t.ID_LANG='FR' AND t.ID_TYPE_DOC = ".intval(gReportagePicturesActiv)."
                                                                                                                                ORDER BY VAL ASC)"
                                                                                                                                ,"DIVTAGS"=>"none",'NOALL'=>'1'
                                                                                                                                ,"onchange" => "toggleReportName(this.value)"));
                            $myForm->displayElt(array("NAME"=>"reportName",'CLASSLABEL'=>'labelNewReportName','ID'=>'newReportName',"TYPE"=>"text","PLACEHOLDER"=>kNomDuNouveauReportage,"VALUE"=>"","DIVTAGS"=>"none"));
                            // MS voir si on laisse placeholder (légere incompatibilité IE), ou si on remet une vraie valeur par défaut
                        }
                    }
                    $myForm->displayElt(array("NAME"=>"uploadToReportage","TYPE"=>"hidden","VALUE"=>1,"DIVTAGS"=>"none"));
                $myForm->endTag("DIV");
            }


			$myForm->endTag("FIELDSET");
        }




		if (isset($_GET['rtn']))
			$myForm->displayElt(array("NAME"=>"rtn","TYPE"=>"hidden","VALUE"=>$_GET['rtn'],"DIVTAGS"=>"none"));

        $myForm->endTag("FIELDSET");

        // paramètres upload
        $myForm->displayElt(array("NAME"=>"getInfos","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));

        ?>
<div id="tabs_upload" >
<ul>
<li>
<a href="#upload_http"><span><?=kUploadChoisirFichier?></span></a>
</li>
<li>
<a href="#upload_url"><span><?=kUploadSaisirUrl?></span></a>
</li><li>
<a href="#upload_ftp"><span><?=kUploadSaisirFTP?></span></a>
</li>
</ul>
<!-- IMPORT HTTP -->
<div id="upload_http" style="display : none ;">
<?php
    if (isset($_GET['id_cat']))
    $id='&id_cat='.$_GET['id_cat'];
    ?>
<div id="fileupload" >
<div class="fileupload-content empty">
<div class="infoupload"><span class="desktop_info"><?=kUploadDragDropInfoMsg;?></span><span class="responsive_info"><?=kUploadDragDropInfoMsgResponsive;?></span></div>
<table class="files"></table>
<div class="fileupload-progressbar"></div>
</div>
<form  action="upload.php" method="post" enctype="multipart/form-data">
<div class="fileupload-buttonbar">
<span class="global-infoprogress"><?=kImportEstimationTemps?><span class="ETA"></span></span>
<label class="fileinput-button">
<span><?=kAjouter?></span>
<input type="file" name="files[]" multiple />
</label>
<button type="submit" class="start"><?=kUploadStart?></button>
<button type="reset" class="cancel"><?=kAnnuler?></button>
<!--button type="button" class="delete"><?=kSupprimer?></button-->
</div>
</form>
</div>
</div>
<!-- IMPORT HTTP depuis URL  -->
<div id="upload_url" style="display : none ;">
<div class="fileupload-content empty">

<?php
    // champs de creation du document
    $myForm->displayElt(array("NAME"=>"getInfos","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));

	echo '<div class="url_files files">';
		echo '<p class="mainFields"><span class="fileupload-label">'.kAdresse.' : http://</span><input type="text" name="url_upload" id="url_upload" placeholder="'.kUploadUrlText.'" size="60" value="" /></p>';

		echo '<div class="supFields">
			<!-- Le champ nom de fichier original est important pr pouvoir updater les "name", fileName sert à récupéré le nom apres stringIteration pr éviter écrasement, fileRenameInput permet de préciser le nouveau nom du mat-->
			<label class="label_titre"><?=kTitre?> : </label>
			<input class="fileCustIdx" 	 type="hidden" name="fileCustIdx" value=""/>
			<input class="fileTimestamp" 	 type="hidden" name="fileTimestamp" value=""/>
			<input class="fileRenameInput" 	name="file_%N%_doc[DOC_TITRE]" value=""/>
			<input class="fileName"			name="file_%N%" type="hidden" value=""/>
			<input class="fileName_ori" 	name="file_%N%_original" type="hidden" value=""/>
		';
		//</div><div class="supFields">
		echo '<label><?=kTheme?> : </label>';
		// $myForm->displayElt(array("NAME"=>"file_%N%_doc[t_doc_val][][ID_VAL]","TYPE"=>"select","class"=>"fileTheme","SELECT_SQL"=>"SELECT ID_VAL as ID, VALEUR as VAL from t_val where ID_LANG='FR' AND val_id_type_val='THM' ORDER BY VAL",));
		// echo '<input class="file_typeVal" name="file_%N%_doc[t_doc_val][][ID_TYPE_VAL]" type="hidden" value="THM"/>
		$myForm->displayElt(array("NAME"=>"file_%N%_doc[t_doc_cat][][ID_CAT]","TYPE"=>"select","class"=>"fileTheme","SELECT_SQL"=>"SELECT ID_CAT as ID, CAT_NOM as VAL from t_categorie where ID_LANG='FR' AND cat_id_type_cat='THM' ORDER BY VAL",));
		echo '<input class="file_typeVal" name="file_%N%_doc[t_doc_cat][][ID_TYPE_CAT]" type="hidden" value="THM"/>
		</div>';
	echo '</div>';

	echo "</div>";
	echo '<div class="fileupload-buttonbar">';
    echo '<input type="button" style="display:block;margin:0px auto;" class="std_button_style" value="'.kUploadStart.'" onclick="envoyerFichierURL(\''.$_GET['type'].'\');" />';
	echo '</div>';
	?>

</div>
<!-- IMPORT FTP  -->
<div id="upload_ftp" style="display : none ;">
<div class="fileupload-content empty">

<?php
    // champs de creation du document
    $myForm->displayElt(array("NAME"=>"getInfos","TYPE"=>"hidden","VALUE"=>"1","DIVTAGS"=>"none"));

	echo '<div class="ftp_files files">';

		echo '<p class="mainFields"><span class="fileupload-label">'.kAdresse.' : ftp://</span><input type="text" name="ftp_upload" placeholder="'.kUploadFtpAddressText.'" id="ftp_upload" size="60"  /></p>';
		echo '<p class="mainFields"><span class="fileupload-label">'.kCompte.' : </span><input type="text" placeholder="'.kUploadFtpLoginText.'" name="login_ftp" id="login_ftp" size="60"  /></p>';
		echo '<p class="mainFields"><span class="fileupload-label">'.kMotDePasse.' : </span><input type="text" placeholder="'.kUploadFtpPasswordText.'" name="mdp_ftp" id="mdp_ftp" size="60"  /></p>';

		echo '<div class="supFields">
		<!-- Le champ nom de fichier original est important pr pouvoir updater les "name", fileName sert à récupéré le nom apres stringIteration pr éviter écrasement, fileRenameInput permet de préciser le nouveau nom du mat-->
		<label class="label_titre">'.kTitre.' : </label>
		<input class="fileCustIdx" 	 type="hidden" name="fileCustIdx" value=""/>
		<input class="fileTimestamp" 	 type="hidden" name="fileTimestamp" value=""/>
		<input class="fileRenameInput" 	name="file_%N%_doc[DOC_TITRE]" value=""/>
		<input class="fileName"			name="file_%N%" type="hidden" value=""/>
		<input class="fileName_ori" 	name="file_%N%_original" type="hidden" value=""/>
	'; // </div><div class="supFields">
	echo '
		<label>'.kTheme.' : </label>';
		$myForm->displayElt(array("NAME"=>"file_%N%_doc[t_doc_cat][][ID_CAT]","TYPE"=>"select","class"=>"fileTheme","SELECT_SQL"=>"SELECT ID_CAT as ID, CAT_NOM as VAL from t_categorie where ID_LANG='FR' AND cat_id_type_cat='THM' ORDER BY VAL"));
	echo '<input class="file_typeVal" name="file_%N%_doc[t_doc_cat][][ID_TYPE_CAT]" type="hidden" value="THM"/>
	</div>';
	echo '<div class="info_upload" >
			<span class="size" style="min-width:75px;"></span>';
	echo '</div>';
	echo '</div>';
	echo "</div>";
	echo '<div class="fileupload-buttonbar">';
	echo '<input type="button" style="display:block;margin:0px auto;" class="std_button_style" value="'.kUploadStart.'" onclick="envoyerFichierFTP(\''.$_GET['type'].'\');" />';
    echo '</div>';
	?>
</div>
</div>
</form>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload ">
    <td class="preview"><span class=""></span></td>
    <td class="name">
		<div class="nameFields">
			<!-- Le champ nom de fichier original est important pr pouvoir updater les "name", fileName sert à récupéré le nom apres stringIteration pr éviter écrasement, fileRenameInput permet de préciser le nouveau nom du mat-->
			<!--input class="fileRenameInput" 	name="file_%N%_rename" value="{%=file.name%}"/-->
			<label class="label_titre"><?=kTitre?> : </label>
			<input class="fileCustIdx" 	 type="hidden" name="fileCustIdx" value=""/>
			<input class="fileTimestamp" 	 type="hidden" name="fileTimestamp" value=""/>
			<input class="fileRenameInput" 	name="file_%N%_doc[DOC_TITRE]" value="{%=file.name%}"/>
			<input class="fileName"			name="file_%N%" type="hidden" value="{%=file.name%}"/>
			<input class="fileName_ori" 	name="file_%N%_original" type="hidden" value="{%=file.name%}"/>
		</div>
	</td>
	<td width="20%">
		<div class="supFields">
			<label><?=kTheme?> : </label>
			<?	$myForm->displayElt(array("NAME"=>"file_%N%_doc[t_doc_cat][][ID_CAT]","TYPE"=>"select","class"=>"fileTheme","SELECT_SQL"=>"SELECT ID_CAT as ID, CAT_NOM as VAL from t_categorie where ID_LANG='FR' AND cat_id_type_cat='THM' ORDER BY VAL"));?>
			<input class="file_typeVal" name="file_%N%_doc[t_doc_cat][][ID_TYPE_CAT]" type="hidden" value="THM"/>
		</div>
	</td>
	 <td class="start" style="display:none!important;">{% if (!o.options.autoUpload) { %}
            <button class="btn btn-primary">
            <i class="icon-upload icon-white"></i>
            <span>Start</span>
            </button>
            {% } %}</td>

    {% if (file.error) { %}
		<td class="error" ><span class="label label-important"><?=kErreur?>:</span> {%=translateErr(file.error)%}
			<div class="cancel">
				<button class="ui-button-icon-only" style="height:27px;width:29px;vertical-align:middle;"></button>
			</div>
		</td>
        {% } else if (o.files.valid && !i) { %}
        <td width="30%" >
			<div class="info_upload" >
				<span class="size" style="min-width:75px;">{%=o.formatFileSize(file.size)%}</span>
				<div class="barTools valign_helper">
					<div class="infoprogress"></div><!-- ici mettre % age de progres et entre parenthèse le débit , ex: 52% (2.3Mb/s) -->
					<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
					<div class="cancel">{% if (!i) { %}
						<button class="ui-button-icon-only" style="height:27px;width:29px;vertical-align:middle;">
						</button>
					{% } %}</div>
				</div>
			</div>
		</td>
        {% } else { %}
        <td colspan="3"></td>
        {% } %}

    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    {% if (file.error) { %}
        <td></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td class="error" colspan="2"><span class="label label-important"><?=kErreur?></span> {%=file.error%}</td>
        {% } else { %}
        <td class="preview">{% if (file.thumbnail_url) { %}
            <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
        <td class="name">
        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
        </td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        <td colspan="2"></td>
        {% } %}
    <td class="delete">
    <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="icon-trash icon-white"></i>
    <span>Delete</span>
    </button>
    <input type="checkbox" name="delete" value="1">
    </td>
    </tr>
    {% } %}
</script>
<!-- 					<div class="fileupload-progress" ><div class="progress-extended"></div></div>
 -->
<script type="text/javascript">
var file_upload_num=1;
var file_upload_cnt = 0 ;

// code compatibilité objet Date pour IE < IE9
if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}
alertStopUpload = function (e){
	return "<?=kUploadQuitAlert?>";
}
<? $maxChunkSize = (defined('kUploadJSMaxChunkSize') && is_int(intval(kUploadJSMaxChunkSize)) && intval(kUploadJSMaxChunkSize)) ?intval(kUploadJSMaxChunkSize):500000000; // 500MB ?>

 requirejs([
	'jquery.fileupload/jquery.fileupload',
	'jquery.fileupload/jquery.fileupload-fp',
	'jquery.fileupload/jquery.fileupload-ui',
	'jquery.fileupload/jquery.fileupload-jui',
	'jquery.fileupload/jquery.iframe-transport'
	],function(){
	$j('#fileupload').fileupload
	(
	 {
	 url:'empty.php?urlaction=jqueryUploadHandler&upload=1&upload_type=<?=$_GET['type']?>',
	 acceptFileTypes : ext_allowed_regex,
	 maxFileSize:<?php echo calcMaxUploadFileSize(ini_get('upload_max_filesize'))?>, // 107G en dev
	 autoUpload : false,
	 downloadTemplateId : null,
	 maxChunkSize: <?=$maxChunkSize?>
	 }
	 );

	$j('#fileupload').on
	(
	'fileuploadadd'  ,
	function(event,data){

		if(typeof data.files != 'undefined'){
			for(i=0; i < data.files.length;i++){
				data.files[i].ops_cust_idx = file_upload_cnt;
				file_upload_cnt ++ ;
			}

		}
		$j("div.fileupload-content.empty").removeClass("empty");
	}
	);
	 $j('#fileupload').on
	(
	'fileuploadadded'  ,
		function(event,data){
			if(typeof $j("#selectTheme").attr("disabled") == 'undefined'){
				$j("table.files tr:last-child select").each(function(idx,elt){
					for( i=0; i < formInherits.length ; i++){
						if($j(elt).is(formInherits[i].slave_selector)){
							$j(elt).val($j(formInherits[i].master_selector).val());
						}
					}
				});
			}
			$j("table.files tr:last-child input").each(function(idx,elt){
				if($j(elt).attr('name') == 'fileCustIdx' && data.files.length == 1 ){
					$j(elt).val(data.files[0].ops_cust_idx);
					$j(elt).next('.fileTimestamp').val(Date.now());
				}
			});
		});

	$j('#fileupload').bind
	(
	'fileuploadstart'  ,
	function(event,data){
		// console.log("fileuploadstart");
		$j(window).bind("beforeunload",alertStopUpload);
		$j("#form_envoi_fichiers").addClass("upload_started");
		avg_bitrate = null ;
	}
	);

	$j('#fileupload').bind
	(
	'fileuploadprogress'  ,
	function(event,data){
		if (data.context) {
			cust_idx = data.files[0].ops_cust_idx;
			current_row = $j("table.files tr").has("input.fileCustIdx[value='"+cust_idx+"']");
			percent = $j.blueimp.fileupload.prototype._formatPercentage(data.loaded / data.total);

			info = percent+" ";
			if(data.bitrate){
				bitrate = $j.blueimp.fileupload.prototype._formatBitrate(data.bitrate);
				info += "("+bitrate+")";
			}
			current_row.find(".infoprogress").html(info);
		}

	}
	);

	var avg_bitrate = null ;
	$j('#fileupload').bind
	(
	'fileuploadprogressall'  ,
	function(event,data){
		// MS - 07.12.15 - calcul estimation du temps de transfert pr l'upload
		// on utilise une moyenne mobile exponentielle (ou exponential moving average) comme conseillé sur SO
		if(avg_bitrate == null){
			avg_bitrate = data.bitrate;
		}else{
			avg_bitrate = 0.01  * data.bitrate + (1-0.01) * avg_bitrate ;
		}
		estimated_time = $j.blueimp.fileupload.prototype._formatTime((data.total - data.loaded) * 8 / avg_bitrate) ;
		$j("#fileupload").find(".global-infoprogress .ETA").html(estimated_time);
	}
	);

	$j('#fileupload').bind
	(
	'fileuploadfailed'  ,
	function(event,data){
		if($j("div.fileupload-content table tbody").children().length == 0 ){
			$j("div.fileupload-content").addClass("empty");
			$j("#form_envoi_fichiers").removeClass("upload_started");
		}
	}
	);

	$j('#fileupload').on
	(
	 'fileuploaddone',
	 function (event, data)
	 {
		var json=data.result;

		cust_idx = data.files[0].ops_cust_idx;
		current_row = $j("table.files tr").has("input.fileCustIdx[value='"+cust_idx+"']");

		current_row.find("select[name*='%N%'],input[name*='%N%']").each(function(idx,elt){
			$j(elt).attr('name',$j(elt).attr('name').replace('%N%',file_upload_num));
		});
		current_row.find("input.fileName").eq(0).attr('value',json[0]["name"]);
		current_row.find(".barTools div.cancel").addClass('done');

		if (file_upload_num>=$j("#fileupload table.files tr").size()){
			$j(".global-infoprogress").html('<?=kImportTransfertTermineMsg?>');
			$j(window).unbind("beforeunload",alertStopUpload);
			$j("#form_envoi_fichiers").submit();
		}

		 file_upload_num++;
	 });

	$j('#fileupload').on
	(
	 'fileuploadstop',
	 function (event, data)
	 {
		if (file_upload_num>=$j("#fileupload table.files tr").size() && $j("#fileupload table.files tr").size() >0){
			$j(window).unbind("beforeunload",alertStopUpload);
			$j("#form_envoi_fichiers").submit();
		}else if ($j("#fileupload table.files tr").size() == 0 ){
			$j(window).unbind("beforeunload",alertStopUpload);
		}
	 }
	 )

    $j('#upload_form > .miniField > input[name=makedoc]').on('click',function(event,data){toggleFields(this,document.getElementById('doc_form'))});


	$j('#tabs_upload').tabs({
		activate : function(event,ui){
			$j("#tabs_upload").find('input , select').prop('disabled',true);
			if (typeof ui.newTab != 'undefined'){
				$j(ui.newPanel).find('input,select').prop('disabled',false);
			}
		}
	});

	// On neutralise l'ensemble des inputs / selects du formulaire d'upload
	$j("#tabs_upload").find('input , select').prop('disabled',true);
	// On réactive les inputs / selects du tab actuellement actif.
	$j($j("#tabs_upload > ul > li.ui-tabs-active > a").attr("href")).find('input,select').prop('disabled',false);

	createSelectInheritance("select#selectTheme","select.fileTheme");
	createCheckboxActivation("input#addThemeCheckbox","select#selectTheme");
	$j("select,input[type='checkbox']").trigger("change");


});



function translateErr(str_err){
    switch(str_err){
        case 'Filetype not allowed':
            return "<?=kErreurUploadInterdit?>";
            break;
        case 'File is too big' :
            return "<?=kFichierTropGros?>";
            break;
        case 'File is too small' :
            return "<?=kFichierTropPetit?>";
            break;
        case 'Maximum number of files exceeded' :
            return "<?=kNombreMaxFichierAtteint?>";
            break;
        case 'Uploaded bytes exceed file size' :
            return "<?=kTropOctetsEnvoyes?>";
            break;
        case 'Uploaded bytes exceed file size' :
            return "<?=kTropOctetsEnvoyes?>";
            break;

    }
}

</script>

<?php
    }
    ?>