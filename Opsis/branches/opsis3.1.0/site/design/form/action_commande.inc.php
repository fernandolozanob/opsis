<div class="title_bar"><?=kTelecharger?><div id="close_export_menu" title="<?=kFermer?>" onclick="hideMenuActions()" style="float:right;"></div></div>
<?php
require_once(libDir."fonctionsGeneral.php");
$myUsr=User::getInstance();
// MS - note sur les commandes et les controles de la disponibilité des documents pour un utilisateur : 
// Etant donné que ce code de téléchargement direct a été développé pour la démo qui laisse globalement un accès assez libre aux divers docs & fonctionnalités, 
// il n'y a pour l'instant pas de réel moyen de controle qu'un utilisateur a le bien le droit de télécharger un document qu'il aurait coché dans un résultat de recherche 
// Ces controles peuvent d'ailleurs varier en nature en fonction du client.
// Il faudrait surement qu'un controle systématique soit fait au moment de la création de la commande, ainsi qu'au moment même de l'envoi en validation / traitement de la commande. 
// Les critères de ce controle pourraient etre paramétrés dynamiquement (au pire comme la fonction AppliqueDroitsCustom pour la recherche, au mieux avec une classe de configuration et de tests ) 


if(isset($_GET['type']) && !empty($_GET['type']) ){
	if(isset($_GET['ids']) && !empty($_GET['ids'])){
		$multi_download = explode('$',$_GET['ids']);
	}else if(isset($_GET['id']) && !empty($_GET['id'])){
		$solo_download = $_GET['id'];
	}
	$type = $_GET['type'] ;


	
	switch($type){
		case 'doc' :
		case "doc_reportage" :
		case "panierdoc" :
		default :
			if(isset($solo_download) && $solo_download){
				require_once(modelDir.'model_doc.php');
				$myDoc = new Doc() ; 
				$myDoc->t_doc['ID_DOC'] = $solo_download;
				$myDoc->t_doc['ID_LANG'] = $_SESSION['langue'];
				$myDoc->getDoc() ; 
				$myDoc->getMats() ; 
			
				$arr_opts = array() ; 
				if(!empty($myDoc->t_doc_mat)){
					foreach($myDoc->t_doc_mat as $doc_mat){
						if($doc_mat['MAT']->t_mat['MAT_TYPE'] == 'MASTER' && $doc_mat['MAT']->hasfile){
							$label_format['MASTER'] = "(".$doc_mat['MAT']->t_mat['MAT_FORMAT'].")";
						}else if($doc_mat['MAT']->t_mat['MAT_TYPE'] == 'VISIO' && $doc_mat['MAT']->hasfile){
							$label_format['VISIO'] = "(".$doc_mat['MAT']->t_mat['MAT_FORMAT'].")";
						}
					}
				}
				
				$page = "null" ; 
				if(!isset($label_format) || empty($label_format)){
					echo '<div class="error_msg" style="text-align : center ; margin : 10px 20px;">'.kAucunMateriel.'</div>';
					exit ; 
				}
			}else{
				if($type=='doc' || $type=='doc_reportage'){
					$nbLignesTotal = (isset($_SESSION['recherche_DOC_Solr']['rows'])?$_SESSION['recherche_DOC_Solr']['rows']:(isset($_SESSION['recherche_DOC']['rows'])?$_SESSION['recherche_DOC']['rows']:''));
				}else if ($type=="panierdoc" && isset($_GET['nbLignesTotal'])){
					$nbLignesTotal = $_GET['nbLignesTotal'];
				}
				$nbLignes=   (isset($_SESSION['recherche_DOC_Solr']['nbLignes'])?$_SESSION['recherche_DOC_Solr']['nbLignes']:(isset($_SESSION['recherche_DOC']['nbLignes'])?$_SESSION['recherche_DOC']['nbLignes']:gNbLignesDocListeDefaut));
				if(isset($nbLignes) && isset($nbLignesTotal) && !empty($nbLignesTotal)){
					$nbLignes = min($nbLignes,$nbLignesTotal);
				}
				$page=   (isset($_SESSION['recherche_DOC_Solr']['page'])?$_SESSION['recherche_DOC_Solr']['page']:(isset($_SESSION['recherche_DOC']['page'])?$_SESSION['recherche_DOC']['page']:1));
			}
			
		break ; 
	}
	
?>
	<form id="small_order_form" >	
		<div id="small_adv_order">
			<input disabled type="hidden" name="pan_id_type_commande" value="2"/>
			<? if (isset($solo_download) && $solo_download){ ?>
				<input disabled type="hidden" name="t_panier_doc[][ID_DOC]" value="<?=$solo_download;?>"/>
			<?}?>
			<? if ($myUsr->loggedAsGuest()){ ?>
				<input disabled type="hidden" id="partage_id" name="partage_id" value="<?=$solo_download;?>"/>
			<? } ?>
			<input id="selection_ids" name="selection_ids" type="hidden"/>
		</div>
		<div class="select_order">
			<?if(!isset($solo_download) || empty($solo_download)){?>
				<label for="select_cible"><?=kQuickOrderItemsToDownload?></label>
				<select id="select_cible" name="select_cible" onchange="checkActivateIds()">
					<option class="selection" value="selection" disabled="true"><?=kSelection?></option>
					<option class="page" data-nb-items="<?=$nbLignes?>" value="page"><?=kPageCourante?> (<?=$nbLignes?>)</option>
					<option class="all" data-nb-items="<? if(isset($nbLignesTotal) && !empty($nbLignesTotal)){echo ' ('.$nbLignesTotal.')';}?>" value="all"><?=kComplet?><? if(isset($nbLignesTotal) && !empty($nbLignesTotal)){echo ' ('.$nbLignesTotal.')';}?></option>
				</select><br />
			<?}?>
			<label for="select_format"><?=kSelectionFormat?></label>
			<select id="select_format" name="select_format">
					<?if(isset($solo_download)){?>
						<? if(isset($label_format['MASTER'])){?>
						<option class="quick_download" data-mat-type="MASTER" value="100"><?= kMasterFile.' '.$label_format['MASTER']?></option>
						<?}
						if(isset($label_format['VISIO'])){?>
						<option class="quick_download" data-mat-type="VISIO" value="160"><?= kVisioFile.' '.$label_format['VISIO']?></option>
						<?}?>
						<option class="redirect_download"><?=kAutresFormats?></option>
					<? }else{ ?>
						<option class="quick_download" data-mat-type="MASTER" value="100"><?= kMasterFile ?></option>
						<option class="quick_download" data-mat-type="VISIO" value="160"><?= kVisioFile ?></option>
						<option class="redirect_download"><?=kAutresFormats?></option>
					<?}?>
			</select>
		</div>
		<div class="return_order hidden">
			<div class="status"></div>
			<div class="refCommande"></div>
		</div>
		<div class="buttons_order">
			<input type="button" id="start_dl_order" class="std_button_style" value="<?=kTelecharger?>" onclick="startDownload($j('.frame_menu_actions #small_order_form').eq(0))"/>
			<input type="button" id="go_to_order" class="std_button_style hidden" value="<?=kCommande?>" />
			<input type="button" class="std_button_style" value="<?=kFermer?>" onclick="closePopup()"/>
		</div>
	
	</form>
	<script>
	current_page = <?=$page?>;
	
	function startDownload(fromForm){
		// formulaire de commande
		form = $j(fromForm);
		
		if(form.length == 0){
			console.log("function startDownload - Erreur définition du formulaire de commande ",form);
			return false ; 
		}
		
		// count nb de docs de la future commande
		if($j(form).find(' #select_cible').length > 0){
			nb_docs = $j(form).find("#select_cible option:selected").attr('data-nb-items');
		}else{
			nb_docs = 1 ; 
		}
	
		$j(form).find(" input").prop('disabled',false);
		
		// récupération des données du formulaire
		formData = $j(form).serialize() ; 
		// préparation des paramètres supplémentaires
		data_init_cmd = {};
		data_init_cmd['commande']="INIT";
		if($j(form).find(' #select_cible').length > 0){
			// cas export par lot avec une sélection renseignée 
			if($j(form).find(' #select_cible').val() == 'selection' && $j(form).find(" #selection_ids").val() != ''){
				ids = $j(form).find(" #selection_ids").val().split(',');
				for ( i=0; i < ids.length ; i++){
					data_init_cmd["t_panier_doc["+ i +"][ID_LIGNE_PANIER]"]="new" ; 
					data_init_cmd["t_panier_doc["+ i +"][ID_DOC]"]=ids[i] ; 
					if( $j(form).find(" #select_format").find('option:selected').hasClass('quick_download')){	
						data_init_cmd["t_panier_doc["+ i +"][PDOC_SUPPORT_LIV]"]=$j(form).find(" #select_format").val();
					}
				}
			}else if($j(form).find(' #select_cible').val() == 'page' ){
				data_init_cmd["pdoc_from_sql[page]"] = current_page;
				data_init_cmd["pdoc_from_sql[nb]"] = $j(form).find(' #select_cible option:selected').attr('data-nb-items');
				if( $j(form).find(" #select_format").find('option:selected').hasClass('quick_download')){
					data_init_cmd["pdoc_from_sql[default_pdoc_prms][PDOC_SUPPORT_LIV]"] = $j(form).find(" #select_format").val();
				}
			}else if($j(form).find(' #select_cible').val() == 'all' ){
				data_init_cmd["pdoc_from_sql[page]"] = current_page;
				data_init_cmd["pdoc_from_sql[nb]"] = $j(form).find(' #select_cible option:selected').val();
				if( $j(form).find(" #select_format").find('option:selected').hasClass('quick_download')){
					data_init_cmd["pdoc_from_sql[default_pdoc_prms][PDOC_SUPPORT_LIV]"] = $j(form).find(" #select_format").val();
				}
			}
		}else{
			data_init_cmd["t_panier_doc[][PDOC_SUPPORT_LIV]"]=$j(form).find(" #select_format").val();
		}
		// ajout flag récupération de l'estimation de la taille du panier. 
		data_init_cmd['getSize'] = 1 ; 
		formData+= "&"+JSObj2QueryString(data_init_cmd) ; 
		$j(form).addClass('loading');
		$j(form).find(" #start_dl_order").addClass('hidden');
		$j(form).parents("#popupModal").addClass('progresscursor');
		
		
		//Envoi de la première requête ajax pour création d'un panier de commande avec alimentation des t_panier_doc en fonction du choix de l'utilisateur 
		$j.ajax({
			url : 'empty.php?urlaction=commande&xslFile=copyXML_t_panier&where=include&dontUpdateSessStyle=true', 
			method: 'POST',
			data : formData,
			success : function(data){
				try{
					if(typeof ow_const != 'undefined' && typeof ow_const.gQuickOrderDownloadSeuilNbItems != 'undefined'){
						seuil_nb_items = ow_const.gQuickOrderDownloadSeuilNbItems; 
					}else{
						seuil_nb_items = 10 ;  // deft value 10 items
					}
					if(typeof ow_const != 'undefined' && typeof ow_const.gQuickOrderDownloadSeuilFilesize != 'undefined'){
						seuil_taille_panier = ow_const.gQuickOrderDownloadSeuilFilesize; 
					}else{
						seuil_taille_panier = 10000000000 ;  // deft value 10Go
					}
					xml = $j.parseXML(data);
					//Récupération des infos du panier de commande nouvellement créé
					id_panier = $j(xml).find('select t_panier ID_PANIER').get(0).textContent;
					if(nb_docs  <= seuil_nb_items && $j(form).find(" #select_format").find('option:selected').hasClass('quick_download')){
						mat_type = $j(form).find(" #select_format").find('option:selected').attr('data-mat-type');
						if(mat_type  == 'MASTER'){
							estimate_size = $j(xml).find('select t_panier ESTIMATE_SIZE_MASTER').get(0).textContent;
						}else if(mat_type == 'VISIO' ){
							estimate_size = $j(xml).find('select t_panier ESTIMATE_SIZE_VISIO').get(0).textContent;
						}
						test_size = (parseInt(estimate_size,10) <= parseInt(seuil_taille_panier,10));
					}else{
						estimate_size = 0 ; 
						test_size = true ; 
					}
					
					// SI le nombre d'items à télécharger est inférieur au seuil défini (gQuickOrderDownloadSeuilNbItems), 
					// et que la somme des tailles des éléments du panier est inférieure au seuil défini (gQuickOrderDownloadSeuilFilesize), 
					// et que le choix de format est un format compatible avec la version rapide de la commande, 
					// ALORS on passe dans le cas d'une execution synchrone des traitements par le frontal et lancemment automatique du téléchargement 
					if(parseInt(nb_docs,10)  <= parseInt(seuil_nb_items,10) && $j(form).find(" #select_format").find('option:selected').hasClass('quick_download') && test_size){
						// console.log("cas quickdownload : nb_docs ("+nb_docs+") < 10 ET estimate_size ("+estimate_size+") < "+ow_const.gQuickOrderDownloadSeuilFilesize);
						data_runSync_ajax = {};
						data_runSync_ajax['id_panier']=id_panier;
						data_runSync_ajax['commande']="LIVRAISON_JOB";
						data_runSync_ajax['runSyncJob']="1";
						data_runSync_ajax['crit_mat[FIELD]']="MAT_TYPE";
						data_runSync_ajax['crit_mat[VAL]']=$j(form).find(" #select_format").find('option:selected').attr('data-mat-type');
						data_runSync_ajax['ID_PROC']=$j(form).find("#select_format").val();
						if($j(form).find(" input#partage_id").length > 0)
							data_runSync_ajax['partage_id']=$j(form).find(" input#partage_id").val();

						$j(form).addClass('loading');
						$j(form).find(" #start_dl_order").addClass('hidden');
						
						if(nb_docs == 1 ){
							xsl_direct_dl = "copyXML";
						}else{
							xsl_direct_dl = "copyXML_t_panier";
						}
						// Appel ajax de lancement des traitements 
						$j.ajax({
							url : 'empty.php?urlaction=commande&xslFile='+xsl_direct_dl+'&where=include&dontUpdateSessStyle=true', 
							method: 'POST',
							data : data_runSync_ajax,
							success : function(data){
								try{
									// récupération des données necessaire : id_panier, id_job, repertoire & fichier de sortie
									xml = $j.parseXML(data);
									id_panier = $j(xml).find('select t_panier ID_PANIER').get(0).textContent;
									id_job_commande = $j(xml).find('select t_panier PAN_XML PANXML_ID_JOB').get(0).textContent;
									if(nb_docs == 1 ){
										dl_final_path = $j(xml).find('select t_panier PAN_XML PANXML_FOLDER_OUT').get(0).textContent.match(/\/P[0-9]+_[0-9]+\//);
										dl_final_path+= $j(xml).find('select t_panier_doc LIGNE_JOB_OUT').get(0).textContent ; 
									}else{
										dl_final_path = $j(xml).find('select t_panier PAN_XML PANXML_FOLDER_OUT').get(0).textContent.match(/\/P[0-9]+_[0-9]+\//);
									}
									
									// modification affichage popin
									$j(form).find(" #go_to_order").removeClass('hidden');
									$j(form).find(" #go_to_order").click(function(){
										window.location.href = "index.php?urlaction=commande&id_panier="+id_panier;
									});
									$j(form).find(" .return_order").removeClass('hidden');
									$j(form).find(" .select_order").addClass('hidden');
									
									$j(form).find(" .return_order .status").html('<div class="traitement">'+str_lang.kQuickOrderMsgTraitement+'</div><div id="etat'+id_job_commande+'"><div id="progressbar" ><span>0%</span></div></div>');
									$j(form).find(" .return_order .refCommande").html(str_lang.kQuickOrderMsgRefCommande.replace('{id_panier}','<b>'+id_panier+'</b>'));
									$j( "#small_order_form .return_order #etat"+id_job_commande+" div[id^='progressbar']").progressbar({
										value:0
									});
									
									// lancement des rafraichissement de la progression
									// + preparation téléchargement automatique quand les traitements seront terminés
									$j(form).removeClass('loading').addClass('processing');
									
									// Mise à jour du lien "dernière commande" dans le panGauche 
									panListeRefToLastOrder = id_panier ; 
									updateLayoutFlags();
									refreshFolders() ; 
									if($j("#panBlock .panListe .last_order.selected").length > 0 ){
										flag_reload_current_pan = true ; 
										loadPanier(panListeRefToLastOrder,null,null,null,'commande') ; 
									}
									
									//Lancement de la boucle updateProgressJob & préparation du lancement du téléchargement zip de la commande lorsque le job panier (global) est traité
									updateProgressJob(id_job_commande,null,function(){
										if($j(form).is(':visible')){
											// Si le popup est toujours visible, alors on lance le téléchargement de la commande.
											window.location.href = 'download.php?livraison='+dl_final_path;
											$j(form).removeClass('processing loading');
											$j(form).parents("#popupModal").removeClass('progresscursor');
											setTimeout(function(){
												$j("#etat"+id_job_commande).remove();
												$j(form).find(" .return_order .status .traitement").html(str_lang.kQuickOrderMsgDownload);
											},500);
											refreshFolders() ; 
											if($j("#panBlock .panListe .last_order.selected").length > 0 ){
												flag_reload_current_pan = true ; 
												loadPanier(panListeRefToLastOrder,null,null,null,'commande') ; 
											}
										}
									},true);
									
									// call ajax pour effectivement lancer les traitements synchrones
									$j.ajax({
										url  : 'empty.php?urlaction=processJob&id_job='+id_job_commande+"&action=execJobSync"
									});
								}catch(e){
									console.log("crash retour commande recup id_panier");
									$j(form).find(" .return_order").removeClass('hidden').addClass('error');
									$j(form).removeClass('loading processing');
									$j(form).parents("#popupModal").removeClass('progresscursor processing');
									$j(form).find(" .select_order").addClass('hidden');
									$j(form).find(" .return_order .status").html(str_lang.kQuickOrderMsgError);
									console.log(e);
								}
							}
						});
					
					
					
					}else if($j("#panBlock #mainPan #panFrame").length > 0 ){
						// console.log("cas non quickdownload avec ouverture du panblock ");
						$j.ajax({
							url : "empty.php?where=include&urlaction=commande&id_panier="+id_panier+"",
							success : function(data){
								if($j(form).is(':visible')){
									// Si le popup est toujours visible, alors on switch sur l'affichage commande dans le block panier dépliable
									$j("#panBlock .title_bar #panTitle").html(str_lang.kQuickOrderRefToLastCommande);
									$j("#panBlock .title_bar #panNbElems").html("("+nb_docs+")");
									$j("#panFrame").html(data);
									togglePanBlock('full');
								}
								
								// Affichage du lien "dernière commande" dans le panGauche
								// on pourrait utiliser refreshFolders + loadpanier mais il me semble que cela devrait être un peu plus rapide comme ça. 
								$j("#panBlock #panGauche ul.panListe li.selected").removeClass('selected');
								last_order_menu_item = '<li id="panier'+id_panier+'" class="last_order selected" onclick="loadPanier('+id_panier+',this,null,null,\'commande\');">'+str_lang.kQuickOrderRefToLastCommande+'</li>';
								panListeRefToLastOrder = id_panier ; 
								id_current_panier= id_panier  ; 
								updateLayoutFlags();
								
								if($j("#panBlock #panGauche ul.panListe li.last_order").length>0){
									$j("#panBlock #panGauche ul.panListe li.last_order").replaceWith(last_order_menu_item);
								}else{
									$j("#panBlock #panGauche ul.panListe").prepend(last_order_menu_item);
								}
								
								// on cache le popup
								$j(form).removeClass('loading');
								$j(form).parents("#popupModal").removeClass('progresscursor');
								try{
									event = event || window.event;
									event.stopPropagation();
								}catch(e){
									console.log("startDownload failed to stopPropagation",e);
								}
								closePopup();
							}
						});
					}else{
						$j(form).find(" #go_to_order").removeClass('hidden');
						$j(form).find(" #go_to_order").click(function(){
							window.location.href = "index.php?urlaction=commande&id_panier="+id_panier;
						});
						$j(form).find(" .return_order").removeClass('hidden');
						$j(form).find(" .select_order").addClass('hidden');
						$j(form).find(" .return_order .status").html(str_lang.kQuickOrderMsgTranscodage);
						$j(form).find(" .return_order .refCommande").html(str_lang.kQuickOrderMsgRefCommande.replace('{id_panier}',id_panier));
						$j(form).removeClass('loading');
						$j(form).parents("#popupModal").removeClass('progresscursor');
					}	
				}catch(e){
					console.log("crash retour commande recup id_panier",e);
					$j(form).find(" .return_order").removeClass('hidden').addClass('error');
					$j(form).removeClass('loading processing');
					$j(form).parents("#popupModal").removeClass('progresscursor processing');
					$j(form).find(" .select_order").addClass('hidden');
					$j(form).find(" .return_order .status").html(str_lang.kQuickOrderMsgError);
				}
			}
		});
	}
	
	/*$j(document).ready(function(){
		refreshSelectedItems('#small_order_form','doc');
	});*/
		
	/*function refreshSelectedItems(formSelector,type_entite){
		str_selection = str_lang.kSelection;
		nb_selection = 0 ; 
		list_ids = "";
		query_str_checkbox = "";
		if(type_entite == "doc"){
			query_str_checkbox = "#resultats .resultsCorps > input[type='checkbox'],#resultats .resultsMos  .mos_checkbox input[type='checkbox']";
		}else if (type_entite == "panierdoc"){
			query_str_checkbox = "#panFrame #formPanier .resultsCorps > input[type='checkbox'],#panFrame #formPanier .resultsMos  .mos_checkbox input[type='checkbox']";
		}
		$j(query_str_checkbox).each(function(idx,elt){
			if($j(elt).get(0).checked){
				if(list_ids != ''){
					list_ids+=",";
				}
				list_ids += $j(elt).val();
				nb_selection++;
			}
		});

		
		if(nb_selection>0){
			$j(formSelector+" select option.selection").attr('data-nb-items',nb_selection) ;
			$j(formSelector+" select option.selection").html(str_selection+" ("+nb_selection+")") ;
			$j(formSelector+" select option.selection").removeAttr("disabled");
			if($j(formSelector+" select[name='select_cible']").length>0){
				$j(formSelector+" select[name='select_cible']").get(0).selectedIndex=0;
			}
			$j(formSelector+" #selection_ids").val(list_ids);
		}else{
			$j(formSelector+" select option.selection").attr('data-nb-items',nb_selection) ;
			$j(formSelector+" select option.selection").html(str_selection) ;
			$j(formSelector+" select option.selection").attr("disabled","true");
			if($j(formSelector+" select[name='select_cible']").length>0){
				$j(formSelector+" select[name='select_cible']").get(0).selectedIndex=1;
			}
			$j(formSelector+" #selection_ids").val("");

		}
		
		if(list_ids != ''){
			return true ; 
		}else{
			return false ; 
		}
	}*/
	
		
	function checkActivateIds(){
		if($j("#small_order_form select[name='select_cible']").get(0).selectedIndex==0){
			$j("#small_order_form #selection_ids").removeAttr('disabled');
		}else{
			$j("#small_order_form #selection_ids").attr('disabled',true);
		}
	}

	
	
	
	</script>
<?
}
?>
