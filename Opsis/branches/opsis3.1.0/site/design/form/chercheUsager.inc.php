<div class="title_bar">
	<div id="backButton"><a class="icoBackSearch" href="index.php?urlaction=admin">
		<?=kRetourAdmin;?>
	</a></div>
	<?= kListeUsagers ?>
	<div class="toolbar">
		<!--div  onclick="triggerPrint('usager')" class=" print tool"-->
		<div  onclick="javascript:popupPrint('print.php?<?=$_SERVER['QUERY_STRING'];?>','main')" class=" print tool">
			<span class="tool_hover_text"><?echo kImprimer;?></span>
		</div>
		<div  class="menu_actions tool"><span class="tool_hover_text"><?echo kMenuActions;?></span>
			<div class="drop_down" style="display:none;">
				<ul>
					<li onclick="displayMenuActions('export','usager');"><? print kExporter?></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="contentBody">
	<?
	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheUsager.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="USAGER";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
	?>
</div>