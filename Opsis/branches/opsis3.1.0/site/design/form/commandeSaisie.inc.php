<script type="text/javascript">
	function selectAll(tf){

		checkedItems=document.getElementsByTagName('input');
		for (i=0;i<checkedItems.length;i++) {
			if (checkedItems[i].type=='checkbox' && checkedItems[i]!=tf ) {
				checkedItems[i].checked=tf.checked;
			}
		}
	}

	function refresh() {
		document.form1.commande.value="SAVE";
		document.form1.submit();
	}

	function livraison(id_job) {
		document.form1.commande.value="LIVRAISON";
		document.form1.action="index.php?urlaction=commandeSaisie&id_panier=<?=$myPanier->t_panier['ID_PANIER'];?>";
		if(id_job) document.form1.action+='&id_job='+id_job;
		if(document.getElementById('livraison')) document.getElementById('livraison').innerHTML='<?= kMsgPreparationCommande ?>';
		submitForm();
	}

	function livraisonLigne(i) {
		document.form1.commande.value="LIVRAISON_LIGNE";
		document.form1.action="index.php?urlaction=commandeSaisie&id_panier=<?=$myPanier->t_panier['ID_PANIER'];?>";
		document.form1.ligne.value=i;
		if (document.getElementById('livraison'))
			document.getElementById('livraison').innerHTML='<h2><?=kMsgPreparationCommande;?></h2>';
		document.form1.submit();
	}

    function livraison_job(myform){
        if($j('#panxml_id_proc').val() >= 190 && $j('#panxml_id_proc').val() < 200)
            myform.commande.value='LIVRAISON_DVD';
        else	
            myform.commande.value='LIVRAISON_JOB';
        
        myform.submit();
    }

	function commander(type) {
		document.form1.commande.value="INIT";
		document.form1.pan_id_type_commande.value=type;
		if (document.form1.id_panier.value!='_session') 
			document.form1.action="?urlaction=commande<xsl:value-of select='$urlparams'/>";
			else 
			document.form1.action="<xsl:value-of select='$scripturl'/>?urlaction=inscription&amp;type_commande="+type;
			document.form1.submit();
	}

	function updateMatLignes(obj){
		support=obj.options[obj.selectedIndex].text;
		if(support=="MPEG4") pattern="vis";
		else pattern="csv";
		selectItems=document.getElementsByTagName('select');
		for (i=0;i<selectItems.length;i++) {
		 if (selectItems[i].name.indexOf('ID_MAT')>0 ) {
			 opts=selectItems[i].options;
			 for (j=0;j<opts.length;j++) {
				 val=opts[j].value.toLowerCase();
				if(val.indexOf(pattern) > 0 && ((pattern=='csv' && val.indexOf('dvd')==-1) || pattern=='dvd') && val.indexOf('vis')==-1) opts.selectedIndex=j;
				if(pattern=='vis' && val.indexOf('mp4')>0) opts.selectedIndex=j;
			 }
		 }
		}
	 
	}

    function updateAllIdProcLignes(main_select,media_type){
        
        if($j(main_select).val() >=190 && $j(main_select).val() < 200){
            $j('#panxml_id_proc').val($j(main_select).val()); // Encodage DVD
            $j(".id_proc_lignes.media_type_"+media_type).each(function(idx,elt){
                                                              $j(elt).val("");
                                                              });
        } else {
            $j('#panxml_id_proc').val(''); // Encodage DVD
            $j("select.id_proc_lignes.media_type_"+media_type).each(function(idx,elt){
                                                          $j(elt).val($j(main_select).val());
                                                          });
        }
        $j("select.id_proc_lignes.media_type_"+media_type).each(function(idx,elt){
                                                          copyToSupportLiv($j(elt).eq(0));
                                                          });
    }

    function copyToSupportLiv(elt){
        $j(elt).parent().parent().find("input.copy_pdoc_support_liv").val($j(elt).val());
    }

    function updateAllIdProcFromSupportLiv(){
        $j("select.id_proc_lignes").each(function(idx,elt){
                                                          $j(elt).val($j(elt).parent().parent().find("input.copy_pdoc_support_liv").val());
                                                          });
    }

</script>
<div class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<div id="pageTitre"><?= kCommande ?>&nbsp;</div>
</div>
<?
foreach ($myPanier->t_panier_doc as &$pdoc) {
	if ($pdoc['PDOC_EXTRAIT'] == "1")
		$pdoc['PDOC_EXT'] = $pdoc['PDOC_EXT_TITRE']."<br />".$pdoc['PDOC_EXT_TCIN']."<br />".$pdoc['PDOC_EXT_TCOUT'];
}

include(libDir."class_formSaisie.php");
$xmlform=file_get_contents(getSiteFile("designDir","form/xml/commandeSaisie.xml"));

//Remplacement des valeurs dans le formulaire
$xmlform=str_replace("[PAN_ID_TYPE_COMMANDE]", $myPanier->t_panier["PAN_ID_TYPE_COMMANDE"], $xmlform);

$myForm=new FormSaisie;
$myForm->saisieObj=$myPanier;
$myForm->entity="PANIER";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->classHelp="help_champs_form";
$myForm->display($xmlform);

echo "<script> updateAllIdProcFromSupportLiv(); \$j('#panxml_id_proc_video').val(\$j('#panxml_id_proc').val()); </script>";

if (isset($myLivraison))
{
	foreach ($myLivraison->arrDocs as $idx=>$doc) {
		if (!empty($doc['fileSize'])) { //le fichier a �t� demand� et bien livr� (taille > 0)
			echo "<iframe src='download.php?livraison=".$myLivraison->idDir.'/'.$doc['fileName']."' name='frameCommande".$idx."' id='frameCommande".$idx."' width='0' height='0'>&#160;</iframe>";
		}
	}
}
?>