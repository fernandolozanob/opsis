<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->

<xsl:param name="scripturl"/>
<xsl:param name="id_lang"/>

<xsl:template match='/t_categorie'>
<data>
	<rows>
	   <xsl:for-each select="t_doc_cat/DOC_CAT/t_doc">
	  
			
			<xsl:variable name="titretraduit">
			<xsl:choose>
			<xsl:when test="ID_LANG='fr' or ID_LANG='FR'">
				<xsl:value-of select="DOC_TITRE"/>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="DOC_AUTRE_TITRE" /></xsl:otherwise>
			</xsl:choose>
			</xsl:variable>
			<xsl:variable name="articletraduit"><xsl:choose><xsl:when test="ID_LANG='fr' or ID_LANG='FR'"><xsl:value-of select="normalize-space(DOC_TITRE_COL)"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="normalize-space(DOC_COTE_ANC)" /></xsl:otherwise></xsl:choose>
			</xsl:variable>
			
			<xsl:variable name="titre"><xsl:value-of select="DOC_TITRE"/></xsl:variable>
			<xsl:variable name="id_priv"><xsl:value-of select="ID_PRIV"/></xsl:variable>
			<xsl:variable name="j"><xsl:number/></xsl:variable>
			<xsl:variable name="rang" select="number($j)"/>
			<row rang="{$rang}">
			  <id_doc><xsl:value-of select="ID_DOC"/></id_doc>
			  <id_lang><xsl:value-of select="ID_LANG"/></id_lang>
			  <titre_original><xsl:if test="DOC_SOUSTITRE!='&#160;'">(<xsl:value-of select="DOC_SOUSTITRE"/>) </xsl:if>
					<xsl:value-of select="DOC_TITREORI"/>
			  </titre_original>
			  <titre_traduit>
					<xsl:if test="normalize-space($articletraduit)!='&#160;'"><xsl:value-of select="$articletraduit"/>&#160;</xsl:if><xsl:value-of select="$titretraduit"/>
			  </titre_traduit>
			  <annee><xsl:value-of select="substring-before(DOC_DATE_PROD,'-')"/></annee>
			  <resume>
				   <xsl:choose>
						<xsl:when test="ID_LANG='fr' or ID_LANG='FR'">
							<xsl:value-of select="DOC_RES"/>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="DOC_RES_CAT" /></xsl:otherwise>
				   </xsl:choose>     	
			  </resume>
			  <duree><xsl:value-of select="DOC_DUREE"/></duree>
			  <lien>service.php?urlaction=detailRang&amp;amp;rang=<xsl:value-of select="$rang"/></lien>
			  <realisateurs>
					<xsl:for-each select="DOC_LEX/XML/LEX[@ROLE='REA']">
						<realisateur>
						<id_pers><xsl:value-of select="normalize-space(ID_PERS)"/></id_pers>
						<nom><xsl:value-of select="normalize-space(TERME)"/></nom>
						<lien>service.php?urlaction=detail&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="normalize-space(ID_PERS)"/></lien>
						</realisateur>
					</xsl:for-each>       	
			  </realisateurs>
			  <pays>
				  <xsl:for-each select="DOC_LEX/XML/LEX[@ROLE='PS']">
				  <pays>
				  <xsl:value-of select="normalize-space(TERME)"/>
				  </pays>
				  </xsl:for-each>
			  </pays>
			  <festivals>
				<xsl:for-each select="DOC_XML/XML/FESTIVAL/FEST">
					<festival>
					<annee><xsl:value-of select="substring-before(FEST_ANNEE,'-')"/></annee>
					</festival>
				</xsl:for-each>
				
			  </festivals>
			</row>
		</xsl:for-each>
	</rows>
</data>


</xsl:template>


<xsl:template name="internal-quote-replace">
	  <xsl:param name="stream" />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>


</xsl:stylesheet>
