<?php
$myPage=Page::getInstance();

// Surbrillance du menu
$HL['tapeListe']='#tabs-1';
$HL['tapeSetListe']='#tabs-2';
$HL['backupLibView']='#tabs-3';
?>
<div id='pageTitre' class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<?= kGestionBackup ?>
</div>
<div id="tabs">

	<ul>
		<li><a href='#tabs-1' onclick="document.location='<?=$myPage->getName()?>?urlaction=tapeListe'"><?= kCartouches ?></a></li>
		<li><a href='#tabs-2' onclick="document.location='<?=$myPage->getName()?>?urlaction=tapeSetListe'"><?= kJeuxCartouches ?></a></li>
		<li><a href='#tabs-3' onclick="document.location='<?=$myPage->getName()?>?urlaction=backupLibView'"><?= kLibrairie ?></a></li>
	</ul>
	<!-- l'usage des tabs jQuery requiert une structure  sous forme de liste et de divs référencés par cette liste
	Dans notre cas on utilise pas réellement les divs référencés, mais on affiche l'onglet requis via XML/XSL,
	mais les divs sont necessaire au bon fonctionnement des onglets-->
	<div style="display : none !important;"> 
		<div id="tabs-1" style="display:none"></div>
		<div id="tabs-2" style="display:none"></div>
		<div id="tabs-3" style="display:none"></div>
	</div>
</div>
<br/>
<script type="text/javascript">
var $tabs = $j('#tabs').tabs();
$j('#tabs li').removeClass('ui-tabs-selected ui-state-active');
$j("a[href='<?=$HL[$_GET['urlaction']]?>']").parent('li').addClass('ui-tabs-selected ui-state-active');
</script>
