<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->

<xsl:param name="scripturl"/>
<xsl:param name="id_lang"/>

<xsl:key name="preg" match="t_doc" use="ID_DOC"/>
<xsl:template match='/t_festival'>

<data>



   <festival id_fest="{ID_FEST}">
  
   
      <id_fest><xsl:value-of select="ID_FEST"/></id_fest>
      <id_lang><xsl:value-of select="ID_LANG"/></id_lang>
      <libelle><xsl:value-of select="FEST_LIBELLE"/></libelle>
      <annee><xsl:value-of select="substring-before(FEST_ANNEE,'-')"/></annee>
      <description><xsl:value-of select="FEST_DESC"/></description>
      <palmares><xsl:value-of select="FEST_PALMARES"/></palmares>
	  <vignette>
	  <xsl:if test="VIGNETTE!=''">
	  	service.php?urlaction=getfile&amp;amp;type=doc_acc&amp;amp;filename=<xsl:value-of select="substring-after(substring-after(VIGNETTE,'/'),'/')"/>
	  </xsl:if>
	  </vignette>
      
      <personnes>
      	<xsl:for-each select="t_fest_pers/TYPE_DESC/t_rol/t_personne">
      		<personne>
      			<id_pers><xsl:value-of select="ID_PERS"/></id_pers>
   	  				<nom>
   	  					<xsl:if test="PERS_PRENOM!=''"><xsl:value-of select="PERS_PRENOM"/><xsl:text> </xsl:text></xsl:if>
   	  					<xsl:if test="PERS_PARTICULE!=''"><xsl:value-of select="PERS_PARTICULE"/><xsl:text> </xsl:text></xsl:if>
   	  					<xsl:value-of select="PERS_NOM"/>
   	  				</nom>
   	  				<pays><xsl:value-of select="PERS_ORGANISATION"/></pays>
   	  				<role><xsl:value-of select="ROLE"/></role>
   	  				<precision><xsl:value-of select="LEX_PRECISION"/></precision>
   	  				<lien>service.php?urlaction=detail&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="ID_PERS"/></lien>
      		</personne>
      	</xsl:for-each>
      </personnes>
      
      <sections>
      	<xsl:for-each select="t_section/FEST">
      		<section>
      			<libelle><xsl:value-of select="FEST_LIBELLE"/></libelle>
      			<films>
      				<xsl:for-each select="FESTIVAL/t_festival/t_fest_doc/DOC">
      					<id_doc><xsl:value-of select="ID_DOC"/></id_doc>
      					<id_lang><xsl:value-of select="ID_LANG"/></id_lang>
      					<xsl:variable name="titretraduit">
		                <xsl:choose>
		                <xsl:when test="$id_lang='fr' or $id_lang='FR'">
		                	<xsl:value-of select="DOC_TITRE"/>
		                </xsl:when>
		                <xsl:otherwise><xsl:value-of select="DOC_AUTRE_TITRE" /></xsl:otherwise>
		                </xsl:choose>
		                </xsl:variable>
		                <xsl:variable name="articletraduit"><xsl:choose><xsl:when test="$id_lang='fr' or $id_lang='FR'"><xsl:value-of select="normalize-space(DOC_TITRE_COL)"/></xsl:when>
		    	            <xsl:otherwise><xsl:value-of select="normalize-space(DOC_COTE_ANC)" /></xsl:otherwise></xsl:choose>
		   			   </xsl:variable>
			        <titre_original><xsl:if test="DOC_SOUSTITRE!=''">(<xsl:value-of select="DOC_SOUSTITRE"/>) </xsl:if>
						<xsl:value-of select="DOC_TITREORI"/>
			        </titre_original>
			        <titre_traduit>
			      		<xsl:if test="normalize-space($articletraduit)!=''"><xsl:value-of select="$articletraduit"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="$titretraduit"/>
			        </titre_traduit>
			        <annee><xsl:value-of select="substring-before(DOC_DATE_PROD,'-')"/></annee>
			        <realisateur>
			        	<xsl:for-each select="DOC_LEX/XML/LEX[@TYPE='GEN' and @ROLE='REA']">
			        		<personne>
			        		<id_pers><xsl:value-of select="normalize-space(ID_PERS)"/></id_pers>
			        		<nom>
								<xsl:if test="normalize-space(TERME/PERS_PRENOM)!=''"><xsl:value-of select="normalize-space(TERME/PERS_PRENOM)"/><xsl:text> </xsl:text></xsl:if>
								<xsl:if test="normalize-space(TERME/PERS_PARTICULE)!=''"><xsl:value-of select="normalize-space(TERME/PERS_PARTICULE)"/><xsl:text> </xsl:text></xsl:if>
								<xsl:value-of select="normalize-space(TERME/PERS_NOM)"/>
							</nom>
			        		<lien>service.php?urlaction=detail&amp;amp;entite=personne&amp;amp;id=<xsl:value-of select="normalize-space(ID_PERS)"/></lien>
			        		</personne>
			        	</xsl:for-each>
			        </realisateur>
			        <prix>
			        	<xsl:for-each select="DOC_FEST_VAL">
			        		<prix><xsl:value-of select="VALEUR"/></prix>
			        	</xsl:for-each>
			        </prix>
			        <lien>service.php?urlaction=detail&amp;amp;entite=doc&amp;amp;id=<xsl:value-of select="ID_DOC"/></lien>
      				</xsl:for-each>
      			</films>
      		</section>
      	</xsl:for-each>
      </sections>
      
     

	  <pieces_jointes>
	  	<xsl:for-each select="t_doc_acc/t_doc_acc">
	  	<piece_jointe>
	  		<id_doc_acc><xsl:value-of select="ID_DOC_ACC"/></id_doc_acc>
	  		<titre><xsl:value-of select="DA_TITRE"/></titre>
	  		<fichier>service.php?urlaction=getfile&amp;amp;type=doc_acc&amp;amp;filename=<xsl:value-of select="DA_CHEMIN"/><xsl:value-of select="DA_FICHIER"/></fichier>
	  		<type><xsl:value-of select="t_doc_acc_val/t_val/t_val/VALEUR"/></type>
	  	</piece_jointe>
	  	</xsl:for-each>
	  </pieces_jointes>

    </festival>
</data>


</xsl:template>


<xsl:template name="internal-quote-replace">
	  <xsl:param name="stream" />
	  <xsl:variable name="simple-quote">'</xsl:variable>

		<xsl:choose>

		<xsl:when test="contains($stream,$simple-quote)">
	<xsl:value-of
	select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
	name="internal-quote-replace"><xsl:with-param name="stream"
	select="substring-after($stream,$simple-quote)"/></xsl:call-template>
		</xsl:when>

		<xsl:otherwise>
	<xsl:value-of select="$stream"/>
	  </xsl:otherwise>

		</xsl:choose>

</xsl:template>


</xsl:stylesheet>
