<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common"  xmlns:str="http://exslt.org/strings" extension-element-prefixes="str">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<xsl:key name="t_lex-by-id_role" match="t_lex" use="concat(../../ID_DOC,'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE)"/>
<xsl:key name="t_pers-by-id_role" match="t_personne" use="concat(../../ID_DOC,'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE)"/>


<xsl:template name='pagerListe'>
	<xsl:param name="form"/>
	<xsl:param name="class"/>
	<xsl:param name="entity"/>
	<xsl:param name="noscript"/>

	<xsl:if test="$noscript!='1'">
		<script type="text/javascript">
		var pagerForm=document.<xsl:value-of select="$form"/>;
		</script>
		<script type="text/javascript" src="&lt;?=libUrl?&gt;/webComponents/opsis/liste.js">&amp;nbsp;</script>

		<div id="sauvPanier" class="resultsMenu" style='display:none'>
			<xsl:processing-instruction name="php">print kNouvelleSelection;</xsl:processing-instruction>
			<input name="newCartName" type="text" id="newCartName"/>
		&#160;<img style="cursor : pointer;" class="cancel" src='design/images/abandon.gif' onclick="javascript:hideSauvPanier();" />
		&#160;<img style="cursor : pointer;" class="valid" src='design/images/save.gif' onclick="javascript:add2cart();" /> 
		</div>
	</xsl:if>
	<div class="resultsMenu pager_control {$class}" >
		<div class="nb_res"><xsl:value-of select="$nb_rows" />&#160;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction></div>
		<div class="browser">
			<xsl:if test="$nb_pages>1">
				<xsl:if test="$page>1"><a href="#" onclick='javascript:pagerForm.page.value={$page - 1};pagerForm.submit();'><img src='design/images/left_pg.gif' border='0' /> </a> </xsl:if>
				<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onkeydown='changePage(event,{$page},${nb_pages})' onchange='goPage(this,{$page},{$nb_pages})' />
				<xsl:value-of select="concat(' / ',$nb_pages)"/>
				<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:pagerForm.page.value={$page + 1};pagerForm.submit();'><img src='design/images/right_pg.gif' border='0' /></a></xsl:if>
			</xsl:if>&#160;
		</div>
		<div class="res_par_pages">
			<span class="desktop_label"><xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction></span>
			<select class='val_champs_form' name="selectNbLignes" onchange="javascript:pagerForm.nbLignes.value=this.value;pagerForm.submit()">
				<option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
				<option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
				<option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
				<option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
				<!--option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option-->
			</select>
			<span class="mobile_label"><xsl:processing-instruction name="php">print kParPage;</xsl:processing-instruction></span>
		</div>
	</div>


</xsl:template>

<xsl:template name='pager'>
	<xsl:param name="export"/>
	<xsl:param name="print"/>
	<xsl:param name="panier"/>
	<xsl:param name="form"/>
	<xsl:param name="modifLot"/>
	<xsl:param name="entity"/>
	<xsl:param name="noscript"/>
	<xsl:param name="duree"/>
	<xsl:param name="diaporama"/>
	<xsl:param name="profil"/>

<xsl:if test="$noscript!='1'">

	<script type="text/javascript">
	var pagerForm=document.<xsl:value-of select="$form"/>;
	</script>
	<script type="text/javascript" src="&lt;?=libUrl?&gt;/webComponents/opsis/liste.js">&amp;nbsp;</script>

	<div id="sauvPanier" class="resultsMenu" style='display:none'>
		<xsl:processing-instruction name="php">print kNouvelleSelection;</xsl:processing-instruction>
		<input name="newCartName" type="text" id="newCartName"/>
	&#160;<img style="cursor : pointer;" class="cancel" src='design/images/abandon.gif' onclick="javascript:hideSauvPanier();" />
	&#160;<img style="cursor : pointer;" class="valid" src='design/images/save.gif' onclick="javascript:add2cart();" /> 
	</div>
</xsl:if>
	<table width="100%" border="0" class="resultsMenu" align="center">
		<tr>
			<td align="left" width="33%">
				<xsl:value-of select="$nb_rows" />&#160;<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>
				<!-- @update VG 12/04/2010 : on ajoute la duree totale du panier -->
				<xsl:if test="$duree != ''">
					<xsl:processing-instruction name="php">print kDuree</xsl:processing-instruction>: <xsl:value-of select="$duree" />
				</xsl:if>
			</td>
			<td align="center" width="33%" rowspan="2">
				<xsl:if test="$nb_pages>1">
					<xsl:if test="$page>1"><a href="#" onclick='javascript:pagerForm.page.value={$page - 1};pagerForm.submit();'><img src='design/images/left_pg.gif' border='0' /> </a> </xsl:if>
					<input type='text' name='rang' value='{$page}' size='5' class="resultsInput" onkeydown='changePage(event,{$page},${nb_page})' onchange='goPage(this,{$page},{$nb_pages})' />
					<xsl:value-of select="concat(' / ',$nb_pages)"/>
					<xsl:if test="$page &lt; $nb_pages"> <a href="#" onclick='javascript:pagerForm.page.value={$page + 1};pagerForm.submit();'><img src='design/images/right_pg.gif' border='0' /></a></xsl:if>
				</xsl:if>&#160;
			</td>
			<td align="right" width="33%">
                <xsl:processing-instruction name="php">print kResultatsParPage;</xsl:processing-instruction>
                <select class='val_champs_form' name="selectNbLignes" onchange="javascript:pagerForm.nbLignes.value=this.value;pagerForm.submit()">
                    <option value="10"><xsl:if test="$nbLigne=10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>10</option>
                    <option value="20"><xsl:if test="$nbLigne=20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>20</option>
                    <option value="50"><xsl:if test="$nbLigne=50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>50</option>
                    <option value="100"><xsl:if test="$nbLigne=100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>100</option>
                    <option value="all"><xsl:if test="$nbLigne='all'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">print kTous;</xsl:processing-instruction></option>
                </select>
			</td>
		</tr>
		<tr>
			<td align="left" colspan="2" width="66%">
				<xsl:if test="cart_list!='' and $panier!=''">
					<xsl:variable name="current_panier"><xsl:value-of select="t_panier/ID_PANIER" /></xsl:variable>
					<xsl:choose>
						<xsl:when test="$profil > 0">
							<select name="cartList" class="val_champs_form" id="cartList" onchange="add2cart(this);">
								<option value='0'><xsl:processing-instruction name="php">print kPanierAjoutDoc;</xsl:processing-instruction></option>
								<xsl:for-each select="cart_list">
								<xsl:sort select="cart_list/PAN_PUBLIC"/>
									<xsl:if test="ID_PANIER!=$current_panier and PAN_PUBLIC=0">
											<xsl:choose>
											<xsl:when test="PAN_ID_ETAT=1">
												<option value='{ID_PANIER}' style='font-weight:bold;'>
												<xsl:processing-instruction name="php">print kPanier;</xsl:processing-instruction>
												</option>
											</xsl:when>
											<xsl:when test="PAN_DOSSIER=0 or ID_PANIER=-1">
												<option value='{ID_PANIER}'>
												<xsl:value-of select="PAN_TITRE"/>
												</option>
											</xsl:when>
											</xsl:choose>

									</xsl:if>
								</xsl:for-each>
							</select>
							&amp;nbsp;
							<select name="dossierList" class="val_champs_form" id="dossierList" onchange="add2cart(this);">
								<option value='0'><xsl:processing-instruction name="php">print kAjouterDossierDoc;</xsl:processing-instruction></option>
								<xsl:for-each select="cart_list">
								<xsl:sort select="cart_list/PAN_PUBLIC"/>
									<xsl:if test="ID_PANIER!=$current_panier and PAN_PUBLIC=1">
											<xsl:choose>
											<xsl:when test="PAN_ID_ETAT=1">
												<option value='{ID_PANIER}' style='font-weight:bold;'>
												<xsl:processing-instruction name="php">print kPanier;</xsl:processing-instruction>
												</option>
											</xsl:when>
											<xsl:when test="PAN_DOSSIER=0 or ID_PANIER=-1">
												<option value='{ID_PANIER}'>
												<xsl:value-of select="PAN_TITRE"/>
												</option>
											</xsl:when>
											</xsl:choose>

									</xsl:if>
								</xsl:for-each>
							</select>

						</xsl:when>
						<xsl:when test="$profil=0"> <!--  pas loggé -->
							<a href="javascript:add2cart(this);"><xsl:processing-instruction name="php">print kAjouterPanier;</xsl:processing-instruction></a>
							<input type='hidden'  name='cartList' id='cartList' value='_session' />
						</xsl:when>
					</xsl:choose>
				</xsl:if>
			</td>
			<td align="right" >
				<xsl:if test="$diaporama!=''">
					<xsl:variable name="typeDiapo">
						<xsl:choose>
							<xsl:when test="t_panier/ID_PANIER!=''">panier&amp;id=<xsl:value-of select="t_panier/ID_PANIER" /></xsl:when>
							<xsl:otherwise>recherche_DOC</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<a href="javascript:void(0);" onclick="genDiaporamaFromCheckbox();">
						<img src="design/images/sync_visio.gif" border="0" />&#160;<xsl:processing-instruction name="php">print kDiaporama;</xsl:processing-instruction>
					</a>
					<xsl:text> | </xsl:text>
				</xsl:if>
                                <xsl:if test="$profil>=3 and $modifLot!='' and $modifLot!='panier'">
					<a href="javascript:popupModifLot('simple.php?urlaction=modifLot&amp;amp;entite={$entity}','main','lots')">
					<img src="design/images/button_modif.gif" border="0" alt="modification" />&#160;<xsl:processing-instruction name="php">print kModifierLot;</xsl:processing-instruction>
					</a>
					<xsl:text> | </xsl:text>
				</xsl:if>
				<xsl:if test="$print!=''">
					<a href="javascript:popupPrint('{$print}','main')">
					<img src="design/images/famfamfam_mini_icons/action_print.gif" border="0" alt="print" />&#160;<xsl:processing-instruction name="php">print kImprimer;</xsl:processing-instruction>
					</a>
				</xsl:if>
				<xsl:if test="$export!='' and $profil>=3">
					<xsl:text> | </xsl:text>
                    <xsl:variable name="exportLink">
                        <xsl:choose>
                            <xsl:when test="$export!=''"><xsl:value-of select="$export"/></xsl:when>
                            <xsl:otherwise><xsl:text>export.php?type=doc&amp;sql=recherche_DOC</xsl:text></xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
					<!--a href="javascript:popupPrint('{$export}&amp;amp;nb={$nbLigne}&amp;amp;page={$page}','main')"!-->
                    <!--a href="javascript:popupPrint('export.php?type=doc&amp;sql=recherche_DOC&amp;amp;nb={$nbLigne}&amp;amp;page={$page}','main')"-->
                    <a href="javascript:popupPrint('{$exportLink}&amp;amp;nb={$nbLigne}&amp;amp;page={$page}','main')" >
<img src="design/images/famfamfam_mini_icons/page_script.gif" border="0" />&#160;<xsl:processing-instruction name="php">print kExporter;</xsl:processing-instruction>
					</a>
				</xsl:if>
			</td>
		</tr>
	</table>
</xsl:template>

<xsl:template name='displayListe'>
	<xsl:param name="xmllist"/>
	<xsl:param name="fromOffset"/>
	<xsl:variable name="entity" select="$xmllist/list/entity"/>
	<xsl:variable name="urlaction" select="$xmllist/list/urlaction"/>
    <table width="97%" align="center" cellspacing="0" class="tableResults" id="{$urlaction}" >
		<tr class="resultsHead_row">
            <xsl:for-each select="$xmllist/list/element">
				<xsl:if test="not(profil_min!='') or (profil_min!='' and $profil &gt;= profil_min)">
					<td class="resultsHead" width="{width}">
					<xsl:choose>
					<xsl:when test="tri!=''">
						<xsl:if test="contains($tri,tri)"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
						<xsl:choose>
							<xsl:when test="$entity='t_panier_doc'">
								<a href="javascript:loadPanier(id_current_panier,null,'{tri}',null,null,null,'{$ordre}')"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></a>
							</xsl:when>
							<xsl:otherwise>
								<a href="javascript:trier('{tri}{$ordre}')"><xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction></a>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="type='checkbox'">
						<input type="checkbox" name="checkAll" onclick="selectAll(this)"  />
					</xsl:when>
					<xsl:otherwise>
						<xsl:processing-instruction name="php">
							if (defined('<xsl:value-of select="label"/>'))
								print <xsl:value-of select="label"/>;
							else
								print '<xsl:value-of select="label"/>';
						</xsl:processing-instruction>
					</xsl:otherwise>
					</xsl:choose>
					</td>
				</xsl:if>
            </xsl:for-each>
        </tr>
		<tr class="separator"><td ><xsl:attribute name="colspan"><xsl:value-of select="count($xmllist/list/element)"/></xsl:attribute>&#160;</td></tr>
         <xsl:for-each select="child::*[name()=$entity]">
			<xsl:variable name="row" select="child::*"/>
			<xsl:variable name="j"><xsl:number/></xsl:variable>

			<xsl:call-template name="displayRow">
				<xsl:with-param name="xmllist" select="$xmllist"/>
				<xsl:with-param name="row" select="$row"/>
				<xsl:with-param name="j" select="$j"/>
				<xsl:with-param name="div" select="0"/>
				<xsl:with-param name="fromOffset" select="$fromOffset"/>
			</xsl:call-template>
        </xsl:for-each>
     </table>
</xsl:template>

<xsl:template name='displaySortableListe'>
	<xsl:param name="xmllist"/>
	<xsl:variable name="entity" select="$xmllist/list/entity"/>
	<xsl:variable name="urlaction" select="$xmllist/list/urlaction"/>
    <div id="{$urlaction}" >
		<xsl:for-each select="$xmllist/list/element">
			<xsl:if test="not(profil_min!='') or (profil_min!='' and $profil &gt;= profil_min)">
				<div class="resultsHead_div" style="{concat('width:',width,'px')}">
				<xsl:choose>
					<xsl:when test="tri!=''">
						<xsl:if test="contains($tri,tri)"><img src="design/images/order_{$ordre}.gif" alt="trier" width="7" height="14" border="0" /></xsl:if>
						<a href="javascript:trier('{tri}{$ordre}')">
							<xsl:choose>
								<xsl:when test="label!='' and label!='&#160;'">
									<xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction>
								</xsl:when>
								<xsl:otherwise>&#160;</xsl:otherwise>
							</xsl:choose>
						</a>
					</xsl:when>
					<xsl:when test="type='checkbox'">
						<input type="checkbox" name="checkAll" onclick="selectAll(this)"  />
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="label!='' and label!='&#160;'">
								<xsl:processing-instruction name="php">print <xsl:value-of select="label"/>;</xsl:processing-instruction>
							</xsl:when>
							<xsl:otherwise>&#160;</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				</div>
			</xsl:if>
		</xsl:for-each>
	</div>

	<xsl:variable name="w"><xsl:value-of select="20 + sum($xmllist/list/element/width)"/></xsl:variable>

	<ul id='resultsTable' style='clear:both;list-style:none;padding:0px;margin:0px;'>
		<xsl:for-each select="child::*[name()=$entity]">
			<xsl:variable name="row" select="child::*"/>
			<xsl:variable name="j"><xsl:number/></xsl:variable>

			<li style='clear:both;cursor:N-resize;margin:0px;height:100px;width:{$w}px'>
				<xsl:call-template name="displayRow">
					<xsl:with-param name="xmllist" select="$xmllist"/>
					<xsl:with-param name="row" select="$row"/>
					<xsl:with-param name="j" select="$j"/>
					<xsl:with-param name="div" select="1"/>
				</xsl:call-template>
			</li>

		</xsl:for-each>
	</ul>
	<div style="clear:both;" >&#160;</div>

	<script type="text/javascript">
	/*$j('#resultsTable').sortable
	(
		{
			appendTo: null,
			axis:'y',
			beforeStop: function(event, ui)
			{
				refresh();
			}
		}
	);*/
	</script>
</xsl:template>

<xsl:template name='displayTabListe'>
	<xsl:param name="xmllist"/>
	<xsl:param name="hasheader"/>
	<xsl:variable name="entity" select="$xmllist//entity"/>
	<xsl:if test="$hasheader='' or $hasheader='1'">
		<!-- l'affichage des tetes de colonne est effectué depuis les constantes php, mais le pb est que la conversion UTF8 -> ISO est faite avant le eval qui permet d'imprimer les valeurs, les valeurs une fois imprimée n'ont donc pas le bon encodage. On fait donc le décodage manuellement ici -->
		<xsl:for-each select="$xmllist//element">&lt;?= defined('<xsl:value-of select="label"/>')?utf8_decode(constant('<xsl:value-of select="label"/>')):utf8_decode('<xsl:value-of select="label"/>'); ?&gt;<xsl:if test="sufLabel and sufLabel != ''"><xsl:text> </xsl:text>&lt;?= defined('<xsl:value-of select="sufLabel"/>')?utf8_decode(constant('<xsl:value-of select="sufLabel"/>')):utf8_decode('<xsl:value-of select="sufLabel"/>'); ?&gt;</xsl:if><xsl:text>	</xsl:text>
		</xsl:for-each>
	<xsl:text>
</xsl:text>
</xsl:if>

	<xsl:for-each select="child::*[name()=$entity]">
		<xsl:variable name="row" select="child::*"/>
		<xsl:for-each select="$xmllist//element">
			<xsl:variable name="field" select="chFields"/>
			<xsl:if test="(not(type_media) or type_media='' or ($row[name()='DOC_ID_MEDIA']!='' and contains(type_media,$row[name()='DOC_ID_MEDIA'])) or ($row[name()='MAT_ID_MEDIA']!='' and contains(type_media,$row[name()='MAT_ID_MEDIA'])))">
				<xsl:choose>
					<xsl:when test="(type='DOC_LEX' or type='LEX') and role!=''">
						<xsl:variable name="role" select="role"/>
						<xsl:for-each select="$row[name()='t_doc_lex']//t_lex[ID_TYPE_DESC=$field and DLEX_ID_ROLE=$role]">
							<xsl:if test="LEX_TERME!=''"><xsl:value-of select="normalize-space(LEX_TERME)"/></xsl:if>
							<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="(type='DOC_LEX' or type='LEX') ">
						<xsl:variable name="condition_role" select="not(dont_show_role) or dont_show_role=''"/>
						<xsl:for-each select="$row[name()='t_doc_lex']/t_lex[ID_TYPE_DESC=$field][not($condition_role) or count(.|key('t_lex-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[1])=1]">
							<xsl:if test="$condition_role and DLEX_ID_ROLE!=''"><xsl:value-of select="ROLE"/><xsl:text> : </xsl:text></xsl:if>
							<xsl:for-each select="current()[not($condition_role)] | key('t_lex-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[$condition_role]">
								<xsl:value-of select="LEX_TERME"/>
								<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
							</xsl:for-each>
							<xsl:if test="position()!=last()">
								<xsl:choose>
									<xsl:when test="$condition_role">
										<xsl:text>&#160;-&#160;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>, </xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='PERS' and role!=''">
						<xsl:variable name="role" select="role"/>
						<xsl:for-each select="$row[name()='t_pers_lex']//t_personne[ID_TYPE_DESC=$field and DLEX_ID_ROLE=$role]">
							<xsl:value-of select="PERS_PRENOM"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="PERS_NOM"/>
							<xsl:if test="position()!=last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='PERS'">
						<xsl:variable name="condition_role" select="not(dont_show_role) or dont_show_role=''"/>
						<xsl:for-each select="$row[name()='t_pers_lex']/t_personne[ID_TYPE_DESC=$field][not($condition_role) or count(.|key('t_pers-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[1])=1]">
							<xsl:if test="$condition_role and DLEX_ID_ROLE!=''"><xsl:value-of select="ROLE"/><xsl:text> : </xsl:text></xsl:if>
							<xsl:for-each select="current()[not($condition_role)] | key('t_pers-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[$condition_role]">
								<xsl:value-of select="PERS_PRENOM"/>
								<!--pour eviter d'avoir un blanc disgracieux en 1ère ligne alors que pers_prenom est vide-->
								<xsl:if test="PERS_PRENOM!=''" >
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="PERS_NOM"/>
								<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
							</xsl:for-each>
							<xsl:if test="position()!=last()">
								<xsl:choose>
									<xsl:when test="$condition_role">
										<xsl:text>&#160;-&#160;</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>, </xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='media' and $row[name()=$field]!=''">
						<xsl:variable name="media" select="$row[name()=$field]"/>
						<xsl:choose>
							<xsl:when test="$media='V'">&lt;?print utf8_decode(kVideo);?&gt;</xsl:when>
							<xsl:when test="$media='P'">&lt;?print utf8_decode(kImage);?&gt;</xsl:when>
							<xsl:when test="$media='A'">&lt;?print utf8_decode(kAudio);?&gt;</xsl:when>
							<xsl:when test="$media='D'">&lt;?print utf8_decode(kDocument);?&gt;</xsl:when>
							<xsl:when test="$media='R'">&lt;?print utf8_decode(kReportage);?&gt;</xsl:when>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="type='count_chidren' and $row[name()=$field] != ''">
						<xsl:value-of select="count($row[name()=$field]/*)"/>
					</xsl:when>
					<xsl:when test="type='DOC_VAL' and $row[name()='DOC_VAL']/XML/*[name()=$field]!=''">
						<xsl:for-each select="$row[name()='DOC_VAL']/XML/*[name()=$field]">
							<xsl:value-of select="."/>
							<xsl:if test="position()!=last()">, </xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='VAL' and $row[name()='t_doc_val']/t_val[ID_TYPE_VAL=$field]!=''">
						<xsl:for-each select=" $row[name()='t_doc_val']//t_val[ID_TYPE_VAL=$field]">
							<xsl:value-of select="VALEUR"/>
							<xsl:if test="position()!=last()">, </xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='FEST' and $row[name()='t_doc_fest']!=''">
						<xsl:for-each select=" $row[name()='t_doc_fest']/FEST">
							<xsl:value-of select="*[name()=$field]"/>
							<xsl:if test="position()!=last()">, </xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='CAT' and $row[name()='t_doc_cat']/t_categorie/CAT_ID_TYPE_CAT!=''">
						<xsl:for-each select="$row[name()='DOC_CAT']/XML/CAT">
							<xsl:if test="CAT_ID_TYPE_CAT=$field">
								<xsl:value-of select="CAT_NOM"/>
								<xsl:if test="position()!=last()">, </xsl:if>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='DOC_XML' and $row[name()='DOC_XML']/XML/DOC/*[name()=$field]!=''">
							<xsl:value-of select="$row[name()='DOC_XML']/XML/DOC/*[name()=$field]"/>
					</xsl:when>
					<xsl:when test="type='MAT'">
						<xsl:for-each select="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()=$field]">
							<xsl:value-of select="."/>
							<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='IMG_DIM' and $row[name()='DOC_ID_MEDIA'] = 'P'">
						<xsl:if test="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_WIDTH']!='' and $row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_HEIGHT']!='' "><xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_WIDTH']"/>&#160;x&#160;<xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_HEIGHT']"/>px</xsl:if>
					</xsl:when>
					<xsl:when test="$entity = 't_mat' and type='MAT_INFO' ">
						<xsl:for-each select="$row[name()='MAT_INFO']//*[name()=$field]">
							<xsl:value-of select="."/>
							<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="$entity = 't_mat' and starts-with($field,'MT_')">
						<xsl:for-each select="$row[name()='tracks']/t_mat_track[*[name()=$field]!='']">
							<xsl:value-of select="child::*[name()=$field]"/><xsl:if test="MT_INDEX != ''">[<xsl:value-of select="MT_INDEX"/>]</xsl:if>
							<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="replace">
							<xsl:with-param name="stream" select="normalize-space($row[name()=$field])"/>
							<xsl:with-param name="old"><xsl:text>
	</xsl:text></xsl:with-param>
							<xsl:with-param name="new"><xsl:text>\n</xsl:text></xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:text>	</xsl:text>
		</xsl:for-each>
	<xsl:text>
</xsl:text>
	</xsl:for-each>
</xsl:template>


<xsl:template name='displayTexteListe'>
	<xsl:param name="xmllist"/>
	<xsl:param name="hasheader"/>
	<xsl:variable name="entity" select="$xmllist//entity"/>

	<xsl:for-each select="child::*[name()=$entity]">
		<xsl:variable name="row" select="child::*"/>
		<xsl:for-each select="$xmllist//element">
			<xsl:variable name="field" select="chFields"/>
			<xsl:variable name="role2" select="role"/>
			<!-- <xsl:if test="$row[name()=$field]!='' or $row[name()='DOC_XML']/XML/DOC/*[name()=$field]!='' or $row[name()='DOC_VAL']/XML/*[name()=$field]!='' or $row[name()='t_doc_lex']//t_lex[ID_TYPE_DESC=$field and DLEX_ID_ROLE=$role2]/LEX_TERME!=''"> --><!-- Pour éviter les champs qui ont une valeur vide (a affiner selon les champs, risque de perdre des champs sur l'export) -->&lt;? print utf8_decode(constant('<xsl:value-of select="label"/>')); ?&gt;<xsl:text>&#160;:&#160;</xsl:text>
				<xsl:if test="(not(type_media) or type_media='' or ($row[name()='DOC_ID_MEDIA']!='' and contains(type_media,$row[name()='DOC_ID_MEDIA'])) or ($row[name()='MAT_ID_MEDIA']!='' and contains(type_media,$row[name()='MAT_ID_MEDIA'])))">
					<xsl:choose>
						<xsl:when test="(type='DOC_LEX' or type='LEX') and role!=''">
							<xsl:variable name="role" select="role"/>
							<xsl:for-each select="$row[name()='t_doc_lex']//t_lex[ID_TYPE_DESC=$field and DLEX_ID_ROLE=$role]">
								<xsl:if test="LEX_TERME!=''"><xsl:value-of select="normalize-space(LEX_TERME)"/></xsl:if>
								<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="(type='DOC_LEX' or type='LEX') ">
							<xsl:variable name="condition_role" select="not(dont_show_role) or dont_show_role=''"/>
							<xsl:for-each select="$row[name()='t_doc_lex']/t_lex[ID_TYPE_DESC=$field][not($condition_role) or count(.|key('t_lex-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[1])=1]">
								<xsl:if test="$condition_role and DLEX_ID_ROLE!=''"><xsl:value-of select="ROLE"/><xsl:text> : </xsl:text></xsl:if>
								<xsl:for-each select="current()[not($condition_role)] | key('t_lex-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[$condition_role]">
									<xsl:value-of select="LEX_TERME"/>
									<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
								</xsl:for-each>
								<xsl:if test="position()!=last()">
									<xsl:choose>
										<xsl:when test="$condition_role">
											<xsl:text>&#160;-&#160;</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>, </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='PERS' and role!=''">
							<xsl:variable name="role" select="role"/>
							<xsl:for-each select="$row[name()='t_pers_lex']//t_personne[ID_TYPE_DESC=$field and DLEX_ID_ROLE=$role]">
								<xsl:value-of select="PERS_PRENOM"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="PERS_NOM"/>
								<xsl:if test="position()!=last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='PERS'">
							<xsl:variable name="condition_role" select="not(dont_show_role) or dont_show_role=''"/>
							<xsl:for-each select="$row[name()='t_pers_lex']/t_personne[ID_TYPE_DESC=$field][not($condition_role) or count(.|key('t_pers-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[1])=1]">
								<xsl:if test="$condition_role and DLEX_ID_ROLE!=''"><xsl:value-of select="ROLE"/><xsl:text> : </xsl:text></xsl:if>
								<xsl:for-each select="current()[not($condition_role)] | key('t_pers-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[$condition_role]">
									<xsl:value-of select="PERS_PRENOM"/>
									<!--pour eviter d'avoir un blanc disgracieux en 1ère ligne alors que pers_prenom est vide-->
									<xsl:if test="PERS_PRENOM!=''" >
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="PERS_NOM"/>
									<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
								</xsl:for-each>
								<xsl:if test="position()!=last()">
									<xsl:choose>
										<xsl:when test="$condition_role">
											<xsl:text>&#160;-&#160;</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>, </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='media' and $row[name()=$field]!=''">
							<xsl:variable name="media" select="$row[name()=$field]"/>
							<xsl:choose>
								<xsl:when test="$media='V'">&lt;?print utf8_decode(kVideo);?&gt;
</xsl:when>
								<xsl:when test="$media='P'">&lt;?print utf8_decode(kImage);?&gt;
</xsl:when>
								<xsl:when test="$media='A'">&lt;?print utf8_decode(kAudio);?&gt;
</xsl:when>
								<xsl:when test="$media='D'">&lt;?print utf8_decode(kDocument);?&gt;
</xsl:when>
								<xsl:when test="$media='R'">&lt;?print utf8_decode(kReportage);?&gt;
</xsl:when>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='count_chidren' and $row[name()=$field] != ''">
							<xsl:value-of select="count($row[name()=$field]/*)"/>
						</xsl:when>
						<xsl:when test="type='FEST' and $row[name()='t_doc_fest']!=''">
						<xsl:for-each select=" $row[name()='t_doc_fest']/FEST">
							<xsl:value-of select="*[name()=$field]"/>
							<xsl:if test="position()!=last()">, </xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="type='CAT' and $row[name()='t_doc_cat']/t_categorie/CAT_ID_TYPE_CAT!=''">
						<xsl:for-each select="$row[name()='DOC_CAT']/XML/CAT">
							<xsl:if test="CAT_ID_TYPE_CAT=$field">
								<xsl:value-of select="CAT_NOM"/>
								<xsl:if test="position()!=last()">, </xsl:if>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
						<xsl:when test="type='DOC_VAL' and $row[name()='DOC_VAL']/XML/*[name()=$field]!=''">
							<xsl:for-each select="$row[name()='DOC_VAL']/XML/*[name()=$field]">
								<xsl:value-of select="."/>
								<xsl:if test="position()!=last()">, </xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='VAL' and $row[name()='t_doc_val']/t_val[ID_TYPE_VAL=$field]!=''">
							<xsl:for-each select=" $row[name()='t_doc_val']//t_val[ID_TYPE_VAL=$field]">
								<xsl:value-of select="VALEUR"/>
								<xsl:if test="position()!=last()">, </xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='DOC_XML' and $row[name()='DOC_XML']/XML/DOC/*[name()=$field]!=''">
								<xsl:value-of select="$row[name()='DOC_XML']/XML/DOC/*[name()=$field]"/>
						</xsl:when>
						<xsl:when test="type='MAT'">
							<xsl:for-each select="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()=$field]">
								<xsl:value-of select="."/>
								<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='IMG_DIM' and $row[name()='DOC_ID_MEDIA'] = 'P'">
							<xsl:if test="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_WIDTH']!='' and $row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_HEIGHT']!='' "><xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_WIDTH']"/>&#160;x&#160;<xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT[.//MAT_TYPE='MASTER']//*[name()='MAT_HEIGHT']"/>px</xsl:if>
						</xsl:when>
						<xsl:when test="$entity = 't_mat' and type='MAT_INFO' ">
							<xsl:for-each select="$row[name()='MAT_INFO']//*[name()=$field]">
								<xsl:value-of select="."/>
								<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="$entity = 't_mat' and starts-with($field,'MT_')">
							<xsl:for-each select="$row[name()='tracks']/t_mat_track[*[name()=$field]!='']">
								<xsl:value-of select="child::*[name()=$field]"/><xsl:if test="MT_INDEX != ''">[<xsl:value-of select="MT_INDEX"/>]</xsl:if>
								<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='booleen' and $field='DOC_ACCES'">
							<xsl:choose>
	                            <xsl:when test="$row[name()=$field]=1">&lt;? print kEnLigne;?&gt;<xsl:text>
</xsl:text></xsl:when>
	                            <xsl:otherwise>&lt;? print kAccesInterdit;?&gt;<xsl:text>
</xsl:text></xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='DOC_CAT' and $field='THM'">
							<xsl:value-of select="$row[name()='DOC_CAT']/XML/CAT[@TYPE='THM']/CAT_NOM"/>
						</xsl:when>
						<xsl:otherwise>
								<xsl:call-template name="replace">
									<xsl:with-param name="stream" select="normalize-space($row[name()=$field])"/>
									<xsl:with-param name="old"><xsl:text>
	</xsl:text></xsl:with-param>
									<xsl:with-param name="new"><xsl:text>\n</xsl:text></xsl:with-param>
								</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:text>
</xsl:text>

			<!-- </xsl:if> -->

		</xsl:for-each>
	<xsl:text>



</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template name="displayRow">
	<xsl:param name="xmllist"/>
	<xsl:param name="row"/>
	<xsl:param name="j"/>
	<xsl:param name="div"/>
	<xsl:param name="fromOffset"/>

	<xsl:variable name="rang">
		<xsl:choose>
			<xsl:when test="not(not($fromOffset)) and $fromOffset != ''">
				<xsl:value-of select="number(number($fromOffset)+number($offset)+$j)"/>
			</xsl:when>
			<xsl:when test="$offset and $offset!=''">
				<xsl:value-of select="number(number($offset)+$j)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="number($j)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="id_priv" select="ID_PRIV"/>

	<xsl:variable name="chemin_story">
		&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,storyboardChemin); ?&gt;
	</xsl:variable>
	<xsl:variable name="doc_story">
		&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,kDocumentUrl); ?&gt;
	</xsl:variable>

	<xsl:variable name="vignette">
		<xsl:choose>
			<xsl:when test="string-length(VIGNETTE)>0"><xsl:value-of select="VIGNETTE" /></xsl:when>
			<xsl:when test="string-length(IM_FICHIER)>3"><xsl:value-of select="$chemin_story" />&lt;? echo urlencode('<xsl:call-template name="replace"><xsl:with-param name="stream" select="concat(IM_CHEMIN,'/',IM_FICHIER)"/><xsl:with-param name="old">'</xsl:with-param><xsl:with-param name="new">\'</xsl:with-param></xsl:call-template>');?&gt;</xsl:when>
			<xsl:when test="string-length(DA_FICHIER)>3"><xsl:value-of select="$doc_story" />&lt;? echo urlencode('<xsl:call-template name="replace"><xsl:with-param name="stream" select="concat(DA_CHEMIN,DA_FICHIER)"/><xsl:with-param name="old">'</xsl:with-param><xsl:with-param name="new">\'</xsl:with-param></xsl:call-template>');?&gt;</xsl:when>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="tr">
		<xsl:choose>
			<xsl:when test="$div='1'">div</xsl:when>
			<xsl:otherwise>tr</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="td">
		<xsl:choose>
			<xsl:when test="$div='1'">div</xsl:when>
			<xsl:otherwise>td</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="class">
		<xsl:choose>
			<xsl:when test="$div='1'">resultsCorps_div</xsl:when>
			<xsl:otherwise>resultsCorps</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:element name="{$tr}">
		<!--xsl:attribute name="class"><xsl:value-of select="concat('altern',$j mod 2)"/></xsl:attribute-->
		<xsl:for-each select="$xmllist/list/element">
			<xsl:variable name="field" select="chFields"/>
			<xsl:variable name="id" select="link_id"/>
			<xsl:variable name="hidden_field" select="hidden_field"/>
			<xsl:variable name="val" select="$row[name()=$field]"/>
			<xsl:if test="not(profil_min!='') or (profil_min!='' and $profil &gt;= profil_min)">
				<xsl:element name="{$td}">
					<xsl:attribute name="class"><xsl:value-of select="$class"/><xsl:if test="dragable='1'"> bloc_image</xsl:if></xsl:attribute>
					<xsl:attribute name="style">
						<xsl:if test="width"><xsl:value-of select="concat('width:',width,'px;')"/></xsl:if>
						<xsl:if test="height"><xsl:value-of select="concat('height:',height,'px;')"/></xsl:if>
					</xsl:attribute>

					<xsl:choose>
						<xsl:when test="id_priv_min!='' and $id_priv &lt; id_priv_min">
							&#160;
						</xsl:when>
						<xsl:when test="type='DOC_LEX' and $row[name()='DOC_LEX']/XML/LEX[@TYPE=$field]!=''">
									<xsl:for-each select="$row[name()='DOC_LEX']/XML/LEX[@TYPE=$field]/TERME">
										<xsl:value-of select="PERS_NOM"/>
										<xsl:if test="position()!=last()">, </xsl:if>
									</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='DOC_VAL' and $row[name()='DOC_VAL']/XML/*[name()=$field]!=''">
									<xsl:for-each select="$row[name()='DOC_VAL']/XML/*[name()=$field]">
										<xsl:value-of select="."/>
										<xsl:if test="position()!=last()">, </xsl:if>
									</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='DOC_CAT' and $row[name()='DOC_CAT']/XML/CAT[CAT_ID_TYPE_CAT=$field]/CAT_NOM!=''">
							<xsl:for-each select="$row[name()='DOC_CAT']/XML/CAT[CAT_ID_TYPE_CAT=$field]/CAT_NOM">
								<xsl:value-of select="."/>
								<xsl:if test="position()!=last()">, </xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='DOC_XML' and $row[name()='DOC_XML']/XML/DOC/*[name()=$field]!=''">
								<xsl:value-of select="$row[name()='DOC_XML']/XML/DOC/*[name()=$field]"/>
						</xsl:when>
						<xsl:when test="type='link' and $field!='' and link_popup!=''">
							<a class="{class}" href="#{$row[name()=$id]}" onclick="javascript:{link_popup}('{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}','main','{$row[name()=$id]}')"><xsl:value-of select="$row[name()=$field]"/></a>
						</xsl:when>
						<xsl:when test="type='link' and img!='' and link_popup!=''">
							<a class="{class}" href="javascript:{link_popup}('{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}','main','{$row[name()=$id]}')"><img src="{img}" border="0"/></a>
						</xsl:when>
						<xsl:when test="type='vignette' and link_popup!='' and dragable='1' and $vignette!=''">
							<div class="conteneur_dragable" >
								<xsl:attribute name="id">
									<xsl:choose>
										<xsl:when test="$xmllist/list/entity = 't_panier_doc'" ><xsl:value-of select="concat('pdoc_',$row[name()='ID_LIGNE_PANIER'])" /></xsl:when>
										<xsl:otherwise><xsl:value-of select="concat('doc_',$row[name()='ID_DOC'])" /></xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>

								<a href="javascript:{link_popup}('{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}','main','{$row[name()=$id]}')" style="font-size:0px;">
									<img src="makeVignette.php?image={$vignette}&amp;amp;h=78&amp;amp;w=104&amp;amp;kr=1" title="{$row[name()='DOC_TITRE']}" alt="vignette" />
								</a>
							</div>
							<script type='text/javascript'>
								<xsl:choose>
									<xsl:when test="$xmllist/list/entity = 't_panier_doc'" >elements[<xsl:value-of select="$j"/>]="<xsl:value-of select="concat('pdoc_',$row[name()='ID_LIGNE_PANIER'])"/>";</xsl:when>
									<xsl:otherwise>elements[<xsl:value-of select="$j"/>]="<xsl:value-of select="concat('doc_',$row[name()='ID_DOC'])"/>";</xsl:otherwise>
								</xsl:choose>
							</script>
						</xsl:when>
						<xsl:when test="type='vignette' and dragable='1' and $vignette!=''">
							<div class="conteneur_dragable">
								<xsl:attribute name="id">
									<xsl:choose>
										<xsl:when test="$xmllist/list/entity = 't_panier_doc'" ><xsl:value-of select="concat('pdoc_',$row[name()='ID_LIGNE_PANIER'])" /></xsl:when>
										<xsl:otherwise><xsl:value-of select="concat('doc_',$row[name()='ID_DOC'])" /></xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<xsl:variable name="lien_vers_doc">
									<xsl:choose>
										<xsl:when test="$row[name()='ID_PANIER']!=''">
											<xsl:value-of select="link_url" /><xsl:value-of select="$row[name()='ID_DOC']" />&amp;rang=<xsl:value-of select="$rang" />&amp;id_panier=<xsl:value-of select="$row[name()='ID_PANIER']" />
										</xsl:when>
										<xsl:otherwise test="$row[name()='ID_PANIER']=''">
											<xsl:value-of select="link_url" /><xsl:value-of select="$row[name()='ID_DOC']" />&amp;rang=<xsl:value-of select="$rang" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<a href="{$lien_vers_doc}" style="font-size:0px;">
									<img src="makeVignette.php?image={$vignette}&amp;amp;h=78&amp;amp;w=142&amp;amp;kr=1&amp;amp;h=80" title="{$row[name()='DOC_TITRE']}" alt="vignette" />
								</a>
							</div>
							<script type='text/javascript'>
								<xsl:choose>
									<xsl:when test="$xmllist/list/entity = 't_panier_doc'" >elements.push("<xsl:value-of select="concat('pdoc_',$row[name()='ID_LIGNE_PANIER'])"/>"); // <xsl:value-of select="$j"/></xsl:when>
									<xsl:otherwise>elements[<xsl:value-of select="$j"/>]="<xsl:value-of select="concat('doc_',$row[name()='ID_DOC'])"/>";</xsl:otherwise>
								</xsl:choose>
							</script>
						</xsl:when>
						<xsl:when test="type='vignette' and link_popup!=''">
							<xsl:if test="$vignette!=''">
							<a href="javascript:{link_popup}('{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}','main','{$row[name()=$id]}')"><img src="makeVignette.php?image={$vignette}&amp;amp;h=90" border="0" alt="vignette" /></a>
							</xsl:if>&#160;
						</xsl:when>
						<xsl:when test="type='player' and link_popup!=''">
							<xsl:choose>
							<xsl:when test="$row[name()=$field]!='0'">
							<a href="javascript:{link_popup}('{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}','main','{$row[name()=$id]}')"><img src="{img}" border="0" alt="player"/></a>
							</xsl:when>
							<xsl:otherwise>&#160;</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='link' and $field!=''">
							<a class="{class}" href="{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}"><xsl:value-of select="$row[name()=$field]"/></a>
						</xsl:when>
						<xsl:when test="type='link_lang' and $field!=''">
							<a class="{class}" href="{concat(link_url,$row[name()=$id],'&amp;id_lang=',$row[name()='ID_LANG'])}"><xsl:value-of select="$row[name()=$field]"/></a>
						</xsl:when>
						<xsl:when test="type='link' and img!=''">
							<a class="{class}" href="{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}">
								<img src="{img}" border="0">
									<xsl:attribute name="style"><xsl:if test="height_img!=''">height:<xsl:value-of select="height_img"/>px;</xsl:if></xsl:attribute>
									<xsl:attribute name="title"><xsl:if test="title!=''">&lt;? echo <xsl:value-of select="title"/>?&gt;</xsl:if></xsl:attribute>
								</img></a>
						</xsl:when>
						<xsl:when test="type='menu_download' and img!=''">
							<a class="{class}" onclick="displayMenuActions('download','doc',{$row[name()='ID_DOC']});">
								<img src="{img}" border="0">
									<xsl:attribute name="style"><xsl:if test="height_img!=''">height:<xsl:value-of select="height_img"/>px;</xsl:if></xsl:attribute>
									<xsl:attribute name="title"><xsl:if test="title!=''">&lt;? echo <xsl:value-of select="title"/>?&gt;</xsl:if></xsl:attribute>
								</img></a>
						</xsl:when>
						<xsl:when test="type='link_lang' and img!=''">
							<a class="{class}" href="{concat(link_url,$row[name()=$id],'&amp;id_lang=',$row[name()='ID_LANG'])}"><img src="{img}" border="0"/></a>
						</xsl:when>
				        <xsl:when test="type='link'">
				          <a class="{class}" href="{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}"><xsl:value-of select="label" /></a>
				        </xsl:when>
						<xsl:when test="type='vignette' and $row[name()='VIGNETTE']!=''">
							<xsl:attribute name="align">center</xsl:attribute>
								<xsl:variable name="vignette_width">
									<xsl:choose>
										<xsl:when test="width"><xsl:value-of select="width"/></xsl:when>
										<xsl:otherwise>120</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="$row[name()='VIGNETTE']!='' and $row[name()='VIGNETTE']!='&#160;'">
										<a href="{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}"><img style="max-width:100%;max-height:100%;" src="makeVignette.php?image={$row[name()='VIGNETTE']}&amp;amp;w={$vignette_width}" border="0" alt="vignette" /></a>
									</xsl:when>
									<xsl:otherwise>
										<a href="{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}"><img style="max-width:100%;max-height:100%;" src="design/images/nopicture.gif" border="0" alt="vignette" /></a>
									</xsl:otherwise>
								</xsl:choose>
							&#160;
						</xsl:when>
						<xsl:when test="type='vignette'">
							<xsl:attribute name="align">center</xsl:attribute>
							<xsl:if test="$vignette!=''">
							<a href="{concat(link_url,$row[name()=$id],'&amp;rang=',$rang)}"><img src="makeVignette.php?image={$vignette}&amp;amp;w=120" border="0" alt="vignette" /></a>
							</xsl:if>&#160;
						</xsl:when>
						<xsl:when test="type='player'">
							<xsl:choose>
								<xsl:when test="$row[name()=$field]!='0'">
									<xsl:variable name="lien_vers_doc">
										<xsl:choose>
											<xsl:when test="$row[name()='ID_PANIER']!=''">
												<xsl:value-of select="link_url" /><xsl:value-of select="$row[name()='ID_DOC']" />&amp;rang=<xsl:value-of select="$rang" />&amp;id_panier=<xsl:value-of select="$row[name()='ID_PANIER']" />
											</xsl:when>
											<xsl:otherwise test="$row[name()='ID_PANIER']=''">
												<xsl:value-of select="link_url" /><xsl:value-of select="$row[name()='ID_DOC']" />&amp;rang=<xsl:value-of select="$rang" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<a href="{$lien_vers_doc}"><img style="height:13px;" src="{img}" border="0" alt="player">
									<xsl:attribute name="title"><xsl:if test="title">&lt;? echo <xsl:value-of select="title"/>;?&gt;</xsl:if></xsl:attribute>
									</img></a>
								</xsl:when>
							<xsl:otherwise>&#160;</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='checkbox'">
							<input type="checkbox" name="checkbox{$row[name()=$id]}" value="{$row[name()=$id]}"  >
							<xsl:attribute name="title"><xsl:if test="title">&lt;? echo <xsl:value-of select="title"/>;?&gt;</xsl:if></xsl:attribute>
							</input>
						</xsl:when>
						<xsl:when test="type='img' and onclick!=''">
							<img src="{img}" border="0" onclick="{concat(onclick,'(',$row[name()=$id],')')}" id="{concat('img_',$row[name()=$id])}"/>
						</xsl:when>
						<xsl:when test="type='date'">
							<xsl:call-template name="format_date"><xsl:with-param name="chaine_date" select="$row[name()=$field]"/></xsl:call-template>
						</xsl:when>
						<xsl:when test="type='hidden'">
							<input type="hidden" name="{name}" value="{$row[name()=$field]}"  />
						</xsl:when>
						<xsl:when test="type='boolean'">
							<xsl:choose>
                                <xsl:when test="$row[name()=$field]='1'"><xsl:processing-instruction name="php">print kOui;</xsl:processing-instruction></xsl:when>
                                <xsl:otherwise><xsl:processing-instruction name="php">print kNon;</xsl:processing-instruction></xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='select' and chFields='PDOC_ID_ETAT'">
							<select name="{name}">
							<xsl:if test="onchange!=''"><xsl:attribute name="onchange"><xsl:value-of select="onchange"/></xsl:attribute></xsl:if>
			&lt;?= listOptions("<xsl:value-of select='select_sql'/>",array("ID","VAL"),"<xsl:value-of select='$val'/>") ?&gt;
							</select>
						</xsl:when>
						<xsl:when test="chFields='EXT_DUREE'">
							<xsl:variable name="extrait"><xsl:value-of select="$row[name()='PDOC_EXTRAIT']"/></xsl:variable>
							<xsl:choose>
							<xsl:when test="$extrait='1'"><xsl:value-of select="$row[name()='EXT_DUREE']"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="$row[name()='DOC_DUREE']"/></xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='concat' and test_field!=''">
							<xsl:variable name="test_field"><xsl:value-of select="test_field"/></xsl:variable>
							<xsl:variable name="flag"><xsl:value-of select="$row[name()=$test_field]"/></xsl:variable>
							<xsl:if test="$flag='1'">
								<xsl:for-each select="str:tokenize($field, ',')">
									<xsl:variable name="f"><xsl:value-of select="."/></xsl:variable>
									<xsl:if test="normalize-space($row[name()=$f])!=''"><xsl:value-of select="$row[name()=$f]"/><br/></xsl:if>
								</xsl:for-each>
							</xsl:if>&#160;
						</xsl:when>
						<xsl:when test="type='concat'">
							<xsl:for-each select="str:tokenize($field, ',')">
								<xsl:variable name="f"><xsl:value-of select="."/></xsl:variable>
								<xsl:if test="normalize-space($row[name()=$f])!=''"><xsl:value-of select="$row[name()=$f]"/><br/></xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="type='delete'">
							<a href="javascript:removeLine({$row[name()=$id]})"><img src="{img}" border="0"/></a>
						</xsl:when>
						<xsl:when test="type='media' and $row[name()=$field]!=''">
							<xsl:variable name="media" select="$row[name()=$field]"/>
							<xsl:choose>
								<xsl:when test="$media='V'">
									<xsl:processing-instruction name="php">print kVideo;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$media='P'">
									<xsl:processing-instruction name="php">print kImage;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$media='A'">
									<xsl:processing-instruction name="php">print kAudio;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$media='D'">
									<xsl:processing-instruction name="php">print kDocument;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$media='R'">
									<xsl:processing-instruction name="php">print kReportage;</xsl:processing-instruction>
								</xsl:when>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='simple_info_media' ">
							<xsl:variable name="array_mat_type">
								<xsl:call-template name="tokenize">
									<xsl:with-param name="pText" select="$row[name()='MAT_TYPE']"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="$row[name()='PDOC_EXTRAIT'] and $row[name()='PDOC_EXTRAIT']='1' and ($row[name()='DOC_ID_MEDIA']='V' or $row[name()='DOC_ID_MEDIA']='A') and $row[name()='EXT_DUREE']!=''">
									<xsl:value-of select="$row[name()='EXT_DUREE']"/>
								</xsl:when>
								<xsl:when test="($row[name()='DOC_ID_MEDIA']='V' or $row[name()='DOC_ID_MEDIA']='A') and $row[name()='DOC_DUREE']!=''">
									<xsl:value-of select="$row[name()='DOC_DUREE']"/>
								</xsl:when>
								<xsl:when test="$row[name()='DOC_ID_MEDIA']='P'  ">
									<xsl:variable name="array_mat_width">
										<xsl:call-template name="tokenize">
											<xsl:with-param name="pText" select="$row[name()='MAT_WIDTH']"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="array_mat_height">
										<xsl:call-template name="tokenize">
											<xsl:with-param name="pText" select="$row[name()='MAT_HEIGHT']"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:choose>
										<!-- On essai de prendre les dimensions du master -->
										<xsl:when test="contains($row[name()='MAT_TYPE'],'MASTER')">
											<xsl:for-each select="exsl:node-set($array_mat_type)/tag">
												<xsl:variable name="idx"><xsl:number/></xsl:variable>
												<xsl:if test=".='MASTER'">
													<!-- MS 01.06.15 on ne prends en compte que les dimensions du premier mat master trouvé -->
													<xsl:variable name="control"><xsl:number/></xsl:variable>
													<xsl:if test="$control &lt; 2">
														<xsl:value-of select="exsl:node-set($array_mat_width)/tag[position()=$idx]"/><xsl:text> x </xsl:text><xsl:value-of select="exsl:node-set($array_mat_height)/tag[position()=$idx]"/>
													</xsl:if>
												</xsl:if>
											</xsl:for-each>&#160;
										</xsl:when>
										<!-- Si pas de master défini, alors on prends les premières dimensions non nullse que l'on trouve -->
										<xsl:when test="exsl:node-set($array_mat_width)/tag!='' and exsl:node-set($array_mat_height)/tag!='' ">
											<xsl:value-of select="exsl:node-set($array_mat_width)/tag"/><xsl:text> x </xsl:text><xsl:value-of select="exsl:node-set($array_mat_height)/tag"/>
										</xsl:when>
										<xsl:otherwise></xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="$row[name()='DOC_ID_MEDIA']='D'  ">
									<xsl:variable name="array_nb_pages">
										<xsl:call-template name="tokenize">
											<xsl:with-param name="pText" select="$row[name()='MAT_NB_PAGES']"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:for-each select="exsl:node-set($array_mat_type)/tag">
										<xsl:variable name="idx"><xsl:number/></xsl:variable>
										<xsl:if test=".='MASTER'">
											<xsl:value-of select="exsl:node-set($array_nb_pages)/tag[position()=$idx]"/><xsl:text> </xsl:text><xsl:processing-instruction name="php">print kPages</xsl:processing-instruction>
										</xsl:if>
									</xsl:for-each>&#160;
								</xsl:when>
								<xsl:when test="$row[name()='DOC_ID_MEDIA']='R' and $row[name()='CNT_DOC_LIES_DST']!=''  ">
									<xsl:value-of select="$row[name()='CNT_DOC_LIES_DST']"/>&#160;<xsl:processing-instruction name="php">print kDocument_s</xsl:processing-instruction>
								</xsl:when>
								<xsl:otherwise>&#160;</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="type='etat_job' and etat_id_field != '' and etat_field!='' and prog_field!='' and id_field!=''">
							<xsl:variable name="id_field" select="id_field"/>
							<xsl:variable name="etat_field" select="etat_field"/>
							<xsl:variable name="etat_id_field" select="etat_id_field"/>
							<xsl:variable name="prog_field" select="prog_field"/>
							<xsl:variable name="jobEnCours">2</xsl:variable>
							<span id="etat{$row[name()='IMP_DOC_ID_JOB']}">
								<xsl:value-of select="$row[name()=prog_field]"/>
								<xsl:choose>
									<xsl:when test="$row[name()=$etat_id_field]=$jobEnCours">
										<div id="progressbar{$row[name()=$id_field]}" class="progressBar" style="width:50px;height:16px">
											<div  class="progress-label" style="margin-left: 15%;float: left;" ><xsl:value-of select="$row[name()=$prog_field]"/>%</div>
										</div>
										<script>
											$j(function() {
												$j( "#progressbar<xsl:value-of select="$row[name()=$id_field]"/>" ).progressbar({value: <xsl:value-of select="$row[name()=$prog_field]"/>});
											});
										</script>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$row[name()=$etat_field]"/>
									</xsl:otherwise>
								</xsl:choose>
								</span>
								<script>
										setTimeout(updateProgressJob(<xsl:value-of select="$row[name()='IMP_DOC_ID_JOB']" />,'',importUpdateVignette),2000);
								</script>
							<!--xsl:if test="$row[name()=$etat_id_field]!=$jobEnCours">
								<xsl:value-of select="$row[name()=$etat_field]"/>
							</xsl:if-->
						</xsl:when>
                        <xsl:when test="type='PAN_XML' and $row[name()='PAN_XML']//*[name()=$field]!=''">
                            <xsl:value-of select="$row[name()='PAN_XML']//*[name()=$field]"/>
                        </xsl:when>
						<xsl:when test="substring($field,1,2)='L_' or substring($field,1,2)='P_' or substring($field,1,2)='V_'">
							<xsl:call-template name="replace">
								<xsl:with-param name="stream" select="$row[name()=$field]"/>
								<xsl:with-param name="old"><xsl:text>,</xsl:text></xsl:with-param>
								<xsl:with-param name="new"><xsl:text>, </xsl:text></xsl:with-param>
							</xsl:call-template>
						</xsl:when>
                        <xsl:otherwise><xsl:value-of select="$row[name()=$field]"/></xsl:otherwise>
					</xsl:choose>
					<xsl:if test="hidden_field">
						<input type="hidden" name="{hidden_name}" value="{$row[name()=$hidden_field]}"  />
					</xsl:if>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:element>
	</xsl:template>


	<!-- FACETTES -->
	<!-- génération des facettes généralistes -->
	<xsl:template name="display_facet">
		<xsl:param name="field_facet"/>
		<xsl:param name="action"/>
		<xsl:param name="label_facet"/>
		<xsl:param name="root"/>

		<xsl:for-each select="$root/facet[field=$field_facet]">
			<xsl:sort select="valeur" data-type="number" order="descending" />
			<xsl:variable name="data_key">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="key" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="escaped_label_facet">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="$label_facet" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="position()&lt;=10">
				<!--div class="facet" onclick="addFacetSearch('{field}','{$data_key}','{$action}')"><xsl:value-of select="key" /> <span class="count"><xsl:value-of select="valeur" /></span></div-->
				<div class="facet" onclick="filterQueryByFacet('{field}','{$data_key}','{$escaped_label_facet}')"><span class="value"><xsl:value-of select="key" /></span><span class="count"><xsl:value-of select="valeur" /></span></div>
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="count($root/facet[field=$field_facet])&gt;10">
			<div style="display:none;">
				<xsl:for-each select="$root/facet[field=$field_facet]">
					<xsl:sort select="valeur" data-type="number" order="descending" />
					<xsl:variable name="data_key">
						<xsl:call-template name="internal-quote-replace">
							<xsl:with-param name="stream" select="key" />
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="escaped_label_facet">
						<xsl:call-template name="internal-quote-replace">
							<xsl:with-param name="stream" select="$label_facet" />
						</xsl:call-template>
					</xsl:variable>
					<xsl:if test="position()&gt;10">
						<!--div class="facet" onclick="addFacetSearch('{field}','{$data_key}','{$action}')"><xsl:value-of select="key" /> <span class="count"><xsl:value-of select="valeur" /></span></div-->
						<div class="facet" onclick="filterQueryByFacet('{field}','{$data_key}','{$escaped_label_facet}')"><span class="value"><xsl:value-of select="key" /></span> <span class="count"><xsl:value-of select="valeur" /></span></div>
					</xsl:if>
				</xsl:for-each>
			</div>
			<div class="facet more_facet" onclick="displayFacettes(this);"><xsl:processing-instruction name="php">print kAfficherTout</xsl:processing-instruction></div>
		</xsl:if>
	</xsl:template>

	<xsl:template name="display_facet_bool">
		<xsl:param name="field_facet"/>
		<xsl:param name="action"/>
		<xsl:param name="label_facet"/>
		<xsl:param name="root"/>

		<xsl:for-each select="$root/facet[field=$field_facet][key='1']">
			<xsl:sort select="valeur" data-type="number" order="descending" />
			<xsl:variable name="data_key">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="key" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="escaped_label_facet">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="$label_facet" />
				</xsl:call-template>
			</xsl:variable>
			<div class="facet" onclick="filterQueryByFacet('{field}','{$data_key}','{$escaped_label_facet}')"><span class="value"><xsl:value-of select="key" /></span> <span class="count"><xsl:value-of select="valeur" /></span></div>
		</xsl:for-each>
	</xsl:template>

	<!-- génération des facettes sur le champ type de média -->
	<xsl:template name="display_facet_media">
		<xsl:param name="field_facet"/>
		<xsl:param name="root"/>
		<xsl:param name="label_facet"/>

		<xsl:for-each select="$root/facet[field=$field_facet]">
			<xsl:sort select="valeur" data-type="number" order="descending" />
			<xsl:variable name="data_key">
				<xsl:value-of select="translate(key,'vapd','VAPD')" />
			</xsl:variable>
			<xsl:variable name="escaped_label_facet">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="$label_facet" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="label_key">
				<xsl:if test="$data_key='V'">&lt;?php print kVideo; ?&gt;</xsl:if>
				<xsl:if test="$data_key='A'">&lt;?php print kAudio; ?&gt;</xsl:if>
				<xsl:if test="$data_key='P'">&lt;?php print kImage; ?&gt;</xsl:if>
				<xsl:if test="$data_key='D'">&lt;?php print kDocument; ?&gt;</xsl:if>
				<xsl:if test="$data_key='R'">&lt;?php print kReportage; ?&gt;</xsl:if>
			</xsl:variable>
			<!--div class="facet" onclick="addFacetSearch('{field}','{$data_key}','')"><xsl:value-of select="$label_key" /> <span class="count"><xsl:value-of select="valeur" /></span></div-->
			<div class="facet" onclick="filterQueryByFacet('{field}','{$data_key}','{concat($escaped_label_facet,' : ',$label_key)}')"><span class="value"><xsl:value-of select="$label_key" /></span> <span class="count"><xsl:value-of select="valeur" /></span></div>
		</xsl:for-each>
	</xsl:template>

	<!-- génération des facettes sur les champs dates -->
    <xsl:template name="display_facet_date">
		<xsl:param name="field_facet"/>
		<xsl:param name="label_facet"/>
		<xsl:param name="action"/>
		<xsl:param name="root"/>
		<xsl:for-each select="$root/facet[field=$field_facet]">
			<!--xsl:sort select="valeur" data-type="number" order="descending" /-->
			<xsl:variable name="data_key">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="key" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="data_key2">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="key2" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="data_vals">
				<xsl:choose>
					<xsl:when test="substring-before($data_key,'-') = substring-before($data_key2,'-')"><xsl:value-of select="substring-before($data_key,'-')" /></xsl:when>
					<xsl:otherwise><xsl:value-of select="substring-before($data_key,'-')" /><xsl:text> - </xsl:text><xsl:value-of select="substring-before($data_key2,'-')" /></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="escaped_label_facet">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="$label_facet" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="position()&lt;=10">
				<!--div class="facet" onclick="addFacetIntDateSearch('{field}','{substring-before(key,'-')}','{substring-before(key2,'-')}','')"><xsl:value-of select="substring-before(key,'-')" /> <span class="count"><xsl:value-of select="valeur" /></span></div-->
				<div class="facet" onclick="filterQueryByFacet('{field}','[{$data_key} TO {$data_key2}]','{$escaped_label_facet}','{substring-before($data_key,'-')} - {substring-before($data_key2,'-')}')"><span class="value"><xsl:value-of select="$data_vals" /></span> <span class="count"><xsl:value-of select="valeur" /></span></div>
			</xsl:if>
		</xsl:for-each>
		<xsl:if test="count($root/facet[field=$field_facet])&gt;10">
			<div style="display:none;">
				<xsl:for-each select="$root/facet[field=$field_facet]">
					<!--xsl:sort select="valeur" data-type="number" order="descending" /-->
					<xsl:variable name="data_key">
						<xsl:call-template name="internal-quote-replace">
							<xsl:with-param name="stream" select="key" />
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="data_key2">
						<xsl:call-template name="internal-quote-replace">
							<xsl:with-param name="stream" select="key2" />
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="data_vals">
						<xsl:choose>
							<xsl:when test="substring-before($data_key,'-') = substring-before($data_key2,'-')"><xsl:value-of select="substring-before($data_key,'-')" /></xsl:when>
							<xsl:otherwise><xsl:value-of select="substring-before($data_key,'-')" /><xsl:text> - </xsl:text><xsl:value-of select="substring-before($data_key2,'-')" /></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="escaped_label_facet">
						<xsl:call-template name="internal-quote-replace">
							<xsl:with-param name="stream" select="$label_facet" />
						</xsl:call-template>
					</xsl:variable>
					<xsl:if test="position()&gt;10">
                        <!--div class="facet" onclick="addFacetIntDateSearch('{field}','{substring-before(key,'-')}','{substring-before(key2,'-')}','')"><xsl:value-of select="substring-before(key,'-')" /> <span class="count"><xsl:value-of select="valeur" /></span></div-->
						<div class="facet" onclick="filterQueryByFacet('{field}','[{$data_key} TO {$data_key2}]','{$escaped_label_facet}')"><span class="value"><xsl:value-of select="$data_vals" /></span> <span class="count"><xsl:value-of select="valeur" /></span></div>
					</xsl:if>
				</xsl:for-each>
			</div>
			<div class="facet more_facet" onclick="displayFacettes(this);"><xsl:processing-instruction name="php">print kAfficherTout</xsl:processing-instruction></div>
		</xsl:if>
	</xsl:template>

     <!-- génération des facettes pour terme hierarchique, sous forme d'arbre-->
    <xsl:template name="display_facet_hier_tree">
        <xsl:param name="field_facet"/>
        <xsl:param name="action"/>
        <xsl:param name="prefix"/>
        <xsl:param name="label_facet"/>
        <xsl:param name="root"/>
        <xsl:param name="subset"/>
        <xsl:param name="include_sub_level"/>
		<xsl:param name="sort"/>
		
		<xsl:variable name="num_prefix">
			<xsl:choose>
				<xsl:when test="contains($prefix,'/')">
					<xsl:value-of select="substring-before($prefix,'/')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$prefix"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="next_prefix">
			<xsl:value-of select="number(number($num_prefix)+1)" />
		</xsl:variable>
		<xsl:variable name="tree_hier">
			<xsl:choose>
				<xsl:when test="contains($prefix,'/')">
					<xsl:value-of select="concat('/',substring-after($prefix,'/'),'/')"/>
				</xsl:when>
				<xsl:otherwise>/</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="escaped_label_facet">
			<xsl:call-template name="internal-quote-replace">
				<xsl:with-param name="stream" select="$label_facet" />
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name="sort_by">
			<xsl:choose>
				<xsl:when test="$sort='key'">key</xsl:when>
				<xsl:otherwise>valeur</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="sort_type">
			<xsl:choose>
				<xsl:when test="$sort_by='key'">text</xsl:when>
				<xsl:otherwise>number</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="sort_order">
			<xsl:choose>
				<xsl:when test="$sort_by='key'">ascending</xsl:when>
				<xsl:otherwise>descending</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
        <xsl:for-each select="$subset[name()='facet']">
			<xsl:sort select="*[name() = $sort_by]" data-type="{$sort_type}" order="{$sort_order}" />
			<!--xsl:sort select="valeur" data-type="number" order="descending" /-->

				<xsl:if test="position()=10">
					<xsl:text>&lt;div style="display:none;"&gt;</xsl:text>
				</xsl:if>
				<xsl:variable name="data_key">
					<xsl:call-template name="internal-quote-replace">
						<xsl:with-param name="stream" select="key" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="leaf_hier">
					<xsl:call-template name="replace">
						<xsl:with-param name="stream">
							<xsl:call-template name="replace">
								<xsl:with-param name="stream" select="key"/>
								<xsl:with-param name="old" select="concat($num_prefix,$tree_hier)"/>
								<xsl:with-param name="new"><xsl:text></xsl:text></xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="old">%SLASH%</xsl:with-param>
						<xsl:with-param name="new">/</xsl:with-param>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="escaped_leaf_hier">
					<xsl:call-template name="internal-quote-replace">
						<xsl:with-param name="stream" select="$leaf_hier" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="pre_concat">
					<xsl:value-of select="concat($next_prefix,$tree_hier,$leaf_hier)" />
				</xsl:variable>
				<xsl:variable name="nbKeys">
					<xsl:value-of select="count($root[starts-with(key,$pre_concat)])" />
				</xsl:variable>
				<xsl:variable name="subField">
					<xsl:value-of select="substring-before(field,'-')" />
				</xsl:variable>
					<div onclick="filterQueryByFacet('{$subField}','{$data_key}','{$escaped_label_facet}','{$escaped_leaf_hier}')">
						<xsl:attribute name="class">
							<xsl:text>facet facet_hier </xsl:text>
							<!-- la classe loaded indique que lors d'un dépliage du niveau inférieur, on ne fait pas d'appel ajax (car on a déjà les infos chargées). Cette classe est appliquée si un élément n'a pas de fils mais à un neveu OU si des enfants existent au niveau courant + include_sub_level + 1 -->
							<xsl:if test="($nbKeys = 0 and count($root[starts-with(key,concat($next_prefix,$tree_hier))]) &gt;0) or (count($root[starts-with(key,concat(number($next_prefix+number($include_sub_level)),$tree_hier,$leaf_hier))]) &gt; 0)">
								<xsl:text> loaded</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<span class="count">
							<xsl:value-of select="valeur" />
						</span>
						<span onclick="toggleFoldHierFacet(event,this,'{$subField}','{$data_key}');">
							<xsl:attribute name="class">
								<xsl:text>hier_unfolder</xsl:text>
								<xsl:if test="$nbKeys = 0">
									<xsl:text> hidden </xsl:text>
								</xsl:if>
							</xsl:attribute>
							<xsl:text>&#160;</xsl:text>
						</span>
						<span class="hier_innerText"><xsl:value-of select="$leaf_hier" /></span>&#160;
					</div>
					<xsl:if test="$root[starts-with(key,$pre_concat)]">
						<div class="facet_hier_lvl_{$next_prefix}">
							<xsl:call-template name="display_facet_hier_tree">
								<xsl:with-param name="field_facet" select="$field_facet" />
								<xsl:with-param name="prefix" select="$pre_concat" />
								<xsl:with-param name="subset" select="exsl:node-set($root[starts-with(field,concat($field_facet,'-')) and starts-with(key,$pre_concat)])" />
								<xsl:with-param name="root" select="$root" />
								<xsl:with-param name="label_facet" select="$label_facet" />
								<xsl:with-param name="include_sub_level" select="$include_sub_level" />
								<xsl:with-param name="sort" select="$sort" />
							</xsl:call-template></div>
					</xsl:if>
			<!--/xsl:if-->
        </xsl:for-each>
		<xsl:if test="count($subset[starts-with(field,concat($field_facet,'-')) and starts-with(key,$prefix)])&gt;=10">
			<xsl:text>&lt;/div&gt;</xsl:text>
			<div class="facet more_facet" onclick="displayFacettes(this);">
                <xsl:processing-instruction name="php">print kAfficherTout</xsl:processing-instruction>
            </div>
		</xsl:if>
    </xsl:template>

	<!-- génération des facettes sur les champs hiérarchiques -->
    <xsl:template name="display_facet_hier">
        <xsl:param name="field_facet"/>
        <xsl:param name="action"/>
        <xsl:param name="label_facet"/>
        <xsl:param name="root"/>

        <xsl:for-each select="$root/facet[starts-with(field,concat($field_facet,'-'))]">
            <xsl:sort select="valeur" data-type="number" order="descending" />
            <xsl:variable name="data_key">
                <xsl:call-template name="internal-quote-replace">
                    <xsl:with-param name="stream" select="key" />
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="escaped_label_facet">
                <xsl:call-template name="internal-quote-replace">
                    <xsl:with-param name="stream" select="$label_facet" />
                </xsl:call-template>
            </xsl:variable>
            <xsl:if test="position()&lt;=10">
                <!--div class="facet" onclick="addFacetSearch('{field}','{$data_key}','{$action}')"><xsl:value-of select="key" /> <span class="count"><xsl:value-of select="valeur" /></span></div-->
                <div class="facet" onclick="filterQueryByFacet('{field}','{$data_key}','{$escaped_label_facet}','{substring-after($data_key, '/')}')">
					<span class="value"><xsl:value-of select="substring-after(key, '/')" /></span>
					<span class="count">
                        <xsl:value-of select="valeur" />
                    </span>
                </div>
            </xsl:if>
        </xsl:for-each>
        <xsl:if test="count($root/facet[field=$field_facet])&gt;10">
            <div style="display:none;">
                <xsl:for-each select="$root/facet[field=$field_facet]">
                    <xsl:sort select="valeur" data-type="number" order="descending" />
                    <xsl:variable name="data_key">
                        <xsl:call-template name="internal-quote-replace">
                            <xsl:with-param name="stream" select="key" />
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:variable name="escaped_label_facet">
                        <xsl:call-template name="internal-quote-replace">
                            <xsl:with-param name="stream" select="$label_facet" />
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:if test="position()&gt;10">
                        <!--div class="facet" onclick="addFacetSearch('{field}','{$data_key}','{$action}')"><xsl:value-of select="key" /> <span class="count"><xsl:value-of select="valeur" /></span></div-->
                        <div class="facet" onclick="filterQueryByFacet('{field}','{$data_key}','{$escaped_label_facet}','{substring-after($data_key, '/')}')">
							<span class="value"><xsl:value-of select="substring-after(key, '/')" /></span>
							<span class="count">
                                <xsl:value-of select="valeur" />
                            </span>
                        </div>
                    </xsl:if>
                </xsl:for-each>
            </div>
            <div class="facet more_facet" onclick="displayFacettes(this);">
                <xsl:processing-instruction name="php">print kAfficherTout</xsl:processing-instruction>
            </div>
        </xsl:if>
    </xsl:template>

<!-- OBSOLETE ajoutait un bouton de remove a coté de chaque critere-->
	<xsl:template name="ajoute_btn_remove_facet">
		<xsl:param name="str" />
		<xsl:param name="field" />

		<xsl:if test="contains($str,',')">
			<xsl:variable name="value">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="substring-before($str,',')" />
				</xsl:call-template>
			</xsl:variable>

			<xsl:value-of select="substring-before($str,',')" />&amp;nbsp;<a href="javascript:void(0);" onclick="removeFacetSearchMulti('{$field}','{$value}')"><img src="design/images/close.jpg" /></a>,

			<xsl:call-template name="ajoute_btn_remove_facet">
				<xsl:with-param name="str" select="substring-after($str,',')" />
				<xsl:with-param name="field" select="$field" />
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="contains($str,',')=false">
			<xsl:variable name="value_fin">
				<xsl:call-template name="internal-quote-replace">
					<xsl:with-param name="stream" select="$str" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:value-of select="$str" />&amp;nbsp;<a href="javascript:void(0);" onclick="removeFacetSearchMulti('{$field}','{$value_fin}')"><img src="design/images/close.jpg" /></a>
		</xsl:if>

	</xsl:template>
	
	
	<!-- TEMPLATE AFFICHAGE MENU GAUCHE RESULT LIST/MOS-->
    
	<xsl:template name="rechercheLeftMenu">
		<xsl:param name="solr_facet"/>
		<xsl:variable name="root_facets" select="."/>

		<script type="text/javascript">
			if ($j(window).width()&lt;tablet_border){
				console.log('tablet or smaller');
				console.log('layout : &lt;?=$layout?&gt;');
			}
		</script>
		<div id="menuGauche"><xsl:attribute name="class">&lt;?= ($layout&lt;1 ||  $layout&gt;2)?'collapsed':'';?&gt;</xsl:attribute>
			<div class="title_bar"><span class="filter_label"><xsl:processing-instruction name="php">echo kFiltres;</xsl:processing-instruction></span><span id="arrow_left" class="toggle_arrow" onclick="toggleLeftMenu();">&#160;</span></div>
			<div id="facet_wrapper">
				<xsl:if test="$solr_facet!=''">
					<xsl:for-each select="$solr_facet/facets/facet">
						<xsl:if test="(not(profil_id) or profil_id=$profil) and (not(profil_min) or profil_min&lt;=$profil) and (not(profil_max) or profil_max&gt;=$profil)">
							<xsl:variable name="field_facet" select="./field"/>
							<xsl:choose>
							<xsl:when test="./field='doc_id_media'">
								<xsl:if test="$root_facets/facet[field=$field_facet]/valeur">
									<div class="type_facet {$field_facet}" onclick="toggleTypeFacet(this)"><span class="facet_icon {./class}">&#160;</span><xsl:processing-instruction name="php">echo <xsl:value-of select="./label"/>;</xsl:processing-instruction>  <span class="arrow">&#160;</span></div>
									<div class="block_facet  {$field_facet}">
									<xsl:call-template name="display_facet_media">
										<xsl:with-param name="field_facet" select="'doc_id_media'" />
										<xsl:with-param name="root" select="$root_facets" />
										<xsl:with-param name="label_facet" >&lt;?echo <xsl:value-of select="./label"/>;?&gt;</xsl:with-param>
									</xsl:call-template>
									</div>
								</xsl:if>
							</xsl:when>
							<xsl:when test="contains(./field,'doc_date') and not(./chrono = '1')">
								<xsl:if test="$root_facets/facet[field=$field_facet]/valeur">
									<div class="type_facet {$field_facet}" onclick="toggleTypeFacet(this)"><span class="facet_icon {./class}">&#160;</span><xsl:processing-instruction name="php">echo <xsl:value-of select="./label"/>;</xsl:processing-instruction>  <span class="arrow">&#160;</span></div>
									<div class="block_facet {$field_facet}">
									<xsl:call-template name="display_facet_date">
										<xsl:with-param name="field_facet" select="./field" />
										<xsl:with-param name="label_facet" >&lt;?echo <xsl:value-of select="./label"/>;?&gt;</xsl:with-param>
										<xsl:with-param name="root" select="$root_facets" />
									</xsl:call-template>
									</div>
								</xsl:if>
							</xsl:when>
							<xsl:when test="./type='FACET_BOOL'">
								<xsl:if test="$root_facets/facet[field=$field_facet][key='1']/valeur">

									<xsl:variable name="data_key">
										<xsl:call-template name="internal-quote-replace">
											<xsl:with-param name="stream" select="key" />
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="escaped_label_facet">
										<xsl:call-template name="internal-quote-replace">
											<xsl:with-param name="stream" >&lt;?echo <xsl:value-of select="./label"/>;?&gt;</xsl:with-param>
										</xsl:call-template>
									</xsl:variable>

									<div class="type_facet {$field_facet}" onclick="filterQueryByFacet('{field}','1','{$escaped_label_facet}')">
										<span class="count"><xsl:value-of select="$root_facets/facet[field=$field_facet][key='1']/valeur" /></span>
										<span class="facet_icon {./class}">&#160;</span>
										<xsl:processing-instruction name="php">echo <xsl:value-of select="./label"/>;</xsl:processing-instruction>
									</div>
								</xsl:if>
							</xsl:when>
                            <xsl:when test="./type='FACET_HIER' and displayType!='' and displayType='tree'">
                                <xsl:if test="$root_facets/facet[starts-with(field,concat($field_facet,'-'))]/valeur">
                                    <div class="type_facet {$field_facet}" onclick="toggleTypeFacet(this)" style="{./style}">
                                        <span class="facet_icon {./class}">&#160;</span>
                                        <xsl:processing-instruction name="php">echo <xsl:value-of select="./label"/>;</xsl:processing-instruction>
                                        <span class="arrow">&#160;</span>
                                    </div>
									<xsl:variable name="include_sub_level">
										<xsl:choose>
											<xsl:when test="include_sub_level !=''"><xsl:value-of select="include_sub_level"/></xsl:when>
											<xsl:otherwise>1</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<xsl:variable name="prefix" select="prefix" />
                                    <div class="block_facet {$field_facet}" data-include-sub-level="{$include_sub_level}">
                                        <xsl:call-template name="display_facet_hier_tree">
                                            <xsl:with-param name="field_facet" select="./field" />
											<xsl:with-param name="prefix" select="$prefix" />
											<xsl:with-param name="subset" select="exsl:node-set($root_facets/facet[starts-with(field,concat($field_facet,'-')) and starts-with(key,$prefix)])" />
											<xsl:with-param name="root" select="exsl:node-set($root_facets/facet[starts-with(field,concat($field_facet,'-'))])" />
											<xsl:with-param name="include_sub_level" select="$include_sub_level" />
                                            <xsl:with-param name="label_facet" >&lt;?echo <xsl:value-of select="./label"/>;?&gt;</xsl:with-param>
											<xsl:with-param name="sort" select="./sort" />
										</xsl:call-template>
                                    </div>
                                </xsl:if>
                            </xsl:when>
							<xsl:when test="./type='FACET_HIER'">
                                <xsl:if test="$root_facets/facet[starts-with(field,concat($field_facet,'-'))]/valeur">
                                    <div class="type_facet {$field_facet}" onclick="toggleTypeFacet(this)" style="{./style}">
                                        <span class="facet_icon {./class}">&#160;</span>
                                        <xsl:processing-instruction name="php">echo <xsl:value-of select="./label"/>;</xsl:processing-instruction>
                                        <span class="arrow">&#160;</span>
                                    </div>
                                    <div class="block_facet {$field_facet}">
                                        <xsl:call-template name="display_facet_hier">
                                            <xsl:with-param name="field_facet" select="./field" />
											<xsl:with-param name="prefix" select="prefix" />
                                            <xsl:with-param name="root" select="$root_facets" />
                                            <xsl:with-param name="label_facet" >&lt;?echo <xsl:value-of select="./label"/>;?&gt;</xsl:with-param>
                                        </xsl:call-template>
                                    </div>
                                </xsl:if>
                            </xsl:when>
							<xsl:when test="./type='CARTO'"><xsl:text> </xsl:text><!-- On n'affiche pas la facette carto pour l'instant -->
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="($affMode != 'chrono' or not(contains(./field,'doc_date')) ) and $root_facets/facet[field=$field_facet]/valeur">
									<!--xsl:value-of select="$root_facets/facet[field=$field_facet]/valeur"/-->
									<div class="type_facet {$field_facet}" onclick="toggleTypeFacet(this)"><span class="facet_icon {./class}">&#160;</span><xsl:processing-instruction name="php">echo <xsl:value-of select="./label"/>;</xsl:processing-instruction>  <span class="arrow">&#160;</span></div>
									<div class="block_facet {$field_facet}">
									<xsl:call-template name="display_facet">
										<xsl:with-param name="field_facet" select="./field" />
										<xsl:with-param name="root" select="$root_facets" />
										<xsl:with-param name="label_facet" >&lt;?echo <xsl:value-of select="./label"/>;?&gt;</xsl:with-param>
									</xsl:call-template>
									</div>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>
			&#160;
			</div>
		</div>
	</xsl:template>


	<xsl:template name="toolbar">
		<xsl:param name="context"/>
		<xsl:param name="entity"/>
		<xsl:param name="pagination"/>
		<xsl:param name="id_item"/>

		<div class="toolbar">
			<xsl:if test="$context='doc_mos' or $context='doc_liste' or $context='doc_carto'">
				<div class="presentation">
					<span id="listChooser"> <!-- updateLayoutOpts({'affMode':'liste'}) presentation_updateLignes('docListe2.xsl') -->
						<xsl:attribute name="onclick"> presentation_updateLignes({'docListe' : {'affMode' : 'liste'}});</xsl:attribute>
						<xsl:attribute name="class">layout_chooser <xsl:if test="$context  = 'doc_liste'">selected</xsl:if></xsl:attribute>
						<span class="tool_hover_text"><xsl:processing-instruction name="php">echo kAffichageListe;</xsl:processing-instruction></span>&#160;
					</span>
					<span id="mosChooser" > <!-- presentation_updateLignes('docListe.xsl'); updateLayoutOpts({'affMode':'mos'}) -->
						<xsl:attribute name="onclick"> presentation_updateLignes({'docListe' : {'affMode' : 'mos'}});</xsl:attribute>
						<xsl:attribute name="class">layout_chooser <xsl:if test="$context  = 'doc_mos'">selected</xsl:if></xsl:attribute>
						<span class="tool_hover_text"><xsl:processing-instruction name="php">echo kAffichageMos;</xsl:processing-instruction></span>&#160;
					</span>
					<!--span id="mapChooser" >
						<xsl:attribute name="onclick"> presentation_updateLignes({'docListe' : {'affMode' : 'carto'}});</xsl:attribute>
						<xsl:attribute name="class">layout_chooser <xsl:if test="$context  = 'carto'">selected</xsl:if></xsl:attribute>
						<span class="tool_hover_text"><xsl:processing-instruction name="php">echo kAffichageCarto;</xsl:processing-instruction></span>&#160;
					</span-->
				</div>
				<div onclick="toggleFlagPagination();">
					<xsl:attribute name="class">toggle_pagination tool <xsl:if test="$pagination = 1">selected</xsl:if></xsl:attribute>
					<span class="tool_hover_text"><xsl:processing-instruction name="php">echo kPagination;</xsl:processing-instruction></span>&#160;
				</div>
				<div  class="toggle_preview tool &lt;?=($_SESSION['USER']['layout_opts']['prev_pop_in'])?'selected':'';?&gt;" onclick="toggleFlagPrevPopin();">
					<span class="tool_hover_text"><xsl:processing-instruction name="php">echo kPrevisualisation;</xsl:processing-instruction></span>&#160;
				</div>
			</xsl:if>
			<!-- ici mettre en place toutes les icones et leur actions parametrable avec les param à ce template -->
			<div  class="check_all tool">
				<xsl:attribute name="onclick">
					<xsl:choose>
						<xsl:when test="$context='doc_liste'">smartCheckAll(this,'#resultats.liste');</xsl:when>
						<xsl:when test="$context='doc_mos'">smartCheckAll(this,'#resultats.mos');</xsl:when>
						<xsl:when test="$context='importView'">smartCheckAll(this,'#resultats');</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<span class="tool_hover_text"><xsl:processing-instruction name="php">echo kToutSelectionner;</xsl:processing-instruction></span>
			&#160;</div>
			<div  class="add tool" ><span class="tool_hover_text"><xsl:processing-instruction name="php">echo kAjouterA;</xsl:processing-instruction></span><div class="drop_down" style="display:none;">&#160;</div>&#160;</div>
			<div  class="tri tool"><span class="tool_hover_text"><xsl:processing-instruction name="php">echo kTrierPar;</xsl:processing-instruction></span>
				<div class="drop_down" style="display:none;">
					<ul>
						<xsl:choose>
							<xsl:when test="$context='importView'">
								<li onclick="trier('doc_titre{$ordre}');"><xsl:processing-instruction name="php">echo kTitre;</xsl:processing-instruction></li>
							</xsl:when>
							<xsl:otherwise>
								<li onclick="trier('sort_doc_titre{$ordre}');"><xsl:processing-instruction name="php">echo kTitre;</xsl:processing-instruction></li>
							</xsl:otherwise>
						</xsl:choose>
						<li onclick="trier('doc_date_prod{$ordre}');"><xsl:processing-instruction name="php">echo kDate;</xsl:processing-instruction></li>
						<li onclick="trier('fonds{$ordre}');"><xsl:processing-instruction name="php">echo kFonds;</xsl:processing-instruction></li>
						<li onclick="trier('doc_id_type_doc{$ordre}');"><xsl:processing-instruction name="php">echo kType;</xsl:processing-instruction></li>
					</ul>
				</div>
			&#160;</div>
			<!--div id="slideshow" class="tool">&#160;</div>
			<div id="modify" class="tool">&#160;</div-->

			<xsl:if test="$context='doc_mos' or $context='doc_liste' or $context='importView'">
				<div  onclick="triggerPrint('doc');" class=" print tool"><span class="tool_hover_text"><xsl:processing-instruction name="php">echo kImprimer;</xsl:processing-instruction></span>&#160;</div>
				<div  class="menu_actions tool"><span class="tool_hover_text"><xsl:processing-instruction name="php">echo kMenuActions;</xsl:processing-instruction></span>
				<div class="drop_down" style="display:none;">
					<ul>
					<xsl:if test="$context!='importView'">
						<li onclick="displayMenuActions('export');" style="width:80px;"><xsl:processing-instruction name="php">print kExporter</xsl:processing-instruction></li>
						<li onclick="displayMenuActions('download','doc');" style="width:80px;"><xsl:processing-instruction name="php">print kTelecharger</xsl:processing-instruction></li>
					</xsl:if>	
                   <xsl:if test="$profil>2">
						<li onclick="displayMenuActions('modifLot','','{$id_item}','','{$context}');"><xsl:processing-instruction name="php">print  kModificationLot</xsl:processing-instruction></li>
					</xsl:if>



						<!--li onclick="displayMenuActions('print','print.php?&lt;?= $_SERVER['QUERY_STRING'] ?&gt;');"><xsl:processing-instruction name="php">print kImprimer</xsl:processing-instruction></li-->
					</ul>
				</div>
				&#160;</div>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template name="rechercheRightPreview">
		<xsl:param name="doc"/>
		<xsl:param name="rang"/>
		<xsl:param name="id_panier"/>
		<xsl:param name="id_ligne_panier"/>
		<div id="menuDroite"><xsl:attribute name="class">&lt;?= ( $layout&lt;2)?'collapsed':'';?&gt;</xsl:attribute>
			<div class="title_bar"><span id="arrow_right" class="toggle_arrow" onclick="toggleRightPreview();">&#160;</span><span class="filter_label"><xsl:processing-instruction name="php">echo kPreview;</xsl:processing-instruction></span></div>
			<div id="previewDoc">
				<h2 id="prevTitre">
					<xsl:choose>
					<xsl:when test="$doc and $doc/DOC_TITRE!=''"><xsl:value-of select="DOC_TITRE"/></xsl:when>
					<xsl:otherwise>&#160;</xsl:otherwise>
					</xsl:choose>
				</h2>
				<div id="prevMedia">
					<script>
						player_options_prevRight = {
							resizeCustom : true,
							controlbar : 'none',
							selectionSliderDisabled : true,
							autoplay : true,
							container_id : 'container',
							enable_streaming : true,
							previewOverTimeline : false,
							code_controller : '<div id="ow_id_progress_slider">	</div>				' +
							'	<div id = "ow_boutons">' +
							'		<span id = "ow_boutons_left">' +
							'			<a class = "btn play" id = "ow_bouton_play_pause" ></a>' +
							'			<p id="timecodeDisplay">00:00:00</p>' +
							'			<p id="timecodeDuration">/ 00:00:00</p>' +
							'		</span><span id = "ow_boutons_right">' +
							'			<a class = "btn" id = "ow_bouton_mute_sound"></a>'+
							'			<div id = "containSound"><div id = "ow_id_sound_slider"></div></div>' +
							'			<a class = "btn" id = "ow_bouton_fullscreen"></a>' +
							'			<a class="btn" id="ow_bouton_loop"></a>'+
							'		</span>' +
							'	</div>',
							hauteur_bg_track : 10,
							timecode_display_format : 'hh:mm:ss',
							controller_on_video : true,
							span_time_passed: true,
							dynamicController : true, 
					};
					
					if(typeof OW_env_user != 'undefined' &amp;&amp; OW_env_user.browser == 'Safari' &amp;&amp; OW_env_user.env_OS == 'Mac'){
						player_options_prevRight['xStand'] = true;
					}
					
					player_options_prevRight.skin="<xsl:processing-instruction name="php"> print kCheminHttp; </xsl:processing-instruction>/design/css/skin_simple_player/skin_flash/skin_flash.xml";
					player_options_prevRight.showFlashController = true;
					player_options_prevRight.controlbar="over";
					player_options_prevRight.offset_controlbar_height=34;
					player_options_prevRight.simpleController = true ;
					player_options_prevRight['css_path']='design/css/skin_simple_player/';
					player_options_prevRight.onPlayerReady = function(){
						setTimeout(function(){$j("#menuDroite #media_space #container").css('visibility','visible');},50);
					}
					</script>
					<xsl:if test=" $doc and $doc/DOC_ID_MEDIA = 'A'">
						<script type="text/javascript">
						player_options_prevRight.show_vignette_audio = true;
						player_options_prevRight.res_vignette_width = "";
						player_options_prevRight.poster_width = 286;
						player_options_prevRight.poster_height = 215;
						player_options_prevRight.dynamicController = true;
						player_options_prevRight.controller_on_video = true;
						</script>
					</xsl:if>
					<xsl:choose>
					<xsl:when test="$doc">
						<xsl:choose>
							<xsl:when test="($doc/DOC_ID_MEDIA = 'V' or $doc/DOC_ID_MEDIA = 'A') and $id_ligne_panier">
								 <div id="media_space" >
								 <div id="container">
								 <img class="prev_vignette" id="poster" onclick="$j('#container').css('visibility','hidden');loadOraoPlayer({$id_ligne_panier},$j('#prev_vignette').width(),'',player_options_prevRight,true,'visuExtrait','myVideo_prevDoc');" src="makeVignette.php?image={$doc/VIGNETTE}&amp;kr=1&amp;w=286&amp;h=215&amp;ol=player"/>
								 </div>
								 </div>
							</xsl:when>
							<xsl:when test="($doc/DOC_ID_MEDIA = 'V' or $doc/DOC_ID_MEDIA = 'A') ">
							<div id="media_space">
							<div id="container">
							<img class="prev_vignette" id="poster" onclick="$j('#container').css('visibility','hidden');loadOraoPlayer({$doc/ID_DOC},$j('#prev_vignette').width(),'',player_options_prevRight,true,'doc','myVideo_prevDoc');" src="makeVignette.php?image={$doc/VIGNETTE}&amp;kr=1&amp;w=286&amp;h=215&amp;ol=player"/></div></div></xsl:when>
							<xsl:otherwise><img class="prev_vignette"  src="makeVignette.php?image={$doc/VIGNETTE}&amp;kr=1&amp;w=286&amp;h=215"/></xsl:otherwise>
						</xsl:choose>
					
					</xsl:when>
					<xsl:otherwise>&#160;</xsl:otherwise>
					</xsl:choose>
				</div>
				<div id="prevWrapper">
				<div id="prevInfos">
					<xsl:choose>
					<xsl:when test="$doc and $doc/DOC_RES!=''">
					<xsl:call-template name="nl2br">
						<xsl:with-param name="stream" select="$doc/DOC_RES"/>
					</xsl:call-template>
					<!--xsl:value-of select="DOC_RES"/-->
					</xsl:when>
					<xsl:otherwise>&#160;</xsl:otherwise>
					</xsl:choose>
				</div>
				<div id="prevLink">
					<button type="button">
						<xsl:if test="$doc">
							<xsl:attribute name="style"><xsl:if test="$doc">visibility : visible ;</xsl:if></xsl:attribute>
							<xsl:attribute name="onclick">
								<xsl:text>window.location.href = 'index.php?urlaction=doc&amp;id_doc=</xsl:text>
								<xsl:value-of select="$doc/ID_DOC"/>
								<xsl:if test="$rang and $rang != ''">&amp;rang=<xsl:value-of select="$rang"/></xsl:if>
								<xsl:if test="$id_panier and $id_panier != ''">&amp;id_panier=<xsl:value-of select="$id_panier"/></xsl:if>
								<xsl:text>'</xsl:text></xsl:attribute>
						</xsl:if>
						<xsl:processing-instruction name="php">echo kVoirFicheDoc;</xsl:processing-instruction>
					</button>
				</div>
				</div>
			</div>
		</div>
	</xsl:template>



	<xsl:template name="panBlock">
	<div id="panBlock" class="expanded">
		<div id="panGauche" >
			<div class="title_bar"><xsl:processing-instruction name="php">echo kPaniers;</xsl:processing-instruction></div>
			<ul class="panListe">
			<xsl:for-each select="cart_list">
				<li><a href="#"><xsl:value-of select="PAN_TITRE"/></a></li><!--  onclick="alert('{ID_PANIER}')"-->

			</xsl:for-each>
			</ul>
		</div>
		<div id="mainPan">
			<div class="title_bar"><xsl:value-of select="cart_list/PAN_TITRE[.!='']"/>&#160;</div>
			<iframe id="panFrame">&#160;</iframe>
		</div>
	</div>
	</xsl:template>




<xsl:template name="mosDoc">
	<xsl:param name="context"/>
	<xsl:variable name="subcontext">
	<xsl:choose>
		<xsl:when test="$context = 'doc_cat_list'">doc_cat_list</xsl:when>
		<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<xsl:variable name="context">
		<xsl:choose>
			<xsl:when test="$context='doc_cat_list'">doc</xsl:when>
			<xsl:when test="$context='t_panier_doc' and ID_PANIER='_session'">doc</xsl:when>
			<xsl:otherwise><xsl:value-of select="$context" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:for-each select="child::*[name() = $context]">
		<xsl:variable name="j"><xsl:number/></xsl:variable>
		<xsl:variable name="rang">
			<xsl:choose>
				<xsl:when test="$context='t_doc'"><xsl:value-of select="number($j)"/></xsl:when>
				<xsl:when test="$fromOffset and $fromOffset != ''">
					<xsl:value-of select="number(number($fromOffset)+number($offset)+$j)"/>
				</xsl:when>
				<xsl:when test="$offset and $offset!=''">
					<xsl:value-of select="number(number($offset)+$j)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number($j)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
			<xsl:variable name="chemin_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,storyboardChemin); ?&gt;</xsl:variable>
		<xsl:variable name="doc_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,kDocumentUrl); ?&gt;</xsl:variable>


		<xsl:variable name="im_chemin"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="IM_CHEMIN"/></xsl:call-template></xsl:variable>
		<xsl:variable name="im_fichier"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="IM_FICHIER"/></xsl:call-template></xsl:variable>
		<xsl:variable name="da_chemin"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="DA_CHEMIN"/></xsl:call-template></xsl:variable>
		<xsl:variable name="da_fichier"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="DA_FICHIER"/></xsl:call-template></xsl:variable>

		<xsl:variable name="vignette"><xsl:choose>
				<xsl:when test="string-length($im_fichier)>3"><xsl:value-of select="concat($chemin_story,$im_chemin,'/',$im_fichier)"/></xsl:when>
				<xsl:when test="string-length($da_fichier)>3 and string-length($da_chemin)>3"><xsl:value-of select="concat($doc_story,$da_chemin,$da_fichier)"/></xsl:when>
				<xsl:when test="string-length($da_fichier)>3"><xsl:value-of select="concat($doc_story,$da_fichier)"/></xsl:when>
				<xsl:when test="VIGNETTE!=''"><xsl:value-of select="VIGNETTE"/></xsl:when>
			</xsl:choose></xsl:variable>

		<xsl:variable name="doc_titre">
			<xsl:choose>
				<xsl:when test="$context='t_panier_doc' and PDOC_EXTRAIT = '1'">
					<xsl:call-template name="remove_html_tags">
						<xsl:with-param name="node" select="PDOC_EXT_TITRE" />
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remove_html_tags">
						<xsl:with-param name="node" select="DOC_TITRE" />
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="raw_doc_titre">
			<xsl:choose>
				<xsl:when test="$context='t_panier_doc' and PDOC_EXTRAIT = '1'">
					<xsl:value-of select="PDOC_EXT_TITRE" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="DOC_TITRE" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div class="resultsMos bloc_image conteneur_dragable" style="-o-text-overflow: ellipsis; text-overflow: ellipsis;"><!-- onclick="loadPrevDoc({ID_DOC},this,{$rang});"  -->

			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="$subcontext != 'doc_cat_list'">resultsMos  bloc_image conteneur_dragable drag_to_cart</xsl:when>
					<xsl:otherwise>resultsMos draggable_doc_cat</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<xsl:attribute name="onclick">
				<xsl:choose>
					<xsl:when test="$context='t_panier_doc'">loadPrevDoc(<xsl:value-of select='ID_DOC'/>,this,<xsl:value-of select='$rang'/>,<xsl:value-of select="ID_PANIER"/>);</xsl:when>
					<xsl:otherwise>loadPrevDoc(<xsl:value-of select='ID_DOC'/>,this,<xsl:value-of select='$rang'/>);</xsl:otherwise>
				</xsl:choose>event.stopPropagation();
			</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:choose>
				<xsl:when test="$context='t_panier_doc'">
					<xsl:value-of select="concat('pdoc_',ID_LIGNE_PANIER)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat('doc_',ID_DOC)"/>
				</xsl:otherwise>
			</xsl:choose>
			</xsl:attribute>
			<xsl:if test="$vignette!=''">
				<xsl:choose>
					<xsl:when test="ID_PRIV>=0  or $context='t_panier_doc'">
						<div class="bgVignetteMos">
							<xsl:choose>
								<xsl:when test="$context='t_panier_doc'">
									<a onclick="checkLayoutRedirect('index.php?urlaction=doc&amp;id_doc={ID_DOC}&amp;rang={$rang}&amp;id_panier={../t_panier/ID_PANIER}')" >
										<img src="makeVignette.php?image={$vignette}&amp;amp;w=200&amp;amp;h=113&amp;amp;kr=1" border="0"  title="{$doc_titre}"/>
									</a>
								</xsl:when>
								<xsl:when test="ID_PRIV>=0">
									<a onclick="checkLayoutRedirect('index.php?urlaction=doc&amp;id_doc={ID_DOC}&amp;rang={$rang}')" >
										<img src="makeVignette.php?image={$vignette}&amp;amp;w=200&amp;amp;h=113&amp;amp;kr=1" border="0"  title="{$doc_titre}"/>
									</a>
								</xsl:when>
							</xsl:choose>
						</div>
						 <xsl:if test="$subcontext != 'doc_cat_list'">
						<script type='text/javascript'>
							<xsl:choose>
								<xsl:when test="$context = 't_panier_doc'" >elements.push("<xsl:value-of select="concat('pdoc_',ID_LIGNE_PANIER)"/>");</xsl:when>
								<xsl:otherwise>elements.push("<xsl:value-of select="concat('doc_',ID_DOC)"/>");</xsl:otherwise>
							</xsl:choose>
						</script>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<img src="makeVignette.php?image={$vignette}&amp;amp;w=200" border="0" height="113" width="200"  title="{$doc_titre}" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="$vignette=''">
				<div class="bgVignetteMos">
					<img src="design/images/nopicture.gif" border="0" height="113"   title="{$doc_titre}" />
				</div>
				<xsl:if test="$subcontext!='doc_cat_list'">
					<script type='text/javascript'>
						<xsl:choose>
							<xsl:when test="$context = 't_panier_doc'" >elements.push("<xsl:value-of select="concat('pdoc_',ID_LIGNE_PANIER)"/>");</xsl:when>
							<xsl:otherwise>elements.push("<xsl:value-of select="concat('doc_',ID_DOC)"/>");</xsl:otherwise>
						</xsl:choose>
					</script>
				</xsl:if>
			</xsl:if>
			<!--xsl:if test="DOC_COTE!=''">
				<div style="overflow: hidden; -o-text-overflow: ellipsis; text-overflow: ellipsis;">
					<xsl:choose>
						<xsl:when test="ID_PRIV>=0"><a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}&amp;rang={$rang}"><xsl:value-of select="DOC_COTE"/></a></xsl:when>
						<xsl:otherwise><xsl:value-of select="DOC_COTE"/></xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:if-->
			<div class="mosTitre" style="">
				<xsl:if test="$doc_titre!=''">
					<xsl:choose>
						<xsl:when test="$context='t_panier_doc'">
							<a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}&amp;rang={$rang}&amp;id_panier={../t_panier/ID_PANIER}" title="{$doc_titre}"><xsl:value-of select="$raw_doc_titre"/></a>
						</xsl:when>
						<xsl:when test="$context='t_doc' or ID_PRIV>=0">
							<a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}&amp;rang={$rang}" title="{$doc_titre}"><xsl:value-of select="$raw_doc_titre"/></a>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="$raw_doc_titre"/></xsl:otherwise>
					</xsl:choose>
				</xsl:if>&#160;

			</div>
			<!--div class="secondary_infos"-->
				<xsl:variable name="array_mat_type">
					<xsl:call-template name="tokenize">
						<xsl:with-param name="pText" select="MAT_TYPE"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="secondary_infos">
				<xsl:choose>
					<xsl:when test="PDOC_EXTRAIT and PDOC_EXTRAIT='1' and (DOC_ID_MEDIA='V' or DOC_ID_MEDIA='A') and EXT_DUREE!=''">
						&#160;<xsl:value-of select="EXT_DUREE"/>&#160;
					</xsl:when>
					<xsl:when test="(DOC_ID_MEDIA='V' or DOC_ID_MEDIA='A') and DOC_DUREE!=''">
						&#160;<xsl:value-of select="DOC_DUREE"/>&#160;
					</xsl:when>
					<xsl:when test="DOC_ID_MEDIA='P'  ">
						<xsl:variable name="array_mat_width">
							<xsl:call-template name="tokenize">
								<xsl:with-param name="pText" select="MAT_WIDTH"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="array_mat_height">
							<xsl:call-template name="tokenize">
								<xsl:with-param name="pText" select="MAT_HEIGHT"/>
							</xsl:call-template>
						</xsl:variable>
						&#160;
						<xsl:choose>
							<!-- On essai de prendre les dimensions du master -->
							<xsl:when test="contains(MAT_TYPE,'MASTER')">
								<xsl:for-each select="exsl:node-set($array_mat_type)/tag">
									<xsl:variable name="idx"><xsl:number/></xsl:variable>
									<xsl:if test=".='MASTER'">
										<xsl:value-of select="exsl:node-set($array_mat_width)/tag[position()=$idx]"/><xsl:text> x </xsl:text><xsl:value-of select="exsl:node-set($array_mat_height)/tag[position()=$idx]"/>
									</xsl:if>
								</xsl:for-each>&#160;
							</xsl:when>
							<!-- Si pas de master défini, alors on prends les premières dimensions non nullse que l'on trouve -->
							<xsl:when test="exsl:node-set($array_mat_width)/tag!='' and exsl:node-set($array_mat_height)/tag!='' ">
								<xsl:value-of select="exsl:node-set($array_mat_width)/tag"/><xsl:text> x </xsl:text><xsl:value-of select="exsl:node-set($array_mat_height)/tag"/>
							</xsl:when>
							<xsl:otherwise></xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="DOC_ID_MEDIA='D'  ">
						<xsl:variable name="array_nb_pages">
							<xsl:call-template name="tokenize">
								<xsl:with-param name="pText" select="MAT_NB_PAGES"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:for-each select="exsl:node-set($array_mat_type)/tag">
							<xsl:variable name="idx"><xsl:number/></xsl:variable>
							<xsl:if test=".='MASTER'">
								&#160;<xsl:value-of select="exsl:node-set($array_nb_pages)/tag[position()=$idx]"/><xsl:text> </xsl:text>&lt;?print kPages;?&gt;
							</xsl:if>
						</xsl:for-each>&#160;
					</xsl:when>
					<xsl:when test="DOC_ID_MEDIA='R' and CNT_DOC_LIES_DST!=''  ">
						&#160;<xsl:value-of select="CNT_DOC_LIES_DST"/>&#160;&lt;?print kDocument_s;?&gt;
					</xsl:when>
					<xsl:otherwise>&#160;</xsl:otherwise>
				</xsl:choose>
				</xsl:variable>
			<!--/div-->
			<div class="infos_mos first_row">
				<xsl:choose>
					<xsl:when test="DOC_ID_MEDIA='V'">
						<img class="icn_type_media" src="design/images/icninfo-video.png" ><xsl:attribute name="title">&lt;?=kVideo;?&gt;</xsl:attribute></img>
					</xsl:when>
					<xsl:when test="DOC_ID_MEDIA='P'">
						<img class="icn_type_media" src="design/images/icninfo-image.png" ><xsl:attribute name="title">&lt;?=kImage;?&gt;</xsl:attribute></img>
					</xsl:when>
					<xsl:when test="DOC_ID_MEDIA='A'">
						<img class="icn_type_media" src="design/images/icninfo-audio.png" ><xsl:attribute name="title">&lt;?=kAudio;?&gt;</xsl:attribute></img>
					</xsl:when>
					<xsl:when test="DOC_ID_MEDIA='D'">
						<img class="icn_type_media" src="design/images/icninfo-file.png" ><xsl:attribute name="title">&lt;?=kDocument;?&gt;</xsl:attribute></img>
					</xsl:when>
					<xsl:when test="DOC_ID_MEDIA='R'">
						<img class="icn_type_media" src="design/images/icninfo-folder.png" ><xsl:attribute name="title">&lt;?=kReportage;?&gt;</xsl:attribute></img>
					</xsl:when>
					<xsl:otherwise>&#160;</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="$secondary_infos"/>
				<xsl:choose>
					<xsl:when test="DOC_NUM=1 and DOC_ACCES=1">
						<img class="icn_type_media icn_online" src="design/images/icninfo-online.png" ><xsl:attribute name="title">&lt;?=kEnLigne;?&gt;</xsl:attribute></img>
					</xsl:when>
					<xsl:when test="DOC_NUM=1">
						<img class="icn_type_media icn_online" src="design/images/icninfo-offline.png" ><xsl:attribute name="title">&lt;?=kAccesInterdit;?&gt;</xsl:attribute></img>
					</xsl:when>
				</xsl:choose>
			</div>
			<div class="infos_mos scnd_row" onclick="stopEvent(event);">
				<xsl:choose>
					<xsl:when test="$context='t_panier_doc'">
						<label class="mos_checkbox">
							<input type="checkbox"  name="checkbox{ID_LIGNE_PANIER}" value="{ID_LIGNE_PANIER}" id="select" >
							<xsl:attribute name="title">&lt;? echo kSelectionner;?&gt;</xsl:attribute>
							</input>
							<input type="hidden" name="t_panier_doc[][ID_LIGNE_PANIER]" value="{ID_LIGNE_PANIER}"/>
							<input type="hidden" name="t_panier_doc[][ID_DOC]" value="{ID_DOC}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXT_TITRE]" value="{PDOC_EXT_TITRE}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXT_TCIN]" value="{PDOC_EXT_TCIN}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXT_TCOUT]" value="{PDOC_EXT_TCOUT}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXTRAIT]" value="{PDOC_EXTRAIT}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_ID_ETAT]" value="{PDOC_ID_ETAT}"/>
						</label>
							<!-- BOUTON EXPORT VA ICI -->
						<label>
						<a onclick="event.stopPropagation();" href="index.php?urlaction=doc&amp;id_doc={ID_DOC}&amp;rang={$rang}&amp;id_panier={../t_panier/ID_PANIER}" >
								<img src="design/images/icninfo-info.png" class="icn_video"  style="height:13px;width:13px;" border="0" title="&lt;?php echo kConsulter; ?&gt;" alt="&lt;?php echo kConsulter; ?&gt;" />
						</a>
						</label>
						<xsl:if test="ID_PRIV>=5">
							<label>
								<a onclick="event.stopPropagation();" href="index.php?urlaction=docSaisie&amp;id_doc={ID_DOC}&amp;rang={$rang}">
									<img src="design/images/icnbar-edit.png" style="height:13px;width:13px;" border="0" id="modif" title="&lt;?php echo kModifier; ?&gt;"  alt="&lt;?php echo kModifier; ?&gt;" />
								</a>
							</label>
						</xsl:if>
						<xsl:if test="ID_LIGNE_PANIER">
							<label>
								<a onclick="removeLine({ID_LIGNE_PANIER});event.stopPropagation();" href="#" >
									<img src="design/images/icnbar-trash.png" style="cursor : pointer ; height:14px; width : 12px;" border="0"  onclick="removeLine({ID_LIGNE_PANIER});event.stopPropagation();" class="removeLine" title="&lt;?php echo kSupprimer; ?&gt;"  alt="&lt;?php echo kSupprimer; ?&gt;" />
								</a>
							</label>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$subcontext = 'doc_cat_list'">
							<input type="hidden" class="input_doc_cat input_id_doc" name="t_doc_cat[][ID_DOC]" value="{ID_DOC}"/>
							<input type="hidden" class="input_doc_cat input_cdoc_ordre" name="t_doc_cat[][CDOC_ORDRE]" value="{$j - 1}"/>
						</xsl:if>
						<label class="mos_checkbox">
							<input type="checkbox" name="checkbox{ID_DOC}" value="{ID_DOC}" id="select" >
								<xsl:attribute name="title">&lt;? echo kSelectionner;?&gt;</xsl:attribute>
							</input>
						</label>
							<!-- BOUTON EXPORT VA ICI -->
						<xsl:choose>
						<xsl:when test="$context='t_doc' or ID_PRIV>=1">
							<xsl:variable name="typePlayer">
								 <xsl:choose>
									<xsl:when test="$profil>=1">extrait</xsl:when>
									<xsl:otherwise>grandpublic</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<label>
								<a onclick="event.stopPropagation();" href="index.php?urlaction=doc&amp;id_doc={ID_DOC}&amp;rang={$rang}" >
									<img src="design/images/icninfo-info.png" class="icn_video"  style="height:13px;width:13px;" border="0" title="&lt;?php echo kConsulter; ?&gt;" alt="&lt;?php echo kConsulter; ?&gt;" />
								</a>
							</label>
						</xsl:when>
						<xsl:otherwise>
							<label>&#160;</label>
						</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
						<xsl:when test="$subcontext = 'doc_cat_list'">
							<label>
								<a onclick="removeDocCat(this);event.stopPropagation();" href="#" >
									<img src="design/images/icnbar-trash.png" style="cursor : pointer ; height:14px; width : 12px;" border="0"  onclick="removeDocCat(this); event.stopPropagation();" class="removeLine" title="&lt;?php echo kSupprimer; ?&gt;"  alt="&lt;?php echo kSupprimer; ?&gt;" />
								</a>
							</label>
						</xsl:when>
						<xsl:when test="ID_PRIV>=5">
							<label>
								<a onclick="event.stopPropagation();" href="index.php?urlaction=docSaisie&amp;id_doc={ID_DOC}&amp;rang={$rang}">
									<img src="design/images/icnbar-edit.png" style="height:13px;width:13px;" border="0" id="modif" title="&lt;?php echo kModifier; ?&gt;"  alt="&lt;?php echo kModifier; ?&gt;" />
								</a>
							</label>
						</xsl:when>
						<xsl:otherwise>
							<label>&#160;</label>
						</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="ID_PRIV>=5 and DOC_NUM = 1">
							<label>
							<a onclick="displayMenuActions('download','doc',{ID_DOC})" >
								<img src="design/images/icnbar-export.png" style="height:13px;width:13px;" border="0" id="download" title="&lt;?php echo kTelecharger; ?&gt;"  alt="&lt;?php echo kTelecharger; ?&gt;" />
							</a>
							</label>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</div>

		</div>
	</xsl:for-each>

	<script type="text/javascript">

		// fonction d'affichage du hover au dessus de la vignette compatible ios7
		function showHoverVignetteX(event) {

			$j("div.scnd_row").css("display","");

			if ( this.className.indexOf("resultsMos") &gt;= 0 ){
				$j(this).find(".scnd_row").css("opacity",'1').css("display","flex").css("display","-webkit-flex");
			} else {
				$j(this).parent().nextAll(".scnd_row").css("opacity",'1').css("display","flex").css("display","-webkit-flex");
			}
		}
		if(typeof tablet_border !== 'undefined'){
			$j(document).ready(function(){
				if ($j(window).width()&lt;tablet_border){

					// désactivation du lien sur le titre de la vignette
					$j('.mosTitre a').prop("href",'#');
				}
			});
		}
		var vignettes = document.getElementsByClassName("resultsMos");

		for (var i = 0 ; i &lt; vignettes.length ; i++ ) {
			vignettes[i].addEventListener("click",showHoverVignetteX,false);
		}

		function check_selected_import(){
			var ids = '';
			$j("form#import_aff_form input[name^='checkbox']").each(function(){
				if ($j(this).is(':checked')) {
					if (ids != ''){
						ids = ids+',';
					}
					ids = ids + $j(this).val();
				}
			});
			if (ids == ''){
				alert("Vous devez sélectionner au moins une notice");
				return false;
			}else{
				return true;
			}
		}
	</script>
</xsl:template>

<xsl:template name="mosRowDoc">
	<xsl:param name="context"/>
	<xsl:param name="h"/>
	<xsl:param name="w"/>
	<xsl:param name="zoom"/>
	<xsl:variable name="subcontext">
		<xsl:choose>
			<xsl:when test="$context = 'doc_cat_list'">doc_cat_list</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="context">
		<xsl:choose>
			<xsl:when test="$context='doc_cat_list'">doc</xsl:when>
			<xsl:when test="$context='t_panier_doc' and ID_PANIER='_session'">doc</xsl:when>
			<xsl:otherwise><xsl:value-of select="$context" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="height">
		<xsl:choose>
			<xsl:when test="$h and $h != ''">
				<xsl:value-of select="$h"/>
			</xsl:when>
			<otherwise>
				<xsl:text>148</xsl:text>
			</otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="width">
		<xsl:choose>
			<xsl:when test="$w and $w != ''">
				<xsl:value-of select="$w"/>
			</xsl:when>
			<otherwise>
				<xsl:text>200</xsl:text>
			</otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:for-each select="child::*[name() = $context]">
		<xsl:variable name="j"><xsl:number/></xsl:variable>
		<xsl:variable name="rang">
			<xsl:choose>
				<xsl:when test="$fromOffset and $fromOffset != ''">
					<xsl:value-of select="number(number($fromOffset)+number($offset)+$j)"/>
				</xsl:when>
				<xsl:when test="$offset and $offset!=''">
					<xsl:value-of select="number(number($offset)+$j)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number($j)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="chemin_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,storyboardChemin); ?&gt;</xsl:variable>
		<xsl:variable name="doc_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,kDocumentUrl); ?&gt;</xsl:variable>
		
		
		<xsl:variable name="im_chemin"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="IM_CHEMIN"/></xsl:call-template></xsl:variable>
		<xsl:variable name="im_fichier"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="IM_FICHIER"/></xsl:call-template></xsl:variable>
		<xsl:variable name="da_chemin"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="DA_CHEMIN"/></xsl:call-template></xsl:variable>
		<xsl:variable name="da_fichier"><xsl:call-template name="remove_html_tags"><xsl:with-param name="node" select="DA_FICHIER"/></xsl:call-template></xsl:variable>
		
		<xsl:variable name="vignette"><xsl:choose>
			<xsl:when test="string-length($im_fichier)>3"><xsl:value-of select="concat($chemin_story,$im_chemin,'/',$im_fichier)"/></xsl:when>
			<xsl:when test="string-length($da_fichier)>3 and string-length($da_chemin)>3"><xsl:value-of select="concat($doc_story,$da_chemin,$da_fichier)"/></xsl:when>
			<xsl:when test="string-length($da_fichier)>3"><xsl:value-of select="concat($doc_story,$da_fichier)"/></xsl:when>
			<xsl:when test="VIGNETTE!=''"><xsl:value-of select="VIGNETTE"/></xsl:when>
		</xsl:choose></xsl:variable>
		
		<xsl:variable name="doc_titre">
			<xsl:choose>
				<xsl:when test="$context='t_panier_doc' and PDOC_EXTRAIT = '1'">
					<xsl:call-template name="remove_html_tags">
						<xsl:with-param name="node" select="PDOC_EXT_TITRE" />
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remove_html_tags">
						<xsl:with-param name="node" select="DOC_TITRE" />
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="raw_doc_titre">
			<xsl:choose>
				<xsl:when test="$context='t_panier_doc' and PDOC_EXTRAIT = '1'">
					<xsl:value-of select="PDOC_EXT_TITRE" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="DOC_TITRE" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div style="-o-text-overflow: ellipsis; text-overflow: ellipsis;"><!-- onclick="loadPrevDoc({ID_DOC},this,{$rang});"  -->
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="$subcontext != 'doc_cat_list'">resultsMosRow resultsMos bloc_image conteneur_dragable drag_to_cart etat_doc<xsl:value-of select="DOC_ID_ETAT_DOC" /></xsl:when>
					<xsl:otherwise>resultsMosRow resultsMos draggable_doc_cat etat_doc<xsl:value-of select="DOC_ID_ETAT_DOC" /></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			
			<xsl:attribute name="onclick">
				<xsl:choose>
					<xsl:when test="$context='t_panier_doc'">loadPrevDoc(<xsl:value-of select='ID_DOC'/>,this,<xsl:value-of select='$rang'/>,<xsl:value-of select="ID_PANIER"/>);</xsl:when>
					<xsl:otherwise>loadPrevDoc(<xsl:value-of select='ID_DOC'/>,this,<xsl:value-of select='$rang'/>);</xsl:otherwise>
				</xsl:choose>event.stopPropagation();
			</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:choose>
					<xsl:when test="$context='t_panier_doc'">
						<xsl:value-of select="concat('pdoc_',ID_LIGNE_PANIER)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat('doc_',ID_DOC)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			
			<xsl:variable name="ol_type_media">
				<xsl:choose>
					<xsl:when test="DOC_ID_MEDIA='V'"><xsl:text>player</xsl:text></xsl:when>
					<xsl:when test="DOC_ID_MEDIA='A'"><xsl:text>audio</xsl:text></xsl:when>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:variable name="linkrang">
				<xsl:choose>
					<xsl:when test="$context='t_panier_doc' and $rang!=''"><xsl:value-of select="concat('&amp;rang=',$rang,'&amp;id_panier=',../t_panier/ID_PANIER)"/></xsl:when>
					<xsl:when test="$rang!=''"><xsl:value-of select="concat('&amp;rang=',$rang)"/></xsl:when>
				</xsl:choose>
			</xsl:variable>
			
			<div class="mosRow_leftPart">
				<xsl:if test="$vignette!=''">
					<xsl:choose>
						<xsl:when test="ID_PRIV>=0  or $context='t_panier_doc'">
							
							<xsl:if test="$height ='' and $width='' ">
								<div class="bgVignetteMos">
									<a onclick="checkLayoutRedirect('index.php?urlaction=doc&amp;id_doc={ID_DOC}{$linkrang}')" >
										<img src="makeVignette.php?image={$vignette}&amp;amp;w=200&amp;amp;h=148&amp;amp;kr=1&amp;amp;ol={$ol_type_media}" border="0"  title="{$doc_titre}"/>
									</a>
								</div>
							</xsl:if>
							<xsl:if test="$height !='' and $width !='' ">
								<div class="bgVignetteMos">
									<a onclick="checkLayoutRedirect('index.php?urlaction=doc&amp;id_doc={ID_DOC}{$linkrang}')" >
										<img src="makeVignette.php?image={$vignette}&amp;amp;w={$width}&amp;amp;h={$height}&amp;amp;kr=1&amp;amp;ol={$ol_type_media}" border="0"  title="{$doc_titre}"/>
									</a>
								</div>
							</xsl:if>
							
						 <xsl:if test="$subcontext != 'doc_cat_list'">
							 <script type='text/javascript'>
								 <xsl:choose>
									 <xsl:when test="$context = 't_panier_doc'" >elements.push("<xsl:value-of select="concat('pdoc_',ID_LIGNE_PANIER)"/>");</xsl:when>
									 <xsl:otherwise>elements.push("<xsl:value-of select="concat('doc_',ID_DOC)"/>");</xsl:otherwise>
								 </xsl:choose>
							 </script>
						 </xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$height ='' and $width ='' ">
								<img src="makeVignette.php?image={$vignette}&amp;amp;w=200" border="0" height="148" width="200"  title="{$doc_titre}" />
							</xsl:if>
							<xsl:if test="$height !='' and $width !='' ">
								<img src="makeVignette.php?image={$vignette}&amp;amp;w={$width}" border="0" height="{$height}" width="{$width}"  title="{$doc_titre}" />
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$vignette=''">
					<div class="bgVignetteMos">
						<xsl:if test="$height ='' and $width ='' ">
							<img src="design/images/nopicture.gif" border="0" height="148"   title="{$doc_titre}" />
						</xsl:if>
						<xsl:if test="$height !='' and $width !='' ">
							<img src="design/images/nopicture.gif" border="0" height="{$height}"   title="{$doc_titre}" />
						</xsl:if>
					</div>
					<xsl:if test="$subcontext!='doc_cat_list'">
						<script type='text/javascript'>
							<xsl:choose>
								<xsl:when test="$context = 't_panier_doc'" >elements.push("<xsl:value-of select="concat('pdoc_',ID_LIGNE_PANIER)"/>");</xsl:when>
								<xsl:otherwise>elements.push("<xsl:value-of select="concat('doc_',ID_DOC)"/>");</xsl:otherwise>
							</xsl:choose>
						</script>
					</xsl:if>
				</xsl:if>
			</div>
			<div class="mosRow_rightPart">
				<div class="mosRow_header">
					<div class="mosTitre" style="">
						<xsl:choose>
							<xsl:when test="ID_PRIV>=0"><a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}{$linkrang}"><xsl:value-of select="DOC_COTE"/></a></xsl:when>
							<xsl:otherwise><xsl:value-of select="DOC_COTE"/></xsl:otherwise>
						</xsl:choose>
					</div>
					<xsl:if test="number(DOC_NOTE) &gt;= 4 or number(./t_doc/DOC_NOTE) &gt;= 4">
						<span class="favorite"><xsl:text> </xsl:text></span>
					</xsl:if>
					
					<div class="infos_mos" onclick="stopEvent(event);">
						<xsl:choose>
							<xsl:when test="$context='t_panier_doc'">
								<label>
									<a onclick="event.stopPropagation();" href="index.php?urlaction=doc&amp;id_doc={ID_DOC}{$linkrang}" >
										<img src="design/images/icn-info-black.png" class="icn_video"  style="height:13px;width:13px;" border="0" title="&lt;?php echo kConsulter; ?&gt;" alt="&lt;?php echo kConsulter; ?&gt;" />
									</a>
								</label>
								<!--xsl:if test="ID_PRIV>=5">
								 <label>
								 <a onclick="event.stopPropagation();" href="index.php?urlaction=docSaisie&amp;id_doc={ID_DOC}&amp;rang={$rang}">
									<img src="design/images/icnbar-edit.png" style="height:13px;width:13px;" border="0" id="modif" title="&lt;?php echo kModifier; ?&gt;"  alt="&lt;?php echo kModifier; ?&gt;" />
								 </a>
								 </label>
								 </xsl:if-->
								<xsl:if test="ID_LIGNE_PANIER">
									<label>
										<a onclick="removeLine({ID_LIGNE_PANIER});event.stopPropagation();" href="#" >
											<img src="design/images/icnbar-trash.png" style="cursor : pointer ; height:14px; width : 12px;" border="0"  onclick="removeLine({ID_LIGNE_PANIER});event.stopPropagation();" class="removeLine" title="&lt;?php echo kSupprimer; ?&gt;"  alt="&lt;?php echo kSupprimer; ?&gt;" />
										</a>
									</label>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="$subcontext = 'doc_cat_list'">
									<input type="hidden" class="input_doc_cat input_id_doc" name="t_doc_cat[][ID_DOC]" value="{ID_DOC}"/>
									<input type="hidden" class="input_doc_cat input_cdoc_ordre" name="t_doc_cat[][CDOC_ORDRE]" value="{$j - 1}"/>
								</xsl:if>
								<xsl:choose>
									<xsl:when test="ID_PRIV>=1">
										<xsl:variable name="typePlayer">
											<xsl:choose>
												<xsl:when test="$profil>=1">extrait</xsl:when>
												<xsl:otherwise>grandpublic</xsl:otherwise>
											</xsl:choose>
										</xsl:variable>
										<label>
											<a onclick="event.stopPropagation();" href="index.php?urlaction=doc&amp;id_doc={ID_DOC}{$linkrang}" >
												<img src="design/images/icngrid-info.png" class="icn_video"  style="height:13px;width:13px;" border="0" title="&lt;?php echo kConsulter; ?&gt;" alt="&lt;?php echo kConsulter; ?&gt;" />
											</a>
										</label>
									</xsl:when>
								</xsl:choose>
								<xsl:choose>
									<xsl:when test="$subcontext = 'doc_cat_list'">
										<label>
											<a onclick="removeDocCat(this);event.stopPropagation();" href="#" >
												<img src="design/images/icn-trash-grey.png" style="cursor : pointer ; height:14px; width : 12px;" border="0"  onclick="removeDocCat(this); event.stopPropagation();" class="removeLine" title="&lt;?php echo kSupprimer; ?&gt;"  alt="&lt;?php echo kSupprimer; ?&gt;" />
											</a>
										</label>
									</xsl:when>
									<xsl:when test="ID_PRIV>=5">
										<label>
											<a onclick="event.stopPropagation();" href="index.php?urlaction=docSaisie&amp;id_doc={ID_DOC}&amp;rang={$rang}">
												<img src="design/images/icn-edit.png" style="height:13px;width:13px;" border="0" id="modif" title="&lt;?php echo kModifier; ?&gt;"  alt="&lt;?php echo kModifier; ?&gt;" />
											</a>
										</label>
									</xsl:when>
								</xsl:choose>
								<xsl:if test="ID_PRIV>=5 and DOC_NUM = 1">
									<label>
										<a onclick="displayMenuActions('download','doc',{ID_DOC})" >
											<img src="design/images/icn-export.png" style="height:13px;width:13px;" border="0" id="download" title="&lt;?php echo kTelecharger; ?&gt;"  alt="&lt;?php echo kTelecharger; ?&gt;" />
										</a>
									</label>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
				<div class="mosRow_infos">
					<div class="mosRow_infos_title"><xsl:value-of select="DOC_TITRE"/></div>
					
					<xsl:if test="DOC_DATE_PROD != ''">
						<div class="infos_label"><xsl:processing-instruction name="php"> echo kDate ;</xsl:processing-instruction></div>
						<div class="infos_value">
							<xsl:call-template name="format_date">
								<xsl:with-param name="chaine_date" select="DOC_DATE_PROD"/>
							</xsl:call-template>
						</div>
					</xsl:if>
					<xsl:if test="DOC_DUREE != '' ">
						<div class="infos_label"><xsl:processing-instruction name="php"> echo kDuree ;</xsl:processing-instruction></div>
						<div class="infos_value"><xsl:value-of select="DOC_DUREE"/></div>
					</xsl:if>
				
				</div>
				<xsl:choose>
					<xsl:when test="$context='t_panier_doc'">
						<label class="mos_checkbox">
							<input type="checkbox"  name="checkbox{ID_LIGNE_PANIER}" value="{ID_LIGNE_PANIER}" id="select" >
								<xsl:attribute name="title">&lt;? echo kSelectionner;?&gt;</xsl:attribute>
							</input>
							<input type="hidden" name="t_panier_doc[][ID_LIGNE_PANIER]" value="{ID_LIGNE_PANIER}"/>
							<input type="hidden" name="t_panier_doc[][ID_DOC]" value="{ID_DOC}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXT_TITRE]" value="{PDOC_EXT_TITRE}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXT_TCIN]" value="{PDOC_EXT_TCIN}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXT_TCOUT]" value="{PDOC_EXT_TCOUT}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_EXTRAIT]" value="{PDOC_EXTRAIT}"/>
							<input type="hidden" name="t_panier_doc[][PDOC_ID_ETAT]" value="{PDOC_ID_ETAT}"/>
						</label>
					</xsl:when>
					<xsl:otherwise>
						<label class="mos_checkbox">
							<input type="checkbox" name="checkbox{ID_DOC}" value="{ID_DOC}" id="select" >
								<xsl:attribute name="title">&lt;? echo kSelectionner;?&gt;</xsl:attribute>
							</input>
						</label>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:for-each>
	<xsl:if test="$height !='' and $width !='' ">
		<script>
			$j('.resultsMos ').css('height','207px');
			$j('.resultsMos ').css('width','300px');
			$j('.resultsMos .infos_mos.scnd_row').css('top','145px');
			$j('.resultsMos .bgVignetteMos').css('height','175px');
			$j('#resultats').css('margin-left','80px');
		</script>
	</xsl:if>
	<script type="text/javascript">
		
		// fonction d'affichage du hover au dessus de la vignette compatible ios7
		function showHoverVignetteX(event) {
		
			$j("div.scnd_row").css("display","");
			
			if ( this.className.indexOf("resultsMos") &gt;= 0 ){
				$j(this).find(".scnd_row").css("opacity",'1').css("display","flex").css("display","-webkit-flex");
			} else {
				$j(this).parent().nextAll(".scnd_row").css("opacity",'1').css("display","flex").css("display","-webkit-flex");
			}
		}
		if(typeof tablet_border !== 'undefined'){
			$j(document).ready(function(){
				if ($j(window).width()&lt;tablet_border){
				
					// désactivation du lien sur le titre de la vignette
					$j('.mosTitre a').prop("href",'#');
				}
			});
		}
		var vignettes = document.getElementsByClassName("resultsMos");
		
		for (var i = 0 ; i &lt; vignettes.length ; i++ ) {
			vignettes[i].addEventListener("click",showHoverVignetteX,false);
		}
		
		function check_selected_import(){
			var ids = '';
			$j("form#import_aff_form input[name^='checkbox']").each(function(){
				if ($j(this).is(':checked')) {
					if (ids != ''){
						ids = ids+',';
					}
					ids = ids + $j(this).val();
				}
			});
			if (ids == ''){
				alert("Vous devez sélectionner au moins une notice");
				return false;
			}else{
				return true;
			}
		}
		
		
	</script>
</xsl:template>

</xsl:stylesheet>
