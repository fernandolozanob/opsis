var	 _window_position_x, _window_position_y, _window_default_width, _window_default_height, _offset_width, _offset_height, _container_width=360, _offset_left, _offset_top ;
var _minimum_available_width=950, _minimum_available_height=500, _free_lateral_width=0, _free_vertical_height=0;
var myVideo,montage_myVideo,  myPanel, _video_id, _visu_type, ext_tcin, ext_tcout, image_default;
var offset_left_ref =0;
var rm_from_cart_items="";
var _use_QT = false ;
var loading;

var panHasChanged = false ;
var montageHasChanged = true ;

var currentClipExt = "";

var confirmExit = true ;


window.onbeforeunload = function(e){
	if(panHasChanged && confirmExit){
		e = e || window.event;
		if(e){
			e.returnValue = str_lang.kJSConfirmExit;
		}
		return str_lang.kJSConfirmExit;
	}
}


var def_player_options ={
	previewOverTimeline : false ,
	span_time_passed : true,
	enable_streaming : true ,
	resizeCustom : true,
	dynamicControllerFS : true,
	onTrackChange : updateHighlightClip,
	container_id : 'montageContainer',
	hauteur_track : 8,
	hauteur_bg_track: 8,
	css_path : ow_const.kCheminHttp+'/design/css/skin_player/'

};



// Fonction d'info apres changement du select permettant de définir le ratio de référence du montage
function updateRatioMontage (select_ratio){
	// Si ow_main_container => implique container & donc player existe déja
	if($j("#bloc_visionneuse #ow_main_container").get(0)){
		showAlert(str_lang.kJSMontageRatioChange,'alertBox','fade',0,0,3);

	}
}

function loadPlaylist(node,titre) {
	if( $$('#visionneuse div.help_infos').length >0){
		$$('#visionneuse div.help_infos')[0].style.display = "none";
	}

	_video_id="playlist";
	idList="";
	for (_nd=0;_nd<node.childNodes.length;_nd++) {
		if(node.childNodes[_nd].id) {
			_video_id=node.childNodes[_nd].id;
			split=_video_id.split("_");
			idDoc=split[1];
			if(split[0]=="Xext" && split.length>3){
				idDoc=_video_id.substring(5);
			}else if(split[0] == "Xcarton"){
				idDoc=split[0].substring(1)+split[1]+"_"+encodeURIComponent($(_video_id).down("span.clipTitre").innerHTML.replace(/,/g,"&#44;").replace(/<br\s*\/?>/mg,"\n"));
			}

			idList=idList+","+idDoc;
		}
	}

	if(idList){
		idList=idList.substring(1);
		if($j('#bloc_atelier #poster').length>0){
			$j('#bloc_atelier #poster').get(0).src='design/images/wait4video.gif';
		}

		// MS copy de l'objet def_player_options
		var player_options = $j.extend({}, def_player_options);


		player_options['force_ratio_ref'] = eval(document.getElementById('formPanier').PANXML_MONTAGE_RATIO.value);

		// Mise à jour titre visionneuse
		if(document.getElementById('titre_visionneuse')) document.getElementById('titre_visionneuse').innerHTML=titre;
		//Préparation du fichier

		if($j("#bloc_visionneuse #ow_main_container").get(0)){
			if(montage_myVideo && montage_myVideo.destroy){
				montage_myVideo.destroy();
			}else{
				montage_myVideo = null;
			}

			if(typeof(lien_css) != "undefined" ){
				lien_css.parentNode.removeChild(lien_css);
			}
			// document.getElementById("container").innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
			$j("#bloc_visionneuse #montageContainer").get(0).innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
		}


		// loadOraoPlayer(idList,document.getElementById("container").offsetWidth,"_vis",player_options,true,"playlist&decoup=false");
		loadOraoPlayer(idList,$j("#bloc_visionneuse #montageContainer").get(0).offsetWidth,"_vis",player_options,true,"playlist&decoup=false",'montage_myVideo');


	}
}


function downloadPlaylist(node, titre) {
	_video_id="playlist";
	idList="";
	ratio = document.getElementById('formPanier').PANXML_MONTAGE_RATIO.value;


	for (_nd=0;_nd<node.childNodes.length;_nd++) {
		if(node.childNodes[_nd].id) {
			_video_id=node.childNodes[_nd].id;
			split=_video_id.split("_");
			idDoc=split[1];
			if(split[0]=="Xext" && split.length>3){
				idDoc=_video_id.substring(5);
			}else if(split[0] == "Xcarton"){
				idDoc=split[0].substring(1)+split[1]+"_"+encodeURIComponent($(_video_id).down("span.clipTitre").innerHTML.replace(/,/g,"&#44;").replace(/<br\s*\/?>/mg,"\n"));
			}

			idList=idList+","+idDoc;
		}
	}

	if(idList) {
		idList=idList.substring(1);
		// MS encodage supplémentaire du à la nature du fonctionnement de la page downloadMontage
		idList = encodeURIComponent(idList);
		if($('pan_titre').value == ''){
			filename = str_lang.kSansTitre;
		}else{
			filename = encodeURIComponent($("pan_titre").value);
		}
		if(typeof downloadWindow == "undefined" || downloadWindow.closed){
			if(montageHasChanged){
				downloadWindow = window.open("empty.php?urlaction=creaFileMontage&id="+idList+"&ratio="+ratio+"&name="+filename,'_blank',"scrollbar=no,location=no,menubar=no,width=500,height=150");
			}else{
				downloadWindow = window.open("empty.php?urlaction=creaFileMontage&id="+idList+"&ratio="+ratio+"&name="+filename+"&noGen",'_blank',"scrollbar=no,location=no,menubar=no,width=500,height=150");
			}
		}else{
			downloadWindow.focus();
			if(montageHasChanged){
				downloadWindow.triggerNewGen();
			}else{
				downloadWindow.downloaded=false;
				downloadWindow.triggerDownload();
			}
		}
		montageHasChanged = false ;
	}
}

function extMarkIn(){
	if(montage_myVideo){
		currentTC = montage_myVideo.GetCurrentLongTimeCode();
		tcin = montage_myVideo.GetCurrentSelectionBeginning();
		tcout = montage_myVideo.GetCurrentSelectionEnd();

		tcin = currentTC;

		if((tcout) ==  montage_myVideo.movie_options.timecode_IN || montage_myVideo.timecodeToSecs(tcout) == 0 || tcout < tcin || tcin == tcout){
			tcout = montage_myVideo.GetTCMax();
		}
		montage_myVideo.SetSelection(tcin,tcout);
	}
}
function extMarkOut(){
	if(montage_myVideo){
		currentTC = montage_myVideo.GetCurrentLongTimeCode();
		tcin = montage_myVideo.GetCurrentSelectionBeginning();
		tcout = montage_myVideo.GetCurrentSelectionEnd();

		tcout = currentTC;

		montage_myVideo.SetSelection(tcin,tcout);
	}
}


function saveExtrait(){
	//currentClipExt => id de l'élément tel que représenté dans le chutier
	splits = currentClipExt.split("_");
	ext_tc_in = montage_myVideo.GetCurrentSelectionBeginning();
	ext_tc_out = montage_myVideo.GetCurrentSelectionEnd();
	if(ext_tc_in == ext_tc_out){
		return false ;
	}

	newRow = document.getElementById(currentClipExt).cloneNode(true);
	newRow.id = "ext_"+splits[1]+"_"+ext_tc_in+"_"+ext_tc_out;

	// a partir du clone de l'élément du chutier qui représente le document qu'on est en train de redécouper en nouveaux extraits,
	// on update les valeurs qui changent (mais on laisse la vignette et l'ID_DOC puisque ils ne changent pas (la vignette devrait changer une fois que les extraits auront des vignettes représentatives))
	$(newRow).down('input[id^="id_ligne_panier$"]').value = "";
	$(newRow).down('input[id^="extrait"]').value = '1';
	$(newRow).down('input[id^="tcin"]').value = ext_tc_in;
	$(newRow).down('input[id^="tcout"]').value = ext_tc_out;

	newTitle = str_lang.kExtrait+" "+$(newRow).down('span[id^="titre"]').innerHTML ;
	$(newRow).down('input[id^="ext_titre"]').value = newTitle;
	$(newRow).down('span[id^="titre"]').innerHTML = newTitle;

	var clickHandler = function(elem){
		return function(){deleteClip(elem);};
	};

	$(newRow).down('span.delete_clip').onclick = clickHandler(newRow);

	$('bin').insertBefore(newRow,$('last_bin'));

	initBulleTexte(newRow);
	setTimeout(function(){$j(newRow).find("span.clipTitre").dblclick();},250);
	new Draggable(newRow, {ghosting:false,onDrag:dragImage, cloning : true});

	panHasChanged = true ;

}


function saveMontage(){

	if(document.getElementById('formPanier').id_panier.value==""){
		document.getElementById('formPanier').commande.value="CREER";
	}else{
		document.getElementById('formPanier').commande.value="SAVE";
	}
	// if(document.getElementById('formPanier').pan_titre.value=="") document.getElementById('formPanier').pan_titre.value=str_lang.kSansTitre;
	// Sauvegarde timeline
	if(document.getElementById('timeline')){
		node=document.getElementById('timeline');
		str="<track>";
		for (_nd=0;_nd<node.childNodes.length;_nd++) {
			if(clipName=node.childNodes[_nd].id){
				clipName=node.childNodes[_nd].id.substring(1);
				if(clipName.charAt(0) == 'c'){ // cas 'c'arton
					clipTitle = node.childNodes[_nd].getElementsByTagName("span")[2].innerHTML.replace(/<br\s*\/?>/mg,"\\n");
				}else{
					clipTitle = node.childNodes[_nd].getElementsByTagName("span")[2].childNodes[0].nodeValue;
				}
				str+="<clipitem><name>"+clipName+"</name><title><![CDATA["+clipTitle+"]]></title></clipitem>";
			}
		}
		str+="</track>";
		document.getElementById('formPanier').PANXML_MONTAGE.value=str;

	}
	confirmExit = false ;
	// Si on doit supprimer des éléments de la selection :
	if(rm_from_cart_items!=''){
		removeFromCart(document.getElementById('formPanier').id_panier.value,rm_from_cart_items);
	}else{
		document.getElementById('formPanier').submit();
	}
}

function loadMontage() {
	loading = true ;
	droparea=document.getElementById('timeline');
	droparea.innerHTML = "";
	if(document.getElementById('formPanier').PANXML_MONTAGE){
		myXML=importXML(document.getElementById('formPanier').PANXML_MONTAGE.value);
		var nodes=myXML.getElementsByTagName('name');
		if(nodes.length > 0 && $$("#montage div.help_infos")){
			$$("#montage div.help_infos")[0].style.display="none";
		}
		for (u=0;u<nodes.length;u++) {
			clipName=nodes[u].firstChild.nodeValue;
			clipTitle=$(nodes[u]).nextSibling.childNodes[0].nodeValue;
			// alert(clipTitle);
			split=clipName.split("_");
			idDoc=split[1];
			if(split[0] == "carton"){
				addCarton("new",split[1],clipTitle.replace(/\\n/g, "<br />"));
			}else if(split.length >3){
				tcin=split[2];
				tcout=split[3];
				addTimeline( idDoc,"new", tcin, tcout,clipTitle);
			} else addTimeline(idDoc,"new");

		}
	}
	loading = false ;
}

function deleteMontage(){
	if(!confirm(str_lang.kConfirmJSDocSuppression)){return false ;}
	document.getElementById('formPanier').commande.value="SAVE";
	document.getElementById('formPanier').PANXML_MONTAGE.value="";
	confirmExit = false;
	if(rm_from_cart_items!=''){
		removeFromCart(document.getElementById('formPanier').id_panier.value,rm_from_cart_items);
	}else{
		document.getElementById('formPanier').submit();
	}

}



function deleteClip(clip){
	// clip=getSelectedItem(obj);
	if(!clip) {alert(str_lang.kJSMsgSelectClip);return;}
	clip.parentNode.removeChild(clip);
	split=clip.id.split("_");

	if(split[0]=="doc" || split[0]=="ext") {
		idClip="X"+clip.id;
		while(document.getElementById(idClip)){
			document.getElementById(idClip).parentNode.removeChild(document.getElementById(idClip));
		}
		// Si suppression du chutier => on prévoit de supprimer de la sélection si sauvegarde montage
		id_panier_doc = $(clip).down("input[id^='id_ligne_panier']").value;

		showAlert(str_lang.kJSMontageConfirmSupprClip,'alertBox','fade',0,0,3);

		if(rm_from_cart_items == ''){
			rm_from_cart_items = id_panier_doc;
		}else{
			rm_from_cart_items +="$"+id_panier_doc;
		}

	}
	panHasChanged = true ;
	montageHasChanged = true ;
	if(scrollTimeline) scrollTimeline.scroll_display_test();
}

function addClip(obj){
	clip=getSelectedItem(obj);
	if(!clip) {alert(str_lang.kJSMsgSelectClip);return;}
	dropTimeline(clip,document.getElementById("timeline"))
}


function dropTimeline( dragitem,droparea,evt){
	// test pour éviter déclenchement du droptimeline par erreur => Si la modif de position est très faible, elle n'est pas prise en compte
	if( dragitem.parentNode.id=="timeline" &&  Math.abs(offset_left_ref - dragitem.offsetLeft) < (dragitem.offsetWidth)*90/100 ){
		offset_left_ref = 0;
	}else{
		if($$("#montage div.help_infos")[0] && $$("#montage div.help_infos")[0].style.display!="none"){
			$$("#montage div.help_infos")[0].style.display="none";
		}
		split=dragitem.id.split("_");
		if(droparea.id=="timeline"){
			place = "new";
		}else if (typeof(droparea.id) != "undefined"){
			place = droparea;
			droparea = document.getElementById("timeline");
		}

		idDoc=split[1];
		if(split[0]=="montdoc" || split[0]=="ext" || split[0]=="Xmontdoc" || split[0]=="Xext" ){
			$(droparea).highlight({endcolor:"#ffffff",restorecolor:"#ffffff"});
			idDoc=split[1];
			if(split.length >=3){
				tcin=split[2];
				tcout=split[3];

				titre_ext = $j(dragitem).find("span.clipTitre, span[id^='titre']").text();
				addTimeline( idDoc,place, tcin, tcout,titre_ext);
			} else addTimeline(idDoc,place);

		}else if (split[0] == "Xcarton"){
			addCarton(place,split[1]);
		}

		if(split[0]=="Xmontdoc" || split[0]=="Xext" || split[0] == "Xcarton"){
			dragitem.parentNode.removeChild(dragitem);
		}
	}

}


function addCarton(position,num,text){

		if(typeof position == "undefined"){
			if(getSelectedItem('timeline')){
				position = getSelectedItem('timeline');
			}else{
				position = "new";
			}
		}
		if($$("#montage div.help_infos")[0] && $$("#montage div.help_infos")[0].style.display!="none"){
			$$("#montage div.help_infos")[0].style.display="none";
		}
		if(typeof num == "undefined"){
			numCarton = $$('#timeline LI[id^="Xcarton"]').length+1;
		}else{
			numCarton= num;
		}

		blankDiv=document.getElementById('clip$blank');
		myNewRow=blankDiv.cloneNode(true);
		myNewRow.id = "Xcarton_"+numCarton;

		clipitem = getChildById(myNewRow,'clipItem');
		if(typeof text != "undefined"){
			$(clipitem).down('span.clipTitre').innerHTML = text;
		}else{
			if(typeof num == "undefined"){
				$(clipitem).down('span.clipTitre').innerHTML = "Carton "+numCarton;
			}else{
				$(clipitem).down('span.clipTitre').innerHTML = $('Xcarton_'+num).down('span.clipTitre').innerHTML;
			}
		}
		$(clipitem).down('span.bgVignette').style.display = 'none';

		var clickHandler = function(elem){
			return function(){deleteClip(elem);};
		};

		$(clipitem).down('span.delete_clip').onclick=clickHandler(myNewRow);

		myNewRow.style.left="0px";
		myNewRow.style.top="0px";
		myNewRow.style.display="none";
		myNewRow.style.opacity="1.0";
		if (typeof(position) == "undefined" || position=="" || position =="new"){
			document.getElementById('timeline').appendChild(myNewRow);
		} else{
			document.getElementById('timeline').insertBefore(myNewRow,position);
		}


		new Draggable(myNewRow, {constraint : 'horizontal', ghosting : false, revert : true,
			onStart : function(dragitem,evt){
				offset_left_ref = dragitem.element.offsetLeft;
				if(dragitem.element.nextSibling){
					$(dragitem.element.nextSibling).addClassName(" noSlide");
				}
			}.bind(document),
			onEnd : function(){
				$$("#timeline li.noSlide").each(function(elt,idx){$(elt).removeClassName("noSlide")});
			}.bind(document)
		});
		Droppables.add( myNewRow, { onDrop: dropTimeline , hoverclass:'showInsertPlace', includeScrolls : true } );

		setTimeout(function(){initcarton(myNewRow)},5);
		function initcarton(row){
			selectItem(row);
			showElems('timeline');
			if(scrollTimeline) {scrollTimeline.scroll_display_test();}
			initBulleTexte(row);
			if(typeof num == "undefined"){$j(row).find("span.clipTitre").dblclick()};
		}
		if(!loading){
			panHasChanged = true ;
			montageHasChanged = true ;
		}
	}


function addTimeline( idDoc, position, tcin, tcout,titre_ext){
	if(idDoc){
		if(tcin && tcout) {
			oldId="ext_"+idDoc+"_"+tcin+"_"+tcout;
			// Recup info elt source
			src=document.getElementById(oldId);
			try{
				var _img=src.getElementsByTagName('img')[0];
				var vignette=_img.src;
				var titre=_img.title;
			}catch(e){
				vignette='';
				titre='';
			}



			newId = "X"+oldId;
			if(typeof(titre_ext)!="undefined"){
				titre=titre_ext;
			}else{
				titre="["+str_lang.kExtrait+"] "+titre;
			}
		} else {
			idDocSrc="montdoc_"+idDoc;
			// Recup info elt source
			src=document.getElementById(idDocSrc);
			try{
				var _img=src.getElementsByTagName('img')[0];
				var vignette=_img.src;
			}catch(e){
				vignette='';
				titre='';
			}
			var titre=_img.title;
			newId="Xmontdoc_"+idDoc;
		}

		blankDiv=document.getElementById('clip$blank');
		myNewRow=blankDiv.cloneNode(true);
		myNewRow.id=newId;

		clipitem = getChildById(myNewRow,'clipItem');

		$(clipitem).down('img[id^="vignette"]').src = vignette;
		$(clipitem).down('img[id^="vignette"]').title = titre;

		var clickHandler = function(elem){
			return function(){deleteClip(elem);};
		};

		$(clipitem).down('span.delete_clip').onclick=clickHandler(myNewRow);

		getChildById(clipitem,'titre$').innerHTML=titre;
		myNewRow.style.left="0px";
		myNewRow.style.top="0px";
		myNewRow.style.display="none";
		myNewRow.style.opacity="1.0";
		if (typeof(position) == "undefined" || position=="" || position =="new"){
			document.getElementById('timeline').appendChild(myNewRow);
		} else{
			document.getElementById('timeline').insertBefore(myNewRow,position);
		}


		new Draggable(myNewRow, {constraint : 'horizontal', ghosting : false, revert : true,
			onStart : function(dragitem,evt){
				offset_left_ref = dragitem.element.offsetLeft;
				if(dragitem.element.nextSibling){
					$(dragitem.element.nextSibling).addClassName(" noSlide");
				}
			}.bind(document),
			onEnd : function(){
				$$("#timeline li.noSlide").each(function(elt,idx){$(elt).removeClassName("noSlide")});
			}.bind(document)
		});
		Droppables.add( myNewRow, { onDrop: dropTimeline , hoverclass:'showInsertPlace', includeScrolls : true } );

		setTimeout(function(){
			selectItem(myNewRow);
			showElems('timeline');
			if(scrollTimeline) {scrollTimeline.scroll_display_test();}
			initBulleTexte();
			}
		,5);
		if(!loading){
			panHasChanged = true ;
			montageHasChanged = true ;
		}
	}
}


function setTC(inout) {
	if (montage_myVideo && _video_id!='playlist') {
		if (inout==1) montage_myVideo.StopTheVideo();
		TC=montage_myVideo.GetCurrentLongTimeCode();
		if (inout==1) ext_tcout=TC;
		else ext_tcin=TC;
		montage_myVideo.SetSelection(ext_tcin,ext_tcout);
	}
}



// Fonction de remplacement de showPage => on affiche tout les elements quoi qu'il arrive
// on rajoutera par la suite un slider permettant de naviguer dans la timeline pour éviter d'avoir à utiliser un pager
function showElems(id){
	if(document.getElementById(id)){
		myDiv=document.getElementById(id);
		items=myDiv.getElementsByTagName('li');

		// Affichage page
		for (_nd=0;_nd<items.length;_nd++) {

			items[_nd].style.display="inline-block";
		}

	}
}

// MS fonction appelée comme callback de la f° ChangeTrack du OWH_player => devrait permettre de highlight l'extrait en cours de lecture
function updateHighlightClip(idx){
	try{
		if(idx < $j("#timeline LI").length && !$$('#timeline LI ')[idx].hasClassName("selectedClip")){
			selectItem($$('#timeline LI ')[idx]);
			if(scrollTimeline.active){
				// permet d'updater le scroll pour garder l'élément en cours de lecture visible dans la représentation de la timeline
				scrollTimeline.sliderVar.setValue(($j("#timelineView li.selectedClip")[0].offsetLeft+($j("#timelineView li.selectedClip").width()/2))/$j("#timeline").width());
			}
		}
	}catch(e){}
}


function selectItem(obj){
	var tf=($(obj).hasClassName("selectedClip"));

	unselectItem(obj.parentNode);

	if(!tf) $(obj).addClassName("selectedClip");
}
function unselectItem(obj){
	if( obj == null ){return ;}
	lis=obj.getElementsByTagName('li');
	for (_u=0;_u<lis.length;_u++) {
		if($(lis[_u]).hasClassName("selectedClip")) {$(lis[_u]).removeClassName('selectedClip');}
	}
}

function getSelectedItem(obj){
	if(typeof obj !=='object' && obj!=null){
		obj = $(obj);
	}
	lis=obj.getElementsByTagName('li');
	for (_u=0;_u<lis.length;_u++) {
		if($(lis[_u]).hasClassName("selectedClip")) return lis[_u];
	}

}


// == fonctions issues montage.xsl

function dropItem( dragitem,droparea,finished){
	if(dragitem.parentNode.parentNode.parentNode.id==droparea.id) return;
	split=dragitem.id.split("_");
	idDoc=split[1];

		var branch = tree.getBranchByIdObj(droparea.id);
		if(branch) {
			idCart=branch.getId();
			add2folder(idCart,idDoc);
		}

}



function dropVideo( dragitem,droparea){
	playClip(dragitem);
}

function playClip(clip){
	if(!clip) {alert(str_lang.kJSMsgSelectClip);return;}

	if($$('#visionneuse div.help_infos').length >0){
		$$('#visionneuse div.help_infos')[0].style.display = "none";
	}

	video_id = clip.id.split("_")[1];
	// MS copy de l'objet def_player_options
	var player_options = $j.extend( {}, def_player_options);

	if(clip.id.split("_")[0] == "ext" ){
		if(clip.id.split("_")[2]){
			player_options['timecode_IN'] = clip.id.split("_")[2];
		}
		if(clip.id.split("_")[3]){
			player_options['timecode_OUT'] = clip.id.split("_")[3];
		}
	}else{
		player_options['timecode_IN'] = null;
		player_options['timecode_OUT'] = null;
	}
	currentClipExt = clip.id ;
	player_options['onExtSave'] = saveExtrait;
	player_options['extsBtnsOnStart'] = true ;
	player_options['extsBtnsDisabled'] = false ;
	player_options['selectionHidden'] = false ;
	player_options['container_id'] = 'montageContainer' ;

	var titre=getChildById(clip,'titre$').innerHTML;
	if(document.getElementById('titre_visionneuse')) document.getElementById('titre_visionneuse').innerHTML=titre;

	if($j("#bloc_visionneuse #ow_main_container").get(0)){
		if(montage_myVideo && montage_myVideo.destroy){
			montage_myVideo.destroy();
		}else{
			montage_myVideo = null;
		}
		if(typeof(lien_css) != "undefined" ){
			lien_css.parentNode.removeChild(lien_css);
		}
		// document.getElementById("container").innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
		$j("#bloc_visionneuse #montageContainer").get(0).innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
	}
	// loadOraoPlayer(video_id, document.getElementById("container").offsetWidth,"_vis",player_options,true,"document");
	loadOraoPlayer(video_id, $j("#bloc_visionneuse #montageContainer").get(0).offsetWidth,"_vis",player_options,true,"document",'montage_myVideo');


}

function dropSup( dragitem,droparea){
	$(droparea).highlight();
	dragitem.parentNode.removeChild(dragitem);
	split=dragitem.id.split("_");

	if(split[0]=="doc" || split[0]=="ext") idClip="X"+dragitem.id;
	while(document.getElementById(idClip)){
		document.getElementById(idClip).parentNode.removeChild(document.getElementById(idClip));
	}
}



function removeFromCart(idCart,ids_panier_doc) {
	if(idCart && ids_panier_doc){ return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&commande=remove&typeItem=pdoc&items='+ids_panier_doc,'document.getElementById("formPanier").submit');}
}



// fonction de modification des titres des extraits via infobulle.
function initBulleTexte(element){
	if(typeof(element) == "undefined"){
		element = $j('span.clipTitre');
	}else if (!$j(element).hasClass('clipTitre')){
		element = $j(element).find('span.clipTitre');
	}

	// pour le refresh, on prevoit d'unbind ces evenements
	element.unbind("dblclick");
	element.dblclick(function(event){
		e = event ;
		// si on est dans le cas d'un document, on ne permet pas la modification ==> Il faudra surement améliorer la visibilité de ce cas la.
		if($j(e.currentTarget).parents('li').attr('id').indexOf("ext")!=-1 || $j(e.currentTarget).parents('li').attr('id').indexOf("carton")!=-1 ){
			$j('#bulle_edit').removeAttr('readonly');
		}else{
		// sinon on permet la modification du champ texte
			$j('#bulle_edit').attr('readonly','true');
		}
		$j('#bulle_close').unbind('click');
		// déplacement de l'infobulle
		if(typeof e.pageY !="undefined" && typeof e.pageX !="undefined" ){
			_x = e.pageX;
			_y = e.pageY;
		}else{
			_x = $j(e.currentTarget).offset().left;
			_y = $j(e.currentTarget).offset().top;

			_x+=($j(e.currentTarget).width()/2);
			_y+=($j(e.currentTarget).height()/2);

		}

		_x -= $j("#panFrame").offset().left;
		_y -= $j("#panFrame").offset().top;



		$j('#bulle_hover').css('top', (_y));	// déplacements en fonction de la souris
		$j('#bulle_hover').css('left',(_x));

		// récupération dans l'infobulle de la valeur courante du titre de l'extrait / document
		if(e.currentTarget.childNodes[0]){
			// $j('#bulle_edit').val(e.currentTarget.childNodes[0].nodeValue.replace(/<br\s*\/>/mg,"\n") );
			$j('#bulle_edit').val(e.currentTarget.innerHTML.replace(/<br\s*\/?>/mg,"\n") );
		}else{
			$j('#bulle_edit').val("");
		}
		var target = e.currentTarget;
		//on prépare l'évenement à déclencher à la fermeture de l'infobulle (valeur modifiée ou non),
		function close_bulle(event){
			$j('#bulle_hover').css('display','none');
			// si on est dans le cas d'un extrait on poursuit
			if($j(target).parents('li').attr('id').indexOf("ext")!=-1 ){
				// update de la cible
				target.innerHTML = $j('#bulle_edit').val();
				// update de tout les champs qui doivent rester synchrone avec le titre
				if($j(target).parents('li').hasClass('fromBin')){
					// cas d'une modif sur le chutier, on update le champ caché ext_titre , on update ensuite toutes les occurences de ce clip dans la timeline
					$j(target).siblings('input[id^="ext_titre"]').val($j('#bulle_edit').val());
					$j('#X'+$j(target).parents('li').attr('id').replace(/(:)/g,'\\$1')).find("span.clipTitre").text(target.innerHTML);
				}else if($j(target).parents('li').attr('id').charAt(0)=="X"){
					// cas d'une modif d'un clip dans le chutier, update de l'affichage du titre dans le chutier, du champ caché ext_titre dans le chutier et des autres occurences de ce clip dans al timeline
					$j('#'+$j(target).parents('li').attr('id').replace(/(:)/g,'\\$1')).find("span.clipTitre").text(target.innerHTML);
					$j('#'+$j(target).parents('li').attr('id').substring(1).replace(/(:)/g,'\\$1')).find("span.clipTitre").text(target.innerHTML);
					$j('#'+$j(target).parents('li').attr('id').substring(1).replace(/(:)/g,'\\$1')).find("input[id^='ext_titre']").val(target.innerHTML);
				}
			}else if ($j(target).parents('li').attr('id').indexOf("carton")!=-1){
				target.innerHTML = $j('#bulle_edit').val().replace(/(\n)/g ,'<br />').replace(/'/g,"’");
				montageHasChanged = true ;
			}
			panHasChanged = true ;
		}
		//
		$j('#bulle_close').unbind('click').click(close_bulle);
		$j('#bulle_edit').unbind('blur').blur(close_bulle);

		// apparition de la bulle et focus sur le champ texte de la bulle
		$j('#bulle_hover').css('display','block');
		$j('#bulle_edit').focus();
	});
}

function triggerLoadPanier(){
	setTimeout(function(){
		flag_reload_current_pan=true;
		loadPanier(id_current_panier,null,null,'montage');
	},400);
}


