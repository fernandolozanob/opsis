<?php echo "<?xml version=\"1.0\" encoding=\"utf-8\"?".">"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<? $img = urldecode($_GET['img']); 
   $txt=trim(urldecode($_GET['txt']));
?>

<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/iframe.dwt.php" codeOutsideHTMLIsLocked="false" -->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


		<? if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'safari')>0 && $_GET['reload']!='1') {
				echo "<meta http-equiv='refresh' content='0.1; url=popupImage.php?img=".urlencode($img)."&txt=".urlencode(str_replace("'","\'",$txt))."&reload=1 '>"; }
		?>
		<title>Image <?=(!empty($txt)?" : ".utf8_decode($txt):"")?></title>

		<script language="javascript" type="text/javascript">
		function checksize(x,y) { 
			var offset=0;

			if (navigator.userAgent.toLowerCase().indexOf('safari')>0) offset=100;
			
			y_margin=<?=(!empty($txt)?"85":"50")?>;
				if (x<screen.availWidth-20-offset && y<screen.availHeight-y_margin-offset) {
						window.resizeTo(x+10,y+y_margin); window.moveTo(0,0); window.focus();
				} else {
						r=x/y;
						myimg=document.getElementById('img0');
						if (x>screen.availWidth-20-offset) {myimg.width=screen.availWidth-20-offset;myimg.height=document.images[0].width/r;}
						if (y>screen.availHeight-y_margin-offset) {myimg.height=screen.availHeight-y_margin-offset;myimg.width=document.images[0].height*r;}
						window.resizeTo(myimg.width+10,myimg.height+y_margin); window.moveTo(0,0);window.focus();
				}

		}
		</script>
	</head>

	<body   leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>
		<table width='100%' border='0' cellspacing='0' cellpadding='0' height='100%'>
			<tr>
				<td valign='middle' align='center'>
					<img src='<?=htmlspecialchars($img, ENT_QUOTES)?>' id='img0' onload='checksize(this.width,this.height)' border=0 alt='Mon image'>
				</td>
			</tr>
			<? if (!empty($txt)) { ?><tr height='20'><td align='center' style='font-family:arial;font-size:12px;color:black;'><?=utf8_decode($txt)?></td></tr><? } ?>
		</table>
	</body>
</html>