<?
define("kA","to");
define("kAcces","Access");
define("kAccesLogin","Please log in again to access this page.");
define("kAccesReserve","Private Access");
define("kAccueil","Home");
define("kAchat","Sales & Loans");
define("kActualiser","Refresh");
define("kAdministration","Administration");
define("kAdresse","Address");
define("kAdresseFacturation","Invoice address");
define("kAdresseLivraison","Delivery address");
define("kAdressePerso","Private address");
define("kAffichage","Presentation");
define("kAffichAlphabetiq","alphabetical");
define("kAffichHierarchie","hierarchical");
define("kAide","Help");
define("kAjouter","Add");
define("kAjouterAcc","Add a joined document");
define("kAjouterPanier","Add to cart");
define("kAjouterImage","Add a picture");
define("kAjouterLigne","Add a line");
define("kAjouterViaUpload","Add by upload");
define("kAlerteMail","Email me");
define("kAncienneCote","Old cote");
define("kAnnuler","Cancel");
define("kAPropos","About");
define("kAu","to");
define("kAucunResultat","No result");
define("kAudience","Audience");
define("kAuteur","Author");
define("kAuteurScientifique","Scientific author");
define("kAuthentification","Authentification");
define("kAutresVersions","Other versions");
define("kAutreTitre","Other Title");
define("kBienvenue","Welcome");
define("kBiographie","Biography");
define("kBonjour","Hello");
define("kChercher","Search");
define("kCodeLangage","Language Code ");
define("kCodePostal","Zip code");
define("kCollection","Collection");
define("kCommande","Order form");
define("kCommander","Order");
define("kCommandes","Orders");
define("kCommanditaire","Sleeping Partner");
define("kComment","Comments");
define("kCommission","Commission of intervention");
define("kCompression","Compression");
define("kConditionExploitation","Conditions of use");
define("kConfirmerAbandon","All changes will be lost ! Continue ?");
define("kConfirmerSuppression","Do you really want to delete this item ?");
define("kConnexion","Login");
define("kConsultationSeule","Read only");
define("kConsulter","Consult");
define("kContact","Contact");
define("kContenuPanier","Cart content");
define("kContributeur","Contributor");
// DOUBLON define("kCoordonnees","Address");
define("kCoordonneesPersonnelles","Personal address");
define("kCopie","Copy");
define("kCopyright","Copyright");
define("kCorpus","Selection");
define("kCote","Number"); // CNRS
define("kCoteOriginal","Cote of original");
define("kCouleur","Color");
define("kCreditSite","Design");
define("kCree","created");
define("kCreerNotice","Create item");
define("kCreerNoticeAudio","Create audio item");
define("kCreerNoticePhoto","Create photo item");
define("kCreerNoticeVideo","Create video item");
define("kCritere","Term");
define("kDate","Date");
define("kDateCommande","Date of order");
define("kDateCopie","Date of copy");
define("kDateCreation","Date of ceation");
define("kDateDiffusion","Date of broadcasting");
define("kDateDuree","Date and duration");
define("kDateEmprunt","Borrowing date");
define("kDateEmpruntFormat","Date of loan (dd/mm/yyyy)");
define("kDateLivraisonSouhait","Wished delivery date");
define("kDateModification","Date of update");
define("kDateNaissance","Date of birth");
define("kDateProd","Date of production");
define("kDateProduction","Date of production");
define("kDatePVDebut","Date begining shooting");
define("kDatePVFin","Date end shooting");
define("kDateRetour","Due date");
define("kDateRetourFormat","Due date format(dd/mm/yyyy)");
define("kDates","Dates");
define("kDateSaisie","Date of entering");
define("kDeconnexion","Log Out");
define("kDemandeLogin","Ask for login");
define("kDemandeLoginTexte","Please fill in your subscribe mail.<br/> A mail containing your identifiers will be sent to you.");
define("kDernierIntervenant","Last user");
define("kDescripteurs","Keywords");
define("kDescripteursContextuels","Contextual keywords");
define("kDescription","Description");
define("kDeselectionnerTout","Deselect all");
define("kDestination","Shipping area");
define("kDestinationFrance","France");
define("kDestinationEurope","Europe CEE");
define("kDestinationInternational","Countries outside CEE");
define("kDiffuseur","Brodcaster");
define("kDiffusion","Brodcasting");
define("kDiscipline","Discipline");
define("kDivers","Other");
define("kDocNum","Document digitized ?");
define("kDocument","Document");
define("kDocumentAcc","Document joined");
define("kDocumentaliste","Documentalist");
define("kFils","Children");
define("kDocumentsFils","Children documents");
define("kDocumentsLies","Linked documents");
define("kDocumentsParents","Father document");
define("kDocuments","Documents");
define("kDomaine","Domain");
define("kDomaineActivite","Domain of activity");
define("kDroits","Rights");
define("kDu","From");
define("kDupliquer","duplicate");
define("kDuree","Duration");
define("kEmissions","Programs");
define("kEmprunteur","Borrower");
define("kEncodage","Encoding");
define("kEnLigne","On line");
define("kEnregistrer","Save");
define("kEntre","Between");
define("kEnvoyer","Send");
define("kEnvoyerMailUtilisateur","Send an e-mail to the user ?");
define("kErreurChampsOblig","This field is required");
define("kErreurChampsObligJS","Some fields are mandatory");
define("kErreurCoteExistant","This cote already exists");
define("kErreurCoteMat","This material already exists");
define("kErreurCoteStoryboard","This storyboard already exists");
define("kErreurCreerRepertoire","Unable to create repertory");
define("kErreurEMailExiste","This e-mail already exists");
define("kErreurEMailInvalide","This e-mail is invalid");
define("kErreurFatale","An error happened on this page.<br/>Please contact the administrator.");
define("kErreurFichierExistant","This file already exists");
define("kErreurFichierExiste","This file already exists");
define("kErreurFondsExistant","This collection already exists");
define("kErreurGroupeExistant","This group already exists");
define("kErreurImageExiste","This picture already exists");
define("kErreurLexiqueExiste","This term already exists");
define("kErreurLoginExiste","this login already exists");
define("kErreurMotDePasseIdentique","The passwords are not identical.");
define("kErreurPageNotFound","Page not found.");
define("kErreurNbDeb","Your datas contain");
define("kErreurNbFin","mistake(s)");
define("kErreurNbSupport","Please indicate the number of delivery material");
define("kErreurRenomerRepertoire","Unable to rename repertory");
define("kErreurRequeteExistant","A query with same title already exists");
define("kErreurSequence","This sequence already exists");
define("kErreurSocieteOblig","Please fill in your organisation or company.");
define("kErreurSupportLiv","Please indicate the delivery material");
define("kErreurSupprimerFichier","Unable to delete file");
define("kErreurUpload","Upload failed");
define("kErreurUploadCopie","Unable to copy to ");
define("kErreurUploadInterdit","File's type is not allowed.");
define("kErreurUploadRemplacer","Unable to overwrite file ");
define("kErreurUploadTaille","File's size is 0 octet.");
define("kErreurUploadTailleMax","Maximum file's size is  ");
define("kEt","And");
define("kEtape","Step");
define("kEtat","State");
define("kEtatDocument","State of document");
define("kEtatMateriel","State of material");
define("kEtatPan","State of cart");
define("kEvenement","Event");
define("kExporter","Export");
define("kExporterFichier","Export in a file");
define("kExtrait","Extract");
define("kExtraits","Extracts");
define("kFax","Fax");
define("kFermer","Close");
define("kFichiers","Files");
define("kFonction","Function");
define("kFonds","Collection");
define("kFondsPere","Collection relative");
define("kFondsSaisie","Collection modification");
define("kFormat","Format");
define("kFormatDateCourt","%Y/%m/%d");
define("kFormatMateriel","Format material");
define("kForme","Form");
define("kFormulaireCommande","Order form");
define("kFraisExpedition","Shipping rate");
define("kFusionDialogue","Warning : a similar term already exists in the lexicon!\\n\\nDo you want to merge this term in the other?");
define("kGenealogie","Genealogy");
define("kGenererAutresVersions","Create other versions");
define("kGenerique","Credits");
define("kGenre","Genre");
define("kGestion","Gestion");
define("kGestionCommandes","Orders management");
define("kGestionFonds","Collection management");
define("kGestionGroupes","Group management");
define("kGestionLexique","Lexicon management");
define("kGestionMateriels","Materials management");
define("kGestionPersonnes","Gestion des personnes");
define("kGestionStoryboards","Storyboards management");
define("kGestionTables","System table management");
define("kGestionUtilisateurs","Users management");
define("kGestionValeurs","Table values management");
define("kGroupe","Group");
define("kGroupeSaisie","Group modification");
define("kHistorique","History");
define("kHistoriqueComplet","Complete history");
define("kHistoriqueSession","Session history");
define("kId","#");
define("kID","#");
define("kIdentifiant","Identifier");
define("kIdentification","Identification");
define("kImage","Image");
define("kImages","Images");
define("kImageur","Imager");
define("kImArch","Archives Images");
define("kImport","Import");
define("kImportVideo","Import video");
define("kImportXML","Import XML");
define("kImprimer","Print");
define("kImprimerCatalogue","Print documents");
define("kImprimerNotices","Print documents");
define("kIndex","Index");
define("kIndexation","Indexation");
define("kInfosCompl","Additional Information");
define("kInfosTechniques","Technical Information");
define("kInscription","Registration form");
define("kInscriptionValidee","Your subscription is validated.<br/>You can now log in.");
define("kInscriptionPersonnelle","Personal inscription, not linked to a company");
define("kInscrivezVous","Sign-In");
define("kIntervenants","Intervenors");
define("kJuridique","Juridique");
define("kLabo","Labo");
define("kLaboratoire","Laboratory");
define("kLancementVideo","Launching video");
define("kLangue","Language");
define("kLangues","Languages");
define("kLexique","Lexicon");
define("kLienDocumentMateriel","Link between document and material");
define("kLieu","Place");
define("kLieuDeces","Place of death");
define("kLieuNaissance","Place of birth");
define("kLieuNegatifs","Place for negatives");
define("kLieuTournage","Place of shooting");
define("kListe","List");
define("kListeCommandes","List of orders");
define("kListeDeValeur","Values Lists");
define("kListeDocuments","List of documents");
define("kListeDocumentAcc","List of joined documents");
define("kListeDocumentsLies","List of linked documents");
define("kListeFonds","List of collections.");
define("kListeFondsPrivileges","List of collections and associated privileges.");
define("kListeGroupes","List of user's group");
define("kListeLexiqueAssocie","List of associated terms from lexicon.");
define("kListeMateriels","List of materials");
define("kListePaniers","List of carts");
define("kListePersonnes","List of persons");
define("kListeSequences","List of sequences");
define("kListeStoryboards","List of storyboards");
define("kListeTables","List of tables");
define("kListeUsagers","List of users");
define("kLivraison","Delivery");
define("kLivrer","Deliver");
define("kLogin","Login");
define("kLoginDemande","Expected login");
define("kMail","Mail");
define("kMaster","Master");
define("kMateriel","Material");
define("kMateriels","Materials");
define("kMaterielsLies","Matériels liés");
define("kMaterielSorti","Material out");
define("kMaterielStockage","Storage of materials");
define("kMedia","Media");
define("kMesCommandes","My orders");
define("kMesRecherches","My queries");
define("kMetrage","Length");
define("kMisAJour"," updated");
define("kModeDiffusion","Distribution");
define("kModePaiement","Payment by ");
define("kModePaiementCheque","Cheque");
define("kModePaiementCR","Bank transfer");
define("kModePaiementCB","Credit card");
define("kModePaiementVISA","VISA Credit card");
define("kModePaiementEUROCARD_MASTERCARD","Eurocard / Mastercard");
define("kModePaiementE_CARD","E-carte bleue");
define("kModifier","Modify");
define("kMonProfil","My account");
define("kMonPanier","My cart");
define("kMosaique","Mosaic");
define("kMotCle","Keyword");
define("kMotCleGeographiqe","Geographical keyword");
define("kMotDePasse","Password");
define("kMotDePasseOublie","Forgot password ?");
define("kMotDePasseRepeter","Confirm");
define("kMsgBienvenue","Your inscription is nowadays valid"); //CNRS
define("kMsgContinue","Do you want to confirm ?");
define("kMsgCoordonneesPerso","Enter your personal address"); // CNRS
define("kMsgCoordonneesSociete","Enter your professional address"); // CNRS
define("kMsgInscription","You are already registered, please enter:"); // CNRS
define("kMsgMailEnvoye","Your mail will be send at this address: ");
define("kMsgMauvaiseIdentification",kAccesReserve.". You have to identify yourself or to subscribe");
define("kMsgMauvaisLogin","Bad login or password.");
define("kMsgPanierVide","You don't have any item in your cart");
define("kMsgPanierMauvaisType","Warning some items are not avaiable for this type of order.");
define("kMsgPasDeRequetes","You don't have any saved query");
define("kMsgPossibiliteConnexion","For this demo, you can connect immediately");
define("kMsgQTcheck","Your browser doesn't support scripting, so you can't check for QuickTime.");
define("kMsgQTversion","You need QuickTime ");
define("kMsgSelectItem","Please select item you want to order");
define("kMsgUsagerNonExistant","This user does not exist, your query has been sent to the web master.");
define("kNature","Nature");
define("kNbDocument","Nb elements");
define("kNbElements","Nb elements");
define("kNbEpisodes","Nb Episodes");
define("kNbExemplaires","Qty");
define("kNbExemplairesCourt","Nb");
define("kNbExtrait","Nb Extracts");
define("kNbFonds", "Nb Collections");
define("kNbGroupes","Nb Groups");
define("kNbImages","Nb Pictures");
define("kNbUsagers","Nb Users");
define("kNbSemainesProjections","N weeks (video)/N Showings (film)");
define("kNo","#");
define("kNom","Name");
define("kNombre","Number");
define("kNombreDocumentsLies","Number of linked documents");
define("kNon","No");
define("kNoteInterne","Internal note");
define("kNote","Note");
define("kNotes","Notes");
define("kNotesTitre","Title notes");
define("kNotice","Item");
define("kNoticesLiees","Linked items");
define("kNoUserAndDoc","Exclude administrators and documentalists");
define("kNouveau","New");
define("kNouveauLexique","New lexicon entry");
// DOUBLON define("kNouveauMat","New material");
define("kNouveauMateriel","New material");
define("kNouveauPers","New person");
define("kNouvellePersonne","New person");
define("kNouvelleSequence","New sequence");
define("kNumero","Number");
define("kNumeroCommande","Order number");
define("kNumeroNotice","Item number");
define("kNumeroTransaction","Transaction number");
define("kNumeroTVA","European VAT number");
define("kObjet","Object");
define("kOnline","online");
define("kOrdre","Order");
define("kOrganisation","Company");
define("kOriginal","Original");
define("kOrigine","Origine");
define("kOu","Or");
define("kOui","Yes");
define("kPages","Page(s)");
define("kPaiementSucces","Thank for you buy.<br/>You can print the following details of your order.");
define("kPaiementRefuse","Payment has been refused.<br/>The ordered programs have been replaced in your cart.");
define("kPaiementAnnule","Payment has been cancelled.<br/>The ordered programs have been replaced in your cart.");
define("kPanier","Cart");
define("kPanierAjoutDoc","Add in...");
define("kPanierEnCours","Cart in course of treatment");
define("kPanierFrais","Shipping fees");
define("kPanierNouveau","New selection");
define("kPanierTotalHT","Total (VAT-free)");
define("kPanierTotalProduits","Total order");
define("kPanierTotalTTC","Total (VAT inc.)");
define("kPanierTraite","Cart treated");
define("kPanierTransfertDoc","Move to...");
define("kPanierVide","The cart is empty");
define("kParent","Generic");
define("kPasHistorique","No history");
define("kPatientez","Please wait...");
define("kPays","Country");
define("kPDM","Market share");
define("kPersonnalite","Personnalities");
define("kPersonne","Person");
define("kPersonnePhysique","Physical person");
define("kPersonneMorale","Moral person");
define("kPhoto","Image");
define("kPlus","More");
define("kPortable","GSM");
define("kPortablePerso","Personnal GSM");
define("kPrecision","Precision");
define("kPremieresLettres","Search : ");
define("kPrenom","First name");
define("kPresentation","Presentation");
define("kPret","Loan");
define("kPrivilege","Privilege");
define("kPrix","Cost");
define("kPrixHT","Cost (VAT-free)");
define("kProcAudiovisuelle","Audiovisual process");
define("kProducteur","Producer");
define("kProducteurAutre","Others producers");
define("kProduction","Production");
define("kProfil","Profil");
define("kPublic","Public");
define("kRaisonSociale","Organization");
define("kRealisateur","Director");
define("kRecherche","Search");
define("kRechercheAvancee","Advanced search");
define("kRechercheExperte","Expert search");
define("kRechercheFonds","Search in a collection");
define("kRechercheSimple"," Simple search");
define("kRechercheTouteDate","Any date");
define("kRechercheToutesDates","Any dates");
define("kRechercheTypeRechFullText","FullText");
define("kRechercheTypeRechFullTextEtendu","Extended FullText");
define("kRechercheTypeRechSelect","Among");
define("kRechercheTypeRechExact","Exact");
define("kRechercheTypeRechEgal","=");
define("kRecomp","Rewards");
define("kRediffusion","Rediffusions");
define("kReinitialiser","Reinitiate");
define("kReferenceMateriel","Reference of material");
define("kRefStoryboard","Reference of storyboard");
define("kRefMat","Reference of material");
define("kRemarques","Remarks");
define("kRequete","Query");
define("kResultats","Result(s)");
define("kResultatsParPage","Results per page");
define("kParPage","per page");
define("kResume","Summary");
define("kResumeCatalogue","Summary catalogue");
define("kRetour","Back");
define("kRetourPanier","Back to Shopping Cart");
define("kRetourRecherche","Back to search");
define("kReversement","Transfer");
define("kRole","Role");
define("kSaisie","Capture");
define("kSanstitre","Untitled");
define("kSauf","Not");
define("kSauvegarderRequete","Save query");
define("kSessionExpiree","Session expired !");
define("kSelection","Selection");
define("kSelections","Selections");
define("kSelectionner","Select");
define("kSelectionnerTout","Select all");
define("kSelectionnerDocument","Select an item");
define("kSelectionnerStoryboard","Select a storyboard");
define("kSelectionnerTerme","Select a term");
define("kSequence","Sequence");
define("kSequences","Sequences");
define("kSociete","Organisation / Company");
define("kSon","Sound");
define("kSonorisation","Sound");
define("kSorti","Out");
define("kSortieMateriel","Material mouvement");
define("kSource","Source");
define("kSousTitre","Sub-title");
define("kSpecifiques","Specific");
define("kStandard","Standard");
define("kStatistiques","Statistics");
define("kStatut","Status");
define("kStockage","Storage");
define("kStoryboard","Storyboard");
define("kSupport","Supports");
define("kSupportDiffusion","Delivery support");
define("kSupportLivraison","Delivery support");
define("kSupportLivraisonCourt","Support");
define("kSupportOriginal","Original Support");
define("kSupprimer","Delete");
define("kSupprimerSelection","Delete selected");
define("kSupprimerTout","Delete all");
define("kSynonyme","Synonym");
define("kSynonymePreferentiel","Prefered synonym");
define("kTable","Table");
define("kTabule","Tabulated");
define("kTarifs","Fares");
define("kTC","TC");
define("kTCdst","TC Dest.");
define("kTCin","TC in");
define("kTCout","TC out");
define("kTCsrc","TC Source");
define("kTelephone","Phone");
define("kTelephonePerso","Personal phone");
define("kTerme","Term");
define("kTermeEtat","State Term");
define("kTermesAssocies","Associated terms");
define("kTermesFils","Children term");
define("kTermeGenerique","Main term");
define("kTermesSpecifiques","Specific terms");
define("kTexte","Text");
define("kTextes","Texts");
define("kTheme","Theme");
define("kTimecode","Time-code");
define("kTitre","Title");
define("kTitreCol","Collection title");
define("kTitreExtrait","Extract title");
define("kTitreOriginal","Original title");
define("kTitreParent","Title of father document");
define("kTitres","Titles");
define("kTournage","Shooting");
define("kTous","All");
define("kTousDocuments","All documents");
define("kTousDomaines","All domains");
define("kTousEvenements","All events");
define("kTousFonds","All collections");
define("kTousGenres","All genres");
define("kTousLieux","All places");
define("kTousMotsCles","All keywords");
define("kTousThemes","All themes");
define("kToutesPersonnalites","All personnalities");
define("kType","Type");
define("kTypeCommande","Order type");
define("kTypeDocument","Type of document");
define("kTypeEmission","Type of program");
define("kTypeUtilisation","Type of use");
define("kUsager","User");
define("kUsagerCrea","Created by");
define("kUsagerModif","Last modified by");
define("kUsagerSaisie","User editing");
define("kValeur","Value");
define("kValeurs","Values");
define("kValider","Submit");
define("kVersion","Version");
define("kVersionInternationale","Internationale version");
define("kVille","City");
define("kVisa","Author aimed");
define("kVisionner","Viewing");
define("kVotreLogin","Your login");
define("kVotreMotDePasse","Your password");
define("kVotreRecherche","Your query");
define("kWait","Please wait...");

define("kAnnee","Year");
define("kAppartientA","Belongs to");
// DOUBLON define("kCritere","Criteria");
define("kDateDebut","Start date");
define("kDateFin","End date");
define("kErrorNoLogin","Please log in.");

// DOCUMENT
define("kErrorAccesProfil","You are not allowed to modify this user.");
define("kErrorCreationDoc","The document could not be created.");
define("kErrorSauveDoc","The document could not be saved.");
define("kErrorSauveDocLex","The associated lexicon entries could not be saved.");
define("kErrorSauveDocVal","The associated values could not be saved.");
define("kErrorSupprDoc","The document or version could not be deleted");
define("kErrorSauveDocLien","The link between documents could not be saved.");
define("kErrorSauveAutreVersion","The other versions could not be saved.");
define("kErrorDocSeqExisteDeja","This sequence already exists.");
define("kErrorCreationDocSeq","This sequence could not be created.");
define("kErrorSauveDocSeq","This sequence could not be updated.");
// DOUBLON define("kErrorSauveDocMat","This link to a material could not be updated.");
define("kErrorCoteExistant","This cote is already used by another document.");
define("kSuccesDocSauve","Document saved.");
define("kErrorDocImpossibleAssocierImageur","Unable to attach this storyboard to this document.");
define("kSuccesSupprDoc","Documents have been sucessfully deleted.");
define("kErrorDocExistePas","This document doesn't exists");
define("kErrorDocNoId","Missing ID");
define("kErrorDocSauveSinequa","Error when saving in Sinequa");

define("kConfirmJSDocSuppression","Do you confirm delete ?");
define("kConfirmJSDocDuplication","Do you confirm duplicate ?");
define("kConfirmJSDocSupprVersion","Do you confirm to delete this version ?");
define("kMsgSauvegardeModif","Would you like to save changes ?");

//LEXIQUE
define("kErrorLexiqueExisteDeja","This entry already exists.");
define("kErrorLexiqueSauve","This entry could not be saved.");
define("kErrorLexiqueCreation","This entry could not be created.");
define("kErrorLexiqueVersionSauve","These versions could not be saved.");
define("kErrorLexiqueCreationSyno","This synonym could not be created.");
define("kErrorLexiqueSauveSyno","This synonym could not be saved.");
define("kErrorLexiqueAttachChildren","The children entries could not be linked.");
define("kErrorLexiqueCreationAsso","The associated word could not be created.");
define("kErrorLexiqueSauveAsso","The associated word could not be saved.");
define("kErrorLexiqueDetacheTous","The children could not be detached from this entry.");
define("kErrorLexiqueSuppr","This entry could not be deleted.");
define("kWarningLexiqueSynonymePreferentiel","Warning, this term has already a prefered synonym.<br/>If you want to modify data, please follow this link.");
define("kConfirmLexiqueFusion","Do you want to merge the two terms ?");
define("kErrorLexiqueFusionNoId","Impossible to merge the terms, missing identifiers");
define("kErrorLexiqueFusion","Impossible to merge the terms");
define("kLexiqueNbDocsLies","Linked documents");
define("kLexiqueNbPersLies","Linked persons");
define("kLexiqueTermeSansLien","No links");
define("kSuccesLexiqueDetacheLien","Links successfully cancelled.");
define("kValide","Validated");
define("kCandidat","Candidate");

// USAGER
define("kErrorUsagerExisteDeja","This user already exists.");
define("kErrorUsagerSauve","The user could not be saved.");
define("kErrorUsagerCreation","The user could not be created.");
define("kErrorUsagerSuppr","The user could not be deleted.");
define("kSuccesSauveUsager","User saved.");
define("kErrorUsagerDernierAdmin","This account is the only remaining administrator. It can't be deleted.");
define("kMsgUtilisateurInconnu","This user does not exist.");
define("kMessageCompteNonModifiable","This account is not editable.");

define("kMailLoginDemandeCorps","Here are your authentification data :\n\n
Login : %s \n
Password : %s \n\n
See you soon on ".kCheminHttp."\n\n
Mail automatically generated.\n");
define("kMailLoginDemandeSujet","Access to ".gSite);

define("kMailLoginDemandeNoUserCorps","The user gave the following :\n
E-mail : %s \n\n
Mail automatically generated.\n");
define("kMailLoginDemandeNoUserSujet","Login request for ".gSite);

define("kMailLoginValidationSujet","Registering validated on ".gSite);
define("kMailLoginValidationCorps","Your register has been validated !\n
Your authentification is : \n\n
Login : %s \n
\n
\n\nSee you soon on ".kCheminHttp."\n
Mail automatically generated.\n");

define("kMailDemandeInscriptionSujet","Registering on ".gSite);
define("kMailDemandeInscriptionCorps","Registering on  ".gSite."\n\n
New registering request for ".kCheminHttp." :\n\n
Name : %s \n
First name : %s \n
Company : %s \n
E-mail : %s \n\n
Login : %s \n
\n\nMail automatically generated.\n");

define("kMemoriserConnexion","Remember me");
define("kInscriptionEntete","Please fill in the subscription below.");
define("kPasEncoreInscrit","Not yes registered ?");
define("kActif","Active");

//VALEUR
define("kCodeValeur","Value code");
define("kErrorValExisteDeja","This value already exists.");
define("kErrorValSauve","The value could not be saved.");
define("kErrorValCreation","The value could not be created.");
define("kErrorValSuppr","The value could not be deleted.");
define("kWarningValDejaAttachee","Warning : this value is already attached to another.");
define("kErrorValAttache","The value could not be attached to another.");
define("kErrorDetacheTous","The children could not be detached of this value.");
define("kValeursRattacheesA","Linked values");
define("kErrorValFusionPbId","Merging not done, an identifier is missing.");
define("kErrorValFusion","Merging could not be done.");
define("kJSConfirmFusion","Do you want to merge those values ?");


//ALERT JAVASCRIPT
define("kJSConfirmSauverModif","Do you want to save changes ?");
define("kJSConfirmSupprUser","Are you sure you want to remove user ");
define("kJSErrorElementOblig","Please add at least one item.\\n");
define("kJSErrorFloat","Please type a float number.\\n"); //Attention aux 2 backslashes !
define("kJSErrorOblig","This field is mandatory.\\n");
define("kJSErrorInt","Please type a non null integer.\\n");
define("kJSErrorLoginOblig","Please fill the login.\\n");
define("kJSErrorMail","Please fill a correct mail.\\n");
define("kJSErrorNomOblig","Please fill a name.\\n");
define("kJSErrorPasswordOblig","Please fill the password.\\n");
define("kJSErrorValeurOblig","Please fill a value.\\n");
define("kJSErrorTitreOblig","Please fill a title.\\n");
define("kJSErrorYear","Please type a valid year.\\n");
define("kJSErrorValeurExisteDeja","This value is already in the list.");

define("kJSErrorPrenomOblig","Please fill the first name.\\n");
define("kJSErrorSocieteOblig","Please fill the organisation.\\n");
define("kJSErrorAdresseOblig","Please fill the adress.\\n");
define("kJSErrorVilleOblig","Please fill the city.\\n");
define("kJSErrorCodePostalOblig","Please fill the zip code.\\n");
define("kJSErrorPaysOblig","Please fill the country.\\n");
define("kJSErrorFonctionOblig","Please fill the domain of activity.\\n");
define("kJSErrorTelOblig","Please fill the phone.\\n");
define("kJSErrorMotDePasseDifferent","Please enter the same passwords.\\n");

// GENERIQUE
define("kDocumentParent","Parent document");
define("kSansParent","No parent");
define("kNouveauDoc","Add a document");
define("kNouveauLexicon","Add a lexicon entry");
define("kNouveauUsager","Add a user");
define("kNouveauValeur","Add a value");
define("kNouveauMat","Add a material");
define("kPlusCriteres","More criterias");
define("kGestionTableSysteme","System table management");
define("kFlecheMonter","One step up");
define("kFlecheDescendre","One step down");
define("kDeplacer","");

define("kRattacheA","Attached to");
define("kTexteLibre","Text");
define("kWarningTypeLexique","The type cannot be changed unless this lexicon entry is not linked to any other entry (parent or children) and any document or person.");
define("kWarningTypeValeur","The type cannot be changed unless this value is not linked to any other value (parent or children) and any document or person.");

// UPLOAD
define("kUploadErreurInitFlash","The Flash upload component could not be set.");
define("kUploadDocAccSucces","The attached document has been sucessfully added.");
define("kUploadDocAccEchec","Failed adding this attached document");
define("kUploadFileSucces","The following file has been successfully transfered :");
define("kUploadPrecisions","Please don't close this window while the transfer happens.<br/>In case of big file transfer, you may experience some holds in the progress bar.");
define("kUploadMaterielSucces","The media file has been successfully transfered.");
define("kUploadMaterielEchec","Failed adding this media file");
define("kUploadMaterielWarning","Warning");
define("kBatchUpload","Batch upload of materials");
define("kBatchUploadJava","Batch upload of materials (JAVA)");
define("kBatchUploadJavaDisclaimer","The Java method is highly recommended with big files.<br/>It requires the presence of the Java runtime on your computer. <a target='_blank' href='http://www.java.com'>http://www.java.com</a>");
define("kGenererDoc","Generate the document");
define("kGenererStoryboard","Generate the storyboard");
define("kPrefixe","Prefix for documents");
define("kUploadAnalyseMedia","Getting informations from file...");
define("kImportMetadonnees","Get informations from file(s)");
define("kUploadTranscodage","Also generate a file with format : ");
define("kUploadTranscodageErreurNoSource","No source file");
define("kUploadTranscodageErreurNoBinary","Transcoding tool missing");
define("kUploadTranscodageErreurNoFormat","No output format selected");
define("kUploadTranscodageErreurFormatNonGere","Required format is not supported");
define("kUploadTranscodageErreurFormatExisteDeja","The original file has already this format");
define("kUploadTranscodageErreurEchec","Failure generating additional media");
define("kUploadTranscodageEnCours","Generating additional format...");
define("kUploadTranscodageSucces","Additionnal format generation complete.");
define("kUploadTranscodageEchec","Failure when generating additional format.");
define("kUploadDebutTraitement","Starting post-processing of uploaded files.<br><b>Please don't close this window while processing.</b>");
define("kUploadFinTraitement","End of post-processing.<br>You can close this window.");
define("kUploadFinTraitementSuivi","The requested processes have been launched.<br>You can close this window or follow them by <a href='simple.php?urlaction=jobListe&init=1'>clicking here</a>.");
define("kUploadResultat","Upload : status and post-processing.");
define("kUploadTranscodageBilan","Generation :");
define("kUploadSelectionFichiers","File selection");
define("kUploadParametres","Parameters");
define("kUploadTranscodageErreurQTFastStart","Failure to apply Fast Start on file");
define("kUploadStart","Launch upload");
define("kUploadSujetFinUpload","New files have been uploaded on the website.");

//xUPLOAD : messages du composant xUpload
define("kXUploadTotalExceeded","Overall max size exceeded.<br>Please stop the transfer.<br>Max allowed size is :");
define("kXUploadTotalMini","Overall min size below.<br>Please stop the transfer.<br>Min allowed size is :");
define("kXUploadFileExceeded"," exceeds file size. Ignored.");
define("kXUploadFileMini"," is below min file size. Ignored.");
define("kXUploadNoTempDir","No temporary dir found temp_dir ! Please ask admin to check this parameter.");
define("kXUploadNoTargetDir","No target directory found ! Please ask admin to check this parameter.");
define("kXUploadNoTemplate","No template found ! Please ask admin to check this parameter.");
define("kXUploadTransferComplete","Transfer complete !");
define("kXUploadUploadFailed","Failure when transfering !");
define("kXUploadNullFile"," has a null size or incorrect path.");
define("kXUploadBadFile"," is not an allowed file name ! Ignored.");
define("kXUploadBadExt"," doesn't have an allowed extension ! Ignored.");
define("kXUploadTooMany"," was saved because max number of files is exceeded.");
define("kXUploadAlreadyExist"," already exists !<br>Renamed to ");
define("kXUploadSaved"," has been sucessfully saved.");
define("kXUploadWrongPass","Wrong password.<br>Unallowed transfer.");
define("kXUploadUnwantedIp","You are not allowed to transfer files.");


// PERSONNES
define("kRecherchePersonne","Person search");
define("kCoordonnees","Location");
define("kErrorPersonneCreation","The person could not be initialized.");
define("kErrorPersonneExistant","This person already exists.");
define("kErrorPersonneSauve","The person could not be saved.");
define("kErrorPersonneSuppr","The person could not be deleted.");
define("kErrorSauvePersLex","The associated terms could not be saved.");
define("kErrorSauvePersVal","The associated values could not be saved.");
define("kErrorPersonneVersionSauve","The other versions could not be saved.");
define("kPersonnesLiees","Linked persons");
define("kMsgDoublon","Another person already exists with the same name");
define("kConserver","Keep");
define("kFusionner","Merge");

// DOC ACC
define("kErrorDocAccCreation","This attachment could not be created.");
define("kErrorDocAccNoId","No specified ID !");
define("kWarningDocAccFichierUtilise","This attachment is also attached elsewhere.");
define("kErrorDocAccExisteDeja","This attachement already exists.");
define("kErrorDocAccSauve","This attachment could not be saved.");
define("kErrorSauveDocAccVal","The associated values could not be saved.");
define("kErroDocAccVersionSauve","The other versions could not be saved.");
define("kVignette","Illustration");
define("kDocAccFichierManquant","Missing file !");
define("kErrorDocAccRenommageFichier","Unable to rename the file");


// MATERIELS
define("kErrorSauveMatVal","The associated values could not be saved.");
define("kErrorSauveDocMat","The association with documents could not be saved.");
define("kErrorMaterielSauve","The material could not be saved.");
define("kErrorMaterielRenommageFichier","The file associated to this material could not be renamed.");
define("kSuccesMaterielRenommageFichier","The file associated to this material has been moved successfully.");
define("kSuccesMaterielSauve","Material saved.");
define("kSuccesMaterielCreation","Material created.");
define("kErrorMaterielVide","The field 'Material' cannot be empty.");
define("kErrorMaterielExisteDeja","This material already exists.");
define("kMaterielFichierExiste","File Ok !");
define("kMaterielFichierManquant","Missing file !");
define("kUpload","Upload a file...");
define("kCharger","Upload...");
define("kChoisir","Choose...");
define("kErrorMaterielLieuImpossibleCreerDir","Error while creating the folder: ");
define("kErrorMaterielLieuImpossibleDeplacerFichier","Error while moving the file");
define("kErrorMaterielExistePasDansDir","The file associated to this material doesn't exist in the folder: ");
define("kLocalisationServeur","Server storage");
define("kInfosFichier","File information");
define("kNumerise","Digital");

//DOC MAT
define("kErrorSauveDocMatVal","Error : Couldn't associate values to this link between document and material");
define("kErrorDocMatVide","No Id provided. Abandonned.");
define("kErrorDocMatSauve","Error while saving those informations");
define("kSuccesDocMatSauve","Those informations have been saved.");

// DOM
define("kDOMlimitvalues"," maximum value(s) only");
define("kDOMnodouble","This value is already in the list");

// PANIER
define("kAjouterTousResultatsPanier","Add all results to card");
define("kConfirmPanierCreation1","Are you sure you want to create ");
define("kConfirmPanierCreation2","in the directory");
define("kConfirmPanierDeplacer","Are you sure you want to move this directory to ");
define("kConfirmPanierDupliquer","Are you sure you want to duplicate this directory ?");
define("kConfirmPanierMove","Are you sure you want to move that folder ?");
define("kConfirmPanierRename","Do you confirm this new name : ");
define("kConfirmPanierSupprimer","Are you sure you want to delete that folder (and all its content)");
define("kConfirmPanierTransmettre","Are you sure you want to transfer the current directory to ");
define("kEntetePanier","You can order the elements in your cart and have them delivered by instant download or DVD/other material shipment");
define("kErrorFolderLigneExisteDeja","This document is already in that folder.");
define("kErrorLignesExistentDeja"," already existing");
define("kErrorPanierCopieLignesVides","There is nothing in the scrap-book !");
define("kErrorPanierCreationNoName","Please give the directory a name");
define("kErrorPanierCreationNoParent","Please choose a parent directory");
define("kErrorPanierDeplacer","Unable to move this folder or selection");
define("kErrorPanierDeplacerDejaParent","This directory already contains the current item.");
define("kErrorPanierDeplacerDossierInterdit","You cannot choose the directory itself.");
define("kErrorPanierLigneExisteDeja","This document is already in the cart.");
define("kErrorPanierMoveSame","Please choose two different folders.");
define("kErrorPanierNoId","Error : missing id");
define("kErrorPanierRemoveLigne","Unable to delete this line");
define("kErrorPanierRenameVide","Please give a name to that folder.");
define("kErrorPanierSauveLigne","Unable to add this document.");
define("kErrorPanierSauveLigneManqueIdDoc","Unable to add this document.");
define("kErrorPanierSupprimer","Unable to delete that folder.");
define("kErrorPanierTransfertDoublon","Warning! %s documents are already in that folder.");
define("kErrorPanierTransmettreNoUsager","Please choose a user");
define("kErrorPanierAccessDenied","You do not have the right to access to this selection");
define("kErrorPanierInvalidIDpanier","No corresponding selection");
define("kFolders","Folders");
define("kPanierCreation","Create");
define("kPanierCreationNom","named");
define("kPanierCreationParent","in");
define("kPanierDeplacer","Move this directory");
define("kPanierDossierParent","Parent directory");
define("kPanierDupliquer","Duplicate this directory");
define("kPanierSelections","Cart and folders");
define("kPanierSession","My selection");
define("kPanierSuccesSauve","Cart updated.");
define("kPanierSupprimer","Delete a directory");
define("kPanierTransmettre","Transfer to ");
define("kPanierTypeDossier","a directory");
define("kPanierTypeSelection","a selection");
define("kQuestionEnvoyerMailClient","Warn the user of these changes ?");
define("kSuccesFolderDocumentAjoute","Document added to that folder.");
define("kSuccesFolderDocumentRetire","Document deleted from that folder");
define("kSuccesPanierCopie","Document(s) pasted sucessfully");
define("kSuccesPanierCreation","Folder or selection has been created");
define("kSuccesPanierDeplacer","Folder or selection moved sucessfully");
define("kSuccesPanierDocumentAjoute","Document added to cart.");
define("kSuccesPanierDocumentRetire","Document deleted from cart.");
define("kSuccesPanierDocumentsAjoutes","Document(s) added to car.");
define("kSuccesPanierDupliquer","Folder or selection duplicated sucessfully");
define("kSuccesPanierDupliquerLignes","Documents have been duplicated");
define("kSuccesPanierSauve","Folder sucessfully saved.");
define("kSuccesSelectionSauve","Folder sucessfully saved.");
define("kSuccesPanierSupprimer","Folder successfully deleted.");
define("kSuccesPanierTransfert","Document(s) tranfered successfully");
define("kSuccesPanierVider","Document(s) deleted successfully");
define("kWarningDeletePanier","Warning! This delete is definitive and can't be undone. Continue ?");
define("kWarningPanierCommandeEnCours","You already had an order in progress.");

define("kNouveauPanier","New cart");
define("kNouvelleSelection","New folder");
define("kSelectionSupprimer","Delete selected items");
define("kSelectionnerToutPanier","Select the full cart");
define("kSelectionDeplacer","Move selected items");
define("kSupprimerDocumentsSelectionnes","Delete selected items");
define("kPanierRenommer","Rename a folder");
define("kDupliquerVers","Duplicate to");
define("kDans","in");
define("kMsgSelectionnerDossierDestination","Please select a destination folder");
define("kMsgCliquerTelecharger","Clic on links to download");
define("kCommanderDocCoches","Order the selected items");
define("kViderSelection","Empty the folder");
define("kViderPanier","Empty the cart");

define("kDocListeTousResultats","Add all items to the cart");
define("kDocListeAjouterResultatsPage","Add this page of items to the cart");
define("kDocListeRetirerResultatsPage","Remove this page of items from the cart");

define("kCopier","Copy");
define("kColler","Paste");
define("kMsgNoSelectLignes","Nothing is selected !");

// COMMANDE
define("kCommandeCGV_OK","I agree with <a href=\"#\" onclick=\"popupInfo('indexPopupHtml.php?html=3','main')\" >General Conditions of Sales</a>.");
define("kCommandeRecap","Order details / Quotation");
define("kCommanderSupport","Material order");
define("kMsgCommandeAchat","Please fill this order form by indicating the type of support that you would like to receive.");
define("kMsgCommandeAchatRecap","<strong>Your order was well taken into account.</strong>");
define("kMsgCommandeTelecharger","The validation of this order form will allow you to download the elements in  MPEG-2 format.<br/>");
define("kMsgCommandeTelechargerRecap","You can download each element of your order, <strong>by clicking on the floppy</strong> of the line.");
define("kContenuCommande","Order content");
define("kErreurCBHorsLimites","Payment using a credit card is only allowed between ".gAchatMontantMin."&euro; and ".gAchatMontantMax."&euro;.");
define("kInscriptionPremiereCommande","Please identify yourself for ordering by filling the access below or subscribing if it is your first visit.");
define("kInfosCommande","Order information");
define("kNb","Nb");
define("kPayerEnLigne","Pay online");
define("kRedirectionCommande","You will be redirected immediatly to the next step of your order.");
define("kTotalTVA","VAT");

// GESTION COMMANDES
define("kRecherchePaniers","Cart search");
define("kGlobale","Global");

// LIVRAISON
define("kTelechargementPanier","Download cart ");
define("kTelecharger","Download");
// DOUBLON define("kMsgPreparationCommande","Please wait while your file is processed...");
define("kErrorLivraisonNoMateriel","No material to be cut.");
define("kErrorLivraisonEchecDecoupage","Error while cutting media.");
define("kErrorLivraisonEchecCopie","Error while copyin media");
define("kMessageAideLivraison","Your order is in progress");
define("kMsgPreparationCommande","While wait while the files are prepared...");
define("kClip","Full clip");
define("kMo","MB");
$msg="Your broadcast video is being downloaded. You can monitor this process in the \"Downloads\" window of your Web browser. If download did not start automatically, please right-clic on the following link and choose \"Save target as...\"<br/>";
$msg.="%s <br/><br/>";
$msg.="This file was encoded in MPEG-2 at 8 Mb/s. Thus, it is compatible with most edit suites :<br/><br/>";
$msg.="With Avid software, you can use Sorenson Squeeze (http://www.sorensonmedia.com/pages/?pageID=2), a software bundled with Avid products, in order to transcode this file into a format that is compatible with your software, e.g. DV25.<br/>";
$msg.="<br/>With Apple Final Cut Pro (FCP), you must first demux audio and video streams, because FCP cannot import multiplexed MPEG-2 files. To do so, you can use MPEG Streamclip, a free software available at http://www.squared5.com/svideo/mpeg-streamclip-mac.html. You must then drag-and-drop the video file in the Streamclip window, select the \"Fix Timecode Breaks…\" command of the \"Edit\" menu, and then \"Demux to Unscaled M2V and AIFF…\" of the \"File\" menu (we advise you to give the same name to the exported video and audio files). You can now import the \"M2V\" video file in FCP, using the \"Import > Files…\" command of the \"File\" menu. If you gave the same name to the video and audio files when exporting, the video and audio are imported at the same time in FCP. Otherwise, you must import the audio file separately.";
$msg.="<br/><br/>In any case, you can also use various software on Mac or PC in order to transcode this file into a format that is compatible with your purpose:<br/>";
$msg.="- MPEG Streamclip, on Mac or PC (http://www.squared5.com/svideo/mpeg-streamclip-win.html): free, but requires QuickTime MPEG-2 component which is not free<br/>";
$msg.="- SUPER (http://www.erightsoft.com/SUPER.html), on PC only: free<br/>";
$msg.="- Commercial products: Sorenson Squeeze on PC or Mac, Episode Pro (http://www.telestream.net/products/episode_pro.htm) on Mac…";

define("kMsgLivraisonFinieV",$msg);

$msg="The audio module you selected is being downloaded. You can monitor this process in the \"Downloads\" window of your Web browser. If download did not start automatically, please right-clic on the following link and choose \"Save target as...\"<br/>";
$msg.="%s <br/><br/>This file was encoded in MP3 at 256 kb/s.";
define("kMsgLivraisonFinieA",$msg);

define("kMsgCopieSucces","Copie done sucessfully");
define("kMsgErrorCreerZip","Error creating ZIP archive");

define("kMsgLivraisonFinie","Your broadcast video is being downloaded. You can monitor this process in the \"Downloads\" window of your Web browser. If download did not start automatically, please right-clic on the following link and choose \"Save target as...\"<br/>%s <br/><br/>");

// Dossiers
define("kAjouterDossierPublic","Add to folder : ");
define("kCreerDossier","Create folder ↓ ");
define("kDossierParent","Parent folder");
define("kDossiers","Folders");
define("kDossiersThematiques","Thematic files");
define("kModifierImage","Change picture");
define("kGestionHomeEtDossiers","Homepage and Thematic Files");
define("kTitreAnglais","English title");
define("kTitreFrancais","French title");

//ADMINISTRATION
define("kAdminEntitesAudiovisuelles","Audiovisual items");
define("kAdminValeursThesaurus","Lexicon, values, persons");
define("kAdminUtilisateursDroits","Users and rights");
define("kAdminCommandesStats","Orders and statistics");
define("kAdminImportsDivers","Imports, miscellaneous");
define("kAdminSysteme","System parameters");
define("kAdminPagesStatiques","Static HTML pages");
define("kAdminMaintenanceFichierOrphelins","Files without data input");

//PAGES STATIQUES HTML
define("kListePages","List of HTML static pages");
define("kPageDetail","Detail of the page");
define("kErrorHtmlExisteDeja","A web page has already this title.");
define("kErrorHtmlSauve","An error occured while saving.");
define("kErrorHtmlCreation","Unable to create this page.");
define("kErrorHtmlSuppr","Unable to delete this page.");

//STORY BOARD
define("kAjoutImagesParUpload","Add pictures...");
define("kUploadImageSucces","Pictures added.");
define("kUploadImageEchec","Error : picture not added");
define("kErrorImageurCreation","Error while creating storyboard.");
define("kErrorImageurExisteDeja","A storyboard already exists.");
define("kErrorImageurCreationRepertoire","Impossible to create the directory.");
define("kImageurIntervalleImage","Time lapse between captures (sec) ");
define("kImageurCreationEnCours","Storyboard in progess...");
define("kImageurGenerationSucces","Storyboard automatically generated.");
define("kImageurGenerationEchec","Failure to automatically create storyboard.");
define("kErrorImageurNoSource","No source file !");
define("kErrorImageurNoTool","Storyboard tool missing");
define("kErrorImageurNoImageur","No storyboard.");
define("kErrorImageurNoImages","Empty storyboard.");
define("kImageurSelectVignette","Pick as thumbnail");
define("kErrorImageurNoVideo","This file format can't handle image extraction.");
define("kErrorImageExisteDeja","This picture already exists.");
define("kErrorImageSauve","The picture could not be saved.");
define("kErrorVersionSauve","Unable to save other versions.");
define("kErrorImageurCapture","Unable to grab picture.");
define("kSuccesImageurSauve","Pictures saved successfully.");
define("kSuccesImageurCapture","Picture grabed successfully.");
define("kErrorImageurCaptureExisteDeja","This picture already exists");
define("kErrorImageurCaptureNoPlayer","No player !");
define("kErrorJSImageurNoRate","Missing number of image per seconde !");
define("kImageurWaitGeneration","Imageur building...");

// VISIONNAGE
define("kErrorVisioCreationTempDir","Error while creating the temporary directory.");
define("kErrorVisioCommandeVide","The command line for cutting the movie is empty.");
define("kErrorVisioCopieMovie","The media could not be copied in the temporary directory.");
define("kErrorVisioFichierRefVide","Reference filename is missing.");
define("kErrorVisioCopieFichierRef","Reference file could not be copied.");
define("kErrorVisioNoMedia","This media is not viewable.");
define("kErrorVisioCreationXML","Impossible to create the XML file.");
define("kVisioNomDefautExtrait","No title");
define("kJSConfirmExit","All changes will be lost !");
define("kErrorVisioNoCatMovie","Missing concat tool");

// ORAOWEB
define("kOraoWebSelectWholeMovie","Select whole movie");
define("kOraoWebCreateNewExcerpt","Add blank line");
define("kOraoWebSetCurrentTCIN","Set TC In with current position");
define("kOraoWebSetCurrentTCOUT","Set TC Out with current position");
define("kOraoWebSaveChanges","Save changes");
define("kOraoWebOpenPlayer","Watch video");
define("kOraoWebSelectDocument","Select a document to add");

// REQUETE / HISTO
define("kErrorRequeteSauve","This search couldn't be saved.");
define("kErrorRequeteExisteDeja","This search is already saved.");
define("kMesRequetes","My queries");

// ADMIN : MAINTENANCE FICHIERS
define("kConfirmJSFichierSuppression","Do you confirm file deletion ?");
define("kWarningMaintenanceFichierMessage","Warning : deletion can't be undone.");
define("kErrorMaintenanceFichierNoFile","No 'orphan' file found.");
define("kCacheVignette","Thumbnails");
define("kGenererMateriel","Generate material and storyboard");

// Import FCP
define("kImportFinalCutPro","Processing");
define("kFichierAImporter","File(s) to import");
define("kMedias","Media(s)");

// PROCESSUS, ÉTAPES ET JOBS								// form/jobSaisie.inc.php
define("kMonCompte","My account");
define("kMenu","Menu");
define("kReference","Reference");
define("kFichiersMedias","Fichiers medias");
define("kFichier","File");
define("kDetail","Detail");
define("kTraitement","Processing");	// ou transformation
define("kLancer","Start");
define("kListeTraitements","Processings list");
define("kSessionEnCours","Current session");
define("kProcessus","Process");
define("kModule","Module");
// DOUBLON define("kSortie","Exit");
define("kSuiviTraitements","Processings follow-through");
define("kRelancer","Restart");
define("kMailConfirmation","Confirmation email");
define("kDebit","Data rate");
define("kDateDemande","Date of request");
define("kDateLivraison","Date of delivery");
define("kDestinataire","Recipient");
define("kFormatSortie","Export to");
define("kDocumentsCrees"," produced file(s)");
define("kEncodageManuel","Advance settings");

define("kErrorProcessusExisteDeja","This process already exist");
define("kErrorProcessusSauve","Error: the process was not correctly saved");
define("kErrorProcessusCreation","Error: the process was not correctly initialised ");
define("kErrorProcessusNoID","Error: process ID is not correct");
define("kErrorProcessusSauveEtapes","Error: the process steps were not correctly saved");
define("kErrorProcessusSuppr","Error: process was not correctly deleted");

define("kErrorEtapeExisteDeja","This step already exists");
define("kErrorEtapeSauve","Error: the step was not correctly saved");
define("kErrorEtapeCreation","Error: the step was not correctly initialised");
define("kErrorEtapeSuppr","Error: the step was not correctly deleted");

define("kErrorJobExisteDeja","This processing already exists");
define("kErrorJobSauve","Error: the processing was not correctly saved");
define("kErrorJobCreation","Error: the processing was not correctly initialised");
define("kErrorJobSuppr","Error: the processing was not correctly deleted");

define("kErrorEnvoiMailValidation","Error when creating validation mail");

define("kMailFinTraitement","New video ");
define("kMailValidation","New request: ");	// Titre du mail envoyé suite à une demande de validation
define("kValidation","Validation");
define("kValidationEtape","Step validation");
define("kMsgValidationJob","the step <b>%s</b> is validated.");
define("kMsgValidationJobRefusee","The step <b>%s</b> is invalidée.");
define("kMsgConfirmerValidation","Do you want to validat this step ?");
define("kErreurJobNonValide","Validation refused");
define("kRefuser","Refuse");
define("kErrorRetourKewego","Error Kewego : missing ID");
define("kLoginSortie","Out login");
define("kMotDePasseSortie","Out password");
define("kDossierSortie","Out folder");
define("kErrorRetourParole","Error speech : missing ID");
define("kAction","Action");

// ALERTE MAIL
define ("kAlerteMailSujet","New results for your favorite searches on :");
define ("kAlerteMailEntete","Hello,\n
some new documents on ".kCheminHttp." correspond to your saved searches.\n");
define ("kAlerteMailDisclaimer","\n\nAutomatically generated mail.\nYou received this mail because you registered on ".kCheminHttp."\n
and you chose to be kept informed by mail of the new search results.\n\n");

// MODIFICATIONS PAR LOTS
define("kModifierLot","Batch process");
define("kInitialiser","Initialise");
define("kEffacer","Delete");
define("kRechercherRemplacer","Search / replace");
define("kModifierNotice","Modify, record by record");
define("kAppliquer","Apply");
define("kWaiterMessage","Please wait while processing is done...");
define("kPrecedent","Previous");
define("kSuivant","Next");
define("kAbandonnerChangements","Abort modifications ?");
define("kErrorModifLotResultsetChanged","Results have changed. Modifications aborted.");
define("kErrorModifLotNoResults","No search result available.<br/>
Please start a search before using this screen.<br/>
<a href='javascript:window.close();'>Close</a>");
define("kErrorModifLotErreurXML","Error in the settings file");
define("kSuccessModifLot","(s) successfully updated");

define("kInitValComment","Initialise a field with a value");
define("kInitValHint","Field initialisation");
define("kInitValFieldLabel","Field expected to be initialised");
define("kInitValValueLabel","Value");

define("kCleanValComment","Empty the field");
define("kCleanValHint","Reset the field");
define("kCleanValFieldLabel","Field to be cleaned");

define("kCopyValComment","Copy the content of a field in another one");
define("kCopyValHint","Field duplication");
define("kCopyValFieldLabel","Source field");
define("kCopyValField2Label","Destination field");

define("kReplaceValComment","Search a string and replace it");
define("kReplaceValHint","Search / Replace within a field");
define("kReplaceValFieldLabel","Searched field");
define("kReplaceValValueLabel","Search");
define("kReplaceValValue2Label","Replace with");

define("kEditValComment","Edition within the results");
define("kEditValHint","Value edition");
define("kEditValFieldLabel","Field to edit");
define("kEditValValueLabel","Value");

define("kSupprValComment","Final deletion of the results");
define("kSupprValHint","Results deletion");

define("kTester","Test");
define("kWindows","Windows");
define("kMac","Mac");

// BACKUP
define("kCapacite","Capacity");
define("kCapaciteUtilisee","Used capacity");
define("kCapaciteRestante","Remaining capacity");
define("kCartouche","Tape");
define("kCartouches","Tapes");
define("kDateFichier","File modification date");
define("kDateSauvegarde","Backup date");
define("kDebutTraitement","Start of process");
define("kErrorBackupJobActionVide","Error : no action");
define("kErrorBackupJobJeuVide","Erreur : no set");
define("kErrorFichierDejaSauve","File already saved");
define("kErrorNoDevice","No device !");
define("kErrorSauveTapeFile","Error when deleting link tape / files");
define("kErrorTapeNoId","No tape identifier !");
define("kErrorTapeExisteDeja","This tape reference already exists");
define("kErrorTapeInfoFile","Info file not found");
define("kErrorTapeSauve","An error occured when saving this tape");
define("kErrorTapeCreation","Unable to create this tape");
define("kErrorTapeSuppr","Unable to delete this tape");
define("kErrorTimeout","Timeout error");
define("kErrorVariableVide","Error empty parameter :");
define("kFichierDejaSauve","File already saved on tape ");
define("kFinTraitement","End of process");
define("kGestionSauvegardes","Backup management");
define("kIdentifiantCartouche","Tape identifier");
define("kJeu","Set");
define("kListeFichiers","List of files");
define("kListageCartouches","Listing tapes ");
define("kMsgFichierASauver","%s file(s) to backup (%s)");
define("kNbFichiers","Nb files");
define("kNomFichier","File name");
define("kOffline","Off-line");
// DOUBLON define("kOnline","On-line");
define("kRechercheCartouche","Tape search");
define("kRechercheFichierASauver","Searching files to backup");
define("kRestaurer","Restore");
define("kRienASauvegarder","Nothing to backup");
define("kSauvegardes","Backups");
define("kSuccesTapeCreation","Tape successfully created");
define("kSuccesTapeSauve","Tape successfully saved");
define("kTaille","Size");
define("kTailleFichier","File size");

//Import / export (copie de fichiers)
define("kErrorExportCopieMediaXML","Impossible to extract the media list from the XML");
define("kErrorExportDirNonDefinie","Undefined export directory");
define("kErrorExportImpossibleEcrireFichier","Impossible to write files in the export directory");
define("kExportDocAccCopie"," jointed document(s) copied");
define("kExportImageurCopie"," storyboard(s) copied");
define("kExportImageCopie"," image(s) copied");
define("kExportMaterielCopie"," video(s) copied");

// RECHERCHE
define("kSauverRecherche","Save search");
define("kAlerte","Alert");

//Web Services
define("kErrorWSRequeteNonValide","Non valid request");//003
define("kErrorWSXSLManquant","XSL stylesheet missing");//004
define("kErrorWSEntiteNonTrouvee","This entity is not supported");//005
define("kErrorWSParametreManquant","One parameter is at least missing"); //006
define("kErrorWSSyntaxeParametre","Parameter syntax is not right :");//007
define("kErrorWSFichierNonTrouve","File not found");//008
define("kErrorWSExtensionNonAutorisee","File extension not allowed");//009
define("kErrorWSResultatManquant","No result"); //010
define("kErrorWSExtensionNonSupportee","File extension not supported"); //011
//kAccesLogin=012, kErreurPageNotFound=014

//Gestion Photo
define("kPhotoUpload","Upload photos");
define("kExtensionImagickNotLoaded","Impossible to load ImageMagick");
define("kFichierImagickNotFound","Impossible to find ImageMagick");
define("kErrorImageurNoPhoto","This media is not a photo");
define("kGenererVignette","Generate thumbnails");
define("kDiaporama","Diaporama");
define("kErrorDiaporamaExtraction","Impossible to install the diaporama");
define("kErrorDiaporamaNotFound","Diaporama not found");

//Gestion des etapes et process
define("kGestionProcessus","Process editing");
define("kGestionEtapes","Steps editing");
define("kNouvelleEtape","Create new step");
define("kNouveauProcessus","Create new process");
define("kEtapePrecedente","Previous step");
define("kProcessusAjouterEtape","Add step");
define("kEtapeChampManquant","Warning, some fields are missing on the form ");
define("kSuccesEtapeDupliquer","Step duplicated, please choose a new name");
define("kSuccesProcessusDupliquer","Process duplicated, please choose a new name");

//Paramètres des étapes
define("kEntree","Input");
define("kSortie","Ouput");
define("kCodecVideo","Video codec");
define("kDebitVideo","Video bitrate");
define("kAspect","Aspect");
define("kLargeur","Width");
define("kHauteur","Height");
define("kRatioLargeur","Width ratio");
define("kRatioHauteur","Height ratio");
define("kCropHaut","Top crop");
define("kCropBas","Bottom crop");
define("kCropGauche","Left crop");
define("kCropDroit","Right crop");
define("kImagesParSeconde","Images / sec.");
define("kCodecAudio","Audio codec");
define("kNbCanaux","Nb. channels");
define("kDebitAudio","Audio bitrate");
define("kFrequenceEchantillonage","Sample rate");
define("kTCIncruste","Insert TC on video ?");
define("kLogo","Logo file");
define("kNumerisation","Digitisation");
define("kIntervalle","Interval");
define("kDetectionPlan","Detect shot changes ?");
define("kDossier","Directory");
define("kServeur","Server");
define("kFTP","FTP");
define("kFTPImage","FTP Images");
define("kTransfertHTTP","HTTP transfert / Kewego");

?>