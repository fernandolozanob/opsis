<?
define ( "kA", "tới");
define ( "kAcces", "truy");
define ( "kAccesLogin", "Xin vui lòng đăng nhập lại để truy cập trang này");
define ( "kAccesReserve", "Private Access");
define ( "kAccueil", "Home");
define ( "kAchat", "Bán hàng và vay");
define ( "kActualiser", "Làm mới");
define ( "kAdministration", "Quảntrị");
define ( "kAdresse", "Địa chỉ");
define ( "kAdresseFacturation", "Hoá đơn địa chỉ");
define ( "kAdresseLivraison", "giao hàng địa chỉ");
define ( "kAdressePerso", "tư nhân địa chỉ");
define ( "kAffichage", "trình bày");
define ( "kAffichAlphabetiq", "theo thứ tự abc");
define ( "kAffichHierarchie", "thứ bậc");
define ( "kAide", "Help");
define ( "kAjouter", "Thêm");
define ( "kAjouterAcc", "Thêm một tài liệu tham gia");
define ( "kAjouterPanier", "Thêm vào giỏ hàng");
define ( "kAjouterImage", "Thêm một hình ảnh");
define ( "kAjouterLigne", "Thêm một đường dây");
define ( "kAjouterViaUpload", "Thêm bằng tải lên");
define ( "kAlerteMail", "Gửi email cho tôi");
define ( "kAncienneCote", "Old cote");
define ( "kAnnuler", "Hủy");
define ( "kAPropos", "Giới thiệu");
define ( "kAu", "tới");
define ( "kAucunResultat", "Không có kết quả");
define ( "kAudience", "Đối tượng");
define ( "kAuteur", "Tác giả");
define ( "kAuteurScientifique", "khoa học giả");
define ( "kAuthentification", "Đăng nhập");
define ( "kAutresVersions", "phiên bản khác");
define ( "kAutreTitre", "Các đề");
define ( "kBienvenue", "Welcome");
define ( "kBiographie", "Tiểu sử");
define ( "kBonjour", "Hello");
define ( "kChercher", "Tìm kiếm");
define ( "kCodeLangage", "Mã Ngôn ngữ");
define ( "kCodePostal", "zip code");
define ( "kCollection", "Bộ sưu tập");
define ( "kCommande", "Đặt hàng");
define ( "kCommander", "đặt hàng");
define ( "kCommandes", "Orders");
define ( "kCommanditaire", "Ngủ Đối tác");
define ( "kComment", "Comment");
define ( "kCommission", "Ủy ban can thiệp");
define ( "kCompression", "nén");
define ( "kConditionExploitation", "Điều kiện sử dụng");
define ( "kConfirmerAbandon", "Mọi thay đổi sẽ bị mất Tiếp tục?!");
define ( "kConfirmerSuppression", "Bạn có thực sự muốn xóa mặt hàng này?");
define ( "kConnexion", "Đăng nhập");
define ( "kConsultationSeule", "Chỉ đọc");
define ( "kConsulter", "Tư vấn");
define ( "kContact", "Liên hệ");
define ( "kContenuPanier", "Hàng nội dung");
define ( "kContributeur", "User");
// DOUBLON define ( "kCoordonnees", "Địa chỉ");
define ( "kCoordonneesPersonnelles", "Cá nhân địa chỉ");
define ( "kCopie", "Copy");
define ( "kCopyright", "Bản quyền");
define ( "kCorpus", "Lựa chọn");
define ( "kCote", "Số"); // CNRS
define ( "kCoteOriginal", "Cote của bản gốc");
define ( "kCouleur", "Màu");
define ( "kCreditSite", "Thiết kế");
define ( "kCree", "tạo ra");
define ( "kCreerNotice", "Tạo mục");
define ( "kCreerNoticeAudio", "Tạo mặt âm thanh");
define ( "kCreerNoticePhoto", "Tạo mục hình ảnh");
define ( "kCreerNoticeVideo", "Tạo mục video");
define ( "kCritere", "hạn");
define ( "kDate", "Ngày");
define ( "kDateCommande", "Ngày thứ tự");
define ( "kDateCopie", "Ngày của các bản sao");
define ( "kDateCreation", "Ngày tạo");
define ( "kDateDiffusion", "Ngày phát sóng");
define ( "kDateDuree", "Ngày và thời gian");
define ( "kDateEmprunt", "vay ngày");
define ( "kDateEmpruntFormat", "Ngày của khoản vay (ngày / tháng / năm)");
define ( "kDateLivraisonSouhait", "chúc ngày giao hàng");
define ( "kDateModification", "Ngày cập nhật");
define ( "kDateNaissance", "Ngày sinh");
define ( "kDateProd", "Năm sản xuất");
define ( "kDateProduction", "Năm sản xuất");
define ( "kDatePVDebut", "Ngày bắt đầu chụp");
define ( "kDatePVFin", "Ngày cuối chụp");
define ( "kDateRetour", "Due date");
define ( "kDateRetourFormat", "Do hạn định dạng (ngày / tháng / năm)");
define ( "kDates", "Ngày");
define ( "kDateSaisie", "Ngày nhập");
define ( "kDeconnexion", "Thoát");
define ( "kDemandeLogin", "Yêu cầu đăng nhập");
define ( "kDemandeLoginTexte", "Hãy điền vào email đăng ký của bạn <br/> Một email có chứa định danh của bạn sẽ được gửi đến cho bạn.");
define ( "kDernierIntervenant", "Last cá nhân sử dụng");
define ( "kDescripteurs", "Từ khoá");
define ( "kDescripteursContextuels", "từ khóa theo ngữ cảnh");
define ( "kDescription", "Mô tả");
define ( "kDeselectionnerTout", "Bỏ chọn tất cả");
define ( "kDestination", "Vận chuyển khu vực");
define ( "kDestinationFrance", "Pháp");
define ( "kDestinationEurope", "Châu Âu CEE");
define ( "kDestinationInternational", "Các nước ngoài CEE");
define ( "kDiffuseur", "Brodcaster");
define ( "kDiffusion", "Brodcasting");
define ( "kDiscipline", "Kỷ luật");
define ( "kDivers", "Khác");
define ( "kDocNum", "Tài liệu số hóa");//?
define ( "kDocument", "Document");
define ( "kDocumentAcc", "Tài liệu kèm theo");
define ( "kDocumentaliste", "Documentalist");
define ( "kFils", "Trẻ em");
define ( "kDocumentsFils", "Trẻ em tài liệu");
define ( "kDocumentsLies", "tài liệu được Liên kết");
define ( "kDocumentsParents", "Cha tài liệu");
define ( "kDocuments", "Văn bản");
define ( "kDomaine", "Domain");
define ( "kDomaineActivite", "Tên miền của hoạt động");
define ( "kDroits", "quyền");
define ( "kDu", "Từ");
define ( "kDupliquer", "nhân bản");
define ( "kDuree", "Thời lượng");
define ( "kEmissions", "Chương trình");
define ( "kEmprunteur", "vay");
define ( "kEncodage", "Encoding");
define ( "kEnLigne", "Trên đường");
define ( "kEnregistrer", "Lưu");
define ( "kEntre", "Giữa");
define ( "kEnvoyer", "Gửi");
define ( "kEnvoyerMailUtilisateur", "Gửi một e-mail cho cá nhân sử dụng?");
define ( "kErreurChampsOblig", "lĩnh vực này là bắt buộc");
define ( "kErreurChampsObligJS", "Một số các trường là bắt buộc");
define ( "kErreurCoteExistant", "cote này đã tồn tại");
define ( "kErreurCoteMat", "vật liệu này đã tồn tại");
define ( "kErreurCoteStoryboard", "kịch bản này đã tồn tại");
define ( "kErreurCreerRepertoire", "Không thể tạo kho tàng");
define ( "kErreurEMailExiste", "This e-mail đã tồn tại");
define ( "kErreurEMailInvalide", "This e-mail không hợp lệ");
define ( "kErreurFatale", "Một lỗi đã xảy ra trên trang này <br/> Xin vui lòng liên hệ với cá nhân quản trị..");
define ( "kErreurFichierExistant", "File này đã tồn tại");
define ( "kErreurFichierExiste", "File này đã tồn tại");
define ( "kErreurFondsExistant", "Bộ sưu tập này đã tồn tại");
define ( "kErreurGroupeExistant", "Nhóm này đã tồn tại");
define ( "kErreurImageExiste", "hình ảnh này đã tồn tại");
define ( "kErreurLexiqueExiste", "thuật ngữ này đã tồn tại");
define ( "kErreurLoginExiste", "đăng nhập này đã tồn tại");
define ( "kErreurMotDePasseIdentique", "Hai mật khẩu không giống nhau");
define ( "kErreurPageNotFound", "Trang không tìm thấy");
define ( "kErreurNbDeb", "của bạn datas chứa");
define ( "kErreurNbFin", "sai lầm (s)");
define ( "kErreurNbSupport", "Vui lòng cho biết số lượng giao hàng vật liệu");
define ( "kErreurRenomerRepertoire", "Không thể đổi tên kho tàng");
define ( "kErreurRequeteExistant", "Một truy vấn với cùng một tiêu đề đã tồn tại");
define ( "kErreurSequence", "trình tự này đã tồn tại");
define ( "kErreurSocieteOblig", "Hãy điền vào tổ chức hay công ty của bạn");
define ( "kErreurSupportLiv", "Hãy chỉ ra các nguyên liệu cung cấp");
define ( "kErreurSupprimerFichier", "Không thể xóa tập tin");
define ( "kErreurUpload", "Upload failed");
define ( "kErreurUploadCopie", "Không thể sao chép tới");
define ( "kErreurUploadInterdit", "kiểu tệp là không được phép");
define ( "kErreurUploadRemplacer", "Không thể ghi đè lên tập tin");
define ( "kErreurUploadTaille", "File size là 0 octet");
define ( "kErreurUploadTailleMax", "kích thước tối đa của tập tin là");
define ( "kEt", "Và");
define ( "kEtape", "Bước");
define ( "kEtat", "Nhà nước");
define ( "kEtatDocument", "nhà nước của tài liệu");
define ( "kEtatMateriel", "nhà nước của vật liệu");
define ( "kEtatPan", "nhà nước của giỏ hàng");
define ( "kEvenement", "Sự kiện");
define ( "kExporter", "xuất khẩu");
define ( "kExporterFichier", "xuất khẩu trong một tập tin");
define ( "kExtrait", "Extract");
define ( "kExtraits", "Trích");
define ( "kFax", "fax");
define ( "kFermer", "Đóng");
define ( "kFichiers", "Files");
define ( "kFonction", "Chức năng");
define ( "kFonds", "Bộ sưu tập");
define ( "kFondsPere", "Bộ sưu tập tương đối");
define ( "kFondsSaisie", "Bộ sưu tập sửa đổi");
define ( "kFormat", "Định dạng");
define ( "kFormatDateCourt", "% Y /% m /% d");
define ( "kFormatMateriel", "Định dạng tài liệu");
define ( "kForme", "Mẫu");
define ( "kFormulaireCommande", "Đặt hàng");
define ( "kFraisExpedition", "Vận chuyển tỷ lệ");
define ( "kFusionDialogue", "Cảnh báo: một thuật ngữ tương tự đã tồn tại trong các tú điêm ! \\n \\nBạn có muốn kết hợp thuật ngữ này trong khác không?");
define ( "kGenealogie", "phả");
define ( "kGenererAutresVersions", "Tạo phiên bản khác");
define ( "kGenerique", "Đoàn làm phim");
define ( "kGenre", "Loại");
define ( "kGestion", "Quản lý");
define ( "kGestionCommandes", "Lệnh quản lý");
define ( "kGestionFonds", "Bộ sưu tập quản lý");
define ( "kGestionGroupes", "Tập đoàn quản lý");
define ( "kGestionLexique", "Từ điển quản lý");
define ( "kGestionMateriels", "Vật liệu quản lý");
define ( "kGestionPersonnes", "Cá nhân quản lý");
define ( "kGestionStoryboards", "Bảng phâm cảnh quản lý");
define ( "kGestionTables", "Hệ thống quản lý bảng");
define ( "kGestionUtilisateurs", "Cá nhân quản lý");
define ( "kGestionValeurs", "Bảng giá trị quản lý");
define ( "kGroupe", "Nhóm");
define ( "kGroupeSaisie", "Nhóm sửa đổi");
define ( "kHistorique", "Lược sử");
define ( "kHistoriqueComplet", "Hoàn thành lược sử");
define ( "kHistoriqueSession", "Kỳ họp thứ lược sử");
define ( "KId ","STT");
define ( "KID ","STT");
define ( "kIdentifiant", "Tên truy nhập");
define ( "kIdentification", "Nhận dạng");
define ( "kImage", "Hình ảnh");
define ( "kImages", "Hình ảnh");
define ( "kImageur", "Imager");
define ( "kImArch", "Lưu trữ hình ảnh");
define ( "kImport", "Nhập");
define ( "kImportVideo", "Nhập video");
define ( "kImportXML", "Nhập XML");
define ( "kImprimer", "In");
define ( "kImprimerCatalogue", "In văn bản");
define ( "kImprimerNotices", "In văn bản");
define ( "kIndex", "Mục lục");
define ( "kIndexation", "Indexation");
define ( "kInfosCompl", "Các tin");
define ( "kInfosTechniques", "kỹ thuật thông tin");
define ( "kInscription", "Đơn đăng ký");
define ( "kInscriptionValidee", "Đăng ký của bạn được xác nhận <br/> Bây giờ bạn có thể đăng nhập.");
define ( "kInscriptionPersonnelle", "Cá nhân khắc, không liên kết với một công ty");
define ( "kInscrivezVous", "Sign-In");
define ( "kIntervenants", "Intervenors");
define ( "kJuridique", "Juridique");
define ( "kLabo", "Labo");
define ( "kLaboratoire", "Phòng thí nghiệm");
define ( "kLancementVideo", "Phát động video");
define ( "kLangue", "Ngôn ngữ");
define ( "kLangues", "Ngôn ngữ");
define ( "kLexique", "Từ điển");
define ( "kLienDocumentMateriel", "Liên kết giữa các tài liệu và vật liệu");
define ( "kLieu", "Địa điểm");
define ( "kLieuDeces", "Nơi cái chết");
define ( "kLieuNaissance", "Nơi sinh");
define ( "kLieuNegatifs", "Nơi cho âm");
define ( "kLieuTournage", "Nơi chụp");
define ( "kListe", "Danh sách");
define ( "kListeCommandes", "Danh sách các đơn đặt hàng");
define ( "kListeDeValeur", "Danh sách các giá trị");
define ( "kListeDocuments", "Danh sách các tài liệu");
define ( "kListeDocumentAcc", "Danh sách các tài liệu tham gia");
define ( "kListeDocumentsLies", "Danh sách các tài liệu liên kết");
define ( "kListeFonds", "Danh sách các bộ sưu tập");
define ( "kListeFondsPrivileges", "Danh sách các bộ sưu tập và đặc quyền liên quan");
define ( "kListeGroupes", "Danh sách các nhóm cá nhân dùng của");
define ( "kListeLexiqueAssocie", "Danh sách các điều khoản liên quan từ điển");
define ( "kListeMateriels", "Danh sách các tài liệu");
define ( "kListePaniers", "Danh sách các xe");
define ( "kListePersonnes", "Danh sách các cá nhân");
define ( "kListeSequences", "Danh sách các cảnh phim");
define ( "kListeStoryboards", "Danh sách các bảng phân cảnh");
define ( "kListeTables", "Danh sách các bảng");
define ( "kListeUsagers", "Danh sách các cá nhân dùng");
define ( "kLivraison", "giao hàng");
define ( "kLivrer", "Phân phối");
define ( "kLogin", "Đăng nhập");
define ( "kLoginDemande", "dự kiến đăng nhập");
define ( "kMail", "Mail");
define ( "kMaster", "Thầy");
define ( "kMateriel", "Hỗ trợ video");
define ( "kMateriels", "Hỗ trợ video");
define ( "kMaterielsLies", "Hỗ trợ video nằm");
define ( "kMaterielSorti", "Hỗ trợ video ra");
define ( "kMaterielStockage", "lưu trữ các hỗ trợ video");
define ( "kMedia", "Media");
define ( "kMesCommandes", "đơn đặt hàng của tôi");
define ( "kMesRecherches", "Giỏ hăng");
define ( "kMetrage", "Số mét");
define ( "kMisAJour", "Cập nhật");
define ( "kModeDiffusion", "phân phối");
define ( "kModePaiement", "Thanh toán bằng");
define ( "kModePaiementCheque", "Check");
define ( "kModePaiementCR", "Ngân hàng chuyển khoản");
define ( "kModePaiementCB", "Thẻ tín dụng");
define ( "kModePaiementVISA", "Thẻ tín dụng VISA");
define ( "kModePaiementEUROCARD_MASTERCARD", "Eurocard / Mastercard");
define ( "kModePaiementE_CARD", "E-carte bleue");
define ( "kModifier", "Sửa đổi");
define ( "kMonProfil", "tài khoản");
define ( "kMonPanier", "My giỏ hàng");
define ( "kMosaique", "Mosaic");
define ( "kMotCle", "Từ khoá");
define ( "kMotCleGeographiqe", "địa lý từ khóa");
define ( "kMotDePasse", "Mật khẩu");
define ( "kMotDePasseOublie", "Quên mật khẩu?");
define ( "kMotDePasseRepeter", "Xác nhận");
define ( "kMsgBienvenue", "dòng chữ của bạn hiện nay là hợp lệ"); // CNRS
define ( "kMsgContinue", "Bạn có muốn xác nhận");//???
define ( "kMsgCoordonneesPerso", "Nhập địa chỉ cá nhân của bạn"); // CNRS
define ( "kMsgCoordonneesSociete", "Nhập địa chỉ chuyên nghiệp của bạn"); // CNRS
define ( "kMsgInscription", "Bạn đã được đăng ký, hãy nhập:"); // CNRS
define ( "kMsgMailEnvoye", "thư của bạn sẽ được gửi tại địa chỉ này:");
define ( "kMsgMauvaiseIdentification", kAccesReserve ." Bạn phải xác định cho bản thân hoặc để đăng ký");
define ( "kMsgMauvaisLogin", "Tình trạng đăng nhập hoặc mật khẩu");
define ( "kMsgPanierVide", "Bạn không có bất kỳ mục nào trong giỏ hàng của bạn");
define ( "kMsgPanierMauvaisType", "Cảnh báo một số mặt hàng không avaiable cho loại thứ tự");
define ( "kMsgPasDeRequetes", "Bạn không có bất kỳ truy vấn lưu");
define ( "kMsgPossibiliteConnexion", "Đối với bản demo này, bạn có thể kết nối ngay lập tức");
define ( "kMsgQTcheck", "trình duyệt của bạn không hỗ trợ kịch bản, nên bạn không thể kiểm tra cho QuickTime");
define ( "kMsgQTversion", "Bạn cần QuickTime");
define ( "kMsgSelectItem", "Hãy chọn mục bạn muốn đặt");
define ( "kMsgUsagerNonExistant", "Cá nhân sử dụng này không tồn tại, truy vấn của bạn đã được gửi đến các chủ trang web");
define ( "kNature", "Thiên nhiên");
define ( "kNbDocument", "Số yếu tố");
define ( "kNbElements", "Số cuốn");
define ( "kNbEpisodes", "Số tập");
define ( "kNbExemplaires", "Số");
define ( "kNbExemplairesCourt", "Số");
define ( "kNbExtrait", "Số Trích");
define ( "kNbFonds", "Số Bộ sưu tập");
define ( "kNbGroupes", "Số nhóm");
define ( "kNbImages", "Số ảnh");
define ( "kNbUsagers", "Số cá nhân sử dụng");
define ( "kNbSemainesProjections", "Số tuần");
define ( "kNo ","#");
define ( "kNom", "Tên");
define ( "kNombre", "Số");
define ( "kNombreDocumentsLies", "Số tài liệu được liên kết");
define ( "kNon", "Không");
define ( "kNoteInterne", "nội lưu ý");
define ( "kNote", "Ghi chú");
define ( "kNotes", "Ghi chú");
define ( "kNotesTitre", "Tiêu đề, ghi chú");
define ( "kNotice", "Mã");
define ( "kNoticesLiees", "Liên kết bài");
define ( "kNoUserAndDoc", "Loại trừ các quản trị viên và documentalists");
define ( "kNouveau", "mới");
define ( "kNouveauLexique", "mới từ điển mục mới");
// DOUBLON define ( "kNouveauMat", "vật liệu mới");
define ( "kNouveauMateriel", "vật liệu mới");
define ( "kNouveauPers", "Cá nhân mới");
define ( "kNouvellePersonne", "Cá nhân mới");
define ( "kNouvelleSequence", "mới trình tự");
define ( "kNumero", "Số");
define ( "kNumeroCommande", "đặt hàng số");
define ( "kNumeroNotice", "Mã số");
define ( "kNumeroTransaction", "số lượng giao dịch");
define ( "kNumeroTVA", "châu Âu số thuế GTGT");
define ( "kObjet", "Đối tượng");
define ( "kOnline", "trực tuyến");
define ( "kOrdre", "đặt hàng");
define ( "kOrganisation", "Công ty");
define ( "kOriginal", "bản gốc");
define ( "kOrigine", "origine");
define ( "KOu", "Hoặc");
define ( "kOui", "Có");
define ( "kPages", "Page (s)");
define ( "kPaiementSucces", "Cám ơn bạn mua <br/> Bạn có thể in các chi tiết sau đây đặt hàng của bạn..");
define ( "kPaiementRefuse", "Thanh toán đã bị từ chối <br/> Các chương trình đã ra lệnh đã được thay thế các mặt hàng..");
define ( "kPaiementAnnule", "Thanh toán đã được hủy bỏ <br/> Các chương trình đã ra lệnh đã được thay thế các mặt hàng..");
define ( "kPanier", "Lựa chọn của bạn");
define ( "kPanierAjoutDoc", "Thêm in ..");
define ( "kPanierEnCours", "Giỏ hàng trong quá trình điều trị");
define ( "kPanierFrais", "Thông lệ phí");
define ( "kPanierNouveau", "mới lựa chọn");
define ( "kPanierTotalHT", "Tổng cộng (VAT-Việt)");
define ( "kPanierTotalProduits", "Tổng số thứ tự");
define ( "kPanierTotalTTC", "Tổng cộng (VAT inc .)");
define ( "kPanierTraite", "hàng được điều trị");
define ( "kPanierTransfertDoc", "Move to ...");
define ( "kPanierVide", "giỏ hàng là trống rỗng");
define ( "kParent", "Chung");
define ( "kPasHistorique", "Không có lịch sử");
define ( "kPatientez", "Xin vui lòng chờ ...");
define ( "kPays", "Quốc gia");
define ( "kPDM", "thị trường chia sẻ");
define ( "kPersonnalite", "Personnalities");
define ( "kPersonne", "Cá nhân");
define ( "kPersonnePhysique", "vật lý cá nhân");
define ( "kPersonneMorale", "đạo đức cá nhân");
define ( "kPhoto", "Hình ảnh");
define ( "kPlus", "thêm");
define ( "kPortable", "GSM");
define ( "kPortablePerso", "Personnal GSM");
define ( "kPrecision", "Precision");
define ( "kPremieresLettres", "Tìm kiếm:");
define ( "kPrenom", "First name");
define ( "kPresentation", "trình bày");
define ( "kPret", "vay");
define ( "kPrivilege", "ưu đãi");
define ( "kPrix", "Chi phí");
define ( "kPrixHT", "Chi phí (thuế VAT-Việt)");
define ( "kProcAudiovisuelle", "nghe nhìn quá trình");
define ( "kProducteur", "Hãng sản xuất");
define ( "kProducteurAutre", "đã đưa hãng sản xuất");
define ( "kProduction", "Hãng sản xuất");
define ( "kProfil", "Hồ sơ");
define ( "kPublic", "Công");
define ( "kRaisonSociale", "Tổ chức");
define ( "kRealisateur", "giám đốc");
define ( "kRecherche", "Tìm kiếm");
define ( "kRechercheAvancee", "Tìm kiếm nâng cao");
define ( "kRechercheExperte", "chuyên gia tìm kiếm");
define ( "kRechercheFonds", "Tìm kiếm trong bộ sưu tập một");
define ( "kRechercheSimple", "Tìm kiếm đơn giản");
define ( "kRechercheTouteDate", "Bất kỳ ngày");
define ( "kRechercheToutesDates", "Bất kỳ ngày");
define ( "kRechercheTypeRechFullText", "Toàn văn");
define ( "kRechercheTypeRechFullTextEtendu", "mở rộng toàn văn");
define ( "kRechercheTypeRechSelect", "Trong số các");
define ( "kRechercheTypeRechExact", "Chính xác");
define ( "kRechercheTypeRechEgal ","=");
define ( "kRecomp", "Giải thưởng");
define ( "kRediffusion", "Rediffusions");
define ( "kReinitialiser", "Xác lập lại");
define ( "kReferenceMateriel", "Số lưu trữ");
define ( "kRefStoryboard", "tham khảo của kịch bản");
define ( "kRefMat", "tham khảo tài liệu");
define ( "kRemarques", "chú");
define ( "kRequete", "truy vấn");
define ( "kResultats", "Kết quả (s)");
define ( "kResultatsParPage", "Kết quả trên mỗi trang");
define ( "kResume", "Tóm lược");
define ( "kResumeCatalogue", "Tóm lược danh mục");
define ( "kRetour", "Quay lại");
define ( "kRetourPanier", "Quay lại Giỏ hàng");
define ( "kRetourRecherche", "Trở về để tìm kiếm");
define ( "kReversement", "Chuyển");
define ( "kRole", "Role");
define ( "kSaisie", "Nhập dữ liệu");
define ( "kSanstitre", "Chưa có tiêu đề");
define ( "kSauf", "Không");
define ( "kSauvegarderRequete", "Lưu truy vấn");
define ( "kSessionExpiree", "Kỳ họp thứ hết!");
define ( "kSelection", "Lựa chọn");
define ( "kSelections", "Selections");
define ( "kSelectionner", "Chọn");
define ( "kSelectionnerTout", "Chọn tất cả");
define ( "kSelectionnerDocument", "Chọn một mục");
define ( "kSelectionnerStoryboard", "Chọn một cốt truyện");
define ( "kSelectionnerTerme", "Chọn một thuật ngữ");
define ( "kSequence", "Cảnh phim");
define ( "kSequences", "Cảnh phim");
define ( "kSociete", "Tổ chức / công ty");
define ( "kSon", "Tiếng");
define ( "kSonorisation", "âm thanh");
define ( "kSorti", "Out");
define ( "kSortieMateriel", "Chất liệu Mouvement");
define ( "kSource", "Nguồn");
define ( "kSousTitre", "phụ đề");
if (!defined('kSousTitrage')) define ( "kSousTitrage", "phụ đề");
define ( "kSpecifiques", "cụ thể");
define ( "kStandard", "chuẩn");
define ( "kStatistiques", "Thống kê");
define ( "kStatut", "Tình trạng");
define ( "kStockage", "Lưu trữ");
define ( "kStoryboard", "Bảng phân cảnh");
define ( "kSupport", "Hỗ trợ");
define ( "kSupportDiffusion", "Giao hỗ trợ");
define ( "kSupportLivraison", "Giao hỗ trợ");
define ( "kSupportLivraisonCourt", "Hỗ trợ");
define ( "kSupportOriginal", "Original Hỗ trợ");
define ( "kSupprimer", "Xóa");
define ( "kSupprimerSelection", "Xóa lựa chọn");
define ( "kSupprimerTout", "Xóa tất cả");
define ( "kSynonyme", "đồng nghĩa");
define ( "kSynonymePreferentiel", "ưu tiên từ đồng nghĩa");
define ( "kTable", "Bảng");
define ( "kTabule", "Tabulated");
define ( "kTarifs", "vé");
define ( "KTC", "TC");
define ( "kTCdst", "TC Đích");
define ( "kTCin", "TC tại");
define ( "kTCout", "TC ra");
define ( "kTCsrc", "TC Source");
define ( "kTelephone", "Điện thoại");
define ( "kTelephonePerso", "cá nhân điện thoại");
define ( "kTerme", "Từ");
define ( "kTermeEtat", "Nhà nước hạn");
define ( "kTermesAssocies", "liên kết với các điều khoản");
define ( "kTermesFils", "Trẻ em thuật ngữ");
define ( "kTermeGenerique", "chính thuật ngữ");
define ( "kTermesSpecifiques", "điều khoản cụ thể");
define ( "kTexte", "Văn bản");
define ( "kTextes", "nội dung");
define ( "kTheme", "Chủ đề");
define ( "kTimecode", "Thời-mã");
define ( "kTitre", "Tên phim");
define ( "kTitreCol", "Bộ sưu tập Tiêu đề");
define ( "kTitreExtrait", "Extract Tiêu đề");
define ( "kTitreOriginal", "Original Tiêu đề");
define ( "kTitreParent", "Tiêu đề của bố tài liệu");
define ( "kTitres", "đề");
define ( "kTournage", "Bắn");
define ( "kTous", "Tất cả");
define ( "kTousDocuments", "Tất cả các tài liệu");
define ( "kTousDomaines", "Tất cả các lĩnh vực");
define ( "kTousEvenements", "Tất cả các sự kiện");
define ( "kTousFonds", "Tất cả các bộ sưu tập");
define ( "kTousGenres", "Tất cả các thể loại");
define ( "kTousLieux", "Tất cả những nơi");
define ( "kTousMotsCles", "Tất cả các từ khóa");
define ( "kTousThemes", "Tất cả các chủ đề");
define ( "kToutesPersonnalites", "Mọi personnalities");
define ( "kType", "Kiểu");
define ( "kTypeCommande", "Sắp xếp loại");
define ( "kTypeDocument", "Loại tài liệu");
define ( "kTypeEmission", "Kiểu của chương trình");
define ( "kTypeUtilisation", "Loại hình sử dụng");
define ( "kUsager", "Cá nhân dùng");
define ( "kUsagerCrea", "Tạo bởi");
define ( "kUsagerModif", "Lần sửa đổi bởi");
define ( "kUsagerSaisie", "Cá nhân dùng chỉnh sửa");
define ( "kValeur", "Giá trị");
define ( "kValeurs", "Giá trị");
define ( "kValider", "Gửi");
define ( "kVersion", "Phiên bản");
define ( "kVersionInternationale", "Internationale phiên bản");
define ( "kVille", "thành phố");
define ( "kVisa", "Tác giả nhằm mục đích");
define ( "kVisionner", "Xem");
define ( "kVotreLogin", "của bạn đăng nhập");
define ( "kVotreMotDePasse", "mật khẩu của bạn");
define ( "kVotreRecherche", "của bạn truy vấn");
define ( "kWait", "Xin vui lòng chờ ...");

define ( "kAnnee", "Năm");
define ( "kAppartientA", "Thuộc về");
// DOUBLON define ( "kCritere", "Tiêu chuẩn");
define ( "kDateDebut", "Ngày bắt đầu");
define ( "kDateFin", "Cuối ngày");
define ( "kErrorNoLogin", "Xin vui lòng đăng nhập");

// LUẬT
define ( "kErrorAccesProfil", "Bạn không được phép sửa đổi thành viên này");
define ( "kErrorCreationDoc", "tài liệu này không thể được tạo ra");
define ( "kErrorSauveDoc", "tài liệu này không thể lưu được");
define ( "kErrorSauveDocLex", "Các mục lexicon liên quan không thể lưu được");
define ( "kErrorSauveDocVal", "Sự kết hợp các giá trị không thể lưu được");
define ( "kErrorSupprDoc", "Những tài liệu hoặc phiên bản không thể được xóa");
define ( "kErrorSauveDocLien", "Sự liên kết giữa các tài liệu không thể lưu được");
define ( "kErrorSauveAutreVersion", "Các phiên bản khác không thể lưu được");
define ( "kErrorDocSeqExisteDeja", "trình tự này đã tồn tại");
define ( "kErrorCreationDocSeq", "trình tự này không thể được tạo ra");
define ( "kErrorSauveDocSeq", "trình tự này không thể được cập nhật");
define ( "kErrorSauveDocMat", "Điều này liên kết đến một tài liệu không thể được cập nhật");
define ( "kErrorCoteExistant", "cote này đã được sử dụng bởi các tài liệu khác");
define ( "kSuccesDocSauve", "Tài liệu đã lưu");
define ( "kErrorDocImpossibleAssocierImageur", "Không thể đính kèm này kịch bản cho tài liệu này");
define ( "kSuccesSupprDoc", "Tài liệu đã được xoá thành công");
define ( "kErrorDocExistePas", "Tài liệu này không tồn tại");
define ( "kErrorDocNoId", "Thiếu ID");
define ( "kErrorDocSauveSinequa", "Lỗi khi tiết kiệm trong Sinequa");

define ( "kConfirmJSDocSuppression", "Bạn có xác nhận xóa");//???
define ( "kConfirmJSDocDuplication", "Bạn có xác nhận bản sao");//???
define ( "kConfirmJSDocSupprVersion", "Bạn có xác nhận để xóa phiên bản này?");
define ( "kMsgSauvegardeModif", "bạn có muốn lưu các thay đổi");//???

// LEXIQUE
define ( "kErrorLexiqueExisteDeja", "Mục nhập này đã tồn tại");
define ( "kErrorLexiqueSauve", "Mục nhập này không thể lưu được");
define ( "kErrorLexiqueCreation", "Mục nhập này không thể được tạo ra");
define ( "kErrorLexiqueVersionSauve", "Những phiên bản này không thể lưu được");
define ( "kErrorLexiqueCreationSyno", "đồng nghĩa này không thể được tạo ra");
define ( "kErrorLexiqueSauveSyno", "đồng nghĩa này không thể lưu được");
define ( "kErrorLexiqueAttachChildren", "Các mục trẻ em không thể được liên kết");
define ( "kErrorLexiqueCreationAsso", "Các từ liên quan không thể được tạo ra");
define ( "kErrorLexiqueSauveAsso", "Các từ liên quan không thể lưu được");
define ( "kErrorLexiqueDetacheTous", "Các em có thể không được tách ra từ cụm từ này");
define ( "kErrorLexiqueSuppr", "Mục nhập này có thể không được xóa");
define ( "kWarningLexiqueSynonymePreferentiel", "Cảnh báo, thuật ngữ này đã là đồng nghĩa ưa thích <br/> Nếu bạn muốn thay đổi dữ liệu., xin theo liên kết này.");
define ( "kConfirmLexiqueFusion", "Bạn có muốn hợp nhất hai điều khoản");//???
define ( "kErrorLexiqueFusionNoId", "Impossible để nhập các điều khoản, thiếu định danh");
define ( "kErrorLexiqueFusion", "Impossible để nhập các điều khoản");
define ( "kLexiqueNbDocsLies", "tài liệu được Liên kết");
define ( "kLexiqueNbPersLies", "Cá nhân được Liên kết");
define ( "kLexiqueTermeSansLien", "Không có liên kết");
define ( "kSuccesLexiqueDetacheLien", "Liên kết thành công bị hủy bỏ");
define ( "kValide", "hiệu lực");
define ( "kCandidat", "Ứng Viên");

// USAGER
define ( "kErrorUsagerExisteDeja", "Cá nhân sử dụng này đã tồn tại");
define ( "kErrorUsagerSauve", "Cá nhân sử dụng không thể lưu được");
define ( "kErrorUsagerCreation", "Cá nhân sử dụng không thể được tạo ra");
define ( "kErrorUsagerSuppr", "Cá nhân sử dụng không thể được xóa");
define ( "kSuccesSauveUsager", "Cá nhân dùng đã lưu");
define ( "kErrorUsagerDernierAdmin", "Tài khoản này là quản trị viên duy nhất còn lại Không thể xóa..");
define ( "kMsgUtilisateurInconnu", "Cá nhân sử dụng này không tồn tại");
define ( "kMessageCompteNonModifiable", "Tài khoản này là không thể chỉnh sửa");

define ( "kMailLoginDemandeCorps", "đây là Authentification dữ liệu: bạn \n \n
		Đăng nhập:% s \n
		Mật khẩu:% s \n \n
		Hẹn gặp lại vào ".kCheminHttp." \n \n
		Mail tự động tạo ra \n. ");
define ( "kMailLoginDemandeSujet", "Tiếp cận".gSite);

define ( "kMailLoginDemandeNoUserCorps", "Cá nhân sử dụng cho việc sau đây: \n
		E-mail:% s \n \n
		Mail tự động tạo ra \n. ");
define ( "kMailLoginDemandeNoUserSujet", "Đăng nhập yêu cầu".gSite);

define ( "kMailLoginValidationSujet", "Đăng ký xác nhận vào".gSite);
define ( "kMailLoginValidationCorps", "đăng ký của bạn đã được xác nhận \n!
		Authentification của bạn là: \n \n
		Đăng nhập:% s \n
		\n \nSee bạn ngay trên ".kCheminHttp." \n
		Mail tự động tạo ra \n. ");

define ( "kMailDemandeInscriptionSujet", "Đăng ký vào".gSite);
define ( "kMailDemandeInscriptionCorps", "Đăng ký vào".gSite."\n \n
		Mới đăng ký yêu cầu ".kCheminHttp." : \n \n
		Tên:% s \n
		First name:% s \n
		Công ty:% s \n
		E-mail:% s \n \n
		Đăng nhập:% s \n
		\n \nMail tự động tạo ra \n. ");

define ( "kMemoriserConnexion", "Ghi nhớ tôi");
define ( "kInscriptionEntete", "Hãy điền vào các thuê bao dưới đây");
define ( "kPasEncoreInscrit", "Không có đăng ký");//???
define ( "kActif", "hoạt động");

// VALEUR
define ( "kCodeValeur", "Giá trị mã");
define ( "kErrorValExisteDeja", "Giá trị này đã tồn tại");
define ( "kErrorValSauve", "Giá trị không thể lưu được");
define ( "kErrorValCreation", "Giá trị không thể được tạo ra");
define ( "kErrorValSuppr", "Giá trị không thể xóa");
define ( "kWarningValDejaAttachee", "Cảnh báo: giá trị này đã được gắn vào một");
define ( "kErrorValAttache", "Giá trị không thể được gắn vào một");
define ( "kErrorDetacheTous", "Các em có thể không được tách ra các giá trị này");
define ( "kValeursRattacheesA", "các giá trị được Liên kết");
define ( "kErrorValFusionPbId", "sát nhập không thực hiện, nhận dạng là thiếu");
define ( "kErrorValFusion", "sát nhập không thể được thực hiện");
define ( "kJSConfirmFusion", "Bạn có muốn kết hợp những giá trị");//???


// ALERT JAVASCRIPT
define("kJSConfirmSauverModif","Do you want to save changes ?");
define ( "kJSConfirmSupprUser", "Bạn có chắc chắn muốn xoá cá nhân sử dụng");
define ( "kJSErrorElementOblig", "Xin vui lòng thêm ít nhất một mục \\n");
define ( "kJSErrorFloat", "Xin vui lòng nhập một số float \\n"); // Attention aux 2 dấu xồ nguợc!
define ( "kJSErrorOblig", "Trường này là bắt buộc \\n");
define ( "kJSErrorInt", "Xin vui lòng nhập một số nguyên null không \\n");
define ( "kJSErrorLoginOblig", "Hãy điền tên đăng nhập \\n");
define ( "kJSErrorMail", "Hãy điền vào một thư đúng \\n");
define ( "kJSErrorNomOblig", "Hãy điền một tên \\n");
define ( "kJSErrorPasswordOblig", "Hãy điền vào mật khẩu \\n");
define ( "kJSErrorValeurOblig", "Hãy điền vào một giá trị \\n");
define ( "kJSErrorTitreOblig", "Hãy điền vào một tiêu đề \\n");
define ( "kJSErrorYear", "Hãy một năm hợp lệ \\n");
define ( "kJSErrorValeurExisteDeja", "Giá trị này là đã có trong danh sách");

define ( "kJSErrorPrenomOblig", "Hãy điền tên đầu tiên \\n");
define ( "kJSErrorSocieteOblig", "Hãy điền vào các tổ chức \\n");
define ( "kJSErrorAdresseOblig", "Hãy điền vào địa chỉ \\n");
define ( "kJSErrorVilleOblig", "Hãy điền vào các thành phố \\n");
define ( "kJSErrorCodePostalOblig", "Hãy điền vào các mã zip \\n");
define ( "kJSErrorPaysOblig", "Hãy điền vào các quốc gia \\n");
define ( "kJSErrorFonctionOblig", "Hãy điền tên miền của hoạt động \\n");
define ( "kJSErrorTelOblig", "Hãy điền vào các điện thoại \\n");
define ( "kJSErrorMotDePasseDifferent", "Hãy nhập mật khẩu cùng \\n");

// Generique
define ( "kDocumentParent", "Parent tài liệu");
define ( "kSansParent", "Không có cha mẹ");
define ( "kNouveauDoc", "Thêm một tài liệu");
define ( "kNouveauLexicon", "Thêm một mục từ điêm");
define ( "kNouveauUsager", "Thêm một cá nhân sử dụng");
define ( "kNouveauValeur", "Thêm một giá trị");
define ( "kNouveauMat", "Thêm một vật liệu");
define ( "kPlusCriteres", "thêm tiêu chí");
define ( "kGestionTableSysteme", "Hệ thống quản lý bảng");
define ( "kFlecheMonter", "Một bước lên");
define ( "kFlecheDescendre", "Một bước xuống");
define ( "kDeplacer ","");

define ( "kRattacheA", "kèm theo");
define ( "kTexteLibre", "Văn bản");
define ( "kWarningTypeLexique", "Các loại không thể thay đổi, trừ khi lexicon cụm từ này không phải là liên kết với bất kỳ mục khác (cha mẹ hoặc trẻ em) và tài liệu nào hoặc cá nhân");
define ( "kWarningTypeValeur", "Các loại không thể thay đổi, trừ khi giá trị này không phải là liên kết với bất cứ giá trị khác (cha mẹ hoặc trẻ em) và tài liệu nào hoặc cá nhân");

// UPLOAD
define ( "kUploadErreurInitFlash", "Hợp phần Flash tải lên không thể được thiết lập");
define ( "kUploadDocAccSucces", "Các tài liệu đính kèm đã được thành công nhất");
define ( "kUploadDocAccEchec", "Không thể thêm tài liệu đính kèm");
define ( "kUploadFileSucces", "Các tập tin sau đây đã được chuyển giao thành công:");
define ( "kUploadPrecisions", "Xin đừng đóng cửa sổ này sẽ xảy ra trong khi chuyển <br/> Trong trường hợp chuyển giao tập tin lớn, bạn có thể gặp một số nắm giữ trong thanh tiến bộ..");
define ( "kUploadMaterielSucces", "Các tập tin phương tiện truyền thông đã được chuyển giao thành công");
define ( "kUploadMaterielEchec", "Không thêm các tập tin media này");
define ( "kUploadMaterielWarning", "Cảnh báo");
define ( "kBatchUpload", "Batch tải lên các tài liệu");
define ( "kBatchUploadJava", "Batch tải lên các tài liệu (JAVA)");
define ( "kBatchUploadJavaDisclaimer", "Phương pháp Java là cao được đề nghị với các tập tin lớn <br/> Nó đòi hỏi sự hiện diện của các thời gian chạy Java trên máy tính của bạn.. <a target = '_blank' href = 'http://www.java com '> http://www.java.com </. a> ");
define ( "kGenererDoc", "Tạo tài liệu");
define ( "kGenererStoryboard", "Tạo ra các bảng phân cảnh");
define ( "kPrefixe", "Tiền tố cho các tài liệu");
define ( "kUploadAnalyseMedia", "Bắt thông tin từ tập tin ...");
define ( "kImportMetadonnees", "Nhận thông tin từ file (s)");
define ( "kUploadTranscodage", "cũng tạo ra một tập tin với các định dạng:");
define ( "kUploadTranscodageErreurNoSource", "Không có nguồn tập tin");
define ( "kUploadTranscodageErreurNoBinary", "Transcoding công cụ mất tích");
define ( "kUploadTranscodageErreurNoFormat", "Không có định dạng đầu ra được lựa chọn");
define ( "kUploadTranscodageErreurFormatNonGere", "Yêu cầu không được hỗ trợ định dạng");
define ( "kUploadTranscodageErreurFormatExisteDeja", "Các tập tin ban đầu đã định dạng này");
define ( "kUploadTranscodageErreurEchec", "Không tạo ra các phương tiện thông tin bổ sung");
define ( "kUploadTranscodageEnCours", "Tạo thêm các định dạng ...");
define ( "kUploadTranscodageSucces", "Additionnal định dạng thế hệ hoàn toàn");
define ( "kUploadTranscodageEchec", "Không khi tạo thêm các định dạng");
define ( "kUploadDebutTraitement", "Bắt đầu chế biến các tập tin được tải lên. <br> <b> Xin vui lòng không đóng cửa sổ này, trong khi chế biến. </ b>");
define ( "kUploadFinTraitement", "cuối cùng của chế biến <br> Bạn có thể đóng cửa sổ này..");
define ( "kUploadResultat", "Upload: tình trạng và chế biến");
define ( "kUploadTranscodageBilan", "thế hệ:");
define ( "kUploadSelectionFichiers", "File chọn");
define ( "kUploadParametres", "Tham số");
define ( "kUploadTranscodageErreurQTFastStart", "Không áp dụng nhanh Bắt đầu vào tập tin");
define ( "kUploadStart", "phóng lên");
define ( "kUploadSujetFinUpload", "tập tin mới đã được tải lên trên trang web");

// xUPLOAD: thư du composant xUpload
define ( "kXUploadTotalExceeded", "Nói chung tôi đa kích thước vượt quá <br> Hãy dừng việc chuyển giao.. <br> kích thước tối đa cho phép là:");
define ( "kXUploadTotalMini", "Nói chung min kích thước bên dưới. <br> Hãy dừng việc chuyển giao <br> Min cho phép kích thước là:");
define ( "kXUploadFileExceeded", "vượt quá kích thước Bỏ qua..");
define ( "kXUploadFileMini", "dưới đây là kích thước tập tin min Bỏ qua..");
define ( "kXUploadNoTempDir", "Không có thư mục tạm thời hàng temp_dir Xin hỏi admin để kiểm tra tham số này.!");
define ( "kXUploadNoTargetDir", "Không có thư mục tiêu hàng Xin hỏi admin để kiểm tra tham số này.!");
define ( "kXUploadNoTemplate", "Không tìm thấy mẫu Xin hỏi admin để kiểm tra tham số này.!");
define ( "kXUploadTransferComplete", "Chuyển hoàn tất!");
define ( "kXUploadUploadFailed", "Không khi chuyển giao");
define ( "kXUploadNullFile", "có kích thước null hoặc đường dẫn sai");
define ( "kXUploadBadFile", "không phải là một tên tập tin được phép Bỏ qua.!");
define ( "kXUploadBadExt", "không có phần mở rộng cho phép Bỏ qua.!");
define ( "kXUploadTooMany", "đã được lưu lại bởi vì đa số các tập tin được vượt quá");
define ( "kXUploadAlreadyExist", "đã tồn tại <br> Đổi tên! tới");
define ( "kXUploadSaved", "đã được lưu thành công");
define ( "kXUploadWrongPass", "mật khẩu sai <br> chuyển Unallowed..");
define ( "kXUploadUnwantedIp", "Bạn không được phép chuyển các tập tin");


// PERSONNES
define ( "kRecherchePersonne", "Cá nhân tìm kiếm");
define ( "kCoordonnees", "Vị trí");
define ( "kErrorPersonneCreation", "Cá nhân không thể được khởi tạo");
define ( "kErrorPersonneExistant", "Cá nhân này đã tồn tại");
define ( "kErrorPersonneSauve", "Cá nhân không thể lưu được");
define ( "kErrorPersonneSuppr", "Cá nhân không thể xóa");
define ( "kErrorSauvePersLex", "Các điều khoản liên quan không thể lưu được");
define ( "kErrorSauvePersVal", "Sự kết hợp các giá trị không thể lưu được");
define ( "kErrorPersonneVersionSauve", "Các phiên bản khác không thể lưu được");
define ( "kPersonnesLiees", "Cá nhân được Liên kết");
define("kMsgDoublon","Another person already exists with the same name");
define("kConserver","Keep");
define("kFusionner","Merge");

// DOC ACC
define ( "kErrorDocAccCreation", "tập tin đính kèm này không thể được tạo ra");
define ( "kErrorDocAccNoId", "Không có ID chỉ định");
define ( "kWarningDocAccFichierUtilise", "tập tin đính kèm này cũng được gắn ở những nơi khác");
define ( "kErrorDocAccExisteDeja", "attachement này đã tồn tại");
define ( "kErrorDocAccSauve", "tập tin đính kèm này không thể lưu được");
define ( "kErrorSauveDocAccVal", "Sự kết hợp các giá trị không thể lưu được");
define ( "kErroDocAccVersionSauve", "Các phiên bản khác không thể lưu được");
define ( "kVignette", "Tác giả");
define ( "kDocAccFichierManquant", "Missing file");
define ( "kErrorDocAccRenommageFichier", "Không thể đổi tên file");


// MATERIELS
define ( "kErrorSauveMatVal", "Sự kết hợp các giá trị không thể lưu được");
define ( "kErrorSauveDocMat", "Các hiệp hội với các tài liệu không thể lưu được");
define ( "kErrorMaterielSauve", "Nguyên liệu không thể lưu được");
define ( "kErrorMaterielRenommageFichier", "Các tập tin liên quan đến nguyên liệu này không thể được đổi tên thành");
define ( "kSuccesMaterielRenommageFichier", "Các tập tin liên quan đến nguyên liệu này đã được di chuyển thành công");
define ( "kSuccesMaterielSauve", "Chất liệu được lưu");
define ( "kSuccesMaterielCreation", "Chất liệu tạo ra");
define ( "kErrorMaterielVide", "Trường 'Vật liệu' không thể để trống");
define ( "kErrorMaterielExisteDeja", "vật liệu này đã tồn tại");
define ( "kMaterielFichierExiste", "File Ok!");
define ( "kMaterielFichierManquant", "Missing file");
define ( "kUpload", "Tải lên một tập tin ...");
define ( "kCharger", "Tải lên...");
define ( "kChoisir", "Chọn ...");
// DOUBLON define ( "kMaterielsLies", "Hỗ trợ video được Liên kết");
define ( "kErrorMaterielLieuImpossibleCreerDir", "Lỗi khi tạo thư mục:");
define ( "kErrorMaterielLieuImpossibleDeplacerFichier", "Lỗi trong khi di chuyển các tập tin");
define ( "kErrorMaterielExistePasDansDir", "Các tập tin liên quan đến nguyên liệu này không tồn tại trong thư mục:");
define ( "kLocalisationServeur", "máy chủ lưu trữ");
define ( "kInfosFichier", "tập tin thông tin");
define("kNumerise","Kỹ thuật số");

// DOC Mat
define ( "kErrorSauveDocMatVal", "Lỗi: không thể kết hợp các giá trị này liên kết giữa các tài liệu và vật liệu");
define ( "kErrorDocMatVide", "Không Mã cung cấp. Abandonned");
define ( "kErrorDocMatSauve", "Lỗi trong khi lưu những thông tin");
define ( "kSuccesDocMatSauve", "Những thông tin đã được lưu");

// DOM
define ( "kDOMlimitvalues", "tối đa giá trị (s) chỉ");
define ( "kDOMnodouble", "Giá trị này là đã có trong danh sách");

// PANIER
define ( "kAjouterTousResultatsPanier", "Thêm tất cả các kết quả vào thẻ");
define ( "kConfirmPanierCreation1", "Bạn có chắc chắn muốn tạo");
define ( "kConfirmPanierCreation2", "trong thư mục");
define ( "kConfirmPanierDeplacer", "Bạn có chắc là bạn muốn di chuyển thư mục này để");
define ( "kConfirmPanierDupliquer", "Bạn có chắc chắn muốn lặp thư mục này");//???
define ( "kConfirmPanierMove", "Bạn có chắc là bạn muốn di chuyển thư mục đó?");
define ( "kConfirmPanierRename", "Bạn có xác nhận tên mới:");
define ( "kConfirmPanierSupprimer", "Bạn có chắc chắn muốn xóa thư mục đó (và tất cả nội dung của nó)");
define ( "kConfirmPanierTransmettre", "Bạn có chắc chắn muốn chuyển thư mục hiện tới");
define ( "kEntetePanier", "Bạn có thể đặt các yếu tố trong giỏ hàng của bạn và đã giao cho họ bằng cách tải về ngay lập tức hoặc DVD / lô hàng nguyên liệu khác");
define ( "kErrorFolderLigneExisteDeja", "Tài liệu này đã có trong thư mục đó");
define ( "kErrorLignesExistentDeja", "đã tồn tại");
define ( "kErrorPanierCopieLignesVides", "Không có gì trong đoạn trích các-book");
define ( "kErrorPanierCreationNoName", "Hãy cung cấp cho các thư mục có tên");
define ( "kErrorPanierCreationNoParent", "Hãy chọn một thư mục mẹ");
define ( "kErrorPanierDeplacer", "Không thể di chuyển thư mục này hoặc lựa chọn");
define ( "kErrorPanierDeplacerDejaParent", "thư mục này đã có chứa mục hiện hành");
define ( "kErrorPanierDeplacerDossierInterdit", "Bạn không thể chọn các thư mục chính nó");
define ( "kErrorPanierLigneExisteDeja", "Tài liệu này đã có trong giỏ hàng của");
define ( "kErrorPanierMoveSame", "Hãy chọn hai thư mục khác nhau");
define ( "kErrorPanierNoId", "Error: missing id");
define ( "kErrorPanierRemoveLigne", "Không thể xoá dòng này:");
define ( "kErrorPanierRenameVide", "Hãy đặt tên cho thư mục đó");
define ( "kErrorPanierSauveLigne", "Không thể thêm tài liệu này");
define ( "kErrorPanierSauveLigneManqueIdDoc", "Không thể thêm tài liệu này");
define ( "kErrorPanierSupprimer", "Không thể xóa thư mục đó");
define ( "kErrorPanierTransfertDoublon", "Cảnh báo!% s tài liệu là đã có trong thư mục đó");
define ( "kErrorPanierTransmettreNoUsager", "Hãy chọn một cá nhân sử dụng");
define ( "kErrorPanierAccessDenied","Bạn không có quyền truy cập vào các lựa chọn này");
define ( "kErrorPanierInvalidIDpanier","Không có lựa chọn tương ứng");
define ( "kFolders", "thư mục");
define ( "kPanierCreation", "Create");
define ( "kPanierCreationNom", "tên");
define ( "kPanierCreationParent", "ở");
define ( "kPanierDeplacer", "Di chuyển thư mục này");
define ( "kPanierDossierParent", "Parent thư mục");
define ( "kPanierDupliquer", "Sao chép thư mục này");
define ( "kPanierSelections", "giỏ hàng và thư mục");
define ( "kPanierSession", "của tôi lựa chọn");
define ( "kPanierSuccesSauve", "Hàng cập nhật");
define ( "kPanierSupprimer", "Xóa một thư mục");
define ( "kPanierTransmettre", "Chuyển tới");
define ( "kPanierTypeDossier", "một thư mục");
define ( "kPanierTypeSelection", "một sự lựa chọn");
define ( "kQuestionEnvoyerMailClient", "Cảnh báo cá nhân dùng về những thay đổi này?");
define ( "kSuccesFolderDocumentAjoute", "Tài liệu được bổ sung vào thư mục đó");
define ( "kSuccesFolderDocumentRetire", "Tài liệu đã xóa từ thư mục đó");
define ( "kSuccesPanierCopie", "Document (s) dán thành công");
define ( "kSuccesPanierCreation", "Thư mục hoặc lựa chọn đã được tạo ra");
define ( "kSuccesPanierDeplacer", "Thư mục hoặc lựa chọn di chuyển thành công");
define ( "kSuccesPanierDocumentAjoute", "tài liệu được thêm vào giỏ hàng");
define ( "kSuccesPanierDocumentRetire", "tài liệu bị xóa khỏi giỏ hàng");
define ( "kSuccesPanierDocumentsAjoutes", "Document (s) thêm vào xe");
define ( "kSuccesPanierDupliquer", "Thư mục hoặc lựa chọn nhân đôi thành công");
define ( "kSuccesPanierDupliquerLignes", "Văn bản đã được nhân đôi");
define ( "kSuccesPanierSauve", "Thư mục đã lưu thành công");
define ( "kSuccesSelectionSauve", "Thư mục đã lưu thành công");
define ( "kSuccesPanierSupprimer", "Thư mục thành công đã xóa");
define ( "kSuccesPanierTransfert", "Document (s) tranfered thành công");
define ( "kSuccesPanierVider", "Document (s) đã xóa thành công");
define ( "kWarningDeletePanier", "Cảnh báo! này xóa là đứt và không thể được hoàn tác Tiếp tục?.");
define ( "kWarningPanierCommandeEnCours", "Bạn đã có một trật tự trong tiến bộ");

define ( "kNouveauPanier", "New giỏ hàng");
define ( "kNouvelleSelection", "Thư mục mới");
define ( "kSelectionSupprimer", "Xóa mục được chọn");
define ( "kSelectionnerToutPanier", "Chọn giỏ hàng đầy đủ");
define ( "kSelectionDeplacer", "Di chuyển mục đã chọn");
define ( "kSupprimerDocumentsSelectionnes", "Xóa mục được chọn");
define ( "kPanierRenommer", "Đổi tên một thư mục");
define ( "kDupliquerVers", "Sao");
define ( "kDans", "ở");
define ( "kMsgSelectionnerDossierDestination", "Hãy chọn một thư mục đích");
define ( "kMsgCliquerTelecharger", "Clic trên các liên kết để tải về");
define ( "kCommanderDocCoches", "đặt hàng các lựa chọn bài");
define ( "kViderSelection", "rỗng thư mục");
define ( "kViderPanier", "rỗng trong giỏ hàng");

define ( "kDocListeTousResultats", "Thêm vào giỏ hàng tất cả các mục trong");
define ( "kDocListeAjouterResultatsPage", "Thêm trang này của các bản ghi vào giỏ hàng");
define ( "kDocListeRetirerResultatsPage", "Hủy bỏ trang này của các mục khỏi giỏ hàng của");

define ( "kCopier", "Copy");
define ( "kColler", "Paste");
define ( "kMsgNoSelectLignes", "Không có gì được chọn!");

// COMMANDE
define ( "kCommandeCGV_OK", "Tôi đồng ý với <a href=\"#\" onclick=\"popupInfo('indexPopupHtml.php?html=3','main')\"> Tổng Điều kiện bán hàng </ a> ");
define ( "kCommandeRecap", "đặt hàng chi tiết / Báo giá");
define ( "kCommanderSupport", "Chất liệu để");
define ( "kMsgCommandeAchat", "Hãy điền vào mẫu đơn này tự do cho thấy loại hỗ trợ mà bạn muốn nhận được");
define ( "kMsgCommandeAchatRecap", "<strong> thứ tự của bạn cũng được đưa vào tài khoản </ strong>.");
define ( "kMsgCommandeTelecharger", "Việc xác nhận các mẫu đơn đặt hàng này sẽ cho phép bạn tải về các yếu tố trong định dạng MPEG-2 <br/>");
define ( "kMsgCommandeTelechargerRecap", "Bạn có thể tải từng phần tử của đơn đặt hàng, <strong> bằng cách bấm vào đĩa mềm </ strong> của dòng");
define ( "kContenuCommande", "đặt hàng nội dung");
define ( "kErreurCBHorsLimites", "Thanh toán bằng thẻ tín dụng chỉ được cho phép giữa".gAchatMontantMin."€ và". gAchatMontantMax. "€.");
define ( "kInscriptionPremiereCommande", "Hãy xác định cho mình để đặt hàng bằng cách điền các truy cập dưới đây hoặc đăng ký nếu đó là chuyến thăm đầu tiên của bạn");
define ( "kInfosCommande", "đặt hàng thông tin");
define ( "KNB", "Nb");
define ( "kPayerEnLigne", "Thanh toán trực tuyến");
define ( "kRedirectionCommande", "Bạn sẽ được chuyển hướng immediatly bước tiếp theo của các đơn hàng của bạn");
define ( "kTotalTVA", "thuế VAT");

// COMMANDES gestion
define ( "kRecherchePaniers", "Hàng tìm kiếm");
define ( "kGlobale", "toàn cầu");

// LIVRAISON
define ( "kTelechargementPanier", "Tải về giỏ hàng");
define ( "kTelecharger", "Download");
// DOUBLON define ( "kMsgPreparationCommande", "Hãy đợi trong khi tập tin của bạn được xử lý ...");
define ( "kErrorLivraisonNoMateriel", "Không có tài liệu để được cắt");
define ( "kErrorLivraisonEchecDecoupage", "Lỗi trong khi cắt phương tiện truyền thông");
define ( "kErrorLivraisonEchecCopie", "Lỗi trong khi copyin phương tiện truyền thông");
define ( "kMessageAideLivraison", "trật tự của bạn đang trong tiến trình");
define ( "kMsgPreparationCommande", "Trong khi chờ đợi trong khi các tập tin được chuẩn bị ...");
define ( "kClip", "Full clip");
define ( "KMO", "MB");
$msg = "video phát sóng của bạn đang được tải về. Bạn có thể giám sát quá trình này trong \" Tải \"cửa sổ của trình duyệt web của bạn. Nếu tải về không tự khởi động, hãy click chuột phải vào liên kết sau và chọn \" Save target như là ... \"<br/>";
$msg .= "%s <br/>";
$msg .= "tập tin này đã được mã hóa trong MPEG-2 lúc 8 Mb / s. Do đó, nó tương thích với hầu hết các bộ sửa: <br/>";
$msg .= "Với phần mềm Avid, bạn có thể sử dụng Sorenson Squeeze (http://www.sorensonmedia.com/pages/?pageID=2), một phần mềm đóng gói với các sản phẩm Avid, để chuyển mã tập tin này vào một định dạng mà tương thích với phần mềm của bạn, ví dụ như DV25 <br/> ";
$msg .= "<br/> Với Apple Final Cut Pro (FCP), trước tiên bạn phải demux file âm thanh và video, vì FCP không thể nhập khẩu multiplexed MPEG-2 files. Để làm như vậy, bạn có thể sử dụng MPEG Streamclip, một phần mềm miễn phí có tại http://www.squared5.com/svideo/mpeg-streamclip-mac.html. Sau đó bạn phải kéo và thả các tệp tin video trong cửa sổ Streamclip, chọn \"Fix timecode Breaks ... \" chỉ huy của các \"Edit \" trình đơn, và sau đó \"Demux để Unscaled M2V và AIFF ... \" của \"File \" trình đơn (chúng tôi khuyên bạn nên cung cấp cho cùng một tên cho đoạn video xuất khẩu và các file âm thanh). Bạn có thể bây giờ nhập khẩu các \"M2V \" video file trong FCP, sử dụng \"Import> Files ... \" lệnh của \"File \" menu. Nếu bạn đã cùng tên với file video và âm thanh khi xuất khẩu, video và âm thanh được nhập khẩu tại cùng một thời điểm trong FCP Nếu không, bạn phải nhập khẩu các tập tin âm thanh riêng rẽ.. ";
$msg .= "<br/> Trong trường hợp nào, bạn cũng có thể sử dụng phần mềm khác nhau trên máy Mac hoặc PC để chuyển mã tập tin này vào một định dạng tương thích với mục đích của bạn: <br/>";
$msg .= "- MPEG Streamclip, trên máy Mac hoặc PC (http://www.squared5.com/svideo/mpeg-streamclip-win.html): miễn phí, nhưng đòi hỏi QuickTime MPEG-2 thành phần đó không phải là Việt <br /> ";
$msg .= "- Super (http://www.erightsoft.com/SUPER.html), trên PC chỉ: Việt <br/>";
$msg .= "- Thương mại các sản phẩm: Sorenson Squeeze trên PC hoặc Mac, Tập Pro (http://www.telestream.net/products/episode_pro.htm) trên Mac ...";

define ( "kMsgLivraisonFinieV", $msg);

$msg = "Module âm thanh bạn đã chọn đang được tải về. Bạn có thể giám sát quá trình này trong \" Tải \"cửa sổ của trình duyệt web của bạn. Nếu tải về không tự khởi động, hãy click chuột phải vào liên kết sau và chọn \" Save target as ... \"<br/>";
$msg .= "% s <br/> tập tin này đã được mã hóa trong MP3 tại 256 kb / s.";
define ( "kMsgLivraisonFinieA", $msg);

define ( "kMsgCopieSucces", "Copie thực hiện thành công");
define ( "kMsgErrorCreerZip", "Lỗi tạo ZIP archive");

define("kMsgLivraisonFinie","Your broadcast video is being downloaded. You can monitor this process in the \"Downloads\" window of your Web browser. If download did not start automatically, please right-clic on the following link and choose \"Save target as...\"<br/>%s <br/><br/>");

// Hồ sơ
define ( "kAjouterDossierPublic", "Thêm vào thư mục:");
define ( "kCreerDossier", "↓ Tạo thư mục");
define ( "kDossierParent", "Parent thư mục");
define ( "kDossiers", "thư mục");
define ( "kDossiersThematiques", "tập Chuyên đề");
define ( "kModifierImage", "Thay đổi hình ảnh");
define ( "kGestionHomeEtDossiers", "Trang chủ và Chuyên đề Files");
define ( "kTitreAnglais", "Anh Tiêu đề");
define ( "kTitreFrancais", "Pháp Tiêu đề");

// ADMINISTRATION
define ( "kAdminEntitesAudiovisuelles", "nghe nhìn bài");
define ( "kAdminValeursThesaurus", "Từ điển, các giá trị, cá nhân");
define ( "kAdminUtilisateursDroits", "Cá nhân dùng và quyền");
define ( "kAdminCommandesStats", "đơn hàng và thống kê");
define ( "kAdminImportsDivers", "Nhập khẩu, linh tinh");
define ( "kAdminSysteme", "Hệ thống cảnh báo các tham số");
define ( "kAdminPagesStatiques", "Các trang tĩnh HTML");
define ( "kAdminMaintenanceFichierOrphelins", "Tập tin mà không có dữ liệu đầu vào");

// TRANG STATIQUES HTML
define ( "kListePages", "Danh sách các trang HTML tĩnh");
define ( "kPageDetail", "Xem chi tiết của trang");
define ( "kErrorHtmlExisteDeja", "Một trang web đã đề này");
define ( "kErrorHtmlSauve", "Một lỗi đã xảy ra trong khi tiết kiệm");
define ( "kErrorHtmlCreation", "Không thể tạo ra trang này");
define ( "kErrorHtmlSuppr", "Không thể xóa trang này");

// Story Board
define ( "kAjoutImagesParUpload", "Thêm hình ...");
define ( "kUploadImageSucces", "ảnh nhất");
define ( "kUploadImageEchec", "Error: hình ảnh không được gửi");
define ( "kErrorImageurCreation", "Lỗi khi tạo kịch bản");
define ( "kErrorImageurExisteDeja", "Một kịch bản đã tồn tại");
define ( "kErrorImageurCreationRepertoire", "Impossible để tạo thư mục");
define ( "kImageurIntervalleImage", "Thời gian trôi đi giữa chụp (giây)");
define ( "kImageurCreationEnCours", "Bảng phâm cảnh trong progess ...");
define ( "kImageurGenerationSucces", "Bảng phâm cảnh tự động tạo ra");
define ( "kImageurGenerationEchec", "Không tự động tạo ra kịch bản");
define ( "kErrorImageurNoSource", "Không có tập tin mã nguồn");
define ( "kErrorImageurNoTool", "Bảng phâm cảnh thiếu công cụ");
define ( "kErrorImageurNoImageur", "Không có cốt truyện");
define ( "kErrorImageurNoImages", "kịch bản rỗng");
define ( "kImageurSelectVignette", "Chọn như hình nhỏ");
define ( "kErrorImageurNoVideo", "định dạng tập tin này không thể xử lý khai thác hình ảnh");
define ( "kErrorImageExisteDeja", "hình ảnh này đã tồn tại");
define ( "kErrorImageSauve", "Những hình ảnh không thể lưu được");
define ( "kErrorVersionSauve", "Không thể lưu các phiên bản khác");
define ( "kErrorImageurCapture", "Không thể lấy hình ảnh");
define ( "kSuccesImageurSauve", "ảnh được lưu thành công");
define ( "kSuccesImageurCapture", "Picture grabed thành công");
define ( "kErrorImageurCaptureExisteDeja", "hình ảnh này đã tồn tại");
define ( "kErrorImageurCaptureNoPlayer", "Không chơi!");
define("kErrorJSImageurNoRate","Missing number of image per seconde !");
define("kImageurWaitGeneration","Imageur building...");

// VISIONNAGE
define ( "kErrorVisioCreationTempDir", "Lỗi khi tạo thư mục tạm thời");
define ( "kErrorVisioCommandeVide", "Dòng lệnh cho cắt bộ phim là sản phẩm nào");
define ( "kErrorVisioCopieMovie", "Các phương tiện truyền thông không thể được sao chép trong các thư mục tạm thời");
define ( "kErrorVisioFichierRefVide", "Tham khảo tên tập tin là mất tích");
define ( "kErrorVisioCopieFichierRef", "Tham khảo tập tin không thể được sao chép");
define ( "kErrorVisioNoMedia", "Điều này là không thể xem được phương tiện truyền thông");
define ( "kErrorVisioCreationXML", "Impossible để tạo ra các tệp tin XML");
define ( "kVisioNomDefautExtrait", "Không có tiêu đề");
define ( "kJSConfirmExit", "Mọi thay đổi sẽ bị mất!");

// ORAOWEB
define ( "kOraoWebSelectWholeMovie", "Chọn toàn bộ phim");
define ( "kOraoWebCreateNewExcerpt", "Thêm dòng trống");
define ( "kOraoWebSetCurrentTCIN", "Set TC Trong với vị trí hiện tại");
define ( "kOraoWebSetCurrentTCOUT", "Set TC ra với vị trí hiện tại");
define ( "kOraoWebSaveChanges", "Lưu thay đổi");
define ( "kOraoWebOpenPlayer", "Xem video");
define ( "kOraoWebSelectDocument", "Chọn để thêm một tài liệu");

// REQUETE / HISTO
define ( "kErrorRequeteSauve", "Điều này không thể tìm kiếm được lưu");
define ( "kErrorRequeteExisteDeja", "tìm kiếm này đã được lưu");
define("kMesRequetes","My queries");

// ADMIN: BẢO TRÌ fichiers
define ( "kConfirmJSFichierSuppression", "Bạn có xác nhận xóa tập tin");//???
define ( "kWarningMaintenanceFichierMessage", "Cảnh báo: không lấy lại được dữ liệu đã xóa");
define ( "kErrorMaintenanceFichierNoFile", "Tập tin mồ côi 'Không có' hàng");
define ( "kCacheVignette", "Thumbnail");
define ( "kGenererMateriel", "Tạo tài liệu và bảng phân cảnh");

// Nhập khẩu FCP
define ( "kImportFinalCutPro", "Chế biến");
define ( "kFichierAImporter", "File (s) để nhập khẩu");
define ( "kMedias", "Truyền thông (s)");

// PROCESSUS, ÉTAPES ET JOBS // form / jobSaisie.inc.php
define ( "kMonCompte", "tài khoản");
define ( "kMenu", "Menu");
define ( "kReference", "Tham khảo");
define ( "kFichiersMedias", "fichiers tiện");
define ( "kFichier", "File");
define ( "kDetail", "Xem chi tiết");
define ( "kTraitement", "Chế biến"); // ou chuyển đổi
define ( "kLancer", "Bắt đầu");
define ( "kListeTraitements", "Processings danh sách");
define ( "kSessionEnCours", "hiện tại phiên");
define ( "kProcessus", "Process");
define ( "kModule", "Module");
// DOUBLON define ( "kSortie", "Thoát");
define ( "kSuiviTraitements", "Processings theo thông qua");
define ( "kRelancer", "Khởi động lại");
define ( "kMailConfirmation", "Xác nhận email");
define ( "kDebit", "liệu tỷ lệ");
define ( "kDateDemande", "Ngày của yêu cầu");
define ( "kDateLivraison", "Ngày giao hàng");
define ( "kDestinataire", "Cá nhân nhận");
define ( "kFormatSortie", "Export to");
define ( "kDocumentsCrees", "sản xuất tập tin (s)");
define ( "kEncodageManuel", "Advance cài đặt");

define ( "kErrorProcessusExisteDeja", "Quá trình này đã tồn tại");
define ( "kErrorProcessusSauve", "Lỗi: quá trình này không chính xác lưu");
define ( "kErrorProcessusCreation", "Lỗi: quá trình khởi tạo không đúng");
define ( "kErrorProcessusNoID", "Lỗi: quá trình ID là không đúng");
define ( "kErrorProcessusSauveEtapes", "Lỗi: các bước xử lý không đúng lưu");
define ( "kErrorProcessusSuppr", "Lỗi: không chính xác đã xóa");

define ( "kErrorEtapeExisteDeja", "Bước này đã tồn tại");
define ( "kErrorEtapeSauve", "Lỗi: bước này không chính xác lưu");
define ( "kErrorEtapeCreation", "Lỗi: các bước đã không được chính xác được khởi tạo");
define ( "kErrorEtapeSuppr", "Lỗi: bước không chính xác đã xóa");

define ( "kErrorJobExisteDeja", "chế biến này đã tồn tại");
define ( "kErrorJobSauve", "Lỗi: chế biến này không chính xác lưu");
define ( "kErrorJobCreation", "Lỗi: việc xử lý không đúng được khởi tạo");
define ( "kErrorJobSuppr", "Lỗi: chế biến này không chính xác đã xóa");

define ( "kErrorEnvoiMailValidation", "Lỗi khi tạo thư xác nhận");

define ( "kMailFinTraitement", "New video");
define ( "kMailValidation", "mới yêu cầu:"); // TITRE du mail envoyé suite à une demande de xác nhận
define ( "kValidationEtape", "Bước xác nhận");
define ( "kMsgValidationJob", "bước <b>% s </ b> được xác nhận");
define ( "kMsgValidationJobRefusee", "Bước <b>% s </ b> là invalidée");
define ( "kMsgConfirmerValidation", "Bạn có muốn validat bước này?");
define ( "kErreurJobNonValide", "Xác nhận từ chối");
define ( "kRefuser", "rác");

// ALERTE MAIL
define ( "kAlerteMailSujet", "mới cho kết quả tìm kiếm ưa thích của bạn vào:");
define ( "kAlerteMailEntete", "Xin chào, \n
một số tài liệu mới về ".kCheminHttp." tương ứng với các tìm kiếm của bạn được lưu \n. ");
define ( "kAlerteMailDisclaimer", "\n \nAutomatically tạo thư \nYou. nhận được email này bởi vì bạn đã đăng ký trên". kCheminHttp. "\n
và bạn đã chọn để được giữ thông qua thư của các kết quả tìm kiếm mới. \n \n ");

// SỬA ĐỔI cải cách hành chính LOTS
define ( "kModifierLot", "Batch quá trình");
define ( "kInitialiser", "khởi");
define ( "kEffacer", "Xóa");
define ( "kRechercherRemplacer", "Tìm kiếm / thay thế");
define ( "kModifierNotice", "Sửa đổi, ghi âm bằng ghi");
define ( "kAppliquer", "Apply");
define ( "kWaiterMessage", "Hãy đợi trong khi chế biến được thực hiện ...");
define ( "kPrecedent", "trước");
define ( "kSuivant", "Next");
define ( "kAbandonnerChangements", "Abort sửa đổi");//???
define ( "kErrorModifLotResultsetChanged", "Kết quả đã thay đổi Modifications hủy bỏ..");
define ( "kErrorModifLotNoResults", "Không có kết quả tìm kiếm có sẵn. <br/>
Vui lòng bắt đầu một tìm kiếm trước khi sử dụng màn hình này. <br/>
<a href='javascript:window.close();'> Close </ a> ");
define ( "kErrorModifLotErreurXML", "Lỗi trong cài đặt tập tin");
define ( "kSuccessModifLot", "(s) cập nhật thành công");

define ( "kInitValComment", "khởi một lĩnh vực có giá trị một");
define ( "kInitValHint", "Field initialisation");
define ( "kInitValFieldLabel", "Trường dự kiến sẽ được khởi tạo");
define ( "kInitValValueLabel", "Giá trị");

define ( "kCleanValComment", "rỗng trường");
define ( "kCleanValHint", "Đặt lại các lĩnh vực");
define ( "kCleanValFieldLabel", "Field để được làm sạch");

define ( "kCopyValComment", "Sao chép nội dung của một trong cùng một lĩnh vực khác");
define ( "kCopyValHint", "Trường nhân đôi");
define ( "kCopyValFieldLabel", "Nguồn lĩnh vực");
define ( "kCopyValField2Label", "Điểm đến lĩnh vực");

define ( "kReplaceValComment", "Tìm kiếm một chuỗi và thay thế nó");
define ( "kReplaceValHint", "Tìm kiếm / Thay thế trong một trường");
define ( "kReplaceValFieldLabel", "tìm trường");
define ( "kReplaceValValueLabel", "Tìm kiếm");
define ( "kReplaceValValue2Label", "Thay bằng:");

define ( "kEditValComment", "bản trong các kết quả");
define ( "kEditValHint", "Giá trị bản");
define ( "kEditValFieldLabel", "Trường để chỉnh sửa");
define ( "kEditValValueLabel", "Giá trị");

define ( "kSupprValComment", "xóa cuối cùng của các kết quả");
define ( "kSupprValHint", "Kết quả xóa");

define ( "kTester", "Test");
define ( "kWindows", "Windows");
define ( "kMac", "Mac");

// Backup
define ( "kCapacite", "năng lực");
define ( "kCapaciteUtilisee", "sử dụng năng lực");
define ( "kCapaciteRestante", "Cá nhân năng lực");
define ( "kCartouche", "băng");
define ( "kCartouches", "Băng");
define ( "kDateFichier", "File sửa đổi ngày");
define ( "kDateSauvegarde", "Sao lưu ngày");
define ( "kDebutTraitement", "Bắt đầu của quá trình");
define ( "kErrorBackupJobActionVide", "Lỗi: không có hành động");
define ( "kErrorBackupJobJeuVide", "Erreur: không đặt");
define ( "kErrorFichierDejaSauve", "tập tin đã lưu");
define ( "kErrorNoDevice", "Không có thiết bị");
define ( "kErrorSauveTapeFile", "Lỗi khi xóa các liên kết băng / file");
define ( "kErrorTapeNoId", "Không nhận dạng băng");
define ( "kErrorTapeExisteDeja", "tham chiếu băng này đã tồn tại");
define ( "kErrorTapeInfoFile", "Thông tin không tìm thấy tập tin");
define ( "kErrorTapeSauve", "Một lỗi đã xảy ra khi tiết kiệm băng này");
define ( "kErrorTapeCreation", "Không thể tạo này băng");
define ( "kErrorTapeSuppr", "Không thể xoá băng");
define ( "kErrorTimeout", "Timeout error");
define ( "kErrorVariableVide", "Lỗi trống tham số:");
// DOUBLON define ( "kFichier", "tập tin");
define ( "kFichierDejaSauve", "File đã được lưu trên băng");
define ( "kFinTraitement", "cuối cùng của quá trình");
define ( "kGestionSauvegardes", "Sao lưu quản lý");
define ( "kIdentifiantCartouche", "Băng nhận dạng");
define ( "kJeu", "Set");
define ( "kListeFichiers", "Danh sách các tập tin");
define ( "kListageCartouches", "danh mục băng");
define ( "kMsgFichierASauver", "%s file (s) để sao lưu (%s)");
define ( "kNbFichiers", "Số tập tin");
define ( "kNomFichier", "File name");
define ( "kOffline", "Off-line");
// DOUBLON define ( "kOnline", "on-line");
define ( "kRechercheCartouche", "Băng tìm kiếm");
define ( "kRechercheFichierASauver", "Tìm kiếm các tập tin sao lưu");
define ( "kRestaurer", "Khôi phục");
define ( "kRienASauvegarder", "Không có gì để sao lưu");
define ( "kSauvegardes", "Sao lưu");
define ( "kSuccesTapeCreation", "băng tạo thành công");
define ( "kSuccesTapeSauve", "Băng đã lưu thành công");
define ( "kTaille", "Kích thước");
define ( "kTailleFichier", "Kích thước");

// Xuất nhập khẩu (copie de fichiers)
define ( "kErrorExportCopieMediaXML", "Impossible để trích xuất các danh sách phương tiện truyền thông từ XML");
define ( "kErrorExportDirNonDefinie", "Không xác định xuất khẩu thư mục");
define ( "kErrorExportImpossibleEcrireFichier", "Impossible để viết các tập tin trong thư mục xuất khẩu");
define ( "kExportDocAccCopie", "jointed văn bản (s) sao chép");
define ( "kExportImageurCopie", "kịch bản (s) sao chép");
define ( "kExportImageCopie", "hình ảnh (s) sao chép");
define ( "kExportMaterielCopie", "video (s) sao chép");

// Recherche
define ( "kSauverRecherche", "Lưu tìm kiếm");
define ( "kAlerte", "Alert");

// Dịch vụ web
define ( "kErrorWSRequeteNonValide", "không hợp lệ yêu cầu ");// 003
define ( "kErrorWSXSLManquant", "XSL stylesheet thiếu ");// 004
define ( "kErrorWSEntiteNonTrouvee", "thực thể này không được hỗ trợ ");// 005
define ( "kErrorWSParametreManquant", "Một trong những tham số là ít nhất là thiếu"); // 006
define ( "kErrorWSSyntaxeParametre", "Tham số cú pháp không phải là quyền :");// 007
define ( "kErrorWSFichierNonTrouve", "Không tìm thấy tệp ");// 008
define ( "kErrorWSExtensionNonAutorisee", "File không được phép mở rộng ");// 009
define ( "kErrorWSResultatManquant", "Không có kết quả"); // 010
define ( "kErrorWSExtensionNonSupportee", "File không được hỗ trợ phần mở rộng"); // 011
// kAccesLogin = 012, kErreurPageNotFound = 014

// Gestion ảnh
define ( "kPhotoUpload", "Upload ảnh");
define ( "kExtensionImagickNotLoaded", "Impossible để tải ImageMagick");
define ( "kFichierImagickNotFound", "Impossible để tìm ImageMagick");
define ( "kErrorImageurNoPhoto", "phương tiện truyền thông này không phải là một hình ảnh");
define ( "kGenererVignette", "Tạo hình thu nhỏ");
define ( "kDiaporama", "Diaporama");
define ( "kErrorDiaporamaExtraction", "Impossible để cài đặt các diaporama");
define ( "kErrorDiaporamaNotFound", "Diaporama không tìm thấy");

// Gestion et des etapes quá trình
define ( "kGestionProcessus", "Quy trình biên tập");
define ( "kGestionEtapes", "Các bước chỉnh sửa");
define ( "kNouvelleEtape", "Tạo bước mới");
define ( "kNouveauProcessus", "Tạo quá trình mới");
define ( "kEtapePrecedente", "Trước bước");
define ( "kProcessusAjouterEtape", "Thêm bước");
define ( "kEtapeChampManquant", "Cảnh báo, một số các trường là mất tích trong đơn");
define ( "kSuccesEtapeDupliquer", "Bước nhân đôi, hãy chọn một tên mới");
define ( "kSuccesProcessusDupliquer", "Quá trình nhân đôi, hãy chọn một tên mới");

// Paramètres des étapes
define ( "kEntree", "đầu vào");
define ( "kSortie", "Ouput");
define ( "kCodecVideo", "Video codec");
define ( "kDebitVideo", "Video bitrate");
define ( "kAspect", "Aspect");
define ( "kLargeur", "Rộng");
define ( "kHauteur", "Chiều cao");
define ( "kRatioLargeur", "Chiều rộng tỷ lệ");
define ( "kRatioHauteur", "Chiều cao tỷ lệ");
define ( "kCropHaut", "đầu mùa");
define ( "kCropBas", "Dưới cây");
define ( "kCropGauche", "lại cây trồng");
define ( "kCropDroit", "Phải thu hoạch");
define ( "kImagesParSeconde", "Hình ảnh / giây");
define ( "kCodecAudio", "Audio codec");
define ( "kNbCanaux", "Số kênh");
define ( "kDebitAudio", "Audio bitrate");
define ( "kFrequenceEchantillonage", "Mẫu tỷ lệ");
define ( "kTCIncruste", "Chèn TC trên video");//???
define ( "kLogo", "Logo file");
define ( "kNumerisation", "Digitisation");
define ( "kIntervalle", "Interval");
define ( "kDetectionPlan", "Phát hiện những thay đổi bắn");//???
define ( "kDossier", "Danh bạ");
define ( "kServeur", "Server");
define ( "kFTP", "FTP");
define ( "kFTPImage", "FTP Hình ảnh");
define ( "kTransfertHTTP", "HTTP transfert / Kewego");
?>
