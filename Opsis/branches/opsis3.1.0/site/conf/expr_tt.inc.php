<?
define("kAcces", "Asesu");
define("kAncienneCote", "Númeru Tuan");
define("kCopyright", "Direitus Autór nian");
define("kDescripteurs","Deskritór sira");
define("kDates", "Data sira");
define("kDateCreation", "Data Halo");
define("kDateModification", "Data Muda");
define("kDuree","Durasaun");
define("kFonds", "Fundu");
define("kGenre","Dokumentu nia Jéneru");
define("kGestion", "Dadus Jestaun nian");
define("kIndexation", "Indeksasaun");
define("kLangue","Lian");
define("kLangues", "Lian sira");
define("kLocalisation", "Fatin");
define("kNotes","Nota sira");
define("kNotesTitre", "Títulu nia Nota");
define("kProducteur", "Produtór");
define("kSequences","Sekuénsia sira");
define("kRealisateur", "Autór");
define("kResume","Rezumu konteúdu");
define("kSource", "Orijen");
define("kTextes", "Testu sira");
define("kTitre","Titulu");
define("kTitreCol", "Títulu Kolesaun nian");
define("kTheme", "Tópiku");
define("kTypeDocument", "Dokumentu nia Tipu");
define("kUsagerCrea", "Halo husi");
define("kUsagerModif", "Edisaun husi");

include_once('expr_pt.inc.php');

?>
