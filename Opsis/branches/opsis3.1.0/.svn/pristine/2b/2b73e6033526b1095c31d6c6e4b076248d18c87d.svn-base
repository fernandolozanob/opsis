<?php
require_once(modelDir."/model.php");

class Categorie extends Model
{
	public $t_categorie;
	public $user_crea;
	public $user_modif;
	private $champs_editables=array('ID_LANG','CAT_ID_GEN','CAT_ID_TYPE_CAT','CAT_ID_DOC_ACC','CAT_NOM','CAT_DESC','CAT_CODE','TYPE_CAT','CAT_IMAGE', 'CAT_INTRO', 'CAT_DATE_DEBUT', 'CAT_DATE_FIN','CAT_ID_DOC_IMAGE');
	private $champ_base=array('ID_CAT','ID_LANG','CAT_ID_GEN','CAT_ID_TYPE_CAT','CAT_ID_DOC_ACC','CAT_NOM','CAT_DESC','CAT_CODE','CAT_ID_USAGER_CREA','CAT_ID_USAGER_MOD','CAT_DATE_CREA','CAT_DATE_MOD', 'CAT_INTRO','CAT_ID_DOC_IMAGE');
	public $vignette;
	public $arrDoc;
	public $arrVersions;
	public $error_msg;
	var $arrFather= array();
	var $arrChildren= array();	
	
	function __construct($id_cat='',$id_lang='')
	{
		parent::__construct("t_categorie",$id_cat, $id_lang);
	}
	
	function getData($arrData=array(),$version="") {
		global $db;
		$arrCats=parent::getData($arrData,$version); 
		
		if (empty($arrCats) && !empty($this->t_categorie) && !$arrData) { 
			$this->t_categorie['TYPE_CAT']=GetRefValue("t_type_cat",$this->t_categorie["CAT_ID_TYPE_CAT"],$this->t_categorie['ID_LANG']);
			if (!empty($this->t_categorie['CAT_ID_GEN'])) {
					$this->arrFather=$db->GetAll("SELECT * FROM t_categorie WHERE ID_CAT=".intval($this->t_categorie['CAT_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_categorie['ID_LANG']));
					$this->t_categorie['CAT_GEN'] = $this->arrFather[0]['CAT_NOM'];
			}
			//recuperation de l'usager crea & de l'usager mod (en une seule requete) 
			$ids_user = array();
			if(isset($this->t_categorie['CAT_ID_USAGER_CREA']) && !empty($this->t_categorie['CAT_ID_USAGER_CREA'])){
				$ids_user[] = intval($this->t_categorie['CAT_ID_USAGER_CREA']);
			}
			if(isset($this->t_categorie['CAT_ID_USAGER_MOD']) && !empty($this->t_categorie['CAT_ID_USAGER_MOD'])){
				$ids_user[] = intval($this->t_categorie['CAT_ID_USAGER_MOD']);
			}
			if(!empty($ids_user)){
				$data=$db->Execute('SELECT ID_USAGER,'.$db->Concat('US_NOM', "' '", 'US_PRENOM').' AS USER FROM t_usager WHERE ID_USAGER in ('.implode(',',$ids_user).')')->GetRows();
			}
			if(!empty($data)){
				foreach($data as $idx=>$row){
					if($row['ID_USAGER'] == $this->t_categorie['CAT_ID_USAGER_CREA']){
						$this->user_crea=$data[$idx]['USER'];
					}else if($row['ID_USAGER'] == $this->t_categorie['CAT_ID_USAGER_MOD']){
						$this->user_modif=$data[$idx]['USER'];
					}
				}
			}
			$this->getVignette();
		}else{
			foreach ($arrCats as $idx=>$categorie){ // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrCats[$idx]->t_categorie['TYPE_CAT']=GetRefValue("t_type_cat",$arrCats[$idx]->t_categorie["CAT_ID_TYPE_CAT"],$arrCats[$idx]->t_categorie['ID_LANG']);
			}
			unset($this); // plus besoin
			return $arrCats;
		}
	}
	
	function getCategorie($prm_id_cat=null,$id_lang='')
	{
		return $this->getData($prm_id_cat,$id_lang);
	}

    /** Récupère les libellés pour toutes les langues de l'appli.
    *  IN : rien (objet)
    * 	OUT : tableau de classe arrVersions
    *  NOTE : si la valeur existe déjà en base, on ramènes ttes les langues même si le lib n'existe pas encore (right join)
    * 		   si la valeur n'existe pas (création), on ramène toutes les langues.
    */
    function getVersions () {
        global $db;
        $tabLg=$_SESSION['tab_langue'];
        unset($tabLg[$_SESSION['langue']]); // retrait de la langue en cours.
        if (!empty($this->t_categorie['ID_CAT'])) {
            $sql="SELECT t_lang.ID_LANG,LANG,CAT_CODE,CAT_NOM,CAT_DESC, CAT_INTRO,ID_CAT FROM t_categorie right join t_lang on (t_lang.ID_LANG=t_categorie.ID_LANG) WHERE t_categorie.ID_LANG<>".$db->Quote($this->t_categorie['ID_LANG'])." AND ID_CAT=".intval($this->t_categorie['ID_CAT']);
            $this->arrVersions=$db->GetAll($sql);
        }
        /*
		// MS - code créant des tas de conflits. laissé pour l'instant au cas ou mais à supprimer si pas de grosses regressions trouvées ...: 
		if (empty($this->arrVersions)) {
            $sql="SELECT t_lang.ID_LANG,LANG,'' as CAT_CODE,'' as CAT_DESC,'' as CAT_INTRO,'' as CAT_NOM, null as ID_CAT FROM t_lang WHERE ID_LANG<>".$db->Quote($this->t_categorie['ID_LANG']);
            $this->arrVersions=$db->GetAll($sql);
        }*/
    }

    function getCats()
	{
		global $db;
		$data=$db->Execute('SELECT t_categorie.*,t_type_cat.TYPE_CAT
								FROM t_categorie 
									LEFT OUTER JOIN t_type_cat ON t_categorie.CAT_ID_TYPE_CAT=t_type_cat.ID_TYPE_CAT 
									')->GetRows();
		foreach($data as $cat)
		{
			$doc_acc=$db->Execute('SELECT ID_DOC_ACC,DA_FICHIER,DA_CHEMIN 
			FROM t_doc_acc 
			WHERE ID_CAT='.intval($cat['ID_CAT']).' 
			ORDER BY ID_DOC_ACC DESC 
			LIMIT 1')->GetRows();
			// Ajout test pour éviter des erreurs "notice", mais je ne suis pas sur que cette fonction accomplisse correctement son role, quid si plusieurs docaccs, pourquoi prend on le premier ? 
			if(!empty($doc_acc) && isset($doc_acc[0])){
				$cat['CAT_IMAGE']=$doc_acc[0]['DA_CHEMIN'].$doc_acc[0]['DA_FICHIER'];
			}
		}
		
		return $data;
	}
	
	function set($var,$val)
	{
		if (in_array($var,$this->champs_editables))
		{
			$this->t_categorie[$var]=$val;
			return true;
		}
		return false;
	}
	
	function getValues()
	{
		return $this->t_categorie;
	}
	
	function getVignette()
	{
		global $db;
		// récupération path doc_acc depuis CAT_ID_DOC_ACC
		if(!empty($this->t_categorie['CAT_ID_DOC_ACC']) && empty($this->t_categorie['CAT_IMAGE'])){
			require_once(modelDir.'model_docAcc.php');
			$docAcc = new DocAcc() ; 
			$docAcc->t_doc_acc['ID_DOC_ACC'] = $this->t_categorie['CAT_ID_DOC_ACC'];
			$docAcc->getDocAcc();
			$this->t_categorie['CAT_IMAGE'] = $docAcc->getFileUrl(true);
		}
		// récupération path depuis vignette objet doc dans CAT_ID_DOC_IMAGE
		if(!empty($this->t_categorie['CAT_ID_DOC_IMAGE']) && empty($this->t_categorie['CAT_IMAGE'])){
			if(empty($this->t_doc_cat)){
				$this->getDocCat(false);
			}
			if(!empty($this->t_doc_cat)){
				foreach($this->t_doc_cat as $doc_cat){
					if($doc_cat['ID_DOC'] == $this->t_categorie['CAT_ID_DOC_IMAGE']){
						require_once(modelDir.'model_doc.php');
						$doc = new Doc();
						$doc->t_doc['ID_DOC']  = $this->t_categorie['CAT_ID_DOC_IMAGE'];
						$doc->getDoc(); 
						$this->t_categorie['CAT_IMAGE'] = $doc->vignette;
					}
				}
			}
		}
	
		$this->vignette=$this->t_categorie['CAT_IMAGE'];
		return $this->vignette;
	}
	
	
	function setVignette(){
		global $db;
		
		$changed = false ; 
		
		// cas de suppression de toutes les images  (est ce necessaire?? );
		// actuellement confus car il semble que l'ancienne méthode de supprimer les images consistait à passer CAT_IMAGE = '0', mais l'input file dans catsaisie génère en permanence des sous variables dans CAT_IMAGE
		// à voir si on conserve comme ça 
		// if (empty($this->t_categorie['CAT_IMAGE']))
			// {
				// $db->Execute('DELETE FROM t_doc_acc WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']));
			// }
	
		require_once(modelDir.'model_docAcc.php');
		if(is_array($this->t_categorie['CAT_IMAGE']) && !empty($this->t_categorie['CAT_IMAGE']["name"])){ // CAS MISE A JOUR DOC_ACC
			$id_vignette=$db->Execute('SELECT ID_DOC_ACC FROM t_doc_acc WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']).' ORDER BY ID_DOC_ACC DESC LIMIT 1')->GetRows();
			$id_vignette=$id_vignette[0]['ID_DOC_ACC'];
			$db->Execute('DELETE FROM t_doc_acc WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']).' AND ID_DOC_ACC!='.intval($id_vignette));
			if (!empty($this->t_categorie['CAT_IMAGE']["name"])) {
				$myDA= new DocAcc;
				$myDA->t_doc_acc['ID_CAT']= intval($this->t_categorie['ID_CAT']);
				
				move_uploaded_file($this->t_categorie['CAT_IMAGE']["tmp_name"], kDocumentDir.getFileFromPath($this->t_categorie['CAT_IMAGE']["name"]));
				$myDA->t_doc_acc['DA_FICHIER']=getFileFromPath($this->t_categorie['CAT_IMAGE']["name"]);
				if ($myDA->checkFileExists()) {
					$myDA->t_doc_acc['DA_TITRE']=$this->t_categorie['CAT_IMAGE']["name"];
					foreach ($_SESSION['arrLangues'] as $lg) {
						$myDA->t_doc_acc['ID_LANG']=$lg;
						$ok=$myDA->create(false);
						$this->t_categorie['CAT_IMAGE']=$myDA->t_doc_acc['DA_CHEMIN'].$myDA->t_doc_acc['DA_FICHIER'];
						$this->t_categorie['CAT_ID_DOC_ACC'] =  $myDA->t_doc_acc['ID_DOC_ACC']; 
						$changed  = true ; 
					}
				}
			}
		}else{
			// on vérifie l'existence du docAcc, si le fichier n'existe plus on supprime la mention du docAcc
			if(!empty($this->t_categorie['CAT_ID_DOC_ACC'])){
				require_once(modelDir.'model_docAcc.php');
				$docAcc = new DocAcc();
				$docAcc->t_doc_acc['ID_DOC_ACC'] = $this->t_categorie['CAT_ID_DOC_ACC'];
				$docAcc->t_doc_acc['ID_LANG'] = $this->t_categorie['ID_LANG'];
				$docAcc->getDocAcc();
				if(!$docAcc->checkFileExists()){
					$this->t_categorie['CAT_ID_DOC_ACC'] = 0 ; 
					$docAcc->delete();
					unset($docAcc);
					$changed  = true ; 
				}
			}
			
			// on vérifie que l'id_doc renseigné dans cat_id_doc_image appartient toujours bien à la catégorie en parcourant t_doc_cat
			if(!empty($this->t_categorie['CAT_ID_DOC_IMAGE'])){
				if(!isset($this->t_doc_cat) ||  empty($this->t_doc_cat)){
					$this->getDocCat(false);
				}
				$found = false ;
				if(!empty($this->t_doc_cat)){
					foreach($this->t_doc_cat as $doc_cat){
						if($doc_cat['ID_DOC'] == $this->t_categorie['CAT_ID_DOC_IMAGE']){
							$found = true ; 
							break ;
						}
					}
				}
				// si on ne trouve pas l'id_doc parmis les t_doc_cat, on remet cat_id_doc_image à 0
				if(!$found){
					$this->t_categorie['CAT_ID_DOC_IMAGE'] = 0 ; 
					$changed  = true ; 
				}
			}
			
			// utilisation automatique de l'image du premier document comme image de la catégorie
			if(empty($this->t_categorie['CAT_ID_DOC_ACC']) && empty($this->t_categorie['CAT_ID_DOC_IMAGE'])){
				if(!isset($this->t_doc_cat) ||  empty($this->t_doc_cat)){
					$this->getDocCat(false);
				}
				if(!empty($this->t_doc_cat)){
					$this->t_categorie['CAT_ID_DOC_IMAGE'] = $this->t_doc_cat[0]['ID_DOC'];
					$changed  = true ; 
				}
			}
		}
		$this->getVignette();
		return $changed ; 
	}
	
	
	function saveDocCat($doc_cat,$ordre=null){
		global $db;
		
		if(!isset($doc_cat) || empty($doc_cat)){
			return false;  
		}
		
		if(!is_array($doc_cat) && is_numeric($doc_cat)){
			$doc_cat = array('ID_DOC'=>$doc_cat);
		}
		if(isset($this->t_categorie['ID_CAT'])){
			$doc_cat['ID_CAT'] = $this->t_categorie['ID_CAT'];
		}
		if ($ordre!==null && (!isset($doc_cat['CDOC_ORDRE']) || $doc_cat['CDOC_ORDRE'] === false)) {
			$doc_cat["CDOC_ORDRE"]= $ordre;
		}
		if (!isset($doc_cat['CAT_ID_TYPE_CAT']) || empty($doc_cat['CAT_ID_TYPE_CAT'])){
			$doc_cat["ID_TYPE_CAT"]= $this->t_categorie['CAT_ID_TYPE_CAT'];
		}
		if(!isset($doc_cat['ID_DOC']) || empty($doc_cat['ID_DOC']) || !isset($doc_cat['ID_CAT']) || empty($doc_cat['ID_CAT'])){
			trace("saveDocCat - enregistrement annulé : manque ID_DOC ou ID_CAT : ".print_r($doc_cat,true));
			return false;  
		}
		
		$db->Execute('DELETE FROM t_doc_cat WHERE ID_DOC='.intval($doc_cat['ID_DOC'])
		." AND ID_CAT=".intval($doc_cat['ID_CAT']));
		//." AND ID_TYPE_CAT=".$db->Quote($doc_cat['ID_TYPE_CAT']));
		
		$ok = $db->insertBase("t_doc_cat","id_doc",$doc_cat);		
		return $ok ; 
	}
	
	function saveAllDocCat(){
		if(empty($this->t_doc_cat)){
			$this->getDocCat() ; 
		}
		
		foreach($this->t_doc_cat as $idx=>$doc_cat){
			$this->saveDocCat($doc_cat, $idx) ; 
		}
	}
	
	function updateDocCats($tab_doc_cat,$replace=false){
		if(empty($this->t_doc_cat)){
			$this->getDocCat() ; 
		}
		if($replace == true ){
			$this->t_doc_cat = $tab_doc_cat ; 
		}else{
			foreach($this->t_doc_cat as $idx=>$doc_cat){
				foreach($tab_doc_cat as $doc_cat_input){
					if($doc_cat['ID_DOC'] === $doc_cat_input['ID_DOC']){
						$this->t_doc_cat[$idx] = array_merge ($doc_cat,$doc_cat_input);
					}
				}
			}
		}
	}
	
	function addDoc($id_doc)
	{
		global $db;
		
		if (!is_array($id_doc))
		{
			$tmp_id_doc=$id_doc;
			$id_doc=array($tmp_id_doc);
		}
		
		for($i=0;$i<count($id_doc);$i++)
			$id_doc[$i]=intval($id_doc[$i]);
			
		foreach ($id_doc as $id)
		{
			$result=$db->Execute('SELECT COUNT(*) AS compte FROM t_doc_cat WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']).' AND ID_DOC='.$id)->GetRows();
			
			if ($result[0]['COMPTE']==0)
			{
				/*ajout d'une ligne dans t_doc_cat*/
				$db->Execute('INSERT INTO t_doc_cat(ID_DOC,ID_CAT) VALUES ('.intval($id).','.intval($this->t_categorie['ID_CAT']).')');
			}
		}
		
		$db->Execute('UPDATE t_categorie SET CAT_DATE_MOD=NOW() WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']));
	}
	
	function removeDoc($id_doc)
	{
		global $db;
		if (!is_array($id_doc))
		{
			$tmp_id_doc=$id_doc;
			$id_doc=array($tmp_id_doc);
		}
		
		for($i=0;$i<count($id_doc);$i++)
			$id_doc[$i]=intval($id_doc[$i]);
	
		foreach ($id_doc as $id)
		{
			$result=$db->Execute('DELETE FROM t_doc_cat WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']).' AND ID_DOC='.intval($id));
		}
		$db->Execute('UPDATE t_categorie SET CAT_DATE_MOD=NOW() WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']));
	}
	
	function removeAllDocs()
	{
		global $db;
		
		$result=$db->Execute('DELETE FROM t_doc_cat WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']));
		
		$db->Execute('UPDATE t_categorie SET CAT_DATE_MOD=NOW() WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']));
	}
	
	function getDocs()
	{
		global $db;
		
		if (isset($this->t_categorie['ID_CAT']) && !empty($this->t_categorie['ID_CAT']))
		{
			$resultat_docs=$db->Execute('SELECT ID_DOC FROM t_doc_cat WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']) . " ORDER BY CDOC_ORDRE,ID_DOC ASC")->GetRows();
			
			$arr_docs_cat=array();
			
			require_once(modelDir.'model_doc.php');
			
			foreach ($resultat_docs as $doc_cat)
			{
				$doc=new Doc();
				$doc->t_doc['ID_DOC']=$doc_cat['ID_DOC'];
				$doc->getDoc();
				
				$arr_docs_cat[]=$doc;
			}
			return $arr_docs_cat;
		}
		
		return null;
	}
	
	function getDocCat($getDocFull = true)
	{
		global $db;
		
		if (isset($this->t_categorie['ID_CAT']) && !empty($this->t_categorie['ID_CAT']))
		{
			// MS - reprise des moyens de tris utilisés dans getPanierDoc pour PDOC_ORDRE
			if (!empty($this->sqlOrder))  $sql.=$this->sqlOrder; 
			elseif (defined("gDocTriCategorie") && (gDocTriCategorie))  $this->sqlOrder.=gDocTriCategorie;
			else $this->sqlOrder.=" ORDER BY CDOC_ORDRE,ID_DOC ASC"; 
		
			$resultat_docs=$db->Execute('SELECT * FROM t_doc_cat WHERE ID_CAT='.intval($this->t_categorie['ID_CAT'])." ".$this->sqlOrder)->GetRows();
			
			$this->t_doc_cat=array();
			
			require_once(modelDir.'model_doc.php');
			foreach ($resultat_docs as $doc_cat){
				$arr_doc_cat = $doc_cat;
				if($getDocFull){
					$doc=new Doc();
					$doc->t_doc['ID_DOC']=$doc_cat['ID_DOC'];
					$doc->getDoc();
					if(!empty($doc->vignette)){
						$doc->t_doc['VIGNETTE'] = $doc->vignette;
					}
					$arr_doc_cat = array_merge($arr_doc_cat ,$doc->t_doc);
					$arr_doc_cat['DOC'] = $doc ; 
				}
				$this->t_doc_cat[] = $arr_doc_cat;
			}
			return $this->t_doc_cat;
		}
		
		return null;
	}
        
    function getAccessiblesDocs()
	{
		global $db;
		
		if (isset($this->t_categorie['ID_CAT']) && !empty($this->t_categorie['ID_CAT']))
		{
			$resultat_docs=$db->Execute('SELECT dc.ID_DOC FROM t_doc_cat dc
											INNER JOIN t_doc d ON dc.ID_DOC = d.ID_DOC AND d.ID_LANG = '.$db->quote($_SESSION['langue']).'
											WHERE dc.ID_CAT='.intval($this->t_categorie['ID_CAT']))->GetRows();
			
			$arr_docs_cat=array();
			
			require_once(modelDir.'model_doc.php');
			
			foreach ($resultat_docs as $doc_cat)
			{
				$doc=new Doc();
				$doc->t_doc['ID_DOC']=$doc_cat['ID_DOC'];
				$doc->getDoc();
				if ($doc->checkAccess(1)){
					$arr_docs_cat[]=$doc;
				}
			}
			
			return $arr_docs_cat;
		}
		
		return null;
	}
    
	 function create()
	{
		global $db;
		
		$valeurs='';
		$vars='';
		$first=true;
		
		 if ($this->checkExist()) {$this->dropError(kErrorCategorieExisteDeja);return false;}
		
		$this->t_categorie['CAT_ID_USAGER_CREA']=intval($_SESSION['USER']['ID_USAGER']);
		$this->t_categorie['CAT_ID_USAGER_MOD']=intval($_SESSION['USER']['ID_USAGER']);
		
		$this->t_categorie['CAT_DATE_MOD']=date('Y-m-d H:i:s');
		$this->t_categorie['CAT_DATE_CREA']=date('Y-m-d H:i:s');
		
		// MS 12/02/2016 : mise à jour cat_path
		// MS 29.01.18 - toujours appelé.
        $this->setCatPath();
       
		$ok = $db->insertBase("t_categorie","id_cat",$this->t_categorie);
		if(empty($this->t_categorie['ID_CAT'])){
			$this->t_categorie['ID_CAT']=$ok;
		}
		
		return $this->t_categorie['ID_CAT'];
	}
	
	/** Vérifie si une valeur existe déjà en base
	 *  IN : objet , $version (opt) Si spécifié cherche la version, sinon se base sur la langue en cours
	 *  OUT : TRUE si existe, FALSE si libre
	 */
	function checkExist() {
		global $db;
		
		// VP 11/07/2017 : possibilité de créer 2 catégories de même nom si elles ont des parents différents
		$sql="SELECT ID_CAT FROM t_categorie WHERE CAT_NOM LIKE ".$db->Quote(trim($this->t_categorie['CAT_NOM']))." AND ID_LANG=".$db->Quote($this->t_categorie['ID_LANG'])." AND CAT_ID_TYPE_CAT=".$db->Quote($this->t_categorie['CAT_ID_TYPE_CAT'])." AND CAT_ID_GEN=".intval($this->t_categorie['CAT_ID_GEN']);
		//si l'objet existe déjà en base, on l'exclue de la recherche de doublon
		if (!empty($this->t_categorie['ID_CAT'])) $sql.=" AND ID_CAT<>".intval($this->t_categorie['ID_CAT']);
		$rs=$db->GetOne($sql);
		return $rs;
	}
	
	
    function checkExistId() {
        global $db;
        $sql="SELECT ID_CAT FROM t_categorie WHERE ID_CAT=".intval($this->t_categorie['ID_CAT']);
        $sql.=" AND ID_LANG=".$db->Quote($this->t_categorie['ID_LANG']);
        return $db->GetOne($sql);
    }
	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 *
	 */
 	function updateFromArray ($tab_valeurs) {
	 	global $db;
		$cols=$db->MetaColumns('t_categorie');
		
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (array_key_exists(strtoupper($fld), $cols)) $this->t_categorie[strtoupper($fld)]=trim($val);
		}
		if(empty($this->t_categorie['ID_LANG']))$this->t_categorie['ID_LANG']=$_SESSION['langue'];
		
		
		if ($tab_valeurs['arrFather']) $this->t_categorie['CAT_ID_GEN']=$tab_valeurs['arrFather'][0]['ID_CAT'];
		$this->arrChildren=$tab_valeurs['arrChildren'];
		$this->arrFather=$tab_valeurs['arrFather'];
		$this->arrVersions=$tab_valeurs['version'];
		
	}
	
	/**
	 *
	 * Créé une nouvelle occurence si la valeur n'existe pas, avec ses versions, et retourne l'id_cat
	 */
	function createFromArray($tab_valeurs, $create = true, $saveVersion = true) {
		// VP 11/07/2017 : possibilité de passer un paramètre FULLPATH qui contient la hiérachie du terme
		if(isset($tab_valeurs['FULLPATH']) && !empty($tab_valeurs['FULLPATH'])){
			  // Séparateur tab par défaut et > ou / sinon
			  $fullpath = $tab_valeurs['FULLPATH'];
			  if(strpos($fullpath, chr(9))===false){
				if(strpos($fullpath, '>')!==false){
				  $fullpath=str_replace('>',chr(9),$fullpath);
				} elseif(strpos($fullpath, '/')!==false) {
				  $fullpath=str_replace('/',chr(9),$fullpath);
				}
			  }
			  $fullpath=trim($fullpath);
			  $_chunks=explode(chr(9), $fullpath);
			  $tab_valeurs2=$tab_valeurs;
			  unset($tab_valeurs2['FULLPATH']);
			  $id_cat=intval($tab_valeurs2['CAT_ID_GEN']);
			  foreach($_chunks as $_chunk){
				  $temp_cat= new Categorie;
				  $tab_valeurs2['CAT_NOM']=trim($_chunk);
				  $tab_valeurs2['CAT_ID_GEN']=$id_cat;
				  $id_cat = $temp_cat->createFromArray($tab_valeurs2, $create, $saveVersion);
			  }
			  $this->t_categorie=$temp_cat->t_categorie;
			  return $this->t_categorie['ID_CAT'];
		}else{
			$this->updateFromArray($tab_valeurs);
			
			//Si la valeur existe déjà, on retourne juste son id
			$exist = $this->checkExist();
			if ($exist) {
				$this->t_categorie['ID_CAT']=$exist;
				return $this->t_categorie['ID_CAT'];
			}else{
	/*                    B.RAVI 2015-03-09 pour rechercher/remplacer de class_modifLot.php, on a besoin juste savoir si une catégorie existe, et PAS la créer si elle n'existe pas */
				if (!$create) {
					return false;
				}
			//Sinon on la créé ou on la sauvegarde
				$this->save();
									  
			//On duplique alors aussi pours les versions en langues étrangères
				if($saveVersion) {
					$aLang = getOtherLanguages();
					$valVersions = array();
					foreach ($aLang as $lang) {
						$valVersions[$lang] = $this->t_categorie['CAT_NOM'];
					}
				}
				$ok = $this->saveVersions($valVersions);

			return $this->t_categorie['ID_CAT'];
			}
		}
    }
	
	/**
	 *
	 * Sauve l'objet
	 */
	function save()
	{
		global $db;
		
		$valeurs='';
		$first=true;
		
		// enregistrement du type de categorie si on la créée
		$result=$db->Execute('SELECT COUNT(*) AS COMPTE FROM t_type_cat WHERE ID_TYPE_CAT='.$db->Quote($this->t_categorie['CAT_ID_TYPE_CAT']).' AND ID_LANG='.$db->Quote($this->t_categorie['ID_LANG']).'')->GetRows();
		if ($result[0]['COMPTE']==0 && !empty($this->t_categorie['CAT_ID_TYPE_CAT']))
		{
			// insertion de la categorie dans la base
			$result=$db->Execute('INSERT INTO t_type_cat(ID_TYPE_CAT,ID_LANG,TYPE_CAT) VALUES ('.$db->Quote($this->t_categorie['CAT_ID_TYPE_CAT']).','.$db->Quote($this->t_categorie['ID_LANG']).','.$db->Quote($this->t_categorie['TYPE_CAT']).')');
		}
		
		$this->t_categorie['CAT_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		
		$this->t_categorie['CAT_DATE_MOD']=date('Y-m-d H:i:s');
		 
		// MS 12/02/2016 : mise à jour cat_path
		// MS 29.01.18 - toujours appelé.
        $this->setCatPath();
        
		
		unset($this->t_categorie['TYPE_CAT']);
		
		if (isset($this->t_categorie['ID_CAT']) && !empty($this->t_categorie['ID_CAT']))
		{
			$this->setVignette() ; 
			$rs = $db->Execute('SELECT * FROM t_categorie WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']).' AND ID_LANG='.$db->Quote($this->t_categorie['ID_LANG']).'');
			$sql = $db->GetUpdateSQL($rs, $this->t_categorie);
			if (!empty($sql)) $db->Execute($sql);
		}
		else{
			$ok = $this->create();
			
			if(!$ok){return false;}
			
			$arrVersions = array_diff($_SESSION['arrLangues'],array($this->t_categorie['ID_LANG']));
			
			foreach($arrVersions as $vers){
                $version= new Categorie;
                $version->t_categorie = $this->t_categorie;
				$version->t_categorie['ID_LANG'] = $vers;
				$version->create();
			}
		
		}
		//VP 30/6/18 : déplacement appel updateDocChampsXML dans catSaisie
		//$this->updateDocChampsXML(true);
	}

    /** Sauve les différentes versions
    *  IN : tableau des versions, MODE = retour de la fonction save appelée avant. Si retour=1=>création
    * 	OUT : màj base
    */
    function saveVersions($tabVersions=null) {
        global $db;
		
		if(!empty($tabVersions)){
			foreach ($tabVersions as $lang=>$valeur) {
				if (!empty($valeur)) {

				//if(is_array($valeur) && isset($valeur['CAT_NOM'])){$valeur = $valeur['CAT_NOM'];}
				if(!is_array($valeur)){$valeur = array('CAT_NOM' => $valeur);}

				$version= new Categorie;
				$version->t_categorie= $this->t_categorie; //recopie de tous les champs par deft
				$version->updateFromArray ($valeur);
				$version->t_categorie['ID_LANG']=$lang; //update langue
				if ($version->checkExistId()) $ok=$version->save(); else $ok=$version->create();

				if ($ok==false && $version->error_msg) {$this->dropError(kErrorValeurVersionSauve.' : '.$version->error_msg);return false;}
				unset($version);
				}
			}
			return true;
		}else{
			if(!empty($this->arrVersions)){
				foreach($this->arrVersions as $version){
					if(isset($version['ID_CAT']) && !empty($version['ID_CAT']) && isset($version['ID_LANG']) && !empty($version['ID_LANG']) ){
						$cat_version= new Categorie;
						$cat_version->t_categorie= $this->t_categorie; //recopie de tous les champs par deft
						$cat_version->updateFromArray ($version);	
						if ($cat_version->checkExistId()){
							$ok=$cat_version->save(); 
						}else{
							$ok=$cat_version->create();
						}
						if ($ok==false && $cat_version->error_msg) {$this->dropError(kErrorValeurVersionSauve.' : '.$cat_version->error_msg);return false;}
						unset($cat_version);
					}
				}
			}
		}
    }

    function delete()
	{
		global $db;
		
		$this->arrDoc = $this->getDocs();
		$this->removeAllDocs();
		
		$this->updateDocChampsXML(true);
		
		if (isset($this->t_categorie['ID_CAT']) && !empty($this->t_categorie['ID_CAT']))
		{
			$db->Execute('DELETE FROM t_categorie WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']));
			$db->Execute('DELETE FROM t_doc_acc WHERE ID_CAT='.intval($this->t_categorie['ID_CAT']));
		}
	}
	
	function getSubCats()
	{
		global $db;
		
		if (isset($this->t_categorie['ID_CAT']) && !empty($this->t_categorie['ID_CAT']))
		{
			$resultat_cats=$db->Execute('SELECT distinct id_cat FROM t_categorie WHERE cat_id_gen='.intval($this->t_categorie['ID_CAT']))->GetRows();
			$arr_cats=array();
			
			require_once(modelDir.'model_doc.php');
			foreach ($resultat_cats as $cat)
			{
				$myCat=new Categorie($cat['ID_CAT']);
				$myCat->getCategorie();
				$arr_cats[]=$myCat;
			}	
			return $arr_cats;
		}
		return null;
	}
	
	function xml_export($entete="",$encodage="",$indent=""){
		$xml='<t_categorie>';
		
		
		foreach($this->t_categorie as $id=>$val)
		{
			$xml.='<'.strtoupper($id).'>'.xml_encode($val).'</'.strtoupper($id).'>';
		}
		
		if (!empty($this->arrDoc)) {
            $xml.="\t<t_doc_cat nb=\"".count($this->arrDoc)."\">\n";
			foreach ($this->arrDoc as $name => $doc) {
				$xml .= "\t\t<DOC_CAT ID_DOC=\"".$doc->t_doc["ID_DOC"]."\">\n";
				if (is_object($doc)) {$xml.="\t\t\t".$doc->xml_export()."\n";}
				$xml .= "\t\t</DOC_CAT>\n";
			}
            $xml.="\t</t_doc_cat>\n";
		}
		
		$arrCats = $this->getSubCats();
		if (!empty($arrCats)) {
            $xml.="\t<t_sub_cat nb=\"".count($arrCats)."\">\n";
			foreach ($arrCats as $k => $cat) {
				$xml .= "\t\t<CAT ID_DOC=\"".$cat->t_categorie["ID_CAT"]."\">\n";
				if (is_object($cat)) {$xml.="\t\t\t".$cat->xml_export()."\n";}
				$xml .= "\t\t</CAT>\n";
			}
            $xml.="\t</t_sub_cat>\n";
		}
		
		$xml.='</t_categorie>';
		
		return $xml;
	}
	

	//update MS 16/01/13
	function updateDocChampsXML($saveSolr=false) {
		require_once(modelDir.'model_doc.php');
		if(empty($this->arrDoc))
			$this->arrDoc = $this->getDocs();
		foreach ($this->arrDoc as $oDoc) {
			$oDoc->updateChampsXML("DOC_CAT");
			if($saveSolr && ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))){
				$oDoc->saveSolr();
			}
			unset($oDoc);
		}
	}
	
	
	function setCatPath() {
		require_once(libDir."class_chercheCat.php");
		
		if(!empty($this->t_categorie['CAT_ID_GEN'])){
			$father_cat = new Categorie();
			$father_cat->t_categorie['ID_CAT'] = $this->t_categorie['CAT_ID_GEN'];
			$father_cat->t_categorie['ID_LANG'] = $this->t_categorie['ID_LANG'];
			$father_cat->getCategorie();
		}else{
			$this->t_categorie['CAT_PATH'] = '';
			return true ;
		}
		// on passe par le pere, en effet passer par rechercheCat avant que cat_id_gen soit enregistré n'est pas très malin ... (cf setLexPath ... )
		// note : si on a toujours des path étranges des qu'on utilise cette fonction, verifiez qu'il ne s'agit pas d'une catégorie ayant un id_cat à 0 (sur une base un peu trop permissive ... ) 
		$mySearch=new RechercheCat();
		if(!empty($father_cat->t_categorie['ID_LANG'])){
			$mySearch->lang=$father_cat->t_categorie['ID_LANG'];
		}
		$mySearch->getFather($father_cat->t_categorie['ID_CAT'],true,$arrNodes);
		$cat_path=$father_cat->t_categorie['CAT_NOM'];
		if(count($arrNodes)>0){
			foreach($arrNodes as $node){
				$cat_path=$node['terme'].chr(9).$cat_path;
			}
		}
		$this->t_categorie['CAT_PATH'] = $cat_path;
	}
	
	
	// fonction de duplication des catégories 
	// Ajoute " - ".kCopie au nom de la catégorie dupliquée, 
	// l'ensemble des autres informations devraient être conservées, à part des mises à jours des dates & profils de création de la nouvelle catégorie 
	
	function duplicateCat(){
		global $db;
		if (empty($this->arrVersions)) $this->getVersions(); // Récup des infos si pas encore loadées
		if (empty($this->t_doc_cat)) $this->getDocCat(false);

        $dupCat=new Categorie();
        $dupCat= clone $this; // Recopie des infos
        unset($dupCat->t_categorie['ID_CAT']); // pour éviter les effets de bord
		
		$dupCat->t_categorie['CAT_NOM']=$dupCat->t_categorie['CAT_NOM']." - ".kCopie;
		
        $dupCat->create(); // récupération du nouvel ID
		
		foreach($_SESSION['arrLangues'] as $lg){
			if($lg!=$_COOKIE['cook_lang'])
			{
				$dupCat1=new Categorie();
				$dupCat1->t_categorie['ID_CAT']=$this->t_categorie['ID_CAT'];
				$dupCat1->t_categorie['ID_LANG']=$lg;
				if($dupCat1->checkExistId()){
					$dupCat1->getCategorie();
					$dupCat1->t_categorie['ID_CAT']=$dupCat->t_categorie['ID_CAT'];
					$dupCat1->t_categorie['CAT_NOM']=$dupCat1->t_categorie['CAT_NOM']." - ".kCopie;
					$dupCat1->t_categorie['CAT_DATE_CREA'] = $dupCat->t_categorie['CAT_DATE_CREA']; 
					$dupCat1->t_categorie['CAT_DATE_MOD'] = $dupCat->t_categorie['CAT_DATE_MOD'];  
					$dupCat1->create();
				}
			}
		}
        return $dupCat;
	}
}
?>
