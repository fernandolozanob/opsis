<?php
function ftdump_to_utf8($str) 
{
        return strtr($str, array("\xE3\xA0"=>"à", "\xE3\xA2"=>"â", "\xE3\xA4"=>"ä",
		"\xE3\xA9"=>"é","\xE3\xA8"=>"è","\xE3\xAA"=>"ê","\xE3\xAB"=>"ë",
		"\xE3\xAE"=>"î","\xE3\xAF"=>"ï",
		"\xE3\xB4"=>"ô","\xE3\xB6"=>"ö",
		"\xE3\xB9"=>"ù","\xE3\xBB"=>"û","\xE3\xBC"=>"ü",
		"\xE3\xA7"=>"ç","\xE2\x82"=>"€","\xE2\xB0"=>"°",
		"\xE5\x93"=>"æ","\xE3\xA6"=>"œ","\xE3\xB1"=>"ñ",
		"\xE3\x80"=>"À", "\xE3\x82"=>"Â", "\xE3\x84"=>"Ä",
		"\xE3\x89"=>"É","\xE3\x88"=>"È","\xE3\x8A"=>"Ê","\xE3\x8B"=>"Ë",
		"\xE3\x8E"=>"Î","\xE3\x8F"=>"Ï",
		"\xE3\x94"=>"Ô","\xE3\x96"=>"Ö",
		"\xE3\x9B"=>"Û","\xE3\x9C"=>"Ü",
		"\xE5\x92"=>"Æ","\xE3\x86"=>"Œ","\xE3\x91"=>"Ñ"));
}
	global $db;

	// Lancement dump index
	$_binary=explode(" ",gFTdumpPath);
	$_binary=$_binary[0];
	if (!is_file($_binary)) {echo "Pas de FTdump ".$_binary;return; }

	$cmd=gFTdumpPath." > ".gFTdumpFile;
	trace($cmd);
	exec($cmd); //Lancement du storyboard
	
	// Suppression lignes t_mot
	$sql="delete from t_mot";
	$ok=$db->Execute($sql);

	// Import fichier
	$cnt=0;
	$handle=@fopen(gFTdumpFile,"r");
	if($handle){
		while(!feof($handle)){
			$ligne=fgets($handle);
			$tab=explode(" ",$ligne);
			$tab=split(" ",$ligne);
			$mot=$tab[count($tab)-1];
			//Vérif
			$mot=ftdump_to_utf8($mot);
			if (!$db->getOne("SELECT MOT FROM t_mot WHERE MOT=".$db->Quote($mot)))
				$sql="insert ignore into t_mot (MOT) values (".$db->Quote($mot).")";
			$ok=$db->Execute($sql);
			if(!$ok) exit;
			$cnt++;
		}
	fclose($handle);
	}
	echo $cnt." mots créés";

?>
