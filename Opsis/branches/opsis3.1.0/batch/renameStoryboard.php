<?php

//require_once('../conf/conf.inc.php');
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 

require_once(modelDir.'model_imageur.php');

header('Content-type: text/plain');

global $db;

$result=$db->Execute('SELECT ID_IMAGEUR FROM t_imageur')->getRows();

foreach ($result as $dat_imgr)
{
	$imageur=new Imageur();
	$imageur->t_imageur['ID_IMAGEUR']=$dat_imgr['ID_IMAGEUR'];
	$imageur->getImageur();
	
	echo 'IMAGEUR : '.$imageur->t_imageur['IMAGEUR']."\n";
	
	// creation du repertoire
	$imageur->createImageurDir();
	
	if (isset($imageur->t_images) && !empty($imageur->t_images))
	{
		foreach ($imageur->t_images as $image)
		{
			$source=kStoryboardDir.$image->getFilePath();
			$destination=kStoryboardDir.$imageur->getImageurPath().$image->t_image['IM_FICHIER'];
			
			if (file_exists($source))
			{
				echo "\t".$source."\n\t\t-> ".$destination.' : ';
				if (rename($source,$destination)===false)
					echo 'FAILED';
				else
					echo 'DONE';
			}
			else
				echo "\t".'le fichier '.$source.' n\'existe pas';
			
			
			$image->t_image['IM_CHEMIN']=$imageur->getImageurPath();
			$ok=$image->save();
			$image->saveVersions($_SESSION['arrLangues'],'save');
			
			echo "\n";
		}
	}
	
	// suppression du repertoire storyboard
	if (file_exists(kStoryboardDir.$imageur->t_imageur['IMAGEUR']))
	{
		if (rmdir(kStoryboardDir.$imageur->t_imageur['IMAGEUR'])===false)
			echo 'Erreur suppression repertoire storyboard '.$imageur->t_imageur['IMAGEUR'].' ('.kStoryboardDir.$imageur->t_imageur['IMAGEUR'].')';
	}
	else
		echo 'repertoire storyboard inexistant ('.kStoryboardDir.$imageur->t_imageur['IMAGEUR'].')';
}

?>