﻿<?php
global $db;

// VP 9/02/10 : construction LEX_PATH
if(isset($_GET['id_type_lex'])){
	calcNbDocs(0,$_GET['id_type_lex']);
}else{
	$rows=$db->GetAll("select distinct ID_TYPE_LEX from t_type_lex where HIERARCHIQUE='1'");
	foreach($rows as $row) {
		print "\n**** TRAITEMENT ".$row['ID_TYPE_LEX']." ****\n";
		calcNbDocs(0,$row['ID_TYPE_LEX']);
	}
}


	function calcNbDocs($id, $type="",$versions=array()){
		// VP 27/05/10 : correction bug sur versions
		global $db;
		if(!empty($id)) {
			$rows=$db->GetAll("select ID_LANG,LEX_TERME,LEX_PATH from t_lexique where ID_LEX=".intval($id)." order by ID_LANG");
			$childs=$db->GetAll("select distinct ID_LEX from t_lexique where ID_LEX!=".intval($id)." and LEX_ID_GEN=".intval($id)."");
		}else{
			$childs=$db->GetAll("select distinct ID_LEX from t_lexique where LEX_ID_TYPE_LEX=".$db->Quote($type)." and LEX_ID_GEN=0 and LEX_ID_SYN=0");
			$rows=$db->GetAll("select ID_LANG from t_lang order by ID_LANG");
		}
		$tmpVers=array();
		foreach($rows as $i=>&$row){
			$tmpVers[$row['ID_LANG']]['LEX_PATH']=$versions[$row['ID_LANG']]['LEX_PATH'].chr(9).$row['LEX_TERME'];
			$terme= $row['LEX_TERME'];
		}
		$idlist=$id;
		foreach($childs as $child){
			$idlist.=",".calcNbDocs($child['ID_LEX'],$type,$tmpVers);
		}
		if(!empty($id)){
			print "\n ".$terme;
			$docs=$db->GetAll("select ID_LANG,count(distinct t_doc_lex.ID_DOC) as nb_docs  from t_doc_lex, t_doc   
							  where t_doc_lex.ID_LEX in(".$idlist.") and t_doc.ID_DOC=t_doc_lex.ID_DOC  and t_doc.DOC_ACCES='1' group by ID_LANG");
			foreach($docs as $i=>$doc){
				$sql="update t_lexique set LEX_NB_DOCS=".intval($doc['nb_docs']).",LEX_PATH=".$db->Quote($versions[$doc['ID_LANG']]['LEX_PATH'])." where ID_LEX=".intval($id)." and ID_LANG=".$db->Quote($doc['ID_LANG']);
				$db->Execute($sql);
			}
		}
		return $idlist;
	}
	
?>
