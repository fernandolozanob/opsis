<?php
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
global $db;
	
$sql = "SELECT distinct ACT_IP FROM t_action WHERE ACT_PAYS is null AND ACT_IP <> ''";
$res = $db->GetAll($sql);

foreach ($res as $act) {
	$pays = geoip_country_name_by_name($act['ACT_IP']);
	if (!$pays && strpos($act['ACT_IP'], "192.168.") === 0) $pays = "France";
	
	echo $act['ACT_IP']." : $pays <br/>\n";
	//Sauvegarde
	$rs = $db->Execute("SELECT * FROM t_action WHERE ACT_PAYS is null AND ACT_IP = ".$db->quote($act['ACT_IP']));
	$sql = $db->GetUpdateSQL($rs, array("ACT_PAYS" => $pays));
	if (!empty($sql)) $ok = $db->Execute($sql);
}
?>