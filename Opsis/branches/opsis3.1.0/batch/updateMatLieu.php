<?php
include_once(modelDir.'model_materiel.php');
global $db;
$fmts=Materiel::getOnlineFormats();
$arrMats=$db->GetAll("select ID_MAT from t_mat where MAT_FORMAT in ('".implode("','",$fmts)."')");
foreach ($arrMats as $i=>$mat) {
	//if ($i>3) break; //pour le moment
	$myMat=new Materiel;
	$myMat->t_mat['ID_MAT']=$mat['ID_MAT'];
	$myMat->getMat();
	$currentLocation=$myMat->getLieu();
	$newLocation=$myMat->setLieu();
	debug($mat['ID_MAT'].' => '.$mat['MAT_NOM']." : ".$currentLocation." >>> ".$newLocation,'red');
	
	if ($currentLocation!=$newLocation) { //les chemins récupérés ne sont pas compatibles.
		if (file_exists($currentLocation.$mat['MAT_NOM'])) { //fichier original existe => on va le déplacer
			if (!is_dir($newLocation)) $ok=mkdir($newLocation); else $ok=true;
			if (!$ok) { echo kErrorMaterielLieuImpossibleCreerDir.$newLocation;continue;}
			$ok=mv_rename($currentLocation.$mat['MAT_NOM'],$newLocation.$mat['MAT_NOM']);	
			if (!$ok) { echo kErrorMaterielLieuImpossibleDeplacerFichier;continue;
			} else {
				$arrMovedFiles[$newLocation]++;
				$myMat->save();
			}
					
		} else { //pas de fichier source => erreur
			echo kErrorMaterielExistePasDansDir.$currentLocation;
					
		}
	
	}
	
}

debug($arrMovedFiles);

?>
