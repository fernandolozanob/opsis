<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- D�veloppeurs : Vincent Prost, Fran�ois Duran, J�rome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

/**
 * TRES IMPORTANT : toujours mettre les sauts de ligne \n entre DOUBLES quotes et en FIN de ligne
 */
    
   echo "DEBUT traitement - Alerte mail";

    require_once(modelDir.'model_requete.php');
    require_once(modelDir.'model_usager.php');

    $tab_usager = array();
	global $db;

    // I. Execution des recherches et mise � jour dans la BDD
    $rst = $db->GetAll("SELECT ID_REQ FROM t_requete WHERE REQ_ALERTE='1'");
    foreach ($rst as $req) {
        // I.1. Infos sur la requete
        $myReq = New Requete();
        $myReq->t_requete['ID_REQ']=$req['ID_REQ'];

        $myReq->getRequete(null,-1);
        
        // I.2. Ex�cution de la requ�te
        // VP 8/06/2012 : correction constitution requête
        $sql=$myReq->t_requete['REQ_SQL'];
        $allDocs = $db->GetAll($sql);
        
        $sql.=" AND t1.DOC_DATE_MOD>".$db->Quote($myReq->t_requete['REQ_DATE_EXEC']);
        if(strpos(strtoupper($sql),'SELECT DISTINCT ')===false) $sql=str_replace('SELECT ','SELECT DISTINCT ', $sql);
        
        $newDocs = $db->GetAll($sql);
        if(!empty($newDocs) && $myReq->t_requete['REQ_NB_DOC'] != count($allDocs)){ //De nouveaux documents sont apparus !
            $tab_usager[$myReq->t_requete['ID_USAGER']][$myReq->t_requete['ID_REQ']]=
            array('REQUETE'=>$myReq->t_requete['REQUETE'],
                  'REQ_LIBRE'=>$myReq->t_requete['REQ_LIBRE'],
                  'ID_REQ'=>$myReq->t_requete['ID_REQ'],
                  'DOC_LISTE'=>$newDocs
                  );
            $myReq->t_requete['REQ_DATE_EXEC']=date("Y-m-d"); //MAJ requete
            $myReq->t_requete['REQ_NB_DOC']=count($allDocs);
            $myReq->save();
        }
    }

    // II. Envoie des emails
    foreach($tab_usager as $id_usager => $requete)
    {
		$message='';
        // II.A. Informations sur l'usager
        $myUser = New Usager();
        $myUser->t_usager['ID_USAGER']=$id_usager;
        $myUser->getUsager();
        $email=($myUser->t_usager['US_SOC_MAIL']?$myUser->t_usager['US_SOC_MAIL']:$myUser->t_usager['US_MAIL']);

		$xml=array2xml($requete,'select');
		$xml=str_replace(array(" ' ","<DOC_LISTE><DOC_LISTE>","</DOC_LISTE></DOC_LISTE>","<select>","</select>"),
						array("'","<DOC_LISTE>","</DOC_LISTE>","<select>","</select>"),$xml);

		$html=TraitementXSLT($xml,getSiteFile("listeDir",'alerteMail.xsl'),null);
        ob_start();
        eval("?>". $html."<?");
        $html=ob_get_contents();
		ob_end_clean();
		$html=str_replace('</td>','</td>'.chr(10),$html); //retour à la ligne, important sinon le client mail fait des
														 //retours arbitraires (genre au milieu d'une balise !)
		echo($html);

		$frontiere = "_----------=_parties_".md5(uniqid (rand()));

       //$headers="From:".gMail."\r\nReply-to:".gMail."\r\nX-Sender:OPSIS\r\nX-Mailer:PHP\r\nMIME-Version: 1.0\r\nContent-Type: multipart/alternative; boundary=\"".$frontiere."\"\n";


        // II.B. Cr�ation du message
        $sujet=kAlerteMailSujet.gSite;

		$message = 'This is a multi-part message in MIME format.'."\n\n";

        $message .= '--'.$frontiere."\n";
     	$message .= 'Content-Type: text/plain; charset="iso-8859-1"'."\n";
     	$message .= 'Content-Transfer-Encoding: 8bit'."\n\n";
     	$message .= kAlerteMailEntete."\n";

		foreach ($requete as $_req) {
			$message.="\n".$_req['REQUETE']." (".$_req['REQ_LIBRE'].")\n";
			foreach ($_req['DOC_LISTE'] as $doc) {
				$message.="\t- ".$doc['DOC_TITRE']."\n";
			}
		}
		$message .=kAlerteMailDisclaimer."\n\n";
		
		if (!empty($html)) {
	        $message.='--'.$frontiere."\n";
	        $message.="Content-Type:text/html; charset=ISO-8859-1\r\nContent-Transfer-Encoding: 8bit\r\n";
	
			$message.=$html."\n";
		}

        $message.='--'.$frontiere."--\n";

        $message=utf8_decode($message);

        echo "<br/>Envoi d'un email pour : ".$email;

        include_once(modelDir.'model_message.php');
		$msg = new Message();
		$ok = $msg->send_mail($email, gMail, $sujet, $message, "multipart/alternative", $frontiere);
    	if (!$ok) echo 'Echec envoi mail';
    }
    echo "<br/>FIN traitement - Alerte mail";
?>