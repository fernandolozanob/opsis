<?php
// VP 15/05/2013 : possibilité de définir un dossier d'import alternatif à kImportDir, en passant une constante dans importRoot définie dans conf
$import_root=kImportDir.'/';
if (isset($_GET['importRoot']) && !empty($_GET['importRoot']) && defined($_GET['importRoot'])&& is_dir(constant($_GET['importRoot']))){
    $import_root=constant($_GET['importRoot']).'/';
}

if (isset($_GET['importFolder']) && !empty($_GET['importFolder']))
{
	$url_import_folder=str_replace('../','',$_GET['importFolder']);
	$import_dir=$import_root.$url_import_folder.'/';
	$url_import_folder='importFolder='.urlencode(str_replace('../','',$_GET['importFolder'])).'&';
}
else
{
	$import_dir=$import_root;
	$url_import_folder='';
}
if (isset($_GET['currentFolder']) && !empty($_GET['currentFolder']))
{
	$url_path=str_replace('../','',$_GET['currentFolder']);
	$import_dir=$import_dir.$url_path;
}

$dh=opendir($import_dir);

if ($dh!=false)
{

	$nb_files=0;
	$fichiers=array();
	$repertoires=array();
// VP 10/10/12 : on ne prend pas en compte les fichiers commençant par '.'
	while (($file=readdir($dh))!==false)
	{
		//if ($file!='.' && $file!='..' && is_file($import_dir.$file))
		if ($file[0]!='.' && is_file($import_dir.$file))
		{
			$nb_files++;
			$fichiers[]=$file;
			$fichier_reel[]=$import_dir.$file;
		}
		//else if ($file!='.' && $file!='..' && is_dir($import_dir.$file))
		else if ($file[0]!='.' && is_dir($import_dir.$file))
		{
			$nb_files++;
			$repertoires[]=$file;
			$repertoires_reel[]=$import_dir.$file;
		}
	}

	closedir($dh);
}
else
	echo kErreurOuvertureRepertoire;

if ($nb_files==0 && (!isset($url_path) || empty($url_path)))
	echo kErreurAucunFichierDossierTrouve;
else
	include(getSiteFile('formDir','importFolder.inc.php'));

?>