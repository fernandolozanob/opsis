<?php

	$myPage=PageWebService::getInstance();
	$type=($_REQUEST["type"]);
	$id_lang=strtoupper($_REQUEST["lang"]);
	global $db;
	if (!$type || !$id_lang) throw new Exception (kErrorWSParametreManquant,"006");

	$content="<data>";
	
	$types=explode(",",$type);

	foreach ($types as $t) {
	
		$desc_champ=explode("_",$t);

		switch ($desc_champ[0]) {
		
			case 'V':
				if (!$desc_champ[1]) throw new Exception (kErrorWSSyntaxeParametre.$t,"007");
				$rst=$db->CacheGetAll(1,"SELECT ID_VAL,VALEUR from t_val where ID_LANG=".$db->Quote($id_lang)." AND VAL_ID_TYPE_VAL=".$db->Quote($desc_champ[1])." ORDER BY VALEUR");
				$content.="<liste type='".$t."'><rows>";
				foreach ($rst as $i=>$row) {
					$content.="<row rang='".$i."'>";
					$content.="<key>".$row['ID_VAL']."</key><value>".$row['VALEUR']."</value>";
					$content.="</row>";
				}
				$content.="</rows></liste>";
			break;
			
			case 'L':
				// VP 15/06/10 : paramètre $desc_champ[2] optionnel
				//if (!$desc_champ[1] || !$desc_champ[2]) throw new Exception (kErrorWSSyntaxeParametre.$t,"007");
				if (!$desc_champ[1] ) throw new Exception (kErrorWSSyntaxeParametre.$t,"007");
				$sql="select distinct t_lexique.ID_LEX, LEX_TERME 
				from t_lexique left join t_doc_lex ON (t_doc_lex.ID_LEX=t_lexique.ID_LEX)
				where t_lexique.ID_LANG=".$db->Quote($id_lang)." 
				and t_doc_lex.ID_TYPE_DESC=".$db->Quote($desc_champ[1]);
				if($desc_champ[2]) $sql.=" and t_doc_lex.DLEX_ID_ROLE=".$db->Quote($desc_champ[2]);
				$sql.=" order by LEX_TERME";
				$rst=$db->CacheGetAll(1,$sql);
				$content.="<liste type='".$t."'><rows>";
				foreach ($rst as $i=>$row) {
					$content.="<row rang='".$i."'>";
					$content.="<key>".$row['ID_LEX']."</key><value>".$row['LEX_TERME']."</value>";
					$content.="</row>";
				}
				$content.="</rows></liste>";			
			break;
			
			case 'FEST':
				if (!$desc_champ[1]) throw new Exception (kErrorWSSyntaxeParametre.$t,"007");	
				$rst=$db->CacheGetAll(1,"select distinct ".$t." from t_festival order by ".$t);
				$content.="<liste type='".$t."'><rows>";
				foreach ($rst as $i=>$row) {
					$content.="<row rang='".$i."'>";
					$content.="<key>".$row[$t]."</key><value>".$row[$t]."</value>";
					$content.="</row>";
				}
				$content.="</rows></liste>";										
			break;
			
		}
	
	}
	
	$content.="</data>";
	
	echo $content;

?>
