<?


//	$myUsr=User::getInstance();
//	debug($_REQUEST,'cyan');

//unset($_SESSION['recherche_DOC']);
	
	$myPage=PageWebService::getInstance();
	
	$id_lang=strtoupper($_REQUEST["lang"]);
	$entite=$_REQUEST['entite'];

	if (!$id_lang || !$entite) throw new Exception (kErrorWSParametreManquant,"006");
	$_SESSION['langue']=$id_lang;
    
    if ($_REQUEST['encodage']) $myPage->encoding=$_REQUEST['encodage'];
    
    //Recherche
    
    switch ($_REQUEST['entite']) {
    	case 'doc':
 		   	require_once(libDir."class_chercheDoc.php");
  		  	$mySearch=new RechercheDoc();
    	break;
    	
    	case 'personne':
    		require_once(libDir."class_cherchePers.php");
    		$mySearch=new RecherchePersonne();
    	break;
    	
    	case 'festival':
    		require_once(libDir."class_chercheFest.php");
    		$mySearch=new RechercheFest();		
    	break;    	
    }
	$nbLignes = 1; //Par défaut, 1 résultat par page !
    
    if (!$mySearch) throw new Exception (kErrorWSEntiteNonTrouvee,"005");
    
    //Cas export Panier / Word
    if ($_REQUEST['filename']) {
 
    	$style=$entite.'ExportWebService.xsl';
    	$filename=basename($_REQUEST['filename']);
		$file_extension = strtolower(substr(strrchr($filename,"."),1));  
		if (!in_array($file_extension,array("pdf","doc","xls","ppt"))) throw new Exception($file_extension." ".kErrorWSExtensionNonSupportee,"011");	    	
	   
	   	switch($file_extension) {
		     case "pdf": $ctype="application/pdf"; break;
			 case "doc": $ctype="application/msword"; break;
			 case "xls": $ctype="application/vnd.ms-excel"; break;
			 case "ppt": $ctype="application/vnd.ms-powerpoint"; break; 
	    }
	    $nbLignes='all'; //on inclut tout le contenu de la recherche / panier
	    $myPage->binary=true; //pour spécifier du contenu non xml
	    
	    $xml_headers="<EXPORT_OPSIS>%s</EXPORT_OPSIS>"; //Comme on a plusieurs entites, il faut un header au XML
	    header("Content-Type: $ctype; charset=".($myPage->encoding?$myPage->encoding:'utf-8'));
	    header("Content-Disposition: attachment; filename=".$filename.";");
    }
    
    
	$mySearch->useSession=false;

	// GT 15-04-11 : calcul de la durée d'un action SQL
	$date_debut_requete_sql=microtime(true);
	$mySearch->prepareSQL();
	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs'],$_POST['chNoHist'],$_POST['chOptions']);

	$mySearch->finaliseRequete(); //fin et mise en session
	logAction("SQL",array("ACT_FORM"=>$_POST["F_doc_form"],"ACT_REQ"=>$mySearch->etape,"ACT_SQL"=>$mySearch->sql,'duree_req_sql'=>(microtime(true)-$date_debut_requete_sql)));

    $etape = $mySearch->etape;
	$myPage->getPageFromURL();


//debug($_POST,'lime');
//debug($myPage,'gold',true);
//debug($_REQUEST,"lightgreen");
//debug($mySearch,"yellow");
//debug($_SESSION[$mySearch->sessVar],'ivory');
//debug($_SESSION,'pink');
//debug($myUsr,'gold');

		
			$sql=$mySearch->sql;		
            $sql.= $myPage->getSort();

   

		/*	debug(str_ireplace(array(',','join','WHERE','in '),
				array(','.chr(10),'join'.chr(10),'WHERE'.chr(10),'in '.chr(10)),$sql),'lightgrey');
			*/
            $ok=$myPage->initPager($sql, $nbLignes,$myPage->page,null,60); // init splitter object

			if (!$ok) throw new Exception ($myPage->error_msg,'003');

 			$content="";

			// 2ème étape => récupération des IDs résultats et chargement du détail de chaque résultat
			
			// VP 4/06/10 : echappement des caractères interdits en XML (&, <, >)
			foreach ($myPage->result as $rst) {
		    switch ($entite)
		    {
		        case "doc":
		            require_once(modelDir.'model_doc.php');
		            require_once(modelDir.'model_docAcc.php');
		            $mon_objet = new Doc();
		  			$mon_objet->t_doc['ID_DOC']=$rst['ID_DOC'];
		  			$mon_objet->t_doc['ID_LANG']=$id_lang;
		            $mon_objet->getDocFull(1,1,1,1,1,1);
			        $mon_objet->getDocAcc();
			        $mon_objet->getFestival();
		            $xml= $mon_objet->xml_export(0,$encodage);
					$xml=str_replace(array("’","&"),array("'","&amp;"),$xml);
					$content.=$xml;
		            if (!$style) $style="docDetailWebService.xsl";

		            break;
		
		  		case "personne" :
		  			require_once(modelDir.'model_personne.php');
		  			$mon_objet = new Personne();
		  			$mon_objet->t_personne['ID_PERS']=$rst['ID_PERS'];
		  			$mon_objet->t_personne['ID_LANG']=$id_lang;
		  			$mon_objet->getPersonne();
		  			$mon_objet->getLexique();
		  			$mon_objet->getValeurs();
		  			$mon_objet->getDocAcc();
		  			$mon_objet->getFestival();
		            $xml= $mon_objet->xml_export(0,$encodage);
					$xml=str_replace(array("’","&"),array("'","&amp;"),$xml);
					$content.=$xml;
		            if (!$style) $style="persDetailWebService.xsl";
		  			break;
		            
		        case "festival":
		            require_once(modelDir.'model_festival.php');
		            $mon_objet = New Festival();
		            $mon_objet->t_fest['ID_FEST']=$rst['ID_FEST'];
		  			$mon_objet->t_fest['ID_LANG']=$id_lang;            
		            $mon_objet->getFest();
		            $mon_objet->getLexique();
		            $mon_objet->getPersonnes();
		       		$mon_objet->getValeurs();
			   		$mon_objet->getSections(true);
			   		$mon_objet->getDocs();
		            $mon_objet->getDocAcc();
		            $xml= $mon_objet->xml_export(0,$encodage);
					$xml=str_replace(array("’","&"),array("'","&amp;"),$xml);
					$content.=$xml;
		            if (!$style) $style="festDetailWebService.xsl";   
		        	break;
		    }
			}
 		
 			//On ajoute des headers XML s'ils existents
 			if ($xml_headers) {$content=sprintf($xml_headers,$content);}
 		
            $param["nbLigne"] = $nbLignes;
            $param["page"] = $myPage->page;
            $param["playerVideo"]=gPlayerVideo;
            $param["pager_link"]=$myPage->PagerLink;
            $param["votre_recherche"]=$mySearch->etape; //on utilise htmlentities car c'est juste de l'affichage
            $param["playerVideo"]=gPlayerVideo;
            $param["urlparams"]=$myPage->addUrlParams();
            $param["frameurl"]=frameUrl;
			//debug($param,'yellow');
			
			if($nbLignes=="all") $param["offset"] = "0";
			else $param["offset"] = $nbLignes*($myPage->page-1);
            $param["nbRows"]=$myPage->found_rows;
            $myPage->addParamsToXSL($param);
 			
 			$xsl=getSiteFile("designDir",$style);
 			if (!file_exists($xsl)) throw new Exception (kErrorWSXSLManquant,"004");
			
			$param['media_path']=kCheminHttpMedia;
			$param['docacc_path']=kDocumentUrl;
			$param['http']=kCheminHttp;
			$param['id_lang']=$id_lang;

 			echo TraitementXSLT ($content, $xsl,$param);
           
?>
