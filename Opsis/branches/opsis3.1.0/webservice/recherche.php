<?


	$myUsr=User::getInstance();
//	debug($_REQUEST,'cyan');

//unset($_SESSION['recherche_DOC']);
	
	$myPage=PageWebService::getInstance();
	
	$id_lang=$_REQUEST["lang"];

	if (!$id_lang) throw new Exception (kErrorWSParametreManquant,"006");
	$_SESSION['langue']=strtoupper($id_lang);

    if ($_REQUEST['encodage']) $myPage->encoding=$_REQUEST['encodage']; 
    
    switch ($_REQUEST['entite']) {
    	case 'doc':
 		   	require_once(libDir."class_chercheDoc.php");
  		  	$mySearch=new RechercheDoc();
  		  	$style='docWebService.xsl';
    	break;
    	
    	case 'personne':
    		require_once(libDir."class_cherchePers.php");
    		$mySearch=new RecherchePersonne();
  		  	$style='personneWebService.xsl';    		
    	break;
    	
    	case 'festival':
    		require_once(libDir."class_chercheFest.php");
    		$mySearch=new RechercheFest();
  		  	$style='festWebService.xsl';    		
    	break;    	
    }
    
	if(isset($_GET['xsl']) && !empty($_GET['xsl'])){
		$style = $_GET['xsl'].((substr($_GET['xsl'],-4)!='.xsl')?".xsl":'');
	}	
	
    if (!$mySearch) throw new Exception (kErrorWSEntiteNonTrouvee,"005");

    
    
	$mySearch->useSession=false;
	
	// GT 15-04-11 : calcul de la durée d'un action SQL
	$date_debut_requete_sql=microtime(true);
	$mySearch->prepareSQL();
	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs'],$_POST['chNoHist'],$_POST['chOptions']);

	if ( (defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits) ) ||(!defined('gSeuilAppliqueDroits') && $myUsr->Type<kLoggedDoc ) ){
		$mySearch->appliqueDroits();
	}
		
	$mySearch->finaliseRequete(); //fin et mise en session
	logAction("SQL",array("ACT_FORM"=>$_POST["F_doc_form"],"ACT_REQ"=>$mySearch->etape,"ACT_SQL"=>$mySearch->sql,'duree_req_sql'=>(microtime(true)-$date_debut_requete_sql)));

    $etape = $mySearch->etape;
	$myPage->getPageFromURL();

//debug($_POST,'lime');
//debug($myPage,'gold',true);
//debug($_REQUEST,"lightgreen");
//debug($mySearch,"yellow");
//debug($_SESSION[$mySearch->sessVar],'ivory');
//debug($_SESSION,'pink');
//debug($myUsr,'gold');

		
			$sql=$mySearch->sql;		
            $sql.= $myPage->getSort();
			
trace($sql);
if (isset($_REQUEST["field_en"]) && !empty($_REQUEST["field_en"])) {
				$sql = str_replace(" from t_doc as t1", ", doc_en.".$_REQUEST["field_en"]." as ".$_REQUEST["field_en"]."_EN from t_doc as t1", $sql);
				$sql = str_replace(" WHERE t1.ID_LANG='FR'", "left join t_doc doc_en on doc_en.id_doc = t1.id_doc and doc_en.id_lang='EN' WHERE t1.ID_LANG='FR'", $sql);
			}

            //Sauvegarde du nombre de lignes de recherche � afficher :
            if($_REQUEST['nbLignes'] != '') $nbLignes = $_REQUEST['nbLignes'];

			$nbDeft=defined("gNbLignesDocListeDefaut")?gNbLignesDocListeDefaut:10;
            if(!isset($nbLignes)) $nbLignes = $nbDeft;

		/*	debug(str_ireplace(array(',','join','WHERE','in '),
				array(','.chr(10),'join'.chr(10),'WHERE'.chr(10),'in '.chr(10)),$sql),'lightgrey');
			*/
            $ok=$myPage->initPager($sql, $nbLignes,$myPage->page,null,60); // init splitter object

			if (!$ok) throw new Exception ($myPage->error_msg,'003');

 			//traiter par retour du web service
  			//print("<div class='errormsg'>".$myPage->error_msg."</div>");

            $param["nbLigne"] = $nbLignes;
            $param["page"] = $myPage->page;
            $param["playerVideo"]=gPlayerVideo;
            $param["pager_link"]=$myPage->PagerLink;
            $param["votre_recherche"]= htmlspecialchars ($mySearch->etape); //htmlentities($mySearch->etape,ENT_COMPAT,'UTF-8');
            $param["playerVideo"]=gPlayerVideo;
            $param["urlparams"]=$myPage->addUrlParams();
            $param["urlparams"]=siteDir;
            $param["frameurl"]=frameUrl;
			$param['media_path']=kCheminHttpMedia;
			//debug($param,'yellow');
			
			if($nbLignes=="all") $param["offset"] = "0";
			else $param["offset"] = $nbLignes*($myPage->page-1);
            $param["nbRows"]=$myPage->found_rows;
            $myPage->addParamsToXSL($param);
 			
 			$xsl=getSiteFile("listeDir",$style);
 			if (!file_exists($xsl)) throw new Exception (kErrorWSXSLManquant,"004");

 
            $myPage->afficherListe($_REQUEST['entite'],$xsl,true,$mySearch->tabHighlight);
           
?>
