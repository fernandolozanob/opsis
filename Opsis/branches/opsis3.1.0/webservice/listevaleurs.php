<?
    global $db;
	//debug($_REQUEST);

    $myFrame=PageWebService::getInstance();
    $myFrame->getSort();
    
	$myFrame->getPageFromURL();
	
   	$id_champ_appelant = trim($_REQUEST['type']);

    if(isset($_POST['valeur'])) $_GET['valeur']=$_POST['valeur'];
    $valeur = trim($_GET['valeur']);

    if(isset($_REQUEST["id_lang"])) $id_lang = strtoupper($_REQUEST["id_lang"]);
	else $id_lang=strtoupper($_SESSION['langue']); //Langue de recherche, mis par déft avec la session par LD le 14/09/07

	$entite=$_REQUEST['entite'];

    if($_REQUEST['nbLignes'] != '') $nbLignes = $_REQUEST['nbLignes'];

	$nbDeft=defined("gNbLignesDocListeDefaut")?gNbLignesDocListeDefaut:10;
    if(!isset($nbLignes)) $nbLignes = $nbDeft;
            	
	if (!$id_champ_appelant || !$id_lang)
   	 	throw new Exception (kErrorWSParametreManquant,"006");
    
    
    $desc_champs_appelant = explode("_",$id_champ_appelant);
	switch ($desc_champs_appelant[0]){

				case "C" : //Recherche de documents avec champ DOC (doc_titre,doc_titre_col, etc) et valeur
				case "FT" :
				case "FTE" : 
				case "DOC" : 
					if ($desc_champs_appelant[0]=="DOC") $desc_champs_appelant[0]="C"; //rétrocompatibilité
				    include(libDir."class_chercheDoc.php");
				    $champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
				    if (empty($desc_champs_appelant[1])) $champ="DOC_TITRE"; //appel sans champ (ex: DOC parent)
                    $mySearch=new RechercheDoc();
					$mySearch->useSession=false; //pas de mise en session
					
					$mySearch->setPrefix(null);
					$mySearch->prepareSQL();

					$mySearch->sql="SELECT $champ as VALEUR, ID_DOC as ID_VAL from t_doc";
										
					$mySearch->sqlSuffixe=" group by $champ, id_doc";
                   	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
					
					if (!empty($valeur)) {

					$mySearch->tab_recherche[]=
  								array('FIELD'=>$champ,
				  					  'VALEUR'=>$valeur.($desc_champs_appelant[0]=='C'?'%':''),
				  					  'TYPE'=>$desc_champs_appelant[0],
									  'OP'=>'AND');
					}
					if (!empty($_GET['type_doc'])) {

					$mySearch->tab_recherche[]=
  								array('FIELD'=>'DOC_ID_TYPE_DOC',
				  					  'VALEUR'=>$_GET['type_doc'],
				  					  'TYPE'=>'C',
									  'OP'=>'AND');
					}					
					//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits							  
       				if ((defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits) ) 
						||(!defined('gSeuilAppliqueDroits') && $myUsr->Type<kLoggedDoc)){
							$mySearch->appliqueDroits();
					}      				
				    $mySearch->makeSQL();
					$mySearch->finaliseRequete();       				
				    $sql=$mySearch->sql.$myFrame->getSort();
				    
				   		
				break;     				

                // I.2.c Index issu de T_DOC et d'une table li�e :
                case "CT":
                    $champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);

					$table="t_".strtolower($champ);
					$champ_id="ID_".$champ;
					$champ_doc="DOC_ID_".$champ;
                    if($champ=="ETAT_DOC") $champ_doc="DOC_ID_ETAT";
					else if(($champ=="USAGER_CREA")||($champ=="USAGER_MODIF")) {
						$champ="US_NOM";
						$champ_id="ID_USAGER";
						$table="t_usager";
					}
                    $sql ="SELECT $table.$champ as VALEUR, count(distinct t_doc.ID_DOC) as NB from t_doc,".$table;
                    $sql.=" WHERE t_doc.".$champ_doc." = ".$table.".".$champ_id;
                    $sql.=" AND t_doc.ID_LANG='".simpleQuote($id_lang)."'";
                    if(($table!="t_fonds")&&($table!="t_etat_doc")&&($table!="t_usager")&&($table!="t_media")){
                        $sql.=" AND $table.ID_LANG='".simpleQuote($id_lang)."'";
                    }
                    if($valeur!="") $sql.=" AND $table.$champ LIKE '".simpleQuote($valeur)."%'";
                    $sql.=" ".$sql_droits." GROUP BY $champ";
                  break;


                // I.2.d Index issu de T_DOC_LEX :
                case "LF": // Fulltext
                case "LFC": //Fulltext avec extension %, by LD 04/06/08
                case "L": // Commence par la saisie VAL%
                case "LC": //Saisie dans la chaine %VAL%
                	//SYNTAXE DU CHAMP APPELANT
                	//TYPE_TYPEDESC_TYPELEX_MODE_ROLE  ex: L_GEN_PP_ASSO_AUT,
                	//les champs peuvent être omis mais il faut conserver les "_" , ex: L_GEN___AUT

                    $champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
                    $id_type_desc=$desc_champs_appelant[1];
   					if(!empty($desc_champs_appelant[2])) $type_lex=$desc_champs_appelant[2];
                    
                    
                    if ($desc_champs_appelant[0]=='LF' || $desc_champs_appelant[0]=='LFC') {
                    		$typeRech='FT'; 
                    		if ($desc_champs_appelant[0]=='LFC') {
                    			$valeur=preg_replace("/[%\*]$/","",$valeur);
                    			$extension="*";
                    			}
                    } else {
                    		$typeRech='C';
							$valeur=preg_replace("/[%\*]$/","",$valeur); //suppression du dernier char si % ou *
							$extension="%";
                    }

                    include(libDir."class_chercheLex.php");
                    $mySearch=new RechercheLex();
					$mySearch->useSession=false; //pas de mise en session
					$mySearch->prepareSQL();
					$mySearch->setPrefix(null);
					$mySearch->sqlSuffixe="";
                   	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);

                	$mySearch->sql = "select distinct t_lexique.ID_LEX as ID_VAL, LEX_TERME as VALEUR,  
											t_doc_lex.ID_TYPE_DESC, t_doc_lex.DLEX_ID_ROLE
									from t_lexique left join t_doc_lex ON (t_doc_lex.ID_LEX=t_lexique.ID_LEX)
									where t_lexique.ID_LANG='".simpleQuote($id_lang)."'
									 ";
                	if (!empty($valeur)) $mySearch->tab_recherche[]=
                								array('FIELD'=>'LEX_TERME',
								  					  'VALEUR'=>($desc_champs_appelant[0]=='LC'?'%':'').$valeur.$extension,
								  					  'TYPE'=>$typeRech,
													  'OP'=>'AND');

					if ($type_lex) $mySearch->tab_recherche[]=
												array('FIELD'=>'LEX_ID_TYPE_LEX',
								  					  'VALEUR'=>$type_lex,
								  					  'TYPE'=>'C',
													  'OP'=>'AND');

					if ($id_type_desc) $mySearch->tab_recherche[]=
												array('FIELD'=>'ID_TYPE_DESC',
													  'VALEUR'=>$id_type_desc,
													  'TYPE'=>'C',
													  'OP'=>'AND');
					if ($dlex_id_role) $mySearch->tab_recherche[]=
												array('FIELD'=>'DLEX_ID_ROLE',
													  'VALEUR'=>$dlex_id_role,
													  'TYPE'=>'C',
													  'OP'=>'AND');
		



				    $mySearch->makeSQL();
					$mySearch->finaliseRequete();

                    $sql=$mySearch->sql." GROUP BY ID_VAL, VALEUR, ID_TYPE_DESC, DLEX_ID_ROLE
									ORDER BY VALEUR" ;
     
                  break;



                // I.2.e Index issu de T_DOC_VAL :
                case "V":
                case "VF":
                case "VFC":
                case "VC":
                    $champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
                    $id_type_val = $desc_champs_appelant[1];
                    include(libDir."class_chercheVal.php");


                    if ($desc_champs_appelant[0]=='VF' || $desc_champs_appelant[0]=='VFC') {
                    		$typeRech='FT'; 
                    		if ($desc_champs_appelant[0]=='VFC') {
                    			$valeur=preg_replace("/[%\*]$/","",$valeur);
                    			$extension="*";
                    			}
                    } else {
                    		$typeRech='C';
							$valeur=preg_replace("/[%\*]$/","",$valeur); //suppression du dernier char si % ou *
							$extension="%";
                    }

     				$mySearch=new RechercheVal();
                    $mySearch->useSession=false;
                    $mySearch->prepareSQL();
    				$mySearch->setPrefix(null);
                    $mySearch->sql = "SELECT ID_VAL as ID_VAL,VALEUR as VALEUR FROM t_val WHERE ID_LANG=".$db->Quote($id_lang);
					$mySearch->sqlSuffixe = " GROUP BY ID_VAL, VALEUR";
                   	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);

                	if (!empty($valeur)) $mySearch->tab_recherche[]=array('FIELD'=>'VALEUR',
								  					  'VALEUR'=>($desc_champs_appelant[0]=='VC'?'%':'').$valeur.$extension,
								  					  'TYPE'=>$typeRech,
													  'OP'=>'AND');
                	
                									  
					$mySearch->tab_recherche[]=array('FIELD'=>'VAL_ID_TYPE_VAL',
													  'VALEUR'=>$id_type_val,
													   'TYPE'=>'C',
													   'OP'=>'AND');

                    $mySearch->makeSQL();
                    $mySearch->finaliseRequete();
                    $sql=$mySearch->sql." ORDER BY VALEUR" ;

                    $urlparams=",".$db->Quote($id_type_val);

    

                  break;

                case "P" : // recherche de personne
                	include(libDir."class_cherchePers.php");
                	$mySearch=new RecherchePersonne();
                	$mySearch->useSession=false; //pas de mise en session
                	$mySearch->setPrefix(null);
                	$mySearch->prepareSQL();
					$mySearch->sqlSuffixe='';
                	$mySearch->sql="select t_personne.ID_PERS as ID_VAL, ".$db->Concat('PERS_PRENOM',"' '",'PERS_PARTICULE',"' '",'PERS_NOM')." as VALEUR
									, count(ID_DOC) as NB
									from t_personne left join t_doc_lex on (t_personne.ID_PERS=t_doc_lex.ID_PERS)
									WHERE 1=1";
                   	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
                	$mySearch->tab_recherche[]=array('FIELD'=>'PERS_NOM,PERS_PRENOM',
								  					  'VALEUR'=>$valeur,
								  					  'TYPE'=>'FT',
													  'OP'=>'AND');
													  											  
					$mySearch->tab_recherche[]=array('FIELD'=>'ID_LANG',
								  					  'VALEUR'=>$id_lang,
								  					  'TYPE'=>'C',
													  'OP'=>'AND');
    				if (isset($_REQUEST["type_lex"])) $type_lex=$_REQUEST["type_lex"];
    				else $type_lex=$desc_champs_appelant[1]; //type lex
					if (!empty($type_lex)) {
					$mySearch->tab_recherche[]=
							array('FIELD'=>'PERS_ID_TYPE_LEX',
								  'VALEUR'=>$type_lex,
								  'TYPE'=>'C',
								  'OP'=>'AND');
					}
				    $mySearch->makeSQL();
					$mySearch->finaliseRequete();
				    $sql=$mySearch->sql." GROUP BY VALEUR, ID_VAL" ;

                break;


                case "REF" : // récupération de valeurs depuis une table de référence;
                	$tbl=$_REQUEST['tbl'];
                	$cols=$db->MetaColumns($tbl);
                	$cols=array_keys($cols);
					$affich_nb=0;
                	if ($cols[0]) $chmp1=$cols[0];
                	if ($cols[1]) $chmp2=$cols[1]; else $chmp2=$cols[0];
                	if ($tbl=='t_diffuseur') $chmp1=$chmp2; //Cas spécial : cette table n'a pas d'ID !

                	$sql="SELECT ".$chmp1." as ID_VAL, ".$chmp2." as VALEUR FROM ".$tbl;
					if (!empty($id_lang)) $sql.=" WHERE ID_LANG=".$db->Quote($id_lang);

			    break;

                

            }

  		//debug($sql);
  		
      	global $db;
      
	
		$myFrame->initPager($sql,$nbLignes,$myFrame->page,null,60);

        $param["nbLigne"] = $nbLignes;
        $param["page"] = $myFrame->page;
        $param["pager_link"]=$myPage->PagerLink;
        $param["votre_recherche"]=$mySearch->etape; //on utilise htmlentities car c'est juste de l'affichage
        $param["urlparams"]=$myFrame->addUrlParams();
        $param["frameurl"]=frameUrl;
			
		if($nbLignes=="all") $param["offset"] = "0";
		else $param["offset"] = $nbLignes*($myFrame->page-1);
        $param["nbRows"]=$myFrame->found_rows;
        $myFrame->addParamsToXSL($param);
 		$xsl=getSiteFile("designDir",'listeValeursWebService.xsl');
 		if (!file_exists($xsl)) throw new Exception (kErrorWSXSLManquant,"004");
        $myFrame->afficherListe('val',$xsl);
?>
