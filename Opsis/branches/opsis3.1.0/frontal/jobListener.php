<?php

require_once(modelDir.'model_engine.php');

global $db;
$modules=$db->Execute('SELECT * FROM t_module')->GetRows();

$array_engine=array();
$array_busy_drives=array(); // Tableau des drives utilisés

// *** Début fonctions jobListener ***
function lancerJob($module,$in_xml,$module_engines,&$engines_slot_dispo)
{
	//test de la date de lancement 
	//file_get_contents(jobInDir.'/'.$module.'/'.basename($in_xml));
	//jobInDir.'/'.$module.'/'.basename($in_xml);
	//;
	//preg_match();
	if (preg_match("|<date_lancement>(.*)</date_lancement+>|i",file_get_contents(jobInDir.'/'.$module.'/'.basename($in_xml)),$matches))
	{
		$date_lancement=$matches[1];
		if (time()<strtotime($date_lancement))
		{
			echo 'Pas de lancement du job '.$in_xml.'. Date de lancement : '.$date_lancement."\n";
			return false;
		}
	}

	// verifier que le job n'est pas déjà lance (jobRunDir)
	if (file_exists(jobRunDir.'/'.$module.'/'.stripExtension(basename($in_xml)).'.pid'))// presence du ficheir pid
	{
		$data_pid=explode("\n",file_get_contents(jobRunDir.'/'.$module.'/'.stripExtension(basename($in_xml)).'.pid'));
		$pid=$data_pid[0];
		$id_engine=$data_pid[1];
		
		foreach($module_engines as $eng)
		{
			if ($eng->getIdEngine()==$id_engine)
			{
				$file_xml=jobRunDir.'/'.$module.'/'.$eng->getNomServeur().'/'.basename($in_xml);
				if (file_exists($file_xml))
				{
					// creation du fichier de cancel
					file_put_contents(jobCancelDir.'/'.$module.'/'.basename($in_xml),'');
					// cancel du job
					echo kPHPcli.' frontal.php jobCancel '.$module.' '.basename($in_xml),"\n";
					shell_exec(kPHPcli.' frontal.php jobCancel '.$module.' '.basename($in_xml));
					// suppression du fichier de cancel car le job est relance
					unlink(jobCanceledDir.'/'.$module.'/'.basename($in_xml));
				}
			}
		}
	}
	
	// on garde uniquement les engine ou on peu encore ajouter des jobs
	//for($i=0;$i<count($module_engines);$i++)
	foreach($module_engines as $key=>$eng)
	{
		if ($engines_slot_dispo[$eng->getIdEngine()]<=0)
			unset($module_engines[$key]);
	}
	//choix de l'engine au hasard
	$choosen_engine=$module_engines[array_rand($module_engines)];
	
	$engines_slot_dispo[$choosen_engine->getIdEngine()]--;
	
	if(eregi('WINNT',PHP_OS)){
		$cmd_process=kPowershellPath.' -command "Start-Process \''.kPHPcli.'\' \''.kCheminLocalSurServeur.'/frontal.php\',\'jobProcess\',\''.$module.'\',\''.basename($in_xml).'\',\''.$choosen_engine->getIdEngine().'\' -NoNewWindow -passthru"';
	}else{
		$cmd_process=kPHPcli.' frontal.php jobProcess '.$module.' '.basename($in_xml).' '.$choosen_engine->getIdEngine().' > /dev/null 2>&1 & echo $!';
    }
	
	//$pid=shell_exec($cmd_process);
	$pipe_descriptor=array
		(
			0 => array('pipe', 'r'),
			1 => array('file',dirname(jobLogFile).'/console.out', 'w'),
			2 => array('file',dirname(jobLogFile).'/console.err', 'w')
		);
	$proc=proc_open($cmd_process,$pipe_descriptor,$exec_pipes);
    //$pid = proc_get_status($proc)['pid']; // Ne fonctionne pas en php5.3
    $proc_get_status = proc_get_status($proc);
    $pid = $proc_get_status['pid'];
	
	trace($pid.' '.$cmd_process);
	file_put_contents(jobRunDir.'/'.$module.'/'.stripExtension(basename($in_xml)).'.pid',$pid);
	file_put_contents(jobRunDir.'/'.$module.'/'.stripExtension(basename($in_xml)).'.pid',$choosen_engine->getIdEngine(),FILE_APPEND);
	echo $pid.' '.$cmd_process,"\n";
	
	return true;
}

function checkBusyDrive($in_xml,&$array_busy_drives)
{
    $params=file_get_contents($in_xml);
    if(preg_match("|<drive>([^<]*)</drive+>|i",$params,$matches)) {
        $drive=$matches[1];
        if(in_array($drive,$array_busy_drives)){
            return true;
        }else{
            $array_busy_drives[]=$drive;
            return false;
        }
    }
    return false;
}

function updateBusyDrive($in_xml,&$array_busy_drives)
{
    $params=file_get_contents($in_xml);
    if(preg_match("|<drive>([^<]*)</drive+>|i",$params,$matches)) {
        if(!in_array($matches[1],$array_busy_drives)) $array_busy_drives[]=$matches[1];
    }
}

// *** Fin fonctions jobListener ***

foreach($modules as $mod)
{
	$array_engine[$mod['MODULE_NOM']]=Engine::getAllModuleEngines($mod['ID_MODULE']);
}

// taches d'annulation
foreach($array_engine as $key=>$module)
{
	if (file_exists(jobCancelDir.'/'.$key))
	{
		$fichiers_cancel=list_directory(jobCancelDir.'/'.$key,array('xml'));
		foreach ($fichiers_cancel as $fichier)
		{
			$cmd_cancel=kPHPcli.' frontal.php jobCancel '.$key.' '.basename($fichier);
			shell_exec($cmd_cancel);
			trace($cmd_cancel);
		}
	}
}

// taches de suppression
foreach($array_engine as $key=>$module)
{
	if (file_exists(jobDeleteDir.'/'.$key))
	{
		$fichiers_delete=list_directory(jobDeleteDir.'/'.$key,array('xml'));
		foreach ($fichiers_delete as $fichier)
		{
			$cmd_delete=kPHPcli.' frontal.php jobDelete '.$key.' '.basename($fichier);
			shell_exec($cmd_delete);
			trace($cmd_delete);
		}
	}
}

    $infos['date']=date('Y-m-d H:i:s');

foreach ($array_engine as $module=>$arr_engines)
{
	$nb_slot_dispo=0;
	$engines_slot_dispo=array();
	echo '---------- ',$module,' ----------',"\n";
	
    $eng_infos = array();
	foreach ($arr_engines as $k=>$eng)
	{
		if (file_exists(jobRunDir.'/'.$module.'/'.$eng->getNomServeur()))
		{
			$fichiers_run=list_directory(jobRunDir.'/'.$module.'/'.$eng->getNomServeur());
			
			$nb_slot_occupe=count($fichiers_run);
			
            // VP 19/11/13 : Extraction des drives utilisés (module Backup)
            if($module=='backup')
            {
                for($i=0;$i<count($fichiers_run);$i++)
                {
                    updateBusyDrive($fichiers_run[$i],$array_busy_drives);
                }
            }
            
			// test nb max jobs
			if ($eng->getNbMaxJobs()>$nb_slot_occupe)
			{
				$engines_slot_dispo[$eng->getIdEngine()]=$eng->getNbMaxJobs()-$nb_slot_occupe;
				$nb_slot_dispo+=$eng->getNbMaxJobs()-$nb_slot_occupe;
			}
			else
				$engines_slot_dispo[$eng->getIdEngine()]=0;
            
            // infos module
            $eng_infos[$k]['module']=$module;
            $eng_infos[$k]['engine']=$eng->getNomServeur();
            $eng_infos[$k]['nb_max_jobs']=$eng->getNbMaxJobs();
            $eng_infos[$k]['file']=$fichiers_run;
            for($i=0;$i<count($fichiers_run);$i++)
            {
                $pid_file=jobRunDir.'/'.$module.'/'.stripExtension(basename($fichiers_run[$i])).'.pid';
                if (file_exists($pid_file))
                {
                    $data_pid=explode("\n",file_get_contents($pid_file));
                    $eng_infos[$k]['pid'][$i]=$data_pid[0];
                    if(checkProcessPid($data_pid[0])) {
                        $eng_infos[$k]['status'][$i]='up';
                    }
                }
            }
		}
		else
		{
			$nb_slot_dispo+=$eng->getNbMaxJobs();
			$engines_slot_dispo[$eng->getIdEngine()]=$eng->getNbMaxJobs();
		}
	}
    if(count($eng_infos)) $infos['run'][]=$eng_infos;

	if (file_exists(jobInDir.'/'.$module))
	{
		$fichiers_in=list_directory(jobInDir.'/'.$module,array('xml'));
		sort($fichiers_in);
		
		$nombre_fichiers_in=count($fichiers_in);
        
        $infos['in'][]=array('module'=>$module,'nb_in'=>$nombre_fichiers_in);
        
        if($nb_slot_dispo>0) {
		
            // si on a plus de slot dispo
            if ($nombre_fichiers_in<=$nb_slot_dispo)
            {
                //on traite tous les fichiers dans in
                for($i=0;$i<count($fichiers_in) && $nb_slot_dispo>0;$i++)
                {
                    // VP 19/11/13 : Vérification que le drive est libre (module Backup)
                    if($module=='backup' && checkBusyDrive($fichiers_in[$i],$array_busy_drives)){
                        //echo "busy...";
                        continue;
                    }
                    //echo $fichiers_in[$i],"\n";
                    $ok=lancerJob($module,$fichiers_in[$i],$arr_engines,$engines_slot_dispo);
                    if($ok) {
                        $nb_slot_dispo--;
                    }
                }
            }
            else
            {
                //liste de clients.
                $clients=array();
                $client_job_count=array();
                
                foreach($fichiers_in as $file)
                {
                    $arr_filename_param=explode('_',$file);
                    
                    if (!in_array($arr_filename_param[2],$clients))
                    {
                        $clients[]=$arr_filename_param[2];
                        $client_job_count[$arr_filename_param[2]]=0;
                    }
                }
                
                // on recupere le nombre de jobs lancés par un client
                $fichiers_run=list_directory(jobRunDir.'/'.$module,array('xml'));
                foreach($fichiers_run as $file)
                {
                    $arr_filename_param=explode('_',basename($file));
                    
                    if (!isset($client_job_count[$arr_filename_param[2]]))
                        $client_job_count[$arr_filename_param[2]]=1;
                    else
                        $client_job_count[$arr_filename_param[2]]++;
                }
                
                
                //shuffle($clients);
                //var_dump($clients);
                // array contenant les xml de jobs déjà lancés
                $jobs_started=array();
                
                for($i=0;$i<count($fichiers_in) && $nb_slot_dispo>0;$i++)
                {
                    // on trie le clients en fonction de leur nombre de jobs lancés jobs lancés par ordre croissant
                    asort($client_job_count,SORT_NUMERIC);
                    
                    $min_jobs=min($client_job_count);
                    $client_min_jobs=array();
                    
                    // recherche de clients qui on un minimum de jobs
                    foreach ($client_job_count as $clt=>$compte)
                    {
                        if ($min_jobs==$compte)
                            $client_min_jobs[]=$clt;
                    }
                    
                    shuffle($client_min_jobs);
                    
                    foreach($client_min_jobs as $clt)
                    {
                        // echo 'client : ',$clt,"\n";
                        for($j=0;$j<count($fichiers_in) && $nb_slot_dispo>0;$j++)
                        {
                            $arr_filename_param=explode('_',$fichiers_in[$j]);
                            
                            if ($clt==$arr_filename_param[2] && !in_array($fichiers_in[$j],$jobs_started)) // on verifie que le job n'a pas déja été lancé
                            {
                                // VP 19/11/13 : Vérification que le drive est libre (module backup)
                                if($module=='backup' && checkBusyDrive($fichiers_in[$j],$array_busy_drives)) {
                                    //echo "busy...";
                                    continue;
                                }
                                // lancement des jobs du client
                                $ok=lancerJob($module,$fichiers_in[$j],$arr_engines,$engines_slot_dispo);
                                if($ok) {
                                    $jobs_started[]=$fichiers_in[$j];
                                    $nb_slot_dispo--;
                                    // le client a un job en plus en cours
                                    $client_job_count[$clt]++;
                                }
                            }
                        }
                        
                        if ($nb_slot_dispo<=0)
                            break;
                    }
                }
            }
		}
	}
}
    //print_r($infos);
    //echo tab2xml($infos);
    file_put_contents(kBackupInfoDir.'/jobListenerInfo.xml', tab2xml($infos));
?>
