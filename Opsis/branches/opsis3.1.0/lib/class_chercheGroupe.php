<?php
require_once(libDir."class_cherche.php");


class RechercheGroupe extends Recherche {

	function __construct() {
	  	$this->entity='';
     	$this->name=kGroupe;
     	$this->prefix='g';
     	$this->sessVar='recherche_GRP';
		$this->tab_recherche=array();
		$this->sqlSuffixe=' GROUP BY  g.ID_GROUPE';
		$this->useSession=true;
	}


    function prepareSQL(){
		global $db;
		$this->sql = "
  select g.ID_GROUPE, g.GROUPE, count(distinct t_usager_groupe.ID_USAGER) NBUSAGERS,
  		count(distinct t_groupe_fonds.ID_FONDS) NBFONDS FROM t_groupe g LEFT JOIN t_usager_groupe ON g.ID_GROUPE=t_usager_groupe.ID_GROUPE AND t_usager_groupe.ID_USAGER in (select id_usager from t_usager) LEFT JOIN t_groupe_fonds ON g.ID_GROUPE=t_groupe_fonds.ID_GROUPE
		 WHERE 1=1 ";
        $this->etape="";
    }



}
?>