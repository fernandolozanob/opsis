<?php

class Upload {

var $arrFiles; // Fichier à downloader (a priori vide)

var $processingUploadUrl; //Url contenant les traitements POST upload
var $mode; //Type de media chargé

var $innerHTML; //HTML pouvant être injecté dans la page, entre les balises form.
				//Ceci permet d'ajouter des champs

var $arrParams;


function __construct() {

	require_once (libDir."class_page.php");
	$myPage=Page::getInstance();

	$this->style="hitech"; //design par défaut
	$this->mode="materiel"; //mode par défaut

	//$this->uploadUrl=kCheminHttp."/".$myPage->getName()."?urlaction=processUpload".$myPage->addUrlParams(null,false);
	$this->processingUploadUrl="../".$myPage->getName()."?urlaction=finUpload".$myPage->addUrlParams(null,false);

}

function updateParamFile() {
	//ld 26 11 08 : transfo en chiffres si tailles de type xK, xM, etc
	$this->arrParams['arrParams']["maxFileSize"]=str_replace(array("K","M","G"),array("000","000000","000000000"),$this->arrParams['arrParams']["maxFileSize"]); // récup du param PHP.ini
	$this->arrParams['arrParams']["maxTotalSize"]=str_replace(array("K","M","G"),array("000","000000","000000000"),$this->arrParams['arrParams']["maxTotalSize"]); // récup du param PHP.ini


$content="package XUploadConfig;
use strict;
use Exporter ();
@XUploadConfig::ISA    = qw(Exporter);
@XUploadConfig::EXPORT = qw(\$c);
use vars qw( \$c );

\$c=
{
 # Directory for temporary using files
 temp_dir        => '".kServerTempDir."',  # Enter your temp dir path here

 # You can use different upload modes for different upload forms
 # Specify upload mode in xmode hidden input in upload_form.html
 modes =>
 {
   upload =>
   {
      # Directory for uploaded files
      target_dir      => '".$this->arrParams['arrParams']['destinationDir']."', # enter your target dir here

      # URL to send all input values from upload page
      url_post        => '', # post URL should be here

      # Max number of upload fields
      max_upload_files => ".$this->arrParams['arrParams']['maxFileCount'].",

      # Minimum/Maximum Total upload size in Kbytes (leave empty or zero to disable)
      min_upload_size => 0,
      max_upload_size => ".$this->arrParams['arrParams']['maxTotalSize'].",

      # Minimum/Maximum upload Filesize in Kbytes (leave empty or zero to disable)
      min_upload_filesize => 0,
      max_upload_filesize => ".$this->arrParams['arrParams']['maxFileSize'].",

      # Allowed file extensions delimited with '|'
      # Use '.*' to allow all extensions
      #ext_allowed     => 'jpg|jpeg',
      ext_allowed     => '".$this->arrParams['arrParams']['strFilesFormat']."',

      # Not Allowed file extensions delimited with '|'
      # Leave it blank to disable this filter
      ext_not_allowed => 'exe|msi|bat|com|inf|js',
   },
 },

 # Max length of uploaded filenames(without ext). Longer filenames will be cuted.
 max_name_length => 64,

 # Type of behavior when uploaded file already exist on disc. Available 3 modes: Rewrite/Rename/Warn
 copy_mode       => '".(defined("gUploadModeRewrite")?gUploadModeRewrite:'Rewrite')."',

 # Use ajax mode instead of streaming
 # Slow down progress refresh, but may help in some situations
 ajax_mode       => 0,

 # Filename required mask (use standard perl regexp)
 # Extension is not using here (e.g. will use 'song123' for 'song123.mp3')
 # Leave empty if you don't need this custom filename validation
 # Examples: '^\w+$' - alphanumeric only, '^photo' - starting with 'photo'
 filaname_mask   => '',

 # Filename rename mask (use standard perl regexp)
 # Removing all characters except these from the mask
 # Extension is not using here (e.g. will use 'song123' for 'song123.mp3')
 # Leave empty if you don't need this custom filename renaming
 # Examples: 'a-zA-Z' - will keep letters only, '\w' - will keep alphanumeric only
 filename_rename_mask   => '',

 # Allow relative folder path for uploading files from HTML form ext_folder field
 # Pathes with '..' are not allowed
 # 0=no,1=yes. [Security Warning!]
 allow_ext_folder => 1,

 # Password for upload (optional)
 # Allow uploads only with correct password. Add '<input type=\"password\" name=\"upload_password\">' to your upload_form.html to use this feature.
 # Leave empty to disable
 upload_password => '',

 # Email field required (optional)
 # Force users to enter their email address. Upload notification to that email will be sent.
 # Leave empty to disable, set to 1 to enable
 email_required => '',

 # Logfile name
 uploads_log => 'logs.txt',

 # Enable users to add descriptions to files
 enable_file_descr => 1,

 # Uploaded files expiration time # days
 # Leave blank or zero to disable
 # WARNING: setting up this value could remove your old files older than X days
 uploaded_files_lifetime => 0,

 # Time to keep temp upload files on server, sec (24 hours = 86400 seconds)
 temp_files_lifetime => 86400,

 # IP restrictions
 # Examples: '^(10\.0\.0\.182)$' - allow only 10.0.0.182, '^(10\.0\.1\.125|10\.0\.0\.\d+)$' - allow 10.0.1.125 & 10.0.0.* (*=number is wildcard)
 # Use '^.*$' for no restrictions
 ip_allowed => '^.*$',

##### Email confirmation #####
### Leave all fields blank OR comment them if you don't want to send confirmations

 # Path to sendmail
 sendmail_path      => '/usr/sbin/sendmail',

 # Email that confirmations will be sent to
 ".(defined("gUploadSendMail")?"":"#")."confirm_email      => '".gMail."',

 # This email will be in \"From:\" field in confirmation emails
 confirm_email_from => '".gMail."',

 # Subject for email notification
 email_subject      => \"Xupload: new file(s) uploaded\",

##### Custom error messages #####

 msg => { upload_size_big   => \"".kXUploadTotalExceeded."\",
          upload_size_small => \"".kXUploadTotalMini."\",
          file_size_big     => \"".kXUploadFileExceeded."\",
          file_size_small   => \"".kXUploadFileMini."\",
          no_temp_dir       => \"".kXUploadNoTempDir."\",
          no_target_dir     => \"".kXUploadNoTargetDir."\",
          no_templates_dir  => \"".kXUploadNoTemplate."\",
          transfer_complete => \"".kXUploadTransferComplete."\",
          transfer_failed   => \"".kXUploadUploadFailed."\",
          null_filesize     => \"".kXUploadNullFile."\",
          bad_filename      => \"".kXUploadBadFile."\",
          bad_extension     => \"".kXUploadBadExt."\",
          too_many_files    => \"".kXUploadTooMany."\",
          already_exist     => \"".kXUploadAlreadyExist."\",
          saved_ok          => \"".kXUploadSaved."\",
          wrong_password    => \"".kXUploadWrongPass."\",
          ip_not_allowed    => \"".kXUploadUnwantedIp."\",
        },
};

1;
";

$prmFile=fopen(baseDir."/UPLOAD2/XUploadConfig.pm",'wb+');
if ($prmFile && fwrite($prmFile,$content)) fclose($prmFile);
else echo "Pb mise à jour paramètres";

}



function renderHTML($print=1) {

$html=
"<Style>
#lightbox{
	xdisplay:none;
	position: absolute;
	top:50%;
	left:-9999px;
	z-index:9999;
	width:390px;
	height:320px;
	margin:-200px 0 0 -200px;
    border-left: 2px solid #d4d0c8;
    border-top: 2px solid #d4d0c8;
    border-right: 2px solid #404040;
    border-bottom: 2px solid #404040;
	background:#FFF;
	text-align:left;
	padding-top: 20px;
}
#lightbox[id]{position:fixed;}
#overlay{
	xdisplay:none;
	position:absolute;
	top:0;
	left:-9999px;
	width:100%;

	z-index:5000;
	background-color:#444;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
#overlay[id]{position:fixed;}
#lhdr{position:absolute; top: 0px; left: 0px;background: #2c4887;font: bold 13px Arial;width:388px;height:18px;padding-left:2px;color:#FFFFFF;}
#close{position:absolute; top: 2px; right: 2px; cursor:pointer;}

FIELDSET {width: 400px; background-color:#F0F0F0; border: 1px solid #888888; font: 13px Arial; text-align: left; -moz-border-radius:16px;}
LEGEND   {background-color: #f3f3f3; border: 1px solid #b3b3b3; font: bold 13px Arial; padding: 2px;}
.div1, .file_list {text-align: left; color: #484B50; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;}
.div1 {width: 100%; padding: 10px;}
.upload_input input {font: 12px Arial; border: 1px solid #636363; margin-top: 1px;}
.file_list {width: 100%; border: 1px solid #434343; background-color: #ffffff;}
.file_list input {font: 12px Arial; border: 1px solid #c3c3c3; margin-top: 1px;}
.myForm {font: 13px 'Trebuchet MS',Helvetica,sans-serif;font-weight:bold; border: 1px solid #636363; margin-top: 1px;cursor:pointer;}
.fdescr {width: 300px;}
.xfname  {color: #333;}
.xdescr  {color: #aaa;}

/* CSS below required for Inline3 mode */
.data {border-collapse:collapse; background-color: #e9e9e9; width: 400px; margin-bottom: 3px;}
.data TR TD {border: 1px solid #a3a3a3; font: 12px Arial;}
.bar1 {border: 1px solid #a3a3a3; background-color: #f6f6f6; text-align: left; vertical-align: middle; height: 20px; width: 98%;}
.bar2 {width: 1%; background-color: #c9c9c9; height: 20px; text-align: left;}
.message {width: 400px; border: 1px solid #909090; font: 11px Arial; margin-top: 5px; text-align: left; padding-left: 3px; background-color: #f9f9f9;}
.myLink  {font: bold 13px Arial; color: #303030; text-decoration: none;}
.btn     {font: 12px Arial; border: 1px solid #a3a3a3; background-color: #e9e9e9; margin-top: 3px; margin-bottom: 5px;}
</Style>
<Center>
<form name=\"F1Upload\" enctype=\"multipart/form-data\" action=\"UPLOAD2/upload_".(strpos(PHP_OS,'WINNT')?"win":"unix").".cgi?upload_id=\" method=\"post\" onSubmit=\"return StartUpload(this);\">
<input type=\"hidden\" name=\"xmode\" value=\"upload\">
<input type='hidden' name='url_post' value='".$this->processingUploadUrl."' />

".$this->innerHTML."

<div id=\"upload_form\">
<FIELDSET><legend>".kUploadSelectionFichiers."</legend>
<table cellpadding=0 cellspacing=0 class=\"div1\" id=\"div1\"><tr><td>
<div id=\"slots\" class='upload_input'>
".(empty($this->arrParams['arrParams']['ext_folder'])?"":"<input type='hidden' name='ext_folder' value='".$this->arrParams['arrParams']['ext_folder']."'/>")."
<br><input name=\"file_0\" type=\"file\" onChange=\"checkExt(this);addUploadSlot();\" size=\"40\" ><br>
</div>
".($this->arrParams['arrParams']['maxFileCount']=='1'?"":"
<!--<input type=\"button\" id=\"x_add_slot\" value=\"".kAjouterFichier."\" class=\"myForm\" onClick=\"addUploadSlot();\"><br>-->")."<br>
<Table class=\"file_list\"><TR><TD nowrap>
<b><font id='x_max_files'>1</font> ".kFichiers."<font id='x_max_size'></font> maximum</b><br>
<div id=\"files_list\" style=\"font: 12px Arial;\"></div>
</TD></TR></Table>
<script>document.write('<script src=\"' + document.F1Upload.action+'&mode=settings' + '\" type=\"text/javascript\"><\/script>');</script>
<input type=\"hidden\" name=\"pbmode\" value=\"inline\" id=\"inline\">
<br>
<center><input type=\"submit\" value=\"".kUploadStart."\" class=\"myForm\" onclick='need2confirm=false'></center>
<br>
</td></tr></table>
</FIELDSET>
</div>

<input type=\"hidden\" name=\"css_name\" value=\"".$this->style."\">
<input type=\"hidden\" name=\"tmpl_name\" value=\"".$this->style."\">
</form>

</Center>

<iframe src=\"javascript:false;\" name=\"xupload\" style=\"position:absolute;left:-9999px;top:0px\"></iframe>
<script src=\"".coreUrl."/upload/xupload_".(strpos(PHP_OS,'WINNT')?"win":"unix").".js\"></script>
<script>InitUploadSelector();</script>
";

	if ($print==1) echo $html; else return $html;

}

	// MS getFileInfo supprimée, remplacée par l'utilisation de la class_matInfo & getMatInfo
	// findMPEGTcIN supprimée également


	static function writedata($filename, $filecontents) {
			if (!$handle = fopen($filename, 'wb')) {
			return('Cannot open file ('.$filename.')');
			}

			if (!fwrite($handle, $filecontents)) {
			return('Cannot write to file ('.$filename.')');
			}
			fclose($handle);
			return true;
	}

}
?>
