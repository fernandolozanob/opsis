<?php
    
    require_once(libDir.'/Math/BigInteger.php');
    
    class RSA
    {
        protected $modulus; // n
        protected $privateExponent; // d
        protected $publicExponent; // e
        
        public function __construct($modulus = NULL,$privateExponent = NULL,$publicExponent = NULL)
        {
            if(isset($modulus) && is_int($modulus)) { $this->modulus = new Math_BigInteger($modulus); }
            if(isset($privateExponent) && is_int($privateExponent)) { $this->privateExponent = new Math_BigInteger($privateExponent); }
            if(isset($publicExponent) && is_int($publicExponent)) { $this->publicExponent = new Math_BigInteger($publicExponent); }
        }
        
        public function crypt($message){
             if($this->canCrypt()) {
                $sb = str_split($message);

                $idPartToCrypt = array();
                foreach($sb as $token){
                        $idpart = new Math_BigInteger(ord($token));
                        array_push($idPartToCrypt, $idpart);
                }
                
                $idPartCrypted = $this->RSACrypt($idPartToCrypt);
                
                $st = implode('-', $idPartCrypted);

                return $st;
            } else {
                trace('ERROR : Data n and e not defined to crypt message.');
            }
        }
        
        public function decrypt($message){
            if($this->canDecrypt()) {
                $st = explode('-',$message);
                $idPartToDecrypt = array();
                foreach($st as $token){
                       $idpart = new Math_BigInteger($token);
                       array_push($idPartToDecrypt, $idpart);
                }

                $idPartDecrypted = $this->RSADecrypt($idPartToDecrypt);
                $sb = '';
                foreach($idPartDecrypted as $bigInteger) {
                       $temp = new Math_BigInteger();
                       $temp = $bigInteger;
                       $aChar = chr($temp->toString());
                       $sb = $sb.$aChar;
                }
                return $sb;
            } else {
                trace('ERROR : Data n and d not defined to decrypt message.');
            }
       }
       
       
       private function RSACrypt($bigIntList) {
             $res = array();
             for ($i = 0; $i < count($bigIntList); $i++) {
                 $temp = new Math_BigInteger();
                 $temp = $bigIntList{$i};
                 $res[$i] = $temp->modPow($this->publicExponent,$this->modulus);
             }
             return $res;
       }
       
       private function RSADecrypt($bigIntList) {
             $res = array();
             for ($i = 0; $i < count($bigIntList); $i++) {
                 $temp = new Math_BigInteger();
                 $temp = $bigIntList{$i};
                 $res[$i] = $temp->modPow($this->privateExponent,$this->modulus);
             }
             return $res;
       }
       
       private function canCrypt() {
           $test = false;
           if (isset($this->publicExponent) && isset($this->modulus)) {
               $test = true;
           }       
           return $test;
       }
       
       private function canDecrypt() {
           $test = false;
           if (isset($this->privateExponent) && isset($this->modulus)) {
               $test = true;
           }       
           return $test;
       }
    }
    
    ?>