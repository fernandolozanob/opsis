<?php


class jsTree {

	var $arrNodes; //Tableau des noeuds. Contient la structure, type de noeuds, etc.

	var $objSearch; //Objet de type recherche

	var $output;

	var $JSlink; //Fonction JS ou URL appelée sur les liens de chaque mot
	var $btnEdit; //Valeur du bouton edit
	var $id_input_result;
	var $id_input_form;

	function __construct($searchobject,$formName,$arr_values=array()) {
		$this->objSearch=$searchobject;
		$this->output='';
		$this->id_input_form=$formName;
		$this->fullTree = false ; 
		$this->checkboxTree = false ; 
		$this->type_field = '' ; 
		$this->arr_values= $arr_values ; 
		$this->styleRootName = 'font-size:14px;font-weight:bold;z-index:99;';
	}

	function setObject($searchobject) {
		$this->objSearch=$searchobject;
	}

	function makeRoot() { //Constitue la racine et les têtes de rubriques (niveau 1)

		$arrChildren=$this->objSearch->getChildren(0,$this->arrNodes,$this->fullTree); //récupération niveau 1
		//Ce noeud primaire n'est utilisé que lorsque l'on a displayRoot=true
		$this->arrNodes['elt_0']=array("id"=>0,
							"id_pere"=>-1,
							"terme"=>$this->objSearch->getRootName(),
							"open"=>1,
							"icone"=>'',
							"valide"=>false,
							"context"=>false,
							"checkbox"=>false,
							"nb_children"=>count($arrChildren),
							 "loaded"=>true,
							 'generateBigTree' => false
							);
	}

    
    // après affichage arbre, on affiche un noeud particulier. >> cherche tous les pères, on les loads (appel ajax donc), et apres on les ouvre en JS donc 
	function revealNode($id,$style='highlight',$withBrother=false) {
		$this->objSearch->getNode($id,$this->arrNodes,$style,$withBrother);
		echo "<script>";
		echo "var myArray = [];";
		echo "var id_hover = '".$id."';";
		foreach ($this->arrNodes as $idx=>$item) {
			if (!empty($item['open']) && ($item['open'] == 1) && $item['id']!= '0') {
				echo "myArray.push('".$item['id']."');";
			}				
		}
		//charger noeud avant de le hover
		echo "jQuery('#".$this->id_div_tree."').bind('ready.jstree', function(e, data) {
				 jQuery('#".$this->id_div_tree."').jstree(true).load_node(myArray, function (){
					 this.open_node(myArray);
					 jQuery('#".$this->id_div_tree."').jstree(true).hover_node(id_hover);
					 hover_contextmenu();
				 });";
		echo "});";
		echo "</script>";
	}

	function openNode() {
		echo '[';
		$cpt= 0;
			foreach ($this->arrNodes as $idx=>$item) {
				$cpt++;
				$id = $item['id'];
				$parent_id=$item['id_pere'];
				$terme=$item['terme'];
				echo "{";
				if (!empty($item['nb_children']) && ($item['nb_children'] > 0)) { //pas d'enfant et 0/1 terme asso
				echo '"children" : true,';
				}
				echo '
				"id":"'.$id.'",
				"class": "jstree-drop",
				"text":"'.$terme.'",
				"parent":"'.$parent_id.'"
				';

				echo '}';
				if($cpt != count($this->arrNodes))
					echo ',';
			}
		echo ']';	
	}



	function renderOutput($id_input_result,$print=true,$url_edit='',$all_open=0,$withDeclaration=true) {
		ob_start();
		echo "\n<div align='left' id='".$this->id_div_tree."' class=\"".(isset($this->treeParams['ID_TYPE_CAT'])?'type_cat_'.$this->treeParams['ID_TYPE_CAT']:'')."\"></div>\n";
	    echo "<script type=\"text/javascript\">\n";
		echo "requirejs(['jstree/dist/jstree'],function(){";
		$this->makeTree($this->includeRoot?-1:0); //Doit-on également include la racine. Oui pour les paniers


		//relier un span à la fonctionnalité contextmenu, obligé de faire un plugin
		if(!empty($this->addNodeByContextMenu)){
			echo '(function ($, undefined) {
				"use strict";
				var span = document.createElement(\'SPAN\');
				span.innerHTML = "+";
				span.className = "jstree-contextmenubtn";
				

				$.jstree.plugins.contextmenubtn = function (options, parent) {
					this.bind = function () {
						parent.bind.call(this);
						this.element
							.on("click.jstree", ".jstree-contextmenubtn", $.proxy(function (e) {
									e.stopImmediatePropagation();
			                        $(e.target).closest(\'.jstree-node\').children(\'.jstree-anchor\').trigger(\'contextmenu\');
								}, this));
					};
					this.teardown = function () {
						this.element.find(".jstree-contextmenubtn").remove();
						parent.teardown.call(this);
					};
					this.redraw_node = function(obj, deep, callback, force_draw) {
						obj = parent.redraw_node.call(this, obj, deep, callback, force_draw);
						if(obj) {
							var tmp = span.cloneNode(true);
							obj.insertBefore(tmp, obj.childNodes[2]);
						}
						return obj;
					};
				};
			})(jQuery);';
		}
		//init tree
		//contextmenu: menu pour créer un noeud
		//dnd : drag and drop
		echo "jQuery('#".$this->id_div_tree."').jstree({
		    'core' : {
		    	'check_callback' : true,
		        'data' : function (node, cb) {          
		       		if(node.id == '#') {
		     		cb(struct_".$this->id_div_tree.");
			   			 }else {
						   	 jQuery.ajax({
						     	 'url' : 'empty.php?urlaction=ajaxtree_child&form_name=".$this->id_input_form."&branch_id='+node.id+'&entity=".$this->objSearch->entity."&id_type_cat=".$this->treeParams['ID_TYPE_CAT']."&treeParams=".$serializedPrms."',
						         'type': 'POST',
		    					 'dataType': 'JSON',
						    	'contentType':'application/json',
						    }).done(function (data) {
						       cb(data);
						    })
					   			 }
		        }  
		    },";

		   if(!empty($this->addNodeByContextMenu)){
			    echo "'contextmenu': {
			    	'select_node': false,
				        'items': {
				           'Create': {
				                'label': 'Add',
								'action'			: function (data) {
									var inst = jQuery.jstree.reference(data.reference),
										obj = inst.get_node(data.reference);
									inst.create_node(obj, {}, 'last', function (new_node) {
										setTimeout(function () {
											 inst.edit(new_node); 
										},0);

									});
								}
				                
							}
						}
				},";
			}

			echo "'plugins' :  [ ".(!empty($this->dnd_in_tree) || !empty($this->dnd_out_tree)?"'dnd',":"")."'contextmenu','contextmenubtn'] 
		 	})
		";
		// event onclick
	echo "jQuery('#".$this->id_div_tree."').on('select_node.jstree', function (e, data) {
		if(data.node.id == '-1'){
			return false ; 
		}
		 ".$this->JSlink."'+data.node.id;
		});";

	//event drag and drop
	if(!empty($this->dnd_in_tree)){
		echo "jQuery('#".$this->id_div_tree."').bind('move_node.jstree', function(e, data) {
			var parentId = data.parent;
			var old_parentId = data.old_parent;
			var node_id = data.node.id;
			jQuery.ajax({
				     	 'url' : 'empty.php?urlaction=ajaxtree_move&id_type_cat=".$this->treeParams['ID_TYPE_CAT']."&parentId='+parentId+'&old_parentId='+old_parentId+'&node_id='+node_id,
				         'type': 'POST',
				          success: function(data) {
				             jQuery('#".$this->id_div_tree."').jstree('select_node',data);
				          }
					     });
		});";
	}

	if(!empty($this->addNodeByContextMenu)){
	//event create node par contextmenu
	    echo "jQuery('#".$this->id_div_tree."').bind('rename_node.jstree', function(e, data) {
	        var parentId=data.node.parent;
	        var newName=data.text;
			jQuery.ajax({
		     	 'url' : 'empty.php?urlaction=ajaxtree_create_node&id_type_cat=".$this->treeParams['ID_TYPE_CAT']."&parentId='+parentId+'&newName='+newName,
		         'type': 'POST',
	 			 success: function(data) {
	 			 	refresh_select_node('".$this->id_div_tree."',data);
	        	  }
			     });

		});";

	 	//fonction pour faire apparaitre le + qui remplace le contextmenu
		echo "hover_contextmenu = function(){
			jQuery('li.jstree-node').mouseover(
			  function(event) {
			  	jQuery(this).parents().has('span.jstree-contextmenubtn').children('span.jstree-contextmenubtn').hide();
			    jQuery(this).children('span').show();
			    event.stopPropagation();
			    event.preventDefault();
			  });
			 jQuery('li.jstree-node').mouseleave(
			  function(event) {
			    jQuery(this).children('span').hide();
			    event.stopPropagation();
			    event.preventDefault();
			  }
			);
		};";

		//span pour contextmenu display:none de base, hover pour le faire apparaitre
		echo "jQuery('#".$this->id_div_tree."').bind('open_node.jstree', function(e, data){
		 	hover_contextmenu();
		});";
	}

	//selectionne le bon noeud apres refresh
 	 echo " refresh_select_node = function (id_tree,id_node){
  			jQuery('#'+id_tree).one('refresh.jstree', function (e, data) {
				data.instance.select_node(id_node); 
			});
  			jQuery('#'+id_tree).jstree('refresh');
 		 };";

	 //event drag an drop élément type doc dans l'arbre
	if(!empty($this->dnd_out_tree)){
		echo "jQuery('#".$this->id_div_tree."').bind('ready.jstree', function(e, data) {";
		echo "jQuery('.draggable_doc_cat')
			.on('mousedown', function(e) {
			     return jQuery.vakata.dnd.start(e, 
					{
						'jstree': true, 
						'obj': jQuery(this),
						'nodes': [{id: true, text: jQuery(this).text(), 'data': {'more': 'stuff'}}]
					},
					'<div id=\'jstree-dnd\' class=\'jstree-default\'><i class=\'jstree-icon jstree-er\'></i>' + jQuery('<span></span>').append(jQuery(this).clone()).html()+ '</div>');    
		       });


		jQuery(document)
		    .on('dnd_stop.vakata', function (e, data) {
				try{
					// console.log(\"dnd_stop.vakata data:\");
					// console.log(data);
					// avant toute chose on check qu'on est dans un jstree 
					if(typeof data.event.target != 'undefined' && jQuery(\"#\"+data.event.target.id).parents('div.jstree').length>0){
						var drop=data.event.target.id.match(/[0-9]+/g);
						var type_cat = \"\";
						// récupération du type de catégorie vers lequel on drop
						jQuery.each(\$j('#'+data.event.target.id).parents('div[class^=\"type_cat_\"]').get(0).className.split(' '),function(idx,val){
							if(val.indexOf('type_cat') != -1 ){
								type_cat = val.replace('type_cat_','');
							}
						});
						// console.log(\"type_cat\"+type_cat+\" ".$this->treeParams['ID_TYPE_CAT']."\");
						var id_dnd=data.element.id.match(/[0-9]+/g);
						// on vérifie que l'arbre vers lequel on drop est bien celui pour lequel cet event handler a été défini (en comparant depuis les treeParams)
						if( (type_cat == '".$this->treeParams['ID_TYPE_CAT']."') && drop != null  && id_dnd != null){		//&& drag != null
							jQuery.ajax({
								 'url' : 'empty.php?urlaction=ajaxtree_doc_cat&id_type_cat='+type_cat+'&drop='+drop+'&drag=".$this->treeParams['CURR_ID_CAT']."&id_dnd='+id_dnd,
								 'type': 'POST',
								 success: function(data) {
									refresh_select_node('".$this->id_div_tree."',data);
								  }
							 });
						}
					}
				}catch(e){
					console.log(\"crash dnd_stop\"+e);
				}
		    });";
	echo "});";
	}
	// referme le callback requirejs
	echo "});";
	echo '</script>';

        $output=ob_get_contents();
        ob_end_clean();
		if ($print) echo $output; else return $output;
	}

function makeTree($rootid=0) { //analyse du tableau des résultats et création de l'arbre hiérarchique

	//debug($this->arrNodes,'tan');
	//trace(print_r($this->arrNodes,true));
	$tree = Array();
	foreach ($this->arrNodes as $idx=>$item) {
	$id = $item['id'];
	if (!isset($tree[$id])) {
	    /* Il a pu ?tre d?j? cr?? par un de ses enfants,
	     * sinon on le cr?e ici */
	   $tree[$id] = Array();
	}
	$tree[$id]['id'] = $id;
	$tree[$id]['terme'] = $item['terme'];
	$parent = $item['id_pere'];
	if (!isset($tree[$parent])) {
	    /* Il a pu ?tre d?j? cr?? pour lui-m?me, ou par un
	     * autre de ses enfants, sinon on le cr?e ici */
	    $tree[$parent] = Array();
	}
	$tree[$parent][$id] =& $tree[$id];
	}
	// foreach ($tree as $idx=>$itm) if ($idx!=$rootid) unset($tree[$idx]);

	//debug($tree,'pink');

echo 'var struct_'.$this->id_div_tree.' = [';
	$this->createJSON($tree[$rootid]);
echo '];';
}

function createJSON($tree) {
	
			$serializedPrms=serialize($this->objSearch->treeParams);
            if (!is_array($tree)) return;
            //racine
            echo "{
				'id':'-1',
				'text':\"".($tree['terme'])."\",
				'icon':\"".imgUrl."/tree_icon.png\",
				'state' : {
           		'opened' : true
				},
				'parent' : '#'
           		 },";
				//'disabled' : true
            foreach ($tree as $idx=>$branch) {
            	if (!is_array($branch)) return; //by LD 23/07/08, remplacement du return par continue pour aff hiér panier
            									  //pour pouvoir continuer si des entités 'nom' (terme, etc) sont placées devant
            	$children=false;unset($arrSons);
            	foreach ($branch as $u=>$fld) {
            		if (is_array($fld)) {$arrSons[$u]=$fld;}
            	}
            	$element=$this->arrNodes['elt_'.$idx];

            	$id=$element["id"];
                $terme=$element["terme"];
 	                echo  "{
						'id':'".$id."',
						'text':\"".($terme)."\",
						'parent' : '-1',
						";
				if (!empty($element['nb_children']) && ($element['nb_children'] > 0)) { //pas d'enfant et 0/1 terme asso
					echo "'children' : true,";
			}
		
			echo  "},";


                
                unset($terme);
	}	
			
 }
}
?>