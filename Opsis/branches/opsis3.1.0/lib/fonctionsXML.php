<?php

// Notre gestionnaire d'erreur xsl
function xslt_error_handler($handler, $errno, $level, $info)
{
  // pour le moment, on regarde juste ce qu'on re�oit
  // var_dump(func_get_args());
  print("XSLT ERROR! Errno = $errno - level = $level<br>\n");
  print_r($info);
}

// Fonction transformant un tableau associatif en XML
function TableauVersXML ($tableau, $nomElement,$nbTab=4,$root="select",$entete=1,$xml2merge='')
{
  // Cr�ation d'une cha�ne avec le nombre $nbTab de tabulations
  $tab="";
  for ($i=0; $i < $nbTab; $i++) $tab .= " ";

  if($entete==1){
    $chaineXML = "<?xml version='1.0' encoding='UTF-8' ?".">\n";
  } else $chaineXML="";
  $chaineXML .= "$tab<$root>\n";
  $tbXML = "";

   // On cr�e un seul �l�ment avec des attributs XML
        foreach($tableau as $vc => $vc_data)
        {


            if(is_array($tableau[$vc])) {
            	$tbXML .= "$tab$tab<$nomElement value=\"".$vc."\">\n";
	            foreach($tableau[$vc] as $ind => $data)        {
	                // On ne traite pas les tableaux

	                if(is_array($data)){ //si tableau -> traitement récursif
	                	$tbXML.=TableauVersXML($data,$ind,4,$ind,0);

	                }
	                elseif (is_object($data) && method_exists($data,'xml_export') ) { // si c'est un objet, on utilise sa propre fonction d'export xml (si elle existe)
	                	if (!is_numeric($ind)) $tbXML.=$tab.$tab.$tab."<".$ind.">".$data->xml_export()."</".$ind.">\n";
	                	else $tbXML.=$tab.$tab.$tab.$data->xml_export()."\n";
	                }
	                else {
	                	// Filtrage apostrophe FT
						// VP 6/05/10 : ajout filtrage apostrophe Word
						$data=str_replace(array(" ' "," ’ "),array("'","’"),$data);

	                    //Si le champs contient du XML (ex : DOC_LEX, DOC_XML, etc), on n'encode pas pour garder les balises !
						// VP (26/9/08) : ajout cas de données XML avec entête
						// VP (14/10/08) : ajout cas balise contenant XML  (ex: PAN_XML) mais pas colonne_xml de Sinequa !!
						if ((substr($data,0,5)=="<XML>")||(substr($data,0,5)=="<?xml")||(strpos(strtoupper($ind),"XML")>0 && substr($data,0,1)=="<")){
							if(substr($data,0,5)=="<?xml") $u_data=substr($data,strpos($data,"?>")+2); // Suppression entête
	                    	else $u_data=$data;
	                    } else $u_data = htmlspecialchars($data,ENT_QUOTES);

	                    //$u_data = iconv("iso-8859-1","utf-8",$u_data);
	                    //if($u_data == "") $u_data = "&#160;"; //ATTENTION : tous les champs vides auront un &#160;
	                    $tbXML .=  "$tab$tab$tab<$ind>$u_data</$ind>\n";
	                }
	            } $tbXML .= "$tab$tab</$nomElement>\n";
            }elseif (is_object($vc_data) && method_exists($vc_data,'xml_export') ) { // si c'est un objet, on utilise sa propre fonction d'export xml (si elle existe)
	                	if (!is_numeric($vc)) $tbXML.=$tab.$tab.$tab."<".$vc.">".$vc_data->xml_export()."</".$vc.">\n";
	                	else $tbXML.=$tab.$tab.$tab.$vc_data->xml_export()."\n";
	                }


     }
    $chaineXML .= "$tbXML";
    $chaineXML .= $xml2merge;
    $chaineXML .= "$tab</$root>\n";
    return $chaineXML;
}

function xmlformat($string,$cdata=false) {
	if($cdata){		
		return "<![CDATA[".$string."]]>";
	}else{
		return str_replace(array("&",">","<"),array("&amp;","&gt;","&lt;"),$string);
	}
}

function array2xml($array, $root = 'xml',$cdata=false) {
	if (!is_array($array)) return false;
	$first_key = key($array);
    if ($root!='' && !is_numeric($first_key))
		$xml = "<$root>";
	
    foreach($array as $k => $v) {
        if (is_array($v)) {
            $xml .= array2xml($v, (is_numeric($k) ? $root : $k),$cdata);
		} else {
			if (is_numeric($k)) {
                $xml .= "<$root>".xmlformat($v,$cdata)."</$root>";
            } else {
                $xml .= "<$k>".xmlformat($v,$cdata)."</$k>";
            }
        }
    }
     if ($root!='' && !is_numeric($first_key)) $xml .= "</$root>";
    return $xml;

}

function tab2xml($array, $root = 'xml') {

	if (!is_array($array)) return false;
    if ($root!='') $xml = "<$root>";
    foreach($array as $k => $v) {

        if (is_array($v)) {
            $xml .= tab2xml($v, (is_numeric($k) ? $root : $k));
        } else {
            if (is_numeric($k)) {
                $xml .= "<$root id=\"$k\">".xmlformat($v)."</$root>";
            } elseif (!empty($k)) {
                $xml .= "<$k>".xmlformat($v)."</$k>";
            }
        }
    }
     if ($root!='') $xml .= "</$root>";
    if (preg_match("/<$root>(<$root>.*<\/$root>)<\/$root>/", $xml, $parts)) {
        $xml = $parts[1];
    }elseif (preg_match("/<$root>(<$root .*>.*<\/$root>)<\/$root>/", $xml, $parts)) {
        $xml = $parts[1];
    }elseif (preg_match("/<$root .*>(<$root .*>.*<\/$root>)<\/$root>/", $xml, $parts)) {
        $xml = $parts[1];
    }
    return $xml;

}

// fonction récursive de traitement d'un objet dom et d'alimentation d' un array
// peut etre utilisé tel quel si l'entrée est un objet DOMDocument
function domxml2array($root) {
	$result = array();

	if ($root->hasAttributes()) {
		$attrs = $root->attributes;
		foreach ($attrs as $attr) {
			$result['@attributes'][$attr->name] = $attr->value;
		}
	}

	if ($root->hasChildNodes()) {
		$children = $root->childNodes;
		if ($children->length == 1) {
			$child = $children->item(0);
			if ($child->nodeType == XML_TEXT_NODE || $child->nodeType == XML_CDATA_SECTION_NODE) {
				$result['_value'] = $child->nodeValue;
				return count($result) == 1
					? $result['_value']
					: $result;
			}
		}
		$groups = array();
		foreach ($children as $child) {
			if (!isset($result[$child->nodeName])) {
				$result_tmp =  domxml2array($child); 
				if(!empty($result_tmp) ){
					$result[$child->nodeName] =$result_tmp ;
				}
			} else {
				if (!isset($groups[$child->nodeName])) {
					$result[$child->nodeName] = array($result[$child->nodeName]);
					$groups[$child->nodeName] = 1;
				}
				$result_tmp =  domxml2array($child); 
				if(!empty($result_tmp) ){
					$result[$child->nodeName][] =$result_tmp;
				}
			}
		}
	}
	return $result;
}


function xml2array($xml){
	if (!empty($xml)) {
		$domxml = new DOMDocument();
		$domxml->loadXML($xml);
		return domxml2array($domxml);
	}
	return "";
}


function xml2array_old($xml) {

    $_data = NULL;

    $xp = xml_parser_create("UTF-8");
    xml_parser_set_option($xp, XML_OPTION_CASE_FOLDING, false);
    xml_parser_set_option($xp, XML_OPTION_SKIP_WHITE, true);

    xml_parse_into_struct($xp,$xml,$vals,$index);
    xml_parser_free($xp);

    $temp = $depth = array();
    $dc = array();

    foreach($vals as $value) {

        $p = join('::', $depth);

        $key = $value['tag'];

        switch ($value['type']) {

          case 'open':
            array_push($depth, $key);
            array_push($depth, (int)$dc[$p]++ );
            break;

          case 'complete':
            array_push($depth, $key);
            $p = join('::',$depth);
            $temp[$p] = $value['value'];
            array_pop($depth);
            array_push($depth, (int)$dc[$p]++ );
            array_pop($depth);
            break;

          case 'close':
            array_pop($depth);
            array_pop($depth);
            break;
        }
    }

    foreach ($temp as $key=>$value) {

        $levels = explode('::',$key);
        $num_levels = count($levels);
        if ($num_levels==1) {
            $_data[$levels[0]] = $value;
        } else {
            $pointer = &$_data;
            for ($i=0; $i<$num_levels; $i++) {
                if ( !isset( $pointer[$levels[$i]] ) ) {
                    $pointer[$levels[$i]] = array();
                }
                $pointer = &$pointer[$levels[$i]];
            }
            $pointer = $value;
        }
    }
    return ($_data);
}


/** BY LD : PHP5 only **/
 function TraitementXSLT ($xml, $nomFichierXsl, $arrProfil,$print=1,$securexml="",$arrHighlight=null,$htmlOutput=true)
  {

	$xmlF=new DOMDocument('1.0','UTF-8');
 	global $arrColors;
// error_reporting(E_ALL);
// ini_set('display_errors',1);
    $log = new Logger("data.xml");
    $log->Log($xml);

 	if($arrHighlight) $xml=highlight($xml,$arrHighlight); // Ajout des balises highlight si demandé

 	//$xml=utf8_encode($xml);
   //	trace($xml);

    // VP 28/07/2015 : suppression caractères de contrôle
    $xml = preg_replace('/[^\PC\s]/u', '', $xml);
    $securexml = preg_replace('/[^\PC\s]/u', '', $securexml);

      //debug($xml,'gold',true);
 	$xmlF->loadXML($xml);

	//debug($xmlF->saveXML(),'khaki');
	$xsl = new DOMDocument;
	$xsl->load($nomFichierXsl);
	$xsl_dir = dirname($nomFichierXsl);

	// Remplacement include
	// permet d'appliquer le raisonnement getSiteFile sur chacune des feuilles xsl incluses à partir de la principale
	// On prendra donc en priorité des fichiers xsl redéfinis dans le site par rapport à ceux existant sur le moteur, même si on a redéfini qu'un sous-xsl, 
	// en revanche ce comportement n'est appliquable que sur les include à partir de la xsl principale, il faut donc que tous les includes XSL soient réalisés dans les fichiers principaux (cf docAff.xsl, docListe.xsl etc .. )
	$tags = $xsl->getElementsByTagName('include');
	foreach($tags as $k=>$tag){
		$href = $tags->item($k)->getAttribute("href");
		// on recompose le chemin absolu de l'inclusion
		$href_path = resolve_path($xsl_dir.'/'.$href);
		// On appelle getSiteFile (dans sa nouvelle version qui gère des path complets indépendamments des constantes)
		$href_path = getSiteFile(dirname($href_path),basename($href_path));
		// href_path 
		if(is_file($href_path) && basename($href) == basename($href_path)){
			// trace("traitementxslt redefine include : ori : ".$href." => ".$href_path);
			$tags->item($k)->setAttribute("href", $href_path);
		}
	}
	
	// Configure the transformer
	$proc = new XSLTProcessor;

	if ($arrProfil) {
		foreach ($arrProfil as $param => $value) {
      $value=str_replace("'",'&#39;',$value);
			$proc->setParameter("", $param, $value);
		}
	}
	libxml_use_internal_errors(true);
	$proc->importStyleSheet($xsl); // attach the xsl rules
	$html= $proc->transformToXML($xmlF);
	if (!$html) {
		foreach (libxml_get_errors() as $error) {
			trace("Libxml error: {$error->message}");
		}
		debug("PB TRANSFO XSL, SECURITE UTILISEE");
		$xmlF->loadXML($securexml);
		$html= $proc->transformToXML($xmlF);
	}
	libxml_use_internal_errors(false);
	//trace($html);
	unset($proc);


	//$html=str_replace(array("&lt;","&gt;","&amp;","||"," ' "),array("<",">","&","<br/>","'"),$html);

	//By LD 28 05 09 => ajout de la variable $htmlOuput : dans ce cas, on remet les caractères interdits XML en HTML
	//Sinon, ces caractères sont gardés tels quels, c'est notamment utile pour les imports
	if ($htmlOutput) $html=str_replace(array("&lt;","&gt;","&amp;"," ' "),array("<",">","&","'"),$html);

	//problème avec les ||, pas terrible + pose problème pour l'export tabulé
	//$html = eval("?".chr(62).$html.chr(60)."?"); // pour évaluer le PHP à l'intérieur du XSL.
	//Effet secondaire : provoque l'affichage du contenu !
	return $html;
 }

/*
FONCTION TABTOXSL : Passe un tableau � une XSL (le tableau �tant converti en XML)
$donnees : tableau de donn�es
$nomelement : nom de l'�l�ment root du XML
$nomFichierXsl : fichier XSL
$options : param�tres � passer � la XSL

NB : Fonctionnement similaire � "afficherListe"
*/
function tabToXSL($donnees,$nomelement,$nomFichierXsl,$options)
{

    //$tab = array();
    $xml = TableauVersXML($donnees,$nomelement,4,"select",1);

    $log = new Logger("data.xml");
    $log->Log($xml);
    if(is_array($options)){
        foreach($options as $ind => $data) $tab[$ind] = $data;
    }
    else if($options == -1);
    // pour debug
    else die("Mauvais param�tres ($options) pour AfficherListe<br>");

    print TraitementXSLT($xml,$nomFichierXsl,$tab);
}

/*
FONCTION TABANDXMLTOXSL : Passe un XML � une XSL � partir d'un XML et d'un tablezau de donn�es de $type multivalu� ou non
$type : 0 ou 1 (simple ou mutivalu�)
$donneesXML : XML � ajouter
$donneesTab : tableau de donn�es
$root : nom de l'�l�ment root du XML
$nomFichierXsl : fichier XSL
$options : param�tres � passer � la XSL

Un tableau multivalu� est de la forme :
array(array("root"=>value, "nomElement"=>value, "donnees"=>array()),...)
*/
function tabAndXMLToXSL($type,$donneesXML,$donneesTab,$root,$nomFichierXsl,$options)
{

    // Entete XML
    $xml="<?xml version='1.0' encoding='utf-8' ?".">\n";
    $xml.="<$root>\n";
    $xml.=$donneesXML;
        if($type==0){
            $xml.= TableauVersXML($donneesTab,$root,4,"select",1);
        }else if ($type==1){
            // Cr�ation du XML pour chaque tableau de donn�es
            foreach($donneesTab as $tab_donnees){

                $xml.= TableauVersXML($tab_donnees["donnees"],$tab_donnees["nomElement"],4,$tab_donnees["root"],0);
            }
        }
    $xml.="</$root>\n";

    // Log
    $log = new Logger("data.xml");
    $log->Log($xml);
    if(is_array($options)){
        foreach($options as $ind => $data) $tab[$ind] = $data;
    }
    else if($options == -1);
    else die("Mauvais param�tres ($options) pour AfficherListe<br>");

    print TraitementXSLT($xml,$nomFichierXsl,$tab);
}

/** dom_to_array
 *  Transformation d'un tableau XML en tableau PHP (récursif)
 *  IN : XML, tableau
 * 	OUT : tableau (param IN en byval)
 */
function dom_to_array($domnode, &$array) {

  $parent=$domnode;
  $domnode = $domnode->firstChild;
  $myname=$domnode->nodeName;
  $x=1;
  while (!is_null($domnode)) {
     switch ($domnode->nodeType) {

       case XML_ELEMENT_NODE: {

             if ( !$domnode->hasChildNodes()) {
                 $array[$domnode->nodeName]='';
             } else if ( $domnode->hasChildNodes() && $domnode->firstChild->nodeType==XML_TEXT_NODE) {
                 $array[$domnode->nodeName]=$domnode->firstChild->nodeValue;
             } else if ( $domnode->hasChildNodes() )  {

         $array_ptr = & $array[$domnode->nodeName];
           dom_to_array($domnode, $array_ptr);
         break;
       }
   }
   }
         $domnode = $domnode->nextSibling;
       if($domnode->nodeName==$myname)
       {
             $domnode->nodeName.=($x++);
       }
  }
}


/** Mise en surbrillance des termes recherchés dans un contenu (du XML généralement)
 *
 * Se base sur l'architecture CHAMP=Array(VALEURS) placée dans la variable de session highlight.
 * IN : chaine à traitée
 * OUT : chaine traitée (balises <span highlight> autour des mots).
 *
 MS - 21.10.15 - modification de la fonction pour qu'elle accepte aussi en entrée (arrHighlight) les tableaux d'objets highlights des responses SolR
	=> arrHighlight peut donc être le SolrObject highlight d'un seul document (cf docSolr), exemple :
		object(SolrObject)#9 (2) {
		  ["ticol"]=>
		  array(1) {
			[0]=>
			string(36) "<highlight>Journal</highlight> 13h00"
		  }
		  ["tiprog"]=>
		  array(1) {
			[...]
		  }
		}
	=> Il peut aussi être le SolrObject highlight dans son ensemble (cf docListe) , exemple :
		object(SolrObject)#95 (20) {
			["RD/5862460.001.001"]=>
			 object(SolrObject)#9 (2) {
			  ["ticol"]=>
			  array(1) {
				[0]=>
				string(36) "<highlight>Journal</highlight> 13h00"
			  }
			  [...]
			["RD/5862462.001.001"]=>
			 object(SolrObject)#9 (2) {
			  ["ticol"]=>
			  array(1) {
				[0]=>
				string(36) "<highlight>Journal</highlight> 13h00"
			  }
			  [...]
	--> Ces remplacements sont plus précis car les remplacements sont faits docs par docs, il y a moins de risque de bruit au niveau du highlight
**/
function highlight($content,$arrHighlight,&$recurs_dom=null) {
	require_once(libDir."class_cherche.php");

	if (empty($arrHighlight)){
		return $content; //Pas de highlight spécifié, on retourne directement la valeur
	}
	$idxCol=1;
	if($recurs_dom == null ){
		$dom=new DOMDocument('1.0','UTF-8');
		$ok=$dom->loadXML($content);
	}else{
		$dom = $recurs_dom;
	}
	
	if(!empty($arrHighlight) && !is_array($arrHighlight)&& get_class($arrHighlight) == 'SolrObject'){
		$highlight_solr = true ; 
			
		if(reset($arrHighlight) && !is_array(reset($arrHighlight)) && get_class(reset($arrHighlight)) == 'SolrObject' ){
			$tmp_dom = $dom->getElementsByTagName((defined("gNomChampIdSolr")?strtoupper(gNomChampIdSolr):'ID'));
			foreach($tmp_dom  as $id_node){
				if(isset($arrHighlight[trim($id_node->nodeValue)]) && !empty($arrHighlight[trim($id_node->nodeValue)])){
					$part_dom = $id_node->parentNode;
					if(isset($part_dom) && !empty($part_dom)){
						highlight($content,$arrHighlight[trim($id_node->nodeValue)],$part_dom);
					}
				}
			}
			$content=$dom->saveXML();
			return $content ;
			}
	}else{
		$highlight_solr = false ;
	}
	// pour chaque élément de arrHighlight
    foreach ($arrHighlight as $XMLfields=>$tabValues) { //Traitement des mots et des couleurs
		if($highlight_solr){
			$XMLfields = strtoupper(preg_replace('/^hl_/i','', trim($XMLfields)));
			$XMLfields = strtoupper(preg_replace('/_'.implode('|_',$_SESSION['arrLangues']).'$/i','', trim($XMLfields)));
		}
							
        $arrWord=array(); //RAZ MOTS
		if (empty($tabValues))
            continue;

        if (is_string($tabValues))
            $tabValues=array($tabValues);

        // pour chaque champ à highlighter on récupère une liste de mot dans l'arr arrWord
        foreach ($tabValues as $_i=>$_val) { //On peut avoir plusieurs sous tableaux, par exemple si plusieurs
            //recherches en LEX ou VAL (lieu + intervenant par ex)
            if (!is_array($_val)) {// Valeur unique, on procède à l'analyse des mots
			   //LD 09 04 09 => pas de spacequote pour ne pas 'endommager' la chaine
			   //MS 09/05/14 => si on arrive de solr, les mots sont déja splitté depuis la recherche FT, et on ne veut pas les split si on vient d'une recherche exacte avec dble quote
                if(defined("useSolr") && useSolr){
					if($XMLfields != "TEXT" ){
						$arrWord['WORD'][]=trim($_val,'"');$arrWord['COLOR'][]=$idxCol;
						$idxCol++;
					}
				}else{
					$_arr=Recherche::analyzeString($_val,$dumb,false); //split intelligent des mots
					foreach ($_arr as $_w) {
						//Si mot de type MOT*, on formatte l'expression régulière
						if (substr($_w,-1,1)=='*') $_w=str_replace(array('+','-','*','"'),'',$_w).".*?"; //étend sur ttes lettres
						// VP 24/05/10 : cas où le pattern est *
						if($_w!=".*?"){
							$arrWord['WORD'][]=trim($_w,'"');$arrWord['COLOR'][]=$idxCol;
							$idxCol++; //1 couleur par mot
						}
					}
				}


            } else {//Tableau de valeurs (ex : extension lexicale)
				foreach ($_val as $_w) {
					$arrWord['WORD'][]=trim($_w,'"');
					$arrWord['COLOR'][]=$idxCol;
				}
                $idxCol++; //la même couleur pour tous
           }
        }
        
        $fields=explode(",",$XMLfields);
		$arrDom = array();
		if(isset($arrWord['WORD']) && !empty($arrWord['WORD'])){
			// on récupère les noeuds XML correspondant aux champs définis dans arrHighlight (= $XMLfields)
            foreach ($fields as $fld) { //Parcours des champs
                switch ($fld) {
                    case 'DOC_LEX': $insideVal='TERME';break; //Champs contenant du XML
                    case 'DOC_VAL': $insideVal='VALEUR';break;
                    case 'DOC_CAT': $insideVal='CAT_NOM';break;
					case 'DOC_XML': $insideVal='XXX';break; // Bypass le highlight sur DOC_XML, à améliorer en prenant en compte les difféntes balises
                    default : $insideVal=false;break;
                }
                if(defined ('useSolr') && useSolr && $insideVal == false  ) {
                    $arrDomTmp = array();
                        $fld = strtoupper($fld);
                    switch(substr($fld,0,strpos($fld,'_'))){
                        case 'L' : 
                            $roles = $dom->getElementsByTagName('t_rol');
                            foreach($roles as $t_role){
                                if($t_role->getAttribute('KEY_SOLR') == $fld){
                                    $nodes = $t_role->getElementsByTagName("LEX_TERME");
                                    foreach($nodes as $node){
										if(preg_match('/(\/t_doc_freres\/|\/t_doc_lies_(?:src|dst)\/)/',$node->getNodePath())){
											continue ; 
										}
                                        array_push($arrDomTmp,$node);
                                    }
                                }
                            }
                            break ;
                        case 'P' : 
                            $roles = $dom->getElementsByTagName('t_rol');
                            foreach($roles as $t_role){
                                if($t_role->getAttribute('KEY_SOLR') == $fld){
                                    $nodes = $t_role->getElementsByTagName("PERS_TERME");
                                    foreach($nodes as $node){
										if(preg_match('/(\/t_doc_freres\/|\/t_doc_lies_(?:src|dst)\/)/',$node->getNodePath())){
											continue ; 
										}
                                        array_push($arrDomTmp,$node);
                                    }
                                }
                            }
                            break ;
                        case 'V' :
                            $types_val = $dom->getElementsByTagName('TYPE_VAL');
                            foreach($types_val as $type_val){
                                if($type_val->getAttribute('KEY_SOLR') == $fld){
                                    $nodes = $type_val->getElementsByTagName("VALEUR");
                                    foreach($nodes as $node){
										if(preg_match('/(\/t_doc_freres\/|\/t_doc_lies_(?:src|dst)\/)/',$node->getNodePath())){
											continue ; 
										}
                                        array_push($arrDomTmp,$node);
                                    }
                                }
                            }
                            break ;
                            
                    }
                    if(empty($arrDomTmp)){
                        $tables=$dom->getElementsByTagName($fld); //On récupère ts les champs correspondant dans le XML
                        $tabs=$tables;
                        foreach($tabs as $t){
							if(preg_match('/(\/t_doc_freres\/|\/t_doc_lies_(?:src|dst)\/)/',$t->getNodePath())){
								continue ; 
							}
							array_push($arrDomTmp,$t);
                        }
                    }
                    
                    $arrDom = array_merge($arrDom,$arrDomTmp);
                    
                }else if ($insideVal) { //XML inside ? Il faut chercher les noeuds contenant des valeurs
                    /* JE METS CA DE COTE, A REACTIVER SI LES LIGNES SUIVANTES POSENT PB
                     foreach ($tables as $tab) { //Parcours des champs trouvés
                     if ($tab->firstChild->childNodes->length>0) {
                     $tabs=$tab->getElementsByTagName($insideVal);
                     }
                     }
                     */
                    $tabs=$dom->getElementsByTagName($insideVal); //on va chercher ttes les balises TERME / VALEUR (expérimental)
                    foreach($tabs as $t){
						if(preg_match('/(\/t_doc_freres\/|\/t_doc_lies_(?:src|dst)\/)/',$t->getNodePath())){
							continue ; 
						}
                        array_push($arrDom,$t);
                    }
                    
                } else if (strpos($fld,'$')>=0 && strpos($fld,'$')!==false) { // HIGHLIGHT DE LEX_TERME uniquement sur UN ROLE si défini
                    $arrFld=explode('$',$fld);
                    if ($arrFld[0]=="LEX_TERME"){
                        
                        $cond="";
                        if (isset($arrFld[1]) && !empty($arrFld[1])){
                            $cond.="ID_TYPE_DESC='".$arrFld[1]."'";
                        }
                        
                        if (isset($arrFld[2]) && !empty($arrFld[2])){
                            if($cond!='') { $cond.=" and ";}
                            $cond.="DLEX_ID_ROLE='".$arrFld[2]."'";
                        }
                        
                        $navXML = simplexml_import_dom($dom);
                        $tabs = $navXML->xpath("//*[".$cond."]/LEX_TERME");
                        foreach($tabs as $t){
                            $tmp=dom_import_simplexml($t);
							if(preg_match('/(\/t_doc_freres\/|\/t_doc_lies_(?:src|dst)\/)/',$tmp->getNodePath())){
								continue ; 
							}
                            array_push($arrDom,$tmp);
                        }
                        unset($navXML);
                    }
                    
                } else { //Pas de XML inside, on reste au niveau de la basique
                    $tables=$dom->getElementsByTagName($fld); //On récupère ts les champs correspondant dans le XML
                    $tabs=$tables;
                    foreach($tabs as $t){
						if(preg_match('/(\/t_doc_freres\/|\/t_doc_lies_(?:src|dst)\/)/',$t->getNodePath())){
							continue ; 
						}
                        array_push($arrDom,$t);
                    }
                }
            }
        }

		if(isset($arrWord['WORD']) && !empty($arrWord['WORD'])){
			if($highlight_solr){
			// si l'arrHighlight est un objet highlight solr , alors on a un remplacement spécifique :
				foreach($arrDom as $idx=>$_tab){ // pour chaque noeud concerné
					foreach($arrWord['WORD'] as $_idx=>$_word){ // pour chaque mot
			
						// les valeurs highligtées dans l'objet solR ont leurs quotes encodées via htmlspecialchars, il faut les décoder
						$_word = htmlspecialchars_decode($_word,ENT_QUOTES);
						// pour être cohérent avec la même transformation qui est effectuée sur le XML dans docSolr
						// A TERME ne devrait plus exister vu qu'on n'est plus sensé faire ces échappements.....
						$_word = str_replace(" ' ","'",$_word);

						$search = preg_replace('/(<[\/]{0,1}highlight>)/i','',$_word);
						$replace = str_replace('<highlight>','<span class="highlight'.$arrWord['COLOR'][$_idx].'">',$_word);
						$replace = str_replace('</highlight>','</span>',$replace);

						// MS - 22.10.15 - je laisse ces traces en commentaires pour débuggage éventuel
						// echo "<pre>";
						// echo "WORD : ".print_r($_word,true)."\n";
						// echo "\nSEARCH : ".print_r($search,true)."\n";
						// echo "\nREPLACE : ".print_r($replace,true)."\n";
						// echo "\nNODEVALUE ORI : ".print_r($_tab->nodeValue,true)."\n";
						$tmp_value = str_replace($search,$replace,$_tab->nodeValue);
						//  lorsqu'on remplace un nodeValue dans un domdocument, le seul caractère problématique est le '&' qui va etre detecté comme un début d'entité html, on le Re remplace donc ... A terme il faudrait normaliser tout ça, mais pour l'instant c'est la correction qui m'évite de craindre des effets de bords chez les divers clients. 
						$_tab->nodeValue =str_replace('&','&amp;',$tmp_value) ; 
						// $_tab->nodeValue = str_replace('&','&amp;',$tmp_value) ; 
						// echo "\nNODEVALUE MOD : ".print_r($_tab->nodeValue,true)."\n\n";
						// echo "</pre>";
					}
				}
			}else{
                foreach ($arrWord['WORD'] as $_idx=>$_word) {
                    $_word=str_replace(array("[","]"), array("\\[","\\]"), $_word); // pour permettre le highlight de lexique comprenant des crochet '[' ou ']' (utilisé dans la regex ci-dessous)
                    
                    foreach (unserialize(gTabAccents) as $char=>$accents) {
                        $_word=str_replace($accents, $char,$_word);
                        $_word=str_replace($char,"[".implode("|",$accents)."]",$_word);
                    }
                    
                    foreach($arrDom as $idx=>$_tab){
                        // foreach ($tabs_ as $_tab ) {
                        
                        //Expérimental : j'ai viré les "\b" de la l.347 pour les mettre ici : surlignage mot entier
                        //$motif='`(.*?)(\b'.$_word.'\b)([^<]*?)`siu'; //NOTE : le modif u est SUPER important sinon ça ne marche pas avec les accents utf8
                        
                        // VP 24/05/10 : pas d'highlight sur le mot "span"
                        /*$motif='`(.*?)(?!span)(\b'.$_word.'\b)([^<]*?)`siu'; //NOTE : le modif u est SUPER important sinon ça ne marche pas avec les accents utf8
                         $sortie='$1<span class=\'highlight'.$arrWord['COLOR'][$_idx].'\'>$2</span>$3';*/
                        //PC 30/11/12 : modification des regex pour corriger bug depuis opsis 2.7 (experimental)
                        
                        // $motif='`(.*?)(?!span)(\b'.$_word.'\b)([^<]*?)`siu';
                        // By NB 03 02 2016 - dans la regex '\b' remplacé par '(?:$|\s)+' pour permettre la détection de lexique comprenant des crochet '[' ou ']' (\b ne match que les caractères alphanumériques)
						// MS - 27.04.16 - modification du motif pour que les espaces en bordure d'un highlight ne disparaisse plus
                        $motif='`(.*?(?:^|\s)+)(?!span)('.$_word.')((?:$|\s)+[^<]*?)`siu';
                        $sortie='$1<span class=\'highlight'.$arrWord['COLOR'][$_idx].'\'>$2</span>$3';
                        
                        $hled=@preg_replace($motif,$sortie,$_tab->nodeValue);
                        //debug($motif.' vs '.$_tab->nodeValue.' >>>'.$sortie);
                        //By LD 09/01/09 => remplacement & par &amp; pour contourner bug PHP5.2.4
                        //qui tronque l'assignation d'un node à toute chaine avant un &
                        $hled=preg_replace('/&(?!\w+;)/', '&amp;', $hled);
                        if ($hled!=$_tab->nodeValue && $hled!='') $_tab->nodeValue=$hled;
                        
                        //var_dump($_tab->nodeValue);
                        //debug($idxCol." ".$_word." ".$_tab->nodeName." >>> ".$_tab->nodeValue,'yellow');
                        // }
                    }
                }
            }
        }
    }
    if($recurs_dom == null){
        $content=$dom->saveXML();
        //debug($content);
        unset($dom);
        return $content;
    }else{
        return true ;
    }
}



/**
 *
 *
 * Parametres : highlight_params : attribut 'highlight' dans la reponse d'une requete solr
 *				id : identifiant du document a surligner
 *				langue : langue du document a surligner
 * retour : tableau a passer en parametre a la fonction TraitementXSLT
 *
 **/
function solrHighlight($highlight_params,$arrNoHighlight=array(),$id,$langue)
{
	function decode_hl($str){
		return (html_entity_decode($str,ENT_COMPAT,'UTF-8'));
	}
	if( defined("gMotsVidesAnalyzeStringSolr")){
		$arr_mots_vides = unserialize(gMotsVidesAnalyzeStringSolr);
	}else{
		$arr_mots_vides = array();
	}

	$highlight_values=array();
	if (isset($highlight_params) && !empty($highlight_params))
	{
		if(isset($id) && !empty($id) && isset($langue) && !empty($langue)){

			$doc=$highlight_params->offsetGet($id.'_'.$langue);

			if (isset($doc) && !empty($doc))
			{
				foreach ($highlight_params->offsetGet($id.'_'.$langue)->getPropertyNames() as $champ)
				{
					if(empty($arrNoHighlight) || (!empty($arrNoHighlight) && !in_array(trim($champ),$arrNoHighlight))){
						foreach ($highlight_params->offsetGet($id.'_'.$langue)->offsetGet($champ) as $data_hl)
						{
							preg_match_all('/<highlight>(.*?)<\/highlight>/i',$data_hl,$matches);

							if(strpos(trim($champ),'_'.strtolower($_SESSION['langue'])) === strlen(trim($champ))-3){
								$champ = substr(trim($champ),0,-3);
							}
							
							$result = array_map("decode_hl",$matches[1]);
							foreach($result as $word){
								if(!in_array(strtolower($word),($arr_mots_vides))){
									if(strpos($champ,'hl_') === 0 && (!isset($highlight_values[strtoupper(substr(trim($champ),3))]) || !in_array($word,$highlight_values[strtoupper(substr(trim($champ),3))]))){
										if(empty($highlight_values[strtoupper(substr(trim($champ),3))])){$highlight_values[strtoupper(substr(trim($champ),3))] = array();}
										$highlight_values[strtoupper(substr(trim($champ),3))][]=$word;
									}else if (!isset($highlight_values[strtoupper(trim($champ))]) || !in_array($word,$highlight_values[strtoupper(trim($champ))])){
										if(empty($highlight_values[strtoupper(trim($champ))])){$highlight_values[strtoupper(trim($champ))] = array();}
										$highlight_values[strtoupper(trim($champ))][]=$word;
									}
								}
							}
						}
					}
				}
			}
		}else{
			foreach($highlight_params as $solr_obj){
				foreach ($solr_obj->getPropertyNames() as $champ)
				{
					if(empty($arrNoHighlight) || (!empty($arrNoHighlight) && !in_array(trim($champ),$arrNoHighlight))){
						foreach ($solr_obj->offsetGet($champ) as $data_hl)
						{
							preg_match_all('/<highlight>(.*?)<\/highlight>/i',$data_hl,$matches);

							if(strpos(trim($champ),'_'.strtolower($_SESSION['langue'])) === strlen(trim($champ))-3){
								$champ = substr(trim($champ),0,-3);
							}
							
							$result = array_map("decode_hl",$matches[1]);
							foreach($result as $word){
								if(!in_array(strtolower($word),($arr_mots_vides))){
									if(strpos($champ,'hl_') === 0 && (!isset($highlight_values[strtoupper(substr(trim($champ),3))]) || !in_array($word,$highlight_values[strtoupper(substr(trim($champ),3))]))){
										if(empty($highlight_values[strtoupper(substr(trim($champ),3))])){$highlight_values[strtoupper(substr(trim($champ),3))] = array();}
										$highlight_values[strtoupper(substr(trim($champ),3))][]=$word;
									}elseif (!isset($highlight_values[strtoupper(trim($champ))]) || !in_array($word,$highlight_values[strtoupper(trim($champ))])){
										if(empty($highlight_values[strtoupper(trim($champ))])){$highlight_values[strtoupper(trim($champ))] = array();}
										$highlight_values[strtoupper(trim($champ))][]=$word;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	function hl_sort($a,$b){
		return strlen($b)-strlen($a);
	}
	// Les chaines arrHighlight sont triées par longueur de chaine, comme ça on fait les remplacements des valeurs matchées les plus longues en premier (permet de trouver les valeurs composées, puis les éléments simples)
	foreach($highlight_values as $field=>$arrWords){
		usort($arrWords,'hl_sort');
		$highlight_values[$field] = $arrWords;
	}
	

	return $highlight_values;
}

function xml2tab($xmldata,$show_empty_tags=false, $case_folding = true) {
	$xml_parser = xml_parser_create();
	xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, $case_folding);
	xml_parser_set_option($xml_parser,XML_OPTION_SKIP_WHITE,1);
	xml_parse_into_struct($xml_parser, $xmldata, $values, $tags);
	xml_parser_free($xml_parser);

	$subLevel="";
	$subArray="";
	$datas = array();
	$ptrs[0] = & $datas;
	foreach ($values as $xml_elem) {
		$level = $xml_elem['level'] - 1;
		switch ($xml_elem['type']) {
			case 'open':
				// VP 5/10/10 : gestion des attributs
				//		$tag_or_id = (array_key_exists ('attributes', $xml_elem)) ? $xml_elem['attributes']['ID'] : $xml_elem['tag'];
				//		$ptrs[$level][$tag_or_id][] = array ();
				//		$ptrs[$level+1] = & $ptrs[$level][$tag_or_id][count($ptrs[$level][$tag_or_id])-1];
				$tag_or_id=$xml_elem['tag'];
				if(array_key_exists ('attributes', $xml_elem)){
					//VP 5/10/10 : suppression utilisation attribut ID
					//if(!empty($xml_elem['attributes']['ID'])) $tag_or_id=$xml_elem['attributes']['ID'];
					$ptrs[$level][$tag_or_id][] = array ('attributes'=>$xml_elem['attributes']);
				}else $ptrs[$level][$tag_or_id][] = array ();
				//$ptrs[$level][$tag_or_id][] = array ();
				$ptrs[$level+1] = & $ptrs[$level][$tag_or_id][count($ptrs[$level][$tag_or_id])-1];

				break;
			case 'complete':
				// VP 1/04/11 : correction bug si noeud avec attribut seulement
				//$ptrs[$level][$xml_elem['tag']] = (isset ($xml_elem['value'])) ? $xml_elem['value'] : '';
				$tag_or_id=$xml_elem['tag'];
				if(array_key_exists ('attributes', $xml_elem)){
					$ptrs[$level][$tag_or_id][] = array ('attributes'=>$xml_elem['attributes']);
				}elseif(isset($xml_elem['value'])) $ptrs[$level][$xml_elem['tag']] = $xml_elem['value'] ;
				else if($show_empty_tags) {$ptrs[$level][$xml_elem['tag']] = '';}
				break;
		}
	}
	return $datas;
}

function hex_str($hex){
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2){
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}

function str_hex($string){
    $hex='';
    for ($i=0; $i < strlen($string); $i++){
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}

function array2json($arr, $usePHPencode = true) {
    if($usePHPencode && function_exists('json_encode')) return json_encode($arr); 
	else {//class_modifLot a besoin d'unt traitement spécifique
	    $parts = array();
	    $is_list = false;
	
	    //Find out if the given array is a numerical array
	    if (is_array($arr)) $keys = array_keys($arr); else $keys = array();
	    $max_length = count($arr)-1;
	    if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {//See if the first key is 0 and last key is length - 1
	        $is_list = true;
	        for($i=0; $i<count($keys); $i++) { //See if each key correspondes to its position
	            if($i != $keys[$i]) { //A key fails at position check.
	                $is_list = false; //It is an associative array.
	                break;
	            }
	        }
	    }
	
		if (is_array($arr))
			foreach($arr as $key=>$value) {
				if(is_object($value)) $value=get_object_vars($value); //objet => tableau
				if(is_array($value)) { //Custom handling for arrays
					if($is_list) $parts[] = array2json($value, $usePHPencode); /* :RECURSION: */
					else $parts[] = '"' . $key . '":' . array2json($value, $usePHPencode); /* :RECURSION: */
				} else {
					$str = '';
					if(!$is_list) $str = '"' . $key . '":';
	
					//Custom handling for multiple data types
					if(is_numeric($value)) $str .= $value; //Numbers
					elseif($value === false) $str .= 'false'; //The booleans
					elseif($value === true) $str .= 'true';
					else {
						if (strpos($value,"<XML>")!==false) continue;
						$value=str_replace(array(chr(10),chr(13)),array("\\n","\\r"),$value);
						//by LD => gestion des constantes k.... via [[..]] => si la valeur est de type [[val]] on la traite comme une constante
						$pattern="/^\[\[(.*?)\]\]$/";
						preg_match($pattern,$value,$out);
						if (!empty($out)) $value=defined($out[1])?constant($out[1]):$out[1];
						$str .= '"' . addslashes($value) . '"'; //All other things
						unset($out);
					}
					// :TODO: Is there any more datatype we should be in the lookout for? (Object?)
	
					$parts[] = $str;
				}
			}
	    $json = implode(',',$parts);
	    unset($parts);
	    if($is_list) return '[' . $json . ']';//Return numerical JSON
	    return '{' . $json . '}';//Return associative JSON
    }
}


/**
 * Converts a CSV file to a simple XML file
 *
 * @param string $file
 * @param string $container
 * @param string $rows
 * @return string
 */
function csv2xml($file, $container = 'data', $rows = 'row',$separator=",")
{
        //VG : fgetcsv a besoin de ces définitions de langue
        mb_language('en'); //ISO-8859-1/quoted printable
        mb_detect_order('auto');
        mb_internal_encoding('UTF-8');
        setlocale(LC_ALL, "fr_FR");
        // VP 4/2/09 : ajout auto_detect_line_endings pour compatibilité Mac
        ini_set('auto_detect_line_endings', true);

        $r = "<{$container}>\n";
        $row = 0;
        $cols = 0;
        $titles = array();

        $handle = @fopen($file, 'r');
        if (!$handle) return $handle;

        // VP 30/08/10 : passage ligne à 16000 caractère et ajout fonction stripControlCharacters
        // VP 8/01/13 : utilisation de fgets+explode car fgetcsv mange des caractères (exemple ;épisode; -> ;pisode;)
        while (($data = fgetcsv($handle, 16000, $separator)) !== FALSE)
        {
            // B.RAVI 2015-06-05 si contient uniquement caractère null ex: CRLF (caractère de fin) on ne traite pas la ligne
            if (count($data)<2 && !isset($data[0])) { 
                continue;
            }
            
             if ($row > 0) $r .= "\t<{$rows}>\n";
             if (!$cols) $cols = count($data);
             for ($i = 0; $i < $cols; $i++)
             {
                  if ($row == 0)
                  {
                       $titles[$i] = removeTrickyChars(stripAccents(str_replace(' ','_',trim($data[$i])))); //LD 27 05 09, gestion des espaces dans les noms de colonne // NB 13 03 2015 gestion des accents et caractères spéciaux
                        //B.RAVI 2015-06-05 le caractère / se retrouve parfois dans le nom des colonnes et posent pb dans la génération du XML (ai préféré ne pas modifier  removeTrickyChars() car utilisé dans nbx fonctions)
                       $titles[$i] = str_replace('/','_',$titles[$i]);
                       continue;
                  }
                  if (!empty($titles[$i])) {
                  $r .= "\t\t<{$titles[$i]}>";
                  $r .= stripControlCharacters(str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),$data[$i])); //LD 27 05 09, protection des caractères
                  $r .= "</{$titles[$i]}>\n";
                  }
             }
             if ($row > 0) $r .= "\t</{$rows}>\n";
             $row++;
        }
        fclose($handle);
        $r .= "</{$container}>";

        return $r;
}

	/* Fonction permettant la conversion d'un DCSubtitle (sous titres des fichiers DCP) vers un fichier srt */
	function DCSub2srt($path_dcsub,$path_file_out=null){
		$xsl_file = jobDir."/etc/dcsub2srt.xsl";

		if(!file_exists($xsl_file)){
			trace("DCSub2srt - xsl file not found - ".$xsl_file);
			return false ; 
		}
		$subs_xml = file_get_contents($path_dcsub);
		$subs_srt =TraitementXSLT($subs_xml,$xsl_file,array(),1,$subs_xml);
		if($path_file_out!=null && is_string($path_file_out)){
			file_put_contents($path_file_out,$subs_srt);
		}
		
		return $subs_srt ; 
	}

?>
