<?php


class Dtree {

	var $arrNodes; //Tableau des noeuds. Contient la structure, type de noeuds, etc.

	var $objSearch; //Objet de type recherche

	var $output;

	var $JSlink; //Fonction JS ou URL appelée sur les liens de chaque mot

	function __construct($searchobject) {
		$this->objSearch=$searchobject;
		$this->output='';
	}

	function setObject($searchobject) {
		$this->objSearch=$searchobject;
	}

	function makeRoot() { //Constitue la racine et les têtes de rubriques (niveau 1)

		$arrChildren=$this->objSearch->getChildren(0,$this->arrNodes); //récupération niveau 1

		$this->arrNodes['elt_0']=array("id"=>0,
							"id_pere"=>-1,
							"terme"=>"<span style='font-size:14px;font-weight:bold' >".$this->objSearch->getRootName()."</span>",
							"open"=>0,
							"icone"=>'',
							"valide"=>false,
							"context"=>false,
							"nb_children"=>count($arrChildren)
							);
	}

	function revealNode($id) {

		$this->objSearch->getNode($id,$this->arrNodes);
		//$this->objSearch->getChildren($id,$this->arrNodes);
		//$this->objSearch->getFather($id,true,$this->arrNodes);

	}

	function complementNodes() {

		$this->objSearch->complementNodes($this->arrNodes);

	}

	/** Affiche l'arbre des termes.
	 * IN : $id_input_result=champ mis à jour qd on clique sur un terme, $print=affiché direct/ou renvoyé, $url_edit=lien href edition
	 * NOTES : cette routine est très consommatrice en CPU et mémoire.
	 * Elle a donc fait l'objet d'optimisations DONT :
	 * - parcours du tableau en byRef
	 * - utilisation du buffer (ob_start, etc...) pour éviter la concaténation.
	 * - 2 parcours de tableaux :
	 * 		1. pour créer les items (folder & document)
	 * 		2. pour créer les liens entre items
	 * Pour info, ces optimisations ont permis de passer de 33s à 2,5s lors de la génération
	 * d'un thésaurus de 8000 termes (synos non comptés)
	 */

	function renderOutput($id_input_result,$print=true,$url_edit='',$all_open=0) {

			static $terme;
			static $url_noeud;

			ob_start();

            echo "\n<div align='left' id='blocDtree' class='blocDtree'>\n";
            echo "<script type=\"text/javascript\">\n";
			echo"			USEFRAMES=0
							USEICONS=1
							ICONPATH='".libUrl."treeview/'
							USETEXTLINKS=1
							WRAPTEXT=0
							PRESERVESTATE=0
							STARTALLOPEN=".$all_open."
			";

			echo'fld0=gFld("'.$this->arrNodes['elt_0']['terme'].'","javascript:void(null)");
							fld0.xID="0";
							fld0.iconSrc="'.imgUrl.'/dtree/livre.gif";
							foldersTree=fld0;
							';
			unset($this->arrNodes['elt_0']);


            /// II.A.7.a. Cr�ation des noeuds
            foreach ($this->arrNodes as &$element) {
                $id=$element["id"];
                // Si le noeud est valide, on lui associe son url et sa checkbox
                if($element["context"]==false){
					//Par défaut, cliquer sur un terme appelle MaJChamp...
                    if (empty($this->JSlink)) $this->JSlink = "javascript:void(null)";

                	$url_noeud = sprintf($this->JSlink,$id_input_result,$element['terme'],$element['id']);

                } else
                {
                	$url_noeud = "javascript:loadSynchPage(".$element['id_orig'].")";
                }
                $terme=$element["terme"];
                if ($element['extra']) $terme.=" (<i>".$element['extra']."</i>)"; //syno
                /*$terme=str_replace (" ' ", '&rsquo;',trim($terme));*/
                $terme=$this->highlight($terme);

                if ($element['asso']) $terme.="<a href='javascript:loadSynchPage(".$element['id_orig'].")' ><img align='absmiddle' border=0 src='".imgUrl."dtree/loupe.gif'/></a>";
                if (!empty($url_edit)) {$terme.="<a href='".$url_edit.$element["id"]."'><img align='absmiddle' border=0 src='".imgUrl."button_modif.gif'/></a>";}
                $begin = microtime(true);
                if (empty($element['nb_children']) && $element['nb_asso']<1) { //pas d'enfant et 0/1 terme asso
	                echo 'fld'.$element["id"].'=gLnk("S","'.$terme.'","'.str_replace('"',"\'",$url_noeud).'");
';					if ($element['icone']) echo 'fld'.$element["id"].'.iconSrc="'.$element["icone"].'"; ';
                } else {
	                echo 'fld'.$element["id"].'=gFld("'.$terme.'","'.$url_noeud.'");
';
                }
                unset($terme);

	             $notsyncedyet=true;
	             if ($element["open"]=="true") {
	                $selectNodes.="loadSynchPage('".$element["id"]."');\n";
	             	$notsyncedyet=false;
	             }

            }
            foreach ($this->arrNodes as &$element) {
            	 $id=$element["id"];
				 if (empty($element['nb_children'])) {
						if ($element["id_pere"]!='') echo 'if (typeof( fld'.$element["id_pere"].')!="undefined") add'.$id.'=insDoc(fld'.$element["id_pere"].',fld'.$id.');if (typeof(add'.$id.')!="undefined")  add'.$id.'.xID="'.$id.'";
';
				 } else {
					if ($element["id_pere"]!='') echo 'if ( typeof(fld'.$element["id_pere"].')!="undefined") add'.$id.'=insFld(fld'.$element["id_pere"].',fld'.$id.');if (typeof(add'.$id.')!="undefined") add'.$id.'.xID="'.$id.'";
';

				 }
            }

        echo "\ninitializeDocument();\n";
        echo $selectNodes;

        unset($selectNodes);
			echo "//-->\n";
            echo "</script>\n";
            echo "</div>\n";

        $output=ob_get_contents();

        ob_end_clean();

		if ($print) echo $output; else return $output;

	}


function highlight($str) {

	$key=$this->objSearch->tabHighlight[$this->objSearch->highlightedFieldInHierarchy];
    if (!$key) return $str;
	$key="`(.*?)(".str_replace('%','',$key[0]).")(.*?)`siu"; //on cherche tout en virant le % s'il existe)
	$sortie='$1<span class=\"highlight1\">$2</span>$3';
	$hled=@preg_replace($key,$sortie,$str);
	return $hled;

}

}
?>