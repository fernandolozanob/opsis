<?php
include_once(libDir."class_visualisation.php");

class VisualisationFlash extends Visualisation {

	var $typeVisu;

	function __construct ($type) {

		$this->typeVisu=$type;
		$this->arrFiles=array(
					array('IN'=>'player.swf','OUT'=>'player.swf')
		);

	}




	/** Récupère et prépare le visionnage et le découpage en fonction du type de visionnage
	 */

	function prepareVisu($id,$id_lang) {

				// Type séquence
				include_once(modelDir.'model_doc.php');

				$search = array(" ' ",'"',"&","<",">");
				$replace = array("'","&quot;","&amp;","&lt;","&gt;");

				$this->objDoc=new Doc();
				$this->objDoc->t_doc['ID_DOC']=$id;
				$this->objDoc->t_doc['ID_LANG']=$id_lang;
				$this->objDoc->getDoc();
				$this->objDoc->getMats(); //récup ID séquences liées
				$this->objDoc->getChildren(); //récup matériel

				//A partir de là, on peut extraire tous les TC nécessaires.
				//D'abord il faut récupérer le matériel de visualisation
				$gotOne=false;
				$ok=$this->makeDir();
				if ($ok) $ok=$this->copyRefFiles();
				
				// VP (18/04/08) : Tri du tableau multidim par création d'un tableau intermédiaire
				foreach ($this->objDoc->t_doc_mat as $key => $dm) {
					$tmpsort[$key]=$dm["ID_MAT"];
				}
				array_multisort($tmpsort,SORT_ASC,$this->objDoc->t_doc_mat);
				unset($tmpsort);
				
				foreach ($this->objDoc->t_doc_mat as $dm) {
					// Seulement les fichiers MP3
					if (in_array($dm['MAT']->t_mat['MAT_FORMAT'],array("MP3")) && $dm['MAT']->hasfile==1) {
						//ATTENTION : pour le Flash, on peut avoir PLUSIEURS MATERIELS (=> playlist);

						$this->id_mat=$dm['ID_MAT'];
						$this->tcin=$dm['DMAT_TCIN'];
						$this->tcout=$dm['DMAT_TCOUT'];
						$this->objMat=$dm['MAT'];

						$numer=tcToSec($this->tcout)-tcToSec($this->tcin);  //durée de l'extrait
						$denom=tcToSec($this->objMat->t_mat['MAT_TCOUT'])-tcToSec($this->objMat->t_mat['MAT_TCIN']); //durée totale du matériel
						// cas de découpage : pas de durée du matériel (dans le doute on découpe) ou bien ratio inférieur à la limite)
						// sinon on ne découpe pas : ratio supérieur, durée extrait inconnue,...
						// VP 28/12/09 : correction découpage systématique
						//if (($denom==0  || $numer/$denom<gRatioDecoupageVideo ) && $numer!=0 ) $decoup=true;
						// VP 6/05/10 : suppression critère gRatioDecoupageVideo
						//$decoup =(($denom==0  || $numer/$denom<gRatioDecoupageVideo ) && $numer!=0 );
						$decoup =(($denom==0  || $numer/$denom!=1 ) && $numer!=0 );
						//trace("numer=$numer denom=$denom ratio=".$numer/$denom." gRatioDecoupageVideo=".gRatioDecoupageVideo);
						$this->offset=(empty($this->objMat->t_mat['MAT_TCIN'])?"00:00:00:00":$this->objMat->t_mat['MAT_TCIN']);

						//Préparation du découpage


						if ($ok && $decoup ) {
							// VP 5/05/10 : appel des outils de découpage dans méthode sliceMovie
							$ok=$this->sliceMovie();

							$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;



						} else { //Pas de découpage, on fait une copie simple du media

							$this->makeFileName(true);

							$ok=$this->copyMovie();

							//$this->mediaUrl=videosServer.$this->id_mat;

							$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;



						}

						// Ajout playList

						$this->XML.="<song path=\"".$this->mediaUrl."\" title=\"".str_replace($search,$replace,$this->objDoc->t_doc['DOC_TITRE'])."\" />";

						$gotOne=true;



					}

				}

				if (!$gotOne) {

					$this->dropError(kErrorVisioNoMedia);

					return false;

				}

				else {

					if ($ok) $ok=$this->makeXML(); // génération du XML

	

					if (!$ok) return false; else {

						if ($this->objDoc) logAction("VIS",Array("ID_DOC"=>$this->objDoc->t_doc['ID_DOC'], "ACT_REQ" => "DOC"));

						else logAction("VIS",array("ID_DOC"=>$this->objMat->t_mat['ID_MAT'], "ACT_REQ" => "MAT"));

						return true;

					}

				}



	}



	/** Création et remplissage du fichier XML interprété par ORAO

	 *  Selon les types de visio et le découpage, le contenu du fichier diffère

	 *  IN : objet Visualisation

	 *  OUT : fichier XML écrit dans le répertoire temporaire + true si succès / false si échec

	 */

	function makeXML() {



		$handle=fopen($this->tempDir."liste.xml",w);

		if (!$handle) {

			$this->dropError(kErrorVisioCreationXML);

			return false;

		}



			fwrite($handle,'<?xml version="1.0" encoding="UTF-8"?>');

			fwrite($handle,'<player showDisplay="yes" showPlaylist="no" autoStart="yes">');

			fwrite($handle,$this->XML);

			fwrite($handle,'</player>');

			fclose($handle);



		return true;

	}





	/** Création du code HTML pour lancer le visionnage

	 *  IN :

	 * 	OUT :

	 */

	function renderComponent($print=true) {

		$html="<object type=\"application/x-shockwave-flash\" data=\"".kVisionnageUrl.$this->tempDirShort."/player.swf?playlist=".kVisionnageUrl.$this->tempDirShort."/liste.xml\" width=\"264\" height=\"40\">

				<param name=\"movie\" value=\"".kVisionnageUrl.$this->tempDirShort."/player.swf?playlist=".kVisionnageUrl.$this->tempDirShort."/liste.xml\" />

				</object>";

			if ($print) echo $html; else return $html;



	}





}

?>

