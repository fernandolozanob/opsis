<?php
if(defined("gSiteVersionMobile") && gSiteVersionMobile!="" && gSiteVersionMobile!="gSiteVersionMobile" && empty($_SESSION['demande_redirection_mobile'])){
    $_SESSION['demande_redirection_mobile']=1; // Flag demande_redirection_mobile
    $browser_user_agent = ( isset( $_SERVER['HTTP_USER_AGENT'] ) ) ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) : '';
    //trace("demande_redirection_mobile HTTP_USER_AGENT=".$_SERVER['HTTP_USER_AGENT']." HTTP_REFERER=".$_SERVER['HTTP_REFERER']);
    if((strpos($browser_user_agent,'iphone')>0 || strpos($browser_user_agent,'ipad')>0 || strpos($browser_user_agent,'ipod')>0 || strpos($browser_user_agent,'android')>0 || strpos($browser_user_agent,'windows phone')>0) && (empty($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'],gSiteVersionMobile)===false)){
        session_write_close();
    ?>
    <script type="text/javascript">
        if (confirm('<?php echo kVoulezVousEtreRedirigeMobile; ?>')){
            window.location.href='<?php echo gSiteVersionMobile; ?>';
        }else{
            window.location.href=document.location.href;
        }
    </script>
<?
    }
}
?>
