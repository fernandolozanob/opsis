<?php 


// class toolbox pour communication avec l'API V3 de youtube
Class SC{
	var $username;
	var $password;
	var $api_key;
	var $api_secret;
	var $api;
	var $type_user;

	function __construct(){

	}
	
	function setDocXsl($doc_xsl){
		if(!file_exists($doc_xsl)){
			$this->dropError("Erreur - fichier doc_xsl non trouvé");
			return false ; 
		}else{
			$this->doc_xsl = $doc_xsl;
			
			return true ; 
		}	
	}
	

	
	// récupération des paramètres de login depuis une etape
	function getLogParamsFromEtape($etape_ref=null){
		global $db ;
		if(isset($etape_ref) && !empty($etape_ref) ){
			$id_etp = intval($etape_ref);
		}elseif(isset($this->etape_ref) && !empty($this->etape_ref)){
			$id_etp = $this->etape_ref;
		}
		
		$etape_arr = $db->GetRow("SELECT * from t_etape where id_etape=".intval($id_etp));
		
		if(!isset($etape_arr) || empty($etape_arr) ||   !isset($etape_arr['ETAPE_PARAM'])){
			$this->dropError("Erreur recupération identifiants depuis etape");
			return false ; 
		}
		$tab_xml=xml2tab($etape_arr['ETAPE_PARAM']);
		$params_job = $tab_xml['PARAM'][0];
		$this->api_key      = $params_job['API_KEY'];
		$this->api_secret  = $params_job['API_SECRET'];
		$this->username = $params_job['USER'];
		$this->password = $params_job['PASS'];
		if(!empty($params_job['PRIVATE'])) $this->private = $params_job['PRIVATE'];
		if(empty($this->doc_xsl)){
			$this->setDocXsl(getSiteFile("designDir","print/".$params_job['DOC_XSL']));
		}
		
		
		if(!empty($this->username) && !empty($this->api_secret) && !empty($this->api_key)){
			return true ;		
		}else{
			$this->dropError("Error - La recuperation des params de connexion a echouee");
			return false ; 
		}
	}
	
	// connexion soundcloud obligatoire avant n'importe quelle autre action
	// necessite que les informations client_id & client_secret soit définis (voir getLogParamsFromEtape, ou implémenter une autre méthode)
	function connectSC(){
		require_once(libDir.'soundcloud_sdk/Soundcloud.php');
		if(!isset($this->api_key) || empty($this->api_key) || !isset($this->api_secret) || empty($this->api_secret)){
			$this->dropError("Erreur connexion soundcloud - paramètres manquants");
			return false ; 
		}
		$login =$this->username;
		$pass =$this->password;
		$this->api =  new Services_Soundcloud($this->api_key,$this->api_secret);

		try
		{
			$accessToken=$this->api->credentialsFlow($login,$pass);
		}	
		catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e)
		{
			throw new InvalidXmlParamException('Soundcloud authentification error : '.$e->getMessage(),$e->getCode());
			trace('Soundcloud authentification error : '.$e->getMessage(),$e->getCode());

		}

	
		// on recupere le type de compte de l'utilisateur
		// thumbnail_url necessite un compte official ou motionmaker
		try
		{
			$this->type_user=json_decode($this->api->get('me'),true);
		}
		catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e)
		{
			throw new Exception('Soundcloud API - GET /me : '.$e->getMessage(),$e->getCode());			
			trace('Soundcloud API - GET /me : '.$e->getMessage(),$e->getCode());
		}
			trace('connexion sc');
			trace(print_r($this->type_user,true));
			$this->user_id=$this->type_user['id'];
	}
	

	
	
	

	
	function publish_track($path,$params_job=null){
		// upload de la video
		if ($params_job['donnees'][0]['private']=='true' || $params_job['donnees'][0]['private']=='1' ||$params_job['private']=='true' || $params_job['private']=='1')
			$sharing='private';
		else
			$sharing='public';
		if(isset($params_job['donnees'][0]['tags']) && !empty($params_job['donnees'][0]['tags']))
		{
			$tags=explode(',',$params_job['donnees'][0]['tags']);
			if(count($tags)==1)
				$tags = $params_job['donnees'][0]['tags'];
		}
		else
			$tags=array();
		$track=array
		(
			'track[asset_data]'=> new CurlFile( $path ),
			'track[title]'=>$params_job['donnees'][0]['title'],
			'track[description]' =>$params_job['donnees'][0]['description'],
			'track[tag_list]' => $params_job['donnees'][0]['tags'],
			'track[sharing]'=>$sharing
		);
		 if(!empty($params_job['donnees'][0]['thumbnail_url'])){
		  	$vignette = $params_job['donnees'][0]['thumbnail_url'];	
		  	//si tif >> transfo jpg car soundcloud les refuse
		 	if(getExtension($vignette)== 'tif' || getExtension($vignette)== 'tiff'){
		 	 	$vignette_tif = $vignette.'.jpg';
		 	 	$vignette_tif=str_replace('doc_acc/0000', 'thumbnails', $vignette_tif);
			 	if (extension_loaded('imagick')) {
					$image=new Imagick();
					$image->readImage($vignette);
					$image->setImageFileName($vignette_tif);
					$image->writeImage();
					$image->clear();
					$image->destroy();
				} 
				$vignette=$vignette_tif;
		 	 }

            $track['track[artwork_data]'] = new CurlFile( $vignette );
        }

		 try{

		 	$tracks=$this->api->post('tracks',$track);
		 	$tracks_json=json_decode($tracks);
			if(empty($tracks_json)) throw new Exception($tracks);
		 	trace('tracks_json');trace(print_r($tracks_json,true));
		 	trace('user id ');trace($this->user_id);
		 	trace('chanson id'.$tracks_json->id);
		 //   $info_track=json_decode($this->api->get('tracks'),true);
			$this->id_audio_soundcloud=$tracks_json->id;
			if($sharing == 'private'){
				$this->private_id = $tracks_json->secret_token;
			}
			 }
			 catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) {
			     echo 'Exception : '.$e->getMessage();
				return false ;
			 }


	}

	
	function update_track($id_doc=null,$etape=null){
		global $db;
		if(!isset($id_doc) || empty($id_doc)){
			$this->dropError("Erreur update_track sur soundcloud, pas d'id_doc défini ");
			return false ; 
		}
		if(!empty($id_doc)){
			require_once(modelDir.'model_doc.php');
			$myDoc = new Doc();
			$myDoc->t_doc['ID_DOC'] = $id_doc;
			$myDoc->getDoc();
			$myDoc->getDocMat();
			$myDoc->getValeurs();
			$myDoc->getDocPublication();
			$myDoc->getMats(true);
			
			if(empty($myDoc->t_doc_publication)){
				$this->dropError("Erreur, pas de publication détectée pour la notice n:".$id_doc);
				return false ;
			}
			foreach($myDoc->t_doc_publication as $dpub){
				if($dpub['PUB_CIBLE'] == 'soundcloud' && $dpub['PUB_ID_ETAPE'] == $etape){
					$id_sc = $dpub['PUB_VALUE'];
					break ; 
				}
			}
			if( !isset($id_sc) || empty($id_sc)){
				$this->dropError("Erreur récupération de l'id soundcloud de la notice");
				return false ; 
			}
			
			try
			{			
				/*------------- update données depuis Opsis ---------*/
				$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$myDoc->xml_export()."</EXPORT_OPSIS>";
				$xml_params = TraitementXSLT($xml,$this->doc_xsl,null,0,"",null,false);
				
				$xml_tab = xml2tab($xml_params);
				$sc_donnees = $xml_tab['DONNEES'][0];
				$donnees_audio=array
				(
					'id'=>$id_sc,
				);
				
				
				if(isset($sc_donnees['TITLE'])){
					$donnees_audio['title'] = $sc_donnees['TITLE'];
				}
				if(isset($sc_donnees['DESCRIPTION']) ){
					$donnees_audio['description'] = $sc_donnees['DESCRIPTION'];
				}
				if(isset($sc_donnees['THUMBNAIL_URL'])){
					$donnees_audio['thumbnail_url'] = kCheminLocalMedia.str_replace("/","_",$sc_donnees['THUMBNAIL_URL']);	
				}		


				if(isset($sc_donnees['PRIVATE']) && !empty($sc_donnees['PRIVATE']) && ($sc_donnees['PRIVATE'] == '1' || $sc_donnees['PRIVATE'] =='true') ||isset($this->private) && !empty($this->private) && ($this->private == '1' || $this->private =='true') ){
						$sharing='private';
				}else{
						$sharing='public';
				}
				
				$track_info_array = array(
									'track[title]'=>$donnees_audio['title'],
									'track[description]' =>$donnees_audio['description'],
									'track[tag_list]' => $sc_donnees['TAGS'],							
									'track[sharing]'=>$sharing
							        );
			 if(!empty($donnees_audio['thumbnail_url'])){
	            $track_info_array['track[artwork_data]'] = new CurlFile( $donnees_audio['thumbnail_url'] );
	        }

			  $track_info = $this->api->put('tracks/'.$id_sc, $track_info_array);
			 }
			 catch(Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) {
			     	echo 'Exception : '.$e->getMessage();
					return false ;
			 }

	}
}	
	function delete_track($id_doc,$etape=null){
			global $db;
		if(!isset($id_doc) || empty($id_doc)){
			$this->dropError("Erreur update_track sur soundcloud, pas d'id_doc défini ");
			return false ; 
		}
		if(!empty($id_doc)){
			require_once(modelDir.'model_doc.php');
			$myDoc = new Doc();
			$myDoc->t_doc['ID_DOC'] = $id_doc;
			$myDoc->getDoc();
			$myDoc->getDocMat();
			$myDoc->getValeurs();
			$myDoc->getDocPublication();
			$myDoc->getMats(true);
			
			if(empty($myDoc->t_doc_publication)){
				$this->dropError("Erreur, pas de publication détectée pour la notice n:".$id_doc);
				return false ;
			}
			foreach($myDoc->t_doc_publication as $dpub){
				if($dpub['PUB_CIBLE'] == 'soundcloud' && $dpub['PUB_ID_ETAPE'] == $etape){
					$id_sc = $dpub['PUB_VALUE'];
					break ; 
				}
			}
			if( !isset($id_sc) || empty($id_sc)){
				$this->dropError("Erreur récupération de l'id soundcloud de la notice");
				return false ; 
			}
			$ok= $this->api->delete('tracks/'.$id_sc);
		}
	}
	
	
	function dropError($errLib) { 
		$this->error_msg.=$errLib."<br/>";
	}
	
	
	
	

}


?>