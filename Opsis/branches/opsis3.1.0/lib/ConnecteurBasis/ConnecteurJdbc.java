import java.util.*;
import java.io.*;
import java.sql.*;
import static java.nio.charset.StandardCharsets.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

/*
Compilation :
javac XmlGen.java;
javac -cp basisjdbc.jar:log4j.jar BasisConnect.java
javac ConnecteurJdbc.java

Execution
java "-Dfile.encoding=UTF-8" -classpath basisjdbc.jar ConnecteurJdbc <arguments>
*/


public class ConnecteurJdbc
{
	private static final String USAGE="Usage : java ConnecteurJdbc conf=<fichier_configuration> db=<base_modele> query=<requete_sql> [highlight=false] [page=1] [nb_lignes=10]";
	
	public static void main(String[] args)
	{
		String[][] valeurs_args=parseArgs(args);
		Properties params_conf=null;
		
		String fichier_config="";
		String db="";
		String[] list_requetes;
		String requete="";
		boolean highlight=false;
		int nb_lignes=10;
		int page=1;
		
		ResultSet resultat_requete;
		ResultSetMetaData meta_data;
		XmlGen xml;
		int compteur=0;
		int total_lignes=0;
		int j;
		String nom_champ,attr,texte;
		
		int nb_query=0;
		boolean resultset_not_scrollable=false;

		
		if (valeurs_args==null)
		{
			System.out.println(USAGE);
			System.exit(-1);
		}
		
		//System.out.println("Execution requete");
		
		for (int i=0;i<valeurs_args.length;i++)
		{
			if (valeurs_args[i][0].compareTo("query")==0)
				nb_query++;
		}
		
		list_requetes=new String[nb_query];
		nb_query=0;
		
		for (int i=0;i<valeurs_args.length;i++)
		{
			if (valeurs_args[i][0].compareTo("conf")==0)
				fichier_config=valeurs_args[i][1];
			else if (valeurs_args[i][0].compareTo("db")==0)
				db=valeurs_args[i][1];
			else if (valeurs_args[i][0].compareTo("query")==0)
			{
				list_requetes[nb_query]=valeurs_args[i][1];
				nb_query++;
			}
			else if (valeurs_args[i][0].compareTo("highlight")==0 && valeurs_args[i][1].compareTo("true")==0)
				highlight=true;
			else if (valeurs_args[i][0].compareTo("nblignes")==0)
				nb_lignes=Integer.parseInt(valeurs_args[i][1]);
			else if (valeurs_args[i][0].compareTo("page")==0)
				page=Integer.parseInt(valeurs_args[i][1]);
		}
		
		try
		{
			params_conf=parseConfigFile(fichier_config);
		}
		catch (Exception e)
		{
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		
		try
		{
		
			BasisConnect bc=new BasisConnect
			(
				params_conf.getProperty("host.name"),
				params_conf.getProperty("host.port"),
				params_conf.getProperty("host.user"),
				params_conf.getProperty("host.password"),
				params_conf.getProperty("user"),
				params_conf.getProperty("password"),
				db,
				"general",
				params_conf.getProperty("database.version"),
				Integer.parseInt(params_conf.getProperty("timeout"))
			);

			if (highlight)
				bc.highlightResults("&lt;span class=\"highlight1\"&gt;", "&lt;/span&gt;");
			
			
			xml=new XmlGen();
			
			xml.push("datas");
			
			for (int k=0;k<list_requetes.length;k++)
			{
				resultat_requete=null;
				requete=list_requetes[k];
				
				resultat_requete=bc.executeQuery(requete);
				meta_data=resultat_requete.getMetaData();
				
				resultset_not_scrollable=false;
				compteur=0;
				total_lignes=0;
				
				xml.push("data");
			
				// rappel de la requete
				xml.element("query",requete.replace("<", "&lt;").replace(">", "&gt;"));
			
				try
				{
					resultat_requete.last();
					total_lignes=resultat_requete.getRow();
					resultat_requete.beforeFirst();
					resultat_requete.absolute((page-1)*nb_lignes);
				}
				catch (SQLException e)
				{
					//System.err.println("SCROLL : "+e.getMessage());
					resultset_not_scrollable=true;
			
					for (j=0;j<((page-1)*nb_lignes) && resultat_requete.next();j++);
	
					total_lignes+=j;
				}
			
			
				while ((resultat_requete.next())==true)
				{
					compteur++;
				
					if (resultset_not_scrollable)
						total_lignes++;
				
					if (compteur>nb_lignes)
						break;
				
					xml.push("row");
				
				
					for(int i=1;i<=meta_data.getColumnCount();i++)
					{
						nom_champ=meta_data.getColumnName(i);
						attr="";
						texte=resultat_requete.getString(i);
						// Il faudrait éventuellement wrap tout ces échappements dans un try/catch, mais je ne sais pas trop où indiquer/stocker/tracer un crash de cet échappement
						if(texte != null ){
							String texte_tmp = new String(texte);
							// L'idée est de forcer l'interpretation UTF8 de la chaine
							Charset charset = Charset.forName("UTF-8");
							texte_tmp = charset.decode(charset.encode(texte_tmp)).toString();
							// Mais ce n'était pas suffisant pour traiter certains cas, on ajoute donc un replace regex des control characters qui ne sont PAS des espaces => permet d'éviter des control character étrange, et donc d'éviter qu'ils ne fassent exploser le XML généré par ce connecteur
							texte_tmp = texte_tmp.replaceAll("[\\p{Cc} && [^\\p{Space}]]", " ");
							texte = texte_tmp ;
						}
						
						if (texte!=null)
							xml.element(nom_champ, texte,attr);
						else
							xml.element(nom_champ,"",attr);
					}
				
				
					xml.pop();
				}
			
				if (resultset_not_scrollable)
				{
					while (resultat_requete.next())
						total_lignes++;
				}
				
				resultat_requete=null;
			
				xml.element("nb_lignes",""+total_lignes);
			
				xml.pop();
			}
			
			xml.pop();
			System.out.println(xml.getXml());
			
			bc.close();
		}
		catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
	}
	
	private static String[][] parseArgs(String[] args)
	{
		boolean args_valid=true;
		int nb_args_oblig_present=0;
		
		String[][] valeurs_args=new String[args.length][];
		
		// on verifie qu'il y a des parametres
		if (args.length<3)
			args_valid=false;
		
		if (args_valid)
		{
			// on parse les parametres et on verifie qu'ils ont le bon format
			for (int i=0;i<args.length;i++)
				valeurs_args[i]=args[i].split("=",2);
			
			for (int i=0;i<valeurs_args.length;i++)
			{
				if (valeurs_args[i].length<2)
					args_valid=false;
			}
		}
		
		if (args_valid)
		{
			// on verifie que les arguments obligatoires son bien la.
			for (int i=0;i<valeurs_args.length;i++)
			{
				if (valeurs_args[i][0].compareTo("conf")==0 || valeurs_args[i][0].compareTo("db")==0 || valeurs_args[i][0].compareTo("query")==0)
					nb_args_oblig_present++;
			}
			
			if (nb_args_oblig_present<3)
				args_valid=false;
		}
		
		if (args_valid)
			return valeurs_args;
		
		return null;
	}
	
	private static Properties parseConfigFile(String fichier) throws Exception
	{
		Properties properties = new Properties();
		InputStream fd = new FileInputStream(fichier);
		try
		{
			properties.load(fd);
		}
		finally
		{
			fd.close();
		}
		
		return properties;
	}
	
}
