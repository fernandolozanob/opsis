public class XmlGen
{
	private String chaine_xml;
	private String char_indentation="  ";
	private String doctype="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	private int niveau;
	private String[] stack;
	
	public XmlGen()
	{
		niveau=0;
		chaine_xml=doctype;
		stack=new String[50];
	}
	
	public void push(String nom_element)
	{
		indente();
		stack[niveau]=nom_element;
		chaine_xml+="<"+nom_element+">\n";
		niveau++;
	}
	
	public void pop()
	{
		niveau--;
		indente();
		chaine_xml+="</"+stack[niveau]+">\n";
	}
	
	public void element(String nom_element,String valeur,String attributs)
	{
		indente();
		
		if (attributs!="")
			attributs=" "+attributs;
		
		if (valeur=="")
			chaine_xml+="<"+nom_element+""+attributs+" />\n";
		else
			chaine_xml+="<"+nom_element+""+attributs+">"+escapeChar(valeur)+"</"+nom_element+">\n";
		niveau++;
		niveau--;
	}
	
	public void element(String nom_element,String valeur)
	{
		element(nom_element,valeur,"");
	}
	
	public void element(String nom_element)
	{
		element(nom_element,"");
	}
	
	private void indente()
	{
		for (int i=0;i<niveau;i++)
		{
			chaine_xml+=char_indentation;
		}
	}
	
	public String escapeChar(String str)
	{
		str=str.replace("&", "&amp;");
		str=str.replace("<", "&lt;");
		str=str.replace(">", "&gt;");
		
		return str;
	}
	
	public String getXml()
	{
		return chaine_xml;
	}
}
