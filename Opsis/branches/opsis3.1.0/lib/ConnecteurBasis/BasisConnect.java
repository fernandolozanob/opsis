import java.sql.*;
import java.util.*;

import com.opentext.basis.jdbc.BasisResultSet;
import com.opentext.basis.jdbc.BasisStatement;


public class BasisConnect
{
	private String error;
	
	private Properties properties;
	
	private String host;
	private String port;
	private String host_user;
	private String host_pass;
	private String user;
	private String password;
	private String db_model;
	private String view;
	
	private Connection connexion;
	private Statement decl;
	private ResultSet resultat;
	
	private boolean highlight;
	private String start_tag;
	private String end_tag;
	
	private int timeout;

	public BasisConnect(String p_host,String p_port,String p_host_user,String p_host_pass,String p_user,String p_pass,String p_db_model,String p_view,String p_version,int p_timeout) throws Exception
	{
		host=p_host;
		port=p_port;
		host_user=p_host_user;
		host_pass=p_host_pass;
		user=p_user;
		password=p_pass;
		db_model=p_db_model;
		view=p_view;
		error="Pas d'erreur :)";
		
		highlight=false;
		start_tag="<strong>";
		end_tag="</strong>";

		timeout=p_timeout;
		
		Class.forName("com.opentext.basis.jdbc.BasisDriver");
		//Driver driver = DriverManager.getDriver("jdbc:opentext:basis:");
		
		properties=new Properties();
		properties.setProperty("host.name",host);
		properties.setProperty("host.port",port);
		properties.setProperty("host.user",host_user);
		properties.setProperty("host.password",host_pass);
		properties.setProperty("user",user);
		properties.setProperty("password",password);
		properties.setProperty("database.list",db_model);
		properties.setProperty("database.version",p_version);
		//properties.setProperty("opirpc.timeout",""+timeout);
		//properties.setProperty("opirpc.loginTimeout",""+timeout);
		
		connexion=DriverManager.getConnection("jdbc:opentext:basis:",properties);
		decl=connexion.createStatement();
	}
	
	public void highlightResults(String start,String end)
	{
		start_tag=start;
		end_tag=end;
		highlight=true;
	}
	
	public ResultSet executeQuery(String requete) throws SQLException
	{
		ResultSet brs;
		
		((BasisStatement)decl).setQueryTimeout(timeout);
		brs=decl.executeQuery(requete);
		
		if (highlight==true)
		{
			((BasisResultSet)brs).setHitHighlighting(highlight);
			((BasisResultSet)brs).setHitStartTag(start_tag);
			((BasisResultSet)brs).setHitEndTag(end_tag);
		}
		
		return brs;
	}
	
	public int testQuery() throws SQLException
	{
		ResultSetMetaData rsmd=null;
		
		String query="SELECT * FROM "+view;
		resultat=decl.executeQuery(query);
		rsmd=resultat.getMetaData();
		return rsmd.getColumnCount();
	}
	
	public void close() throws SQLException
	{
		if (decl!=null)
			decl.close();
			
		if (connexion!=null)
			connexion.close();
	}
}
