<?php

class InvalidXmlParamException extends Exception
{
	function __construct($message,$code=0)
	{
		parent::__construct('Invalid XML parameter : '.$message,$code);
	}
}

?>