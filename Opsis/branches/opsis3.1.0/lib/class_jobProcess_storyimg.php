<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_directoryException.php');
require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_uploadException.php');

require_once(libDir.'class_matInfo.php');
require_once(modelDir.'model_materiel.php');

class JobProcess_storyimg extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('storyimg');
		
		$this->required_xml_params=array
		(
			
		);
		
		$this->optional_xml_params=array
		(
			'largeur'=>'int',
			'hauteur'=>'int'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		if (is_dir(dirname($this->getFileOutPath()))===false)
		{
			if (mkdir(dirname($this->getFileOutPath()))===false)
				throw new DirectoryException('failed to create storyboard directory ('.dirname($this->getFileOutPath()).')');
		}
		
		$params_job=$this->getXmlParams();
		
		$height=320;
		$width=240;
		
		$fileSrc=$this->getFileInPath();
		
		$infos=MatInfo::getMatInfo($fileSrc);
		
		if(!isset($infos['id_media']) || $infos['id_media'] != 'P'){
			throw new Exception('Storyimg error : '.kStoryimgWrongFileType);
		}
		
		if($dim=GetImageSize($fileSrc)){
			list($src_w,$src_h) = $dim;
		}else{
			list($src_w,$src_h)=array($infos['width'],$infos['height']);
		}
		
		if ((isset($params_job['largeur']) && !empty($params_job['largeur'])) && (isset($params_job['hauteur']) && !empty($params_job['hauteur'])))
		{
			$height=intval($params_job['hauteur']);
			$width=intval($params_job['largeur']);
		}else if ((isset($params_job['largeur']) && !empty($params_job['largeur'])) && !(isset($params_job['hauteur']) && !empty($params_job['hauteur'])))
		{
			$width=intval($params_job['largeur']);
			$height=intval(round(($params_job['largeur']*$src_h)/$src_w));
		}else if (!(isset($params_job['largeur']) && !empty($params_job['largeur'])) && (isset($params_job['hauteur']) && !empty($params_job['hauteur'])))
		{
			$height=intval($params_job['hauteur']);
			$width=intval(round(($params_job['hauteur']*$src_w)/$src_h));
		}else{
			$dst_h_temp = round(($width / $src_w) * $src_h);
			$dst_w_temp = round(($height / $src_h) * $src_w);
			if($dst_h_temp > $height) $width = $dst_w_temp;
			elseif($dst_w_temp > $width) $height = $dst_h_temp;
		}
		
		$fileOut=$this->getFileOutPath()."_".$width."x".$height.".jpg";
		
		if (!extension_loaded('imagick') && !defined("kConvertPath")) {throw new UploadException('Storyimg error : '.kExtensionImagickNotLoaded);return false;}
		if (extension_loaded('imagick') && empty($params_job['imagick_cli'])) { //par extension
			
			Imagick::setResourceLimit (6, 1);
			
			$image=new Imagick();
			$image->readImage($fileSrc);
			$image->resizeImage($width,$height,Imagick::FILTER_LANCZOS,1);
			$image->setImagePage($width, $height, 0, 0);
			$image->setImageFileName($fileOut);
			$image->writeImage();
			$image->clear();
			$image->destroy();
		} else { //par ligne de commande
			$this->shellExecute(kConvertPath,escapeQuoteShell($fileSrc)." -auto-orient -thumbnail ".$width."x".$height." -unsharp 0x.5 ".escapeQuoteShell($fileOut));
		}
		
		$this->setProgression(100,'Génération storyboard');
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
