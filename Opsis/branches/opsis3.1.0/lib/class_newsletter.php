<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Opsis; // Newsletter est un nom de classe commun, qui pourrait se retrouver dans plusieurs librairies externes

require_once(libDir . 'mailchimp-api-master/src/MailChimp.php');

class Newsletter {

	public $apikey;
	public $mailChimp;

	const TIMEOUT_CURL = 0;

	public function __construct($apikey) {
		$this->apikey = $apikey;
		$this->mailChimp = new \DrewM\MailChimp\MailChimp(MailChimpApiKey);
	}

	public function getLists() {
		return $this->mailChimp->get('lists', array(), self::TIMEOUT_CURL);
	}

	public function getOneList($id_list) {
		return $this->mailChimp->get('lists/' . $id_list, array(), self::TIMEOUT_CURL);
	}

	/*
	 * 1) Retourne array('OK'=>id_list)  si aucun doute sur la liste d'email ACTF
	 * 
	 * 2) Retourne array('KO'=> array de message d'erreur(string)) SI : 
	 * 	2.1) si 1 seule liste trouvé mais failure de mailChimp(mauvais format ..etc)
	 * 	2.2) si 1 seule liste trouvé, mais ne correspond pas à la constante MailChimpSuscribersList
	 * 
	 * 	2.3)  si plusieurs listes de mail sur le compte mailChimp (du client) mais on n'y trouve pas la constante MailChimpSuscribersList définie
	 *  2.4)  si plusieurs listes de mail sur le compte mailChimp (du client) mais la constante MailChimpSuscribersList n'a pas été définie, dans ce cas on ne	*     *	 sait pas laquelle on doit choisir

	 */


	/* l'user peut à fois se désinscrire via le mail, mais également pour se réabonner (le client doit lui envoyer un mail de confirmation), il le fait via un mail de mail chimp */

	public function getSubscriptionChange($id_list = NULL) {
		$return_GetActiveList = $this->getActiveList($id_list);

		if (array_key_exists('OK', $return_GetActiveList)) {
			$idListGot = $return_GetActiveList['OK'];
		} else {
			$error_returnBASE_GetActiveList = array();

			if (!array_key_exists('KO', $return_GetActiveList)) {
				$error_returnBASE_GetActiveList['KO'][0] = 'erreur non determiné';
				return $error_returnBASE_GetActiveList;
			} else {
				return $return_GetActiveList;
			}
		}

		$tabReturn = array();

		$tab_count = $this->mailChimp->get('lists/' . $idListGot . '/members?count=1', array(), self::TIMEOUT_CURL);
		$tab_all = $this->mailChimp->get('lists/' . $idListGot . '/members?count=' . (int) $tab_count["total_items"], array(), self::TIMEOUT_CURL);


		if (is_array($tab_all["members"])) {
			foreach ($tab_all["members"] as $v) {
				if (trim($v['status']) == 'unsubscribed') {
					$tabReturn['OK']['unsubscribed'][] = $v['email_address'];
				} elseif (trim($v['status']) == 'subscribed') {
					$tabReturn['OK']['subscribed'][] = $v['email_address'];
				}
			}
		} else {
			$tabReturn['KO'] = '"ERREUR(mailChimp) : Problème de réponse de MailChimp sur les listes (problème de structure)';
		}

		return $tabReturn;
	}

	/* Si l'user clique sur le lien désabonner de son mail reçu de newsletter, il sera désabonné de mailChimp pas d'opsis
	  1) Retourne array('OK'=>array(unsuscribed@gmail.com,unsuscribed2@gmail.com)  si aucun doute sur la liste d'email ACTIF
	 * 	2) Retourne array('KO'=> array de message d'erreur(string))
	 * 	 */

	public function getUnsuscribedMembers($id_list = NULL) {
		$return_GetActiveList = $this->getActiveList($id_list);

		if (array_key_exists('OK', $return_GetActiveList)) {
			$idListGot = $return_GetActiveList['OK'];
		} else {
			$error_returnBASE_GetActiveList = array();

			if (!array_key_exists('KO', $return_GetActiveList)) {
				$error_returnBASE_GetActiveList['KO'][0] = 'erreur non determiné';
				return $error_returnBASE_GetActiveList;
			} else {
				return $return_GetActiveList;
			}
		}

		$tabReturn = array();
		$tab_all = $this->mailChimp->get('lists/' . $idListGot . '/members', array(), self::TIMEOUT_CURL);


		if (is_array($tab_all["members"])) {
			foreach ($tab_all["members"] as $v) {
				if (trim($v['status']) == 'unsubscribed') {
					$tabReturn['OK'][] = $v['email_address'];
				}
			}
		} else {
			$tabReturn['KO'] = '"ERREUR(mailChimp) : Problème de réponse de MailChimp sur les listes (problème de structure)';
		}

		return $tabReturn;
	}

	public function getActiveList($id_list = NULL) {
		$tabReturn = array();

		if (empty($id_list)) {

			$MailChimpSuscribersList = null;
			if (defined('MailChimpSuscribersList')) {
				$MailChimpSuscribersList = trim(constant('MailChimpSuscribersList'));
			}


			$tab_list = $this->getLists();

			$idListGot = false;


			if (is_array($tab_list["lists"])) {

				if (count($tab_list["lists"]) == 1) {

					$idListGot = trim($tab_list["lists"][0]['id']);

					if (!empty($idListGot)) {
						if (!empty($MailChimpSuscribersList) && $MailChimpSuscribersList != $idListGot) {
							$tabReturn['KO'][] = "ERREUR(mailChimp) :  l'id de l'unique liste existante ne correspond pas à votre valeur de MailChimpSuscribersList";
						} else {

							$tabReturn['OK'] = $idListGot;
						}
					} else {
						$tabReturn['KO'][] = "ERREUR(mailChimp) : Impossible de trouver l'id de l'unique liste existante";
					}
				} elseif (count($tab_list["lists"]) > 1) {

					//on peut pas !empty(constant('MailChimpSuscribersList')	=>   Fatal error: Can't use function return value in write context 
//					if (defined('MailChimpSuscribersList') && !empty(constant('MailChimpSuscribersList'))) {

					if (!empty($MailChimpSuscribersList)) {

						foreach ($tab_list["lists"] as $value) {
							$tab_id = trim($value['id']);
							if ($tab_id == $MailChimpSuscribersList && !empty($tab_id)) {

								$idListGot = $tab_id;
								break;
							}
						}

						if (!$idListGot) {
							$tabReturn['KO'][] = "ERREUR(mailChimp) : la constante MailChimpSuscribersList spécifié n'a pas été trouvé dans les listes du compte mailChimp";
						} else {
							$tabReturn['OK'] = $idListGot;
						}
					} else {
						$tabReturn['KO'][] = "ERREUR(mailChimp) : Il existe plusieurs listes rattachés au compte MailChimp, précisez MailChimpSuscribersList";
					}
				}
			} else {
				$tabReturn['KO'][] = "ERREUR(mailChimp) : Problème de réponse de MailChimp sur les listes (problème de structure)";
			}
		} else {
			//TODO, cas !empty($id_list)  peut etre dans l'avenir possibilité de choisir parmi plusieurs liste d'email mais c'est peu probable car mailChimp recommande d'avoir qu'une seule liste (divisés en groupes) pour éviter les doublons d'emails (le prix de facturation de  mailChimp dépend du nb d'email) 
			$tabReturn['KO'][] = "ERREUR(mailChimp) : Fonctionnement non implémenté, possibilité de choisir à la volée parmi plusieurs listes";
		}

		return $tabReturn;
	}

//	Retourne true en cas de success, sinon renvoie 1 string d'erreur
//	On regarde si l'email existe dans la liste, si oui update sinon création
// j'ai mis un array pour tab_user si à l'avenir, on doit rajouter plusieurs champs (nom, prénoms), c'est + pratique....	
	public function save_user($email, $tab_user, $id_list = NULL) {

		$email = trim($email);
		$tab_user = array_map("trim", $tab_user); //attention, ne marche pas pour des arrays d'arrays (seulement 1 dimension)


		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return "ERREUR(mailChimp) :  Paramètre email vide ou au mauvais format";
		}

		$status = strtolower($tab_user['status']);
		if (!in_array($status, array('subscribed', 'unsubscribed'))) {
			return "ERREUR(mailChimp) :  Paramètre status invalide ";
		}

		$return_GetActiveList = $this->getActiveList($id_list);

		if (array_key_exists('OK', $return_GetActiveList)) {
			$idListGot = $return_GetActiveList['OK'];
		} else {

			$error_return_GetActiveListBASE = 'erreur non determiné';

			$error_return_GetActiveList = $this->tabErrorsToString($return_GetActiveList['KO']);
			if (!isset($error_return_GetActiveList)) {
				$error_return_GetActiveList = $error_return_GetActiveListBASE;
			}

			return $error_return_GetActiveList;
		}


		$commande = 'post'; //= insert
		$hashId = '.';
		if ($this->isSuscriberExists($email, $idListGot)) {
			$commande = 'patch'; //=update
			$hashId = $this->getIdSuscriber($email);
		}


		$result = $this->mailChimp->$commande("lists/$idListGot/members/" . $hashId, [
			'email_address' => $email, //ne sert que dans l'insert (on peut pas changer le mail lors de l'update)
			'status' => $tab_user['status'],
			'merge_fields' => [
				'FNAME' => $tab_user['firstname'],
				'LNAME' => $tab_user['lastname']
			],
				], self::TIMEOUT_CURL);

//				ex d'erreur =400, 404
		if (is_numeric($result["status"])) {
			$str_err.="ERREUR(mailChimp) : " . $result["detail"] . '<br />';
			if (is_array($result["errors"])) {
				foreach ($result["errors"] as $value) {
					$str_err.=$value["field"] . ' ' . $value["message"] . '<br />';
				}
			}
			return $str_err;
		}
		return TRUE;
	}

	/*
	 * Renvoie un string d'erreur si succès sinon TRUE
	 */

	public function sendNewsletter($mailSubject, $internalCampaignTitle, $html, $id_panier, $test = FALSE, $testEmail, $id_list = NULL) {
		$tabErrors = array();
		$testEmail = trim($testEmail);
		$return_GetActiveList = $this->getActiveList($id_list);

		if (array_key_exists('OK', $return_GetActiveList)) {
			$idListGot = $return_GetActiveList['OK'];
		} else {

			if (is_array($return_GetActiveList['KO'])) {
				foreach ($return_GetActiveList['KO'] as $v) {
					$error_return_GetActiveList.=$v . ' ';
				}
			}
			return $error_return_GetActiveList;
		}



		if (trim($mailSubject) == '') {
			$tabErrors[] = 'Subject of campaign missing';
		}

		if (trim($internalCampaignTitle) == '') {
			$tabErrors[] = 'Name (internal use) of campaign missing';
		}

		if (!defined('gSite')) {
			$tabErrors[] = 'Constant gSite missing';
		}

		if (!defined('gMailNoReply')) {
			$tabErrors[] = 'Constant gMailNoReply missing';
		} elseif (!filter_var(gMailNoReply, FILTER_VALIDATE_EMAIL)) {
			$tabErrors[] = 'Constant gMailNoReply is not in the right format';
		}

		if (trim($html) == '') {
			$tabErrors[] = 'html not provided to campaign creation';
		}

		if (empty($id_panier)) {
			$tabErrors[] = 'Param id_panier missing';
		}



		if ($test) {

			if (empty($testEmail)) {
				$tabErrors[] = 'Param testEmail missing';
			} else {
				$tab_testEmail = explode(',', $testEmail);

				foreach ($tab_testEmail as $k => $v) {
					$v = trim($v);
					if (!filter_var($v, FILTER_VALIDATE_EMAIL)) {
						$tabErrors[] = "$v is not a valid email address";
					}
					$tab_testEmail[$k] = strtolower(trim($v));
				}
			}
		}


		if (!empty($tabErrors)) {
			return $this->tabErrorsToString($tabErrors);
		}


		if (empty($_SESSION['newsletterMailing']['currentCampaign'][$id_panier])) {

			$createCampaign = $this->mailChimp->post("campaigns", [
				'type' => 'regular',
				'settings' => ['subject_line' => $mailSubject, 'title' => $internalCampaignTitle, 'from_name' => gSite, 'reply_to' => gMailNoReply],
					], self::TIMEOUT_CURL);


			if (!array_key_exists('id', $createCampaign)) {

				if (is_array($createCampaign['errors'])) {
					foreach ($createCampaign['errors'] as $value) {
						$tabErrors[] = 'createCampaign : ' . $value['field'] . ' : ' . $value['message'];
					}
				}

				goto end_sendNewsletter;
			} else {
				//création de la campagne réussi, on crée 1 session sur laquelle on se base pour éviter de créer la campagne plusieurs fois quand on "envoie un test"
				//comme un utilisateur peut créer 1 newsletter par panier, y'a un session par panier
				//nb: quand un newlsetter est envoyé (bouton envoyer la newsletter), l'id_panier n'est plus le même
				$_SESSION['newsletterMailing']['currentCampaign'][$id_panier] = $createCampaign['id'];
			}
		} else {
			$createCampaign = array();
			$createCampaign['id'] = $_SESSION['newsletterMailing']['currentCampaign'][$id_panier];
		}


		//imaginons que l'user envoie un test (session qui se crée) puis change le conf.ini ou le titre de sa newsletter, puis ernvoie un test, il faut mettre à jour la campagne déja crée
		$patchHTML = $this->mailChimp->patch("campaigns/" . $createCampaign['id'], [
			'recipients' => ['list_id' => $idListGot],
			'settings' => ['subject_line' => $mailSubject, 'title' => $internalCampaignTitle, 'from_name' => gSite, 'reply_to' => gMailNoReply],
				], self::TIMEOUT_CURL);


		if (is_array($patchHTML['errors'])) {
			foreach ($patchHTML['errors'] as $value) {
				$tabErrors[] = 'patchCampaign : ' . $value['field'] . ' : ' . $value['message'];
			}
			goto end_sendNewsletter;
		}


		$putHTML = $this->mailChimp->put("campaigns/" . $createCampaign['id'] . '/content ', [
			'html' => $html,
				], self::TIMEOUT_CURL);


		if (is_array($putHTML['errors'])) {
			foreach ($putHTML['errors'] as $value) {
				$tabErrors[] = 'putCampaign : ' . $value['field'] . ' : ' . $value['message'];
			}
			goto end_sendNewsletter;
		}


		if ($test) {
			$sendCampaign = $this->mailChimp->post("campaigns/" . $createCampaign['id'] . '/actions/test ', ['test_emails' => $tab_testEmail, 'send_type' => 'html'], self::TIMEOUT_CURL);
		} else {
			$sendCampaign = $this->mailChimp->post("campaigns/" . $createCampaign['id'] . '/actions/send ', array(), self::TIMEOUT_CURL);
		}
		//ex:   ["status"]=>int(406)
		//		["detail"]=> string(68) "Your Campaign is not ready to send. address(es) - no-reply@colas.com"
		if (is_numeric($sendCampaign["status"])) {
			$tabErrors[] = $sendCampaign["detail"];
			if (is_array($sendCampaign["errors"])) {
				foreach ($sendCampaign["errors"] as $value) {
					$tabErrors[] = 'field : ' . $value["field"] . ' ' . $value["message"];
				}
			}
			goto end_sendNewsletter;
		}

		// bizarrement en cas de succès, $sendCampaign retourne bool(false)

		end_sendNewsletter:
		if (!empty($tabErrors)) {
			return $this->tabErrorsToString($tabErrors);
		}

		return TRUE;
	}

	public function getIdSuscriber($email) {
		return md5(strtolower(trim($email)));
	}

	public function isStatusSuscribed($email) {

		$return_GetActiveList = $this->getActiveList($id_list);

		if (array_key_exists('OK', $return_GetActiveList)) {
			$idListGot = $return_GetActiveList['OK'];
		} else {
			$error_returnBASE_GetActiveList = array();

			if (!array_key_exists('KO', $return_GetActiveList)) {
				$error_returnBASE_GetActiveList['KO'][0] = 'erreur non determiné';
				return $error_returnBASE_GetActiveList;
			} else {
				return $return_GetActiveList;
			}
		}

		$tabReturn = array();

		$tab_user = $this->mailChimp->get('lists/' . $idListGot . '/members/' . $this->getIdSuscriber($email), array(), self::TIMEOUT_CURL);


		if (is_array($tab_user)) {

			if (trim($tab_user['status']) == 'subscribed') {
				return true;
			} else {
				return false;
			}
		} else {
			$tabReturn['KO'] = '"ERREUR(mailChimp) : Problème de réponse de MailChimp sur isStatusSuscribed (problème de structure)';
		}

		return $tabReturn;
	}

	public function isSuscriberExists($email, $id_list) {
		$result = $this->mailChimp->get("lists/$id_list/members/" . $this->getIdSuscriber($email), array(), self::TIMEOUT_CURL);

		if (is_numeric($result ["status"])) {
//		if ($result ["status"] == 404) {
			return false;
		}

		return true;
	}

	public function tabErrorsToString($tab) {

		if (is_array($tab)) {
			foreach ($tab as $v) {
				$s.=$v . '<br />';
			}
		}

		return $s;
	}

	public function test($email) {



//		**************************************** EXEMPLES DE CAS FOIREUX ******************************************************
//		array(5) {
//			["type"] =>
//			string(77) "http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/"
//			["title"] =>
//			string(16) "Invalid Resource"
//			["status"] =>
//			int(400)
//			["detail"] =>
//			string(40) "An email address must contain a single @"
//			["instance"] =>
//			string(0) ""
//		}
//		array(5) {
//			["type"] =>
//			string(77) "http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/"
//			["title"] =>
//			string(16) "Invalid Resource"
//			["status"] =>
//			int(400)
//			["detail"] =>
//			string(74) "davy@example.com looks fake or invalid, please enter a real email address."
//			["instance"] =>
//			string(0) ""
//		}
//		
//		
//		
//		 avec post, 'status' => 'unsubscribed',
//		array(5) {
//			["type"] =>
//			string(77) "http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/"
//			["title"] =>
//			string(13) "Member Exists"
//			["status"] =>
//			int(400)
//			["detail"] =>
//			string(68) " is already a list member. Use PUT to insert or update list members."
//			["instance"] =>
//			string(0) ""
//		}
//		
//		
//		
//		********************************************* EXEMPLES DE  CAS REUSSI ******************************************************************
//		array(19) {
//			["id"] =>
//			string(32) "06d61b538f6033b5e500da142b8fa23b"
//			["email_address"] =>
//			string(20) "babou.ravi@gmail.com"
//			["unique_email_id"] =>
//			string(10) "5a730cc008"
//			["email_type"] =>
//			string(4) "html"
//			["status"] =>
//			string(10) "subscribed"
//			["merge_fields"] =>
//			array(2) {
//				["FNAME"] =>
//				string(0) ""
//				["LNAME"] =>
//				string(0) ""
//			}
//			["stats"] =>
//			array(2) {
//				["avg_open_rate"] =>
//				int(0)
//				["avg_click_rate"] =>
//				int(0)
//			}
//			["ip_signup"] =>
//			string(0) ""
//			["timestamp_signup"] =>
//			string(0) ""
//			["ip_opt"] =>
//			string(14) "46.218.210.214"
//			["timestamp_opt"] =>
//			string(25) "2016-08-12T09:43:18+00:00"
//			["member_rating"] =>
//			int(2)
//			["last_changed"] =>
//			string(25) "2016-08-12T09:43:18+00:00"
//			["language"] =>
//			string(0) ""
//			["vip"] =>
//			bool(false)
//			["email_client"] =>
//			string(0) ""
//			["location"] =>
//			array(6) {
//				["latitude"] =>
//				int(0)
//				["longitude"] =>
//				int(0)
//				["gmtoff"] =>
//				int(0)
//				["dstoff"] =>
//				int(0)
//				["country_code"] =>
//				string(0) ""
//				["timezone"] =>
//				string(0) ""
//			}
//			["list_id"] =>
//			string(10) "221f644c36"
//			["_links"] =>
//			array(8) {
//				[0] =>
//				array(4) {
//					["rel"] =>
//					string(4) "self"
//					["href"] =>
//					string(92) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members/06d61b538f6033b5e500da142b8fa23b"
//					["method"] =>
//					string(3) "GET"
//					["targetSchema"] =>
//					string(69) "https://us13.api.mailchimp.com/schema/3.0/Lists/Members/Instance.json"
//				}
//				[1] =>
//				array(5) {
//					["rel"] =>
//					string(6) "parent"
//					["href"] =>
//					string(59) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members"
//					["method"] =>
//					string(3) "GET"
//					["targetSchema"] =>
//					string(71) "https://us13.api.mailchimp.com/schema/3.0/Lists/Members/Collection.json"
//					["schema"] =>
//					string(76) "https://us13.api.mailchimp.com/schema/3.0/CollectionLinks/Lists/Members.json"
//				}
//				[2] =>
//				array(4) {
//					["rel"] =>
//					string(6) "update"
//					["href"] =>
//					string(92) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members/06d61b538f6033b5e500da142b8fa23b"
//					["method"] =>
//					string(5) "PATCH"
//					["schema"] =>
//					string(69) "https://us13.api.mailchimp.com/schema/3.0/Lists/Members/Instance.json"
//				}
//				[3] =>
//				array(4) {
//					["rel"] =>
//					string(6) "upsert"
//					["href"] =>
//					string(92) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members/06d61b538f6033b5e500da142b8fa23b"
//					["method"] =>
//					string(3) "PUT"
//					["schema"] =>
//					string(69) "https://us13.api.mailchimp.com/schema/3.0/Lists/Members/Instance.json"
//				}
//				[4] =>
//				array(3) {
//					["rel"] =>
//					string(6) "delete"
//					["href"] =>
//					string(92) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members/06d61b538f6033b5e500da142b8fa23b"
//					["method"] =>
//					string(6) "DELETE"
//				}
//				[5] =>
//				array(4) {
//					["rel"] =>
//					string(8) "activity"
//					["href"] =>
//					string(101) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members/06d61b538f6033b5e500da142b8fa23b/activity"
//					["method"] =>
//					string(3) "GET"
//					["targetSchema"] =>
//					string(80) "https://us13.api.mailchimp.com/schema/3.0/Lists/Members/Activity/Collection.json"
//				}
//				[6] =>
//				array(4) {
//					["rel"] =>
//					string(5) "goals"
//					["href"] =>
//					string(98) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members/06d61b538f6033b5e500da142b8fa23b/goals"
//					["method"] =>
//					string(3) "GET"
//					["targetSchema"] =>
//					string(77) "https://us13.api.mailchimp.com/schema/3.0/Lists/Members/Goals/Collection.json"
//				}
//				[7] =>
//				array(4) {
//					["rel"] =>
//					string(5) "notes"
//					["href"] =>
//					string(98) "https://us13.api.mailchimp.com/3.0/lists/221f644c36/members/06d61b538f6033b5e500da142b8fa23b/notes"
//					["method"] =>
//					string(3) "GET"
//					["targetSchema"] =>
//					string(77) "https://us13.api.mailchimp.com/schema/3.0/Lists/Members/Notes/Collection.json"
//				}
//			}
//		}




		if (defined('MailChimpSuscribersList')) {
			$MailChimpSuscribersList = trim(constant('MailChimpSuscribersList'));

//			return $MailChimpSuscribersList;
//			$result = $this->mailChimp->put("lists/$MailChimpSuscribersList/members", [
//				'email_address' => 'babou.ravi@gmail.com',
////				'email_address' => 'davy@example.com',
////				'status' => 'subscribed',
//				'status' => 'unsubscribed',
//			]);



			$hashId = ($this->getIdSuscriber($email));



			$commande = 'patch';
			$commande = 'post';
			$hashId = '.';
			$result = $this->mailChimp->$commande("lists/4$MailChimpSuscribersList/members/" . $hashId, [
				'email_address' => 'babou.ravi4@gmail.com',
//				'email_address' => 'davy@example.com',
				'status' => 'subscribed',
//				'status' => 'unsubscribed',
					], self::TIMEOUT_CURL);



			return $result;
		}
	}

}
