<?

require_once(libDir."class_chercheDoc.php");
require_once(libDir."sinequa/fonctions.inc");
require_once(libDir."sinequa/Intuition.inc"); 
		
class RechercheSinequa extends RechercheDoc {



    function __construct() {
	  	$this->entity='DOC';
     	$this->name=kDocument;
     	$this->prefix=null;
     	$this->sessVar='recherche_DOC';
		$this->tab_recherche=array();
		$this->useSession=true;
		
		
		

		if (defined("gParamsRecherche")) $this->params=unserialize(gParamsRecherche);
		
		if (defined("gParamsRechercheSinequa")) $this->paramsSinequa=unserialize(gParamsRechercheSinequa);
	}

    function analyzeFields ($myFields,$myVals,$myTypes,$myOps=array(),$myLibs=array(),$myVals2=array(),$myNOHL=array(),$myNOHist=array(),$myOptions=array()) {
		// VP (7/10/08) ajout tableau optionel chNoHist

	        if (!empty($myFields)) {
		        foreach ($myFields as $idx=>$field) {
	
					//différence avec la classe normale : on peut avoir un value vide et un value2 renseigné
					//notamment dans le cas des affinages de concepts
			        if ($field!='' && ($myVals[$idx]!=''||$myVals2[$idx]!='' ) && $myTypes[$idx]!='' ) {
	
							if (empty($myOps[$idx])) $myOps[$idx]="AND";
							if (empty($myLibs[$idx])) $myLibs[$idx]=kCritere;
							//si plusieurs valeurs en tableau, on les concatène (cas des checkbox)
							//if (is_array($myVals[$idx])) $myVals[$idx]=implode(',',$myVals[$idx]);
	
							// les obligatoires
							$this->tab_recherche[$idx]['FIELD']=$field;
							$this->tab_recherche[$idx]['VALEUR']=$this->removeWeirdCharacters($myVals[$idx]);
							$this->tab_recherche[$idx]['OP']=$myOps[$idx];
							$this->tab_recherche[$idx]['TYPE']=$myTypes[$idx];
							$this->tab_recherche[$idx]['LIB']=$myLibs[$idx];
							// les optionnels
							if (isset($myVals2[$idx])) $this->tab_recherche[$idx]['VALEUR2']=$myVals2[$idx];
							if (isset($myNOHL[$idx])) $this->tab_recherche[$idx]['NOHIGHLIGHT']=$myNOHL[$idx];
							if (isset($myNOHist[$idx])) $this->tab_recherche[$idx]['NOHIST']=$myNOHist[$idx];
							if (isset($myOptions[$idx])) $this->tab_recherche[$idx]['OPTIONS']=$myOptions[$idx];
			        }
		        }
	        }
	      $this->makeSQL(); //Lancement du moteur
     }



    /**
     * Initialise le SQL de recherche (DOC)
     *
     */
    function prepareSQL(){
		$usr=User::getInstance();

		if ($this->paramsSinequa["getConcepts"]==true) $concepts=",concepts";
		if ($this->paramsSinequa["getNounGroups"]==true) $noungroups=",noun-groups";

		foreach ($this->paramsSinequa["correlationGroups"] as $correlation) {
				$correlationSql.=",".strtoupper($correlation["mode"])."('".$correlation["entity"].
				",count=".($correlation["count"]?$correlation["count"]:10).
				",separator=".($correlation["separator"]?$correlation["separator"]:';')."') as cor_".$correlation["entity"];
		}

        $this->sql = "SELECT *,query-expansion $concepts $noungroups $correlationSql FROM *";
        $prefix=(!is_null($this->prefix)?$this->prefix.".":"");
        $this->sqlWhere=" WHERE ID_LANG='".$_SESSION["langue"]."' ";
        //by VP/LD 14/05/08 utilisation sqlWhere au lieu de sql, LD 26/05/08, utilisation var $prefix
        if($usr->getTypeLog() <= kLoggedNorm) $this->sqlWhere.=" AND DOC_ACCES='1' "; 
        // Historique
        $this->etape="";

    }
	
	
	/**
	 * Refine : analyse une chaine de type refine et génère le sql correspondant.
	 * IN : chaine de critères
	 * OUT : sql
	 */
	static function refine($string) {
		//by LD 27/11/08 : on sort l'analyse refine de la gestion formulaire
		//le refine est remis au niveau navigation
		$refineCollection=new RefineCollection($string);
		$conceptsTokens=$refineCollection->getTokensByType("concepts");
			for($i=0;$i<count($conceptsTokens);$i++) {
				$Xsearch.=' "'.$conceptsTokens[$i]->content.'"';
			}
		debug($refineCollection);
			
		for($i=0;$i<count($refineCollection->tokens);$i++)
		{
			if($refineCollection->tokens[$i]->type!="concepts" && $refineCollection->tokens[$i]->content!='')
			{
				//pour les champs de type csv (personnes, etc) => contains
				//pour les champs string ou autre (fonds, id, etc) => =
				$operateur=(stripos($refineCollection->tokens[$i]->type,'E_')===0?'contains':'=');
				$sql.=" AND ".strtoupper($refineCollection->tokens[$i]->type)." ".$operateur." '".str_replace("'","''",$refineCollection->tokens[$i]->content)."'";
			}
		}
		return $sql;
	}
	

	function chercheTexteAmongFields($tabCrit,$prefix='',$exactsearch=false) {
	
		global $db;
		
		$Xsearch=$tabCrit['VALEUR'];
		
		//by ld 27/11/08 : on sort l'analyse refine du formulaire pour le traiter au niveau navigation (comme le tri, etc)
		/*if (!empty($tabCrit['VALEUR2'])) { //Valeur 2 (refine ?) => analyse de refine
			$refineSQL=$this->refine($tabCrit['VALEUR2']);
			$this->saveHisto=false; //on ne sauve pas la requete dans le contexte d'un refine
		}
		*/
		// VP 20/11/08 : suppression remplacement car utilisation Quote() plus bas
		//$Xsearch=str_replace("'","''",$Xsearch);

		if (!empty($Xsearch)) {
		// VP 27/11/08 : prise en compte opérateur booléen (à modifier car cela ne fonctionne pas)
		//$this->sqlRecherche.=" ".$tabCrit['OP']." (";
		$this->sqlRecherche.=" AND (";
			
		// by VP 14/8/08 : ajout recherche directe par concepts et correction bug sur recherche sur un champ
		if(empty($tabCrit['FIELD'])) $tabCrit['FIELD']='TEXT';
		if(strtoupper($tabCrit['FIELD'])=="TEXT" || strtoupper(substr($tabCrit['FIELD'],0,2))=="E_") $this->sqlRecherche.=$tabCrit['FIELD'];
		else {
			$this->sqlRecherche.=' TEXT ';
			if (!empty($tabCrit['FIELD'])) $this->sqlRecherche.= " IN '".str_replace("'","''",$tabCrit['FIELD'])."'";
		}
		$this->sqlRecherche.=' CONTAINS ';
	
		$this->sqlRecherche.=$this->Quote($Xsearch);
		
		// by VP (16/09/08) : STRATEGY est mis plus bas après le test des options
		//$this->sqlRecherche.=' AND STRATEGY='.$this->Quote($this->paramsSinequa["strategy"]?$this->paramsSinequa["strategy"]:'default');		

		$this->sqlRecherche.=' AND LANGUAGE='.$this->Quote(strtolower($_SESSION['langue']));		
 
		$this->sqlRecherche.=" AND GLOBALRELEVANCE>=".($this->paramsSinequa["globalrelevance"]?$this->paramsSinequa["globalrelevance"]:50);		

		$this->sqlRecherche.=" AND RELEVANCEDELTA<".($this->paramsSinequa["relevancedelta"]?$this->paramsSinequa["relevancedelta"]:20);	
		
		if($this->paramsSinequa["wordsrelevance"]) $this->sqlRecherche.=" AND WORDSRELEVANCE>".$this->paramsSinequa["wordsrelevance"];
		
		if($exactsearch) $this->sqlRecherche.=" AND EXACT-SEARCH='true'";
		
		if ($tabCrit['OPTIONS']) { //ajout des critères optionnels (formulaires:méthodes de recherche)
								//inséré tel quel donc attention à la saisie
				foreach ($tabCrit['OPTIONS'] as $option=>$val) {
					if($option=='STRATEGY') $opt_strategy=true;
					$this->sqlRecherche.=" AND ".$option."=".$this->Quote($val)." \n";
				}
			}
		}
		if(!isset($opt_strategy))  $this->sqlRecherche.=' AND STRATEGY='.$this->Quote($this->paramsSinequa["strategy"]?$this->paramsSinequa["strategy"]:'default');		
		
		$this->sqlRecherche.=")";

		//$this->sqlRecherche.=$refineSQL;
		
		//On stocke ces critères dans tabHighlight, qui du coup ne contient pas un tableau mais la chaine nécessaire
		//au highlight. On se sert particulièrement de cette chaine lors de l'affichage d'une notice où ces critères
		//sont nécessaires au highlight et où les autres critères ne le sont pas.
		$this->tabHighlight=$this->sqlRecherche;
		
		// VP 14/8/08 : ajout dans histo si valeur non vide (car lors d'un refine valeur peut être vide)
		// VP (7/10/08) : prise en compte NOHIST
	    if(!empty($tabCrit['VALEUR'])&&empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
	    
	    
	}
	
// by VP 14/8/08 : ancien chercheLex
    protected function chercheChamp($tabCrit,$prefix=null,$keepIntact=false){ // Cherche VALS dans 1 CHAMP de la table PRINC
        
        global $db;
// by VP 23/8/08 : modif appel sql_op
		//$this->sqlRecherche.= " AND (".$tabCrit['FIELD']." ".str_replace("%","",$this->sql_op($tabCrit['VALEUR'],true)).")";
		// VP 27/11/08 : prise en compte opérateur booléen
		$this->sqlRecherche.= " ".$tabCrit['OP']." (".$this->sql_op($tabCrit['FIELD'],$tabCrit['VALEUR'],true).")";
		// VP (7/10/08) : prise en compte NOHIST
        if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
    }
	


//by VP 14/8/08 : reprise de class_cherche.php
// - remplacement like par =
// traitement recherche par année
/**
* Recherche par DATE dans la table principale
* IN : tab critère, prefixe tables (opt)
* OUT : sql + etape + highlight
* NOTE :
* 	le tableau de critère aura la structure suivante.
* 		FIELD : un ou plusieurs champs
* 		VALEUR : date de début ou date
* 		VALEUR2 (opt) : si spécifié dante de fin, la recherche devient BETWEEN
*/
protected function chercheDate($tabCrit,$prefix=""){
	//$champ_table,$valeur_date1, $valeur_date2,$operateur="AND"
	global $db;
	
	$valeur_date1=$this->convDate($tabCrit['VALEUR'],true);
	$valeur_date2=$this->convDate($tabCrit['VALEUR2'],true);

	// recherche par année, retournées sous la form aaaa-00-00 par convdate
	if(strpos($valeur_date1,"-00-00")>0) {
		$valeur_date1=substr($valeur_date1,0,4)."-01-01";
		if(empty($valeur_date2)) $valeur_date2=substr($valeur_date1,0,4)."-12-31";
		else if(strpos($valeur_date2,"-00-00")>0) $valeur_date2=substr($valeur_date2,0,4)."-12-31";
	}
	$prefix=($prefix!=""?$prefix.".":"");
	
	if ($valeur_date1<>"") {
		$arrFldDate=split(',',$tabCrit['FIELD']);
		$sql= " ".$tabCrit['OP']." (";
		
		if(strlen($valeur_date2)>0) {
			//$this->sqlRecherche.= " $operateur (t1.".$champ_table." between ".$this->Quote(fDate($valeur_date1))." AND ".$this->Quote(fDate($valeur_date2)).")";
			
			
			foreach ($arrFldDate as $i=>$date) {
				$sql.="(".$prefix.$date." between ".$this->Quote($valeur_date1)." AND ".$this->Quote($valeur_date2).")";
				if (count($arrFldDate)!=($i+1)) $sql.=" OR ";
			}
			
			// VP (7/10/08) : prise en compte NOHIST
			if(empty($tabCrit['NOHIST'])){
				//$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".kEntre." (".$valeur_date1." ".kEt." ".$valeur_date2.")  ";
				if(empty($tabCrit['VALEUR2'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
				else $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." ".kEntre." (".$tabCrit['VALEUR']." ".kEt." ".$tabCrit['VALEUR2'].")  ";
			}
		}
		else{
			
			foreach ($arrFldDate as $i=>$date) {
				//$sql.="(".$prefix.$date." like '".$valeur_date1."%')";
				$sql.="(".$prefix.$date." = '".$valeur_date1."')";
				$highlight[$date]=array($valeur_date1);
				if (count($arrFldDate)!=($i+1)) $sql.=" OR ";
			}
			
			// VP (7/10/08) : prise en compte NOHIST
			if(empty($tabCrit['NOHIST'])) $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
			if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
			
		}
		$this->sqlRecherche.=$sql.")";
	}
}
// by VP 14/8/08 : reprise de convDate pour Sinequa
protected function convDate($date,$zeropad=false) {
	//Convertit toute date de format dd/mm/yyyy, dd-mm-yyyy, yyyy/mm/dd en yyyy-mm-dd ou false si date non correcte
	
	$date=str_replace('/','-',$date);
	if ( ereg( "^([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})$", $date, $regs ) ) {
		if($regs[3]<10) $regs[3]+=2000;
		else if($regs[3]<100) $regs[3]+=1900;
		return $regs[3]."-".sprintf('%02d',$regs[2])."-".sprintf('%02d',$regs[1]);
	}
	else if ( ereg( "^([0-9]{1,2})-([0-9]{2,4})$", $date, $regs ) ) {
		if($regs[2]<10) $regs[2]+=2000;
		else if($regs[2]<100) $regs[2]+=1900;
		return $regs[2]."-".sprintf('%02d',$regs[1])."-01";
		
	}
	else if ( ereg( "^([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})$", $date, $regs ) ) {
		if($regs[1]<10) $regs[1]+=2000;
		else if($regs[1]<100) $regs[1]+=1900;
		return $regs[1]."-".sprintf('%02d',$regs[2])."-".sprintf('%02d',$regs[3]);
	}
	else if (!empty($date)) {
		$annee=sprintf('%d',$date);
		if($annee<10) $annee+=2000;
		else if($annee<100) $annee+=1900;

		if($zeropad) return $annee."-00-00";
		else return $annee."-01-01";
	}
}


// VP (7/10/08) : reprise fonction Quote
protected function Quote($str){
	return "'".str_replace("'","''",$str)."'";
}

/**
// VP (23/10/08) : reprise fonction sql_op
* Complétion "intelligente" selon les critères : si XX* -> LIKE, si array -> IN(,,) sinon =
 * IN : valeur (string comprenant 1 ou plusieurs vals), $keepIntact=>pas d'analyse de la chaine (utilisé pr DOC_COTE par ex)
 * OUT : string SQL
 */
protected function sql_op($field,$valeur,$keepIntact=false) {
	global $db;
	if (is_array($valeur)) $valeur=implode(',',$valeur);
	
	//By LD 26/06, utilisation du split , pour pouvoir chercher sur plusieurs valeurs, sans analyse de chaine
	if (is_numeric($valeur) || $keepIntact) $arrWords=split(",",$valeur);//une seule valeur, de type ID (numérique)
	else $arrWords=$this->analyzeString($valeur,$op); //Découpage intelligent des mots.
	
	//debug($arrWords);
	if(count($arrWords)>1) {
		
		$valeur="";
		for($i = 0; $i <= (count($arrWords) - 1); $i++){
			// suppression caract�res parasites et quotes
			$valeur.=",".$db->Quote(preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),trim($arrWords[$i])));
		}
		//$valeur=" IN (".substr($valeur,1).")";
		$tabVal=explode(",",substr($valeur,1));
		foreach($tabVal as $i => $v){
			if($i>0) $expr.=" or ";
			$expr.=$field." = ".$v;
		}
		$expr="(".$expr.")";
	} else if((strpos($valeur,"%")!==false)||(strpos($valeur,"*")!==false)) {
		// suppression caract�res parasites et quotes
		$valeur=trim($arrWords[0]); //suppression des espaces
		$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
		// VP 30/1/09 : ajout trim()
		$valeur=" LIKE ".$db->Quote(trim(str_replace(array("*","%")," ",$valeur)));
		$expr=$field.$valeur;
			} else {
				// suppression caract�res parasites
				
				$valeur=trim($arrWords[0]); //suppression des espaces
				$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
				// VP 30/1/09 : ajout trim()
				$valeur=" = ".$db->Quote(trim($valeur));
				$expr=$field.$valeur;
	}
	return $expr;
}


/** Recherche dans les fonds et sous-fonds en se basant sur un ou plusieurs ID
 *   La recherche de fonds est systématiquement étendue aux sous-fonds
 *  IN : tabCrit, prefix
 *  OUT : sql + tableau highlight
 *  NOTE : structure de tabCrit
 * 		FIELD => champ à afficher, FONDS ou FONDS_COL (FONDS par déft)
 * 		VALEUR => un ou plusieurs ID (array)
 * 		VALEUR2 => intitulé de la recherche : ce champ est renseigné par chercheFonds et contient la recherche textuelle originale
 *  NOTE2 : on passe dans cette recherche après une recherche en chercheFonds
 */
/*
	protected function chercheFondsId($tabCrit,$prefix) {
		global $db;
		if (is_array($tabCrit['VALEUR'])) $myVals=implode(",",$tabCrit['VALEUR']);
		else $myVals=$tabCrit['VALEUR'];
		if (empty($myVals)) return false;
		//Extension aux fonds fils
		$sql2="SELECT ID_FONDS from t_fonds WHERE ID_FONDS_GEN IN (".simpleQuote($myVals).")";
		$sons=$db->GetAll($sql2);
		foreach ($sons as $son) {
			if (!empty($son['ID_FONDS'])) $myVals.=",".$son['ID_FONDS'];	
		}
		$prefix=(!is_null($prefix)?$prefix.".":"");
		//Création requete SQL
		$this->sqlRecherche.= $tabCrit['OP']." ".$prefix."DOC_ID_FONDS IN (".$myVals.") ";
		
		$fld=($tabCrit['FIELD']!=''?$tabCrit['FIELD']:'FONDS');
		//Récupération des intitulés si on n'a pas de VALEUR2
		if (empty($tabCrit['VALEUR2'])) {
			$sql3="SELECT DISTINCT ID_FONDS,".$fld." from t_fonds WHERE ID_FONDS IN(".$myVals.") ";
			$libs=$db->GetAll($sql3);
			foreach ($libs as $lib) {
				$highlight[]=$lib[$fld]; //liste des fonds pour le highlight
				// Le libellé correspond à une valeur d'origine (hors extension)
				if (in_array($lib['ID_FONDS'],(array)$tabCrit['VALEUR'])) $orgLibs[]=$lib[$fld];
			}
		} else { //On a déjà un intitulé ou une valeur dans VALEUR2 => c notamment le cas quand on est passé par chercheFonds
			$orgLibs=(array)$tabCrit['VALEUR2'];
			$highlight=$orgLibs;
		}
		
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kFonds;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$orgLibs)." ";		
		if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight=$this->tabHighlight+$highlight;
	}
	
/** Recherche textuelle sur des fonds (typiquement depuis rech experte et/ou avancée)
 * 	IN : tabCrit, prefix
 * 	OUT : redirection vers chercheFondsId pour terminer la recherche
 * 	NOTE : structure de tabCrit
 * 		FIELD => champ à afficher (FONDS ou FONDS_COL, FONDS par déft)
 * 		VALEUR => noms de fonds qui sera analysé
 *  NOTE2 : cette recherche ne ramène rien en soi, elle prépare la recherche par chercheFondsId
 * 	 en allant simplement chercher les ID correspondant à la valeur saisie
 */
	/*
	protected function chercheFonds($tabCrit,$prefix) {
		global $db;
		$fld=($tabCrit['FIELD']!=''?$tabCrit['FIELD']:'FONDS');
		//Récupération des ID_FONDS par analyse "intelligente" du contenu du champ
		$sql2="SELECT ID_FONDS from t_fonds WHERE ".$fld.$this->sql_op($tabCrit['VALEUR']);
		$ids=$db->GetAll($sql2);
		foreach ($ids as $id) $arrIDs[]=$id['ID_FONDS'];
		//redirection vers recherche par ID
		$tabCrit['VALEUR2']=$tabCrit['VALEUR'];
		$tabCrit['VALEUR']=$arrIDs;
		$this->chercheFondsId($tabCrit,$prefix); //appel de la recherche par ID
	}
	
	*/
/** Recherche sur les matériels liés (doc_mat)
 * 	IN : tabCrit, prefix
 * 	OUT : sql, highlight
 * 	NOTE : structure de tabCrit
 * 		VALEUR => chaine faite des ID_MAT
 * 	NOTE : pas de highlight pour les ID_MAT !
 */	
	protected function chercheMat($tabCrit,$prefix) {
		global $db;
		
		$prefix=(!is_null($prefix)?$prefix.".":"");
		//by LD 26/6/8 : possté utilisation d'un autre champ via FIELD
		//by LD 26/6/8 : réintroduction du keepIntact=true
		// VP 23/8/08 : modif appel sql_op
		$this->sqlRecherche=$tabCrit['OP']." ".$prefix."ID_DOC IN 
					(SELECT ID_DOC from t_doc_mat WHERE ".$this->sql_op($tabCrit['FIELD'],$tabCrit['VALEUR'],true).")";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kMateriel;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";		
	}

/**
 * En fonction des types de recherche, appelle les fonctions de traitement dédiée.
 * Cette fonction peut se trouver surchargée si une classe introduit des recherches spécifiques
 */
    protected function ChooseType($tabCrit,$prefix) {
            // I.2.A.1 Tests des champs et appel des fonctions de recherche ad�quates :
            switch($tabCrit['TYPE'])
            {
            	case "M" : 
            		$this->chercheMat($tabCrit,$prefix);
            	break;
            	
            	case "F": //Fonds par saisie de texte
					$this->chercheChamp($tabCrit,$prefix);
            		//$this->chercheFonds($tabCrit,$prefix);
            	break;
            		
            	case "FI": //Fonds par ID
            		//$this->chercheFondsId($tabCrit,$prefix);
					$this->chercheChamp($tabCrit,$prefix);
            	break;
            	
            	case "DX": //Toutes dates
            		$this->chercheTouteDate($tabCrit,$prefix);
            	break;
            	
                case "FT": //FullText
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    break;
                case "FTS": //FullText avec découpage de chaque mot
                	$this->FToperator='+';
                	$this->chercheTexteAmongFields($tabCrit,$prefix);
                	break;
                case "FTE": //FullText exact
					// VP 19/03/09 : remplacement : par "" (caractère : interdit dans Sinequa)
					$tabCrit['VALEUR']="\"".str_replace(array("  ",":"),array(" ","\" \""),str_replace("\""," ",$tabCrit['VALEUR']))."\"";
                    $this->chercheTexteAmongFields($tabCrit,$prefix,true);
                    break;
					
                case "C":   // Recherche sur un champ précis
                    $this->chercheChamp($tabCrit,$prefix);
                    break;
                case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
                    $this->chercheChamp($tabCrit,$prefix,true);
                    break;                    

                case "D":   // Recherche sur date(s)
					//by VP 14/8/08 : reprise recherche date
					//$this->chercheChamp($tabCrit,$prefix);
					$this->chercheDate($tabCrit,$prefix);
                    break;
                case "H":   // Recherche sur durées(s)
					$this->chercheDuree($tabCrit,$prefix);
                    break;
                case "V":  // Recherche sur valeur
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    //$this->chercheVal($tabCrit,$prefix);
                    break;
                case "VI":    // Recherche sur valeur par ID (récursif)
                    $this->chercheIdVal($tabCrit,$prefix,true);
                    break;
                case "CD" : //comparaison de date
                	$this->compareDate($tabCrit,$prefix);
                	break;
					
				//by VP 14/8/08 : ajout cas CDLT et CDGT
                case "CDLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
					$arrFldDate=split(',',$tabCrit['FIELD']);
					$tabCrit['FIELD']=$arrFldDate[0];
					$tabCrit['VALEUR']=$this->convDate($tabCrit['VALEUR']);
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CDGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
					$arrFldDate=split(',',$tabCrit['FIELD']);
					$tabCrit['FIELD']=$arrFldDate[0];
					$tabCrit['VALEUR']=$this->convDate($tabCrit['VALEUR']);
                	$this->compareDate($tabCrit,$prefix);
                	break;
					
                case "CH" : //comparaison de date
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CV" : //comparaison de valeur =
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVGT" : //comparaison de valeur >
                	$tabCrit['VALEUR2']='>';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVLT" : //comparaison de valeur <
                	$tabCrit['VALEUR2']='<';
                	$this->compareValeur($tabCrit,$prefix);
                	break;

                case "LI" : // Recherche sur ID LEX unique (récursif)
                case "L" : 
                case "LP" :
                case "LF" : 
                	$this->chercheChamp($tabCrit,$prefix,true);
                	break;
                case "P" :
                	$this->cherchePers($tabCrit,$prefix);
                	break;
                case "PI" :
                	$this->chercheIdPers($tabCrit,$prefix);
                	break;
				case "U" : 
					$this->chercheUsager($tabCrit,$prefix);
					break;
				
                default : break;
            }
    }


    function saveSearchInDB ($requete) {
		global $db;
		require_once(modelDir.'model_requete.php');

        $myQuery = New Requete();

        $myQuery->t_requete['REQ_SQL']=$this->sql;
		$myQuery->t_requete['REQ_LIBRE']=$this->etape;


		$exist=$myQuery->checkExist();
		if ($exist) {

			$myQuery->t_requete['ID_REQ']=$exist;
			$myQuery->getRequete();
		}

        $myQuery->t_requete['REQ_NB_DOC']=$this->found_rows;
        $myQuery->t_requete['REQ_LIBRE']=$this->etape;
       	$myQuery->t_requete['REQ_PARAMS']=$this->tab_recherche;
       	$myQuery->t_requete['REQ_DATE_EXEC']=str_replace("'","",$db->DBTimeStamp(time()));

		if ($requete['SAUVE']) { //on sauve "sciemment" la requête ?
	        $myQuery->t_requete['REQUETE']=$requete['REQUETE'];
	        if (!empty($requete['REQUETE']))  {
	        	$myQuery->t_requete['REQ_SAVED']=1;
	        	$myQuery->t_requete['REQ_ALERTE']=$requete['REQ_ALERTE']; }
	        else {
	        	$myQuery->t_requete['REQ_ALERTE']=0; //... ou on la met juste dans l'histo...
	        	$myQuery->t_requete['REQ_SAVED']=0; }
		}

       	$myQuery->save(false);
        $this->requete['REQUETE']=$myQuery->t_requete['REQUETE'];
        $this->requete['REQ_ALERTE']=$myQuery->t_requete['REQ_ALERTE'];
        unset($myQuery);
    }


/*
    //Recherche sur un champs dans t_mat :
    protected function chercheMat($champ_table,$valeur_champ_form,$operateur="AND"){
        global $db;
        if(strlen($valeur_champ_form)>0) {
            $this->sqlRecherche.= " $operateur (t1.ID_DOC ";
            $this->sqlRecherche.= " in (select ID_DOC from t_doc_mat,t_mat where t_mat.ID_MAT=t_doc_mat.ID_MAT ";
            $this->sqlRecherche.= " AND t_mat.$champ_table=".$this->Quote($valeur_champ_form)."))";

            $highlight["t_doc_mat"]=array(str_replace(array(' +',' -','"'),' ',$valeur_champ_form));
        	$this->tabHighlight=$this->tabHighlight+$highlight;


            $this->etape.=$this->tab_libelles[$operateur]." ".$this->tab_libelles[$champ_table]." : ".$valeur_champ_form." ";
        }
    }

*/
    /** Applique les restrictions de droits sur la recherche.
    // i.e. : Sp�cifie que la liste des docs r�sultats doit faire partie d'une liste de fonds/collection auquelle l'utilisateur a acc�s.
    // Ajoute donc un "AND DOC_ID_FONDS in ('fonds1', 'fonds5', 'coll3',...) � la requette sql
    // NB. : On ne prend pas en compte les fonds qui ont des privil�ges � 0.
    **/
    
    function appliqueDroits(){
    	//by LD 04/06/08 => utilisation d'un tableau et non d'une chaine pour éviter les id_fonds in ('',.....)
		$prefix=(!is_null($this->prefix)?$this->prefix.".":"");
        $usr=User::getInstance();
        //$liste_id="''";
        foreach($usr->Groupes as $value){
           // if ($value["ID_PRIV"]!=0){ $liste_id.=", '".$value["ID_FONDS"]."'"; }
           if ($value["ID_PRIV"]!=0 && !empty($value["ID_FONDS"])) $liste_id[]=$value["ID_FONDS"];
        }

        //if(strlen($liste_id)>0) {
        if (count($liste_id)>0) {
            //$this->sqlRecherche.= " AND ".$prefix."DOC_ID_FONDS ";
            //$this->sqlRecherche.= " in ('".implode("','",$liste_id)."')";
			$expr="";
			foreach($liste_id as $i => $v){
				if($i>0) $expr.=" or ";
				$expr.=$prefix."DOC_ID_FONDS  = ".intval($v);
			}
			$this->sqlRecherche.=" AND (".$expr.")";
        }
    }

    /**
     * Initialisation du Pager.
     * IN : SQL, new_max (opt, nb de lignes max),
     * OUT : var de classe : Result (resultat de requete "tronqué"),
     * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales), PagerLink (url passée de page en page)
     */
    function execute($max_rows = 10, $secstocache=0, $highlight=true)
    {
        
        global $db;
        
        if(empty($this->sql))
            return;
        
        $iSession=new iSession;
        // VP (23/10/08) : changement max_answers_count et connexion par méthode connect
        $prms= array ('host' => sinequa_host,
                      'port' => sinequa_port,
                      'read_only' => 1,
                      'charset' => in_UTF8,
                      'page_size' => empty($this->nbLignes)?$this->nbLignes:10,
                      'max_answers_count' => 500000,
                      'default-language' => $_SESSION['langue']
                      );
        
        //$link = $iSession->in_connect($prms);
        $link = $iSession->connect($prms);
        
        if (!isset($this->nbLignes)) $this->max_rows = $max_rows; else $this->max_rows=$this->nbLignes;
        
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) ) $this->page=1;
        
        if($this->max_rows == "all"){ //on retourne tout
            $i=0;
            $iQuery=$iSession->in_query($sql);
            
            if (!$iQuery) { //0 résultats retournés !
                $this->rows=0;
                $this->found_rows=0;
                $this->page=1;
                $this->PagerLink="";
                $this->result=array();
                return false;
            }
            
            $this->max_rows = $iQuery->in_num_rows();
            
            
            if ($this->max_rows>kMaxRows) $this->max_rows=kMaxRows; // précaution : si trop de lignes, on revient à un mode paginé pour ne pas surcharger le serveur
            else {
                
                for($i=0;$i<$this->max_rows;$i++) $this->result[$i]=$iQuery->in_fetch_array();
                $this->rows =$this->max_rows;
                $this->found_rows=$this->max_rows;
                $this->page=1;
				$this->ajoutPrivResult() ;
                if(isset($iQuery))$iQuery->close();
            }
        }
        
        if ($this->max_rows!="all")
        { //pagination
            $i=0;
            
            $sql.=" SKIP ".(($this->page-1)*$this->nbLignes)." COUNT ".$this->nbLignes;
            //debug($sql);
            //debug(str_ireplace(array(',','join','WHERE','in '),array(','.chr(10),'join'.chr(10),'WHERE'.chr(10),'in '.chr(10)),$sql),'pink');
            
            $iQuery=$iSession->in_query($sql);
            
            // debug($iQuery,'orange',true); //attention : débug actif => stop des window.onload
            
            if ($iQuery) {
                $this->found_rows = $iQuery->in_num_rows();
                $i_min = ($this->page - 1) * $this->max_rows;
                if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
                
                $linesToShow=$iQuery->the_available_tuples_count;
                for($i=0;$i<$linesToShow;$i++)
                {$this->result[$i]=$iQuery->in_fetch_array();
                    //j'ai viré les balises XML encadrantes, pour les mettre dans la XSL
                    $this->result[$i]['colonne_xml']=trim($this->result[$i]['colonne_xml']);
                    //Pour contourner un pb sinequa, et récupérer le champ colonne XML avec du XML dedans,
                    //on retourner chercher le champ source document-content et on le transforme pour pouvoir
                    //l'utiliser
                    $this->result[$i]['document-content']=hex_str($this->result[$i]['document-content']); //décodage hexa
                    $this->result[$i]['document-content']=str_replace('<?xml version="1.0" encoding="utf-8"?>',"",$this->result[$i]['document-content']); //suppression entete xml
                    $this->result[$i]['document-content']="<XML>".$this->result[$i]['document-content']."</XML>"; //encadrement par des balises
                    
                }
                $this->rows =$linesToShow;
                
                /*Ensuite, récupération concepts et corrélations
                 */
                $this->conceptsSinequa=parseConcepts( $iQuery->in_fetch_attribute("concepts"));
                //debug($this->conceptsSinequa,'lightgreen');
                $arrParamsSinequa=unserialize(gParamsRechercheSinequa);
                foreach ($arrParamsSinequa["correlationGroups"] as $correlation) {
                    $_tmp=$iQuery->in_fetch_attribute("cor_".$correlation['entity']);
                    if ($_tmp) $this->correlationsSinequa[$correlation['entity']]=parseCorrelations($_tmp);
                }
                // VP 20/11/08 : Suppression chr(10)
                foreach($this->correlationsSinequa as &$cor){
                    foreach($cor as &$val){
                        $val["label"]=str_replace(chr(10)," ",$val["label"]);
                    }
                }
                
                //debug($this->correlationsSinequa,'lightblue');
                
                if(isset($iQuery))$iQuery->close();
                $this->ajoutPrivResult() ;
            } else {
                $this->found_rows=0;
                $this->page=1;
                $this->error_msg="Problem with request.";
            }
        }
        
        if(isset($iSession))$iSession->in_close();
        
        //debug($this,'gold',true);
    }
	
	/**
	 * fonction ajoutPrivResult 
	 * Alimente le tableau de résultats avec le niveau de privilège de l'utilisateur pour chaque résultat.
     */
	function ajoutPrivResult(){
		$usr=User::getInstance();
		// VP 2/3/09 : prise en compte paramètre useMySQL
		if (defined("useSinequa") && useSinequa==true && $_SESSION["useMySQL"]!="1"){
			$fld="doc_id_fonds";
		}else{
			$fld="DOC_ID_FONDS";
		}
		
		for ($i=0;$i<count($this->result);$i++){
			// VP 21/4/10 : modif valeur privilege admin
			$this->result[$i]["ID_PRIV"]=5;
			if ($usr->Type!=kLoggedAdmin){
			$this->result[$i]["ID_PRIV"]=0; //par défaut

				foreach($usr->Groupes as $value){
					if(isset($this->result[$i][$fld])){
						if($value["ID_FONDS"]==$this->result[$i][$fld]){
							$this->result[$i]["ID_PRIV"]=$value["ID_PRIV"];
						}
					}
				}
			}
		}
		// MS - à optimiser / patcher, je n'aime pas trop l'idée d'avoir une fonction externe pouvant faire n'importe quoi sur le tableau. 
		// Conservé pour rétrocompatibilité.
		if(function_exists("ajoutPrivResultCustom")){
			$this->result=ajoutPrivResultCustom($this->result);
		}
		
		return true;
	}
	

}//Fin de la classe ChercheDoc

?>