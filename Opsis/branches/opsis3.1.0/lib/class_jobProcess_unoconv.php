<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

class JobProcess_unoconv extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('unoconv');
		
		$this->required_xml_params=array
		(
			'format'=>'string'
		);
		
		$this->optional_xml_params=array
		(
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{
		//$logFile=jobTmpDir."/".stripExtension($myJob->inXml).".log";
		
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		
		// test execution unoconv listener
		$this->shellExecute('ps','aux | grep "unoconv --listener"');
		$stdout=file_get_contents($this->getTmpDirPath().'stdout.txt');
		$stdout=explode("\n",$stdout);
		
		foreach ($stdout as $index=>$ligne)
		{
			if (empty($ligne) || strstr($ligne,'grep'))
				unset($stdout[$index]);
		}
		
		if (count($stdout)==0) // pas de listenner lanc�
		{
			if ($this->getEngine()->getIpAdress()!='localhost' && $this->getEngine()->getIpAdress()!='127.0.0.1')
			{
				$cmd=kBinUnoconv.' --listener &';
				$cmd='ssh '.$this->getEngine()->getIpAdress().' "'.$cmd.'"';
			}
			else
			{
				$cmd=kBinUnoconv.' --listener &';
			}
			
			$this->writeOutLog($cmd);
			proc_open($cmd,array(0=>array('pipe','r'),1=>array('pipe','w'),2=>array('pipe','a')),$exec_pipes);
			sleep(10);
		}
		
		/*if(!isset($params_job['format']) || empty($params_job['format']))
			$params_job['format']="pdf";*/
		
		$tmp_file_in=$this->getTmpDirPath().basename($this->getFileInPath());
		$tmp_file_out=$this->getTmpDirPath().stripExtension(basename($this->getFileInPath())).'.'.$params_job['format'];
		
		// deplacement vers tmp
		$copie=copy($this->getFileInPath(),$tmp_file_in);
		
		if ($copie===false)
			throw new FileNotFoundException('Erreur copie : le fichier n\'a pas �t� copi� dans le r�pertoire temporaire');
		
		//$this->shellExecute(kBinUnoconv,'-v -f '.$params_job['format'].' '.escapeQuoteShell($tmp_file_in).'');
		$this->shellExecute(kBinUnoconv,'-v --server localhost -f '.$params_job['format'].' -d document '.escapeQuoteShell($tmp_file_in).'');
		
		//$this->writeOutLog('stdout : '.file_get_contents($this->getTmpDirPath().'stdout.txt'));
		//$this->writeOutLog('stderr : '.file_get_contents($this->getTmpDirPath().'stderr.txt'));
		
		if (!file_exists($tmp_file_out))
			throw new FileNotFoundException('Erreur conversion : fichier sortie n\'existe pas.');
		
		// deplacement vers le repertoire de sortie
		rename($tmp_file_out,$this->getFileOutPath());
		
		// on supprime le fichier d'entree qui a ete copie
		unlink($tmp_file_in);
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
