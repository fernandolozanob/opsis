<?php
require_once(modelDir.'model_tape.php');
require_once(modelDir.'model_librairieElement.php');

class BackupJob {

	var $error_msg; //messages d'erreur
	var $arraySlots; // Tableau des slots de la librarie
	var $arrayTapes; // Tableau des cartouches de la librarie
	var $arrayFiles; // Tableau des fichiers à sauvegarder

	function __construct() {
	}

	/**
	* Ajoute un message d'erreur pour la sortie
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function dropError($errLib) { 
		$this->error_msg.=$errLib."<br/>";
	}

	/**
	 * Création et lancement Job de backup
	 * IN : 
		$jobObj (retour) = objet job
		$action = type de job [listing|init|backup|restore]
		$id_tape_file=identifiant sauvegarde, 
		$id_tape_set=identifiant jeu de sauvegarde, 
		$id_tape=identifiant cartouche, 
        $id_mat=identifiant fichier à sauvegarder, 
		$fichier = fichier à sauvegarder
		$media = media à sauvegarder [videos|storyboard|doc_acc]
		$id_job_prec= ID_JOB précédent
        $id_job_gen= ID_JOB parent
	 * OUT : succès [true|false] et objet Job en paramètre
	 */
	// VP 20/07/09 : ajout paramètres device et id_tape_set
	// VP 4/02/10 : correction bug sur $device issu de t_tape_set
    // VP 10/05/12 : ajout paramètre plateforme pour gérer plusieurs frontaux
    // VP 18/10/12 : ajout paramètre id_mat
	function createJob(&$jobObj, $action, $device="", $id_tape_file=0, $id_tape_set=0, $id_tape="", $id_mat="", $fichier="", $media="", $id_job_prec=0, $id_job_gen=0,$priority=2,$plateforme="",$job_date_lancement='') {
		if(empty($action)){$this->dropError(kErrorBackupJobActionVide);return false;}
		if(!empty($id_tape_file)){
			$myTape=new Tape();
			$tf=$myTape->getOneTapeFile($id_tape_file);
			$id_tape=$tf["ID_TAPE"];
			$fichier=$tf["TF_FICHIER"];
			$id_mat_tape=$tf["TF_ID_MAT"];
			$media=$tf["TF_MEDIA"];
			$myTape->t_tape['ID_TAPE']=$tf["ID_TAPE"];
			$myTape->getTapeSet();
			$device=$myTape->t_tape_set[0]["TS_DEVICE"];
			$drive=$myTape->t_tape_set[0]["TS_DRIVE"];
			$id_tape_set=$myTape->t_tape_set[0]["ID_TAPE_SET"];
			unset($myTape);
		}elseif(!empty($id_tape_set)){
			$myTape=new Tape();
			$myTape->getTapeSet($id_tape_set);
			$device=$myTape->t_tape_set[0]["TS_DEVICE"];
			$drive=$myTape->t_tape_set[0]["TS_DRIVE"];
			$id_mat_tape=$id_mat;
			unset($myTape);
		}
		// VP 3/12/09 : ajout device par défaut
		if(empty($device)) $device="changer";
		require_once(modelDir.'model_job.php');
		$jobObj=new Job;
		$jobObj->setModule(array("MODULE_TYPE"=>"BACK"));
		$jobObj->t_job["JOB_NOM"]=$action." ".$id_tape." ".$fichier;
		$jobObj->t_job["JOB_ID_PROC"]=0;
		$jobObj->t_job["ID_JOB_PREC"]=$id_job_prec;
		$jobObj->t_job["ID_JOB_GEN"]=$id_job_gen;
		$jobObj->t_job["JOB_ID_ETAPE"]=0;
		$jobObj->t_job["JOB_IN"]=$fichier;
		$jobObj->t_job["JOB_ID_MAT"]=$id_mat_tape;
		$jobObj->t_job["JOB_OUT"]=$fichier;
		$jobObj->t_job["JOB_PRIORITE"]=$priority;
        if(defined("kJobPlateforme")){
            $jobObj->t_job["JOB_PLATEFORME"]=(empty($plateforme)?kJobPlateforme:$plateforme);
        }
		
		if (isset($job_date_lancement) && !empty($job_date_lancement))
			$jobObj->t_job["JOB_DATE_LANCEMENT"]=$job_date_lancement;
		
		//Paramètres
		$myPrms="<action>".$action."</action>";
		if(!empty($id_tape)) $myPrms.="<tape>".$id_tape."</tape>";
		if(!empty($media)) {
            $myPrms.="<media>".$media."</media>";
            if($media=="doc_acc") $myPrms.="<entity>".$media."</entity>";
        }
		if(!empty($device)) $myPrms.="<device>".$device."</device>";
		if(!empty($drive)) $myPrms.="<drive>".$drive."</drive>";
		if(!empty($id_tape_set)) $myPrms.="<set>".$id_tape_set."</set>";
		// VP 6/2/09 : envoi de <block> si 0
		if(isset($tf['TF_BLOCK'])) $myPrms.="<block>".$tf['TF_BLOCK']."</block>";
		// VP 18/07/11 : ajout paramètre <file>
		if(isset($tf['TF_N_FILE'])) $myPrms.="<numfile>".$tf['TF_N_FILE']."</numfile>";
        if($action=='restore' && getExtension($fichier)=='tar'){
            $myPrms.="<extract>1</extract>";
        }
		$jobObj->t_job["JOB_PARAM"]="<param>".$myPrms."</param>"; 
		$jobObj->t_job["JOB_DATE_CREA"]=date("Y-m-d H:i:s");
		$jobObj->t_job["JOB_ID_SESSION"]=session_id();
		if(empty($id_job_prec)) $jobObj->t_job["JOB_ID_ETAT"]=1;
		$ok=$jobObj->create();
		
		return $ok;
	}

	/**
	* Lecture fichier listing
	 * IN : 
	 * OUT : listing des cartouches
	 */
	// VP 20/07/09 : gestion devices et jeux
	function initArrayTapes($deleteXml=false) {
		$this->getDevices();
		foreach($this->devices as $device_name=>$device){
			$infoFile=kBackupInfoDir.'/'.$device['name'].".xml";
 			//if(!file_exists($infoFile) || $deleteXml) $this->initInfoFile($device['name']);
			// recherche des element dans la librairie
			$lib_elt=new LibrairieElement();
			$lib_elt->setDevice($device['name']);
			$slots=$lib_elt->updateLibElements();
			
			foreach($slots as $slot){
				if(!empty($slot['id_tape'])){
                    // VP 31/08/2016 : filtrage tape sur prefix
                    if(defined("gPrefixTape") && gPrefixTape!='' && strpos($slot['id_tape'], gPrefixTape)===false){
                        continue;
                    }
					$myTape=New Tape();
					$myTape->t_tape['ID_TAPE']=trim($slot['id_tape']);
					//$myTape->t_tape['ID_TAPE']=trim($slot['TAPE']);
					// Creation tape si non existante
					if(!$myTape->checkExist()){
						if(defined("gTapeCapacite")){
							$c=trim(str_replace(array("KB","MB","GB","Ko","Mo","Go"),array("*1024","*1048576","*1073741824","*1024","*1048576","*1073741824"),gTapeCapacite)); 
							eval("\$x= $c ;");
						}else $x=40000000000; // environ 40 Go
						$myTape->t_tape['TAPE_CAPACITE']=$x;
						// Affectation du jeu
						$id_tape_set=0;
						// VP 26/11/13 : Affectation automatique jeu, à faire avec un pattern
							$setNotFound=true;
							foreach($device['t_tape_set'] as $ts){
                                $t_prefix=explode(',',str_replace(array('*','%'),'',$ts['TS_TAPE_PREFIX']));
                                foreach($t_prefix as $prefix){
                                    $prefix=trim($prefix);
                                    if(strpos($myTape->t_tape['ID_TAPE'],$prefix)===0){
                                        $id_tape_set=$ts['ID_TAPE_SET'];
                                        $setNotFound=false;
                                    }
                                }
//								if($ts['nb_tapes']<$ts['TS_NB_TAPES'] && $setNotFound){
//									$id_tape_set=$ts['ID_TAPE_SET'];
//									$setNotFound=false;
//								}
							}
						$myTape->t_tape['ID_TAPE_SET']=$id_tape_set;
						$myTape->create(true);
					}else{
					}
					$myTape->getTape();
					$device['t_tape_set'][$myTape->t_tape['ID_TAPE_SET']]['nb_tapes']++;
					// VP 12/04/10 : suppression appel systématique à getTapeFile()
					//$myTape->getTapeFile();
					$this->arrayTapes[]=$myTape;
				}
			}
		}
		
	}

	/**
		* Mise à jour statut des cartouches
	 * IN : 
	 * OUT : listing des cartouches
	 */
	function updateTapeStatus($deleteXml=false) {
		global $db;
		$tapeList='';
		$this->initArrayTapes($deleteXml);
		foreach($this->arrayTapes as $tape){
			$tapeList.=",'".$tape->t_tape['ID_TAPE']."'";
		}
		$tapeList=substr($tapeList,1);
		// VP 5/05/10 : ajout retour true/false et suppression ordre sql
		if(empty($tapeList)){
			//$sql="UPDATE t_tape set TAPE_STATUS=0";
			//$db->Execute($sql);
			return false;
		}else{
			$sql="UPDATE t_tape set TAPE_STATUS=0 where ID_TAPE not in (".$tapeList.")";
			$db->Execute($sql);
			$sql="UPDATE t_tape set TAPE_STATUS=1 where ID_TAPE in (".$tapeList.")";
			$db->Execute($sql);
			return true;
		}
	}
	/**
		* Liste des jeux de sauvegarde et mise à jour devices
	 * IN : 
	 * OUT : liste des jeux de sauvegarde et mise à jour devices
	 */
	// VP 20/07/09 : création
	function getDevices() {
		global $db;
		$sql="SELECT * from t_tape_set order by ID_TAPE_SET";
		$tab=$db->GetAll($sql);
		if(count($tab)>0){
			foreach($tab as $row){
				$this->devices[$row['TS_DEVICE']]['name']=$row['TS_DEVICE'];
				$this->devices[$row['TS_DEVICE']]['t_tape_set'][$row['ID_TAPE_SET']]=$row;
				$this->devices[$row['TS_DEVICE']]['t_tape_set'][$row['ID_TAPE_SET']]['nb_tapes']=0;
			}
		}else{
			$this->devices["changer"]['name']="changer";
		}
	}
	/**
		* Création fichier listing d'un device
	 * IN : 
	 * OUT : listing des cartouches
	 */
	// VP 20/07/09 : ajout paramètres device
    // VP 18/10/12 : ajout paramètre id_mat
	function initInfoFile($device) {
		
		$ok=$this->createJob($jobObj, "listing",$device);
		return $ok;
	}
	
	/**
		* Infos fichiers
	 * IN : 
	 * OUT : infos sur fichier
	 */
	function getFinfo($filename,$media,$id=0){
		switch($media){
			case "videos":
                // VP 18/10/12 : utilisaiton class_materiel pour récupérer le chemin du fichier
				require_once(modelDir.'model_materiel.php');
                $matObj=new Materiel();
                $matObj->t_mat['ID_MAT']=$id;
                $matObj->getMat();
                if(isset($matObj->t_mat['ID_MAT'])){
                    $filename=$matObj->getFilePath();
                }else{
                    $filename=kVideosDir.$filename;
                }
				break;
			case "storyboard":
                $id=basename($filename);
				$filename=kStoryboardDir.$filename;
				break;
			case "doc_acc":
                // VP 11/12/14 : utilisation class_docAcc pour récupérer le chemin du fichier
				require_once(modelDir.'model_docAcc.php');
                $daObj=new DocAcc();
                $daObj->t_doc_acc['ID_DOC_ACC']=$id;
                $daObj->getDocAcc();
                if(isset($daObj->t_doc_acc['ID_DOC_ACC'])){
                    $filename=$daObj->getFilePath();
                }else{
                    $filename=kDocumentDir.basename($filename);
                }
               
				/*
                 if (strpos($filename, "http") === 0 || strpos($filename, "/") === 0)
                 $filename=basename($filename);
                 $filename=kDocumentDir.$filename;
                 */
				break;
		}
		if(is_file($filename)){
			// VP 5/05/10 : utilisation de getFileSize()
			$finfo=array("id"=>$id,"name"=>basename($filename),"path"=>$filename, "size"=>getFileSize($filename),"mdate"=>date("Y-m-d H:i:s",filemtime($filename)),"media"=>$media);
		}else{
			// VP 16/03/09 : renvoi structure finfo si fichier non présent
			$finfo=array("id"=>$id,"name"=>basename($filename),"path"=>$filename,"media"=>$media);
		}
		return $finfo;
	}
	/**
		* Initialisation tableau des fichiers
	 * IN : 
	 * OUT : listing des fichiers
	 */
	// VP 3/12/09 : utilisation paramètre id_tape_set
    // VP 30/08/2016 : prise en compte pattern regexp
    // - Compatibilité avec existant : "*.mp4,*.mov" =>  fichiers mp4 et mov
    // - Définition pattern par regexp : "([^v]..|v[^i].|vi[^s])\.mp4$" => fichiers mp4 sauf vis.mp4)
    // - Possibilité de définir le media : "videos/*.mp4 doc_acc/*" => fichiers mp4 et doc_acc)

    function initArrayFiles($id_tape_set) {
		global $db;
		if($id_tape_set!=0){
			$myTape=new Tape();
			$myTape->getTapeSet($id_tape_set);
			$ts_media=$myTape->t_tape_set[0]["TS_MEDIA"];
			$id_group_mat=$myTape->t_tape_set[0]["TS_ID_GROUPE_MAT"];
            $ts_pattern=$myTape->t_tape_set[0]["TS_PATTERN"];

            $_patterns=explode(" ",$ts_pattern);
            foreach($_patterns as $pattern){
                if($ts_media=='*' && strpos($pattern, '/')>0){
                    $media=substr($pattern,0,strpos($pattern, '/'));
                    $pattern=substr($pattern,strpos($pattern, '/')+1);
                }else{
                    $media=$ts_media;
                }
                if(empty($pattern)) $pattern="%";
                unset($myTape);
                switch($media){
                    case "videos":
                        require_once(modelDir.'model_materiel.php');

                        $sql="SELECT DISTINCT ID_MAT,MAT_NOM,MAT_CHEMIN,LIEU_PATH FROM t_mat left join t_lieu on (MAT_LIEU=LIEU) WHERE NOT EXISTS (select TF_ID_MAT from t_tape_file where ID_MAT=TF_ID_MAT and ID_TAPE_SET=".intval($id_tape_set)." and TF_TAILLE_FICHIER=MAT_TAILLE and TF_DATE_FICHIER=MAT_DATE_FICHIER)".$this->getSqlPattern("MAT_NOM", $pattern);
                        if(!empty($id_group_mat))
                            $sql.=" and MAT_ID_GROUPE_MAT=".intval($id_group_mat);
                        $sql.=" ORDER BY ID_MAT ASC";

                        $tab=$db->GetAll($sql);
                        foreach($tab as $idx=>$row) {
                            $path=(empty($row['LIEU_PATH'])?kVideosDir:$row['LIEU_PATH']);
                            $_fichier=$path.$row["MAT_CHEMIN"]."/".$row["MAT_NOM"];
                            if(is_file($_fichier)){
                                $this->arrayFiles[]=$this->getFinfo($_fichier,$media,$row["ID_MAT"]);
                            }
                        }
                        break;
                    case "storyboard":
                        foreach($_types as $_ext) {$sql.=" or IM_FICHIER like '".$_ext."'";}
                        $sql="SELECT IMAGEUR,IM_CHEMIN FROM t_image where ID_LANG=".$db->Quote($_SESSION['langue'])." AND (".substr($sql,3).") ORDER BY ID_IMAGE ASC";
                        $tab=$db->GetAll($sql);
                        foreach($tab as $idx=>$row) {
                            $_fichier=kStoryboardDir.$row["IM_CHEMIN"]."/".$row["IM_FICHIER"];
                            if(is_file($_fichier)){
                                $this->arrayFiles[]=$this->getFinfo($row["IM_CHEMIN"]."/".$row["IM_FICHIER"],$media); 
                            }
                        }
                        break;
                    case "doc_acc":
                        $sql="SELECT ID_DOC_ACC,DA_FICHIER, DA_CHEMIN FROM t_doc_acc where ID_LANG=".$db->Quote($_SESSION['langue']).$this->getSqlPattern("DA_FICHIER", $pattern)." AND DA_FICHIER not like 'http%' ORDER BY ID_DOC_ACC ASC";
                        
                        $tab=$db->GetAll($sql);
                        foreach($tab as $idx=>$row) {
                            $_fichier=kDocumentDir.$row["DA_CHEMIN"]."/".$row["DA_FICHIER"];
                            if(is_file($_fichier)){
                                $this->arrayFiles[]=$this->getFinfo($_fichier,$media,$row["ID_DOC_ACC"]);
                                //$this->arrayFiles[]=$this->getFinfo($row["DA_CHEMIN"]."/".$row["DA_FICHIER"],$media); 
                            }
                        }
                        break;
                }
            }
		} else $this->error_msg=kErrorVariableVide." ID_TAPE_SET";
	}

    /**
     * Calcul pattern
     * IN : $field, $pattern
     * OUT : $sqlPattern
     */
    function getSqlPattern($field, $pattern) {
        global $db;
        
        $sqlPattern="";
        if($pattern[0]=='*' || $pattern[0]=='%'){
            // Compatibilité existant : *.mp4,*.mov
            $_types=explode(",",str_replace("*","%",$pattern));
            foreach($_types as $_ext) {$sqlPattern.=" or $field ".$db->like($_ext);}
            $sqlPattern=" AND (".substr($sqlPattern,3).")";
        }elseif(!empty($pattern)){
            // Définiton pattern par regexp
            $sqlPattern=" AND ".$db->getRegexpSQL($field, $pattern);
        }
        return $sqlPattern;
    }
    
	/**
		* Recherche dernière sauvegarde fichier dans jeu
	 * IN :
	 * OUT : listing des fichiers à sauvegarder
	 */
	// VP 20/07/09 : ajout paramètre id_tape_set
	function updateArrayFiles($id_tape_set) {
		if(count($this->arrayFiles)>0){
			// VP 5/05/10 : utilisation checkBackupExists pour trouver les fichiers à sauver
			foreach($this->arrayFiles as $idx=>$finfo){
                // VP 4/07/13 : vérification si job déjà programmé
				if($this->checkBackupJobScheduled($finfo,$id_tape_set)){
					// Fichier déjà programmé
					print $finfo['name']." : scheduled\n";
					unset($this->arrayFiles[$idx]);
				}elseif($this->checkBackupExists($finfo,$id_tape_set)){
					// Fichier déjà sauvé
					print $finfo['name']." : saved\n";
					unset($this->arrayFiles[$idx]);
				}else{
					print $finfo['name']." : not saved\n";
					$size+=$finfo['size'];
				}
			}
			printf(kMsgFichierASauver."\n",count($this->arrayFiles),convertFileSize($size));
		}
		
	}

	/**
		* vérification si fichier sauvé
	 * IN : info sur fichier
	 * OUT
	 */
	// VP 20/07/09 : ajout paramètre id_tape_set
	function checkBackupExists($finfo, $id_tape_set) {
		global $db;
        // VP 22/10/12 : ajout critère TF_ID_MAT
		$sql="select t_tape_file.ID_TAPE from t_tape_file,t_tape where t_tape_file.ID_TAPE=t_tape.ID_TAPE and TF_MEDIA=".$db->Quote($finfo['media'])." and TF_FICHIER=".$db->Quote($finfo['name'])." and TF_DATE_FICHIER=".$db->Quote($finfo['mdate'])." and TF_TAILLE_FICHIER=".$db->Quote($finfo['size']); 
		if(!empty($finfo['id'])) $sql.=" and TF_ID_MAT=".intval($finfo['id']);
		if(!empty($id_tape_set)) $sql.=" and t_tape_file.ID_TAPE_SET=".intval($id_tape_set);
		$id_tape=$db->GetOne($sql);
		return $id_tape; //false si n'existe pas
	}

    /**
     * vérification si job déjà programmé
	 * IN : info sur fichier
	 * OUT
	 */
	function checkBackupJobScheduled($finfo, $id_tape_set, $action="backup") {
		global $db;
        $sql="select ID_JOB from t_job,t_module where";
		if(!empty($finfo['id'])) $sql.=" JOB_ID_MAT=".intval($finfo['id']);
        else $sql.=" JOB_IN=".$db->Quote($finfo['name']);
        $sql.=" and JOB_PARAM like '%<action>".$action."</action>%<set>".intval($id_tape_set)."</set>%' and JOB_ID_ETAT<".intval(jobFini)." and JOB_ID_MODULE=ID_MODULE and MODULE_TYPE='BACK'";
		$id_job=$db->GetOne($sql);
		return $id_job; //false si n'existe pas
	}
    
	/**
		* recherche première cartouche dispo
	 * IN : info sur fichier
	 * OUT : ID_TAPE
	 */
	// VP 20/07/09 : ajout paramètre id_tape_set
	function getCurrentTape($finfo, $id_tape_set) {
		global $db;
		// Recherche des cartouches on-line non pleines
		if(!empty($id_tape_set)) $sqlTapeSet=" and ID_TAPE_SET=$id_tape_set ";
		$sql="select * from t_tape where TAPE_STATUS=1 and TAPE_FULL!=1 $sqlTapeSet order by ID_TAPE"; 
		$tab=$db->GetAll($sql);
        
        if(count($tab)>0){
            $fill_rate=0.85;
            $tape_capacite=$tab[0]['TAPE_CAPACITE'];// capacité totale première cartouche trouvée
            if($finfo['size']>$tape_capacite) {
                // Fichier plus gros que la capacité totale d'une cartouche
                $this->dropError(kFichierTropGros." ".convertFileSize($finfo['size']));
                return;
            } elseif($finfo['size']>($tape_capacite*$fill_rate)) {
                // Fichier plus gros que la cartouche avec ratio mais moins gros que la capacité totale
                // On ne tient pas compte du ratio
                $fill_rate=1.0;
            }
            foreach($tab as $idx=>$row) {
                // VP 19/10/12 : prise en compte de 90% de la capacité
                // VP 28/10/12 : prise en compte de 85% de la capacité
                if($finfo['size']<(($row['TAPE_CAPACITE']*$fill_rate) - $row['TAPE_UTILISE'])){
                    $id_tape=$row['ID_TAPE'];
                    break;
                } else {
                    // La cartouche est pleine
                    $myTape=New Tape();
                    $myTape->t_tape['ID_TAPE']=$row['ID_TAPE'];
                    $myTape->setTapeFull();
                    $myTape->getTapeSet();
                    // envoi du mail si cartouche a externaliser
                    if ($myTape->t_tape_set['TS_EXTERNE']=='1')
                    {
                        $objet=kObjetMailCartoucheExternaliser;
                        $message=sprintf(kMessageMailCartoucheExternaliser,$row['ID_TAPE']);
                    }
                    else
                    {
                        $objet=kObjetMailCartouchePleine;
                        $message=sprintf(kMessageMailCartouchePleine,$row['ID_TAPE']);
                    }
                    // Choix du destinataire
                    if(isset($_SESSION['USER']['US_SOC_MAIL']) && !empty($_SESSION['USER']['US_SOC_MAIL'])) $dest=$_SESSION['USER']['US_SOC_MAIL'];
                    elseif(defined("gMailTape")) $dest=gMailTape;
                    elseif(defined("gMailJob")) $dest=gMailJob;
                    else $dest=gMail;
                    include_once(modelDir.'model_message.php');
                    $mail_obj=new Message();
                    $mail_obj->send_mail($dest,gMail,$objet,$message,'text/html');
                    
                    //si le jeu est a externaliser
                }
            }
            return $id_tape;
        }
	}
	

}
?>