<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_uploadException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');

require_once(modelDir.'model_materiel.php');

class JobProcess_ftp extends JobProcess implements Process
{
	private $curl;
	private $tmp_xml_file;
	private $in_xml_path;
	
	private $total_file_size;
	private $uploaded_size;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('ftp');
		
		$this->required_xml_params=array
		(
			'url'=>'string'
		);
		
		$this->optional_xml_params=array
		(
            'user'=>'string',
            'pass'=>'string',
            'folder'=>'string',
			'ftp_folder'=>'string',
			'file_out_name'=>'string',
			'img_data'=>'file',
			'out'=>'string',
			'xml_name'=>'string',
			'xml_data'=>'array',
            'sftp'=>'int',
            'key'=>'file',
            'pubkey'=>'file'
		);
		
		$this->tmp_xml_file='';
		$this->uploaded_size=array(0,0,0);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
		$this->in_xml_path=$file_xml;
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
        
        // Choix ftp/sftp
		if($params_job['sftp']==1){
            $url_ftp='sftp://'.$params_job['url'].'/'.$params_job['folder'].'/';
            if (isset($params_job['key']) && !empty($params_job['key']))
                $privkey_filename=$params_job['key'];
            if (isset($params_job['pubkey']) && !empty($params_job['pubkey']))
                $pubkey_filename=$params_job['pubkey'];
        }else{
            $url_ftp='ftp://'.$params_job['url'].'/'.$params_job['folder'].'/';
        }
		
		// url du ftp
		if (isset($params_job['ftp_folder']) && !empty($params_job['ftp_folder']))
			$url_ftp.=$params_job['ftp_folder'].'/';
		
		$nom_fichier_final=$this->getFileOutPath();
		
		if (!empty($nom_fichier_final) && strpos($nom_fichier_final,'.')===false) 
			$nom_fichier_final=$this->getFileOutPath().'.'.getExtension($this->getFileInPath());
		
		if (!empty($params_job['file_out_name']))
			$nom_fichier_final=$params_job['file_out_name'];
		
		// calcul de la taille totale a uploader		
		$this->total_file_size=filesize($this->getFileInPath());
		
		if (isset($params_job['img_data']) && !empty($params_job['img_data']))
			$this->total_file_size+=filesize($params_job['img_data']);
		
		if(isset($params_job['xml_name']) && !empty($params_job['xml_name']))
		{
			$data_xml_file_in=file_get_contents(jobRunDir.'/'.$this->getModuleName().'/'.$this->getEngine()->getNomServeur().'/'.$this->in_xml_path);
			
			$startpos=strpos($data_xml_file_in,'<xml_data>');
			$endpos=strpos($data_xml_file_in,'</xml_data>');
			$xml_data='<?xml version="1.0" encoding="UTF-8"?>'."\n".substr($data_xml_file_in,$startpos+10,$endpos-$startpos-10);
			
			$this->total_file_size+=strlen($xml_data);
		}
		
		// upload du fihcier video
		$this->curl = curl_init();
		
		if (defined('kProxyFTP'))
			curl_setopt($this->curl, CURLOPT_PROXY, kProxyFTP);
		
		$fd = fopen($this->getFileInPath(), "rb");
		curl_setopt($this->curl, CURLOPT_URL, $url_ftp.$nom_fichier_final);
        if(!empty($params_job['user'])) {
            if(!empty($params_job['pass'])) curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user'].':'.$params_job['pass']);
            else curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user']);
        }
        if(is_file($pubkey_filename) && is_file($privkey_filename)){
            curl_setopt($this->curl, CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PUBLICKEY);
            curl_setopt($this->curl, CURLOPT_SSH_PUBLIC_KEYFILE, $pubkey_filename);
            curl_setopt($this->curl, CURLOPT_SSH_PRIVATE_KEYFILE, $privkey_filename);
            curl_setopt($this->curl, CURLOPT_KEYPASSWD, '');                    
        }
		curl_setopt($this->curl, CURLOPT_UPLOAD, 1);
		curl_setopt($this->curl, CURLOPT_INFILE, $fd);
		curl_setopt($this->curl, CURLOPT_INFILESIZE, filesize($this->getFileInPath()));
		
		curl_setopt($this->curl, CURLOPT_NOPROGRESS, false);
		curl_setopt($this->curl, CURLOPT_PROGRESSFUNCTION, array($this,'updateUploadProgress'));
		
		$curl_result=curl_exec($this->curl);
		
		if ($curl_result===false)
			throw new UploadException('Error sending file ('.curl_errno($this->curl).' -> '.curl_error($this->curl).')');
		
		fclose($fd);
		
		// envoi de img_data
		if (isset($params_job['img_data']) && !empty($params_job['img_data']))
		{
			if (!file_exists($params_job['img_data']))
				throw new FileNotFoundException('image not found ('.$params_job['img_data'].')');
			
			if(isset($params_job['out']) && !empty($params_job['out']) && strpos($params_job['out'],".")===false) 
				$nom_fichier_final=$params_job['out'].".".getExtension($params_job['img_data']);
			else if(isset($params_job['file_out_name']) && !empty($params_job['file_out_name']) ) 
				$nom_fichier_final=stripExtension($params_job['file_out_name']).".".getExtension($params_job['img_data']);
			else
				$nom_fichier_final=basename($params_job['img_data']);
			
			$fd = fopen($params_job['img_data'], "rb");
			
			curl_setopt($this->curl, CURLOPT_URL, $url_ftp.$nom_fichier_final);
            if(!empty($params_job['user'])) {
                if(!empty($params_job['pass'])) curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user'].':'.$params_job['pass']);
                else curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user']);
            }
            if(is_file($pubkey_filename) && is_file($privkey_filename)){
                curl_setopt($this->curl, CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PUBLICKEY);
                curl_setopt($this->curl, CURLOPT_SSH_PUBLIC_KEYFILE, $pubkey_filename);
                curl_setopt($this->curl, CURLOPT_SSH_PRIVATE_KEYFILE, $privkey_filename);
                curl_setopt($this->curl, CURLOPT_KEYPASSWD, '');                    
            }
			curl_setopt($this->curl, CURLOPT_UPLOAD, 1);
			curl_setopt($this->curl, CURLOPT_INFILE, $fd);
			curl_setopt($this->curl, CURLOPT_INFILESIZE, filesize($params_job['img_data']));
			curl_setopt($this->curl, CURLOPT_PROGRESSFUNCTION, array($this,'updateUploadProgressImage'));
			
			$curl_result=curl_exec($this->curl);
			
			if ($curl_result===false)
				throw new UploadException('Error sending img file ('.curl_errno($this->curl).' -> '.curl_error($this->curl).')');
			
			fclose($fd);
		}
		
		// envoi de xml_name
		if(isset($params_job['xml_name']) && !empty($params_job['xml_name']))
		{
			$this->tmp_xml_file=$this->getTmpDirPath().$params_job['xml_name'];
			
			$data_xml_file_in=file_get_contents(jobRunDir.'/'.$this->getModuleName().'/'.$this->getEngine()->getNomServeur().'/'.$this->in_xml_path);
			
			$startpos=strpos($data_xml_file_in,'<xml_data>');
			$endpos=strpos($data_xml_file_in,'</xml_data>');
			$xml_data=substr($data_xml_file_in,$startpos+10,$endpos-$startpos-10);
			
			if(isset($xml_data) && !empty($xml_data))
			{
				$xml_data='<?xml version="1.0" encoding="UTF-8"?>'."\n".$xml_data;
				file_put_contents($this->tmp_xml_file, $xml_data);
				
				$fd=fopen($this->tmp_xml_file, "r");
				
				curl_setopt($this->curl, CURLOPT_URL, $url_ftp.$params_job['xml_name']);
                if(!empty($params_job['user'])) {
                    if(!empty($params_job['pass'])) curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user'].':'.$params_job['pass']);
                    else curl_setopt($this->curl, CURLOPT_USERPWD, $params_job['user']);
                }
                if(is_file($pubkey_filename) && is_file($privkey_filename)){
                    curl_setopt($this->curl, CURLOPT_SSH_AUTH_TYPES, CURLSSH_AUTH_PUBLICKEY);
                    curl_setopt($this->curl, CURLOPT_SSH_PUBLIC_KEYFILE, $pubkey_filename);
                    curl_setopt($this->curl, CURLOPT_SSH_PRIVATE_KEYFILE, $privkey_filename);
                    curl_setopt($this->curl, CURLOPT_KEYPASSWD, '');                    
                }
				curl_setopt($this->curl, CURLOPT_UPLOAD, 1);
				curl_setopt($this->curl, CURLOPT_INFILE, $fd);
				curl_setopt($this->curl, CURLOPT_INFILESIZE, filesize($this->tmp_xml_file));
				curl_setopt($this->curl, CURLOPT_PROGRESSFUNCTION, array($this,'updateUploadProgressXml'));
				
				$curl_result=curl_exec($this->curl);
				
				if ($curl_result===false)
					throw new UploadException('Error sending xml file ('.curl_errno($this->curl).' -> '.curl_error($this->curl).')');
				
				fclose($fd);
			}
		}
		
		curl_close($this->curl);
		
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function finishJob()
	{
		if (file_exists($this->tmp_xml_file))
		{
			if (unlink($this->tmp_xml_file)===false)
				throw new FileException('failed to delete XML file ('.$this->tmp_xml_file.')');
		}
		
		parent::finishJob();
	}
	
	public function updateProgress()
	{
		$this->writeOutLog(($this->uploaded_size[0]+$this->uploaded_size[1]+$this->uploaded_size[2]).'/'.$this->total_file_size);
		$this->setProgression(($this->uploaded_size[0]+$this->uploaded_size[1]+$this->uploaded_size[2])/$this->total_file_size*100,'uploading file');
	}
	
	public function updateUploadProgress($ressource_curl,$download_size, $downloaded, $upload_size, $uploaded)
	{
		// PHP 5.5 modifie l'ordre des arguments des callback progress curl, il faut donc réarranger les paramètres si ce script est executé sur une version PHP inférieure à 5.5.0
		if (version_compare(PHP_VERSION, '5.5.0') < 0) {
			$uploaded = $upload_size;
			$upload_size = $downloaded;
			$downloaded = $download_size;
			$download_size = $ressource_curl;
		}
		$this->uploaded_size[0]=$uploaded;
		$this->updateProgress();
	}
	
	public function updateUploadProgressImage($ressource_curl,$download_size, $downloaded, $upload_size, $uploaded)
	{
		// PHP 5.5 modifie l'ordre des arguments des callback progress curl, il faut donc réarranger les paramètres si ce script est executé sur une version PHP inférieure à 5.5.0
		if (version_compare(PHP_VERSION, '5.5.0') < 0) {
			$uploaded = $upload_size;
			$upload_size = $downloaded;
			$downloaded = $download_size;
			$download_size = $ressource_curl;
		}
		$this->uploaded_size[1]=$uploaded;
		$this->updateProgress();
	}
	
	public function updateUploadProgressXml($ressource_curl,$download_size, $downloaded, $upload_size, $uploaded)
	{
		// PHP 5.5 modifie l'ordre des arguments des callback progress curl, il faut donc réarranger les paramètres si ce script est executé sur une version PHP inférieure à 5.5.0
		if (version_compare(PHP_VERSION, '5.5.0') < 0) {
			$uploaded = $upload_size;
			$upload_size = $downloaded;
			$downloaded = $download_size;
			$download_size = $ressource_curl;
		}
		$this->uploaded_size[2]=$uploaded;
		$this->updateProgress();
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>