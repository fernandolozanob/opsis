<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_directoryException.php');

class JobProcess_streamz extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('streamz');
		
		$this->required_xml_params=array
		(
			'format'=>'string',
			'tcin'=>'string',
			'tcout'=>'string'
		);
		
		$this->optional_xml_params=array
		(
		);
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		// dans le cas streamz il n'y a pas a de fichier d'entree
		
		// verification des parametres et de leurs types
		if (!empty($this->required_xml_params))
		{
			foreach($this->required_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
				{
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
				}
				else if (isset($this->params_job[$param_name]) && empty($this->params_job[$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($this->optional_xml_params))
		{
			foreach($this->optional_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
			}
		}
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		
		//creaion du fichier capture.xml
		// fichier xml entree + xsl capture.xsl
		//file_get_contents($this->getFileInPath())
		$data_xsl_streamz='<?xml version="1.0" encoding="UTF-8"?>
		<data>
			<in>'.$this->getFileInPath().'</in>
			<out>'.$this->getFileOutPath().'</out>
			<param>
				<TcIn>'.$params_job['tcin'].'</TcIn>
				<TcOut>'.$params_job['tcout'].'</TcOut>
			</param>
		</data>';
		$xml_res=TraitementXSLT($data_xsl_streamz,gStreamzXsl,array());
		file_put_contents($this->getTmpDirPath().'/capture.xml',$xml_res);
		copy($this->getTmpDirPath().'/capture.xml',kPathNumCaptureFile);
		
		
		
		$xml_res=TraitementXSLT($data_xsl_streamz,gStreamzProjetXsl,array());
		file_put_contents($this->getTmpDirPath().'/streamprojet.xml',$xml_res);
		copy($this->getTmpDirPath().'/streamprojet.xml',kPathNumProjetsFile);
		
		try
		{
			$soap=new SoapClient(kWsdlStreamZ,array('cache_wsdl' => WSDL_CACHE_NONE));
			
			$response=$soap->isAppLaunched(array('arg0'=>0)); //boolean
			
			if ($response->return==true)
			{
				$response=$soap->launchEncoder(array('arg0'=>'device','arg1'=>0,'arg2'=>array('-remote','-remoteenablegui',kPathNumProjetsFile))); //integer
				//var_dump($response);

				$response=$soap->startEncoding(array('arg0'=>0,'arg1'=>0)); //booleen
				//var_dump($response);

				$response=$soap->isWaitingForTape(array('arg0'=>0)); //boolean
				//var_dump($response);

				$response=$soap->setTapeReady(array('arg0'=>0,'arg1'=>'El fin del Mundo')); //boolean
				//var_dump($response);

				do
				{
					sleep(3);
					$response=$soap->isAppEncoding(array('arg0'=>0)); //boolean
					//var_dump($response->return);
				}
				while($response->return==true);

				$response=$soap->exitEncoder(array('arg0'=>0)); //boolean
				
				//deplacement vers fichier sortie
				rename(kPathNumOutput.'/'.$this->getFileInPath(),$this->getFileOutPath());
				//var_dump($response);
			}
			
			$this->setProgression(100,'StreamZ');
			$this->writeOutLog('Fin traitement');
		}
		catch (SoapFault $soap_fault)
		{
			/*$this->writeOutLog($soap_fault->getMessage());
			$this->writeOutLog($soap_fault->getTraceAsString());
			$this->writeOutLog($soap_fault->__toString());
			*/
			echo 'faultcode: '.$soap_fault->faultcode.', faultstring: '.$soap_fault->faultstring."\n";
			echo $soap->__getLastRequest()."\n";
			echo $soap->__getLastResponse()."\n";
		}
		
		
		unlink($this->getTmpDirPath().'/capture.xml');
		unlink($this->getTmpDirPath().'/streamprojet.xml');
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>