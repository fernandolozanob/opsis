<?php


class AjaxDtree {

	var $arrNodes;

	var $objSearch; //Objet de type recherche

	var $output;

	function __construct($searchobject) {
		$this->objSearch=$searchobject;
		$this->output='<script type="text/javascript" src="'.libUrl.'webComponents/prototype/dtree.js" ></script>';
	}


	function setObject($searchobject) {
		$this->objSearch=$searchobject;
	}


	function makeRoot() { //Constitue la racine et les têtes de rubriques (niveau 1)

		$arrChildren=$this->objSearch->getChildren(0,$this->arrNodes); //récupération niveau 1

		$this->arrNodes['elt_0']=array("id"=>0,
							"id_pere"=>-1,
							"terme"=>$this->objSearch->getRootName(),
							"open"=>0,
							"icone"=>'',
							"valide"=>false,
							"context"=>false,
							"nb_children"=>count($arrChildren)
							);
	}

	function revealNode($id) {
		$this->objSearch->getNode($id,$this->arrNodes);
		$this->objSearch->getChildren($id,$this->arrNodes);
		$this->objSearch->getFather($id,true,$this->arrNodes);


	}

	function complementNodes() {

		$this->objSearch->complementNodes($this->arrNodes);

	}


	function renderOutput($id_input_result,$print=true,$only_adding=false) {


             if (!$only_adding){
                $this->output.= "\n<div align='left' class=\"blocDtree\">\n";
                $this->output.= "<script type=\"text/javascript\">\n";
                $this->output.= "<!--\n";
				$this->output.= "var d = new dTree('myTree');\n";
             }
            /// II.A.7.a. Cr�ation des noeuds
            foreach ($this->arrNodes as $element) {
                // Si le noeud est valide, on lui associe son url et sa checkbox
                $url_noeud = "";
                $checkbox = "";
                if($element["valide"]==true){
                    $url_noeud = "javascript:MaJChamp(\'".$id_input_result."\',\'".ereg_replace ("'", '&rsquo;',trim($element["terme"]))."\')";
                }

                $this->output.= "d.add('".$element["id"]."','".$element["id_pere"]."','".ereg_replace ("'", '&rsquo;',trim($element["terme"]))."','".$url_noeud."','','','".$element["icone"]."','','','','".$checkbox."','".($tab_pere[$element["id"]] < 2 ? 1:0)."','".$element["context"]."')\n";

                // Si le noeud doit �tre ouvert, on l'ajoute � $tab_elem_open
                if ($element["open"]=="true"){
                    $tab_elem_open[] = $element["id"];
                }
            }

        if (!$only_adding) {
        	$this->output.= "document.write(d);\n";
        	$this->output.=  "d.closeAll();\n"; }
        /// II.A.7.b. Ouverture des noeuds demand�s
        foreach($tab_elem_open as $value){
            $this->output.=  "d.openTo('".$value."', true);\n";
        }

        if (!$only_adding) {
            $this->output.= "//-->\n";
            $this->output.= "</script>\n";
            $this->output.= "</div>\n";
        }



		if ($print) echo $this->output; else return $this->output;

	}






}

?>
