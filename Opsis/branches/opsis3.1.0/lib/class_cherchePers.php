<?php
require_once(libDir."class_cherche.php");


class RecherchePersonne extends Recherche {

	var $treeParams;
	var $entity;
	var $affichage;
	
	function __construct() {
	  	$this->entity='PERS';
     	$this->name=kPersonne;
     	$this->prefix='P';
     	$this->sessVar='recherche_PERS';
		$this->tab_recherche=array();
		$this->useSession=true;
		$this->sqlSuffixe="";
	}


    function prepareSQL($groupby=true){
		global $db;
		//PC 30/09/10 : Ajout du champs TYPE_LEX pour les résultats de recherche sur les personnes
		// VP 5/09/11 : optimisation requête sur jointure PERS_ID_GEN (ajouter index PERS_ID_GEN/ID_LANG) 
		
        // QRY -> lente utiliser en opsis 2.6.2
        /*$this->sql = "select P.*, t.*, t_im.IM_CHEMIN, t_im.IMAGEUR ,t_dacc.DA_FICHIER, 
					count(dl.ID_DOC) as NB_DOC, count(pc.ID_PERS) as NB_CHILD
					from
					t_personne P
					left join t_doc_lex dl on (dl.ID_PERS=P.ID_PERS)
					left join t_type_lex t on t.ID_TYPE_LEX = PERS_ID_TYPE_LEX 
					left join t_personne pc on pc.PERS_ID_GEN = P.ID_PERS and pc.ID_LANG=P.ID_LANG
					left join t_image as t_im on P.PERS_PHOTO=t_im.ID_IMAGE AND t_im.ID_LANG='".$_SESSION["langue"]."' 
					left join t_doc_acc as t_dacc on (P.PERS_PHOTO=t_dacc.ID_DOC_ACC AND t_dacc.ID_LANG='".$_SESSION['langue']."' ) 
					WHERE P.ID_LANG=".$db->Quote($_SESSION['langue']);*/
        
        // QRY -> optimisation avec restriction pour opsis 2.6.3
        if($groupby){
            $this->sql = "select P.*, t.*, t_dacc.DA_FICHIER, t_dacc.DA_CHEMIN, 
                        count(distinct NULLIF(DL.ID_DOC,0)) as NB_DOC
                        from
                        t_personne P
                        left join t_doc_lex dl on (dl.ID_PERS=P.ID_PERS and DL.ID_PERS!=0)
                        left join t_type_lex t on (t.ID_TYPE_LEX = PERS_ID_TYPE_LEX AND t.ID_LANG='".$_SESSION["langue"]."') 
                        left join t_doc_acc as t_dacc on (P.PERS_PHOTO=t_dacc.ID_DOC_ACC AND t_dacc.ID_LANG='".$_SESSION['langue']."' and P.PERS_PHOTO!=0) 
                        WHERE P.ID_LANG=".$db->Quote($_SESSION['langue']);
            $this->sqlSuffixe=" GROUP BY P.id_pers, P.id_lang, t.id_type_lex, t.id_lang, t_dacc.ID_DOC_ACC,t_dacc.ID_LANG HAVING 1=1 ";
        }else{
            $this->sql = "select P.*, t.*, t_dacc.DA_FICHIER, t_dacc.DA_CHEMIN 
            from
            t_personne P
            left join t_type_lex t on (t.ID_TYPE_LEX = PERS_ID_TYPE_LEX AND t.ID_LANG='".$_SESSION["langue"]."') 
            left join t_doc_acc as t_dacc on (P.PERS_PHOTO=t_dacc.ID_DOC_ACC AND t_dacc.ID_LANG='".$_SESSION['langue']."' and P.PERS_PHOTO!=0) 
            WHERE P.ID_LANG=".$db->Quote($_SESSION['langue']);
            $this->sqlSuffixe="";
        }
    }

    // Lexique (valeur)
    protected function chercheLex($tabCrit,$prefix="",$extLexiquePre=true,$extLexiquePost=true){
        if(!empty($tabCrit['VALEUR'])) {
            global $db;
           	$prefix=($prefix!=""?$prefix.".":"");

			

			if ($extLexiquePre) { // Extension lexicale PRE : analyse de la chaine pour trouver les ID_LEX
		 		$arrLex=$this->extLexique($tabCrit);
			}


			if ($arrLex) { // Si extension demandée ET existe
					$strVals=$arrLex['VALEURS'];
					$arrHL=$arrLex['HIGHLIGHT'];

			}
			
			else { //Pas d'extension PRE, on se contente de retrouver les ID_LEX par fulltext
				   $strVals=str_replace(array(",","'"),array(" "," ' "),$tabCrit['VALEUR']);
				  //by LD 06/06/08 => ajout spaceQuote (bug cnrs pour recherche thésaurus avec quotes)
				  $arrHL=array(0=>$strVals);
		 		  //$sql2 = "select DISTINCT ID_LEX from t_lexique where match(PERS_NOM) against (".$db->Quote($strVals)." in boolean mode)";
				  $sql2 = "select DISTINCT ID_PERS from t_personne where ".$db->getFullTextSQL(array($strVals), array("PERS_NOM"));

			  $arrIDs=$db->GetAll($sql2);
			  if (!empty($arrIDs)) foreach ($arrIDs as $lx) $arrLex['ID'][]=$lx['ID_LEX'];
			  else $arrLex['ID']=array('-1');

			} 
			
			//Extension lexique Post Recherche : un ou plusieurs ID ont été trouvés, on les étend => comme dans chercheIdLex
			if ($extLexiquePost=='true') {
	     		$extHier=$this->extLexiqueHier($arrLex,true);
	    		if (!empty($extHier)) $arrLex['ID']=array_merge($arrLex['ID'],$extHier);
	
	    		$extSyno=$this->extLexiqueSyno($arrLex);
		 		if (!empty($extSyno)) $arrLex['ID']=array_merge($arrLex['ID'],$extSyno);
	 			$this->getTermesFromLexique($arrLex,true);
			}

			//debug($arrLex,"red");
           	$this->sqlRecherche.= $tabCrit['OP']." (".$prefix."ID_PERS in (select ID_PERS from t_pers_lex where ID_LEX ";
		   	$this->sqlRecherche.= $this->sql_op(implode(",",$arrLex['ID']));

			/*
            if($tabCrit['VALEUR2']){ // Un rôle est spécifié
                    $this->sqlRecherche.= " AND ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD'])." AND DLEX_ID_ROLE=".$db->Quote($tabCrit['VALEUR2']);
                    if (empty($tabCrit['LIB']))
                    	$tabCrit['LIB']=$db->GetOne("select ROLE from t_role where ID_ROLE=".$db->Quote($tabCrit['VALEUR2'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));

                } else {
                    $this->sqlRecherche.= " AND ID_TYPE_DESC='".$tabCrit['FIELD']."' ";
                    if (empty($tabCrit['LIB']))
                    	$tabCrit['LIB']=$db->GetOne("select TYPE_DESC from t_type_desc where ID_TYPE_DESC=".$db->Quote($tabCrit['FIELD'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
            }
			*/

            $this->sqlRecherche.="))";



        	if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight["LEX_TERME"][]=array_values($arrHL);

            $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$tabCrit['VALEUR']." ";
        }
    }


 /** Recherche par jointure dans la table t_doc_lex, en filtrant également sur un ROLE, TYPE_DESC, etc.
     * IN : tableau critère, préfixe, flag récursif
     * OUT
     * Tableau de critère structure
     * 		FIELD : id type desc (GEN, DE, etc).
     * 		VALEUR : id terme (unique). Peut être vide.
     * 		VALEUR2 : rôle
     * NOTE : à des fins de performances, dans ce mode, la partie sql initiale est réécrite avec :
     * 	- disparition du left_join t_doc_lex (puisqu'il est fait dans cette fonction)
     * 	- disparition du select in habituel
     */
   protected function chercheDocLex($tabCrit,$prefix="",$recursif=false){
            global $db;

		if ($tabCrit['VALEUR']==-1 && empty($tabCrit['VALEUR2']) && empty($tabCrit['OPTIONS'])) return;
		
// 		$this->sql = "select distinct P.*, count(dl.ID_DOC) as NB_DOC from
//					t_personne P 
//					inner join t_doc_lex dl ON dl.ID_PERS=P.ID_PERS
//					WHERE P.ID_LANG=".$db->Quote($_SESSION['langue']);
//					
//		$this->sqlSuffixe=' GROUP BY P.ID_PERS,P.ID_LANG';
 		
 		if ($tabCrit['VALEUR']!='-1') {
	 		$arrLex['ID']=(array)$tabCrit['VALEUR'];
	
	 		$this->getTermesFromLexique($arrLex,true);
			if ($arrLex) { // Si extension demandée ET existe
					$strVals=$arrLex['VALEURS'];
					$arrHL=$arrLex['HIGHLIGHT'];
			}
 		}

    	$prefix=($prefix!=""?$prefix.".":"");

        $this->sqlRecherche.= $tabCrit['OP']." (";
        
		if (!empty($arrLex['ID'])) $this->sqlRecherche.="ID_DOC ".$this->sql_op(implode(",",$arrLex['ID'])); 
		else $this->sqlRecherche.=" 1=1 ";


            //Parametres suppl�mentaires :
			if (!empty($tabCrit['OPTIONS'])){ 
			$this->sqlRecherche.= " AND dl.ID_TYPE_DESC".$this->sql_op($tabCrit['FIELD'])." AND DLEX_ID_ROLE=".$db->Quote($tabCrit['OPTIONS']);
			}
			else if($tabCrit['VALEUR2']){ // rôle renseigné
				$roles=(array)$tabCrit['VALEUR2'];
				$roles=" IN ('".implode("','",$tabCrit['VALEUR2'])."')";
				$this->sqlRecherche.= " AND dl.ID_TYPE_DESC".$this->sql_op($tabCrit['FIELD'])." AND dl.DLEX_ID_ROLE $roles";

                if (empty($tabCrit['LIB'])) { // Si pas de libellé on le récupère en base

                    $result = $db->GetOne("select ROLE from t_role where ID_ROLE $roles AND ID_LANG=".$db->Quote($_SESSION["langue"]));
                    if ($result) $tabCrit['LIB']=$result;
                }
            } else {
                $this->sqlRecherche.= " AND dl.ID_TYPE_DESC".$this->sql_op($tabCrit['FIELD']);
                if (empty($tabCrit['LIB'])) {
                    $result=$db->GetOne("select TYPE_DESC from t_type_desc where ID_TYPE_DESC".$this->sql_op($tabCrit['FIELD'])." AND ID_LANG=".$db->Quote($_SESSION["langue"]));
                    if($result) $tabCrit['LIB']=$result;
                }
            }

            $this->sqlRecherche.=")";
       echo $this->sqlRecherche;
            if (empty($tabCrit['NOHIGHLIGHT']) && !empty($arrHL)) $this->tabHighlight["LEX_TERME"][]=array_values($arrHL);

            //Requete pour retrouver le terme original, pas terrible
            if (!empty($arrLex['ID'])) $myOrgTerme=$db->GetOne("SELECT LEX_TERME from t_lexique where ID_LANG=".$db->Quote($_SESSION['langue'])." AND ID_LEX=".intval($tabCrit['VALEUR']));


            $this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".$myOrgTerme." ";
        
    }
    
    
	protected function ChooseType($tabCrit,$prefix) {
            // I.2.A.1 Tests des champs et appel des fonctions de recherche ad�quates :
            switch($tabCrit['TYPE'])
            {
                case "FT": //FullText
                    $this->chercheTexteAmongFields($tabCrit,$prefix);
                    break;
                case "FTS": //FullText avec découpage de chaque mot
                	$this->FToperator='+';
                	$this->chercheTexteAmongFields($tabCrit,$prefix);
                	break;
                case "C":   // Recherche sur un champ précis
                    $this->chercheChamp($tabCrit,$prefix);
                    break;
                case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
                    $this->chercheChamp($tabCrit,$prefix,true);
                    break;                    
                case "CI":    // Recherche sur un champ d'une table annexe par ID
                    $this->chercheIdChamp($tabCrit,$prefix);
                    break;
                case "CT": // Recherche sur un champ par texte exact dans une table liée (fonds, genre,...)
                    $this->chercheChampTable($tabCrit,$prefix);
                    break;
                case "D":   // Recherche sur date(s)
                    $this->chercheDate($tabCrit,$prefix);
                    break;
                case "H":   // Recherche sur durées(s)
                    $this->chercheDuree($tabCrit,$prefix);
                    break;
                case "V":  // Recherche sur valeur
                    $this->chercheVal($tabCrit,$prefix);
                    break;
                case "VI":    // Recherche sur valeur par ID (récursif)
                    $this->chercheIdVal($tabCrit,$prefix,true);
                    break;
                case "CD" : //comparaison de date
                	$this->compareDate($tabCrit,$prefix);
                	break;
                case "CH" : //comparaison de date
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHLT" : //comparaison de date
                	$tabCrit['VALEUR2']='<=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CHGT" : //comparaison de date
                	$tabCrit['VALEUR2']='>=';
                	$this->compareDuree($tabCrit,$prefix);
                	break;
                case "CV" : //comparaison de valeur =
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVGT" : //comparaison de valeur >
                	$tabCrit['VALEUR2']='>';
                	$this->compareValeur($tabCrit,$prefix);
                	break;
                case "CVLT" : //comparaison de valeur <
                	$tabCrit['VALEUR2']='<';
                	$this->compareValeur($tabCrit,$prefix);
                	break;

                case "LI" : // Recherche sur ID LEX unique (récursif)
                	$this->chercheIdLex($tabCrit,$prefix,true);
                	break;
                case "L" : // Recherche sur termes lexique (av. extension lex PRE)
                	$this->chercheLex($tabCrit,$prefix,true,false);
                	break;
                case "LP" : // Recherche sur termes lexique (av. extension lex PRE et POST)
                	$this->chercheLex($tabCrit,$prefix,true,true);
                	break;

                case "P" :
                	$this->cherchePers($tabCrit,$prefix);
                	break;
                case "PI" :
                	$this->chercheIdPers($tabCrit,$prefix);
                	break;
				case "U" :
					$this->chercheUsager($tabCrit,$prefix);
					break;
					
				case "DL" :
					$this->chercheDocLex($tabCrit,$prefix);
					break;
									
                default : break;
            }
    }
    

	//@update VG 28/04/2011 : surcharge de la méthode putSearchInSession de la classe parente avec la sauvegarde en session
	//du paramètre 'affichage'
    function putSearchInSession($id='') {
   		parent::putSearchInSession();
   		$_SESSION[$this->sessVar]["affichage"] = $this->affichage;
    }
    

    // VP 7/06/2012 : ajout paramètre $withBrother pour récupérer les frères
    function getNode($id_pers,&$arrNodes,$style='highlight',$withBrother=false) {
    	global $db;
    	$this->getFather($id_pers,true,$arrNodes,$withBrother);
    	$sql="select * from t_personne where ID_PERS=".intval($id_pers);
     	//$rows=$db->CacheGetAll(120,$sql);
		$rows=$db->GetAll($sql);
     	foreach($rows as $row) {

		    	$arrNodes['elt_'.$row['ID_PERS']]['id']=$row['ID_PERS'];
		    	$arrNodes['elt_'.$row['ID_PERS']]['id_pere']=$row['PERS_ID_GEN'];
		    	$arrNodes['elt_'.$row['ID_PERS']]['terme']=str_replace (" ' ", "'",trim($row['PERS_NOM']).' '.trim($row['PERS_PRENOM']));

		    	$arrNodes['elt_'.$row['ID_PERS']]['valide']=true;
		    	$arrNodes['elt_'.$row['ID_PERS']]['context']=false;
		    	$arrNodes['elt_'.$row['ID_PERS']]['style']=$style;
     	}
     	$this->getChildren($id_pers,$arrNodes);
    	return $arrNodes;

    }

    function complementNodes(&$arrNodes,$_rowIDs,$fatherID) { //ajouts complémentaires

    	global $db;
    	//debug($_rowIDs,'beige');




		/*
     	$sql="SELECT  l1.ID_LEX,l1.LEX_TERME,la.ID_LEX_ASSO as ID_LEX_ASSO from t_lexique_asso la, t_lexique l1
			   WHERE la.id_lex=l1.id_lex
			   and l1.ID_LANG='".$_SESSION['langue']."'
			   AND l1.LEX_ID_ETAT_LEX AND l1.LEX_ID_TYPE_LEX=".$db->Quote($this->treeParams['ID_TYPE_LEX']);

    	$rs=$db->Execute($sql);

    	while ($asso=$rs->FetchRow()) {
                       $arrNodes['elt_'.$asso["ID_LEX"]."_asso"]=array(
							'id'=>$asso["ID_LEX_ASSO"]."_asso",
                       		'id_pere'=>$asso["id_lex"],
                       		'terme'=>str_replace (" ' ", '&rsquo;',$asso['LEX_TERME']),
                       		'icone'=>imgUrl."dtree/page2.gif",
                       		'valide'=>true,
                       		'context'=>true,
							'id_orig'=>$asso["ID_LEX"]);
    	}
		$rs->Close();
		*/

    }


    function getRootName() {
    	return GetRefValue('t_type_lex',$this->treeParams['ID_TYPE_LEX'],$_SESSION['langue']);

    }
    
    
    function getChildren($id_pers,&$arrNodes) {
    	global $db;

		$sql="SELECT p1. * , count( p2.ID_PERS ) AS nb_children FROM t_personne p1
				LEFT JOIN t_personne p2 ON ( p2.PERS_ID_GEN = p1.ID_PERS AND p2.PERS_ID_TYPE_LEX = p1.PERS_ID_TYPE_LEX
				AND p2.ID_LANG = '".$_SESSION['langue']."' )
				WHERE p1.ID_LANG = '".$_SESSION['langue']."'
				AND p1.PERS_ID_GEN = ".intval($id_pers)."
				AND p1.PERS_ID_TYPE_LEX = ".$db->Quote($this->treeParams['ID_TYPE_LEX']);
		// VP 28/06/2011 : prise en compte paramètre tri
		$sql.=" GROUP BY p1.id_pers, p1.id_lang, p1.pers_id_gen, p1.pers_id_type_lex, p1.pers_prenom, p1.pers_particule, p1.pers_nom, p1.pers_prenom,p1.pers_date_naissance, p1.pers_lieu_naissance, p1.pers_date_deces, p1.pers_lieu_deces, p1.pers_organisation, p1.pers_tel_perso, p1.pers_gsm_perso, p1.pers_tel_prof, p1.pers_gsm_prof, p1.pers_fax, p1.pers_mail_perso, p1.pers_mail_prof, p1.pers_adresse_perso, p1.pers_adresse_prof, p1.pers_contact, p1.pers_biographie, p1.pers_notes, p1.pers_diffusion, p1.pers_date_crea, p1.pers_id_usager_crea, p1.pers_date_mod, p1.pers_id_usager_mod, p1.pers_photo, p1.pers_statut, p1.pers_web, p1.pers_contact_prenom, p1.pers_zip_prof, p1.pers_ville_prof ";
		if(empty($this->treeParams['tri'])) $sql.=" ORDER BY p1.PERS_ID_GEN, p1.PERS_NOM";
		else $sql.=" ORDER BY p1.PERS_ID_GEN,p1.".$this->treeParams['tri'];
    	//$rows=$db->CacheGetAll(120,$sql);
    	$rows=$db->GetAll($sql);
    	

    	foreach($rows as $row) {

		    	$arrNodes['elt_'.$row['ID_PERS']]['id']=$row['ID_PERS'];
		    	$arrNodes['elt_'.$row['ID_PERS']]['id_pere']=$row['PERS_ID_GEN'];
		    	$arrNodes['elt_'.$row['ID_PERS']]['terme']=str_replace (" ' ", "'",trim($row['PERS_NOM']).' '.trim($row['PERS_PRENOM']));

		    	$arrNodes['elt_'.$row['ID_PERS']]['valide']=true;
		    	$arrNodes['elt_'.$row['ID_PERS']]['context']=false;
		    	$arrNodes['elt_'.$row['ID_PERS']]['nb_children']=$row['NB_CHILDREN'];
				$_rowIDs[]=$row['ID_PERS'];
     	}

     	
    	return $arrNodes;
    }
    
    
    // VP 7/06/2012 : ajout paramètre $withChildren pour récupérer les enfants
    function getFather($id_pers,$recursif=false,&$arrNodes,$withChildren=false){     	
        global $db;
        
    	$sql=" SELECT ID_PERS,PERS_NOM,PERS_ID_GEN from t_personne where ID_LANG='".$_SESSION['langue']."' and ID_PERS
			in (select PERS_ID_GEN from t_personne where ID_PERS=".intval($id_pers).") ";
		//$rows=$db->CacheGetAll(120,$sql);
		$rows=$db->GetAll($sql);
    	foreach($rows as $row) {

            $arrNodes['elt_'.$row['ID_PERS']]['id']=$row['ID_PERS'];
            $arrNodes['elt_'.$row['ID_PERS']]['id_pere']=$row['PERS_ID_GEN'];
            $arrNodes['elt_'.$row['ID_PERS']]['terme']=str_replace (" ' ", "'",trim($row['PERS_NOM']).' '.trim($row['PERS_PRENOM']));

            $arrNodes['elt_'.$row['ID_PERS']]['valide']=true;
            $arrNodes['elt_'.$row['ID_PERS']]['context']=false;
            $arrNodes['elt_'.$row['ID_PERS']]['open']=true;

            if ($recursif && $row['PERS_ID_GEN']!=0) $this->getFather($row['ID_PERS'],$recursif,$arrNodes,$withChildren);

            if (!isset($arrNodes['elt_'.$row['ID_PERS']]['nb_children']))  $arrNodes['elt_'.$row['ID_PERS']]['nb_children']=1;

            if($withChildren) $this->getChildren($row['ID_PERS'],$arrNodes);

    	}
    	return $arrNodes;
    }

}
?>
