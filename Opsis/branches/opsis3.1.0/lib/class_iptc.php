<?php

/* INFOS SUR LE FICHIER

Nom : iptc.class.php
Rôle : contient la classe permettant de gérer les IPTC d'un fichier image
Développeur(s) : Arica Alex, Thies C. Arntzen

FIN INFOS SUR LE FICHIER 



INFOS SUR LA CLASSE 'class_iptc'

    REFERENCES : Développée le 04 Octobre 02 par Arica Alex avec l'aide de Thies C. Arntzen
    
    ROLE : permet de manipuler les iptc d'une image

    VARIABLES :
        -    $h_codesIptc
        -    $h_cheminFichier
        -    $h_iptcData

    METHODES :
        -    fct_lireIPTC
        -    fct_ecrireIPTC
        -    fct_iptcMaketag

FIN INFOS SUR LA CLASSE 

*/


class class_IPTC
{



    /* VARIABLES statics */
    
    var $h_codesIptc; /* $h_codesIptc : (tableau associatif) contient les codes des champs IPTC associés à un libellé */
    var $h_cheminImg; /* $h_cheminImg : (chaine) contient le chemin complet du fichier d'image */
    var $h_iptcData;  /* $h_iptcData  : (chaine) contient les données encodées de l'iptc de l'image */
    
    /* FIN VARIABLES statics


    
    INFOS SUR LA FONCTION

        ROLE : constructeur
        FONCTION : class_IPTC($cheminImg)
        DESCRIPTION DES PARAMETRES : 
            -    $cheminImg  = (chaine) le chemin complet du fichier d'image à traiter
        
    FIN INFOS SUR LA FONCTION */
    
    function class_IPTC($cheminImg)
    {
    
            // Inititalisations
            
                    // Les valeurs IPTC pouvant être manipulées
                    $this -> h_codesIptc = array("005" => "Object_name",
                                                 "007" => "Editorial_status",
                                                 "010" => "Priority",
                                                 "015" => "Category",
                                                 "020" => "Additional_category",
                                                 "022" => "Id",
                                                 "025" => "Keywords",
                                                 "030" => "Disposal_date",
                                                 "035" => "Disposal_hour",
                                                 "040" => "Special_instructions",
                                                 "045" => "Reference_service",
                                                 "047" => "Reference_date",
                                                 "050" => "Reference_number",
                                                 "055" => "Created_date",
                                                 "060" => "Created_time",
                                                 "062" => "DigitalCreationDate",
                                                 "063" => "DigitalCreationTime",
                                                 "065" => "Originating_program",
                                                 "070" => "Program_version",
                                                 "075" => "Object_cycle",
                                                 "080" => "Byline",
                                                 "085" => "Byline_title",
                                                 "090" => "City",
                                                 "092" => "Sublocation",
                                                 "095" => "Province_state",
                                                 "100" => "Country_code",
                                                 "101" => "Country",
                                                 "103" => "Original_transmission_reference",
                                                 "105" => "Headline",
                                                 "110" => "Credit",
                                                 "115" => "Source",
                                                 "116" => "Copyright_string",
                                                 "118" => "Contact",
                                                 "120" => "Caption",
                                                 "121" => "Local_caption",
                                                 "122" => "Local_caption",
                                                 "301" => "Intellectual_genre");

                                                 
                    // On enregistre le chemin de l'image à traiter
                            $this -> h_cheminImg = $cheminImg;
                    
                            
                    // On extrait les données encodées de l'iptc
                            getimagesize($this -> h_cheminImg, $info);
                            $this -> h_iptcData = $info["APP13"];
                    
    }
    
    /* FIN FONCTION class_IPTC();


    
    INFOS SUR LA FONCTION

        ROLE : lit les IPTC d'une image et les renvoie dans un tableau associatif
        FONCTION : fct_lireIPTC()
        TYPE RETOURNE : chaine sous forme de tableau associatif
        
    FIN INFOS SUR LA FONCTION */
  
    function fct_lireIPTC()
    {
        $tblIPTC = iptcparse($this->h_iptcData);
        //trace(print_r($tblIPTC,true));
        //echo "<pre>";var_dump($tblIPTC);echo "</pre>";
        
        if(isset($tblIPTC["1#090"]) && $tblIPTC["1#090"][0] == "\x1B%G") $charset="UTF8";
        else $charset="ISO-8859-1";
        //else $charset="MacRoman";
        unset($tblIPTC["1#090"]);
        
        while( (is_array($tblIPTC)) && (list($codeIPTC, $valeurIPTC) = each($tblIPTC)) )
        {
            $codeIPTC = str_replace("2#", "", $codeIPTC);
            
            if( ($codeIPTC != "000") && ($codeIPTC != "140") )
            {
                while(list($index, ) = each($valeurIPTC))
                {
                    //$valeurIPTC[$index]=utf8_encode($valeurIPTC[$index]);
                    if($charset=="MacRoman") $valeurIPTC[$index]=str_replace(chr(13),chr(10),macRoman_to_utf8($valeurIPTC[$index]));
                    elseif($charset=="ISO-8859-1") $valeurIPTC[$index]=utf8_encode($valeurIPTC[$index]);
                    else $valeurIPTC[$index]=$valeurIPTC[$index];
                    
                    //if(isset($lesIptc[strtolower($this->h_codesIptc[$codeIPTC])])) $lesIptc[strtolower($this->h_codesIptc[$codeIPTC])] .= "\n".$valeurIPTC[$index];
                    //else $lesIptc[strtolower($this->h_codesIptc[$codeIPTC])] = $valeurIPTC[$index];
                    if(isset($this->h_codesIptc[$codeIPTC])) $idx_code = strtolower($this->h_codesIptc[$codeIPTC]);
                    else $idx_code = $codeIPTC;
                    if(isset($lesIptc[$idx_code])) $lesIptc[$idx_code] .= "\n".$valeurIPTC[$index];
                    else $lesIptc[$idx_code] = $valeurIPTC[$index];
                }
            }
        }
        
        if(is_array($lesIptc)) return $lesIptc; else return false;
    }

    /* FIN FONCTION fct_lireIPTC();
    
 
    
    INFOS SUR LA FONCTION

        ROLE : écrit des IPTC dans le fichier image
        FONCTION : fct_ecrireIPTC()
        DESCRIPTION DES PARAMETRES : 
            -   $tblIPTC_util         =     (tableau associatif) contient les codes des champs IPTC à modifier associés leur valeur
            -    $cheminImgAModifier =    (chaine) stocke le chemin de l'image dont l'IPTC est à modifier ; s'il est null
                                                 le chemin sera celui contenu dans '$this -> h_cheminImg'
        TYPE RETOURNE : booléen
        
    FIN INFOS SUR LA FONCTION */
  
    function fct_ecrireIPTC($tblIPTC_util, $cheminImgAModifier = "")
    {
			// VP 15/02/2018 : iptcembed ne peut écrire que dans des fichiers JPEG, on filtre ici au cas où
		if(getExtension($cheminImgAModifier)!="jpg" && getExtension($cheminImgAModifier)!="jpeg"){
			return false;
		}
            // La tableau devant contenir des IPTC est vide ou n'est pas un tableau associatif
                    if( (empty($tblIPTC_util)) || (!is_array($tblIPTC_util)) ) return false;
                    
                    
            // Si le chemin de l'image à modifier est vide alors on lui spécifie le chemin par défaut
                    if(empty($cheminImgAModifier)) $cheminImgAModifier = $this -> h_cheminImg;
        
                    
            // On récupère l'IPTC du fichier image courant
                    $tblIPTC_old = iptcparse($this -> h_iptcData);
                    
                    
            // On prélève le tableau contenant les codes et les valeurs des IPTC de la photo
                    while(list($codeIPTC, $codeLibIPTC) = each($this -> h_codesIptc))
                    {                
                                    
                            // On teste si les données originelles correspondant au code en cours sont présents
                                    if (is_array($tblIPTC_old["2#".$codeIPTC])) $valIPTC_new = $tblIPTC_old["2#".$codeIPTC];
                                    else $valIPTC_new = array();
                            
                                    
                            // On remplace les valeurs des IPTC demandées
                                    if (is_array($tblIPTC_util[$codeIPTC]))
                                    {
                                            if (count($tblIPTC_util[$codeIPTC])) $valIPTC_new = $tblIPTC_util[$codeIPTC];
                                        
                                    }else{
                                        
                                            $val = trim(strval($tblIPTC_util[$codeIPTC]));
                                            if (strlen($val)) $valIPTC_new[0] = $val;
                                    }


                            // On crée un nouveau iptcData à partir de '$tblIPTC_new' qui contient le code et la valeur de l'IPTC
                                    foreach($valIPTC_new as $val)
                                    {
                                            $iptcData_new .= $this -> fct_iptcMaketag(2, $codeIPTC, $val);
                                    }
                            
                    }
                    
                    
            /*    A partir du nouveau iptcData contenu dans '$iptcData_new' on crée grâce à la fonction 'iptcembed()'
                le contenu binaire du fichier image avec le nouveau IPTC inclu */
                    $contenuImage = iptcembed($iptcData_new, $this -> h_cheminImg);
            
                    
            // Ecriture dans le fichier image
                    $idFichier = fopen($cheminImgAModifier, "wb");
                    fwrite($idFichier, $contenuImage);
                    fclose($idFichier);
                    
                    
            return true;
            
    }

    /* FIN FONCTION fct_ecrireIPTC(); 
    
 
    
    INFOS SUR LA FONCTION

        ROLE : permet de transformer une valeur de d'IPTC (code + valeur) en iptcData
        AUTEUR : Thies C. Arntzen
        FONCTION : fct_iptcMaketag($rec, $dat, $val)
        DESCRIPTION DES PARAMETRES : 
            -   $rec = (entier) toujours à mettre à 2
            -    $dat = (chaine) le code de l'IPTC (de type '110' et non '2#110')
            -    $val = (chaine) la valeur de l'IPTC
        TYPE RETOURNE : booléen
        
    FIN INFOS SUR LA FONCTION */
    
    function fct_iptcMaketag($rec, $dat, $val)
    {
            $len = strlen($val);
            if ($len < 0x8000) 
            return chr(0x1c).chr($rec).chr($dat).
            chr($len >> 8).
            chr($len & 0xff).
            $val; 
            else 
            return chr(0x1c).chr($rec).chr($dat).
            chr(0x80).chr(0x04).
            chr(($len >> 24) & 0xff).
            chr(($len >> 16) & 0xff).
            chr(($len >> 8 ) & 0xff).
            chr(($len ) & 0xff).
            $val;
    }
    
    // FIN FONCTION fct_iptcMaketag();
    
    
    
}

/*  Fin class_IPTC
 
*/
?> 
