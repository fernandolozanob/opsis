<?php
/**
 * reference tag plugin file.
 * @filesource

 */

// doc {{{

/**
 * <reference> tag.
 *
 
 */ // }}}
 
 require_once('Xml2PdfTag.php');

 
 
Class xml2pdf_tag_reference  extends Xml2PdfTag {
    // xml2pdf_tag_rect::__construct() {{{
    
	
	public $ref_code = "ref";
	public $ref_X="auto";
	public $ref_Y="auto";
	public $add_x=0;
	public $add_y=0;
	public $set_ref_X =false;
	public $set_ref_Y =false;
	public $set_ref =false;
	public $move_to_ref_X  =false;
	public $move_to_ref_Y  =false;
	public $move_to_ref =false;
	public $test ="";
	
	
	
	
    /**
     * Constructor.
     *
     * Parse the tag attributes and add the new line to the document.
     *
     * @param array $tagProperties tag properties
     * @return void
     */
    public function __construct($tagProperties) {
        $this->pdf = Pdf::singleton();
		
			
		if(isset($tagProperties['REF_CODE'])){
			$this->ref_code = $tagProperties['REF_CODE'];
		}
		
		if(isset($tagProperties['REF_X'])){
			$this->ref_X = $tagProperties['REF_X'];
		}
		if(isset($tagProperties['REF_Y'])){
			$this->ref_Y = $tagProperties['REF_Y'];
		}
		
		if(isset($tagProperties['ADD_X'])){
			$this->add_x = $tagProperties['ADD_X'];
		}
		if(isset($tagProperties['ADD_Y'])){
			$this->add_y = $tagProperties['ADD_Y'];
		}
		
		if(isset($tagProperties['SET_REF'])){
			$this->set_ref = $tagProperties['SET_REF'];
		}
		if(isset($tagProperties['SET_REF_X'])){
			$this->set_ref_X = $tagProperties['SET_REF_X'];
		}
		if(isset($tagProperties['SET_REF_Y'])){
			$this->set_ref_Y = $tagProperties['SET_REF_Y'];
		}
		if(isset($tagProperties['MOVE_TO_REF_X'])){
			$this->move_to_ref_X = $tagProperties['MOVE_TO_REF_X'];
		}
		if(isset($tagProperties['MOVE_TO_REF_Y'])){
			$this->move_to_ref_Y = $tagProperties['MOVE_TO_REF_Y'];
		}
		if(isset($tagProperties['MOVE_TO_REF'])){
			$this->move_to_ref = $tagProperties['MOVE_TO_REF'];
		}

		if(isset($tagProperties['TEST'])){
			$this->test = $tagProperties['TEST'];
		}

		
		
		if($this->set_ref || $this->set_ref_X ){
			if($this->ref_X =="auto"){
				$x = $this->pdf->GetX();
			}else{
				$x = $this->ref_X;
			}
			
			if(intval($this->add_x)!=0){
				$x = $this->add_x+$x;
			}
			
			 // echo "set_ref x = ".$x."\n";
			$this->pdf->references[$this->ref_code]['x'] = $x;
		}
		if ($this->set_ref || $this->set_ref_Y ){
			if($this->ref_Y =="auto"){
				$y = $this->pdf->GetY();
			}else{
				$y = $this->ref_Y;
			}
			
			if(intval($this->add_y)!=0){
				$y = $this->add_y+$y;
			}
			 // echo "set_ref y = ".$y."\n";
			$this->pdf->references[$this->ref_code]['y'] = $y;	
		}
		
		
		// var_dump($this->pdf->references[$this->ref_code]);
		// echo "<br />\n";
		if( $this->move_to_ref && $this->pdf->references[$this->ref_code]['x'] &&$this->pdf->references[$this->ref_code]['y'] ){
			$this->pdf->SetXY($this->pdf->references[$this->ref_code]['x'],$this->pdf->references[$this->ref_code]['y']);
		}else if($this->move_to_ref_X  && $this->pdf->references[$this->ref_code]['x']){
			$this->pdf->SetX($this->pdf->references[$this->ref_code]['x']);
		}else if($this->move_to_ref_Y && $this->pdf->references[$this->ref_code]['y']){
			$this->pdf->SetY($this->pdf->references[$this->ref_code]['y']);
		}
		

    }

    // }}}
    // xml2pdf_tag_ln::close() {{{
    
    /**
     *
     * @return void
     */
    public function close() {
    }

    // }}}
}
?>
