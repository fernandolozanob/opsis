<?php
/**
 * rect tag plugin file.
 * @filesource
 *
 * @author guillaume l. <guillaume@geelweb.org> 
 * @link http://www.geelweb.org geelweb-dot-org 
 * @license http://opensource.org/licenses/bsd-license.php BSD License 
 * @copyright Copyright � 2006, guillaume luchet
 * @version CVS: $Id: xml2pdf.tag.rect.php,v 1.4 2007/01/05 23:07:31 geelweb Exp $
 * @package Xml2Pdf
 * @subpackage Tag
 */

// doc {{{

/**
 * <rect> tag.
 *
 *
 * @author guillaume l. <guillaume@geelweb.org>
 * @link http://www.geelweb.org
 * @license http://opensource.org/licenses/bsd-license.php BSD License 
 * @copyright copyright � 2006, guillaume luchet
 * @version CVS: $Id: xml2pdf.tag.ln.php,v 1.4 2007/01/05 23:07:31 geelweb Exp $
 * @package Xml2Pdf
 * @subpackage Tag
 * @tutorial Xml2Pdf/Xml2Pdf.Tag.rect.pkg
 */ // }}}
 
 require_once('xml2pdf.tag.paragraph.php');

 
 
Class xml2pdf_tag_rect  extends xml2pdf {
    // xml2pdf_tag_rect::__construct() {{{
    
	
	public $color = "#ffffff";
	public $x = 0;
	public $y = 0;
	public $w = 0;
	public $h = 0;
	public $style;
	public $ln;
	public $position="absolute";
	
	function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}
	
    /**
     * Constructor.
     *
     * Parse the tag attributes and add the new line to the document.
     *
     * @param array $tagProperties tag properties
     * @return void
     */
    public function __construct($tagProperties) {
        $this->pdf = Pdf::singleton();
		
        
			
		$this->x =  isset($tagProperties['X'])?$tagProperties['X']:0;
		
		
		$this->y = isset($tagProperties['Y'])?$tagProperties['Y']:0;
			
		$this->w = isset($tagProperties['WIDTH'])?$tagProperties['WIDTH']:0;
		$this->h = isset($tagProperties['HEIGHT'])?$tagProperties['HEIGHT']:0;
			
		if(isset($tagProperties['COLOR'])){
			$this->color = $tagProperties['COLOR'];
		}
		
		if(isset($tagProperties['POSITION'])){
			$this->position = $tagProperties['POSITION'];
		}
		if(isset($tagProperties['STYLE'])){
			$this->style = $tagProperties['STYLE'];
		}

		if(isset($tagProperties['LN'])){
			$this->ln = $tagProperties['LN'];
		}
		
		$colors = $this->hex2rgb($this->color);
		if($this->style == 'D'){
			$this->pdf->setDrawColor($colors[0],$colors[1],$colors[2]);
		}else if($this->style == 'F'){
			$this->pdf->setFillColor($colors[0],$colors[1],$colors[2]);
		}else if ($this->style=='FD' || $this->style=='DF'){
			$this->pdf->setDrawColor($colors[0],$colors[1],$colors[2]);
			$this->pdf->setFillColor($colors[0],$colors[1],$colors[2]);
		}
		
		if($this->position =="relative"){
			$this->x = $this->x+$this->pdf->GetX();
			$this->y = $this->y+$this->pdf->GetY();
		}
		
		
        $this->pdf->Rect($this->x,$this->y,$this->w,$this->h,$this->style);

    }

    // }}}
    // xml2pdf_tag_ln::close() {{{
    
    /**
     *
     * @return void
     */
    public function close() {
    }

    // }}}
}
?>
