<?php
require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_uploadException.php');
require_once(libDir.'class_brightcove.php');

class JobProcess_brightcove extends JobProcess implements Process
{
	private $id_video_dailymotion;
	private $link_file_path;
	
	function __construct($file_xml,$engine)
	{
		parent::__construct();
		
		$this->setModuleName('brightcove');
		
		$this->required_xml_params=array
		(
			'account_id'=>'string',
			'api_secret'=>'string',
			'api_key'=>'string',
			'donnees'=>'array'
		);
		
		$this->optional_xml_params=array
		(

		);
		
		
		$this->setXmlFile($file_xml);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
		
		$required_params=array
		(
			'title'=>'string',			
			'reference_id'=>'string',		
		);
		
		$optional_params=array
		(
			'ends_at'=>'string',
			'doc_visa'=>'string',
			'description'=>'string',
			'playlist'=>'string',
			'tags'=>'string',
			'long_description'=>'string',
			'description'=>'string',
			'callbacks'=>'string',
			'poster'=>'string',
			'thumbnail'=>'string'
		);

		//verification des donnees presentes dans le noeud donnees
		if (!empty($required_params))
		{
			foreach($required_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
				{
					$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
				}
				else if (isset($this->params_job['donnees'][0][$param_name]) && empty($this->params_job['donnees'][0][$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($optional_params))
		{
			foreach($optional_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
                {
					//$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
                }
			}
		}
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		if($this->getFileInPath() != $this->getFileOutPath() ) {
			if(file_exists($this->getFileOutPath())) {
				unlink($this->getFileOutPath());
			}
			symlink($this->getFileInPath(),$this->getFileOutPath());
		}
		//appel class
		$bc = new BC();
		$bc->account_id=$params_job['account_id'];
		$bc->client_id=$params_job['api_key'];
		$bc->client_secret=$params_job['api_secret'];

		//création des métadonnées, dans le cas d'un remplacement du matériel seulement, pas besoin
		
		$video_id=$bc->get_video_id($params_job['donnees'][0]['reference_id']);
		if(empty($video_id)){
			try{
				$video_id=$bc->create_video($params_job);
			}

			catch (Exception $e) {
			    exit($e->getMessage());
			}
		}

		//upload
		$this->setProgression(25); 
		try{
		 	$bc->ingest_video($params_job,$video_id);
		 }
			catch (Exception $e) {
			    exit($e->getMessage());
		}
		
		if(!empty($video_id)){
			$this->writeOutLog('Brightcove video_id:<id>'.$video_id.'</id>');
		}
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');

	}
	
	public function finishJob()
	{
		if (file_exists($this->link_file_path))
		{
			if (unlink($this->link_file_path)===false) {
				throw new FileException('Failed to delete link ('.$this->link_file_path.')');
            }
		}
		
		parent::finishJob();
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}
?>