<?php
require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_uploadException.php');
require_once(libDir.'class_dailymotion.php');
require_once(libDir.'dailymotion_sdk/Dailymotion.php');

class JobProcess_dailymotion extends JobProcess implements Process
{
	private $id_video_dailymotion;
	private $link_file_path;
	
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('dailymotion');
		
		$this->required_xml_params=array
		(
			'user'=>'string',
			'pass'=>'string',
			'api_secret'=>'string',
			'api_key'=>'string',
			'donnees'=>'array'
		);
		
		$this->optional_xml_params=array
		(
			'video_star'=>'string',
			'account'=>'string',
			'nb_retries'=>'int'
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	protected function inXmlCheck()
	{
		parent::inXmlCheck(false);
		
		$required_params=array
		(
			'title'=>'string',			
			'language'=>'string',		
			'published'=>'string',
			'allow_comments'=>'string'
		);
		
		$optional_params=array
		(
			'creative'=>'string',
			'official'=>'string',
			'description'=>'string',
			'playlist'=>'string',
			'tags'=>'string',
			'private'=>'string',
			'channel'=>'string',
			'private'=>'string',
			'thumbnail_url'=>'string'
		);
		
		//verification des donnees presentes dans le noeud donnees
		if (!empty($required_params))
		{
			foreach($required_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
				{
					//$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
				}
				else if (isset($this->params_job['donnees'][0][$param_name]) && empty($this->params_job['donnees'][0][$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($optional_params))
		{
			foreach($optional_params as $param_name=>$type)
			{
				if (isset($this->params_job['donnees'][0][$param_name]) && !empty($this->params_job['donnees'][0][$param_name]))
                {
					//$this->params_job['donnees'][0][$param_name]=$this->checkParamType($this->params_job['donnees'][0][$param_name],$param_name,$type);
                }
			}
		}
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		$user_login = array('username'=>$params_job['user'], 'password' =>$params_job['pass'],'scope'=>'read write manage_playlists');
		$api = new Dailymotion();
		
		if (defined('kProxyHTTPS'))
			$api->proxy = kProxyHTTPS;

		$dm_obj = new DM();
		$dm_obj->username=$params_job['user'];
		$dm_obj->password=$params_job['pass'];
		$dm_obj->api_key=$params_job['api_key'];
		$dm_obj->api_secret=$params_job['api_secret'];

		//Configuration du nombre de tentatives d'envois
		if(!empty($params_job['nb_retries'])) {
			$nbRetriesMax = $params_job['nb_retries'];
		} else {
			$nbRetriesMax = 1;
		}

		$delay = 30;
		$retry = 0;
		$ok = false;
		
		while($ok===false && $retry<$nbRetriesMax) {	//En cas d'échec, on boucle et on recommence
			$retry++;
			$ok=true;
			$nbEssaisRestants = $nbRetriesMax - $retry;
			if($nbRetriesMax > 1) {
				$msgEssaisRestants = $retry. " essais effectués";
			}
			
			try {
				$dm_obj->connectDM();				
				$this->setProgression(50);          
				$TmpDirPath=$this->getTmpDirPath();
				$FileInPath=$this->getFileInPath();
				$video_id = $dm_obj->publish_video_dm_job($TmpDirPath,$FileInPath,$params_job);
				
				if(!empty($dm_obj->id_video_dailymotion)){
					$this->writeOutLog('DM video_id:<id>'.$dm_obj->id_video_dailymotion.'</id>');
				}		
				if(!empty($dm_obj->private_id)){
					$this->writeOutLog('DM private_id:<id>'.$dm_obj->private_id['private_id'].'</id>');
				}
				$this->writeJobLog('Dailymotion : fichier : '.$this->getFileInPath().', video_id:'.$this->id_video_dailymotion.'');
			} catch (Exception $e) {
					$ok = false;
					$this->writeOutLog("Exception : code ".$e->getCode()." : ".$e->getMessage());
					if($retry == $nbRetriesMax) {
						throw $e;
					}  else {
						sleep($delay);
						$this->writeOutLog("Problème d'envoi. Tentative n°".($retry+1)." en attente.");
					}
				}
			}
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');

	}
	
	public function finishJob()
	{
		if (file_exists($this->link_file_path))
		{
			if (unlink($this->link_file_path)===false) {
				throw new FileException('Failed to delete link ('.$this->link_file_path.')');
            }
		}
		
		parent::finishJob();
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}
?>