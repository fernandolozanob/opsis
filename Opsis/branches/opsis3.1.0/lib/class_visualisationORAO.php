<?php
include_once(libDir."class_visualisation.php");

class VisualisationORAO extends Visualisation {

	var $typeVisu;

	function __construct ($type) {

		$this->typeVisu=$type;


		if ($type=='grandPublic')
			$this->arrFiles=array(
						array('IN'=>'OraoGP.mov','OUT'=>'Orao.mov'),
						array('IN'=>'open.mov','OUT'=>'open.mov')
			);
		else
			$this->arrFiles=array(
						array('IN'=>'Orao.mov','OUT'=>'Orao.mov'),
						array('IN'=>'open.mov','OUT'=>'open.mov')
			);


	}


/** Choisit le meilleur matériel parmi le tableau des matériels d'un document
 *  IN : var de classe : tableau t_doc_mat du doc, $id_lang
 * 	OUT : la ligne de t_doc_mat correspondant au meilleur matériel
 *  NOTE : par meilleur matériel on entend :
 * 		- matériel au format visionnable (ex : mpeg4)
 * 		- matériel avec un fichier en ligne
 * 		- et SI une version existe, matériel dont la version correspond à la langue demandée (svt : la langue de l'appli)
 */
	protected function getMatForVisio($id_lang) {
		
		//trace(print_r($this->objDoc->t_doc_mat,true));
		
		foreach ($this->objDoc->t_doc_mat as $dm) {
			//Récupération du matériel pour la visio
			if (in_array($dm['MAT']->t_mat['MAT_FORMAT'],$this->getFormatsForVisu()) && $dm['MAT']->hasfile==1) {
				//Ce matériel est visionnable (format + existence fichier)
				//Certains matériels (peu) sont versionnés, dans ce cas, on choisit ce matériel si c la même langue
				if ($dm['DMAT_ID_LANG']==$id_lang) { 
					$_dm=$dm;
				} else if (!isset($_dm)) $_dm=$dm; //Sinon on garde le premier qui convient
			}	
		}
		return $_dm;
	}

	/** Récupère et prépare le visionnage et le découpage en fonction du type de visionnage
	 *
	 */

	function prepareVisu($id,$id_lang) {

		switch ($this->typeVisu) {

			case 'sequence' :
			case 'extrait' :
			case 'grandPublic' :
				// Type séquence
				include_once(modelDir.'model_doc.php');
				$this->objDoc=new Doc();
				$this->objDoc->t_doc['ID_DOC']=$id;
				$this->objDoc->t_doc['ID_LANG']=$id_lang;
				$this->objDoc->getDoc();
				$this->objDoc->getMats(); //récup ID séquences liées
				$this->objDoc->getChildren(); //récup matériel

				//A partir de là, on peut extraire tous les TC nécessaires.
				//D'abord il faut récupérer le matériel de visualisation
				$dm=$this->getMatForVisio($id_lang);				
				if (isset($dm)) {
						//On a trouvé un format apte à être visualisé, et dont le fichier existe physiquement
								//TRAITEMENT DES TIMECODE POUR LE DECOUPAGE et LE VISIONNAGE

						$this->id_mat=$dm['ID_MAT'];
						$this->tcin=$dm['DMAT_TCIN'];
						$this->tcout=$dm['DMAT_TCOUT'];
						$this->objMat=$dm['MAT'];

						$numer=tcToSec($this->tcout)-tcToSec($this->tcin);  //durée de l'extrait
						$denom=tcToSec($this->objMat->t_mat['MAT_TCOUT'])-tcToSec($this->objMat->t_mat['MAT_TCIN']); //durée totale du matériel

						// cas de découpage : pas de durée du matériel (dans le doute on découpe) ou bien ratio inférieur à la limite)
						// sinon on ne découpe pas : ratio supérieur, durée extrait inconnue,...
						if (($denom==0  || $numer/$denom<gRatioDecoupageVideo ) && $numer!=0 ) $decoup=true;

						$this->offset=(empty($this->objMat->t_mat['MAT_TCIN'])?"00:00:00:00":$this->objMat->t_mat['MAT_TCIN']);

						//Préparation du découpage
						$ok=$this->makeDir();
						if ($ok) $ok=$this->makeFileName();
						if ($ok) $ok=$this->copyRefFiles();
						//trace(print_r($this->objMat,true));

						if ($ok && $decoup ) {
							$this->makeFileName();

							if (defined('kBinDecoupage') && kBinDecoupage=='sliceOfMov' && ($this->objMat->t_mat['MAT_FORMAT']=='MPEG4' || $this->objMat->t_mat['MAT_FORMAT']=='MPG4') ) {
								$shell=kSliceOfMovpath." \"%s\" %s %s %s \"%s\" \"%s\" \n";
							} else {
								if (!strpos(PHP_OS,'Darwin')) $includeClassPath="%%CLASSPATH%%;";
								$shell="\"C:\Program Files\Java\jre1.6.0_05\bin\java\" -cp ".$includeClassPath.kJavaMpgtx." JavaMpgtx \"%s\" %s %s %s \"%s\" \n";
							}

							/*
							 && !strpos(PHP_OS,'WINNT') ) {
								$shell="/usr/local/bin/sliceOfMov \"%s\" %s %s %s \"%s\" \"%s\" & \n";
							} elseif (strpos(PHP_OS,'WINNT')) $shell="java -cp %%CLASSPATH%%;".kJavaMpgtx." JavaMpgtx \"%s\" %s %s %s \"%s\" \n";
							else $shell=kMPGTXpath." -s \"%1\$s\" [%3\$s-%4\$s]  -f -o \"%5\$s\" 2>%6\$sfich.log";
							*/

							$ok=$this->sliceMovie($shell);

						} else { //Pas de découpage, on fait une copie simple du media
							$this->makeFileName(true);
							$ok=$this->copyMovie();

						}

						if ($ok) $ok=$this->makeXML(); // génération du XML
						

				}
				else {$this->dropError(kErrorVisioNoMedia);}
			break;

			case 'commande' :
				include_once(modelDir.'model_panier.php');
				include_once(modelDir.'model_doc.php');
				$myPanierDoc=Panier::getLignePanier($id);
				if (!$myPanierDoc) {return false;}
				$this->tcin=$myPanierDoc['PDOC_EXT_TCIN'];
				$this->tcout=$myPanierDoc['PDOC_EXT_TCOUT'];
				$titre=$myPanierDoc['PDOC_EXT_TITRE'];
				$this->id_doc=$myPanierDoc['ID_DOC'];
				unset($myPanierDoc); //plus besoin
				$this->objDoc=new Doc();
				$this->objDoc->t_doc['ID_DOC']=$this->id_doc;
				$this->objDoc->t_doc['ID_LANG']=$id_lang;
				$this->objDoc->getDoc();
				$this->objDoc->getMats();
				$this->objDoc->t_doc['DOC_TITRE']=$titre; //Astuce : on passe le titre dans le champ DOC_TITRE. Pas de pb puisqu'on ne sauve pas
				
				$dm=$this->getMatForVisio($id_lang);
				if (isset($dm)) {
					$this->objMat=$dm['MAT'];
					$this->id_mat=$dm['ID_MAT'];
				}
				if (!$this->objMat) return false;

				$this->offset=(empty($this->objMat->t_mat['MAT_TCIN'])?"00:00:00:00":$this->objMat->t_mat['MAT_TCIN']);

				//Préparation du découpage
				$ok=$this->makeDir();
				if ($ok) $ok=$this->makeFileName();
				if ($ok) $ok=$this->copyRefFiles();

				if (defined('kBinDecoupage') && kBinDecoupage=='sliceOfMov' && (strpos($this->objMat->t_mat['MAT_FORMAT'],'MPEG4')!==false || strpos($this->objMat->t_mat['MAT_FORMAT'],'MPG4')!==false) ) {
					$shell=kSliceOfMovpath." \"%s\" %s %s %s \"%s\" \"%s\" \n";
				} else {
					if (strpos(PHP_OS,'WINNT')) $includeClassPath="%%CLASSPATH%%;";
					$shell="java -cp ".$includeClassPath.kJavaMpgtx." JavaMpgtx \"%s\" %s %s %s \"%s\" \n";
				}


				$ok=$this->sliceMovie($shell);
				if ($ok) $ok=$this->makeXML(); // génération du XML

			break;

			case 'materiel' :
				include_once(modelDir.'model_materiel.php');
				$this->objMat= new Materiel();
				$this->objMat->t_mat['ID_MAT']=$id;

				$this->objMat->getMat();
				$this->tcin=$this->objMat->t_mat['MAT_TCIN'];
				$this->tcout=$this->objMat->t_mat['MAT_TCOUT'];
				$this->offset=$this->objMat->t_mat['MAT_TCIN'];
				$this->id_mat=$this->objMat->t_mat['ID_MAT'];
				$this->objMat->id_lang=$id_lang;
				$ok=$this->makeDir();

				if ($ok) $ok=$this->makeFileName(true);

				if ($ok) $ok=$this->copyRefFiles();
				$ok=$this->copyMovie();
				if ($ok) $ok=$this->makeXML(); // génération du XML

			break;

		}

		if (!$ok) return false; else {
				if ($this->objDoc) logAction("VIS",Array("ID_DOC"=>$this->objDoc->t_doc['ID_DOC'], "ACT_REQ" => "DOC"));
				else logAction("VIS",array("ID_DOC"=>$this->objMat->t_mat['ID_MAT'], "ACT_REQ" => "MAT"));
				return true;
				}


	}

	/** Création et remplissage du fichier XML interprété par ORAO
	 *  Selon les types de visio et le découpage, le contenu du fichier diffère
	 *  IN : objet Visualisation
	 *  OUT : fichier XML écrit dans le répertoire temporaire + true si succès / false si échec
	 */
	function makeXML() {

		$handle=fopen($this->tempDir."orao_XML_data.xml",w);
		if (!$handle) {
			$this->dropError(kErrorVisioCreationXML);
			return false;
		}

		switch($this->typeVisu) {

			case 'sequence' :
				if ($this->sliced) {
					$generationExtrait=1;
					$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));

				} else {
					$generationExtrait=0;
					//$dynamic_url=videosServer.$this->id_mat;
				$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));

				}
				$addXML.="<nbextrait>".count($this->objDoc->arrChildren)."</nbextrait>";
				foreach ($this->objDoc->arrChildren as $i=>$chld) {
					list($tcin,$tcout)=$this->objDoc->getTC($chld['ID_DOC'],$this->id_mat,$this->objMat->t_mat['MAT_FORMAT']); //Il faut récupérer les TC de la séquence, définis dans la table t_doc_mat
					$addXML.="<extrait".($i+1)."><idExtrait>".$chld["ID_DOC"]."</idExtrait><titre>".urlencode(mb_convert_encoding($chld["DOC_TITRE"],"ISO-8859-1","UTF-8"))."</titre><tcin>".$tcin."</tcin><tcout>".$tcout."</tcout></extrait".($i+1).">";
				}
				$titre=urlencode(mb_convert_encoding($this->objDoc->t_doc['DOC_TITRE'],"ISO-8859-1","UTF-8"));
				$custom=session_id()."_".$this->typeVisu."_".$this->objDoc->t_doc['ID_DOC'];
				$langId=strtolower($this->objDoc->t_doc['ID_LANG']);
			break;

			case 'extrait' :
				if ($this->sliced) {
					$generationExtrait=1;
					$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));

				} else {
					$generationExtrait=0;
					//$dynamic_url=videosServer.$this->id_mat;
				$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));

				}
				$titre=urlencode(mb_convert_encoding($this->objDoc->t_doc['DOC_TITRE'],"ISO-8859-1","UTF-8"));
				$custom=session_id()."_".$this->typeVisu."_".$this->objDoc->t_doc['ID_DOC'];
				$langId=strtolower($this->objDoc->t_doc['ID_LANG']);
			break;

			case 'grandPublic' : // On ajoute les infos sur le DOC
				if ($this->sliced) {
					$generationExtrait=1;
					$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));

				} else {
					$generationExtrait=0;
					//$dynamic_url=videosServer.$this->id_mat;
				$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));

				}
				$this->objDoc->getLexique();
				$this->objDoc->getValeurs();
				$xml=$this->objDoc->xml_export();
				$xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";
				$content = TraitementXSLT($xml,getSiteFile("designDir","docOrao.xsl"),array(),0);
				ob_start();
				eval("?>". $content. "<?");
				$content=ob_get_contents();
				ob_end_clean();
				// on remplit la balise avec le contenu + on est en mode VISIONNAGE seul
				$addXML="<texteGP>".str_replace(array("%0A","%09"),array("\\r",""),urlencode(mb_convert_encoding($content,"ISO-8859-1","UTF-8")))."</texteGP>";
				$typeEditionOpsis=2;
				$titre=urlencode(mb_convert_encoding($this->objDoc->t_doc['DOC_TITRE'],"ISO-8859-1","UTF-8"));
				$custom=session_id()."_".$this->typeVisu."_".$this->objDoc->t_doc['ID_DOC'];
				$langId=strtolower($this->objDoc->t_doc['ID_LANG']);
			break;

			case 'commande' :
				$typeEditionOpsis=2;
				$generationExtrait=1;
				$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));
				$this->id_mat=0; // id_mat doit être mis à zéro. Je n'ai pas d'explications
				$titre=urlencode(mb_convert_encoding($this->objDoc->t_doc['DOC_TITRE'],"ISO-8859-1","UTF-8"));
				$custom=session_id()."_".$this->typeVisu."_".$this->objDoc->t_doc['ID_DOC'];
				$langId=strtolower($this->objDoc->t_doc['ID_LANG']);
			break;

			case 'materiel' :
				$typeEditionOpsis=0;
				$generationExtrait=0;
				//$dynamic_url=videosServer.$this->id_mat;
				$dynamic_url=kVisionnageUrl.$this->tempDirShort."/".rawurlencode(mb_convert_encoding($this->fileOut,"ISO-8859-1","UTF-8"));

				$titre=urlencode(mb_convert_encoding($this->id_mat,"ISO-8859-1","UTF-8"));
				$langId=strtolower($this->objMat->id_lang);
				$custom=session_id()."_".$this->typeVisu."_".urlencode(mb_convert_encoding($this->id_mat,"ISO-8859-1","UTF-8"));
				$this->objMat->getDocs();
				$addXML.="<nbextrait>".count($this->objMat->t_doc_mat)."</nbextrait>";
				foreach ($this->objMat->t_doc_mat as $i=>$dm) {
					list($tcin,$tcout)=$dm['DOC']->getTC($dm['DOC']->t_doc['ID_DOC'],$this->id_mat,$this->objMat->t_mat['MAT_FORMAT']); //Il faut récupérer les TC de la séquence, définis dans la table t_doc_mat
					$addXML.="<extrait".($i+1)."><idExtrait>".$dm['DOC']->t_doc["ID_DOC"]."</idExtrait><titre>".urlencode(mb_convert_encoding($dm['DOC']->t_doc["DOC_TITRE"],"ISO-8859-1","UTF-8"))."</titre><tcin>".$dm['DMAT_TCIN']."</tcin><tcout>".$dm['DMAT_TCOUT']."</tcout></extrait".($i+1).">";
				}
			break;

		}

		fwrite($handle,"<dynamic_url>".$dynamic_url."</dynamic_url>
						<generationExtrait>".$generationExtrait."</generationExtrait>
						<typeEditionOpsis>".$typeEditionOpsis."</typeEditionOpsis>
						<cheminLocalVideo>".$this->tempDir."</cheminLocalVideo>
						<format>".$this->objMat->t_mat['MAT_FORMAT']."</format>
						<deny>false</deny>
						<idNotice>".$this->objDoc->t_doc['ID_DOC']."</idNotice>
						<idMateriel>".$this->id_mat."</idMateriel>
						<titreProg>".$titre."</titreProg>
						<langId>".$langId."</langId>
						<custom>".$custom."</custom>
						<tcin>".$this->tcin."</tcin>
						<tcout>".$this->tcout."</tcout>
						<offset>".$this->offset."</offset>");

		if ($addXML) fwrite($handle,$addXML);
		fclose($handle);
		return true;
	}


	/** Création du code HTML pour lancer le visionnage
	 *  IN :
	 * 	OUT :
	 */
	function renderComponent($print=true) {
		$html="<script type=\"text/javascript\">
		    <!-- hide from pre-script browsers
		         var haveqt = false;
			 var version = 0;
		    //-->
		    </script>

		    <script language=\"VBScript\">
		    <!-- hide from pre-script browsers
		    On Error Resume Next
		    Set theObject = CreateObject(\"QuickTimeCheckObject.QuickTimeCheck.1\")
		    On Error goto 0

		    If IsObject(theObject) Then
		         If theObject.IsQuickTimeAvailable(0) Then 'Just check for file'
		              haveqt = true
			      version = theObject.QuickTimeVersion
		         End If
		    End If
		    //-->
		    </script>
		    <script type=\"text/javascript\">
		    <!-- hide from pre-script browsers
		     //conversion de decimal vers binaire afin d'obtenir la version
		     var hexa='0123456789ABCDEF',hex='', versiontemp=\"\", detect=\"\", safari=''
		     while (version>15)
		     {
		          tmp=version-(Math.floor(version/16))*16;
		          hex=hexa.charAt(tmp)+hex;
		          version=Math.floor(version/16);
		     }
		     hex=hexa.charAt(version)+hex;
		     version=hex/1000000;

			if (navigator.plugins) {
		              for (i=0; i < navigator.plugins.length; i++ ) {
		                   if (position=navigator.plugins[i].name.indexOf(\"QuickTime\") >= 0)
		                        {
									haveqt = true;
									//version=versiontemp.concat(navigator.plugins[i].name.substring(18,21),navigator.plugins[i].name.substring(22,23));
									version=versiontemp.concat(navigator.plugins[i].name.substring(navigator.plugins[i].name.lastIndexOf(\" \")));
									versiontemp=\"\";
									//document.write(version);
									//document.write(\"<br>\");
									if(version.indexOf(\".\")!=version.lastIndexOf(\".\"))
									{
										version=versiontemp.concat(version.substring(1,4),version.substring(version.lastIndexOf(\".\")+1));
									}
									//document.write(version);
									//document.write(\"<br>\");
									version = parseFloat(version);
							}
		               }
					   //document.write(version);
			}
				detect = navigator.userAgent.toLowerCase();
			if(detect.indexOf(\"safari\") >= 0) safari=true;
			if (version >=7.3 ) { // plus de support de flash, il faut lancer la visio QT
				document.write(\"<p style='font-size:10px;font-family:Arial'>".kWarningQTVersion731."</p>\");
				window.open('popupVideo.php?id=".$_REQUEST['id']."&type=".$this->typeVisu."','visionnage','height=335,width=512,screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=0,scrollbars=0,menubar=0,toolbar=0');
			}
			else if ((haveqt && version >=".gQTversion.") || safari==true){
					document.write('".kLancementVideo ."<br/><object CLASSID=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\"  WIDTH=\"200\" HEIGHT=\"100\" CODEBASE=\"http://www.apple.com/qtactivex/qtplugin.cab\"><param name=\"SRC\" VALUE=\"".kVisionnageUrl.$this->tempDirShort."/open.mov\"><PARAM NAME=\"target\" VALUE=\"quicktimeplayer\"><param name=\"HREF\" VALUE=\"".kVisionnageUrl.$this->tempDirShort."/Orao.mov\"><param name=\"AUTOPLAY\" VALUE=\"TRUE\"><param name=\"AUTOHREF\" VALUE=\"TRUE\"><param name=\"KIOSKMODE\" VALUE=\"TRUE\"><embed TARGET=\"quicktimeplayer\" src=\"".kVisionnageUrl.$this->tempDirShort."/open.mov\" AUTOHREF=\"TRUE\" HREF=\"".kVisionnageUrl.$this->tempDirShort."/Orao.mov\" WIDTH=\"200\" HEIGHT=\"100\" AUTOPLAY=\"TRUE\" KIOSKMODE=\"TRUE\" TYPE=\"video/quicktime\" PLUGINSPAGE=\"http://www.apple.com/quicktime/download/\"></embed></object>');}
			else {
					document.write('<p>".kMsgQTversion.gQTversion ."</p><p><a href=\"http://www.apple.com/quicktime/download\" target=\"blank\"><img src=\"../../design/images/quicktime.gif\" border=0></a></p>');
			}
		    //-->
		   </script>
			";
			if ($print) echo $html; else return $html;

	}


	function sauvegardeCapsule($urlParams) {

	    global $db;
	    trace('params sauve capsule');
	    trace (print_r($urlParams,true));
	    $idExtrait=simpleQuote($urlParams["idExtrait"]);
	    $sessionEntiere=$urlParams["session"];
	    $tcin=simpleQuote($urlParams["tcin"]);
	    $tcout=simpleQuote($urlParams["tcout"]);
	    $duree = simpleQuote(secToTime(tcToSec(trim($urlParams["tcout"]))-tcToSec(trim($urlParams["tcin"]))));
	    $titreExtrait=simpleQuote(mb_convert_encoding($urlParams["titreExtrait"],"UTF-8","ISO-8859-1"));

	    $id_lang=strtoupper($_SESSION["langue"]);

	    $sessionTab=explode("_",simpleQuote($urlParams["custom"]),3);
	    $typeVisionnage=$sessionTab[1];
	    $idDoc=$sessionTab[2];
	   	$id_usager=$_SESSION["ID_USAGER"];



	//$handle=fopen(mediasVisionnageChemin."/debug.txt",w);

	    $sql="SELECT * FROM t_session2_ado WHERE SESSKEY='".$sessionTab[0]."'";

	    $sessionResult=$db->Execute($sql);
	    if($sessionResult->RecordCount()>0){

			// Envoi du header (QT7 Windows)
	    	ob_clean();
	    	ob_start();
	        if(!strcmp($typeVisionnage,"extrait")){
				// I. Cas des extraits
	        //if(strcmp($sessionTab[0],session_id())==0){
	            $sql="SELECT ID_LIGNE_PANIER FROM t_panier_doc WHERE ID_LIGNE_PANIER=".intval($idExtrait);
	            $result=$db->Execute($sql);
	            if($result->RecordCount()>0){
	                $sql="UPDATE t_panier_doc SET PDOC_EXT_TITRE=".$db->Quote($titreExtrait).", PDOC_EXT_TCIN='".$tcin."', PDOC_EXT_TCOUT='".$tcout."' WHERE ID_LIGNE_PANIER=".intval($idExtrait);
	                $db->Execute($sql);
	                print("<idExtrait>".$idExtrait."</idExtrait>");
	            }
	            else{
	                $sql="SELECT t_panier.ID_PANIER FROM t_panier JOIN t_session2_ado tsa on t_panier.PAN_ID_USAGER||''=tsa.EXPIREREF where tsa.SESSKEY='".$sessionTab[0]."' AND PAN_ID_ETAT='1'";
	                $result=$db->GetAll($sql);
	                if($result) {
	                	$idPanier=$result[0]['ID_PANIER'];
	                }
	                else {
	                    // Creation panier
	                    $sql="SELECT expireref FROM t_session2_ado where sesskey='".$sessionTab[0]."'";
	                    $result=$db->GetRow($sql);
	                    if($result->RecordCount()>0) {
	                        $id_usager=$result['expireref'];
							$now = str_replace("'","",$db->DBTimeStamp(time()));
							$idPanier =$db->insertBase("t_panier","ID_PANIER",array("PAN_ID_ETAT" => 1, "PAN_ID_USAGER" => intval($id_usager), "PAN_TITRE" => kSanstitre, "PAN_DATE_CREA" => $now));
	                    }
	                }
	                if(isset($idPanier)){ //Note : titreExtrait est déjà "échappé" par la capsule donc pas de db->Quote
						$idExtrait = $db->insertBase("t_panier_doc","id_ligne_panier",array("ID_DOC" => intval($idDoc), "ID_PANIER" => intval($idPanier), "PDOC_EXT_TITRE" => $titreExtrait, "PDOC_EXT_TCIN" => $tcin, "PDOC_EXT_TCOUT" => $tcout, "PDOC_EXTRAIT" => 1, "PDOC_ID_ETAT" => 1));
	                    print("<idExtrait>".$idExtrait."</idExtrait>");
	                }
	            }
	            // A FAIRE : Supprimer les autres extraits

	        }
	        elseif(!strcmp($typeVisionnage,"sequence")){
				// II. Cas de la saisie de s�quences
				// Filtrage apostrophe FT
				$titreExtrait=str_replace("'"," ' ",$titreExtrait);
	            $sql="SELECT ID_DOC FROM t_doc WHERE ID_DOC=".intval($idExtrait);
	            $result=$db->Execute($sql);
	            if($result->RecordCount()>0){
					// II.1 Mise � jour s�quence
					// II.1.a Mise � jour document
	                $sql="UPDATE t_doc SET doc_titre=".$db->Quote($titreExtrait).", DOC_DUREE='".$duree."', DOC_TCIN='".$tcin."', DOC_TCOUT='".$tcout."',DOC_ID_USAGER_MODIF=".intval($id_usager).", DOC_DATE_MOD=NOW() WHERE id_doc=".intval($idExtrait)." and ID_LANG='".$id_lang."'";
	                $result=$db->Execute($sql);
	                print("<idExtrait>".$idExtrait."</idExtrait>");
	                //->freeSQL($result); // sort un warning
					// II.1.b Mise � jour liens document-mat�riel
	                // Liens materiels
	                $sql="select ID_MAT from t_doc_mat where ID_DOC=".intval($idExtrait);
	                $result=$db->Execute($sql);
	                while($row=$result->FetchRow()){
	                    $id_mat_row=$row["ID_MAT"];
	                    $sql="UPDATE t_doc_mat set DMAT_TCIN='".$tcin."',DMAT_TCOUT='".$tcout."'";
	                    $sql.=" where ID_MAT=".intval($id_mat_row)." and ID_DOC=".intval($idExtrait);
	                    $db->Execute($sql);
	                }
	                $result->Close();
	            }
	            else{
					// II.2 Cr�ation s�quence
					// II.2.a Cr�ation document
					
	                $sql="INSERT INTO t_doc (ID_LANG, DOC_ID_MEDIA, DOC_ID_GEN, DOC_TITRE, DOC_DUREE,
					DOC_TCIN, DOC_TCOUT, DOC_ID_USAGER_CREA, DOC_DATE_CREA, DOC_ID_IMAGEUR, DOC_NUM, DOC_ID_FONDS,
					DOC_DATE_PROD, DOC_ORI_COULEUR,DOC_ID_TYPE_DOC)";
	                //$sql.=" VALUES(NULL,'".$id_lang."','V','".$idDoc."',".$db->Quote($titreExtrait).",'".$duree."', '".$tcin."','".$tcout."')";
	                $sql.=" select '".$id_lang."',DOC_ID_MEDIA,".intval($idDoc).",".$db->Quote($titreExtrait).",'".$duree."', '".$tcin."','".$tcout."',".intval($id_usager).",NOW()";
					$sql.=", DOC_ID_IMAGEUR, DOC_NUM, DOC_ID_FONDS, DOC_DATE_PROD,
						DOC_ORI_COULEUR,".(defined("gTypeDocSequence")?"'".gTypeDocSequence."'":"DOC_ID_TYPE_DOC")." from t_doc where ID_DOC=".intval($idDoc)." and ID_LANG='".$id_lang."'";
	          
	                $result=$db->Execute($sql);
	                $idExtrait = $db->Insert_ID("t_doc");

	                // Création des versions pour les autres langues
					foreach ($_SESSION['arrLangues'] as $lg) {
						if ($lg !=$id_lang) {
							$sql="INSERT INTO t_doc (ID_DOC, ID_LANG, DOC_ID_MEDIA, DOC_ID_GEN, DOC_TITRE, DOC_DUREE,
							DOC_TCIN, DOC_TCOUT, DOC_ID_USAGER_CREA, DOC_DATE_CREA, DOC_ID_IMAGEUR, DOC_NUM, DOC_ID_FONDS,
							DOC_DATE_PROD, DOC_ORI_COULEUR,DOC_ID_TYPE_DOC)
							select ".intval($idExtrait).",'".$lg."',DOC_ID_MEDIA,DOC_ID_GEN,DOC_TITRE,DOC_DUREE,DOC_TCIN,
							DOC_TCOUT,DOC_ID_USAGER_CREA,NOW(), DOC_ID_IMAGEUR, DOC_NUM, DOC_ID_FONDS, DOC_DATE_PROD,
							DOC_ORI_COULEUR,DOC_ID_TYPE_DOC from t_doc where ID_DOC=".intval($idExtrait)." and ID_LANG='".$id_lang."'";
							
							$db->Execute($sql);
						}
					}

					// II.2.b S�lection image repr�sentative
					$result=$db->Execute("select DOC_ID_IMAGEUR from t_doc where id_doc=".intval($idExtrait));
					$row=$result->FetchRow();
					if($row["DOC_ID_IMAGEUR"]!=''){
						$result1 = $db->Execute("select ID_IMAGE from t_image where ID_IMAGEUR='".$row["DOC_ID_IMAGEUR"]."' and IM_TC=(select min(IM_TC) from t_image where ID_IMAGEUR='".$row["DOC_ID_IMAGEUR"]."' and IM_TC>='".$tcin."')");
						if($list1=$result1->FetchRow())
							$db->Execute("update t_doc set DOC_ID_IMAGE =".intval($list1["ID_IMAGE"])." where ID_DOC=".intval($idExtrait));
						$result1->Close();
						}
					// II.2.c Liens materiels
	                $sql="select ID_MAT from t_doc_mat where ID_DOC=".intval($idDoc);
	                $result=$db->Execute($sql);
	                while($row=$result->FetchRow()){
	                    $id_mat_row=$row["ID_MAT"];
	                    $sql="INSERT INTO t_doc_mat (ID_DOC,ID_MAT,DMAT_TCIN,DMAT_TCOUT)";
	                    $sql.=" values (".intval($idExtrait).",".intval($id_mat_row).",'".$tcin."','".$tcout."')";
	                    $db->Execute($sql);
	                }
	                $result->Close();
	                print("<idExtrait>".$idExtrait."</idExtrait>");
	            }
	            // A FAIRE : Supprimer les autres sequences
	        }
	        elseif(strpos("materiel",$typeVisionnage)==0){
				// III. Cas des mat�riels
	        	// Recuperation ID_MAT
	            $id_mat=$idDoc;
				// Filtrage apostrophe FT
				$titreExtrait=str_replace("'"," ' ",$titreExtrait);

	            $sql="SELECT id_doc FROM t_doc WHERE id_doc=".intval($idExtrait)." and ID_LANG='".$id_lang."'";
	            $result=$db->Execute($sql);
	            if($result->RecordCount()>0){
	                $sql="UPDATE t_doc SET DOC_TITRE=".$db->Quote($titreExtrait).", DOC_DUREE='".$duree."', DOC_TCIN='".$tcin."', DOC_TCOUT='".$tcout."', DOC_ID_USAGER_MODIF=".intval($id_usager).", DOC_DATE_MOD=NOW() WHERE ID_DOC=".intval($idExtrait)." and ID_LANG='".$id_lang."'";
	                $db->Execute($sql);
	                print("<idExtrait>".$idExtrait."</idExtrait>");
	                // Liens materiels
	                $sql="UPDATE t_doc_mat SET DMAT_TCIN='".$tcin."',DMAT_TCOUT='".$tcout."' WHERE ID_DOC=".intval($idExtrait)." and ID_MAT=".intval($id_mat);
	                $db->Execute($sql);
	            }
	            else{
	                $sql="INSERT INTO t_doc (ID_LANG, DOC_ID_MEDIA, DOC_TITRE, DOC_DUREE, DOC_TCIN, DOC_TCOUT, DOC_ID_USAGER_CREA, DOC_DATE_CREA)";
					$sql.=" VALUES('".$id_lang."','V',".$db->Quote($titreExtrait).",'".$duree."','".$tcin."','".$tcout."',".intval($id_usager).",NOW())";
	                $result=$db->Execute($sql);
	                $idExtrait = $db->Insert_ID("t_doc");
	                print("<idExtrait>".$idExtrait."</idExtrait>");
	                // Liens materiels

	                include (modelDir.'model_doc.php');
	                $myDoc=new Doc();
	                $myDoc->t_doc['ID_DOC']=$idExtrait;
	                $myDoc->t_doc['DOC_TCIN']=$tcin;
	                $myDoc->t_doc['DOC_TCOUT']=$tcout;
	                $myDoc->insererDocMat($id_mat);
	                unset($myDoc);
	            }
	        }
			// Envoi du header (QT7 Windows)
			print ("   ");
			header('Content-Length:'.ob_get_length());
			ob_end_flush();
	    }
	    else print "log please";

	//fclose($handle);

	}

}
?>
