<?
/**
* Classe d'affichage de formulaires
*/
require_once(libDir."class_form.php");

class FormSaisie extends Form {

	var $countElt;		// Compteur utilisé pour les formulaires de recherche
	var $saisieObj;		// Objet saisie / ex : $myDoc
	var $saisieTab;		// Tableau saisie / ex : $myDoc->t_doc

	/**
		* Affiche début formulaire
	 * IN : tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	function beginForm($attr){
		print "<div id='chooser' class='iframechoose' name='chooser' style='position:absolute;display:none;'></div>\n";
		print "<script>makeDraggable(document.getElementById('chooser'));</script>\n";
		print "<form method='post' ";
		if(isset($attr['NAME'])) print " NAME='".$attr['NAME']."'";
		if(isset($attr['ID'])) print " ID='".$attr['ID']."'";
		if(isset($attr['ACTION'])) print " ACTION='".$attr['ACTION']."'";
		if(isset($attr['ONSUBMIT'])) print " ONSUBMIT='".$attr['ONSUBMIT']."'";
		if(isset($attr['ENCTYPE'])) print " ENCTYPE='".$attr['ENCTYPE']."'";
		print " >\n";
		// Spécifique saisie
			// Variables spécifiques
		// VP 18/03/09 : ajout cas PERS et USAGER
		// LD 04 05 09 : ajout ETAPE
			switch($this->entity){
                case "CAT":
                    $this->saisieTab = $this->saisieObj->getValues();
                    break;
                    
				case "DOC":
					$this->saisieTab=$this->saisieObj->t_doc;
					break;

                case "DMAT":
                    $this->saisieTab=$this->saisieObj->t_doc_mat;
                    break;
                    
                case "ETAPE":
                    $this->saisieTab=$this->saisieObj->t_etape;
                    break;
                    
                case "EXP":
                    $this->saisieTab = $this->saisieObj->t_exploitation;
                    break;
                    
                case "FEST":
                    $this->saisieTab = $this->saisieObj->t_festival;
                    break;
                    
               case "FONDS":
                    $this->saisieTab=$this->saisieObj->t_fonds;
                    break;
                    
                case "HTML":
                    $this->saisieTab = $this->saisieObj->t_html;
                    break;
                    
				case "MAT":
					$this->saisieTab=$this->saisieObj->t_mat;
					break;

                case "PANIER":
                    $this->saisieTab = $this->saisieObj->t_panier;
                    //Ajoute les champs PANXML à t_panier pour retrouver leur valeurs
					if (is_array($this->saisieObj->t_panier['XML']['commande'])){
						// l'ordre du merge a été inversé de façon à ce qu'un champ PANXML qui aurait été manuellement ajouté à t_panier conserve sa valeur au lieu d'etre écrasé par la version récupérée directement depuis t_panier[XML]
						// ce workaround est necessaire car beaucoup de fonction des class_form ne savent pas gérer les array / les xml ..
						$this->saisieTab = array_merge($this->saisieObj->t_panier['XML']['commande'],$this->saisieTab);
					}
                    break;
                    
				case "PERS":
					$this->saisieTab=$this->saisieObj->t_personne;
					break;

                case "PREX":
                    $this->saisieTab = $this->saisieObj->t_prex;
                    break;
                    
                case "TS":
                    $this->saisieTab = $this->saisieObj->t_tape_set;
                    break;
                    
				case "USAGER":
					$this->saisieTab=$this->saisieObj->t_usager;
					break;

				case "VAL":
					$this->saisieTab=$this->saisieObj->t_val;
					break;

				default:
					echo "<div class='error'>".kEntiteNonGeree."</div>";
					break;

			}
			// Champs spécifiques saisie
			// VP 4/06/09 : ajout valeur SAVE par défaut à commande
			print "
<input name='commande' type='hidden' value='SAVE' />
<input name='page' type='hidden' value='' />
";
			if($this->entity=="DOC"){
				print "
<input name='version2suppr' type='hidden' value='' />
<input name='save_action' type='hidden' value='SAVE_DOC' />
<input name='id_doc2link' type='hidden' value=\"".$this->saisieObj->id_doc2link."\" />
<input name='relation' type='hidden' value=\"".$this->saisieObj->relation."\" />
";
			}else if($this->entity=="PERS"){
				print "
<input name='version2suppr' type='hidden' value='' />
<input name='fldToPassToRedirection' type='hidden' value='valeur' />
<input name='fusion' type='hidden' value='' />
";
			}else if($this->entity=="MAT"){
				print "
<input name = 'mat_id_doc' id = 'mat_id_doc' type = 'hidden' value ='".$mat_id_doc."' />
<input name='id_mat_ref' id='id_mat_ref' type='hidden' value='".$this->saisieTab['MAT_NOM']."' />
";
			}else if ($this->entity == "VAL"){
				print "	<input type='hidden' id='fusionVal' name='fusionVal' value='' />
			    <input name='fldToPassToRedirection' value='valeur' type='hidden' />
";


			}
		print "<table border='0' cellspacing='0' cellpadding='0' ".(isset($attr['CLASS'])?" CLASS=".$attr['CLASS']:"").">\n";
	}


	/**
	 * Mise à jour paramètre
	 * IN : tableau de paramètres
	 * OUT : tableau de paramètres
	 **/
	function parseField(&$attr){
		if(!empty($attr['OPSFIELD'])){
			$arrCrit=explode("_",$attr['OPSFIELD']);
			switch ($arrCrit[0]) {
				case "V":
					$attr['CHFIELDS'] = $arrCrit[1];
					$attr['SUBTYPE'] = "VI";
					break;
					
				case "CAT":
					$attr['CHFIELDS'] = $arrCrit[1];
					$attr['SUBTYPE'] = "CAT";
					break;
					
				case "L":
					$attr['CHFIELDS'] = $arrCrit[1];
					$attr['SUBTYPE'] = "LI";
					if(isset($arrCrit[2]))
						$attr['ROLE_VALUE'] = $arrCrit[2];
					break;
					
				case "P":
					$attr['CHFIELDS'] = $arrCrit[1];
					$attr['SUBTYPE'] = "P";
					if(isset($arrCrit[2]))
						$attr['ROLE_VALUE'] = $arrCrit[2];
					break;

				case "MAT":
					$attr['CHFIELDS'] = $attr['OPSFIELD'];
					$attr['SUBTYPE'] = "MAT";
					break;

				default:
					$attr['CHFIELDS'] = $attr['OPSFIELD'];
					break;
					
			}
		}
		// Affectation selon CHFIELDS
		switch($attr['CHFIELDS']){
			case "US_CREA":
				if($this->saisieObj)
					$attr['CHVALUES'] = $this->saisieObj->user_crea;
				break;
			case "US_MODIF":
				if($this->saisieObj)
					$attr['CHVALUES'] = $this->saisieObj->user_modif;
				break;
		}
		// Compatiblité SUBTYPE
		if(isset($attr['MULTI_TYPE']))
			$attr['SUBTYPE'] = $attr['MULTI_TYPE'];
		if(isset($attr['SELECT_TYPE']))
			$attr['SUBTYPE'] = $attr['SELECT_TYPE'];
		if(isset($attr['RADIO_TYPE']))
			$attr['SUBTYPE'] = $attr['RADIO_TYPE'];
		if(isset($attr['BUTTON_TYPE']))
			$attr['SUBTYPE'] = $attr['BUTTON_TYPE'];
		if(isset($attr['SPAN_TYPE']))
			$attr['SUBTYPE'] = $attr['SPAN_TYPE'];
	}

	/**
	* Affiche d'un élément
	 * IN : tableau de paramètres
	 * OUT : affichage de l'élément
	 **/
	function displayField($name, $chValue, $attr){
		$this->parseField($attr);
		if(isset($attr['CHFIELDS'])){
			// PC 10/08/10 : possibilité de forcer NAME par NAME
			if (!isset($attr['NAME'])) $name=strtolower($attr['CHFIELDS']);

			// VP 30/11/09 : possibilité de forcer VALUE par CHVALUES
			if (!isset($this->saisieTab[strtoupper($name)]))
				$this->saisieTab[strtoupper($name)]=null;

			$chValue=(isset($attr['CHVALUES'])?$attr['CHVALUES']:$this->saisieTab[strtoupper($name)]);
			// Affichage champ principal
			// ATTENTION : formChanged ne marche pas pour radio, checkbox et multi
			if (!isset($attr['ONKEYPRESS']))
				$attr['ONKEYPRESS']='';

			if (!isset($attr['ONCHANGE']))
				$attr['ONCHANGE']='';

			$attr['ONKEYPRESS'].=";formChanged=true;";
			$attr['ONCHANGE'].=";formChanged=true;";
		}

		// VP 2/06/09 : ajout cas "buttonspan"
		// VP 24/06/09 : ajout cas "span"
		// VP 27/11/09 : ajout RTEditor (fckEditor)
		switch(strtolower($attr['TYPE'])){
			case "textarea":
				$this->addTextarea($name, $chValue, $attr);
				// Versions
				if (isset($attr['VERSIONS']) && $attr['VERSIONS']==1){
					foreach ($this->saisieObj->arrVersions as $idx=>$version) {
						$name="version[".$version['ID_LANG']."][".$attr['CHFIELDS']."]";
						$value=$version[strtoupper($attr['CHFIELDS'])];
						echo "<br/>".$version['ID_LANG']."&nbsp;";
						$this->addTextarea($name, $value, $attr);
					}
				}
				break;

			case "rte":
				$this->addRTEditor($name, $chValue, $attr);
				break;

			case "select":
				if (!isset($attr['SUBTYPE']))
					$attr['SUBTYPE']=null;

				$this->addSelect($attr['SUBTYPE'], $name, $chValue, $attr);
				break;

			case "radio":
				$this->addRadio($attr['SUBTYPE'], $name, $chValue,$attr);
				break;

			case "checkbox":
				if (!isset($attr['SUBTYPE']))
					$attr['SUBTYPE']=null;

				$this->addCheckbox($attr['SUBTYPE'], $name, $chValue,$attr);
				break;

			case "multi":
				$this->addMulti($attr['SUBTYPE'], $attr['CHFIELDS'], $attr);
				break;

            case "multitext":
				$this->addMultiText($attr['SUBTYPE'], $attr['CHFIELDS'], $attr);
				break;

			case "tree_checkbox":
				$this->addTree('checkbox',$attr['CHFIELDS'],$attr);

				break ;
			case "buttonspan":
				if (!isset($attr['SUBTYPE']))
					$attr['SUBTYPE']='button';

				$this->addButton($attr['SUBTYPE'], $name, $chValue, $attr);
				break;

			case "span":
				$this->addSpan( $name, $chValue, $attr);
				break;

			//PC 04/10/10 : ajout cas vignette
			case "vignette":
				$this->addVignette( $name, $chValue, $attr);
				break;

			case "image":
				$this->addImage($attr['TYPE'], $name, $chValue, $attr);
				break;

            case "file_image" :
				$this->addFileImage($attr['TYPE'],$name,$chValue,$attr);
				break ;
                
			case "captcha":
				$this->addCaptcha( $name, $chValue, $attr);
				break;

			case "custom":
				$this->addCustom($name, $chValue, $attr);
				break;

			default:
				if(isset($attr['SUBTYPE'])) $this->addInputSaisie($attr['TYPE'], $name, $chValue,$attr);
				else $this->addInput($attr['TYPE'], $name, $chValue, $attr);
				// Versions
				if(isset($attr['VERSIONS']) && $attr['VERSIONS']==1){
					foreach ($this->saisieObj->arrVersions as $idx=>$version) {
						$name="version[".$version['ID_LANG']."][".$attr['CHFIELDS']."]";
						$value=$version[strtoupper($attr['CHFIELDS'])];
						echo "<br/>".$version['ID_LANG']."&nbsp;";
						$this->addInput($attr['TYPE'], $name, $value, $attr);
					}
				}
				break;
		}

		// Affichage index
		if(isset($attr['INDEX'])) {
			if(!isset($attr['INDEX_NAME'])) $attr['INDEX_NAME']="bIndex_".$name;
			//if(!isset($attr['INDEX_CHOOSE'])) $attr['INDEX_CHOOSE']="document.getElementById(\"".$name."\")";
			if(!isset($attr['INDEX_CHOOSE'])) $attr['INDEX_CHOOSE']="this";
			//MSTEST
			if( $this->entity=='DOC' && isset($this->saisieTab['ID_LANG']) && !empty($this->saisieTab['ID_LANG'])){
				$attr['ID_LANG'] = $this->saisieTab['ID_LANG'];
			}
			$this->addIndexBtn($attr['INDEX_NAME'], $attr['INDEX_CHOOSE'], $attr);
		}
	}


	function addCaptcha($type, $nom, $attr)
	{
		echo '<img id="captcha_form" src="captcha.php?'.md5(uniqid()).'" alt="captcha" />';
		echo '<a href="javascript:void(0);"  tabindex="-1" class="reload_cap"  onclick="$j(\'#captcha_form\').attr(\'src\',\'captcha.php?\'+Math.random())"><img src="design/images/refresh.gif" alt="reload" title="'.kRecharger.'"/></a>';

		if (isset($attr['LABEL']) && !empty($attr['LABEL']))
			$attr['LABEL']=defined($attr['LABEL'])?constant($attr['LABEL']):$attr['LABEL'];

		
		echo '<p><input type="text" autocomplete="off" '.(isset($attr['LABEL'])?'label="'.$attr['LABEL'].'"':'').' name="'.$attr['CHFIELDS'].'" ';
		
		if ($attr['MANDATORY']=='1')
			echo 'mandatory="1" ';

		echo '/></p>';
	}



/**
* Affiche un champ contenant une vignette ainsi que deux boutons pour charger ou supprimer
 * IN : type de champ vignette, nom, tableau de paramètres
 * OUT : affichage du champ et mise à jour tableau params
 **/
	function addVignette($type, $nom, &$attr){
		global $db;

		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;

		$id_doc_acc = $this->saisieObj->getVignette();


		if (isset($this->saisieTab['ID_DOC']))
			$id='id_doc='.$this->saisieTab['ID_DOC'];
		else if (isset($this->saisieTab['ID_PERS']))
			$id='id_pers='.$this->saisieTab['ID_PERS'];
		else if (isset($this->saisieTab['ID_PANIER']))
			$id='id_panier='.$this->saisieTab['ID_PANIER'];
		else if (isset($this->saisieTab['ID_FEST']))
			$id='id_fest='.$this->saisieTab['ID_FEST'];
		else if (isset($this->saisieTab['ID_CAT']))
			$id='id_cat='.$this->saisieTab['ID_CAT'];
		else if (isset($this->saisieTab['ID_EXP']))
			$id='id_exp='.$this->saisieTab['ID_EXP'];

		$pos = strrpos($this->saisieObj->vignette, ".");
		$addlink = ($pos > 0 && strcmp(substr($this->saisieObj->vignette, $pos), '.pdf') == 0);
		if ($addlink)
			echo '<a href="'.kCheminHttpMedia.$this->saisieObj->vignette.'" target="blank">';

		print "<img id=\"vignette_" . $attr['CHFIELDS'] . "\" ";
		if ($this->saisieObj->vignette){
			//print "src=\"makeVignette.php?image=".$this->saisieObj->vignette."&amp;type=doc_acc&amp;w=120&amp;h=120&amp;kr=1\"";
			//PC 08/04/13 : enlevage du param "type=doc_acc" car provoque un bug pour les imagette issus de storyboards...
			print "src=\"makeVignette.php?image=".$this->saisieObj->vignette."&amp;w=120&amp;h=120&amp;kr=1\"";
		}
        if (isset($attr['LINK_URL']) && !empty($attr['LINK_URL'])) {
            if(isset($attr['LINK_ID'])){
				$link = str_replace("[".$attr['LINK_ID']."]", $this->saisieTab[strtoupper($attr['LINK_ID'])], $attr['LINK_URL']);
			}else{
				// devrait matcher une suite de champs définis tels que [ID_DOC],[ID_PANIER] par ex
				preg_match_all("/\[([\S]+?)\]/",$attr['LINK_URL'],$matches);
				$link =$attr['LINK_URL']; 
				// ensuite, en fonction de ce qu'on trouve dans le tableau saisieTab, devrait permettre de faire le remplacement [ID_DOC] sera donc remplacé par la valeur contenue dans saisieTab[ID_DOC]
				if(isset($matches[1]) && !empty($matches[1])){
					foreach($matches[1] as $match){
						if(!empty($match) && isset($this->saisieTab[strtoupper($match)]) && !empty($this->saisieTab[strtoupper($match)])){
							$link = str_replace('['.$match.']',$this->saisieTab[strtoupper($match)],$link);
						}
					}
				}
			}
            if (isset($attr['LINK_JS']) && !empty($attr['LINK_JS'])) {
                echo " onclick=\"".$attr['LINK_JS']."('$link')\"";
            }else{
                echo " onclick=\"window.location.href='$link'\"";
            }
        }
		print " border=\"0\">";

		if ($addlink)
			echo '</a>';

		if (isset($attr['CHFIELDS']))
			print '<input type="hidden" name="' . $attr['CHFIELDS'] . '" id="' . $attr['CHFIELDS'] . '" value="' . $id_doc_acc . '" />';

		if (!isset($attr['NOADD'])) {
			print '<button id="bNew" class="ui-state-default ui-corner-all"
					onclick="javascript:popupUpload(\'indexPopupUpload.php?urlaction=simpleUploadJS&type=doc_acc&'.$id.'\',\'main\')"
					style="font-size: 10px;" type="button">
						<span class="ui-icon ui-icon-plus">$nbsp;</span>'
						. kLoadVignette .
					'</button>';

			print '<button id="bNew" class="ui-state-default ui-corner-all"
					onclick="resetVignette()" style="font-size: 10px;" type="button">
						<span class="ui-icon ui-icon-plus">$nbsp;</span>'
						. kReset .
					'</button>';
		}
	}

	function addTree($input_type,$nom,$attr){
		global $db;
		require_once(libDir."class_tafelTree.php");

		$title=GetRefValue('t_type_lex',$attr['ID_TYPE_LEX'],$_SESSION['id_lang']);

		echo "<script type=\"text/javascript\" src='<?=libUrl?>webComponents/tafelTree/Tree.js'></script>";
		echo "<script type=\"text/javascript\" src='<?=libUrl?>webComponents/prototype/dragdrop.js'></script>";
		echo '<div class="'.$nom.'_tree_inputs_wrapper">';

		switch($attr['TREE_TYPE']){
			// pour l'instant seul les lexiques font sens sous cette présentation.
			// on pourrait ajouter les fonds etc ..
			case  'L' :
				if($this->entity=="DOC") {
					$linkTable="t_doc_lex";
					$valTable=$this->saisieObj->t_doc_lex;
				}

				require_once(libDir."class_chercheLex.php");
				$treeSearch=new RechercheLex();
				$treeSearch->useSession=false; //pas de mise en session
				// $treeSearch->prepareSQL();
				$treeSearch->sql="select distinct t_lexique.ID_LEX as ID_VAL, LEX_ID_SYN, LEX_ID_GEN, LEX_ID_TYPE_LEX, LEX_ID_ETAT_LEX, LEX_TERME as VALEUR from t_lexique";
				$treeSearch->setPrefix(null);
				$treeSearch->sqlSuffixe="";

				$treeSearch->tab_recherche[]=array('FIELD'=>'ID_LANG',
					'VALEUR'=>$_SESSION['id_lang'],
					'TYPE'=>'C',
					'OP'=>'AND');

				$treeSearch->tab_recherche[]=
				array('FIELD'=>'t_lexique.LEX_ID_TYPE_LEX',
					'VALEUR'=>$attr['ID_TYPE_LEX'],
					'TYPE'=>'C',
					'OP'=>'AND');

				if (isset($attr['ID_TYPE_DESC']) && !empty($attr['ID_TYPE_DESC'])) $treeSearch->tab_recherche[]=
                          array('FIELD'=>'ID_TYPE_DESC',
                            'VALEUR'=>$attr['ID_TYPE_DESC'],
                            'TYPE'=>'C',
                            'OP'=>'AND');


                $treeSearch->treeParams=array(
                    "ID_TYPE_LEX"=>$attr['ID_TYPE_LEX'],
                    "ID_TYPE_DESC"=>$attr['ID_TYPE_DESC'],

					"tri"=>"LEX_TERME ASC",
					);
				if(isset($attr['ID_ROLE']) && !empty($attr['ID_ROLE'])){
					$treeSearch->treeParams['ID_ROLE'] = $attr['ID_ROLE'];
				}
				if(isset($attr['MODE']) && !empty($attr['MODE'])){
					$treeSearch->treeParams['MODE'] = $attr['MODE'];
				}
				if(isset($attr['DSPASSO']) && !empty($attr['DSPASSO'])){
					$treeSearch->treeParams['dspAsso'] = $attr['DSPASSO'];
				}
				if(isset($attr['CHECK_GEN']) && !empty($attr['CHECK_GEN'])){
					$treeSearch->treeParams['checkGenBox'] = $attr['CHECK_GEN'];
				}
				$treeSearch->makeSQL();
                $sql=$treeSearch->sql;
                // $btnEdit=$myFrame->getName()."?urlaction=lexSaisie&type_lex=".$type_lex."&currentId=".$currentId."&id_lex=";
				$btnEdit='';



				// ======== génération inputs =========
				echo '<div class="tree_inputs tree_inputs_empty">';
					echo '<input class="tree_id_item" type="hidden" name="'.$linkTable."[][ID_LEX]".'" value="0"/>';
					echo '<input type="hidden" name="'.$linkTable."[][ID_TYPE_DESC]".'" value="'.$nom.'"/>';
				echo '</div>';
				$current_values = array();
				foreach ($valTable as $idx=>$lex){
					if ($lex['ID_TYPE_DESC']==$nom && (!isset($attr['ROLE_VALUE']) || $lex['DLEX_ID_ROLE']==$attr['ROLE_VALUE'] || empty($attr['ROLE_VALUE'])) ) {
						$current_values[] = $lex['ID_LEX'];
						echo '<div class="tree_inputs id_'.$lex['ID_LEX'].'">';
							echo '<input class="tree_id_item" type="hidden" name="'.$linkTable."[][ID_LEX]".'"  value="'.$lex['ID_LEX'].'"/>';
							echo '<input type="hidden" name="'.$linkTable."[][ID_TYPE_DESC]".'" value="'.$nom.'"/>';
						echo '</div>';
					}
				}

				break ;
		}

		echo "</div>";


		$myAjaxTree=new tafelTree($treeSearch,'form1',$current_values);
		$myAjaxTree->fullTree = true ;
		$myAjaxTree->checkboxTree = true ;
		if(isset($attr['TREE_OPEN_AT_LOAD'])){
			$myAjaxTree->openAtLoad = intval($attr['TREE_OPEN_AT_LOAD']) ;
		}
		$myAjaxTree->id_div_tree = 'myTree_'.$nom ;
		$myAjaxTree->type_field = $nom ;
		$myAjaxTree->noAjax = true ;
		$myAjaxTree->useCookies=false;
		$myAjaxTree->styleRootName='font-weight:bold;';
		$myAjaxTree->imgBase='empty.gif';
		if($treeSearch->imgBase) $myAjaxTree->imgBase=$treeSearch->imgBase;
		$myAjaxTree->includeRoot=0;

		//update VG 19/05/2011 : ajout constante
		if(defined('gTafelTree_ImgBaseUrl')) $myAjaxTree->imgBaseUrl = gTafelTree_ImgBaseUrl;
		// VP 27/01/10 : ajout paramètre imgBaseUrl
		if ($_REQUEST['imgBaseUrl']) $myAjaxTree->imgBaseUrl=$_REQUEST['imgBaseUrl'];
		$myAjaxTree->makeRoot();
		debug($myAjaxTree, "red", true);

		if ($_POST['id_asso']) $myAjaxTree->revealNode(intval($_POST['id_asso']),'rebond');
		$myAjaxTree->renderOutput('',true,$btnEdit);
	}


/**
* Affiche un champ multivalué
 * IN : type de champ multi, nom, tableau de paramètres
 * OUT : affichage du champ et mise à jour tableau params
 **/
	function addMulti($type, $nom, &$attr){
		global $db;
	//by LD 30/10/08 : Trash => ajout param TRASH_IMG, ajout INDEX_TYPE (button, submit, image)
	//case LI + btnIndex (en bas de la fonction) : prise en compte du role s'il existe.
	//ceci permet de distinguer des blocs avec même TYPE_DESC mais rôle différent
	//ajout d'un saut de ligne si attr BR
	// VP 24/06/09 : traitement cas READONLY
	// VP 16/11/09 : ajout paramètre VALEUR_TYPE pour saisie en ligne
	switch($type){

			case "LI":
				if($this->entity=="DOC") {
					$linkTable="t_doc_lex";
					$valTable=$this->saisieObj->t_doc_lex;
				//VP 01/06/11
				} else if($this->entity=="PERS"){
					$linkTable="t_pers_lex";
					$valTable=$this->saisieObj->t_pers_lex;
				} else if($this->entity=="FEST"){
					$linkTable="t_fest_lex";
					$valTable=$this->saisieObj->t_fest_lex;
				}
				print "<div id='".$linkTable.$nom.(isset($attr['ROLE_VALUE'])?$attr['ROLE_VALUE']:'')."\$tab' ".((isset($attr['MANDATORY']) && $attr['MANDATORY'])?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				// MS 22/01/13 On ajoute des valeurs par défaut aux tabs multi, permet d'envoyer l'information du tableau vide si suppression de toute les valeurs
				// pas d'ID sinon ces valeurs vides par défaut sont prises en compte si une limite de nombre de val/lex est définie
				print '<input type="HIDDEN" name="t_doc_lex[][ID_LEX]" value="0" >';
				print '<input type="HIDDEN" name="t_doc_lex[][ID_TYPE_DESC]" value="'.$nom.'" >';
				if (isset($attr['ALLOW_DOUBLE']) && $attr['ALLOW_DOUBLE']) echo "<input type='hidden' name='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$allowdouble' id='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$allowdouble' value='1'/>";
				if (isset($attr['LIMIT']) && $attr['LIMIT']) echo "<input type='hidden' name='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$limit' id='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$limit' value='".$attr['LIMIT']."'/>";

				if(isset($attr['VALEUR_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['VALEUR_TYPE'],"NAME"=>$linkTable."[][LEX_TERME]","ATTR"=>"size='".$attr['SIZE']."' autocomplete='off'");
				else $arrFields[]=array("ID"=>"","TYPE"=>"DIV", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_LEX]");
                if(isset($attr['ROLE_TYPE_DESC'])) $role_type_desc=$attr['ROLE_TYPE_DESC'];
                else $role_type_desc=$nom;
				if(isset($attr['ROLE_TYPE']) && strtoupper($attr['ROLE_TYPE'])=="SELECT") $arrFields[]=array("ID"=>"","TYPE"=>"SELECT","NAME"=>$linkTable."[][DLEX_ID_ROLE]","LIB"=>(isset($attr['ROLE_LIB'])?
(defined($attr['ROLE_LIB'])?constant($attr['ROLE_LIB']):$attr['ROLE_LIB']):""),"VALUE"=>(isset($attr['ROLE_VALUE'])?$attr['ROLE_VALUE']:""),"ATTR"=>" class='".(isset($attr['ROLE_CLASS'])?$attr['ROLE_CLASS']:"")."' ","OPTIONS"=>$this->saisieObj->getRoles($role_type_desc));

				else $arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][DLEX_ID_ROLE]","VALUE"=>(isset($attr['ROLE_VALUE'])?$attr['ROLE_VALUE']:""));

				if(isset($attr['REVERSEMENT_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['REVERSEMENT_TYPE'],"NAME"=>$linkTable."[][DLEX_REVERSEMENT]","ATTR"=>"size=".$attr['REVERSEMENT_SIZE'],"LIB"=>(defined($attr['REVERSEMENT_LABEL'])?constant($attr['REVERSEMENT_LABEL']):$attr['REVERSEMENT_LABEL']));

				else $arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][DLEX_REVERSEMENT]");
				if(isset($attr['PRECISION_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['PRECISION_TYPE'],"NAME"=>$linkTable."[][LEX_PRECISION]","ATTR"=>"size=".$attr['PRECISION_SIZE'],"LIB"=>(defined($attr['PRECISION_LABEL'])?constant($attr['PRECISION_LABEL']):$attr['PRECISION_LABEL']));
				// VP (17/10/08) : ajout attribut PRECISION_INDEX
				if(isset($attr['PRECISION_INDEX'])) $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl."bouton-index.gif' onClick=\\\"indexClicked=this;choose(this,'champ=PRECISION&titreIndex=".kPrecision."&rtn=fillPrecision','',400,470)\\\"  ","LIB"=>'');
				// VP (17/10/08) : ajout attribut JS_FONCTION pour fonction Javascript spécifique
				if(isset($attr['JS_FONCTION'])) $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl.$attr['JS_IMAGE']."' onClick='".$attr['JS_FONCTION']."'","LIB"=>'');
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_LEX]","VALUE"=>$attr['ID_TYPE_LEX']);
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_DESC]","VALUE"=>$nom);
				if(isset($attr['COMMANDE']) && !empty($attr['COMMANDE'])){$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][COMMANDE]","VALUE"=>$attr['COMMANDE']);}
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","IMG"=>(isset($attr['TRASH_IMG'])?$attr['TRASH_IMG']:""),"ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				if (isset($attr['BR']) && $attr['BR']) $arrFields[]=array("ID"=>"","TYPE"=>"BR");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom.(isset($attr['ROLE_VALUE'])?$attr['ROLE_VALUE']:""),$arrFields,'miniTab');
				if(count($valTable)>0){
					foreach ($valTable as $idx=>$lex){
						//By LD 29/10/08 : si ROLE spécifié, filtrage aussi par rôle
						if ($lex['ID_TYPE_DESC']==$nom && (!isset($attr['ROLE_VALUE']) || $lex['DLEX_ID_ROLE']==$attr['ROLE_VALUE'] || empty($attr['ROLE_VALUE'])) ) {
							if (!isset($lex['LEX_PRECISION']) || empty($lex['LEX_PRECISION']))
								$lex['LEX_PRECISION']='';

							$js.="arrRow=new Array('".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$DIV\$".$lex['ID_LEX']."',".quoteField($lex['LEX_TERME']).",'".$lex['ID_LEX']."','".$lex['DLEX_ID_ROLE']."',".quoteField($lex["DLEX_REVERSEMENT"]).",".quoteField($lex['LEX_PRECISION']).");";
							$js.="out=add_".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."(arrRow);\n document.write(out[0]);";
						}
					}
				}
					$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="LFC";
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
				if(!isset($attr['INDEX_TYPE'])) $attr['INDEX_TYPE']="button";
				if(!isset($attr['INDEX_TYPE_LEX'])) $attr['INDEX_TYPE_LEX']=$attr['ID_TYPE_LEX'];

				if(isset($attr['VALEUR_TYPE'])) {
					$tabIndexParam['onclick']="addValue('".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$tab','',0,'".$attr['ROLE_VALUE']."','".$attr['AUTOCOMPLETE']."')";
					$this->addInput("BUTTON", "bIndex_".$nom, defined($attr['INDEX'])?constant($attr['INDEX']):$attr['INDEX'], $tabIndexParam);
					unset($attr['INDEX']);
				}
				break;

				
			//MS - 11.02.16 ajout cas catégorie
			case "CAT":
				if($this->entity=="DOC") {
					$linkTable="t_doc_cat";
					$valTable=$this->saisieObj->t_doc_cat;
				//VP 01/06/11
				} 
				print "<div id='".$linkTable.$nom."\$tab' ".((isset($attr['MANDATORY']) && $attr['MANDATORY'])?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				// MS 22/01/13 On ajoute des valeurs par défaut aux tabs multi, permet d'envoyer l'information du tableau vide si suppression de toute les valeurs
				// pas d'ID sinon ces valeurs vides par défaut sont prises en compte si une limite de nombre de val/lex est définie
				print '<input type="HIDDEN" name="t_doc_cat[][ID_CAT]" value="0" >';
				print '<input type="HIDDEN" name="t_doc_cat[][ID_TYPE_CAT]" value="'.$nom.'" >'; 
				if (isset($attr['LIMIT']) && $attr['LIMIT']) echo "<input type='hidden' name='".$linkTable.$nom."\$limit' id='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$limit' value='".$attr['LIMIT']."'/>";

				if(isset($attr['VALEUR_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['VALEUR_TYPE'],"NAME"=>$linkTable."[][CAT_NOM]","ATTR"=>"size='".$attr['SIZE']."' autocomplete='off'");
				else $arrFields[]=array("ID"=>"","TYPE"=>"DIV", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_CAT]");
                                
				// VP (17/10/08) : ajout attribut JS_FONCTION pour fonction Javascript spécifique
				if(isset($attr['JS_FONCTION'])) $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl.$attr['JS_IMAGE']."' onClick='".$attr['JS_FONCTION']."'","LIB"=>'');
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_CAT]","VALUE"=>$nom);
				if(isset($attr['COMMANDE']) && !empty($attr['COMMANDE'])){$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][COMMANDE]","VALUE"=>$attr['COMMANDE']);}
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","IMG"=>(isset($attr['TRASH_IMG'])?$attr['TRASH_IMG']:""),"ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				if (isset($attr['BR']) && $attr['BR']) $arrFields[]=array("ID"=>"","TYPE"=>"BR");
				$js="<script>divs='';\n";
				
				echo makeFormJSFunction2($linkTable.$nom,$arrFields,'miniTab');
				foreach ($valTable as $idx=>$cat){
					//By LD 29/10/08 : si ROLE spécifié, filtrage aussi par rôle
					if ($cat['CAT_ID_TYPE_CAT']==$nom) {
						if(isset($attr['SHOW_FULLPATH']) && $attr['SHOW_FULLPATH']=='1'){
							if(isset($attr['FULLPATH_SEP'])){
								$sep = $attr['FULLPATH_SEP'];
							}else{
								$sep = "/";
							}
							$catValue = trim($cat['CAT_PATH'].$sep.$cat['CAT_NOM'], $sep);
							
						}else{
							$catValue = $cat['CAT_NOM'];
						}
						$js.="arrRow=new Array('".$linkTable.$nom."\$DIV\$".$cat['ID_CAT']."',".quoteField($catValue).",'".$cat['ID_CAT']."');";
						$js.="out=add_".$linkTable.$nom."(arrRow);\n document.write(out[0]);";
					}
				}
					$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="CAT";
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
				if(!isset($attr['INDEX_TYPE'])) $attr['INDEX_TYPE']="button";
				if(!isset($attr['INDEX_TYPE_CAT'])) $attr['INDEX_TYPE_CAT']=$attr['ID_TYPE_CAT'];
				if(!isset($attr['INDEX_TYPE_CAT'])) $attr['INDEX_TYPE_CAT']=$nom;

				if(isset($attr['VALEUR_TYPE'])) {
					$tabIndexParam['onclick']="addValue('".$linkTable.$nom."\$tab','',0,'','".$attr['AUTOCOMPLETE']."')";
					$this->addInput("BUTTON", "bIndex_".$nom, defined($attr['INDEX'])?constant($attr['INDEX']):$attr['INDEX'], $tabIndexParam);
					unset($attr['INDEX']);
				}
				break;
				
			case "UGR":

				$linkTable = "t_usager_groupe";
				$valTable = $this->saisieObj->t_usager_groupe;

				print "<div id='".$linkTable.$nom."\$tab' ".((isset($attr['MANDATORY']) && $attr['MANDATORY'])?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				// MS 22/01/13 On ajoute des valeurs par défaut aux tabs multi, permet d'envoyer l'information du tableau vide si suppression de toute les valeurs
				// pas d'ID sinon ces valeurs vides par défaut sont prises en compte si une limite de nombre de val/lex est définie
				print '<input type="HIDDEN" name="t_usager_groupe[][ID_GROUPE]" value="0" >';
				if (isset($attr['ALLOW_DOUBLE']) && $attr['ALLOW_DOUBLE']) echo "<input type='hidden' name='".$linkTable."\$allowdouble' id='".$linkTable."\$allowdouble' value='1'/>";
				if (isset($attr['LIMIT']) && $attr['LIMIT']) echo "<input type='hidden' name='".$linkTable."\$limit' id='".$linkTable."\$limit' value='".$attr['LIMIT']."'/>";

				if(isset($attr['VALEUR_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['VALEUR_TYPE'],"NAME"=>$linkTable."[][LEX_TERME]","ATTR"=>"size='".$attr['SIZE']."' autocomplete='off'");
				else $arrFields[]=array("ID"=>"","TYPE"=>"DIV", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_GROUPE]");
				if(isset($attr['JS_FONCTION'])) $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl.$attr['JS_IMAGE']."' onClick='".$attr['JS_FONCTION']."'","LIB"=>'');
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","IMG"=>isset($attr['TRASH_IMG'])?$attr['TRASH_IMG']:'',"ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom,$arrFields,'miniTab');
				if ($valTable)
					foreach ($valTable as $idx=>$gr){
						//By LD 29/10/08 : si ROLE spécifié, filtrage aussi par rôle
						$js.="arrRow=new Array('".$linkTable.$nom."\$DIV\$".$gr['ID_GROUPE']."',".quoteField($gr['GROUPE']->t_groupe['GROUPE']).",'".$gr['ID_GROUPE']."');";
						$js.="out=add_".$linkTable.$nom."(arrRow);\n document.write(out[0]);";
					}
				$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="GRP";
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteSimple";
				if(!isset($attr['INDEX_TYPE'])) $attr['INDEX_TYPE']="button";

				if(isset($attr['VALEUR_TYPE'])) {
					$tabIndexParam['onclick']="addValue('".$linkTable."\$tab','',0,'"."','".$attr['AUTOCOMPLETE']."')";
					$this->addInput("BUTTON", "bIndex_".$nom, defined($attr['INDEX'])?constant($attr['INDEX']):$attr['INDEX'], $tabIndexParam);
					unset($attr['INDEX']);
				}
				break;

			case "USR":
				switch($this->entity) {
					case "DOC":
						$id=$this->saisieObj->t_doc[$nom];
					break;
					case "MAT":
						$id=$this->saisieObj->t_mat[$nom];
						break;
					case "PANIER":
						$id=$this->saisieObj->t_panier[$nom];
						break;
				}
				$sqlUsager="select DISTINCT trim(concat(US_PRENOM, ' ', US_NOM)) from t_usager WHERE ID_USAGER=".intval($id);
				$val=$db->GetOne($sqlUsager);
			
				// VP 6/06/2018 : ajout attribut hidden_value, utile pour saisieLot
				if(isset($attr['HIDDEN_VALUE'])){
					if($attr['HIDDEN_VALUE']!="none"){
						$this->addInput("hidden", $nom,  $attr['HIDDEN_VALUE']);
					}
				}else{
					$this->addInput("hidden", $nom, "0");
				}
				//print "<input type='hidden' id='".$nom."' name='".$nom."' value='0' />";
				print "<div id='".$nom."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				print "<input type='hidden' name='".$nom."\$limit' id='".$nom."\$limit' value='1'/>";
				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$nom);
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($nom,$arrFields,'miniTab');
				if(!empty($val)){
					$js.="arrRow=new Array('".$nom."\$DIV\$".$id."',".quoteField($val).",'".$id."');";
					$js.="out=add_".$nom."(arrRow);\n document.write(out[0]);";
					
				}
				$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="USR";
				break;

			case "F":

				if($this->entity=="DOC") {
					$linkTable="t_doc_fest";
					$valTable=$this->saisieObj->t_doc_fest;
				}
				//debug($this->saisieObj->t_doc_fest,'gold');
				print "<div id='".$linkTable.$nom."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				$arrFields[]=array("ID"=>"","TYPE"=>"DIV", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_FEST]");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_DOC_FEST]");
				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN");
				if ($attr['VALEUR']) $arrFields[]=array("ID"=>"","TYPE"=>"SPAN","CLASSE"=>$attr['VALEUR_CLASSE'],"ATTR"=>$attr['VALEUR_ATTR']);

				if(isset($attr['JS_FONCTION'])) $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl.$attr['JS_IMAGE']."' onClick='".$attr['JS_FONCTION']."'","LIB"=>'');

				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","IMG"=>(isset($attr['TRASH_IMG'])?$attr['TRASH_IMG']:""),"ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				
				if ($attr['BR']) $arrFields[]=array("ID"=>"","TYPE"=>"BR");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom,$arrFields,'miniTab');

				foreach ($valTable as $idx=>$lex){
						$strval='';
						if (!empty($lex['DOC_FEST_VAL'])) {

							foreach ($lex['DOC_FEST_VAL'] as $v) if ($v['ID_TYPE_VAL']=$attr['VALEUR']) $strval.=$v['VALEUR'];
						}
						$js.="arrRow=new Array('".$linkTable.$nom."\$DIV\$".$lex['ID_DOC_FEST']."',".quoteField($lex['FEST_LIBELLE']).",'".$lex['ID_FEST']."','".$lex['ID_DOC_FEST']."',".quoteField("&nbsp;".substr($lex["FEST_ANNEE"],0,4)).",".quoteField("&nbsp;".$strval).");";
						$js.="out=add_".$linkTable.$nom."(arrRow);\n document.write(out[0]);";
				}
				$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				//if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="FEST";
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
				if(!isset($attr['INDEX_TYPE'])) $attr['INDEX_TYPE']="button";
				break;

			case "P":
				// VP 8/07/09 : prise en compte attribut ROLE_VALUE dans cas P
				if($this->entity=="DOC") {
					$linkTable="t_doc_lex";
					$valTable=$this->saisieObj->t_pers_lex;
				}
                elseif($this->entity=="MAT") {
                    $linkTable="t_mat_pers";
                    $valTable=$this->saisieObj->t_mat_pers;
                } else if($this->entity=="FEST"){
					$linkTable="t_fest_lex";
					$valTable=$this->saisieObj->t_fest_pers;
				}

				if(isset($attr['ROLE_OPTION_VALUE'])){
					$tabOptions=explode(',',$attr['ROLE_OPTION_VALUE']);
					$tabLibs=explode(',',$attr['ROLE_OPTION_LIB']);
					$tabValue=array_combine($tabOptions, $tabLibs);
					$attr['ROLE_VALUE_OPTION']=$tabValue;
					// trace(print_r($tabValue,true));
				}
				print "<div id='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				if ($attr['ALLOW_DOUBLE']) echo "<input type='hidden' name='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$allowdouble' id='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$allowdouble' value='1'/>";
				if ($attr['LIMIT']) echo "<input type='hidden' name='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$limit' id='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$limit' value='".$attr['LIMIT']."'/>";
				if(isset($attr['VALEUR_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['VALEUR_TYPE'],"NAME"=>$linkTable."[][PERS_NOM]","ATTR"=>"size='".$attr['SIZE']."' autocomplete='off'");
				else $arrFields[]=array("ID"=>"","TYPE"=>"DIV", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_PERS]");
                if(isset($attr['ROLE_TYPE_DESC'])) $role_type_desc=$attr['ROLE_TYPE_DESC'];
                else $role_type_desc=$nom;
				if(strtoupper($attr['ROLE_TYPE'])=="SELECT") $arrFields[]=array("ID"=>"","TYPE"=>"SELECT","NAME"=>$linkTable."[][DLEX_ID_ROLE]","LIB"=>$attr['ROLE_LIB'],"VALUE"=>(isset($attr['ROLE_VALUE'])?$attr['ROLE_VALUE']:""),"ATTR"=>" class='".$attr['ROLE_CLASS']."' ","OPTIONS"=>isset($attr['ROLE_VALUE_OPTION'])?$attr['ROLE_VALUE_OPTION']:$this->saisieObj->getRoles($role_type_desc));
				else $arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][DLEX_ID_ROLE]","VALUE"=>$attr['ROLE_VALUE']);
				if(isset($attr['REVERSEMENT_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['REVERSEMENT_TYPE'],"NAME"=>$linkTable."[][DLEX_REVERSEMENT]","ATTR"=>"size=".$attr['REVERSEMENT_SIZE'],"LIB"=>(defined($attr['REVERSEMENT_LABEL'])?constant($attr['REVERSEMENT_LABEL']):$attr['REVERSEMENT_LABEL']));
				else $arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][DLEX_REVERSEMENT]");
				if(isset($attr['PRECISION_TYPE']) && isset($attr['PRECISION_VALUE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['PRECISION_TYPE'],"NAME"=>$linkTable."[][LEX_PRECISION]","ATTR"=>"size=".$attr['PRECISION_SIZE'],"LIB"=>(defined($attr['PRECISION_LABEL'])?constant($attr['PRECISION_LABEL']):$attr['PRECISION_LABEL']),"VALUE"=>$attr['PRECISION_VALUE']);
				elseif(isset($attr['PRECISION_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>$attr['PRECISION_TYPE'],"NAME"=>$linkTable."[][LEX_PRECISION]","ATTR"=>"size=".$attr['PRECISION_SIZE'],"LIB"=>(defined($attr['PRECISION_LABEL'])?constant($attr['PRECISION_LABEL']):$attr['PRECISION_LABEL']));
				// VP (17/10/08) : ajout attribut PRECISION_INDEX
				if(isset($attr['PRECISION_INDEX']) && $attr['PRECISION_INDEX'] == "LIMIT_DESC") $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl."bouton-index.gif' onClick=\\\"indexClicked=this;choose(this,'champ=PRECISION&autreChamps=".$nom."&titreIndex=".kPrecision."&rtn=fillPrecision','',400,470)\\\"  ","LIB"=>'');
				elseif(isset($attr['PRECISION_INDEX'])) $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl."bouton-index.gif' onClick=\\\"indexClicked=this;choose(this,'champ=PRECISION&titreIndex=".kPrecision."&rtn=fillPrecision','',400,470)\\\"  ","LIB"=>'');
				// VP (17/10/08) : ajout attribut JS_FONCTION pour fonction Javascript spécifique
				if(isset($attr['JS_FONCTION'])) $arrFields[]=array("ID"=>"","TYPE"=>"IMG","NAME=''","ATTR"=>"src='".imgUrl.$attr['JS_IMAGE']."' onClick='".$attr['JS_FONCTION']."'","LIB"=>'');
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_LEX]","VALUE"=>$attr['ID_TYPE_LEX']);
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_DESC]","VALUE"=>$nom);
				if(isset($attr['COMMANDE']) && !empty($attr['COMMANDE'])){$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][COMMANDE]","VALUE"=>$attr['COMMANDE']);}
				//XB trash img
				// if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
			if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","IMG"=>(isset($attr['TRASH_IMG'])?$attr['TRASH_IMG']:""),"ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				if ($attr['BR']) $arrFields[]=array("ID"=>"","TYPE"=>"BR");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE'],$arrFields,'miniTab');
				// B.RAVI 2016-05-06 Eviter Warning: Invalid argument supplied for foreach()
				if (is_object($valTable) || is_array($valTable)){
					foreach ($valTable as $idx=>$lex){
						//PC - 29/09/10 : Ajout d'un parametre pour différenciation par role
						if ($lex['ID_TYPE_DESC']==$nom && ($lex['DLEX_ID_ROLE']==$attr['ROLE_VALUE'] || empty($attr['ROLE_VALUE'])) && ($lex['DLEX_ID_ROLE']!=$attr['ROLE_VALUE_NOT'] || empty($attr['ROLE_VALUE_NOT'])) && ($lex['LEX_PRECISION']==$attr['PRECISION_VALUE'] || empty($attr['PRECISION_VALUE'])) ) {
							if (defined("gFieldsForPersTerme")) {
								$pers_terme = "";
								$fields = unserialize(gFieldsForPersTerme);
								foreach ($fields as $f)
									$pers_terme .= $lex[strtoupper($f)] . " ";
								$pers_terme = trim($pers_terme);
							} else
								$pers_terme = $lex['PERS_NOM']." ".$lex['PERS_PRENOM'];
							$js.="arrRow=new Array('".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$DIV\$".$lex['ID_PERS']."',".quoteField($pers_terme).",'".$lex['ID_PERS']."','".$lex['DLEX_ID_ROLE']."',".quoteField($lex["DLEX_REVERSEMENT"]).",".quoteField($lex['LEX_PRECISION']).");";
							$js.="out=add_".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."(arrRow);\n document.write(out[0]);";
						}
					}
				}
				$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				// VP 24/07/09 : changement INDEX_CHAMP en PFC
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="PFC";
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
				if(!isset($attr['INDEX_TYPE_LEX'])) $attr['INDEX_TYPE_LEX']=$attr['ID_TYPE_LEX'];

				if(isset($attr['VALEUR_TYPE'])) {
					$tabIndexParam['onclick']="addValue('".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$tab','',0,'".$attr['ROLE_VALUE']."','".$attr['AUTOCOMPLETE']."')";
					$this->addInput("BUTTON", "bIndex_".$nom, defined($attr['INDEX'])?constant($attr['INDEX']):$attr['INDEX'], $tabIndexParam);
					unset($attr['INDEX']);
				}
				break;

			//PC 20/09/12 : Ajout cas PL (multiple personnes avec texte libre)
			case "PL":
				if($this->entity=="DOC") {
					$linkTable="t_doc_lex";
					$valTable=$this->saisieObj->t_pers_lex;
				}
				print "<div id='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				// VP 15/03/10 : ajout attribut LIMIT
				if ($attr['LIMIT']) echo "<input type='hidden' name='".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$limit' id='".$linkTable.$nom."\$limit' value='".$attr['LIMIT']."'/>";
				$arrFields[]=array("ID"=>"","TYPE"=>"TEXT","NAME"=>$linkTable."[][PERS_NOM]");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_PERS]");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_DESC]","VALUE"=>$nom);
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][PERS_ID_TYPE_LEX]","VALUE"=>(isset($attr['ID_TYPE_LEX'])?$attr['ID_TYPE_LEX']:$nom));
				if(isset($attr['ROLE_VALUE'])) $arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][DLEX_ID_ROLE]","VALUE"=>$attr['ROLE_VALUE']);
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE'],$arrFields,'miniTab');
				$nbNoms = 0;
				foreach ($valTable as $idx=>$val){
					//by ld 22/12/08 : ID_TYPE_VAL (lien docVal) au lieu de VAL_ID_TYPE_VAL (t_val)
					if ($val['ID_TYPE_DESC']==$nom && (empty($attr['ROLE_VALUE']) || $val['DLEX_ID_ROLE']==$attr['ROLE_VALUE'])) {
						$nbNoms++;
						$js.="arrRow=new Array('".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$DIV\$".$val['ID_PERS'].(!empty($nbNoms)?"_".$nbNoms:"")."',".quoteField($val["PERS_NOM"]).",'".$val['ID_PERS']."');";
						$js.="out=add_".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."(arrRow);\n document.write(out[0]);";

					}
				}
					$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="PFC_".$nom;
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";

				$tabIndexParam['onclick'] = "addValue('".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$index','','0')";
				$tabIndexParam['class'] = $attr['INDEX_CLASS'];
				$this->addInput("BUTTON", "bIndex_".$nom, constant("kAjouter"), $tabIndexParam);
				unset($attr['INDEX']);

				break;

			case "VGEN":
				if($this->entity=="DOC") {
					$linkTable="t_doc_val";
					$valTable=$this->saisieObj->t_doc_val;
				}else if($this->entity=="MAT"){
					$linkTable="t_mat_val";
					$valTable=$this->saisieObj->t_mat_val;
				}else if($this->entity=="FEST"){
					$linkTable="t_fest_val";
					$valTable=$this->saisieObj->t_fest_val;
				}
				//PC 14/10/10 : ajout du cas pour les droits preexistants
				else if($this->entity=="PREX"){
					$linkTable="t_dp_val";
					$valTable=$this->saisieObj->t_dp_val;
				}
				//VP 01/06/11
				else if($this->entity=="PERS"){
					$linkTable="t_pers_val";
					$valTable=$this->saisieObj->t_pers_val;
				}
				else if($this->entity == "VAL") {
					$valTable=$this->saisieObj->t_val;
				}
				//by LD 29/10/08 : récupération de la valeur déjà présente si val non spécifié
				if(isset($attr['TYPE_VAL'])) $current_type_val=$attr['TYPE_VAL'];
				else $current_type_val = $valTable['VAL_ID_TYPE_VAL'];
				$id_current_val = $valTable['ID_VAL'];
				$nom = $valTable['VAL_ID_TYPE_VAL'];


				print "<div id='".$linkTable.$nom."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				// VP 15/03/10 : ajout attribut LIMIT
				// MS 12/02/2014 On ajoute des valeurs par défaut aux tabs multi, permet d'envoyer l'information du tableau vide si suppression de toute les valeurs
				// pas d'id sinon ces valeurs vides par défaut sont prises en compte si une limite de nombre de val/lex est définie


				if ($attr['LIMIT']) echo "<input type='hidden' name='".$linkTable.$nom."\$limit' id='".$linkTable.$nom."\$limit' value='".$attr['LIMIT']."'/>";
				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN", "NAME"=>"VAL_ID_GEN","CLASSE"=>"miniTabLigne");
				if(isset($attr['COMMANDE']) && !empty($attr['COMMANDE'])){$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][COMMANDE]","VALUE"=>$attr['COMMANDE']);}
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","IMG"=>(isset($attr['TRASH_IMG'])?$attr['TRASH_IMG']:""),"ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom,$arrFields,'miniTab');
				if(!empty($valTable['VAL_ID_GEN'])) {
					global $db;
					$sql = "select VALEUR from t_val where ID_VAL = '".$valTable['VAL_ID_GEN']."';";
					$valeur = $db->GetOne($sql);
					$js.="arrRow=new Array('".$nom."\$DIV\$".$valTable['VAL_ID_GEN']."',".quoteField($valeur).",'".$valTable['VAL_ID_GEN']."');";
					$js.="out=add_".$linkTable.$nom."(arrRow);\n document.write(out[0]);";
					$js.='</script>';
					echo $js;
				}
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="VFC_".$nom;
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
			break;
			case "VI":
				if($this->entity=="DOC") {
					$linkTable="t_doc_val";
					$valTable=$this->saisieObj->t_doc_val;
				}else if($this->entity=="MAT"){
					$linkTable="t_mat_val";
					$valTable=$this->saisieObj->t_mat_val;
				}else if($this->entity=="FEST"){
					$linkTable="t_fest_val";
					$valTable=$this->saisieObj->t_fest_val;
				}
				//PC 14/10/10 : ajout du cas pour les droits preexistants
				else if($this->entity=="PREX"){
					$linkTable="t_dp_val";
					$valTable=$this->saisieObj->t_dp_val;
				}
				//VP 01/06/11
				else if($this->entity=="PERS"){
					$linkTable="t_pers_val";
					$valTable=$this->saisieObj->t_pers_val;
				}

				print "<div id='".$linkTable.$nom."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				// VP 15/03/10 : ajout attribut LIMIT
				// MS 12/02/2014 On ajoute des valeurs par défaut aux tabs multi, permet d'envoyer l'information du tableau vide si suppression de toute les valeurs
				// pas d'id sinon ces valeurs vides par défaut sont prises en compte si une limite de nombre de val/lex est définie
				print '<input type="HIDDEN" name="t_doc_val[][ID_VAL]" value="0" >';
				print '<input type="HIDDEN" name="t_doc_val[][ID_TYPE_VAL]" value="'.$nom.'" >';

				if ($attr['LIMIT']) echo "<input type='hidden' name='".$linkTable.$nom."\$limit' id='".$linkTable.$nom."\$limit' value='".$attr['LIMIT']."'/>";
				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_VAL]");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_VAL]","VALUE"=>$nom);
				if(isset($attr['COMMANDE']) && !empty($attr['COMMANDE'])){$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][COMMANDE]","VALUE"=>$attr['COMMANDE']);}
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","IMG"=>(isset($attr['TRASH_IMG'])?$attr['TRASH_IMG']:""),"ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom,$arrFields,'miniTab');
				foreach ($valTable as $idx=>$val){
					// MS 20.11.14 :  suppression de l'autre cas de test :  // || $val['VAL_ID_TYPE_VAL']==$nom
					// => Certains sites créent plusieurs liens DOC_MAT avec des id_type_val différents, => bugs lors de l'affichage des valeurs => bugs lors d'un nouvel enregistrement de la fiche
					//by ld 22/12/08 : ID_TYPE_VAL (lien docVal) au lieu de VAL_ID_TYPE_VAL (t_val)
					if ($val['ID_TYPE_VAL']==$nom){
						$js.="arrRow=new Array('".$linkTable.$nom."\$DIV\$".$val['ID_VAL']."',".quoteField($val["VALEUR"]).",'".$val['ID_VAL']."');";
						$js.="out=add_".$linkTable.$nom."(arrRow);\n document.write(out[0]);";

					}
				}
					$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) {
					if(isset($attr['TYPE_VAL']))
						$attr['INDEX_CHAMP']="VFC_".$attr['TYPE_VAL'];
					else
						$attr['INDEX_CHAMP']="VFC_".$nom;
				}
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
				break;

			//PC 20/09/12 : Ajout cas V (multile valeur champs texte)
			// XB 02/12/15 ajout autcomplete
			case "V":
				if($this->entity=="DOC") {
					$linkTable="t_doc_val";
					$valTable=$this->saisieObj->t_doc_val;
				}else if($this->entity=="MAT"){
					$linkTable="t_mat_val";
					$valTable=$this->saisieObj->t_mat_val;
				}
				print "<div id='".$linkTable.$nom."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				// VP 15/03/10 : ajout attribut LIMIT
				if ($attr['LIMIT']) echo "<input type='hidden' name='".$linkTable.$nom."\$limit' id='".$linkTable.$nom."\$limit' value='".$attr['LIMIT']."'/>";
				if(isset($attr['VALEUR_TYPE'])) $arrFields[]=array("ID"=>"","TYPE"=>"TEXT","NAME"=>$linkTable."[][VALEUR]","ATTR"=>"size='".$attr['SIZE']."' autocomplete='off'");
				else $arrFields[]=array("ID"=>"","TYPE"=>"TEXT","NAME"=>$linkTable."[][VALEUR]");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_VAL]");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][ID_TYPE_VAL]","VALUE"=>$nom);
				if(isset($attr['COMMANDE']) && !empty($attr['COMMANDE'])){$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][COMMANDE]","VALUE"=>$attr['COMMANDE']);}

				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom,$arrFields,'miniTab');
				$nbNoms = 0;
				foreach ($valTable as $idx=>$val){
					//by ld 22/12/08 : ID_TYPE_VAL (lien docVal) au lieu de VAL_ID_TYPE_VAL (t_val)
					if ($val['ID_TYPE_VAL']==$nom) {
						$nbNoms++;
						$js.="arrRow=new Array('".$linkTable.$nom."\$DIV\$".$val['ID_VAL'].(!empty($nbNoms)?"_".$nbNoms:"")."',".quoteField($val["VALEUR"]).",'".$val['ID_VAL']."');";
						$js.="out=add_".$linkTable.$nom."(arrRow);\n document.write(out[0]);";

					}
				}
					$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="VFC_".$nom;
				if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
				if(isset($attr['VALEUR_TYPE'])) {
					$tabIndexParam['onclick'] = "addValue('".$linkTable.$nom."\$index','','0','','".$attr['AUTOCOMPLETE']."')";
				}
				else{
					$tabIndexParam['onclick'] = "addValue('".$linkTable.$nom."\$index','','0')";
				}
				$tabIndexParam['class'] = $attr['INDEX_CLASS'];
				$this->addInput("BUTTON", "bIndex_".$nom, constant("kAjouter"), $tabIndexParam);
				unset($attr['INDEX']);

				break;

			case "FI":
				$fonds_champ = (isset($attr['FONDS_CHAMP'])?$attr['FONDS_CHAMP']:"FONDS");
				if($this->entity=="DOC") {
					$id=$this->saisieObj->t_doc[$nom];
					$sqlFonds="select DISTINCT $fonds_champ from t_fonds WHERE ID_LANG='".$_SESSION["langue"]."' and ID_FONDS=".intval($id);
					$val=$db->GetOne($sqlFonds);
				}
			
				// VP 6/06/2018 : ajout attribut hidden_value, utile pour saisieLot
				if(isset($attr['HIDDEN_VALUE'])){
					if($attr['HIDDEN_VALUE']!="none"){
						$this->addInput("hidden", $nom,  $attr['HIDDEN_VALUE']);
					}
				}else{
					$this->addInput("hidden", $nom, "0");
				}
				//print "<input type='hidden' id='".$nom."' name='".$nom."' value='0' />";
				print "<div id='".$nom."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				print "<input type='hidden' name='".$nom."\$limit' id='".$nom."\$limit' value='1'/>";
				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$nom);
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($nom,$arrFields,'miniTab');
				if(!empty($val)){
					$js.="arrRow=new Array('".$nom."\$DIV\$".$id."',".quoteField($val).",'".$id."');";
					$js.="out=add_".$nom."(arrRow);\n document.write(out[0]);";
					
				}
				$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="FON";
				break;

			//VP 4/2/09 : ajout cas father
			case "father":
				if($this->entity=="DOC") {
					$linkTable="arrFather";
					$id="ID_DOC";
					$id_gen="DOC_ID_GEN";
					$valTable=$this->saisieObj->arrFather;
				} elseif($this->entity=="PERS") {
					$linkTable="arrFather";
					$id="ID_PERS";
					$id_gen="PERS_ID_GEN";
					$valTable=$this->saisieObj->arrFather;
				}elseif($this->entity=="CAT") {
					$linkTable="arrFather";
					$id="ID_CAT";
					$id_gen="CAT_ID_GEN";
					$valTable=$this->saisieObj->arrFather;
					if(!isset($attr['INDEX_FILTER']) || empty($attr['INDEX_FILTER'])){
						$attr['INDEX_FILTER'] = $this->saisieObj->t_categorie['ID_CAT'] ;
					}
				}
				debug($valTable, "blue", true);
			
				// VP 6/06/2018 : ajout attribut hidden_value, utile pour saisieLot
				if(isset($attr['HIDDEN_VALUE'])){
					if($attr['HIDDEN_VALUE']!="none"){
						$this->addInput("hidden", $id_gen,  $attr['HIDDEN_VALUE']);
					}
				}else{
					$this->addInput("hidden", $id_gen, "0");
				}
				//print "<input type='hidden' id='".$id_gen."' name='".$id_gen."' value='0' />";
				print "<input type='hidden' id='".$linkTable.$nom."\$limit' name='".$linkTable.$nom."\$limit' value='1' />";
				print "<div id='".$linkTable.$nom."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";
				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"","CLASSE"=>"miniTabLigne");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>$linkTable."[][".$id."]");
				if(!isset($attr['READONLY'])) $arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");
				$js="<script>divs='';\n";
				echo makeFormJSFunction2($linkTable.$nom,$arrFields,'miniTab');
				foreach ($valTable as $idx=>$val){
					$js.="arrRow=new Array('".$linkTable.$nom."\$DIV\$".$val[$id]."',".quoteField($val[$nom]).",'".$val[$id]."');";
					$js.="out=add_".$linkTable.$nom."(arrRow);\n document.write(out[0]);";

				}
				$js.='</script>';
				echo $js;
				unset($arrFields);
				print "</div>\n";
				// Paramètres index par défaut
				if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
				if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="DOC";
				break;

			//PC 30/7/10 : ajout cas TL (Table Link)
		case "TL":
			$linkTable = "t_" . strtolower($this->entity) . "_" . strtolower($nom);
			eval("\$objTable = \$this->saisieObj->$linkTable;" );

			$id_prim = 0;
			eval("\$id_prim = \$this->saisieObj->t_" . strtolower($this->entity) . "[ID_" . strtoupper($this->entity) . "];" );

			print "<div id='".$linkTable."\$tab' ".($attr['MANDATORY']?"MANDATORY='true'":"")." class='".$attr['CLASS']."'>\n";

			if ($attr['LIMIT']) echo "<input type='hidden' name='".$linkTable."\$limit' id='".$linkTable."\$limit' value='".$attr['LIMIT']."'/>";
			print "</div>\n";
			$jsButton = "add_$linkTable('" . $linkTable . "', 'ID_" . strtoupper($this->entity) . "', 0, 0);";
			$attrButton = array("style" => "cursor:pointer;", "onclick" => "$jsButton");
			$this->addInput("button", $linkTable . "\$add", kAjouter, $attrButton);

			if (isset($attr['INNERFIELDS'])){
				$attr['INNERFIELDS'][0]['ID'] = $linkTable."\$blank";
				$attr['INNERFIELDS'][0]['DISPLAY'] = "none";
				foreach ($attr['INNERFIELDS'][0]['ELEMENT'] as $idx => $elt) {
					$attr['INNERFIELDS'][0]['ELEMENT'][$idx]['NAME'] = strtolower($linkTable) . "[][" . $elt['CHFIELDS'] . "]";
				}
				$innerElt = array("DIV" => array(0 => $attr['INNERFIELDS'][0]));
				$this->displayTab($innerElt);
				unset($attr['innerFields']);
			}

			$js="<script>divs='';\n";
			echo makeFormJSFunctionTL($linkTable, 'lineDoc');
			foreach ($objTable as $idx => $link){
				$tabfields = '';
				$tabvalue = '';
				foreach ($link as $idy => $value)
				{
					$prefix = '';
					if ($tabfields != '')
						$prefix = ', ';
					if (!is_array($value) && !is_object($value)){
						$tabfields .= "$prefix'$idy'";
						$tabvalue .= "$prefix'$value'";
					}
				}
				$js.= "arrRow = new Array($tabfields);";
				$js.= "arrVal = new Array($tabvalue);";
				$js.= "add_$linkTable('" . $linkTable . "', 'ID_" . strtoupper($this->entity) . "', arrRow, arrVal);\n";
			}
			$js.='</script>';
			echo $js;

			break;

		case "TL2":
			$linkTable = "t_" . strtolower($this->entity) . "_" . strtolower($nom);
			// GT 16/10/13 On ajoute des valeurs par défaut aux tabs multi, permet d'envoyer l'information du tableau vide si suppression de toute les rediffusions
			// pas d'ID sinon ces valeurs vides par défaut sont prises en compte si une limite de nombre de val/lex est définie
			if ($linkTable=='t_doc_redif')
			{
				print '<input type="hidden" name="'.$linkTable.'[][REDIF_DIFFUSEUR]" value="" />';
				print '<input type="hidden" name="'.$linkTable.'[][REDIF_DATE]" value="" />';
			}
			eval("\$objTable = \$this->saisieObj->$linkTable;" );
			if (isset($attr['CHENTITY'])){
				$tmpentity = $attr['CHENTITY'];
				$linkTable2 = "t_" . strtolower($tmpentity);
			}

			$linkTableName = $linkTable;
			if (isset($attr['CHFILTER']) && isset($attr['CHFILTERVAL']))
				$linkTableName .= str_replace("!", "_", str_replace(",", "", $attr['CHFILTERVAL']));

			if (isset($attr['INNERFIELDS'])){
				$newForm=new FormSaisie;
				$newForm->entity = $tmpentity;
				if ($tmpentity == "PREX"){
					include_once(modelDir.'model_doc_prex.php');
					$newForm->saisieObj = new DroitPrex();
				}
				elseif ($tmpentity == "MAT"){
					include_once(modelDir.'model_materiel.php');
					$newForm->saisieObj = new Materiel();
				}
				elseif ($tmpentity == "DOC"){
					include_once(modelDir.'model_doc.php');
					$newForm->saisieObj = new Doc();
				}
				if (isset($attr['CHENTITY']))
					$curform = $newForm;
				else
					$curform = $this;

				//Ajout du filtre dynamique
				if (isset($attr['FILTERFIELD'])){
					foreach ($attr['INNERFIELDS'][0]['ELEMENT'] as $idx => $elt)
					if ($elt['CHFIELDS'] == $attr['FILTERFIELD'])
						$elt_filter = $elt;
					$elt_filter['TRTAGS'] = 'start';
					$elt_filter['LABEL'] = kFiltre . " :&nbsp;";
					$elt_filter['NAME'] = "filter$linkTableName";
					$elt_filter['ONCHANGE'] = "filter_$linkTableName('" . $attr['FILTERFIELD'] . "')";
					if ($elt_filter['TYPE'] == "select" && isset($elt_filter['NOALL']))
						unset($elt_filter['NOALL']);
					$curform->displayELt($elt_filter);
				}

				echo "<table cellspacing=\"0\" border=\"0\">
				<tbody id=\"$linkTableName\$tab\"><tr>";

				//Ajout champs lexique si besoin
				if (isset($attr['ID_TYPE_LEX'])){
					$tablelex = "t_" . strtolower($this->entity) . "_lex";
					$arrlex1 = array("TYPE" => "hidden", "CHFIELDS" => "ID_PERS", "TABLE" => $tablelex);
					$arrlex2 = array("TYPE" => "hidden", "CHFIELDS" => "DLEX_ID_ROLE", "TABLE" => $tablelex, "CHVALUES" => $attr['ROLE_VALUE']);
					$arrlex3 = array("TYPE" => "hidden", "CHFIELDS" => "ID_TYPE_DESC", "TABLE" => $tablelex, "CHVALUES" => $attr['DESC_VALUE']);
					array_unshift($attr['INNERFIELDS'][0]['ELEMENT'], $arrlex1, $arrlex2, $arrlex3);
				}
				
				//En-tete du tableau
				echo "<th class=\"resultsHead\"></th>";
				foreach ($attr['INNERFIELDS'][0]['ELEMENT'] as $idx => $elt) {
					if(isset($elt['LABEL'])) $label=defined($elt['LABEL'])?constant($elt['LABEL']):$elt['LABEL'];
					else if(isset($elt['CHLIBS'])) $label=$elt['CHLIBS'];

					if(isset($label) && defined($label))
						$label=constant($label);
					else if (!isset($label))
						$label='';

					$tmpclass = '';
					if (isset($elt['CLASS']))
						$tmpclass = $elt['CLASS'];
					echo "<th class=\"resultsHead $tmpclass\" >$label</th>";

					unset($label);
				}
				echo "<th class=\"resultsHead\"></th>";

				//Ligne vide pour les ajouts
				echo "</tr><tr id=\"$linkTableName\$blank\" style=\"display:none\">";
				echo "<td class=\"resultsCorps\"></td>";
				foreach ($attr['INNERFIELDS'][0]['ELEMENT'] as $idx => $elt) {
					if (!isset($elt['TABLE']))
						$elt['TABLE'] = $attr['INNERFIELDS'][0]['ELEMENT'][$idx]['TABLE'] = $linkTable;
					$elt['ID'] = $elt['TABLE'] . "[][".(isset($elt['CHFIELDS'])?$elt['CHFIELDS']:'')."]";
					$elt['NAME'] = "input$linkTableName";
					if (isset($elt['TYPE']) && $elt['TYPE'] == "checkboxSel") $elt['TYPE'] = "span";
					$tmpclass = '';
					if (isset($elt['CLASS']))
						$tmpclass = $elt['CLASS'];
					echo "<td align=\"center\" class=\"resultsCorps $tmpclass\">";
					$curform->displayELt($elt);
					echo "</td>";
				}
				if (!isset($attr['NODELETE'])){
					$trashimg = "<img id=\"del\$[nb]\" onclick=\"remove_$linkTableName(this.id, '$linkTableName')\" name=\"miniTrash\" class=\"miniTrash\" src=\"" . imgUrl . "button_drop.gif\" title=\"" . kSupprimer . "\" alt=\"" . kSupprimer . "\" />";
                                      //B.RAVI 20150218 permettre l'appel d'une fct js personnalisé pour le projet parisien (du nécessité IE7, obligation de passer par le socle et pas par jquery, car jquery.attr() ne marche pas sur id contenant un $
                                      if (isset($attr['DELETE_JS'])){
                                          $trashimg = "<img id=\"del\$[nb]\" onclick=\"".$attr['DELETE_JS']."([nb])\" name=\"miniTrash\" class=\"miniTrash\" src=\"" . imgUrl . "button_drop.gif\" title=\"" . kSupprimer . "\" alt=\"" . kSupprimer . "\" />";
                                      }

                                }
				echo "<td class=\"resultsCorps\">$trashimg</td>";
				//Lignes remplies
				if (isset($attr['NUMMULTITL']))
					$preid = $attr['NUMMULTITL'];
				else
					$preid=null;
				$num_row_id = 0;
				$lastid = $preid . $num_row_id;

				if (isset($objTable) && !empty($objTable))
				{
					foreach ($objTable as $idx => $link){
						if (isset($attr['CHENTITY'])){
							$first_level_link = array_filter($link,'is_scalar');
							eval("\$objTable2 = \$link['".$tmpentity."']->$linkTable2;");
							$objTable2 = array_merge($first_level_link,$objTable2);	
						}else
							$objTable2 = $link;
						if (isset($attr['CHFILTER']) && isset($objTable2[$attr['CHFILTER']])){
							$arrfilter = array();
							$arrnotfilter = array();
							if (isset($attr['CHFILTERVAL'])){
								$tmp = explode(",", $attr['CHFILTERVAL']);
								foreach ($tmp as $idf=>$filterval)
								if (substr($filterval, 0, 1) == '!')
									$arrnotfilter[] = substr($filterval, 1);
								else
									$arrfilter[] = $filterval;
							}
						}
						if ((isset($arrfilter) && isset($arrnotfilter) && (empty($arrfilter) || in_array($objTable2[$attr['CHFILTER']], $arrfilter)) && !in_array($objTable2[$attr['CHFILTER']], $arrnotfilter)) || (!isset($arrfilter) && !isset($arrnotfilter))){
							$newid = $preid . $num_row_id++;
							$lastid = $newid;
							$newForm->saisieObj = $link[$tmpentity];
							$newForm->saisieTab = $objTable2;
							if (isset($attr['CHENTITY']))
								$curform = $newForm;
							else
								$curform = $this;

							echo "</tr><tr id=\"$linkTableName\$$newid\">";

							$editimg='';
							if (isset($attr['BUTTON_EDIT'])){

								if (isset($objTable2[$attr['BUTTON_EDIT']])){
                                                                $edit_button_val = $objTable2[$attr['BUTTON_EDIT']];}
								if (isset($attr['BUTTON_EDIT_JS'])){
									$edit_js = $attr['BUTTON_EDIT_JS'];
								} else {
									$edit_js = "popupDoc";
								}

								$editimg = '<a href="#1" onclick="javascript:'.$edit_js.'(\'index.php?urlaction=' . strtolower($nom) . 'Saisie&id_' . strtolower($nom) . '=' . $edit_button_val . '\')">
								<img border="0" class="miniTrash" src="' . imgUrl . 'button_modif.gif" title="' . kModifier . '" alt="' . kModifier . '"/></a>';

                                                                //B.RAVI 20150218 Car sous IE7, le clic sur le bouton modifier (qui est englobé dans un <a>) provoque la redirection vers le lien du <a> et pas le declenchement de l'attribut onClick de <img>
                                                                if (isset($attr['BUTTON_EDIT_IE7'])){
                                                                    $id_link_editimg='urlaction=' . strtolower($nom) . 'Saisie&id_' . strtolower($nom) . '=' . $edit_button_val;
                                                                    if ($attr['BUTTON_EDIT_IE7']=='id_tr'){
                                                                        $id_link_editimg='include=' . strtolower($nom) . 'Saisie&id_tr='. $newid;
                                                                    }
                                                                    $editimg = '<img border="0" onclick="javascript:popupDoc(\'indexPopup.php?'.$id_link_editimg. '\')"  class="miniTrash" src="' . imgUrl . 'button_modif.gif" title="' . kModifier . '" alt="' . kModifier . '"/>';
                                                                }// if (isset($attr['BUTTON_EDIT_IE7'])){



							}
							echo "<td class=\"resultsCorps\" >$editimg</td>";
							foreach ($attr['INNERFIELDS'][0]['ELEMENT'] as $idy => $elt) {
								$tmpclass = '';
								if (isset($elt['CLASS']))
									$tmpclass = $elt['CLASS'];
								echo "<td align=\"center\" class=\"resultsCorps $tmpclass\">";

								if (isset($elt['NONUMROW']))
									$elt['NAME'] = $elt['TABLE'] . "[][" . $elt['CHFIELDS'] . "]";
								else
									$elt['NAME'] = $elt['TABLE'] . "[$newid][" . $elt['CHFIELDS'] . "]";
								if (isset($objTable2[$elt['CHFIELDS']]))
									$elt['CHVALUES'] = $objTable2[$elt['CHFIELDS']];
								elseif (isset($link[$elt['CHFIELDS']]))
									$elt['CHVALUES'] = $link[$elt['CHFIELDS']];
								if ($elt['CHFIELDS'] == 'ID_ROW_TL')
									$elt['CHVALUES'] = $newid;

								if (isset($elt['CHNAME'])) {
									$elt['NAME'] = $elt['CHNAME'] . $elt['CHVALUES'];
									$elt['CHVALUES'] = "";
								}
								if (isset($elt['TYPE']) && $elt['TYPE'] == "checkboxSel") {
									$elt['TYPE'] = "checkbox";
									//var_dump($curform->saisieTab);
									$elt['NAME'] = "checkbox" . $elt['CHVALUES'];
									$elt['ID'] = "SEL_" . $elt['CHVALUES'];
									$elt['VALUE'] = "1";
								}

								$curform->displayElt($elt);
								echo "</td>";
							}
							$editimg = "";
							echo "<td class=\"resultsCorps\" ><table style=\"margin:0px;padding:0px;\"><tr><td>" . str_replace("[nb]", $newid, $trashimg) . "</td><td>" . $editimg . "</td></tr></table></td>";
						}
					}
				}

				echo "</tr></tbody></table>";
				echo makeFormJSFunctionTL2($linkTableName, ++$lastid);

				if (isset($attr['ID_TYPE_LEX'])){
					//Bouton ajout lexique
					if(!isset($attr['INDEX'])) $attr['INDEX']="kAjouter";
					if(!isset($attr['INDEX_CHAMP'])) $attr['INDEX_CHAMP']="PFC";
					if(!isset($attr['INDEX_XSL'])) $attr['INDEX_XSL']="paletteEdit";
					if(!isset($attr['INDEX_TYPE_LEX'])) $attr['INDEX_TYPE_LEX']=$attr['ID_TYPE_LEX'];
					$linkTable = $linkTableName;
					$nom = '';
					$attr['ROLE_VALUE'] = '';
				}
				//PC 13/10/10 : ajout d'un bouton ouvrant un formulaire détaillé
				elseif (isset($attr['ADD_LINK'])){
					eval('$idadd = $this->saisieObj->t_' . strtolower($this->entity).'['.$attr['ADD_ID'].'];');
					$jsButton = "javascript:popupDoc('".$attr['ADD_LINK'].$idadd."')";
					$attrButton = array("style" => "cursor:pointer;", "onclick" => "$jsButton");
					$this->addInput("button", $linkTableName . "\$add", kAjouter, $attrButton);
				}
				elseif (!isset($attr['NOADD'])) {
					//Bouton ajouter ligne
					$jsButton = "add_$linkTableName('$linkTableName', '', '', '');";
					$attrButton = array("style" => "cursor:pointer;", "onclick" => "$jsButton");
					$this->addInput("button", $linkTableName . "\$add", kAjouter, $attrButton);
				}

				unset($attr['INNERFIELDS']);
			}

			break;

			case "lien_src" :
				if($this->entity!="DOC") {
					return false ;
				}

				$linkTable = "t_" . strtolower($this->entity) . "_" . strtolower($nom);
				echo '<div id="t_doc_lies_SRC$tab" class="lineDoc" border="2">';
				// documents parents
				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"","ATTR"=>"style='font-size:12px;font-weight:bold;' ");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>"t_doc_liesSRC[][ID_DOC_DST]");
				$arrFields[]=array("ID"=>"","TYPE"=>"EDIT","ATTR"=>" alt='".kModifier."' title='".kModifier."' ");
				$arrFields[]=array("ID"=>"","TYPE"=>"MOVEUP");
				$arrFields[]=array("ID"=>"","TYPE"=>"MOVEDOWN");
				$arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");

				$js="<script>divs='';\n";
				echo makeFormJSFunction2('t_doc_lies_SRC',$arrFields,'lineDoc');
				foreach ($this->saisieObj->arrDocLiesSRC as $idx=>$dlsrc){

					$js.="arrRow=new Array('t_doc_lies_SRC$DIV$".$dlsrc['ID_DOC']."',".quoteField($dlsrc['DOC_TITRE']).",'".$dlsrc['ID_DOC']."','index.php?urlaction=docSaisie&id_doc='+encodeURIComponent('".$dlsrc['ID_DOC']."'),'".$dlsrc['DD_TCSRC']."','".$dlsrc['DD_TCDST']."','".$dlsrc['DD_DUREE']."');";
					$js.="out=add_t_doc_lies_SRC(arrRow);\n document.write(out[0]);\n";

				}
				$js.='</script>';echo $js;
				unset($arrFields);
				echo '<button id="t_doc_lies_SRC$add" class="ui-state-default ui-corner-all" type="button" onClick="choose(this,\'titre_index='.(isset($attr['LABEL'])?$attr['LABEL']:kParent).'&id_lang='.$this->saisieObj->t_doc['ID_LANG'].'&valeur=&champ=DOC'.(isset($attr['FILTER_TYPE_DOC'])?'&type_doc='.$attr['FILTER_TYPE_DOC']:'').'\')">
				<span class="ui-icon ui-icon-open"></span>'.kChoisir.'</button>';
				echo "</div>";

				break ;
			case "lien_dst" :
				if($this->entity!="DOC") {
					return false ;
				}
				$linkTable = "t_" . strtolower($this->entity) . "_" . strtolower($nom);

				// documents fils
				echo '<div id="t_doc_lies_DST$tab" class="lineDoc" border="2">';

				$arrFields[]=array("ID"=>"","TYPE"=>"SPAN", "NAME"=>"","ATTR"=>"style='font-size:12px;font-weight:bold;' ");
				$arrFields[]=array("ID"=>"","TYPE"=>"HIDDEN","NAME"=>"t_doc_liesDST[][ID_DOC_SRC]");
				$arrFields[]=array("ID"=>"","TYPE"=>"EDIT","ATTR"=>" alt='".kModifier."' title='".kModifier."' ");
				$arrFields[]=array("ID"=>"","TYPE"=>"MOVEUP");
				$arrFields[]=array("ID"=>"","TYPE"=>"MOVEDOWN");
				$arrFields[]=array("ID"=>"","TYPE"=>"TRASH","ATTR"=>" alt='".kSupprimer."' title='".kSupprimer."' ");

				$js="<script>divs='';\n";
				echo makeFormJSFunction2('t_doc_lies_DST',$arrFields,'lineDoc');
				foreach ($this->saisieObj->arrDocLiesDST as $idx=>$dldst){

					$js.="arrRow=new Array('t_doc_lies_DST$DIV$".$dldst['ID_DOC']."',".quoteField($dldst['DOC_TITRE']).",'".$dldst['ID_DOC']."','index.php?urlaction=docSaisie&id_doc='+encodeURIComponent('".$dldst['ID_DOC']."'),'".$dldst['DD_TCDST']."','".$dldst['DD_TCSRC']."','".$dldst['DD_DUREE']."');";
					$js.="out=add_t_doc_lies_DST(arrRow);\n document.write(out[0]);\n";

				}
				$js.='</script>';echo $js;
				unset($arrFields);

				echo '<button id="t_doc_lies_DST$add" class="ui-state-default ui-corner-all" type="button" onClick="choose(this,\'titre_index='.(isset($attr['LABEL'])?$attr['LABEL']:kFils).'&id_lang='.$this->saisieObj->t_doc['ID_LANG'].'&valeur=&champ=DOC\')">
				<span class="ui-icon ui-icon-open"></span>'.kChoisir.'</button>
				</div>';

			break ;
	}
		// Mise à jour paramètres bouton index
		// VP 18/03/09 : INDEX_NAME et INDEX_CHOOSE à XXX$index
		$attr['INDEX_NAME']=$linkTable.$nom.(isset($attr['ROLE_VALUE'])?$attr['ROLE_VALUE']:'').(isset($attr['PRECISION_VALUE'])?$attr['PRECISION_VALUE']:'')."\$index";
		$attr['INDEX_CHOOSE']="document.getElementById(\"".$attr['INDEX_NAME']."\")";
		//$attr['INDEX_CHOOSE']="document.getElementById(\"".$linkTable.$nom.$attr['ROLE_VALUE'].$attr['PRECISION_VALUE']."\$tab"."\")";
		if(isset($attr['READONLY'])) unset($attr['INDEX']);
	}
/**
* Affiche des checkbox
* IN : type checkbox , nom, tableau de paramètres
* OUT : affichage du champ
**/
// VP 3/07/09 : ajout paramètre pour fixer le nb de checkbox par ligne dans listCheckbox()

	function addCheckbox($type, $nom, $val, $attr){

		// VP 1/12/08 : suppression [] dans nom du champ
		//$nom.="[]";
		if (!isset($attr['CHTYPES']))
			$attr['CHTYPES']=null;

		if(!isset($type)) $type=$attr['CHTYPES'];
		if(!empty($this->saisieObj) && $type=="VI"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_val";
				$valTable=$this->saisieObj->t_doc_val;
			}else if($this->entity=="MAT"){
				$linkTable="t_mat_val";
				$valTable=$this->saisieObj->t_mat_val;
			}else if($this->entity=="DMAT"){
				$linkTable="t_doc_mat_val";
				$valTable=$this->saisieObj->t_doc_mat_val;
			}else if($this->entity=="PREX"){
				$linkTable="t_dp_val";
				$valTable=$this->saisieObj->t_dp_val;
			}
			$tabId=array();
			foreach ($valTable as $idx=>$v){
				//PC 01/02/13 : remplacement de VAL_ID_TYPE_VAL par ID_TYPE_VAL...
				//if ($v['VAL_ID_TYPE_VAL']==$attr['CHFIELDS']) $tabId[]=$v['ID_VAL'] ;
				if ($v['ID_TYPE_VAL']==$attr['CHFIELDS']) $tabId[]=$v['ID_VAL'] ;
			}
			$nom=$linkTable."[][ID_VAL]";
		}
		// Ajout champ caché dans le cas de la saisie
		if(isset($attr['HIDDEN_VALUE'])){
			$this->addInput("hidden", $nom,  $attr['HIDDEN_VALUE']);
		}elseif(!empty($this->saisieObj) && in_array($type,array('VI','LI','CAT'))){
			$this->addInput("hidden", $nom, "0");
		}
		
		switch($type){
			case "VI":
				// VP  4/01/10 : ajout paramètre TYPE_VAL
				if(isset($attr['TYPE_VAL'])) $type_val=$attr['TYPE_VAL'];
				else $type_val=$attr['CHFIELDS'];
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				// VP 7/05/10 : ajout paramètre TRI
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				//PC 16/11/10 : ajout paramètre pour limiter les valeurs
				$limit = '';
				if(isset($attr['LIMIT_VAL']))
					$limit = ' AND VALEUR ' . $attr['LIMIT_VAL'];

				listCheckbox($nom,"SELECT ID_VAL,VALEUR FROM t_val where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='". $_SESSION["langue"]."'$limit order by $tri",
							 array("ID_VAL","VALEUR"), $tabId,1,'',$attr['NB_CHECKBOX']);
				$this->addInput("hidden", "t_doc_val[][ID_TYPE_VAL]",  $type_val);
				

				break;
			
			case "CAT" : 
				$nom="t_doc_cat[][ID_CAT]";
				if(isset($attr['ID_TYPE_CAT'])) $type_val=$attr['ID_TYPE_CAT'];
				else $type_val=$attr['CHFIELDS'];
				$val = array() ; 
				foreach($this->saisieObj->t_doc_cat as $doccat){
					$val[] = $doccat['ID_CAT'];
				}
				
				print listCheckbox($nom,"select distinct ID_CAT as ID, CAT_NOM as VAL from t_categorie where CAT_ID_TYPE_CAT='".$type_val."' and ID_LANG='".$_SESSION['langue']."' order by 1",array('ID','VAL'),$val,1,'',$attr['NB_CHECKBOX']);
				$this->addInput("hidden", "t_doc_cat[][ID_TYPE_CAT]",  $type_val);
			break ; 
			
			case "CE":
			case "C":
				if($this->entity=="MAT"){
                                print listCheckbox($nom,"select distinct ".$attr['CHFIELDS']." from t_mat where ".$attr['CHFIELDS']."!='' order by 1",$attr['CHFIELDS'],$val,1,'',$attr['NB_CHECKBOX']);}
				else if($this->entity=="DOC"){
                                print listCheckbox($nom,"select distinct ".$attr['CHFIELDS']." from t_doc where ".$attr['CHFIELDS']."!='' and ID_LANG='".$_SESSION['langue']."' order by 1",$champ,$val,1,'',$attr['NB_CHECKBOX']);}
				else if($this->entity=="DMAT"){
                                print listCheckbox($nom,"select distinct ".$attr['CHFIELDS']." from t_doc_mat where ".$attr['CHFIELDS']."!='' order by 1",$champ,$val,1,'',$attr['NB_CHECKBOX']);}
                break;

			default:
				//$attr['CHECKBOX_SQL'] : "select ID_XX as ID, XX as VAL ...
				// langue à substituer éventuellement
				if(isset($attr['CHECKBOX_SQL'])) {
					$sql=$attr['CHECKBOX_SQL'];
					if((strpos($sql,"ID_LANG='FR'")>0) && strtoupper($_SESSION['langue'])!='FR') $sql=str_replace("ID_LANG='FR'","ID_LANG='".$_SESSION['langue']."'",$sql);
					print listCheckbox($nom,$sql,Array("ID","VAL"),$val,1,'',$attr['NB_CHECKBOX']);

				// VG 30/08/10 : ajout cas RADIO_OPTION/RADIO_LIB (déjà utilisé/fait dans class_form)
				}elseif(isset($attr['CHECKBOX_OPTION'])) {
						$tabOptions=explode(",",$attr['CHECKBOX_OPTION']);
						$tabLibs=explode(",",$attr['CHECKBOX_LIB']);
						foreach($tabOptions as $i=>$x) {
							//$checked="";
							//if((is_array($val) && in_array($tabOptions[$i],$val)) || ($val==$tabOptions[$i])) $checked=" checked";
							//print "<input type=\"checkbox\" name=\"".$nom."\" value=\"".$tabOptions[$i]."\"".$checked."/>".(defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i]);
                            $tabAttr[$i]=$attr;
							if((is_array($val) && in_array($tabOptions[$i],$val)) || ($val==$tabOptions[$i])) $tabAttr[$i]['CHECKED']="checked";
                            $this->addInput("checkbox", $nom,  $tabOptions[$i], $tabAttr[$i]);
                            print (defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i]);
						}
				}else{
					//VP (17/10/08) ajout cas checkbox de base + LD 04/03/09 statut checked
					// VP 25/09/09 : cas checked=false
					if(isset($attr['CHECKED']) && $attr['CHECKED']=="false") unset($attr['CHECKED']);
					elseif($val==$attr['VALUE']) $attr['CHECKED']=true;
					$this->addInput("checkbox", $nom,  $attr['VALUE'], $attr);
				}
				break;
		}


		
		if(!empty($this->saisieObj) && $type=='CAT'){ // cas categorie
			$this->addInput("hidden", $linkTable."[][ID_TYPE_CAT]", $attr['CHFIELDS']);
		}else if(!empty($this->saisieObj) && $type=='VI'){
			$this->addInput("hidden", $linkTable."[][ID_TYPE_VAL]", $attr['CHFIELDS']);
		}elseif(!empty($this->saisieObj) && ($type=='P' || $type=='LI')){
			// VP 11/06/09 : ajout paramètre ROLE_VALUE
			if(isset($attr['ROLE_VALUE'])) $this->addInput("hidden", $linkTable."[][DLEX_ID_ROLE]", $attr['ROLE_VALUE']);
			if (isset($attr['TYPE_DESC'])) $this->addInput("hidden", $linkTable."[][ID_TYPE_DESC]", $attr['TYPE_DESC']);
			else $this->addInput("hidden", $linkTable."[][ID_TYPE_DESC]", $attr['CHFIELDS']);
		}
	}
/**
* Affiche des boutons radio
 * IN : type radio , nom, tableau de paramètres
 * OUT : affichage du champ
 **/

	function addRadio($type, $nom, $val, $attr){
		if(!empty($this->saisieObj) && $type=="VI"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_val";
				$valTable=$this->saisieObj->t_doc_val;
			}else if($this->entity=="MAT"){
				$linkTable="t_mat_val";
				$valTable=$this->saisieObj->t_mat_val;
			}else if($this->entity=="DMAT"){
				$linkTable="t_doc_mat_val";
				$valTable=$this->saisieObj->t_doc_mat_val;
			}
			foreach ($valTable as $idx=>$v){
				if ($v['VAL_ID_TYPE_VAL']==$attr['CHFIELDS']) $val=$v['ID_VAL'] ;
			}
			$nom=$linkTable."[][ID_VAL]";
		}
		if(!isset($type)) $type=$attr['CHTYPES'];
		switch($type){
			case "VI":
				// VP  4/01/10 : ajout paramètre TYPE_VAL
				if(isset($attr['TYPE_VAL'])) $type_val=$attr['TYPE_VAL'];
				else $type_val=$attr['CHFIELDS'];
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				// VP 7/05/10 : ajout paramètre TRI
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				listRadio($nom,"SELECT ID_VAL,VALEUR FROM t_val where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='". $_SESSION["langue"]."' order by $tri",
						  array("ID_VAL","VALEUR"), $val);
				if(!empty($this->saisieObj)){
					$this->addInput("hidden", $linkTable."[][ID_TYPE_VAL]", $attr['CHFIELDS']);
				}
				break;

			case "CAT" :
				$nom="t_doc_cat[][ID_CAT]";
				if(isset($attr['ID_TYPE_CAT'])) $type_val=$attr['ID_TYPE_CAT'];
				else $type_val=$attr['CHFIELDS'];
				$val = array() ;
				foreach($this->saisieObj->t_doc_cat as $doccat){
					$val[] = $doccat['ID_CAT'];
				}
				
				print listRadio($nom,"select distinct ID_CAT as ID, CAT_NOM as VAL from t_categorie where CAT_ID_TYPE_CAT='".$type_val."' and ID_LANG='".$_SESSION['langue']."' order by 1",array('ID','VAL'),$val,1,'',$attr['NB_CHECKBOX']);
				if(!empty($this->saisieObj)){
					$this->addInput("hidden", "t_doc_cat[][ID_TYPE_CAT]",  $type_val);
				}
				break ;

			default:
				//$attr['RADIO_SQL'] : "select ID_XX as ID, XX as VAL ...
				// langue à substituer éventuellement
				if(isset($attr['RADIO_SQL'])) {
					$sql=$attr['RADIO_SQL'];
					if((strpos($sql,"ID_LANG='FR'")>0) && strtoupper($_SESSION['langue'])!='FR') $sql=str_replace("ID_LANG='FR'","ID_LANG='".$_SESSION['langue']."'",$sql);
					print listRadio($nom,$sql,Array("ID","VAL"),$val);
				}

				// VG 27/08/10 : ajout cas RADIO_/RADIO_LIB (déjà fait dans class_form)
				if(isset($attr['RADIO_OPTION'])) {
					$tabOptions=explode(",",$attr['RADIO_OPTION']);
					$tabLibs=explode(",",$attr['RADIO_LIB']);
					debug($tabOptions, "gold", true);
					foreach($tabOptions as $i=>$x) {

						$valOption = (defined($x)?constant(htmlentities($x)):htmlentities($x));
						$valOption = $x;
						// VP 28/04/11 : utilisation de addInput pour afficher les boutons radio
						$attrOption=$attr;
						$attrOption['VALUE']=$valOption;
						if($valOption==$val) $attrOption['CHECKED']="true";
						unset($attrOption['RADIO_OPTION']);
						unset($attrOption['RADIO_LIB']);
						if (!isset($attr['READONLY']) || $attrOption['CHECKED']=="true") {
							$this->addInput("radio", $nom, $valOption, $attrOption);
							if (isset($attr['WITH_LABEL'])) echo "<label>";
							print (defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i])."&nbsp;&nbsp;";
							if (isset($attr['WITH_LABEL'])) echo "</label>";
						}
//						print "<input ";
//						/*foreach($attr as $j=>$y) {
//							print " ".htmlentities($j)."=\"".htmlentities($y,ENT_QUOTES)."\"";
//						}*/
//
//						print " type=\"radio\" name=\"".$nom."\" value=\"".$valOption."\"".($valOption==$val?" checked":"")."/>".(defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i])."&nbsp;&nbsp;";
					}
				}
				break;
		}
	}

	/**
		* Affiche une balise span
	 * IN : nom, valeur par défaut, tableau de paramètres
	 * OUT : affichage du champ
	 **/
	function addSpan($nom, $val, $attr=null){
		if(!empty($this->saisieObj) && isset($attr['SPAN_TYPE']) && $attr['SPAN_TYPE']=="V"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_val";
				$valTable=$this->saisieObj->t_doc_val;
			}else if($this->entity=="MAT"){
				$linkTable="t_mat_val";
				$valTable=$this->saisieObj->t_mat_val;
			}else if($this->entity=="DMAT"){
				$linkTable="t_doc_mat_val";
				$valTable=$this->saisieObj->t_doc_mat_val;
			}else if($this->entity=="PERS"){
				$linkTable = "t_pers_val";
				$valTable = $this->saisieObj->t_pers_val;
			}
			foreach ($valTable as $idx=>$v){
				if ($v['VAL_ID_TYPE_VAL']==$attr['CHFIELDS'] || $v['ID_TYPE_VAL']==$attr['CHFIELDS']) $val=$v['VALEUR'] ;
			}
		} elseif(!empty($this->saisieObj) && isset($attr['SPAN_TYPE']) && $attr['SPAN_TYPE']=="L"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_lex";
				$valTable=$this->saisieObj->t_doc_lex;
			}
			foreach ($valTable as $idx=>$v){
				if ($v['LEX_ID_TYPE_LEX']==$attr['CHFIELDS'] || $v['ID_TYPE_DESC']==$attr['CHFIELDS']) $val=$v['LEXIQUE'] ;
			}

		} elseif(!empty($this->saisieObj) && isset($attr['SPAN_TYPE']) && $attr['SPAN_TYPE']=="P"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_lex";
				$valTable=$this->saisieObj->t_pers_lex;
			}
			foreach ($valTable as $idx=>$v){
				if ($v['LEX_ID_TYPE_LEX']==$attr['CHFIELDS'] || $v['ID_TYPE_DESC']==$attr['CHFIELDS']) $val=$v['PERS_NOM'] .' '.$v['PERS_PRENOM'];
			}

		} elseif(!empty($this->saisieObj) && isset($attr['SPAN_TYPE']) && $attr['SPAN_TYPE']=="MAT"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_mat";
				$valTable=$this->saisieObj->t_doc_mat;
			}
			
			// MS - ajout possibilité d'aller récupérer des informations du mat master
			// pour l'instant developpé uniquement dans le cas du form d'entité 'doc' 
			// il faudra éventuellement ajouter un critère permettant de déterminer si on souhaite le format master ou un format visio...
			foreach ($valTable as $idx=>$mat){
				if (isset($mat['MAT']->t_mat) && !empty($mat['MAT']->t_mat) && $mat['MAT']->t_mat['MAT_TYPE'] == 'MASTER'){
					if($attr['CHFIELDS'] == 'MAT_IMG_DIM' && isset($mat['MAT']->t_mat['MAT_WIDTH']) && !empty($mat['MAT']->t_mat['MAT_WIDTH']) && isset($mat['MAT']->t_mat['MAT_HEIGHT']) && !empty($mat['MAT']->t_mat['MAT_HEIGHT'])){
						$val=$mat['MAT']->t_mat['MAT_WIDTH']." x ".$mat['MAT']->t_mat['MAT_HEIGHT']." px";
					}else if(isset($mat['MAT']->t_mat[$attr['CHFIELDS']]) && !empty($mat['MAT']->t_mat[$attr['CHFIELDS']])){
						$val=$mat['MAT']->t_mat[$attr['CHFIELDS']];
					}else if(isset($mat['MAT']->$attr['CHFIELDS']) && !empty($mat['MAT']->$attr['CHFIELDS'])){
						$val=$mat['MAT']->$attr['CHFIELDS'];
					}
				}
			}
		
		} elseif(!empty($this->saisieObj) && isset($attr['SPAN_TYPE']) && $attr['SPAN_TYPE']=="VAR"){
			eval('$valTable=$this->saisieObj->'.$attr['CHFIELDS'].';');
			if (!empty($valTable)) $val=$valTable;

		}elseif(!empty($this->saisieObj) && isset($attr['SPAN_TYPE']) && $attr['SPAN_TYPE']=="booleen"){
			if ($val == "1") $val = constant("kOui");
			else $val = constant("kNon");
		}

		$attr['NAME']=$nom;
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		if($val) $val=(defined($val)?constant($val):$val);
		unset($attr['VALUE']);
		unset($attr['TYPE']);
		print "<span ";
		foreach($attr as $i=>$x) {
			print " $i=\"$x\"";
		}

		print ">";
		if (isset($attr['LINK_URL']) && !empty($attr['LINK_URL'])) {
			if(isset($attr['LINK_ID'])){
				$link = str_replace("[".$attr['LINK_ID']."]", $this->saisieTab[strtoupper($attr['LINK_ID'])], $attr['LINK_URL']);
			}else{
				// devrait matcher une suite de champs définis tels que [ID_DOC],[ID_PANIER] par ex
				preg_match_all("/\[([\S]+?)\]/",$attr['LINK_URL'],$matches);
				$link =$attr['LINK_URL']; 
				// ensuite, en fonction de ce qu'on trouve dans le tableau saisieTab, devrait permettre de faire le remplacement [ID_DOC] sera donc remplacé par la valeur contenue dans saisieTab[ID_DOC]
				if(isset($matches[1]) && !empty($matches[1])){
					foreach($matches[1] as $match){
						if(!empty($match) && isset($this->saisieTab[strtoupper($match)]) && !empty($this->saisieTab[strtoupper($match)])){
							$link = str_replace('['.$match.']',$this->saisieTab[strtoupper($match)],$link);
						}
					}
				}
			}
            if (isset($attr['LINK_JS']) && !empty($attr['LINK_JS'])) {
                echo "<a href=\"#\" onclick=\"".$attr['LINK_JS']."('$link')\">$val</a>";
            }else{
			echo "<a href=\"$link\" ".(!empty($attr['LINK_TARGET'])?'target='.$attr['LINK_TARGET']:'').">$val</a>";
            }
		}
		else print $val;
		print "</span>\n";
	}

/**
* Affiche un champ select
 * IN : type select , nom, tableau de paramètres
 * OUT : affichage du champ
 **/
	function addSelect($type, $nom, $val, $attr){
// VP 24/06/09 : traitement cas READONLY
// VP 25/06/09 : traitement attribut TYPE_VAL pour le cas VAL_ID_TYPE_VAL != ID_TYPE_VAL
        global $db;
		$tabId=array();
		if(!empty($this->saisieObj) && $type=="VI"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_val";
				$valTable=$this->saisieObj->t_doc_val;
			}else if($this->entity=="MAT"){
				$linkTable="t_mat_val";
				$valTable=$this->saisieObj->t_mat_val;
			}else if($this->entity=="DMAT"){
				$linkTable="t_doc_mat_val";
				$valTable=$this->saisieObj->t_doc_mat_val;
			}else if($this->entity=="PREX"){
				$linkTable = "t_dp_val";
				$valTable = $this->saisieObj->t_dp_val;
			//17/11/10 Ajout cas Personne pour t_pers_val
			}else if($this->entity=="PERS"){
				$linkTable = "t_pers_val";
				$valTable = $this->saisieObj->t_pers_val;
			}else if($this->entity=="FEST"){
				$linkTable = "t_fest_val";
				$valTable = $this->saisieObj->t_fest_val;
			}
			$nom=$linkTable."[][ID_VAL]";
                        // Eviter Warning: Invalid argument supplied for foreach()
                        if (is_object($valTable) || is_array($valTable)) {
                            foreach ($valTable as $idx=>$v){
                                    if ($v['VAL_ID_TYPE_VAL']==$attr['CHFIELDS'] || $v['ID_TYPE_VAL']==$attr['CHFIELDS']) $tabId[]=$v['ID_VAL'];
                            }
                        }

		}elseif(!empty($this->saisieObj) && $type=="LI"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_lex";
				$valTable=$this->saisieObj->t_doc_lex;
			}else if($this->entity=="PERS"){
				$linkTable = "t_pers_lex";
				$valTable = $this->saisieObj->t_pers_lex;
			}
			$nom=$linkTable."[][ID_LEX]";
			foreach ($valTable as $idx=>$v){
				if (isset($attr['TYPE_DESC'])){
					if ($v['ID_TYPE_DESC']==$attr['TYPE_DESC']) $tabId[]=$v['ID_LEX'] ;
				}
				else
					if ($v['ID_TYPE_DESC']==$attr['CHFIELDS']) $tabId[]=$v['ID_LEX'] ;
			}

		}else if($this->entity=="DOC" && $type=="P") {
			$linkTable="t_doc_lex";
			$valTable=$this->saisieObj->t_pers_lex;
			$nom=$linkTable."[][ID_PERS]";
			foreach ($valTable as $idx=>$v){
				if ($v['ID_TYPE_DESC']==$attr['CHFIELDS']) $tabId[]=$v['ID_PERS'] ;
			}
		}
		// MS 04/04/13 - ajout du cas entité == "VAL" pour formulaire valSaisie
		else if($this->entity == "VAL"){
			$valTable=$this->saisieObj->t_val;
		}

		//PC 31/08/10 : Ajout cas t_dp_pers
		else if($this->entity=="PREX" && $type=="P") {
			$linkTable = "t_dp_pers";
			$valTable = $this->saisieObj->t_dp_pers;
			$nom = $linkTable."[][ID_PERS]";
			foreach ($valTable as $idx=>$v){
				if ($v['ID_DOC_PREX'] == $this->saisieObj->t_prex["ID_DOC_PREX"])
					$tabId[] = $v['ID_PERS'] ;
			}

		}
		else if ($type=='CAT')
		{
			$linkTable="t_doc_cat";
			$valTable=$this->saisieObj->t_doc_cat;
			$nom=$linkTable."[][ID_CAT]";

			// choix des valeurs $tabId[]
			foreach ($valTable as $v)
			{
				if (!empty($attr['ID_TYPE_CAT']) && $v['CAT_ID_TYPE_CAT']==$attr['ID_TYPE_CAT'])
					$tabId[]=$v['ID_CAT'];
				elseif ($v['CAT_ID_TYPE_CAT']==$attr['CHFIELDS'])
					$tabId[]=$v['ID_CAT'] ;
			}
		}


		//VP 22/06/09 : suppression id par défaut dans select
		print "<select name='".$nom."'";
		if(!isset($attr['ID'])) $attr['ID']=$nom;
		foreach($attr as $i=>$x) {
			print " $i=\"$x\"";
		}
		//if(isset($attr['CLASS'])) print " CLASS='".$attr['CLASS']."'";
		print ">\n";
		if (!$attr['NOALL'] && !$attr['READONLY']) {
			if(!empty($this->saisieObj)) print "<option value=''></option>";
			else print "<option value=''>".kTous."</option>";
		}
		if(!isset($type)) $type=$attr['CHTYPES'];
		// VP 2/3/09 : ajout paramètre cacheTime pour requête SQL
		if(isset($attr['CACHETIME'])) $cacheTime=$attr['CACHETIME'];
		else $cacheTime=360;
		// VP 30/11/09 : ajout paramètre TRI
		switch($type){
			case "V":
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				if(isset($attr['READONLY'])) print listOptions("select VALEUR from t_val where VAL_ID_TYPE_VAL='".$attr['CHFIELDS']."' and ID_LANG='".$_SESSION['langue']."' and ID_VAL=".intval($val)." order by $tri","VALEUR",$val,1,$cacheTime);
				else print listOptions("select VALEUR from t_val where VAL_ID_TYPE_VAL='".$attr['CHFIELDS']."' and ID_LANG='".$_SESSION['langue']."' order by $tri","VALEUR",$val,1,$cacheTime);
				break;

			case "VI":
				// MS 19.01.15 - Dans le cas readonly, l'affichage plantait car $val était mal récupéré.
				// dans le cas normal, on utilise même pas $val ....
				if(!empty($tabId) && is_array($tabId)){ $val = implode(',',$tabId); }
                else $val=intval($val);
				//by LD 29/10/08 : récupération de la valeur déjà présente si val non spécifié
				if (empty($val)) {
                                    // Eviter Warning: Invalid argument supplied for foreach()
                                    if (is_object($valTable) || is_array($valTable)) {
					foreach ($valTable as $_v){
						// MS 16/12/14 ne marchera jamais car $nom est le nom de l'input,
						// la récupération est faite plus haut si le t_doc_val est correctement alimenté ...
						if ($_v['ID_TYPE_VAL']==strtoupper($nom)) $val=$_v['ID_VAL'];
					}
                                    }
				}
				
				if(isset($attr['TYPE_VAL'])) $type_val=$attr['TYPE_VAL'];
				else $type_val=$attr['CHFIELDS'];
				// VP 29/07/14 : filtrage par val_code
				if(isset($attr['VAL_CODE'])){
					$sql_codes="and VAL_CODE in ('".str_replace(",","','", $attr['VAL_CODE'])."')";
				}
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				if(isset($attr['READONLY']) ){ print listOptions("select ID_VAL,VALEUR from t_val where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='".$_SESSION['langue']."' and ID_VAL in (".$val.") $sql_codes order by $tri",Array("ID_VAL","VALEUR"),$tabId,1,$cacheTime);
				}else if(isset($attr['SELECT_LIST'])){
					print listOptionsFromArray($this->listOptions[$attr['SELECT_LIST']], $tabId);
				}else{
					print listOptions("select ID_VAL,VALEUR from t_val where VAL_ID_TYPE_VAL='".$type_val."' and ID_LANG='".$_SESSION['langue']."' $sql_codes order by $tri",Array("ID_VAL","VALEUR"),$tabId,1,$cacheTime);
				}
				break;

			case "LI":


                if(!empty($tabId)) $val=implode(',',$tabId);
                else $val=intval($val);
				if (empty($val)) {
					// MS 16/12/14 ne marchera jamais car $nom est le nom de l'input,
					// la récupération est faite plus haut si le t_doc_lex est correctement alimenté ...
					foreach ($valTable as $_v) if ($_v['ID_TYPE_DESC']==strtoupper($nom)) $val=$_v['ID_LEX'];
				}
				if(isset($attr['TYPE_LEX'])) $type_val=$attr['TYPE_LEX'];
				else $type_val=$attr['CHFIELDS'];
				$tri=(isset($attr['TRI'])?$attr['TRI']:"LEX_TERME");

                                if (isset($attr['SELECT_SQL'])){
                                   print listOptions($attr['SELECT_SQL']." and ID_LANG='".$_SESSION['langue']."' order by $tri",Array("ID_LEX","LEX_TERME"),$tabId,1,$cacheTime);
                                }
                                else if(isset($attr['READONLY'])) {print listOptions("select ID_LEX,LEX_TERME from t_lexique where ID_TYPE_LEX='".$type_val."' and ID_LANG='".$_SESSION['langue']."'  and ID_LEX in (".$val.") order by $tri",Array("ID_LEX","LEX_TERME"),$tabId,1,$cacheTime);}
                                else {print listOptions("select ID_LEX,LEX_TERME from t_lexique where LEX_ID_TYPE_LEX='".$type_val."' and ID_LANG='".$_SESSION['langue']."' order by $tri",Array("ID_LEX","LEX_TERME"),$tabId,1,$cacheTime);}

				break;

			case "VGEN":
				//by LD 29/10/08 : récupération de la valeur déjà présente si val non spécifié
				if(isset($attr['TYPE_VAL'])) $current_type_val=$attr['TYPE_VAL'];
				else $current_type_val = $valTable['VAL_ID_TYPE_VAL'];
				$id_current_val = $valTable['ID_VAL'];
				$val_id_gen = $valTable['VAL_ID_GEN'];
				// VP 27/07/09 : tri par VAL_CODE avant VALEUR
				$tri=(isset($attr['TRI'])?$attr['TRI']:"VAL_CODE,VALEUR");
				if(isset($attr['READONLY'])) print listOptions("select ID_VAL,VALEUR from t_val where VAL_ID_TYPE_VAL='".$current_type_val."' and ID_LANG='".$_SESSION['langue']."' and ID_VAL<>".intval($id_current_val)." order by $tri",Array("ID_VAL","VALEUR"),$val_id_gen,1,$cacheTime);
				else print listOptions("select ID_VAL,VALEUR from t_val where VAL_ID_TYPE_VAL='".$current_type_val."' and ID_LANG='".$_SESSION['langue']."' and ID_VAL<>".intval($id_current_val)." order by $tri",Array("ID_VAL","VALEUR"),$val_id_gen,1,$cacheTime);
				break;

			case 'CAT':
				// champ select pour la categorie

				if(isset($attr['ID_TYPE_CAT'])) $id_type_cat=$attr['ID_TYPE_CAT'];
				else $id_type_cat=$attr['CHFIELDS'];
				if (!empty($id_type_cat))
                    $sql_type_cat = ' AND CAT_ID_TYPE_CAT='.$db->Quote($id_type_cat);
				else
					$sql_type_cat = '';
				$tri=(isset($attr['TRI'])?$attr['TRI']:"CAT_NOM");

				echo listOptions('SELECT ID_CAT,CAT_NOM FROM t_categorie WHERE ID_LANG=\''.$_SESSION['langue'].'\''.$sql_type_cat." order by $tri",array('ID_CAT','CAT_NOM'),$tabId,1,$cacheTime);

				break;

			case "P":
                if(!empty($tabId)) $val=implode(',',$tabId);
                else $val=intval($val);

				$tri=(isset($attr['TRI'])?$attr['TRI']:"PERS");
				// GT 19/10/2011 : ajout d'un close sur PERS_ID_TYPE_LEX au cas ou il y a la valeur id_type_lex dans le XML
				if (!empty($attr['ID_TYPE_LEX']))
					$type_lexique=" AND PERS_ID_TYPE_LEX='".$attr['ID_TYPE_LEX']."'";
				else
					$type_lexique="";

                
				// GT 19/10/2011 : ajout d'un close sur PERS_ID_TYPE_LEX au cas ou il y a la valeur select_sql dans le XML
				if (!empty($attr['SELECT_SQL']))
					$select_sql=$attr['SELECT_SQL']." AND ID_LANG='".$_SESSION['langue']."' order by $tri";
				else
					$select_sql="select ID_PERS,".$db->Concat('PERS_NOM',"' '","PERS_PRENOM")." as PERS from t_personne where ID_LANG='".$_SESSION['langue']."'".$type_lexique." order by $tri";

                // VP 3/06/09 : ajout cas Personne
				if(isset($attr['READONLY'])) print listOptions("select ID_PERS,".$db->Concat('PERS_NOM',"' '","PERS_PRENOM")." as PERS from t_personne where ID_LANG='".$_SESSION['langue']."'".$type_lexique." and ID_PERS in (".$val.") order by $tri",Array("ID_PERS","PERS"),$tabId);
				else print listOptions($select_sql,Array("ID_PERS","PERS"),$tabId);
				break;

			case "F":
				$fonds_champ = (isset($attr['FONDS_CHAMP'])?$attr['FONDS_CHAMP']:"FONDS_COL");
				$tri=(isset($attr['TRI'])?$attr['TRI']:$fonds_champ);
				// VP 20/11/08 : modif liste_id
				// VP 19/05/10 : utilisation tableau pour liste_id
				$usr=User::getInstance();
				if ($usr->Type!=kLoggedAdmin){
					foreach($_SESSION["USER"]["US_GROUPES"] as $v){
						if ($v["ID_PRIV"]>4 && !empty($v["ID_FONDS"])) $liste_id[]=$v["ID_FONDS"];
					}
					if(count($liste_id)>0) {
						$sqlFonds="select DISTINCT FONDS, $fonds_champ from t_fonds WHERE (ID_FONDS in ('".implode("','",$liste_id)."') or ID_FONDS_GEN in ('".implode("','",$liste_id)."'))";
						if(isset($attr['READONLY'])) $sqlFonds.=" AND ID_FONDS='".$val."'";
					}
				}else{
					$sqlFonds="select DISTINCT FONDS, $fonds_champ from t_fonds";
					if(isset($attr['READONLY'])) $sqlFonds.=" WHERE FONDS='".$val."'";
				}
				if($sqlFonds) {
					$sqlFonds.=" order by $tri";
					print listOptions($sqlFonds,Array("FONDS",$fonds_champ),$val,1,$cacheTime);
				}

				break;

			case "FI":
				$fonds_champ = (isset($attr['FONDS_CHAMP'])?$attr['FONDS_CHAMP']:"FONDS_COL");
				$tri=(isset($attr['TRI'])?$attr['TRI']:$fonds_champ);
				// VP 20/11/08 : modif liste_id
				// VP 19/05/10 : utilisation tableau pour liste_id
				//PC 08/04/11 : Ajout du filtre par langue
				$usr=User::getInstance();
				if ($usr->Type!=kLoggedAdmin){
					foreach($_SESSION["USER"]["US_GROUPES"] as $v){
						if ($v["ID_PRIV"]>4 && !empty($v["ID_FONDS"])) $liste_id[]=$v["ID_FONDS"];
					}
					if(count($liste_id)>0) {
						$sqlFonds="select DISTINCT ID_FONDS, $fonds_champ from t_fonds WHERE (ID_FONDS in ('".implode("','",$liste_id)."') or ID_FONDS_GEN in ('".implode("','",$liste_id)."'))  and ID_LANG='".$_SESSION["langue"]."'";
						if(isset($attr['READONLY'])) $sqlFonds.=" AND ID_FONDS=".intval($val);
					}
				}else{
					$sqlFonds="select DISTINCT ID_FONDS, $fonds_champ from t_fonds WHERE ID_LANG='".$_SESSION["langue"]."'";
					if(isset($attr['READONLY'])) $sqlFonds.=" and ID_FONDS=".intval($val);
				}
				if($sqlFonds) {
					$sqlFonds.=" order by $tri";
					print listOptions($sqlFonds,Array("ID_FONDS",$fonds_champ),$val,1,$cacheTime);
				}
				break;

			case "C":
			case "CE":
				if($this->entity=="MAT"){
					if(isset($attr['READONLY'])) print listOptions("select distinct ".$attr['CHFIELDS']." from t_mat where ".$attr['CHFIELDS']."='".$val."' order by 1",$attr['CHFIELDS'],$val,1,$cacheTime);
					else print listOptions("select distinct ".$attr['CHFIELDS']." from t_mat order by 1",$attr['CHFIELDS'],$val,1,$cacheTime);
				}else if($this->entity=="DOC"){
					if(isset($attr['READONLY'])) print listOptions("select distinct ".$attr['CHFIELDS']." from t_doc where ID_LANG='".$_SESSION['langue']."' and ".$attr['CHFIELDS']."='".$val."' order by 1",$champ,$val,1,$cacheTime);
					else print listOptions("select distinct ".$attr['CHFIELDS']." from t_doc where ID_LANG='".$_SESSION['langue']."' order by 1",$champ,$val,1,$cacheTime);
				}else if($this->entity=="DMAT"){
					if(isset($attr['READONLY'])) print listOptions("select distinct ".$attr['CHFIELDS']." from t_doc_mat where ".$attr['CHFIELDS']."='".$val."' order by 1",$champ,$val,1,$cacheTime);
					else print listOptions("select distinct ".$attr['CHFIELDS']." from t_doc_mat order by 1",$champ,$val,1,$cacheTime);
				}
				break;

				//@update VG 28/09/2010 : ajout du cas "DATE", qui permet de lister des dates sur un certain intervalle, avec un certain pas.
			case "DATE" :
				$dateTime = new DateTime();
				if(isset($attr['DATE_DEBUT'])) {
					$aDate = explode('-', $attr['DATE_DEBUT']);
					$year = (!empty($aDate[0]))?$aDate[0]:$dateTime->format('Y');
					$month = (!empty($aDate[1]))?$aDate[1]:$dateTime->format('m');
					$day = (!empty($aDate[2]))?$aDate[2]:$dateTime->format('d');
					$dateTime->setDate($year, $month, $day);
				}
				switch ($attr['DATE_ELEMENT']) {
					case 'Y':
						for ($i=0;$i<$attr['SELECT_NB']; $i++) {
							$date = $dateTime->format('Y');
							$dateTime->sub(new DateInterval('P1Y'));
							print "<option value=\"".$date."\"".($date==$val?" selected":"").">".$date."</option>";
						}
						break;
					case 'M' :
						for ($i=0;$i<$attr['SELECT_NB']; $i++) {
							$date = $dateTime->format('F');
							$dateTime->add(new DateInterval('P1M'));
							print "<option value=\"".$date."\"".($date==$val?" selected":"").">".$date."</option>";
						}
						break;
				}
				break;

			default:
				//$attr['SELECT_SQL'] : "select ID_XX as ID, XX as VAL ...
				// langue à substituer éventuellement
				// VP 24/07/09 : prise en compte READONLY pour cas SELECT_SQL
				if(isset($attr['SELECT_LIST'])){
					print listOptionsFromArray($this->listOptions[$attr['SELECT_LIST']], $val);
				} elseif(isset($attr['SELECT_SQL'])) {
					$sql=$attr['SELECT_SQL'];
					if ($attr['LINK_ID']) {
						if(!empty($this->saisieTab[$attr['LINK_ID']])) {
							$sql .= $this->saisieTab[$attr['LINK_ID']];
						} else {
							$sql = preg_replace("/(WHERE)?\s+".$attr['LINK_ID']."\s*=/i","",$sql);
						}
					}
					if((stripos($sql,"ID_LANG='FR'")>0) && strtoupper($_SESSION['langue'])!='FR') $sql=str_replace("ID_LANG='FR'","ID_LANG='".$_SESSION['langue']."'",$sql);
					if(isset($attr['READONLY']) && !empty($val)) {
						$_mots=explode(" ",strtoupper($sql));
						$key=array_search("AS",$_mots);
						if($key>1) $sql=str_ireplace("WHERE","WHERE ".$_mots[$key-1]."='".$val."' and ",$sql);
					}
					print listOptions($sql,Array("ID","VAL"),$val,1,$cacheTime);
				} elseif(isset($attr['SELECT_OPTION'])) {
					$tabOptions=explode(",",$attr['SELECT_OPTION']);
					$tabLibs=explode(",",$attr['SELECT_LIB']);
					foreach($tabOptions as $i=>$x) {
						print "<option value=\"".$tabOptions[$i]."\"".($tabOptions[$i]==$val?" selected":"").">".(defined($tabLibs[$i])?constant($tabLibs[$i]):$tabLibs[$i])."</option>";
					}
				}

				break;
		}

		print "</select>\n";
		// Ajout champ caché dans le cas de la saisie
		if(!empty($this->saisieObj) && $type=='CAT'){ // cas categorie
			$this->addInput("hidden", $linkTable."[][ID_TYPE_CAT]", $attr['CHFIELDS']);
		}else if(!empty($this->saisieObj) && $type=='VI'){
			$this->addInput("hidden", $linkTable."[][ID_TYPE_VAL]", $attr['CHFIELDS']);
		}elseif(!empty($this->saisieObj) && ($type=='P' || $type=='LI')){
			// VP 11/06/09 : ajout paramètre ROLE_VALUE
			if(isset($attr['ROLE_VALUE'])) $this->addInput("hidden", $linkTable."[][DLEX_ID_ROLE]", $attr['ROLE_VALUE']);
			if (isset($attr['TYPE_DESC'])) $this->addInput("hidden", $linkTable."[][ID_TYPE_DESC]", $attr['TYPE_DESC']);
			else $this->addInput("hidden", $linkTable."[][ID_TYPE_DESC]", $attr['CHFIELDS']);
		}
	}

	// VP 27/06/09 : création méthode addInputSaisie()
	function addInputSaisie($type, $nom, $val, &$attr){
		global $db;
		if(!empty($this->saisieObj) && $attr['SUBTYPE']=="VI"){
			if($this->entity=="DOC") {
				$linkTable="t_doc_val";
				$valTable=$this->saisieObj->t_doc_val;
			}else if($this->entity=="MAT"){
				$linkTable="t_mat_val";
				$valTable=$this->saisieObj->t_mat_val;
			}else if($this->entity=="DMAT"){
				$linkTable="t_doc_mat_val";
				$valTable=$this->saisieObj->t_doc_mat_val;
			}else if($this->entity=="PREX"){
				$linkTable="t_dp_val";
				$valTable=$this->saisieObj->t_dp_val;
			}
			foreach ($valTable as $idx=>$v){
				if ($v['VAL_ID_TYPE_VAL']==$attr['CHFIELDS']) $val=$v['VALEUR'] ;
			}
			$nom=$linkTable."[][VALEUR]";
		}else if (!empty($this->saisieObj) && ($attr['SUBTYPE']=="LTEXT" || $attr['SUBTYPE']=="PTEXT")){
			if($this->entity=="DOC") {
				$linkTable="text_doc_lex";
				$valTable=$this->saisieObj->t_doc_lex;
			}
			$type_desc=$attr['CHFIELDS'];
			$nom = $linkTable."[][text]";
		}
		if(!empty($this->saisieObj) && ($attr['SUBTYPE']=="LTEXT" || $attr['SUBTYPE']=="PTEXT")){
			$this->addInput("hidden", $linkTable."[][ID_TYPE_DESC]",$type_desc );

			if(isset($attr['ID_TYPE_LEX']) && $attr['SUBTYPE'] == 'LTEXT'){
				$this->addInput("hidden", $linkTable."[][ID_TYPE_LEX]",$attr['ID_TYPE_LEX']);
			}else if (isset($attr['ID_TYPE_LEX']) && $attr['SUBTYPE'] == 'PTEXT'){
				$this->addInput("hidden", $linkTable."[][PERS_ID_TYPE_LEX]",$attr['ID_TYPE_LEX']);
			}else if (isset($attr['PERS_ID_TYPE_LEX'])){
				$this->addInput("hidden", $linkTable."[][PERS_ID_TYPE_LEX]",$attr['PERS_ID_TYPE_LEX']);
			}

			if(isset($attr['ROLE_VALUE'])){
				$this->addInput("hidden", $linkTable."[][DLEX_ID_ROLE]",$attr['ROLE_VALUE']);
			}
		}

		$this->addInput($type, $nom, $val,$attr);
		// Ajout champs cachés dans le cas de la saisie
		if(!empty($this->saisieObj) && $attr['SUBTYPE']=='VI'){
			$this->addInput("hidden", $linkTable."[][ID_VAL]", "0");
			$this->addInput("hidden", $linkTable."[][ID_TYPE_VAL]", $attr['CHFIELDS']);
		}

	}

    // VP 29/07/14 : création méthode addMultiText()
	function addMultiText($type, $nom, &$attr){
		global $db;
        switch($type){
            case "LI" :
                if($this->entity=="DOC") {
                    $linkTable="text_doc_lex";
                    $valTable=$this->saisieObj->t_doc_lex;
                }
                $type_desc=$attr['CHFIELDS'];
                $nom = $linkTable."[][text]";
                $val="";
                foreach ($valTable as $idx=>$v){
                    if ($v['ID_TYPE_DESC']==$type_desc && ($v['DLEX_ID_ROLE']==$attr['ROLE_VALUE'] || empty($attr['ROLE_VALUE'])) ) {
                        $val.=", ".$v['LEX_TERME'];
						if(!empty($v['LEX_PRECISION'])) $val.=" (".$v['LEX_PRECISION'].")";
                   }
                }
                $val=substr($val,2);
                // Affichage champs cachés
                $this->addInput("hidden", $linkTable."[][ID_TYPE_DESC]",$type_desc );
                if(isset($attr['ID_TYPE_LEX'])){
                    $this->addInput("hidden", $linkTable."[][ID_TYPE_LEX]",$attr['ID_TYPE_LEX']);
                }
                if(isset($attr['ROLE_VALUE'])){
                    $this->addInput("hidden", $linkTable."[][DLEX_ID_ROLE]",$attr['ROLE_VALUE']);
                }
                break;
            case "P":
                if($this->entity=="DOC") {
                    $linkTable="text_doc_lex";
                    $valTable=$this->saisieObj->t_pers_lex;
                }
                $type_desc=$attr['CHFIELDS'];
                $nom = $linkTable."[][text]";
                $val="";
                foreach ($valTable as $idx=>$v){
                    if ($v['ID_TYPE_DESC']==$type_desc && ($v['DLEX_ID_ROLE']==$attr['ROLE_VALUE'] || empty($attr['ROLE_VALUE'])) ) {
                        $val.=", ".trim($v['PERS_PRENOM']." ".$v['PERS_NOM']);
						if(!empty($v['LEX_PRECISION'])) $val.=" (".$v['LEX_PRECISION'].")";
                    }
                }
                $val=substr($val,2);
                // Affichage champs cachés
                $this->addInput("hidden", $linkTable."[][ID_TYPE_DESC]",$type_desc );
                if (isset($attr['ID_TYPE_LEX'])){
                    $this->addInput("hidden", $linkTable."[][PERS_ID_TYPE_LEX]",$attr['ID_TYPE_LEX']);
                }else if (isset($attr['PERS_ID_TYPE_LEX'])){
                    $this->addInput("hidden", $linkTable."[][PERS_ID_TYPE_LEX]",$attr['PERS_ID_TYPE_LEX']);
                }

                if(isset($attr['ROLE_VALUE'])){
                    $this->addInput("hidden", $linkTable."[][DLEX_ID_ROLE]",$attr['ROLE_VALUE']);
                }
                break;

            case "VI":
                if($this->entity=="DOC") {
                    $linkTable="text_doc_val";
                    $valTable=$this->saisieObj->t_doc_val;
                }
                $nom = $linkTable."[][text]";
                foreach ($valTable as $idx=>$v){
                    if ($v['VAL_ID_TYPE_VAL']==$attr['CHFIELDS']) {
                        $val.=", ".$v['VALEUR'];
                    }
                }
                $val=substr($val,2);
                // Affichage champs cachés
                $this->addInput("hidden", $linkTable."[][ID_TYPE_VAL]",$attr['CHFIELDS']);
                break;
		}

        // Affichage Textarea
        $this->addTextarea($nom, $val, $attr);

	}
	
		
	function addFileImage($type, $nom, $val, $attr=null){
		$name_field = strtolower($attr['CHFIELDS']);
		if(!empty($attr['CHVALUES'])){
			$src_image = (isset($attr['SRC_PREFIX']) && !empty($attr['SRC_PREFIX'])?$attr['SRC_PREFIX']:'').(isset($attr['CHVALUES']) && !empty($attr['CHVALUES'])?$attr['CHVALUES']:$val);
		}else{
			$src_image = (isset($attr['SRC_PREFIX']) && !empty($attr['SRC_PREFIX'])?$attr['SRC_PREFIX']:'').(isset($attr['CHVALUES']) && !empty($attr['CHVALUES'])?$attr['CHVALUES']:$val);
		}
		echo '<div id="'.$name_field .'_upload" class="file_image_upload" >';
		echo '<input id="'.$name_field .'_upload_input" class="file_image_upload_input" name="'.$name_field .'" accept="image/pjpeg, image/x-png, image/jpeg, image/png, imge/gif" type="file" data-accept-file="jpg,jpeg,png,gif"/>';
		echo '<img id="'.$name_field .'_upload_img"  class="file_image_upload_img"  src="'.$src_image.'"/>';
		echo '<button id="'.$name_field .'_upload_button" class="file_image_upload_button std_button_style"><?=kModifier?></button>';
		echo '</div>';
	
	
		echo '<script type="text/javascript">
		try{
			// définition du FileReader si le browser le permet => permet le preview du fichier à uploader. 
			if(typeof FileReader != \'undefined\'){
				fileupload_reader = new FileReader(); 
				fileupload_reader.onload = function(e){
					$j("#'.$name_field .'_upload_img").eq(0).attr(\'src\',e.target.result);
					$j("#'.$name_field .'_upload").removeClass(\'loading\');
				}
			}
			// event handler lors de l\'ajout d\'un fichier à l\'input file
			$j("#'.$name_field .'_upload_input").change(function(event){
				// cas avec filereader => on va génèrer une preview
				if(typeof FileReader!=\'undefined\'){
					$j("#'.$name_field .'_upload").addClass(\'loading\');
					fileupload_reader.readAsDataURL($j(this).get(0).files[0]);
				}else{
					if(typeof $j(this).get(0).files != \'undefined\'){
						filename = $j(this).get(0).files[0].name; 
					}else{
						filename= $j(this).get(0).value;
						filename = filename.substring(filename.lastIndexOf("fakepath\\\\")+9);
					}
					$j("#'.$name_field .'_upload").addClass(\'changed\');
					$j("#'.$name_field .'_upload_img").replaceWith(\'<div id="'.$name_field .'_upload_info" class="file_image_upload_info"><span id="'.$name_field .'_upload_filename" class="file_image_upload_filename">\'+filename+\'</span></div>\');
				}
			});
			
			// Répercute les clicks de la zones d\'upload image vers l\'input file => ouvre la fenetre de choix du fichier lors d\'un click sur toutes la zone d\'upload
			$j("#'.$name_field .'_upload").click(function(event){
				event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);
				$j("#'.$name_field .'_upload_input").click();
			});
			// annulation de la propagation des events depuis '.$name_field .'_upload_input => evite une loop avec l\'évenement précédent ^
			$j("#'.$name_field .'_upload_input").click(function(event){
				event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);
			});
		}catch(e){
			console.log("crash fileimageuploader : "+e);
		}
		</script>';
	
	
	}

}
?>
