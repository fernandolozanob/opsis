<?php
class Visualisation {

// Récupération d'infos
// Découpage
// Génération de répertoire
	 var $tempDir;

	 var $offset;
	 var $tcin;
	 var $tcout;

	 var $id_mat; //matériel original
	 var $objMat; // objet matériel en cours de visionnage
	 var $id_doc; //document à visionner
	 var $objDoc; // objet document en cours de visionnage

	 var $fileOut; // fichier pour le visionnage

	 var $arrFiles; //tableau des fichiers à copier dans chaque répertoire visionnage, structure array(['IN']=>nom du fichier source,['OUT']=>nom du fichier de sortie)

	 var $sliced; //Flag indiquant s'il y a eu découpage ou non

	 function __construct() {

	 }

 	 // génération du répertoire sur la base de microtime
	 function makeDir () {

		$nomDossierCourt=preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()); //nom de dossier défini par microtime

		$this->tempDir=kCheminLocalVisionnage.$nomDossierCourt."/";
		$this->tempDirShort=$nomDossierCourt;
		if(!mkdir($this->tempDir,0775)){
			$this->dropError(kErrorVisioCreationTempDir);
			return false;
		}
		return true;

	 }

	 // encodage du nom avec des caractères autorisés
	function makeFileName ($keep_extension=false) {
		// VP 24/02/10 : ajout des TC dans le nom du fichier de visionnage
		if (!$keep_extension) $extension="mov";
		else $extension=getExtension($this->objMat->t_mat['MAT_NOM']);
		
		// VP 6/05/10 : test sur tcin et tcout (cas diaporama par exemple)
		if((isset($this->objMat->t_mat['MAT_FORMAT']) && stripos($this->objMat->t_mat['MAT_FORMAT'],'HLS')!==false ) && !empty($this->tcin)&&!empty($this->tcout)){ 
			$id_mov=stripExtension($this->objMat->t_mat['MAT_NOM']).str_replace(":","","_".$this->tcin."_".$this->tcout);
		}elseif(!empty($this->tcin)&&!empty($this->tcout)){ 
			$id_mov=stripExtension($this->objMat->t_mat['MAT_NOM']).str_replace(":","","_".$this->tcin."_".$this->tcout).".".$extension;
		}else{
			$id_mov=stripExtension($this->objMat->t_mat['MAT_NOM']).".".$extension;
		}
		$this->fileOut=$this->removeTrickyChars($id_mov);
		if (empty($this->fileOut)) return false;
		return true;
		
	}
	
	function removeTrickyChars($str) {

		return str_replace(array(' ','\'','"','$','%','?',';',':','!','#','|','>','<','/','*','\\','&',"'",'~','(',')','=',','),'_',$str);
	}


	 function sliceMovie($shellcommand="") {
		 // VP 5/05/10 : intégration appels outils de découpage
		 $this->makeFileName();
         $useQTFastStart=false;
		 if (strpos($this->objMat->t_mat['MAT_FORMAT'],'MPEG4')!==false || strpos($this->objMat->t_mat['MAT_FORMAT'],'MPG4')!==false || strpos($this->objMat->t_mat['MAT_FORMAT'],'H264')!==false ) {
			 //$shellcommand="echo DEBUT\n /usr/local/bin/sliceOfMov \"%s\" %s %s %s \"%s\" \"%s\"\necho FIN\n";
			 if (defined('kBinDecoupage') && kBinDecoupage=='sliceOfMov' && strpos(PHP_OS,'Darwin')===0  ) {
				 $shellcommand=kSliceOfMovpath." \"%s\" %s %s %s \"%s\" \"%s\" \n";
			 } else if (defined('kBinDecoupage') && kBinDecoupage=='modmovie' ) {
				 // VP 23/02/10 : intégration modmovie
				 $duree=(tcToFrame($this->tcout) - tcToFrame($this->tcin) +1)/25;
				 $start=(tcToFrame($this->tcin) - tcToFrame($this->offset))/25;
				 // modmovie -self-contained Test.mp4 -startAt 00:00:10.40 -trimTo 00:00:10.44 -o Extrait.mov
				 $shellcommand=kBinModMovie." -self-contained \"%1\$s\" -startAt ".$start." -trimTo ".$duree." -o \"%5\$s\" 2>\"%6\$sfich.log\"";
			 } else if (defined('kBinDecoupage') && kBinDecoupage=='mp4box' ) {
				 $this->makeFileName(true);
				 $in=floor(tcToSec($this->tcin) - tcToSec($this->offset));
				 $out=1 + floor(tcToSec($this->tcout) - tcToSec($this->offset));
				 $shellcommand=kMp4Boxpath." \"%1\$s\" -split-chunk ".$in.":".$out." -out \"%5\$s\" 2>\"%6\$sfich.log\"";
			 } else {
//				 if (!strpos(PHP_OS,'Darwin')) $includeClassPath="%%CLASSPATH%%;";
//				 $shellcommand="java -cp ".$includeClassPath.kJavaMpgtx." JavaMpgtx \"%s\" %s %s %s \"%s\" \n";
				 $this->makeFileName(true);
                 $start=tcToSecDec($this->tcin)-tcToSecDec($this->offset);
                 $duration=tcToSecDec($this->tcout)-tcToSecDec($this->tcin);
                 // VP 5/04/2013 : utilisatin ffmbc et paramètres -ss et -t avant -i
                 if(defined("kFFMBCpath")){
                     $shellcommand=kFFMBCpath." -ss ".$start." -t ".$duration." -i \"%1\$s\" -faststart auto -vcodec copy -acodec copy \"%5\$s\" 2>\"%6\$sfich.log\"";
                     $useQTFastStart=false;
                 }else {
                     $shellcommand=kFFMPEGpath." -ss ".$start." -t ".$duration." -i \"%1\$s\" -vcodec copy -acodec copy \"%5\$s\" 2>\"%6\$sfich.log\"";
                     $useQTFastStart=true;
                 }
			 }
		 } else {
			 $this->makeFileName(true); 							
			 if($this->objMat->t_mat['MAT_TCFORCE']=='1' || strpos($this->objMat->t_mat['MAT_FORMAT'], "MPEG2")===false && strpos($this->objMat->t_mat['MAT_FORMAT'], "MPEG1")===false){ // cas où le fichier mpeg n'a pas de TC (MS : devrait etre not(MPEG2) ET not(MPEG1), non ? )
				 $this->tc_embedded=false;
				 $in=frameToTC(tcToFrame($this->tcin) - tcToFrame($this->offset));
				 $out=frameToTC(tcToFrame($this->tcout) - tcToFrame($this->offset));
			 } else {
				 $this->tc_embedded==true;
				 $in=$this->tcin;
				 $out=$this->tcout;
			 }
			 //VP 5/05/10 : intégration mp3splt
			 if ($this->objMat->t_mat['MAT_FORMAT']=='MP3' && defined('kMp3SpltPath')) {
				 $in=$this->formatTCmp3splt($in);
				 $out=$this->formatTCmp3splt($out);
				 $dir=$this->tempDir;
				 $fic=stripExtension($this->fileOut);
				 $shellcommand=kMp3SpltPath." \"%1\$s\" -d $dir -o $fic $in $out  2>%6\$sfich.log";
			 }elseif (defined('kDecoupAvidemuxPath')) {
				 $shellcommand=kDecoupAvidemuxPath." \"%1\$s\" $in $out \"%5\$s\" %6\$sfich.log";
			 }elseif (defined('kMPGTXpath'))  {
				 $in=$this->formatTC($in);
				 $out=$this->formatTC($out);
				 $shellcommand=kMPGTXpath." -s \"%1\$s\" [$in-$out]  -f -o \"%5\$s\" 2>%6\$sfich.log";
			 }else{
				 $this->makeFileName(true);
                 $start=tcToSecDec($this->tcin)-tcToSecDec($this->offset);
                 $duration=tcToSecDec($this->tcout)-tcToSecDec($this->tcin);
                 // VP 24/04/2013 : utilisation ffmbc et paramètres -ss et -t avant -i
                 if(defined("kFFMBCpath")){
                     $shellcommand=kFFMBCpath." -ss ".$start." -t ".$duration." -i \"%1\$s\"  -vcodec copy -acodec copy \"%5\$s\" 2>\"%6\$sfich.log\"";
                 }else {
                     $shellcommand=kFFMPEGpath." -ss ".$start." -t ".$duration." -i \"%1\$s\" -vcodec copy -acodec copy \"%5\$s\" 2>\"%6\$sfich.log\"";
                 }
             }
			 
		 }
		 
		//require_once(modelDir.'model_materiel.php');
	 	$exec_shell=sprintf($shellcommand,$this->objMat->getLieu().$this->objMat->t_mat['MAT_NOM'],$this->offset,$this->tcin,$this->tcout,$this->tempDir.$this->fileOut,$this->tempDir);
	 	
	 	//$exec_shell=sprintf($shellcommand,Materiel::getLieuByIdMat($this->id_mat).$this->id_mat,$this->offset,$this->tcin,$this->tcout,$this->tempDir.$this->fileOut,$this->tempDir);
	 	//debug($exec_shell,'red');
	 	trace($exec_shell);
	 	if (empty($exec_shell)) {$this->dropError(kErrorVisioCommandeVide);return false;} 

	 	exec($exec_shell,$out); //lancement
	 	//trace($exec_shell);
	 	//if (!is_file($this->tempDir.$this->fileOut)) return false; // échec de découpage
	 	$this->sliced=true;
		// By VP (4/9/08) : Extraction du TC réel de découpage d'après le fichier de log (MPGTX)
		// A implémenter pour mp4box
		if(is_file($this->tempDir."/fich.log")){
			$log = file_get_contents($this->tempDir."/fich.log");
			if(strpos($log,"debut: ")>0) {
				$_offset=str_replace(".",":",substr($log,strpos($log,"debut: ")+7,11));
				// La valeur de 5 est empirique, elle est dû à un défaut de lecture de QT suite à un découpage par mpgtx
				// Inutile pour aviDemux
				if($this->tc_embedded==true){
					//$this->offset=frameToTC(tcToFrame($_offset) - 5);
					$this->offset=$_offset;
				}else{
					//$this->offset=frameToTC(tcToFrame($_offset) + tcToFrame($this->offset) - 5);
					$this->offset=frameToTC(tcToFrame($_offset) + tcToFrame($this->offset));
				}
			} else $this->offset=$this->tcin;
		} else $this->offset=$this->tcin;

         // QTfaststart
		 if ($useQTFastStart) {
             $cmd=kBinQTFastStart." "."\"".$this->tempDir.$this->fileOut."\" \"".$this->tempDir.$this->fileOut.".mp4\"";
             exec($cmd,$out);
             trace($cmd);
             if(is_file($this->tempDir.$this->fileOut.".mp4")) rename($this->tempDir.$this->fileOut.".mp4",$this->tempDir.$this->fileOut);
         }

         return true;

	 }

	 function copyMovie() {

		$ok = symlink($this->objMat->getLieu().$this->objMat->t_mat['MAT_NOM'], $this->tempDir.$this->fileOut);

	 	//debug($this->objMat->getLieu().$this->objMat->t_mat['MAT_NOM']." >>>> ".$this->tempDir.$this->fileOut,'red');
	 	if (!$ok) {$this->dropError(kErrorVisioCopieMovie);return false;}
	 	return true;
	 }

	//MS backup ancienne fonction catMovie, remplacé par la fonction suivante-- VP 12/01/10 : ajout fonction catMovie (attention, Mac OS X uniquement)
	function old_catMovie($list) {
		if (defined('kBinCatMovie') ) {
			//foreach($list as $file){$catList.=" \"".$file."\"";}
			//$cmd=kBinCatMovie." ‑self‑contained ".$catList."  -o \"".$this->tempDir.$this->fileOut."\"";
			foreach($list as $file){$catList.=" ".$file;}
			//$cmd=kBinCatMovie." -mp4 -force-same-tracks ".$catList."  -o ".$this->tempDir.$this->fileOut;
			$cmd=kBinCatMovie." -self-contained ".$catList."  -o ".$this->tempDir.$this->fileOut;
			trace($cmd);
			exec($cmd);
			return true;
		}else{
			$this->dropError(kErrorVisioNoCatMovie);
			return false;
		}
	}
	
	function catMovie($list) {
		if (defined('kFFMPEGpath') ) {
			if(count($list)==1 && file_exists($list[0])){
				$this->mediaPath = $list[0];
				$this->mediaUrl = kVisionnageUrl.$this->tempDirShort."/".basename($list[0]);
				return true ; 
			}
			
			$cmd = "";
			$filter_mapping = "";
			foreach($list as $idx=>$file){
				$cmd .=" -i ".$file;
				$filter_mapping .= "[".$idx.":0][".$idx.":1]";
			}
			$cmd.= " -filter_complex '".$filter_mapping." concat=n=".count($list).":v=1:a=1 [v][a]' -map '[v]' -map '[a]' -f mp4  -vcodec libx264 -aspect ";
			if(isset($this->playlist_ratio) && $this->playlist_ratio <1.5){
				$cmd.="4:3 ";
			}else{
				$cmd.="16:9 ";
			}
			$cmd .= "-r 25 -pix_fmt yuv420p -vb 700k  -profile:v baseline -acodec libfaac -sample_fmt s16 -ac 2 -ar 44100 -ab 96k ".$this->tempDir."/montage.mp4";
			
			if(strpos(kFFMPEGpath,'ssh') !== false){
				$cmd = kFFMPEGpath." ".escapeQuoteShell($cmd);
			}else{
				$cmd = kFFMPEGpath.$cmd;
			}
			
			trace($cmd);
			exec($cmd,$output,$ret_code);
			if($ret_code!=0){
				$this->dropError(kErrorCatMovie);
				return false ; 
			}
			$this->mediaPath = $this->tempDir."/montage.mp4";
			$this->mediaUrl = kVisionnageUrl.$this->tempDirShort."/montage.mp4";
			
			return true;
		}else{
			$this->dropError(kErrorVisioNoCatMovie);
			return false;
		}
	}
	
	function genVisuCarton($id,$node=null){
		// trace("genVisuCarton");
		if(defined('kFFMPEGpath') && defined("kFFMBCpath")){
			$filename = substr($id , 0 , strpos($id,'_'));
			// Si on a des informations clip_params (nouvelle version du montage en ligne)
			if(!empty($node) && isset($node[0]->clip_params)){
				$node_params = array();
				foreach($node[0]->clip_params[0] as $key=>$node_value){
					$node_params[$key] = (string)$node_value;
				}
			}else{
				$node_params = array(
					'fontfamily'=>'Vera',
					'alignment'=>'middle'
				);
			}
			if(strpos($id,'pic') === 0 ){
				// cas d'un carton depuis un doc image
				$id_doc = substr($id , (strpos($id,'_')+1)) ; 
				try{
					require_once(modelDir."model_doc.php");
					$imgDoc = new Doc() ; 
					$imgDoc->t_doc['ID_DOC'] = $id_doc ; 
					$imgDoc->t_doc['ID_LANG'] = $_SESSION['langue'] ; 
					$imgDoc->getDoc(); 
					$imgDoc->getMats(); 
					$dmat = $imgDoc->getMaterielVisu(false,false, '_vis');
					$imgPath = $dmat['MAT']->getFilePath() ; 
					unset($imgDoc);
					unset($dmat);
					// trace("doc ".$id_doc." imgPath ".$imgPath);
				}catch(Exception $e){
					trace("genVisuCarton - crash lors de la récupération du mat de visionnage du doc ".$id_doc);
				}
			}/*else{
				// cas d'un carton standard
			}*/
			if(!isset($imgPath) || empty($imgPath) || !file_exists($imgPath)){
				$imgPath = jobImageDir."/PixelNoir.png";
				
			}
			
			if(isset($node_params['print_text'])){
				$text = str_replace('\n',"\n",$node_params['print_text']);
			}else if (strpos($id, 'carton') === 0 ){
				$text = substr($id , (strpos($id,'_')+1));
			}else{
				$text= "" ; 
			}
			// récupération du fichier de police à utiliser en fonction de la police et du style
			$fontfile = getFontFilename('gAvailableFontMap',
				(isset($node_params['fontfamily'])?$node_params['fontfamily']:null),
				(isset($node_params['fontweight'])?$node_params['fontweight']:null),
				(isset($node_params['fontitalic'])?$node_params['fontitalic']:null)
			);
			// gestion des cas d'erreurs / fallbacks vers les anciens comportements 
			if(!isset($fontfile) || !$fontfile || !file_exists(jobFontDir.'/'.$fontfile)){
				$fontfile = "Vera.ttf";
			}
			
			// gestion de l'alignement du texte : 
			$margin = 60 ; 
			switch($node_params['alignment']){
				case 'left' :
					$str_align = (string)$margin ; 
					break ; 
				case 'right' :
					$str_align ="main_w-(text_w + ".$margin .")"; 
					break ; 
				case 'middle' : 
				default : 
					$str_align ="(main_w-text_w)/2"; 
					break ; 
			
			}

			// taille de la police
			switch((int)$node_params['fontsize']){
				case 1 :
					// très petite
					$fontsize = 16 ; 
					$line_max_length = 80;
					$interligne = 8 ; 
					$line_h = 15;
					break ;
				case 2 :
					// petite
					$fontsize = 32 ; 
					$line_max_length = 45;
					$interligne = 10 ; 
					$line_h = 24;
					break ;
				case 3 :
					// moyenne
					$fontsize = 48 ; 
					$line_max_length = 26;
					$interligne = 12 ; 
					$line_h = 38;
					break ;
				case 4 :
					// grande
					$fontsize = 64 ; 
					$line_max_length = 21;
					$interligne = 14 ;
					$line_h = 56;					
					break ;
				case 5 :
					// très grande
					$fontsize = 80 ; 
					$line_max_length = 17;
					$interligne = 16 ; 
					$line_h = 64;
					break ; 
				 
				default : 
					// default => même valeurs que "grande"
					$fontsize = 64 ; 
					$line_max_length = 21;
					$interligne = 14 ;
					$line_h = 56;					
					break ; 
			
			}
			
			// permet principalement de récupérer la virgule, qui doit être échappée en tant que &#44; 
			//car la virgule est le délimiteur des ID qu'on appelle avec prepareVisu est
			$lines = explode("\n",html_entity_decode($text)); 
			$lines2 = array();
			// traitement de la chaine à incruster : pour l'instant 25char/lignes, 4lignes max
			foreach($lines as $idx => $line){
				// traitement des chaines de + de $line_max_length char
				if(strlen($line) > $line_max_length ){
					$tmp = $line;
					while (strlen($tmp)>$line_max_length){
						$length_cut = strrpos($tmp,' ',($line_max_length-strlen($tmp)));
						
						//sécurité pour éviter loop infinie si le texte est long et ne contient aucun espace 
						if($length_cut ==0 ){
							$length_cut = $line_max_length ; 
						}
						$tmp_line = substr($tmp,0,$length_cut);
						array_push($lines2,$tmp_line);
						$tmp = substr($tmp,strlen($tmp_line));
					}
					array_push($lines2,$tmp);
					unset($tmp);
					unset($tmp_line);
				}else{
					array_push($lines2,$line);
				}
			}
			$lines = $lines2;
			unset($lines2);
			$count_lines = count($lines);
			$constant_offset = "-$line_h/2";
			
			$anchors_h = array() ; 
			if($count_lines%2 == 0 ){
				$constant_middle = 0.5*$interligne;
			}else if ($count_lines%2 != 0 ){
				$constant_middle = "($line_h/2 + ".$interligne.")" ; 
			}
			
			for($i = - intval($count_lines/2); $i <= intval($count_lines/2) ; $i++ ){
				if($count_lines%2 == 0 && $i == 0 ){
					continue ; 
				}
				$sign = (($i>=0)?"+":"-") ;
				$j = abs($i);

				 if($i == 0 ){
					$anchors_h[] = "main_h/2 ".$constant_offset;
				}else{
					$anchors_h[] = "main_h/2 ".$sign.$constant_middle." ".$sign."($line_h/2) ".$sign.($j-1)."*($line_h + ".$interligne.") ".$constant_offset;
				}
			}

			if(!defined("jobImageDir")){$this->dropError(kErrorGenCarton." jobImageDir undefined"); return false ; }
			$cmd =" -f image2 -framerate 25 -loop 1 -y -i ".$imgPath." -f lavfi -i anullsrc=r=44100:cl=stereo -f mp4 -vcodec libx264 -vprofile baseline -aspect 16:9 -r 25 -pix_fmt yuv420p -vb 1000k -vf \"";
			
			$cmd.= "scale=min(960\,2*trunc((540*dar+1)/2)):min(540\,2*trunc((960/dar+1)/2)),pad=960:540:max(0\,(960 - iw)/2):max(0\,(540-ih)/2):black,";
			
			// ajout d'un filtre par ligne de texte à afficher.
			if(!defined("jobFontDir")){$this->dropError(kErrorGenCarton." jobFontDir undefined"); return false ; }
			foreach($lines as $idx=> $line){
				if($idx!=0){
					$cmd.=",";
				}
				$escaped_line = str_replace("'","’",$line);
				$escaped_line = preg_replace('/([^[:alnum:]| ])/','\\\\$1',$escaped_line);
				$cmd .="drawtext=fontfile=".jobFontDir."/".$fontfile.":fontsize=".$fontsize.":text='".$escaped_line."':x=".$str_align.":y=".$anchors_h[$idx].":fontcolor=white";	
			}
			$filename = $filename.'.mp4';
			
			$cmd .= ",fade=in:0:25,fade=out:100:25\" -t 5 ".$this->tempDir."/".$filename;
			if(strpos(kFFMPEGpath,'ssh') !== false){
				$cmd = kFFMPEGpath." ".escapeQuoteShell($cmd);
			}else{
				$cmd = kFFMPEGpath.$cmd;
			}
			trace("genVisuCarton - ".$cmd);
			exec($cmd,$output,$ret_code);
			if($ret_code!=0){
				$this->dropError(kErrorGenCarton);
				return false ; 
			}
			
			$this->tcin = secToTc(0);
			$this->tcout = secToTc(5);
			$this->mediaPath = $this->tempDir."/".$filename;
			$this->mediaUrl = kVisionnageUrl.'/'.$this->tempDirShort."/".$filename;
			$this->offset = 0;
			return true;
		}else{
			$this->dropError(kError);
			return false; 
		}
	}
	function genPartMontage(){
		// trace("genPartMontage");
		if(defined("kFFMBCpath") && isset($this->mediaPath)){
			$duration = secToTime(tcToSecDec($this->tcout)-tcToSecDec($this->tcin));
			// MS - 12.06.14  ajout prise en copte offset lors de la préparation des éléments du montage
			$tcin=secToTime(tcToSecDec($this->tcin)-tcToSecDec($this->offset));

			$newMediaPath = dirname($this->mediaPath)."/montage_".basename($this->mediaPath);
			
			// MS detection de la présence d'un stream audio, si pas de stream => ajout manuel d'un stream audio vide;
			$cmd = kFFPROBEpath." -show_streams ".$this->mediaPath." | grep codec_type ";
			$output = shell_exec($cmd);
			if(strpos($output,"audio") === false ){
				$sound_file = "audio_".stripExtension(basename($this->mediaPath)).".wav";
				$audio_gen_cmd = kFFMBCpath." -ar 44100 -t ".$duration." -f s16le -acodec pcm_s16le -i /dev/zero -f wav -acodec copy -y ".$this->tempDir."/".$sound_file;
				trace($audio_gen_cmd);
				$output = shell_exec($audio_gen_cmd);
				$addSound = " -i ".$this->tempDir."/".$sound_file;
			}else{
				$addSound = "";
			}
			
			$cmd = " -y -ss ".$tcin." -t ".$duration." -i ".$this->mediaPath.$addSound." -f mp4 -faststart auto -vcodec libx264 ";
			if(isset($this->playlist_ratio) && $this->playlist_ratio < 1.5){
				$cmd.="-aspect 4:3 -r 25 -pix_fmt yuv420p -profile baseline -b 700k -vf \"yadif=0:-1:1,scale=min(640\,480*dar*sar+1):min(480\,640/(dar*sar)+1),pad=640:480:max(0\,(640-iw)/2):max(0\,(480-ih)/2):black\" ";
			}else{
				$cmd.="-aspect 16:9 -r 25 -pix_fmt yuv420p -profile baseline -b 700k -vf \"yadif=0:-1:1,scale=min(640\,360*dar*sar+1):min(360\,640/(dar*sar)+1),pad=640:360:max(0\,(640-iw)/2):max(0\,(360-ih)/2):black\" ";
			}
			$cmd.= " -acodec libfaac -sample_fmt s16 -ac 2 -ar 44100 -ab 96k ".$newMediaPath;
			
			if(strpos(kFFMBCpath,'ssh') !== false){
				$cmd = kFFMBCpath." ".escapeQuoteShell($cmd);
			}else{
				$cmd = kFFMBCpath.$cmd;
			}
			trace($cmd);
			exec($cmd,$output,$ret_code);
			if($ret_code!=0){
				$this->dropError(kErrorGenCarton);
				return false ; 
			}
			
			$this->mediaPath = $newMediaPath;
			return true; 
		}
	}
	
	
	function copyRefFiles() {

	 	foreach ($this->arrFiles as $file) {

	 		if (empty($file['IN']) || empty($file['OUT'])) {$this->dropError(kErrorVisioFichierRefVide);return false;}
			if(!copy(kCheminLocalVisionnage.$file['IN'],$this->tempDir.$file['OUT'])) {
					$this->dropError(kErrorVisioCopieFichierRef);
				return false;
			}
	 	}
	 	return true;
	 }



 	/**
	 * Ajoute un message d'erreur pour la sortie
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function dropError($errLib) { // A ETOFFER EN TRY CATCH ?
		$this->error_msg.=$errLib."<br/>";
		trace($errLib);
	}

	/** Récupération des formats aptes à être visualisés (=FORMAT_ONLINE dans t_format_mat)
	 *  et stockage en session
	 *  IN : reset (si true, la liste est forcément remise à jour par SQL)
	 *  OUT : $_SESSION['FORMATS']['VISU'] (tab 1 dimension). Ce tableau est également envoyé en retour de fonction
	 * 	 */
	static function getFormatsForVisu ($reset=false) {
		global $db;
		if ($reset) unset($_SESSION['FORMATS']['VISU']);
		if (!isset($_SESSION['FORMATS']['VISU'])) {
				$sql="SELECT FORMAT_MAT from t_format_mat WHERE FORMAT_ONLINE=1";
				$rs=$db->GetAll($sql);
				foreach ($rs as $r) $_SESSION['FORMATS']['VISU'][]=$r['FORMAT_MAT'];
		}
		return $_SESSION['FORMATS']['VISU'];

	}

	/** Récupération des formats aptes à être découpé (=FORMAT_RECOPIE dans t_format_mat)
	 *  et stockage en session
	 *  IN : reset (si true, la liste est forcément remise à jour par SQL)
	 *  OUT : $_SESSION['FORMATS']['SLICE'] (tab 1 dimension). Ce tableau est également envoyé en retour de fonction
	 * 	 */
	static function getFormatsForSlice($reset=false) {
		global $db;
		if ($reset) unset($_SESSION['FORMATS']['SLICE']);
		if (!isset($_SESSION['FORMATS']['SLICE'])) {
				$sql="SELECT FORMAT_MAT from t_format_mat WHERE FORMAT_RECOPIE=1";
				$rs=$db->GetAll($sql);
				foreach ($rs as $r) $_SESSION['FORMATS']['SLICE'][]=$r['FORMAT_MAT'];
		}
		return $_SESSION['FORMATS']['SLICE'];
	}


 /**
 * Prend un TC et remplace le dernier separateur par un .
 * Obligatoire pour mpgtx
 */
	function formatTC($str) {

	   if (substr_count($str,":") > 1) {
	   $lastsemicolon = strrpos($str,":");
	   $str = substr_replace($str,".",$lastsemicolon,1);
	   }
	return $str;

	}
 /**
  * rend un TC sous la forme mm.ss.cc où cc est un centième de seconde
  * Format d'entrée de mp3splt
  */
 
 function formatTCmp3splt($tc,$timebase=25) {
	 $tab_tc = explode(":", $tc);
	 return ($tab_tc[0]*60 + $tab_tc[1]).".".$tab_tc[2].".".floor(100*$tab_tc[3]/$timebase);
 }
 
/**
 * Calcule la résolution et l'échelle de la vidéo, sur la base des infos xml stockées dans MAT_INFO
 * IN : string à interpreter (ie. contenu du champ MAT_INFO)
 * OUT : 3 var de classes => width, height et scale (tofit/aspect, utilisé par QT)
 * NOTE : pour le calcul de la résolution, les priorités sont les suivantes :
 * 	display_height (si existe) > video_window > 320x240
 */
 //update VP 29/06/11
	function calcResolution ($sourceInfo) {
		$width=($this->objMat->t_mat['MAT_DISPLAY_WIDTH']?$this->objMat->t_mat['MAT_DISPLAY_WIDTH']:$this->objMat->t_mat['MAT_WIDTH']);
		$height=($this->objMat->t_mat['MAT_DISPLAY_HEIGHT']?$this->objMat->t_mat['MAT_DISPLAY_HEIGHT']:$this->objMat->t_mat['MAT_HEIGHT']);
		//update VG 20/05/2013 : pour assurer la compatibilité avec les matériels n'ayant que MAT_INFO initialisé,
		// et les appels spécifiques, où la propriété objMat ne serait pas initialisée
		if(empty($height) || empty($width)) {
			$native_resol=@preg_match("%<video_window>(.*?)</video_window>%",$sourceInfo,$out);
			list($nativeX,$nativeY)=@split('x',$out[1]);
	
			// VP 29/11/10 : résolution par défaut
			// VP 29/06/11 : correction bug ordre sur display_width et display_height
			//$resol=@preg_match("%<display_width>(.*?)</display_width><display_height>(.*?)</display_height>%",$sourceInfo,$out);
			$resol=@preg_match("%<display_width>(.*?)</display_width>%",$sourceInfo,$outw);
			$resol=@preg_match("%<display_height>(.*?)</display_height>%",$sourceInfo,$outh);
			$width=($outw[1]?$outw[1]:($nativeX?$nativeX:320));
			$height=($outh[1]?$outh[1]:($nativeY?$nativeY:240));
		}
		if ($width && $height && $width/$height>1.34) $scale='tofit'; else $scale='aspect';

		$this->width=$width;
		$this->height=$height;
		$this->scale=$scale;

	}
 
	
} //fin classe
?>
