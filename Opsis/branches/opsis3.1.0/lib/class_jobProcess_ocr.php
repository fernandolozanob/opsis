<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once(modelDir.'model_materiel.php');
require_once(libDir."class_matInfo.php");

require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_fileException.php');

class JobProcess_ocr extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('ocr');
		
		$this->required_xml_params=array
		(
			'langue'=>'string'
		);
		
		$this->optional_xml_params=array
		(
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		
		$params_job=$this->getXmlParams();
		
		$list_fichier=array();
		
		$info_fichier_in=MatInfo::getMatInfo($this->getFileInPath());
		
		if ($info_fichier_in['type_mime']=='application/pdf')
		{
			// le fichier in est un pdf
			//pdftojpg.sh 47\ 02\ 13\ -réalisateur\ plateau\ EMJDS\ 27\ 01\ 2013\ ASoland.pdf 0 0 tmp/
			// Usage : pdftojpg.sh <fichier pdf> <largeur> <hauteur> <repertoire de sortie>
			if(defined("kBinConvertText")){
				$output_tmp_dir = $this->getTmpDirPath()."/pdftotext_tmp/";
				if (mkdir($output_tmp_dir)!== false){
					$this->shellExecute(kBinConvertText," ".escapeQuoteShell($this->getFileInPath())." ".escapeQuoteShell($output_tmp_dir."/extract.txt"));
								
					if(file_exists($output_tmp_dir."/extract.txt") && filesize($output_tmp_dir."/extract.txt") > 1){
						$skip_tesseract = true;
					}else{
						if(file_exists($output_tmp_dir."/extract.txt")){
							unlink($output_tmp_dir."/extract.txt");
							rmdir($output_tmp_dir);
						}
						$skip_tesseract = false ; 
					}
				}
				$this->writeOutLog('skip_tesseract en faveur de pdftotext flag : '.intval(isset($skip_tesseract) && intval($skip_tesseract)));
			}
			
			if(!isset($skip_tesseract) || (isset($skip_tesseract) && !$skip_tesseract)){
				// creation repertoire temporaire
				$tmp_dir=$this->getTmpDirPath().'/story_tmp/';
				
				if (mkdir($tmp_dir)===false)
					throw new DirectoryException('failed to create tmp dir ('.$tmp_dir.')');
				
				$this->shellExecute(kConvertPath,' -density 300 -normalize -depth 8 '.escapeQuoteShell($this->getFileInPath()).' "'.$tmp_dir.'/'.stripExtension(basename($this->getFileInPath())).'.jpg"');
							
				foreach (scandir($tmp_dir) as $fichier)
				{
					if(substr($fichier,0,1)!='.'){
						$list_fichier[]=$tmp_dir.$fichier;
					}
				}
			}
		}
		else if ($info_fichier_in['type_mime']=='image/png' || $info_fichier_in['type_mime']=='image/jpg' || $info_fichier_in['type_mime']=='image/jpeg' || $info_fichier_in['type_mime']=='image/gif' || $info_fichier_in['type_mime']=='image/tiff')
		{
			// fichier in est une image
			$list_fichier[]=$this->getFileInPath();
		}
		else
			throw new FileException('File Format');
		
		
		if(!isset($skip_tesseract) || (isset($skip_tesseract) && !$skip_tesseract)){
			// creation du repertoire temporaire
			$output_tmp_dir=$this->getTmpDirPath().'/text_tmp/';
			echo $output_tmp_dir;
			if (mkdir($output_tmp_dir)===false)
				throw new DirectoryException('failed to create output tmp dir ('.$output_tmp_dir.')');
			
			// boucle sur les fichiers du storyboard pdf
			$num_page=0;
			foreach ($list_fichier as $fichier)
			{
				$num_page++;
				$nom_fichier_sortie=$output_tmp_dir.sprintf('%06d',$num_page).''; 
				// execution de tesseract
				$this->shellExecute(kTesseractPath,'-l '.$params_job['langue'].' '.escapeQuoteShell($fichier).' '.escapeQuoteShell($nom_fichier_sortie).'');
				$this->setProgression(intval(($num_page)*100/count($list_fichier)));
			}
		}
		
		// suppression des fichiers temporaires 
		if ($info_fichier_in['type_mime']=='application/pdf' && (!isset($skip_tesseract) || (isset($skip_tesseract) && !$skip_tesseract)))
		{
			foreach (scandir($tmp_dir) as $fichier)
			{
				if(substr($fichier,0,1)!='.'){
					if (file_exists($tmp_dir.$fichier))
					{
						if (unlink($tmp_dir.$fichier)==false)
							throw new FileException('failed to delete file ('.$fichier.')');
					}
				}
			}
			
			if (rmdir($tmp_dir)===false)
				throw new DirectoryException('failed to delete directory ('.$tmp_dir.')');
		}
		
//		foreach (list_directory($output_tmp_dir) as $fichier_sortie)
//		{
//			copy($fichier_sortie,$this->getFileOutPath().'/'.basename($fichier_sortie));
//			unlink($fichier_sortie);
//		}
        
        // Constitution fichier XML de sortie
        file_put_contents($this->getFileOutPath(), '<?xml version="1.0" encoding="UTF-8"?>'.chr(10).'<pages>');
		$count_page = 0;
		
		if(!isset($skip_tesseract) || (isset($skip_tesseract) && !$skip_tesseract)){
			foreach (scandir($output_tmp_dir) as $idx => $fichier_sortie)
			{
				if(substr($fichier_sortie,0,1)!='.'){
					$count_page++;
					//copy($fichier_sortie,$this->getFileOutPath().'/'.basename($fichier_sortie));
					$page=file_get_contents($output_tmp_dir.$fichier_sortie);
					if(!empty($page)){
						$xml='<page n="'.($count_page).'">'.strtr(stripControlCharacters($page),array('<'=>'&lt;', '>'=>'&gt;','&'=>'&amp;' )).'</page>';
						file_put_contents($this->getFileOutPath(), $xml,FILE_APPEND);
					}
					unlink($output_tmp_dir.$fichier_sortie);
				}
			}
			
		}else{
			$page=file_get_contents($output_tmp_dir."/extract.txt");
			$xml='<page n="1">'.strtr(stripControlCharacters($page),array('<'=>'&lt;', '>'=>'&gt;','&'=>'&amp;' )).'</page>';
			file_put_contents($this->getFileOutPath(), $xml,FILE_APPEND);
			unlink($output_tmp_dir."/extract.txt");
		}
		
		file_put_contents($this->getFileOutPath(), '</pages>',FILE_APPEND);
		
        if (rmdir($output_tmp_dir)===false)
        throw new DirectoryException('failed to delete directory ('.$output_tmp_dir.')');

		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
