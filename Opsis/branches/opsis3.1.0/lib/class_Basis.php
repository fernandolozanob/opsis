<?php
    
    require_once(libDir.'fonctionsGeneral.php');
    
    class Basis
    {
        //private $server;
        protected $sql;
        
        protected $nb_lignes;
        protected $page;
        protected $highlight;
        
        protected $xml_retour_requete;
        
        protected $result_sql;
        protected $nb_lignes_sql;
        
        protected $database;
        
        // si l'url est vide utiliser une constante ?
        public function __construct($nom_db='')
        {
            $this->sql='';
            
            $this->nb_lignes=10; // bof bof la valeur a 10 par defaut
            $this->page=1;
            $this->highlight=false;
            
            $this->xml_retour_requete='';
            
            $this->result_sql=array();
            $this->nb_lignes_sql=0;
            
            $this->setDb($nom_db);
        }
        
        public function setDb($nom_db='')
        {
            $this->database='';
            
            if (!empty($nom_db))
                $this->database=$nom_db;
            else if (defined('kBaseBasis'))
                $this->database=kBaseBasis;
        }
        
        /*
         arguments Fonctionnement servlet Jdbc : 
         page
         nb_lignes -> fixe a 10 ca m'embete
         highlight
         host
         query
         db
         
         => ajouter getters et setters sur ces parametres (peut etre pas pour tous)
         */
        
        public function select($base,$champs,$where='',$group_by='',$order_by='',$nb_ligne=10,$page=1,$highlight=false)
        {
            if (!empty($base) && !empty($champs) && is_array($champs))
            {
                $this->sql='SELECT '.implode(',',$champs).' FROM '.$base;
                
                if (!empty($this->nb_lignes))
                    $this->nb_lignes=intval($nb_ligne);
                else
                    $this->nb_lignes=10;
				
                if (!empty($this->page))
                    $this->page=intval($page);
                else
                    $this->page=1;
                
                if (!empty($highlight) && $highlight===true)
                    $this->highlight=true;
                else
                    $this->highlight=false;
                
                
                if (!empty($where))
                {
                    $this->sql.=' WHERE '.$where;
                }
                
                if (!empty($group_by))
                {
                    $this->sql.=' GROUP BY '.$group_by;
                }
                
                if (!empty($order_by))
                {
                    $this->sql.=' ORDER BY '.$order_by;
                }
                
                $this->Execute();
                $this->parseResults();
                return $this->result_sql;
                
            }
            else
                throw new BasisException('base et champs vides');
        }
        
        public function browseIndex($index,$from='',$nb_ligne=10,$page=1,$highlight=false)
        {
            if (!empty($index))
            {
                $this->sql='BROWSE INDEX OF '.$index;
                
                if (!empty($from))
                    $this->sql.=' FROM '.$from;
                
                if (!empty($this->nb_lignes))
                    $this->nb_lignes=intval($nb_ligne);
                else
                    $this->nb_lignes=10;
				
                if (!empty($this->page))
                    $this->page=intval($page);
                else
                    $this->page=-1;
                
                if (!empty($highlight) && $highlight===true)
                    $this->highlight=true;
                else
                    $this->highlight=false;
                
                $this->Execute();
                $this->parseResults();
                return $this->result_sql;
            }
            else
                throw new BasisException('base et champs vides');
        }
        
        public function getNbLignes()
        {
            return $this->nb_lignes_sql;
        }
        
        public function Execute($sql='', $nb_lignes=10, $num_page=1, $highlight=false)
        {
            if (!empty($sql))
            {
                if ($highlight)
                    $str_highlight='true';
                else
                    $str_highlight='false';
            }
            else
            {
                // utilisation du prgm java
                if ($this->highlight)
                    $str_highlight='true';
                else
                    $str_highlight='false';
                
                $nb_lignes=$this->nb_lignes;
                $num_page=$this->page;
                
                $sql=$this->sql;
            }
            
            if (!defined('kJavaBasis'))
                throw new BasisException('kJavaBasis non defini');
            
            if (!defined('kConfigJavaBasis'))
                throw new BasisException('kConfigJavaBasis non defini');
            
            // on verifie qu'on a bien choisi une base de donnees
            if (empty($this->database))
                throw new BasisException('pas de base donnees selectionee');
            
            //java "-Dfile.encoding=UTF-8" -classpath basisjdbc.jar
            $this->xml_retour_requete='';
            
            $descriptors=array
            (
             0=>array('pipe','r'),
             1=>array('pipe','w'),
             2=>array('pipe','w')
             );
            $pipes=array();
            
			$locale = 'fr_FR.UTF-8';
			setlocale(LC_ALL, $locale);
			putenv('LC_ALL='.$locale);
			
            $cmd=kJavaBasis.' ConnecteurJdbc conf='.kConfigJavaBasis.' db='.$this->database.' query="'.str_replace('"','\"',$sql).'" nblignes='.$nb_lignes.' page='.$num_page.' highlight='.$str_highlight.'';
            trace($cmd);
            $proc=proc_open($cmd,$descriptors,$pipes);
            
            $this->xml_retour_requete=stream_get_contents($pipes[1]);
            $stderr=trim(stream_get_contents($pipes[2]));
            fclose($pipes[1]);
            fclose($pipes[2]);
            proc_close($proc);
            
            if (!empty($stderr))
                throw new BasisException('Erreur SQL : '.$stderr);
            //$this->result_sql=''.$this->sql;
            
            return $this->xml_retour_requete;
        }
        
        private function parseResults()
        {
            // parser le XML en array
            $this->result_sql=xml2tab($this->xml_retour_requete);
            $this->nb_lignes_sql=$this->result_sql['DATAS'][0]['DATA'][0]['NB_LIGNES'];
            $this->result_sql=$this->result_sql['DATAS'][0]['DATA'][0]['ROW'];
        }
    }
    
    class BasisException extends Exception
    {
        public function __construct($message='',$code=0,Exception $previous=null)
        {
            parent::__construct('BasisException : '.$message,$code,$previous);
        }
    }
    
    ?>