<?php
    require_once(libDir."class_user.php");
    require_once(libDir."browser_detection.php");


class Export{

	var $entity; // entité à exporter
	var $entity_xml;// entité telle que représentée dans le xml export opsis (entity doc utilise entity_xml t_doc)
	var $xsl;
	var $nb_rows_max;
	var $ext;
	var $filename_out;
	var $type_out;
	var $hasHeader;
	var $output_file;
	var $output_path;
	var $encodage;
	var $ids_objets = null;
	var $use_fop = false ;
	var $fop_prms  =null;
	var $prms_xsl  =array();
	var $options  =array();  // options diverses
	var $id_lang ;  // options diverses
	var $xml_fields  = null ;

	function __construct($entity,$encodage="UTF-8",$xsl="",$nb_max=100){
		$this->entity = $entity;
		$this->xsl = $xsl;
		$this->nb_rows_max = $nb_max;
		$this->hasHeader = 1;
		$this->fop_prms = new stdClass();

		$this->encodage = $encodage;
		$this->id_lang = $_SESSION['langue'];
		$this->initSessionStateOfExport();
        $this->root="data";
	}



	function setIdsObjects($ids) {
		// fonction permettant de tester si les ids passés en input sont bien des entiers
		function validate_int($i){
			if(is_numeric($i)){
				$i = (int)$i;
			}
			if(is_int($i)){
				return true;
			}else{
				return false ;
			}
		}
		if(is_array($ids)){
			$validation = array_map(validate_int,$ids);
			foreach($ids as $idx=> $id){
				if($validation[$idx]){
					$this->ids_objets[] =$id;
				}
			}
			if(!empty($this->ids_objets)){
				return true ;
			}else{
				return false ;
			}
		}else if(!is_array($ids) && validate_int($ids)){
			$this->ids_objets = array($ids);
			return true ;
		}else{
			return false ;
		}
	}

	function initExport($type,$filename="opsis_export",$ext=null, $output_path="", $root=""){
		//trace("export->initExport(".$type.",".$filename.",".$ext);
		$this->ext = strtolower($ext);
		$this->filename_out = $filename;
		$this->hasHeader = 1;
		$this->type_out = $type;
        if(!empty($root))
            $this->root = $root;

		switch($type){
			case "download" :
				ob_clean();
				switch($this->ext){
					case "csv":
						header('Content-Type: text/csv');
					break;
					case "":
					case "0":
					case "xml":
						$this->ext = 'xml';
						header('Content-Type: text/xml');
					break;
					case "xls":
						header('Content-Type: application/vnd.ms-excel');
					break;
					case "xslx" :
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					break;
					case "pdf" :
						$this->use_fop = true;
						$this->initFop();
						header('Content-Type: application/pdf');
					break;
					case "doc":
						header('Content-Type: application/msword;charset=UTF-8');
					break;
					case "txt" :
						 header('Content-Type: text/plain');
					default:
					break;
				}

				header('Content-Description: File Transfer');
				header('Content-Disposition: attachment; filename='.$filename.'.'.$ext);
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				if($this->ext == '' || $this->ext == '0'){
					$this->ext = 'xml';
				}
				if($this->ext =="xml"){
					echo "<?xml version=\"1.0\" encoding=\"".$this->encodage."\"?>\n";
                    echo "<".$this->root.">\n";
				}
			break;
			case "file" :
                if(empty($output_path)){
                    $this->output_path = klivraisonRepertoire."/".microtime();
                    mkdir($this->output_path);
                }else{
                    $this->output_path=$output_path;
                    if(!is_dir($this->output_path)){
                        mkdir($this->output_path);
                    }
                }
				$this->output_file = fopen($this->output_path."/".$filename.".".$ext,'w');
				if($this->ext == '' || $this->ext == '0'){
					$this->ext = 'xml';
				}
				if($this->ext =="xml"){
					fwrite($this->output_file,"<?xml version=\"1.0\" encoding=\"".$this->encodage."\"?>\n");
					fwrite($this->output_file,"<".$this->root.">\n");
				}

			break;
			case "screen" :
			default : // sortie écran
			$this->type_out = "screen";
			
			switch($this->ext){
				case "":
				case "0":
				case "xml":
					$this->ext = 'xml';
					header('Content-Type: text/xml');
					if($this->ext =="xml"){
						echo "<?xml version=\"1.0\" encoding=\"".$this->encodage."\"?>\n";
						echo "<".$this->root.">\n";
					}
					break;
				default:
					break;
			}
			break;
		}


	}

	function getLastSearchSQL () { //&$rows
		//trace("export->getLastSearchSQL entity=".$this->entity);
		if(empty($this->entity)){ exit ; }
		if($this->entity == "doc" && defined('useSolr') && useSolr){
			$sql = $_SESSION['recherche_'.strtoupper($this->entity)."_Solr"]['sql'];

			$existing_sort = $sql->getSortFields();
			if(!empty($existing_sort)){
				foreach($existing_sort as $sort){
					$sql->removeSortField(trim(str_replace(array('asc', 'desc'), '', $sort)));
				}
			}

			foreach ($_SESSION['recherche_DOC_Solr']["tri"] as $champ_tri){
				$sql->addSortField($champ_tri[0],$champ_tri[1]);
			}
			if(isset($_SESSION['recherche_DOC_Solr']['rows'])){
				$this->max_search_results = $_SESSION['recherche_DOC_Solr']['rows'];
			}


		}elseif($this->entity == "stat"){
			$sql = $_SESSION['stat_sql']['sql'];
			$tri = $_SESSION['stat_sql']['tri'];
			$sql.= $tri;
		}else{
			$entity = $this->entity;
			if(stripos($entity,"usager") !==false){
				$entity = "USR";
			}
			elseif(stripos($entity,"fonds") !==false){
				$entity = "FON";
			}
			elseif(stripos($entity,"groupe") !==false){
				$entity = "GRP";
			}
            elseif(stripos($entity,"exploitation") !==false){
                $entity = "EXP";
            }
            
            
			$sql = $_SESSION['recherche_'.strtoupper($entity)]['sql'];
			$refine = $_SESSION['recherche_'.strtoupper($entity)]['refine'];
			$tri = $_SESSION['recherche_'.strtoupper($entity)]['tri'];
			if(isset($_SESSION['recherche_'.strtoupper($entity)]['val_rows'])){
				$this->max_search_results = $_SESSION['recherche_'.strtoupper($entity)]['val_rows'];
			}

			$sql.= $refine.$tri;
		}
		return $sql;
}







	function exportXMLList($sql,$nb_offset=0,$nb="all",$mode=null){
		// trace("export->exportXMLList");
		if($mode == null || !is_array($mode)){
			$mode = $this->mode;
		}

		switch ($this->entity)
			{
			case 'doc_reportage' :
				// spécifique cas reportage
				// ne devrait être appelé que pour un export unitaire de la fiche du reportage. Si le reportage est au milieu d'un résultat de recherche ou d'un panier, on n'exporte que le document
				$this->entity_xml = "t_doc";
				require_once(modelDir.'model_doc.php');
				$mon_objet = new Doc();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					global $db;
					$sql = "SELECT distinct ID_DOC from t_doc LEFT JOIN t_doc_lien ON t_doc_lien.ID_DOC_SRC = t_doc.ID_DOC WHERE ID_DOC_DST in (".implode(',',$this->ids_objets).")";
					$ids = $db->getCol($sql);
					$this->ids_objets=  array_merge($this->ids_objets, $ids);
					// on redéfini la requete sql avec les ids des documents du reportage, on ajoute un critère de tri complexe qui permet de conserver l'ordre défini dans l'array ids_objets
					// => le tri devrait permettre au document reportage de toujours ressortir avant ses fils
					$sql = "SELECT * from t_doc WHERE id_doc in (".implode(',',$this->ids_objets).") AND id_lang='".$this->id_lang."' order by position(','||id_doc::text||',' in ',".implode(',',$this->ids_objets).",')";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}

				}
			// break;

			case "doc":
				$this->entity_xml = "t_doc";
				require_once(modelDir.'model_doc.php');
				$mon_objet = new Doc();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					//MS - 12.04.16 - ajout du préfixe t1 à t_doc, sinon causait des bugs lors de l'export unitaire d'un document
					$sql = "SELECT * from t_doc t1 WHERE t1.id_doc in (".implode(',',$this->ids_objets).") AND t1.id_lang='".$this->id_lang."'";

					// MS - récupération du tri de la requete principale même si on a des ids_objets définis
					// => l'idée est que les ids_objets définis doivent venir de la recherche principale & donc que meme si on a une liste d'ids définis par l'user on veut si possible conserver l'ordre de présentation de la recherche principale
					// => pour l'instant la seule version faite est celle de Solr, voir si necessaire pour pgsql ...
					if(defined("useSolr") && isset($_SESSION['recherche_DOC_Solr']['tri']) && !empty($_SESSION['recherche_DOC_Solr']['tri'])){
						$i_addTri=0;
						foreach($_SESSION['recherche_DOC_Solr']['tri'] as $arr_tri){
							if(isset($arr_tri[0]) && isset($arr_tri[1]) &&( strpos($arr_tri[0],'id_doc')===0 || strpos($arr_tri[0],'doc')===0|| strpos($arr_tri[0],'sort_doc')===0)){
								$i_addTri++;
								$arr_tri[0] = str_replace("sort_doc","doc",$arr_tri[0]);
								$field = $arr_tri[0];
								$direction = "";
								if($arr_tri[1] == 1){
									$direction = "desc";
								}else if ($arr_tri[1] == 0 ){
									$direction = "asc";
								}
								
								//	bug constaté, ajout de $i_addTri Pour éviter 2xORDER	(ex: SELECT * from t_doc t1 WHERE t1.id_doc in (31838) AND t1.id_lang='FR' order by doc_date desc order by doc_cote desc)
								if ($i_addTri==1){
									$sql.= ' order by '.$field." ".$direction;
								}else{
									$sql.= ','.$field." ".$direction;
								}
							}
						}
					}
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;

			case "personne" :
			case "pers" :
				require_once(modelDir.'model_personne.php');
				$mon_objet = new Personne();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					$sql = "SELECT * from t_personne P WHERE P.id_pers in (".implode(',',$this->ids_objets).") AND P.id_lang='".$this->id_lang."'";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;

            case "exp":
                require_once(modelDir.'model_exploitation.php');
                $mon_objet = new Exploitation();
                if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
                    $sql = "SELECT * from t_exploitation E WHERE E.id_exp in (".implode(',',$this->ids_objets).")";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
                }
                break;
                    
            case "fonds":
				require_once(modelDir.'model_fonds.php');
				$mon_objet = new Fonds();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					$sql = "SELECT * from t_fonds f WHERE f.id_fonds in (".implode(',',$this->ids_objets).")";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;

			case "groupe":
				require_once(modelDir.'model_groupe.php');
				$mon_objet = New Groupe();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					$sql = "SELECT * from t_groupe g WHERE g.id_groupe in (".implode(',',$this->ids_objets).")";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;

			case "imageur":
				require_once(modelDir.'model_imageur.php');
				$mon_objet = New Imageur();
				break;

			case "lex":
				require_once(modelDir.'model_lexique.php');
				$mon_objet = new Lexique();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					$sql = "SELECT * from t_lexique L WHERE L.id_lex in (".implode(',',$this->ids_objets).") AND L.id_lang='".$this->id_lang."'";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;

			case "materiel":
			case "mat":
				require_once(modelDir.'model_materiel.php');
				$mon_objet = New Materiel();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					$sql = "SELECT * from t_mat M WHERE M.id_mat in (".implode(',',$this->ids_objets).") ";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;

			// VP 30/12/08 : ajout cas docmat
			case "docmat":
				require_once(modelDir.'model_docMat.php');
				$mon_objet = New DocMat();
                if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
                    $sql = "SELECT * from t_doc_mat DM WHERE DM.id_doc_mat in (".implode(',',$this->ids_objets).") ";
                }
                break;

			case "usager":
				require_once(modelDir.'model_usager.php');
				$mon_objet = New Usager();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					$sql = "SELECT * from t_usager u WHERE u.id_usager in (".implode(',',$this->ids_objets).")";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;

			case "val" :
				require_once(modelDir."model_val.php");
				$mon_objet = new Valeur ();
				if (empty($sql) && $this->ids_objets != null && !empty($this->ids_objets)){
					$sql = "SELECT * from t_val v WHERE v.id_val in (".implode(',',$this->ids_objets).") AND v.id_lang='".$this->id_lang."'";
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
				break;


			case "panier":
				require_once(modelDir.'model_panier.php');
				$mon_objet = New Panier();
				if ($this->ids_objets != null){
					$mon_objet->t_panier['ID_PANIER'] = $this->ids_objets;
					if($this->hasHeader){
						$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
					}
				}
			   break;

			case "panierdoc":

				require_once(modelDir.'model_doc.php');

				$mon_objet = new Doc();
				if(empty($sql) && isset($this->ids_objets)){
					if(!empty($this->options) && isset($this->options['fromPanId']) && $this->options['fromPanId']){
						$sql="select t1.*,tp1.* FROM t_panier_doc tp1 left join t_doc t1 on tp1.id_doc=t1.id_doc WHERE tp1.id_panier in (".implode(',',$this->ids_objets).") and t1.id_lang='".$this->id_lang."'";
						
						global $db ; 
						$count_pdoc = $db->GetOne("SELECT count(*) from t_panier_doc where id_panier in (".implode(',',$this->ids_objets).")");
						if($this->hasHeader){
							$this->setSessionStepsOfExport(	ceil($count_pdoc /$this->nb_rows_max));
						}
					}else{
						$sql="select t1.*,tp1.* FROM t_panier_doc tp1 left join t_doc t1 on tp1.id_doc=t1.id_doc WHERE tp1.id_ligne_panier in (".implode(',',$this->ids_objets).") and t1.id_lang='".$this->id_lang."'";
						if($this->hasHeader){
							$this->setSessionStepsOfExport(	ceil(count($this->ids_objets)/$this->nb_rows_max));
						}
					}
					if (isset($_SESSION['tri_panier_doc']) && !empty($_SESSION['tri_panier_doc'])){
						$sql.=$_SESSION['tri_panier_doc'];
					}else{
						$sql.=" ORDER BY PDOC_ORDRE,ID_LIGNE_PANIER ASC";
					}

				}
				$mode = "pdoc";

				break;

			case "festival":
				require_once(modelDir.'model_festival.php');
				$mon_objet = New Festival();
				break;

			case "stat":
				require_once(libDir."class_stats.php");
				$mon_objet = New Stats();
				break;

		}

		if($this->ext == "csv"){
			$search[] = '"';
			$replace[] = '""';
		}
		
		if(!isset($mon_objet))
			return false;
		else
			return $mon_objet->getXMLList($sql,$nb_offset,$nb,$mode,$search,$replace);
	}

	/*function applyXSL($xml,$xsl){
		//trace("export->applyXSL");
	}*/

	function processXML($content,$charset,$xmlfile=false){
		//trace("export->processXML(".$content.','.$charset.','.print_r($xmlfile,true).')');
		$filename=$this->filename_out;
		$ext = $this->ext;
		$xml = new DOMDocument('1.0',$charset);

		if($xmlfile){
			$filename  .= '.'.$ext;
			$valid = $xml->load($content);
			if(!$valid){
				$this->errorStateOfExport();
				trace("crash loading XML (mal formé)");
				throw new Exception("crash loading XML (mal formé)");
			}
		}else{
			if($ext == "xml" || empty($ext)){
				$filename  .= '.xml';
				$mime_type = 'text/xml';
				$contentExport=$content;
			}else{
				$filename  .= '.'.$ext;
				//PC 23/11/10 : ajout export format html
				if ($ext=="xls")
					$mime_type = 'text/html';
				else
					$mime_type = 'text/plain';

				 if ($charset && $charset=='UTF-8') $os=''; else $os=browser_detection('os');

				// VP 1/12/2011 : Attribution encodage si non précisé
				if(empty($charset)){
					if (($os == 'win' || $os=='nt')) $charset="ISO-8859-1";
					elseif($os=='mac') $charset="MacRoman";
				}
				if ($charset=="ISO-8859-1") { //format Windows
					ob_start();
					eval("?>". str_replace(array(" ' "),array("'"),$content)."<?");
					$contentExport=ob_get_contents();
					$size=ob_get_length();
					ob_end_clean();
					// VP 25/05/10 : Conversion des caractères qui n'existent pas en ISO-8859-1 : ’ œ
					// VP 1/12/2011 : encodage marques paragraphe Windows
					$contentExport=str_replace(array("’","œ","\n"),array("'","oe","\r\n"),$contentExport);
					$contentExport = mb_convert_encoding($contentExport,"ISO-8859-1","UTF-8");
					// } elseif ($os=='mac') { //format Mac pour Excel:MAC
				} elseif ($charset=="MacRoman") { //format Mac
					ob_start();
					eval("?>". str_replace(array(" ' "),array("'"),$content)."<?");
					$contentExport=ob_get_contents();
					$size=ob_get_length();
					ob_end_clean();
					$contentExport = utf8_to_MacRoman($contentExport);
				}
			}
            $xml->loadXML($contentExport);
		}

		if(!empty($this->xsl) ){
			$xsl_file = new DOMDocument();
			$xsl_file->load(getSiteFile("designDir","print/".$this->xsl.".xsl"));
			error_reporting(E_ALL);
			ob_start();
			$proc = new XSLTProcessor;
			$proc->setParameter ('','hasheader',$this->hasHeader);
			if(isset($this->cust_xml_path) && !empty($this->cust_xml_path)){
				$proc->setParameter('','custom_fields',$this->cust_xml_path);
			}
			if ($this->prms_xsl) {
				foreach ($this->prms_xsl as $param => $value) {
					$proc->setParameter("", $param, $value);
				}
			}
			$proc->importStyleSheet($xsl_file);
			$out = $proc->transformToXML($xml);
			$err=ob_get_clean();

            error_reporting(0);
			//trace("err:".$err);
			if(!$out){
				trace("err:".$err);
			}
		}else{
			// MS - A une epoque on différenciait en fonction du type d'export, mais en réalité un export screen sans XSL est toujours un XML, pas de raison de le sauver avec autre chose que saveXML
			// par ailleurs saveHTML réalise un htmlentities sur les champs, ce qui peut causer des crash JS coté reception de ce XML 
			$out = $xml->saveXML($xml->getElementsByTagName('EXPORT_OPSIS')->item(0));
			// $out = $xml->saveHTML(); // désactivé depuis 20.06.2016 - réalisait un htmlentities non souhaité. n'était appliqué que dans le cas type=screen et pas de XSL
		}

		if( $this->type_out!="screen" && $charset == "ISO-8859-1"){ // Pas de utf8_decode pour les sortie écran (charset UTF8 dans le xml et sur les pages)
			$out = utf8_decode($out);
		}
		elseif($ext == "xls" && $this->type_out!="screen"){
			$out = utf8_decode($out);
		}

		return $out;

	}




	function display($content){
		//trace("export->display");
		eval("?>". $contentExport. "<?");
	}

	// fonction qui lance le process d'export
	// MS 13.08.15 - nombreuses modifications pour avoir la possibilité de segmenter un export, meme si il s'agit d'un export d'une page d'un resultat de recherche
	// principalement => le calcul de l'offset par rapport à la page & au nb de document par page est fait dans cette fonction au lieu d'être calculé dans la fonction getXMLList (objet cible export)
	function renderExport($sql='',$page=1,$nb="all",$mode=null){
		//trace("export->renderExport(".$sql.",".$page.",".$nb.",".$mode);
		if($nb == "all"){
			$nb_offset = 0;
		}else{
			$nb_offset = ($page - 1) * $nb;
		}

		// cas 1 : on a un (des) id(s) objet(s) définis, on va donc aller extraire en one shot les ids associés
		// => pas de découpage en chunk, il semble assez improbable d'avoir une liste d'ids dépassant le chunksize classique de 100 documents par passe de traitement
		if(empty($sql) && isset($this->ids_objets) && !empty($this->ids_objets)){
			// $this->setSessionStepsOfExport(1);
			while(($xml_path = $this->exportXMLList("",$nb_offset,$this->nb_rows_max,$mode))){
				$this->nextStepStateOfExport();
				if($this->type_out == "file" || $this->use_fop){
					ob_start();
					$processed = $this->processXML($xml_path,$this->encodage,true);
					$processed = str_replace(array('&lt;','<?xpacket','&gt;'),array('<','&lt;?xpacket','>'),$processed);
					eval('?>'.$processed.'<?');
					
					fwrite($this->output_file,ob_get_clean());
				}else{
					$processed = $this->processXML($xml_path,$this->encodage,true);
					$processed = str_replace(array('&lt;','<?xpacket','&gt;'),array('<','&lt;?xpacket','>'),$processed);
					eval('?>'.$processed.'<?');
				}
				$this->hasHeader = 0;
				$nb_offset = $nb_offset+$this->nb_rows_max;
				
				if(file_exists($xml_path)){
					unlink($xml_path);
				}
			}

		// cas 2 : on veut un export complet sur une base donnée (où sur un résultat de recherche)
		}else if($nb == "all"){
			// on va donc utiliser le chunksize défini dans nb_rows_max, et boucler sur la requete, pour exporter l'ensemble du résultat de la requête
			$this->setSessionStepsOfExport(	ceil($this->max_search_results/$this->nb_rows_max));
			while(($xml_path = $this->exportXMLList($sql,$nb_offset,$this->nb_rows_max,$mode))){
				$this->nextStepStateOfExport();
				if($this->type_out == "file" || $this->use_fop){
					ob_start();
					$processed = $this->processXML($xml_path,$this->encodage,true);
					$processed = str_replace(array('&lt;','<?xpacket','&gt;'),array('<','&lt;?xpacket','>'),$processed);
					eval('?>'.$processed.'<?');
					fwrite($this->output_file,ob_get_clean());
					
				}else{
					$processed = $this->processXML($xml_path,$this->encodage,true);
					$processed = str_replace(array('&lt;','<?xpacket','&gt;'),array('<','&lt;?xpacket','>'),$processed);
					eval('?>'.$processed.'<?');
				}
				$this->hasHeader = 0;
				$nb_offset = $nb_offset+$this->nb_rows_max;

				if(file_exists($xml_path)){
					unlink($xml_path);
				}
			}
		}else{
			// cas 3 : on veut exporter une partie d'un résultat (de recherche ou de la base),
			// mais dans le cas où la page contient 500+ document , on veut conserver la possibilité de diviser l'export en chunk, on fait donc qqs calculs pr s'assurer de prendre la bonne partie du resultset
			$nb_get = $nb;
			$rest = $nb;
			$nb_offset_tmp = $nb_offset;
			$this->setSessionStepsOfExport(ceil($nb/$this->nb_rows_max));
			do{
				$this->nextStepStateOfExport();
				$nb_get = min($this->nb_rows_max,$rest);
				// trace('nb_get:'.$nb_get." nb_offset_tmp:".$nb_offset_tmp);
				$xml_path = $this->exportXMLList($sql,$nb_offset_tmp,$nb_get,$mode);
				if($this->type_out == "file" || $this->use_fop){
					ob_start();
					$processed = $this->processXML($xml_path,$this->encodage,true);
					$processed = str_replace(array('&lt;','<?xpacket','&gt;'),array('<','&lt;?xpacket','>'),$processed);
					eval('?>'.$processed.'<?');
					fwrite($this->output_file,ob_get_clean());
				}else{
					$processed = $this->processXML($xml_path,$this->encodage,true);
						$processed = str_replace(array('&lt;','<?xpacket','&gt;'),array('<','&lt;?xpacket','>'),$processed);
					eval('?>'.$processed.'<?');
				}
				$this->hasHeader = 0;
				$rest = max(0,($rest - $nb_get));
				$nb_offset_tmp = $nb_offset_tmp + $nb_get ;
				if(file_exists($xml_path)){
					unlink($xml_path);
				}
			}while ($rest > 0 );

		}

		//trace("/fin renderExport");

		if($this->type_out == "file" || $this->use_fop){
            if($this->ext =="xml"){
            	$aTmp = explode(" ", $this->root);
            	$root_whithout_attributes = $aTmp[0];
                fwrite($this->output_file,"</".$root_whithout_attributes.">\n");
            }
			if ($this->use_fop){
				$this->nextStepStateOfExport(1,'fop');
				$this->applyFop();
			}else if($this->type_out == 'file'){
				fclose($this->output_file);
				$this->nextStepStateOfExport();
				return $this->output_path;

			}
		} else {
            if($this->ext =="xml"){
            	$aTmp = explode(" ", $this->root);
            	$root_whithout_attributes = $aTmp[0];
                echo "</".$root_whithout_attributes.">\n";
            }
			$this->nextStepStateOfExport();
			exit;
		}

		if(isset($this->cust_xml_path) && !empty($this->cust_xml_path) && file_exists($this->cust_xml_path)){
			unlink($this->cust_xml_path);
		}
	}


	// dans le cas d'un export pdf vers fop, on appelera cette fonction de manière à initialiser l'ensemble des variables relatives au traitement FOP
	// puis on lancera la loop d'export xml / xsl classique, mais on l'utilisera pour générer le fichier fo
	// une fois le fichier fo & la boucle passée, il faudra appeler l'autre fonction fop "applyFop"
	function initFop(){
		// trace("export->initFop");
		/*  B.RAVI 2015/03/18
		 * Pour générer le pdf via fop nous devons utiliser une ligne de commande, nous ne pouvons malheureusement pas passer
		 * de variable PHP à cette ligne de commande, seulement des adresses de fichier, nous créons donc un fichier temporaire
		 * sur le serveur (qu'on delete juste après l'affichage du pdf)
		 *
		 *  La ligne de commande accepte un fichier xml (voir ligne ci-dessous), nous aurions pu donc juste créer un xml temporaire
		 *  fop -xml foo.xml -xsl foo.xsl -pdf foo.pdf
		 *
		 *  Mais nous préférons créer le .fo (pour economiser les ressources du programme executant la ligne de commande)
		 */

		// Ne pas utiliser $fopTemporaryFolder=sys_get_temp_dir()(renvoie /tmp) car pb (pour unlink dedans) et trop limité en espace
		if (!defined('kServerTempDir')) {
		   die('Veuillez contacter le service technique (ERREUR : fop001)');
		}

		$this->fop_prms->fopTemporaryFolder=kServerTempDir;

		if (substr($this->fop_prms->fopTemporaryFolder, -1)!=='/'){
			$this->fop_prms->fopTemporaryFolder=$this->fop_prms->fopTemporaryFolder.'/';
		}

		if (!is_dir($this->fop_prms->fopTemporaryFolder)) {
			die('Veuillez contacter le service technique (ERREUR : fop002)');
		}

		/*$this->fop_prms->fop_id_user=(int)$_SESSION['USER']['ID_USAGER'];
		if (empty($this->fop_prms->fop_id_user)){// car il peut y avoir un session_id() même si l'utilisateur n'est pas connecté
			 die('Vous devez être connecté pour utiliser ce service');
		}*/


		$this->fop_prms->fop_id_session=trim(session_id());
		if (!empty($this->fop_prms->fop_id_session)){
			//on autorise seulement les lettres, chiffres, _ , - (ex: / peut causer des souci causant un tentative de création de épertoire dans kServerTempDir)
			$this->fop_prms->fop_caractereNonautorise='/[^a-z0-9_-]+/i';
			$this->fop_prms->fop_id_user=preg_replace($this->fop_prms->fop_caractereNonautorise,'-',$this->fop_prms->fop_id_session) ;
		}

		// Important de conserver l'unicité du nom du fichier temporaire
		$this->fop_prms->fop_timestamp=time();

		$this->fop_prms->fop_temporaryFileName=$this->fop_prms->fopTemporaryFolder.$this->fop_prms->fop_id_user.'_'.$this->fop_prms->fop_timestamp.'.fo';

		//rajouter juste &debug_temporaryFileName dans l'url pour connaitre le chemin complet du .fo généré
		// if ( isset($_REQUEST['debug_temporaryFileName'])){
		// 	die('Debug $fop_temporaryFileName : '.$fop_temporaryFileName);
		// }
		$this->fop_prms->fop_TemporaryFo = fopen($this->fop_prms->fop_temporaryFileName, "w") ;

		if (!$this->fop_prms->fop_TemporaryFo){
			die('Veuillez contacter le service technique (ERREUR : fop003 )');
		}

		// MSEXP
		// on recopie les infos pertinentes dans output_path / output_file pour simplicité d'acces / mutualisation du code dans le reste des fonctions
		$this->output_path = $this->fop_prms->fopTemporaryFolder;
		$this->output_file = $this->fop_prms->fop_TemporaryFo;
	}

	// cette fonction sera appelée sur le fichier .fo généré par le process standard xml/xsl de façon à génerer le pdf
	function applyFop(){
		//trace("export->applyFop");
		// MS - le fo généré devrait avoir ses header (définition des template master / page etc ...)
		// mais il lui manque la fermeture de ses tags. plutot que de detecter quelle est la derniere écriture dans fo pour l'ajouter à ce moment la, on referme les tags ici dans applyFop


		fwrite($this->output_file,' <fo:block id="last-page"/></fo:flow></fo:page-sequence></fo:root>');
		fclose($this->output_file);
		$this->fop_prms->fop_cmd = kFopPath . ' '.$this->fop_prms->fop_temporaryFileName.' -pdf -';
		ob_start();
		$pdf=system($this->fop_prms->fop_cmd, $retval);//important, system envoie déja vers la sortie buffer (comme un print, echo...)
		$this->nextStepStateOfExport();
		//trace("export::fop_cmd : ".$this->fop_prms->fop_cmd);
		if (!$pdf){
		   ob_end_clean(); //pour un meilleur affichage avec juste le message d'erreur de la ligne suivante
		   die('Veuillez contacter le service technique (ERREUR : fop006 )');
		}

		if (file_exists($this->fop_prms->fop_temporaryFileName)){
			unlink($this->fop_prms->fop_temporaryFileName);
			ob_end_flush();
			unset($contentExport);
		}
	}


	function addPrmsXsl($array){
		foreach($array as $key=>$val){
			$this->prms_xsl[$key] = $val ;
		}
	}

	function buildCustomXMLList($arr_fields){
		$this->xml_fields = '<?xml version="1.0" encoding="UTF-8" ?><view>'; // //

		//MSEXP
			switch ($this->entity)
			{
            case "exp":
                $this->entity_xml = "t_exploitation";
                break;
            case "panierdoc":
				$this->entity_xml = "t_doc";
				break;
			case "lex":
				$this->entity_xml = "t_lexique";
				break;
			case "materiel":
			case "mat":
				$this->entity_xml = "t_mat";
				break;
			case "personne":
			case "pers":
				$this->entity_xml = "t_personne";
				break;
			case "docmat":
				$this->entity_xml = "t_doc_mat";
				break;

			default :
				$this->entity_xml = "t_".$this->entity;
				break;
		}
		$this->xml_fields .= "<entity>".$this->entity_xml."</entity>";

		foreach($arr_fields as $field){
			if(isset($field['XML']) && !empty($field['XML']) &&  preg_match('/<\!\[CDATA\[([\s\S]+?)\]\]>/',$field['XML'],$matches)){
				$this->xml_fields .= $matches[1];
			}
		}
		$this->xml_fields .='</view>';

		//trace("built custom xml : ".print_r($this->xml_fields,true));

		$this->cust_xml_path = kServerTempDir."/custom_fields_".session_id().time(true).".xml";
		$ok = file_put_contents($this->cust_xml_path,$this->xml_fields);
		$this->storeCustomXMLSession();
	}




	// fonction d'initialisation du rapport d'éxport stocké en session
	function initSessionStateOfExport(){
		session_start();
		$_SESSION['etat_export']['state']="init";
		$_SESSION['etat_export']['current'] = 0;
		session_write_close();
		session_start();
		// trace("initSessionStateOfExport :".print_r($_SESSION['etat_export'],true));
	}

	// fonction d'initialisation du nombre d'étapes du rapport d'export stocké en session
	function setSessionStepsOfExport($steps){
		session_start();
		// l'utilisation de fop ajoute une étape au process
		if($this->use_fop){
			$steps++ ;
		}
		$_SESSION['etat_export']['steps']=$steps;
		session_write_close() ;
		session_start();
		// trace("setSessionStepsOfExport :".print_r($_SESSION['etat_export'],true));

	}
	// avancement d'une étape du rapport d'export stocké en session
	function nextStepStateOfExport($step=1,$str="running"){
		session_start();
		$_SESSION['etat_export']['state']=$str;
		$_SESSION['etat_export']['current']+=$step;
		if($_SESSION['etat_export']['current']>$_SESSION['etat_export']['steps']){
			$_SESSION['etat_export']['state']="ready";
		}
		session_write_close() ;
		session_start();
		// trace("nextStepStateOfExport :".print_r($_SESSION['etat_export'],true));

	}

	// fonction de passage en état 'erreur' dans le rapport d'export stocké en session
	function errorStateOfExport($str){
		session_start() ;
		$_SESSION['etat_export']['state']="error";
		$_SESSION['etat_export']['error_msg'] = $str ;
		header('HTTP/1.1 500 Internal Server Error');
		session_write_close() ;
		session_start();
	}

	function storeCustomXMLSession(){
		if(isset($this->xml_fields) && !empty($this->xml_fields)){
			session_start();
			$entity = $this->entity;
			if($entity == 'panierdoc'){
				$entity = 'doc';
			}
			$_SESSION['export'][$entity]['lastCustomFields'] = $this->xml_fields ;
			session_write_close();
			session_start();
		}
	}

	// fonction de récupération sous forme xml du rapport d'export stocké en session
	static function getStateOfExport(){
		session_start();
		if(isset($_SESSION['etat_export'])){
			$xml ='<?xml version="1.0" encoding="UTF-8"?><xml>'."\n";
			$xml.='<state>'.$_SESSION['etat_export']['state'].'</state>'."\n";
			$xml.='<current>'.$_SESSION['etat_export']['current'].'</current>'."\n";
			$xml.='<steps>'.$_SESSION['etat_export']['steps'].'</steps>'."\n";
			$xml.='</xml>';
		}
		return $xml;
	}


	// deprecated - fonction servant a récupérer l'export unitaire d'une entité (appel à xml_export)
	// n'a finalement pas été réutilisé tel quel, on préfère passer par getXMLList, meme pr 1 élément.
	function exportXMLObj($id,$id_lang=NULL,$encodage=0,$mode=''){
		//trace("export->exportXMLObj");
		$content="";
		global $param;
		switch ($this->entity)
		{
			case "doc":
				require_once(modelDir.'model_doc.php');
				require_once(modelDir.'model_docAcc.php');
				$mon_objet = new Doc();
				$mon_objet->t_doc['ID_DOC']=$id;

				if ($mode=='light') {
					$mon_objet->getDocFull(1,1,0,1,1);
					 $mon_objet->getDocAcc();
				} else {
					$mon_objet->getDocFull(1,1,1,1,1,1,1,1,1);
					$mon_objet->getDocAcc();
					$mon_objet->getDocLiesDST();
					$mon_objet->getDocLiesSRC();
					$mon_objet->getFestival();
				}
				$content= $mon_objet->xml_export(0,$encodage);
				$param["profil"]=$myUser->Type;

				break;

			case "personne" :
			case "pers" :
				require_once(modelDir.'model_personne.php');
				$mon_objet = new Personne();
				$mon_objet->t_personne['ID_PERS']=$id;
				$mon_objet->getPersonne();
				$mon_objet->getLexique();
				$mon_objet->getValeurs();
				$mon_objet->getDocAcc();
				$mon_objet->getFestival();
				$content= $mon_objet->xml_export(0,$encodage);
				break;

            case "exp":
                require_once(modelDir.'model_exploitation.php');
                
                $mon_objet = new Exploitation();
                $mon_objet->t_exploitation['ID_EXP']=$id;
                $mon_objet->getExp();
                $content= $mon_objet->xml_export(0,$encodage);
                break;
                
            case "fonds":
				require_once(modelDir.'model_fonds.php');

				$mon_objet = new Fonds();
				$mon_objet->t_fonds['ID_FONDS']=$id;
				$mon_objet->getFonds();
				$mon_objet->getGroupeFonds();
				$content= $mon_objet->xml_export(0,$encodage);
				break;

			case "groupe":
				require_once(modelDir.'model_groupe.php');

				$mon_objet = New Groupe();
				$mon_objet->t_groupe['ID_GROUPE']=$id;
				$mon_objet->getGroupe();
				$mon_objet->getGroupeFonds();
				$mon_objet->getUsagerGroupe();
				$content= $mon_objet->xml_export(0,$encodage);
				break;

			case "imageur":
				require_once(modelDir.'model_imageur.php');

				$mon_objet = New Imageur();
				$mon_objet->t_imageur['IMAGEUR']=$id;
				$mon_objet->getImageur();
				$content= $mon_objet->xml_export(0,$encodage,null,true);
				break;

			case "lex":
				require_once(modelDir.'model_lexique.php');

				$mon_objet = new Lexique();
				$mon_objet->t_lexique['ID_LEX']=$id;
				$mon_objet->getLexique();
				$mon_objet->getChildren();
				$mon_objet->getSyno();
				$mon_objet->getSynoPref();
				$mon_objet->getAsso();
				$mon_objet->getVersions();
				$content= $mon_objet->xml_export(0,$encodage);
				break;

			case "materiel":
			case "mat":
				require_once(modelDir.'model_materiel.php');

				$mon_objet = New Materiel();
				$mon_objet->t_mat['ID_MAT']=$id;
				$mon_objet->getMat();
				//PC 02/02/11 : Chargement de toutes les données lors de mode doc
				if ($mode=='doc') {
					$mon_objet->getDocs();
					foreach ($mon_objet->t_doc_mat as &$doc)
						$doc['DOC']->getDocFull(1,1,1,1,1,1,1,1,1);
				}
				else $mon_objet->getDocMat();
				$mon_objet->getValeurs();
				$content= $mon_objet->xml_export(0,$encodage);
				break;

			// VP 30/12/08 : ajout cas docmat
			case "docmat":
				require_once(modelDir.'model_docMat.php');

				$mon_objet = New DocMat();
				$mon_objet->t_doc_mat['ID_DOC_MAT']=$id;
				$mon_objet->getDocMat();
				$mon_objet->getValeurs();
				// VP 19/05/10 : ajout export doc et mat
				$mon_objet->getDoc();
				$mon_objet->getMateriel();
				$mon_objet->t_mat['MAT']->getValeurs();
				$content= $mon_objet->xml_export(0,$encodage);
				break;

			case "usager":
				require_once(modelDir.'model_usager.php');
				$mon_objet = New Usager();
				$mon_objet->t_usager['ID_USAGER']=$id;
				$mon_objet->getUsager();
				$content= $mon_objet->xml_export(0,$encodage);
				break;

			case "val" :
				require_once(modelDir."model_val.php");
				$mon_objet = new Valeur ();
				$mon_objet->t_val['ID_VAL']=$id;
				$mon_objet->getVal();
				$content= $mon_objet->xml_export(0,$encodage);
				break;


			case "panier":
				require_once(modelDir.'model_panier.php');
				$mon_objet = New Panier();
				$mon_objet->t_panier['ID_PANIER']=$id;
				if ($_SESSION['tri_panier_doc']) $mon_objet->sqlOrder=$_SESSION['tri_panier_doc'];
				$mon_objet->getPanier();
				if ($mode=='mat') {
					$mon_objet->getDocsFull(0,0,1,0,0);
				} else {
					$mon_objet->getDocs();
				}
				$content= $mon_objet->xml_export(0,$encodage);
				$content.= $mon_objet->xml_export_panier_doc(0,$encodage);
			   break;

			case "panierdoc":

				require_once(modelDir.'model_doc.php');
				$content='';
				if ($id=='_session') {
					require_once(modelDir.'model_panier_session.php');
					$mon_objet = New PanierSession();
				} else {
					require_once(modelDir.'model_panier.php');
					$mon_objet = New Panier();
				}
				$mon_objet->t_panier['ID_PANIER']=$id;
				if ($_SESSION['tri_panier_doc']) $mon_objet->sqlOrder=$_SESSION['tri_panier_doc'];
				$mon_objet->getPanier();
				foreach ($mon_objet->t_panier_doc as $pdoc) {
					$myDoc= new Doc();
					$myDoc->t_doc['ID_DOC']=$pdoc['ID_DOC'];
					$myDoc->getDocFull(1,1,1,1,1,1);
					$myDoc->getDocAcc();
					$myDoc->getFestival();
					$content.=$myDoc->xml_export(0,$encodage);
					unset($myDoc);
				}
				$content.=$mon_objet->xml_export(0,$encodage);
				//PC 15/10/10 filtre les retour chariot
				$content = str_replace( array( '<br>', '<br />', "\n", "\r" ), array( '', '', '', '' ), $content );
				$param["profil"]=$myUser->Type;

				break;

			case "festival":
				require_once(modelDir.'model_festival.php');
				$content='';
				$mon_objet = New Festival();
				$mon_objet->t_fest['ID_FEST']=$id;
				$mon_objet->getFest();
				$mon_objet->getLexique();
				$mon_objet->getPersonnes();
				$mon_objet->getValeurs();
				$mon_objet->getSections(true);
				$mon_objet->getDocs();

				$mon_objet->getDocAcc();
				$content.=$mon_objet->xml_export(0,$encodage);
				$param["profil"]=$myUser->Type;
				break;

		}
	return $content;
	}

	// deprecated - recopié depuis export.php, servait à faire des exports qui embarquaient les médias des docs concernés => ne fonctionne plus pour l'instant,
	// je garde la fonction pour référence .
	function copyMedia($copie,$xml,&$result){
		$copies=split(",",$copie);
		$myXML=new DOMDocument();
		$myXML->loadXML($xml);
		if (!$myXML) {die(kErrorExportCopieMediaXML);}

		if (in_array('doc_acc',$copies)) {
			$result['DocAcc']=0;
			if (!is_dir(kExportDir."/doc_acc")) mkdir(kExportDir."/doc_acc");
			$arrDAs=$myXML->getElementsByTagName("DA_FICHIER");
			for ($i=0;$i<$arrDAs->length;$i++) {
				$doc_acc=$arrDAs->item($i)->nodeValue;
				if (copy (kDocumentDir.$doc_acc,kExportDir."/doc_acc/".$doc_acc)) $result['DocAcc']++;
			}
		}

		if (in_array('story',$copies)) {
			$result['Imageur']=0;
			$result['Image']=0;
			if (!is_dir(kExportDir."/storyboard")) mkdir(kExportDir."/storyboard");
			$arrImgs=$myXML->getElementsByTagName("IM_FICHIER");
			for ($i=0;$i<$arrImgs->length;$i++) {
				//récup imageur (next->next car juste next renvoie un noeud text)
				$imageur=$arrImgs->item($i)->nextSibling->nextSibling->nodeValue;
				$image=$arrImgs->item($i)->nodeValue;
				if (!is_dir(kExportDir."/storyboard/".$imageur)) {
					if (mkdir(kExportDir."/storyboard/".$imageur)) $result['Imageur']++;
				}
				if (copy (kStoryboardDir.$imageur."/".$image,kExportDir."/storyboard/".$imageur."/".$image)) $result['Image']++;
			}
		}

		if (in_array('mat',$copies)) {
			include_once(modelDir.'model_materiel.php');
			$result['Materiel']=0;
			if (!is_dir(kExportDir."/videos")) mkdir(kExportDir."/videos");
			$arrDAs=$myXML->getElementsByTagName("ID_MAT");
			for ($i=0;$i<$arrDAs->length;$i++) {
				$idmat=$arrDAs->item($i)->nodeValue;
				$path=Materiel::getLieuByIdMat($idmat);
				// on ne copie que des matériels susceptible d'être des fichiers video
				if (in_array(strtolower(getExtension($idmat)),unserialize(gVideoTypes))
				&& copy ($path.$idmat,kExportDir."/videos/".$idmat)) $result['Materiel']++;
			}
		}
	}


}





?>
