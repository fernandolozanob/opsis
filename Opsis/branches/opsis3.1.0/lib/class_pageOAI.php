<?php
include_once(libDir."class_page.php");

class PageOAI extends Page {

private static $instance;


	function __construct () { //contructeur
		$this->prmAction="verb";
		$this->prmPage="page";
		$this->prmSort="tri";
		$this->prmNbLignes="nbLignes";
		$this->page=1;
		$this->titre=""; // titre par défaut
		$this->nomEntite=""; // titre par défaut
		$this->titreSite=gSite; // titre par défaut		$this->initParams4XSL();
		$this->includePath=includeDir;
	}

	/** Singleton : une seule classe Page  en même temps !*/
   public static function getInstance()
   {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
   }



	/** Calcule et renvoie le HTML d'une page
	 * Analyse du template : extraction et traitement des balises spéciales
	 * IN : template (init par méthode SetDesign)
	 * OUT : HTML
	*/
	function render($buffered=true) {
		
		//var_dump($_SERVER);	exit;
		
		
		try {
			if ($_SERVER['REQUEST_METHOD'] == 'GET') {
				$this->args = $_GET;
				$getarr = explode('&', $_SERVER['QUERY_STRING']);
			} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
				$this->args = $_POST;
			} 
						
			$reqattr = '';
			if (is_array($this->args)) {
				foreach ($this->args as $key => $val) {
					$reqattr .= ' '.$key.'="'.htmlspecialchars(stripslashes($val)).'"';
				}
			}


			$MY_URI = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
	
			// Current Date
			$datetime = gmstrftime('%Y-%m-%dT%T');
			$responseDate = $datetime.'Z';
			
			// Header
			$xmlHeader = '<?xml version="1.0" encoding="UTF-8"?>
			<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
			http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">'."\n"
			.' <responseDate>'.$responseDate."</responseDate>\n";

				$request = ' <request'.$reqattr.'>'.$MY_URI."</request>\n";
				$request_err = ' <request>'.$MY_URI."</request>\n";
			// Footer
			$xmlFooter="</OAI-PMH>\n";

		$src=file_get_contents($this->template);

		$regexp='`<php_param name="(.*)" />`siu';
		$src=preg_replace ($regexp,"<?=$1?>",$src); //Remplacement des balises <php_param... par la valeur

		$src=preg_replace ("`/\*.*\*/`siu","",$src);

		$motifbalise='`<include[^>]+/>`siu';
		$html=@preg_split($motifbalise,$src); // Pour le HTML hors balise

		@preg_match_all($motifbalise,$src,$includes,PREG_SET_ORDER); // Pour les balises include

		
		if ($buffered) ob_start();
		///ob_start("ob_gzhandler"); //experimental : compression


		for ($i=0;$i<count($includes);$i++) {
			print($html[$i]);

			$attribs=get_attributes_from_tag($includes[$i][0],array("id","file"));
			
			switch ($attribs["id"][0]) {
					case "content" : $this->dspContent(); break;
					default : print ("Included order not existing"); break;
					}
			}
			print($html[$i]);
		
			if ($buffered) {
				$html=ob_get_contents(); // récupération du buffer
				ob_end_clean(); //vidage du buffer		
				ob_start();
				//Si flux binaire, pas d'eval du code
				if ($this->binary===true) 
					echo $html;
					else eval("?".chr(62).$html.chr(60)."?"); //eval du code pour interpréter le PHP embarqué.
				$html2=ob_get_contents(); // récupération du buffer
				ob_end_clean(); //vidage du buffer	
				
				if ($html2===false) { //erreur lors du rendu => erreur interne
					header("HTTP/1.0 500 Internal Server Error",TRUE,500);
			        header("Status: 500 Internal Server Error",TRUE,500);
			        $_SERVER['REDIRECT_STATUS'] = 500;
					if (!defined('debugMode') || debugMode==false) { //si en prod, on redirige vers une page d'erreur
																		
					}else {
						//debug($html); //sinon on crache le code fautif
					}
				
			}
		//	var_dump($this);
			
		//On a importé les données brutes (include php + interprétation)
		//On va maintenant vérifier d'éventuels headers
			
		//Si on a une date de modif => ajout au header (fichiers + page détails)
			if (isset($this->lastModified)) {
				header("Last-Modified: " . $this->lastModified);
			}
		
		//Requête avec un header if-modified-since + date => on vérifie si le contenu est plus récent,
		//sinon on renvoie un header 304 => la page n'est pas transmise alors
			if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])&& isset($this->lastModified)) {
				$if_modified_since = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
				$lastModified = strtotime($this->lastModified);
				if ($if_modified_since >= $lastModified)
		         {
		        	header("HTTP/1.0 304 Not Modified",TRUE,304);
		         	header("Status: 304 Not Modified",TRUE,304);
		         	$_SERVER['REDIRECT_STATUS'] = 304;
		        	exit;
		         }
			}
				if ($this->encoding=='ISO-8859-1') $html2 = mb_convert_encoding($html2,"ISO-8859-1","UTF-8");
				elseif ($this->encoding=='MAC-ROMAN') $html2 = utf8_to_MacRoman($html2);
			}
		}

		catch (Exception $e) { //si erreur quelconque, throw depuis le fichier include
			$this->dropError($e->GetMessage());
		}

		// Renvoi
		if(empty($this->error_msg)){
			if ($this->binary===true) { //flux binaires => renvoyé tel quel
				return $html2;
			}
			else { //sinon c du xml => ajout des headers & footers
				header("Content-Type: text/xml");
				return $xmlHeader.$request.$html2.$xmlFooter;
			}
		}else{
			if(strpos($this->error_msg, "noRecordsMatch") === false) {
				header("HTTP/1.0 400 Bad Request",TRUE,400);
				header("Status: 400 Bad Request",TRUE,400);
				header("Content-Type: text/xml");
				$_SERVER['REDIRECT_STATUS'] = 400;
			} else {
				header("Content-Type: text/xml");
			}
			
			return $xmlHeader.$request_err.$this->error_msg.$xmlFooter;
		}
	}


	function dropError($code, $argument = '', $value = ''){
	
	switch ($code) {
		case 'badArgument' :
			$text = "The argument '$argument' (value='$value') included in the request is not valid.";
			break;
			
		case 'badGranularity' :
			$text = "The value '$value' of the argument '$argument' is not valid.";
			$code = 'badArgument';
			break;
			
		case 'badResumptionToken' :
			$text = "The resumptionToken '$value' does not exist or has already expired.";
			break;
			
		case 'badRequestMethod' :
			$text = "The request method '$argument' is unknown.";
			$code = 'badVerb';
			break;
			
		case 'badVerb' :
			$text = "The verb '$argument' provided in the request is illegal.";
			break;
			
		case 'cannotDisseminateFormat' :
			$text = "The metadata format '$value' given by $argument is not supported by this repository.";
			break;
			
		case 'exclusiveArgument' :
			$text = 'The usage of resumptionToken as an argument allows no other arguments.';
			$code = 'badArgument';
			break;
			
		case 'idDoesNotExist' :
			$text = "The value '$value' of the identifier is illegal for this repository.";
			break;
			
		case 'missingArgument' :
			$text = "The required argument '$argument' is missing in the request.";
			$code = 'badArgument';
			break;
			
		case 'noRecordsMatch' :
			$text = 'The combination of the given values results in an empty list.';
			break;
			
		case 'noMetadataFormats' :
			$text = 'There are no metadata formats available for the specified item.';
			break;
			
		case 'noVerb' :
			$text = 'The request does not provide any verb.';
			$code = 'badVerb';
			break;
			
		case 'noSetHierarchy' :
			$text = 'This repository does not support sets.';
			break;
			
		case 'sameArgument' :
			$text = 'Do not use them same argument more than once.';
			$code = 'badArgument';
			break;
			
		case 'sameVerb' :
			$text = 'Do not use verb more than once.';
			$code = 'badVerb';
			break;
			
		default:
			$text = "Unknown error: '$code', '$argument', value: '$value'";
			$code = 'badArgument';
	}
	
	$this->error_msg .= " <error code=\"".htmlspecialchars($code, ENT_QUOTES)."\">".htmlspecialchars($text, ENT_QUOTES)."</error>\n";
}


	function getActionFromURL() {
		$this->urlaction=$_REQUEST[$this->prmAction]; //XXX : changé par LD le 10/05/07 pour AJAX type POST
        if(strpos($this->urlaction, '../')!==false) $this->urlaction="";
		return $this->urlaction;
	}



	function getAccess($type) {
		global $adminPages,$docPages,$usagerPages,$loggedPages,$usagerAdvPages;

		switch ($type) {

			case kLoggedAdmin : $pages=$adminPages;break;
			case kLoggedDoc : $pages= $docPages;break;
			case kLoggedNorm : $pages = $usagerPages;break;
			case kLoggedUsagerAdv : $pages = $usagerAdvPages;break;
			case kNotLogged : $pages = $loggedPages;break;
			default : $pages = $loggedPages;
		}
		//trace(strpos($pages,$this->urlaction.".php"));
		//On cherche la page demandée parmi les url interdites (extension php pour être backward compatible avec la v1)
		if (strpos($pages,$this->urlaction.".php")===false) return true; else return false;
	}

	private function dspContent () {

	  	$this->getActionFromURL();
		$user=User::getInstance();

	    if (!empty($this->urlaction) ) {
		    if (file_exists($this->includePath.$this->urlaction.".php")) {
				$incfile=$this->includePath.$this->urlaction.".php";
		    }
	    } 
		//trace("INCFILE=$incfile");
	    if (!empty($incfile)) {
			require($incfile);
		} else { //tentative d'accès à une page qui n'existe pas
			$this->dropError("badVerb", $this->urlaction);
		}
		
	}




	/**
	 * Préparation d'un ordre SQL à partir de l'analyse du paramètre sort dans l'url.
	 * IN : paramètre tri dans le GET, récup ordre par défaut true/false
	 * OUT : SQL (ORDER BY..., direct) et màj des variables order (col), triInvert (sens passé à l'url)
	 * 		et params4XSL (params de base passés aux XSL)
	 */
	function getSort ($withDefault=true,&$default=true,$default_field='') {
		global $db;
        $tri=$this->getSortFromURL();
        
        if($tri!=""){ //Calcul de la colonne de tri, cette valeur sera envoyée à la XSL pour afficher les flèches
            $ordre=substr($tri,-1,1);
            $col = substr($tri,0,-1);
			if (!is_numeric($ordre)) {
				$ordre=0;
				$this->order=$tri;
				$col=$tri;
			}
			if ($ordre==0) {
				$this->triInvert="1";
				$this->col=$col;
			}else{
				$this->triInvert="0";
				$this->col=$col;
			}

			$this->initParams4XSL();

			//Deuxième partie, calcul du SQL de tri qui peut comporter plusieurs colonnes
			$_hasbrackets=preg_match("/^\[(.*)\](.*)$/i",$tri,$matches); //Présence de crochet autour des champs => concaténation
			if ($_hasbrackets) {
				$tri=$matches[1].$matches[2]; //on se débarrasse des crochets (mais on garde le sens)
				$sqlTri=str_replace(';',',',$tri);
				if (substr($sqlTri,-1,1)=='1') {
						$_cols=strtoupper(substr($sqlTri,0,-1));
						$_sens=" DESC "; }
				elseif (substr($sqlTri,-1,1)=='0') {
						$_cols=strtoupper(substr($sqlTri,0,-1));
						$_sens=" ASC "; }
				elseif (!is_numeric(substr($sqlTri,-1,1))) {
						$_cols=strtoupper($sqlTri);
						$_sens=" ASC ";
				}
				if (!empty($_cols)) return " ORDER BY ".$db->Concat($_cols).$_sens;
			} else {
				// VP 19/1/09 : ajout tri multiple
				$sqlTri="";
				$fields=explode(",",$tri);
				foreach ($fields as $field) {
					$field=trim($field);
					$ordre=substr($field,-1,1);
					if (is_numeric($ordre)){
						if($ordre==1){$direction="DESC";}else{$direction="ASC";}
						$fld=substr($field,0,-1);
					}
					else {
						$direction="ASC";
						$fld=$field;
					}
					$sqlTri.=",".strtoupper($fld)." ".$direction;
				}
				if(!empty($sqlTri)) return " ORDER BY ".substr($sqlTri,1);
			}
        } elseif ($withDefault) {
        	if ($_POST['orderbydeft'])
        	$_SESSION['sqlorderbydeft'][$this->urlaction]=$this->addDeftOrder($_POST['orderbydeft']);
        	//by ld 22/12/08: introduction de urlaction pour gérer des ordres par deft différents suivant la page
        	//puisqu'on n'a pas l'objet recherche ici...
			
			return $_SESSION['sqlorderbydeft'][$this->urlaction];
        }
	}



	/**
	 * Initialisation du Pager.
	 * IN : SQL, new_max (opt, nb de lignes max),
	 * 		$altVar = autre mode d'initialisation de la page en cours (ex : session),
	 * 			NOTE: cette variable n'est utile que si le sélecteur du nb de lignes est mis dans la XSL.
	 * 			Si ce sélecteur est appelé dans la page PHP, la var de session est déjà initialisée
	 * 		$addGETVars = variables additionnelles à passer aux liens du Pager
	 * OUT : var de classe : Result (resultat de requete "tronqué"),
	 * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales), PagerLink (url passée de page en page)
	 */
    function initPager($sql, $new_max = 10,$altVar=null,$addGETVars=null,$secstocache=0)
    {
        global $db;
		
        if (!isset($this->nbLignes)) $this->max_rows = $new_max; else $this->max_rows=$this->nbLignes;

       	$this->getPageFromURL();
        if ($this->page=="" && $altVar!=null) $this->page=$altVar;
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) ) $this->page=1;

        if($this->max_rows == "all"){
        	$i=0;
            if ($secstocache==0) $rsfull= $db->Execute($sql); else $rsfull=$db->CacheExecute($secstocache,$sql);

            if (!$rsfull) { //0 résultats retournés !
            	$this->rows=0;
            	$this->found_rows=0;
            	$this->page=1;
            	$this->PagerLink="";
            	$this->result=array();
            	$this->error_msg=kErrorWSRequeteNonValide;
            	return false;
            	}

            $this->max_rows = $rsfull->RecordCount();

            if ($this->max_rows>kMaxRows) $this->max_rows=kMaxRows; // précaution : si trop de lignes, on revient à un mode paginé pour ne pas surcharger le serveur
            else {
            $this->result=$rsfull->GetArray();
            $this->rows =$this->max_rows;
            $this->found_rows=$this->max_rows;
            $this->page=1;
            $rsfull->Close();
            }
        }

		if ($this->max_rows!="all")
		{
				
	        $i=0;
	        $rs_found_rows=$db->PageExecute($sql,$this->max_rows,$this->page,false,$secstocache);
		    if ($rs_found_rows) {
		        $this->found_rows = $db->_maxRecordCount; // Attention : propriété "cachée" de ADODB
		        $i_min = ($this->page - 1) * $this->max_rows;
		        if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
		       	$this->result=$rs_found_rows->GetArray();
		        //$this->result=array_slice($rs_found_rows,$i_min,$this->max_rows);
		        $this->rows =count($this->result);
				$rs_found_rows->Close();

		    } else {$this->found_rows=0;$this->page=1;$this->error_msg=kErrorWSRequeteNonValide;return false;}
		}
        $this->PagerLink=$this->getName()."?urlaction=".$this->getActionFromUrl().$this->addUrlParams($addGETVars)."&page=";
        if ($this->page>$this->num_pages()) $this->page=$this->num_pages();
        $this->params4XSL['nb_pages']=$this->num_pages();
        $this->params4XSL['nb_rows']=$this->found_rows;
        return true;
    }


	/**
	$nomelement : nom de l'�lement dans le xml
	$nomFichierXsl : nom du fichier xsl de transformation
	$options : 1 user, 2 doc, 3 admin
	$page : num�ro de la page

	$ajoutPrivilegeDoc : Faut-il ajouter aux r�sultats le statuts de leurs privil�ges (seulement utile pour la gestion des documents). N.b. Ceci pourrait �tre g�rer de fa�on plus propre dans une classe Doc
	*/
	function afficherListe($nomelement,$nomFichierXsl,$ajoutPrivilegeDoc=FALSE,$arrHighlight=null,$xml2merge='',$eval=false)
	{
	        if ($ajoutPrivilegeDoc){
	            $this->result = ajoutPrivResult($this->result);
	        }

			// if ($this->result!=null)
			// {
			$xml = TableauVersXML($this->result,$nomelement,4,"select",1,$xml2merge); //classique

			$log = new Logger("data.xml");
			$log->Log($xml);
			foreach($this->params4XSL as $ind => $data) $tab[$ind] = $data;
			$html=TraitementXSLT($xml,$nomFichierXsl,$tab,0,$xml,$arrHighlight);
			if(!empty($eval) && $eval=true){
				$html = eval("?".chr(62).$html.chr(60)."?");
			}
			echo $html;
	      //  }
	      //  else print kAucunResultat;
	}

function getRecord($identifier,$metadataPrefix){
    global $db;

	// Parsing identifier "oai:archives-sonores.bpi.fr:doc-123-fr"
	//$parse=parse_url($this->args["identifier"]);
	$_split=explode(":",$identifier);
	if($_split[0]!="oai") $this->dropError ('idDoesNotExist', '', $identifier);
	if($_split[1]!=gRepositoryId) $this->dropError ('idDoesNotExist', '', $identifier);
	$_split=explode("-",$_split[2]);
	$entite=$_split[0];
	$id=$_split[1];
	$id_lang=strtoupper($_split[2]);
	// VP 24/11/10 : test metadata
	if(empty($metadataPrefix)) {
		$this->dropError('noMetadataFormats');
		return;
	}
	// Style doc_oai_dc.xsl
	$style=$entite."_".$metadataPrefix.".xsl";

	$xsl=getSiteFile("designDir","oai/".$style);
 	if (!file_exists($xsl)) $this->dropError(kErrorWSXSLManquant);
	$encodage="0";
	
	// VP 10/02/11 : gestion OAI festival et personne (changement case festival en fest et personne en pers)
	switch ($entite)
    {
        case "doc":
            require_once(modelDir.'model_doc.php');
            require_once(modelDir.'model_docAcc.php');
            $mon_objet = new Doc();
  			$mon_objet->t_doc['ID_DOC']=$id;
  			$mon_objet->t_doc['ID_LANG']=$id_lang;
  			if (!$mon_objet->checkExistId()) {
                // VP 13/11/14 : vérification dans t_action
                $row=$db->GetAll("select ID_DOC,ACT_DATE from t_action where ACT_TYPE='DEL' and ID_DOC!=0 and ID_DOC=".intval($id));
                $datestamp=date("c", strtotime($row[0]['ACT_DATE']));
                if(count($row)>0){
                    $output="
                    <record>
                    <header status=\"deleted\">
                    <identifier>".$identifier."</identifier>
                    <datestamp>".$datestamp."</datestamp>
                    </header>\n
                    </record>\n";
                    return $output;
                }
                $this->dropError ('idDoesNotExist', '', $identifier);
            }
			else{
				$mon_objet->getDocFull(1,1,1,1,1,1);
				$mon_objet->getDocAcc();
				$mon_objet->getDocLiesSRC();
				$mon_objet->getDocLiesDST();
				$mon_objet->getFestival();
				$content= $mon_objet->xml_export(0,$encodage);
				//trace($content);
				$content=str_replace(array("&#60;BR&#62;","&#60;BR/&#62;","&#60;B&#62;","&#60;/B&#62;","’","&"),array(chr(10),chr(10),"","","'","&amp;"),$content);
				if ($mon_objet->t_doc['DOC_DATE_MOD']!='0000-00-00') 
					$datestamp=date("c", strtotime($mon_objet->t_doc['DOC_DATE_MOD'])); 
			}
			unset($mon_objet);
            break;
			
  		case "pers" :
  			require_once(modelDir.'model_personne.php');
  			$mon_objet = new Personne();
  			$mon_objet->t_personne['ID_PERS']=$id;
  			$mon_objet->t_personne['ID_LANG']=$id_lang;
  			//attention, pour des raisons historiques, checkExistId de personne renvoie une valeur inversée
  			if ($mon_objet->checkExistId()) $this->dropError ('idDoesNotExist', '', $identifier);
			else{
				$mon_objet->getPersonne();
				$mon_objet->getLexique();
				$mon_objet->getValeurs();
				$mon_objet->getDocAcc();
				$mon_objet->getFestival();
				$content= $mon_objet->xml_export(0,$encodage);
				$content=str_replace(array("&#60;BR&#62;","&#60;BR/&#62;","&#60;B&#62;","&#60;/B&#62;","’","&"),array(chr(10),chr(10),"","","'","&amp;"),$content);
				if ($mon_objet->t_personne['PERS_DATE_MOD']!='0000-00-00') 
					$datestamp=date("c", strtotime($mon_objet->t_personne['PERS_DATE_MOD'])); 
			}
 			unset($mon_objet);
 			break;
            
        case "fest":
            require_once(modelDir.'model_festival.php');
            $mon_objet = New Festival();
            $mon_objet->t_fest['ID_FEST']=$id;
  			$mon_objet->t_fest['ID_LANG']=$id_lang;  
  			if (!$mon_objet->checkExistId()) $this->dropError ('idDoesNotExist', '', $identifier);
			else{
				$mon_objet->getFest();
				$mon_objet->getLexique();
				$mon_objet->getPersonnes();
				$mon_objet->getValeurs();
				$mon_objet->getSections(true);
				$mon_objet->getDocs();
				$mon_objet->getDocAcc();
				$content=$mon_objet->xml_export(0,$encodage);
				$content=str_replace(array("&#60;BR&#62;","&#60;BR/&#62;","&#60;B&#62;","&#60;/B&#62;","’","&"),array(chr(10),chr(10),"","","'","&amp;"),$content);
				if ($mon_objet->t_fest['FEST_DATE_MOD']!='0000-00-00') 
					$datestamp=date("c", strtotime($mon_objet->t_fest['FEST_DATE_MOD']));
			}
			unset($mon_objet);
        	break;
    }
	$content=TraitementXSLT ($content, $xsl,array("identifier"=>$identifier,"id_lang"=>$id_lang));
	$output="
	<record>
		<header>
			<identifier>".$identifier."</identifier>
			<datestamp>".$datestamp."</datestamp>
		</header>\n";
	$output .= $content;
	$output .="
	</record>\n";
	if(empty($this->error_msg))return $output;
	
}

function get_token() {
	list($usec, $sec) = explode(" ", microtime());
	return ((int)($usec*1000) + (int)($sec*1000));
}


} // fin de classe PAGE
?>
