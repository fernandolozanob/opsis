<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');
require_once(modelDir.'model_materiel.php');
require_once(libDir."class_matInfo.php");

require_once('exception/exception_fileNotFoundException.php');

class JobProcess_ffmbc extends JobProcess implements Process
{
	//private $process_running;
	//private $
	
	private $nb_frames; // nombre theorique de frames
	private $nb_frames_encodees;
	
	private $nb_fps_fichier;

	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('ffmbc');
		
		$this->required_xml_params=array
		(

		);
		
		$this->optional_xml_params=array
		(
			'vcodec'=>'string',
			'acodec'=>'string',
            'image'=>'string',
			'ac'=>'int',
			'ar'=>'int',
            'b'=>'string',
            'ab'=>'string',
			'vb'=>'string',
			'f'=>'string',
			'faststart'=>'string',
			'aspect'=>'string',
			'aspect_ratio'=>'string',
			'aspect_mode'=>'string',
			'pix_fmt'=>'string',
			'profile'=>'string',
			'vf'=>'string',
			'sample_fmt'=>'string',
			'logo_marge'=>'int',
			'tcin'=>'string',
			'tcout'=>'string',
            'preroll'=>'string',
            'postroll'=>'string',
			'logo'=>'string',
            'map_audio_channel'=>'string',
            'map'=>'string',
			's'=>'int',
			's_h'=>'int',
			's_w'=>'int',
			'height'=>'int',
			'width'=>'int',
			'deinterlace'=>'int',
			'threads'=>'int',
			'g'=>'int',
			'target'=>'string',
			'bf'=>'int',
			'sc_treshold'=>'int',
			'bff'=>'int',
			'tff'=>'int',
			'intra'=>'int',
			'coder'=>'int',
			'crf'=>'int',
			'level'=>'string',
			'channel_layout'=>'string',
			'timecode'=>'string',
			'newaudio'=>'int',
			'minrate'=>'string',
			'maxrate'=>'string',
			'flags'=>'string',
			'flags2'=>'string',
			'dc'=>'int',
			'ps'=>'int',
			'qmin'=>'int',
			'qmax'=>'int',
			'lmin'=>'string',
			'bufsize'=>'string',
			'refs'=>'int',
			'vtag'=>'string',
            'rc_init_occupancy'=>'int',
			'keepnbaudio'=>'int',
            'async'=>'int',
			
			'croptop'=>'int',
			'cropbottom'=>'int',
			'cropleft'=>'int',
			'cropright'=>'int',
			
			'anamorphose'=>'string',
			'path_sub'=>'string',
			
			'force_fps' => 'int',
				
			'logo_before_padding' => 'int'	//précise un cas spécifique : on veut un logo en dehors des bandes noires. Default : 0
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
		
		//$this->process_running=Mutex::create();
	}
	
	function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
        

		// MS rétrocompatibilité anciens paramètres  : 
		if(isset($params_job['s_w']) && !isset($params_job['width'])){
			$params_job['width'] = $params_job['s_w'];
			unset($params_job['s_w']);
		}
		if(isset($params_job['s_h']) && !isset($params_job['height'])){
			$params_job['height'] = $params_job['s_h'];
			unset($params_job['s_h']);
		}
		if(isset($params_job['aspect']) && !isset($params_job['aspect_ratio'])){
			$params_job['aspect_ratio'] = $params_job['aspect'];
			unset($params_job['aspect']);
		}
		
        $options_ffmbc='-y';
        
 		if (!empty($params_job['image']) && file_exists(jobImageDir.$params_job['image']))
        {
            $options_ffmbc.=' -f image2 -loop 1 -shortest -i "'.jobImageDir.$params_job['image'].'"';
        }
		else if (!empty($params_job['image']) && !file_exists(jobImageDir.$params_job['image']))
			throw new FileNotFoundException('image file not found');

		$infos=MatInfo::getMatInfo($this->getFileInPath());
        foreach ($infos['tracks'] as $track)
        {
            if (strtolower($track['type'])=='video')
            {
                $video_format=$track['format'];
                $codec_id=$track['codec_id'];
                $version_format=$track['format_version'];
                $gop_info=$track['format_settings_gop'];
                $video_frame_rate=$track['frame_rate'];
                $video_scan_type=$track['scan_type'];
                $nb_fps=$track['frame_rate'];
				$bit_rate=$track['bit_rate'];
				$pix_fmt=$track['pix_fmt'];
				$profile=$track['format_profile'];
				$timecode=$track['timecode'];
                break;
            }
        }
		//XB encodage origine avec sous titres >> obligé de passer dans ffmbc pour sous titres meme si encodage d origine donc il faut retrouver les param
		if(!empty($params_job['path_sub']) || !empty($params_job['logo'])){
			if(!empty($params_job['path_sub']))
				$subtitles=1;
			
			
			//etape origine ==> on réécrit les paramètre selon le fichier
			if(!empty($params_job['vcodec']) && $params_job['vcodec']=='copy'){
				if($infos['type_mime'] == 'video/quicktime' && strpos($infos['format_calc'],'PRORES')!==false){
					$params_job['f']='mov';
					$params_job['vcodec']='prores';
					$params_job['pix_fmt']='yuv422p10le';			
					if(empty($profile)) $profile='std';
					elseif(strstr($profile,'LT')){
						$params_job['profile']='LT';
					}
					elseif(strstr($profile,'LT')){
						$params_job['profile']='lt';
					}				
					elseif(strstr($profile,'Standard')){
						$params_job['profile']='std';
					}				
					elseif(strstr($profile,'High')){
						$params_job['profile']='hq';
					}
					else $params_job['profile']='hq';
					$params_job['fieldorder']='tff';					
					//$params_job['b']=$bit_rate/1024;
									
				}
				elseif($infos['type_mime'] == 'video/mp4' && strpos($infos['format_calc'],'H264')!==false){
					$params_job['f']='mp4';
					$params_job['vcodec']='libx264';
					$params_job['pix_fmt']=$infos['pix_fmt'];
					if(strstr($profile,'Baseline')){
						$params_job['profile']='baseline';
					}
					elseif(strstr($profile,'Main')){
						$params_job['profile']='main';
					}				
					elseif(strstr($profile,'High')){
						$params_job['profile']='high';
					}
					else $params_job['profile']='high';	
					$params_job['b']=$bit_rate/1024;					
				}
				elseif($infos['type_mime'] == 'video/mpeg' && strpos($infos['format_calc'],'MPEG2')!==false){
					$params_job['f']='vob';
					$params_job['vcodec']='mpeg2video';
					$params_job['pix_fmt']='yuv422p';					
					$params_job['b']=$bit_rate/1024;
					$params_job['minrate']=$params_job['b'].'k';
					$params_job['maxrate']=$params_job['b'].'k';
					
					
				}
				elseif($infos['type_mime'] == 'video/quicktime' && strpos($infos['format_calc'],'DV')!==false){
					$params_job['f']='mov';
					$params_job['vcodec']='dvvideo';
					$params_job['target']='dvcam';
					$params_job['pix_fmt']='yuv420p';
					$params_job['fieldorder']='bff';				
					$params_job['b']=$bit_rate/1024;
					
					
				}				
				elseif($infos['type_mime'] == 'video/mxf' && strpos($infos['format_calc'],'DV')!==false){
					$params_job['f']='mxf';
					$params_job['vcodec']='dvvideo';
					$params_job['target']='dvcam';
					$params_job['pix_fmt']='yuv420p';				
					$params_job['b']=$bit_rate/1024;
				
					
				}			
				elseif($infos['type_mime'] == 'video/quicktime' && strpos($infos['format_calc'],'DNxHD')!==false){
					$params_job['f']='mov';
					$params_job['vcodec']='dnxhd';
					$params_job['pix_fmt']=$infos['pix_fmt'];
					$params_job['fieldorder']='tff';			
				}		
				elseif($infos['type_mime'] == 'video/mxf' && strpos($infos['format_calc'],'DNxHD')!==false){
					$params_job['f']='mxf';
					$params_job['vcodec']='dnxhd';
					$params_job['pix_fmt']=$infos['pix_fmt'];
					$params_job['fieldorder']='tff';			
				}
				//par défaut prores HQ
				else{
					$params_job['f']='mov';
					$params_job['vcodec']='prores';
					$params_job['pix_fmt']='yuv422p10le';			
					$params_job['profile']='hq';
					$params_job['fieldorder']='tff';					
					//$params_job['b']=$bit_rate/1024;							
				}
				//bitrate doit etre le plus proche de valeur établi			
				if(strpos($infos['format_calc'],'DNxHD')!==false){
					$bitrate=$bit_rate/1024;
					$bit_rate_ok= array('36','45','60','75','90','110','115','120','145','175','185','220','240','290','365','440');	
					foreach ($bit_rate_ok 	as $idx =>$val ) 
					{
						$ecart[$val]= abs($val - $bitrate);
					}	
					asort($ecart);
					foreach($ecart as $key => $value)
					{
						$params_job['b']=$key;
					break;
					}
				}	
			if (!empty($timecode)) $params_job['timecode']=$timecode;
			if (!empty($keepnbaudio)) $params_job['keepnbaudio']='1';
			$params_job['aspect_ratio']=$infos['display_aspect_ratio'];				
			}

		}

		
        if(!isset($nb_fps)) $nb_fps=25;
        $duree_max=$infos['duration'];
        $gop=$this->getGopSize($gop_info, $video_format, $infos['format_calc']);


		if (isset($params_job['r']) && !empty($params_job['r']))
			$nb_fps_final=$params_job['r'];
		else {
			 $this->writeJobLog("cadence origine : source nb_fps : ".$nb_fps);
			$nb_fps_final=$nb_fps;
			//Gestion des cadences/framerate non gérées par l'enveloppe "MOV"
			$cadences_auto = array(23.97,23.976,24000/1001,24,25,29.97,29.976,30000/1001,30,50,59.94,60000/1001,60);
			if(strcasecmp(getExtension($this->getFileOutPath()), 'mov') === 0 && !in_array($nb_fps_final, $cadences_auto) ) {
				$params_job['force_fps'] = true;
				$params_job['timecode'] = '';
			}
		}
		
		
		$this->nb_fps_fichier=$nb_fps_final;
		
		//nombre d'images total
		// utilisation de la floor a voir pour un replacement avec round(?)
		
		$this->nb_frames_encodees=0;

		if (isset($params_job['t']) && !empty($params_job['t']))
		{
			if ((timeToSec($params_job['ss'])+$params_job['t'])>$duree_max)
				$this->nb_frames=floor(($duree_max-timeToSec($params_job['ss']))*$nb_fps_final);
			else
				$this->nb_frames=floor($params_job['t']*$nb_fps_final);
		}
		else
			$this->nb_frames=floor($duree_max*$nb_fps_final);
		
		if (isset($params_job['tcin']) && !empty($params_job['tcin']) && isset($params_job['tcout']) && !empty($params_job['tcout']))
		{
            $tcin_sec=tcToSecDec($params_job['tcin'],$nb_fps_final);
            $tcout_sec=tcToSecDec($params_job['tcout'],$nb_fps_final);
			if(!empty($params_job['preroll'])){
				$tcin_sec=$tcin_sec-tcToSecDec($params_job['preroll'],$nb_fps_final);
				if ($tcin_sec < 0) $tcin_sec = 0;
			}
			if(!empty($params_job['postroll'])){
				$tcout_sec = tcToSecDec($params_job['postroll'],$nb_fps_final)+tcToSecDec($params_job['tcout'],$nb_fps_final);
            }
            
			$params_job['ss']=$tcin_sec;
			$params_job['t']=$tcout_sec-$tcin_sec;
			
			$this->nb_frames=$params_job['t']*$nb_fps_final;
		}

		$this->writeJobLog('duree_max='.$duree_max.' nb_fps='.$nb_fps_final.' nb_frames='.$this->nb_frames);
		
        // VP 4:09/2014 : paramètres -ss et -t avant -i si gop=1
        $options_sst='';
		if (isset($params_job['ss']) && !empty($params_job['ss']))
            $options_sst.=' -ss '.$params_job['ss'];
		if (isset($params_job['t']) && !empty($params_job['t']))
            $options_sst.=' -t '.$params_job['t'];
        
		if($gop==1)
            $options_ffmbc.=$options_sst.' -i '.escapeQuoteShell($this->getFileInPath());
        else
            $options_ffmbc.=' -i '.escapeQuoteShell($this->getFileInPath()).$options_sst;

		// init de la chaine des filtres : parametre vf :
		$chaine_vfilters="";
		

		
		// ajout filtre désentralecement OU  fieldorder => info ordre d'entrelacement
		if ( isset( $params_job['yadif'] ) && !empty( $params_job['yadif'] ) )
			$chaine_vfilters.='yadif='.$params_job['yadif'];
		else if (isset($params_job['deinterlace']) && !empty($params_job['deinterlace']) && strcasecmp($video_scan_type,'progressive') != 0)
			$chaine_vfilters.='yadif=0:-1:1';
		else if (isset($params_job['fieldorder'])&& !empty($params_job['fieldorder']) && ($params_job['fieldorder']=="bff" || $params_job['fieldorder']=="tff" )){
			$chaine_vfilters.='fieldorder='.$params_job['fieldorder'].'';
		}
		
		// ajout du filtre crop
		if (
				isset($params_job['croptop']) && !empty($params_job['croptop']) &&
				isset($params_job['cropbottom']) && !empty($params_job['cropbottom']) &&
				isset($params_job['cropleft']) && !empty($params_job['cropleft']) &&
				isset($params_job['cropright']) && !empty($params_job['cropright'])
		)
		{
			if (!empty($chaine_vfilters))
				$chaine_vfilters.=',';
			
			$chaine_vfilters.='crop=in_w-'.$params_job['cropleft'].'-'.$params_job['cropright'].':in_h-'.$params_job['croptop'].'-'.$params_job['cropbottom'].':'.$params_job['cropleft'].':'.$params_job['croptop'];
		}
			
			

		// si aspect_ratio auto => detection auto de l'aspect ratio cible,
		if(isset($params_job['aspect_ratio']) && $params_job['aspect_ratio']=="auto"){
//			$tmp_aspect = explode(':',$infos['display_aspect_ratio']);
//			if((floatval($tmp_aspect[0])/floatval($tmp_aspect[1])) < 1.5){
//				$params_job['aspect_ratio'] = "4:3";
//			}else if (floatval($tmp_aspect[0])/floatval($tmp_aspect[1])>=1.5){
//				$params_job['aspect_ratio'] = "16:9";
//			}
            // VP 17/09/14 : prise en compte ratio flottant (ex : 1.8962962962963)
            if(strpos($infos['display_aspect_ratio'],':')>0){
                $tmp_aspect = explode(':',$infos['display_aspect_ratio']);
                $aspect_ratio=floatval($tmp_aspect[0])/floatval($tmp_aspect[1]);
            }else{
                $aspect_ratio=floatval($infos['display_aspect_ratio']);
            }
            if($aspect_ratio < 1.5){
                $params_job['aspect_ratio'] = "4:3";
            }else if ($aspect_ratio>=1.5){
                $params_job['aspect_ratio'] = "16:9";
            }
			$aspect_cible = $params_job['aspect_ratio'];
			unset($tmp_aspect);
		// si aspect_ratio défini => aspect_ratio cible
		}else if (isset($params_job['aspect_ratio']) && !empty($params_job['aspect_ratio'])){
			$aspect_cible = $params_job['aspect_ratio'];
		}else{
			$aspect_cible = $infos['display_aspect_ratio'];
		}
		
		
		
		
		/*if ((!isset($params_job['width']) || empty($params_job['width'])) && (!isset($params_job['height']) || empty($params_job['height'])))
			throw new Exception('Erreur encodage ffmbc, param manquant : aucune dimension de sortie n\'est définie ');
		else
		{*/
		$largeur=$params_job['width'];
		$hauteur=$params_job['height'];
		
		$src_w=$infos['display_width'];
		$src_h=$infos['display_height'];
		
		if(empty($src_w) || empty($src_h))
		{
			//largeur et hauteur par défaut  
			$src_w=320;
			$src_h=240;
		}
		
		if(isset($aspect_cible)){
			if(strpos($aspect_cible, ':') !== false) {
				$tmp_aspect = explode(':',$aspect_cible);
				$aspect_value = floatval($tmp_aspect[0])/floatval($tmp_aspect[1]);
				unset($tmp_aspect);		
			} else {
				$aspect_value = floatval($aspect_cible);
			}		
		}
		
		if (!empty($largeur) && empty($hauteur) && isset($aspect_value))
			$hauteur = round($largeur *(1/$aspect_value)); //largeur sans hauteur
		
		if (empty($largeur) && !empty($hauteur)&& isset($aspect_value))
			$largeur = round($hauteur*$aspect_value); //hauteur sans largeur
		
	
		if (empty($largeur) && empty($hauteur)) //pas de size : on garde la taille originale
		{
			$hauteur=$src_h;
			$largeur=$src_w;
		}
		
		$largeur+=$largeur%2;
		$hauteur+=$hauteur%2;
		
		$params_job['aspect'] = str_replace(':1','',$params_job['aspect_ratio']);
        
		$largeur_scale='min('.$largeur.'\,2*trunc(('.$hauteur.'*dar*sar+1)/2))';
		$hauteur_scale='min('.$hauteur.'\,2*trunc(('.$largeur.'/(dar*sar)+1)/2))';
		
		
		if (isset($params_job['anamorphose']) && !empty($params_job['anamorphose']))
		{
            if($params_job['anamorphose']=='auto')
                $params_job['anamorphose']=$params_job['aspect_ratio'];
			//16:9 ou 1.7777
			//4:3 ou 1.3333
			if ($params_job['anamorphose']=='16:9')
				$anamorphose='16/9';
			else if ($params_job['anamorphose']=='4:3')
				$anamorphose='4/3';
			else
				$anamorphose=floatval($params_job['anamorphose']);
		}
		
		if (isset($anamorphose) && !empty($anamorphose))
		{
			if(isset($params_job['aspect_mode']) && $params_job['aspect_mode']=='fill')
			{
				if(!empty($chaine_vfilters)){$chaine_vfilters.=",";}
				$chaine_vfilters .= "scale=".$largeur.":".$hauteur;
				$chaine_padding="";
			}
			else if(isset($params_job['aspect_mode']) && $params_job['aspect_mode']=='letterbox')
			{
				if(!empty($chaine_vfilters)){$chaine_vfilters.=",";}
				$chaine_vfilters.='scale=min('.$hauteur.'*'.$anamorphose.'\,2*trunc(('.$hauteur.'*dar*sar+1)/2)):min('.$hauteur.'\,2*trunc(('.$hauteur.'*'.$anamorphose.'/(dar*sar)+1)/2))';
				$chaine_padding=',pad='.$hauteur.'*'.$anamorphose.':'.$hauteur.':max(0\,('.$hauteur.'*'.$anamorphose.'-iw)/2):max(0\,('.$hauteur.'-ih)/2):black';
				$chaine_second_scale = ',scale='.$largeur.':'.$hauteur.'';
			}
		}
		else
		{
			if(isset($params_job['aspect_mode']) && $params_job['aspect_mode']=='letterbox'){
				if(!empty($chaine_vfilters)){$chaine_vfilters.=",";}
				//$chaine_vfilters .= "scale=min(".$largeur."\,".$hauteur."*dar*sar+1):min(".$hauteur."\,".$largeur."/(dar*sar)+1)";
				$chaine_vfilters .= "scale=".$largeur_scale.":".$hauteur_scale."";
				$chaine_padding=",pad=".$largeur.":".$hauteur.":max(0\,(".$largeur."-iw)/2):max(0\,(".$hauteur."-ih)/2):black";
				// VP 22/05/2014 : bug IMX50
				if($params_job['target']=='imx50'){
					$chaine_vfilters.=$chaine_padding;
					$chaine_padding="";
				}
			}else if(isset($params_job['aspect_mode']) && $params_job['aspect_mode'] == 'fill'){
				if(!empty($chaine_vfilters)){$chaine_vfilters.=",";}
				$chaine_vfilters .= "scale=".$largeur.":".$hauteur;
				$chaine_padding="";
			}
		}
		
		$chaine_logo = "";
		// ajout du logo dans vf
		if (!empty($params_job['logo']) && file_exists(jobImageDir.$params_job['logo']))
		{
		
			if (isset($params_job['logo_marge']) && !empty($params_job['logo_marge']))
				$marge_logo=$params_job['logo_marge'];
			else
				$marge_logo=10;
		
		
			
			if(isset($params_job['logo_scale']) && !empty($params_job['logo_scale'])){
				$logo = new Imagick(jobImageDir.$params_job['logo']);
				$logo_w_ori = $logo->getImageWidth();
				$logo_h_ori = $logo->getImageHeight();
				
				if($largeur > $hauteur) {
					// $resize_w_logo = $hauteur - ($marge_logo * $hauteur /100)*2;
					$resize_h_logo = ($hauteur*$params_job['logo_scale']/100);
					$resize_w_logo = $resize_h_logo*$logo_w_ori/$logo_h_ori;
				} else {
					// $resize_h_logo = $largeur - ($marge_logo * $largeur /100)*2;
					$resize_w_logo = ($params_job['logo_scale'] * $largeur /100);
					$resize_h_logo = $resize_w_logo*$logo_h_ori/$logo_w_ori;
				}
			}
			
			
		
			
			$checkPosition = '';

			switch($params_job['logo_position'])
			{
				case 'center':
					$x_logo="main_w/2-overlay_w/2";
					$y_logo="main_h/2-overlay_h/2";
					break;
				case 'top-center':
					$x_logo="main_w/2-overlay_w/2";
					$y_logo=$marge_logo;
					break;
				case 'top-left':
					$x_logo=$marge_logo;
					$y_logo=$marge_logo;
					break;
				case 'top-right':
					$x_logo="main_w-overlay_w-".$marge_logo;
					$y_logo=$marge_logo;
					break;
				case 'bottom-left':
					$x_logo=$marge_logo;
					$y_logo="main_h-overlay_h-".$marge_logo;
					break;
				case 'bottom-center':
					$x_logo="main_w/2-overlay_w/2";
					$y_logo="main_h-overlay_h-".$marge_logo;
					break;
				default: //bottom_right
					$x_logo="main_w-overlay_w-".$marge_logo;
					$y_logo="main_h-overlay_h-".$marge_logo;
			}

			//$param['vf'].=' -vf '.escapeQuoteShell('movie='.jobImageDir.$param['logo'].' [wm];[in][wm] overlay='.($x_logo).':'.($y_logo).' [out]',$ssh);
			//$cmd.=' -vf '.escapeQuoteShell('movie='.jobImageDir.$param['logo'].' [wm];[in][wm] overlay='.($x_logo).':'.($y_logo).' [out]',$ssh);
			if(isset($params_job['logo_scale']) && !empty($params_job['logo_scale']) && $params_job['logo_scale']){
				if (isset($chaine_vfilters) && !empty($chaine_vfilters))
					$chaine_logo.=' [mov] ,movie='.jobImageDir."/".$params_job['logo'].' [logo];[logo] scale='.$resize_w_logo.':'.$resize_h_logo.' [logo2];[mov][logo2] overlay='.($x_logo).':'.($y_logo).'';
				else
					$chaine_logo='movie='.jobImageDir."/".$params_job['logo'].' [logo];[logo] scale='.$resize_w_logo.':'.$resize_h_logo.' [logo2];[in][logo2] overlay='.($x_logo).':'.($y_logo).'';
			}else{
				if (isset($chaine_vfilters) && !empty($chaine_vfilters))
					$chaine_logo.=' [mov] ,movie='.jobImageDir."/".$params_job['logo'].' [logo];[mov][logo] overlay='.($x_logo).':'.($y_logo).'';
				else
					$chaine_logo='movie='.jobImageDir."/".$params_job['logo'].' [logo];[in][logo] overlay='.($x_logo).':'.($y_logo).'';	
			}
		}
		else if (!empty($params_job['logo']) && !file_exists(jobImageDir.$params_job['logo']))
			throw new FileNotFoundException('logo file not found');
		
		//ajout du logo
		if (isset($params_job['logo_before_padding']) && !empty($params_job['logo_before_padding'])) {
			//en dehors des bandes noires (spécifique GPA 16/11/2016)
			$chaine_vfilters=$chaine_vfilters.$chaine_logo.$chaine_padding;
		} else {
			//Cas standard : Fixe même s'il y a des bandes noires
			$chaine_vfilters=$chaine_vfilters.$chaine_padding.$chaine_logo;
		}
		
		// incrustation texte
		if (isset($params_job['incrust_text']) && !empty($params_job['incrust_text']))
		{
			// police_text
			if (isset($params_job['police_text']) && !empty($params_job['police_text']))
			{
				if (file_exists($params_job['police_text']))
					$police_text=$params_job['police_text'];
				else
					$police_text=jobFontDir.$params_job['police_text'];
			}
			else
				$police_text=jobFontDir.'Tahoma.ttf';
			
			// size_text
			if (isset($params_job['size_text']) && !empty($params_job['size_text']))
			{
				$incrust_font_size=intval($params_job['size_text']);
			}
			else
				$incrust_font_size=20;
			
			$incrust_text="";
			if (isset($params_job['incrust_text']) && !empty($params_job['incrust_text']))
			{
				$incrust_text=$params_job['incrust_text'];
			}
			
			// VP 25/08/2016 : paramètre incrust_text_y (entier ou 'middle')
			$text_y=4;
			if (isset($params_job['incrust_text_y']) && !empty($params_job['incrust_text_y']))
			{
				$text_y=intval($params_job['incrust_text_y']);
			}
			$text_x=4;
			if (isset($params_job['incrust_text_x']) && !empty($params_job['incrust_text_x']))
			{
				$text_x=intval($params_job['incrust_text_x']);
			}
			
			$filter_tc='drawtext=fontfile='.$police_text.':fontsize='.$incrust_font_size.':text=\''.$incrust_text.'\':fontcolor=white@0.5:x='.$text_x.':y='.$text_y;
			
			if (isset($chaine_vfilters) && !empty($chaine_vfilters))
				$chaine_vfilters.=' ,'.$filter_tc;
			else
				$chaine_vfilters=$filter_tc;
		}
		
		
		if(isset($chaine_second_scale)){
			$chaine_vfilters .= $chaine_second_scale;
		}
		
		if(isset($params_job['vf']) && !empty($params_job['vf'])){
			if(!empty($chaine_vfilters))
				$chaine_vfilters.=",";
			
			$chaine_vfilters.=$params_job['vf'];
			unset($params_job['vf']);
		}
		
		//subtitles
		if(!empty($subtitles) && $subtitles==1){
			if (file_exists($params_job['path_sub'])) {
				if(strcasecmp(getExtension($params_job['path_sub']), 'vtt') === 0 || strcasecmp(getExtension($params_job['path_sub']), 'srt') === 0){
					if(strcasecmp(getExtension($params_job['path_sub']), 'vtt') === 0 ){
						$path_srt=stripExtension($params_job['path_sub']).'.srt';
						convertVTTtoSRT($params_job['path_sub'],$path_srt);
					}
					elseif(strcasecmp(getExtension($params_job['path_sub']), 'srt') === 0 ){
						$path_srt=$params_job['path_sub'];
					}
					//$options_ffmbc.=' -vf sub=file='.$path_srt.':fontsize=50:vmargin=40';				
//					$options_ffmbc.=' -vf "sub=file='.$path_srt.':fontsize=50:vmargin=40:boxcolor=0x00505050"';				
					$options_ffmbc.=' -vf "sub=file=\''.escapeShellParamFile($path_srt).'\':fontsize=60:vmargin=45:boxcolor=0x00505050:font='.jobFontDir.'Tahoma.ttf"';				
				}
				
			}			
		}
		if (isset($params_job['bff']) && !empty($params_job['bff']))
			$options_ffmbc.=' -bff';
		
		if (isset($params_job['tff']) && !empty($params_job['tff']))
			$options_ffmbc.=' -tff';
		
		if (isset($params_job['intra']) && !empty($params_job['intra']))
			$options_ffmbc.=' -intra';

		if (isset($params_job['force_fps']) && !empty($params_job['force_fps']))
			$options_ffmbc.=' -force_fps';
			
		if (isset($params_job['timecode']) && !empty($params_job['timecode']))
		{
            // VP 28/05/2014 : possibilité de passer un TC en dûr
            if($params_job['timecode']=='1'){
                $tc=$infos['timecode'];
            }else{
                $tc=$params_job['timecode'];
            }
			
            if(isset($params_job['ss'])){
                $tc=secDecToTc(tcToSecDec($tc)+$params_job['ss']);
            }
			
			if(empty($tc)) {
				$tc = '00:00:00:00';
			}
			
			$options_ffmbc.=' -timecode '.$tc.'';
		}
		
        // VP 4/09/2014 : paramètre threads pour libx264 avec x264opts
        if($params_job['vcodec']=='libx264' && isset($params_job['threads'])){
            $params_job['x264opts']='threads='.$params_job['threads'];
            unset($params_job['threads']);
        }
        
		$ffmbc_param=array
		(
			'f',
			'r',
            'target',
			'threads',
			'faststart',
			'vcodec',
            'x264opts',
			'profile',
			'aspect',
			'pix_fmt',
			'b',
			'vf',
			'acodec',
			'sample_fmt',
			'ab',
			'ar',
			'ac',
            'async',
			's',
			'g',
			'bf',
			'sc_threshold',
			'coder',
			'crf',
			'channel_layout',
			'minrate',
			'maxrate',
			'flags',
			'flags2',
			'refs',
			'level',
			'dc',
			'ps',
			'qmin',
			'qmax',
			'lmin',
			'vtag',
            'bufsize',
            'rc_init_occupancy',
			'b_strategy', 
			'map'
		);

		
		if(!empty($chaine_vfilters)){
			$params_job['vf'] = $chaine_vfilters;
		}
		
		$this->writeJobLog("lmin : ".$params_job['lmin']);
		
		if(isset($params_job['lmin']) && !empty($params_job['lmin'])){
			$params_job['lmin'] = str_replace(array("\'",'\"'),array("'",'"'),$params_job['lmin']);		
		}
		$this->writeJobLog("2 lmin : ".$params_job['lmin']);
		
		foreach($ffmbc_param as $param)
		{
			if (isset($params_job[$param]) && !empty($params_job[$param]))
			{
				if (($param=='b' || $param=='vb' || $param=='ab') && strpos($params_job[$param],'k') === false){
					$options_ffmbc.=' -'.$param.' '.$params_job[$param].'k';
				}else if ($param=='vf'){
					$options_ffmbc.=' -'.$param.' "'.$params_job[$param].'"';
				}else if ($param=='acodec' && $params_job[$param]=='none'){
					$options_ffmbc.=' -an';
				}else{
					$options_ffmbc.=' -'.$param.' '.$params_job[$param];
				}
			}
		}
		
		
		// mise en place d'un nom temporaire
		// le fichier de sortie est un fichier temporaire. le fichhier est renomme apres traitement.
		// le fichier temporaire est supprimé en cas d'annulation ou de suppression.
		$tmp_file=stripExtension($this->getFileOutPath()).'_'.time().'_'.rand(100000,999999).'.'.getExtension($this->getFileOutPath());
		$options_ffmbc.=' '.escapeQuoteShell($tmp_file).'';
		file_put_contents($this->getPidFilePath(),"\n".'tmp_file='.$tmp_file,FILE_APPEND);
		
		if (isset($params_job['newaudio']) && !empty($params_job['newaudio'])){
			$options_ffmbc .= $this->makeNewAudio();
			// $options_ffmbc.=' -newaudio';
		}
		
		if (isset($params_job['keepnbaudio']) && !empty($params_job['keepnbaudio']) && $params_job['keepnbaudio']==1)
		{
			// nombre de pistes audio
			$nb_pistes_audio=0;
			
			foreach ($infos['tracks'] as $track)
			{
				if (strtolower($track['type'])=='audio')
					$nb_pistes_audio++;
			}
			$this->writeJobLog('keepnbaudio = '.$nb_pistes_audio);
			
			if ($nb_pistes_audio>1)
			{
				for ($i=0;$i<$nb_pistes_audio-1;$i++)
				{
					//$options_ffmbc.=' -newaudio';
					$options_ffmbc .= $this->makeNewAudio();
				}
			}
		}
		 // VP 29/03/2013 : Map audio Channel
        if (isset($params_job['map_audio_channel']) && !empty($params_job['map_audio_channel'])){
            // Separation des directives map_audio_channel par ;
            // Ex : $param['map_audio_channel']="0:1:0:0:1:0;0:1:1:0:1:1"
			// 05/11/13 - ajout prise en charge d'une valeur "auto" pour le map_audio_channel, 
			//  => Si on detecte plus de 2 channels dans un stream audio, alors on utilise map_audio_channel pour ne récupérer que les deux premiers channels 
			// MS - 02/12/13 - Ajout prise en charge paramètre "type_mapping"
				// Actif uniquement si params_job['map_audio_channel'] == "auto",
				// params_jobs['type_mapping'] : valeurs possibles : stereo_multiples, mono_multiples, stereo_unique, 1_piste_4_canaux (voir fichier excel de référence des encodages)
				// permet de gérer les mappings audio automatiquement pour les sources ayant des formats audios type : 2xMono, 2xStereo, 4xMono, 1x4Canaux
			if($params_job['map_audio_channel'] == "auto"){
				if(isset($params_job['type_mapping']) && !empty($params_job['type_mapping'])){
					$src_audio_streams = array();
					if (isset($infos) && !empty($infos)){
						foreach($infos['tracks'] as $idx=>$track){
							if(strtolower($track['type']) == 'audio' && isset($track["channels"])){
								$src_audio_streams[] = array("idx"=>$idx,"nb_chan"=>$track["channels"]);
							}
						}
					}
					$newaudio_string = $this->makeNewAudio();
					if(count($src_audio_streams) == 1 && $src_audio_streams[0]['nb_chan'] >= 4 ){
						// CAS 1 piste x 4 canaux
						switch ($params_job['type_mapping']){
							case "stereo_multiples" : 
								$options_ffmbc.=" ".$newaudio_string." -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":2:0:2:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":3:0:2:1 ";
							break ;
						
							case "mono_multiples" : 
								$options_ffmbc.=" ".$newaudio_string." ".$newaudio_string." -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:2:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":2:0:3:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":3:0:4:0 ";
							break;
							
							case "stereo_unique" : 
								if (!isset($params_job['vcodec']) || empty($params_job['vcodec']))
									$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:0:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:0:1 ";
								else
									$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 ";
							break ; 
							
							case "1_piste_4_canaux" :
								// le mapping est automatique si la source est déjà en 1_piste_4_canaux
                                if($src_audio_streams[0]['nb_chan'] > 4){
                                    $options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":2:0:1:2 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":3:0:1:3 ";
                                }
							break;
						}
						
					}else if(count($src_audio_streams) == 2 ){
						$is_mono = true ; 
						foreach($src_audio_streams as $stream){
							if($stream['nb_chan'] != 1){
								$is_mono = false ; break ; 
							}
						}
						
						$is_stereo = true ; 
						foreach($src_audio_streams as $stream){
							if($stream['nb_chan'] != 2){
								$is_stereo = false ; break ; 
							}
						}
						if($is_mono){
							// CAS 2 pistes  x MONO 
							switch ($params_job['type_mapping']){
								case "stereo_multiples" : 
								case "1_piste_4_canaux" : 
								case "stereo_unique" : 
									if (!isset($params_job['vcodec']) || empty($params_job['vcodec']))
										$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:0:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:0:1 ";
									else
										$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:1 ";
								break ;
							
								case "mono_multiples" : 
									$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:2:0 ";
								break;
							}
						}
						
						if($is_stereo){
							// CAS 2 pistes x STEREO 
							switch ($params_job['type_mapping']){
								case "stereo_multiples" : 
									$options_ffmbc.=" ".$newaudio_string." -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:2:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":1:0:2:1 ";
								break ;
							
								case "mono_multiples" : 
									$options_ffmbc.=" ".$newaudio_string." ".$newaudio_string." -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:2:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:3:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":1:0:4:0 ";
								break;
								
								case "stereo_unique" : 
									if (!isset($params_job['vcodec']) || empty($params_job['vcodec']))
										$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:0:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:0:1 ";
									else
										$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 ";
								break ; 
								
								case "1_piste_4_canaux" : 
									$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[0]["idx"].":1:0:1:1 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:2 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":1:0:1:3 ";
								break ;
								
							}
						}
					}else if (count($src_audio_streams) == 4){
						$is_mono = true ; 
						foreach($src_audio_streams as $stream){
							if($stream['nb_chan'] != 1){
								$is_mono = false ; 
								break ; 
							}
						}
						if($is_mono){
							// CAS 4 pistes  x MONO 
							switch ($params_job['type_mapping']){
								case "stereo_multiples" : 
									$options_ffmbc.=" ".$newaudio_string." -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:1 -map_audio_channel 0:".$src_audio_streams[2]["idx"].":0:0:2:0 -map_audio_channel 0:".$src_audio_streams[3]["idx"].":0:0:2:1 ";
								break ;
							
								case "mono_multiples" : 
									$options_ffmbc.=" ".$newaudio_string." ".$newaudio_string." -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:2:0 -map_audio_channel 0:".$src_audio_streams[2]["idx"].":0:0:3:0 -map_audio_channel 0:".$src_audio_streams[3]["idx"].":0:0:4:0 ";
								break;
								
								case "stereo_unique" : 
									if (!isset($params_job['vcodec']) || empty($params_job['vcodec']))
										$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:0:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:0:1 ";
									else
										$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:1 ";
								break ; 
								
								case "1_piste_4_canaux" : 
								case "1_piste_n_canaux" :
									$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:1 -map_audio_channel 0:".$src_audio_streams[2]["idx"].":0:0:1:2 -map_audio_channel 0:".$src_audio_streams[3]["idx"].":0:0:1:3 ";
								break ;
								
							}
						}
					} else if (count($src_audio_streams) == 8){
						switch ($params_job['type_mapping']){
							case "1_piste_8_canaux" : 
							case "1_piste_n_canaux" :
								$options_ffmbc.=" -map_audio_channel 0:".$src_audio_streams[0]["idx"].":0:0:1:0 -map_audio_channel 0:".$src_audio_streams[1]["idx"].":0:0:1:1 -map_audio_channel 0:".$src_audio_streams[2]["idx"].":0:0:1:2 -map_audio_channel 0:".$src_audio_streams[3]["idx"].":0:0:1:3 -map_audio_channel 0:".$src_audio_streams[4]["idx"].":0:0:1:4 -map_audio_channel 0:".$src_audio_streams[5]["idx"].":0:0:1:5 -map_audio_channel 0:".$src_audio_streams[6]["idx"].":0:0:1:6 -map_audio_channel 0:".$src_audio_streams[7]["idx"].":0:0:1:7 ";
							break;
						}
					}
				}else{
					if (isset($infos) && !empty($infos)){
						foreach($infos['tracks'] as $idx=>$track){
							if(strtolower($track['type']) == 'audio' && isset($track["channels"]) && $track["channels"] > 2){
								$options_ffmbc.=" -map_audio_channel 0:".$idx.":0:0:1 -map_audio_channel 0:".$idx.":1:0:1 ";
								break;
							}
						}
					}
				}
			}else{
				$chunks=explode(';', $params_job['map_audio_channel']);
				foreach($chunks as $chunk){
					$options_ffmbc.=" -map_audio_channel ".trim($chunk);
				}
			}
        }
		trace($options_ffmbc);
		if ($params_job['vcodec']=='libx264' && $params_job['pix_fmt']=='yuv422p10le')
			$this->shellExecute(kFFMBC10bitPath,$options_ffmbc,false);
		else
			$this->shellExecute(kFFMBCpath,$options_ffmbc,false);
		do
		{
			sleep(5);
			$this->updateProgress();
		}
		while($this->checkIfFfmbcRunning());
		$this->updateProgress();
		
		if ($this->nb_frames_encodees<($this->nb_frames-250))
		{
			$data_ffprobe = MatInfo::getFFprobe($tmp_file);
			$duree_out = $data_ffprobe['duration'];
			$nb_frames_out = $duree_out*$nb_fps_final;
			if($nb_frames_out<($this->nb_frames-250)){
				//throw new Exception('Erreur encodage ffmbc (commande : '.kFFMBCpath.' '.$options_ffmbc.')');
				throw new Exception('Erreur encodage ffmbc (nb frame encodees :'.$this->nb_frames_encodees.', nb frames attendues :  '.$this->nb_frames.')');
			}
		}
		
		rename($tmp_file,$this->getFileOutPath());
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
		$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
		// VP 8/04/13 : compatibilite Mac OS X
		$donnees_stderr=str_replace("\r","\n",$donnees_stderr);
		$params_job=$this->getXmlParams();
		
		if (isset($params_job['vcodec']) && !empty($params_job['vcodec']))
		{	
			preg_match_all('/frame=([ 0-9]+) fps=([ 0-9.]+) (?:q=([- 0-9.]+) ){0,1}(Lsize|size)=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
			$this->nb_frames_encodees=intval($infos_encodage[1][count($infos_encodage[1])-1]);
		}
		else
		{
			// CAS fichier audio (pas de codec video)
			// pour les fichiers audio la sortie est : size=    5420kB time=00:03:18.17 bitrate= 224.0kbits/s
			preg_match_all('/size=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
			$nb_fps_encod=timeToSec($infos_encodage[2][count($infos_encodage[2])-1])*$this->nb_fps_fichier;
			$this->nb_frames_encodees=intval($nb_fps_encod);
		}
		
		$this->writeOutLog($this->nb_frames_encodees.' / '.$this->nb_frames);
		$ratio=$this->nb_frames_encodees/$this->nb_frames;
		if ($ratio>1)
			$ratio=1;
		
		$this->setProgression($ratio*100);
	}
	
	private function checkIfFfmbcRunning()
	{
		//if ($this->nb_frames_encodees<$this->nb_frames)
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$this->getPid().'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1)
				return true;
			else
				return false;
		}
	}
	
	private function makeNewAudio(){
		$params_job=$this->getXmlParams();
		$string_newaudio = "";
		$audio_params = array("acodec","sample_fmt","ar","ac","ab");
		foreach($audio_params as $name_param){
			if(isset($params_job[$name_param]) && !empty($params_job[$name_param])){
				$string_newaudio.= " -".$name_param." ".$params_job[$name_param];
			}
		}
		$string_newaudio .=" -newaudio ";
		return $string_newaudio;
	}
	
	private function getGopSize($gop_info, $video_format, $format_calc)
	{
        if(!empty($gop_info)){
            $donnees_gop=explode(',',$gop_info);
            foreach($donnees_gop as $dat_gop)
            {
                if (strpos($dat_gop,'N=')!=false)
                {
                    $valeur_gop=explode('=',trim($donnees_gop[1]));
                    return intval($valeur_gop[1]);
                }
            }
        } else {
            if ($video_format=='VC-3' || $video_format=='ProRes' || $video_format=='DV')
                $gop=1;
            else if ($video_format=='VC-1' || strpos($format_calc,'MPEG2')!==false || $video_format=='AVC')
                $gop=0; // GOP != 1
            else
                $gop=1;
        }
		return $gop;
	}
	
	public static function kill($module,$xml_file)
	{
		$job_name=stripExtension($xml_file);
		$contenu=file_get_contents(jobRunDir.'/'.$module.'/'.$job_name.'.pid');

		$contenu=explode("\n",$contenu);
		
		foreach ($contenu as $ligne)
		{
			$tmp_ligne=explode('=',$ligne);
			if ($tmp_ligne[0]=='tmp_file')
			{
				unlink($tmp_ligne[1]);
				break;
			}
		}
		
		JobProcess::kill($module,$xml_file);
	}
}

?>
