<?php

    require_once(modelDir.'model_materiel.php');

class Livraison {

var $idDir;
var $livraisonDir;
var $livraisonUrl;

var $arrDocs; // tableau de type panier_doc

var $error_msg;

var $iframelist;

var $zipFile;

	function __construct() {
		$this->overwrite=true; //par défaut, on écrase si un fichier existe déjà
	}


	function createDir() {

		if (!is_dir($this->livraisonDir) && !mkdir($this->livraisonDir)) {
            $this->dropError('Echec création répertoire livraison');
            return false;
        } else {
            // chmod au cas où le dossier existe déjà
            chmod($this->livraisonDir,0755);
            return true;
        }
	}



	function getMat2Copy($id_doc,$format='',$id_mat='',$mat_type='') {
		global $db;
		// VP 01/06/10 : ajout paramètre ID_MAT
		if(!empty($id_mat)){
			// VP 23/11/10 : ajout appel à sql_op
			$sql="SELECT distinct m.*,d.*,dm.* from t_mat m, t_doc_mat dm,t_format_mat fm, t_doc d
			WHERE m.ID_MAT=dm.ID_MAT and m.ID_MAT ".$this->sql_op($id_mat)."
			and d.ID_DOC=dm.ID_DOC and d.ID_LANG='".$_SESSION['langue']."'
			and dm.ID_DOC=".intval($id_doc)." and dm.DMAT_INACTIF!='1'";
		}
		else if(!empty($mat_type))
		{
			$sql="SELECT distinct m.*,d.*,dm.* from t_mat m, t_doc_mat dm,t_format_mat fm, t_doc d
				WHERE m.ID_MAT=dm.ID_MAT and m.MAT_TYPE ".$this->sql_op($mat_type)."
				and d.ID_DOC=dm.ID_DOC and d.ID_LANG='".$_SESSION['langue']."'
				and dm.ID_DOC=".intval($id_doc)." and dm.DMAT_INACTIF!='1'";
		}else{
			// VP 16/10/09 : ajout critère DMAT_INACTIF
			// VP 14/12/10 : possibilité de faire une livraison sur un format indépendemment de FORMAT_RECOPIE
			$sql="SELECT distinct m.*,d.*,dm.* from t_mat m, t_doc_mat dm,t_format_mat fm, t_doc d
			WHERE m.ID_MAT=dm.ID_MAT
			and d.ID_DOC=dm.ID_DOC and d.ID_LANG='".$_SESSION['langue']."'
			and dm.ID_DOC=".intval($id_doc)." and dm.DMAT_INACTIF!='1'";
			if($format!='') $sql .= " and m.MAT_FORMAT ILIKE ".$db->Quote('%'.$format.'%');
			else $sql .=" and m.MAT_FORMAT=fm.FORMAT_MAT and fm.FORMAT_RECOPIE='1'";
		}

		$myMat=$db->GetAll($sql);
		//by LD 19/01/08 => plus de myMat[0], on renvoie le tableau car on peut avoir plusieurs mat à copier
		return $myMat;
	}

	// VP 23/11/10 : création méthode sql_op inspirée de class_cherche
    protected function sql_op($valeur) {
        global $db;
        if (is_array($valeur)) $valeur=implode(',',$valeur);
		$arrWords=explode(",",$valeur);
		if(count($arrWords)>1) {
			$valeur="";
        	for($i = 0; $i <= (count($arrWords) - 1); $i++){
				$valeur.=",".$db->Quote(preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),trim($arrWords[$i])));
			}
			$valeur=" IN (".substr($valeur,1).")";
		} else if((strpos($valeur,"%")!==false)||(strpos($valeur,"*")!==false)) {
			$valeur=trim($arrWords[0]);
			$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
			$valeur=" LIKE ".$db->Quote(str_replace("*","%",$valeur));
		} else {
			$valeur=trim($arrWords[0]);
			$valeur=preg_replace(array("/^[\"\']/","/[\"\']$/"),array('',''),$valeur); //suppression des " ou ' encadrant
			$valeur=" = ".$db->Quote($valeur);
		}
		return $valeur;
    }

	function prepareLivraison_DVD($param,$crit_mat_liv=array('MAT_TYPE'=>'CSV'))
	{
		/*
		enveloppe_liv
		standard_liv
		ratio_liv
		 PANXML_ID_PROC
		*/

		//$param

        if (!isset($param['PANXML_ID_PROC']) || empty($param['PANXML_ID_PROC']))
			return null;

        if (isset($param['job_plateforme']) && !empty($param['job_plateforme']))
            $job_plateforme=$param['job_plateforme'];
        elseif(defined("kJobPlateforme"))
            $job_plateforme=kJobPlateforme;
        else $job_plateforme="";
		
		if (isset($param['runSyncJob']) && !empty($param['runSyncJob'])){
			$flag_runSyncJob = $param['runSyncJob'] ; 
		}else{
			$flag_runSyncJob  = false ; 
		}
        require_once(modelDir.'model_doc.php');
		require_once(modelDir.'model_etape.php');



		$xml_param_iso='<param>';
		
        if (isset($param['standard_liv']) || !empty($param['standard_liv']))
            $xml_param_iso.='<standard>'.$param['standard_liv'].'</standard>';
		
        if (isset($param['ratio_liv']) || !empty($param['ratio_liv']))
            $xml_param_iso.='<aspect_ratio>'.$param['ratio_liv'].'</aspect_ratio>';
		
        $xml_param_iso.='<FOLDER_OUT>'.$this->livraisonDir.'</FOLDER_OUT>';

		foreach ($this->arrDocs as $idx=>$doc)
		{
			//choix du mat pour la livraison
			$tmp_doc=new Doc();
			$tmp_doc->t_doc['ID_DOC']=$doc['ID_DOC'];
			$tmp_doc->getDoc();
			$tmp_doc->getDocMat();
			$tmp_doc->getMats();
            if(!empty($doc['PDOC_ID_MAT'])){
                $crit_mat_liv = array('ID_MAT'=>$doc['PDOC_ID_MAT']);
            }
			$mat_liv=$tmp_doc->getMatLivraison($crit_mat_liv);
			
			if ($mat_liv!=null)
			{
				if($mat_liv->t_mat['MAT_ID_MEDIA']=='V')
				{
					$xml_param_iso.='<video>';

					if(isset($doc['PDOC_EXTRAIT']) && $doc['PDOC_EXTRAIT']=='1' && isset($doc['PDOC_EXT_TCIN']) && isset($doc['PDOC_EXT_TCOUT'])){
						$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
						$tcin=frameToTC(tcToFrame($doc['PDOC_EXT_TCIN']) - tcToFrame($offset));
						$tcout=frameToTC(tcToFrame($doc['PDOC_EXT_TCOUT']) - tcToFrame($offset));

						$xml_param_iso.='<TcIn>'.$tcin.'</TcIn>';
						$xml_param_iso.='<TcOut>'.$tcout.'</TcOut>';

					}else if ((isset($doc['DMAT_TCIN']) && $mat_liv->t_mat['MAT_TCIN']!=$doc['DMAT_TCIN']) || (isset($doc['DMAT_TCOUT']) && $mat_liv->t_mat['MAT_TCOUT']!=$doc['DMAT_TCOUT'])){
					$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
					$xml_param_iso.='<TcIn>'.frameToTC(tcToFrame($doc['DMAT_TCIN']) - tcToFrame($offset)).'</TcIn>';
					$xml_param_iso.='<TcOut>'.frameToTC(tcToFrame($doc['DMAT_TCOUT']) - tcToFrame($offset)).'</TcOut>';
					}else if (isset($tmp_doc->t_doc_mat)){
						foreach ($tmp_doc->t_doc_mat as $dmat){
							if($dmat['ID_MAT'] == $mat_liv->t_mat['ID_MAT'] && ($mat_liv->t_mat['MAT_TCIN']!=$dmat['DMAT_TCIN'] && $mat_liv->t_mat['MAT_TCOUT']!=$dmat['DMAT_TCOUT'])){
								$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
								$xml_param_iso.='<TcIn>'.frameToTC(tcToFrame($dmat['DMAT_TCIN']) -  tcToFrame($offset)).'</TcIn>';
								$xml_param_iso.='<TcOut>'.frameToTC(tcToFrame($dmat['DMAT_TCOUT']) - tcToFrame($offset)).'</TcOut>';
							}
						}
					}
					

					$xml_param_iso.='<file>'.$mat_liv->getFilePath().'</file>';

					// pas besoin d'utiliser makeFileName sur les dvd puisque le nom n'est utilisé que dans le menu ?

					if ($doc['PDOC_EXTRAIT']==1)
						$xml_param_iso.='<name>'.$tmp_doc->t_doc['DOC_TITRE'].' - '.$doc['PDOC_EXT_TITRE'].'</name>';
					else
						$xml_param_iso.='<name>'.$tmp_doc->t_doc['DOC_TITRE'].'</name>';
					$xml_param_iso.='</video>';
				}
				
			}

		}
		// finalisation du xml param
		$xml_etape = $xml_param_iso.'</param>';

        // creation du job pour l'image ISO
		require_once(modelDir.'model_job.php');

		$str_sendmail='';

		if (is_bool($param['send_mail']) && $param['send_mail']==true)
			$str_sendmail='<send_mail>1</send_mail>';
		if (isset($param['mailJobErreur']) && !empty($param['mailJobErreur']))
			$str_sendmail.='<mailJobErreur>'.$param['mailJobErreur'].'</mailJobErreur>';

		$job_proc=new Job();
		if($flag_runSyncJob){
			$job_proc->flag_runSyncJob = $flag_runSyncJob ; 
		}
		$job_proc->t_job['JOB_NOM']='Preparation DVD';
		$job_proc->t_job['JOB_PARAM']='<param><ID_PANIER>'.$this->arrDocs[0]['ID_PANIER'].'</ID_PANIER><updatePanier>1</updatePanier>'.$str_sendmail;

		if(	isset($param['livraison_ftp'])
		&& isset($param['ftp_login']) && !empty($param['ftp_login'])
		&& isset($param['ftp_pw']) && !empty($param['ftp_pw'])){
			$job_proc->t_job['JOB_PARAM'].= '<livraison_ftp>'.$param['livraison_ftp'].'</livraison_ftp>'.
											'<ftp_login>'.$param['ftp_login'].'</ftp_login>'.
											'<ftp_pw>'.$param['ftp_pw'].'</ftp_pw>'.
											'<FOLDER_OUT>'.$this->livraisonDir.'</FOLDER_OUT>';
		}
		$job_proc->t_job['JOB_PARAM'].='</param>';
		$job_proc->t_job['JOB_ID_SESSION']=session_id();
		$job_proc->t_job['JOB_DATE_CREA']=date('Y-m-d H:i:s');
		$job_proc->t_job['JOB_PRIORITE']=(defined("gJobCommandePrioriteDefaut")?gJobCommandePrioriteDefaut:2);
        $job_proc->t_job['JOB_PLATEFORME']=$job_plateforme;
		$job_proc->save();

		// gestion processus => désactivée pour l'instant
		// la livraison DVD devra être remise dans un cas plus général permettant d'autres opérations sur les lignes du panier avant le passage en format DVD
	/*	require_once(modelDir.'model_processus.php');
		$tmp_proc = new Processus();
		$tmp_proc->t_proc['ID_PROC'] = $param['PANXML_ID_PROC'];
		$tmp_proc->getProc();
		$tmp_proc->getProcEtape();

		$tmp_proc->createJobs(0,"",$xml_etape,&$id_job,null,$job_proc->t_job['ID_JOB']);*/


		require_once(modelDir.'model_etape.php');
		$tmp_etape = new Etape();
		if($flag_runSyncJob){
			$tmp_etape->flag_runSyncJob = $flag_runSyncJob ; 
		}
		$tmp_etape->t_etape['ID_ETAPE'] = $param['PANXML_ID_PROC'];
		$tmp_etape->getEtape();
		$tmp_etape->createJob(0,"",$job_proc->t_job['ID_JOB'],0,$tmp_etape_valid,$tmp_jobObj,$xml_etape, null,2,$job_plateforme);
		unset($tmp_etape);

		return $job_proc->t_job['ID_JOB'];
	}
	
	function prepareLivraison_Montage($param,$crit_mat_liv=array('MAT_TYPE'=>'MASTER'))
	{
		global $db ; 
		
        if (!isset($param['PANXML_ID_PROC']) || empty($param['PANXML_ID_PROC']))
			return null;

        if (isset($param['job_plateforme']) && !empty($param['job_plateforme']))
            $job_plateforme=$param['job_plateforme'];
        elseif(defined("kJobPlateforme"))
            $job_plateforme=kJobPlateforme;
        else $job_plateforme="";

		if (isset($param['runSyncJob']) && !empty($param['runSyncJob'])){
			$flag_runSyncJob = $param['runSyncJob'] ; 
		}else{
			$flag_runSyncJob  = false ; 
		}
		
        require_once(modelDir.'model_doc.php');
		require_once(modelDir.'model_etape.php');



		$xml_param='<param>';
		
        if (isset($param['montage_xml']) || !empty($param['montage_xml'])){
            // $xml_param.='<montage_xml>'.$param['montage_xml'].'</montage_xml>';
			$montage_xml = xml2tab($param['montage_xml']);
		}
        if (isset($param['ratio_liv']) || !empty($param['ratio_liv']))
            $xml_param.='<aspect_ratio>'.str_replace('/',':',$param['ratio_liv']).'</aspect_ratio>';
		
		// trace("prepareLivraison_Montage montage_xml : ".print_r($montage_xml,true));
        $xml_param.='<ID_PANIER>'.intval($param['ID_PANIER']).'</ID_PANIER>';
		$xml_param.='<FOLDER_OUT>'.$this->livraisonDir.'</FOLDER_OUT>';
		
		// construction d'une map plus complete / utilisable représentant le montage
		$montage_map = array();
		foreach($montage_xml['TRACK'][0]['CLIPITEM'] as $idx=>$clipItem){
			if(strpos($clipItem['NAME'],'carton') !== false){
				$montage_map[$idx] = array(
					'type'=>'carton',
					'texte'=>$clipItem['TITLE'],
					'titre'=>$clipItem['NAME'], 
					'clip_params'=>$clipItem['CLIP_PARAMS']
				);
			}else if(strpos($clipItem['NAME'],'montdoc') !== false || strpos($clipItem['NAME'],'ext') !== false){
				if(strpos($clipItem['NAME'],'ext') !== false){
					$ext = explode('_',$clipItem['NAME']);
				}else{
					$ext = null ; 
				}
				$montage_map[$idx] = array(
					'type'=>'doc',
					'id' => 	(!empty($ext)? $ext[1] : intval(str_replace('montdoc_','',$clipItem['NAME']))),
					'tcin'=> 	(!empty($ext)? $ext[2] : null), 
					'tcout'=> 	(!empty($ext)? $ext[3] : null),
					'titre'=>$clipItem['TITLE'], 
					'mediaType' => $clipItem['MEDIATYPE'], 
					'clip_params'=>$clipItem['CLIP_PARAMS']
				);
				if(isset($param['PANXML_MONTAGE_MATSRC_'.$idx]) && !empty($param['PANXML_MONTAGE_MATSRC_'.$idx])){
					$montage_map[$idx]['id_mat'] = $param['PANXML_MONTAGE_MATSRC_'.$idx] ; 
				}
			}
			if(isset($clipItem['LOGO'])){
				$montage_map[$idx]['logo']=$clipItem['LOGO'];
				if(isset($clipItem['LOGO_POSITION'])){
					$montage_map[$idx]['logo_position']=$clipItem['LOGO_POSITION'];
				}
			}
		}
		
        $first_mat_liv = null ; 
		$crit_mat_liv_general = $crit_mat_liv;
		foreach ($montage_map as $idx=>$item)
		{
			if($item['type'] == 'doc'){
				//choix du mat pour la livraison
				$tmp_doc=new Doc();
				$tmp_doc->t_doc['ID_DOC']=$item['id'];
				$tmp_doc->getDoc();
				$tmp_doc->getDocMat();
				$tmp_doc->getMats();
				$crit_mat_liv = $crit_mat_liv_general ; 
				if(isset($item['id_mat']) && !empty($item['id_mat'])){
					$crit_mat_liv = array_merge($crit_mat_liv,array('ID_MAT'=>$item['id_mat'])) ; 
				}else if ($item['mediaType'] == 'picture'){
					$crit_mat_liv = array('MAT_ID_MEDIA'=>'P','MAT_TYPE'=>'VISIO','MAT_FORMAT'=>'JPEG');
				}
				$mat_liv=$tmp_doc->getMatLivraison($crit_mat_liv);

				if ($mat_liv!=null)
				{
					if($first_mat_liv == null ){
						$first_mat_liv = $mat_liv;
					}
					
					if($mat_liv->t_mat['MAT_ID_MEDIA']=='V')
					{
						if($item['tcin'] != null && $item['tcout'] != null ){
							$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
							$tcin=frameToTC(tcToFrame($item['tcin']) - tcToFrame($offset));
							$tcout=frameToTC(tcToFrame($item['tcout']) - tcToFrame($offset));
							$montage_map[$idx]['file_tcin'] = $tcin;
							$montage_map[$idx]['file_tcout'] = $tcout;
						}else if (isset($tmp_doc->t_doc_mat)){
							foreach ($tmp_doc->t_doc_mat as $dmat){
								if($dmat['ID_MAT'] == $mat_liv->t_mat['ID_MAT']){
									if($mat_liv->t_mat['MAT_TCIN']!=$dmat['DMAT_TCIN'] && $mat_liv->t_mat['MAT_TCOUT']!=$dmat['DMAT_TCOUT']){
										$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
										$tcin=frameToTC(tcToFrame($dmat['DMAT_TCIN']) - tcToFrame($offset));
										$tcout=frameToTC(tcToFrame($dmat['DMAT_TCOUT']) - tcToFrame($offset));

										$montage_map[$idx]['file_tcin'] = $tcin;
										$montage_map[$idx]['file_tcout'] = $tcout;
									}else{
                                        $montage_map[$idx]['file_tcin'] = "00:00:00:00";
                                        $montage_map[$idx]['file_tcout'] = frameToTC(tcToFrame($mat_liv->t_mat['MAT_TCOUT']) - tcToFrame($mat_liv->t_mat['MAT_TCIN']));
									}
								}
							}
						}	
						$montage_map[$idx]['file'] = $mat_liv->getFilePath() ; 
					}else if ($mat_liv->t_mat['MAT_ID_MEDIA'] == 'P'){
						$montage_map[$idx]['file'] = $mat_liv->getFilePath() ; 
					}
				}else {
					trace("livraison_montage - aucun matériel correspondant aux critères n'a été trouvé. crit_mats :  ".print_r($crit_mat_liv,true)." item : ".print_r($item,true));
				}
			}
		}
		// trace("prepareLivraison_Montage montage_map : ".print_r($montage_map,true));
		// trace("prepareLivraison_Montage array2xml(montage_map ): ".print_r(array2xml($montage_map,'clip'),true));
		// trace("prepareLivraison_Montage tab2xml(montage_map ): ".print_r(tab2xml($montage_map,'clip'),true));
		
		$xml_etape_pivot = $xml_param.'<montage>'.array2xml($montage_map,'clip').'</montage></param>';
		$xml_etape= $xml_param.'<remove_src_from_livraison>true</remove_src_from_livraison></param>';

		require_once(modelDir.'model_job.php');

		$str_sendmail='';

		if (is_bool($param['send_mail']) && $param['send_mail']==true)
			$str_sendmail='<send_mail>1</send_mail>';
		if (isset($param['mailJobErreur']) && !empty($param['mailJobErreur']))
			$str_sendmail.='<mailJobErreur>'.$param['mailJobErreur'].'</mailJobErreur>';

		// CREATION JOB PARENT
		$job_proc=new Job();
		if($flag_runSyncJob){
			$job_proc->flag_runSyncJob = $flag_runSyncJob ; 
		}
		$job_proc->t_job['JOB_NOM']= kPanierJobName.' '.$this->arrDocs[0]['ID_PANIER'].' '.kMontage;
		$job_proc->t_job['JOB_PARAM']='<param><ID_PANIER>'.$this->arrDocs[0]['ID_PANIER'].'</ID_PANIER>'.
									'<updatePanier>1</updatePanier>'.
									'<FOLDER_OUT>'.$this->livraisonDir.'</FOLDER_OUT>'.$str_sendmail;
		if(	isset($param['livraison_ftp'])
			&& isset($param['ftp_login']) && !empty($param['ftp_login'])
			&& isset($param['ftp_pw']) && !empty($param['ftp_pw'])){
			$job_proc->t_job['JOB_PARAM'].= '<livraison_ftp>'.$param['livraison_ftp'].'</livraison_ftp>'.
											'<ftp_login>'.$param['ftp_login'].'</ftp_login>'.
											'<ftp_pw>'.$param['ftp_pw'].'</ftp_pw>';
		}
		$job_proc->t_job['JOB_PARAM'].='</param>';
		$job_proc->t_job['JOB_ID_SESSION']=session_id();
		$job_proc->t_job['JOB_DATE_CREA']=date('Y-m-d H:i:s');
		$job_proc->t_job['JOB_PRIORITE']=(defined("gJobCommandePrioriteDefaut")?gJobCommandePrioriteDefaut:2);
		$job_proc->save();

		
		// recuperation id_etape de l'encodage pivot pour le montage
		$id_etape_encodage_pivot = $db->GetOne("SELECT ID_ETAPE from t_etape WHERE etape_param like '%<flag_etape>montage_pivot</flag_etape>%'");
		if(empty($id_etape_encodage_pivot) || is_int($id_etape_encodage_pivot )){
			$this->dropError(kErrorEtapePivotMontageNotFound);
			return false ; 
		}
		
		// GENERATION JOB CONCATENATION + FORMAT PIVOT MONTAGE 
		require_once(modelDir.'model_etape.php');
		$tmp_etape = new Etape();
		if($flag_runSyncJob){
			$tmp_etape->flag_runSyncJob = $flag_runSyncJob ; 
		}
		$tmp_etape->t_etape['ID_ETAPE'] = $id_etape_encodage_pivot ; // a redéfinir en constante eventuellement ? 
		$tmp_etape->getEtape();
		$tmp_etape->createJob(0,$first_mat_liv->t_mat['MAT_NOM'],$job_proc->t_job['ID_JOB'],0,$tmp_etape_valid,$tmp_jobObj,$xml_etape_pivot, array('rename'=>kMontage.'_pivot'),2,$job_plateforme);
		unset($tmp_etape);
		
		// GENERATION JOB ENCODAGE FINAL 
		/*VERSION ETAPE
		require_once(modelDir.'model_etape.php');
		$etape_encod = new Etape();
		$etape_encod->t_etape['ID_ETAPE'] = $param['PANXML_ID_PROC'];
		$etape_encod->getEtape();
		$etape_encod->createJob(0,kMontage."_pivot.mov",$job_proc->t_job['ID_JOB'],$tmp_jobObj->t_job['ID_JOB'],$tmp_etape_valid,$encod_jobObj,$xml_etape, array('rename'=>kMontage),2,$job_plateforme);
		unset($etape_encod); */ 
		//VERSION PROCESSUS 
		require_once(modelDir.'model_processus.php');
		$proc_encod = new Processus();
		if($flag_runSyncJob){
			$proc_encod->flag_runSyncJob = $flag_runSyncJob ; 
		}
		$proc_encod->t_proc['ID_PROC'] = $param['PANXML_ID_PROC'];
		$proc_encod->getProc();
		$proc_encod->getProcEtape();
		$proc_encod->createJobs(0,kMontage."_pivot.mov",$xml_etape,$id_job_proc ,array('rename'=>kMontage),$job_proc->t_job['ID_JOB'],$tmp_jobObj->t_job['ID_JOB'],array(),2,$job_plateforme);
		unset($proc_encod);

		return $job_proc->t_job['ID_JOB'];
	}


	function prepareLivraison($param,$crit_mat_liv=array('MAT_TYPE'=>'CSV'),$dynamic_param=array())
	{
		trace(print_r($crit_mat_liv,1));
        global $db;

        // trace('prepareLivraison panier : '.$this->arrDocs[0]['ID_PANIER']);
		// trace("prepareLivraison param : ".print_r($param,true));
		require_once(modelDir.'model_doc.php');
		require_once(modelDir.'model_etape.php');
		require_once(modelDir.'model_processus.php');
		require_once(modelDir.'model_job.php');
		$str_sendmail='';
		$id_job_liv;

		// Création du job global de traitement du panier =====

		if (isset($param['send_mail']) && ($param['send_mail']==true || intval($param['send_mail'])==1))
			$str_sendmail='<send_mail>1</send_mail>';
		if (isset($param['mails']) && !empty($param['mails']))
			$str_sendmail.='<mails>'.$param['mails'].'</mails>';
		if (isset($param['no_send_mail_usager']) && ($param['no_send_mail_usager']==true || intval($param['no_send_mail_usager'])==1))
			$str_sendmail.='<no_send_mail_usager>1</no_send_mail_usager>';
		if (isset($param['mailJobErreur']) && !empty($param['mailJobErreur']))
			$str_sendmail.='<mailJobErreur>'.$param['mailJobErreur'].'</mailJobErreur>';
        if (isset($param['job_plateforme']) && !empty($param['job_plateforme']))
            $job_plateforme=$param['job_plateforme'];
        elseif(defined("kJobPlateforme"))
            $job_plateforme=kJobPlateforme;
        else $job_plateforme="";
		
		if (isset($param['runSyncJob']) && !empty($param['runSyncJob'])){
			$flag_runSyncJob = $param['runSyncJob'] ; 
		}else{
			$flag_runSyncJob  = false ; 
		}

		$job_proc=new Job();
		if($flag_runSyncJob){
			$job_proc->flag_runSyncJob = $flag_runSyncJob ; 
		}
		$job_proc->t_job['JOB_NOM']= kPanierJobName.' '.$this->arrDocs[0]['ID_PANIER'];
		$job_proc->t_job['JOB_PARAM']='<param><ID_PANIER>'.$this->arrDocs[0]['ID_PANIER'].'</ID_PANIER>'.
									'<updatePanier>1</updatePanier>'.
									'<FOLDER_OUT>'.$this->livraisonDir.'</FOLDER_OUT>'.$str_sendmail;
		if(	isset($param['livraison_ftp'])
			&& isset($param['ftp_login']) && !empty($param['ftp_login'])
			&& isset($param['ftp_pw']) && !empty($param['ftp_pw'])){
			$job_proc->t_job['JOB_PARAM'].= '<livraison_ftp>'.$param['livraison_ftp'].'</livraison_ftp>'.
											'<ftp_login>'.$param['ftp_login'].'</ftp_login>'.
											'<ftp_pw>'.$param['ftp_pw'].'</ftp_pw>';
		}
		$job_proc->t_job['JOB_PARAM'].='</param>';
		$job_proc->t_job['JOB_ID_SESSION']=session_id();
		$job_proc->t_job['JOB_DATE_CREA']=date('Y-m-d H:i:s');
		$job_proc->t_job['JOB_PRIORITE']=(defined("gJobCommandePrioriteDefaut")?gJobCommandePrioriteDefaut:2);
		$job_proc->save();

		$id_job_liv = $job_proc->t_job['ID_JOB'];
		// ==================================================
        // Création jobs zip
        if(isset($param['livraison_zip']) && $param['livraison_zip']==1){
            $id_module_zip=$db->GetOne("select id_module from t_module where module_nom='zip'");
            if(!empty($id_module_zip)){
                $job_lignes=new Job();
				if($flag_runSyncJob){
					$job_lignes->flag_runSyncJob = $flag_runSyncJob ; 
				}
                $job_lignes->t_job['JOB_NOM']= 'Préparation fichiers '.$this->arrDocs[0]['ID_PANIER'];
                $job_lignes->t_job['ID_JOB_GEN']= $job_proc->t_job['ID_JOB'];
                $job_lignes->t_job['JOB_ID_SESSION']=session_id();
                $job_lignes->t_job['JOB_DATE_CREA']=date('Y-m-d H:i:s');
                $job_lignes->t_job['JOB_PLATEFORME']=$job_plateforme;
				$job_lignes->t_job['JOB_PRIORITE']=(defined("gJobCommandePrioriteDefaut")?gJobCommandePrioriteDefaut:2);
                $job_lignes->save();
                $id_job_liv = $job_lignes->t_job['ID_JOB'];
				
				if(isset($param['livraison_export_zip']) && !empty($param['livraison_export_zip'])){
					require_once(libDir."class_export.php");
					
					$prms_xsl = array();
					$prms_xsl["xmlfile"] = getSiteFile("designDir","/print/xml/".$param['livraison_export_zip'].".xml");
					$prms_xsl['date_US'] = date('m/d/Y');
					$prms_xsl['date_FR'] = date('d/m/Y');
					$prms_xsl['id_lang'] = $_SESSION['langue'];
					
					$myExport = new Export("panierdoc","ISO-8859-1","exportTab",100);
					$myExport->initExport("file","metadata","xls",$this->livraisonDir);
					$myExport->options['fromPanId'] = true ; 
					$myExport->addPrmsXsl($prms_xsl);
					$myExport->setIdsObjects($this->arrDocs[0]['ID_PANIER']);
					$myExport->renderExport();
				}

                $job_zip=new Job();
				if($flag_runSyncJob){
					$job_zip->flag_runSyncJob = $flag_runSyncJob ; 
				}
                $job_zip->t_job['JOB_NOM']= 'zip '.$this->arrDocs[0]['ID_PANIER'];
                $job_zip->t_job['ID_JOB_GEN']= $job_proc->t_job['ID_JOB'];
                $job_zip->t_job['ID_JOB_PREC']= $id_job_liv;
                $job_zip->t_job['JOB_PARAM']='<param><ID_PANIER>'.$this->arrDocs[0]['ID_PANIER'].'</ID_PANIER>'.'<FOLDER_IN>'.$this->livraisonDir.'</FOLDER_IN><FOLDER_OUT>'.$this->livraisonDir.'</FOLDER_OUT></param>';
                $job_zip->t_job['JOB_ID_MAT']=0;
                $job_zip->t_job['JOB_IN']='';
                $job_zip->t_job['JOB_OUT']=$this->arrDocs[0]['ID_PANIER'].'.zip';
                $job_zip->t_job['JOB_ID_MODULE']=$id_module_zip;
                $job_zip->t_job['JOB_ID_SESSION']=session_id();
                $job_zip->t_job['JOB_DATE_CREA']=date('Y-m-d H:i:s');
                $job_zip->t_job['JOB_PLATEFORME']=$job_plateforme;
				$job_zip->t_job['JOB_PRIORITE']=(defined("gJobCommandePrioriteDefaut")?gJobCommandePrioriteDefaut:2);
                $job_zip->save();
            }else{
                trace("Module ZIP manquant");
            }
        }

		// ==================================================


		// Traitement de chacun des documents du panier
		foreach ($this->arrDocs as $idx=>&$doc)
		{
			if(isset($param['id_proc_'.$doc['ID_LIGNE_PANIER']])){
				$doc['ID_PROC'] = $param['id_proc_'.$doc['ID_LIGNE_PANIER']];
			}elseif(isset($param['ID_PROC'])){
                $doc['ID_PROC'] = $param['ID_PROC'];
            }elseif(isset($param['id_proc'])){
                $doc['ID_PROC'] = $param['id_proc'];
            }elseif(isset($param['PANXML_ID_PROC'])){
                $doc['ID_PROC'] = $param['PANXML_ID_PROC'];
            }

			// trace("prepareLivraison id_proc par ligne : ".$doc['ID_PROC']);
			// MS - support des étapes désactivé => le fonctionnement updatePanier présent dans updateJobs ne fonctionne que pour des processus, pour l'instant on désactive la gestion par etape des lignes du panier
			/*if(isset($param['id_etape_'.$doc['ID_LIGNE_PANIER']])){
				$doc['ID_ETAPE'] = $param['id_etape_'.$doc['ID_LIGNE_PANIER']];
			}*/

	
			//récupération du matériel de livraison
			$tmp_doc=new Doc();
			$tmp_doc->t_doc['ID_DOC']=$doc['ID_DOC'];
			$tmp_doc->getDoc();
			$tmp_doc->getDocMat();
			$tmp_doc->getMats();

			$a_tmp_diff = array_diff_key($crit_mat_liv, array('FIELD' => '', 'VAL' => ''));
			if(!empty($doc['PDOC_ID_MAT'])){
				$tmp_crit = array('ID_MAT'=>$doc['PDOC_ID_MAT']);
			}
			else if(isset($crit_mat_liv[$doc['ID_LIGNE_PANIER']]) && is_array($crit_mat_liv[$doc['ID_LIGNE_PANIER']]) ){
				$tmp_crit = $crit_mat_liv[$doc['ID_LIGNE_PANIER']];
			// on vérifie si la méthode, plus pratique, crit_mat[nom_du_champ] = valeur n'a pas été utilisée à la place ou en surcharge de crit_mat[FIELD]=nom_du_champ (cf méthode suivante)
			}else if (!empty($a_tmp_diff)) {
				$tmp_crit = array_diff_key($crit_mat_liv, array('FIELD' => '', 'VAL' => ''));
			}else if (isset($crit_mat_liv['FIELD']) && isset($crit_mat_liv['VAL'])){
				$tmp_crit = array($crit_mat_liv['FIELD']=>$crit_mat_liv['VAL']);
			}else{
				$tmp_crit = array('MAT_TYPE'=>'MASTER');
			}

			$mat_liv=$tmp_doc->getMatLivraison($tmp_crit);

			trace('id_ligne_panier : '.$doc['ID_LIGNE_PANIER'].'id_doc : '.$doc['ID_DOC'].', materiel de livraison : '.$mat_liv->t_mat['ID_MAT'].' '.$mat_liv->t_mat['MAT_NOM'].' crit('.implode(',',array_keys($tmp_crit)).'=>'.implode(',',$tmp_crit).'), id_proc : '.$doc['ID_PROC']);
			
			if ($mat_liv!=null)
			{
				// Paramètres pour les jobs du panier_doc en cours
				$tmp_param.='<param>';
				$tmp_param.='<ID_LIGNE_PANIER>'.$doc['ID_LIGNE_PANIER'].'</ID_LIGNE_PANIER>';
				$tmp_param.='<FOLDER_OUT>'.$this->livraisonDir.'</FOLDER_OUT>'.$str_sendmail;
				$tmp_param.='<updatePanier>1</updatePanier>';

				$tmp_dyn_args['rename'] = $this->removeTrickyChars($doc['DOC_TITRE']);

				// Paramètres tcin/tcout pour la découpe d'extraits / séquence
				
				// if(in_array($mat_liv->t_mat['MAT_FORMAT'],array('DPX','DCP'))){
				if(strpos($mat_liv->t_mat['MAT_FORMAT'],'DPX')!==false || strpos($mat_liv->t_mat['MAT_FORMAT'],'DCP')!==false){
					// MS 14/11/14 => pour l'instant on empeche completement la définition des tc lors du processus de livraison de fichiers masters dcp / dpx 
				}else if(isset($doc['PDOC_EXTRAIT']) && $doc['PDOC_EXTRAIT']=='1' && isset($doc['PDOC_EXT_TCIN']) && isset($doc['PDOC_EXT_TCOUT'])){
					$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
					$tcin=frameToTC(tcToFrame($doc['PDOC_EXT_TCIN']) - tcToFrame($offset));
					$tcout=frameToTC(tcToFrame($doc['PDOC_EXT_TCOUT']) - tcToFrame($offset));
					$tmp_param.='<offset>00:00:00:00</offset>';
					$tmp_param.='<TcIn>'.$tcin.'</TcIn>';
					$tmp_param.='<TcOut>'.$tcout.'</TcOut>';
					$tmp_dyn_args['rename'] = $this->removeTrickyChars($doc['PDOC_EXT_TITRE']);

				}else if ((isset($doc['DMAT_TCIN']) && $mat_liv->t_mat['MAT_TCIN']!=$doc['DMAT_TCIN']) || (isset($doc['DMAT_TCOUT']) && $mat_liv->t_mat['MAT_TCOUT']!=$doc['DMAT_TCOUT'])){
					$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
					$tmp_param.='<offset>00:00:00:00</offset>';
					$tmp_param.='<TcIn>'.frameToTC(tcToFrame($doc['DMAT_TCIN']) - tcToFrame($offset)).'</TcIn>';
					$tmp_param.='<TcOut>'.frameToTC(tcToFrame($doc['DMAT_TCOUT']) - tcToFrame($offset)).'</TcOut>';
				}else if (isset($tmp_doc->t_doc_mat)){
					foreach ($tmp_doc->t_doc_mat as $dmat){

						if($dmat['ID_MAT'] == $mat_liv->t_mat['ID_MAT'] && ($mat_liv->t_mat['MAT_TCIN']!=$dmat['DMAT_TCIN'] || $mat_liv->t_mat['MAT_TCOUT']!=$dmat['DMAT_TCOUT'])){
							$offset=(empty($mat_liv->t_mat['MAT_TCIN'])?"00:00:00:00":$mat_liv->t_mat['MAT_TCIN']);
							$tmp_param.='<offset>00:00:00:00</offset>';
							$tmp_param.='<TcIn>'.frameToTC(tcToFrame($dmat['DMAT_TCIN']) -  tcToFrame($offset)).'</TcIn>';
							$tmp_param.='<TcOut>'.frameToTC(tcToFrame($dmat['DMAT_TCOUT']) - tcToFrame($offset)).'</TcOut>';
							break;
						}
					}
				}
				if (strpos($mat_liv->t_mat['MAT_FORMAT'], "DPX") === 0) {
					$tmp_param.='<is_dpx>1</is_dpx>';
					foreach ($mat_liv->getMatTrack() as $track) {
						$framerate = $track->get("MT_FRAME_RATE");
						if (!empty($framerate))
							$tmp_param.="<framerate>$framerate</framerate>";
					}
				}

				if (!empty($dynamic_param)) {
					foreach ($dynamic_param as $f=>$v)
						if (is_array($v)) {
							foreach ($v as $idp=>$vp)
								if ($idp == $doc['ID_LIGNE_PANIER']) {
									$tmp_param.="<$f>$vp</$f>";break; }
						}
						elseif (!empty($v))
							$tmp_param.="<$f>$v</$f>";
				}

				if(getExtension($tmp_dyn_args['rename']) ==getExtension($mat_liv->t_mat['MAT_NOM']) ){
					$tmp_dyn_args['rename'] = stripExtension($tmp_dyn_args['rename']);
				}
				
				if(defined("gLivraisonTypeNom")){
					$doc['ID_DOC'] = $tmp_doc->t_doc['ID_DOC'];
					$doc['DOC_TITRE'] = $tmp_doc->t_doc['DOC_TITRE'];
					$doc['MAT'] = $mat_liv->t_mat;
					$tmp_name = $this->makeFileName($doc,'',true);
					if($tmp_name !== false){
						$tmp_dyn_args['rename'] = stripExtension($tmp_name);
					}
				}

				// passage en dynamic_args de l'extension courante
				if(getExtension($mat_liv->t_mat['MAT_NOM']) != ''){
					$tmp_dyn_args['suffixe'] = '.'.getExtension($mat_liv->t_mat['MAT_NOM']);
				}else{
					$tmp_dyn_args['suffixe'] = '';
				}
				$tmp_param.='</param>';


				
				// Init du job du panier_doc en cours (étape ou processus)
				// ces jobs sont des descendants du job global de traitement du panier

                if(!empty($doc['ID_PROC'])){
					$tmp_proc = new Processus();
					if($flag_runSyncJob){
						$tmp_proc->flag_runSyncJob = $flag_runSyncJob ; 
					}
					$tmp_proc->t_proc['ID_PROC'] = $doc['ID_PROC'];
					$tmp_proc->getProc();
					$tmp_proc->getProcEtape();
					

					//Lancement restore si matériel non-présent et archivé
                    $id_job_restore=0;
					if (!$mat_liv->checkFileExists()) {
                        require_once(modelDir.'model_tape.php');
                        require_once(libDir."class_backupJob.php");
						trace("restore");
						$mat_liv->getTapeFile();
                        $id_etape_restore=0;
                        $id_tape_file=0;
                        $id_tape_set=0;
                        $tf_offline=null;
						foreach ($mat_liv->t_tape_file as $tape) {
                            $myTape=new Tape();
                            $myTape->t_tape['ID_TAPE']=$tape['ID_TAPE'];
                            $myTape->getTape();
                            if($tape['TF_TAILLE_FICHIER'] == $mat_liv->t_mat['MAT_TAILLE'] && $tape['TF_DATE_FICHIER'] == $mat_liv->t_mat['MAT_DATE_FICHIER']){
                                $myTape->getTapeSet();
                                $drive=$myTape->t_tape_set[0]['TS_DRIVE'];
                                $set=$myTape->t_tape_set[0]['ID_TAPE_SET'];
                                if (!empty($set)) {
                                    if($myTape->t_tape['TAPE_STATUS']=='1'){
                                        $ts_externe = $myTape->t_tape_set[0]['TS_EXTERNE'];
                                        if(empty($id_tape_file) || $ts_externe!='1'){
                                            $id_tape_file = $tape['ID_TAPE_FILE'];
                                            $id_tape_set = $set;
                                        }
                                    }elseif(empty($tf_offline)){
                                        $tf_offline = $tape;
                                     }
                                }

                            }
						}
                        // Création Job Restore
 						if (!empty($id_tape_file)) {
                            $myBackupJob=new BackupJob();
                            $id_job_restore=$myBackupJob->checkBackupJobScheduled(array('id'=>$mat_liv->t_mat['ID_MAT']), $id_tape_set, "restore");
                            if(empty($id_job_restore)){
                                $ok=$myBackupJob->createJob($objRestoreJob, "restore", "", $id_tape_file, null, null, null, null, null,0,$id_job_liv, 2, $job_plateforme);
                                $id_job_restore=$objRestoreJob->t_job['ID_JOB'];
                            }
                        }elseif (!empty($tf_offline) && defined("gEtapeRestoreOffline")) {
                            $myBackupJob=new BackupJob();
                            $id_job_restore=$myBackupJob->checkBackupJobScheduled(array('id'=>$mat_liv->t_mat['ID_MAT']), $tf_offline['ID_TAPE_SET'], "restoreOffline");
                            if(empty($id_job_restore)){
                                $etape = new Etape;
                                $etape->t_etape['ID_ETAPE'] = gEtapeRestoreOffline;
                                $etape->getEtape();
                                $tabParam=array('tape' => $tf_offline['ID_TAPE'],
                                              'set' => $tf_offline['ID_TAPE_SET'],
                                              'numfile' => $tf_offline['TF_N_FILE']);
                                if(getExtension($mat_liv->t_mat['MAT_NOM'])=='tar')
                                    $tabParam['extract']=1;
                                $xmlEtape = array2xml($tabParam,'param');
                                $etape->createJob($mat_liv->t_mat['ID_MAT'],$mat_liv->t_mat['MAT_NOM'],0,0,$id_job_valid,$jobEtapeObj, $xmlEtape);
                                $id_job_restore = $jobEtapeObj->t_job['ID_JOB'];
                            }
                        }
					}


					$tmp_proc->createJobs($mat_liv->t_mat['ID_MAT'],$mat_liv->t_mat['MAT_NOM'],$tmp_param,$id_job,$tmp_dyn_args,$id_job_liv,$id_job_restore, array(), (defined("gJobCommandePrioriteDefaut")?gJobCommandePrioriteDefaut:2), $job_plateforme);
					unset($tmp_proc);
					unset($doc['ID_PROC']);
					$doc['PDOC_ID_JOB'] = $id_job;
				}
				// MS - support des étapes désactivé => le fonctionnement updatePanier présent dans updateJobs ne fonctionne que pour des processus, pour l'instant on désactive la gestion par etape des lignes du panier
				/*else if(!empty($doc['ID_ETAPE'])){
					$tmp_etape = new Etape();
					$tmp_etape->t_etape['ID_ETAPE'] = $doc['ID_ETAPE'];
					$tmp_etape->getEtape();
					$tmp_etape->createJob($mat_liv->t_mat['ID_MAT'],$mat_liv->t_mat['MAT_NOM'],$id_job_liv,0,&$tmp_etape_valid,&$tmp_jobObj,$tmp_param);
					unset($tmp_etape);
				}*/

				unset($tmp_param);
			}
		}
		
		return $id_job_liv;
	}



	/**Effectue la livraison : soit copie, soit découpage, selon que l'on demande un movie entier
	 * ou bien une séquence ou un extrait.
	 * IN : tab de classe arrDocs (type t_panier_doc)
	 * OUT : appel des fn de copy ou découp, msg si erreur
	 * NOTE : le découpage s'effectue selon les timecode de la table t_doc_mat.
	 */
	function doLivraison($doPlayList=false,$fileOut="",$add_idx=false,$mat_type='') {

		$olddelay=ini_get('max_execution_time');
		ini_set('max_execution_time',300); //Temps maxi = 5 minutes

		if (!$this->createDir()) return false;
		//By LD => si on a plusieurs mat associés à un doc, il faut livrer ts les mats.
		//la structure actuelle de la classe ne permet pas ce cas, alors on "ruse" :
		//on recrée un tableau this->arrDocs qui compte autant de lignes que de matériels
		foreach ($this->arrDocs as $idx=>$doc) {
			// VP 01/06/10 : ajout paramètre ID_MAT
			// VP 12/10/10 : ajout paramètre PDOC_ID_MAT
			if(!empty($doc['PDOC_ID_MAT'])) $doc['ID_MAT']=$doc['PDOC_ID_MAT'];
			$materiels=$this->getMat2Copy($doc['ID_DOC'],$doc['PDOC_SUPPORT_LIV'],$doc['ID_MAT'],$mat_type); //récup des matériels
			foreach ($materiels as $mat) {
                // VP 26/11/2012  : vérification existence fichier avant livraison
                $objMat=new Materiel();
                $objMat->t_mat['ID_MAT']=$mat['ID_MAT'];
                $objMat->getMat();
                $fileSrc=$objMat->getFilePath();
                unset($objMat);
                if(is_file($fileSrc)){
                    $doc['MAT']=$mat;
                    $arrDocs[]=$doc; //tableau temp
                }
			}
		}
		$this->arrDocs=$arrDocs;
		unset($arrDocs); //mémoire
		// VP 13/07/09 : découpage si DMAT_TCs != MAT_TCs et remplissage variables file_titre, file_tcin, file_tcout et file_offset
		$index_ligne=0;
		if(!empty($this->arrDocs)){
			foreach ($this->arrDocs as $idx=>&$doc) {
				$index_ligne++;
				
				$offset=(empty($doc['MAT']['MAT_TCIN'])?"00:00:00:00":$doc['MAT']['MAT_TCIN']);
				$doc['file_offset']=$offset;
				if($doc['PDOC_EXTRAIT']=='1') $doc['file_titre']=$doc['PDOC_EXT_TITRE'];
				else $doc['file_titre']=$doc['MAT']['DOC_TITRE'];
			// Choix découpage ou non
				$numer=tcToSec($doc['MAT']['DMAT_TCOUT'])-tcToSec($doc['MAT']['DMAT_TCIN']);  //durée de l'extrait
				$denom=tcToSec($doc['MAT']['MAT_TCOUT'])-tcToSec($doc['MAT']['MAT_TCIN']); //durée totale du matériel
				//trace("ratio decoup :".$numer."/".$denom);
				// VP 9/02/11 : changement test $numer/$denom de !=1 à <1
				$decoup =(($denom==0  || $numer/$denom<1 ) && $numer!=0 );
				if (!empty($doc['MAT'])) {
					if(!empty($doc['RESIZE_IMG'])) {
						$this->resizeImage($doc, ($idx+1));
					}
					elseif(!empty($doc['PDOC_SUPPORT_LIV']) && $doc['MAT']['MAT_FORMAT']!=$doc['PDOC_SUPPORT_LIV']){
						if ($doc['PDOC_EXTRAIT']=='1') { //extrait
							$this->transcodeMovie($doc,$idx+1,$doc['PDOC_EXT_TCIN'],$doc['PDOC_EXT_TCOUT'],$doc['PDOC_SUPPORT_LIV']);
							$doc['file_tcin']=$doc['PDOC_EXT_TCIN'];
							$doc['file_tcout']=$doc['PDOC_EXT_TCOUT'];
						}
						elseif ($doc['PDOC_EXTRAIT']!='1' && $decoup) {
						//sequence
							$this->transcodeMovie($doc,$idx+1,$doc['MAT']['DMAT_TCIN'],$doc['MAT']['DMAT_TCOUT'],$doc['PDOC_SUPPORT_LIV']);
							$doc['file_tcin']=$doc['MAT']['DMAT_TCIN'];
							$doc['file_tcout']=$doc['MAT']['DMAT_TCOUT'];
						}
						else { //programme
							$this->transcodeMovie($doc,'','','',$doc['PDOC_SUPPORT_LIV']);
							$doc['file_tcin']=$doc['MAT']['DMAT_TCIN'];
							$doc['file_tcout']=$doc['MAT']['DMAT_TCOUT'];
						}
					}
					elseif ($doc['PDOC_EXTRAIT']=='1') { //extrait
						$this->sliceMovie($doc,$idx+1,$doc['PDOC_EXT_TCIN'],$doc['PDOC_EXT_TCOUT'],$offset);
						$doc['file_tcin']=$doc['PDOC_EXT_TCIN'];
						$doc['file_tcout']=$doc['PDOC_EXT_TCOUT'];
					}
					elseif ($doc['PDOC_EXTRAIT']!='1' && $decoup ) {
					//sequence
						$this->sliceMovie($doc,$idx+1,$doc['MAT']['DMAT_TCIN'],$doc['MAT']['DMAT_TCOUT'],$offset);
						$doc['file_tcin']=$doc['MAT']['DMAT_TCIN'];
						$doc['file_tcout']=$doc['MAT']['DMAT_TCOUT'];
					}
					else { //programme
						if ($add_idx==true)
							$this->copyMovie($doc,$index_ligne);
						else
							$this->copyMovie($doc);
						$doc['file_tcin']=$doc['MAT']['DMAT_TCIN'];
						$doc['file_tcout']=$doc['MAT']['DMAT_TCOUT'];
					}
					if($doPlayList) $mediaPaths[]=$doc['fileDir'];
					// Log livraison
					logAction("LIV",array("ID_DOC"=>$doc['ID_DOC'],"ACT_SQL"=>$doc['ID_LIGNE_PANIER'], "ACT_REQ"=>$this->idDir));
				} else $this->dropError(kErrorLivraisonNoMateriel);

			}
		}

		// VP 10/02/10 : livraison playList par catMovie
		if($doPlayList){
			$ok=$this->catMovie($mediaPaths,$doc,$fileOut);
			unset($this->arrDocs);
			if ($ok) {
				$this->arrDocs=array($doc);
			}
		}

		// VP 23/11/10 : ajout zip optionnel avec test librairie zip
		if(!empty($this->zipFile)){
			$zipFilePath=$this->livraisonDir.$this->zipFile;
			if(file_exists($zipFilePath)) unlink($zipFilePath);
			if(extension_loaded("zip")){
				$zip = new ZipArchive();
				if ($zip->open($zipFilePath, ZIPARCHIVE::CREATE)!==TRUE) {
					$this->dropError(kErrorImpossibleCreerZip);
				}else{
					//PC 14/01/11 : changement de $doc en $ddoc car cela provoquait un bug
					foreach ($this->arrDocs as $idx=>$ddoc){
						if(!empty($ddoc['fileDir'])){
							$zip->addFile($ddoc['fileDir'], basename($ddoc['fileDir']));
						}
					}
					$liv['fileUrl']=$this->livraisonUrl.$this->zipFile;
					$liv['fileDir']=$this->livraisonDir.$this->zipFile;
					$liv['fileName']=$this->zipFile;
					$this->arrDocs=array($liv);
					$zip->close();
				}
			}elseif(defined("kBinZip")){
				$cmd=kBinZip." -j ".$zipFilePath;
				foreach ($this->arrDocs as $idx=>$doc){
					if(!empty($doc['fileDir'])){
						$cmd.=" ".$doc['fileDir'];
					}
				}
				trace($cmd);
				exec($cmd);
				$liv['fileUrl']=$this->livraisonUrl.$this->zipFile;
				$liv['fileDir']=$this->livraisonDir.$this->zipFile;
				$liv['fileName']=$this->zipFile;
				$this->arrDocs=array($liv);
			}
		}
		ini_set('max_execution_time',$olddelay);

	}

/**
 * Calcul du nom de fichier de sortie selon les préférences
 * IN : doc (ligne panier doc), idx => numéro d'index, param kLivraisonTypeNom ds initialisationGeneral
 * OUT : nom du fichier, false si erreur ou impossible à calculer
 * NOTE : si le paramètre manque, ou que le résultat est vide ou inexploitable,
 * on repasse sur l'ID_MAT.
 * Dans le cas de noms de type 'texte' (doc_titre, etc.), le résultat est expurgé
 * des caractères interdits.
 * Dans le cas de DOC_TITRE, si un nom d'extrait existe, on le choisit préférentiellement
 */
	function makeFileName(&$doc,$idx='',$enforceUniqueName = false)
	{
		if($enforceUniqueName && !isset($this->array_verif_unique_name)){
			$this->array_verif_unique_name = array() ; 
		}
		
		if(isset($doc['MAT'])){
			require_once(modelDir.'model_materiel.php');
			$mat=new Materiel();
			$mat->t_mat['ID_MAT']=$doc['MAT']['ID_MAT'];
			$mat->getMat();

			$ext=getExtension($mat->getFilePath());
		}else{
			$ext ="";
		}
		if (defined('gLivraisonTypeNom') ) {
			switch (gLivraisonTypeNom) {

				case 'ID_DOC_DOC_TITRE':
					if(isset($doc['ID_DOC'])){
						if ($doc['PDOC_EXTRAIT']=='1'){
							$fileOut=$this->removeTrickyChars($doc['ID_DOC']."_".((!empty($doc['PDOC_EXT_TITRE']))?$doc['PDOC_EXT_TITRE']:"ext_".$doc['DOC_TITRE']));
						}else if (isset($doc['DOC_TITRE']))
							$fileOut=$this->removeTrickyChars($doc['ID_DOC']."_".$doc['DOC_TITRE']);
					}
				break;
				case 'DOC_TITRE':
					if (($doc['PDOC_EXTRAIT']=='1')&&(!empty($doc['PDOC_EXT_TITRE']))){
						$fileOut=$this->removeTrickyChars($doc['PDOC_EXT_TITRE']);
					}else if (isset($doc['MAT']) && isset($doc['MAT']['DOC_TITRE']) && !empty($doc['MAT']['DOC_TITRE'])){
						$fileOut=$this->removeTrickyChars($doc['MAT']['DOC_TITRE']);
					}else if (isset($doc['DOC_TITRE'])){
						$fileOut=$this->removeTrickyChars($doc['DOC_TITRE']);
					}
				break;
				case 'DOC_COTE' :
						// VP 14/10/10 : prise en compte champ PDOC_EXT_COTE
						//$fileOut=$this->removeTrickyChars($doc['MAT']['DOC_COTE']);
						if (($doc['PDOC_EXTRAIT']=='1')&&(!empty($doc['PDOC_EXT_COTE']))) {
							$fileOut=$this->removeTrickyChars($doc['PDOC_EXT_COTE']);
                            $fileOut.= "_".str_replace(':', '', $doc['PDOC_EXT_TCIN']."_".$doc['PDOC_EXT_TCOUT']);
						} else if(isset($doc['MAT'])&&(!empty($doc['MAT']['DOC_COTE']))){
							$fileOut=$this->removeTrickyChars($doc['MAT']['DOC_COTE']);
							$fileOut.= "_".str_replace(':', '', $doc['PDOC_EXT_TCIN']."_".$doc['PDOC_EXT_TCOUT']);
						}else if (isset($doc['DOC_COTE'])){
							$fileOut=$this->removeTrickyChars($doc['DOC_COTE']);
							if($doc['PDOC_EXTRAIT']=='1'){
                                $fileOut.= "_".str_replace(':', '', $doc['PDOC_EXT_TCIN']."_".$doc['PDOC_EXT_TCOUT']);
                            }
						}
				break;
				default :
					$fileOut=stripExtension($mat->t_mat['MAT_NOM']);
                    if($doc['PDOC_EXTRAIT']=='1'){
                        $fileOut.= "_".str_replace(':', '', $doc['PDOC_EXT_TCIN']."_".$doc['PDOC_EXT_TCOUT']);
                    }
			}
		}
		
		// MS - 20.07.2017 - rationalisation des controles / corrections a appliquer sur les noms de fichier (idx / extension)
		// Quelque soit le pattern choisi pour le calcul des noms de fichiers, on applique maintenant toujours les mêmes règles supplémentaires
		
		// On limite la taille du nom de fichier à 250 chars
		$fileOut=substr($fileOut,0,250);
		// Si on a un fileOut
		if(!empty($fileOut)){
			// Si aucun idx n'a été explicité lors de l'appel, mais que la règle enforceUniqueName est active, on détermine, sur l'ensemble de la livraison, les renommages automatiques à effectuer
			if(empty($idx) && $enforceUniqueName){
				$found = false ; 
				foreach($this->array_verif_unique_name as $idx_arr=>$name){
					if($fileOut == $name['name']){
						$found = true ; 
						if(isset($name['count'])){
							$this->array_verif_unique_name[$idx_arr]['count']++;
						}else{
							$this->array_verif_unique_name[$idx_arr]['count'] = 1;
						}
						$idx = $this->array_verif_unique_name[$idx_arr]['count'];
					}
				}
				if(!$found){
					$this->array_verif_unique_name[]['name'] = $fileOut;
				}
			}
			// Si un idx (explicité, ou calculé via enforceUniqueName), alors il est ajouté au nom du fichier
			if (!empty($idx)){
				$fileOut.='-'.sprintf('%03d',$idx);
			}
			// Si une extension est explicitée, alors elle est ajoutée au nom du fichier
			if(!empty($ext)){
				$fileOut .= ".".$ext;
			}
		}
		
		if (empty($fileOut)) return false; else return $fileOut;
	}

	/**
	 * Copie un movie dans le répertoire de livraison
	 * IN : ligne de type t_panier_doc, ByRef
	 * OUT : fichier copié + 4 nv champs ajouté à la ligne : fileUrl, fileDir, fileName et fileSize. Msg si erreur
	 */

	function copyMovie(&$doc,$idx='') {
		$fileOut=$this->makeFileName($doc,$idx); //calcul du nom du fichier à générer
		if (!$fileOut) {$this->dropError(kErrorLivraisonGenerationNom);return false; }
		//$lieu=Materiel::getLieuByIdMat($doc['MAT']['ID_MAT']);
		$mat=new Materiel();
		$mat->t_mat['ID_MAT']=$doc['MAT']['ID_MAT'];
		$mat->getMat();

		$fileSrc=$mat->getFilePath();
		debug($fileSrc,'red');
	 	if (!eregi('WINNT', PHP_OS)) { //OSX / UNIX, création d'un alias
            // VP 3/11/14 : hard link plutôt que lien symbolique
            link($fileSrc,$this->livraisonDir.$fileOut);
//	 		$cmd="ln -s \"".$fileSrc."\" \"".$this->livraisonDir.$fileOut."\"";
//	 		trace($cmd);
//	 		exec($cmd);
	 		$ok=true; //pas de gestion du retour pour le moment
	 	} else
	 	{ //Windows, on copie le programme... donc éviter windows ;)
	 		$ok=true;
	 		$ok=copy(kVideosDir.$doc['MAT']['ID_MAT'],$this->livraisonDir.$fileOut);
			//Commande FSUTIL à valider ?
	 		//$cmd="fsutil hardlink create \"".$this->livraisonDir.$fileOut."\" \"".$fileSrc."\"";
	 		//trace($cmd);
	 		//exec($cmd);
	 	}

		if (!ok) {
			$this->dropError(kErrorLivraisonEchecCopie);
			return false ;
		} else {
			$doc['fileUrl']=$this->livraisonUrl.$fileOut;
			$doc['fileDir']=$this->livraisonDir.$fileOut;
			$doc['fileName']=$fileOut;
			// VP 8/11/2011 : utilisation de getFileSize à la place de filesize
			//$doc['fileSize']=filesize($this->livraisonDir.$fileOut);
			$doc['fileSize']=getFileSize($fileSrc);
			return true;
		}

	}

	function formatTC($str) {

	   if (substr_count($str,":") > 1) {
	   $lastsemicolon = strrpos($str,":");
	   $str = substr_replace($str,".",$lastsemicolon,1);
	   }
	return $str;

	}

	/**
	 * rend un TC sous la forme mm.ss.cc où cc est un centième de seconde
	 * Format d'entrée de mp3splt
	 */

	function formatTCmp3splt($tc,$timebase=25) {
		$tab_tc = explode(":", $tc);
		return ($tab_tc[0]*60 + $tab_tc[1]).".".$tab_tc[2].".".floor(100*$tab_tc[3]/$timebase);
	}

	/** Découpage d'un movie pour placer une version courte dans le répertoire de livraison
	 * IN : ligne type panier_doc (ByRef), index (servira de prefixe), TCIN et TCOUT
	 * OUT : fichier + 4 champs ajoutés à la ligne fileUrl, fileDir, fileName et fileSize. Msg si erreur.
	 * NOTE : le découpage est effectué en envoyant une ligne de commande à mpgtx.
	 */
	function sliceMovie(&$doc,$idx='',$tcin,$tcout,$offset="00:00:00:00") {

		include_once(modelDir.'model_materiel.php');
		unset($out);
		$fileOut=$this->makeFileName($doc,$idx);
		if (!$fileOut) {$this->dropError(kErrorLivraisonGenerationNom);return false; }

		if (file_exists($this->livraisonDir.$fileOut)) unlink($this->livraisonDir.$fileOut);

		$mat=new Materiel();
		$mat->t_mat['ID_MAT']=$doc['MAT']['ID_MAT'];
		$mat->getMat();

		//$in=Materiel::getLieuByIdMat($doc['MAT']['ID_MAT']).$doc['MAT']['ID_MAT'];
		$in=$mat->getFilePath();
		$out=$this->livraisonDir.$fileOut;
		$log=$this->livraisonDir."fich.log";
		// VP 8/11/2011 : calcul des TC avec Offset pour tous et mémorisaiotn des TC d'origine
		$tcinOri=$tcin;
		$tcoutOri=$tcout;
		$tcin=frameToTC(max(tcToFrame($tcin) - tcToFrame($offset),0));
		$tcout=frameToTC(tcToFrame($tcout) - tcToFrame($offset));
        // Utilisation jobProcess
        global $db;
        $id_engine=$db->GetOne("select id_engine from t_engine,t_module where eng_id_module=id_module and module_nom='cut'");
        if(!empty($id_engine)){
            return $this->sliceMovieByJob($id_engine,$doc,$in,$out,$tcin,$tcout);
        }
		if((strpos($doc['MAT']['MAT_FORMAT'],"MPEG1")!==false)||(strpos($doc['MAT']['MAT_FORMAT'],"MPEG2")!==false)||(strpos($doc['MAT']['MAT_FORMAT'],"MP3")!==false)){
			//MPEG1, MPEG2 et MP3
			/* if (defined('gOffsetMpgtx')){ // cas où le fichier mpeg n'a pas de TC
			 $tcin=frameToTC(tcToFrame($tcin) - tcToFrame($offset));
			 $tcout=frameToTC(tcToFrame($tcout) - tcToFrame($offset));
			 }*/
			// VP 5/05/10 : utilisation de MAT_TCFORCE
			//				if($doc['MAT']['MAT_TCFORCE']=='1' || ($doc['MAT']['MAT_FORMAT']!= "MPEG2" && $doc['MAT']['MAT_FORMAT']!= "MPEG1")){ // cas où le fichier mpeg n'a pas de TC
			//					$tcin=frameToTC(tcToFrame($tcin) - tcToFrame($offset));
			//					$tcout=frameToTC(tcToFrame($tcout) - tcToFrame($offset));
			//				}

			// VP 5/05/10 : intégration mp3splt
			if (strpos($doc['MAT']['MAT_FORMAT'],'MP3')!==false && defined('kMp3SpltPath')) {
				$tcin=$this->formatTCmp3splt($tcin);
				$tcout=$this->formatTCmp3splt($tcout);
				$dir=$this->livraisonDir;
				$fic=stripExtension($fileOut);
				$cmd=kMp3SpltPath." \"".$in."\" -d $dir -o $fic $tcin $tcout  2>".$log;
			}elseif (defined('kDecoupAvidemuxPath')) {
				if(empty($doc['MAT']['MAT_TCFORCE'])){ // cas où le fichier mpeg n'a pas de TC
					$tcin=$tcinOri;
					$tcout=$tcoutOri;
				}
				$cmd=kDecoupAvidemuxPath." \"".$in."\" ".$tcin." ".$tcout."  \"".$out."\" ".$log;
			}elseif(defined('kMPGTXpath')){
				$tcin=$this->formatTC($tcin);
				$tcout=$this->formatTC($tcout);
				$cmd=kMPGTXpath." -s \"".$in."\" [".$tcin."-".$tcout."]  -f -o \"".$out."\" 2>".$log;
			} else {// FFMpeg
                if(getExtension($mat->t_mat['MAT_NOM'])=="mov") $f="-f mov";
                elseif(getExtension($mat->t_mat['MAT_NOM'])=="mp4") $f="-f mp4";
                elseif(getExtension($mat->t_mat['MAT_NOM'])=="mxf") $f="-f mxf";
				elseif(strpos($doc['MAT']['MAT_FORMAT'],"MPEG2")!==false) $f="-f vob";
                // VP 17/09/2012 : utilisation de tcToSecDec
                $duration=tcToSecDec($tcout)-tcToSecDec($tcin);
                // VP 5/04/2013 : utilisation ffmbc
                if(defined("kFFMBCpath")){
                    $cmd = kFFMBCpath;
                }else {
                    $cmd = kFFMPEGpath;
                }
				$cmd.=" -y -i \"".$in."\" $f -vcodec copy -acodec copy -ss ".tcToSecDec($tcin)." -t ".$duration." \"".$out."\"";
			}
		}elseif (getExtension($mat->t_mat['MAT_NOM'])=="mov" || strpos($doc['MAT']['MAT_FORMAT'],'MPEG4')!==false || strpos($doc['MAT']['MAT_FORMAT'],'MPG4')!==false ) {
			// VP 13/07/09 : Decoupage par sliceOfMov pour les fichiers mov
			//$shell="echo DEBUT\n /usr/local/bin/sliceOfMov \"%s\" %s %s %s \"%s\" \"%s\"\necho FIN\n";
			if (defined('kBinDecoupage') && kBinDecoupage=='sliceOfMov' && strpos(PHP_OS,'Darwin')===0  ) {
				// On remplace l'extension par .mov
				$fileOut=stripExtension($fileOut).".mov";
				// VP  11/9/09 : changement log en dossier livraison
				//$cmd=kSliceOfMovpath." \"".$in."\" $offset $tcin $tcout \"".$this->livraisonDir.$fileOut."\" \"".$log."\" \n";
				$cmd=kSliceOfMovpath." \"".$in."\" $offset $tcin $tcout \"".$this->livraisonDir.$fileOut."\" \"".$this->livraisonDir."\" \n";
			} else if (defined('kBinDecoupage') && kBinDecoupage=='mp4box' ) {
				$this->makeFileName($doc);
				//$tcin=floor(tcToSec($tcin) - tcToSec($offset));
				//$tcout=1 + floor(tcToSec($tcout) - tcToSec($offset));
				//$tcout=floor(tcToSec($tcout) - tcToSec($offset));
				$tcin=floor(tcToSec($tcin));
				$tcout=floor(tcToSec($tcout));
				$cmd=kMp4Boxpath." \"".$in."\" -split-chunk ".$tcin.":".$tcout." -out \"".$out."\" 2>\"".$log."\"";
			} else {
                if(getExtension($mat->t_mat['MAT_NOM'])=="mov") $f="-f mov";
                elseif(getExtension($mat->t_mat['MAT_NOM'])=="mp4") $f="-f mp4";
                elseif(getExtension($mat->t_mat['MAT_NOM'])=="mxf") $f="-f mxf";
                elseif(strpos($doc['MAT']['MAT_FORMAT'],"MPEG2")!==false) $f="-f vob";
                // VP 17/09/2012 : utilisation de tcToSecDec
                $duration=tcToSecDec($tcout)-tcToSecDec($tcin);
                // VP 5/04/2013 : utilisation ffmbc
                if(defined("kFFMBCpath")){
                    $cmd = kFFMBCpath;
                }else {
                    $cmd = kFFMPEGpath;
                }
				$cmd.=" -y -i \"".$in."\" $f -vcodec copy -acodec copy -ss ".tcToSecDec($tcin)." -t ".$duration." \"".$out."\"";
			}
		} else {
            if(getExtension($mat->t_mat['MAT_NOM'])=="mov") $f="-f mov";
            elseif(getExtension($mat->t_mat['MAT_NOM'])=="mp4") $f="-f mp4";
            elseif(getExtension($mat->t_mat['MAT_NOM'])=="mxf") $f="-f mxf";
            elseif(strpos($doc['MAT']['MAT_FORMAT'],"MPEG2")!==false) $f="-f vob";
            // VP 17/09/2012 : utilisation de tcToSecDec
            $duration=tcToSecDec($tcout)-tcToSecDec($tcin);
			$cmd=kFFMPEGpath." -y -i \"".$in."\" $f -vcodec copy -acodec copy -ss ".tcToSecDec($tcin)." -t ".$duration." \"".$out."\"";
		}
		trace($cmd);
		exec($cmd);
		// VP 8/11/2011 : utilisation de getFileSize
		if (is_file($this->livraisonDir.$fileOut)) {
			trace('getFileSize OK');
			$doc['fileUrl']=$this->livraisonUrl.$fileOut;
			$doc['fileDir']=$this->livraisonDir.$fileOut;
			$doc['fileName']=$fileOut;
			if (@getFileSize($this->livraisonDir.$fileOut)) {
				$doc['fileSize']=filesize($this->livraisonDir.$fileOut);
			}
			return true;
		}
		else {
			trace('getFileSize NOK');
			$this->dropError(kErrorLivraisonEchecDecoupage);
			return false;
		}

	}


	function sliceMovieByJob($id_engine,&$doc,$in,$out,$tcin,$tcout) {
        require_once(modelDir.'model_engine.php');
		include_once(libDir."class_jobProcess_cut.php");

        // Création fichier XML in
        $file_xml_in=jobInDir."/cut/P0_".date("YmdHis")."_".kDatabaseName."_".microtime(true).".xmltmp";
        $content="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$content.="<data>
<in>".$in."</in>
<out>".$out."</out>
<module>cut</module>
<param>
<tcin>".$tcin."</tcin>
<tcout>".$tcout."</tcout>
<offset>00:00:00:00</offset>
<FOLDER_OUT>".$this->livraisonDir."</FOLDER_OUT>
</param>
</data>";

        $handle=fopen($file_xml_in,'w');
        if (!$handle) {
            $this->dropError(kErrorVisioCreationXML);
            return false;
        }
        fwrite($handle,$content);
        fclose($handle);

        // Instanciation Engine
        $engine=new Engine($id_engine);

        // Lancement job
        $job=new JobProcess_cut(basename($file_xml_in),$engine);
        $job->prepareProcess();
        $job->doProcess();
        $job->finishJob();

        //supression du fichier log
        unlink(jobOutDir.'/cut/'.stripextension(basename($file_xml_in)).'.log');

        $fileOut=basename($out);
        if (@getFileSize($this->livraisonDir.$fileOut)) {
            trace('getFileSize OK');
            $doc['fileUrl']=$this->livraisonUrl.$fileOut;
            $doc['fileDir']=$this->livraisonDir.$fileOut;
            $doc['fileName']=$fileOut;
            $doc['fileSize']=filesize($this->livraisonDir.$fileOut);
            return true;
        }
        else {
            trace('getFileSize NOK');
            $this->dropError(kErrorLivraisonEchecDecoupage);
            return false;
        }

    }
	function transcodeMovie(&$doc,$idx='',$tcin='00:00:00:00',$tcout='00:00:00:00',$format) {
		global $db;
		unset($out);
		// Nom du fichier transcodé
		$fileOut=$this->makeFileName($doc,$idx);
		require_once(modelDir.'model_materiel.php');
		$objMat=new Materiel();
		$objMat->t_mat['ID_MAT']=$doc['MAT']['ID_MAT'];
		$objMat->getMat();
		$ext=$objMat->getExtensionByFormat($format);
		if (empty($format) || empty($ext)) {$this->dropError(kUploadTranscodageErreurFormatNonGere);return false;}
		$fileOut=stripExtension($fileOut).".".$ext;
		if (!$fileOut) {$this->dropError(kErrorLivraisonGenerationNom);return false; }
		if (!is_file(kFFMPEGpath)) {$this->dropError(kUploadTranscodageErreurNoBinary);return false;}

		if (file_exists($this->livraisonDir.$fileOut)) unlink($this->livraisonDir.$fileOut);
		// Récup des paramètres
		$xmlParams=$db->GetOne("SELECT ENCOD_XML from t_encodage_preset WHERE ENCOD_NOM=".$db->Quote($format));
		if(empty($xmlParams)) { $this->dropError(kErrorTranscodageParametres); return false; }
		$tab=xml2array($xmlParams);
		$arrParams=$tab["data"];
    	//Vérif paramètres
    	if (empty($arrParams['vcodec']) && empty($arrParams['acodec']) && empty($arrParams['target']) )
    		{ $this->dropError(kUploadTranscodageErreurNoFormat); return false; }
    	if (empty($arrParams['s_w']) || empty($arrParams['s_h'])) $arrParams['s']=''; else $arrParams['s']=$arrParams['s_w']."x".$arrParams['s_h'];
    	unset($arrParams['s_w']);unset($arrParams['s_h']);

    	//Process ligne de commande
    	$cmd=kFFMPEGpath." -y -i \"".$objMat->getFilePath()."\"";
    	foreach ($arrParams as $_prm=>$_val) {
    		if (!empty($_val)) $cmd.=" -".strtolower($_prm)." ".strtolower($_val).($_prm=='ab' || $_prm=='bt' || $_prm=='vb'?'k':'');
    	}
    	if ($arrParams['vcodec']=='' && empty($arrParams['target'])) $cmd.=" -vn "; //pas de video
    	if ($arrParams['acodec']=='' && empty($arrParams['target'])) $cmd.=" -an "; //pas d'audio
		// Découpage éventuel
		if($tcout!='00:00:00:00' && $tcout!='') $cmd.=" -ss ".$this->formatTC($tcin)." -t ".(tcToSec($tcout)-tcToSec($tcin)+1);

    	$cmd.=" \"".$this->livraisonDir.$fileOut."\"";

    	trace($cmd);
    	exec($cmd); //lancement du transcodage

		if (filesize($this->livraisonDir.$fileOut)) {
			$doc['fileUrl']=$this->livraisonUrl.$fileOut;
			$doc['fileDir']=$this->livraisonDir.$fileOut;
			$doc['fileName']=$fileOut;
			$doc['fileSize']=filesize($this->livraisonDir.$fileOut);
			return true;
		}
		else {
			$this->dropError(kErrorLivraisonEchecDecoupage);
			return false;
		}
	}

	// VP 10/02/10 : ajout fonction catMovie (attention, Mac OS X uniquement)
	function catMovie($list,&$doc,$fileOut="playlist") {
		if (defined('kBinCatMovie') ) {
			$fileOut=$this->removeTrickyChars($fileOut).".mov";
			$doc['fileUrl']=$this->livraisonUrl.$fileOut;
			$doc['fileDir']=$this->livraisonDir.$fileOut;
			$doc['fileName']=$fileOut;
			foreach($list as $file){$catList.=" ".$file."";}
			$cmd=kBinCatMovie." -self-contained ".$catList." -o ".$doc['fileDir']."";
			trace($cmd);
			exec($cmd);
			$doc['fileSize']=filesize($doc['fileDir']);
			return true;
		}else{
			$this->dropError(kErrorVisioNoCatMovie);
			return false;
		}
	}

	function resizeImage(&$doc,$idx='') {
		include_once(modelDir.'model_materiel.php');
		unset($out);
		$fileOut=$this->makeFileName($doc,$idx);
		if (!$fileOut) {$this->dropError(kErrorLivraisonGenerationNom);return false; }

		if (file_exists($this->livraisonDir.$fileOut)) unlink($this->livraisonDir.$fileOut);

		$mat=new Materiel();
		$mat->t_mat['ID_MAT']=$doc['MAT']['ID_MAT'];
		$mat->getMat();

		$in=$mat->getFilePath();
		$out=$this->livraisonDir.$fileOut;
		$log=$this->livraisonDir."fich.log";


		//Valeurs par défaut, modifiées selon les formats
		if(defined("gResolutionImageDPI")) {
			$XresDPI = gResolutionImageDPI;
			$YresDPI = gResolutionImageDPI;
		} else {
			$XresDPI = 300;
			$YresDPI = 300;
		}
		preg_match("/(.*?)[\W]/", $doc['RESIZE_IMG'], $matches);
		$format = $matches[1];
		switch ($format) {
			case "A5" :
				$format_w = "14.8";
				$format_h = "21";
			case "A4" :
				$format_w = "21";
				$format_h = "29.7";
				break;
			case "A3" :
				$format_w = "29.7";
				$format_h = "42";
				break;
			case "Web" :
				$format_w = "";
				$format_h = "";
				$XresDPI = "72";
				$YresDPI = "72";
				break;
			default :
				$format_w = "";
				$format_h = "";
				break;
		}
		//debug($out, "red", true);
        /*** a new imagick object ***/
        $oImage = new Imagick();
        $oImage->pingImage($in);
        $oImage->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
		$oImage->setResolution($XresDPI,$YresDPI);

		$oImage->readImage($in);

		$src_w = $oImage->getImageWidth();
		$src_h = $oImage->getImageHeight();
		//Portrait
		if($src_w < $src_h) {
	        $dst_w=$format_w/2.54*300;
	        $dst_h=$format_h/2.54*300;
			//Paysage
		} else {
	        $dst_w=$format_h/2.54*300;
	        $dst_h=$format_w/2.54*300;
		}
		$dst_h_temp = round(($dst_w / $src_w) * $src_h);
		$dst_w_temp = round(($dst_h / $src_h) * $src_w);
		if($dst_h_temp > $dst_h) $dst_w = $dst_w_temp;
		elseif($dst_w_temp > $dst_w) $dst_h = $dst_h_temp;
        if(!empty($format_w))	$oImage->resizeImage($dst_w, $dst_h, imagick::FILTER_UNDEFINED, 1, 0);

        $oImage->writeImage($out);
		$oImage->destroy();
		//trace($cmd);
		//exec($cmd);
		if (@filesize($this->livraisonDir.$fileOut)) {
			$doc['fileUrl']=$this->livraisonUrl.$fileOut;
			$doc['fileDir']=$this->livraisonDir.$fileOut;
			$doc['fileName']=$fileOut;
			$doc['fileSize']=filesize($this->livraisonDir.$fileOut);
			return true;
		}
		else {
			$this->dropError(kErrorLivraisonEchecDecoupage);
			return false;
		}

	}
	function setArray(&$arr) { //ne pas utiliser, cause des bugs de passage byref

		$this->arrDocs=$arr;

	}

	function removeTrickyChars($str) {
		$str=utf8_decode($str);
		$str= str_replace(array(' ','\'','"','$','%','?',';',':','!','#','|','>','<','/','*','\\','&',"'",'~','(',')','=',',',"\xB0"),'_',$str);
	    $str = strtr($str,
	      "\xA1\xAA\xBA\xBF\xC0\xC1\xC2\xC3\xC5\xC7\xC8\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD1\xD2\xD3\xD4\xD5\xD8\xD9\xDA\xDB\xDD\xE0\xE1\xE2\xE3\xE5\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF1\xF2\xF3\xF4\xF5\xF8\xF9\xFA\xFB\xFD\xFF",
	       "!ao?AAAAACEEEEIIIIDNOOOOOUUUYaaaaaceeeeiiiidnooooouuuyy");
 		$str = strtr($str, array("\xC4"=>"Ae", "\xC6"=>"AE", "\xD6"=>"Oe", "\xDC"=>"Ue", "\xDE"=>"TH", "\xDF"=>"ss", "\xE4"=>"ae", "\xE6"=>"ae", "\xF6"=>"oe", "\xFC"=>"ue", "\xFE"=>"th"));

                //B.RAVI 2015-08-27 on enlève les caractères qui risquent d'être remplacés par un losange noire avec un point d'interrogation car n'existant pas en iso (cf. utf8_decode au dessus)
                $tab_str=str_split($str);
                $str='';
                foreach ($tab_str as $v){
//                    echo 'DEBUG :  '.$v.' '.ord($v).'<br />';
                    if (ord($v)>122){
                        $str.='_';
                    }else{
                        $str.=$v;
                    }
                }
		return $str;
	}

	function setDir($str,$dir_liv='',$url_liv='') {
		$this->idDir=$this->removeTrickyChars($str);

		if (isset($dir_liv) && !empty($dir_liv))
		{
			$this->livraisonDir=$dir_liv.$this->idDir.'/';
		}
		else
		{
			if (defined('klivraisonRepertoire'))
				$this->livraisonDir=klivraisonRepertoire.$this->idDir.'/';
			else
				$this->livraisonDir=kCheminLocalMedia.'/livraison/'.$this->idDir.'/';
		}

		if (isset($url_liv) && !empty($url_liv))
		{
			$this->livraisonUrl=$url_liv.$this->idDir.'/';
		}
		else
		{
			if (defined('kLivraisonUrl'))
				$this->livraisonUrl=kLivraisonUrl.$this->idDir.'/';
			else
				$this->livraisonUrl=kCheminHttpMedia.'/livraison/'.$this->idDir.'/';
		}
		//debug($this->livraisonDir.' >>> '.$this->livraisonUrl,'cyan');
	}

	function dropError($errLib) { //: A ETOFFER EN TRY CATCH ?
		$this->error_msg.=$errLib."<br/>";
	}

	function xml_export($entete=0,$encodage=0,$prefix=""){
		// VP 13/07/09 : ajout balises ID_PANIER et ID_LIGNE_PANIER

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.=$prefix."<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

        $content.=$prefix."<t_livraison>\n";

            $content.=$prefix."\t<LIVRAISON_DIR>".$this->livraisonDir."</LIVRAISON_DIR>\n";
            $content.=$prefix."\t<LIVRAISON_URL>".$this->livraisonUrl."</LIVRAISON_URL>\n";
            $content.=$prefix."\t<ID_PANIER>".$this->arrDocs[0]['ID_PANIER']."</ID_PANIER>\n";


        	foreach ($this->arrDocs as $idx=>$tab_value){

            	$content.=$prefix."\t<t_livraison_doc id_ligne_panier='".$tab_value['ID_LIGNE_PANIER']."'>\n";
                foreach ($tab_value as $name => $value) {

					// on ne garde que les champs dédiés livraison (FILExxx) sinon ca fait un duplicata des lignes panier_doc
					if (strpos($name,'file')===0) $content .=$prefix. "\t\t<".strtoupper($name).">".str_replace($search,$replace, $value)."</".strtoupper($name).">\n";
                    //$content .=$prefix. "\t\t<".strtoupper($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".strtoupper($name).">\n";
                    }
                $content.=$prefix."\t</t_livraison_doc>\n";
        	}
        $content.=$prefix."</t_livraison>\n";

        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }

/**
 * Ajout de tags aux fichiers générés (id3, image, etc)
 * IN : tableau de classe des "livrables" arrDocs
 * OUT : fichiers mis à jour
 * Ceci est particulièrement utile pour les podcast ou les livraisons enrichies
 * NOTE : en raison des informations traitées et des manipulations de fichier, ce traitement
 * 	est assez lourd.
 * NOTE2 : ce traitement doit être lancé après doLivraison, il est appliqué au fichier généré, pas au fichier source
 */
	function tagFile() {
		require_once(modelDir.'model_doc.php');
		require_once(libDir.'getid3/getid3/getid3.php');

		debug($this->arrDocs,'beige');

		foreach ($this->arrDocs as $idx=>$doc) {
			if (empty($doc['fileDir']) && @filesize($doc['fileDir'])==0) continue; //fichier de sortie pas généré ? on abandonne

			$_doc=new Doc();
			$_doc->t_doc['ID_DOC']=$doc['ID_DOC'];
			$_doc->t_doc['ID_LANG']=$doc['ID_LANG']?$doc['ID_LANG']:$_SESSION['langue'];
			$_doc->getDocFull(1,1); //récupération des infos + lex + val
			$_doc->getPersonnes(); // récupération des personnes
			$_doc->getVignette(); //et de la vignette

			//export xml puis transfo par la xsl de mapping des champs entre structure t_doc & "norme" ID3
			$xmlDoc=$_doc->xml_export();
			$xmlDoc=str_replace("||","\n",$xmlDoc);
			$param=array('filename'=>stripExtension($doc['fileDir']),'idx'=>$idx);
			//debug ($xmlDoc);
			$data = TraitementXSLT($xmlDoc,getSiteFile("designDir","print/tagLivraison.xsl"),$param,0);

			$arrMap=xml2tab($data); //on transforme le xml de sortie en tableau


			//Init tag writer
			$getID3 = new getID3;
			$getID3->setOption(array('encoding'=>'UTF-8'));
			getid3_lib::IncludeDependency(GETID3_INCLUDEPATH.'write.php', __FILE__, true);


			switch (strtolower($doc['MAT']['MAT_FORMAT'])) { //classes de tags à procéder selon format
			case 'mp3':
			case 'mp2':
			case 'mp1':
				$ValidTagTypes = array('id3v1', 'id3v2.3', 'ape');
				break;

			case 'mpc':
				$ValidTagTypes = array('ape');
				break;

			case 'ogg':
				$ValidTagTypes = array('vorbiscomment');
				break;

			case 'flac':
				$ValidTagTypes = array('metaflac');
				break;

			case 'real':
				$ValidTagTypes = array('real');
				break;

			default:
				$ValidTagTypes = array();
				break;
			}

			$tagwriter = new getid3_writetags;
			$tagwriter->filename       = $doc['fileDir'];
			$tagwriter->tagformats     = $ValidTagTypes;
			$tagwriter->overwrite_tags = true;
			$tagwriter->tag_encoding   = 'UTF-8';
			$tagwriter->remove_other_tags = true;

			//traitement des champs pour le tagging
			foreach ($arrMap['DATA'][0] as $fld=>$val) { //boucle sur les champs existant
				if (!is_array($val)) $TagData[strtolower($fld)][]=$val;	 //champs simples
				else { //images : $val est un tableau de n paires src (url image) / type (type image)
					foreach ($val as $idx_im=>$picture) { //pour chaque image
						debug($val,'khaki');
					//$_a=split("media",$picture['SRC']);
						//PC 17/10/11 - corretion d'un bug sur les images
						$img_src = $picture['SRC'];
						if ($_doc->vignette!='' && file_exists(kCheminLocalMedia.$img_src) ) {
							if (in_array('id3v2.4', $tagwriter->tagformats) || in_array('id3v2.3', $tagwriter->tagformats) || in_array('id3v2.2', $tagwriter->tagformats)) {
								if ($fd = @fopen(kCheminLocalMedia.$img_src, 'rb')) {
									$APICdata = fread($fd, filesize(kCheminLocalMedia.$img_src));
									fclose ($fd);

									list($APIC_width, $APIC_height, $APIC_imageTypeID) = GetImageSize(kCheminLocalMedia.$img_src);
									$imagetypes = array(1=>'gif', 2=>'jpeg', 3=>'png');
									if (isset($imagetypes[$APIC_imageTypeID])) {

										$TagData[strtolower($fld)][$idx_im]['data']          = $APICdata;
										$TagData[strtolower($fld)][$idx_im]['picturetypeid'] = $picture['TYPE'];
										$TagData[strtolower($fld)][$idx_im]['description']   = kCheminLocalMedia.$img_src;
										$TagData[strtolower($fld)][$idx_im]['mime']          = 'image/'.$imagetypes[$APIC_imageTypeID];
									}
								}
							}
						}
					}
				}
			} //fin boucle
			$tagwriter->tag_data = $TagData;

			$tagwriter->WriteTags(); //écriture des tags dans le fichier et raz des variables
			unset($_doc);
			unset($tagwriter);
			unset($getID3);
			unset($TagData);
			unset($arrMap);

		} //fin boucle sur les fichiers
	}



} //fin classe
?>
