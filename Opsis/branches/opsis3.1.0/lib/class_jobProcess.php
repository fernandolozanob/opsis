<?php

require_once('exception/exception_fileNotFoundException.php');
require_once('exception/exception_xmlInNotFoundException.php');
require_once('exception/exception_mediaNotFoundException.php');
require_once('exception/exception_invalidXmlParamException.php');
require_once('exception/exception_directoryException.php');
require_once('exception/exception_fileException.php');
require_once('exception/exception_engineException.php');

require_once(libDir.'fonctionsGeneral.php');

abstract class JobProcess
{
	private $module_name;
	private $syncJobProcess;
	
	private $in_xml_path;
	protected $tmp_dir_path;
	
	private $in_xml_data;
	private $jobObj;
	private $local_out_log ;
	
	private $file_in_path;
	private $file_out_path;
	protected $params_job;
	
	private $pourcentage_progression;
	
	protected $required_xml_params;
	protected $optional_xml_params;
	
	private $engine;
	
	private $pid;
	private $last_prog;
	protected $exitCode;
	
	function __construct($syncJobProcess = null )
	{
		$this->module_name='none';
		
		$this->in_xml_path='';
		$this->tmp_dir_path='';
		
		$this->in_xml_data='';
		
		$this->jobObj=null;
		$this->local_out_log = "" ;
		
		$this->file_in_path='';
		$this->file_out_path='';
		$this->params_job=array();
		
		$this->pourcentage_progression=0;
		
		$this->required_xml_params=array();
		$this->optional_xml_params=array();
		
		$this->engine=null;
		$this->syncJobProcess=$syncJobProcess;
		
		setImagickPath();
	}
	
	public static function getJobprocess($type_process,$input_job=null,$engine=null,$syncJobProcess=null){
		
		if(is_numeric($engine)){
			require_once(modelDir.'model_engine.php');
			$id_engine = $engine ;
			$engine = new Engine($id_engine);
		}else if ($engine == null){
			require_once(modelDir.'model_engine.php');
			$engine = new Engine() ; 
			$engine->getEngineFromModule($type_process);
		}
		
		switch($type_process)
		{
			case 'audiowaveform':
				require_once(libDir."class_jobProcess_audiowaveform.php");
				$job=new JobProcess_audiowaveform($input_job,$engine,$syncJobProcess);
				break;
			case 'authot':
				require_once(libDir."class_jobProcess_authot.php");
				$job=new JobProcess_authot($input_job,$engine,$syncJobProcess);
				break;
			case 'backup':
				require_once(libDir."class_jobProcess_backup.php");
				$job=new JobProcess_backup($input_job,$engine,$syncJobProcess);
				break;
			case 'cut':
				require_once(libDir."class_jobProcess_cut.php");
				$job=new JobProcess_cut($input_job,$engine,$syncJobProcess);
				break;
			case 'dailymotion':
				require_once(libDir."class_jobProcess_dailymotion.php");
				$job=new JobProcess_dailymotion($input_job,$engine,$syncJobProcess);
				break;
			case 'dmcloud':
				require_once(libDir."class_jobProcess_dmcloud.php");
				$job=new JobProcess_dmcloud($input_job,$engine,$syncJobProcess);
				break;
			case 'dvd':
				require_once(libDir."class_jobProcess_dvd.php");
				$job=new JobProcess_dvd($input_job,$engine,$syncJobProcess);
				break;
			case 'episode':
				require_once(libDir."class_jobProcess_episode.php");
				$job=new JobProcess_episode($input_job,$engine,$syncJobProcess);
				break;
			case 'facebook':
				require_once(libDir."class_jobProcess_facebook.php");
				$job=new JobProcess_facebook($input_job,$engine,$syncJobProcess);
				break;
			case 'ffmbc':
				require_once(libDir."class_jobProcess_ffmbc.php");
				$job=new JobProcess_ffmbc($input_job,$engine,$syncJobProcess);
				break;
			case 'ffmpeg':
				require_once(libDir."class_jobProcess_ffmpeg.php");
				$job=new JobProcess_ffmpeg($input_job,$engine,$syncJobProcess);
				break;
			case 'ftp':
				require_once(libDir."class_jobProcess_ftp.php");
				$job=new JobProcess_ftp($input_job,$engine,$syncJobProcess);
				break;
			case 'imagick':
				require_once(libDir."class_jobProcess_imagick.php");
				$job=new JobProcess_imagick($input_job,$engine,$syncJobProcess);
				break;
			case 'kewego':
				require_once(libDir."class_jobProcess_kewego.php");
				$job=new JobProcess_kewego($input_job,$engine,$syncJobProcess);
				break;
			case 'mediaspeech':
				require_once(libDir."class_jobProcess_mediaspeech.php");
				$job=new JobProcess_mediaspeech($input_job,$engine,$syncJobProcess);
				break;
			case 'ocr':
				require_once(libDir."class_jobProcess_ocr.php");
				$job=new JobProcess_ocr($input_job,$engine,$syncJobProcess);
				break;
			case 'record':
				require_once(libDir."class_jobProcess_record.php");
				$job=new JobProcess_record($input_job,$engine,$syncJobProcess);
				break;
			case 'soundcloud':
				require_once(libDir."class_jobProcess_soundcloud.php");
				$job=new JobProcess_soundcloud($input_job,$engine,$syncJobProcess);
				break;
			case 'story':
				require_once(libDir."class_jobProcess_story.php");
				$job=new JobProcess_story($input_job,$engine,$syncJobProcess);
				break;
			case 'storyimg':
				require_once(libDir."class_jobProcess_storyimg.php");
				$job=new JobProcess_storyimg($input_job,$engine,$syncJobProcess);
				break;
			case 'storypdf':
				require_once(libDir."class_jobProcess_storypdf.php");
				$job=new JobProcess_storypdf($input_job,$engine,$syncJobProcess);
				break;
			case 'streamz':
				require_once(libDir."class_jobProcess_streamz.php");
				$job=new JobProcess_streamz($input_job,$engine,$syncJobProcess);
				break;
			case 'unoconv':
				require_once(libDir."class_jobProcess_unoconv.php");
				$job=new JobProcess_unoconv($input_job,$engine,$syncJobProcess);
				break;
			case 'webservice':
				require_once(libDir."class_jobProcess_webservice.php");
				$job=new JobProcess_webservice($input_job,$engine,$syncJobProcess);
				break;
			case 'youtube':
				require_once(libDir."class_jobProcess_youtube.php");
				$job=new JobProcess_youtube($input_job,$engine,$syncJobProcess);
				break;
			case 'zip':
				require_once(libDir."class_jobProcess_zip.php");
				$job=new JobProcess_zip($input_job,$engine,$syncJobProcess);
				break;
			default : 
				return false ; 
		}
		
		return $job ; 
	}
	
	
	
	protected function setModuleName($name)
	{
		$this->module_name=$name;
		
		// creation des repertoires pour les differents module
		if (!file_exists(jobInDir.'/'.$this->module_name))
		{
			if (!mkdir(jobInDir.'/'.$this->module_name))
				throw new DirectoryException('failed to create directory ('.jobInDir.'/'.$this->module_name.')');
		}
		
		if (!file_exists(jobRunDir.'/'.$this->module_name))
		{
			if (!mkdir(jobRunDir.'/'.$this->module_name))
				throw new DirectoryException('failed to create directory ('.jobRunDir.'/'.$this->module_name.')');
		}
		
		if (!file_exists(jobDoneDir.'/'.$this->module_name))
		{
			if (!mkdir(jobDoneDir.'/'.$this->module_name))
				throw new DirectoryException('failed to create directory ('.jobDoneDir.'/'.$this->module_name.')');
		}
		
		if (!file_exists(jobOutDir.'/'.$this->module_name))
		{
			if (!mkdir(jobOutDir.'/'.$this->module_name))
				throw new DirectoryException('failed to create directory ('.jobOutDir.'/'.$this->module_name.')');
		}
		
		if (!file_exists(jobTmpDir.'/'.$this->module_name))
		{
			if (!mkdir(jobTmpDir.'/'.$this->module_name))
				throw new DirectoryException('failed to create directory ('.jobTmpDir.'/'.$this->module_name.')');
		}
		
		if (!file_exists(jobCancelDir.'/'.$this->module_name))
		{
			if (!mkdir(jobCancelDir.'/'.$this->module_name))
				throw new DirectoryException('failed to create directory ('.jobCancelDir.'/'.$this->module_name.')');
		}
		
		if (!file_exists(jobErrorDir.'/'.$this->module_name))
		{
			if (!mkdir(jobErrorDir.'/'.$this->module_name))
				throw new DirectoryException('failed to create directory ('.jobErrorDir.'/'.$this->module_name.')');
		}
	}
	
	protected function getModuleName()
	{
		return $this->module_name;
	}
	
	public function prepareProcess()
	{
		if(isset($this->in_xml_path)&&!empty($this->in_xml_path)){
			//creation du fichier de log du job
			file_put_contents(jobOutDir.'/'.$this->getModuleName().'/'.stripextension(basename($this->in_xml_path)).'.log', '');
		}
		
		$this->parseXml();
		
		if(isset($this->in_xml_path)&&!empty($this->in_xml_path)){
			// deplacement le XML de jobIn vers le run du module.
			$in_xml_path_dest=jobRunDir.'/'.$this->getModuleName().'/'.$this->engine->getNomServeur().'/'.basename($this->in_xml_path);
			
			if(rename($this->in_xml_path,$in_xml_path_dest)===false)
				throw new FileNotFoundException('Failed to copy file in pending directory');
			
			$this->in_xml_path=jobRunDir.'/'.$this->getModuleName().'/'.$this->engine->getNomServeur().'/'.basename($this->in_xml_path);
		}
		
		if(isset($this->in_xml_path)&&!empty($this->in_xml_path)){
			$this->tmp_dir_path=jobTmpDir.'/'.$this->getModuleName().'/'.stripextension(basename($this->in_xml_path)).'/';
		}else if (isset($this->jobObj) && !empty($this->jobObj) ){
			$this->tmp_dir_path = jobTmpDir.'/'.$this->getModuleName().'/P'.$this->jobObj->t_job["JOB_PRIORITE"]."_".str_replace(array('-',' ',':'),'',$this->jobObj->t_job["JOB_DATE_CREA"])."_".kDatabaseName."_".sprintf('%08d',$this->jobObj->t_job["ID_JOB"])."/";
		}else {
			$this->tmp_dir_path = jobTmpDir.'/'.$this->getModuleName().'/'.random_alphanum_string().'/';
		}

		// creation repertoire temporaire
		if (!file_exists($this->tmp_dir_path))
		{
			if (mkdir($this->tmp_dir_path)===false)
				throw new DirectoryException('failed to create tmp directory ('.$this->tmp_dir_path.')');
		}
	}
	
	protected function setInput($input_job){
		if(is_a($input_job,'Job')){
			$this->jobObj = $input_job ; 
			if(!isset($this->jobObj->full_xml_job_process)){
				$this->jobObj->makeXML() ; 
			}
			$this->in_xml_data = $this->jobObj->full_xml_job_process;
		}else if(is_string($input_job) && strpos($input_job,'.xml') !== false){
			$this->setXmlFile($input_job);
		}
	}
	
	
	protected function setXmlFile($file)
	{
		$this->in_xml_path=jobInDir.'/'.$this->getModuleName().'/'.$file;
		// test existance fichier xml entrée
		if (!file_exists($this->in_xml_path))
			throw new XmlInNotFountException($this->in_xml_path);
		
		$this->in_xml_data=file_get_contents($this->in_xml_path);

	}
	
	protected function parseXml()
	{
		$tab_xml=xml2tab($this->in_xml_data);
		$tab_xml=JobProcess::arrayChangeKeyCaseRecursive($tab_xml);
		$this->file_in_path=$tab_xml['data'][0]['in'];
		$this->file_out_path=$tab_xml['data'][0]['out'];
		
		$this->params_job=$tab_xml['data'][0]['param'][0];
		$this->inXmlCheck();
	}
	
	protected function inXmlCheck($checkInOutput = true)
	{
		//verification de l'existence du fichier d'entree
		//si vide dans le cas de record par exemple
		if (isset($this->file_in_path) && !empty($this->file_in_path))
		{
			if ((!isset($this->params_job['job_without_mat']) || empty($this->params_job['job_without_mat'])) && !file_exists($this->file_in_path))
				throw new MediaNotFoundException('(Input file) '.$this->file_in_path);
			
			if ($checkInOutput && $this->file_in_path==$this->file_out_path)
				throw new FileException('Input and output are the same');
		}
		// verification des parametres et de leurs types
		if (!empty($this->required_xml_params))
		{
			foreach($this->required_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
				{
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
				}
				else if (isset($this->params_job[$param_name]) && empty($this->params_job[$param_name]))
					throw new InvalidXmlParamException($param_name.' is empty');
				else
					throw new InvalidXmlParamException($param_name.' is not set');
			}
		}
		
		
		// verification des parametres et de leurs types
		if (!empty($this->optional_xml_params))
		{
			foreach($this->optional_xml_params as $param_name=>$type)
			{
				if (isset($this->params_job[$param_name]) && !empty($this->params_job[$param_name]))
					$this->params_job[$param_name]=$this->checkParamType($this->params_job[$param_name],$param_name,$type);
			}
		}
	}
	
	protected function setEngine($eng)
	{
		if (isset($eng) && $eng!=null)
		{
			$this->engine=$eng;
			
			// on verifie que l'engine est bien compatible avec le module
			if ($this->engine->getModuleName()!=$this->getModuleName())
				throw new EngineException('this engine is not compatible with the module');
			
			// on verifie que le repertoire de l'engine existe bien
			if (!file_exists(jobRunDir.'/'.$this->getModuleName().'/'.$this->engine->getNomServeur()))
			{
				if (!mkdir(jobRunDir.'/'.$this->getModuleName().'/'.$this->engine->getNomServeur()))
					throw new DirectoryException('failed to create engine directory ('.jobRunDir.'/'.$this->getModuleName().'/'.$this->engine->getNomServeur().')');
			}
		}
	}
	
	protected function getEngine()
	{
		return $this->engine;
	}
	
	public function getFileInPath()
	{
		return $this->file_in_path;
	}
	
	public function getFileOutPath()
	{
		return $this->file_out_path;
	}
	
	public function getPidFilePath()
	{
		return jobRunDir.'/'.$this->getModuleName().'/'.stripExtension(basename($this->in_xml_path)).'.pid';
	}
	
	public function getTmpDirPath()
	{
		return $this->tmp_dir_path;
	}
	
	public function getInXmlPath()
	{
		return $this->in_xml_path;
	}
	
	//si dans paramjob, patern inherit_, on le remplace par sa valeur
	protected function getXmlParams()
	{
	  $keys = implode(',', array_keys($this->params_job));
	  $keys = str_replace('inherit_', '', $keys);
	  $this->params_job=array_combine(explode(',', $keys), array_values($this->params_job));
	  return $this->params_job;

	}
	
	// verifie que le type des parametres est correct et retourne la valeur de ce parametre dans la bon type
	protected function checkParamType($param_value,$param_name,$type,$addslashes=true)
	{
		switch ($type)
		{
			case 'int':
				if (!is_numeric($param_value))
					throw new InvalidXmlParamException($param_name.' not an int');
				
				return intval($param_value);
				break;
			case 'string':
				if (!is_string($param_value))
					throw new InvalidXmlParamException($param_name.' not a string');
					
				if($addslashes) return addslashes($param_value);
                else return $param_value;
				break;
			case 'timecode':
				if (!isTc($param_value))
					throw new InvalidXmlParamException($param_name.' is not a timecode');
				
				return $param_value;
				break;
			case 'file':
				if (!is_string($param_value))
					throw new InvalidXmlParamException($param_name.' not a file path');
				
				if (!file_exists($param_value))
					throw new FileNotFoundException($param_name.' ('.$param_value.'): file not found');
				
				return $param_value;
				break;
			case 'array';
				if (!is_array($param_value))
					throw new InvalidXmlParamException($param_name.' not an array');
				
				return $param_value;
				break;
			default:
				throw new ParamTypeException('Param type not found');
				break;
		}
	}
	
	public function checkIfFinished()
	{
	}
	
	public function finishJob()
	{
		if (file_exists($this->tmp_dir_path.'/stdout.txt'))
		{
			if (unlink($this->tmp_dir_path.'/stdout.txt')===false)
				throw new FileException('Failed to delete file ('.$this->tmp_dir_path.'/stdout.txt)');
		}
		
		if (file_exists($this->tmp_dir_path.'/stderr.txt'))
		{
			if (unlink($this->tmp_dir_path.'/stderr.txt')===false)
				throw new FileException('Failed to delete file ('.$this->tmp_dir_path.'/stderr.txt)');
		}
		
        // Attention s'il reste des fichiers dans $this->tmp_dir_path, rmdir($this->tmp_dir_path) va échouer
        $list_hidden_files = list_directory($this->tmp_dir_path);
        if (!empty($list_hidden_files)) {
            $this->writeOutLog('repertoire non vide (' . $this->tmp_dir_path . ')' . print_r($list_hidden_files, true));

            foreach ($list_hidden_files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
		if(isset($this->params_job['remove_src_from_livraison']) && !empty($this->params_job['remove_src_from_livraison']) && $this->params_job['remove_src_from_livraison'] == 'true'){
			// MS - 26.09.16 - Dans certains cas on veut pouvoir remove le fichier source (lors d'encodages en plusieurs jobs par ex)
			// Afin d'éviter toute mauvaise surprise, pour l'instant on ne peut supprimer un fichier source que si il se trouve dans le meme repertoire que le fichier de sortie (=> cohérent avec le cas livraison, à faire evoluer eventuellement)
			if(file_exists($this->getFileOutPath()) && file_exists(dirname($this->getFileOutPath()).'/'.basename($this->getFileInPath()))){
				unlink(dirname($this->getFileOutPath()).'/'.basename($this->getFileInPath()));
				$this->writeJobLog("finishJob - remove_src_from_livraison - unlink ".dirname($this->getFileOutPath()).'/'.basename($this->getFileInPath()));
			}
		}
        
                
		if (jobProcess::supRep($this->tmp_dir_path)===false){
            throw new DirectoryException('failed to delete tmp directory ('.$this->tmp_dir_path.')');
        }
		
		if(!empty($this->in_xml_path) && file_exists($this->in_xml_path)){
			//deplacement du XML de run vers done du module
			$in_xml_path_dest=jobDoneDir.'/'.$this->getModuleName().'/'.basename($this->in_xml_path);
			if(rename($this->in_xml_path,$in_xml_path_dest)===false)
				throw new FileNotFoundException('Failed to copy file in done directory');
			$this->in_xml_path=jobDoneDir.'/'.$this->getModuleName().'/'.basename($this->in_xml_path);
		}else if (!empty($this->jobObj)){
			$this->jobObj->updateJob() ; 
		}
	}
	
	public function shellExecute($prog,$options,$bloquant=true)
	{
		/*if (!file_exists($prog))
			throw new FileNotFoundException($prog.' not found');*/

		$this->last_prog=$prog;

		if(isset($this->exec_to_pipe) && $this->exec_to_pipe){
			$pipe_descriptor=array
			(
				0 => array('pipe', 'r'),
				1 => array('pipe', 'w'),
				2 => array('file',$this->tmp_dir_path.'/stderr.txt', 'w')
			);
		}else{
			$pipe_descriptor=array
			(
				0 => array('pipe', 'r'),
				1 => array('file',$this->tmp_dir_path.'/stdout.txt', 'w'),
				2 => array('file',$this->tmp_dir_path.'/stderr.txt', 'w')
			);
		}

		if ($this->engine->getIpAdress()!='localhost' && $this->engine->getIpAdress()!='127.0.0.1')
		{
			$cmd=$prog.' '.$options;
			$cmd='ssh '.$this->engine->getIpAdress().' "'.str_replace(array('\"','"'),array('\\\"','\"'),$cmd).'"';

			if(!isset($this->exec_to_pipe)){
				if ($bloquant===true)
					$cmd=$cmd;
				else
					$cmd=$cmd.' >/dev/null & echo $!';
			}
			// execution avec ssh
			/*$cmd=$prog.' '.$options;
			$cmd='ssh '.$this->engine->getIpAdress().' "'.str_replace('"','\"',$cmd).' 1>'.$this->tmp_dir_path.'/stdout.txt 2>'.$this->tmp_dir_path.'/stderr.txt " & echo $!';

			JobProcess::writeJobLog($cmd);
			$this->pid=shell_exec($cmd);*/

		}
		else
		{
			if ($bloquant===true)
				$cmd=$prog.' '.$options;
			else
				if (eregi('WINNT', PHP_OS))
					$cmd=kPowershellPath.' -command "$cmd = Start-Process \''.$prog.'\' \''.$options.'\' -NoNewWindow -passthru; Write-Host $cmd.id"';
				else{
					if(isset($this->exec_to_pipe)){
						$cmd=$prog.' '.$options; // .' > /dev/null & echo $!';  
					}else{
						$cmd=$prog.' '.$options.' > /dev/null & echo $!';  
					}
				}
		}
		JobProcess::writeJobLog($cmd);

		// execution avec proc_open voir si on peut recuperer le pid
		$this->proc=proc_open($cmd,$pipe_descriptor,$this->exec_pipes);
		
		if ($bloquant===true)
		{
			$sExitCode = proc_close($this->proc);
			$this->exitCode = $sExitCode;
		}
		else
		{
			sleep(1);
			if(isset($this->exec_to_pipe)){
				$status = proc_get_status($this->proc);
				if(isset($status['pid'])){
					$this->pid=$status['pid'];
				}
			}else{
				$this->pid=trim(file_get_contents($this->tmp_dir_path.'/stdout.txt'));
			}
		}
		
		// MS - on retourne le contenu de stdout => le retour de la commande executée 
		return file_get_contents($this->tmp_dir_path.'/stdout.txt');
	}
	
	public function getProgression()
	{
		return $this->pourcentage_progression;
	}
	
	public function getPid()
	{
        if(empty($this->pid) && is_file($this->tmp_dir_path.'/stdout.txt')){
            $this->pid=intval(file_get_contents($this->tmp_dir_path.'/stdout.txt'));
        }
		return $this->pid;
	}
	
	protected function setProgression($progression,$etat='')
	{
		$this->pourcentage_progression=intval($progression);
		
		$text_etat='';
		if (!empty($etat))
			$text_etat=' - '.$etat;
		
		if (!empty($this->pid))
			$text_etat.=' - '.$this->pid.' - '.$this->last_prog;
		
		if (!empty($this->jobObj)){
			$this->jobObj->updateJob() ; 
		}
		
		$this->writeOutLog('Progression '.$this->pourcentage_progression.'%'.$text_etat);
	}
	
	public function dropError($message)
	{
		$this->writeOutLog('Erreur : '.$message);
		
		if(isset($this->in_xml_path) && !empty($this->in_xml_path)){
			// copie du XML vers le dossier erreur
			$in_xml_path_dest=jobErrorDir.'/'.$this->getModuleName().'/'.basename($this->in_xml_path);
			
			if(rename($this->in_xml_path,$in_xml_path_dest)===false)
				throw new FileNotFoundException('Failed to copy file in error directory');
			
			$this->in_xml_path=jobErrorDir.'/'.$this->getModuleName().'/'.basename($this->in_xml_path);
		}else if (!empty($this->jobObj)){
			$this->jobObj->updateJob() ; 
		}
		// on supprime le repertoire temporaire
		JobProcess::supRep($this->tmp_dir_path);
	}
	
	protected function getOutLogPath()
	{
		return jobOutDir.'/'.$this->getModuleName().'/'.stripextension(basename($this->in_xml_path)).'.log';
	}
	
	protected function writeOutLog($message)
	{
		$message=date("Y-m-d H:i:s").' '.$message."\n";
		if(isset($this->in_xml_path) && !empty($this->in_xml_path) && file_exists($this->getOutLogPath())){
			file_put_contents(jobOutDir.'/'.$this->getModuleName().'/'.stripextension(basename($this->in_xml_path)).'.log', $message,FILE_APPEND);
		}else{
			$this->local_out_log .= $message;
			if(isset($this->jobObj) && !empty($this->jobObj)){
				$this->jobObj->local_out_log = $this->local_out_log;
			}
		}
	}
	
	public static function writeJobLog($message)
	{
        if(defined("jobLogFile")){
            $message=date("Y-m-d H:i:s").' '.$message."\n";
            file_put_contents(jobLogFile, $message,FILE_APPEND);
        }else{
            trace($message);
        }
	}
	
	private static function arrayChangeKeyCaseRecursive($input,$case=CASE_LOWER)
	{
		if(!is_array($input))
			return false;
	
		if(!in_array($case, array(CASE_UPPER, CASE_LOWER)))
			return false;
		
		$input = array_change_key_case($input, $case);
		
		foreach($input as $key=>$val)
		{
			if(is_array($val))
				$input[$key] = JobProcess::arrayChangeKeyCaseRecursive($val, $case);
		}
		
		return $input;
	}
	
	public static function supRep($rep)
	{
		if (is_dir($rep) && ($rep_a_supprimer=opendir($rep))!==true)
		{
			while (($fichier_a_suppr=readdir($rep_a_supprimer))!==false)
			{
				if ($fichier_a_suppr!='.' && $fichier_a_suppr!='..')
				{
					if (is_dir($rep.'/'.$fichier_a_suppr))
						JobProcess::supRep($rep.'/'.$fichier_a_suppr);
					else
						unlink($rep.'/'.$fichier_a_suppr);
				}
			}
			
			closedir($rep_a_supprimer);
			
			// SUPPRESSION DES FICHIER .nfs (ex : .nfs000000003c1f000200000057)
			$list_hidden_files=list_directory($rep);
			if (!empty($list_hidden_files))
			{
				foreach ($list_hidden_files as $file)
				{
					if (file_exists($file))
					{
						trace('SUPRRESSION FICHIER .NFS : '.$file);
						unlink($file);
					}
				}
			}
			
			$rmdir_return=rmdir($rep);
			
			if ($rmdir_return){
				trace('jobProcess::supRep rmdir suppr OK '.$rep);
				return true;
			}else{
				trace('jobProcess::supRep rmdir suppr NOK '.$rep);
				return false;
			}
		}
	}
	
	public static function kill($module,$xml_file)
	{
		$job_name=stripExtension($xml_file);
		
		$pid_file = jobRunDir.'/'.$module.'/'.$job_name.'.pid';
		
		if(is_file($pid_file)){
			$contenu=file_get_contents(jobRunDir.'/'.$module.'/'.$job_name.'.pid');

			$contenu=explode("\n",$contenu);
			$pid=$contenu[0];
			$id_engine=$contenu[1];
			
			//  on tue le processus parent (jobProcess)
			// si on tue les processus fils en premier le job va continuer.
			JobProcess::writeJobLog('Arret du job parent (jobProcess) : '.$pid);
			if(!empty($pid)){
				if(eregi('WINNT',PHP_OS))
					shell_exec('powershell -command "stop-process '.$pid.' -Force "');
				else
					shell_exec('kill -9 '.$pid.'');
			}
			// lecture du log pour tuer le processus fils
			$data=file_get_contents(jobOutDir.'/'.$module.'/'.$job_name.'.log');
			preg_match_all('/(.*) Progression (.*)% - (.*)/',$data,$matches);
			
			if (!empty($matches[0]))
			{
				//$last_pid=$matches[4][count($matches[4])-1];
				$data_progress=$matches[3][count($matches[3])-1];
				$data_progress=explode(' - ',$data_progress);
				$last_pid=$data_progress[count($data_progress)-2];
				
				if (count($data_progress)>=2 && !empty($last_pid) && is_numeric($last_pid))
				{
					JobProcess::writeJobLog('Arret du processus en cours de fonctionnement ('.$data_progress[count($data_progress)-1].') : '.$last_pid);
					if(eregi('WINNT',PHP_OS))
						shell_exec('powershell -command "stop-process '.$last_pid.' -Force "');
					else
						shell_exec('kill '.$last_pid.'');
				}
			}
		}
	}
	
	public static function execJobSync($job_root = null){
		
		
		// récupération d'un job avant lancement synchrone 
		if(is_a($job_root,'Job')){
			$mainjob = $job_root;
		}else if(is_numeric($job_root)){
			require_once(modelDir.'model_job.php');
			$mainjob = new Job();
			$mainjob->t_job['ID_JOB'] = $job_root; 
			$mainjob->getJob() ; 
		}
		
		if(!isset($mainjob) || !isset($mainjob->t_job['JOB_ID_SESSION'])){
			throw new Exception('execJobSync - root job not found');
		}
		
		$arrJobsTraitement = $mainjob->getChildrenJobs(true,true);
		
		
		do{
			$flag_job_traite = false; 
			foreach($arrJobsTraitement as $id_job=>$job){
				// trace("parcours arrJobsTraitement, id_job courant : ".$id_job);
				// mise à jour de l'objet job, sera necessaire pour certains processus complexes, 
				$job->getJob() ; 
				if($job->t_job['JOB_ID_ETAT'] == jobAttente){ // Si job en attente => on le lance
					// trace("execJobSync: lancement traitement process for ".$job->t_job['ID_JOB']);
					
					// définition du flag_runSyncJob indiquant un traitement synchrone du job (a faire avant setXmlFile pour trouver le chemin du fichier xml du job)
					$job->flag_runSyncJob = true ; 
					if(empty($job->full_xml_job_process)){
						// trace ("jobProcess:execJobSync call makeXML on ".$job->t_job['ID_JOB']);
						$job->makeXML() ; 
					}
					
					// trace("execJobSync: setXmlFile done : ".$job->xmlFile);
					// récupération du jobProcess correspondant au job 
					$sync_jobProcess = JobProcess::getJobprocess($job->t_module['MODULE_NOM'],$job,null,true);
					try{
						$sync_jobProcess->prepareProcess();
						$sync_jobProcess->doProcess();
						$sync_jobProcess->finishJob();
					}catch (Exception $e){
						$sync_jobProcess->dropError('('.$e->getCode().') '.$e->getMessage());
					}
					
				
					// on indique qu'un traitement a eu lieu
					$flag_job_traite = true ; 
					// on appelle Job::updateJob de façon à mettre à jour le job après la fin du traitement
					$job->updateJob(); 
				}
			}
			// tant que des traitements ont été effectués, on refait une passe sur l'arrJobTraitement, pour voir si de nouveaux jobs sont traitables. ( c a d avec un etat "en attente");
		}while($flag_job_traite);
	}
}

?>
