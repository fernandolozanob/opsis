<?php

/*
 * Ceci est un wrapper de la facebook_sdk/facebook.php
 */

namespace Opsis; // Facebook est un nom de classe commun, qui pourrait se retrouver dans plusieurs librairies externes

require_once libDir . 'facebook_sdk/autoload.php';
require_once libDir . 'facebook_sdk/Facebook.php';

class Facebook {

	public $facebook_sdk;
	public $app_id;
	public $app_secret;
	public $user_access_token;
	public $pages;
	public $error_msg;

	public function __construct($tab = null) {


		$this->app_id = $tab['app_id'];
		$this->app_secret = $tab['app_secret'];
		$this->user_access_token = $tab['user_access_token'];
	}

	/**
	 * @author B.RAVI 06/07/17 16:55
	 * return user-access-token sinon FALSE, TODO si y'a moyen de le generer via l'api ca serait mieux
	 * pour l'instant, il faut aller sur cette page https://developers.facebook.com/tools/debug/accesstoken/ et générer un token utilisateur AVEC le droit publish_actions
	 * et le mettre dans t_etape <user_access_token>xxxxxx</user_access_token>
	 */
	public function connect() {

		if (empty($this->user_access_token)) {
			//todo
		}

		//	On ne met pas ceci dans construct car on peut getLogParamsFromEtape
		$this->facebook_sdk = new \Facebook\Facebook([
			'app_id' => $this->app_id,
			'app_secret' => $this->app_secret,
			'default_graph_version' => 'v2.9',
		]);
		
	}

	// $asPageId : nom de page qui sera auteure de la publication
	public function publish_video($filepath, $tab, $target='me', $asPageId = '') {
		try {
			if(!empty($asPageId)) {
				$access_token = $this->getPageToken($asPageId);
			} else {
				$access_token = $this->user_access_token;
			}
			trace(" param uploadVideo : $target, $filepath, $tab, $access_token ");
			$response = $this->facebook_sdk->uploadVideo($target, $filepath, $tab, $access_token);
		} catch (\Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			return array('KO' => 'Graph returned an error (code : '.$e->getCode().') : '. $e->getMessage());
		} catch (\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			return array('KO' => 'Facebook SDK returned an error: ' . $e->getMessage().' '.print_r($this->pages,1). ', $filepath : '.$filepath);
		}

		if ($response['success'] && !empty($response['video_id'])) {
			$tab_return['OK'] = $response['video_id'];
		} else {
			$tab_return['KO'] = 'Erreur api facebook indeterminee';
		}

		return $tab_return;
	}

	public function publish_link($tab, $target='me') {

		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $this->facebook_sdk->post('/$target/feed', $tab, $this->user_access_token);
			$graphNode = $response->getGraphNode();
		} catch (\Facebook\Exceptions\FacebookResponseException $e) {
			return array('KO' => 'Graph returned an error: ' . $e->getMessage());
		} catch (\Facebook\Exceptions\FacebookSDKException $e) {
			return array('KO' => 'Facebook SDK returned an error: ' . $e->getMessage());
		}


		if ($response['success'] && !empty($graphNode['id'])) {
			$tab_return['OK'] = $graphNode['id'];
		} else {
			$tab_return['KO'] = 'Erreur api facebook indeterminee';
		}

		return $tab_return;
	}

	public function unpublish($postId,$params=array()) {
		
		try {
			$response = $this->facebook_sdk->delete($postId,$params, $this->user_access_token);
		} catch (\Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			return array('KO' => 'Graph returned an error: ' . $e->getMessage());
		} catch (\Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			return array('KO' => 'Facebook SDK returned an error: ' . $e->getMessage());
		}
		
		//on ne peut malheureusement pas affiner le critère pour déterminer un succès car la proprieté est en protected, on ne peut donc pas faire if ($response->decodedBody["success"]===true
		
		return array('OK' => 'success');
	}

	// récupération des paramètres de login depuis une etape
	function getLogParamsFromEtape($etape_ref = null) {
		global $db;
		if (!empty($etape_ref)) {
			$id_etp = intval($etape_ref);
		} elseif (isset($this->etape_ref) && !empty($this->etape_ref)) {
			$id_etp = $this->etape_ref;
		}

		$etape_arr = $db->GetRow("SELECT * from t_etape where id_etape=" . intval($id_etp));

		if (empty($etape_arr['ETAPE_PARAM'])) {
			$this->dropError("Erreur recupération identifiants depuis etape");
			return false;
		}

		$tab_xml = xml2tab($etape_arr['ETAPE_PARAM']);

		$params_job = $tab_xml['PARAM'][0];
		$this->app_id = $params_job['ID_APPLICATION'];
		$this->app_secret = $params_job['API_KEY'];
		$this->user_access_token = $params_job['ACCESS_TOKEN'];

		if (!empty($this->app_id) && !empty($this->app_secret) && !empty($this->user_access_token)) {
			return true;
		} else {
			$this->dropError("Error - La recuperation des params de connexion a echouee");
			return false;
		}
	}
	
	function getPageToken($id_page) {
		if(empty($this->pages)) {
			$this->getPagesInfos();
		}
		return $this->pages[$id_page]['token'];
	}
	
	//Permet d'avoir les id des pages controlées par l'entité connectée, sous la forme :
	// array('id' => id_page, 'acess_token' => page_access_token, .....)
	function getPagesInfos() {
		$this->pages = array();
		$pages = $this->facebook_sdk->get('/me/accounts', $this->user_access_token );
		$aPages = $pages->getGraphEdge()->asArray();
		var_dump($aPages);
		foreach ( $aPages as $aPage ) {
			$this->pages [$aPage ['id']] = array (
					'id' => $aPage ['id'],
					'token' => $aPage ['access_token'],
					'name' => $aPage ['name'] 
			);
		}
		
		return $this->pages;
	}
	
	//Permet de sauvegarder dasn notre base les pages accessibles via le user_token_access
	//In : $getFromFb (bool): permet d'indiquer si l'on souhaite récupérer le spages depuis Facebook, ou bien si l'on veut les fixer "à la main"
	function savePageInfo($getFromFb = true) {
		require_once(modelDir.'model_val.php');
		if($getFromFb) {
			try {
				$this->getPagesInfos();
			} catch (\Facebook\Exceptions\FacebookResponseException $e) {
				$this->dropError('Graph returned an error (code : '.$e->getCode().') : ' . $e->getMessage());
				return false;
			} catch (\Facebook\Exceptions\FacebookSDKException $e) {
				$this->dropError('Facebook SDK returned an error (code : '.$e->getCode().') : ' . $e->getMessage());
				return false;
			}
		}
			
		foreach($this->pages as $aPage) {
			$tmp_val = new \Valeur();
			
			$tmp_val->t_val ['ID_LANG'] = 'FR';
			$tmp_val->t_val ['VAL_ID_TYPE_VAL'] = 'PFB';
			$tmp_val->t_val ['VAL_CODE'] = $aPage ['id'];
			$tmp_val->t_val ['VALEUR'] = $aPage ['name'];
				
			$tmp_val->save();
			unset ( $tmp_val );
		}
		
		return true;
	}
	

	function dropError($errLib) {
		$this->error_msg.=$errLib . "<br/>";
	}

}
