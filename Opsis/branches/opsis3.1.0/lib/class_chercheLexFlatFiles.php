<?php
require_once(libDir."class_cherche.php");
//extension de la classe recherche pour générer des fichiers plats
//pour Sinequa

class RechercheLexFlatFiles extends Recherche {


	function __construct() {
	  	$this->entity='LEX';
     	$this->name=kLexique;
     	$this->prefix='L';
     	$this->sessVar='recherche_LEX';
		$this->tab_recherche=array();
		$this->useSession=true;
		$this->sqlSuffixe=" GROUP BY L.ID_LEX HAVING 1=1 ";
		$this->params['RechercheLexiqueRestrictionLangage']=true;
		$this->highlightedFieldInHierarchy='LEX_TERME';
	}


    function prepareSQL(){
		global $db;
		$this->sql = "";
    }

	 function chercheIdLex($tabCrit,$prefix="",$recursif=false,$hier=true,$syno=true){
	        if(strlen($tabCrit['VALEUR'])>0) {
	            global $db;
	
	 		$arrLex['ID']=(array)$tabCrit['VALEUR'];
	     	if ($hier) $extHier=$this->extLexiqueHier($arrLex,true);
	    	if (!empty($extHier)) $arrLex['ID']=array_merge($arrLex['ID'],$extHier);
	
	    	if ($syno) $extSyno=$this->extLexiqueSyno($arrLex);
	 		if (!empty($extSyno)) $arrLex['ID']=array_merge($arrLex['ID'],$extSyno);
	
	
	
	 		$this->getTermesFromLexique($arrLex,true);
			if ($arrLex) { // Si extension demandée ET existe
					$strVals=$arrLex['VALEURS'];
					$arrHL=$arrLex['HIGHLIGHT'];
				}
		
	          	$prefix=($prefix!=""?$prefix.".":"");

	            if (empty($tabCrit['NOHIGHLIGHT'])) $this->tabHighlight["LEX_TERME"][]=array_values($arrHL);
	            
	        }
	    }


}
?>