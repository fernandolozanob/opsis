<?
global $refTables;


//require_once("classFd.php");
require_once(libDir."class_logger.php");
require_once(libDir."fonctionsXML.php");
require_once(libDir."class_user.php");
//$lx = extension_loaded("xslt") or die("You must install xslt module");

/*-----------------------------------------------------------------------------------------------------------------------*/

// Controle de l'acces aux pages
/*
$loggedPages="panierListe.php,panier.php";
$docPages="docSaisie.php,docMat.php,docSeqSaisie.php,docImageurSaisie.php,lexListe.php,lexSaisie.php,matListe.php,matSaisie.php,comListe.php";
$adminPages="docSaisie.php,docMat.php,docSeqSaisie.php,docImageurSaisie.php,lexListe.php,lexSaisie.php,matListe.php,matSaisie.php,usagerListe.php,comListe.php,refListe.php,importXML.php,importVideo.php,usagerSaisie.php";
if((!loggedIn() && strstr($loggedPages,basename($_SERVER['SCRIPT_NAME']))) || (getTypeLog() != kLoggedAdmin && strstr($adminPages,basename($_SERVER['SCRIPT_NAME'])))){
    print "ACCES INTERDIT ".basename($_SERVER['SCRIPT_NAME']);
    //header("location : index.php");
    exit;
}
*/

//----
//TODO : gestionDesConnexions();
//----



//JC 20/06/05 : gestion des pages/profils .. � finir

// $num de 0 � 3
// 0 : not logged
// 1 : user // client
// 2 : doc //documentaliste
// 3 : admin


/*
// VP : 24/5/2005 : Specifique ANATEC
$loggedPages="docListe.php,cherche2.php,historique.php,panierListe.php,panier.php";
$docPages="lexListe.php,matListe.php,comListe.php,refListe.php,importXML.php,importVideo.php";
$adminPages="lexListe.php,matListe.php,usagerListe.php,comListe.php,refListe.php,importXML.php,importVideo.php";
$generalPages="docListe.php,panierListe.php,panier.php,usagerListe.php,comListe.php,refListe.php,importXML.php,importVideo.php"; // pages dans fenetre principale
*/




/************
// Changement de langue
if(isset($_GET["langue"])){
    if(strcmp(strtolower($_GET["langue"]),"en")==0) $_SESSION["langue"]="en";
    elseif(strcmp(strtolower($_GET["langue"]),"fr")==0) $_SESSION["langue"]="fr";
    elseif(strcmp(strtolower($_GET["langue"]),"kh")==0) $_SESSION["langue"]="kh";
    else die("Language ".$_GET["langue"]." not supported<br>");
}
// fd++ : bug au cas ou quelqu'un aurait fait index.php?language=xx (comme moi
// pour faire un test)
else if(isset($_SESSION["langue"]))
{
    $arr = Array("fr","en","kh");
    $find = in_array(strtolower($_SESSION["langue"]),$arr);
    if($find == false) $_SESSION["langue"] = $arr[0];
}
******************/


function pageprintingduration($nb) {
	trace('page start time : '.$_SESSION['timestart'].", durée intermédiaire $nb : ".(microtime(true) - $_SESSION['timestart']));
}
/**
 * Renvoie un tableau contenant toutes les langues de l'appli sauf la langue en cours
 * IN : session arrLangues et langue
 * OUT : tableau des langues
 * NOTE : utile pour les fonctions saveVersions
 */

function getOtherLanguages() {
	$arrLg=$_SESSION['arrLangues'];
	foreach ($arrLg as $i=>$lg) {
		if ($lg==$_SESSION['langue']) unset ($arrLg[$i]);
	}
	return $arrLg;
}


/**
 * Affiche le contenu d'un item
 * $thing = item à voir, accepte tout type (objet, tableau, variable,...)
 * $col = couleur de fond
 * $windowed (true/false) = affiche le résultat dans un popup
 */
function debug($thing,$col=null,$windowed=false) {

	if (!defined('debugMode') ||  debugMode==false) return false;

	if (empty($col)) $col='white';
	$str="<pre style='font-size:10px;background-color:".$col."'>";
	$str.=htmlentities(utf8_decode(print_r($thing,true)));
	$str.="</pre>";
	if ($windowed) {

		$str=str_replace(array('\'',chr(10),chr(13),chr(32)),array('\\\'','<br/>','',' '),$str);
		$arr=str_split($str,600); //la chaine est splittée par blocs -> evite un débordement ou crash JS

		?>
	<script>
			myWin=window.open('','debug','width=550,height=900,screenX=0,screenY=0,left=0,top=0,scrollbars=1,resize=1');
			if (myWin) {
			myWin.document.title='DEBUG WINDOW';
			myWin.document.body.ondblclick=function dumb() { if (confirm('flush ?'))myWin.document.body.innerHTML=''; }
			<? foreach ($arr as $chunk) { ?>
			myWin.document.write('<?=$chunk?>');
			<? } ?>
			myWin.scrollBy(0,document.documentElement.clientHeight);
			myWin.focus();
			window.onload= function () {myWin.document.close();} //fin de chargement de la page ? on boucle le popup
			}
		</script><?
	}
	else
	{
	echo $str;
	}
}
/*
class CustomException extends Exception {
  public static function errorHandlerCallback($code, $string, $file, $line, $context) {
   $e = new self($string, $code);
   $e->line = $line;
   $e->file = $file;
   throw $e;
  }
}
set_error_handler(array("CustomException", "errorHandlerCallback"), E_ERROR);



/*-----------------------------------------------------------------------------------------------------------------------*/

//JC : 09/06/2005 : Fonction permettant de g�n�rer l'affichage des r�sultats de diverses fa�ons :
// -> utilis�e � la fois pour la liste d�roulante du choix du nombre de ligne de r�sultat � afficher et pour celle des feuilles de style.
// -> Finalement on peut l'utiliser pour g�n�rer n'importe quelle type de liste d�roulante

function makeAff($form_nom,$form_action,$form_method,$label,$select_name,$var_session,$tab_options){

            $param_aff.="<form name=\"$form_nom\" id=\"$form_nom\" action=\"$form_action\" method=\"$form_method\" style=\"margin:0px\">$label";
            $param_aff.="<select name='$select_name' onChange='javascript:document.$form_nom.submit()'>";
            //$tab_options = array(10 => "10", 20 => "20", 50 => "50", 100 => "100", "all" => kTous);
            foreach($tab_options as $tab_options=>$valeur) {
                //echo "Clef : $tab_options; Valeur : $valeur var_session : $var_session<br />\n";
                $param_aff .= "<option value=\"$tab_options\"";
                if(strcmp($tab_options,$var_session) == 0)
                    $param_aff .= " selected=\"selected\"";
                $param_aff .= ">".$valeur."</option>\n";
            }
            $param_aff.="</select></form>";
    return $param_aff;
}
/*-----------------------------------------------------------------------------------------------------------------------*/

function formatDateDisplay ($d) {
	if(empty($d) || strpos('0000', $d) !== false) return false;

	$timestamp = strtotime($d);
	if($timestamp === false) return false;

	return strftime(kFormatDateCourt, $timestamp);
}


function fDate($d) {
    $tab=explode("-",$d);
    switch (count($tab)) {
    case 1:
        $d.="-00-00";
        break;
    case 2:
        $d.="-00";
        break;
    }
    return $d;
}

function fDuree($d) {
	$tab=explode(":",$d);
	switch (count($tab)) {
	case 1:
		$d="00:".sprintf('%02d',$d).":00";
		break;
	case 2:
		$d.=":00";
		break;
	}
	return $d;
}

/*-----------------------------------------------------------------------------------------------------------------------*/
function convDateTime($dateTime) {
	$tmp = explode(" ", $dateTime);
	$date = convDate($tmp[0]);
	$time = false;
	if (isset($tmp[1]) && preg_match( "/^([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})$/", $tmp[1], $regs ))
		$time = sprintf('%02d',$regs[1]).":".sprintf('%02d',$regs[2]).":".sprintf('%02d',$regs[3]);

	$dateTime = $date;
	if ($time) $dateTime .= " ".$time;

	return $dateTime;
}

function convDate($date) {
	//Convertit toute date de format dd/mm/yyyy, dd-mm-yyyy, dd.mm.yyyy, yyyy/mm/dd en yyyy-mm-dd ou false si date non correcte

	$date=str_replace(array('.','/'),'-',$date);
	//MS ajout cas AAAA_AAAA-MM-JJ => peut arriver lors de l'import de données, on le rationalise en utilisant la première date (ex : FFF)
	if ( preg_match( "/^([0-9]{2,4})_([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})$/", $date, $regs ) ) {
		return $regs[1]."-".sprintf('%02d',$regs[3])."-".sprintf('%02d',$regs[4]);
	}
	else if ( preg_match( "/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})$/", $date, $regs ) ) {
	  return $regs[3]."-".sprintf('%02d',$regs[2])."-".sprintf('%02d',$regs[1]);
	}
	else if ( preg_match( "/^([0-9]{1,2})-([0-9]{2,4})$/", $date, $regs ) ) {
	  return $regs[2]."-".sprintf('%02d',$regs[1]);
	}
	else if ( preg_match( "/^([0-9]{2,4})-([0-9]{1,2})$/", $date, $regs ) ) {
	  return $regs[1]."-".sprintf('%02d',$regs[2]);
	}
	else if  ( preg_match( "/^([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})$/", $date, $regs ) ) {
		return $regs[1]."-".sprintf('%02d',$regs[2])."-".sprintf('%02d',$regs[3]);
	}
	else if  ( preg_match( "/^([0-9]{4})([0-9]{2})([0-9]{2})$/", $date, $regs ) ) {
		return $regs[1]."-".sprintf('%02d',$regs[2])."-".sprintf('%02d',$regs[3]);
	}
	else if  ( preg_match( "/[a-zA-Z]+/", $date, $regs ) ) {
		return "";
	}
	else return $date;
}

function convTimestamp($date) {
	// cas d'un timestamp UNIX
	// MS 17.03.15 - le test sur !strtotime ne suffit pas pour identifié un timestamp, exemple avec ces deux timestamp du 17.03.15 : 1426602563 et 1426612079 qui à
	// quelques heures d'écart renvoient respectivement fail ou passent le test.
	// pour l'instant, je vérifie donc à la place que la $date fournie ne contient aucun séparateur classique de date et qu'il s'agit d'un entier de + de 9 chiffres
	// ce n'est tjs pas parfait mais devrait réduire les risques d'erreur ...
	if(is_int($date) && (!strtotime($date) || (!preg_match( "/(-:\/ \.)/", $date ) && preg_match( "/([0-9]{9,})/", $date )))){
		return date("Y-m-d H:i:s",$date);
	}

	if (strpos($date, " ") > 0) {
		$tmp = explode(" ", $date);
		$date = $tmp[0];
		$time = $tmp[1];
	}
	else {
		$time = "00:00:00";
	}

	$date=convDate($date);
	if (preg_match( "/^([0-9]{2,4})-([0-9]{1,2})$/", $date, $regs ))
		$date .= '-01';
	elseif  (!preg_match( "/^([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})$/", $date, $regs ))
		$date = '1970-01-01';

	$time=str_replace(array('/', '-'),':',$time);
	if ( !preg_match( "/^([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})$/", $time, $regs ) ) {
		$time = "00:00:00";
	}

	return $date." ".$time;
}

function convDuree($duree){
    // Conversion durée format 1  h 30 mn / 45mn ...
    if(strpos($duree,'h')>0){
        $h = intval($duree);
        $duree = substr($duree, strpos($duree,'h')+1);
    }else{
        $h=0;
    }
    $mn = intval($duree);
    if(strpos($duree,'m')>0){
        $duree = substr($duree, strpos($duree,'m')+3);
    }elseif(strpos($duree,$mn)>0){
        $duree = substr($duree, strpos($duree,$mn)+2);
    }
    if(strpos($duree,'s')>0){
        $duree = substr($duree, strpos($duree,'s')+3);
    }else{
        $sec=0;
    }
    $mn += floor($sec / 60);
    $sec = $sec % 60;
    $h += floor($mn / 60);
    $mn = $mn % 60;

    return sprintf('%02d:%02d:%02d',$h,$mn,$sec);
}

/*-----------------------------------------------------------------------------------------------------------------------*/
//PC 02/09/11 : Ajoute le dernier jour et le dernier s'ils manquent dans la date (ex: 2001-08 en 2001-08-31)
function convToLastDate($date) {
	$date=str_replace('/','-',$date);
	if  ( preg_match( "/^([0-9]{2,4})$/", $date, $regs ) ) {
		return $date."-12-31";
	}
	else if ( preg_match( "/^([0-9]{2,4})-([0-9]{1,2})$/", $date, $regs ) ) {
	  return $date."-31";
	}
	else return $date;
}

//VP 18/04/2013 : Ajoute le premier jour et le premier mois s'ils manquent dans la date (ex: 2001-08 en 2001-08-31)
function convToFirstDate($date) {
	$date=str_replace('/','-',$date);
	if  ( preg_match( "/^([0-9]{2,4})$/", $date, $regs ) ) {
		return $date."-01-01";
	}
	else if ( preg_match( "/^([0-9]{2,4})-([0-9]{1,2})$/", $date, $regs ) ) {
        return $date."-01";
	}
	else return $date;
}

/*-----------------------------------------------------------------------------------------------------------------------*/
/* Conversion vers timezone serveur */

function convertTZ($date) {
	// VP 15/10/09 : On applique la fonction que si la date fait plus de 19 caractères : aaaa-mm-jjThh:mm:ssZ ou aaaa-mm-jjThh:mm:ss+00:00
	if(strlen($date)>19){
		$myDateTime = new DateTime($date);
		return date('Y-m-d H:i:s', $myDateTime->format('U'));
	}else return $date;
}


/*-----------------------------------------------------------------------------------------------------------------------*/

/* PC 10/10/12 : permet de convertir un nombre d'octet en une taille compréhensible (ex: 1500 devient 1,5 Mo) */
function convertFileSize($bytes)
{
	if ($bytes >= 1024*1024*1024*1024)
		return round(($bytes / 1024)/1024/1024/1024, 1) ." To";
	if ($bytes >= 1024*1024*1024)
		return round(($bytes / 1024)/1024/1024, 1) ." Go";
	elseif ($bytes >= 1024*1024)
		return round(($bytes / 1024)/1024, 1) ." Mo";
	elseif ($bytes >= 1024)
		return round(($bytes / 1024), 1) ." ko";
	elseif (is_numeric($bytes))
		return $bytes ." octets";
}

/*conversion VTT vers SRT */
function convertVTTtoSRT($vtt,$srt){
		$fp = fopen($vtt,"r"); //lecture du fichier
		if (!$fp) {
			$this->dropError('fichier non trouvé');
			return false;
		}
		$i=0;
		while (!feof($fp)) { //on parcoure toutes les lignes
			$ligne = fgets($fp, 4096); // lecture du contenu de la ligne		
			if ($i == 0 && preg_match("#WEBVTT#", $ligne)){
				$ligne='';
			}
			if (preg_match("#-->#", $ligne))
			{
			   $ligne=str_replace('.', ',', $ligne);
			}
			$page.= $ligne;
			$i=1;
		}
		$monfichier = fopen($srt, 'w');
		fseek($monfichier, 0); // On remet le curseur au début du fichier
		fputs($monfichier, $page); 
		fclose($monfichier);
		fclose($fp);
}
/****
 * Titre : Tableau BD � tableau HTML
 * Auteur : pkostov
 * Email : nexen_net@spmail.freesurf.fr
 * Url : n/a
 * Description : Cette fonction retourne le contenu d'un tableau d'une base de donn�es sous forme de tableau HTML.
****/

function table2html($table,$sql,$start=0,$numRec=0){
/* Exemple d'appel:
$server = ""; // L'adresse du serveur ici
$user = ""; // Login
$pass = ""; // Mot de passe
$dbb = ""; // Le nom de la BD
$table = ""; // Le nom du tableau BD
$link = mysql_connect($server,$user,$pass) or die ("La connexion au serveur n'a pas r�ussi!");
@mysql_select_db($dbb, $link) or die("La connexion � la base de donn�es n'a pas r�ussi!");
PRINT(table2html($table,0,30)); // Par exemple
mysql_close($link);
*/
    global $db;


    // Cr�ation de la requ�te SQL
    $query = "SELECT * FROM $table ".$sql;
    if (isset($start) AND isset($numRec)){
        if($numRec > 0)
            $query .= " LIMIT $numRec OFFSET $start ";
    }

    // Execute la requ�te
    $mysqlResult = $db->Execute($query);

    // D�termine la structure du tableau
    $fields = $mysqlResult->FieldCount();
    $totalLen = 0;

    // Construction de vecteurs avec les noms et la longueur des champs
    for ($i = 0; $i < $fields; $i++) {
    	$thisField=$mysqlResult->FetchField($i);
        $tableN[$i] = $thisField->name;
		// max_length n'est pas toujours correctement récupéré, si adodb n'arrive pas à déterminer la longueur du champ,
		//  on utilise la longueur du nom du champ plutot que de garder le -1 => permet de répartir un peu mieux la largeur des colonnes
		if(intval($thisField->max_length) != -1){
			$tableL[$i] = $thisField->max_length;
		}else{
			$tableL[$i] = strlen($thisField->name);
		}
        $totalLen = $totalLen + $tableL[$i];
    }

    // Ajout de la largeur des colonnes en % pour le tableau en HTML � afficher.
    $som = 0;
    if ($totalLen != 0 ){
	    for ($i = 0; $i < $fields-1; $i++) {
	        $tableP[$i] = intval($tableL[$i]/$totalLen*100);
	        $som = $som + $tableP[$i];
	    }
    }
    // De cette fa�on je m'assure que la longueur totale du tableau est 100%
    $tableP[$fields-1] = 100 - $som;

    // Pour la mise en form CSS
    $tab_bg=array("class=\"altern0\"","class=\"altern1\"");

    // D�but de la construction du tableau HTML
    $htmlTable = "";
    $htmlTable .= "<table width=\"100%\" cellspacing=\"0\" border=\"0\" class=\"tableResults\">";

    // Ajout de l'ent�te du tableau
    $htmlTable .= "<TR align=\"left\">";
    for ($i = 0; $i < $fields; $i++) {
        $htmlTable .= "<TD class=\"resultsHead\" width=\"$tableP[$i]%\" ><B>$tableN[$i]</B></TD>";
    }
    $htmlTable .= "</TD>";
    // Maitenant le vrai boulot: on construit le tableau!
    $rows = $mysqlResult->RecordCount();
    for ($j = 0; $j < $rows; $j++) {
        $mysqlResult->Move($j);

        $htmlTable .= "<TR class=\"resultsCorps\" ".$tab_bg[$j%2].">";
        for ($i = 0; $i < $fields; $i++) {

            $htmlTable .= "<TD width=\"$tableP[$i]%\">";
            if($i==0) $htmlTable .="<a href=\"index.php?urlaction=refSaisie&table=".urlencode($table)."&ref=".urlencode($mysqlResult->fields[strtoupper($tableN[$i])])."\" >";
            $htmlTable .= $mysqlResult->fields(strtoupper($tableN[$i]));
            if($i==0) $htmlTable .="</a>";
            $htmlTable .= "</TD>";
        }
        $htmlTable .= "</TR>";
    }

    // Et le tableau HTML est cr��!
    $htmlTable .= "</table>";

    return $htmlTable;
}



// **************************************************************************************************************************************************************************************

//-------------------- FONCTION D'AFFICHARGE HIERARCHIQUE DES TERMES DU LEXIQUE

       // Function AFFICHE_TYPELEX : Selectionne les $limit terme "type" du lexique existants dans la langue $id_lang et r�pondants aux sp�cifications $sql_fin
        function affiche_typeLex($id_pere=0,$id_lang,$sql_fin="",$limit=0)
        {
            global $db;
            global $TAB_HIERARCHIQUE;

            $result=$db->Execute("SELECT * FROM t_type_lex WHERE ID_LANG=".$db->Quote($id_lang)." $sql_fin AND HIERARCHIQUE='1' ORDER by TYPE_LEX ".($limit!=0 ? "LIMIT $limit OFFSET 0": ""));

            while($list_fils=$result->FetchRow()){
                // Cr�ation du noeud de l'�l�ment
                $TAB_HIERARCHIQUE[$list_fils["ID_TYPE_LEX"]]= array("id"=>$list_fils["ID_TYPE_LEX"], "id_pere"=>$id_pere, "terme"=>$list_fils["TYPE_LEX"], "open"=>'', "icone"=>"","valide"=>false,"context"=>false);
                /*
                // Ecriture du noeud qui indique le nombre de fils de l'�l�ment en cours (ce noeud est de type "valide"=false, ce qui signifie qu'il n'aura ni lien, ni champs input)
                $resultNB=->selectSQL("SELECT ID_LEX FROM t_lexique WHERE ID_LANG=".$db->Quote($id_lang)." AND LEX_ID_TYPE_LEX=".$db->Quote($list_fils["ID_TYPE_LEX"]));
                $nbFils = ->numRowSQL($resultNB);
                if ($nbFils>0){
                    $TAB_HIERARCHIQUE[$list_fils["ID_TYPE_LEX"]."_nbFils"]= array("id"=>$list_fils["ID_TYPE_LEX"]."_nbFils", "id_pere"=>$list_fils["ID_TYPE_LEX"], "terme"=>$nbFils." element".($nbFils>1 ? "s" : ""), "open"=>'', "icone"=>"","valide"=>false,"context"=>false);
                }
                */
            }
            $result->Close();
        }



        // Function AFFICHE_LEX_FILS : Selectionne les $limit �l�ments du lexique fils de $id_pere, en langue $id_lang et r�pondants aux sp�cifications $sql_fin
        function affiche_lex_fils($id_pere=0,$id_lang,$id_doc="",$sql_fin="",$id_type_desc="",$limit=0)
        {
            // F.I. Initialisation, le tableau global contenant les noeuds qui devront �tre affich�
            global $db;
            global $TAB_HIERARCHIQUE;

            // F.II. Selection des �l�ments fils de $id_pere
            $sql = "SELECT * FROM t_lexique
					INNER JOIN t_type_lex  ON t_lexique.LEX_ID_TYPE_LEX=t_type_lex.ID_TYPE_LEX
					WHERE t_lexique.ID_LANG=".$db->Quote($id_lang)." AND t_lexique.LEX_ID_GEN=".intval($id_pere)." $sql_fin ";
            $sql.= "AND t_type_lex.ID_LANG=t_lexique.ID_LANG AND t_type_lex.HIERARCHIQUE=1 AND t_lexique.LEX_ID_ETAT_LEX>=2 ORDER by LEX_TERME ".($limit!=0 ? "LIMIT 0 , ".$limit: "");
            $result=$db->Execute($sql);
            while($list_fils=$result->FetchRow())
            {
                // Cr�ation du noeud dtree de l'�l�ment
                    // F.II.A. Choix de l'�l�ment p�re du noeud � ajouter
                    $id_pere_lex = $id_pere;
                    if($id_pere == 0 ){
                        $id_pere_lex = $list_fils["LEX_ID_TYPE_LEX"];
                    }
                    // L'�l�ment a t-il un synonime pr�f�rentiel ?
                    if($list_fils["LEX_ID_SYN"] !=0){
                        $id_pere_lex = $list_fils["LEX_ID_SYN"];
                    }

                    // F.II.B. Ecriture du noeud (si c'est un synonime pr�f�rentiel on lui donne une icone sp�cifique)
                    $TAB_HIERARCHIQUE[$list_fils["ID_LEX"]]= array("id"=>$list_fils["ID_LEX"], "id_pere"=>$id_pere_lex, "terme"=>$list_fils["LEX_TERME"], "open"=>'', "icone"=>($list_fils["LEX_ID_SYN"]!=0 ? "design/images/dtree/page3.gif" : ""),"valide"=>true,"context"=>false);

                    // F.II.C. Ecriture du noeud qui indique le nombre de fils de l'�l�ment en cours (ce noeud est de type "valide"=false, ce qui signifie qu'il n'aura ni lien, ni champs input)
                    $resultNB=$db->Execute("SELECT ID_LEX FROM t_lexique WHERE ID_LANG=".$db->Quote($id_lang)." AND LEX_ID_GEN=".intval($list_fils["ID_LEX"])." AND LEX_ID_ETAT_LEX>=2");
                    $nbFils = $resultNB->RecordCount();
                    if ($nbFils!=false){
                        $TAB_HIERARCHIQUE[$list_fils["ID_LEX"]."_nbFils"]= array("id"=>$list_fils["ID_LEX"]."_nbFils", "id_pere"=>$list_fils["ID_LEX"], "terme"=>$nbFils." element".($nbFils>1 ? "s" : ""), "open"=>'', "icone"=>"","valide"=>false,"context"=>false);
                    }


                    // F.II.D. On va chercher les termes associ�s � ce terme $list["ID_LEX"]
                    $sql = "SELECT t_lexique.* FROM t_lexique
							INNER JOIN t_lexique_asso ON t_lexique.ID_LEX=t_lexique_asso.ID_LEX_ASSO
							WHERE t_lexique_asso.ID_LEX=".intval($list_fils["ID_LEX"])." ";
                    $sql.= " AND t_lexique.ID_LANG=".$db->Quote($id_lang)."  AND t_lexique.LEX_ID_ETAT_LEX>=2 ORDER by LEX_TERME";
                    $result_asso=$db->Execute($sql);
                    while($list_asso=$result_asso->FetchRow())
                    {
                        $TAB_HIERARCHIQUE[$list_asso["ID_LEX"]."_asso"]= array("id"=>$list_asso["ID_LEX"]."_asso", "id_pere"=>$list_fils["ID_LEX"], "terme"=>$list_asso["LEX_TERME"], "open"=>'', "icone"=>"design/images/dtree/page2.gif","valide"=>true,"context"=>true);
                    }

                // F.II.E. Appel r�cursif pour afficher tous les fils de l'�l�ment
                //affiche_lex_fils($list_fils["ID_LEX"],$id_lang,$id_doc,$sql_fin,$id_type_desc,$limit);
            }
            $result->Close();
        }




        // Function AFFICHE_LEX_PERE : Selectionne r�cursivement les p�res de $id_noeud, en langue $id_lang
        function affiche_lex_pere($id_noeud=0,$id_lang,$id_doc="",$id_type_desc)
        {
            // F.I. Initialisation, le tableau global contenant les noeuds qui devront �tre affich�
            global $db;
            global $TAB_HIERARCHIQUE;

            if ($id_noeud !=0){
                // F.II. Selection de l'�l�ment
                $result=$db->Execute("SELECT * FROM t_lexique WHERE ID_LANG=".$db->Quote($id_lang)." AND ID_LEX=".intval($id_noeud)." AND LEX_ID_ETAT_LEX>=2");
                while($list_peres=$result->FetchRow()){
                    // Cr�ation du noeud dtree de l'�l�ment
                        // F.II.A. Choix de l'�l�ment p�re du noeud � ajouter
                            $id_pere_lex = $list_peres["LEX_ID_GEN"];
                            if($id_pere_lex == 0 ){
                                $id_pere_lex = $list_peres["LEX_ID_TYPE_LEX"];
                            }
                            // L'�l�ment a t-il un synonime pr�f�rentiel ?
                            if($list_peres["LEX_ID_SYN"] !=0){
                                $id_pere_lex = $list_peres["LEX_ID_SYN"];
                            }
                        // F.II.B. Ecriture du noeud
                        $TAB_HIERARCHIQUE[$list_peres["ID_LEX"]]= array("id"=>$list_peres["ID_LEX"], "id_pere"=>$id_pere_lex, "terme"=>$list_peres["LEX_TERME"], "open"=>'', "icone"=>($list_peres["LEX_ID_SYN"]!=0 ? "design/images/dtree/page3.gif" : ""),"valide"=>true,"context"=>false);

                    // F.II.C. Appel r�cursif pour afficher la g�n�alogie de l'�l�ment (ses p�res);
                    affiche_lex_pere($id_pere_lex,$id_lang,$id_doc,$id_type_desc);
                }
                $result->Close();
            }
        }




// FUNCTION VERIFRECHERCHEUSAGER : Retourne le nombre de requetes qu'un usager a enregistr�
function verifRechercheUsager(){
    global $db;
    if(isset($_SESSION["USER"]["ID_USAGER"])){
        $result = $db->Execute("SELECT ID_REQ FROM t_requete WHERE ID_USAGER=".intval($_SESSION["USER"]["ID_USAGER"]));
        return $result->RecordCount();
    }else
        return 0;
}


// **************************************************************************************************************************************************************************************

// FUNCTION VALIDE_EMAIL : Permet de verifier si un eamil est bien forme
// $email : email de l'utilisateur
function valide_email($email){

   $regexp = "/^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
   // Verification de la syntaxe
   if (preg_match($regexp, strtolower($email))){
   return TRUE;
   } else {

      return FALSE;
   }

}


/** mergeFields (v1,v2, included (false) )
 *  Vérifie les valeurs 1 et 2 et renvoie la plus complète.
 * 	si le param included est true, on vérifie si l'une contient l'autre.
 * 	OUT : string contenant v1 ou v2.
 *  NOTE : utilisé pour les fusions d'objets (personnes).
 */
function mergeFields($field1,$field2,$included=false) {
	if (empty($field1) && !empty($field2)) {return $field2;} //une des val est vide l'autre pas, on renvoie la pleine
	if (!empty($field1) && empty($field2)) {return $field1;}

	if (!empty($field1) && !empty($field2)) { //aucune n'est vide
		if ($included) { //check du contenu : si une info est comprise dans l'autre, on prend la plus complète
			if (strpos($field2,$field1)===true) return $field2; else return $field1;
		}
		else return $field1; //par défaut,si l'on ne vérifie pas le contenu
	}

	return $field1; //par défaut, si les 2 vals sont vides...

}


/** mergeArrays(arr1,arr2)
 *  Renvoie la fusion entre les tableaux arr1 et arr2
 * 	Si un des tableaux est vide, l'autre est renvoyé.
 * 	NOTE 1 : pour que la fusion se fasse, il faut que les 2 tableaux soient de même structure
 *  NOTE 2 : utilisé pour la fusion d'objets (personnes,..)
 */
function mergeArrays($arr1,$arr2) {

	if (!empty($arr1) && (empty($arr2) || !is_array($arr2))) return $arr1;
	if ((empty($arr1) || !is_array($arr1)) && !empty($arr2)) return $arr2;

	if (!empty($arr1) && !empty($arr2)) return array_merge($arr1,$arr2);

	return array();
}


function merge_array_recurs($Arr1, $Arr2){
	foreach($Arr2 as $key => $Value){
		if(array_key_exists($key, $Arr1) && is_array($Value))
			$Arr1[$key] = merge_array_recurs($Arr1[$key], $Arr2[$key]);
		else
			$Arr1[$key] = $Value;
	}
	return $Arr1;
}
	


function my_serialize(&$arr,$pos){
  $arr = serialize($arr);
}

function my_unserialize(&$arr,$pos){
  $arr = unserialize($arr);
}

/**
 * Custom_array_diff(arr1,arr2)
 * Renvoie la différence entre 2 tableaux de valeurs
 * Substitut de la fonction PHP array_diff qui fonctionne très
 * aléatoirement pour les tableaux de tableaux.
 * IN : array1, array2
 * OUT : toutes les valeurs de array1 non présentes dans array2, soit array1-array2
 */
function custom_array_diff($first_array,$second_array){

	// serialize all sub-arrays
	array_walk($first_array,'my_serialize');
	array_walk($second_array,'my_serialize');

	// array_diff the serialized versions
	$diff = array_diff($first_array,$second_array);

	// unserialize the result
	array_walk($diff,'my_unserialize');

	// you've got it!
	return($diff);
}

function custom_array_intersect($first_array,$second_array){

	// serialize all sub-arrays
	array_walk($first_array,'my_serialize');
	array_walk($second_array,'my_serialize');

	// array_diff the serialized versions
	$diff = array_intersect($first_array,$second_array);

	// unserialize the result
	array_walk($diff,'my_unserialize');

	// you've got it!
	return($diff);
}
/**
* sortArrayByField ( $original, $field, $descending )
 * Trie un tableau par une clé
 * IN : $original, $field, $descending
 * OUT : tableau trié
 */

function sortArrayByField ( $original, $field, $descending = false ){
	$sortArr = array();
	foreach ( $original as $key => $value ){
		$sortArr[ $key ] = $value[ $field ];
	}

	if ( $descending ){
		arsort( $sortArr );
	} else {
		asort( $sortArr );
	}

	$resultArr = array();
	foreach ( $sortArr as $key => $value ){
		$resultArr[ $key ] = $original[ $key ];
	}
	return $resultArr;
}

// **************************************************************************************************************************************************************************************

// FUNCTION SECTOTIME : Concevertir une dur�e en seconde en hh:mm:ss
function secToTime($temps) {
	//PC : correction pour gérer le cas où TCIN > TCOUT
	if ($temps < 0) $temps += 24 * 60 * 60;

  $h = floor($temps / 3600);
  if ($h < 10)
    $h = "0".$h;

  $m = floor(($temps - ($h * 3600)) / 60);
  if ($m < 10)
    $m = "0".$m;

  $s = round($temps - ($h * 3600) - ($m * 60));
  if ($s < 10)
    $s = "0".$s;

  return $h.":".$m.":".$s;
}

function secToTc($temps) {
	return secToTime($temps).":00";
}

// FUNCTION secDecToTc : Convertir une durée en seconde décimales en timecode
function secDecToTc($temps,$timebase=25) {
    $f = round(($temps-floor($temps))*$timebase);
    if($f >= $timebase ) {
    	$f = 0;
    	++$temps;
    }
	$h = floor($temps / 3600);
    $m = floor(($temps - ($h * 3600)) / 60);
    $s = floor($temps - ($h * 60 + $m) * 60);
	return sprintf("%02d:%02d:%02d:%02d",$h,$m,$s,$f);
}

// FUNCTION tcToSecDec : Convertir un time code hh:mm:ss:ii en secondes décimales
function tcToSecDec($tc,$timebase=25) {
    if (empty($tc)) return 0;
	if(strpos($tc, ";") !== false) {
		$timebase = 30000/1001;
        $tc=str_replace(";",":",$tc);
	}
    $tab_duree = explode(":", $tc);
    $h=$tab_duree[0];
    $m=$tab_duree[1];
    $s=$tab_duree[2];
    $fr=$tab_duree[3];
    return ($h*3600 + $m*60 + $s + $fr/$timebase);
}

// VP 29/07/09 : ajout paramètre timebase aux fonctions de conversion (=25 par défaut)
// FUNCTION TCTOSEC : Convertir un time code hh:mm:ss:ii en secondes
function tcToSec($tc,$timebase=25) {
    if (empty($tc)) return 0;
    $tab_duree = explode(":", $tc);
    $h=$tab_duree[0];
    $m=$tab_duree[1];
    $s=$tab_duree[2];
    $fr=$tab_duree[3];
    //trace($h*3600 + $m*60 + $s + floor($fr/$timebase));
    return ($h*3600 + $m*60 + $s + floor($fr/$timebase));
}

function timeToSec($duree) {
    if(!empty($duree)) {
        $tab_duree = explode(":", $duree);
        $h=$tab_duree[0];
        $m=$tab_duree[1];
        $s=$tab_duree[2];
        return ($h*3600 + $m*60 + $s );
    }
}

// FUNCTION frameToTC : Conversion frames en time-code hh:mm:ss:ii, base PAL (25im/s) par défaut
// VP 18/11/09 : conversion de timebase
function frameToTC($frame,$timebase=25,$timebasein=25) {
	$frame=round($frame*$timebase/$timebasein);
	$sec=floor($frame/$timebase);
	$tc=secToTime($sec);
	$i=intval($frame - ($sec*$timebase));
	if ($i < 10) $i = "0".$i;
	return $tc.":".$i;
}

// FUNCTION frameToTC : Conversion frames en time-code hh:mm:ss:ii, base PAL (25im/s) par défaut
// VP 18/11/09 : conversion de timebase
function frameToTime($frame,$timebase=25,$timebasein=25) {
	$frame=round($frame*$timebase/$timebasein);
	$sec=floor($frame/$timebase);
	$tc=secToTime($sec);
	return $tc;
}

// FUNCTION TCTOFRAME : Convertir un time code hh:mm:ss:ii en frames, base PAL (25im/s) par défaut
function tcToFrame($tc,$timebase=25) {
    if (empty($tc)) return 0;
    $tab_duree = explode(":", $tc);
    $h=$tab_duree[0];
    $m=$tab_duree[1];
    $s=$tab_duree[2];
    $fr=$tab_duree[3];
    return ((($h*60 + $m)*60 + $s )*$timebase+ $fr);
}

/**
 * Remplace les caractères accentués par leur équivalent sans accent.
 * Ne traite que les simples caractères (pas le oe de coeur par exemple)
 * IN : chaine, fromUTF8 = flag à mettre à true si la chaîne d'origine est déjà en UTF8
 * OUT : chaine
 * NOTE : si la chaine est en UTF8, elle est décodée, substituée puis réencodée en UTF8.
 * Utilisé dans l'upload Java : processUpload et finUpload
 */
function stripAccents($string,$fromUTF8=false){

    // VP 12/03/2012 : utilisation mb_convert_encoding en 7bit
    $string=mb_convert_encoding($string,"7bit");
    return $string;

//   if ($fromUTF8) $string=utf8_decode($string);
//   $string = strtr($string,
//      "\xA1\xAA\xBA\xBF\xC0\xC1\xC2\xC3\xC5\xC7\xC8\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD1\xD2\xD3\xD4\xD5\xD8\xD9\xDA\xDB\xDD\xE0\xE1\xE2\xE3\xE5\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF1\xF2\xF3\xF4\xF5\xF8\xF9\xFA\xFB\xFD\xFF",
//       "!ao?AAAAACEEEEIIIIDNOOOOOUUUYaaaaaceeeeiiiidnooooouuuyy");
//   $string = strtr($string, array("\xC4"=>"Ae", "\xC6"=>"AE", "\xD6"=>"Oe", "\xDC"=>"Ue", "\xDE"=>"TH", "\xDF"=>"ss", "\xE4"=>"ae", "\xE6"=>"ae", "\xF6"=>"oe", "\xFC"=>"ue", "\xFE"=>"th"));
//   if ($fromUTF8) $string=utf8_encode($string);
//   return($string);
}

function replaceAccents($str){
	$unwanted_chars = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
	return strtr( $str, $unwanted_chars );
}

/**
 * Repmlace les accents d'une chaine par leur caractère non-accentué
 *
 * @param string $str : chaine avec accents
 * @param array $aAccents : correspondance caractère <=> accents. Par défaut : gTabAccents
 * @return string : chaine sans accents
 */
function replaceAccentsListed($str, $aAccents = array () ) {
	if(empty($aAccents)) {
		$aAccents = unserialize(gTabAccents);
	}
	foreach ( $aAccents as $char => $accents ) {
		$str = str_replace ( $accents, $char, $str );
	}

	return $str;
}
/**
 * Remplace les caractères "hauts" par leur équivalent HTML
 * Equiv de htmlentities en plus étendu mais ne touche qu'aux caractères spéciaux.
 * NOTE : pas utilisé pour le site, mais repompé d'un site et gardé sous le coude
 */
function unicode2html($string) {
	$string = preg_replace('/([\xc0-\xdf].)/se', "'&#' . ((ord(substr('$1', 0, 1)) - 192) * 64 + (ord(substr('$1', 1, 1)) - 128)) . ';'", $string);
	$string = preg_replace('/([\xe0-\xef]..)/se', "'&#' . ((ord(substr('$1', 0, 1)) - 224) * 4096 + (ord(substr('$1', 1, 1)) - 128) * 64 + (ord(substr('$1', 2, 1)) - 128)) . ';'", $string);
	return $string;
}


// **************************************************************************************************************************************************************************************
// FUNCTION LOGACTION : log les actions de l'usager dans la table t_action
// $type = IN, OUT, SQL, DOC, VIS
// $param contient les valeurs � logger : ID_DOC pour type=VIS ou DOC, ACT_FORM, ACT_REQ et ACT_SQL pour type=SQL
function logAction($type,$param)
{
	global $db;
	// VP 12/02/10 : possibilté de ne pas enregistrer les actions
	if (defined('gDontLogAction') && gDontLogAction==true) return;

    // VP 13/11/12 : prise en compte HTTP_X_FORWARDED_FOR
	if(!isset($param)) $param=Array();
	$param["ACT_TYPE"]=$type;
	$param["ID_USAGER"]=$_SESSION["USER"]["ID_USAGER"];
	$param["ACT_DATE"]=date("Y-m-d H:i:s");
	$param["ACT_IP"]=(empty($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER["REMOTE_ADDR"]:$_SERVER['HTTP_X_FORWARDED_FOR']);
	$param["ACT_SESSKEY"]=session_id();
	if(!empty($param['ACT_IP']) && function_exists("geoip_country_name_by_name")){
		// Si le site ne fonctionne plus apres une mise à jour vers 3.0.5 où cette ligne a été ajoutée, merci de vérifier la configuration SE Linux, qui bloque potentiellement l'accès aux fichiers Geoip.dat
		// Ajout d'un test pour éviter de crash un site a cause de la base GeoIP.dat non accessible 
		// => devrait être testable via geoip_db_avail(GEOIP_COUNTRY_EDITION), mais en réalité cela déclenche aussi la segfault apache...
		// => is_readable(geoip_db_filename(GEOIP_COUNTRY_EDITION)) semble fonctionner en dev 
		
		if(is_readable(geoip_db_filename(GEOIP_COUNTRY_EDITION))){
			$param['ACT_PAYS'] = geoip_country_name_by_name($param['ACT_IP']);
		}else{
			trace("logAction - base geoip inaccessible (".geoip_db_filename(GEOIP_COUNTRY_EDITION)."), ACT_PAYS ne sera pas sauvegardé");
		}
	}

	if(!isset($param['ACT_BASE']) && defined("kLogActAsBase") && is_string(kLogActAsBase)){
		if(strlen(kLogActAsBase) > 10 ){
			$param["ACT_BASE"] = substr(kLogActAsBase,0,10);
		}else{
			$param["ACT_BASE"] = kLogActAsBase;
		}
	}

	// GT 15-04-11 : on logge la durée des actions de type SQL
	if ($type=='SQL' && defined('kLogDureeActionSql') && kLogDureeActionSql=='1')
	{
		$param['ACT_TIME']=$param['duree_req_sql'];
	}
	unset($param['duree_req_sql']);
	// insertSQL("t_action",$param);
	$ok = $db->insertBase("t_action","id_action",$param);

	return $ok;
}

function updateAction($id_action, $param) {
	global $db;
	$rs = $db->Execute("SELECT * FROM t_action WHERE ID_ACTION = ".intval($id_action));
	$sql = $db->GetUpdateSQL($rs, $param);
	if (!empty($sql)) $ok = $db->Execute($sql);
}

/**
 * Retourne une valeur depuis une table de référence et charge cette table en mémoire si elle n'existe pas.
 * IN : table, id (valeur à ramener), langue (optionnel)
 * OUT : valeur demandée.
 * NOTE: si le param SYSTEME gPutRefInSession est vrai, alors les données sont stockées et récupérées depuis la session
 * */

function GetRefValue ($table,$id='',$idLang="") {
	global $db,$refTable;

	if (gPutRefInSession==true) {
		$refTable[$table]=(isset($_SESSION['refTables'][$table]))?$_SESSION['refTables'][$table]:'';
	}
	if (!is_array($refTable[$table])) {
		$db->SetFetchMode(ADODB_FETCH_NUM);
		$sql="SELECT * FROM $table";

		$rset=$db->Execute($sql);

		while (!$rset->EOF) {
			if (isset($rset->fields[2])) { //table avec ID_LANG ???
                            $refTable[$table][$rset->fields[0]][strtoupper($rset->fields[1])]=$rset->fields[2];
			} else {
                            $refTable[$table][$rset->fields[0]]=$rset->fields[1];
			}
			$rset->MoveNext();
		}
		$rset->Close();
		$db->SetFetchMode(ADODB_FETCH_ASSOC);
		if (gPutRefInSession==true) $_SESSION['refTables'][$table]=$refTable[$table];

	}
	// VP 9/03/09 : changement test de == en ===
	if ($id===NULL) return '';
	//update VG 30/08/2011 : ajout du test is_array, afin de déterminer si le champ a bien une distinction de langue (ex : dans t_media, on n'a pas de champ ID_LANG...)
	//						Sinon on prend directement $refTable[$table][$id]
	if ($idLang) {
		if ($id!='') {
			if(isset($refTable[$table][$id]) && is_array($refTable[$table][$id]))
				return $refTable[$table][$id][$idLang];
			else if (isset($refTable[$table][$id]))
				return $refTable[$table][$id];
		} elseif (!empty($refTable[$table])) { // no ID, on retourne le tableau complet pour la langue
			foreach ($refTable[$table] as $idx=>$val) {
				if(is_array($refTable[$table][$idx]))
					$arrVals[$idx]=$refTable[$table][$idx][$idLang];
				else
					$arrVals[$idx]=$refTable[$table][$idx];
			}
			return $arrVals;
		} else {
			return '';
		}
	}
	else {
		if ($id!='') {
		return $refTable[$table][$id];
		}
		else {return $refTable[$table];}
	}
}

/**
 * Récupération des valeurs liées à un document
 * (destiné à bouger dans la classe_doc)
 * typeVal désigne le type de valeur, si pas renseigné
 * toutes valeurs ramenées.
 * Tableau de type IDX=>array (LANG, VALEUR, TYPE_VAL, VAL)
 */

function getVal($id_doc,$typeVal='') {
		global $db;

		$sql="SELECT t_val.*
				FROM t_val
				INNER JOIN t_doc_val ON t_val.ID_VAL=t_doc_val.ID_VAL
				WHERE t_val.ID_LANG='".$_SESSION['langue']."'
				AND t_doc_val.ID_DOC=".intval($id_doc);
		if (!empty($typeVal)) $sql.=" AND t_val.VAL_ID_TYPE_VAL='".$typeVal."'";

		$rset = $db->Execute($sql);
               while ($row = $rset->FetchRow()) {

               $dumb["ID"]=$row["ID_VAL"] ;
               $dumb["VALEUR"]= $row["VALEUR"];
               $tab_val[$row["VAL_ID_TYPE_VAL"]][$row["ID_VAL"]]=$row["VALEUR"];
               }
		return $tab_val;

}

/**
 * Transforme en XML un tableau de valeurs (issu de t_val).
 */
function tabVal2XML ($tab_val)  {


        	$search = array('&','<','>');
       		$replace = array('&#38;','&#60;','&#62;');
            $content.="\t<t_doc_val nb=\"".count($tab_val)."\">\n";


                // Pour chaque type de valeur
                foreach ($tab_val as $name => $value) {
                    $content .= "\t\t<TYPE_VAL type=\"".$name."\" nb=\"".count($tab_val[$name])."\">";
                    // Pour chaque valeur
                    foreach ($tab_val[$name] as $val_name => $val_value) {
                        $content .= "\t\t<t_val ID_VAL=\"".$val_name."\">\n";
                        $content .= "\t\t\t<ID_VAL>".$val_name."</ID_VAL>\n";
                        $content .= "\t\t\t<VALEUR>".str_replace($search,$replace, $val_value)."</VALEUR>\n";
                        $content .= "\t\t</t_val>\n";
                    }
                    $content .= "</TYPE_VAL>\n";
                }

            $content.="\t</t_doc_val>\n";
            return $content;
}


/** Extraction des attributs/valeurs d'un tag HTML
 * IN : tag HTML (chaine), type (tableau des attributs à récupérer)
 * OUT : tableau 2 dimensions Attribut / Valeur
 * Exemple : get_attributes_from_tag("<a href='i'>",array('href'));
 * */
function get_attributes_from_tag($string,$types) {

	while(list(,$type) = each($types)) {
	preg_match_all ("|$type\=\"?'?`?([[:alnum:]:?=&@/._-]+)\"?'?`?|i", $string, $matches);
	$ret[$type] = $matches[1];
	}

	return $ret;
}

/** Wrapper entre Opsis V1 et Opsis V2, à virer ! */
function verifieDroits($doc_id_fonds,$id_priv) {
	include_once (libDir.'class_user.php');
	User::getInstance()->verifieDroits($doc_id_fonds,$id_priv);
}

/** FUNCTION AJOUTPRIVRESULT : Ajoute au tableau de resultat $result les privileges des fonds auxquels les r�sultats sont associ�s
// $result : tableau de resultat
// NB. Si l'utilisateur courant a un profil "admin" (3) on lui donne lui droits en modification sur tous les fonds
*/
function ajoutPrivResult($result){
	$usr=User::getInstance();
	// VP 2/3/09 : prise en compte paramètre useMySQL
	if (defined("useSinequa") && useSinequa==true && $_SESSION["useMySQL"]!="1")
		$fld="doc_id_fonds";
	else
		$fld="DOC_ID_FONDS";


	for ($i=0;$i<count($result);$i++){
		// VP 21/4/10 : modif valeur privilege admin
		$result[$i]["ID_PRIV"]=5;
		if ($usr->Type!=kLoggedAdmin){
        $result[$i]["ID_PRIV"]=0; //par défaut

        foreach($usr->Groupes as $value){
			if(isset($result[$i][$fld])){
				if($value["ID_FONDS"]==$result[$i][$fld]){
					$result[$i]["ID_PRIV"]=$value["ID_PRIV"];
					}
				}
			}
        }
    }
    return $result;
}


/** Créé un tableau de tri à partir d'une chaine de paramètres **/
function tri2array($tri) {
    if($tri!=""){
        // Choix de l'ordre du tri (le choix est indiqu� � la fin de la colonne de tri. Ex: &tri=groupe1 ou & tri=groupe0)
        $ordre=1;
        $fields=explode(",",$tri);
        foreach ($fields as $field) {
	        $ordre=substr($field,-1,1);
	        if (is_numeric($ordre)){
	            if($ordre==1){$direction="DESC";}else{$direction="ASC";}
	            $fld=substr($field,0,-1);
	        }
	        else {
	        	$direction="ASC";
	        	$fld=$field;
	        }
	        $arrSort[$fld]=$direction;
        }
    return $arrSort;
    }
}

/** Wrapper à virer**/
function getTypeLog() {
return User::getInstance()->getTypeLog();
}
function loggedIn() {
return User::getInstance()->loggedIn();
}


/** Supprime une valeur de plusieurs tables
 * IN : array de tables, champ, valeur
 * OUT : maj DB
 */

function deleteSQLFromArray($tbArray,$fields,$values)  {
    global $db;
    $fields=(array)$fields;
    $values=(array)$values;
    $criterias="";

    foreach($fields as $idx=>$field) {
        $criterias.=" AND ".$field."=".$db->Quote($values[$idx]);
	}
	$criterias=preg_replace("/AND/","WHERE",$criterias,1);

	$db->StartTrans();
    foreach($tbArray as $ind => $data){
        $sql = "DELETE FROM ".$data.$criterias;
		$db->Execute($sql);
    }
    return $db->CompleteTrans();
}


/** Supprime une ligne d'un table selon un tableau de critères
 * IN : Table, tableau de critère de type (CRIT=>VALUE)
 * OUT : maj DB
 */
function deleteSQLFromArrayOfCrit($table,$arrId)

    {
    global $db;
        $sql = "DELETE FROM $table  WHERE ";
        $vc = count($arrId);
        foreach($arrId as $ind => $data){
            $sql .= "$ind=".$db->Quote($data);
            $vc--;
            if($vc) $sql .= " AND ";
        }

        $db->Execute($sql);
        debug("delete: ".$sql, "red", true);
}


/**
listCheckbox : remplit et selectionne une liste d'options
$nom : nom des boutons
$que : requ�te sql
$index_a : chaine ou array contenant les index a comparer pour la selection
$str : chaine � comparer
$print : affiche directement (1) ou retourne une chaine ?
$addCodeToInsert : permet d'insérer du code brut après chaque input.
	C'est notamment utile pour ajouter d'autres input (id_type_val) qd on génère des liste de valeurs (ld 07/11/08)
$cols : nb de checkbox par ligne avec mise en place d'un tableau
*/
// VP 3/07/09 : ajout paramètre $cols
// VP 27/07/09 : correction bug affichage table
function listCheckbox($nom,$que,$index_a,$check=array(),$print=1,$addCodeToInsert='',$cols='',$display='inline-block')
{
    global $db;
    if(is_array($index_a))
    {
        $index = $index_a[0];
        $cnt = count($index_a); // pour le cas ou le tableau n'a qu'un element
        $index2 = $index_a[$cnt - 1];
    }
    else
    {
        $index = $index_a;
        $index2 = $index_a;
    }

    $result = $db->Execute($que);
    $strRes = "";
	if ($result) {
		if(!empty($cols)){
			$strRes .="<table border='0' width='100%'>";
			if($cols > 0) $pourcent= intval(100 / $cols);
		}
		$i=0;
	    while($list = $result->FetchRow())
	    {
			if(!empty($cols)) {
				if(($i % $cols) == 0) $strRes .="<tr>";
				$strRes .="<td width='".$pourcent."%' valign='top'>";
			}
			// MS 31/01/2013 ajout d'un span qui regroupe checkbox et label pour �viter que le label ne se retrouve parfois � la ligne suivante
	        $strRes .= '<span style="'.(isset($display) && !empty($display)?'display : '.$display.';':'').'"><input id="'.$nom.$list[$index].'" name="'.$nom.'" type="checkbox" value="'. $list[$index].'" alt="'.htmlspecialchars ($list[$index2]).'"';
			if(!is_array($check)) $check = array($check);
	        if(is_array($check) && in_array($list[$index],$check)) $strRes .= " checked=\"checked\" ";
	        $strRes .= "/><label for='".$nom.$list[$index]."'>".$list[$index2]."</label></span>";
	    	$strRes .=$addCodeToInsert; //code additionnel inséré
			if(!empty($cols)) {
				$strRes .="</td>";
				$i++;
				if(($i % $cols) == 0) $strRes .="</tr>";
			}
	    }
	    $result->Close();
		if(!empty($cols)){
			while(($i % $cols) != 0) {
				$strRes .="<td></td>";
				$i++;
				if(($i % $cols) == 0) $strRes .="</tr>";
			}
			$strRes .="</table>";
		}
	} else $strRes="";

    if ($print) print $strRes; else return ($strRes);
}

/**
listRadio : remplit et selectionne une liste d'options
$nom : nom des boutons
$que : requ�te sql
$index_a : chaine ou array contenant les index a comparer pour la selection
$str : chaine � comparer
$print : affiche directement (1) ou retourne une chaine ?
*/

function listRadio($nom,$que,$index_a,$str,$print=1, $cols='')
{
    global $db;
    if(is_array($index_a))
    {
        $index = $index_a[0];
        $cnt = count($index_a); // pour le cas ou le tableau n'a qu'un element
        $index2 = $index_a[$cnt - 1];
    }
    else
    {
        $index = $index_a;
        $index2 = $index_a;
    }

    $result = $db->Execute($que);
    debug($que, "red",true);
    debug($result, "gold",true);

	if($str === 0) $str = $index;
	$strRes="";
	if ($result) {
		if(!empty($cols)){
			$strRes .="<table border='0' width='100%'>";
			if($cols > 0) $pourcent= intval(100 / $cols);
		}
		$i=0;
            $num=0;
	    while($list = $result->FetchRow())
	    {
	    	if(!empty($cols)) {
				if(($i % $cols) == 0) $strRes .="<tr>";
				$strRes .="<td width='".$pourcent."%' valign='top'>";
			}
	        $strRes .= '<input name="'.$nom.'" type="radio" id="'.$nom.$num.'" value="'. $list[$index].'"';
	        if(in_array($list[$index],(array)$str)) $strRes .= " checked=\"checked\" ";
	        $strRes .= "/><label for='".$nom.$num."'>".$list[$index2]."</label>&nbsp;&nbsp;&nbsp;";
	    	if(!empty($cols)) {
				$strRes .="</td>";
				$i++;
				if(($i % $cols) == 0) $strRes .="</tr>";
			}
                $num++;        
	    }
	    $result->Close();
		if(!empty($cols)){
			while(($i % $cols) != 0) {
				$strRes .="<td></td>";
				$i++;
				if(($i % $cols) == 0) $strRes .="</tr>";
			}
			$strRes .="</table>";
		}
	}

    if ($print) print $strRes; else return ($strRes);
}

function getLastIdDoc() {
		global $db;
		$sql="SELECT max(id_doc) AS LASTID from t_doc";
		$result = $db->GetRow($sql);
		return $result['LASTID'];
	}

	
	
	
	
	
// MS - 25.11.16 - Fonction équivalente à listOptions mais qui utilise la fonction browseindex (navigation dans les termes indexés d'un champ Solr) au lieu d'une requête SQL 
function listOptionsSolr($chercheSolrObj,$field_solr,$str=0,$print=1,$alphanum_sort = false){
	$index = $chercheSolrObj->browseIndex($field_solr,'',1000,$alphanum_sort);	
	$strRes = "";
	
	if(!empty($index)){
		foreach($index as $key=>$number){
			$strRes .= '<option value="'. $key.'"';
			if(strtoupper($key) == strtoupper($str)
				|| in_array(strtoupper($key),array_ucase((array)$str),true) == true) {
				$strRes .= " selected=\"selected\" ";
			}
			$strRes .= ">".$key."</option>";
		}
	}
	
	if ($print) {
        print $strRes;
    } else {
        return ($strRes);
    }
}
	
	
/**
listOptions : remplit et selectionne une liste d'options
$que : requ�te sql
$index_a : chaine ou array contenant les index a comparer pour la selection
$str : chaine � comparer
$print : affiche directement (1) ou retourne une chaine ?
*/
// VP 10/9/09 : option disabled si commence par -
function listOptions($que,$index_a,$str = 0,$print=1,$cachedTime=360)
{
    global $db;
    if(is_array($index_a)) {
        $index = $index_a[0];
        $cnt = count($index_a); // pour le cas ou le tableau n'a qu'un element
        $index2 = $index_a[$cnt - 1];
    } else {
        $index = $index_a;
        $index2 = $index_a;
    }
    $result = $db->CacheExecute($cachedTime,$que);
    if($str === 0) {
        $str = $index;
    }
    $strRes = "";
	if ($result) {
	    while($fonds = $result->FetchRow())
	    {
	        $strRes .= '<option value="'. $fonds[$index].'"';
	        //update VG 07/11/2012 : comparaison d�sormais insensible � la casse
	        if(in_array(strtoupper($fonds[$index]),array_ucase((array)$str),true) == true) {
                $strRes .= " selected=\"selected\" ";
            }
			if($fonds[$index][0]=='-' || $fonds[$index2][0]=='-') {
                $strRes .= " disabled=\"disabled\" ";
            }
			$lbl = (defined($fonds[$index2])?constant($fonds[$index2]):$fonds[$index2]);
	        $strRes .= ">".$lbl."</option>";
	    }
	    $result->Close();
	} else {
        $strRes="";
    }
    if ($print) {
        print $strRes;
    } else {
        return ($strRes);
    }
}


/** Transforme un tableau d'index/val en chaine d'options HTML
 *  IN : tableau de valeurs (type IDX=>VAL, 1 dimension), prefixe(ajouté en face des IDX), valeur sélectionnée, sortie (print ou string)
 *  OUT : chaine HTML avec les options (+ selected si valeur trouvée). Si print est vrai, la chaine est imprimée directement
 */
// VP 10/9/09 : option disabled si commence par -
function listOptionsFromArray($tlist,$value,$prefix = "",$print=true,$addBlank=false)
{
	if(!is_array($value)) {
		$value = array($value);
	}

	if ($addBlank) $strRes="<option value=''></option>";
	if (!empty($tlist)) {
		foreach ($tlist as $idx=>$val) {
			$strRes.="<option value='".$prefix.$idx."' ";
			//if ($idx==$value) $strRes.=" selected='selected' ";
			if (in_array($idx, $value)) $strRes.=" selected='selected' ";
			if((isset($idx[0]) && $idx[0]=='-') || (isset($val[0]) && $val[0]=='-')) $strRes .= " disabled='disabled' ";
			if ($val) $strRes.=">".$val."</option>";
			else $strRes.=">".$idx."</option>";

		}
	}
	if ($print) print $strRes; else return $strRes;
}

/** addCriterias : complète une chaine SQL à partir d'un tableau de critères
 *  L'ajout de critères est intelligent (choix automatiquement entre =, LIKE ou IN)
 */
function addCriterias ($arrCriterias) {
        $sql = '';
    	$arrCriterias=(array)$arrCriterias;
    	if (count($arrCriterias) >=1) {
    	$sql.=" WHERE ";
    	foreach($arrCriterias as $fld => $data){
                $tabCrit[]= $fld.sql_op($data, $fld); //Par LD le 27/12, strtoupper viré
        }
        $sql.=implode(" AND ",$tabCrit);
		}
		return $sql;
}

/** addOrders : complète une chaine SQL à partir d'un tableau d'ordres
 * 	le tableau est de type : 'FIELD' => 'ORDER' ex: 'DOC_TITRE'=>'ASC'
 */

function addOrders ($arrOrders) {
        $sql = '';
	$arrOrders=(array)$arrOrders;
    	if (count($arrOrders) >=1) {
    	$sql.=" ORDER BY ";
     	foreach($arrOrders as $fld => $direction){
                $tabOrder[]= strtoupper($fld." ".$direction);
    	}
    	$sql.=implode (" , ",$tabOrder);
		}
		return $sql;
}

function getItem($table,$arrFields="",$arrCriterias="",$arrOrders="",$uniquerow=1) {
	global $db;
	/**Fonction générique de récupération de données simple (une table)
	* $table = table
	* $arrFields = tableau des champs à récupérer, si vide = *
	* $arrCriterias = tableau des critères de type CHAMP => VALEUR. VALEUR peut être lui-même un tableau. Cr
	* dans ce cas, la requête est faite avec IN...
	* $arrOrder = tableau des champs à ordonner, de type CHAMP => SENS (ASC ou DESC) **/

	$arrFields=(array)$arrFields;

	$fldList=strtoupper(implode(" , ",$arrFields));
	if (empty($fldList)) $fldList=" * ";
	$sql="SELECT ". $fldList." FROM ".strtolower($table);


	$sql.=addCriterias($arrCriterias);
	$sql.=addOrders($arrOrders);

	if ($uniquerow) return $db->GetRow($sql); else {
		$rs=$db->GetAll($sql);
		// Les lignes suivantes sont destinées à la réorganisation des résultats dans l'ordre des critères.
		// Ex : si on a "WHERE champs in (4,2,1)" on retourne la valeur 4 puis 2 puis 1.
		foreach ($arrCriterias as $fld=>$vals) {
			if (is_array($vals)) { //On a un tableau de valeurs, on trie. Sinon au
				foreach ($vals as $val) { //alors on fait correspondre l'ordre des résultats avec l'ordre des valeurs
					foreach ($rs as $idx=>$row) {
						if ($row[$fld]==$val) $rsSorted[]=$row;
					}
				}
			}
		}
		if (isset($rsSorted) && $rsSorted) $rs=$rsSorted;
		return $rs;
	}


}


/** Escaping the value WITHOUT surrounding quotes
	 * 	NOTE : les cas \\' et \\" permettent d'échapper des caractères " et ' déjà échappés.
	 *  C'est notamment le cas lorsque l'on sauve des requetes dans l'historique
	 *
	 */
function simpleQuote($s) {

		$s = str_replace(array("\\'",'\\"','"',"'"),array("\\\\'",'\\\\"','\"',"\'"),$s);

		return $s;
	}


/** tranformation intelligente d'une valeur ou d'un tableau de valeur en chaine SQL.
 *  gardé pour rétrocompatibilité, puisque l'on utilise sql_op de class_cherche pour ttes les recherches
 */
function sql_op($valeur, $field=""){
		global $db;
        $valeur=(array)$valeur;

        // Une seule valeur passée
        if (count($valeur)==1) {
			$valeur=$valeur[0];
        	//Il y a un % ou un * : recherche type textuelle (LIKE) sinon recherche avec =
        	if((strpos($valeur,"%")>0)||(strpos($valeur,"*")>0))  {
        		$valeur=" LIKE '".(str_replace("*","%",simpleQuote($valeur)))."'";
        	} elseif(!empty($field) && strpos($field, "ID_") === 0 && strpos($field, "ID_LANG") === false && strpos($field, "ID_TAPE") === false) {
				$valeur = " = '".intval($valeur)."'";
        	}else {
        	$valeur = " = ".$db->Quote($valeur);
        	}
        } else {
        // Plusieurs valeurs, on agrège
        $valeur= " IN ('".implode("','",simpleQuote($valeur))."')";

        }
		return $valeur;
    }

function insertSQL($table,$tabInsert)
{
    global $db;
    // $rs = $db->Execute("SELECT * FROM $table WHERE 1=-1");
	// $sql = $db->GetInsertSQL($rs, $tabInsert);
	// $ok = $db->Execute($sql);
	$ok = $db->insertBase($table,"",$tabInsert);
}

function updateSQL($table,$tabSet,$tabWhere)
{
    global $db;

	$vc = count($tabWhere);
    if($vc){
        $sql = " WHERE ";
        foreach($tabWhere as $ind => $data){
            $sql .= $ind."=".$db->Quote($data);
            $vc--;
            if($vc) $sql .= " AND ";
        }
    }
	$rs = $db->Execute("SELECT * FROM $table $sql");
	$sql = $db->GetUpdateSQL($rs, $tabSet);
	if (!empty($sql)) $ok = $db->Execute($sql);
}

/** Fonction pour mettre en majuscule les valeurs d'un tableau à 1 dimension
 *  IN : tableau
 * 	OUT : tableau avec valeurs en majuscule.
 */
function array_ucase($arr) {
	foreach ($arr as $k=>& $v) $v=strtoupper($v); //mise en maj des valeurs du tableau
	return $arr;
}

/** Fonction universelle d'encodage de chaine pour passer au format XML
 *  IN : chaine
 *  OUT : chaine encodée
 */
function xml_encode($str) {

	return str_replace(array("<",">","<br/>","&"),array("&lt;","&gt;","\n","&amp;"),$str);

}


// Fonction qui crée une fonction JS à partir de paramètres d'un formulaire de saisie.
// Cette fonction JS permet la création d'une ligne d'une liste dynamique (ex: lex dans saisie doc, doc_acc etc)
//
function makeFormJSFunction2($divID,$arrFields,$class) {
	global $db;

	if (empty($arrFields)) return '';
	$html="<script>\n
		function add_".$divID."(row) {\n //alert(row[0]+' '+row[1]);\n
	    str=\"<div class='".$class."' id='\"+row[0]+\"'>\";\n inHTML='';\n";
	$html.="for (i=1;i<".(count($arrFields)+1).";i++) {if (typeof(row[i])=='undefined') row[i]='';}\n";
		foreach ($arrFields as $idx=>$fld) {
					switch($fld['TYPE']) {
						case 'SPAN' :
						case 'DIV' :
						case 'LABEL' :
							$html.="inHTML+=\"<".$fld['TYPE']."  ".(isset($fld['ATTR'])?$fld['ATTR']:'')."  ".((isset($fld['CLASSE']) && $fld['CLASSE'])?"class='".$fld['CLASSE']."'":"")." ".($fld['ID']?"id='".$fld['ID']."'":"")." ".($fld['NAME']?"name='".$fld['NAME']."'":"").">\"+row[".($idx+1)."]+\"</".$fld['TYPE'].">\";\n";
						break;
						case 'HREF' :
							$html.="inHTML+=\"&nbsp;<a ".($fld['CLASSE']?" class='".$fld['CLASSE']."'":"")."' href='\"+row[".($idx+1)."]+\"' ".$fld['ATTR']."  '>".($fld['LIB']?$fld['LIB']:"\"+row[".($idx+1)."]+\"")."</a>  \";\n";
						break;
						case 'SELECT' :
							// A faire évoluer pour avoir ou non une valeur vide ?
							$html.="inHTML+=\"&nbsp;".$fld['LIB']."<select ".(isset($fld['CLASSE'])?" class='".$fld['CLASSE']."'":"")." name='".$fld['NAME']."' ".(isset($fld['ATTR'])?$fld['ATTR']:'')." id='".$fld['NAME']."'>\";\n";
							$html.="strOptions=\"".listOptionsFromArray($fld['OPTIONS'],$fld['VALUE'],'',false,true)."\";\n";
							//$html.="alert(row[".($idx+1)."]);\n";
							$html.="offset=strOptions.indexOf(\"'\"+row[".($idx+1)."]+\"'\");\n";

							$html.="if (offset!=-1 && row[".($idx+1)."] != '') {\n";
							$html.="start=offset +1;\n";
							$html.="  strOptions=strOptions.substr(0,start+row[".($idx+1)."].length+1)+\" selected='true' \"+strOptions.substr(start+row[".($idx+1)."].length+2,strOptions.length-start);}\n";
							$html.="inHTML+=strOptions+\"</select>\";\n";
						break;
						case 'MOVEUP' :
							$html.= "inHTML+=\"<img class='miniTrash' ".(isset($fld['ATTR'])?$fld['ATTR']:'')." id='mvVal' onClick='moveUp(this)' src='".imgUrl."arrow_moveup.gif'/>\";\n ";
						break;
						case 'MOVEDOWN' :
							$html.= "inHTML+=\"<img class='miniTrash' ".(isset($fld['ATTR'])?$fld['ATTR']:'')." id='mvVal' onClick='moveDown(this)' src='".imgUrl."arrow_movedown.gif'/>\";\n ";
						break;
						case 'TRASH' :
							//LD 30/10/08 : nv param trash_img
							$html.= "inHTML+=\"<img class='miniTrash' ".(isset($fld['ATTR'])?$fld['ATTR']:'')." id='delVal' onClick=\\\"removeValue('\"+row[0]+\"')\\\" src='".imgUrl.(!empty($fld['IMG'])?$fld['IMG']:"button_drop.gif")."' />\";\n";
						break;
						//XB 25/02/14: sauvegarde si on clique sur un lien ?
						case 'MOVEUP_CONFIRM' :
							$html.= "inHTML+=\"<img class='miniTrash' ".(isset($fld['ATTR'])?$fld['ATTR']:'')." id='mvVal' onClick='moveUp(this);formChanged=true;' src='".imgUrl."arrow_moveup.gif'/>\";\n ";
						break;
						case 'MOVEDOWN_CONFIRM' :
							$html.= "inHTML+=\"<img class='miniTrash' ".(isset($fld['ATTR'])?$fld['ATTR']:'')." id='mvVal' onClick='moveDown(this);formChanged=true;' src='".imgUrl."arrow_movedown.gif'/>\";\n ";
						break;
						case 'TRASH_CONFIRM' :
							//LD 30/10/08 : nv param trash_img
							$html.= "inHTML+=\"<img class='miniTrash' ".(isset($fld['ATTR'])?$fld['ATTR']:'')." id='delVal' onClick=\\\"removeValue('\"+row[0]+\"');formChanged=true;\\\" src='".imgUrl.(!empty($fld['IMG'])?$fld['IMG']:"button_drop.gif")."' />\";\n";
						break;
						case 'EDIT' :
							$html.= "inHTML+=\"<img class='miniTrash' ".$fld['ATTR']." id='editVal' onClick=\\\"saveIfAndGo('\"+Quote(row[".($idx+1)."])+\"')\\\" src='".imgUrl.((isset($fld['IMG']) && $fld['IMG'])?$fld['IMG']:'button_modif.gif')."' />\";\n";
						break;
						case 'LIB' :
							$html.="inHTML+=\"".$fld['LIB']."\";";
						break;
						case 'IMG' :
							$html.="inHTML+=\"<img ".$fld['ATTR']." ".($fld['CLASSE']?" class='".$fld['CLASSE']."'":"")." ".($fld['ID']?" id='".$fld['ID']."'":"")." src='\"+row[".($idx+1)."]+\"' />\";\n";
						break;
						case 'BR' :
						//by LD 30/10/08 : transfo en div clear both
							$html.="inHTML+=\"<div style='clear:both;'> </div>\";";
						break;
						//by ld 30/10/08 : gestion des buttons
						case 'BTN' :
							$html.="inHTML+=\"".$fld['LIB']."<input ".($fld['CLASSE']?" class='".$fld['CLASSE']."'":"")." type='button' name=\\\"".$fld['NAME']."\\\" ".(empty($fld['VALUE'])?"value=\\\"\"+encodeQuotes(row[".($idx+1)."])+\"\\\"":"value=".quoteField($fld['VALUE']) ).($fld['ID']?" id='".$fld['ID']."'":" id='\"+row[0]+\"".$fld['NAME']."'")." ".$fld['ATTR']." >".addslashes($fld['CONTENT'])."</button>\";\n";
						break;
						default :
						//$html.="alert(row[".($idx+1)."]);";
							//$html.="inHTML+=\"".$fld['LIB']."<input ".($fld['CLASSE']?" class='".$fld['CLASSE']."'":"")." type='".$fld['TYPE']."' name=\\\"".$fld['NAME']."\\\" ".(empty($fld['VALUE'])?"value=\\\"\"+encodeQuotes(row[".($idx+1)."])+\"\\\"":"value=".$db->Quote($fld['VALUE']) ).($fld['ID']?" id='".$fld['ID']."'":"")." ".$fld['ATTR']." />\";\n";
							$html.="inHTML+=\"".(isset($fld['LIB'])?$fld['LIB']:'')."<input ".((isset($fld['CLASSE']) && $fld['CLASSE'])?" class='".$fld['CLASSE']."'":"")." type='".$fld['TYPE']."' name=\\\"".$fld['NAME']."\\\" ".(empty($fld['VALUE'])?"value=\\\"\"+encodeQuotes(row[".($idx+1)."])+\"\\\"":"value=".quoteField($fld['VALUE']) ).($fld['ID']?" id='".$fld['ID']."'":" id='\"+row[0]+\"".$fld['NAME']."'")." ".((isset($fld['ATTR']))?$fld['ATTR']:'')." />\";\n";
						break;
						}
		}
	$html.= "str=str+inHTML+\"</div>\";\n return new Array(str,inHTML);\n}";
	$html.= "</script>";
	return $html;
}

// Fonction qui crée une fonction JS à partir de paramètres d'un formulaire de saisie.
// Cette fonction JS permet la création d'une ligne d'une table de lien
//
function makeFormJSFunctionTL($divID, $class) {
	global $db;

	$html = "<script>\n
	var curelt = document.getElementById('$divID\$blank');
	while (curelt != null && curelt.tagName != 'FORM')
	curelt = curelt.parentNode;
	if (curelt.tagName == 'FORM'){
	var onsubmit = document.createAttribute('onsubmit');
	onsubmit.nodeValue = \"clear_empty_$divID('$divID\$blank');\" + curelt.getAttribute('onsubmit');
	curelt.setAttributeNode(onsubmit);
	}

	function clear_empty_$divID(divId) {
	document.getElementById(divId).innerHTML = '';
	}

	var num_row = 1;\n";

	$html .= "function add_".$divID."(link_name, id_prim, fieldtab, valuetab) {
	var div_id = link_name + '\$tab';
	var div = $(this).document.getElementById(div_id);
	var inform = $(this).document.getElementById(link_name + '\$blank').cloneNode(true);

	inform.style.display='block';
	inform.className='miniTab';
	inform.id='" . $divID . "$' + num_row;
	inform.name='" . $divID . "\$div';

	var trashimg = document.createElement('img');
	trashimg.id='delTL';
	trashimg.className='miniTrash';
	trashimg.src = '" . imgUrl . "button_drop.gif';
	trashimg.title='" . kSupprimer . "';
	trashimg.alt='" . kSupprimer . "';
	trashimg.id='del\$'+num_row_$divID;
	trashimg.onclick=function(){remove_$divID(this.id, '' + link_name + '');};

	inform.appendChild(trashimg);
	div.appendChild(inform);

	if (fieldtab != 0)
	for (var i = 0; i < fieldtab.length; i++) {
	var divtab = document.getElementsByName('".$divID."[][' + fieldtab[i] + ']');
	if(divtab.length > 0)
	divtab[num_row - 1].value = valuetab[i];
	}
	num_row++;
	}\n";

	$html .= "function remove_$divID(num_div, link_name){
	if (confirm('" . kConfirmDelete . "')) {
	if(num_div.indexOf('\$')>0){
	arrTmp=num_div.split('$');
	num_div=arrTmp[1];
	}
	var div_id = link_name + '\$tab';
	var bigdiv = $(this).document.getElementById(div_id);
	var divtab = document.getElementsByName(link_name + '\$div');
	for (var i = 0; i < divtab.length; i++)
	if (divtab[i].id == link_name + '\$' + num_div)
	bigdiv.removeChild(divtab[i]);
	//alert(divtab[i].id);
	}}\n";

	$html.= "</script>";
	return $html;
}


// Fonction qui crée une fonction JS à partir de paramètres d'un formulaire de saisie.
// Cette fonction JS permet la création d'une ligne dans le tableau de la table de lien

function makeFormJSFunctionTL2($divID, $lastid) {
	global $db;

	$html = "<script>
	var num_row_$divID = $lastid;

	var curelt = document.getElementById('$divID\$blank');
	while (curelt != null && curelt.tagName != 'FORM')
	curelt = curelt.parentNode;
	if (curelt.tagName == 'FORM'){
	var onsubmit = document.createAttribute('onsubmit');
	onsubmit.nodeValue =  \"clear_empty_$divID('$divID');\" + curelt.getAttribute('onsubmit');
	curelt.setAttributeNode(onsubmit);
	}

	function clear_empty_$divID(link_name) {
	var table = $(this).document.getElementById(link_name + '\$tab');
	var row = $(this).document.getElementById(link_name + '\$blank');
	//table.removeChild(row);
	row.innerHTML = '';
	}

	function concat_collection(obj1, obj2) {
	var i;
	var arr = new Array();
	var len1 = obj1.length;
	var len2 = obj2.length;
	for (i=0; i<len1; i++) {
	arr.push(obj1[i]);
	}
	for (i=0; i<len2; i++) {
	arr.push(obj2[i]);
	}
	return arr;
	}

	function add_$divID(link_name, id_pers, var_field, var_value, with_treatment) {
	var div_id = link_name + '\$tab';
	var div = $(this).document.getElementById(div_id);
	var inform = $(this).document.getElementById(link_name + '\$blank').cloneNode(true);

	inform.removeAttribute('style');
	var newid = inform.id;
	newid = newid.replace('blank', '' + num_row_$divID + '');
	inform.id=newid;

	var elems = inform.getElementsByTagName('input');
	var elems2 = inform.getElementsByTagName('select');
	elems = concat_collection(elems, elems2);
	var elems2 = inform.getElementsByTagName('textarea');
	elems = concat_collection(elems, elems2);
	
	k = 0;
	for(i = 0; i < elems.length; i++)
	if (elems[i].name == \"input$divID\"){
	ename = elems[i].id;
	if (ename.indexOf('_val[]') == -1)
	ename = ename.replace('[]', '[' + num_row_$divID + ']');
	elems[i].name= ename;

	if (ename.indexOf('[ID_ROW_TL]') >= 0)
	elems[i].value=num_row_$divID;

	if (id_pers != '' && ename.indexOf('[ID_PERS]') >= 0)
	elems[i].value=id_pers;
	if (var_field != '' && var_value != '' && ename.indexOf(var_field) >= 0)
	elems[i].value=var_value;
	}

	var elems = inform.getElementsByTagName('img');
	for(i = 0; i < elems.length; i++){
	if (elems[i].name == \"miniTrash\"){
		elems[i].id='del\$'+num_row_$divID;
		elems[i].onclick=function(){remove_$divID(this.id, '' + link_name + '');};
	}
	}

	div.appendChild(inform);
	num_row_$divID++;

	if (with_treatment == true)
	treat_$divID(num_row_$divID - 1, '$divID');
	}

	function remove_$divID(num_row, link_name){
		if (confirm('" . kConfirmDelete . "')) {
			if(num_row.indexOf('\$')>0){
				arrTmp=num_row.split('$');
				num_row=arrTmp[1];
			}
			var table = $(this).document.getElementById(link_name + '\$tab');
			var row = $(this).document.getElementById(link_name + '\$' + num_row);
			table.removeChild(row);
		}
	}

	function filter_$divID(field_name){
	var inform = $(this).document.getElementById('$divID\$tab');
	var elems = inform.getElementsByTagName('input');
	var elems2 = inform.getElementsByTagName('select');
	elems = concat_collection(elems, elems2);
	k = 0;
	for(i = 0; i < elems.length; i++){
	if (elems[i].getAttribute('name').indexOf('[' + field_name + ']') >= 0){
	var valfilter = document.getElementById('filter$divID').value;
	var curelt = elems[i];
	while (curelt != null && curelt.tagName != 'TR')
	curelt = curelt.parentNode;
	if (curelt.tagName == 'TR'){
	var style = document.createAttribute('style');
	if (valfilter != '' && valfilter != elems[i].value)
	style.nodeValue = 'display:none';
	else
	style.nodeValue = '';
	curelt.setAttributeNode(style);
	}
	}
	}
	}
	</script>";
	return $html;
}


/** Décode une chaine ($str) avec une clé ($filter) **/
function password_decode($filter, $str)
{
   $filter = md5($filter);
   $letter = -1;
   $newstr = '';
   $str = base64_decode($str);
   $strlen = strlen($str);

   for ( $i = 0; $i < $strlen; $i++ )
   {
      $letter++;

      if ( $letter > 31 )
      {
         $letter = 0;
      }

      $neword = ord($str{$i}) - ord($filter{$letter});

      if ( $neword < 1 )
      {
         $neword += 256;
      }

      $newstr .= chr($neword);
   }

   return $newstr;
}

/** Encode une chaine ($str) avec une clé ($filter) **/
function password_encode($filter, $str)
{
   $filter = md5($filter);
   $letter = -1;
   $newpass = '';

   $strlen = strlen($str);

   for ( $i = 0; $i < $strlen; $i++ )
   {
      $letter++;

      if ( $letter > 31 )
      {
         $letter = 0;
      }

      $neword = ord($str{$i}) + ord($filter{$letter});

      if ( $neword > 255 )
      {
         $neword -= 256;
      }

      $newstr .= chr($neword);

   }

   return base64_encode($newstr);
}

function check_passwd_normes_cnil($password=''){
	if(strlen($password)>=8 && strlen($password)<=30
	&& preg_match('/[A-Z]/',$password) && preg_match('/[a-z]/',$password) && preg_match('/[0-9$*?!,.()-+]/',$password) && !preg_match('/[ \t]/',$password) ){
		return true;
	}else{
		return false ;
	}
}

function getPasswordHash($password=null,$salt=null){
	if( $password == null || $salt == null ){
		return false;
	}
	return (hash("sha256",$salt.$password));
}

function random_alphanum_string($opts = array())
{
	if(!empty($opts)){
		if(isset($opts['fullrandom_length'])){
			// possibilité de définir un nombre de caractère et d'avoir une répartition aléatoire de char minuscules, majuscules et de chiffres (avec un minimum de 1 char par catégorie)
			$nb_nums= rand(1,$opts['fullrandom_length'] - 2);
			$nb_mins= rand(1,$opts['fullrandom_length'] - 1 - $nb_nums);
			$nb_majs= $opts['fullrandom_length'] - $nb_nums - $nb_mins;
		}else{
			if(isset($opts['majs']) && is_int($opts['majs'])){
				$nb_majs = $opts['majs'] ; 
			}else{
				$nb_majs = 1;
			}
			if(isset($opts['mins']) && is_int($opts['mins'])){
				$nb_mins = $opts['mins'] ; 
			}else{
				$nb_mins = 6;
			}
			if(isset($opts['nums']) && is_int($opts['nums'])){
				$nb_nums = $opts['nums'] ; 
			}else{
				$nb_nums = 1;
			}
		}
	}else{
		$nb_majs = 1;
		$nb_mins = 6;
		$nb_nums = 1;
	}
	$character_set_array = array();
	$character_set_array[] = array('count' => $nb_majs, 'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
	$character_set_array[] = array('count' => $nb_mins, 'characters' => 'abcdefghijklmnopqrstuvwxyz');
	$character_set_array[] = array('count' => $nb_nums, 'characters' => '0123456789');
	$temp_array = array();
	foreach ($character_set_array as $character_set) {
		for ($i = 0; $i < $character_set['count']; $i++) {
			$temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
		}
	}
	shuffle($temp_array);
	return implode('', $temp_array);
}

//Scan des headers de page
function getHeaders() {
			foreach ($_SERVER as $h => $v ) {
                //if( ereg( 'HTTP_(.+)', $h, $hp ) ) $headers[$hp[1]] = $v ;
                if(preg_match ( '/HTTP_(.+)/' , $h, $hp )) $headers[$hp[0]] = $v ;
            }
			return $headers;
	}

// Renvoie le chemin du fichier demandé en prenant s'il existe celui du site sinon celui du moteur
function getSiteFile($dir,$fichier){
	if(defined($dir) && is_file(constant($dir).$fichier)){
		return constant($dir).$fichier;
	}elseif(defined($dir."Core") && is_file(constant($dir."Core").$fichier)){
		return constant($dir."Core").$fichier;
	}elseif(strpos($dir."/".$fichier,baseDir) === 0 ||  strpos($dir."/".$fichier,siteDirCore) === 0){
		// Si le path complet est un path et non une constante et un fichier, on applique une méthode un peu différente 
		// Si on détecte qu'on travaille sur un path faisant référence au site, 
		if(strpos($dir."/".$fichier,baseDir) === 0 ){
			// Alors on déduit le path équivalent dans le site moteur, 
			$site_path = $dir."/".$fichier; 
			$core_path = str_replace(baseDir."/",siteDirCore,$dir."/".$fichier);
		}else{
			// Si on détecte qu'on travaille sur une référence à un fichier site moteur, alors on déduit le path équivalent sur le site 
			$site_path = str_replace(siteDirCore,baseDir."/",$dir."/".$fichier); 
			$core_path =$dir."/".$fichier; 
		}
		// De là on applique la meme logique, 
		// si le site_path existe, alors il prends la priorité sur le path équivalent coté moteur
		if(is_file($site_path)){
			return $site_path ; 
		}else if (is_file($core_path)){
			return $core_path ; 
		}else{
			return siteDirCore."index.php";
		}
	}else{
		return siteDirCore."index.php";
	}
}

    function resolve_path($path) {
        $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
		if(substr($path,0,1) == DIRECTORY_SEPARATOR ){
			$add_root = true ; 
		}else{
			$add_root = false ;
		}
        $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
        $absolutes = array();
        foreach ($parts as $part) {
            if ('.' == $part) continue;
            if ('..' == $part) {
                array_pop($absolutes);
            } else {
                $absolutes[] = $part;
            }
        }
        return ($add_root?DIRECTORY_SEPARATOR:'').implode(DIRECTORY_SEPARATOR, $absolutes);
    }

/*****************************************/
/*					CONVERSION CARACTERES               */
/*****************************************/


function macRoman_to_utf8($str) {
        // Changement \x6 en \xC3\xA6 (æ)
  return strtr($str, array("\x80"=>"\xC3\x84", "\x81"=>"\xC3\x85",
  "\x82"=>"\xC3\x87", "\x83"=>"\xC3\x89", "\x84"=>"\xC3\x91",
  "\x85"=>"\xC3\x96", "\x86"=>"\xC3\x9C", "\x87"=>"\xC3\xA1",
  "\x88"=>"\xC3\xA0", "\x89"=>"\xC3\xA2", "\x8A"=>"\xC3\xA4",
  "\x8B"=>"\xC3\xA3", "\x8C"=>"\xC3\xA5", "\x8D"=>"\xC3\xA7",
  "\x8E"=>"\xC3\xA9", "\x8F"=>"\xC3\xA8", "\x90"=>"\xC3\xAA",
  "\x91"=>"\xC3\xAB", "\x92"=>"\xC3\xAD", "\x93"=>"\xC3\xAC",
  "\x94"=>"\xC3\xAE", "\x95"=>"\xC3\xAF", "\x96"=>"\xC3\xB1",
  "\x97"=>"\xC3\xB3", "\x98"=>"\xC3\xB2", "\x99"=>"\xC3\xB4",
  "\x9A"=>"\xC3\xB6", "\x9B"=>"\xC3\xB5", "\x9C"=>"\xC3\xBA",
  "\x9D"=>"\xC3\xB9", "\x9E"=>"\xC3\xBB", "\x9F"=>"\xC3\xBC",
  "\xA0"=>"\xE2\x80\xA0", "\xA1"=>"\xC2\xB0", "\xA2"=>"\xC2\xA2",
  "\xA3"=>"\xC2\xA3", "\xA4"=>"\xC2\xA7", "\xA5"=>"\xE2\x80\xA2",
  "\xA6"=>"\xC2\xB6", "\xA7"=>"\xC3\x9F", "\xA8"=>"\xC2\xAE",
  "\xA9"=>"\xC2\xA9", "\xAA"=>"\xE2\x84\xA2", "\xAB"=>"\xC2\xB4",
  "\xAC"=>"\xC2\xA8", "\xAD"=>"\xE2\x89\xA0", "\xAE"=>"\xC3\x86",
  "\xAF"=>"\xC3\x98", "\xB0"=>"\xE2\x88\x9E", "\xB1"=>"\xC2\xB1",
  "\xB2"=>"\xE2\x89\xA4", "\xB3"=>"\xE2\x89\xA5", "\xB4"=>"\xC2\xA5",
  "\xB5"=>"\xC2\xB5", "\xB6"=>"\xE2\x88\x82", "\xB7"=>"\xE2\x88\x91",
  "\xB8"=>"\xE2\x88\x8F", "\xB9"=>"\xCF\x80", "\xBA"=>"\xE2\x88\xAB",
  "\xBB"=>"\xC2\xAA", "\xBC"=>"\xC2\xBA", "\xBD"=>"\xCE\xA9",
  "\xBE"=>"\xC3\xA6", "\xBF"=>"\xC3\xB8", "\xC0"=>"\xC2\xBF",
  "\xC1"=>"\xC2\xA1", "\xC2"=>"\xC2\xAC", "\xC3"=>"\xE2\x88\x9A",
  "\xC4"=>"\xC6\x92", "\xC5"=>"\xE2\x89\x88", "\xC6"=>"\xE2\x88\x86",
  "\xC7"=>"\xC2\xAB", "\xC8"=>"\xC2\xBB", "\xC9"=>"\xE2\x80\xA6",
  "\xCA"=>"\xC2\xA0", "\xCB"=>"\xC3\x80", "\xCC"=>"\xC3\x83",
  "\xCD"=>"\xC3\x95", "\xCE"=>"\xC5\x92", "\xCF"=>"\xC5\x93",
  "\xD0"=>"\xE2\x80\x93", "\xD1"=>"\xE2\x80\x94", "\xD2"=>"\xE2\x80\x9C",
  "\xD3"=>"\xE2\x80\x9D", "\xD4"=>"\xE2\x80\x98", "\xD5"=>"\xE2\x80\x99",
  "\xD6"=>"\xC3\xB7", "\xD7"=>"\xE2\x97\x8A", "\xD8"=>"\xC3\xBF",
  "\xD9"=>"\xC5\xB8", "\xDA"=>"\xE2\x81\x84", "\xDB"=>"\xE2\x82\xAC",
  "\xDC"=>"\xE2\x80\xB9", "\xDD"=>"\xE2\x80\xBA", "\xDE"=>"\xEF\xAC\x81",
  "\xDF"=>"\xEF\xAC\x82", "\xE0"=>"\xE2\x80\xA1", "\xE1"=>"\xC2\xB7",
  "\xE2"=>"\xE2\x80\x9A", "\xE3"=>"\xE2\x80\x9E", "\xE4"=>"\xE2\x80\xB0",
  "\xE5"=>"\xC3\x82", "\xE6"=>"\xC3\x8A", "\xE7"=>"\xC3\x81",
  "\xE8"=>"\xC3\x8B", "\xE9"=>"\xC3\x88", "\xEA"=>"\xC3\x8D",
  "\xEB"=>"\xC3\x8E", "\xEC"=>"\xC3\x8F", "\xED"=>"\xC3\x8C",
  "\xEE"=>"\xC3\x93", "\xEF"=>"\xC3\x94", "\xF0"=>"\xEF\xA3\xBF",
  "\xF1"=>"\xC3\x92", "\xF2"=>"\xC3\x9A", "\xF3"=>"\xC3\x9B",
  "\xF4"=>"\xC3\x99", "\xF5"=>"\xC4\xB1", "\xF6"=>"\xCB\x86",
  "\xF7"=>"\xCB\x9C", "\xF8"=>"\xC2\xAF", "\xF9"=>"\xCB\x98",
  "\xFA"=>"\xCB\x99", "\xFB"=>"\xCB\x9A", "\xFC"=>"\xC2\xB8",
  "\xFD"=>"\xCB\x9D", "\xFE"=>"\xCB\x9B", "\xFF"=>"\xCB\x87"));
}

function utf8_to_MacRoman($str)
{
        // Changement \x6 en \xC3\xA6 (æ)
return strtr($str, array("\xC3\x84"=>"\x80", "\xC3\x85"=>"\x81",
"\xC3\x87"=>"\x82", "\xC3\x89"=>"\x83", "\xC3\x91"=>"\x84",
"\xC3\x96"=>"\x85", "\xC3\x9C"=>"\x86", "\xC3\xA1"=>"\x87",
"\xC3\xA0"=>"\x88", "\xC3\xA2"=>"\x89", "\xC3\xA4"=>"\x8A",
"\xC3\xA3"=>"\x8B", "\xC3\xA5"=>"\x8C", "\xC3\xA7"=>"\x8D",
"\xC3\xA9"=>"\x8E", "\xC3\xA8"=>"\x8F", "\xC3\xAA"=>"\x90",
"\xC3\xAB"=>"\x91", "\xC3\xAD"=>"\x92", "\xC3\xAC"=>"\x93",
"\xC3\xAE"=>"\x94", "\xC3\xAF"=>"\x95", "\xC3\xB1"=>"\x96",
"\xC3\xB3"=>"\x97", "\xC3\xB2"=>"\x98", "\xC3\xB4"=>"\x99",
"\xC3\xB6"=>"\x9A", "\xC3\xB5"=>"\x9B", "\xC3\xBA"=>"\x9C",
"\xC3\xB9"=>"\x9D", "\xC3\xBB"=>"\x9E", "\xC3\xBC"=>"\x9F",
"\xE2\x80\xA0"=>"\xA0", "\xC2\xB0"=>"\xA1", "\xC2\xA2"=>"\xA2",
"\xC2\xA3"=>"\xA3", "\xC2\xA7"=>"\xA4", "\xE2\x80\xA2"=>"\xA5",
"\xC2\xB6"=>"\xA6", "\xC3\x9F"=>"\xA7", "\xC2\xAE"=>"\xA8",
"\xC2\xA9"=>"\xA9", "\xE2\x84\xA2"=>"\XAA", "\xC2\xB4"=>"\xAB",
"\xC2\xA8"=>"\xAC", "\xE2\x89\xA0"=>"\xAD", "\xC3\x86"=>"\XAE",
"\xC3\x98"=>"\xAF", "\xE2\x88\x9E"=>"\xB0", "\xC2\xB1"=>"\xB1",
"\xE2\x89\xA4"=>"\xB2", "\xE2\x89\xA5"=>"\xB3", "\xC2\xA5"=>"\xB4",
"\xC2\xB5"=>"\xB5", "\xE2\x88\x82"=>"\xB6", "\xE2\x88\x91"=>"\xB7",
"\xE2\x88\x8F"=>"\xB8", "\xCF\x80"=>"\xB9", "\xE2\x88\xAB"=>"\xBA",
"\xC2\xAA"=>"\xBB", "\xC2\xBA"=>"\xBC", "\xCE\xA9"=>"\xBD",
"\xC3\xA6"=>"\xBE", "\xC3\xB8"=>"\xBF", "\xC2\xBF"=>"\xC0",
"\xC2\xA1"=>"\xC1", "\xC2\xAC"=>"\xC2", "\xE2\x88\x9A"=>"\xC3",
"\xC6\x92"=>"\xC4", "\xE2\x89\x88"=>"\xC5", "\xE2\x88\x86"=>"\xC6",
"\xC2\xAB"=>"\xC7", "\xC2\xBB"=>"\xC8", "\xE2\x80\xA6"=>"\xC9",
"\xC2\xA0"=>"\xCA", "\xC3\x80"=>"\xCB", "\xC3\x83"=>"\xCC",
"\xC3\x95"=>"\xCD", "\xC5\x92"=>"\xCE", "\xC5\x93"=>"\xCF",
"\xE2\x80\x93"=>"\xD0", "\xE2\x80\x94"=>"\xD1", "\xE2\x80\x9C"=>"\xD2",
"\xE2\x80\x9D"=>"\xD3", "\xE2\x80\x98"=>"\xD4", "\xE2\x80\x99"=>"\xD5",
"\xC3\xB7"=>"\xD6", "\xE2\x97\x8A"=>"\xD7","\xC3\xBF"=>"\xD8",
"\xC5\xB8"=>"\xD9", "\xE2\x81\x84"=>"\xDA", "\xE2\x82\xAC"=>"\xDB",
"\xE2\x80\xB9"=>"\xDC", "\xE2\x80\xBA"=>"\xDD","\xEF\xAC\x81"=> "\xDE",
"\xEF\xAC\x82"=>"\xDF", "\xE2\x80\xA1"=>"\xE0", "\xC2\xB7"=>"\xE1",
"\xE2\x80\x9A"=>"\xE2", "\xE2\x80\x9E"=>"\xE3", "\xE2\x80\xB0"=>"\xE4",
"\xC3\x82"=>"\xE5", "\xC3\x8A"=>"\xE6", "\xC3\x81"=>"\xE7",
"\xC3\x8B"=>"\xE8", "\xC3\x88"=>"\xE9", "\xC3\x8D"=>"\xEA",
"\xC3\x8E"=>"\xEB", "\xC3\x8F"=>"\xEC", "\xC3\x8C"=>"\xED",
"\xC3\x93"=>"\xEE", "\xC3\x94"=>"\xEF", "\xEF\xA3\xBF"=>"\xF0",
"\xC3\x92"=>"\xF1", "\xC3\x9A"=>"\xF2", "\xC3\x9B"=>"\xF3",
"\xC3\x99"=>"\xF4", "\xC4\xB1"=>"\xF5", "\xCB\x86"=>"\xF6",
"\xCB\x9C"=>"\xF7", "\xC2\xAF"=>"\xF8", "\xCB\x98"=>"\xF9",
"\xCB\x99"=>"\xFA", "\xCB\x9A"=>"\xFB", "\xC2\xB8"=>"\xFC",
"\xCB\x9D"=>"\xFD", "\xCB\x9B"=>"\xFE", "\xCB\x87"=>"\xFF"));
}
/*****************************************/
/*					FICHIERS               */
/*****************************************/

    /** Fonction qui récupère l'extension d'un fichier. Stt utilisé pour les matériels / doc_mat et doc_acc
     *  IN : nom de fichier
     * 	OUT : extension en minuscule
     */
    //PC 18/05/2011 : ajout d'une option afin laisser les majuscules intacts
    function getExtension ($file, $keepCase=false) {

        //@update VG 18/05/2010 : "split" devient "explode"
        $ext=explode(".",$file);
        // VP 22/10/2012 : cas des fichiers sans extension
        if(count($ext)>1) $ext=$ext[count($ext)-1];
        else $ext="";
        if ($keepCase)
            return $ext;
        else
            return strtolower($ext);
    }

    /** Retire l'extension d'un chaine de type filename **/
        function stripExtension ($file)
        {
        //$_file=split("\.",$file); //fonction dépreciée depuis php 5.3.0
        $_file=explode(".",$file); // mise a jour avec explode

        if (count($_file)>1)
		unset($_file[count($_file)-1]);//on ôte l'extension

        return implode(".",$_file);
        }

    /** Retourne la taille du fichier **/
    function getFileSize ($file) {
        $size = filesize($file);
        if (empty($size) || $size < 0) {
        // VP 6/11/2015 : utilisation de shell_exec car system affiche le résultat à l'écran
            if(strpos(PHP_OS,'Darwin')===0) {// Mac^
                $size = trim(shell_exec('stat -f %z "$file"'));
            } elseif ((strtoupper(substr(PHP_OS, 0, 3)) == 'LIN')) {// Linux
                $size = trim(shell_exec('stat -c%s "$file"'));
            } else{ // Windows
                $fsobj = new COM("Scripting.FileSystemObject");
                $f = $fsobj->GetFile($file);
                $size = $file->Size;
            }
        }
        return $size;
    }

    // VP 16/09/10 : ajout fonction getFileFromPath (upload)
    function getFileFromPath($path) {
        $name=explode("/",str_replace("\\","/",$path));

        $name=$name[count($name)-1];

        //$name=split("\.",$name);
        //$name=$name[0];
        // $name=str_replace(array('\\' , '/' , ':' , '*' , '?' , '"' , '<' , '>' , '|', ' '),'_', $name);
		$name = removeTrickyChars(replaceAccents($name));
        return $name;
    }
    // MS 20/09/13 : ajout fonction getDirFromPath (upload)
	// renvoi un nom de dossier un épuré (suppression d'un certain nombre de char speciaux )
    function getDirFromPath($path) {
        $path=removeTrickyChars(replaceAccents($path));
        return $path;
    }


    /** Calcul une nouvelle itération pour une chaine **/
    function stringIteration ($str) {
        if(preg_match_all('/(\d+)/', $str, $matches, PREG_OFFSET_CAPTURE )) {
            //print_r($matches);
            $idx=$matches[0][count($matches[0])-1][0];
            $offset=$matches[0][count($matches[0])-1][1];
            $newstr=substr($str,0,$offset).str_pad($idx+1, strlen($idx), "0", STR_PAD_LEFT);
        }else $newstr=$str."1";
        return $newstr;
        }

    function filesize_format ($bytes, $decimal='.', $spacer=' ', $lowercase=false, $unit_fr=false ) {
        $bytes = max(0, (int)$bytes);
		
		if($unit_fr == 'bit'){
			$units = array('Yb' => 1208925819614629174706176, // yottabit
                       'Zb' => 1180591620717411303424,    // zettabit
                       'Eb' => 1152921504606846976,      // exabit
                       'Pb' => 1125899906842624,          // petabit
                       'Tb' => 1099511627776,            // terabit
                       'Gb' => 1073741824,                // gigabit
                       'Mb' => 1048576,                  // megabit
                       'Kb' => 1024,                      // kilobit
                       'b'  => 0);                        // bit
		}else if($unit_fr){
			$units = array('Yo' => 1208925819614629174706176, // yottabyte
                       'Zo' => 1180591620717411303424,    // zettabyte
                       'Eo' => 1152921504606846976,      // exabyte
                       'Po' => 1125899906842624,          // petabyte
                       'To' => 1099511627776,            // terabyte
                       'Go' => 1073741824,                // gigabyte
                       'Mo' => 1048576,                  // megabyte
                       'Ko' => 1024,                      // kilobyte
                       'o'  => 0);                        // byte

		}else{
			$units = array('YB' => 1208925819614629174706176, // yottabyte
                       'ZB' => 1180591620717411303424,    // zettabyte
                       'EB' => 1152921504606846976,      // exabyte
                       'PB' => 1125899906842624,          // petabyte
                       'TB' => 1099511627776,            // terabyte
                       'GB' => 1073741824,                // gigabyte
                       'MB' => 1048576,                  // megabyte
                       'KB' => 1024,                      // kilobyte
                       'B'  => 0);                        // byte
		}
        foreach ($units as $unit => $qty) {
            if ($bytes >= $qty)
            return number_format(!$qty ? $bytes: $bytes /= $qty, 2, $decimal, $spacer).$spacer.($lowercase?strtolower($unit):$unit);
        }
    } // end of 'filesize_format' function();



    /** Retourne récursivement le contenu d'un répertoire
     *  IN  : répertoire, $filter = tableau d'extension possibles
     *  OUT : tableau des fichiers avec  chemin récursif à partir du répertoire $dir
     *  NOTE : LD 22 05 09 => ajout d'un filtre d'exclusion
     */
    function list_directory($dir,$filter='',$exclude='') {

        // VP 28/02/11 : changement initialisation $file_list de '' à array()
        $file_list = array();
        $stack[] = $dir;
        while ($stack) {
            $current_dir = array_pop($stack);
            if ($dh = opendir($current_dir)) {
                while (($file = readdir($dh)) !== false) {

                    if ($file !== '.' && $file !== '..' ) {
                        $current_file = "{$current_dir}/{$file}";
                        if (is_file($current_file)
                            && (!is_array($filter) || in_array(getExtension($file),$filter))
                            && (!is_array($exclude) || !in_array(getExtension($file),$exclude))
                            ) {
                        $file_list[] = "{$current_dir}/{$file}";
                        } elseif (is_dir($current_file)) {
                            $stack[] = $current_file;
                        }
                    }
                }
            }
        }
        return $file_list;
    }

    /** Retire d'une chaine tous les caractères à ne pas utiliser dans un fichier
     *  IN : chaine
     * 	OUT : chaine, les caractères interdits (*,!,/,\,+,", etc.) ayant été remplacés par un underscore _ .
     *  NOTE : utilisé dans les créations de fichiers + répertoires : visio, livraison, upload
     */
    if(!function_exists('removeTrickyChars')) {
	    function removeTrickyChars($str) {
	
	        //return str_replace(array(' ','\'','"','$','+','%','?',';',':','!','#','|','>','<','/','*','\\','&',"'",'~','(',')','=',',','`',"\xC2\xA0","«","»"),'_',$str);
	        return str_replace(array(' ','\'','"','$','+','%','?',';',':','!','#','|','>','<','*','\\','&',"'",'~','(',')','=',',','`',"\xC2\xA0","«","»","\xB0"),'_',$str);
	    }
    }

        
        
    /** Récupère le type MIME pour les fichiers multimédia en fonction de l'extension de fichier
     * Utilisé pour le visionnage, notamment pour avoir le même type MIME entre l'appel (visualisation.inc)
     * et le "stream" des données (getVisioUrl)
     * ld 13/02/09 => ajout des types images pour création Vignette
     */
    function getMimeType($file) {
        // VP 15/12/2011 : ajout extension mpa
        switch (getExtension($file)) {

            case 'mov' :
            case 'qt'  :
            $mime='video/quicktime'; break;
            case 'mp4' :
            $mime='video/mp4'; break;
            case 'mpg' :
            case 'mp2' :
            case 'mpeg':
            case 'mpe' :
            case 'mpga':
            $mime='video/mpeg'; break;
            case 'avi' :
            $mime='video/x-msvideo'; break;
            case 'm4v' :
            $mime='x-m4v';break;
            case 'm3u' :
            $mime='x-mpegurl';break;
            case 'rm'  :
            $mime='application/vnd.rn-realmedia'; break;
            case 'dv'  :
            case 'dvi' :
            $mime='application/x-dv'; break;

            //case 'mp2' :
            case 'mp3' :
            case 'mpa' :
            $mime='audio/mpeg';break;
            case 'wav' :
            $mime='audio/x-wave'; break;
            case 'aif' :
            case 'aiff':
            case 'aifc':
            $mime='audio/x-aiff'; break;
            case 'au'  :
            case 'snd' :
            $mime='audio/basic'; break;
            case 'ogg' :
            $mime='application/ogg'; break;
            case 'ra'  :
            case 'ram' :
            $mime='audio/x-pn-audio';break;
            case 'smi' :
            case 'smil':
            $mime='application/smil'; break;

            case 'gif' :
            $mime='image/gif'; break;
            case 'jpg' :
            case 'jpeg':
            $mime='image/jpeg';break;
            case 'png' :
            $mime='image/png';break;
            case 'tiff':
            $mime='image/tiff';break;
			case 'tif':
            $mime='image/tiff';break;
            case 'pdf':
            $mime='image/pdf';break;


            default : $mime='video/quicktime'; break;

        }
        return $mime;
    }

// Recherche récursive de fichiers (Nexen)
  function searchFile($pathRoot , &$listFile , $search='') {
    // $pathRoot, répertoire à partir duquel la recherche commence
    // $listFile, tableau contenant les fichiers trouvés
    // $search, critère de recherche
     $directory = dir($pathRoot);
    static $index_file = 0;
    while ($file = $directory->read()) {
      if (($file != '.') && ($file != '..') && !(strstr(strtolower($file),'.php'))){
        // fichiers '.php' ignorés
        $sub_path = $pathRoot . "/" . $file;
        if ( is_dir($sub_path)){
          searchFile($sub_path,$listFile,$search);
        } else {
          //if(similar_text(strtoupper($search),strtoupper($file)) == strlen($search)){
          if($search=='' || strpos(strtoupper($file),strtoupper($search)) == (strlen($file)-strlen($search))){
            $listFile[$index_file] = array("fileName"=>$file,"pathFile"=>$sub_path);
            $index_file++;
          }
        }
      }
    }
  }

  function stripControlCharacters($str) {
// VG 14/02/12 : on ne remplace plus ces caractères : "\xBB","\xBF","\xEF" : ne font apparemment pas partie des BOM nous concernant
// VG 15/09/11 : ajout des caractères type BOM utf8 et utf16 : "\xBB","\xBF","\xEF","\xFE","\xFF"
// VP 29/10/09 : modification fonction stripControlCharacters pour gérer correctement UTF8
//		$str = str_replace('œ', '&oelig;', $str); //cas spécial du e dans o
//  	$str=utf8_decode($str);
//		$str=strtr($str,"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x0B\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F",
//		"                              ");
//	$str=utf8_encode($str);
	//$str=eregi_replace('\x00|\x01|\x02|\x03|\x04|\x05|\x06|\x07|\x08|\x0B|\x0D|\x0E|\x0F|\x10|\x11|\x12|\x13|\x14|\x15|\x16|\x17|\x18|\x19|\x1A|\x1B|\x1C|\x1D|\x1E|\x1F',' ',$str);
	$str=str_replace(array("\x00","\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08","\x0B","\x0C","\x0D","\x0E","\x0F","\x10","\x11","\x12","\x13","\x14","\x15","\x16","\x17","\x18","\x19","\x1A","\x1B","\x1C","\x1D","\x1E","\x1F","\xFE","\xFF")," ",$str);

  	return $str;
  }

// VP 22/07/09 : création fonction
// VP 29/07/09 : renvoi true
// VP 29/07/09 : renvoi true
function mv_rename($in,$out){
	// VP 14/10/10 : pas de move si in=out
	if($in==$out) return true;
    // VP 2/03/12 : utilisation de rename par défaut
    // VP 7/03/12 : ajout retour rename
    $ok=rename($in,$out);
    // Commande system si rename ne marche pas
    if(!file_exists($out)){
        if(!strpos(PHP_OS,'WINNT')) { //OSX / UNIX
            // VP 2/03/12 : suppression sudo
            //$cmd="sudo /bin/mv -f \"".$in."\" \"".$out."\"";
            $cmd="/bin/mv -f \"".$in."\" \"".$out."\"";
            trace($cmd);
            exec($cmd);
            return true;
        } else {
            // VP 13/07/11 : changement rename en move pour Windows
            //$cmd="move /Y ".str_replace(array("//","/"),"\\","\"".$in."\" \"".$out."\"");
            $cmd="move /Y \"".str_replace("/","\\",dirname($in)."/".basename($in)."\" \"".dirname($out)."/".basename($out)."\"");
            trace($cmd);
            exec($cmd);
            return true;
            //trace("rename($in,$out)");
            //return rename($in,$out);
        }
    }
    return $ok;
}
/** Echappe les caractères pour utilisation dans un shell
 *  IN : chaine
 * 	OUT : chaine avec caractères échappés
 */

    function escapeQuoteShell($chaine, $ssh=false){
        // VP 21/11/2012 : Escape du $
        if($ssh) return "\"\\\"".str_replace(array('"','$'),array('\\\\\"','\\\\\$'),$chaine)."\\\"\"";
        else return "\"".str_replace(array('\"','"','$'),array('\\\"','\"','\$'),$chaine)."\"";
    }

/** Echappe les caractères pour utilisation dans les options d'un shell
 *  IN : chaine
 * 	OUT : chaine avec caractères échappés
 */
    function escapeShellParamFile($filepath){
    	// VP 21/11/2012 : Escape du $
    	return str_replace(array('[',']'),array('\[','\]'),$filepath);
    }
	
/** Vérifie qu'un process tourne
 *  IN : pid
 * 	OUT : true|false
 */
    function checkProcessPid($pid){
	
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$pid.'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			exec("ps ".$pid, $output);
			if (count($output) > 1) {
				return true;
			}
			return false;
		}
    }

/** Fonction de remplacement de db->Quote pour tout ce qui n'est pas à destination d'une requete SQL
	in : $value,
	out : chaine value traitée suivant le cas :
		Si c'est un nombre, alors on le renvoi tel quel,
			// MS - 07.06.2016 - on ajoute un garde fou pour le cas où le "nombre" qu'on teste est 05.2016 par ex => peut etre interprété comme un float par is_numeric mais pose pb notament dans l'affichage tabCrit
			// /0[0-9]\.[0-9]{4}/ devrait etre un test suffisant pour bloquer le cas précis d'une date mois.année sans trop perturber le reste des inputs de cette fonction ..
		Si c'est une chaine alors on l'encadre de simple quote,
		et si la chaine contient des simple quote, alors on les echape avec un backslash

*/

	function quoteField($value){
		if(is_array($value) && count($value) == 1 && isset($value[0])){
			return "'".str_replace(array("'", "\n"),array("\'", " "),$value[0])."'";
		}else if(is_numeric($value) && !preg_match('/0[0-9]\.[0-9]{4}/',$value)){
			return $value;
		}else if (is_string($value)){
			return "'".str_replace(array("'", "\n"),array("\'", " "),$value)."'";
		}else if (is_null($value) || empty($value)){
			return "''";
		}
	}
/*****************************************/
/*					KEWEGO               */
/*****************************************/
/**
 * Récupère la liste des chaînes
 * IN : tableau d'étapes
 * OUT : mise à jour champ Valeur
 */

function updateKewegoChannels($type_val="CHKW") {
	// Creation job Kewego list
	global $db;
	$rows=$db->GetAll("select ID_ETAPE from t_etape
						INNER JOIN t_module ON ETAPE_ID_MODULE=ID_MODULE
						where MODULE_NOM='Kewego'");
	foreach($rows as $row){
		$arrID[]=$row['ID_ETAPE'];
	}
	require_once(modelDir.'model_etape.php');
	$etapeObj = New Etape();
	$arrEtape=$etapeObj->getEtape($arrID);
	foreach($arrEtape as $etapeObj){
		$id_job_valid=0;
		//Modification etape pour getChannel
		if(preg_match("|<login_url>(.*)</login_url+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $login_url=$matches[1];
		if(preg_match("|<appkey>(.*)</appkey+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $appkey=$matches[1];
		if(preg_match("|<user>(.*)</user+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $user=$matches[1];
		if(preg_match("|<pass>(.*)</pass+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $pass=$matches[1];
		if(isset($user) && isset($pass) && isset($login_url)){
			$etapeObj->t_etape['ETAPE_IN']="/";
			$etapeObj->t_etape['ETAPE_OUT']="channels".str_replace(array("Kewego"," "),array("","_"),$etapeObj->t_etape['ETAPE_NOM']).".xml";
			// VP 5/10/10 : changement FOLDER en FOLDER_OUT
			$etapeObj->t_etape['ETAPE_PARAM']="<param><action>getChannels</action><login_url>".$login_url."</login_url><appkey>".$appkey."</appkey><user>".$user."</user><pass>".$pass."</pass><type_val>".$type_val."</type_val><FOLDER_IN></FOLDER_IN><FOLDER_OUT>".kBackupInfoDir."</FOLDER_OUT></param>";
			$etapeObj->createJob("",0,0,$id_job_valid,$jobObj);
			$jobObj->t_job["JOB_ID_SESSION"]="0";
			$jobObj->save();
			unset($jobObj);
		} else trace("MISSING KEWEGO PARAM user=".$user." pass=".$pass." appkey=".$appkey." login_url=".$login_url);
	}
}

function sendKewegoThumbnail($id_doc,$id_etape,$champ_kewego="DOC_COTE_ANC") {
	require_once(modelDir.'model_doc.php');
	$docObj = New Doc();
	$docObj->t_doc['ID_DOC']=$id_doc;
	$docObj->getDoc();
	$sig=$docObj->t_doc[$champ_kewego];
	if(!empty($sig) && $docObj->vignette){
		$imgName=basename($docObj->vignette);
		$imgPath=kCheminLocalMedia.dirname($docObj->vignette)."/";
		require_once(modelDir.'model_etape.php');
		$etapeObj = New Etape();
		$etapeObj->t_etape['ID_ETAPE']=$id_etape;
		$etapeObj->getEtape();
		$id_job_valid=0;
		//VP 7/04/10 : ajout paramètre mail
		//Modification etape pour uploadThumbnails
		if(preg_match("|<login_url>(.*)</login_url+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $login_url=$matches[1];
		if(preg_match("|<appkey>(.*)</appkey+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $appkey=$matches[1];
		if(preg_match("|<mail>(.*)</mail+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $mail=$matches[1];
		if(preg_match("|<user>(.*)</user+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $user=$matches[1];
		if(preg_match("|<pass>(.*)</pass+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $pass=$matches[1];
		if(isset($user) && isset($pass) && isset($appkey) && isset($login_url) && isset($mail)){
			$etapeObj->t_etape['ETAPE_IN']="";
			$etapeObj->t_etape['ETAPE_OUT']="";
			$etapeObj->t_etape['ETAPE_PARAM']="<param><action>uploadThumbnails</action><sig>".$sig."</sig><login_url>".$login_url."</login_url><appkey>".$appkey."</appkey><mail>".$mail."</mail><user>".$user."</user><pass>".$pass."</pass><FOLDER_IN>".$imgPath."</FOLDER_IN></param>";
			$etapeObj->createJob($imgName,0,0,$id_job_valid,$jobObj);
			unset($jobObj);
			return true;
		} else trace("MISSING KEWEGO PARAM user=".$user." pass=".$pass." appkey=".$appkey." login_url=".$login_url." mail=".$mail);
	} else trace("MISSING KEWEGO PARAM sig=".$sig." vignette=".$docObj->vignette);
	return false;
}

// VP 22/04/10 : création fonction sendKewegoXML
function sendKewegoXML($id_doc,$id_etape,$champ_kewego="DOC_COTE_ANC") {
	require_once(modelDir.'model_doc.php');
	$docObj = New Doc();
	$docObj->t_doc['ID_DOC']=$id_doc;
	$docObj->getDoc();
	$sig=$docObj->t_doc[$champ_kewego];
	if(!empty($sig)&&!empty($id_etape)){
		require_once(modelDir.'model_etape.php');
		$etapeObj = New Etape();
		$etapeObj->t_etape["ID_ETAPE"]=$id_etape;
		$etapeObj->getEtape();
		$id_job_valid=0;
		$param="<param><ID_DOC>".$docObj->t_doc['ID_DOC']."</ID_DOC></param>";
		$etapeObj->createJob($docObj->t_doc['DOC_COTE'],0,0,$id_job_valid,$jobObj,$param);
	} else trace("MISSING KEWEGO PARAM sig=".$sig);
	return false;
}

// VP 22/04/10 : création fonction getKewegoPlayer
function getKewegoPlayer($id_doc,$id_etape,&$blog,$champ_kewego="DOC_COTE_ANC") {
	require_once(modelDir.'model_doc.php');
	$docObj = New Doc();
	$docObj->t_doc['ID_DOC']=$id_doc;
	$docObj->getDoc();
	$sig=$docObj->t_doc[$champ_kewego];
	if(!empty($sig)){
		require_once(modelDir.'model_etape.php');
		$etapeObj = New Etape();
		$etapeObj->t_etape['ID_ETAPE']=$id_etape;
		$etapeObj->getEtape();
		$id_job_valid=0;
		if(preg_match("|<login_url>(.*)</login_url+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $login_url=$matches[1];
		if(preg_match("|<appkey>(.*)</appkey+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $appkey=$matches[1];
		if(preg_match("|<mail>(.*)</mail+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $mail=$matches[1];
		if(preg_match("|<user>(.*)</user+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $user=$matches[1];
		if(preg_match("|<pass>(.*)</pass+>|",$etapeObj->t_etape['ETAPE_PARAM'],$matches)) $pass=$matches[1];
		if(isset($user) && isset($pass) && isset($appkey) && isset($login_url) && isset($mail)){
			$etapeObj->t_etape['ETAPE_IN']="";
			$etapeObj->t_etape['ETAPE_OUT']="";
			$etapeObj->t_etape['ETAPE_PARAM']="<param><action>getPlayerCode</action><sig>".$sig."</sig><login_url>".$login_url."</login_url><appkey>".$appkey."</appkey><mail>".$mail."</mail><user>".$user."</user><pass>".$pass."</pass></param>";
			$ok=$etapeObj->createJob($docObj->t_doc['DOC_COTE'],0,0,$id_job_valid,$jobObj);
			if($ok){
				$ok=$jobObj->makeXML();
				$ok=$jobObj->waitForJob(120);
				if($ok){
					// Lecture xml log
					if($jobObj->readLogXml($logXml)){
						if(preg_match("|<blog>(.*)</blog+>|",$logXml,$matches)) $blog=$matches[1];
					}
				}
			}
			$error_msg=$jobObj->error_msg;
			unset($jobObj);
			if(empty($blog)) {
				trace("ERROR getKewegoPlayer : ".$error_msg);
				return false;
			} else return true;
		} else trace("MISSING KEWEGO PARAM user=".$user." pass=".$pass." appkey=".$appkey." login_url=".$login_url." mail=".$mail);
	} else trace("MISSING KEWEGO PARAM sig=".$sig);
	return false;
}

/**
 * Retourne un tableau contenant la dimension (pour affichage) en cm d'une vidéo, image, etc, en fonction d'une résolution, d'une largeur et d'une hauteur
 * La clé du tableau retourné est à utiliser comme clé des différents contenants de cette taille, pour uniformisation.
 */
function getMediaDim($dpi, $width, $height) {
	$arr = array();
	$wDPI = number_format( round(($width/$dpi)*2.54, 1), 1);
	$hDPI = number_format( round(($height/$dpi)*2.54, 1), 1);
	$dimensions = $wDPI."x".$hDPI;
	$arr['TAILLECM'] = $dimensions;
	$arr['WIDTH_CM'] = $wDPI;
	$arr['HEIGHT_CM'] = $hDPI;

	return $arr;
}

function getMediaResDpi($dpi, $arrDim) {
	$resolutionDPI = 'XXX';

	return $resolutionDPI;
}



function prepareMatForDownload($matPath, $id_mat) {
	$nomDossierCourt="DL".preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime());
	$dir = kCheminLocalVisionnage.$nomDossierCourt;
	$newPath = $dir."/".$id_mat;

	if(mkdir($dir,0775)){
		symlink($matPath, $newPath);
	}

	$new_url = str_replace(kCheminLocalMedia, kCheminHttpMedia, $newPath);
	if (file_exists($newPath)){
 		//return $new_url;
 		return "downloadMat.php?fichier=".$nomDossierCourt."/".$id_mat;
	}
 	else
 		return str_replace(kCheminLocalMedia, kCheminHttpMedia, $matPath);
}


function calcMaxUploadFileSize($str_taille)
{
	$str_taille=trim($str_taille);
	$unite=substr($str_taille,-1);
	$valeur=substr($str_taille,0,-1);

	switch ($unite)
	{
		case 'K':
			return $valeur << 10;
			break;
		case 'M':
			return $valeur << 20;
			break;
		case 'G':
			return $valeur << 30;
			break;
		default:
			return intval($str_taille);
			break;
	}
}

function isTc($tc)
{
	if (preg_match('/^[0-9]{2}:[0-9]{2}:[0-9]{2}:[0-9]{2}$/',$tc)===1)
		return true;
	else
		return false;
}

function encode_utf8_array(&$arr) {
	if (is_array($arr))
		foreach ($arr as &$val)
			encode_utf8_array($val);
	else {
		if (mb_detect_encoding($arr) != "UTF-8" || !mb_detect_encoding($arr))
			$arr = utf8_encode($arr);
		$arr = preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $arr);
	}
}


// MS - 22/09/14 - fonction permettant de corriger le parenthésage dans une chaîne de caractères
function stripUnmatchedParentheses($str){
	// depth => profondeur
	$depth = 0 ;
	// array regroupant les parenthèses fermantes seules
	$arr_idx_solo_closing = array();
	// array permettant de recenser les parenthèses ouvrantes seules
	$arr_idx_total = array();

	// pour chaque char
	for($i=0; $i < strlen($str) ; $i++){
		// si c'est une parenthèse ouvrante, alors on alimente arr_idx_total
		if($str[$i] == '('){
			$depth ++ ;
			$arr_idx_total[] = $i;
		}
		//sinon si parenthèse fermante
		else if ($str[$i] == ')'){
			$depth -- ;
			// si depth < 0 => alors paranthèse fermante seule
			if ($depth < 0){
				// on stocke la référence, puis reset depth pour continuer l'analyse
				$arr_idx_solo_closing[] = $i;
				$depth=0;
			}
			// on dépile arr_idx_total
			array_pop($arr_idx_total);
		}

	}
	// on regroupe l'ensemble des références de parenthèses non matchées, puis on les tri
	$arr_wrong_chars = array_merge($arr_idx_solo_closing,$arr_idx_total);
	sort($arr_wrong_chars);

	// enfin on supprime toutes les occurences de parenthèses isolées
	$out_str = $str;
	foreach($arr_wrong_chars as $idx=>$index_char){
		$out_str = substr($out_str,0,$index_char-$idx).substr($out_str,$index_char-$idx+1);
	}

	return $out_str;

}

// MS - implémentation rapide d'une génération de shortcode a partir d'un entier ou d'un int.
// on peut éventuellement désactiver la concaténation avec un nombre random, mais elle permet d'ajouter du bruit et d'éviter des suites de valeurs facilement trouvable

function generateShortCode($val,$withRand = true ){
	$chars = "123456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
	$length = strlen($chars);
	// Retour ajax
	$code = "";
	// rand 0-10000 concaténé avec la  concatenation des valeurs ascii de chaque caractère de la  valeur en entrée
	$tp = ($withRand?mt_rand(0,10000):'').intval(implode('',array_map('ord',str_split($val))));
	while ($tp > $length - 1) {
		$code = $chars[fmod($tp, $length)] .$code;
		$tp = floor($tp / $length);
	}
	$code = $chars[$tp] . $code;
	return $code ;
}

//XB - supression balise en paramètre par expression rég
function strip_defined_tags($str, $tags) {
  $content = '';
  if (!is_array($tags)) {
    $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
    if(end($tags) == '') array_pop($tags);
  }
  foreach($tags as $idx=>$tag) {
      $content = '(.+</'.$tag.'(>|\s[^>]*>)|)';
      $str = preg_replace('#</?'.$tag.'(>|\s[^>]*>)'.'#is', '', $str);
  }
  return $str;
}


function getFormatsMatVisu ($reset=false) {
	global $db;
	if ($reset) unset($_SESSION['FORMATS']['VISU']);
	if (!isset($_SESSION['FORMATS']['VISU'])) {
			$sql="SELECT FORMAT_MAT from t_format_mat WHERE FORMAT_ONLINE=1";
			$rs=$db->GetAll($sql);
			foreach ($rs as $r) $_SESSION['FORMATS']['VISU'][]=$r['FORMAT_MAT'];
	}
	return $_SESSION['FORMATS']['VISU'];
}

function parseSimpleSQL($sql){
	$array_parse = array(array("keyword"=>"select"),array("keyword"=>"from"),array('keyword'=>"where"),array('keyword'=>"group by"), array('keyword'=>"order by"),array('keyword'=>"limit"));
	foreach($array_parse  as $key=>$part){
		$array_parse[$key]['index'] = stripos($sql,$part['keyword']);
	}

	if(!function_exists("sort_query_parts")){
		function sort_query_parts($a,$b){
			return ($a['index']<$b['index'] )?-1:1;
		}
	}
	uasort($array_parse,"sort_query_parts");

	// on parcourt l'array via les fonctions each / next / current / reset pour éviter d'altérer l'index dont on a besoin plus tard pr réordonner la requete
	reset($array_parse);
	while ($part = each($array_parse)){
		 if($part['value']['index'] !== false){
			$next = current($array_parse); // ici on appelle "current()" car la fonction "each" a déjà avancé le pointeur
			if( isset($next['index'])){
				$array_parse[$part['key']]['segment'] = substr($sql,$part['value']['index'],intval($next['index']-$part['value']['index']));
			}else{
				$array_parse[$part['key']]['segment'] = substr($sql,$part['value']['index']);
			}
		}
	}
	// on retrie via les clés => on récupère l'ordre défini lors de la création d'array_parse, et donc l'ordre naturel des éléments de la requete SQL
	ksort($array_parse);

	return $array_parse;
}

//Coupe string par rapport à length sans couper un mot
//retour position__string
 function cleanCut($string,$length,$cutString = '...') {
    $string=trim($string);
    if(strlen($string) <= $length)
    {
        return $string;
    }
    $str = substr($string,0,$length-strlen($cutString)+1);
    return strrpos($str,' ').'__'.substr($str,0,strrpos($str,' ')).$cutString;
    }

	
function removeScriptTags($str,$bypass_file_test = false){
	try{
		if($str === null ){
			return null;
		}
		if($str === '' ){
			return '';
		}
		// set_error_handler permettant de rediriger les warning "parser error" de la création "SimpleXMLElement vers une exception
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			throw new Exception($errstr, $errno);
		});
		// ajout tag englobant xml pour simuler un XML
		$str2 = "<?xml version=\"1.0\" encoding=\"utf-8\"?><xml_wrapper_esc>".$str."</xml_wrapper_esc>";
		// initialisation du xml_parser
		$xml_parser = new SimpleXMLElement($str2);
		$continue= 0;
		// récupération des tags scripts
		// -Version en deux appel, je le laisse la au cas ou on ait besoin d'optimiser le temps de traitement : 
		// $nodes = $xml_parser->xpath('//script');
		// $nodes2 = $xml_parser->xpath("//*[@*[starts-with(translate(.,'JAVSCRIPT','javscript'),'javascript')]]");
		// $nodes = array_merge($nodes,$nodes2);
		// -Version en un seul appel : 
		$nodes = $xml_parser->xpath("//*[name()='script' or @*[starts-with(translate(.,'JAVSCRIPT','javscript'),'javascript')]]");
		$arr_rm = array();
		foreach($nodes as $node){
			$arr_rm[] =$node;
		}
		// suppression des tags scripts
		foreach($arr_rm as $idx => $rm){
			unset($arr_rm[$idx][0]);
		}
		// retour sous la  forme xml string
		$ret = trim($xml_parser->asXML());
		// suppression tag xml wrapper
		$ret_alt = substr($ret,strpos($ret,'<xml_wrapper_esc>')+strlen('<xml_wrapper_esc>'));
		$ret_fin = substr($ret_alt,0,strpos($ret_alt,'</xml_wrapper_esc>'));
		// retour au fonctionnement default du error handler
		restore_error_handler() ; 
		// retour de la chaine traitée
		return $ret_fin;
	}catch(Exception $e){
		// Si une exception a été lancée, message d'erreur et passage par strip_tags plutot 
		trace("removeScriptTags failed - ".$e);
		// retour au fonctionnement default du error handler
		restore_error_handler() ; 
		trace("removeScriptTags => fallback htmlspecialchars : ".htmlspecialchars ($str,ENT_NOQUOTES | ENT_HTML401));
		// Pour l'instant return la chaine en input avec les caractères spéciaux échappés  => on devrait à terme envoyer une exception, catch par adodb => exception adodb => fail transaction au niveau des actions class_controller de l'entité concernée
		return htmlspecialchars ($str,ENT_NOQUOTES | ENT_HTML401 ) ;
		// throw new Exception('removeScriptTags failed - '.$e);
	}
}


function getFontFilename($constant_font_map,$fontfamily = null ,$fontweight=null,$italic=null){
	if(is_string($constant_font_map) && defined($constant_font_map)){
		$font_map = unserialize(constant($constant_font_map));
	}else if(is_array($constant_font_map) && !empty($constant_font_map)){
		$font_map = $constant_font_map;
	}else{
		return false ; 
	}
	
	if(empty($fontfamily)){
		return false ; 
	}
	
	if(!empty($fontweight) && $fontweight && !empty($italic) && $italic){
		$fontstyle_key = 'bold-italic';
	}else if (!empty($fontweight) && $fontweight ){
		$fontstyle_key = 'bold';
	}else if (!empty($italic) && $italic){
		$fontstyle_key = 'italic';
	}else{
		$fontstyle_key = 'normal';
	}
	
	
	$fontfile_name = false ; 
	if(array_key_exists($fontfamily,$font_map) 
	&& !empty($font_map[$fontfamily]) 
	&& !empty($font_map[$fontfamily][$fontstyle_key])){
		
		$fontfile_name = $font_map[$fontfamily][$fontstyle_key];
	
	}else if(array_key_exists($fontfamily,$font_map) 
	&& !empty($font_map[$fontfamily]) 
	&& !empty($font_map[$fontfamily]['normal'])){
		
		$fontfile_name = $font_map[$fontfamily]['normal'];
	
	}else{
		
		foreach($font_map as $font){
			if(!empty($font['normal'])){
				$fontfile_name = $font['normal'];
				break ; 
			}
		}
	}
	return $fontfile_name ; 
}

// VP 10/4/18 : définitiond les path imagick le cas échéant
function setImagickPath(){
	if(!defined("kConvertPath") && defined("kImagickPath")){
		define("kConvertPath", kImagickPath."convert");
	}
	if(!defined("kIdentifyPath") && defined("kImagickPath")){
		define("kIdentifyPath", kImagickPath."identify");
	}
}
	
?>
