<?php

// VP 11/02/2013 : ajout variable Societe (getFromSession, putInSession, Login)
class User {

private static $instance;


var $UserID, $Nom, $Prenom, $Type, $Mail, $Statut, $Fonction, $Societe;
var $Groupes = array();
var $message;



	function User () { // Constructeur
			global $ADODB_SESS_CONN;
			if (isset($ADODB_SESS_CONN->customExpireMsg))
				$this->message=$ADODB_SESS_CONN->customExpireMsg;
			$this->getFromSession();
			if (!$this->loggedAsGuest() && $this->UserID=="") $this->Init();	
	}
	
	function Init() {
			global $db;
		    $result=$db->Execute("SELECT PARAMETRE, VALEUR FROM t_config where PARAMETRE='ID_GROUPE'");
	    	$list = $result->FetchRow();
	    	$id_groupe = $list["VALEUR"];
	    	$result->Close();

			$this->Type=0;
			$this->UserID="";
			$this->Nom="";$this->Prenom="";$this->Mail="";$this->Fonction="";;$this->Societe="";
			$this->Statut=0;
	    	$this->creeDroitsSession("groupe",$id_groupe);
			$this->putInSession();
			$this->initSessLayoutOpts() ; 
	}
	
	
	function initSessLayoutOpts(){
		// Fonction d'initialisation des options d'affichage (layout) pour un site standard en v3, 
		// ci dessous les valeurs par défaut des options, 
		// + possibilité de définir des valeurs overwrite pour ces options dans le fichier initialisationGeneral.inc.php , 
		if(!isset($_SESSION['USER']['layout_opts'])){
			// initialisation session
			// valeurs par défaut, 
			$default_opts = array(
			'prev_pop_in'=>'1',
			'layout' => '2',
			'panListeRefToLastOrder'=>0,
			'docListe'=>array(
					'affMode'=>'mos',
					'pagination' => '0'
			),
			'panier'=>array(
				'affMode'=>'mos'
			));
			
			if(defined('defaultLayoutOptions') && defaultLayoutOptions!=''){
				$custom_opts = unserialize(defaultLayoutOptions);
				$final_opts = merge_array_recurs($default_opts,$custom_opts);
			}else{
				$final_opts = $default_opts ; 
			}
			$_SESSION['USER']['layout_opts'] = $final_opts;			
			// trace("init layout opts final : ".print_r($_SESSION['USER']['layout_opts'],true));
		}
	}
	
	
	function getFromSession() {
		global $USERID; // NOTE : variable réservée pour la conservation du USERID dans les sessions en base
		if (isset($_SESSION["USER"])) {
			$this->UserID=$_SESSION["USER"]["ID_USAGER"];
			$this->Nom=$_SESSION["USER"]["US_NOM"];
			$this->Prenom=$_SESSION["USER"]["US_PRENOM"];
			$this->Type=$_SESSION["USER"]["US_ID_TYPE_USAGER"];
			$this->Statut=$_SESSION["USER"]["US_ID_ETAT_USAGER"];
			$this->Mail=$_SESSION["USER"]["US_SOC_MAIL"];
			$this->Groupes=$_SESSION["USER"]["US_GROUPES"];
			$this->Fonction=$_SESSION["USER"]["US_ID_FONCTION_USAGER"];
			$this->Societe=$_SESSION["USER"]["US_SOCIETE"];
			if(isset($_SESSION['USER']['GUEST_PARTAGE'])){
				$this->guestPartage = $_SESSION['USER']['GUEST_PARTAGE'] ; 
			}
			$USERID=$this->UserID;	
		} 
	}
	
	function putInSession() {
		$_SESSION["USER"]["ID_USAGER"]=$this->UserID;
		$_SESSION["USER"]["US_NOM"]=$this->Nom;
		$_SESSION["USER"]["US_PRENOM"]=$this->Prenom;
		$_SESSION["USER"]["US_ID_TYPE_USAGER"]=$this->Type;
		$_SESSION["USER"]["US_ID_ETAT_USAGER"]=$this->Statut;
		$_SESSION["USER"]["US_SOC_MAIL"]=$this->Mail;
		$_SESSION["USER"]["US_GROUPES"]=$this->Groupes;
		$_SESSION["USER"]["US_ID_FONCTION_USAGER"]=$this->Fonction;			
		$_SESSION["USER"]["US_SOCIETE"]=$this->Societe;			
		if(isset($this->guestPartage)){
			$_SESSION['USER']['GUEST_PARTAGE'] = $this->guestPartage;
		}
	}
	
	

	
	
/**
 * Singleton : une seule classe User en même temps !
 * @return User
 */

   public static function getInstance() 
   {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }

       return self::$instance;
   }

	// VP 20/08/2010 : création fonction LoginFromMail pour se connecter à partir d'un mail (ex: connexion SSO)
	function LoginFromMail($mail,$refer='',$storecookie=false){
    	global $db;
		
        $sql="select US_LOGIN, US_PASSWORD from t_usager where US_SOC_MAIL".$db->like($mail);
        $rs = $db->GetAll($sql);
		if(count($rs)>0) return $this->Login($rs[0]['US_LOGIN'],$rs[0]['US_PASSWORD'],$refer,$storecookie,true);
		else return false;
   		
	}
	
	function LoginSSO($login,$refer='',$storecookie=false){
		global $db;
		
		$sql="select US_LOGIN, US_PASSWORD from t_usager where US_LOGIN=".$db->Quote($login);
		$rs = $db->GetAll($sql);
		if(count($rs)>0) return $this->Login($rs[0]['US_LOGIN'],$rs[0]['US_PASSWORD'],$refer,$storecookie,true);
		else return false;
		
	}
	
	function Login($login,$password,$refer='',$storecookie=false,$already_hashed = false){
    	global $db;

    	$login = trim( $login );
    	$password = trim( $password );
    	    	
		require_once(libDir.'fonctionsGeneral.php');
    	
		$this->Init();
        $sql="select ID_USAGER, US_PASSWORD,US_SALT, US_NOM,US_PRENOM,US_ID_TYPE_USAGER,US_SOC_MAIL,US_ID_ETAT_USAGER,US_ID_FONCTION_USAGER,US_SOCIETE,US_DATE_ACTIF_DEB,US_DATE_ACTIF_FIN
				 from t_usager where US_LOGIN=".$db->Quote($login);
        $rs = $db->GetAll($sql);
		if ($rs && 
			(($rs[0]['US_PASSWORD'] == getPasswordHash($password,$rs[0]['US_SALT'])) 
			|| ($already_hashed && $rs[0]['US_PASSWORD']== $password))
		){
			$sesame=true;
		}else{
			$sesame=false;
		}
		
        // I. User existe + pwd ok ?
        if($login!='' && $password!='' && $rs && $sesame){
            $tverificationLogin=$rs[0];
            // I.1. L'utilisateur est-il valid� ? On l'ajoute dans la session
			$isTemp = $this->isTemp($tverificationLogin);
			$isActif = $this->isActif($tverificationLogin);
			
			if($isActif) { //Compte actif
				//SESSION
				foreach($tverificationLogin as $key=>$value){
					if($key!='US_PASSWORD') $_SESSION["USER"][$key]=$value; // Mise en session des paramètres SAUF PWD
				}
				$this->getFromSession();
				// I.2 Cr�ation de la variable de session "groupes" qui liste les groupes auxquels appartient l'utilisateur ainsi que les privil�ges sur les fonds et collections auxquels il a acces.
				$this->creeDroitsSession("usager",$this->UserID);
				$this->putInSession();
				
				logAction("IN",""); // Log action
				$this->getPaniersUser(); //Récupération des paniers/sélections associés à l'utilisateur
	
				if (!empty($storecookie)) { //création des cookie de conservation du login / pass, ld 26/11/08
					$this->storeCookie($login,$password);
				}
				if (!empty($refer)){
					Page::getInstance()->redirect($refer);// Réactivé par loic pour auto inscription user
				}
				return true;
				
			} elseif($isTemp) { //Compte temporaire périmé
				$this->message=kCompteTemporaireErreur;
				
			} else { // Inscription non validée
				// $this->message=kMsgBienvenue;
				$this->message=kMsgConnexionRefuseeEtat;
				if (!empty($refer))
					Page::getInstance()->redirect($refer);
				return false;
			}
			
        } elseif (!$rs) { //User non trouvé
        	$this->message=kMsgUtilisateurInconnu;
        	$this->failedLogin=1; //echec cause login 
        	return false;
			
        } else { // User existe ms mv pass
			// MS - 20.01.15 - Aucune raison d'avoir du html dans message (necessitera correction coté site éventuellement ...)
        	// $this->message= "<div class=\"error\">".kMsgMauvaisLogin."<br/><br/><a class='pass_oublie' href=\"index.php?urlaction=loginDemande\">".kMotDePasseOublie."</a></div>";
        	$this->message= kMsgMauvaisLogin;
        	$this->failedLogin=2; //echec cause pwd
        	return false; 
        }
    }

/** FUNCTION CREEDROITSSESSION : Cr�e la variable de session "US_GROUPES" qui stocke les fonds/collections et leurs privil�ges respectifs que l'utilisateur/groupe ($type) d'id $quoi peut consulter
*	$type : Récupère t-on les droits par rapport � un groupe ou un usager
*	 $quoi : id du groupe/usager
 */
	// VP 23/11/09 : ajout GROUPE dans informations rappatriées
	function creeDroitsSession($type,$quoi){
	    global $db;
	    
	    $sql="";
	    $sqlGroupe="";
	    $this->Groupes=array();
		
		switch ($type)
	    {
	        case "groupe":
	            $sql="SELECT t_fonds.ID_FONDS, t_fonds.ID_FONDS_GEN, t_groupe.GROUPE, t_groupe_fonds.ID_PRIV FROM t_groupe_fonds, t_fonds, t_groupe ";
	            $sql.="WHERE (t_groupe_fonds.ID_FONDS=t_fonds.ID_FONDS or t_groupe_fonds.ID_FONDS=t_fonds.ID_FONDS_GEN) ";
	            $sql.="AND t_groupe.ID_GROUPE=t_groupe_fonds.ID_GROUPE AND t_groupe_fonds.ID_GROUPE = ".intval($quoi)." ORDER BY ID_FONDS_GEN, ID_PRIV";
	            break;
	            
	        case "usager":
	          /*  $sql="SELECT t_fonds.ID_FONDS, t_fonds.ID_FONDS_GEN, t_groupe.GROUPE,max(t_groupe_fonds.ID_PRIV) as ID_PRIV FROM t_usager_groupe, t_groupe_fonds, t_fonds, t_groupe ";
	            $sql.="WHERE (t_groupe_fonds.ID_FONDS=t_fonds.ID_FONDS or t_groupe_fonds.ID_FONDS=t_fonds.ID_FONDS_GEN) AND t_usager_groupe.ID_GROUPE = t_groupe_fonds.ID_GROUPE ";
	            $sql.="AND t_groupe.ID_GROUPE=t_usager_groupe.ID_GROUPE AND t_usager_groupe.ID_USAGER = ".$db->Quote($quoi)." GROUP BY ID_FONDS_GEN, ID_FONDS";
	            $sqlGroupe="SELECT DISTINCT t_usager_groupe.ID_GROUPE FROM t_usager_groupe WHERE t_usager_groupe.ID_USAGER = ".$db->Quote($quoi)." ORDER BY ID_GROUPE";*/
				
				// MS On récupère uniquement le premier rang des liens t_groupe_fonds, l'héritage des privilèges vers les fonds enfants est fait ensuite
				$sql="SELECT t_fonds.ID_FONDS, t_fonds.ID_FONDS_GEN, t_groupe.GROUPE,max(t_groupe_fonds.ID_PRIV) as ID_PRIV FROM t_usager_groupe, t_groupe_fonds, t_fonds, t_groupe ";
	            $sql.="WHERE (t_groupe_fonds.ID_FONDS=t_fonds.ID_FONDS) AND t_usager_groupe.ID_GROUPE = t_groupe_fonds.ID_GROUPE ";
	            $sql.="AND t_groupe.ID_GROUPE=t_usager_groupe.ID_GROUPE AND t_usager_groupe.ID_USAGER = ".intval($quoi)." GROUP BY ID_FONDS_GEN, t_fonds.ID_FONDS, t_groupe.groupe";
				$sqlGroupe="SELECT DISTINCT t_usager_groupe.ID_GROUPE,t_groupe.GROUPE FROM t_usager_groupe left join t_groupe on t_usager_groupe.id_groupe=t_groupe.id_groupe WHERE t_usager_groupe.ID_USAGER = ".$db->Quote($quoi)." ORDER BY ID_GROUPE";

				
				break;
	    }
	    $result=$db->Execute($sql);
	    if ($result) {

	    while ($list = $result->FetchRow()){
		
	        if (isset($this->Groupes[$list["ID_FONDS"]])){
	            // S'il s'agit d'une collection ($list["ID_FONDS_GEN"]!=0) le privil�ge d'une collection a toujours raison sur le privil�ge h�rit� d'un fonds ($list["ID_PRIV"] != $_SESSION["US_GROUPES"][$list["ID_FONDS_GEN"]]["ID_PRIV"])
	            if (isset($this->Groupes[$list["ID_FONDS_GEN"]]["ID_PRIV"]) && $list["ID_PRIV"] != $this->Groupes[$list["ID_FONDS_GEN"]]["ID_PRIV"] && $list["ID_FONDS_GEN"]!=0){
	               $this->Groupes[$list["ID_FONDS"]]=array("ID_FONDS"=>$list["ID_FONDS"],"ID_PRIV"=>$list["ID_PRIV"],"GROUPE"=>$list["GROUPE"]);  
	            }else if (isset($this->Groupes[$list["ID_FONDS"]]) && $list["ID_PRIV"] >= $this->Groupes[$list["ID_FONDS"]]["ID_PRIV"]){
					$this->Groupes[$list["ID_FONDS"]]=array("ID_FONDS"=>$list["ID_FONDS"],"ID_PRIV"=>$list["ID_PRIV"],"GROUPE"=>$list["GROUPE"]); 
				}
	        } else{
	            $this->Groupes[$list["ID_FONDS"]]=array("ID_FONDS"=>$list["ID_FONDS"],"ID_PRIV"=>$list["ID_PRIV"],"GROUPE"=>$list["GROUPE"]); 
	        }
	    }
		
		// MS get héritage : 
		$tmp_Groupes = $this->Groupes;
		
		foreach ($this->Groupes as $idx => $gr){
			$tmp_id_fond = $gr['ID_FONDS'];
			$tmp_ids = $db->GetCol('SELECT t_fonds.ID_FONDS from t_fonds where ID_FONDS_GEN ='.intval($tmp_id_fond).' GROUP BY ID_FONDS');
			
			while(!empty($tmp_ids)){
				foreach($tmp_ids as $id){
					// on update les privilèges d'un fonds si il n'est pas déja présent dans l'objet Groupes,
					// ou si le nouveau privilège est plus important que l'existant
					if((isset($tmp_Groupes[$id]) && $tmp_Groupes[$id]["ID_PRIV"] < $gr["ID_PRIV"]) || !isset($tmp_Groupes[$id])){
						$tmp_Groupes[$id] = array("ID_FONDS"=>$id,"ID_PRIV"=>$gr['ID_PRIV'],"GROUPE"=>$gr['GROUPE']);
					}
				}
				$tmp_id_fond = implode(',',$tmp_ids);
				$tmp_ids = $db->GetCol('SELECT t_fonds.ID_FONDS from t_fonds where ID_FONDS_GEN in ('.$tmp_id_fond.') GROUP BY ID_FONDS');
			}
		}
		
		$this->Groupes =$tmp_Groupes;
		ksort($this->Groupes);
	    
	    // On stocke les ids des groupes auxquels appartient l'utilisateur
	    if ($sqlGroupe!=""){
	        $result=$db->Execute($sqlGroupe);
	        while ($list = $result->FetchRow()){
	            $this->Groupes["GROUPES"][]=array("ID_GROUPE"=>$list["ID_GROUPE"],"GROUPE"=>$list['GROUPE']);
	        }
	    }else{
	       $this->Groupes["GROUPES"][]=$quoi;
	    }

		$result->Close();
	    }
	}


/** FUNCTION VERIFIEDROITS : V�rifie que l'utilisateur a des droits >= $id_priv sur le fond $doc_id_fonds
* $doc_id_fonds : l'id du fonds que l'on veut tester
* $id_priv : Le privil�ge que l'on veut tester
* */
	function verifieDroits_old($doc_id_fonds,$id_priv){
	
	    $id_priv_us_doc=0;

	    foreach($this->Groupes as $value){
	        if($doc_id_fonds==$value["ID_FONDS"] && $value["ID_PRIV"]>=$id_priv){
	            $id_priv_us_doc=$value["ID_PRIV"];
	        }
	    }
	    return $id_priv_us_doc;
	}	
	
	function verifieDroits($doc_id_fonds,$id_priv){
		if(isset($this->Groupes[$doc_id_fonds]) && $this->Groupes[$doc_id_fonds]['ID_PRIV']>=$id_priv){
			return $this->Groupes[$doc_id_fonds]['ID_PRIV'];
		}else{
			return false;
		}
	}
	function verifieGroupe($id_groupe=0,$nom_groupe=""){
		if(empty($id_groupe) && empty($nom_groupe) ){
			return false;
		}
		if(isset($this->Groupes['GROUPES']) ){
			foreach($this->Groupes['GROUPES'] as $grp){
				// trace("verifieGroupe cmp : ".$id_groupe." =? ".intval($grp['ID_GROUPE'])." | ".$nom_groupe." =? ".$grp['GROUPE']);
				if((!empty($id_groupe) && intval($grp['ID_GROUPE']) == $id_groupe ) || (!empty($nom_groupe) && $grp['GROUPE'] == $nom_groupe)){
					return true ; 
				}
			}
			return $this->Groupes[$doc_id_fonds]['ID_PRIV'];
		}else{
			return false;
		}
	}
	
	
	function loggedIn()
	{
	  return($this->Type != kNotLogged);
	}
	
	function loggedAsGuest(){
		return (!$this->loggedIn() && isset($this->guestPartage));
	}
	
	// $num de 0 � 3
	// 0 : not logged
	// 1 : user
	// 2 : doc
	// 3 : admin
	function typeLog($num) { return($this->getTypeLog() == $num);}
	
	function getTypeLog() { return $this->Type;}
	
	function getProfileName()
	{
		global $db;
		
		$res=$db->Execute('SELECT TYPE_USAGER FROM t_type_usager WHERE ID_TYPE_USAGER='.intval($this->Type))->getRows();
		$profil=$res[0]['TYPE_USAGER'];
		
		return $profil;
	}
	
// Deconnexion
   function Logout($keepcookie=false,$page_cible='index.php') {
        //mysql_query("UPDATE t_session SET ID_USAGER = '0' WHERE SID = '".session_id()."' AND EXPIRE > " . time());
		logAction("OUT",Array());    
        $this->Init();
        $this->putInSession();
        
        if (!$keepcookie && $_COOKIE["i"]) { //si les cookies existents, suppression, ld 26/11/08
        $xx = setcookie('i','',time()-3600);
        $xx = setcookie('p','',time()-3600);
        }
		
		session_unset();
        session_destroy();
        
        //A mettre en paramètre
        Page::getInstance()->redirect($page_cible);
    }


//----------------------------------------------
//Protection des pages en fonction des profils : voir le fichier conf.inc.php pour la d�finition des interdictions
// OBSOLETE utiliser getAccess dans class_page
//----------------------------------------------
function checkRights() {
	global $loggedPages,$adminPages,$docPages,$usagerPages,$usagerAdvPages;
	if((!loggedIn() && strstr($loggedPages,basename($_SERVER['SCRIPT_NAME']))) 
	        || (getTypeLog() == kLoggedAdmin && strstr($adminPages,basename($_SERVER['SCRIPT_NAME'])) != '')
	        || (getTypeLog() == kLoggedDoc && strstr($docPages,basename($_SERVER['SCRIPT_NAME'])) != '')
	        || (getTypeLog() == kLoggedUsagerAdv && strstr($usagerAdvPages,basename($_SERVER['SCRIPT_NAME'])) != '')	
	        || (getTypeLog() == kLoggedNorm && strstr($usagerPages,basename($_SERVER['SCRIPT_NAME'])) != '')	){
	        
	    if(strstr($loggedPages,basename($_SERVER['SCRIPT_NAME']))) {
	        //print "ACCES RESERVE ".basename($_SERVER['SCRIPT_NAME']);
	        //exit;
	        // A mettre en paramètre
	        Page::getInstance()->redirect("index.php");
	    }
	    else {
	        print "ACCES INTERDIT ".basename($_SERVER['SCRIPT_NAME']);
	        exit;
	    }
	}

}

	
	
	function compteurPanier() {
	    global $db;
	    if($this->loggedIn()){

	    	
	    	
	    	
	        //$sql="SELECT COUNT(distinct t_panier_doc.ID_DOC) as CNT_CART FROM t_panier_doc,t_panier WHERE t_panier.ID_PANIER=t_panier_doc.ID_PANIER and t_panier.PAN_ID_USAGER=".$db->Quote($this->UserID)." and t_panier.PAN_ID_ETAT='1'";
	        $sql="SELECT COUNT(*) as CNT_CART FROM t_panier_doc,t_panier WHERE t_panier.ID_PANIER=t_panier_doc.ID_PANIER and t_panier.PAN_ID_USAGER=".intval($this->UserID)." and t_panier.PAN_ID_ETAT=1";
	        $rsCount=$db->Execute($sql);
	        $nbPanier=$rsCount->FetchRow();
	        return $nbPanier['CNT_CART'];
	    }
	}
	
	
	function isActif($infosUser)  {
		global $db ;
		$code = $db->GetOne("SELECT ETAT_USAGER_CODE from t_etat_usager where id_etat_usager = ".$db->Quote($infosUser['US_ID_ETAT_USAGER']));			
		switch($code) {
			case 'TEMP' : //Temporaire
				//Vérification de la conformité des données
				if(empty($infosUser["US_DATE_ACTIF_DEB"]) || empty($infosUser["US_DATE_ACTIF_FIN"]) || $infosUser["US_DATE_ACTIF_DEB"] == '0000-00-00 00:00:00' || $infosUser["US_DATE_ACTIF_FIN"]=='0000-00-00 00:00:00') {
					$isActif = false; //Valeur de retour de la fonction
					break;
				}
				
				//Calcul et vérification de la période
				list($y,$m,$d) = explode('-', $infosUser["US_DATE_ACTIF_DEB"]);
				$deb_contrat = mktime(0,0,0,$m,$d,$y);
				list($y,$m,$d) = explode('-', $infosUser["US_DATE_ACTIF_FIN"]);
				$fin_contrat = mktime(0,0,0,$m,$d,$y);
				if($deb_contrat<=time() && time()<$fin_contrat) {
					$isActif=true; //Valeur de retour de la fonction
				} else  {
					$isActif = false; //Valeur de retour de la fonction
				}
			break;
			case 'ACTIF' : //Actif
				$isActif = true; //Valeur de retour de la fonction
			break;
			default:
			case 'MAIL_VALID':	// Mail validé, la validation de l'adresse mail l'a amené dans cet état intermédiaire, 
								//=> en général veut dire qu'on attends une validation de l'administrateur
			case 'INACTIF' : //Inactif
				$isActif = false; //Valeur de retour de la fonction
			
		}
		
		return $isActif;
	}	
	
	function isTemp($infosUser)  {
		require_once(modelDir.'model_usager.php');
		if($infosUser["ID_ETAT_USAGER"] == Usager::getIdEtatUsrFromCode('TEMP')) {
			return true;
		} else {
			return false;
		}
	}
	
	function getPaniersUser() {
		//On vérifie si l'utilisateur a déjà un panier...
		require_once(modelDir.'model_panier.php');
		$carts=Panier::getFolders();
		//by LD 17/09/08 : on filtre la liste pour vérifier qu'un panier (id_etat=1) existe, indépendemment des dossiers & sélections

		//PC 28/02/13 : prise en compte du paramêtre gEtatPanierDefaut
		$etatPanierDefaut = 1;
		if (defined("gEtatPanierDefaut")) $etatPanierDefaut = gEtatPanierDefaut;
		foreach ($carts as $i=>$_cart) {if ($_cart['PAN_ID_ETAT']!=$etatPanierDefaut || $_cart['PAN_DOSSIER']!=0) unset ($carts[$i]);}
		
		if (empty($carts)) {
			//Pas de panier (=1er login) => on en crée un...
			$myCart=new Panier();
			$myCart->t_panier['PAN_ID_USAGER']=$this->UserID;
			$myCart->t_panier['PAN_ID_ETAT']=$etatPanierDefaut;
			$myCart->t_panier['PAN_ID_TYPE_COMMANDE']=0;
			if ($etatPanierDefaut == 0) $myCart->t_panier['PAN_TITRE']=kMaSelection;
			$myCart->createPanier();
		} else {
			$myCart=new Panier();
			$myCart->t_panier['ID_PANIER']=$carts[0]['ID_PANIER'];
		}
		//By LD 30/01/09 => si panier session, transfo en vrai panier (on transfère les docs)
		
		if (isset($_SESSION['panier_session']) && !empty($_SESSION['panier_session'][0]['t_panier_doc'])) {
			//il y a des documents dans le panier de session
			
			//$myCart->getPanier();
			$myCart->getPanierDoc(); // on récupère la liste.
			
			$nb=$myCart->mergePanierDoc($_SESSION['panier_session'][0]['t_panier_doc']);
			//debug($myCart->t_panier_doc,'red');
			if ($nb!=false) $myCart->saveAllPanierDoc();
			unset($_SESSION['panier_session']); //on supprime le panier de session
		}
		unset($_SESSION['FOLDERS']['_session']); //et on le supprime de la liste des folders
		
	   unset($myCart); // pas la peine de le garder...	
	}
	
	function storeCookie($login,$password) {
		//VP 15/12/08 : Bizarement 'i' et 'p' ne fonctionnent pas comme nom de cookies
		$xx = setcookie('ops_i',password_encode('ops0mai',$login),mktime(0,0,0,12,31,2037));
		$xx = setcookie('ops_p',password_encode('ops0mai',$password),mktime(0,0,0,12,31,2037));	
	}
}

?>
