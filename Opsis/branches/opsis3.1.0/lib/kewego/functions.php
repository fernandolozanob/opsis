<?php
/*
	* This program is part of "upload with curl" code sample. This is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	* 
	* Created by Kewego - 2009  - See more details on <http://developers.kewego.com/>
	* 
	* Version 1.0.0
*/	
function get_app_token($appKey) {
	$response = APICall()->Call('http://api.kewego.com/app/getToken/', array('appKey' => $appKey));
	$xml_message = simplexml_load_string($response);
	$appToken = $xml_message->message->appToken;
	debug('--> appToken is: '.$appToken);
	debug ('<br />');
	return (string)$appToken;
}

function check_app_token($appToken) {
	$response = APICall()->Call('http://api.kewego.com/app/checkToken/', array('appToken' => $appToken));
	$xml_message = simplexml_load_string($response);
	$validity = $xml_message->message->appToken;

	if ($validity == true) {
		debug('--> appToken is still valid');
		debug ('<br />');
		return true;
	}
	return false;
}

function get_auth_token($username, $password, $appToken) {
	$response = APICall()->Call('http://login.kewego.com/api/getAuthToken/', array('username' => $username, 'password' => $password, 'appToken' => $appToken));
	$xml_message = simplexml_load_string($response);
	$authToken = $xml_message->message->token;
	//print_r($response);
	debug('authToken is: '.$authToken);
	debug ('<br />');
	return (string)$authToken;
}

function check_auth_token($authToken, $appToken) {
	$response = APICall()->Call('http://api.kewego.com/auth/checkToken/', array('token' => $authToken, 'appToken' => $appToken));
	$xml_message = simplexml_load_string($response);
	$validity = $xml_message->message->check_token;
	if ( "true" == $validity) {
		debug('--> authToken is still valid');
		debug ('<br />');
		return true;
	}
	return false;
}

function debug($text) {

}

?>