<?php
/*
	* This program is part of "upload with curl" code sample. This is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	* 
	* Created by Kewego - 2009  - See more details on <http://developers.kewego.com/>
	* 
	* Version 1.0.0
*/	
/**
 * DO NOT EDIT BELOW
 */
require_once 'KCurl.class.php';
require_once 'functions.php';
require_once 'config.php';
session_start();


/**
 * MANAGING APP TOKEN
 */
if (isset($_SESSION['appToken'])) {
	debug('Checking previous appToken validity');
	debug ('<br />');
	if (check_app_token($_SESSION['appToken']) === false) {
		if ($appToken = get_app_token(APPLICATION_KEY)) {
			$_SESSION['appToken'] = $appToken;
			debug('--> appToken saved into session');
			debug ('<br />');
		}
		else {
			echo 'ERROR: Cannot get appToken';
			exit;
		}
	}
}
else {
	if ($appToken = get_app_token(APPLICATION_KEY)) {
			$_SESSION['appToken'] = $appToken;
		debug('--> appToken saved into session');
		debug ('<br />');
	}
	else {
		echo 'ERROR: Cannot get appToken';
		exit;
	}

}

/**
 * MANAGING AUTH TOKEN
 */
if (isset($_SESSION['authToken'])) {
	debug('Checking previous authToken validity');
	debug ('<br />');
	if (check_auth_token($_SESSION['authToken'], $_SESSION['appToken']) === false) {
		$_SESSION['authToken'] = get_auth_token(USERNAME, PASSWORD, $_SESSION['appToken']);
		debug('--> authToken saved into session');
		debug ('<br />');
	}
}
else {
	if ($authToken = get_auth_token(USERNAME, PASSWORD, $_SESSION['appToken'])) {
			$_SESSION['authToken'] = $authToken;
		debug('--> authToken saved into session');
		debug ('<br />');
	}
	else {
		echo 'ERROR: Cannot get authToken';
		//exit;
	}
}

?>