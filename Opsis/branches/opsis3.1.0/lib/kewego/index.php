<?php
/*
	* This program is part of "upload with curl" code sample. This is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	* 
	* Created by Kewego - 2009  - See more details on <http://developers.kewego.com/>
	* 
	* Version 1.0.0
*/	
include_once 'init.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kewego Curl upload code sample</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body bgcolor="#ccc">
<h3>Formulaire exemple d'upload avec curl : joindre un fichier video et les donn&eacute;es sous forme de fichier XML
  <br>
</h3>
<form action="upload_curl.php" method="post" enctype="multipart/form-data" name="form1">
<p>Video File : <input type="file" name="video_file" /></p>
<p>Xml Data File : <input type="file" name="xml_file" /></p>
<input type="submit" name="Submit" value="Envoyer">
</form>

</body>
</html>
