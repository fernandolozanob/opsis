<?php
/*
	* This program is part of "upload with curl" code sample. This is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,é
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	* 
	* Created by Kewego - 2009  - See more details on <http://developers.kewego.com/>
	* 
	* Version 1.0.0
*/	
set_time_limit(0);

class KCurl
{
	function Call($api_url, $data_array = '', $post = true,$sendFile = false)
	{
		if (substr($api_url, strlen($api_url)-1, 1) != '/') {
			$api_url = $api_url.'/';
		}

		if (extension_loaded('curl'))
		{
			debug('Calling API method: '.$api_url);
 			debug('<br />');
			$parsed_url = parse_url($api_url);
			$ch         = curl_init($parsed_url['scheme'].'://'.$parsed_url['host'].$parsed_url['path']);

			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // VP 5/04/2013 : ajout proxy http
			if (isset($this->proxy)) curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
			if ($post === true ) curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
			curl_setopt($ch, CURLOPT_TIMEOUT, 7200);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);

			$post_data = array();
			if (isset($parsed_url['query']))
			{
				$query_arg = explode('&',$parsed_url['query']);
				foreach($query_arg as $key => $arg)
				{
					$detail_arg = explode('=', $arg);
					$post_data[$detail_arg[0]] = $detail_arg[1];
				}
			}

			if ($data_array != '')
			{
				foreach($data_array as $data_key => $data)
				{
					$post_data[$data_key] = $data;
				}
			}

			if(!$sendFile){
				curl_setopt($ch, CURLOPT_POSTFIELDS, substr(KCurl::data_encode($post_data), 0, -1));
			}else{
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
				curl_setopt($ch, CURLOPT_URL, $api_url);
			}
			
			$xml	= curl_exec($ch);

			$error = curl_error($ch);
			curl_close($ch);

			if (!empty($error)) {
				debug($error);
			}
			return $xml;
		}
		else
		{
		    debug('You need the CURL extension');
			debug('<br />');
		}
	}


	function getMessage()
	{
  		if ($response = $this->getResponse())
		{
			$xml_message = simplexml_load_string($response);
			$message = $xml_message->message;
			return (string)$message;
		}
		else
		{
		    return false;
		}
	}


	function createXMLData($datas)
	{
		$xml = new XMLWriter();
		$xml->openMemory();
		$xml->setIndentString("\t");
		$xml->setIndent(true);
		$xml->startDocument('1.0', 'utf-8');

		$xml->startElement('kewego_call');
		$xml->startElement('params');
		foreach ($datas as $key => $value)
		{
			$xml->startElement($key);
			if (is_array($value))
			{
				foreach($value as $subkey => $subvalue)
			    {
					if (is_numeric($subkey))
					{
						foreach($subvalue as $balise => $balise_value)
						{
							$xml->startElement($balise);
					        $xml->writeRaw($balise_value);
					        $xml->endElement();
						}
					}
					else
					{
						$xml->startElement($subkey);
				        $xml->writeRaw($subvalue);
				        $xml->endElement();
					}
				}
			}
			else
			{
		        $xml->writeRaw($value);
			}
	        $xml->endElement();
		}
		$xml->endElement();
		$xml->endElement();
		return $xml->outputMemory(false);
	}

	function data_encode($data, $keyprefix = "", $keypostfix = "") {
		assert( is_array($data) );
		$vars=null;
		foreach($data as $key=>$value) {
			if(is_array($value)) {
				$vars .= data_encode($value, $keyprefix.$key.$keypostfix.urlencode("["), urlencode("]"));
			}
			else {
				$vars .= $keyprefix.$key.$keypostfix."=".urlencode($value)."&";
			}
		}
		return $vars;
	}
}

function APICall() {
	static $app;

	if (!is_object($app)) {
		$app = new KCurl;
	}
	return $app;
}



?>