<?php
/*
	* This program is part of "upload with curl" code sample. This is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	* 
	* Created by Kewego - 2009  - See more details on <http://developers.kewego.com/>
	* 
	* Version 1.0.0
*/	
session_start();
require_once 'config.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Upload curl sample</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>


<?php 
// print details of the files sent by form

echo "<p>Informations about files sent by Post Method: </p>";
echo "<pre>";
print_r($_FILES);
echo "</pre>";

// print php configuration settings
//  phpinfo();

// Check Video and XML Files according to php server configuration
$video_file_error = $_FILES['video_file']['error'];
$xml_file_error = $_FILES['xml_file']['error'];

check_file_error($xml_file_error, 'xml');
check_file_error($video_file_error, 'video');

function check_file_error($file_error, $file_type)
{
	if ($file_error) 
	{
          switch ($file_error)
		  {
                   case 1: // UPLOAD_ERR_INI_SIZE
                   die("The $file_type file exceeds the limit authorized by the server (file php.ini)!");
                   case 2: // UPLOAD_ERR_FORM_SIZE
                   die("The $file_type file exceeds authorized limit in HTML form!");
                   case 3: // UPLOAD_ERR_PARTIAL
                   die("Transfert of $file_type file has been interrupted!");
                   case 4: // UPLOAD_ERR_NO_FILE
                   die("The $file_type file that you sent has a null size!");
          }
	}
	else 
	{
	// No file error, checking xml file extension (video file will be checked by APIs)
	 	if ( $file_type == 'xml' && $_FILES['xml_file']['type']!= 'text/xml')
	 	{
		die("Error : Data file is not a valid XML file");
	 	}
 	}
}

// Get video file and save in temporary directory
$nom_fichier = $_FILES['video_file']['name'];
$destination_path = DESTINATION_PATH;
move_uploaded_file($_FILES['video_file']['tmp_name'], $destination_path.$nom_fichier);

// Get xml file and put into string
$xml_file = $_FILES['xml_file']['tmp_name'];
$xml_data = file_get_contents($xml_file); 

$apptoken = $_SESSION['appToken'];
$authtoken = $_SESSION['authToken'];

/*
echo "$apptoken";
echo "<br/>";
echo "$authtoken";
echo "<br/>";
print_r($xml_data);
*/

// Get video file from temporary directory
$video_file = $destination_path.$nom_fichier;

        $url = 'http://upload.kewego.com/api/uploadVideo/index.php';
        $ch  = curl_init($url);

        if (!file_exists($video_file))
        {
            die('No file found, please check PHP params in phpinfo()');
        }
        else
		{
		
		// setting curl options and datas for upload
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data       = array(
                        'file'         => '@'.$video_file,
                        'xml_data'     => $xml_data,
                        'token'       => $authtoken,
                        'appToken'    => $apptoken
                        );

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        
        // $reponse containts sig if everything has been OK
        if(( $response = curl_exec($ch)) === false)
		{
   		 echo 'Erreur Curl : '.curl_error($ch);
		}
		else
		{
		echo 'Video has been correctly sent to API. API returned : <br/><br/>'.$response;
		}
     
        curl_close($ch);

		}	
// delete temporary file from server
unlink($video_file);		
?>
</body>
</html>
