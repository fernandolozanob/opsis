<?php
require_once(libDir."class_cherche.php");

class RechercheDocMat extends Recherche {

// VP 30/12/08 : changement recherche_DMAT en recherche_DOCMAT pour compatibilité modifLot
	function __construct() {
	  	$this->entity='DMAT';
	  	$this->entity2='DOC_MAT';
     	$this->name=kDocument." / ".kMateriel;
     	$this->prefix='DM';
     	$this->sessVar='recherche_DOCMAT';
		$this->tab_recherche=array();
		$this->useSession=true;
	}


    function prepareSQL(){
		global $db;
		$this->sql = "select DM.*, M.* from t_doc_mat DM inner join t_mat M on DM.ID_MAT = M.ID_MAT WHERE 1=1 ";
    }


	/**
	* En fonction des types de recherche, appelle les fonctions de traitement dédiée.
	* Cette fonction peut se trouver surchargée si une classe introduit des recherches spécifiques
	*/
    protected function ChooseType($tabCrit,$prefix) {
		// I.2.A.1 Tests des champs et appel des fonctions de recherche ad�quates :
		switch($tabCrit['TYPE'])
		{
			case "FT": //FullText
				$this->chercheTexteAmongFields($tabCrit,$prefix);
				break;
			case "FTS": //FullText avec découpage de chaque mot
				$this->FToperator='+';
				$this->chercheTexteAmongFields($tabCrit,$prefix);
				break;
			case "C":   // Recherche sur un champ précis
				$this->chercheChamp($tabCrit,$prefix);
				break;
			case "CE":   // Recherche sur un champ précis sans analyse de valeur (ex : doc_cote)
				$this->chercheChamp($tabCrit,$prefix,true);
				break;
			case "CI":    // Recherche sur un champ d'une table annexe par ID
				$this->chercheIdChamp($tabCrit,$prefix);
				break;
			case "CT": // Recherche sur un champ par texte exact dans une table liée (fonds, genre,...)
				$this->chercheChampTable($tabCrit,$prefix);
				break;
			case "D":   // Recherche sur date(s)
				$this->chercheDate($tabCrit,$prefix);
				break;
			case "H":   // Recherche sur durées(s)
				$this->chercheDuree($tabCrit,$prefix);
				break;
			case "V":  // Recherche sur valeur
				$this->chercheVal($tabCrit,$prefix);
				break;
			case "VI":    // Recherche sur valeur par ID (récursif)
				$this->chercheIdVal($tabCrit,$prefix,true);
				break;
			case "CD" : //comparaison de date
				$this->compareDate($tabCrit,$prefix);
				break;
			 //@update VG 07/06/2010 : ajout de la comparaison de dates
			case "CDLT" : //comparaison de date
				$tabCrit['VALEUR2']='<=';
				$this->compareDate($tabCrit,$prefix);
				break;
			case "CDGT" : //comparaison de date
				$tabCrit['VALEUR2']='>=';
				$this->compareDate($tabCrit,$prefix);
				break;
			case "CH" : //comparaison de date
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CHLT" : //comparaison de date
				$tabCrit['VALEUR2']='<=';
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CHGT" : //comparaison de date
				$tabCrit['VALEUR2']='>=';
				$this->compareDuree($tabCrit,$prefix);
				break;
			case "CV" : //comparaison de valeur =
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVGT" : //comparaison de valeur >
				$tabCrit['VALEUR2']='>';
				$this->compareValeur($tabCrit,$prefix);
				break;
			case "CVLT" : //comparaison de valeur <
				$tabCrit['VALEUR2']='<';
				$this->compareValeur($tabCrit,$prefix);
				break;

			case "LI" : // Recherche sur ID LEX unique (récursif)
				$this->chercheIdLex($tabCrit,$prefix,true);
				break;
			case "L" : // Recherche sur termes lexique (av. extension lex PRE)
				$this->chercheLex($tabCrit,$prefix,true,false);
				break;
			case "LP" : // Recherche sur termes lexique (av. extension lex PRE et POST)
				$this->chercheLex($tabCrit,$prefix,true,true);
				break;

			case "ME": //Recherche matériel sur valeur exacte, avec le minimum de traitement, sans découpage sur virgule
				if(strpos("\"",$tabCrit['VALEUR'])===false) $tabCrit['VALEUR']="\"".$tabCrit['VALEUR']."\"";
			case "M" :
				$this->chercheMat($tabCrit,$prefix);
            	break;

			case "DOC" :
				debug($tabCrit, "red", true);
				$this->chercheDoc($tabCrit,$prefix);
            	break;

			case "P" :
				$this->cherchePers($tabCrit,$prefix);
				break;
			case "PI" :
				$this->chercheIdPers($tabCrit,$prefix);
				break;
			case "U" :
				$this->chercheUsager($tabCrit,$prefix);
				break;

default : break;
		}
    }

	/** Recherche sur les matériels liés (doc_mat)
	* 	IN : tabCrit, prefix
	* 	OUT : sql
	* 	FIELD => champ de t_mat
	* 	VALEUR => valeur recherchée
	* 	NOTE : pas de highlight
	*/
	protected function chercheMat($tabCrit,$prefix) {
		global $db;

		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche=$tabCrit['OP']." M.".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true);
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kMateriel;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
	}


	/** Recherche sur les documents liés (doc_mat)
	* 	IN : tabCrit, prefix
	* 	OUT : sql
	* 	FIELD => champ de t_doc
	* 	VALEUR => valeur recherchée
	* 	NOTE : pas de highlight
	*/
	//@update VG : changement dans la chaine sql : placement des conditions avant les jointures, afin d'augmenter les perfs
	protected function chercheDoc($tabCrit,$prefix) {
		global $db;

		$prefix=(!is_null($prefix)?$prefix.".":"");
		$this->sqlRecherche=$tabCrit['OP']." ".$prefix."ID_DOC_MAT IN
(SELECT distinct ID_DOC_MAT from t_doc_mat,t_doc WHERE t_doc.".$tabCrit['FIELD']." ".$this->sql_op($tabCrit['VALEUR'],true)." and t_doc_mat.ID_DOC=t_doc.ID_DOC and t_doc.ID_LANG='".$_SESSION['langue']."')";
		if (empty($tabCrit['LIB'])) $tabCrit['LIB']=kDocument;
		$this->etape.=$this->tab_libelles[$tabCrit['OP']]." ".$tabCrit['LIB']." : ".implode(",",$tabCrit['VALEUR'])." ";
		debug($this->sqlRecherche, "red", true);
	}



}
?>