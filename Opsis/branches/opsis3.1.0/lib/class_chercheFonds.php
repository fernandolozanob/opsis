
<?php
require_once(libDir."class_cherche.php");


class RechercheFonds extends Recherche {

	var $treeParams;
	var $entity;

	function __construct() {
	  	$this->entity='FON';
     	$this->name=kFonds;
     	$this->prefix='f';
     	$this->sessVar='recherche_FON';
		$this->tab_recherche=array();
		$this->sqlSuffixe=' GROUP BY f.ID_FONDS, f.id_lang, f2.FONDS ';
		$this->useSession=true;
	}


    function prepareSQL(){
		global $db;
		
		//PC 28/03/11 : Ajout du filtre par langue
		$prefix=(!is_null($this->prefix)?$this->prefix.".":"");
		$this->sql = "
		  	select f.*, f2.FONDS as FONDS_GEN,
			count(distinct t_groupe_fonds.ID_GROUPE) as NB_GROUPE
			FROM t_fonds f
			LEFT JOIN t_groupe_fonds ON (f.ID_FONDS=t_groupe_fonds.ID_FONDS)
			LEFT JOIN t_fonds f2 ON (f.ID_FONDS_GEN=f2.ID_FONDS AND f.ID_LANG=f2.ID_LANG)
			WHERE ".$prefix."ID_LANG='".$_SESSION["langue"]."' ";
        $this->etape="";
    }
	
	function appliqueDroits($priv_mini = 0){
    	//by LD 04/06/08 => utilisation d'un tableau et non d'une chaine pour �viter les id_fonds in ('',.....)
		$prefix=(!is_null($this->prefix)?$this->prefix.".":"");
        $usr=User::getInstance();
        //$liste_id="''";
        foreach($usr->Groupes as $value){
           // if ($value["ID_PRIV"]!=0){ $liste_id.=", '".$value["ID_FONDS"]."'"; }
           if ($value["ID_PRIV"]>$priv_mini && !empty($value["ID_FONDS"])) $liste_id[]=$value["ID_FONDS"];
        }

        //if(strlen($liste_id)>0) {
        if (count($liste_id)>0) {
            $this->sqlRecherche.= " AND ".$prefix."ID_FONDS ";
            $this->sqlRecherche.= " in ('".implode("','",$liste_id)."')";
        }
	}
	
	
	
	// MS 31/01/13 - Ajout des fonctions necessaires � la repr�sentation sous forme d'arbre des fonds
	// Si beaucoup de fonds, il est plus pratique d'avoir une repr�sentation hierarchique avec recherche (via la palette)
	
	function getChildren($id_fond,&$arrNodes) {
    	global $db;
		
		// VP 23/10/09 : ajout crit�re onlyDocs
		// VP 8/11/12 : utilisation de sql_op pour ID_TYPE_LEX 
		$sql="SELECT f1.* , count( f2.ID_FONDS ) AS nb_children FROM t_fonds f1
        LEFT JOIN t_fonds f2 ON ( f2.ID_FONDS_GEN = f1.ID_FONDS AND f2.ID_LANG = '".$_SESSION['langue']."' )
        WHERE f1.ID_LANG = '".$_SESSION['langue']."'
        AND f1.ID_FONDS_GEN = ".intval($id_fond);
		
		// VP 28/06/2011 : prise en compte param�tre tri
		if(empty($this->treeParams['tri'])) $sql.=" GROUP BY f1.ID_FONDS, f1.id_lang, f1.fonds, f1.id_fonds_gen, f1.fonds_col, f1.fonds_code ORDER BY f1.ID_FONDS_GEN, f1.FONDS";
		else $sql.=" GROUP BY f1.ID_FONDS, f1.id_lang, f1.fonds, f1.id_fonds_gen, f1.fonds_col, f1.fonds_code ORDER BY f1.ID_FONDS_GEN, f1.".$this->treeParams['tri'];
    	
    	$rows=$db->GetAll($sql);
    	foreach($rows as $row) {
			$arrNodes['elt_'.$row['ID_FONDS']]['id']=$row['ID_FONDS'];
			$arrNodes['elt_'.$row['ID_FONDS']]['id_pere']=$row['ID_FONDS_GEN'];
			$arrNodes['elt_'.$row['ID_FONDS']]['terme']=str_replace (" ' ", "'",trim($row['FONDS']));

			$arrNodes['elt_'.$row['ID_FONDS']]['valide']=true;
			$arrNodes['elt_'.$row['ID_FONDS']]['context']=false;
			$arrNodes['elt_'.$row['ID_FONDS']]['nb_children']=$row['NB_CHILDREN'];
			$arrNodes['elt_'.$row['ID_FONDS']]['nb_asso']=$row['NB_ASSO'];
     	}
   
    	return $arrNodes;
    }
	
	
   // VP 7/06/2012 : ajout param�tre $withChildren pour r�cup�rer les enfants
	function getFather($id_fonds,$recursif=false,&$arrNodes,$withChildren=false) {
		global $db;
		
		$sql=" SELECT ID_FONDS,FONDS,ID_FONDS_GEN from t_fonds where ID_LANG='".$_SESSION['langue']."' and ID_FONDS
		in (select ID_FONDS_GEN from t_fonds where ID_FONDS=".intval($id_fonds).") ";
		//$rows=$db->CacheGetAll(120,$sql);
		$rows=$db->GetAll($sql);
		foreach($rows as $row) {
			
			$arrNodes['elt_'.$row['ID_FONDS']]['id']=$row['ID_FONDS'];
			$arrNodes['elt_'.$row['ID_FONDS']]['id_pere']=$row['ID_FONDS_GEN'];
			$arrNodes['elt_'.$row['ID_FONDS']]['terme']=str_replace (" ' ", "'",trim($row['FONDS']));
			
			$arrNodes['elt_'.$row['ID_FONDS']]['valide']=true;
			$arrNodes['elt_'.$row['ID_FONDS']]['context']=false;
			$arrNodes['elt_'.$row['ID_FONDS']]['open']=true;
			
			if ($recursif && $row['ID_FONDS_GEN']!=0) $this->getFather($row['ID_FONDS'],$recursif,$arrNodes,$withChildren);
			
			if (!isset($arrNodes['elt_'.$row['ID_FONDS']]['nb_children']))  $arrNodes['elt_'.$row['ID_FONDS']]['nb_children']=1;
			
			if($withChildren) $this->getChildren($row['ID_FONDS'],$arrNodes);
		}
		return $arrNodes;
	}
	
	function getRootName() {
		return $this->name;
    }
	
	
	// VP 7/06/2012 : ajout param�tre $withBrother pour r�cup�rer les fr�res
    function getNode($id_fonds,&$arrNodes,$style='highlight',$withBrother=false) {
    	global $db;
    	$this->getFather($id_fonds,true,$arrNodes,$withBrother);
    	$sql="select * from t_fonds where ID_FONDS=".intval($id_fonds);
     	//$rows=$db->CacheGetAll(120,$sql);
		$rows=$db->GetAll($sql);
     	foreach($rows as $row) {
            $arrNodes['elt_'.$row['ID_FONDS']]['id']=$row['ID_FONDS'];
            $arrNodes['elt_'.$row['ID_FONDS']]['id_pere']=$row['ID_FONDS_GEN'];
            $arrNodes['elt_'.$row['ID_FONDS']]['terme']=str_replace (" ' ", "'",trim($row['FONDS']));
            
            $arrNodes['elt_'.$row['ID_FONDS']]['valide']=true;
            $arrNodes['elt_'.$row['ID_FONDS']]['context']=false;
            $arrNodes['elt_'.$row['ID_FONDS']]['style']=$style;
     	}

     	$this->getChildren($id_fonds,$arrNodes);

    	return $arrNodes;   
    }



}
?>
