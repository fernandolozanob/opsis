<?php

require_once(libDir.'getid3/getid3/getid3.php');

// MS 24/09/13 modification variables d'environnement (charset)
// pour l'appel mediainfo via shell_exec dans le cas des fichiers 
// dont le nom comporte des accents encodés en ASCII
putenv('LC_CTYPE=fr_FR.UTF-8');


class MatInfo
{
	public static function getMatInfo($file)
	{
		if (!file_exists($file))
			return null;
		
		$type_mime=MatInfo::getTypeMimeFromFile($file);
		// on évite de lancer le mediainfo sur des repertoires car renvoi une version concaténée des mediainfos des fichiers contenus. (et prends du temps ...)
		if($type_mime!="directory"){
			$data_mediainfo=MatInfo::getMediaInfo($file);
			$type = MatInfo::getTypeFromMediainfo($data_mediainfo);
		}else{
			$in_files = scandir($file);
			$hls_pl=false;
			$cpt_dcp=0;
			$cpt_dpx_files=0;
			$cpt_dpx_files_sub=0;
			$cpt_dcp_unwrap=0; 
			
			
			// recherche du manifest m3u8
			foreach($in_files as $in_file){
				if(strpos($in_file,'.')!==0){
					if(strpos($in_file,'.m3u8')!==false){
						$hls_pl = $in_file ; 
						break;
					}else if($in_file == 'ASSETMAP' || $in_file == 'VOLINDEX'){
						$cpt_dcp++;
					}else if($in_file == 'project.dcpproj'){
						$cpt_dcp_unwrap++;
					}else if(in_array(strtolower(getExtension($in_file)),array('dpx','jpg','j2c'))){
						$cpt_dpx_files++;
					}else if(is_dir($file.'/'.$in_file)){
						$dir_dpx=$in_file;
						$dpx_files=scandir($file.'/'.$dir_dpx);
						foreach ($dpx_files as $file_){
							if ($file_!='.' && $file_!='..' && in_array(strtolower(getExtension($file_)),array('dpx','jpg','j2c'))){
								$cpt_dpx_files_sub++;
							}
						}
					}
				}
			}
		
			if ($cpt_dcp >= 2) $type_dossier='DCP';
			if ($cpt_dcp_unwrap == 1) $type_dossier='DCP_unwrap';
			if ($cpt_dpx_files_sub >= 100) $type_dossier='DPX_main';
			if ($cpt_dpx_files >= 100) $type_dossier='DPX_dir';
			if($hls_pl){
				// si manifest m3u8 trouvé (hls) 
				$type_dossier='HLS';
				$hls_dir = $file;
				$file = $file."/".$hls_pl;
				$data_mediainfo=MatInfo::getMediaInfo($file);
				$type = MatInfo::getTypeFromMediainfo($data_mediainfo);
			}
			if($type_dossier == "DCP" || $type_dossier == "DCP_unwrap" ){
				$type_dossier='DCP';
				$dcp_sample_file = null ; 
				foreach($in_files as $in_file){
					if(strtolower(getExtension($in_file)) == 'mxf'){
						$dcp_sample_file = $in_file ; 
						$data_mediainfo=MatInfo::getMediaInfo($file."/".$dcp_sample_file);
						foreach($data_mediainfo['track'] as $track){
							if(trim(strtolower($track['type'])) == 'video'){
								break ; 
							}
						}
					}
				}
				$type = MatInfo::getTypeFromMediainfo($data_mediainfo);
			}
			if($type_dossier == 'DPX_main' || $type_dossier == 'DPX_dir'){
				$dpx_sample_file = null;
				$wav_audio_file = null;
				if($type_dossier == 'DPX_main'){
					foreach($in_files as $in_file){
						if(substr($in_file,0,1) != '.' && $in_file != '..'){
							if(is_dir($file.'/'.$in_file)){
								foreach ($dpx_files as $file_){
									if (empty($dpx_sample_file) && $file_!='.' && $file_!='..' && in_array(strtolower(getExtension($file_)),array('dpx','jpg','j2c'))){
										$dpx_sample_file = $file.'/'.$in_file.'/'.$file_;
									}
								}
							}else if (strtolower(getExtension($in_file)) == 'wav'){
								$wav_audio_file = $file.'/'.$in_file;
							}
						}
					}
				}else if ($type_dossier == 'DPX_dir'){
					foreach($in_files as $in_file){
						if (empty($dpx_sample_file) && $in_file!='.' && $in_file!='..' && in_array(strtolower(getExtension($in_file)),array('dpx','jpg','j2c'))){
							$dpx_sample_file = $file.'/'.$in_file;
							break ; 
						}
					}
				}
				
				$data_mediainfo=MatInfo::getMediaInfo($dpx_sample_file);
				$type = MatInfo::getTypeFromMediainfo($data_mediainfo);
				
				if(!empty($wav_audio_file) && is_file($wav_audio_file)){
					$data_audio = MatInfo::getMediaInfo($wav_audio_file);
					foreach($data_audio['tracks'] as $track){
						if(strtolower($track['type']) == 'audio' && isset($track['duration'])){
							$data_mediainfo['duration'] = $track['duration'];
							foreach($data_mediainfo['tracks'] as $v_idx => $v_track){
								if(strtolower($v_track['type']) == 'video' && empty($v_track['duration'])){
									$data_mediainfo['tracks'][$v_idx]['duration'] = $track['duration']; 
									if(floatval($track['duration']) !== 0 ){
										$data_mediainfo['tracks'][$v_idx]['frame_rate'] = ($cpt_dpx_files_sub / floatval($track['duration'])); 
									}
									break ; 
								}
							}
						}
					}
				}else{
					if($type_dossier == 'DPX_main'){
						$data_mediainfo['duration'] = $cpt_dpx_files_sub / 24 ; 
					}else if ($type_dossier == 'DPX_dir'){
						$data_mediainfo['duration'] = $cpt_dpx_files / 24 ; 
					}
					$data_mediainfo['frame_rate'] = 24 ; 
					// recopie de ces infos dans la track video 
					foreach($data_mediainfo['tracks'] as $idx=>$v_track){
						if(strtolower($v_track['type']) == 'video'){
							$data_mediainfo['tracks'][$idx]['duration'] = $data_mediainfo['duration'];
							$data_mediainfo['tracks'][$idx]['frame_rate'] = $data_mediainfo['frame_rate'];
							break ; 
						}
					}
					
				}

			}
		}
		
		// si type non detecté par mediainfo, alors on utilise le début du type mime comme type pour déterminer la suite des analyses.		
		if(!isset($type) || empty($type)){
			$tmp_mime=explode('/',$type_mime);
			$type=$tmp_mime[0];
			$sous_type=$tmp_mime[1];
			// trace("tmp_mime : ".print_r($tmp_mime,true)." ".$type." ".$sous_type);
		}else if ($type == "audio" && strpos($type_mime,"video")==0){
			// MS tentative de correction du type mime dans les cas ou on detecte le type mime par défaut comme vidéo, mais on analyse le fichier comme étant un audio par mediainfo
			$type_mime = str_replace('video','audio',$type_mime);
		}
		// dans le cas des fichiers eps, on redirige vers l'analyse type image
		//if($type == "application" && ($sous_type=="postscript" || ($sous_type=='octet-stream' && strtolower(getExtension($file)) == 'pct'))){
		if( $type == "application" && ( $sous_type=="postscript" || ( $sous_type=='octet-stream'  && in_array( strtolower(getExtension($file)), array('pct', 'ai', 'eps', 'ps') ) ) ) ) {
			$type = "image";
		}
		
		$arr_data_file=array();
		
		if ($type=='video')
		{
			// $data_mediainfo=MatInfo::getMediaInfo($file);
			$data_ffprobe=MatInfo::getFFprobe($file);

			$idx_piste_video_ffprobe=null;
			$idx_piste_video_mediainfo=null;
			
			foreach($data_mediainfo['tracks'] as $id_piste=>$piste)
			{
				if (strtolower($piste['type'])=='video')
				{
					$idx_piste_video_mediainfo=$id_piste;
					if(!isset($hls_dir)){
						// MS 05.05.15 - Si on est sur un HLS, on ne break pas sur la première piste video 
						// => on veut récupérer l'index de la dernière piste pour avoir des infos cohérentes les tracks remontées par ffprobe 
						break;
					}
				}
			}
			
			foreach($data_ffprobe['tracks'] as $id_piste=>$piste)
			{
				if (strtolower($piste['type'])=='video')
				{
					$idx_piste_video_ffprobe=$id_piste;
					break;
				}
			}
			

			$arr_data_file=array
			(
				'format_calc'=>'',
                'format'=>$data_mediainfo['format'],
				'format_profile'=>$data_mediainfo['format_profile'],
				'bit_rate' => $data_ffprobe['bit_rate'],
				'duration'=>$data_ffprobe['duration'],
				//'timecode'=>$data_mediainfo[0]['format_profile'],
				
				'width'=>$data_mediainfo['tracks'][$idx_piste_video_mediainfo]['width'],
				'height'=>$data_mediainfo['tracks'][$idx_piste_video_mediainfo]['height'],
				
				'timecode'=>$data_ffprobe['timecode'],
				'system_timecode'=>$data_ffprobe['system_timecode'],
				
				'display_aspect_ratio'=>$data_mediainfo['tracks'][$idx_piste_video_mediainfo]['display_aspect_ratio'],
				
				'tracks'=>array()
			);
			// MS dans le cas HLS on préfère avoir la durée de mediainfo (et donc dernière piste vidéo du HLS, voir plus haut) car elle est plus précise
			// on cast en string pour être cohérent avec la durée habituellement fournie par ffprobe
			if(isset($hls_dir)){
				$arr_data_file['duration'] = (string)$data_mediainfo['tracks'][$idx_piste_video_mediainfo]['duration'];
			}
			if(isset($type_dossier) && !empty($type_dossier)){
				$arr_data_file['ops_type_dossier'] = $type_dossier;
			}
			
			if(!empty($data_mediainfo['format_long'])) {
				$arr_data_file['format_long'] = $data_mediainfo['format_long'];
			} elseif(!empty($data_mediainfo['FORMAT_COMMERCIAL_IFANY'])) {
				$arr_data_file['format_long']=$data_mediainfo['FORMAT_COMMERCIAL_IFANY'];
			} else {
				$arr_data_file['format_long']=$data_mediainfo['COMMERCIAL_NAME'];
			}
			// MS 03/03/14 - Dans certains cas l'ordre des tracks récupérées par ffprobe diffère de l'ordre rendu par mediainfo
			// => On doit donc réordonner les tracks mediainfo en fonction de FFProbe (ffprobe fonctionnant comme FFMBC, c'est cet ordre qui est important pr les encodages)
			// le nom des types des tracks peut différer suivant mediainfo & ffprobe, pour l'instant : 
			//  ffprobe | mediainfo
			// 	audio  ==  audio 
			// 	video  ==  video 
			//	data   ==  menu
			//	subtitle== text 	, à compléter éventuellement...
			
			$arr_tmp_tracks = array();
			foreach($data_ffprobe['tracks'] as $idx=>$track){
				// si les tracks ont le meme index => on les reporte
				if(strtolower($track['type']) == strtolower($data_mediainfo['tracks'][$idx][type])
				|| (strtolower($track['type'])== 'subtitle' && strtolower($data_mediainfo['tracks'][$idx][type]) == 'text')
				|| (strtolower($track['type'])== 'data' && strtolower($data_mediainfo['tracks'][$idx][type]) == 'menu')
				){
					$arr_tmp_tracks[$idx] = $data_mediainfo['tracks'][$idx];
					unset($data_mediainfo['tracks'][$idx]);
				}else {
					//Si les tracks n'ont pas le meme index, on va chercher la track correspondante en fonction du type
					foreach($data_mediainfo['tracks'] as $idx_=>$track_){
						if(strtolower($track['type']) == strtolower($data_mediainfo['tracks'][$idx_][type])
						|| (strtolower($track['type'])== 'subtitle' && strtolower($data_mediainfo['tracks'][$idx_][type]) == 'text')
						|| (strtolower($track['type'])== 'data' && strtolower($data_mediainfo['tracks'][$idx_][type]) == 'menu')
						){
							$tmp_track = $data_mediainfo['tracks'][$idx_];
							$idx_tmp = $idx_;
							break;
						}
					}
					$arr_tmp_tracks[$idx] = $tmp_track;
					unset($tmp_track);
					unset($data_mediainfo['tracks'][$idx_tmp]);
					unset($idx_tmp);
				}
			}
			
			
			// on remplace l'array tracks mediainfo avec la nouvelle correctement ordonnée
			// uniquement si arr_tmp_tracks n'est pas vide (cf corpus test Schneider_Electric_Company_Story-_Our_technology_is_everywhere.avi => pas de tracks ffprobe, mais tracks mediainfo)
			if(!empty($arr_tmp_tracks)){
				$data_mediainfo['tracks'] = $arr_tmp_tracks;
			}
			unset($arr_tmp_tracks);
			
			$index_track=0;
			// liste des pistes audio issues de ffprobe ou mediainfo
			foreach ($data_mediainfo['tracks'] as $track_mediainfo)
			//foreach ($data_ffprobe['tracks'] as $track_ffprobe)
			{
				$arr_fields=array('index','type','format','format_long','format_profile','format_settings_gop','format_setings_wrapping','format_version','codec_id','height','width','display_aspect_ratio','bit_rate','frame_rate','bit_depth','language','channels','sample_rate','scan_type','scan_order');
				$arr_data_file['tracks'][$index_track]['index']=$index_track;
                
				$track_ffprobe=$data_ffprobe['tracks'][$index_track];
				//$track_mediainfo=$data_mediainfo['tracks'][$index_track];
				
				if (strtolower($track_mediainfo['type'])=='audio' && (!empty($track_mediainfo['muxing_mode']) && strpos($track_mediainfo['muxing_mode'],'HLS') === false))
				{
					unset($arr_data_file['tracks'][$index_track]);
					$index_track++;
					continue;
				}
				
				foreach($arr_fields as $field)
				{
					if (isset($track_mediainfo[$field]) && !empty($track_mediainfo[$field]))
						$arr_data_file['tracks'][$index_track][$field]=$track_mediainfo[$field];
                    elseif (isset($track_ffprobe[$field]) && !empty($track_ffprobe[$field]))
                        $arr_data_file['tracks'][$index_track][$field]=$track_ffprobe[$field];
				}
				$arr_data_file['tracks'][$index_track]['display_width']=$track_mediainfo['width'];
				
				if (isset($track_mediainfo['display_aspect_ratio']) && !empty($track_mediainfo['display_aspect_ratio']))
				{
					$ratio=explode(':',$track_mediainfo['display_aspect_ratio']);
					$arr_data_file['tracks'][$index_track]['display_height']=intval($track_mediainfo['width']*$ratio[1]/$ratio[0]);
					$arr_data_file['display_width']=$arr_data_file['tracks'][$index_track]['display_width'];
					$arr_data_file['display_height']=$arr_data_file['tracks'][$index_track]['display_height'];
				}
				
				if(empty($arr_data_file['format_long'])) {
					if(!empty($track_mediainfo['format_long'])) {
						$arr_data_file['format_long'] = $track_mediainfo['format_long'];
					} elseif(!empty($track_mediainfo['FORMAT_COMMERCIAL_IFANY'])) {
						$arr_data_file['format_long']=$track_mediainfo['FORMAT_COMMERCIAL_IFANY'];
					} else {
						$arr_data_file['format_long']=$track_mediainfo['COMMERCIAL_NAME'];
					}
				}
				
				if (strtolower($track_mediainfo['type'])=='video')
				{
					$arr_data_file['tracks'][$index_track]['pix_fmt']=$data_ffprobe['tracks'][$idx_piste_video_ffprobe]['pix_fmt'];
				}
				if(!empty($data_ffprobe['duration'])){
					$arr_data_file['tracks'][$index_track]['duration']=floatval(trim($data_ffprobe['duration']));
				}else{
					$arr_data_file['tracks'][$index_track]['duration']=floatval(trim($track_mediainfo['duration']));
				}
				$arr_data_file['tracks'][$index_track]['start_time']=floatval(trim($data_ffprobe['start_time']));
				$arr_data_file['tracks'][$index_track]['timecode']=$track_ffprobe['timecode'];
				if (strtolower($track_mediainfo['type'])=='video' && !empty($arr_data_file['tracks'][$index_track]['timecode']) && empty($arr_data_file['timecode']))
				{
					$arr_data_file['timecode']=$arr_data_file['tracks'][$index_track]['timecode'];
				}
				/*
				$arr_data_file['tracks'][$index_track]['codec_name']=
				*/
				$index_track++;
			}
			
			$arr_data_file['format_calc'] = MatInfo::getFormatCalc($file,'video',$arr_data_file,$arr_format);

			if(!empty($arr_format) && !empty($arr_format['codec_long'])){
				$arr_fmt_tmp = $arr_format;
				unset($arr_fmt_tmp['codec']);
				$arr_data_file['format_calc_long'] = implode(' - ',array_filter($arr_fmt_tmp));
				unset($arr_fmt_tmp);
			}
			
            // VP 16/05/2013 : estimation tcin et tcout
            if(!empty($arr_data_file['system_timecode']))
				$arr_data_file['tcin']=$arr_data_file['system_timecode'];
            else if(!empty($arr_data_file['timecode']))
				$arr_data_file['tcin']=$arr_data_file['timecode'];
            else 
				$arr_data_file['tcin']='00:00:00:00';
            $arr_data_file['tcin']=secDecToTc(tcToSecDec($arr_data_file['tcin']));
			$arr_data_file['tcout']=secDecToTc(tcToSecDec($arr_data_file['tcin'])+$arr_data_file['duration']);
		}
		else if ($type=='audio')
		{
			$id_data=MatInfo::getID3($file);

			if (isset($id_data['data']) && !empty($id_data['data']))
				MatInfo::saveCover(kDocumentDir.$id_data['image_path'],$id_data['data']);

			$arr_data_file=$id_data;
			
			$arr_data_file['format_calc'] = MatInfo::getFormatCalc($file,'audio',$data_mediainfo,$arr_format);
			if(!empty($arr_format) && !empty($arr_format['codec_long'])){
				$arr_fmt_tmp = $arr_format;
				unset($arr_fmt_tmp['codec']);
				$arr_data_file['format_calc_long'] = implode(' - ',array_filter($arr_fmt_tmp));
				unset($arr_fmt_tmp);
			}
			
			$arr_data_file['format'] = $arr_data_file['format_calc'];
			$data_ffprobe=MatInfo::getFFprobe($file);
			$arr_data_file['bit_rate']=$data_ffprobe['bit_rate'];
			$arr_data_file['duration']=$data_ffprobe['duration'];
			$arr_data_file['timecode']=$data_ffprobe['timecode'];
		    $arr_data_file['tcin']=secDecToTc(tcToSecDec($arr_data_file['tcin']));
			$arr_data_file['tcout']=secDecToTc(tcToSecDec($arr_data_file['tcin'])+$arr_data_file['duration']);
			
			$arr_tmp_tracks = array();
			foreach($data_ffprobe['tracks'] as $idx=>$track){
				// si les tracks ont le meme index => on les reporte
				if(strtolower($track['type']) == strtolower($data_mediainfo['tracks'][$idx][type])
				|| (strtolower($track['type'])== 'subtitle' && strtolower($data_mediainfo['tracks'][$idx][type]) == 'text')
				|| (strtolower($track['type'])== 'data' && strtolower($data_mediainfo['tracks'][$idx][type]) == 'menu')
				){
					$arr_tmp_tracks[$idx] = $data_mediainfo['tracks'][$idx];
					unset($data_mediainfo['tracks'][$idx]);
				}else {
					//Si les tracks n'ont pas le meme index, on va chercher la track correspondante en fonction du type
					foreach($data_mediainfo['tracks'] as $idx_=>$track_){
						if(strtolower($track['type']) == strtolower($data_mediainfo['tracks'][$idx_][type])
						|| (strtolower($track['type'])== 'subtitle' && strtolower($data_mediainfo['tracks'][$idx_][type]) == 'text')
						|| (strtolower($track['type'])== 'data' && strtolower($data_mediainfo['tracks'][$idx_][type]) == 'menu')
						){
							$tmp_track = $data_mediainfo['tracks'][$idx_];
							$idx_tmp = $idx_;
							break;
						}
					}
					$arr_tmp_tracks[$idx] = $tmp_track;
					unset($tmp_track);
					unset($data_mediainfo['tracks'][$idx_tmp]);
					unset($idx_tmp);
				}
			}
			// on remplace l'array tracks mediainfo avec la nouvelle correctement ordonnée
			$data_mediainfo['tracks'] = $arr_tmp_tracks;
			unset($arr_tmp_tracks);
			
			$index_track=0;
			// liste des pistes audio issues de ffprobe ou mediainfo
			foreach ($data_mediainfo['tracks'] as $track_mediainfo)
			{
				$arr_fields=array('index','type','format','format_long','format_profile','format_settings_gop','format_setings_wrapping','format_version','codec_id','height','width','display_aspect_ratio','bit_rate','frame_rate','bit_depth','language','channels','sample_rate','scan_type','scan_order');
				$arr_data_file['tracks'][$index_track]['index']=$index_track;
                
				$track_ffprobe=$data_ffprobe['tracks'][$index_track];
				//$track_mediainfo=$data_mediainfo['tracks'][$index_track];
				
				if (strtolower($track_mediainfo['type'])=='audio' && !empty($track_mediainfo['muxing_mode']))
				{
					unset($arr_data_file['tracks'][$index_track]);
					$index_track++;
					continue;
				}
				
				foreach($arr_fields as $field)
				{
					if (isset($track_mediainfo[$field]) && !empty($track_mediainfo[$field]))
						$arr_data_file['tracks'][$index_track][$field]=$track_mediainfo[$field];
                    elseif (isset($track_ffprobe[$field]) && !empty($track_ffprobe[$field]))
                        $arr_data_file['tracks'][$index_track][$field]=$track_ffprobe[$field];
				}
				if(!empty($data_ffprobe['duration'])){
					$arr_data_file['tracks'][$index_track]['duration']=floatval(trim($data_ffprobe['duration']));
				}else{
					$arr_data_file['tracks'][$index_track]['duration']=floatval(trim($track_mediainfo['duration']));
				}
				$arr_data_file['tracks'][$index_track]['start_time']=floatval(trim($data_ffprobe['start_time']));
				$arr_data_file['tracks'][$index_track]['timecode']=$track_ffprobe['timecode'];
				if (!empty($arr_data_file['tracks'][$index_track]['timecode']) && empty($arr_data_file['timecode']))
				{
					$arr_data_file['timecode']=$arr_data_file['tracks'][$index_track]['timecode'];
				}
				$index_track++;
			}
		}
		else if ($type=='image')
		{
			$arr_data_file=MatInfo::getImagickInfo($file);
			
			$arr_data_file['format_calc'] = MatInfo::getFormatCalc($file,'image',$arr_data_file,$arr_format);
			if(!empty($arr_format) && !empty($arr_format['codec_long'])){
				$arr_fmt_tmp = $arr_format;
				unset($arr_fmt_tmp['codec']);
				$arr_data_file['format_calc_long'] = implode(' - ',array_filter($arr_fmt_tmp));
				unset($arr_fmt_tmp);
			}

			if(!isset($sous_type) || empty($sous_type)){
				$tmp_mime=explode('/',$type_mime);
				$sous_type=$tmp_mime[1];
			}
			
			if ($sous_type=='jpeg' || $sous_type=='tiff' || $sous_type=='tif')
			{
				$data_exif=MatInfo::getExif($file); // en cas de jpg ou tiff
				$arr_data_file['EXIF']=$data_exif;
				$arr_data_file['resolution']=$data_exif['DPI'];
				
				$data_iptc=MatInfo::getIptc($file);
				$arr_data_file['IPTC']=$data_iptc;
				$data_xmp=MatInfo::getXMP($file);
				if(!$data_xmp){
					$data_xmp=MatInfo::getXMPfromExif($data_exif);
				}

				$arrXml['EXIF']=$data_exif;
				$arrXml['IPTC']=$data_iptc;
				encode_utf8_array($arrXml);
                //$xml=tab2xml($arrXml);
                $xml=array2xml($arrXml);
                trace('mat info 1');
                trace($xml);
                if(strpos($xml,"</xml>")>0){
                    $xml=substr($xml,0,strpos($xml,"</xml>")).$data_xmp."</xml>";
                } else $xml.="<xml>".$data_xmp."</xml>";
                trace('mat info 2');
                trace($xml);
				$arr_data_file['xml']=$xml;
			}
			trace('$arr_data_file');
			trace(print_r($arr_data_file,1));
			
		}else{
			if ($sous_type == "pdf"){ // limitation de l'appel de pdfinfo aux fichiers pdf/ai 
				if( strcasecmp(getExtension($file),'ai') == 0 ) 
					$type = "image";
				$data_pdf=MatInfo::getPdfInfo($file);
				$arr_data_file=$data_pdf;
			}
			//le sous_type peut-être plain, vtt
			//attention in_array sensible à la casse, rajouter donc les extensions en minuscules 
			else if ($type=='text' &&  in_array(getExtension($file),array('vtt','srt'))){
				$arr_data_file=MatInfo::getSubtitleInfo($file);
			}
			try{
				$arr_data_file['format_calc'] = MatInfo::getFormatCalc($file,'document',$arr_data_file,$arr_format);
				if(!empty($arr_format) && !empty($arr_format['codec_long'])){
					$arr_fmt_tmp = $arr_format;
					unset($arr_fmt_tmp['codec']);
					$arr_data_file['format_calc_long'] = implode(' - ',array_filter($arr_fmt_tmp));
					unset($arr_fmt_tmp);
				}
				
			}catch(Exception $e){
				trace("crash getFormatCalc document : ".$e->getMessage());
			}
		}
		
		// VP 27/11/13 : informations communes à tous les fichiers
        $arr_data_file['type_mime']=$type_mime;
		if(isset($hls_dir) && is_dir($hls_dir)){
			// récupération de la taille du dossier
			$ret = shell_exec("du -s -B1 ".$hls_dir);
			if($ret && strpos($ret,"\t")!==false){
				$arr_data_file['file_size'] = substr($ret,0,strpos($ret,"\t"));
			}else{
				$arr_data_file['file_size']=getFileSize($file);
			}
		}else{
			$arr_data_file['file_size']=getFileSize($file);
        }
		$arr_data_file['file_time']=MatInfo::getDateTimeFromFile($file);

		if ($type == 'video'){
			$arr_data_file['id_media'] = 'V';
		}else if ($type == 'audio'){
			$arr_data_file['id_media'] = 'A';
		}else if ($type == 'image'){
			$arr_data_file['id_media'] = 'P';
		}

		return $arr_data_file;
	}
	
	private static function getMediaInfo($file)
	{
        $ssh=(strpos(kMediainfoPath,"ssh")!==false);
		$cmd=kMediainfoPath." --Output=XML ".escapeQuoteShell($file,$ssh);
		
		$xml_infos=shell_exec($cmd);
		$xml_infos=mb_convert_encoding(str_replace("\xA9","",$xml_infos),"UTF-8");
		$arr_info=xml2tab($xml_infos);

		$arr_tracks=array('tracks'=>array());
		
		// lecture de la piste video
		if(isset($arr_info['MEDIAINFO'][0]['FILE'][0]['TRACK'])){
			foreach($arr_info['MEDIAINFO'][0]['FILE'][0]['TRACK'] as $track)
			{
				
				$donnees_track=array();
				
				if (strtolower($track['attributes']['TYPE'])=='general')
				{
					$arr_tracks['type']=$track['attributes']['TYPE'];
					$arr_tracks['format']=$track['FORMAT'];
					if(!empty($track['FORMAT_COMMERCIAL_IFANY'])) {
						$donnees_track['format_long']=$track['FORMAT_COMMERCIAL_IFANY'];
					} else {
						$donnees_track['format_long']=$track['COMMERCIAL_NAME'];
					}
					$arr_tracks['format_profile']=$track['FORMAT_PROFILE'];
				}
				elseif (strtolower($track['attributes']['TYPE'])=='video')
				{
					$donnees_track['type']=$track['attributes']['TYPE'];
					$donnees_track['format']=$track['FORMAT'];
					if(!empty($track['FORMAT_COMMERCIAL_IFANY'])) {
						$donnees_track['format_long']=$track['FORMAT_COMMERCIAL_IFANY'];
					} else {
						$donnees_track['format_long']=$track['COMMERCIAL_NAME'];
					}
					$donnees_track['format_profile']=$track['FORMAT_PROFILE'];
					$donnees_track['format_settings_gop']=$track['FORMAT_SETTINGS__GOP'];
					$donnees_track['format_setings_wrapping']=$track['FORMAT_SETINGS_WRAPPING'];
					$donnees_track['format_version']=$track['FORMAT_VERSION'];
					$donnees_track['codec_id']=$track['CODEC_ID'];
					$donnees_track['duration']=MatInfo::calcDurationWithUnits($track['DURATION']);
					
					// largeur et hauteur
					$data_taille=explode(' ',$track['HEIGHT']);
					unset($data_taille[count($data_taille)-1]);
					$donnees_track['height']=intval(implode('',$data_taille));
					
					$data_taille=explode(' ',$track['WIDTH']);
					unset($data_taille[count($data_taille)-1]);
					$donnees_track['width']=intval(implode('',$data_taille));
					
					$donnees_track['display_aspect_ratio']=$track['DISPLAY_ASPECT_RATIO'];
					$donnees_track['bit_rate_mode']=$track['BIT_RATE_MODE'];
					$donnees_track['bit_rate']=MatInfo::calcSizeWithUnit($track['BIT_RATE']);
					$donnees_track['frame_rate_mode']=$track['FRAME_RATE_MODE'];
					$donnees_track['frame_rate']=floatval($track['FRAME_RATE']);
					$donnees_track['standard']=$track['STANDARD'];
					$donnees_track['color_space']=$track['COLOR_SPACE'];
					$donnees_track['chroma']=$track['CHROMA_SUBSAMPLING'];
					$donnees_track['bit_depth']=MatInfo::calcSizeWithUnit($track['BIT_DEPTH']);
					$donnees_track['scan_type']=strtolower($track['SCAN_TYPE']);
					$donnees_track['scan_order']=$track['SCAN_ORDER'];
					$donnees_track['language']=$track['LANGUAGE'];
					
					$arr_tracks['tracks'][]=$donnees_track;
				}
				else if(strtolower($track['attributes']['TYPE'])=='audio')
				{
					//if (empty($track['MUXING_MODE']))
					//{
						$donnees_track['id']=$track['ID'];
						$donnees_track['type']=$track['attributes']['TYPE'];
						$donnees_track['format']=$track['FORMAT'];
						$donnees_track['muxing_mode']=$track['MUXING_MODE'];
						$donnees_track['format_profile']=$track['FORMAT_PROFILE'];
						$donnees_track['format_settings_wrapping']=$track['FORMAT_SETTINGS_WRAPPING'];
						$donnees_track['format_version']=$track['FORMAT_VERSION'];
						
						if(strlen($track['CODEC_ID'])==4)
							$donnees_track['codec_id']=$track['CODEC_ID'];
						
						$donnees_track['duration']=MatInfo::calcDurationWithUnits($track['DURATION']);
						$donnees_track['channels']=intval($track['CHANNEL_S_']);
						$donnees_track['bit_rate_mode']=$track['BIT_RATE_MODE'];
						$donnees_track['bit_rate']=MatInfo::calcSizeWithUnit($track['BIT_RATE']);
						$donnees_track['bit_depth']=MatInfo::calcSizeWithUnit($track['BIT_DEPTH']);
						
						//calcul de la frequence d'echantillonage
						$data_sampling_rate=explode(' ',$track['SAMPLING_RATE']);
						$unit=$data_sampling_rate[count($data_sampling_rate)-1];
						unset($data_sampling_rate[count($data_sampling_rate)-1]);
						if (strtolower(substr($unit,0,1))=='k')
							$sampling_rate=intval(implode('',$data_sampling_rate))*1000;
						else
							$sampling_rate=intval(implode('',$data_sampling_rate));
						
						$donnees_track['sample_rate']=$sampling_rate;
						$donnees_track['language']=$track['LANGUAGE'];
						
						$arr_tracks['tracks'][]=$donnees_track;
					//}
				}
				else
				{
					$donnees_track['type']=$track['attributes']['TYPE'];
					$donnees_track['format']=$track['FORMAT'];
					$donnees_track['language']=$track['LANGUAGE'];
					$arr_tracks['tracks'][]=$donnees_track;
				}
			}
		}

		
		// trace("return getMediainfo : ".print_r($arr_tracks,true));

		return $arr_tracks;
	}
	
	 static function getFFprobe($file)
	{
        $ssh=(strpos(kFFPROBEpath,"ssh")!==false);
		$cmd=kFFPROBEpath.' -show_format -show_streams '.escapeQuoteShell($file, $ssh);
		// trace($cmd);
		$infos_ffprobe=shell_exec($cmd);
		
		$arr_tracks=array();
		$arr_general=array();
		
		preg_match_all('/\[FORMAT\](.*)\[\/FORMAT\]/smU',$infos_ffprobe,$arr_info_format);
		foreach (explode("\n",trim($arr_info_format[1][0])) as $format)
		{
			$fmt=explode('=',$format,2);
			$arr_general[$fmt[0]]=$fmt[1];
		}
		
		$arr_tracks['format']=$arr_general['format_name'];
		$arr_tracks['format_long']=$arr_general['format_long'];
		$arr_tracks['format_profile']=$arr_general['TAG:operational_pattern'];
		$arr_tracks['duration']=$arr_general['duration'];
		$arr_tracks['start_time']=$arr_general['start_time'];
		$arr_tracks['system_timecode']=$arr_general['TAG:system_timecode'];
		$arr_tracks['timecode']=$arr_general['TAG:timecode'];
		$arr_tracks['bit_rate']=$arr_general['bit_rate'];
		$arr_tracks['tracks']=array();
		
		preg_match_all('/\[STREAM\](.*)\[\/STREAM\]/smU',$infos_ffprobe,$arr_info);
		//$arr_info=explode('[STREAM]',$infos_ffprobe);
		//$arr_info=xml2tab(str_replace(array('[',']'),array('<','>'),$infos_ffprobe));
		foreach ($arr_info[1] as $stream)
		{
			$donnees_track=array();
			$params_stream=array();
			
			foreach (explode("\n",trim($stream)) as $param)
			{
				$param=explode('=',trim($param));
				$params_stream[$param[0]]=$param[1];
			}
			
			if (strcasecmp($params_stream['codec_type'],'video') == 0)
			{
				//var_dump($params_stream);
				$donnees_track['index']=$params_stream['index'];
				$donnees_track['type']=$params_stream['codec_type'];
				$donnees_track['format']=$params_stream['codec_name'];
				$donnees_track['format_long']=$params_stream['format_long_name'];
				
				if(strlen($params_stream['codec_tag_string'])==4)
					$donnees_track['codec_id']=$params_stream['codec_tag_string'];
				
				$donnees_track['duration']=floatval($params_stream['duration']);
				$donnees_track['start_time']=floatval($params_stream['start_time']);
				$donnees_track['height']=intval($params_stream['height']);
				$donnees_track['width']=intval($params_stream['width']);
				$donnees_track['display_aspect_ratio']=$params_stream['display_aspect_ratio'];
				$donnees_track['sample_aspect_ratio']=$params_stream['sample_aspect_ratio'];
				$donnees_track['bit_rate']=$params_stream['bit_rate'];
				$data_fr=explode('/',$params_stream['r_frame_rate']);
				$donnees_track['frame_rate']=$data_fr[0]/$data_fr[1];
				
				$donnees_track['scan_order']=$params_stream['interlaced'];
				
                if(!empty($params_stream['interlaced']))
					$donnees_track['scan_type']="interlaced";
                elseif(!empty($params_stream['progressive']))
					$donnees_track['scan_type']="progressive";
				
				$donnees_track['pix_fmt']=$params_stream['pix_fmt'];
				
				$donnees_track['language']=$params_stream['TAG:language'];
				
				if (isset($params_stream['TAG:timecode']) && !empty($params_stream['TAG:timecode']))
					$donnees_track['timecode']=$params_stream['TAG:timecode'];
				else if ($params_stream['timecode']!='N/A')
					$donnees_track['timecode']=$params_stream['timecode'];

			}
			else if ($params_stream['codec_type']=='audio')
			{
				//var_dump($params_stream);
				$donnees_track['index']=$params_stream['index'];
				$donnees_track['type']=$params_stream['codec_type'];
				$donnees_track['format']=$params_stream['codec_name'];
				$donnees_track['format_long']=$params_stream['format_long_name'];
				
				if(strlen($params_stream['codec_tag_string'])==4)
					$donnees_track['codec_id']=$params_stream['codec_tag_string'];
				
				$donnees_track['duration']=floatval($params_stream['duration']);
				$donnees_track['channels']=intval($params_stream['channels']);
				$donnees_track['bit_rate']=$params_stream['bit_rate'];
				$donnees_track['bit_depth']=$params_stream['bits_per_sample'];
				$donnees_track['sample_rate']=intval($params_stream['sample_rate']);
				$donnees_track['language']=$params_stream['TAG:language'];
			}
			else
			{
				//var_dump($params_stream);
				$donnees_track['index']=$params_stream['index'];
				$donnees_track['type']=$params_stream['codec_type'];
				$donnees_track['format']=$params_stream['codec_name'];
				$donnees_track['format_long']=$params_stream['format_long_name'];
				
				if(strlen($params_stream['codec_tag_string'])==4)
					$donnees_track['codec_id']=$params_stream['codec_tag_string'];
				
				$donnees_track['language']=$params_stream['TAG:language'];

			}
			//var_dump($params_stream);
			$arr_tracks['tracks'][]=$donnees_track;
		}
		// trace("return getFFprobe : ".print_r($arr_tracks,true));
		return $arr_tracks;
	}
	
	private static function getID3($file)
	{
		//echo 'getID3'."\n";
		
		$data_id3=array();
		 
		$getid3=new getID3();
		$infos_id3=$getid3->Analyze($file);

		
		if ($extension_vignette=='image/jpeg')
			$extension_vignette='jpg';
		else if ($extension_vignette=='image/png')
			$extension_vignette='png';
		else
			$extension_vignette='jpg';
		
		$data_id3['title']=$infos_id3['id3v2']['comments']['title'][0];
		$data_id3['artist']=$infos_id3['id3v2']['comments']['artist'][0];
		$data_id3['album']=$infos_id3['id3v2']['comments']['album'][0];
		$data_id3['year']=$infos_id3['id3v2']['comments']['year'][0];
		$data_id3['genre']=$infos_id3['id3v2']['comments']['genre'][0];
		$data_id3['image_path']=basename($file).'.art.'.$extension_vignette;
		$data_id3['data']=$infos_id3['id3v2']['APIC'][0]['data'];
		return $data_id3;
	}
	
	public static function saveCover($file_name,$contenu)
	{
		//echo 'saveCover'."\n";
		
		file_put_contents($file_name,$contenu);
	}
	
	public static function writedata($filename, $filecontents) 
	{
		if (!$handle = fopen($filename, 'wb')) 
			return 'Cannot open file ('.$filename.')';

		if (!fwrite($handle, $filecontents)) 
			return 'Cannot write to file ('.$filename.')';

		fclose($handle);
		return true;
	}
	
	public static function getImagickInfo($file)
	{		
		setImagickPath();

		$arr_donnees=array();
        if (extension_loaded('imagick')) {
                try{
                $imagick=new Imagick();
                $imagick->readImage($file);
                
                $arr_donnees['width']=$imagick->getImageWidth();
                $arr_donnees['display_width']=$imagick->getImageWidth();
                
                $arr_donnees['height']=$imagick->getImageHeight();
                $arr_donnees['display_height']=$imagick->getImageHeight();
                
                $arr_donnees['format_calc']=$imagick->getImageFormat();
                $arr_donnees['format']=$imagick->getImageFormat();
            }catch(Exception $e){
                trace("Matinfo::getImagickInfo exception:".$e);
            }
        }elseif(defined('kIdentifyPath')){
            $identify=shell_exec(kIdentifyPath." -verbose ".escapeQuoteShell($file));
            
            preg_match_all('/Format: (.*)/', $identify, $matches);
            $format=$matches[1][0];
            if(strpos($format,' ')>0) $format=substr($format,0,strpos($format,' '));
            
            preg_match_all('/Geometry: (.*)/', $identify, $matches);
            $geometry=$matches[1][0];
            if(strpos($geometry,'+')>0) $geometry=substr($geometry,0,strpos($geometry,'+'));
            $dim=explode('x',$geometry);
            $width=$dim[0];
            $height=$dim[1];
            
            $arr_donnees['width']=$width;
            $arr_donnees['display_width']=$width;
            
            $arr_donnees['height']=$height;
            $arr_donnees['display_height']=$height;
            
            $arr_donnees['format']=$format;
            $arr_donnees['format_calc']=$format;
            
        }
        return $arr_donnees;
	}
	
	private static function getExif($file)
	{
		
		
		/**
		 * @author B.RAVI 09/02/17 11:57
		 * éviter Warning: exif_read_data(image.jpg) : File too small(0)
		 * par ex dans le cas ou le _vis.jpg n'a pas été généré
		 */
		if (file_exists($file) && filesize($file)!=0){
			$exif=exif_read_data($file,'ANY_TAG',true);
		}
		// permet de filtrer tout ce qui n'est pas directement utile (et d'eviter de stocker des array gigantesque dans le champ mat_info)
		/**
		 * @author B.RAVI 09/02/17 11:37
		 *  éviter Warning: Invalide argument supplied foreach()
		 */
		if(is_object($exif) || is_array($exif)){
			foreach($exif as $section=>$arr_section){
				if(!in_array($section,array("COMPUTED","EXIF","FILE","IFD0"))){
					unset($exif[$section]);
                }else{
                    foreach($exif[$section] as $soussection=>$arr_soussection){
                        if(strpos($soussection, "UndefinedTag")===0){
                            unset($exif[$section][$soussection]);
                        }elseif(strpos($soussection, "/")>0){
                            $exif[$section][str_replace("/","_",$soussection)]=$exif[$section][$soussection];
                            unset($exif[$section][$soussection]);
                        }
                    }
                }
			}
			
		}

		$arr_data=array
		(
			'EXIF'=>$exif,
			'format'=>$exif['FILE']['MimeType'],
			'date'=>$exif['EXIF']['DateTimeOriginal'],
			'title'=>$exif['IFD0']['ImageDescription'],
			'artist'=>$exif['COMPUTED']['Copyright'],
			'orientation_exif'=>$exif['IFD0']['Orientation']
		);

		if(!empty($exif['IFD0']['XResolution'])){
			$dpi=$exif['IFD0']['XResolution'];
			$calc_dpi=explode('/',$dpi);
			$dpi = ($calc_dpi[0]/$calc_dpi[1]);
			$arr_data['DPI'] = $dpi;
		}
		if(isset($exif['IFD0']['ExtensibleMetadataPlatform'])){
			$arr_data['xmp'] = $exif['IFD0']['ExtensibleMetadataPlatform'];
		}
		//unset($arr_data['EXIF']['EXIF']);
		//unset($arr_data['EXIF']['IFD0']);
		return $arr_data;
	}
	
	private static function getIptc($file)
	{
		require_once(libDir."class_iptc.php");
		$iptc=new class_iptc($file);
		$ip=$iptc->fct_lireIPTC();
		
		if ($ip['caption']&& empty($ip['title'])){
			$arr = explode("\n", $ip['caption']);
			$ip['title']=$arr[0];
		}
		
		return $ip;
	}

    private static function getXmp($file)
	{
		setImagickPath();
		
		if ( !defined('kConvertPath') ) {
            echo 'Program convert not found.';
            return null;
        }
        try {
        	$xmp_infos=shell_exec(kConvertPath." ".escapeQuoteShell($file)." XMP:-");
        } catch (Exception $e) {
        	trace("erreur récupération XMP : ".kConvertPath." ".escapeQuoteShell($file)." XMP:-");
        	return "";
        }
		
        trace(print_r($xmp_infos,1));
        
        $xmpdata_start = strpos($xmp_infos,"<x:xmpmeta");

        //Pour une raison encore inconnue, imageMagick ne récupère pas correctement les meta XMP de certains fichiers que NOUS générons : il manque le premier chevron
        if ( $xmpdata_start === false  && strpos($xmp_infos,"x:xmpmeta") !== false ) { 
        	//On ajoute le chevron manquant (<) à la balise d'ouverture : <x:xmpmeta ... >
        	$xmp_infos = preg_replace("#(?<!/)x:xmpmeta(?=\s+|>)#", "<x:xmpmeta", $xmp_infos);
        	$xmpdata_start = strpos($xmp_infos,"<x:xmpmeta");
        }
        $xmpdata_end = strpos($xmp_infos,"</x:xmpmeta>");
		
		if ($xmpdata_start !== false && $xmpdata_end !== false) {
			$xmplenght = $xmpdata_end-$xmpdata_start;
			$xmp_infos = substr($xmp_infos,$xmpdata_start,$xmplenght+12);
			return $xmp_infos;
		}
		else
			return "";
	}
	private static function getXmpfromExif(&$exif)
	{
		if(isset($exif['xmp'])){
			$xmp_infos = $exif['xmp'];
			unset($exif['xmp']);
		}else{
			return false;
		}
        $xmpdata_start = strpos($xmp_infos,"<x:xmpmeta");
        $xmpdata_end = strpos($xmp_infos,"</x:xmpmeta>");
        if($xmpdata_start === false || $xmpdata_end === false){
            return '';
        }
        $xmplenght = $xmpdata_end-$xmpdata_start;
        $xmp_infos = substr($xmp_infos,$xmpdata_start,$xmplenght+12);
		
		return $xmp_infos;
	}
	private static function getPdfInfo($file)
	{
		$data_pdf=array();
		
		if (!file_exists(kPdfinfoPath))
		{
			echo 'Program pdfinfo not found.';
			return null;
		}
		
		$pdf_infos=shell_exec(kPdfinfoPath.' '.escapeQuoteShell($file));
		$infos=array();
		foreach (explode("\n",$pdf_infos) as $info)
		{
			$data_infos=explode(':',$info,2);
			if (!empty($data_infos[0]))
				$infos[$data_infos[0]]=trim($data_infos[1]);
		}
		
		$data_pdf['pages']=intval($infos['Pages']);
		
		$taille=explode('x',$infos['Page size']);
		$data_pdf['width']=intval($taille[0]);
		$data_pdf['height']=intval($taille[1]);
		
		$data_pdf['pdfinfo']=$infos;
		
		return $data_pdf;
	}
        
        
        private static function getSubtitleInfo($file){
            
                $data_subtitles = array();
                $file_as_array = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                /* Attention on met === et pas juste == car on veut detecter le cas ou file() a echoué (c'est a dire bool FALSE)
                  or dans un fichier existant mais vide , on obtient $file_as_array=array(0)==false */
                if ($file_as_array === FALSE) {
                    trace('getSubtitleInfo : file() failed');
                    return null;
                }
                $i = 1;
                foreach ($file_as_array as $f) {
                // Find lines containing "-->"  
                // Attention, en bdd, format hh:mm:ss:mm donc on capture hh:mm:ss:mm et pas hh:mm:ss:mmm
                    if (preg_match("/^(\d{2}:\d{2}:\d{2})(\.|,)(\d{2})\d{1} --> (\d{2}:\d{2}:\d{2})(\.|,)(\d{2})\d{1}/", $f, $match)) {
                        if ($i == 1) {
                            $tcin = $match[1].':'.$match[3];
                        }
                        $tcout = $match[4].':'.$match[6];
                        $i++;
                    }
                }

                if (!empty($tcin)) {
                    $data_subtitles['tcin'] = $tcin;
                }

                if (!empty($tcout)) {
                    $data_subtitles['tcout'] = $tcout;
                }

                return $data_subtitles;
        }
	
        
	public static function getTypeMimeFromFile($filename)	
	{
		//argument magic_file non supporté pour les versions php > 5.4
		if (version_compare(phpversion(), '5.4', '<')) {
			$magic_file=(is_file("/usr/share/misc/magic.mgc")?"/usr/share/misc/magic.mgc":"");
			$fileinfo_mime_type_version=finfo_open(FILEINFO_MIME_TYPE,$magic_file);
			$fileinfo_mime_version=finfo_open(FILEINFO_MIME,$magic_file);
		}
		else{
			$fileinfo_mime_type_version=finfo_open(FILEINFO_MIME_TYPE);
			$fileinfo_mime_version=finfo_open(FILEINFO_MIME);
		}
        
		if (((defined("FILEINFO_MIME_TYPE") && ($fd=$fileinfo_mime_type_version))) || (defined("FILEINFO_MIME") && ($fd=$fileinfo_mime_version)))
		{
			$donnees=null;
			
			if (file_exists($filename))
			{
				$donnees=finfo_file($fd,$filename);
			}
			finfo_close($fd);
			
			if ($donnees=='application/octet-stream')
			{
				// BUG DE FILE INFO
				// le type MIME des fichier mpeg 1 n'est pas detecte correctement. finfo_file renvoie application/octet-stream
				// le bug est le meme avec le fichiers .vob
				// les fichier mov renvoient aussi application/octet-stream
				// les fichier mxf renvoient aussi application/octet-stream
				// les fichier mkv renvoient aussi application/octet-stream dans certains cas (voir /mnt/nas-intranet/encodage/decoupages-ina/Sources/Video/test5.mkv)
				// le test sur l'extension est provisoire.
                // VP 20/06/13 ajout extension ts
				$ext_file=getExtension($filename);
				if ($ext_file=='mpeg' || $ext_file=='mpg' || $ext_file=='ts' || $ext_file=='vob')
				{
					$donnees='video/mpeg';
				}
				else if ($ext_file=='mov')
					$donnees='video/quicktime';
				else if ($ext_file=='mxf')
					$donnees='video/mxf';
				else if ($ext_file=='mkv')
					$donnees='video/x-matroska';
				else if ($ext_file=='mp3')
					$donnees='audio/mpeg';
			}
			// trace("gettypemime ".$donnees);
			return $donnees;
		}
		else
			throw new Exception('finfo_open');
	}
	
	public static function getDateTimeFromFile($filename)
	{
		if (file_exists($filename))
			return date('Y-m-d H:i:s',filemtime($filename)); // recuperation de la date de modification
		
		return null;
	}
	
	public static function findMPEGTcIN($file)
	{
		$buffer="";
		$isGop=false;
		
		$fd = fopen ($file, "rb");
		
		if($fd)
		{
			while (!feof($fd) && !($isGop))
			{
				$chunk=bin2hex(fread($fd, 8192));
				$buffer.=$chunk;
				// On cherche le header de GOP
				if(strpos($buffer,"000001b8")===false) // GOP pas trouvé
					$buffer=$chunk;
				else
				{
					// GOP trouvé, on alimente un peu le buffer
					$isGop=true;
					$buffer.=bin2hex(fread($fd, 8));
				}
			}
			
			fclose ($fd);
			
			if($isGop)
			{
				// Extraction du TC
				$tc_hex=substr($buffer,strpos($buffer,"000001b8")+8,8); // Le TC est codé dans les 4 octets suivants
				$tc_bin=sprintf("%032s",base_convert($tc_hex,"16","2")); // Conversion en binaire avec complétion des zéros
				// Extraction de h, m, s et f
				$h=base_convert(substr($tc_bin,1,5),"2","10");
				$m=base_convert(substr($tc_bin,6,6),"2","10");
				$s=base_convert(substr($tc_bin,13,6),"2","10");
				$f=base_convert(substr($tc_bin,19,6),"2","10");
				return sprintf("%02d:%02d:%02d:%02d",$h,$m,$s,$f);
			}
		}
	}
	
	public static function findMovTcIN($file)
	{
		if(defined('kFFMBCpath'))
		{
			$cmd=kFFMBCpath.' -i "'.$file.'" 2>&1';
			$stdout=trim(shell_exec($cmd));
			preg_match('/timecode: ([0-9]{2}:[0-9]{2}:[0-9]{2}:[0-9]{2})/',$stdout,$matches);
			$tcin=$matches[1];
			$chunk=explode(':',$tcin);
			
			if(count($chunk)==4)
				return $tcin;
		}
		return '00:00:00:00'; 
	}
	
	public static function saveID3Cover($file)
	{
		//echo 'saveID3Cover';
	}
	
	private static function calcSizeWithUnit($str_size)
	{
		if ($str_size==null)
			return null;
		
		$data_size=explode(' ',$str_size);
		$unite=$data_size[count($data_size)-1];
		unset($data_size[count($data_size)-1]);
		$size=intval(implode('',$data_size));
		
		switch(strtolower(substr($unite,0,1)))
		{
			case 'g':
				$size=$size<<10;
			case 'm':
				$size=$size<<10;
			case 'k':
				$size=$size<<10;
				break;
		}
		
		return $size;
	}
	
	private static function calcDurationWithUnits($str_duration)
	{
		if ($str_duration==null)
			return null;
		
		$arr_duration=explode(' ',$str_duration);
		$duration_in_seconds=0;
		
		foreach ($arr_duration as $duree)
		{
			if (strstr($duree,'ms'))
				$duration_in_seconds+=intval($duree)/1000;
			else if (strstr($duree,'mn'))
				$duration_in_seconds+=intval($duree)*60;
			else if (strstr($duree,'h'))
				$duration_in_seconds+=intval($duree)*3600;
			else
				$duration_in_seconds+=intval($duree);
		}
		
		return $duration_in_seconds;
	}
	
	public static function getFormatByCodecId($codec_id)
	{
		$arr_formats=array
		(
			'avdn'=>array('DNxHD','DNxHD'),
			'avd1'=>array('DV','AVID DV'),
			'avdv'=>array('DV','AVID DV'),
			'cdv2'=>array('DV','DV Video'),
			'cdvc'=>array('DV','Canopus DV Video'),
			'cdvh'=>array('DV','Canopus DV Video'),
			'cdvp'=>array('DV','DV Video'),
			'dc25'=>array('DV','DV Video'),
			'dv'=>array('DV','DV Video'),
			'dv1n'=>array('DV','DV Video C Pro 100 NTSC'),
			'dv1p'=>array('DV','DV Video C Pro 100 PAL'),
			'dv25'=>array('DV','DV Video'),
			'dv50'=>array('DV','DV Video C Pro 50'),
			'dv5n'=>array('DV','DV Video C Pro 50 NTSC'),
			'dv5p'=>array('DV','DV Video C Pro 50 PAL'),
			'dvc '=>array('DV','DV Video NTSC'),
			'dvcp'=>array('DV','DV Video PAL'),
			'dvcs'=>array('DV','DV Video'),
			'dvh1'=>array('DV','DV Video'),
			'dvh2'=>array('DV','DV Video 720p24'),
			'dvh3'=>array('DV','DV Video 720p25'),
			'dvh4'=>array('DV','DV Video 720p30'),
			'dvh5'=>array('DV','DV Video C Pro HD 1080i50'),
			'dvh6'=>array('DV','DV Video C Pro HD 1080i60'),
			'dvhd'=>array('DV','DV Video'),
			'dvhp'=>array('DV','DV Video C Pro HD 720p'),
			'dvhq'=>array('DV','DV Video'),
			'dvis'=>array('DV','DV Video'),
			'dvp'=>array('DV','DV Video Pro'),
			'dvpp'=>array('DV','DV Video Pro PAL'),
			'dvs1'=>array('DV','DV Video'),
			'dvsd'=>array('DV','DV Video'),
			'dvsl'=>array('DV','DV Video'),
			'ipdv'=>array('DV','DV Video'),
			'pdvc'=>array('DV','DV Video'),
			'sldv'=>array('DV','SoftLab DVCAM codec'),
			'avc1'=>array('H264','H264'),
			'h264'=>array('H264','H264'),
			'x264'=>array('H264','H264'),
			'hdv1'=>array('HDV','HDV 720p30'),
			'hdv2'=>array('HDV','Sony HDV 1080i60'),
			'hdv3'=>array('HDV','FCP HDV 1080i50'),
			'hdv4'=>array('HDV','HDV 720p24'),
			'hdv5'=>array('HDV','HDV 720p25'),
			'hdv6'=>array('HDV','HDV 1080p24'),
			'hdv7'=>array('HDV','HDV 1080p25'),
			'hdv8'=>array('HDV','HDV 1080p30'),
			'hdv9'=>array('HDV','HDV 720p60 JVC'),
			'hdva'=>array('HDV','HDV 720p50'),
			'mx3n'=>array('IMX','MPEG2 IMX NTSC 525/60 30Mb/s'),
			'mx3p'=>array('IMX','MPEG2 IMX NTSC 625/50 30Mb/s'),
			'mx4n'=>array('IMX','MPEG2 IMX NTSC 525/60 40Mb/s'),
			'mx4p'=>array('IMX','MPEG2 IMX PAL 625/50 40Mb/s'),
			'mx5n'=>array('IMX','MPEG2 IMX NTSC 525/60 50Mb/s'),
			'mx5p'=>array('IMX','MPEG2 IMX PAL 625/60 50Mb/s'),
			'jpeg'=>array('M-JPEG','M-JPEG'),
			'ap4c'=>array('PRORES','Apple ProRes 444'),
			'ap4h'=>array('PRORES','Apple ProRes 444'),
			'apch'=>array('PRORES','Apple ProRes 422 HQ'),
			'apcn'=>array('PRORES','Apple ProRes 422 Standard'),
			'apco'=>array('PRORES','Apple ProRes 422 Proxy'),
			'apcs'=>array('PRORES','Apple ProRes 422 LT'),
			'raw'=>array('RAW','RAW'),
			'rle'=>array('RLE','RLE'),
			'wmva'=>array('WMA','Windows Media Audio'),
			'vc-1'=>array('WMV','Windows Media Video VC1'),
			'wmv1'=>array('WMV','Windows Media Video 7'),
			'wmv2'=>array('WMV','Windows Media Video 8'),
			'wmv3'=>array('WMV','Windows Media Video 9'),
			'wvc1'=>array('WMV','Windows Media Video VC1'),
			'xdvb'=>array('XDCAM EX','XDCAM EX 1080i60 50Mb/s'),
			'xdvc'=>array('XDCAM EX','XDCAM EX 1080i50 50Mb/s'),
			'xdvd'=>array('XDCAM EX','XDCAM EX 1080p24 50Mb/s'),
			'xdve'=>array('XDCAM EX','XDCAM EX 1080p25 50Mb/s'),
			'xdvf'=>array('XDCAM EX','XDCAM EX 1080p30 50Mb/s'),
			'xdh2'=>array('XDCAM HD','XDCAM HD422 540p'),
			'xdhd'=>array('XDCAM HD','XDCAM HD 540p'),
			'xdv1'=>array('XDCAM HD','XDCAM HD 720p30 35Mb/s'),
			'xdv2'=>array('XDCAM HD','XDCAM HD 1080i60 35Mb/s'),
			'xdv3'=>array('XDCAM HD','XDCAM HD 1080i50 35Mb/s'),
			'xdv4'=>array('XDCAM HD','XDCAM HD 720p24 35Mb/s'),
			'xdv5'=>array('XDCAM HD','XDCAM HD 720p25 35Mb/s'),
			'xdv6'=>array('XDCAM HD','XDCAM HD 1080p24 35Mb/s'),
			'xdv7'=>array('XDCAM HD','XDCAM HD 1080p25 35Mb/s'),
			'xdv8'=>array('XDCAM HD','XDCAM HD 1080p30 35Mb/s'),
			'xdv9'=>array('XDCAM HD','XDCAM HD 720p60 35Mb/s'),
			'xdva'=>array('XDCAM HD','XDCAM HD 720p50 35Mb/s'),
			'xd54'=>array('XDCAM HD422','XDCAM HD422 720p24 50Mb/s'),
			'xd55'=>array('XDCAM HD422','XDCAM HD422 720p25 50Mb/s'),
			'xd59'=>array('XDCAM HD422','XDCAM HD422 720p60 50Mb/s'),
			'xd5a'=>array('XDCAM HD422','XDCAM HD422 720p50 50Mb/s'),
			'xd5b'=>array('XDCAM HD422','XDCAM HD422 1080i60 50Mb/s'),
			'xd5c'=>array('XDCAM HD422','XDCAM HD422 1080i50 50Mb/s'),
			'xd5d'=>array('XDCAM HD422','XDCAM HD422 1080p24 50Mb/s'),
			'xd5e'=>array('XDCAM HD422','XDCAM HD422 1080p25 50Mb/s'),
			'xd5f'=>array('XDCAM HD422','XDCAM HD422 1080p30 50Mb/s'),
			'2vuy'=>array('YUV','YUV'),
			'27'=>array('HLS','HLS')
		);
		
		return $arr_formats[trim(strtolower($codec_id))];
	}
	
	
	public static function getFormatCalc($file,$type,&$data_file,&$arr_format = null ){
		$arr_format = array('def'=>null,'ar'=>null,'env'=>null,'codec'=>null);
		if(strtolower(trim($type)) == 'video'){
			$track_video = null ; 
			foreach($data_file['tracks'] as $id=>$track){
				if(strtolower(trim($track['type'])) == 'video'){
					if($track_video == null || (isset($track_video['width']) && isset($track['width']) && $track['width'] > $track_video['width'])){
						$track_video = $track ; 
					}
				}
			}
			
			if(isset($track_video['height']) && !empty($track_video['height']) && $track_video['height'] >= 720){
				$arr_format['def'] = 'HD';
			}else{
				$arr_format['def'] = 'SD';
			}
			
			if($arr_format['def'] == 'SD' 
			&& isset($track_video['display_aspect_ratio']) 
			&& !empty($track_video['display_aspect_ratio'])){
				$arr_format['ar'] = $track_video['display_aspect_ratio'];
			}
			$codecs = MatInfo::getCodecFromVideoTrack($track_video);
			
			if(isset($codecs['codec_long'])){
				$data_file['tracks'][$id]['codec_long'] = $codecs['codec_long'];
			}
			$arr_format = array_merge($arr_format,$codecs);
			
			if(isset($data_file['ops_type_dossier'])){
				switch($data_file['ops_type_dossier']){
					case 'DCP':
						$arr_format['env'] = 'DCP';
					break ;
					case 'DPX_main':
					case 'DPX_dir':
						$arr_format['env'] = 'DPX';
					break ; 
				}
			}else{
				$arr_format['env'] =  MatInfo::getEnvVideo($data_file,getExtension($file));
			}
			$data_file['format_env'] = $arr_format['env'];
		}else if ($type == "audio" ){
			foreach($data_file['tracks'] as $id=>$track){
				if(strtolower(trim($track['type'])) == 'audio'){
					$arr_format['def'] = MatInfo::getDefAudio($track);
					$codecs = MatInfo::getCodecFromAudioTrack($track);
					$arr_format = array_merge($arr_format,$codecs);
					break ; 
				}
			}
		}else if ($type == "image"){
			$codecs = MatInfo::getFormatImage($data_file,getExtension($file));
			$arr_format = array_merge($arr_format,$codecs);
		}else {
			$magic_file=(is_file("/usr/share/misc/magic")?"/usr/share/misc/magic":null);
			// c'est curieusement en utilisant la const FILEINFO_NONE que les infos renvoyées sont les plus proches de l'appel shell à l'utilitaire "file"
			$finfo = new finfo(FILEINFO_NONE  ,$magic_file); 
			$fileinfo_str = $finfo->file($file);
			$codecs = MatInfo::getFormatDocument($fileinfo_str ,getExtension($file));
			$arr_format = array_merge($arr_format,$codecs);
		}
		// suppression des valeurs null,false,0,"" de arr_format
		$arr_format_calc = array_filter($arr_format);
		if(isset($arr_format_calc['codec_long'])){
			unset($arr_format_calc['codec_long']) ; 
		}
		return implode(" - ",$arr_format_calc);
	}
	
	
	// MS 30/09/13 ajout fonction getTypeFromMediainfo => plus précis que de déduire le type (et donc les méthodes de récupération d'informations) du type mime du fichier
	private static function getTypeFromMediainfo($arr_mediainfo){
		$type="";
		// MS ajout filtrage sur certains types pas encore gérer sur lesquels on veut empecher la detection par comptage de tracks mediainfo
		if(defined("gPreventDetectTypeMediainfo") && in_array($arr_mediainfo['format'],unserialize(gPreventDetectTypeMediainfo))){
			return $type;
		}
		// On analyse les tracks contenue dans le retour de mediainfo
		if(isset($arr_mediainfo['tracks']) && is_array($arr_mediainfo['tracks']) && count($arr_mediainfo['tracks'])>0){
			foreach($arr_mediainfo['tracks'] as $track){
				if(isset($track['type']) && !empty($track['type'])){
					// Si type non défini & on trouve une track image => alors type devient image
					if($type=="" && strtolower($track['type'])=="image"){
						$type=strtolower($track['type']);
					}
					// Si type non défini & on trouve une track image => alors type devient image
					if($type=="" && strtolower($track['type'])=="audio"){
						$type=strtolower($track['type']);
					}
					// Si au moins une track type Vidéo => alors type devient video
					if(strtolower($track['type'])=="video"){
						$type=strtolower($track['type']);
					}
					// Si aucun type detecté, on detectera en se basant sur le mimeType
				}
			}
		}
		return ($type);
	}
	
	
	/* utilisé dans l'import (le client peut importer des csv) de l'onglet sous-titrage d'une modification de notice
	 * permet de transformer un fichier csv et d'écraser physiquement un fichier .vtt ou .srt (qui sont des matériels de la notice)
	 */
	public static function subtitle_convertCSV($filepath_csv, $filepath_sub) {

		$sub = fopen($filepath_sub, 'w');
		$ext_sub = strtolower(substr($filepath_sub, -3));

		if ($sub !== FALSE) {

			if (($csv = fopen($filepath_csv, "r")) !== FALSE) {

				/* Attention ! mettre les \n entre guillemets doubles (et pas guillemet simple '\n') pour que cela soit correctement interpreté !!!! */

				$separator_line = "\n";
				$separator_time = ',';

				if ($ext_sub == 'vtt') {
					fwrite($sub, "WEBVTT");
					$separator_time = '.';
				}

				$row = 0;

				while (($data = fgetcsv($csv, 0, ";")) !== FALSE) {
	//				$num = count($data);
	//				echo "<p> $num champs à la ligne $row: <br /></p>\n";
	//				$row++;
	//				for ($c = 0; $c < $num; $c++) {
	//					echo $data[$c] . "<br />\n";
	//				}

					/*
					  0
					  00:00:03.000 --> 00:00:07.000
					  Highway A54: a vast project

					  1
					  00:00:12.290 --> 00:00:16.110
					  Highway A54, located in southern France, */

					$data[0] = str_replace('.', $separator_time, $data[0]);
					$data[0] = str_replace(',', $separator_time, $data[0]);

					$data[1] = str_replace('.', $separator_time, $data[1]);
					$data[1] = str_replace(',', $separator_time, $data[1]);

					$before_line = $separator_line . $separator_line;

					if ($ext_sub == 'srt' && $row == 0) {
						$before_line = null;
					}

					//utf8_encode car le csv est encodé en iso, car la cliente ouvre sous Excel
					$bloc_line = utf8_encode($before_line . $row . $separator_line . $data[0] . ' --> ' . $data[1] . $separator_line . $data[2]);
					fwrite($sub, $bloc_line);
					$row++;
				}
				fclose($csv);
			}
			fclose($sub);
		}
	}
	
	
	
	private static function getCodecFromAudioTrack($track_audio){
		$codec = array() ; 
		// trace("getCodecFromAudioTrack, input track_video :  ".print_r($track_video,true));
		$arr_id_to_codec = array(
			'1'=>array('codec'=>'PCM','codec_long'=>'PCM Uncompressed'),
			'40'=>array('codec'=>'AAC','codec_long'=>'AAC'),
			'161'=>array('codec'=>'WMA','codec_long'=>'WMA 9'),
			'162'=>array('codec'=>'WMA','codec_long'=>'WMA 9 Professional')
		);
		$arr_fmt_to_codec = array(
			'AAC'=>array('codec'=>'AAC','codec_long'=>'AAC'),
			'AC-3'=>array('codec'=>'AC-3','codec_long'=>'Dolby AC-3'),
			'PCM'=>array('codec'=>'PCM','codec_long'=>'PCM Uncompressed'),
			'Vorbis'=>array('codec'=>'Vorbis','codec_long'=>'Vorbis'),
			'WMA'=>array('codec'=>'WMA','codec_long'=>'WMA')
		);
		$arr_fmtprofile_to_codec = array(
			'Layer 2'=>array('codec'=>'MP2','codec_long'=>'MPEG-1 Layer 2'),
			'Layer 3'=>array('codec'=>'MP3','codec_long'=>'MP3')
		);
		
		if(isset($track_audio)&& !empty($track_audio)){
			if(isset($track_audio['codec_id']) && isset($arr_id_to_codec[strtolower(trim($track_audio['codec_id']))])){
				$codec = $arr_id_to_codec[strtolower(trim($track_audio['codec_id']))];
				// trace("get codec from codec_id");
			}else if (isset($track_audio['format']) && isset($arr_fmt_to_codec[$track_audio['format']])){
				$codec = $arr_fmt_to_codec[$track_audio['format']];
				// trace("get codec from format");
			}else if (isset($track_audio['format']) && isset($track_audio['format_profile'])
				&& $track_audio['format'] == 'MPEG Audio' && isset($arr_fmtprofile_to_codec[$track_audio['format_profile']])){
				$codec = $arr_fmtprofile_to_codec[$track_audio['format_profile']];
				// trace("get codec from format_profile");
			}
		}
		
		if(empty($codec)){
			$codec = array("codec"=>"Unknown Audio Format","codec_long"=>"Unknown Audio Format");
			// trace("codec unknown");
		}
		return $codec;
		
	}
	
	private static function getCodecFromVideoTrack($track_video){
		$codec = array(); 
		// trace("getCodecFromVideoTrack, input track_video :  ".print_r($track_video,true));
		// liste des codecs en fonction des codec_ids (cf Référentiel FormatsV1 (nas public, projets, démo, annexes)) 	
		// les clés ont été passés en lowercase, ça ne devrait pas causer de problèmes et évite des erreurs dans l'identification du codec
		$arr_id_to_codec = array(
			'2'=>array('codec'=>'MPEG2','codec_long'=>'MPEG2 TS'),
			'4'=>array('codec'=>'VP6','codec_long'=>'TrueMotion VP6'),
			'27'=>array('codec'=>'H264','codec_long'=>'H264 TS'),
			'2vuy'=>array('codec'=>'YUV','codec_long'=>'YUV'),
			'ai15'=>array('codec'=>'AVC INTRA','codec_long'=>'AVC INTRA 100'),
			'ai52'=>array('codec'=>'AVC INTRA','codec_long'=>'AVC INTRA 50'),
			'ap4c'=>array('codec'=>'PRORES','codec_long'=>'Apple ProRes 444'),
			'ap4h'=>array('codec'=>'PRORES','codec_long'=>'Apple ProRes 444'),
			'apch'=>array('codec'=>'PRORES','codec_long'=>'Apple ProRes 422 HQ'),
			'apcn'=>array('codec'=>'PRORES','codec_long'=>'Apple ProRes 422 Standard'),
			'apco'=>array('codec'=>'PRORES','codec_long'=>'Apple ProRes 422 Proxy'),
			'apcs'=>array('codec'=>'PRORES','codec_long'=>'Apple ProRes 422 LT'),
			'avc1'=>array('codec'=>'H264','codec_long'=>'H264'),
			'avd1'=>array('codec'=>'DV','codec_long'=>'AVID DV'),
			'avdj'=>array('codec'=>'Avid Meridien','codec_long'=>'Avid Meridien Compressed'),
			'avdn'=>array('codec'=>'DNxHD','codec_long'=>'DNxHD'),
			'avdv'=>array('codec'=>'DV','codec_long'=>'AVID DV'),
			'cdv2'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'cdvc'=>array('codec'=>'DV','codec_long'=>'Canopus DV Video'),
			'cdvh'=>array('codec'=>'DV','codec_long'=>'Canopus DV Video'),
			'cdvp'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'cram'=>array('codec'=>'MS Video','codec_long'=>'Microsoft Video 1'),
			'cvid'=>array('codec'=>'Cinepak','codec_long'=>'Apple Cinepack'),
			'dc25'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'dv'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'dv1n'=>array('codec'=>'DV','codec_long'=>'DVCPRO 100 NTSC'),
			'dv1p'=>array('codec'=>'DV','codec_long'=>'DVCPRO 100 PAL'),
			'dv25'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'dv50'=>array('codec'=>'DV','codec_long'=>'DVCPRO 50'),
			'dv5n'=>array('codec'=>'DV','codec_long'=>'DVCPRO 50 NTSC'),
			'dv5p'=>array('codec'=>'DV','codec_long'=>'DVCPRO 50 PAL'),
			'dvc '=>array('codec'=>'DV','codec_long'=>'DV / DVCPRO NTSC'),
			'dvcp'=>array('codec'=>'DV','codec_long'=>'DV Video PAL'),
			'dvcs'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'dvhp'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD 720p60'),
			'dvhq'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD 720p50'),
			'dvh1'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'dvh2'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD 1080p25'),
			'dvh3'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD 1080p30'),
			'dvh4'=>array('codec'=>'DV','codec_long'=>'DV Video 720p30'),
			'dvh5'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD 1080i50'),
			'dvh6'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD 1080i60'),
			'dvsd'=>array('codec'=>'DV','codec_long'=>''),
			'dvsl'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'dx50'=>array('codec'=>'DivX','codec_long'=>'DivX 5'),
			'h264'=>array('codec'=>'H264','codec_long'=>'H264'),
			'hdv1'=>array('codec'=>'HDV','codec_long'=>'HDV 720p30'),
			'hdv2'=>array('codec'=>'HDV','codec_long'=>'HDV 1080i60'),
			'hdv3'=>array('codec'=>'HDV','codec_long'=>'HDV 1080i50'),
			'hdv4'=>array('codec'=>'HDV','codec_long'=>'HDV 720p24'),
			'hdv5'=>array('codec'=>'HDV','codec_long'=>'HDV 720p25'),
			'hdv6'=>array('codec'=>'HDV','codec_long'=>'HDV 1080p24'),
			'hdv7'=>array('codec'=>'HDV','codec_long'=>'HDV 1080p25'),
			'hdv8'=>array('codec'=>'HDV','codec_long'=>'HDV 1080p30'),
			'hdv9'=>array('codec'=>'HDV','codec_long'=>'HDV 720p60'),
			'hdva'=>array('codec'=>'HDV','codec_long'=>'HDV 720p50'),
			'icod'=>array('codec'=>'Apple Intermediate Codec','codec_long'=>'Apple Intermediate Codec'),
			'ipdv'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'jpeg'=>array('codec'=>'M-JPEG','codec_long'=>'Motion JPEG'),
			'mx3n'=>array('codec'=>'IMX','codec_long'=>'MPEG2 IMX NTSC 525/60 30Mb/s'),
			'mx3p'=>array('codec'=>'IMX','codec_long'=>'MPEG2 IMX NTSC 625/50 30Mb/s'),
			'mx4n'=>array('codec'=>'IMX','codec_long'=>'MPEG2 IMX NTSC 525/60 40Mb/s'),
			'mx4p'=>array('codec'=>'IMX','codec_long'=>'MPEG2 IMX PAL 625/50 40Mb/s'),
			'mx5n'=>array('codec'=>'IMX','codec_long'=>'MPEG2 IMX NTSC 525/60 50Mb/s'),
			'mx5p'=>array('codec'=>'IMX','codec_long'=>'MPEG2 IMX PAL 625/60 50Mb/s'),
			'pdvc'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'raw'=>array('codec'=>'RAW','codec_long'=>'RAW'),
			'rle '=>array('codec'=>'Apple Animation','codec_long'=>'Apple Animation'),
			'sldv'=>array('codec'=>'DV','codec_long'=>'SoftLab DVCAM codec'),
			'rv30'=>array('codec'=>'Real Video','codec_long'=>'Real Media Video'),
			'v_vp8'=>array('codec'=>'VP8','codec_long'=>'On2 VP8'),
			'vc-1'=>array('codec'=>'WMV','codec_long'=>'Windows Media Video VC1'),
			'wmv1'=>array('codec'=>'WMV','codec_long'=>'Windows Media Video 7'),
			'wmv2'=>array('codec'=>'WMV','codec_long'=>'Windows Media Video 8'),
			'wmv3'=>array('codec'=>'WMV','codec_long'=>'Windows Media Video 9'),
			'wmva'=>array('codec'=>'WMA','codec_long'=>'Windows Media Audio'),
			'wvc1'=>array('codec'=>'WMV','codec_long'=>'Windows Media Video VC1'),
			'xd54'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 720p24 50Mb/s'),
			'xd55'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 720p25 50Mb/s'),
			'xd59'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 720p60 50Mb/s'),
			'xd5a'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 720p50 50Mb/s'),
			'xd5b'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 1080i60 50Mb/s'),
			'xd5c'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 1080i50 50Mb/s'),
			'xd5d'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 1080p24 50Mb/s'),
			'xd5e'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 1080p25 50Mb/s'),
			'xd5f'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 1080p30 50Mb/s'),
			'xdh2'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422 540p'),
			'xdhd'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 540p'),
			'xdv1'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 720p30 35Mb/s'),
			'xdv2'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 1080i60 35Mb/s'),
			'xdv3'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 1080i50 35Mb/s'),
			'xdv4'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 720p24 35Mb/s'),
			'xdv5'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 720p25 35Mb/s'),
			'xdv6'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 1080p24 35Mb/s'),
			'xdv7'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 1080p25 35Mb/s'),
			'xdv8'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 1080p30 35Mb/s'),
			'xdv9'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 720p60 35Mb/s'),
			'xdva'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD 720p50 35Mb/s'),
			'xdvb'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM EX 1080i60 50Mb/s'),
			'xdvc'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM EX 1080i50 50Mb/s'),
			'xdvd'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM EX 1080p24 50Mb/s'),
			'xdve'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM EX 1080p25 50Mb/s'),
			'xdvf'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM EX 1080p30 50Mb/s'),
			'xvid'=>array('codec'=>'Xvid','codec_long'=>'Xvid')
		);
		
		// liste des codecs en fonction des noms commerciaux (cf Référentiel FormatsV1 (nas public, projets, démo, annexes)) 	
		// Attention, le 'commercial_name' est rangé dans "format_long" lors des traitements préalables d'identification des tracks
		$arr_com_to_codec = array(
			'DV'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'DVCPRO'=>array('codec'=>'DV','codec_long'=>'DVCPRO'),
			'DVCPRO 100'=>array('codec'=>'DV','codec_long'=>'DVCPRO 100'),
			'DVCPRO 50'=>array('codec'=>'DV','codec_long'=>'DVCPRO 50'),
			'DVCPRO HD'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD'),
			'IMX 30'=>array('codec'=>'IMX','codec_long'=>'IMX 30'),
			'IMX 40'=>array('codec'=>'IMX','codec_long'=>'IMX 40'),
			'IMX 50'=>array('codec'=>'IMX','codec_long'=>'IMX 50'),
			'XDCAM EX'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM EX'),
			'XDCAM HD'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD'),
			'XDCAM HD422'=>array('codec'=>'XDCAM','codec_long'=>'XDCAM HD422')
		);
		
		// liste des codecs en fonction des formats (cf Référentiel FormatsV1 (nas public, projets, démo, annexes)) 
		$arr_fmt_to_codec = array(
			'AVC'=>array('codec'=>'H264','codec_long'=>'H264'),
			'MPEG-4 Visual'=>array('codec'=>'MPEG-4','codec_long'=>'MPEG-4 Visual'),
			'RGB'=>array('codec'=>'RGB','codec_long'=>'Bitmap RGB'),
			'VC-3'=>array('codec'=>'DNxHD','codec_long'=>'DNxHD'),
			'DV'=>array('codec'=>'DV','codec_long'=>'DV Video'),
			'DVCPRO'=>array('codec'=>'DV','codec_long'=>'DVCPRO'),
			'DVCPRO 100'=>array('codec'=>'DV','codec_long'=>'DVCPRO 100'),
			'DVCPRO 50'=>array('codec'=>'DV','codec_long'=>'DVCPRO 50'),
			'DVCPRO HD'=>array('codec'=>'DV','codec_long'=>'DVCPRO HD'),
			'DPX'=>array('codec'=>'DPX','codec_long'=>'DPX'),
			'JPEG 2000'=>array('codec'=>'JPEG 2000','codec_long'=>'JPEG 2000')
		);
		//cas format == 'MPEG Video' => du coup format n'est pas inclus dans arr_fmt_to_codec
		$arr_fmt_version_to_codec = array(
			'Version 1'=>array('codec'=>'MPEG-1','codec_long'=>'MPEG-1'),
			'Version 2'=>array('codec'=>'MPEG-2','codec_long'=>'MPEG-2'),
		);
		
		if(isset($track_video)&& !empty($track_video)){
			if(isset($track_video['codec_id']) && isset($arr_id_to_codec[strtolower(trim($track_video['codec_id']))])){
				$codec = $arr_id_to_codec[strtolower(trim($track_video['codec_id']))];
				// trace("get codec from codec_id");
			}else if (isset($track_video['format_long']) && isset($arr_com_to_codec[$track_video['format_long']])){
				//Au cours des traitements préalables, commercial_name est rangé dans format_long, cf specs 7.2.4, Référentiel FormatsV1 (nas public, projets, démo, annexes)
				$codec = $arr_com_to_codec[$track_video['format_long']];
				// trace("get codec from format_long");
			}else if (isset($track_video['format']) && isset($arr_fmt_to_codec[$track_video['format']])){
				$codec = $arr_fmt_to_codec[$track_video['format']];
				// trace("get codec from format");
			}else if (isset($track_video['format']) && isset($track_video['format_version'])
				&& $track_video['format'] == 'MPEG Video' && isset($arr_fmt_version_to_codec[$track_video['format_version']])){
				$codec = $arr_fmt_version_to_codec[$track_video['format_version']];
				// trace("get codec from format_version");
			}
		}
		
		if(empty($codec)){
			$codec = array("codec"=>"Unknown Video Format","codec_long"=>"Unknown Video Format");
			// trace("codec unknown");
		}
		return $codec;
	}
	
	private static function getEnvVideo($track_general,$ext = null ){
		// détermination de l'enveloppe vidéo depuis le codec_id, ou le format, ou l'extension
		// trace("getEnvVideo");
		// trace("track_general : ".print_r($track_general,true));
		$env = "" ;
		$arr_id_to_env = array(
			'qt'=>'MOV',
			'M4V'=>'MP4',
			'mp42'=>'MP4',
			'isom'=>'MP4'
		);
		
		$arr_fmt_to_env = array(
			'MXF'=> 'MXF',
			'MPEG-PS'=> 'MPEG',
			'MPEG-TS'=> 'MPEG-TS',
			'QuickTime'=> 'MOV',
			'WebM'=> 'WebM',
			'AVI'=> 'AVI',
			'Windows Media'=> 'WMV',
			'BDAV'=> 'Blu-ray',
			'Flash Video'=> 'Flash',
			'RealMedia'=> 'Real',
			'Matroska'=> 'MKV',
			'DivX'=> 'DivX',
			'HLS'=> 'HLS',
			'DPX'=> 'DPX'
		);
		
		$arr_ext_to_env = array(
			'MOV'=> 'MOV',
			'MP4'=> 'MP4',
			'M4V'=> 'MP4',
			'MXF'=> 'MXF',
			'MPG'=> 'MPEG',
			'MP2'=> 'MPEG',
			'VOB'=> 'MPEG',
			'TS'=> 'MPEG-TS',
			'WEBM'=> 'WebM',
			'AVI'=> 'AVI',
			'WMV'=> 'WMV',
			'FLV'=> 'Flash',
			'RM'=> 'Real',
			'MKV'=> 'MKV',
			"DIVX" => 'DivX',
			"M3U8" => 'HLS',
			"DPX" => 'DPX'
		);
		
		if(isset($track_general) && !empty($track_general)){
			if(isset($track_general['codec_id']) && isset($arr_id_to_env[$track_general['codec_id']])){
				$env = $arr_id_to_env[$track_general['codec_id']];
				// trace("get env from codec_id");
			}else if (isset($track_general['format']) && isset($arr_fmt_to_env[$track_general['format']])){
				$env = $arr_fmt_to_env[$track_general['format']];
				// trace("get env from format");
			}
		}
		
		if (empty($env) && isset($ext) && !empty($ext) && isset($arr_ext_to_env[strtoupper($ext)])){
			$env = $arr_ext_to_env[strtoupper($ext)];
			// trace("get env from ext");
		}
	
		if(empty($env)){
			// trace("env unknwon");
			$env = "Unknown File Format";
		}
		return $env ; 
	}
	
	private static function getDefAudio($track_audio){
		$def = "" ; 
		$arr_channels_to_def = array(
			'1'=>'Mono',
			'2'=>'Stereo',
			'6'=>'5.1'
		);
		
		// trace("getDefaudio track_audio : ".print_r($track_audio,true));
		if(isset($track_audio['channels']) && !empty($track_audio['channels'])){
			$channels = $track_audio['channels'];
			if(strpos($channels,' ')!==false){
				$channels = explode(' ',$channels);
				$channels = $channels[0];
			}
			if(isset($arr_channels_to_def[$channels]) && !empty($arr_channels_to_def[$channels])){
				$def = $arr_channels_to_def[$channels];
			}
		}
		
		if(empty($def)){
			$def = "Unknown Layout" ;
		}
		return $def ; 
	}
	
	
	private static function getFormatImage($info_img,$ext=null){
		$codec = array() ; 
		// trace("getFormatImage info_img : ".print_r($info_img,true)."\next :".$ext);
		$arr_fmt_to_codec = array(
			'BMP'=>array('codec'=>'BMP','codec_long'=>'BMP (Microsoft Windows bitmap image)'),
			'CR2'=>array('codec'=>'RAW','codec_long'=>'CR2 (Canon Digital Camera Raw Image Format)'),
            'DNG'=>array('codec'=>'RAW','codec_long'=>'DNG (Digital Negative)'),
            'GIF'=>array('codec'=>'GIF','codec_long'=>'GIF (CompuServe graphics interchange format)'),
			'JP2'=>array('codec'=>'JPEG-2000','codec_long'=>'JP2 (JPEG-2000 File Format Syntax)'),
			'JPEG'=>array('codec'=>'JPEG','codec_long'=>'JPEG (Joint Photographic Experts Group JFIF format)'),
			'PCX'=>array('codec'=>'Paintbrush','codec_long'=>'PCX (ZSoft IBM PC Paintbrush)'),
			'PDF'=>array('codec'=>'PDF','codec_long'=>'PDF (Portable Document Format)'),
			'PICT'=>array('codec'=>'PICT','codec_long'=>'PICT (Apple Macintosh QuickDraw/PICT)'),
			'PNG'=>array('codec'=>'PNG','codec_long'=>'PNG (Portable Network Graphics)'),
			'PS'=>array('codec'=>'PostScript','codec_long'=>'PS (PostScript)'),
			'PSD'=>array('codec'=>'Photoshop','codec_long'=>'PSD (Adobe Photoshop bitmap)'),
			'TGA'=>array('codec'=>'TGA','codec_long'=>'TGA (Truevision Targa image)'),
			'TIFF'=>array('codec'=>'TIFF','codec_long'=>'TIFF (Tagged Image File Format)')
		);
		$arr_ext_to_codec = array(
			'CR2'=>array('codec'=>'RAW','codec_long'=>'CR2 (Canon Digital Camera Raw Image Format)'),
            'DNG'=>array('codec'=>'RAW','codec_long'=>'DNG (Digital Negative)'),
            'JPG'=>array('codec'=>'JPEG','codec_long'=>'JPEG (Joint Photographic Experts Group JFIF format)'),
			'JPEG'=>array('codec'=>'JPEG','codec_long'=>'JPEG (Joint Photographic Experts Group JFIF format)'),
			'BMP'=>array('codec'=>'BMP','codec_long'=>'BMP (Microsoft Windows bitmap image)'),
			'EPS'=>array('codec'=>'PostScript','codec_long'=>'PostScript'),
			'GIF'=>array('codec'=>'GIF','codec_long'=>'GIF (CompuServe graphics interchange format)'),
			'JPF'=>array('codec'=>'JPEG-2000','codec_long'=>'JP2 (JPEG-2000 File Format Syntax)'),
			'TIF'=>array('codec'=>'TIFF','codec_long'=>'TIFF (Tagged Image File Format)'),
			'TIFF'=>array('codec'=>'TIFF','codec_long'=>'TIFF (Tagged Image File Format)'),
			'PCX'=>array('codec'=>'Paintbrush','codec_long'=>'PCX (ZSoft IBM PC Paintbrush)'),
			'PDF'=>array('codec'=>'PDF','codec_long'=>'PDF (Portable Document Format)'),
			'PNG'=>array('codec'=>'PNG','codec_long'=>'PNG (Portable Network Graphics)'),
			'TGA'=>array('codec'=>'TGA','codec_long'=>'TGA (Truevision Targa image)'),
			'AI'=>array('codec'=>'PostScript','codec_long'=>'Adobe Illustrator PostScript'),
			'PCT'=>array('codec'=>'PICT','codec_long'=>'PICT (Apple Macintosh QuickDraw/PICT)')
		);
		if(isset($info_img)&& !empty($info_img) && isset($info_img['format']) && isset($arr_fmt_to_codec[strtoupper(trim($info_img['format']))])){
			$codec = $arr_fmt_to_codec[strtoupper(trim($info_img['format']))];
			// trace("get codec from format");
		}
		if(empty($codec) && !empty($ext) && isset($arr_ext_to_codec[strtoupper(trim($ext))])){
			$codec = $arr_ext_to_codec[strtoupper(trim($ext))]; 
		}
		
		if(empty($codec)){
			$codec = array("codec"=>"Unknown Image Format","codec_long"=>"Unknown Image Format");
			// trace("codec unknown");
		}
		return $codec ; 
	}
	
	
	private static function getFormatDocument($file_info,$ext=null){
		// trace("getFormatDocument file_info ".print_r($file_info,true)."\next:".$ext); 
		// traitement de la chaine fileinfo : 
		preg_match("/([\S \(\)-]+?)(?:,|$)/i",$file_info,$match);
		if(!empty($match) && !empty($match[1])){
			$format_finfo = $match[1];
		}
		preg_match("/Name of Creating Application:([\S \(\)-]+?)(?:,|$)/i",$file_info,$match);
		if(!empty($match) && !empty($match[1])){
			$name_creating_app = $match[1];
		}
		
		// trace("macth format_finfo : ".$format_finfo);
		// trace("macth name_creating_app : ".$name_creating_app);
		
		
		$codec = array();
		$arr_file1_to_format = array(
			'ASCII text'=>array('codec'=>'TEXT','codec_long'=>'ASCII Text'),
			'IFF data'=>array('codec'=>'TEXT','codec_long'=>'IFF Text (Interchange File Format)'),
			'Motorola Quark Express Document (English)'=>array('codec'=>'XPRESS','codec_long'=>'QuarkXPress Document'),
			'OpenDocument Presentation'=>array('codec'=>'OpenOffice Presentation','codec_long'=>'OpenOffice Presentation'),
			'OpenDocument Spreadsheet'=>array('codec'=>'OpenOffice Spreadsheet','codec_long'=>'OpenOffice Spreadsheet'),
			'OpenDocument Text'=>array('codec'=>'OpenOffice Text','codec_long'=>'OpenOffice Text'),
			'PDF document'=>array('codec'=>'PDF','codec_long'=>'PDF (Portable Document Format)'),
			'Rich Text Format data'=>array('codec'=>'RTF','codec_long'=>'RTF (Rich Text Format)'),
			'UTF-8 Unicode text'=>array('codec'=>'TEXT','codec_long'=>'UTF-8 Text')
			// 'CDF V2 Document'=>array('codec'=>'Tester "Name of Creating Application"','codec_long'=>'')
		);
		
		$arr_name_of_app_to_format = array(
			'Microsoft Excel'=>array('codec'=>'EXCEL','codec_long'=>'Microsoft Excel'),
			'Microsoft Macintosh Excel'=>array('codec'=>'EXCEL','codec_long'=>'Microsoft Macintosh Excel'),
			'Microsoft Macintosh PowerPoint'=>array('codec'=>'POWERPOINT','codec_long'=>'Microsoft Macintosh PowerPoint'),
			'Microsoft Macintosh Word'=>array('codec'=>'WORD','codec_long'=>'Microsoft Macintosh Word'),
			'Microsoft Office Excel'=>array('codec'=>'EXCEL','codec_long'=>'Microsoft Excel'),
			'Microsoft Office PowerPoint'=>array('codec'=>'POWERPOINT','codec_long'=>'Microsoft PowePoint'),
			'Microsoft Office Word'=>array('codec'=>'WORD','codec_long'=>'Microsoft Word'),
			'Microsoft PowerPoint'=>array('codec'=>'POWERPOINT','codec_long'=>'Microsoft PowePoint'),
			'Microsoft Word'=>array('codec'=>'WORD','codec_long'=>'Microsoft Word')
		);
		$arr_ext_to_codec = array(
			'CSV'=>array('codec'=>'TEXT','codec_long'=>'TEXT CSV'),
			'DOC'=>array('codec'=>'WORD','codec_long'=>'Microsoft Word'),
			'DOCX'=>array('codec'=>'WORD','codec_long'=>'Microsoft Word XML'),
			'ODP'=>array('codec'=>'OpenOffice Presentation','codec_long'=>'OpenOffice Presentation'),
			'ODS'=>array('codec'=>'OpenOffice Spreadsheet','codec_long'=>'OpenOffice Spreadsheet'),
			'ODT'=>array('codec'=>'OpenOffice Text','codec_long'=>'OpenOffice Text'),
			'PDF'=>array('codec'=>'PDF','codec_long'=>'PDF (Portable Document Format)'),
			'PPT'=>array('codec'=>'POWERPOINT','codec_long'=>'Microsoft PowePoint'),
			'PPTX'=>array('codec'=>'POWERPOINT','codec_long'=>'Microsoft PowePoint XML'),
			'QXP'=>array('codec'=>'XPRESS','codec_long'=>'QuarkXPress Document'),
			'RTF'=>array('codec'=>'RTF','codec_long'=>'RTF (Rich Text Format)'),
			'RTFD'=>array('codec'=>'RTFD','codec_long'=>'RTFD (Rich Text Format Directory)'),
			'TEXT'=>array('codec'=>'TEXT','codec_long'=>'TEXT'),
		    'SRT'=>array('codec'=>'SRT','codec_long'=>'SRT'),
		    'VTT'=>array('codec'=>'VTT','codec_long'=>'WebVTT'),
		    'XLS'=>array('codec'=>'EXCEL','codec_long'=>'Microsoft Excel'),
			'XLSX'=>array('codec'=>'EXCEL','codec_long'=>'Microsoft Excel XML'),
			'ZIP'=>array('codec'=>'ZIP','codec_long'=>'ZIP Archive')
		);
		if(isset($file_info)&& !empty($file_info) && isset($format_finfo) && isset($arr_file1_to_format[trim($format_finfo)])){
			$codec = $arr_file1_to_format[trim($format_finfo)];
			// trace("get codec from arr_file1_to_format $format_finfo".print_r($codec,true));
		}else if(isset($file_info)&& !empty($file_info) && isset($format_finfo) && isset($name_creating_app)
			&& ($format_finfo == 'CDF V2 Document' || $format_finfo == 'Composite Document File V2 Document')
			&& isset($arr_name_of_app_to_format[trim($name_creating_app)])){
			$codec = $arr_name_of_app_to_format[trim($name_creating_app)];
			// trace("get codec from arr_name_of_app_to_format ".print_r($codec,true));
		}
		if((in_array($ext,array('vtt','srt')) || empty($codec)) && !empty($ext) && isset($arr_ext_to_codec[strtoupper(trim($ext))])){
			$codec = $arr_ext_to_codec[strtoupper(trim($ext))]; 
			// trace("get codec from ext ".print_r($codec,true));
		}
		
		if(empty($codec)){
			$codec = array("codec"=>"Unknown File Format","codec_long"=>"Unknown File Format");
			// trace("codec unknown");
		}
		return $codec;
	}
}

?>
