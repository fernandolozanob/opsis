<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link type="text/css" href="<?=libUrl?>webComponents/tinymce/skins/lightgray/skin.min.css" rel="stylesheet" />	
<style type="text/css">

body .mce-abs-layout-item, .mce-abs-end {
    position: relative;
}
.tab
{
	border: 1px solid #CECECE;
}

 .warning {
	border: 1px solid;
	margin: 10px 0px;
	padding:15px 10px 15px 50px;
	background-repeat: no-repeat;
	background-position: 10px center;
	color: #9F6000;
	background-color: #FEEFB3;
	background-image: url('<?=libUrl?>webComponents/tinymce/skins/lightgray/img/warning.png');
}
.entete {

	background-color: #F0F0F0;
	background-image: linear-gradient(to bottom, #FDFDFD, #DDDDDD);
	background-repeat: repeat-x;
	border-bottom: 1px solid #9E9E9E;
	overflow: hidden;
}


 



<!--
        #uploadFrame { display: none; }
//-->
</style>

</head>

<script type='text/javascript'>

//quand on choisit une image on envoie son url � la page parente et on ferme le pop up
function submit_url(URL) {
window.parent.document.getElementById('<?php echo trim($_REQUEST['field']); ?>').value = URL;
//ferme le pop up !!! ! ! ! !!
top.tinymce.activeEditor.windowManager.close();
}

function delete_file(URL,field) {
			var answer = confirm("Delete file?");
			if (answer){
		  	document.location.href='empty.php?urlaction=upload&deletefile='+URL+'&field='+field+'&type=<?php echo $_REQUEST['type'];?>&currentFolder=<?php echo $_REQUEST['currentFolder'];?>';
			} 
		}
		
function delete_rep(URL,field) {
			var answer = confirm("Attention, supprimer le dossier et son contenu?");
			if (answer){
		  	document.location.href='empty.php?urlaction=upload&deletefolder='+URL+'&field='+field+'&type=<?php echo $_REQUEST['type'];?>&currentFolder=<?php echo $_REQUEST['currentFolder'];?>';
			} 
		}
		
function create_folder() {
			var name=prompt("Nom du repertoire","Nom du repertoire");
			if (name!=null && name!=""){
				document.location.href='empty.php?urlaction=upload&createfolder='+name+'&field=<?php echo $_REQUEST['field']; ?>&currentFolder=<?php echo $_REQUEST['currentFolder'];?>&type=<?php echo $_REQUEST['type'];?>';
			}
		}		
</script>


<?php



 $error = '';
$filename = NULL;
$path_upload=kTinymceUploadPath.'/';
$relative_path=kTinymceUploadPathRelativ;
$maxsize=2097152;

//extension autoris�es selon le type
if($_REQUEST['type']=='image')
$allowed_types = array("jpeg","png","gif","bmp","jpg");
if($_REQUEST['type']=='media')
$allowed_types = array("avi");
if($_REQUEST['type']=='file')
$allowed_types = array("txt");

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
///////////////// UPLOAD /////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

if(isset($_FILES['uploadFile']) && $_FILES['uploadFile']['error'] == 0) {
//si le repertoire change
	if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '')
		{
		$targetpath = $path_upload.$_REQUEST['currentFolder'].'/'.$_FILES['uploadFile']['name'];
		}
	else{
		 $targetpath =$path_upload.$_FILES['uploadFile']['name'];
	}
	if ($_FILES['uploadFile']['size'] > $maxsize) $error = "Le fichier est trop gros";
	$filename   = $_FILES['uploadFile']['name'];
	$extension_upload = strtolower(  substr(  strrchr($_FILES['uploadFile']['name'], '.')  ,1)  );
	if (!in_array($extension_upload,$allowed_types)) $error ="Le fichier n a pas la bonne extension";
	

	
	if($error == ''){
	if(@move_uploaded_file($_FILES['uploadFile']['tmp_name'], $targetpath)) {
		 $error = ''; 
	  } 
	else {
		 $error = "L'upload a échoué !"; 
		} 
	}
		
//redirection apres upload
if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '') {		
?>
<script>
document.location.href='empty.php?urlaction=upload&currentFolder&currentFolder=<?php echo $_REQUEST['currentFolder']; ?> &field=<?php echo $_REQUEST['field']; ?>&type=<?php echo $_REQUEST['type']; ?>&error=<?php echo $error; ?>';
</script>
<?php				
}
else 
?>
<script>
document.location.href='empty.php?urlaction=upload&field=<?php echo $_REQUEST['field']; ?>&type=<?php echo $_REQUEST['type']; ?>&error=<?php echo $error; ?>';
</script>
<?php
			} 
			
//fonction pour supprimer un repertoire et son contenu
 function sup_repertoire($chemin) {

        // v�rifie si le nom du repertoire contient "/" � la fin
        if ($chemin[strlen($chemin)-1] != '/') // place le pointeur en fin d'url
           { $chemin .= '/'; } // rajoute '/'

        if (is_dir($chemin)) {
             $sq = opendir($chemin); // lecture
             while ($f = readdir($sq)) {
             if ($f != '.' && $f != '..')
             {
             $fichier = $chemin.$f; // chemin fichier
             if (is_dir($fichier))
             {sup_repertoire($fichier);} // rapel la fonction de mani�re r�cursive
             else
             {unlink($fichier);} // sup le fichier
             }
                }
                closedir($sq);
                rmdir($chemin); // sup le r�pertoire
                             }
        else {
                unlink($chemin);  // sup le fichier
             }
       }
		

//fonction pour pour corriger le nom de dossier	
function format_filename($filename) {
	$bads = array(' ','a','c','e','g','i','k','l','n','r','š','u','ž','A','C','E','G','I','K','L','N','R','Š','U','Ž','$','&','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','??','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','??','?','?','?','?','?','?');
	$good = array('-','a','c','e','g','i','k','l','n','r','s','u','z','A','C','E','G','I','K','L','N','R','S','U','Z','s','and','A','B','V','G','D','E','J','Z','Z','I','J','K','L','M','N','O','P','R','S','T','U','F','H','C','C','S','S','T','T','E','Ju','Ja','a','b','v','g','d','e','e','z','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','c','c','s','t','t','y','z','e','ju','ja');
	$filename = str_replace($bads,$good,trim($filename));
	$allowed = "/[^a-z0-9\\.\\-\\_\\\\]/i";
	$filename = preg_replace($allowed,'',$filename);
	return $filename;
}
		
 	//remove unnecessary files
	if(isset($_REQUEST['deletefile'])) {
		if(!file_exists($_REQUEST['deletefile'] )) {
			$erreur =  'le fichier existe pas';
		} else {
			if(unlink($_REQUEST['deletefile'] )) {
				$erreur =  '';
			} else {
				$erreur =  'Echec de la supression';
			}
		}
	}
	
	//r�cup�re l extension du fichier
function get_file_ext($file)
	{
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		return $ext;
	}

//supprime le dossier
	if(isset($_REQUEST['deletefolder'])) {
	sup_repertoire($_REQUEST['deletefolder']);
	$erreur =  '';

	}


	//cr�er r�pertoire
if(isset($_GET['createfolder'])) {
    $new_title = format_filename($_GET['createfolder']);
	if(isset($_REQUEST['currentFolder'])){
		$chemin=$path_upload.$_REQUEST['currentFolder'];
		}
	else $chemin = 	$path_upload;
	  if(!is_dir($chemin.$_REQUEST['createfolder'])) {
			if(mkdir($chemin.$_REQUEST['createfolder'], 0777)) {
				$erreur = '';
			} else {
				$erreur = 'Erreur lors de la création du répertoire';
			}
		} else {
			$erreur = 'Le dossier existe déjà';
		}
	}

?>
 



 
<div id="uploadDiv" style="padding: 4px;">
        <form id="uploadForm" enctype="multipart/form-data"  action="empty.php?urlaction=upload"   method="post">

                <div class="center">
						<input type="button" class= "mce-widget mce-btn mce-last mce-abs-layout-item"  style=" height: 28px;" value="Creer repertoire" onclick="create_folder();">
						<?php 
						if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '')
							echo '<input type="hidden"  name="currentFolder"  value="'.$_REQUEST['currentFolder'].'">';
						?> 		
						<input type="hidden"  name="field"  value="<?php if(isset($_REQUEST['field'])) echo $_REQUEST['field'];?>">
						<input type="hidden"  name="type"  value="<?php if(isset($_REQUEST['type'])) echo $_REQUEST['type'];?>">	
	                    <input id="uploadFile" class= "mce-widget mce-btn mce-last mce-abs-layout-item" style=" height: 28px;" name="uploadFile" type="file" />						
                        <input id="uploadSubmit" class="mce-widget mce-btn mce-primary mce-first mce-abs-layout-item"  style=" height: 28px;" type="submit" value="Upload" />
					
                </div>
				
				<?php if(isset($_REQUEST['error']) && $_REQUEST['error']!='' ) {echo '<br><div class="warning" style="width:250px;margin-left:150px;">'.$_REQUEST['error'].'</div>';}
				if(isset($erreur) && $erreur !='' ) {echo '<br><div class="warning" style="width:250px;margin-left:150px";>'.$erreur.'</div>';}
				
				?>

        </form>
</div>




 <?php
// include(libDir."webComponents/tinymce/upload/importFolder.php");
 ?>
</body>
</html>

<?php

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
/////////////////////// ARBORESCENCE FICHIER ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '')
	$relative_path=$relative_path.'/'.$_REQUEST['currentFolder'];
		
$import_dir=$path_upload;
$url_import_folder='';

if (isset($_REQUEST['currentFolder']) && !empty($_REQUEST['currentFolder']))
{
	$url_path=str_replace('../','',$_REQUEST['currentFolder']);
	$import_dir=trim($import_dir.$url_path);
}
$dh=opendir($import_dir);
if ($dh!=false)
{

	$nb_files=0;
	$fichiers=array();
	$repertoires=array();
// VP 10/10/12 : on ne prend pas en compte les fichiers commen�ant par '.'
	while (($file=readdir($dh))!==false)
	{
		//if ($file!='.' && $file!='..' && is_file($import_dir.$file))
		if ($file[0]!='.' && is_file($import_dir.$file))
		{
			$nb_files++;
			$fichiers[]=$file;
			$fichier_reel[]=$import_dir.$file;
		}
		//else if ($file!='.' && $file!='..' && is_dir($import_dir.$file))
		else if ($file[0]!='.' && is_dir($import_dir.$file))
		{
			$nb_files++;
			$repertoires[]=$file;
			$repertoires_reel[]=$import_dir.$file;
		}
	}

	closedir($dh);
}
else
	echo kErreurOuvertureRepertoire;

if ($nb_files==0 && (!isset($url_path) || empty($url_path)))
	echo kErreurAucunFichierDossierTrouve;
else
	// include(getSiteFile('formDir','importFolder.inc.php'));

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
/////////////////////// IMPORT FOLDER INC ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


{

?>







<span style="float:left;height:520px; width: 120px;" class='tab' >
<table>
		<?php
		
		if (!empty($url_path))
		{
			$rep_parent=dirname($url_path);
			
			if ($rep_parent=='.')
				$rep_parent='';
			
			
			echo '<tr>';

			
			if (empty($rep_parent))
				echo '<td><a href="empty.php?urlaction=upload&'.$url_import_folder.'&field='.$_REQUEST['field'].'&type='.$_REQUEST['type'].'"><img src="'.libUrl.'webComponents/tinymce/skins/lightgray/img/fleche_precedent.jpg" alt="precedent" />&nbsp;Retour</a></td>';
			else
				echo '<td><a href="empty.php?urlaction=upload&'.$url_import_folder.'&type='.$_REQUEST['type'].'&field='.$_REQUEST['field'].'&currentFolder='.urlencode(dirname($url_path).'/').'"><img src="'.libUrl.'webComponents/tinymce/skins/lightgray/img/fleche_precedent.jpg" alt="precedent" />&nbsp;Retour</a></td>';
			
			echo '</tr>';
		
		}
		
		$i=0;
		$j=0;
		$cpt=0;
		
		foreach ($repertoires as $rep)
		{
	
			$i++;
			echo '<tr>';
		//	echo '<td class="resultsCorps"><input type="checkbox" name="file_'.$i.'" /></td>';
			echo '<td>';
			echo '<span ><img src="'.designUrl.'/images/folder_magnify.png" alt="repertoire" /></span>';
			echo '<span>';
			echo '<a href="empty.php?urlaction=upload&'.$url_import_folder.'&field='.$_GET['field'].'&type='.$_REQUEST['type'].'&currentFolder='.urlencode($url_path.$rep.'/').'">'.$rep.'</a>';
			echo '</td>';
			echo '<input type="hidden" name="file_'.$i.'_path" value="'.$repertoires_reel[$j].'" />';
			echo '<input type="hidden" name="file_'.$i.'_original" value="'.basename($repertoires_reel[$j]).'" />';
			echo '</span>';
			echo '<td>';
			echo '<span><img src="'.libUrl.'webComponents/tafelTree/imgs/trash.gif" alt="effacer" onclick="delete_rep(\''.$repertoires_reel[$j].'\',\''.$_REQUEST['field'].'\')"/></span>';
			echo '</td>';echo '</tr>';
			$j++;
		}
		echo '</table></span>';
		
		?>
		
			<div  class='tab' style="float:left;width:500px;">
			<span>
			<?php echo $relative_path; ?>
			</span>
			</div>
	
		
		<?php
		$j=0;

		echo '<span class="tab" style="float:left;width:500px;height:500px;">';
		echo '<table  width="500px" cellpadding="12"  border=1 frame=void rules=rows align="center">';
		echo'<tr class="entete"><th></th><th>Nom</th><th>Taille(Ko)</th><th>Date de modification</th><th></th></tr>';
		foreach ($fichiers as $fich)
		{
		//on voit le fichier que si c est un fichier image
		$ext=get_file_ext($fich);
		if(in_array($ext, $allowed_types))
		{
			$i++;
			$cpt++;
			$size =  round(filesize($fichier_reel[$j])/1000,2);
			$date =  filemtime($fichier_reel[$j]);
			//concatene le chemin relatif et le nom
			$path= $relative_path.'/'.$fich;
			
			 echo '<tr>';
			echo '<td style="border:0px;"><img src="'.designUrl.'/images/page_white_text.png" alt="repertoire" onclick="submit_url(\''.$path.'\')"/></td>';
			 echo '<td style="border:0px;" onclick="submit_url(\''.$path.'\')" >'.$fich.'</td>';
			 echo '<td style="border:0px;text-align : center;">'.$size.'</td>';
			 echo '<td style="border:0px;">'.date ("d/m/Y H:i:s.",$date).'</td>';
			echo '<td><img src="'.libUrl.'webComponents/tafelTree/imgs/trash.gif" alt="effacer" onclick="delete_file(\''.$fichier_reel[$j].'\',\''.$_REQUEST['field'].'\')"/></td>';
			
			 echo '</tr>';
			$j++;
		}	
		else {$i++;$j++;}	
		}
		// if($cpt == 0)
			// echo '<tr><td>Il n\'y a pas de fichiers dans ce répertoire</td></tr>';

		
		echo '</table>';
		echo '</span>';


}



  ?>