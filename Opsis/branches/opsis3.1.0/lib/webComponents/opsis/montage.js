var	 _window_position_x, _window_position_y, _window_default_width, _window_default_height, _offset_width, _offset_height, _container_width=360, _offset_left, _offset_top ;
var _minimum_available_width=950, _minimum_available_height=500, _free_lateral_width=0, _free_vertical_height=0;
var myVideo,montage_myVideo,  myPanel, _video_id, _visu_type, ext_tcin, ext_tcout, image_default;
var offset_left_ref =0;
var rm_from_cart_items="";
var _use_QT = false ;
var loading;

var panHasChanged = false ;
var montageHasChanged = true ;

var currentClipExt = "";

var confirmExit = true ;
if(typeof pattern == 'undefined'){
	pattern = '_vis';
}

function dialConfirmExit(e){
	if(panHasChanged && confirmExit){
		e = e || window.event;
		if(e){
			e.returnValue = str_lang.kJSConfirmExit;
		}
		return str_lang.kJSConfirmExit;
	}
}
window.onbeforeunload = dialConfirmExit;


var def_player_options = {
	previewOverTimeline : false ,
	span_time_passed : true,
	enable_streaming : true ,
	resizeCustom : true,
	dynamicControllerFS : true,
	onTrackChange : updateHighlightClip,
	container_id : 'montageContainer',
	hauteur_track : 8,
	hauteur_bg_track: 8,
	activateAudioContext : false 
};
try{
	def_player_options['css_path']  =  ow_const.kCheminHttp+'/design/css/skin_player/' ; 
}catch(e){
	console.log("montage.js def_player_options css_path");
}


// Fonction d'info apres changement du select permettant de définir le ratio de référence du montage
function updateRatioMontage (select_ratio){
	// Si ow_main_container => implique container & donc player existe déja
	if($j("#bloc_visionneuse #ow_main_container").get(0)){
		showAlert(str_lang.kJSMontageRatioChange,'alertBox','fade',0,0,3);

	}
}

function loadPlaylist(node,titre) {
	if( $$('#visionneuse div.help_infos').length >0){
		$$('#visionneuse div.help_infos')[0].style.display = "none";
	}

	_video_id="playlist";
	idList="";
	num_pic = 1 ; 
	for (_nd=0;_nd<node.childNodes.length;_nd++) {
		if(node.childNodes[_nd].id) {
			_video_id=node.childNodes[_nd].id;
			split=_video_id.split("_");
			idDoc=split[1];
			if(node.childNodes[_nd].className.indexOf('doc_picture') != -1 ){
				idDoc = "pic"+num_pic+"_"+idDoc ;
				num_pic ++ ;
			}else if(split[0]=="Xext" && split.length>3){
				idDoc=_video_id.substring(5);
			}else if(split[0] == "Xcarton"){
				idDoc=split[0].substring(1)+split[1]+"_"+encodeURIComponent($(_video_id).down("span.clipTitre").innerHTML.replace(/,/g,"&#44;").replace(/<br\s*\/?>/mg,"\n"));
			}

			idList=idList+","+idDoc;
		}
	}
	if($j("#panBlock #atelier input[name='PANXML_MONTAGE']").length>0){
		prm_montage_xml = "&montage_xml="+encodeURIComponent($j("#panBlock #atelier input[name='PANXML_MONTAGE']").val());
	}else{
		prm_montage_xml = "" ; 
	}

	if(idList){
		idList=idList.substring(1);
		if($j('#bloc_atelier #poster').length>0){
			$j('#bloc_atelier #poster').get(0).src='design/images/wait4video.gif';
		}

		// MS copy de l'objet def_player_options
		var player_options = $j.extend({}, def_player_options);


		player_options['force_ratio_ref'] = eval(document.getElementById('formPanier').PANXML_MONTAGE_RATIO.value);

		// Mise à jour titre visionneuse
		if(document.getElementById('titre_visionneuse')){
			if(typeof titre=='undefined' || titre == '') document.getElementById('titre_visionneuse').innerHTML=str_lang.kMontage;
			else document.getElementById('titre_visionneuse').innerHTML=titre;
		}		//Préparation du fichier

		if($j("#bloc_visionneuse #ow_main_container").get(0)){
			if(montage_myVideo && montage_myVideo.destroy){
				montage_myVideo.destroy();
			}else{
				montage_myVideo = null;
			}

			if(typeof(lien_css) != "undefined" && lien_css != null 
			&& typeof lien_css.parentNode != 'undefined' && lien_css.parentNode != null ){
				lien_css.parentNode.removeChild(lien_css);
				lien_css = null ; 
			}
			// document.getElementById("container").innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
			$j("#bloc_visionneuse #montageContainer").get(0).innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
		}


		loadOraoPlayer(idList,$j("#bloc_visionneuse #montageContainer").get(0).offsetWidth,pattern,player_options,true,"playlist&decoup=false"+prm_montage_xml,'montage_myVideo');

	}
}


function downloadPlaylist(node, titre) {
	_video_id="playlist";
	idList="";
	ratio = document.getElementById('formPanier').PANXML_MONTAGE_RATIO.value;


	for (_nd=0;_nd<node.childNodes.length;_nd++) {
		if(node.childNodes[_nd].id) {
			_video_id=node.childNodes[_nd].id;
			split=_video_id.split("_");
			idDoc=split[1];
			if(split[0]=="Xext" && split.length>3){
				idDoc=_video_id.substring(5);
			}else if(split[0] == "Xcarton"){
				idDoc=split[0].substring(1)+split[1]+"_"+encodeURIComponent($(_video_id).down("span.clipTitre").innerHTML.replace(/,/g,"&#44;").replace(/<br\s*\/?>/mg,"\n"));
			}

			idList=idList+","+idDoc;
		}
	}
	if($j("#panBlock #atelier input[name='PANXML_MONTAGE']").length>0){
		prm_montage_xml = "&montage_xml="+encodeURIComponent($j("#panBlock #atelier input[name='PANXML_MONTAGE']").val());
	}else{
		prm_montage_xml = "" ; 
	}

	if(idList) {
		idList=idList.substring(1);
		// MS encodage supplémentaire du à la nature du fonctionnement de la page downloadMontage
		idList = encodeURIComponent(idList);
		if($('pan_titre').value == ''){
			filename = str_lang.kSansTitre;
		}else{
			filename = encodeURIComponent($("pan_titre").value);
		}
		if(typeof downloadWindow == "undefined" || downloadWindow.closed){
			if(montageHasChanged){
				downloadWindow = window.open("empty.php?urlaction=creaFileMontage&id="+idList+"&ratio="+ratio+"&name="+filename,'_blank',"scrollbar=no,location=no,menubar=no,width=500,height=150");
			}else{
				downloadWindow = window.open("empty.php?urlaction=creaFileMontage&id="+idList+"&ratio="+ratio+"&name="+filename+"&noGen"+prm_montage_xml,'_blank',"scrollbar=no,location=no,menubar=no,width=500,height=150");
			}
		}else{
			downloadWindow.focus();
			if(montageHasChanged){
				downloadWindow.triggerNewGen();
			}else{
				downloadWindow.downloaded=false;
				downloadWindow.triggerDownload();
			}
		}
		montageHasChanged = false ;
	}
}

function extMarkIn(){
	if(montage_myVideo){
		currentTC = montage_myVideo.GetCurrentLongTimeCode();
		tcin = montage_myVideo.GetCurrentSelectionBeginning();
		tcout = montage_myVideo.GetCurrentSelectionEnd();

		tcin = currentTC;

		if((tcout) ==  montage_myVideo.movie_options.timecode_IN || montage_myVideo.timecodeToSecs(tcout) == 0 || tcout < tcin || tcin == tcout){
			tcout = montage_myVideo.GetTCMax();
		}
		montage_myVideo.SetSelection(tcin,tcout);
	}
}
function extMarkOut(){
	if(montage_myVideo){
		currentTC = montage_myVideo.GetCurrentLongTimeCode();
		tcin = montage_myVideo.GetCurrentSelectionBeginning();
		tcout = montage_myVideo.GetCurrentSelectionEnd();

		tcout = currentTC;

		montage_myVideo.SetSelection(tcin,tcout);
	}
}


function saveExtrait(){
	//currentClipExt => id de l'élément tel que représenté dans le chutier
	splits = currentClipExt.split("_");
	ext_tc_in = montage_myVideo.GetCurrentSelectionBeginning();
	ext_tc_out = montage_myVideo.GetCurrentSelectionEnd();
	if(ext_tc_in == ext_tc_out){
		return false ;
	}

	newRow = document.getElementById(currentClipExt).cloneNode(true);
	newRow.id = "ext_"+splits[1]+"_"+ext_tc_in+"_"+ext_tc_out;

	// a partir du clone de l'élément du chutier qui représente le document qu'on est en train de redécouper en nouveaux extraits,
	// on update les valeurs qui changent (mais on laisse la vignette et l'ID_DOC puisque ils ne changent pas (la vignette devrait changer une fois que les extraits auront des vignettes représentatives))
	$(newRow).down('input[id^="id_ligne_panier$"]').value = "";
	$(newRow).down('input[id^="extrait"]').value = '1';
	$(newRow).down('input[id^="tcin"]').value = ext_tc_in;
	$(newRow).down('input[id^="tcout"]').value = ext_tc_out;

	newTitle = str_lang.kExtrait+" "+$(newRow).down('span[id^="titre"]').innerHTML ;
	$(newRow).down('input[id^="ext_titre"]').value = newTitle;
	$(newRow).down('span[id^="titre"]').innerHTML = newTitle;

	var clickHandler = function(elem){
		return function(){deleteClip(elem);};
	};

	$(newRow).down('span.delete_clip').onclick = clickHandler(newRow);

	$('bin').insertBefore(newRow,$('last_bin'));

	initBulleTexte(newRow);
	setTimeout(function(){$j(newRow).find("span.clipTitre").dblclick();},250);
	new Draggable(newRow, {ghosting:false,onDrag:dragImage, cloning : true});

	panHasChanged = true ;

}

/*
function saveMontage(showAlert_flag,callback_fn){


	if(typeof showAlert_flag == "undefined"){
		showAlert_flag = false ; 
	}

	if(document.getElementById('formPanier').id_panier.value==""){
		document.getElementById('formPanier').commande.value="CREER";
	}else{
		document.getElementById('formPanier').commande.value="SAVE";
	}
	// if(document.getElementById('formPanier').pan_titre.value=="") document.getElementById('formPanier').pan_titre.value=str_lang.kSansTitre;
	// Sauvegarde timeline
	if(document.getElementById('timeline')){
		node=document.getElementById('timeline');
		str="<track>";
		for (_nd=0;_nd<node.childNodes.length;_nd++) {
			if(clipName=node.childNodes[_nd].id){
				clipName=node.childNodes[_nd].id.substring(1);
				if(clipName.charAt(0) == 'c'){ // cas 'c'arton
					clipTitle = node.childNodes[_nd].getElementsByTagName("span")[2].innerHTML.replace(/<br\s*\/?>/mg,"\\n");
				}else{
					clipTitle = node.childNodes[_nd].getElementsByTagName("span")[2].childNodes[0].nodeValue;
				}
				str+="<clipitem><name>"+clipName+"</name><title><![CDATA["+clipTitle+"]]></title></clipitem>";
			}
		}
		str+="</track>";
		document.getElementById('formPanier').PANXML_MONTAGE.value=str;

	}
	confirmExit = false ;
	// Si on doit supprimer des éléments de la selection :
	if(rm_from_cart_items!=''){
		removeFromCart(document.getElementById('formPanier').id_panier.value,rm_from_cart_items);
	}else{
		// document.getElementById('formPanier').submit();
		submitToAjax("#formPanier",function(data){
			if(typeof callback_fn == 'function'){
				callback_fn();
			}
			if(showAlert_flag){
				showAlert(str_lang.kMontageSauvegardeConfirm,"alertBox","fade",0,0,3);
			}
		});
	}
}*/

// Sauvegarde timeline
function updateXMLMontage(){
	if(document.getElementById('timeline')){
		node=document.getElementById('timeline');
		str="<track>";
		for (_nd=0;_nd<node.childNodes.length;_nd++) {
			if(clipName=node.childNodes[_nd].id){
				clipName=node.childNodes[_nd].id.substring(1);
				if(clipName.charAt(0) == 'c'){ // cas 'c'arton
					clipTitle = node.childNodes[_nd].getElementsByTagName("span")[2].innerHTML.replace(/<br\s*\/?>/mg,"\\n");
				}else{
					clipTitle = node.childNodes[_nd].getElementsByTagName("span")[2].childNodes[0].nodeValue;
				}
				if($j(node.childNodes[_nd]).find('input.clipParams').length > 0 && $j(node.childNodes[_nd]).find('input.clipParams').val() != '' ){
					clipParams = $j(node.childNodes[_nd]).find('input.clipParams').val(); 
				}else{
					clipParams = "" ; 
				}
				if(node.childNodes[_nd].className.indexOf('doc_picture') != -1){
					mediaType_str = "<mediaType>picture</mediaType>";
					console.log("found mediaType pic");
				}else if (node.childNodes[_nd].className.indexOf('doc_video') != -1){
					mediaType_str = "<mediaType>video</mediaType>";
					console.log("found mediaType video");
				}else{
					mediaType_str = "";
				}
				
				str+="<clipitem><name>"+clipName+"</name><title><![CDATA["+clipTitle+"]]></title>"+clipParams+mediaType_str+"</clipitem>";
			}
		}
		str+="</track>";
		document.getElementById('formPanier').PANXML_MONTAGE.value=str;
	}
}

function saveMontage(showAlert_flag,callback_fn){

	if(typeof showAlert_flag == "undefined"){
		showAlert_flag = false ; 
	}

	if(document.getElementById('formPanier').id_panier.value==""){
		document.getElementById('formPanier').commande.value="CREER";
	}else{
		document.getElementById('formPanier').commande.value="SAVE";
	}
	// if(document.getElementById('formPanier').pan_titre.value=="") document.getElementById('formPanier').pan_titre.value=str_lang.kSansTitre;
	updateXMLMontage() ; 
	confirmExit = false ;
	// Si on doit supprimer des éléments de la selection :
	if(rm_from_cart_items!=''){
		removeFromCart(document.getElementById('formPanier').id_panier.value,rm_from_cart_items);
	}else{
		// document.getElementById('formPanier').submit();
		submitToAjax("#formPanier",function(data){
			if(typeof callback_fn == 'function'){
				callback_fn();
			}
			if(showAlert_flag){
				showAlert(str_lang.kMontageSauvegardeConfirm,"alertBox","fade",0,0,3);
			}
		});
	}
}

function commanderMontage(type_commande){
	// récupération des valeurs id_ligne_panier pour que le panier de commande ne contienne plus que les éléments utilisés dans le montage 
	ow_post = {
		'commande' : 'INIT_LIGNES',
		'PANXML_MONTAGE_COMMANDE' : true,
		
	}
	// Fermeture fenêtre modification des titres des extraits
	if($j("#bulle_hover").parents('.ui-dialog').length > 0 && $j("#bulle_hover").dialog('isOpen')){
		$j("#bulle_hover").dialog('close') ;
	}
	
	$j("#timelineView #timeline li").each(function(idx,elt){
		if($j(elt).attr('id') != ''  && typeof $j(elt).attr('id') != 'undefined'){
			if($j(elt).attr('id').indexOf('Xext') != -1 && $j("#binView").find("li[id='"+$j(elt).attr('id').replace('Xext','ext')+"']").length>0){
				// cas ext
				// récupération de id_ligne_panier
				id_ligne_panier  = $j("#binView").find("li[id='"+$j(elt).attr('id').replace('Xext','ext')+"']").find("input[name='t_panier_doc[][ID_LIGNE_PANIER]']").val();
				ow_post['checkbox'+id_ligne_panier] = "1";
				// console.log("id_ligne_panier : "+id_ligne_panier) ;
				
			}else if ($j(elt).attr('id').indexOf('Xmontdoc') != -1 && $j("#binView").find("li[id='"+$j(elt).attr('id').replace('Xmontdoc','montdoc')+"']").length>0){
				// cas doc simple
				id_ligne_panier = $j("#binView").find("li[id='"+$j(elt).attr('id').replace('Xmontdoc','montdoc')+"']").find("input[name='t_panier_doc[][ID_LIGNE_PANIER]']").val();
				ow_post['checkbox'+id_ligne_panier] = "1";
				// console.log("id_ligne_panier : "+id_ligne_panier) ;
			}
			// on ne s'occupe pas des cartons puisqu'ils ne sont pas représentés dans t_panier_doc 
		}
	});
	
	$j("#panBlock").removeClass('montage');
	$j("#formPanier input[name='PANXML_MONTAGE_COMMANDE']").val(true);
	$j("#formPanier").removeAttr('onsubmit');
	
	// $j("#framePanier").on('load',function(){
		// downloadCurrentCart(type_commande,ow_post);
	// });
	
	saveMontage( false, downloadCurrentCart.bind(this,type_commande,ow_post) ) ; 
}


function loadMontage() {
	loading = true ;
	droparea=document.getElementById('timeline');
	droparea.innerHTML = "";
	if(document.getElementById('formPanier').PANXML_MONTAGE){
		myXML=importXML(document.getElementById('formPanier').PANXML_MONTAGE.value);
		// var nodes=myXML.getElementsByTagName('name');
		var nodes = $j(myXML).find('clipitem');
		if(nodes.length > 0 && $$("#montage div.help_infos")){
			$$("#montage div.help_infos")[0].style.display="none";
		}
		for (u=0;u<nodes.length;u++) {
			// clipName=nodes[u].firstChild.nodeValue;
			// clipTitle=$(nodes[u]).nextSibling.childNodes[0].nodeValue;
			////alert(clipTitle);
			// split=clipName.split("_");
			split=$j(nodes[u]).find('name').get(0).firstChild.data.split("_");
			idDoc=split[1];
			if(split[0] == "carton"){
				addCarton("new",nodes[u]);
			}else{
				addTimeline("new",nodes[u]);
			}
			/*if(split[0] == "carton"){
				addCarton("new",split[1],clipTitle.replace(/\\n/g, "<br />"));
			}else if(split.length >3){
				tcin=split[2];
				tcout=split[3];
				addTimeline( idDoc,"new", tcin, tcout,clipTitle);
			} else addTimeline(idDoc,"new");*/

		}
	}
	loading = false ;
}

function deleteMontage(){
	if(!confirm(str_lang.kConfirmJSDocSuppression)){return false ;}
	document.getElementById('formPanier').commande.value="SAVE";
	document.getElementById('formPanier').PANXML_MONTAGE.value="";
	confirmExit = false;
	// document.getElementById("framePanier").onload = function(){
		// reloadMontage() ; 
	// }
	if(rm_from_cart_items!=''){
		removeFromCart(document.getElementById('formPanier').id_panier.value,rm_from_cart_items);
	}else{
		// document.getElementById('formPanier').submit();
		submitToAjax("#formPanier",function(){
			reloadMontage() ;
		});
	}
}



function deleteClip(clip){
	// clip=getSelectedItem(obj);
	if(!clip) {alert(str_lang.kJSMsgSelectClip);return;}
	clip.parentNode.removeChild(clip);
	split=clip.id.split("_");

	if(split[0]=="doc" || split[0]=="ext") {
		idClip="X"+clip.id;
		while(document.getElementById(idClip)){
			document.getElementById(idClip).parentNode.removeChild(document.getElementById(idClip));
		}
		// Si suppression du chutier => on prévoit de supprimer de la sélection si sauvegarde montage
		id_panier_doc = $(clip).down("input[id^='id_ligne_panier']").value;

		showAlert(str_lang.kJSMontageConfirmSupprClip,'alertBox','fade',0,0,3);

		if(rm_from_cart_items == ''){
			rm_from_cart_items = id_panier_doc;
		}else{
			rm_from_cart_items +="$"+id_panier_doc;
		}

	}
	panHasChanged = true ;
	montageHasChanged = true ;
	if(scrollTimeline) scrollTimeline.scroll_display_test();
}

function addClip(obj){
	clip=getSelectedItem(obj);
	if(!clip) {alert(str_lang.kJSMsgSelectClip);return;}
	dropTimeline(clip,document.getElementById("timeline"))
}


function dropTimeline( dragitem,droparea,evt){
	// test pour éviter déclenchement du droptimeline par erreur => Si la modif de position est très faible, elle n'est pas prise en compte
	if( dragitem.parentNode.id=="timeline" &&  Math.abs(offset_left_ref - dragitem.offsetLeft) < (dragitem.offsetWidth)*90/100 ){
		offset_left_ref = 0;
	}else{
		if($$("#montage div.help_infos")[0] && $$("#montage div.help_infos")[0].style.display!="none"){
			$$("#montage div.help_infos")[0].style.display="none";
		}
		split=dragitem.id.split("_");
		if(droparea.id=="timeline"){
			place = "new";
		}else if (typeof(droparea.id) != "undefined"){
			place = droparea;
			droparea = document.getElementById("timeline");
		}

		idDoc=split[1];
		if(split[0]=="montdoc" || split[0]=="ext" || split[0]=="Xmontdoc" || split[0]=="Xext" ){
			$(droparea).highlight({endcolor:"#ffffff",restorecolor:"#ffffff"});
			/*idDoc=split[1];
			if(split.length >=3){
				tcin=split[2];
				tcout=split[3];

				titre_ext = $j(dragitem).find("span.clipTitre, span[id^='titre']").text();
				addTimeline( idDoc,place, tcin, tcout,titre_ext);
			} else addTimeline(idDoc,place);
			*/
			addTimeline(place,dragitem);
		}else if (split[0] == "Xcarton"){
			// console.log(split[1]);
			// addCarton(place,split[1]);
			addCarton(place,dragitem);
		}

		if(split[0]=="Xmontdoc" || split[0]=="Xext" || split[0] == "Xcarton"){
			dragitem.parentNode.removeChild(dragitem);
		}
	}

}



function addCarton(position,node){
	var text = null ; 
	var num = null ; 
	var clip_params = null ; 
	// console.log("position : ",position,"node",node);
	if(typeof position == "undefined"){
		// if(getSelectedItem('timeline')){
			// position = getSelectedItem('timeline');
		// }else{
			position = "new";
		// }
	}
	if(typeof node != 'undefined' && node.nodeName == 'LI'){
		num = $j(node).attr('id').split('_')[1];
		// num = node ; 
		text=$j(node).find('span.clipTitre').get(0).innerHTML;
		if($j(node).find('input.clipParams').length > 0 ){
			clip_params=$j(node).find('input.clipParams').val();
		}
	}else if(typeof node != 'undefined'){
		split=$j(node).find('name').get(0).firstChild.data.split("_");
		num = split[1];
		text=$j(node).find('title').get(0).firstChild.data.replace(/\\n/g, "<br />");
		// console.log("addCarton node clip_params :",$j(node).find('clip_params'));
		if($j(node).find('clip_params').length > 0 ){
			clip_params = $j(node).find('clip_params').get(0).outerHTML;
		}
	}
	// console.log("num : "+num);
	if($$("#montage div.help_infos")[0] && $$("#montage div.help_infos")[0].style.display!="none"){
		$$("#montage div.help_infos")[0].style.display="none";
	}
	if(position == 'new' && typeof node=="undefined"){
		cartons = $j('#timeline li[id^="Xcarton"]');
		var tmp_max_num = 0 ; 
		cartons.each(function(idx,elt){
			if(typeof $j(elt).attr('id') != 'undefined' && parseInt($j(elt).attr('id').replace('Xcarton_',''),10) > tmp_max_num){
				tmp_max_num = parseInt($j(elt).attr('id').replace('Xcarton_',''),10)  ; 
			}
		});
		numCarton = tmp_max_num+1 ; 
	}else{
		numCarton= num;
	}
	// console.log("numCarton : "+numCarton);

	blankDiv=document.getElementById('clip$blank');
	var myNewRow=blankDiv.cloneNode(true);
	myNewRow.id = "Xcarton_"+numCarton;
	myNewRow.className = "carton";
	clipitem = getChildById(myNewRow,'clipItem');
	if(typeof text != "undefined" && text != null){
		$(clipitem).down('span.clipTitre').innerHTML = text;
	}else{
		if(position == 'new' && typeof node=="undefined"){
			$(clipitem).down('span.clipTitre').innerHTML = "Carton "+numCarton;
		}else{
			$(clipitem).down('span.clipTitre').innerHTML = $('Xcarton_'+num).down('span.clipTitre').innerHTML;
		}
	}
	$(clipitem).down('span.bgVignette').style.display = 'none';

	// console.log("clip_params",clip_params);
	if(clip_params != null){
		$j(clipitem).find('.clipParams').val(clip_params);
	}
	
	var clickHandler = function(elem){
		return function(){deleteClip(elem);};
	};

	$(clipitem).down('span.delete_clip').onclick=clickHandler(myNewRow);

	myNewRow.style.left="0px";
	myNewRow.style.top="0px";
	myNewRow.style.display="none";
	myNewRow.style.opacity="1.0";
	if (typeof(position) == "undefined" || position=="" || position =="new"){
		document.getElementById('timeline').appendChild(myNewRow);
	} else{
		document.getElementById('timeline').insertBefore(myNewRow,position);
	}


	new Draggable(myNewRow, {constraint : 'horizontal', ghosting : false, revert : true,
		onStart : function(dragitem,evt){
			offset_left_ref = dragitem.element.offsetLeft;
			if(dragitem.element.nextSibling){
				$(dragitem.element.nextSibling).addClassName(" noSlide");
			}
		}.bind(document),
		onEnd : function(){
			$$("#timeline li.noSlide").each(function(elt,idx){$(elt).removeClassName("noSlide")});
		}.bind(document)
	});
	Droppables.add( myNewRow, { onDrop: dropTimeline , hoverclass:'showInsertPlace', includeScrolls : true } );

	setTimeout(function(){initcarton(myNewRow)},5);
	function initcarton(row){
		selectItem(row);
		showElems('timeline');
		if(scrollTimeline) {scrollTimeline.scroll_display_test();}
		initBulleTexte(row);
		if(position == 'new' && typeof node=="undefined"){$j(row).find("span.clipTitre").dblclick()};
	}
	if(!loading){
		panHasChanged = true ;
		montageHasChanged = true ;
	}
}



function addTimeline(position,node){
	var tcin ; 
	var tcout ; 
	var split ; 
	var idDoc ; 
	
	if(typeof node != 'undefined' && node.nodeName == 'LI'){
		split=$j(node).attr('id').split('_');
	}else if(typeof node != 'undefined' && node.nodeName == 'clipitem'){
		split=$j(node).find('name').get(0).firstChild.data.split("_");
	}else{
		return false ; 
	}

	if(typeof split != 'undefined'){
		idDoc = split[1];
		if(split.length > 3 ){
			tcin=split[2];
			tcout=split[3];
		}
	}
	
	console.log("idDoc",idDoc,"tcin",tcin,"tcout",tcout);
	if(typeof idDoc != 'undefined' && idDoc){
		if(typeof tcin !='undefined' && tcin && typeof tcout !='undefined' && tcout) {
			oldId="ext_"+idDoc+"_"+tcin+"_"+tcout;
			// Recup info elt source
			src=document.getElementById(oldId);
			if(src != null ){
				try{
					var _img=src.getElementsByTagName('img')[0];
					var vignette=_img.src;
					var titre=_img.title;
				}catch(e){
					vignette='';
					titre='';
				}
				newId = "X"+oldId;
				if(typeof(titre_ext)!="undefined"){
					titre=titre_ext;
				}else{
					titre="["+str_lang.kExtrait+"] "+titre;
				}
			}else{
				panHasChanged = true ;
				montageHasChanged = true ;
				return false ; 
			}
		} else {
			idDocSrc="montdoc_"+idDoc;
			// Recup info elt source
			src=document.getElementById(idDocSrc);
			if(src != null){
				try{
					var _img=src.getElementsByTagName('img')[0];
					var vignette=_img.src;
				}catch(e){
					vignette='';
					titre='';
				}
				var titre=_img.title;
				newId="Xmontdoc_"+idDoc;
			}else{
				panHasChanged = true ;
				montageHasChanged = true ;
				return false ; 
			}
		}
		
		blankDiv=document.getElementById('clip$blank');
		var myNewRow=blankDiv.cloneNode(true);
		myNewRow.id=newId;

		console.log("addTimeline newId ",newId);

		
		clipitem = getChildById(myNewRow,'clipItem');

		$(clipitem).down('img[id^="vignette"]').src = vignette;
		$(clipitem).down('img[id^="vignette"]').title = titre;

		var clickHandler = function(elem){
			return function(){deleteClip(elem);};
		};

		$(clipitem).down('span.delete_clip').onclick=clickHandler(myNewRow);

		getChildById(clipitem,'titre$').innerHTML=titre;
		myNewRow.style.left="0px";
		myNewRow.style.top="0px";
		myNewRow.style.display="none";
		myNewRow.style.opacity="1.0";
		
		// divers moyen de déterminer le type de document qu'on est en train d'ajouter
		if(node.nodeName=="LI"){
			if($j(node).hasClass('doc_picture')){
				myNewRow.className = "doc_picture";
			}else if ($j(node).hasClass('doc_picture')){
				myNewRow.className = "doc_video";
			}
		}else if($j(node).find('mediaType').length > 0){
			myNewRow.className = "doc_"+$j(node).find('mediaType').get(0).firstChild.data ;
		}else if ($j("#formPanier #bloc_atelier #binView #"+newId.substring(1).replace(/(:)/g,'\\$1')).length>0){
			elt_ori = $j("#formPanier #bloc_atelier #binView #"+newId.substring(1).replace(/(:)/g,'\\$1')); 
			if(elt_ori.hasClass('doc_video')){
				myNewRow.className = "doc_video" ;
			}else if (elt_ori.hasClass('doc_picture')){
				myNewRow.className = "doc_picture" ;
			}
		}
		// report des clip_params (pour doc_image (cf cartons))
		if(node.nodeName == 'clipitem' && $j(node).find('clip_params').length > 0 && myNewRow.className == 'doc_picture'){
			$j(myNewRow).find('.clipParams').val($j(node).find('clip_params').get(0).outerHTML);
			$j(myNewRow).find('.clipText').html($j(node).find('clip_params print_text').get(0).firstChild.data.replace(/(\\n)/g ,'<br />').replace(/'/g,"’"));
		}
		
		
		
		if (typeof(position) == "undefined" || position=="" || position =="new"){
			document.getElementById('timeline').appendChild(myNewRow);
		} else{
			document.getElementById('timeline').insertBefore(myNewRow,position);
		}


		new Draggable(myNewRow, {constraint : 'horizontal', ghosting : false, revert : true,
			onStart : function(dragitem,evt){
				offset_left_ref = dragitem.element.offsetLeft;
				if(dragitem.element.nextSibling){
					$(dragitem.element.nextSibling).addClassName(" noSlide");
				}
			}.bind(document),
			onEnd : function(){
				$$("#timeline li.noSlide").each(function(elt,idx){$(elt).removeClassName("noSlide")});
			}.bind(document)
		});
		Droppables.add( myNewRow, { onDrop: dropTimeline , hoverclass:'showInsertPlace', includeScrolls : true } );

		setTimeout(function(){initDoc(myNewRow);},5);
		function initDoc(row){
			selectItem(row);
			showElems('timeline');
			if(scrollTimeline) {scrollTimeline.scroll_display_test();}
			initBulleTexte(row);
		}
		if(!loading){
			panHasChanged = true ;
			montageHasChanged = true ;
		}
	}
}


function setTC(inout) {
	if (montage_myVideo && _video_id!='playlist') {
		if (inout==1) montage_myVideo.StopTheVideo();
		TC=montage_myVideo.GetCurrentLongTimeCode();
		if (inout==1) ext_tcout=TC;
		else ext_tcin=TC;
		montage_myVideo.SetSelection(ext_tcin,ext_tcout);
	}
}



// Fonction de remplacement de showPage => on affiche tout les elements quoi qu'il arrive
// on rajoutera par la suite un slider permettant de naviguer dans la timeline pour éviter d'avoir à utiliser un pager
function showElems(id){
	if(document.getElementById(id)){
		myDiv=document.getElementById(id);
		items=myDiv.getElementsByTagName('li');

		// Affichage page
		for (_nd=0;_nd<items.length;_nd++) {

			items[_nd].style.display="inline-block";
		}

	}
}

// MS fonction appelée comme callback de la f° ChangeTrack du OWH_player => devrait permettre de highlight l'extrait en cours de lecture
function updateHighlightClip(idx){
	try{
		if(idx < $j("#timeline LI").length && !$$('#timeline LI ')[idx].hasClassName("selectedClip")){
			selectItem($$('#timeline LI ')[idx]);
			if(scrollTimeline.active){
				// permet d'updater le scroll pour garder l'élément en cours de lecture visible dans la représentation de la timeline
				scrollTimeline.sliderVar.setValue(($j("#timelineView li.selectedClip")[0].offsetLeft+($j("#timelineView li.selectedClip").width()/2))/$j("#timeline").width());
			}
		}
	}catch(e){}
}


function selectItem(obj){
	var tf=($(obj).hasClassName("selectedClip"));

	unselectItem(obj.parentNode);

	if(!tf) $(obj).addClassName("selectedClip");
}
function unselectItem(obj){
	if( obj == null ){return ;}
	lis=obj.getElementsByTagName('li');
	for (_u=0;_u<lis.length;_u++) {
		if($(lis[_u]).hasClassName("selectedClip")) {$(lis[_u]).removeClassName('selectedClip');}
	}
}

function getSelectedItem(obj){
	if(typeof obj !=='object' && obj!=null){
		obj = $(obj);
	}
	lis=obj.getElementsByTagName('li');
	for (_u=0;_u<lis.length;_u++) {
		if($(lis[_u]).hasClassName("selectedClip")) return lis[_u];
	}

}


// == fonctions issues montage.xsl

function dropItem( dragitem,droparea,finished){
	if(dragitem.parentNode.parentNode.parentNode.id==droparea.id) return;
	split=dragitem.id.split("_");
	idDoc=split[1];

		var branch = tree.getBranchByIdObj(droparea.id);
		if(branch) {
			idCart=branch.getId();
			add2folder(idCart,idDoc);
		}

}



function dropVideo( dragitem,droparea){
	playClip(dragitem);
}
function playClip(clip){
	if(!clip) {alert(str_lang.kJSMsgSelectClip);return;}

	if($$('#visionneuse div.help_infos').length >0){
		$$('#visionneuse div.help_infos')[0].style.display = "none";
	}
	if($j("#bloc_visionneuse #ow_main_container").get(0)){
		if(montage_myVideo && montage_myVideo.destroy){
			montage_myVideo.destroy();
		}else{
			montage_myVideo = null;
		}
		if(typeof(lien_css) != "undefined" && lien_css.parentNode != null ){
			lien_css.parentNode.removeChild(lien_css);
		}
		// document.getElementById("container").innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
		$j("#bloc_visionneuse #montageContainer").get(0).innerHTML = "<img id='poster' src='' style='cursor:pointer' width='100%' />";
	}
	var titre=getChildById(clip,'titre$').innerHTML;
	if(document.getElementById('titre_visionneuse')) document.getElementById('titre_visionneuse').innerHTML=titre;
	
	if($j(clip).hasClass('doc_picture')){
		src_vignette = $j(clip).find('a.bgVignette > img').attr('src');
		dimension = $j("#montageContainer").width() ; 
		src_vignette = src_vignette.replace(/&h=[0-9]+&w=[0-9]+/,'&h='+dimension+"&w="+dimension);
		$j("#montageContainer").html('<img  id="poster" src="'+src_vignette+'" width="100%"/>');
	}else{
		video_id = clip.id.split("_")[1];
		// MS copy de l'objet def_player_options
		var player_options = $j.extend( {}, def_player_options);

		if(clip.id.split("_")[0] == "ext" ){
			if(clip.id.split("_")[2]){
				player_options['timecode_IN'] = clip.id.split("_")[2];
			}
			if(clip.id.split("_")[3]){
				player_options['timecode_OUT'] = clip.id.split("_")[3];
			}
		}else{
			player_options['timecode_IN'] = null;
			player_options['timecode_OUT'] = null;
		}
		currentClipExt = clip.id ;
		player_options['onExtSave'] = saveExtrait;
		player_options['extsBtnsOnStart'] = true ;
		player_options['extsBtnsDisabled'] = false ;
		player_options['selectionHidden'] = false ;
		player_options['container_id'] = 'montageContainer' ;
		
		loadOraoPlayer(video_id, $j("#bloc_visionneuse #montageContainer").get(0).offsetWidth,pattern,player_options,true,"document",'montage_myVideo');
	}
}

function dropSup( dragitem,droparea){
	$(droparea).highlight();
	dragitem.parentNode.removeChild(dragitem);
	split=dragitem.id.split("_");

	if(split[0]=="doc" || split[0]=="ext") idClip="X"+dragitem.id;
	while(document.getElementById(idClip)){
		document.getElementById(idClip).parentNode.removeChild(document.getElementById(idClip));
	}
}



function removeFromCart(idCart,ids_panier_doc) {
	if(idCart && ids_panier_doc){ return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&commande=remove&typeItem=pdoc&items='+ids_panier_doc,'document.getElementById("formPanier").submit');}
}



// fonction de modification des titres des extraits via infobulle // affichage du titre // modification des cartons .
function initBulleTexte(element){
	// pour le refresh, on prevoit d'unbind ces evenements
	$j(element).unbind("dblclick");
	$j(element).dblclick(function(event){
		if($j("#bulle_hover").parents('.ui-dialog').length > 0 && $j("#bulle_hover").dialog('isOpen')){
			$j("#bulle_hover").dialog('close') ;
		}
		$j("#bulle_hover").removeClass('editMode editTitleMode carton extrait doc_picture');
		e = event ;
		// si on charge la bulle d'un document image, on active le mode d'édition sans toutefois permettre la modification du titre
		if($j(e.currentTarget).hasClass('doc_picture')){
			$j("#bulle_hover").addClass('editMode doc_picture');
			$j('#bulle_edit_title').attr('readonly','true');
		}else if($j(e.currentTarget).attr('id').indexOf("ext")!=-1 || $j(e.currentTarget).attr('id').indexOf("carton")!=-1 ){
			// si on est dans le cas d'un extrait, alors on permet la modification du titre, mais on reste sur une version limitée du dialogue
			if($j(e.currentTarget).attr('id').indexOf("ext")!=-1){
				$j("#bulle_hover").addClass('editTitleMode extrait');
				$j('#bulle_edit_title').removeAttr('readonly');
			}else if($j(e.currentTarget).attr('id').indexOf("carton")!=-1 ){
				// Si on est dans le cas d'un carton, on active le mode d'édition complet 
				$j("#bulle_hover").addClass('editMode carton');
			}
		}else{
			$j("#bulle_hover").removeClass('editMode editTitleMode carton extrait doc_picture');
			//sinon on est dans un cas d'infobulle sur un doc standard => on retire les modes d'éditions et on bloque la modification du titre
			$j('#bulle_edit_title').attr('readonly','true');
		}
		// chargement des paramètres spécifiques du clip (pour carton & doc image principalement) à partir des données clip_params => necessaire pour l'édition 
		// console.log("initBulleText on e.currentTarget",e.currentTarget);
		if($j(e.currentTarget).find('.clipParams').length>0 && $j(e.currentTarget).find('.clipParams').val() != ''){
			loadFormFromXmlField($j("#bulleEditForm"),$j(e.currentTarget).find('.clipParams'),'clip_params');
		}else{
			resetForm($j("#bulleEditForm"));
		}
		
		// récupération dans l'infobulle de la valeur courante du titre de l'extrait / document
		if($j(e.currentTarget).find('span.clipTitre').length > 0 ){
			// $j('#bulle_edit_title').val(e.currentTarget.childNodes[0].nodeValue.replace(/<br\s*\/>/mg,"\n") );
			$j('#bulle_edit_title').val($j(e.currentTarget).find('span.clipTitre').get(0).innerHTML.replace(/<br\s*\/?>/mg,"\n") );
		}else{
			$j('#bulle_edit_title').val("");
		}
		
		if($j("#bulle_hover").hasClass('carton') && $j("#bulle_hover #bulle_edit_title").val() != $j("#bulle_hover #bulle_print_text").val()){
			$j("#bulle_hover #bulle_print_text").val($j("#bulle_hover #bulle_edit_title").val());
		}
		
		var target = e.currentTarget;
		
		//on prépare l'évenement à déclencher à la fermeture de l'infobulle (valeur modifiée ou non),
		function close_bulle(event,ui){
			// si on est dans le cas d'un extrait on poursuit
			if($j(target).attr('id').indexOf("ext")!=-1 ){
				// update de la cible
				$j(target).find('span.clipTitre').get(0).innerHTML = $j('#bulle_edit_title').val();
				// update de tout les champs qui doivent rester synchrone avec le titre
				if($j(target).hasClass('fromBin')){
					// cas d'une modif sur le chutier, on update le champ caché ext_titre , on update ensuite toutes les occurences de ce clip dans la timeline
					$j(target).find('span.clipTitre').siblings('input[id^="ext_titre"]').val($j('#bulle_edit_title').val());
					$j('#X'+$j(target).attr('id').replace(/(:)/g,'\\$1')).find("span.clipTitre").text($j(target).find('span.clipTitre').get(0).innerHTML);
				}else if($j(target).attr('id').charAt(0)=="X"){
					// cas d'une modif d'un clip dans le chutier, update de l'affichage du titre dans le chutier, du champ caché ext_titre dans le chutier et des autres occurences de ce clip dans al timeline
					$j('#'+$j(target).attr('id').replace(/(:)/g,'\\$1')).find("span.clipTitre").text($j(target).find('span.clipTitre').get(0).innerHTML);
					$j('#'+$j(target).attr('id').substring(1).replace(/(:)/g,'\\$1')).find("span.clipTitre").text($j(target).find('span.clipTitre').get(0).innerHTML);
					$j('#'+$j(target).attr('id').substring(1).replace(/(:)/g,'\\$1')).find("input[id^='ext_titre']").val($j(target).find('span.clipTitre').get(0).innerHTML);
				}
			}else if ($j(target).attr('id').indexOf("carton")!=-1){
				$j(target).find('span.clipTitre').get(0).innerHTML = $j('#bulle_print_text').val().replace(/(\n)/g ,'<br />').replace(/'/g,"’");
				montageHasChanged = true ;
			}else if($j(target).hasClass('doc_picture')){
				$j(target).find('span.clipText').get(0).innerHTML = $j('#bulle_print_text').val().replace(/(\n)/g ,'<br />').replace(/'/g,"’");
				montageHasChanged = true ;
			}
			
			storeFormAsXmlField($j("#bulleEditForm"),$j(target).find('.clipParams'),'clip_params');
			panHasChanged = true ;
			updateXMLMontage() ; 
		}
		
		$j("#bulle_hover").dialog({
			width : 360, 
			height : "auto", 
			minHeight : 50, 
			closeOnEscape : true,
			draggable:true,
			close : close_bulle
		});
	});
}
function storeFormAsXmlField(form_elt,textfield,root){
	if($j(form_elt).length == 0 || $j(textfield).length == 0 ){
		// console.log("storeFormAsXmlField crash - elements not found");
		return false ; 
	}
	if (typeof root == 'undefined' || typeof root != 'string'){
		root = "xml" ; 
	}
	
	q_string = $j(form_elt).serialize();
	vars = q_string.split('&');
	xml = "" ;
	for(i = 0 ; i < vars.length ; i++ ){
		tmp = vars[i].split('=');
		xml += "<"+decodeURIComponent(tmp[0])+"><![CDATA["+decodeURIComponent(tmp[1].replace(/\+/g,' ')).replace(/\n/g,'\\n')+"]]></"+decodeURIComponent(tmp[0])+">"
	}
	$j(textfield).val("<"+root+">"+xml+"</"+root+">");
}

function resetForm(form_elt){
	// console.log("resetForm",form_elt);
	if($j(form_elt).length == 0){
		return false ; 
	}
	$j(form_elt).find('*:input:not(:input[type="button"],:input[type="submit"])').each(function(idx,elt){
		if($j(elt).get(0).nodeName == 'SELECT'){
			if($j(elt).find('option.default').length >0){
				$j(elt).val($j(elt).find('option.default').eq(0).val());
			}else{
				$j(elt).val($j(elt).find('option').eq(0).val());
			}
		}else if ($j(elt).get(0).nodeName == 'INPUT' && ($j(elt).get(0).type == 'radio' || $j(elt).get(0).type == 'checkbox')){
			$j(elt).prop('checked',false);
		}else {
			$j(elt).val('');
		}
	});
}

function loadFormFromXmlField(form_elt,textfield,root){
	if($j(form_elt).length == 0 || $j(textfield).length == 0 ){
		console.log("loadFormFromXmlField crash - elements not found");
		return false ; 
	}
	if (typeof root == 'undefined' || typeof root != 'string'){
		root = "xml" ; 
	}
	resetForm(form_elt);
	var xml = $j.parseXML($j(textfield).val());
	var elements = $j(xml).find(root).children() ; 
	for (i = 0 ; i < elements.length ; i++){
		if($j(form_elt).find('*:input[name="'+elements[i].nodeName+'"]').length > 0 ){
			form_input = $j(form_elt).find('*:input[name="'+elements[i].nodeName+'"]') ;
			value = elements[i].childNodes[0].data.replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/\\\\n/g,"\n").replace(/(\\n)/g,"\n");
			if($j(form_input).get(0).nodeName == 'SELECT'){
				// console.log("SELECT");
				$j(form_input).val(value);
			}else if ($j(form_input).get(0).nodeName == 'INPUT' && $j(form_input).get(0).type == 'checkbox' ){
				// console.log("CHECKBOX");
				for (j = 0; j < form_input.length ; j++){
					if($j(form_input).eq(j).val() == value){
						$j(form_input).eq(j).prop('checked',true);
					}
				}
			}else if ($j(form_input).get(0).nodeName == 'INPUT' && $j(form_input).get(0).type == 'radio' ){
				// console.log("RADIO");
				for (j = 0; j < form_input.length ; j++){
					if($j(form_input).eq(j).val() == value){
						$j(form_input).eq(j).prop('checked',true);
						break ; 
					}
				}
			}else{
				console.log("DEFAULT ",$j(form_input).eq(0),value);
				$j(form_input).eq(0).val(value);
			}
			$j(form_input).trigger('onchange');
		}
	}
}


function reloadMontage(){
	flag_reload_current_pan=true;
	loadPanier(id_current_panier,null,null,'montage');
}
function triggerLoadPanier(){
	setTimeout(function(){
		flag_reload_current_pan=true;
		loadPanier(id_current_panier,null,null,'montage');
	},400);
}


