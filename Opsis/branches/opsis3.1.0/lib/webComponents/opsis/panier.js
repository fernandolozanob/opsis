// Fonctions d'interactions simples avec les paniers, ex :
// ajout à un panier via les selects, création rapide d'un panier, toggle de l'affichage du formulaire de nommage rapide d'un panier,
// + fonctions des formulaires de commandes


//Fonction hide du formulaire de nommage rapide d'une nouvelle sélection (docListe & menuDoc)
function hideSauvPanier(elemListe) {
	// Si sauvPanier existe => on le cache
	if(document.getElementById('sauvPanier')) {
		myDiv=document.getElementById('sauvPanier');
		myDiv.style.display='none';

	}
	// si elemListe renseigné => remise au style display par défaut & remise du selectedIndex à 0
	if(elemListe){
		elemListe.style.display = "" ;
		elemListe.selectedIndex=0;
	}
}

// Fonction d'affichage du formulaire de nommage rapide d'une nouvelle sélection (docListe & menuDoc)
function displaySauvPanier(elemListe,id_opt) {
	// récupération de l'élément sauvPanier
	myDiv=document.getElementById('sauvPanier');

	// Si le sauvPanier n'est pas juste après le select elemListe (ce qui permet de le remplacer graphiquement par le formulaire de nommage)
	// + devrait apporter un support direct si on a plusieurs pager / #sauvPanier définis ...
	if(!$j(myDiv).prev().is(elemListe)){
		// On déplace le sauvPanier juste après l'elemListe
		$j(myDiv).remove;
		$j(myDiv).insertAfter(elemListe);
	}

	// set des event handlers en fonction de l'elemListe courant
	$j(myDiv).children(".cancel")[0].onclick = function(){hideSauvPanier(elemListe);return false;};
	$j(myDiv).children(".valid")[0].onclick =  function(){add2cart(elemListe,id_opt);return false;};
	$j(myDiv).children("#newCartName")[0].onkeypress =  function(e){ if(e.keyCode==13){add2cart(elemListe,id_opt);}};


	// reset du champ newCartName
	document.getElementById('newCartName').value="";
	// affichage de l'élément sauvPanier & hide de l'elemListe
	myDiv.style.display='';
	elemListe.style.display = "none";

}

// Fonction d'ajout au panier/affichage du formulaire de nommage du panier dans le cas de la création d'un nouveau panier (docListe & menuDoc)
function add2cart(elem,id_opt)
{
	newCartName='';
	// id du panier ou ajouter
	if(typeof(elem)!='undefined'){
		idCart=elem.value;

		// Si elem défini (on est pas dans le cas d'une création de nouvelle sélection)
		//& que l'élément sauvPanier n'est pas juste après la liste & qu'il est affiché,
		// ==> on a agi sur un autre select d'ajout au panier => on click sur le bouton cancel du formulaire courant de nommage de sélection
		if(elem && $j("#sauvPanier")[0] && !$j("#sauvPanier").prev().is(elem) && $j("#sauvPanier")[0].style.display !='none'){
			$j("#sauvPanier").children('.cancel').click();
		}

	}else{
		idCart = -1;
	}

	// cas ajout vers nouveau panier
	if(idCart==-1 && document.getElementById("sauvPanier")){
		// Si sauvPanier pas déja affiché => première étape : affichage du formulaire de nommage et return (=> attente de l'input de l'utilisateur pour le nom du panier)
		if(document.getElementById('sauvPanier').style.display=="none"){
			displaySauvPanier(elem,id_opt);
			return;
		// sauvPanier déja affiché => l'input newCartName contient le nom rempli par l'utilisateur => on l'utilisera dans l'appel à processPanier pour créer le nouveau panier
		}else {
			newCartName=document.getElementById('newCartName').value;
		}
	}
	hideSauvPanier(elem);

	items2add='';
	// récupération des docs à ajouter au panier (cas docListes)(=> checkboxs)
	if (typeof(id_opt)=='undefined') {
		var form = null;
		if(typeof document.documentSelection != "undefined"){
			form = document.documentSelection;
		}else{
			form = document.form1;
		}

		checkedItems=form.getElementsByTagName('input');

		for (i=0;i<checkedItems.length;i++) {
			if(checkedItems[i].type=='checkbox' && checkedItems[i].checked==true && checkedItems[i].getAttribute('name') == 'checkAll'){
				check_all_elt = checkedItems[i];
			}
			if (checkedItems[i].type=='checkbox' && checkedItems[i].checked==true && checkedItems[i].getAttribute('name') != 'checkAll') {
				items2add+=checkedItems[i].value+'$';
				checkedItems[i].checked=false; //on retire la checkbox au fur et à mesure
			}
		}
		if(typeof check_all_elt != "undefined"){
			check_all_elt.checked = false ;
		}

	// cas id_opt fourni => contient un id_doc => ajout direct de cette valeur (cas menuDoc)
	} else if (id_opt!='' && typeof(id_opt)!='undefined'){
		form
		items2add+=id_opt;
	}

	//appel AJAX pour ajouter les doc checkés au folder sélectionné
	if (idCart!='' && idCart!=0 && items2add!='' ) {
		if(form != null && form.action.indexOf("urlaction=panier")!= -1 ){
			return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&pan_titre='+newCartName+'&commande=add&items='+items2add+"&typeItem=pdoc",'updateCarts');
		}else{
			return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&pan_titre='+newCartName+'&commande=add&items='+items2add,'updateCarts');
		}
	}

}

// fonction de mise à jour de l'affichage des paniers => met à jour les select#cartList, select.cartList avec le nouveau panier si on reçoit des infos de création de panier
// + appel à refreshFolders  (docListe & menuDoc)
function updateCarts(str)
{
	myDom=importXML(str);
	myBalise=myDom.getElementsByTagName('msg_out');
	showAlert(myBalise[0].firstChild.nodeValue,'alertBox','fade',0,0,3); //ne PAS oublier le firstChild

	// récupération des infos du nouveau panier lorsqu'il y en a
	new_pan_id = myDom.getElementsByTagName('pan_id');
	new_pan_titre = myDom.getElementsByTagName('pan_titre');

	// si nouveau panier => ajout à la volée du panier au select cartList
	if(typeof(new_pan_id[0])!="undefined" && new_pan_id[0].firstChild && typeof(new_pan_titre[0])!="undefined"){
		// MS - ajout cas ou le cartList est séparé en optgroup (voir FFF), permet de regrouper dans un select les fonctions d'ajouts aux séléctions & aux dossiers thématiques
		if($j("select#cartList").children("optgroup").length>0){
			$j("<option value=\""+new_pan_id[0].firstChild.nodeValue+"\">"+new_pan_titre[0].firstChild.nodeValue+"</option>").insertBefore('select#cartList optgroup:eq(0) option:last-child , select.cartList optgroup:eq(0) option:last-child');
		}else{
			$j("<option value=\""+new_pan_id[0].firstChild.nodeValue+"\">"+new_pan_titre[0].firstChild.nodeValue+"</option>").insertBefore('select#cartList option:last-child , select.cartList option:last-child');
		}
	}
    // Affichage panier créé dans zone preview panier
    flag_reload_current_pan = true;
    // check if variable exist (not null AND not undefined) to not trigger javascript ERROR
    if (!(typeof new_pan_id[0] === 'undefined' || new_pan_id[0]=== null)) {
        id_current_panier=new_pan_id[0].firstChild.nodeValue;
    }

    //une fois l'opération effectuée, le select est remis dans son état par défaut "Ajouter à ..."  (pour l'instant jquery requis et effectif sur select avec classe ou id "cartList")
	$j('select#cartList option, select.cartList option').removeAttr('selected');
	$j('select#cartList option[value=""], select.cartList option[value=""], select#cartList option[value="0"], select.cartList option[value="0"]').attr('selected','selected');


	if (window.opener && window.opener.refreshFolders)
		window.opener.refreshFolders();
	else
		refreshFolders();
}

// fonction d'affichage hierarchique des paniers sous forme de folder.
// + créa du formulaire de nommage pour la création d'une nouvelle sélection (menuGauche)
function dspFolders(str) {
    var lang = ow_const['langue'].toLowerCase();
    var jsNom = {'en':'Name','fr':'Nom'};
    var newsel = '<div id="newPanLeft" class="resultsMenu" style="clear:both;display:none;">' +
	 							'<form name="formCreaSelection" onsubmit="add2folder(-1,this.cartIdItem.value,this.cartTypeItem.value,this.cartName.value);return false;">' +
								'<input name="cartTypeItem" id="cartTypeItem" type="hidden" />'+
								'<input name="cartIdItem" id="cartIdItem" type="hidden" />'+
								jsNom[lang] +'<input name="cartName" type="text" style="width:100px" />' +
					 		    '<input type="submit" value="OK"  />' +
					 		    '</form>' +
					 		    '</div>' +
	 							'<a id="link_new_selection" href="javascript:displayNewPanLeft(0, this)" style="padding-left:10px;">'+str_lang.KJSCreerSelection+'</a>';
	document.getElementById('folders').innerHTML=newsel+str;

	if (refreshDroppables)
		refreshDroppables();
}

// Fonction de rafraichissement de l'affichage hierarchique des paniers (menuGauche)
function refreshFolders(id) {
    var lang = ow_const['langue'].toUpperCase();
	if (id) {
		fromlink=document.getElementById('folder_button'+id);
		if (fromlink.className=='collapseFolder') {

			fromlink.className='expandFolder';
			fromlink.innerHTML="<img src='design/images/mini_arrow_right.gif' />";
			action='collapse';
			if (document.getElementById('folderDetail'+id)) document.getElementById('folderDetail'+id).style.display='none';
		} else {

			fromlink.className='collapseFolder';
			fromlink.innerHTML="<img src=''design/images/mini_arrow_down.gif' />";
			action='expand';
		}
	} else {action='';}
	return !sendData('GET','blank.php','urlaction=loadPanier&action='+action+'&id='+id+'&id_lang='+lang,'dspFolders');
}

// fonction d'ajout à un folder (utilisé lors de création de sel par le menugauche et les dragNdrop)
function add2folder(idCart,idItem,typeItem,cartName) {
    hideNewPanLeft();
    // MS - les sélections créées depuis cette fonction etait pan_dossier=3 >> suppression pour l'instant. (même selections dans menuGauche & select du docListe)
    if(idCart && idItem=="") {
        return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&pan_titre='+cartName,'updateCarts');
    } else if(idCart && idItem && typeItem) {
        return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idCart+'&pan_titre='+cartName+'&commande=add&typeItem='+typeItem+'&items='+idItem,'updateCarts');
    }
}

function formPan(){
		ajax_url = "empty.php?urlaction=panierPersoListe";
		$j.ajax({
			type:"POST", 
			data: $j("#panForm").serialize(), 
			url : ajax_url,
			success : function(data){
				console.log(data);
				$j("#panFrame").html(data);
			}
		});
}
// fonction qui cache le formulaire de nommage rapide d'une nouvelle sélection (menuGauche)
function hideNewPanLeft() {
    if(document.getElementById('newPanLeft')) {
        myDiv=document.getElementById('newPanLeft');
        myDiv.style.display='none';
    }
    document.getElementById('link_new_selection').style.display='block';
}

// fonction d'affichage du formulaire de nommage rapide d'une nouvelle sélection (menuGauche)
function displayNewPanLeft(id,ctrl,type) {

    myDiv=document.getElementById('newPanLeft');
    myDiv.style.display='block';
    document.formCreaSelection.cartName.focus();
    document.formCreaSelection.cartIdItem.value = id;
    document.formCreaSelection.cartTypeItem.value = type;
    y=getTop(ctrl);
    x=getLeft(ctrl);
    myDiv.style.top=(y+20)+"px";
    myDiv.style.left=x+"px";
    document.getElementById('link_new_selection').style.display='none';
}



// Fonction de lancement de la commande
// test si des checkbox du formulaire sont cochées (l'utilisateur a défini ce qu'il voulait commander),
// sinon => on les coche toutes (si pas d'action, tout les docs sont considérés pas la demande de commande)
// + soumet le form
function commander(type)
{
	var checkbox_checked=false;
	$j('input[type="checkbox"][name^="checkbox"]').each(function (idx,elt)
	{
		if ($j(elt).prop('checked')==true)
			checkbox_checked=true;
	});

	if (!checkbox_checked)
		$j('input[type="checkbox"][name^="checkbox"]').attr('checked','checked');

	document.form1.commande.value="INIT_LIGNES";
	document.form1.pan_id_type_commande.value=type;

	if (document.form1.id_panier.value!='_session')
		document.form1.action="index.php?urlaction=commande&id_panier="+document.form1.id_panier.value;
	else
		document.form1.action="index.php?urlaction=inscription&type_commande="+type;

	document.form1.submit();
}

function livraison(id_panier) {
	document.form1.commande.value="LIVRAISON";
	document.form1.action='index.php?urlaction=livraison&id_panier='+id_panier+'';
	document.form1.submit();
}

function livraisonLigne(id_panier,i) {
	myframe=document.getElementById('frame_livraison');
	myframe.src='blank.php?urlaction=livraison&noframe=1&id_panier='+id_panier+'&ligne='+i+'&commande=LIVRAISON_LIGNE';
}

function confirmSuppr() {

	if (!confirm(str_lang.kConfirmJSDocSuppression)) return false;
	document.form1.commande.value='SUP';
	document.form1.submit();

}


// fonction d'update de la présentation du panier 
// permet d'etre indépendant du style utilisé par docListe et de fonctionner sur l'intégralité des pages sur lesquelles le panierFooter existe
function pres_panier_update(layout_opts){
	$j("#panBlock .presentation #panListChooser,#panBlock .presentation #panMosChooser").toggleClass("selected");
	// panier_xsl = xsl ; 
	updateLayoutOpts(layout_opts,function(){
		flag_reload_current_pan = true ; 
		loadPanier(id_current_panier);		
	});
}
	
function loadPanier(id_pan, elt,tri,xsl,urlaction,loadPanier_callback){
	id = Math.round(Math.random()*1000);
	// console.log("CALL loadPanier , id instance : "+id+" , params : ","id_pan",id_pan,"elt",elt,"tri",tri,"xsl",xsl,"urlaction",urlaction,"id_current_panier",id_current_panier) ; 
	
	try{
		if($j("#bulle_hover").parents('.ui-dialog').length > 0 && $j("#bulle_hover").dialog('isOpen')){
			$j("#bulle_hover").dialog('close').dialog('destroy') ;
		}
	}catch(e){}
	
	if(typeof tri != "undefined" && tri!= null){
		flag_reload_current_pan = true;
	}

	if(typeof urlaction == "undefined" || urlaction == null){
		if(typeof panListeRefToLastOrder != 'undefined' && panListeRefToLastOrder != 0 && panListeRefToLastOrder == id_pan){
			urlaction = "commande";
		}else{
			urlaction = "panier";
		}
	}

	// console.log("loadPanier , id_pan : "+id_pan+"tri "+tri+" ordre "+ordre);
	if(typeof elt != "undefined" && elt != null){
		$j("ul.panListe *").removeClass("selected");
		$j(elt).addClass("selected");
	}

	
	if(urlaction == 'panier' && (typeof xsl == 'undefined' || xsl == null)){
		xsl = panier_xsl ; 
	}


	if(id_pan !='' && ( id_pan != id_current_panier || flag_reload_current_pan || flag_noupdate_current_pan_ref) ) {
		ajax_url = "empty.php?where=include&urlaction="+urlaction+"&id_panier="+id_pan+"&nbLignes=all"+((xsl!=null)?"&xsl="+xsl:"");

		if(tri){
			ajax_url += "&tri="+tri+ordre;
			ordre ++ ;
			ordre = ordre%2;
		}else{
			ajax_url+="&tri=pdoc_ordre0,id_ligne_panier1";
		}
		$j("#panFrame").addClass('loading')	;
		// console.log("loadPanier trigger ajax ");
		$j.ajax({
			url : ajax_url,
			success : function(data){
				$j("#panFrame").html(data);
				$j('.modifLot').attr("href","javascript:popupModifLot('simple.php?urlaction=modifLot&entite=panier_doc&id_panier="+id_pan+"')");
				
				if($j(".mosWrapper").length>0){
					adjustMosaique();
					$j("#panFrame .resultsMos .mos_checkbox input[type='checkbox']").change(function(){
						if($j(this).is(':checked')){
							$j(this).parents('.resultsMos').addClass('checked');
						}else{
							$j(this).parents('.resultsMos').removeClass('checked');
						}
					});
				}
				if(!flag_noupdate_current_pan_ref){
					id_current_panier = id_pan;
					updateLayoutOpts({'id_current_panier':id_current_panier });
				}
				flag_noupdate_current_pan_ref = false ; 
				updateDraggables();
				var pan_title = "";
				
				if (urlaction == "commande" && typeof panListeRefToLastOrder != 'undefined' && panListeRefToLastOrder != 0 && panListeRefToLastOrder == id_pan){
					pan_title = str_lang.kQuickOrderRefToLastCommande;
				}else if($j('#panFrame form input[name="pan_titre"]').length>0 && $j('#panFrame form input[name="pan_titre"]').val() != ''){
					pan_title = $j('#panFrame form input[name="pan_titre"]').val();
				}else if($j("#panFrame form input[name='pan_id_etat']").val()=="1" && $j("#panFrame form input[name='pan_id_type_commande']").val()=="0"){
					pan_title = str_lang.kPanier;
				}
				
				if(typeof $j("#panFrame form input[name='pan_nb_elt']").val()!="undefined" && $j("#panFrame form input[name='pan_nb_elt']").val() != '' && $j("#panFrame form input[name='pan_nb_elt']").val() == 1 ){
					nb_elts_txt="("+$j("#panFrame form input[name='pan_nb_elt']").val()+" "+str_lang.kElementPanier+")";
				}else if(typeof $j("#panFrame form input[name='pan_nb_elt']").val()!="undefined" && $j("#panFrame form input[name='pan_nb_elt']").val() != '' && $j("#panFrame form input[name='pan_nb_elt']").val() != 0 ){
					nb_elts_txt="("+$j("#panFrame form input[name='pan_nb_elt']").val()+" "+str_lang.kElementsPanier+")";
				}else if( typeof $j("#panFrame form input[name='pan_nb_elt']").val()!="undefined" && $j("#panFrame form input[name='pan_nb_elt']").val() != ''){
					nb_elts_txt="("+str_lang.kElementsPanier+")";
				}else{
					nb_elts_txt="";
				}


				$j("#panTitle").text(pan_title);
				$j("#panNbElems").text(nb_elts_txt);
				if($j("ul.panListe #panier"+id_pan).text() != pan_title){
					$j("ul.panListe #panier"+id_pan).text(pan_title);
				}


				if($j('#panier'+id_current_panier).length>0 &&  !$j('#panier'+id_current_panier).hasClass('selected')){
					$j("ul.panListe *").removeClass("selected");
					$j('#panier'+id_current_panier).addClass("selected");
				}

				if($j('#panListe option').length>0 ){ //&&  !$j('#panListe option').hasClass('selected')
					$j("#panListe option").removeAttr("selected");
					$j('#panListe option[value='+id_current_panier+']').prop("selected","selected");
				}

				$j("#panBlock div.toolbar .check_all").removeClass('checked');

				// on n'active le lecteur preview hover uniquement sur les paniers, pas sur des commande éventuelles
				if(urlaction == 'panier'){
					$j("#panFrame .resultsMos, #panFrame tr:not(.separator, .resultsHead_row)").hover(callHoverPlayer);
					$j("#panFrame .resultsMos, #panFrame tr:not(.separator, .resultsHead_row)").mouseleave(hideHoverPlayer);
				}
				if ($j(window).width()<tablet_border)
					$j("#panBoutons button.montage-btn").css('display','none');

				setTimeout(function(){
					$j("#panFrame").removeClass('loading');
				},100);

				if(typeof loadPanier_callback == 'function'){
					loadPanier_callback() ;
				}
				
				$j("#notice_pan_result").css("width", "calc(100% - 390px)");
			}
		});
		flag_reload_current_pan = false ;
	} else if (id_pan ==''){
		displayNewPanResponsiveForm(0, this);
	}
}

function refreshPan(){
	document.getElementById("formPanier").commande.value="SAVE";
	document.getElementById("formPanier").submit();
}

function retourSelFromMontage(){
	 if(($j("#panBlock").hasClass("montage") && panHasChanged && confirmExit)){
		var check1 = confirm(str_lang.kJSConfirmExit);
		try{
			if($j("#bulle_hover").parents('.ui-dialog').length > 0 && $j("#bulle_hover").dialog('isOpen')){
				$j("#bulle_hover").dialog('close').dialog('destroy') ;
			}
		}catch(e){}
		if(check1){
			confirmExit = false ;
			retourSel();
		}else{
			return false ;
		}
	 }else{
		retourSel();
	 }
}

function retourSel(){
	$j("#formPanier").attr('onsubmit','');
	$j("#formPanier").unbind('onsubmit');
	$j("#formPanier").unbind('submit');
	$j("#panBlock").removeClass('montage');
	$j("#montageContainer video").each(function(idx,elt){
		try{
			$j(elt).get(0).pause();
		}catch(e){}
	});
	veto_right_menu = false ;
	flag_reload_current_pan=true;
	loadPanier(id_current_panier);
}

function goToMontage(){
	if(typeof myVideo!='undefined' && myVideo != null && backupLaunch_myVideo != null){
		myVideo.media = null;
		myVideo = null ;
		$j("#media_space #container").html(backupLaunch_myVideo);
	}

	if($j("#menuDroite").length>0 && !$j('#menuDroite').hasClass('collapsed')){
		toggleRightPreview();
	}
	veto_right_menu = true ;
	$j("#panBlock").addClass('montage');
	flag_reload_current_pan=true;
	loadPanier(id_current_panier,null,null,'montage&dontUpdateSessStyle=1&getMats=1');
}

function saveCurrentCart(){
	$j("#formPanier input[name='commande']").val('SAVE');
	$j("#formPanier").submit();
	flag_reload_current_pan=true;
	setTimeout(function(){loadPanier(id_current_panier,null,null);refreshFolders();},500);
}

function emptyCurrentCart(){
	if ($j("form#formPanier input[name^='checkbox']:checked").length > 0) {
		if (confirm(str_lang.kConfirmDocSupprimer))
			$j("#formPanier input[name='commande']").val('SUP_LIGNES');
		else
			return;
	}
	else {
		if (confirm(str_lang.kConfirmPanierVider))
			$j("#formPanier input[name='commande']").val('SUP_ALL');
		else return;
	}
	
	$j("#formPanier").submit();
	flag_reload_current_pan=true;
	setTimeout(function(){loadPanier(id_current_panier,null,null);},500);
}

function deleteCurrentCart(){
	if (confirm(str_lang.kConfirmPanierSupprimer)){
		$j("#formPanier input[name='commande']").val('SUP');
		$j("#formPanier").submit();
		flag_reload_current_pan=true;
		id_current_panier = 'first';
		// if($j("ul.panListe li[id^='panier']").length>1){
			// toclick = $j("ul.panListe li[id^='panier'][id!='panier"+id_current_panier+"']").eq(0);
		// }else{
			// toclick = $j("ul.panListe li").eq(0);
		// }

		setTimeout(function(){refreshFolders();},500);
	}
}

function downloadCurrentCart(type,ow_post_data){
	var checkbox_checked=false;
	var array_checkbox = new Array();
	$j('#panFrame input[type="checkbox"][name^="checkbox"]').each(function (idx,elt)
	{
		if ($j(elt).prop('checked')==true){
			checkbox_checked=true;
			array_checkbox.push($j(elt).attr("name"));
		}
	});

	if (!checkbox_checked){
		$j('#panFrame input[type="checkbox"][name^="checkbox"]').attr('checked','checked');
		$j('#panFrame input[type="checkbox"][name^="checkbox"]').each(function (idx,elt){
			array_checkbox.push($j(elt).attr("name"));
		});
	}

	if(type == 5){
		commande_type = "INIT";
	}else{
		commande_type = "INIT_LIGNES";
	}
	
	data_post = {
		commande : commande_type,
		pan_id_type_commande : type,
		conserverPanier : '1'
	};
	
	if(typeof ow_post_data == 'object'){
		for (o in ow_post_data){
			data_post[o] = ow_post_data[o];
		}
	}
	
	for(i=0 ; i < array_checkbox.length;i++){
		data_post[""+array_checkbox[i]] = "1";
	}

	$j.ajax({
		url : "empty.php?where=include&urlaction=commande&id_panier="+id_current_panier+"&nbLignes=all",
		method : "POST",
		data : data_post,
		success : function(data){
			$j("#panFrame").html(data);
			id_panier_commande = $j("#panFrame").find('input[name="id_panier"]').val() ;
			if(panListeRefToLastOrder != id_panier_commande){
				panListeRefToLastOrder = id_panier_commande ; 
				updateLayoutOpts({'panListeRefToLastOrder' : id_panier_commande});
				refreshFolders() ; 
			}
		}
	});
}

function validCommandeToDownload(){

	id_commande = $j("#panFrame form input[name='id_panier']").val();

	data_post = {
		commande : "VALID",
		noSendMailAdmin : "1",
		pan_id_type_commande : $j("#panFrame form input[name='pan_id_type_commande']").val()
	};

	$j("#order_form input,#order_form textarea").each(function(idx,elt){
		if(typeof data_post[$j(elt).attr("name")] == "undefined"){
			if($j(elt).is("textarea")){
				data_post[$j(elt).attr("name")] = $j(elt).text();
			}else{
				data_post[$j(elt).attr("name")] = $j(elt).attr("value");
			}
		}
	});
	$j.ajax({
		url : "empty.php?where=include&urlaction=commande&id_panier="+id_commande+"&nbLignes=all",
		method : "POST",
		data : data_post,
		success : function(data){
			$j("#panFrame").html(data);
			id_panier_commande = $j("#panFrame").find('input[name="id_panier"]').val() ;
			if(panListeRefToLastOrder != id_panier_commande){
				panListeRefToLastOrder = id_panier_commande ; 
				updateLayoutOpts({'panListeRefToLastOrder' : id_panier_commande});
				refreshFolders() ; 
			}
		}

	});
	// refreshCommande(id_commande,data_post);

}

function refreshCommande(id_commande,data_post){
	$j.ajax({
		url : "empty.php?where=include&urlaction=commande&id_panier="+id_commande+"&nbLignes=all",
		method : "POST",
		data : data_post,
		success : function(data){
			$j("#panFrame").html(data);
		}

	});
}

function removeLine(i,type,action,elt_origin_call) {
	if((typeof elt_origin_call != 'undefined' && $j(elt_origin_call).parents('#formPanier').length > 0) || (typeof elt_origin_call == 'undefined' && document.getElementById("formPanier") != null)){
		formPan = document.getElementById("formPanier");
		if (confirm(str_lang.kConfirmJSLineSuppression)) {
			formPan.ligne.value=i;
			if (typeof(action)=='undefined' || action == null ){
				action="SUP";
			}
			formPan.commande.value=action;
			if (type){
				formPan.type.value=type;
			}
			formPan.submit();
			document.getElementById("framePanier").onload = function(){
				$j(".panListe .selected").click();
			}
			flag_reload_current_pan = true ;


		}
	}else if ((typeof elt_origin_call != 'undefined' && $j(elt_origin_call).parents('#order_form').length > 0) || (typeof elt_origin_call == 'undefined' &&  document.getElementById("order_form")!= null)){
		formPan = document.getElementById("order_form");
		if (confirm(str_lang.kConfirmJSLineSuppression)) {
			// console.log("here");
			formPan.ligne.value=i;
			if (typeof(action)=='undefined' || action == null ){
				action="SUP";
			}
			formPan.commande.value=action;
			if (type){
				formPan.type.value=type;
			}
			formPan.submit();

			flag_reload_current_pan = true ;

			setTimeout(function(){refreshCommande($j("#order_form input[name='id_panier']").val(),"")},500);
		}
	}
}

function removeAll() {
	if( document.getElementById("formPanier") != null){
		formPan = document.getElementById("formPanier");
		if (confirm(str_lang.kConfirmJSAllLineSuppression)) {
			formPan.commande.value="SUP_ALL";
			formPan.submit();
			document.getElementById("framePanier").onload = function(){
				$j(".panListe .selected").click();
			}
			flag_reload_current_pan = true ;
		}
	}else if ( document.getElementById("order_form")!= null){
		formPan = document.getElementById("order_form");
		if (confirm(str_lang.kConfirmJSAllLineSuppression)) {
			formPan.commande.value="SUP_ALL";
			formPan.submit();
			flag_reload_current_pan = true ;
			setTimeout(function(){refreshCommande($j("#order_form input[name='id_panier']").val(),"")},500);
		}
	}
}

function check_selected(){
	var ids = '';
	$j("form#formPanier input[name^='checkbox']").each(function(){
		if ($j(this).is(':checked')) {
			if (ids != ''){
				ids = ids+',';
			}
			ids = ids + $j(this).val();
		}
	});
	if (ids == ''){
		alert("Vous devez sélectionner au moins une notice");
		return false;
	}else{
		return true;
	}
}