// apparement actif uniquement sur la recherche (demo & bycn)
function toggleAdvForm() {
	// DÈtermine ou non s'il faut ouvrir le panneau de recherche av = si au moins une valeur y est renseignÈe
	advRech=0;
	myDiv=document.getElementById('advForm');
	if(myDiv){
		myInp=myDiv.getElementsByTagName('input');
		for (i = 0; i < myInp.length; i++) {
			if (myInp[i].name.indexOf('chValues[')!=-1 && myInp[i].value!=''){
				if(myInp[i].type != "checkbox" || (myInp[i].type=="checkbox" && myInp[i].checked))
				advRech++;
			}
		}
		myInp=myDiv.getElementsByTagName('select');
		for (i = 0; i < myInp.length; i++) {
			if (myInp[i].name.indexOf('chValues[')!=-1 && myInp[i].value!='') advRech++;
		}
		if (advRech>0) toggleVisibility(document.getElementById('advForm'),document.getElementById('arrowadvForm'));
	}
}

// ==> devrait peut-être être déplacer dans les lib/webcomponents/fonctionsJavascript.js
function submitFormEvt(ev) {
	if (ev.keyCode==13)
		submitForm();
}


function addValue(id_champ,valeur,idx,role,autocomplete,multi) {
	formChanged=true;
	if(id_champ.indexOf('$')== -1){
		fld=document.getElementById(id_champ);
		if (fld.value!='') fld.value+=", ";
		fld.value+=valeur;
	} else {
		arrField=id_champ.split('$');
		type=arrField[0];
		cntFields=0;
		if (document.getElementById(type+'$limit')) limit=document.getElementById(type+'$limit').value; else limit=250;
		allowdouble=document.getElementById(type+'$allowdouble');

		if(idx==0){
			var d=new Date();
			idx="T"+d.getTime();
		}

		// ajouter test sur valeur existante
		prt=document.getElementById(type+'$tab');
			if ( typeof(multi) == "undefined" ) {
				for(i=0;i<prt.childNodes.length;i++) {
					chld=prt.childNodes[i];

					if (chld.id && chld.id.indexOf('$DIV$')!=-1) cntFields++;
					if (cntFields==limit) {
						alert (limit+str_lang.kDOMlimitvalues);
						return false;
						}

					if (chld.id==type+'$DIV$'+idx && !allowdouble ) {
						alert(str_lang.kDOMnodouble); return false;

					}
				}
		}
		btnAdd=document.getElementById(type+'$add');

		newFld=document.createElement('DIV');
		newFld.id=type+'$DIV$'+idx;
		newFld.name=type+'$DIV$'+idx;
		newFld.className=prt.className;
		addRow=new Array(newFld.id,valeur,idx,role) 
		str=eval('add_'+type+'(addRow)');
		newFld.innerHTML=str[1];
		if (btnAdd) {prt.insertBefore(newFld,btnAdd);} else {prt.appendChild(newFld);}
		if(autocomplete){
			if(document.getElementById(newFld.id+"t_doc_lex[][LEX_TERME]")) {
				eval("document.getElementById('"+newFld.id+"t_doc_lex[][LEX_TERME]').onfocus=function () {initAutoComplete(this.form, this,null,'blank.php?urlaction=chercheMots&amp;type="+autocomplete+"')}");
				document.getElementById(newFld.id+"t_doc_lex[][LEX_TERME]").focus();
			}
			if(document.getElementById(newFld.id+"t_doc_lex[][PERS_NOM]")) {
				eval("document.getElementById('"+newFld.id+"t_doc_lex[][PERS_NOM]').onfocus=function () {initAutoComplete(this.form, this,null,'blank.php?urlaction=chercheMots&amp;type="+autocomplete+"')}");
				document.getElementById(newFld.id+"t_doc_lex[][PERS_NOM]").focus();
			}
			if(document.getElementById(newFld.id+"t_mat_val[][VALEUR]")) {
				eval("document.getElementById('"+newFld.id+"t_mat_val[][VALEUR]').onfocus=function () {initAutoComplete(this.form, this,null,'blank.php?urlaction=chercheMots&amp;type="+autocomplete+"')}");
				document.getElementById(newFld.id+"t_mat_val[][VALEUR]").focus();
			}
			if(document.getElementById(newFld.id+"t_doc_val[][VALEUR]")) {
				eval("document.getElementById('"+newFld.id+"t_doc_val[][VALEUR]').onfocus=function () {initAutoComplete(this.form, this,null,'blank.php?urlaction=chercheMots&amp;type="+autocomplete+"')}");
				document.getElementById(newFld.id+"t_doc_val[][VALEUR]").focus();
			}
		}
	}
}


function addHiddenValue(id_champ,valeur,idx){
	if (document.getElementById(id_champ) && document.getElementById('hidden_value')){
		document.getElementById(id_champ).name='';
		document.getElementById('hidden_value').name='value';
		
		document.getElementById(id_champ).value=valeur;
		document.getElementById('hidden_value').value=idx;
		
	}
}
function addHiddenId(id_champ,valeur,idx){
	if (document.getElementById(id_champ) && document.getElementById('hidden_value')){
		document.getElementById(id_champ).name='';
		document.getElementById('hidden_value').name='id';
		
		document.getElementById(id_champ).value=valeur;
		document.getElementById('hidden_value').value=idx;
		
	}
}



/*
* Remplissage du champ valeur par retour de palette : concaténation (comme en recherche par exemple)
*/
//callback spécial pour retour palette d'un fonds
// pas d'appel à cette fonction trouvé dans modifLot.inc ou cherche.inc
function addValueFonds(id_champ,valeur,idx) {
        addValueUnique(id_champ,valeur,idx);
}

//callback spécial pour retour palette d'un usager
// pas d'appel à cette fonction trouvé dans modifLot.inc ou cherche.inc

function addValueUsager(id_champ,valeur,idx) {
	fld=document.getElementById(id_champ);
	fld.value = idx;
}


// FONCTIONS MODIF LOT (form/modifLot.inc.php)

/*
* Remplissage du champ valeur par retour de palette : valeur unique (utilisé pour rechercher/remplacer)
*/
// ==> utilisé dans modifLot.inc.php 
function addValueUnique(id_champ,valeur,idx) {
	if(id_champ.indexOf("$") != -1 ){
		arrChamp=id_champ.split('$');
		id_champ=arrChamp[0];
	}

	fld=document.getElementById(id_champ);
	fld.value=valeur;
}

function addIdUnique(id_champ,valeur,idx) {
	if(id_champ.indexOf("$") != -1 ){
		arrChamp=id_champ.split('$');
		id_champ=arrChamp[0];
	}

	fld=document.getElementById(id_champ);
	fld.value=idx;
}

/*
* Affichage du waiter après validation au dessus de la page (ie modal)
*/
// ==> utilisé dans modifLot.inc.php (via chkField)
function showWaiter(mode) {
	arrSel=document.getElementsByTagName('select');
	if (mode=='on' ) {
		document.getElementById('waiter').style.display='block';
		for (i=0;i<arrSel.length;i++) arrSel[i].style.display='none'; //bug IE select
		}
	if (mode=='off' ){
		document.getElementById('waiter').style.display='none';
		for (i=0;i<arrSel.length;i++) {if (arrSel[i].disabled==false) arrSel[i].style.display='inline';}//bug IE select

	}
}

/*
* Check avant soumission du formulaire : les champs obligatoire (mandatory) sont checkés + les select
*/
// ==> utilisé dans modifLot.inc.php
function chkField() {
	if (document.form1.value.disabled==false && document.form1.value.getAttribute('mandatory')=='yes' && document.form1.value.value.Trim()=='') {

			document.form1.value.className='fld_error';
			alert(str_lang.kJSErrorOblig);return false;

		} else {document.form1.value.className='fld_normal';}

	if (document.form1.value2.disabled==false && document.form1.value2.getAttribute('mandatory')=='yes'
			&& document.form1.value2.value.Trim()=='') {
				document.form1.value2.className='fld_error';
				alert(str_lang.kJSErrorOblig);return false;
		} else {document.form1.value2.className='fld_normal';}

	if (document.form1.field.disabled==false && document.form1.field.selectedIndex==0) return false;
	if (document.form1.field2.disabled==false && document.form1.field2.selectedIndex==0) return false;


	_hasChanged=false;
	showWaiter('on');
	return true;
}

// ==> utilisé dans modifLot.inc.php
function cancel () {
	if (confirm(str_lang.kAbandonnerChangements)) window.close();
}


/*
* Recharge le formulaire en fonction de l'action
* IN : commande, bouton d'action, réinit du sélecteur de champ (true/false)
*/
// ==> utilisé dans modifLot.inc.php
function reloadForm(commande,button,resetSelect) { //clic sur un bouton
	if (_hasChanged && !confirm(str_lang.kAbandonnerChangements)) return;

	for (i=0;i<arrActions.items.length;i++) { //on parcours les actions
		if (commande==arrActions.items[i].COMMANDE) {
			if (commande=="SAISIE"){
				$j('#saisie_lot').show();
				$j('#input_container').hide();
				$j('#action_comment').hide();
			}
			else{
				$j('#saisie_lot').hide();
				$j('#input_container').show();
				$j('#action_comment').show();
			for(j=0;j<document.getElementById('form1').elements.length;j++) { //parcour éléments du formulaire
				elmt=document.getElementById('form1').elements[j];
				if (elmt.type=='select-one' && resetSelect) elmt.selectedIndex=0; //raz select si appui btn
				if (elmt.type=='text') {
						elmt.value=''; //raz texte
						elmt.className='fld_normal';
				}

				//note : le test sur elmt.type est pour les fieldset qui n ont pas de type (undefined)
				if (elmt.type && elmt.type!='hidden' && elmt.type!='fieldset') {
						elmt.disabled=true;
						elmt.style.display='none';
						lbl=document.getElementById('lbl'+elmt.name);
						if (lbl) lbl.innerHTML='';
				} //par defaut, on cache tous les champs + raz des labels
				for (u=0;u<arrActions.items[i].ELEMENT.length;u++) { //parcours des élements de l action
					if (arrActions.items[i].ELEMENT[u].NAME==elmt.name) { //on a trouvé l input dans la liste
						//on l affiche
						elmt.disabled=false;
						//alert(arrActions.items[i].ELEMENT[u].NAME);
						elmt.style.display='inline';

						//obligatoire ?
						if (arrActions.items[i].ELEMENT[u].MANDATORY) {
							//Note : pas de true ici car interprété diversement suivant les browsers !
							elmt.setAttribute('mandatory','yes');
						} else elmt.removeAttribute('mandatory');

						//Pour le onclick de supp 
						if (arrActions.items[i].ELEMENT[u].ONCLICK) {
							//Note : pas de true ici car interprété diversement suivant les browsers !
							elmt.setAttribute('onclick',arrActions.items[i].ELEMENT[u].ONCLICK);
						} else elmt.removeAttribute('onclick');


						//callback index ?
						if (arrActions.items[i].ELEMENT[u].CALLBACK) {
							elmt.setAttribute('callback',arrActions.items[i].ELEMENT[u].CALLBACK);
						} else elmt.removeAttribute('callback');

						//label ?
						if (arrActions.items[i].ELEMENT[u].LABEL) {
							lbl=document.getElementById('lbl'+elmt.name);
							if (lbl) lbl.innerHTML=arrActions.items[i].ELEMENT[u].LABEL;
						}
					}
				}
			}
		}
			//Commentaire de l action
			if (arrActions.items[i].COMMENT)
			document.getElementById('action_comment').innerHTML=arrActions.items[i].COMMENT
			else document.getElementById('action_comment').innerHTML='';
			_hasChanged=false;
			document.getElementById('form1').commande.value=arrActions.items[i].COMMANDE;
		}
	}
	//affichage des boutons avec mise en forme particulière pour l action en cours
	arrBtns=document.getElementById('action_bar').getElementsByTagName('div');

	for (u=0;u< arrBtns.length;u++) {
		if (button==arrBtns[u]) arrBtns[u].className='btnActionSelected'; else arrBtns[u].className='btnAction';
	}
	if (commande=="SAISIE" && document.getElementById('saisie_container')) {
		if(typeof document.getElementById('errormsg') != 'undefined' && document.getElementById('errormsg') != null) {
			document.getElementById('errormsg').parentNode.removeChild(document.getElementById('errormsg'));
		}
		document.getElementById('saisie_container').style.display='block';
		document.getElementById('saisie_container').style.height='100%';
		document.getElementById('input_container').style.display='none';
	}
	else {
		if (document.getElementById('saisie_container')) document.getElementById('saisie_container').style.display='none';
	}
	document.getElementById('info_entite').innerHTML='';
}



// FONCTIONS RECHERCHE EXPERTE (form/cherche3.inc.php)

//Crée un select avec les différents champs de recherche possible
// In : nom du DIV dans lequel le champ est créé : type$x
// Out : false sur échec, true sur succès
function makeListOfSearches(insideDiv,var_tabRech) {
	myDiv=document.getElementById(insideDiv);
	if (!myDiv) {return false; }
	arrFld=insideDiv.split('_');
//        console.log(var_tabRech);
	str="<select id='selectType$"+arrFld[1]+"' >"; //onPropertyChange='actualize(this)'  onchange='actualize(this)'
        
        if(var_tabRech.length>0){
            for (var key in var_tabRech){
                var first_index=key;
                break;
            }
        }
        
//        console.log('first_index '+first_index);
        // B.RAVI 2015-09-15 pour eviter erreur js car désormais on peut "sauter" certains index avec la balise <profil_min> de <innerfields> (@see class_formCherche.php)
        // donc var_tabRech[0].value peut être null si l'utilisateur n'a pas le profil suffisant
//        if(var_tabRech.length>0 && var_tabRech[0].value != ''){
        if(var_tabRech.length>0 && var_tabRech[first_index].value != ''){
			start = 0 ;
		}else{
			str+="<option value=''>"+str_lang.kCritere+"</option>\n";
			start = 1;
		}
	
	// Tri des paramètres avant affichage du select
	// Fonction de comparaison à modifier eventuellement, 
	// fonction normalizeAccents dans fonctionsJavascript.js
	function compare (a,b){
		if (normalizeAccents(a['LIB'])< normalizeAccents(b['LIB'])){
			return -1;
		}
		if (normalizeAccents(a['LIB']) > normalizeAccents(b['LIB'])){
			return 1;
		}
		return 0;
	}
	var_tabRech.sort(compare);
	
	for (i=start;i<var_tabRech.length;i++) {
            // B.RAVI 2015-09-15 pour eviter erreur js car désormais on peut "sauter" certains index avec la balise <profil_min> de <innerfields> (@see class_formCherche.php)
            // check if variable exist (not null AND not undefined) to not trigger javascript ERROR
            if (!(typeof var_tabRech[i] === 'undefined' || var_tabRech[i]=== null)) {
            str+="<option value='"+i+"'>"+var_tabRech[i]['LIB']+"</option>\n";
            }
	}
	str+="</select>";
	
	myDiv.innerHTML=str;
	// document.getElementById("selectType$"+arrFld[1]).addEventListener("change",function(){actualize(this,var_tabRech)},false);
	// document.getElementById("selectType$"+arrFld[1]).addEventListener("propertychange",function(){actualize(this,var_tabRech)},false);
	document.getElementById("selectType$"+arrFld[1]).onchange=function(){actualize(this,var_tabRech)};
	document.getElementById("selectType$"+arrFld[1]).onpropertychange=function(){actualize(this,var_tabRech)};
	if(var_tabRech.length>0 && var_tabRech[1].value != ''){
		actualize(document.getElementById("selectType$"+arrFld[1]),var_tabRech);
	}
	return true;
}

function initCriteres(start,end,var_tabRech, var_tabCrit) {
	for (j=start;j<=end;j++) {
		makeListOfSearches('type_'+j,var_tabRech);
	}
	k=start;
	if(var_tabRech.length>0 && var_tabRech[1].value != ''){
		start_tab_rech = 0 ;
	}else{
		start_tab_rech = 1 ;
	}

    if(typeof var_tabCrit == "undefined"){
         var_tabCrit = tabCrit;
    }
	
    for (j=start;j<=end;j++) {
		//valeur
		for (u=start_tab_rech;u<var_tabRech.length;u++) {

			if (
				typeof var_tabCrit[j]!="undefined"
                && typeof var_tabRech[u]!="undefined" // B.RAVI 2015-09-15 pour eviter erreur js car désormais on peut "sauter" certains index avec la balise <profil_min> de <innerfields> (@see class_formCherche.php)
				&& var_tabRech[u]['FIELDS'].toUpperCase()==var_tabCrit[j]['FIELD'] .toUpperCase()
				&& var_tabRech[u]['TYPES'].toUpperCase().indexOf(var_tabCrit[j]['TYPE'].toUpperCase())!=-1
				&& (typeof var_tabRech[u]['OPTIONS'] == "undefined" || var_tabRech[u]['OPTIONS'].toUpperCase()==var_tabCrit[j]['OPTIONS'].toUpperCase())
				&& k<=end ){
				document.getElementById('selectType$'+k).options[u].selected=true;
				actualize(document.getElementById('selectType$'+k),var_tabRech);
				document.getElementById('chValues['+k+']').value=var_tabCrit[j]['VALEUR'];
				document.getElementById('chOps['+k+']').value=var_tabCrit[j]['OP'];
				document.getElementById('chTypes['+k+']').value=var_tabCrit[j]['TYPE'];
				if(var_tabRech[u]['TYPE']=='select') {
					option_val= var_tabCrit[j]['VALEUR'].toString().split(',');
					for(var i = 0; i<option_val.length;i++) { 
						$j('#chValues\\['+k+'\\] option[value="'+option_val[i]+'"]').attr("selected","selected");
					}
					$j('#chValues\\['+k+'\\]').trigger('chosen:updated');
					document.getElementById('chOps['+k+']').value=var_tabCrit[j]['OP'];
				}
				k++;
				break;
			}else{
				// console.log("NOK - j:"+j);
			}
		}
	}
	
}

function ajouterCritere(btn,offset)
{
	console.log('ajouterCritere');
	if (typeof offset != "number"){
		offset = 0 ;
	}

	var elt_cloned=$j(btn).parent().parent().prev().prev().clone(true,true);
	// recuperation de l'id en cours
	var id_courant=parseInt($j(btn).parent().parent().prev().prev().find('td:first-child select').attr('id').substring(6,$j(btn).parent().parent().prev().prev().find('td:first-child select').attr('id').length - 1));
	var id_futur=id_courant+1+offset;
	
	$j(elt_cloned).find("select,input,span").each(function(idx,elt){
		if($j(elt).attr('id').indexOf("selectType") == 0){
			$j(elt).get(0).onchange = $j("#selectType\\\$"+id_courant).get(0).onchange;
		}
		
		if(typeof $j(elt).attr('name')!= "undefined" &&   $j(elt).attr('name')!=""){
			$j(elt).attr("name",$j(elt).attr("name").replace(new RegExp('\\\['+id_courant+'\\\]','g'),'['+id_futur+']'));
			$j(elt).attr("name",$j(elt).attr("name").replace(new RegExp('_'+id_courant,'g'),'_'+id_futur));
			$j(elt).attr("name",$j(elt).attr("name").replace(new RegExp('\\\$'+id_courant,'g'),'\$'+id_futur));
		}
		if(typeof $j(elt).attr('id')!= "undefined" && $j(elt).attr('id')!=""){
			$j(elt).attr("id",$j(elt).attr("id").replace(new RegExp('\\\['+id_courant+'\\\]','g'),'['+id_futur+']'));
			$j(elt).attr("id",$j(elt).attr("id").replace(new RegExp('_'+id_courant,'g'),'_'+id_futur));
			$j(elt).attr("id",$j(elt).attr("id").replace(new RegExp('\\\$'+id_courant,'g'),'\$'+id_futur));
		}
	});
	

	$j(elt_cloned).insertAfter($j(btn).parent().parent().prev().prev());
	$j('#btnIndex_'+id_futur).css('display','none');
	$j("span#typeRech\\\["+id_futur+"\\\]").html("");
	$j("input#chValues\\\["+id_futur+"\\\]").val("");
	
	$j(elt_cloned).find("select[id^='selectType']").change();
	
	if($j("#compte_champs").length>0){
		var nb_champs=$j('#compte_champs').val();
		nb_champs++;
		$j('#compte_champs').val(nb_champs);
	}
	
}


// Mise à jour d'une ligne de critères.
// Appelé sur onchange d'un champ de recherche (+init)
// IN : tableau des critères issus de la session (tabCrit) et tab des types de recherche (tabRech).
//		field = obj de champ de select de recherche.
// OUT : génération DOM/JS des champs en fn du type de recherche
function actualize(field, var_tabRech) {
	
	arrFld=field.id.split('$');
	index=arrFld[1]; //ligne en cours d'édition
	mySelectType=document.getElementById(field);
	
	
	if (var_tabRech[field.selectedIndex]['TYPE']=='select'){ 
		$j('#chValues_'+index+'__chosen').remove();
		//console.log("field.selectedIndex : "+field.selectedIndex);
		if(typeof var_tabRech[field.selectedIndex]['SELECT_VALS'] !='undefined'){
			tab_val=var_tabRech[field.selectedIndex]['SELECT_VALS'];
		}else{
			tab_val=eval(var_tabRech[field.selectedIndex]['FIELDS']);
		}
		//console.log(tab_val);
		if(typeof jQuery().chosen != 'undefined') {
			var selectMultiple = "multiple=true";
		} else {
			var selectMultiple = '';
		}
		$j('#chValues\\['+index+'\\]').replaceWith('<select '+selectMultiple+' id="chValues['+index+']" class="chosen_style" name="chValues['+index+'][]"></select>');
		 var mySelect = $j('#chValues\\['+index+'\\]');
		$j.each(tab_val, function(val, text) {
			mySelect.append(
				$j('<option></option>').val(val).html(text)
			);
		});
		
		//VG : A faire évoluer pour prévoir le cas où on aura besoin de select avec chosen et de select sans chosen (laisser le choix du type de select, en définitif)
		if(typeof jQuery().chosen != 'undefined') {
			if(typeof var_tabRech[field.selectedIndex]['WIDTH_CHOSEN'] != 'undefined'){
				width_chosen = var_tabRech[field.selectedIndex]['WIDTH_CHOSEN'];
			}else{
				width_chosen = "73%";
			}
			$j('#chValues\\['+index+'\\]').chosen({ width: width_chosen,placeholder_text_multiple:'Choisissez une valeur...' });
		}
	
	 }else{
		$j('#chValues_'+index+'__chosen').remove();
		$j('#chValues\\['+index+'\\]').replaceWith('<input type="text" id="chValues['+index+']" name="chValues['+index+']" />');
	 }
	
	
	//if (typeof(field.selectedIndex)=='undefined') field.options[0].selected=true;
	if (field.selectedIndex==-1) field.options[0].selected=true; // rien de défini, on positionne sur la val par déft ('critère');
	
	if (field.options[0].innerHTML == str_lang.kCritere && field.options[0].selected==true) { // on a sél. la valeur muette "critère", il faut virer le reste
		if(document.getElementById('chTypes['+index+']') != null){
			document.getElementById('chTypes['+index+']').style.display='none';
		}
		document.getElementById('chValues['+index+']').value='';
		document.getElementById('chValues['+index+']').style.display='none';//pas besoin d'afficher le field de recherche
		document.getElementById('btnIndex_'+index).style.display='none';//on cache la loupe
		return false;
	}else{
	//	document.getElementById('chValues['+index+']').style.display='inline-block';
	//	document.getElementById('btnIndex_'+index).style.display='inline-block';
	}
	
        
	// Maj des champs de base concernés (cf. moteur de recherche)
	document.getElementById('chFields['+index+']').value=var_tabRech[field.selectedIndex]['FIELDS'];
	
	// Maj du champ select des types de recherches possibles (ex: FT,FTE,C pour une recherche sur du texte, etc.)
	lstTypes=var_tabRech[field.selectedIndex]['TYPES'].split(',');
	lstTypesLib=var_tabRech[field.selectedIndex]['TYPES_LIB'].split(',');
	
	//Selecteur de type
	selType="<select id='chTypes["+index+"]' name='chTypes["+index+"]'>\n";
	for (i=0;i<lstTypes.length;i++) {		
			selType+="<option value='"+lstTypes[i]+"'>"+lstTypesLib[i]+"</option>\n";
	}
	selType+="</select>";
	
	document.getElementById('typeRech['+index+']').innerHTML=selType;
        
        //BR 2015/01/06 ex: si la liste déroulante ne contient qu'1 seule valeur FULLTEXT on peut choisir de ne pas afficher celle-ci en <hidden>1</hidden>1 dans le xml, mais le mode fulltext sera bien pris en compte
        if (var_tabRech[field.selectedIndex]['HIDDEN']==1){
            document.getElementById('chTypes['+index+']').style.display='none';
            
        }
        
	
	//réinit Valeur
	//document.getElementById('chValues\['+index+'\]').value='';
	$j('#chValues\\\['+index+'\\\]').val('');
	
	try{
		$j('#chValues\\\['+index+'\\\]').datepicker('destroy');
	}catch(e){
		console.log("suppression datepicker failed");
	}
	
	//récup libellé de recherche
	document.getElementById('chLibs['+index+']').value=var_tabRech[field.selectedIndex]['LIB'];

	// Affichage ou non du bouton de selection "Choisir" qui appelle l'affichage paramétré de l'iframe de sélection.
	btnIndex=document.getElementById('btnIndex_'+index);
        
        
    //console.log('debug'+var_tabRech[field.selectedIndex]['HIDDEN']);
        
    if(typeof var_tabRech[field.selectedIndex]['DATEPICKER'] !='undefined' &&  var_tabRech[field.selectedIndex]['DATEPICKER'] ==1 ){
		$j('#chValues\\\['+index+'\\\]').datepicker(dp_config);
	}
	if (var_tabRech[field.selectedIndex]['SELECTOR']!='' ) {
        if (typeof var_tabRech[field.selectedIndex]['INDEX_IMG']!='undefined' && var_tabRech[field.selectedIndex]['INDEX_IMG']!=''){
            _parent=btnIndex.parentNode;
            _parent.removeChild(btnIndex);
            btnIndex=document.createElement('img');
            btnIndex.id='btnIndex_'+index;
			if(typeof ow_const != 'undefined' && typeof ow_const.kCheminHttp != 'undefined'){
				btnIndex.src = ow_const.kCheminHttp+"/";
			}else{
				btnIndex.src ="" ; 
			}
            btnIndex.src+='design/images/'+var_tabRech[field.selectedIndex]['INDEX_IMG'];
            _parent.appendChild(btnIndex);
        }
		btnIndex.style.display='inline';
		btnIndex.style.visibility='visible';
		btnIndex.className='miniButton';
                
		params_choose="titre_index="+(var_tabRech[field.selectedIndex]['LIB'])+"&valeur=&champ="+var_tabRech[field.selectedIndex]['SELECTOR'];
                
                // B.RAVI possibilité de choisir une autre fct que addValue quand on choisit une valeur dans la palette
                // check if variable exist (not null AND not undefined) to not trigger javascript ERROR
                if (!(typeof var_tabRech[field.selectedIndex]['RTN'] === 'undefined' || var_tabRech[field.selectedIndex]['RTN']=== null)) {
                    if ( var_tabRech[field.selectedIndex]['RTN'].trim() != '' ) {
                        params_choose+="&rtn="+var_tabRech[field.selectedIndex]['RTN'];
                    }
                }

                // check if variable exist (not null AND not undefined) to not trigger javascript ERROR
                if (!(typeof var_tabRech[field.selectedIndex]['INDEX_NB'] === 'undefined' || var_tabRech[field.selectedIndex]['INDEX_NB']=== null)) {
//                  if ( var_tabRech[field.selectedIndex]['INDEX_NB'].trim() != '' ) {
                    if ( var_tabRech[field.selectedIndex]['INDEX_NB'] != '' ) { //B.R 2017-04-14 on enlève trim() car index_nb peut être de type number
                        params_choose+="&affich_nb="+var_tabRech[field.selectedIndex]['INDEX_NB'];
                    }
                }
                if (!(typeof var_tabRech[field.selectedIndex]['INDEX_LEX_AFF'] === 'undefined' || var_tabRech[field.selectedIndex]['INDEX_LEX_AFF']=== null)) {
                    if ( var_tabRech[field.selectedIndex]['INDEX_LEX_AFF'].trim() != '' ) {
                        params_choose+="&lex_aff="+var_tabRech[field.selectedIndex]['INDEX_LEX_AFF'];
                    }
                }               
                
                
		fldValue=document.getElementById('chValues\['+index+'\]');
		if (var_tabRech[field.selectedIndex]['JS_FUNC']=='chooseSolr')
			btnIndex.onclick=(function(fldValue,params_choose){return function(){chooseSolr(fldValue,params_choose);};})(fldValue,params_choose);
		else{
			btnIndex.onclick=(function(fldValue,params_choose){return function(){choose(fldValue,params_choose);};})(fldValue,params_choose);
			}

	} else {
               
		btnIndex.style.display='none';
		btnIndex.style.visibility='hidden';
	}

	if (document.getElementById('chValues2['+index+']')!=null)
	{
		if (var_tabRech[field.selectedIndex]['VALUES2']) {
			document.getElementById('chValues2['+index+']').value=var_tabRech[field.selectedIndex]['VALUES2'];
		} else {
			document.getElementById('chValues2['+index+']').value="";
		}
	}

	if (document.getElementById('chNoHLs['+index+']')!=null)
	{
		if (var_tabRech[field.selectedIndex]['NOHL']) {
			document.getElementById('chNoHLs['+index+']').value=var_tabRech[field.selectedIndex]['NOHL'];
		} else {
			document.getElementById('chNoHLs['+index+']').value="";
		}
	}
	
	if(typeof var_tabRech[field.selectedIndex]['AUTOCOMPLETE'] != "undefined" ) {
		var myFunc = var_tabRech[field.selectedIndex]['AUTOCOMPLETE_FUNC']+"(this.form, document.getElementById('chValues["+index+"]'),null,'blank.php?urlaction=chercheMotsSolr&type="+var_tabRech[field.selectedIndex]['AUTOCOMPLETE']+"',null,'"+var_tabRech[field.selectedIndex]['AUTOCOMPLETE_QUOTE']+"');";
		document.getElementById('chValues['+index+']').onfocus=function () {
			eval(myFunc);
		}
	}

	if (document.getElementById('chOptions['+index+']')!=null)
	{
		if (var_tabRech[field.selectedIndex]['OPTIONS']) {
			document.getElementById('chOptions['+index+']').value=var_tabRech[field.selectedIndex]['OPTIONS'];
		} else {
			document.getElementById('chOptions['+index+']').value="";
		}
	}
	
}

// Init des critères
// Crée les select de champs de recherche
// Puis remplit les champs en fn des critères présents en session
function init(langue,nb_critere_recherche) {
	for (j=1;j<=nb_critere_recherche;j++) {
		makeListOfSearches('type$'+j,langue);
	}
	
	if (document.getElementById('chNoHLs['+index+']')!=null)
	{
		if (var_tabRech[field.selectedIndex]['NOHL']) {
			document.getElementById('chNoHLs['+index+']').value=var_tabRech[field.selectedIndex]['NOHL'];
		} else {
			document.getElementById('chNoHLs['+index+']').value="";
		}
	}

}


// Fonction lexListe => permet de switcher de mode (alphabetique / hierarchique)
function toggleMode(ctrl)
{
	//verouillage du statut en valide
	
	if ($j(ctrl).val()=='HIERARCHIQUE')
	{
		$j(document.getElementById('chValues[2]')).val('2');
		$j(document.getElementById('chValues[2]')).attr('disabled','disabled');
	}
	else
		$j(document.getElementById('chValues[2]')).removeAttr('disabled');
	
	myTR=document.getElementById('modes_affichage');
	myChecks=myTR.cells[1].getElementsByTagName('input');
	mySelect=myTR.cells[2].getElementsByTagName('select');

	myTR.cells[(ctrl.value=='LISTE'?2:1)].style.display='none';
	myTR.cells[(ctrl.value=='LISTE'?1:2)].style.display='block';

	for (i=0;i<myChecks.length;i++)
	{
		if (myChecks[i].type=='checkbox')
			myChecks[i].disabled=(ctrl.value=='LISTE'?false:true);
	}

	for (i=0;i<mySelect.length;i++)
	{
		mySelect[i].disabled=(ctrl.value=='LISTE'?true:false);
	}

}

// Fonctions d'affichage des informations d'un lexique (utile dans la palette lexique & dans la gestion des lexiques lexListe)
var lastidx;
function showInfos(type,idx,xsl) 
{
	if (idx!='' && type!='' && idx != lastidx) 
		{
		if(xsl=='')
			xsl = '0';
		lastidx = idx;
		return !sendData('GET','export.php','xmlhttp=1&id='+idx+'&type='+type+'&export='+xsl,'dspInfos');
	}
}

function dspInfos(str)
{
	if(document.getElementById('infos'))
		document.getElementById('infos').innerHTML=str;
}


// utilisé dans la paletteEdit
function submitIfReturn(event)
{
	if (event.which)
		mykey=event.which;
	
	if (event.keyCode)
		mykey=event.keyCode;
	
	if (mykey==13)
	{
		document.form1.page.value=eval(document.getElementById('rang').value);
		document.form1.submit();
	}
}

/***************************FACETTES !! **************************/
/*function getNumValueField(champ)
{
	var nom_champ_colonne='chFields';
	var nom_champ_form=$j('input[name^="'+nom_champ_colonne+'"][value="'+champ+'"]').attr('id');
	
	if (typeof(nom_champ_form)!='undefined' && nom_champ_form!=null)
	{
		return nom_champ_form.substring(nom_champ_colonne.length+1,nom_champ_form.length-1);
	}
	
	return null;
}

function addFacetSearch(champ,valeur,action)
{
	//alert(champ+' = '+valeur);
	var num_champ=getNumValueField(champ);
	
	if (num_champ!=null)
	{
		//alert('#chValues\\\['+num_champ+'\\\]');
		if (action=='add')
		{
			if ($j('#chValues\\\['+num_champ+'\\\]').val()=='')
				$j('#chValues\\\['+num_champ+'\\\]').val(valeur);
			else if($j('#chValues\\\['+num_champ+'\\\]').val().indexOf(valeur))
				$j('#chValues\\\['+num_champ+'\\\]').val($j('#chValues\\\['+num_champ+'\\\]').val()+','+valeur);
		}
		else
			$j('#chValues\\\['+num_champ+'\\\]').val(valeur);
		
		if($j("form.facet_form").length>0){
			$j("form.facet_form").eq(0).submit();
		}else{
			$j('#chercheForm').submit();
		}
	}
}

function addFacetDateSearch(champ,valeur)
{
	//alert(champ+' = '+valeur);
	var num_champ=getNumValueField(champ);
	
	if (num_champ!=null)
	{
		//alert('#chValues\\\['+num_champ+'\\\]');

		$j('#chValues\\\['+num_champ+'\\\]').val(valeur+'-01-01');
		$j('#chValues2\\\['+num_champ+'\\\]').val(valeur+'-12-31');
		
		if($j("form.facet_form").length>0){
			$j("form.facet_form").eq(0).submit();
		}else{
			$j('#chercheForm').submit();
		}
	}
}

function addFacetIntDateSearch(champ,valeur, valeur2)
{
	//alert(champ+' = '+valeur);
	var num_champ=getNumValueField(champ);
	
	if (num_champ!=null)
	{
		//alert('#chValues\\\['+num_champ+'\\\]');

		$j('#chValues\\\['+num_champ+'\\\]').val(valeur+'-01-01');
		$j('#chValues2\\\['+num_champ+'\\\]').val(valeur2+'-12-31');
		
		if($j("form.facet_form").length>0){
			$j("form.facet_form").eq(0).submit();
		}else{
			$j('#chercheForm').submit();
		}
	}
}

function removeFacetSearch(champ)
{
	var num_champ=getNumValueField(champ);
	
	if (num_champ!=null)
	{
		if ($j("input[name^='chValues\\\["+num_champ+"\\\]']").attr('type')=='checkbox')
		{
			$j("input[name^='chValues\\\["+num_champ+"\\\]']").removeAttr('checked');
			// $j("input[name^='chValues\\\["+num_champ+"\\\]']").removeAttr('checked');
		}
		else
		{
			$j('#chValues\\\['+num_champ+'\\\]').val('');
			// $j('#chValues2\\\['+num_champ+'\\\]').val('');
		}
		
		if($j("form.facet_form").length>0){
			$j("form.facet_form").eq(0).submit();
		}else{
			$j('#chercheForm').submit();
		}
	}
}

function removeFacetSearchIntDate(champ)
{
	var num_champ=getNumValueField(champ);
	
	if (num_champ!=null)
	{
		$j('#chValues\\\['+num_champ+'\\\]').val('');
		$j('#chValues2\\\['+num_champ+'\\\]').val('');
		
		if($j("form.facet_form").length>0){
			$j("form.facet_form").eq(0).submit();
		}else{
			$j('#chercheForm').submit();
		}
	}
}


function removeFacetSearchMulti(champ,value)
{
	var num_champ=getNumValueField(champ);
	
	if (num_champ!=null)
	{
		var contenu;
		if ($j('#chValues\\\['+num_champ+'\\\]').val().indexOf(value+',')!=-1)
			contenu=$j('#chValues\\\['+num_champ+'\\\]').val().replace(value+',','');
		else if ($j('#chValues\\\['+num_champ+'\\\]').val().indexOf(','+value)!=-1)
			contenu=$j('#chValues\\\['+num_champ+'\\\]').val().replace(','+value,'');
		else if($j('#chValues\\\['+num_champ+'\\\]').val() == value)
			contenu='';
			
		$j('#chValues\\\['+num_champ+'\\\]').val(contenu);
		if($j("form.facet_form").length>0){
			$j("form.facet_form").eq(0).submit();
		}else{
			$j('#chercheForm').submit();
		}
	}
}

function removeFacets(fields)
{
	var num_champ;
	
	for (i=0;i<fields.length;i++)
	{
		num_champ=getNumValueField(fields[i]);
		
		if (num_champ!=null)
		{
			$j('#chValues\\\['+num_champ+'\\\]').val('');
			$j('#chValues2\\\['+num_champ+'\\\]').val('');
		}
	}
}

function displayFacettes(elt)
{
	if ($j(elt).prev().is(':visible'))
		$j(elt).prev().hide('fast');
	else
		$j(elt).prev().show('fast');
}
*/
/****** FACETTE DEMO  ***/

function toggleTypeFacet(elt){
	
	if($j(elt).hasClass("open") && showLeftMenu ){
		$j(elt).next().slideUp('fast');
		$j(elt).next('.block_facet').removeClass("open");
		$j(elt).removeClass("open");
	}else{
		$j("#menuGauche .block_facet.open").each(function(idx,elt){
			$j(elt).slideUp("fast"); ;//css("display","none");
		});
		$j("#menuGauche .open").removeClass("open");
		
		$j(elt).next().slideDown('fast');
	
		$j(elt).addClass("open");
		$j(elt).next('.block_facet').addClass("open");
		
	}
	
	if((typeof mobil_border =='undefined'|| $j(window).width()>= mobil_border) && $j("#menuGauche").hasClass("collapsed")){
		toggleLeftMenu();
	}
}


function toggleFoldHierFacet(event,elt,field,value){
	event.preventDefault() ; 
	event.stopPropagation() ; 
	// toggle affichage du niveau hierarchique descendant du niveau cliqué en utilisant des animations jQuery & des classes
	if($j(elt).hasClass("hier_open")){
		$j(elt).removeClass("hier_open");
		$j(elt).parent('.facet_hier').next("div[class^='facet_hier_lvl'].hier_open").slideUp('fast',function(){
			$j(elt).parent('.facet_hier').next("div[class^='facet_hier_lvl'].hier_open").removeClass("hier_open");
			$j(elt).parent('.facet_hier').removeClass("hier_open");
		});
	}else{
		$j(elt).addClass("hier_open");
		$j(elt).parent('.facet_hier').addClass("hier_open");
		$j(elt).parent('.facet_hier').next("div[class^='facet_hier_lvl']").slideDown('fast',function(){
			$j(elt).parent('.facet_hier').next("div[class^='facet_hier_lvl']").addClass("hier_open");
		});
		
		// Dans le cas où on déplie les descendants, on peut avoir besoin de charger via ajax : 
	
		// Si la facette qu'on tente de déplier n'a pas la classe "loaded"
		// => on doit faire une requête solr via ajax qui permet de remonter les facettes des X niveaux "suivants" où X est le include_sub_level défini dans solr_facet.xml pour une facette hierarchique (devrait au minimum être 1)
		// => cela permet une fois le block facet_hier "courant" deplié, de charger les "prochains fils" si il y en a, et ce afin que la navigation soit plus simple 
		// En gros : on doit toujours connaitre les facettes du niveau +1 par rapport au block visible pr l'user, afin d'améliorer la navigation

		if(!$j(elt).parent('.facet_hier').hasClass('loaded')){
			block_facet = $j(elt).parents('.block_facet').addClass('loading_hier');
			include_sub_level = parseInt(block_facet.attr('data-include-sub-level'),10);
			num_prefix = parseInt(value.substring(0,value.indexOf('/')),10);
			next_facet = {
				'TYPE':'FACET_HIER',
				'FIELD':field,
				'PREFIX' : value.replace(num_prefix,(num_prefix+include_sub_level+1)),
				'REFINE_CRIT' : 'ajax'
			};
			
			$j.ajax({
				url : 'empty.php?urlaction=docListe&where=include&cherche=0&fromAjax=1',
				method: 'POST',
				data : {
					refine_facet : JSON.stringify(next_facet),
					style : 'refreshFacets.xsl',
					dontUpdateSessStyle : 'true'
				},
				success : function(data){
					$j(elt).parent('.facet_hier').addClass('loaded');
					dom = $j("<body>").append(data);
					$j(dom).find("div[class^='facet_hier_lvl_']").each(function(idx,element){
						onclick_handler = $j(element).prev().attr('onclick');
						parent_facet_elt = $j("#menuGauche").find('div.facet_hier[onclick="'+onclick_handler.split("'").join("\\'")+'"][class!="loaded"]');
						if(parent_facet_elt.length >0 && parent_facet_elt.next('div[class^="facet_hier_lvl"]').length == 0 ){
							$j(element).insertAfter(parent_facet_elt);
							$j(parent_facet_elt).find("span.hier_unfolder").removeClass('hidden');
						}
					});
					block_facet.removeClass('loading_hier');
				}
			});
		}
	}
}

function checkHisto(){
	if($j("#histoScrollable .resultsCorps input[type='checkbox']:checked").length < 2 ){
		$j("#crossSrchBtns input[type='submit']").attr("disabled","true");
	}else{
		$j("#crossSrchBtns input[type='submit']").removeAttr("disabled");
	}
}

