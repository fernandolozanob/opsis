function hideSauvRecherche() {
	if(document.getElementById('sauvRecherche')){
		if(document.getElementById('REQUETE[REQUETE]')) document.getElementById('REQUETE[REQUETE]').value='';
		if(document.getElementById('REQUETE[SAUVE]')) document.getElementById('REQUETE[SAUVE]').value='';
		document.getElementById('sauvRecherche').style.display='none';
	}
}

function sauvRecherche() {
	if(document.getElementById('REQUETE[SAUVE]')) document.getElementById('REQUETE[SAUVE]').value='1';
	pagerForm.submit();
}

function displaySauvRecherche(ctrl) 
{
	if(document.getElementById('sauvRecherche')){
		myDiv=document.getElementById('sauvRecherche');
		myDiv.style.display='block';
		y=getTop(ctrl);
		x=getLeft(ctrl);

		myDiv.style.top=(y+20)+"px";
		myDiv.style.left=(x-100)+"px";
	}
}


function presentation(xsl) {
	pagerForm.style.value=xsl;
	pagerForm.submit();
}

function trier(col) {
	pagerForm.tri.value=col;
	pagerForm.submit();
}


function goPage(obj,pageold,pagemax){
	pagenew=parseInt(obj.value,10);
	if((pagenew>0) && (pagenew<=pagemax) && (pagenew!=pageold)){
		pagerForm.page.value=pagenew;pagerForm.submit();
	}
	else
		obj.value=pagemax;
}


function changePage(e,page,nb_pages)
{
	var keynum;
	var keychar;
	var numcheck;
	
	if(window.event) // IE
	{
		keynum = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		keynum = e.which;
	}

	if (keynum==13)
		goPage(document.getElementById('rang'),page,nb_pages);
}

// Fonction de génération du diaporama depuis un resultat de recherche ou un panier (popup)
function genDiaporamaFromCheckbox()
{
	var list_id='';
	
	$j('input[type="checkbox"][name^="checkbox"]').each
	(
		function (index)
		{
			if ($j(this).attr('checked')=='checked')
			{
				if (typeof($j(this).parent().parent().find('input[name="t_panier_doc[][ID_DOC]"]').val())!='undefined')
				{
					if (list_id=='')
						list_id+=$j(this).parent().parent().find('input[name="t_panier_doc[][ID_DOC]"]').val();
					else
						list_id+='$'+$j(this).parent().parent().find('input[name="t_panier_doc[][ID_DOC]"]').val();
				}
				else
				{
					if (list_id=='')
						list_id+=$j(this).attr('value');
					else
						list_id+='$'+$j(this).attr('value');
				}
			}
		}
	);
	
	//popupDoc('simple.php?urlaction=visualisation&amp;method=Diaporama&amp;type={$typeDiapo}','main');
	
	if (list_id!='')
		popupDoc('simple.php?include=visualisationListDocs&list_id='+list_id+'&export=1','main');
	else
	{
		if (document.URL.indexOf('urlaction=panier')!=-1)
		{
			var id=document.URL.substr(document.URL.indexOf('id_panier')+"id_panier=".length);
			
			if (id.indexOf('&')>0)
				id=id.substr(0,id.indexOf('&'));
			
			popupDoc('simple.php?urlaction=visualisation&method=Diaporama&type=panier&id='+id+'','main');
		}
		else
			popupDoc('simple.php?urlaction=visualisation&method=Diaporama&type=recherche_DOC','main');
	}
}



