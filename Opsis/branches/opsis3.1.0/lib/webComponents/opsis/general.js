//remplacement de la fonction trim qui ne fonctionne pas sous certains version d'IE
if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, ''); 
  }
}

// fonction de rebond (usage jquery pour éviter des ciblages de champs par leurs index)
function cherche(val,fld,lib,type, id) {
	if (typeof (id) == "undefined") id=100;
	$j("form#formRebond input[name^='chValues\\["+id+"\\]']").val(val)
	$j("form#formRebond input[name^='chFields\\["+id+"\\]']").val(fld.toLowerCase())
	$j("form#formRebond input[name^='chLibs\\["+id+"\\]']").val(lib)
	$j("form#formRebond input[name^='chTypes\\["+id+"\\]']").val(type)	
	
	$j("form#formRebond").submit();
}

//=== fonctions pour l'alimentation de "panier_count"
function updatePanierCount(str) {
	//alert(str);
	var myDom=importXML(str);
	var myBalise=myDom.getElementsByTagName('PAN_ID_ETAT');
	for (var i = 0; i < myBalise.length; i++) {
		if (myBalise[i].firstChild.nodeValue=='1') break;
	}
	myCount=myDom.getElementsByTagName('NB');
	panierCount=document.getElementById('panier_count');
	if (myCount && panierCount) panierCount.innerHTML=myCount[i].firstChild.nodeValue;
}

function viderPanier(id) {
	if (id>0) {
		var msg=str_lang.kConfirmViderpanier;
		if(document.getElementById('panier_count')) {
			msg=msg.replace(/notice/,document.getElementById('panier_count').innerHTML+' notice');
		}
		if(confirm(msg))
			return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&commande=remove_all&id_panier='+id,'retourViderPanier');
 	}
}

function retourViderPanier(str) {
	//alert(str);
	panierCount=document.getElementById('panier_count');
	if (panierCount)
		panierCount.innerHTML=0;
}
// ===



// fonction permettant de fermer une palette
function hideMe() {
    // check if variable exist (not null AND not undefined) to not trigger javascript ERROR
    if (!(typeof document.getElementById('chooser') === 'undefined' || document.getElementById('chooser')=== null
          || typeof window === 'undefined' || window=== null  )) {
        mychooser=window.parent.document.getElementById('chooser');
	mychooser.style.display='none';
    }
}

function getCookie(sName) {
        var oRegex = new RegExp("(?:; )?" + sName + "=([^;]*);?");
 
        if (oRegex.test(document.cookie)) {
                return decodeURIComponent(RegExp["$1"]);
        } else {
                return null;
        }
}

// ??? 
function addIdMedia(idmedia) {
	document.getElementById("chValues[1]").value=idmedia;
	document.form1.submit();
}
// ??? 
function goToPage(url) {
	document.location = url;
}



function hidePanel(){
	if(document.getElementById('sidecommande')) document.getElementById('sidecommande').style.display='none';
	
}

function showTab(tab_id)
{
	if(document.getElementById('desc'))
		document.getElementById('desc').style.display='none';
	
	if(document.getElementById('vote'))
		document.getElementById('vote').style.display='none';

	if(document.getElementById('story'))
		document.getElementById('story').style.display='none';
	
	if(document.getElementById('doc_acc'))
		document.getElementById('doc_acc').style.display='none';
	
	if(document.getElementById('extraits'))
		document.getElementById('extraits').style.display='none';
	
	if(document.getElementById('report'))
		document.getElementById('report').style.display='none';
	
	if(document.getElementById('transcription'))
		document.getElementById('transcription').style.display='none';
	
	if(document.getElementById('doc_fest'))
		document.getElementById('doc_fest').style.display='none';
	
	if(document.getElementById('comment'))
		document.getElementById('comment').style.display='none';
	
	if(document.getElementById(tab_id))
		document.getElementById(tab_id).style.display='block';
}

function updatePage () {
	if (pagerForm.rang.value!=pagerForm.page.value)
		pagerForm.page.value=pagerForm.rang.value
	return true;
}

function selectAll(tf){
	var checkedItems=document.getElementsByTagName('input');
	for (i=0;i<checkedItems.length;i++) {
		if (checkedItems[i].type=='checkbox' && checkedItems[i]!=tf && checkedItems[i].name.indexOf('checkbox')==0) {
			checkedItems[i].checked=tf.checked;
		}
	}
}

function refresh() {
	document.form1.commande.value="SAVE";
	submitForm();
}


// === fonctions de partage sur les reseaux sociaux
function shareFacebook(host,id_doc,titre_doc,description_doc)
{
	var url=host+'/video.php?urlaction=visualisation&type=document&id='+id_doc;

	FB.init({appId:'252385221547003',status: true, cookie: true, xfbml:true});

	var obj = 
	{
		method: 'feed',
		link: url,
		name: 'Facebook Dialogs',
		caption: titre_doc
		//description: description_doc
		
	};

	function callback(response)
	{
		//alert("Post ID: " + response['post_id']);
	}

	FB.ui(obj, callback);
	
	/*var url_facebook='http://www.facebook.com/dialog/feed?app_id=212825708840170&link='+encodeURIComponent(url)+'&name='+encodeURIComponent(titre_doc)+'redirect_uri='+encodeURIComponent('http://192.168.0.150/dev/ghislain/demo/trunk/index.php?urlaction=doc&id_doc=1&id_lang=FR')+'display=popup';
	//var url_facebook='http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(titre_doc);

	//alert('http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(titre_doc));
	
	window.open(url_facebook,'popup_facebook','status=no, scrollbars=no, menubar=no, width=600, height=180');*/
	
}

function shareTwitter(host,id_doc,titre_doc)
{
	var url_non_encodee=host+'/video.php?urlaction=visualisation&type=document&id='+id_doc
	var url=encodeURIComponent(url_non_encodee);
	var titre_url=encodeURIComponent(titre_doc); // titre encode pour l'url
	
	if ((titre_doc+' '+url_non_encodee).length>140)
		alert(str_lang.titre_trop_long_twitter);
	else
		window.open('http://twitter.com/intent/tweet?url='+url+'&text='+titre_url);
}

function shareLinkedIn(host,id_doc,titre_doc)
{
	var url=encodeURIComponent(host+'/video.php?urlaction=visualisation&type=document&id='+id_doc);
	var titre_url=encodeURIComponent(titre_doc); // titre encode pour l'url
	
	if (titre_doc.length>200)
		alert(str_lang.titre_trop_long_linkedin);
	else
		window.open('http://www.linkedin.com/shareArticle?mini=true&url='+url+'&title='+titre_url);
}
// ===



// === Fonctions menuDoc.inc ===
function changeRang(e,rang,doc_rows)
{
	var keynum;
	var keychar;
	var numcheck;
	
	if(window.event) // IE
	{
		keynum = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
	{
		keynum = e.which;
	}
	if (keynum==13)
		goRang(document.getElementById('rang'),rang,doc_rows);
}

function toggleMe(tab_id,panel) {
	if (myVideo && myVideo.UnSetSelection) {
		myVideo.UnSetSelection();
	}
	
	if (myPanel && myPanel._hasChanged) {ok=confirm(str_lang.kConfirmerAbandon); if (!ok) return;}
	if (myPanel) myPanel._hasChanged=false;
	
	//Le sidecommande qui comprend rightPanel doit suivre l onglet selectionne.
	sidecommandeObj=document.getElementById('sidecommande').cloneNode(true); //clonage du sidecommande
	
	//suppression du sidecommande original
	document.getElementById('sidecommande').parentNode.removeChild(document.getElementById('sidecommande')); 

	//suppression du icon_bar original
	if(document.getElementById('icon_bar')) document.getElementById('icon_bar').parentNode.removeChild(document.getElementById('icon_bar')); 

	//re création du sidecommande dans l onglet sélectionné
	tab=document.getElementById(tab_id);
	tab.appendChild(sidecommandeObj); 

	if(panel)
		showPanel(panel);
	else
		hidePanel(); 
	
	showTab(tab_id);
	//movePanelIconBar();
	
	toggleSelection(tab_id)
}


function toggleSelection(tab_id)
{
	if(myVideo)
	{
		if(myVideo.OW_OraoWeb_version==undefined)
		{
			if(tab_id=='extraits')
				myVideo.ShowSelectionSlider();
			else
				myVideo.HideSelectionSlider();
		}
	}
}

// === Fonction d'affichage de la progression d'un job par ajax
function updateProgressJob(id_job,redirect_url,onEndJob,always_show_bar) {
	if(typeof redirect_url == 'undefined'){
		redirect_url = '' ; 
	}
	if(typeof onEndJob == 'undefined'){
		onEndJob = null ; 
	}	
	if(typeof always_show_bar == 'undefined'){
		always_show_bar = null ; 
	}
	$j.ajax({
		type: "POST",
		url: "empty.php?urlaction=processJob&action=getProgression",
		data: {id_job: id_job},
		success: function(data){
			// console.log(data);
			parsed_data = JSON.parse(data);
			if(always_show_bar || parsed_data['PROGRESS'] != '0' && parseInt(parsed_data['ID_ETAT'],10) < 9 ){ // parsed_data['ID_ETAT'] == '2' 
				valeur=parseInt(parsed_data['PROGRESS'],10);
				if($j("#etat"+id_job+" #progressbar").length == 0 || $j("#etat"+id_job+" #progressbar span").length == 0){
					value_to_print = '<div id="progressbar" style="width:50px;height:16px"><span style="position:absolute;margin-left:10px; margin-top:0px;">'+valeur+'%</span></div>';
					$j("#etat"+id_job).html(value_to_print);
				}else{
					 $j("#etat"+id_job+" #progressbar span").html(valeur+"%");
				}
				$j( "#etat"+id_job+" div[id^='progressbar']").progressbar({
					value:valeur
				});
			}else{
				value_to_print = parsed_data['ETAT'];
				$j("#etat"+id_job).html(value_to_print);
			}
			
			
			if(parsed_data['ID_ETAT'] == '9') {
				//console.log("job fini elt exist #etat"+id_job+" "+$j("#etat"+id_job).length);
				if($j("#etat"+id_job).length>0){
					if(typeof (onEndJob) == 'function'){
						try{
							//console.log("udpateProgressJob call onEndJob");
							onEndJob(id_job);
						}catch(e){
							console.log("updateProgressJob - crash callback onEndJob :"+e);
						}
					}
					if (typeof redirect_url !=undefined && redirect_url != null && redirect_url != '' && window.location.href != redirect_url ){
						 document.location.href = redirect_url; 
					}
				}
			}else{
				// MS - on vérifie que l'élément permettant l'affichage de l'avancée du progres du job est toujours sur la page avant de continuer la boucle
				if($j("#etat"+id_job).length>0){
					setTimeout(function(){updateProgressJob(id_job,redirect_url,onEndJob,always_show_bar)},2000);
				}
			}
		}
	});
}


// fonction de mise à jour de la vignette lorsque le traitement d'import est terminé, 
// utilisée dans l'affichage du contenu d'un import (doc)
// est appelée en callback onEndJob de la fonction updateProgressJob
function importUpdateVignette(id_job_l){
	var id_doc = $j("#etat"+id_job_l).parents("tr").find("input[type='checkbox']").eq(0).val();
	var img_vignette = $j("#etat"+id_job_l).parents("tr").find(".bloc_image img").eq(0);
	if(typeof id_doc != 'undefined'){
		$j.ajax({
			url : 'empty.php?urlaction=loadDoc&id_doc='+id_doc, 
			success : function(data){
				vignette_path = $j(data).find("VIGNETTE").text();
				img_vignette.attr('style','max-width:100%;max-height:100%;');
				img_vignette.attr('src','makeVignette.php?image='+vignette_path);
			}
		});
	}
}



function displayHoverEllipsis(elem,classBubble){
	var bubble = document.createElement('div');
	bubble.className = classBubble;
	bubble.style.display = 'none';
	
	$j(elem).hover(
		function(event){
			try{
				// detection taille du texte par rapport à taille d'affichage
				if($j(elem)[0].scrollWidth > $j(elem).innerWidth()){
					bubble.innerHTML = $j(elem).text();
					$j(bubble).insertAfter(elem);
					$j(bubble).css('display','block');
					$j(bubble).css('position','absolute');
					$j(bubble).css('width','auto');
					$j(bubble).css('height','auto');
				}		
			}catch(e){
				// console.log(e);
			}
		}, 
		function(event){
			$j(bubble).css('display','none');
		}
	);	
}


// MS  - 06.07.15 - fonction compatible sur les browsers récents + IE < 9
function stopEvent(e,stopBubbling,stopDeftAction) {
	if (typeof stopBubbling == 'undefined'){
		stopBubbling = true;
	}
	if (typeof stopDeftAction == 'undefined'){
		stopDeftAction = false;
	}

	if(!e) var e = window.event;
 
	//e.cancelBubble is supported by IE -
	// this will kill the bubbling process.
	if(stopBubbling){
		e.cancelBubble = true;
	}
	if(stopDeftAction){
		e.returnValue = false;
	}
	//e.stopPropagation pour les nouveaux browsers
	if(stopBubbling){
		if ( e.stopPropagation ) e.stopPropagation();
	}
	if(stopDeftAction){
		if ( e.preventDefault ) e.preventDefault();		
	}	  

	return false;
}


function initSubmitToAjax(form_selector,test_function,conf){
	$j(form_selector).on('submit', function() {
		if(test_function(this)){
			$j(conf.loading_parent).addClass('loading');
			$j.ajax({
				url     : $j(this).attr('action'),
				method    : 'POST',
				data    : $j(this).serialize(),
				success : function( data ) {
					$j(conf.result_element).html(data);
					$j(conf.loading_parent).removeClass('loading');
					initSubmitToAjax(form_selector,test_function,conf);
				},
				error   : function( xhr, err ) {
					console.log(err);
				}
			});
		}
		return false;
	});
}


function submitToAjax(form_selector,success_fn,error_fn){
	if(typeof success_fn != 'function'){
		success_fn = null ; 
	}
	if(typeof error_fn != 'function'){
		error_fn = null ; 
	}
	$j.ajax({
		url     : $j(form_selector).attr('action'),
		method    : 'POST',
		data    : $j(form_selector).serialize(),
		success : success_fn,
		error   : error_fn
	});
}

// 08/06/2018 permette la desactivation d'un compte en le passant a l'etat 0
function desinscription(){
	document.getElementById("us_id_etat_usager").value = "0";
	document.getElementsByName('commande')[0].value = "UNSUBSCRIBE"
}




