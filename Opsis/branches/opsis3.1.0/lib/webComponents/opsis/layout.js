


function updateLayoutOpts(jsObj, callback){
	
	ajax_url = "empty.php?urlaction=sessLayout&action=update" ; 
	
	// console.log("jsObj",jsObj,JSON.stringify(jsObj));
	$j.ajax({
		url : ajax_url, 
		method : 'POST',
		data : {
			'sessLayout' : JSON.stringify(jsObj)
		},
		success : function(data){
			// console.log("updateLayoutOpts data ",data);
			try{
				if(typeof callback == 'function'){
					callback();
				}
			}catch(e){
				console.log("updateLayoutOpts - crash call to callback :"+e);
			}
		}
	});
	
}