// =============== docDetail
function chkFormFields (myform) {
	msg='';
	var i;
	var k;
	var el_next;



//pour les champs multivalué, valeur rempli si un div est inclus dans le div qui contient le mandatory
	div= document.getElementsByTagName("div");
	for (k=0;k<div.length;k++) {
		if (!div[k].getAttribute('mandatory')) continue;
				if($j(div[k]).children("div").length) {  }
				else {
				lib = $j(div[k]).parents(".val_champs_form").prevAll(".label_champs_form").eq(0).text();
				lib = lib.replace('*', '');
				msg=msg+"\n"+lib;

				}
	}

	if (typeof myform != 'undefined'){
		arrElts=$j(myform).get(0).getElementsByTagName('*');
	}else{
		arrElts=$j("form[name='form1']").get(0).getElementsByTagName('*');
	}
	for (i=0;i<arrElts.length;i++) {
		if (!arrElts[i].getAttribute('mandatory')) continue; //test si obligatoire
			if (arrElts[i].value != undefined && arrElts[i].value.Trim()=='') {
				switch(arrElts[i].getAttribute('label')){
					default :
						lib=arrElts[i].getAttribute('label');
						break;
				}

				// tentative de récupérer le label via jquery si il n'a pas été trouvé auparavant
				if(lib==null ){
					lib = $j(arrElts[i]).parents(".val_champs_form").prevAll(".label_champs_form").eq(0).text();
				}

				msg=msg+"\n"+lib;
				arrElts[i].className='errorjs';
			} else {
				arrElts[i].className='';
			}
	}
	if (msg!='') {alert(str_lang.kJSErrorOblig+msg);return false; }
	//MAJ champ DOC_DATE
	if(document.getElementById('doc_date')){
		if(document.getElementById('doc_date_pv_debut')) doc_date=document.getElementById('doc_date_pv_debut').value;
		else if(document.getElementById('doc_date_diff')) doc_date=document.getElementById('doc_date_diff').value;
		else if(document.getElementById('doc_date_prod')) doc_date=document.getElementById('doc_date_prod').value;
		else doc_date="";
		document.getElementById('doc_date').value=doc_date;
	}
	formChanged=false;
	//sendData('GET','ajax.php','xmlhttp=1&commande=getDocCote&doc_cote='+document.getElementById('doc_cote').value+'&id_doc='+document.getElementById('id_doc').value,'rtn_getDocCote');
	return true;
}

function rtn_getDocCote(str){
	//alert(str);
	myXML=importXML(str);
	_xmlFlds=myXML.getElementsByTagName('DOC_COTE');
	if(_xmlFlds){
		_xmlFld=_xmlFlds[0].firstChild;
		document.form1.doc_cote.value=_xmlFld.nodeValue;
		document.form1.commande.value='SAVE';
		document.form1.submit();
	}
}

//by LD : pour ajouter une valeur dynamiquement ? un tableau avec DOM.
// multi : permet d'avoir plusieurs fois la même valeur
function addValue(id_champ,valeur,idx,role,autocomplete,multi) {
	formChanged=true;
	if(id_champ.indexOf('$')== -1){
		fld=document.getElementById(id_champ);
		if (fld.value!='') fld.value+=", ";
		fld.value+=valeur;
	} else {
		arrField=id_champ.split('$');
		type=arrField[0];
		cntFields=0;
		if (document.getElementById(type+'$limit')) limit=document.getElementById(type+'$limit').value; else limit=250;
		allowdouble=document.getElementById(type+'$allowdouble');

		if(idx==0){
			var d=new Date();
			idx="T"+d.getTime();
		}

		// ajouter test sur valeur existante
		prt=document.getElementById(type+'$tab');
			if ( typeof(multi) == "undefined" ) {
				for(i=0;i<prt.childNodes.length;i++) {
					chld=prt.childNodes[i];

					if (chld.id && chld.id.indexOf('$DIV$')!=-1) cntFields++;
					if (cntFields==limit) {
						alert (limit+str_lang.kDOMlimitvalues);
						return false;
						}

					if (chld.id==type+'$DIV$'+idx && !allowdouble ) {
						alert(str_lang.kDOMnodouble); return false;

					}
				}
		}
		btnAdd=document.getElementById(type+'$add');

		newFld=document.createElement('DIV');
		newFld.id=type+'$DIV$'+idx;
		newFld.name=type+'$DIV$'+idx;
		newFld.className=prt.className;
		addRow=new Array(newFld.id,valeur,idx,role)
		str=eval('add_'+type+'(addRow)');
		newFld.innerHTML=str[1];
		if (btnAdd) {prt.insertBefore(newFld,btnAdd);} else {prt.appendChild(newFld);}
		if(autocomplete){
			if(document.getElementById(newFld.id+"t_doc_lex[][LEX_TERME]")) {
				eval("document.getElementById('"+newFld.id+"t_doc_lex[][LEX_TERME]').onfocus=function () {initAutoComplete(this.form, this,null,'blank.php?urlaction=chercheMots&amp;type="+autocomplete+"')}");
				document.getElementById(newFld.id+"t_doc_lex[][LEX_TERME]").focus();
			}
			if(document.getElementById(newFld.id+"t_doc_lex[][PERS_NOM]")) {
				eval("document.getElementById('"+newFld.id+"t_doc_lex[][PERS_NOM]').onfocus=function () {initAutoComplete(this.form, this,null,'blank.php?urlaction=chercheMots&amp;type="+autocomplete+"')}");
				document.getElementById(newFld.id+"t_doc_lex[][PERS_NOM]").focus();
			}
			if(document.getElementById(newFld.id+"t_doc_val[][VALEUR]")) {
				eval("document.getElementById('"+newFld.id+"t_doc_val[][VALEUR]').onfocus=function () {initAutoComplete(this.form, this,null,'blank.php?urlaction=chercheMots&amp;type="+autocomplete+"')}");
				document.getElementById(newFld.id+"t_doc_val[][VALEUR]").focus();
			}
		}
	}
}


function removeVersion(version) {

	if (confirm(str_lang.kConfirmJSDocSupprVersion)) {
		document.form1.commande.value="SUP_DOC";
		document.form1.version2suppr.value=version;
		document.form1.submit();
		}
}

function removeDoc(url) {
	if (confirm(str_lang.kConfirmJSDocSuppression)) {
		if(window.jQuery && $j("#saisieForm").length == 1){
			$j("#saisieForm").get(0).commande.value="SUP_DOC";
			$j("#saisieForm").get(0).page.value=url;
			$j("#saisieForm").get(0).submit();
		}
		else if(typeof document.form1.commande != "undefined" && typeof document.form1.page != "undefined"){
			document.form1.commande.value="SUP_DOC";
			document.form1.page.value=url;
			document.form1.submit();
		} 
	}
}

function duplicateDoc(url) {
	var form1 = $j("form[name='form1']").get(0);
	if(chkFormFields(form1)){
		if (confirm(str_lang.kConfirmJSDocDuplication)) {
			form1.commande.value="DUP_DOC";
			if (url!='') form1.page.value=url;
			form1.submit();
		}
	}
}

function traduireDoc(url) {
	if(chkFormFields(document.form1)){
		if(typeof document.form1.doc_trad != "undefined"){
			document.form1.doc_trad.value="1";
			document.form1.commande.value="SAVE_DOC";
			if (url!='') document.form1.page.value=url;
			document.form1.submit();
		}else if (window.jQuery && $j("#saisieForm").length == 1){
			$j("#saisieForm").get(0).doc_trad.value="1";
			$j("#saisieForm").get(0).commande.value="SAVE_DOC";
			if (url!='') $j("#saisieForm").get(0).page.value=url;
			$j("#saisieForm").get(0).submit();
		}
	}
}


// =============== docImageur

function setAsVignette(id_image,image) {
	var addr_serveur=ow_const.kCheminHttpMedia;
	all_images=document.getElementById('planche').getElementsByTagName('div'); //raz classe pour ttes les images
	for (i=0;i<all_images.length;i++) {
		all_images[i].className='imgNormal'
	}
	if (document.form2.vignette.value==id_image) {//déselection
		img2update=ow_const.imgUrl+'vignette_blank.png';
		document.form2.vignette.value='';
		$j('#poster').attr('src',img2update);
	}
	else
	{
		document.form2.vignette.value=id_image; //sélection
		_pic=getChildById(image,'img$');
		if (_pic)
			img2update=_pic.src;
		image.className='imgSelected';
		// remplacement de l'image rendu plus propre => on garde tout les paramètres de makevignette mis à part
		current_poster =  $j('#poster').attr('src');
                // check if variable exist (not null AND not undefined) to not trigger javascript ERROR
                if (!(typeof current_poster === 'undefined' || current_poster=== null)) {
                new_poster = current_poster.replace(/image=.*?&|image=.*$/,"image="+img2update.substring(addr_serveur.length)+"&");
                $j('#poster').attr('src',new_poster);
                }

	}

	_vignette=document.getElementById('vignette'); //vignette du doc

	if (_vignette) {_vignette.src=img2update;}
	myPanel.hasChanged(image);
	$j(image).attr('style','');

}

function addPictureToStoryboard(str) {
	myDom=importXML(str);
	//alert(str);
	//Vérif de base : si pas d ID, on abandonne
	myBalise=myDom.getElementsByTagName('ID_IMAGE');
	if (!myBalise[0] || !myBalise[0].firstChild){ //Erreur de génération
		myImage=document.getElementById('row$cloned');
		myImage.parentNode.removeChild(myImage); //suppr vignette
		showAlert(str_lang.kErrorImageurCapture.replace('"','\"'),'alertBox');
		return;
	}

	id_image=myBalise[0].firstChild.nodeValue;
	myBalise=myDom.getElementsByTagName('IM_FICHIER');
	im_fichier=myBalise[0].firstChild.nodeValue;
	myBalise=myDom.getElementsByTagName('IM_CHEMIN');
	im_chemin=myBalise[0].firstChild.nodeValue;


	myImage=document.getElementById('row$cloned');
	myImage.id='row$';
	myPic=getChildById(myImage,'img$');
	imgFullPath=ow_const.storyboardChemin+im_chemin+"/"+im_fichier;
	//myPic.style.width='160px';
	myPic.setAttribute('width', '160px');
	myPic.src=imgFullPath;
	//eval("myPic.onclick=function () {popupImage('"+imgFullPath+"','< ?=str_replace("'","\'",kVisioNomDefautExtrait)? >');}");


	myBtnVignette=getChildById(myImage,'setVignette$');
	eval("myBtnVignette.onclick=function () {setAsVignette('"+id_image+"',this.parentNode);}");
	setAsVignette(id_image,myImage); //Autoselection en vignette

	myBalise=myDom.getElementsByTagName('msg_out');

}

function capturePicture() {
	id_mat=document.form2.id_mat.value;
	if (!id_mat) alert('no mat');
	if (!myPanel || !myPanel.video) {
		showAlert(str_lang.kErrorImageurCaptureNoPlayer,'alertBox');
		return false;
	}
	tc=myPanel.video.GetCurrentLongTimeCode();

	//Scan TC image existants pour ne pas avoir de doublon
	all_fields=document.getElementById('form2').getElementsByTagName('input'); //raz classe pour ttes les images
	for (i=0;i<all_fields.length;i++) {
		if (all_fields[i].name=='IM_TC[]' && all_fields[i].value==tc){
			showAlert(str_lang.kErrorImageurCaptureExisteDeja,'alertBox');
			return;
		}
	}

	var newRow = myPanel.addExtrait(); //Ajout nouvelle image et scroll auto en bas de div
	var objDiv = document.getElementById("planche");
	objDiv.scrollTop = objDiv.scrollHeight;

    // Affectation TC
    _n=getChildById(newRow,'tcin$');
    if (_n) _n.value=tc;

	all_divs=document.getElementById('form2').getElementsByTagName('div'); //raz classe pour ttes les images
	tcsearch = true;
	for (i=0;tcsearch && i<all_divs.length;i++) {
		fields = all_divs[i].getElementsByTagName('input');
		for (j=0;tcsearch && j<fields.length;j++) {
			if (fields[j].name=='IM_TC[]' && convertTcToMs(fields[j].value) > convertTcToMs(tc)){
				$j(all_divs[i]).before(newRow);
				tcsearch = false;
			}
		}
	}
//on enleve 2 div car la ligne en cours d ajout ne doit pas etre prise en compte, et décalage de 1
	if(tcsearch == true) {
		$j(all_divs[all_divs.length - 2]).after(newRow);
	}
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=processStoryboard&id='+encodeURIComponent(id_mat)+'&tc='+encodeURIComponent(tc)+'&from=materiel&commande=grab','addPictureToStoryboard');
}

function generateStoryboard() {
	err=0;fld=0;
	if(document.getElementById('sb_rate')) document.form2.sb_rate.value=document.getElementById('sb_rate').value;
	if(document.getElementById('sb_number')) document.form2.sb_number.value=document.getElementById('sb_number').value;
	if (myPanel._hasChanged) if (!confirm(myPanel.tooltips['rp_loose_changes'])) return;
	//Vérif rate ou number : il faut qu au moins un champ existe et soit non nul
	if (document.form2.sb_rate) {fld++;if(document.form2.sb_rate.value=='') err++;}
	if (document.form2.sb_number) {fld++;if(document.form2.sb_number.value=='') err++;}
	if (fld==0 || (fld==1 && err>0) || (fld==2 && err==2) ) {alert(str_lang.kErrorJSImageurNoRate);return;}

	showAlert(str_lang.kImageurWaitGeneration,'alertBox','fade',0,0,120);
	new Effect.Opacity(document.getElementById('form2'),{from:1,to:0.2,duration:0.3});
	new Effect.Opacity(document.getElementById('planche'),{from:1,to:0.2,duration:0.3});
	document.form2.commande.value='generateStoryboard';
	document.form2.submit();
}

function generateStoryboardByFrontal() {
	document.form2.commande.value='generateStoryboardByFrontal';
	document.form2.submit();
}

function generateMosaic() {
	err=0;fld=0;
	showAlert(str_lang.kMosaicWaitGeneration,'alertBox','fade',0,0,120);
	new Effect.Opacity(document.getElementById('form2'),{from:1,to:0.2,duration:0.3});
	new Effect.Opacity(document.getElementById('planche'),{from:1,to:0.2,duration:0.3});
	document.form2.commande.value='generateMosaic';
	document.form2.submit();
}

function downloadPhotoShoot() {
	if (id_mat_master==0) {
		alert('There is no material for this action');
		return false;
	}
	if (!myPanel || !myPanel.video) {
		showAlert(str_lang.kErrorImageurCaptureNoPlayer.replace('"','\"'),'alertBox');
		return false;
	}
	tc=myPanel.video.GetCurrentLongTimeCode();
	
	showAlert(str_lang.kImageWaitCapture,'alertBox','fade',0,0,10);
	
	return !sendData('GET','blank.php','xmlhttp=1&urlaction=processStoryboard&id='+encodeURIComponent(id_mat_master)+'&tc='+encodeURIComponent(tc)+'&from=materiel&commande=grab','downloadPicture');
}


function downloadPicture(str) {
	el=document.getElementById('alertBox');
	el.style.display="none";
	
	myDom=importXML(str);
	myBalise=myDom.getElementsByTagName('ID_IMAGE');
	if (!myBalise[0] || !myBalise[0].firstChild){ //Erreur de génération
		myImage=document.getElementById('row$cloned');
		myImage.parentNode.removeChild(myImage); //suppr vignette
		showAlert(str_lang.kErrorImageurCapture.replace('"','\"'),'alertBox');
		return;
	}
	
	id_image=myBalise[0].firstChild.nodeValue;
	myBalise=myDom.getElementsByTagName('IM_FICHIER');
	im_fichier=myBalise[0].firstChild.nodeValue;
	myBalise=myDom.getElementsByTagName('IM_CHEMIN');
	im_chemin=myBalise[0].firstChild.nodeValue;
	
	document.getElementById('frame_livraison').src = "download.php?image=" + im_chemin + im_fichier;
	
}



// =============== docAccSaisie
// MS - non utilisée actuellement
function deleteAll() { //ne fonctionne pas pour le moment
	images = document.getElementById("planche").getElementsByTagName('div');

	for (i=0;i<images.length;i++) {
		if (images[i].id=='row$') {
			if (images[i].style.display=='none') {
				images[i].style.display='block';
			} else {
				images[i].style.display='none';
			}
		}
	}

}

// MS - non utilisée actuellement
function convertTcToMs(tc) {
	arrtc = tc.split(":");
	var ms = parseInt(arrtc[0]) * 360000 + parseInt(arrtc[1]) * 6000 + parseInt(arrtc[2]) * 100 + parseInt(arrtc[0]);
	return ms;
}


function onlyCheck(elt)
{
	checkboxes=document.getElementsByTagName('input');

	for (i=0;i<checkboxes.length;i++)
	{
		if (checkboxes[i].type=='checkbox' && checkboxes[i].id!=elt.id)
			checkboxes[i].checked=false;
	}

}

function removeDocAcc(id_div) {

	if (!confirm(str_lang.kConfirmerSuppression))
		return false;

	dumb=id_div.split('§');
	id=dumb[1];
	fld=document.getElementById(id_div);
	copy=document.getElementById('selVal'+id);
	paste=document.getElementById('selVal'+(id-1));

	copy2=document.getElementById('selVignette'+id); //idem avec les checkbox
	paste2=document.getElementById('selVignette'+(id-1));


	// Les select de valeur ne sont présents que sur la DERNIERE ligne en cas de versions multiples.
	// Donc SI on retire cette ligne, il faut recopier ces select sur une autre ligne de version.

	if (copy && paste && copy.childNodes.length>0 && paste.childNodes.length==0) { // il y a des select sur cette ligne et AUCUN sur la précédente !

		while (copy.childNodes.length>0) { // Copie du contenu du TD en bouclant sur les fils.
			itm=copy.childNodes[0];        // NOTE : pas de innerHTML car buggy sous IE ET ca ne préserve pas les valeurs sélectionnées
			copy.removeChild(itm);
			paste.appendChild(itm);
		};
	}

	if (copy2 && paste2 && copy2.childNodes.length>0 && paste2.childNodes.length==0) { // il y a des checkbox sur cette ligne et AUCUN sur la précédente !

		while (copy2.childNodes.length>0) { // Copie du contenu du TD en bouclant sur les fils.
			itm=copy2.childNodes[0];        // NOTE : pas de innerHTML car buggy sous IE ET ca ne préserve pas les valeurs sélectionnées
			copy2.removeChild(itm);
			paste2.appendChild(itm);
		};
	}

	fld.parentNode.removeChild(fld); // Suppression de la ligne.
}




// =============== docMat

function addMatAJAX(str) { // Récupération du XML
	xmlMat=importXML(str);

	myBalise=xmlMat.getElementsByTagName('ID_MAT');
	var id_mat=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild

	myBalise=xmlMat.getElementsByTagName('MAT_NOM');
	if (myBalise[0].firstChild)
	var mat_nom=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_nom='';

	myBalise=xmlMat.getElementsByTagName('MAT_FORMAT');
	if (myBalise[0].firstChild)
	var mat_format=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_format='';

	myBalise=xmlMat.getElementsByTagName('MAT_TCIN');
	if (myBalise[0].firstChild)
	var mat_tcin=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_tcin ='';

	myBalise=xmlMat.getElementsByTagName('MAT_TCOUT');
	if (myBalise[0].firstChild)
	var mat_tcout=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_tcout ='';


	if (document.getElementById('t_doc_mat$limit')) limit=document.getElementById('t_doc_mat$limit').value; else limit=20;
	allowdouble=document.getElementById('t_doc_mat$allowdouble');

	myTab=document.getElementById('t_doc_mat$tab');

	cntFields=0;
	for(i=0;i<myTab.childNodes.length;i++) { //vérif de la limite de DOC / matériel et des doublons
		chld=myTab.childNodes[i];

		if (myTab.id && chld.nodeType==1 &&  chld.id.indexOf('$DIV$')!=-1) cntFields++;
		if (cntFields==limit) {
			alert (limit+str_lang.kDOMlimitvalues);
			return false;
			}

		if (chld.id=='t_doc_mat$DIV$'+id_mat && !allowdouble) {
			alert(str_lang.kDOMnodouble); return false;
		}
	}

	$urlmat="index.php?urlaction=matSaisie&id_mat="+id_mat;

	arrRow=new Array('t_doc_mat$DIV$'+id_mat,id_mat,mat_nom,$urlmat,'',mat_format, mat_tcin, mat_tcout);
	out=add_t_doc_mat(arrRow);
	newFld=document.createElement('DIV');
	newFld.innerHTML=out[0];

	myBtn=document.getElementById('toolbar');
	if (myBtn) {myTab.insertBefore(newFld,myBtn);} else {myTab.appendChild(newFld);}

	xmlDoc=null; //vidage mémoire

}

function addDocMatAJAX(str) { // Récupération du XML
	xmlMat=importXML(str);

	myBalise=xmlMat.getElementsByTagName('ID_MAT');
	var id_mat=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild

	myBalise=xmlMat.getElementsByTagName('MAT_NOM');
	if (myBalise[0].firstChild)
	var mat_nom=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_nom='';

	myBalise=xmlMat.getElementsByTagName('MAT_FORMAT');
	if (myBalise[0].firstChild)
	var mat_format=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_format='';

	myBalise=xmlMat.getElementsByTagName('MAT_TCIN');
	if (myBalise[0].firstChild)
	var mat_tcin=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_tcin ='';

	myBalise=xmlMat.getElementsByTagName('MAT_TCOUT');
	if (myBalise[0].firstChild)
	var mat_tcout=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var mat_tcout ='';

	myBalise=xmlMat.getElementsByTagName('ID_DOC_MAT');
	if (myBalise[0].firstChild)
	var id_doc_mat=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
	else var id_doc_mat ='';


	if (document.getElementById('t_doc_mat$limit')) limit=document.getElementById('t_doc_mat$limit').value; else limit=20;
	allowdouble=document.getElementById('t_doc_mat$allowdouble');

	myTab=document.getElementById('t_doc_mat$tab');

	cntFields=0;
	for(i=0;i<myTab.childNodes.length;i++) { //vérif de la limite de DOC / matériel et des doublons
		chld=myTab.childNodes[i];

		if (myTab.id && chld.nodeType==1 &&  chld.id.indexOf('$DIV$')!=-1) cntFields++;
		if (cntFields==limit) {
			alert (limit+str_lang.kDOMlimitvalues);
			return false;
			}

		if (chld.id=='t_doc_mat$DIV$'+id_mat && !allowdouble) {
			alert(str_lang.kDOMnodouble); return false;
		}
	}

	$urlmat="index.php?urlaction=matSaisie&id_mat="+id_mat;

	arrRow=new Array('t_doc_mat$DIV$'+id_mat,id_mat,mat_nom,$urlmat,id_doc_mat,mat_format, mat_tcin, mat_tcout);
	out=add_t_doc_mat(arrRow);
	newFld=document.createElement('DIV');
	newFld.innerHTML=out[0];

	myBtn=document.getElementById('toolbar');
	if (myBtn) {myTab.insertBefore(newFld,myBtn);} else {myTab.appendChild(newFld);}

	xmlDoc=null; //vidage mémoire

}
// ============= matSaisie.inc.php
	function addDocAJAX (str) {
		myXML=importXML(str);

		_row=myPanel.addExtrait();

		//Affectation des valeurs.
		_xmlFlds=myXML.getElementsByTagName('DOC_TITRE');
		_xmlFld=_xmlFlds[0].firstChild;
		getChildById(_row,'ext_titre$').value=_xmlFld.nodeValue;

		_xmlFlds=myXML.getElementsByTagName('ID_DOC');
		_xmlFld=_xmlFlds[0].firstChild;
		getChildById(_row,'id_doc$').value=_xmlFld.nodeValue;

		_xmlFlds=myXML.getElementsByTagName('DOC_COTE');
		_xmlFld=_xmlFlds[0].firstChild;
		getChildById(_row,'doc_cote$').value=_xmlFld.nodeValue;

		_xmlFlds=myXML.getElementsByTagName('DOC_ID_FONDS');
		_xmlFld=_xmlFlds[0].firstChild;
		getChildById(_row,'doc_id_fonds$').value=_xmlFld.nodeValue;

		_xmlFlds=myXML.getElementsByTagName('DOC_ID_TYPE_DOC');
		_xmlFld=_xmlFlds[0].firstChild;
		getChildById(_row,'doc_id_type_doc$').value=_xmlFld.nodeValue;

		_xmlFlds=myXML.getElementsByTagName('VIGNETTE');
		_xmlFld=_xmlFlds[0].firstChild;
		if (_xmlFld) getChildById(_row,'vignette$').src= ow_const.kCheminHttpMedia +_xmlFld.nodeValue;
	}

// ============= FondsSaisie
// Fonctions d'interaction de la liste de groupes dans la saisie d'un fonds
function addGroupeLine(str) {
  addRow(str,document.getElementById('groupeListe'));
}

function addRow(str,myTable) {
//alert(str);
  if(str.indexOf('?>')!=-1) str = str.substring(1+str.indexOf('>')); //on vire l'entete php / xml
  x=document.createElement('tr');
  x.className='altern'+(myTable.rows.count % 2)
  myTable.appendChild(x);
  _tmpstr=str;
  _idx=0;
  _cellIdx=0;
  while (_tmpstr.indexOf('<td')!=-1) {
    _idx=_tmpstr.indexOf('<td');
    _tmpstr=_tmpstr.substr(_idx+3);
    _argus=_tmpstr.indexOf('>');
    _closetag=_tmpstr.indexOf('</td>');
    myStr=_tmpstr.substring(_argus+1,_closetag);
    _cell=x.insertCell(_cellIdx);
    _cell.innerHTML=myStr;
    _cell.className='resultsCorps';
    _cellIdx++;
  }
}

function removeRow(elt) { //ote une ligne d'un tableau via DOM
  _myTR=elt.parentNode.parentNode;
  if (_myTR.nodeName!='TR') return false;
  _myTR.parentNode.removeChild(_myTR);
}


function addGroupe(field,val,id) {
  if (document.getElementById('t_groupe_fonds§'+id)) {
      alert(str_lang.kJSErrorValeurExisteDeja);
      return false;}
  return !sendData('GET','blank.php','urlaction=getLines&type=groupe&xmlhttp=1&id='+id,'addGroupeLine');
}
// ===

// === menuMatSaisie.inc.php

function confirmExit()
{
	if (formChanged)
		return str_lang.kJSConfirmExit;
}

var indexClicked; //Dernier bouton "Index" précision cliqué
function fillPrecision(dumb,myVal,myID) {
	if (!indexClicked) return false; //On a bien un bouton cliqué
	//Si le noeud DOM précédent est un INPUT, on copie la valeur en provenance de la palette dedans
	//CONDITION : il faut que le bouton INDEX suive immédiatement le champ INPUT
	if(indexClicked.previousSibling.nodeName=='INPUT') indexClicked.previousSibling.value=myVal;

}
