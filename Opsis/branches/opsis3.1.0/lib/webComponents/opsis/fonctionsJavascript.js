
// VP : 30/11/2005 : Type thesaurus dans showAndInitRechInd()
// VP : 22/11/2005 : Test existance dans MaJChamp()
// VP : 07/11/2005 : Modification retour valeurs dans MaJChamp()
// XS : 18/10/2005 : Ajout de la fonction "explodeArray()" + Simplification du traitement des index, utilisation de la seule fonction "showAndInitRechInd()"
// XS : 13/10/2005 : Modif de MaJChamp pour que les valeurs du champs input se concatennent
// XS : 05/10/2005 : Modification de la  fonction "removeLine()", ajout d'un param�tre "type" + Ajout de la gestion des cancels (retour de la fonction "confirm()") dans "removeLine()" et "removeAll()"
// XS : 27/09/2005 : Fonction affiche/cache qui permet d'afficher/chacher des �l�ments + Cr�ation de "removeAll()"
// JC : 17/06/2005 : rajout d'un fonction pour se connecter facilement (d�mo opsis uniquement)
// JC : 01/06/2005 : rajout de quelques fonctions pour la recherche avanc�e
// XB : 08/01/2014 : rajout de fonction pop up et mail
// B.RAVI : 2016-04-07 : ajout de fonction qui permet de changer le sous-titre actif sur player vidéo HTML5 (OWH_player)
function showFormModal(id,classname_to_add) {

	$j("#popupModal").children("#frameModal").append($j("#"+id).clone());
	$j("#popupModal").children("#frameModal").children("#"+id).css("display","block");
	if(typeof classname_to_add != "undefined"){
		$j("#popupModal").children("#frameModal").addClass(classname_to_add);
	}
	$j("body").css("overflow","hidden");
	$j("#popupModal").css('display', 'block');

}

function closePopup(clear_frameModal) {
	if(typeof clear_frameModal == "undefined"){
		clear_frameModal = false ;
	}
	$j("#popupModal").hide();
	if(clear_frameModal){
		$j("#popupModal").children("#frameModal").html("");
		$j("#popupModal").children("#frameModal").removeAttr('class');

	}
	$j("body").css("overflow","auto");
}

function closeMailForm(){
 		//$j("#mailBloc").slideUp("fast");
 		closePopup();
	}

function afficheMailForm(){
	//showFormModal("formMailSelection");
    $j( "#formMailSelection" ).dialog({
      height:300,
      width: 500,
	  title: "Envoyer la sélection"
    });


	}

function sendMailSubmit() {
		if (document.mailForm.email_to_send.value=='') {alert('Vous devez renseigner l\'adresse email du destinataire de cette sélection.');return;}

		if (!confirm('Etes-vous sûr(e) de vouloir envoyer cette sélection à  '+ document.mailForm.email_to_send.value + ' ?')) return;
		//Transfert des valeurs du mini formulaire vers le vrai formulaire
		document.form1.email_to_send.value=document.mailForm.email_to_send.value;
		document.form1.corpsMail.value=document.mailForm.corpsMail.value;
		document.form1.objetMail.value=document.mailForm.objetMail.value;
		if (document.mailForm.copyForExp.checked == true) {
			document.form1.copyForExp.value='1';
		} else {
			document.form1.copyForExp.value='0';
		}
		document.form1.commande.value='SEND_MAIL';
		submitForm();

	}

/** Récupère la valeur d'un paramètre dans une url. IN: param, OUT:valeur (string) **/
function getURLParam(strParamName){
  var strReturn = "";
  var strHref = window.location.href;
  if ( strHref.indexOf("&") > -1 ){
    var strQueryString = strHref.substr(strHref.indexOf("&")).toLowerCase();
    var aQueryString = strQueryString.split("&");
    for ( var iParam = 0; iParam < aQueryString.length; iParam++ ){
      if (
aQueryString[iParam].indexOf(strParamName + "=") > -1 ){
        var aParam = aQueryString[iParam].split("=");
        strReturn = aParam[1];
        break;
      }
    }
  }
  return strReturn;
}


function checkAll() {
    // fonction appelee lors du clic sur une checkbox
    var compteur = 0;
    for (var j = 1; j <= document.form1.length-1; j++) {
    box = eval("document.liste.checkbox" + j);
    if (box.checked == true) compteur += 1;
   }
   //alert("You selected " + compteur + " boxes.");
    compteurPanier(compteur);
}

function compteurPanier(valeur){
    // fonction qui affiche dans le compteur du panier le nombre de document selectionnes
    //if(valeur!=null) document.all['compteur'].innerHTML=valeur;
    if(valeur!=null) document.panier.compteur.value=document.panier.compteurref.value+valeur;
    else document.write('0');
}

function reloadIframe(id){
    var url;
    box = eval("document.documentSelection.checkbox" + id);
    if(box.checked==true) url="iframePanier.php?ajouteDoc="+id;
    else url="iframePanier.php?enleveDoc="+id;
    document.getElementById('framePanier').src=url;
}

function ajouterPanier(id){
    document.getElementById('framePanier').src="iframePanier.php?ajouteDoc="+id;
}

function addItem() {
    document.form1.commande.value="ADD";
    document.form1.submit();
}

function removeVersion(version, form) {
	if (typeof(form)=='undefined') {
		form=document.getElementsByName("form1")[0];
	}
	if (confirm(str_lang.kConfirmJSDocSupprVersion)) {
		form.commande.value="SUP";
		form.version2suppr.value=version;
		form.submit();
		}
}

function removeItem(url, form) {
	if (typeof(form)=='undefined') {
		form=document.getElementsByName("form1")[0];
	}
    if (confirm(str_lang.kConfirmerSuppression)) {
    	form.commande.value="SUP";
    	form.page.value=url;
        form.submit();
    }
}

function purgeItem(url, form) {
	if (typeof(form)=='undefined') {
		form=document.getElementsByName("form1")[0];
	}
    if (confirm(str_lang.kConfirmPurgeFichier)) {
        form.commande.value="PURGE";
        form.page.value=url;
        form.submit();
	}
}

function purgeFichier(url,form) {
	if (typeof(form)=='undefined') {
		form=document.getElementsByName("form1")[0];
	}
	if (confirm(str_lang.kConfirmJSFichierSuppression)) {
        form.commande.value="PURGE_FICHIER";
        if (url && url!='') form.page.value=url;
        form.submit();
	}
}

function duplicateItem(url, form) {
	if (typeof(form)=='undefined') {
		form=document.getElementsByName("form1")[0];
	}
     if (confirm(str_lang.kConfirmJSDocDuplication)) {
        form.commande.value="DUP";
        if (url && url!='') form.page.value=url;
        form.submit();
     }
}

function save() {
    document.form1.commande.value="SAVE";
    document.form1.submit();
}

function saveAndGo(url) {
	// Le formulaire doit avoir un champ save_action qui détermine le type de sauvegarde
	// à effectuer (ex : doc, sequence, matériel, etc...)
	if (!document.form1.save_action) commande="SAVE";
	else commande=document.form1.save_action.value;
    document.form1.commande.value=commande;
    document.form1.page.value=url;
    document.form1.submit();
}

function chkFormFields (myform) {
	msg='';
	var i;
	if (typeof(myform)=='undefined') {
		myform=document.form1;
	}
	//arrElts=document.form1.getElementsByTagName('*');
	arrElts=myform.getElementsByTagName('*');
	for (i=0;i<arrElts.length;i++) {
		if (!arrElts[i].getAttribute('mandatory')) continue; //test si obligatoire

		if (arrElts[i].value.Trim()=='') {
			switch(arrElts[i].getAttribute('label')){
				default :
					lib=arrElts[i].getAttribute('label');
					break;
			}
			msg=msg+' '+lib+'\n';
			arrElts[i].className='errorjs';
		} else {
			arrElts[i].className='';
		}
	}
	if (msg!='') {alert(str_lang.kJSErrorOblig+msg);return false; }
	return true;
}

function saveIfAndGo(url){
	if(formChanged) {
		//PC 24/11/10 : ajout d'une vérification sur form1
		if (!form1) var form1 = document.form1;
		if (form1)
			if(confirm(str_lang.kJSConfirmSauverModif)){
				if(chkFormFields(form1)){
					saveAndGo(url);
				}
			} else document.location.href=url;
		else document.location.href=url;
	} else document.location.href=url;
}


function submitKey() {
    if(event.keyCode == 13){
        submitForm();
    }
}

function submitForm() {
    document.form1.submit();
}


function goUrl(url) {
    document.form1.action=url;
    document.form1.submit();
}

function getSiteRoot() {
	url=window.location.href.split('?');
	return url[0];
}


function removeAll() {
    if (confirm(str_lang.kConfirmJSAllLineSuppression)) {
        document.form1.commande.value="SUP_ALL";
        document.form1.submit();
    }else{
//        document.form1.commande.value="CANCEL";
//        document.form1.submit();
    }
}

//pour le cas SUP_LIGNES DE commande.php
function suppression(what,refer) {
	if (!confirm(str_lang.kConfirmJSDocSuppression))
		return;
	if(refer) document.form1.refer.value=refer;
	document.form1.commande.value=what;
	document.form1.submit();
}

function removeLine(i,type,action) {
    if (confirm(str_lang.kConfirmJSLineSuppression)) {
        document.form1.ligne.value=i;
        if (typeof(action)=='undefined') action="SUP";
        document.form1.commande.value=action;
        if (type){
            document.form1.type.value=type;
        }
        document.form1.submit();
    }else{
//    	if (typeof(action)=='undefined') action="CANCEL";
//        document.form1.commande.value=action;
//        document.form1.submit();
    }
}

function updateLine(i,action) {
    document.form1.ligne.value=i;

    if (typeof(action)=='undefined') action="SAVE";
    document.form1.commande.value=action;
    document.form1.submit();
}

function addLine(i,action,form_id){
	if(typeof form == 'undefined'){
		form = document.form1;
	}else{
		form = document.getElementById(form_id);
	}

	form.ligne.value=i;
    if (typeof(action)=='undefined') action="ADD";
    form.commande.value=action;
    form.submit();
}


function popupWindow(newURL, newName, newFeatures, orgName) {
  var remote = open(newURL, newName, newFeatures);
  if(remote){
	  if (remote.opener == null)
	    remote.opener = window;
	  remote.opener.name = orgName;
	  //remote.print();
	  remote.focus();
}
  //return remote;
}


function popupSaisieLot(newURL,orgName,formName) {
 	popupWindow(newURL, 'popup', 'height=600,width=900,screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=1,scrollbars=1,menubar=1,toolbar=1', orgName);
 	if(formName)document.getElementsByName(formName)[0].submit();
}

function popupVideo(newURL,orgName,w,h){

    if(newURL.indexOf("popUpVisionnage")==0) {
		// Appel capsule
		if (!w) w=512; if(!h) h=335;
		popupWindow(newURL, 'Visionnage', 'height='+h+', width='+w+',screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=1,scrollbars=0,menubar=0,toolbar=0', orgName);
    } else if(w && h ){
		// Appel player
		popupWindow(newURL, 'Visionnage', 'height='+h+', width='+w+',screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=1,scrollbars=0,menubar=0,toolbar=0', orgName);
    }else{
		popupWindow(newURL, 'Visionnage', 'height=400,width=400,screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=1,scrollbars=0,menubar=0,toolbar=0', orgName);
	}
}

function popupPrint(newURL,orgName,formName){
	var ids = '';
	$j("form input[name^='checkbox']").each(function () {
		if ($j(this).is(':checked')) {
			if (ids != '')
				ids = ids+',';
			ids = ids + $j(this).val();
		}
	});
	if (ids != '')
		popupWindow(newURL + "&tIds=" + ids, 'Print','height=500,width=520,screenX=100,left=100,screenY=100,top=100,fullscreen=0,resizable=1,scrollbars=1,menubar=1,toolbar=1', orgName);
	else{
		popupWindow(newURL, 'Print','height=500,width=520,screenX=100,left=100,screenY=100,top=100,fullscreen=0,resizable=1,scrollbars=1,menubar=1,toolbar=1', orgName);

	}
	if(formName)document.getElementsByName(formName)[0].submit();
}

function attachDocsToNewReport(reportName,id_fonds){

	ids="";
	var target_url;
	$j("form#import_aff_form input[name^='checkbox']").each(function () {
		if ($j(this).is(':checked')) {
			if (ids != '')
				ids = ids+',';
			ids = ids + $j(this).val();
		}
	});


	if(ids==''){
		showAlert(str_lang.kMsgNoSelectLignes,'alertBox','slideDown');
		return false ;
	}

	target_url= "empty.php?urlaction=processReportage&commande=attachToNewReportage&ids="+ids;

	if(typeof reportName  != "undefined"){
		target_url+="&reportName="+encodeURIComponent(reportName);
	}

	if(typeof id_fonds != "undefined" && id_fonds!=''){
		target_url+="&id_fonds="+encodeURIComponent(id_fonds);
	}

	$j.ajax({
		url : target_url,
		complete : function(data){
			showAlert(data.responseText,'alertBox','slideDown');
		}
	});
}

function popupUpload(newURL,orgName,type){
	var width = 480;
	if (type == 'jobs') width = 1000;

    // on n'ouvre pas si elle existe déjà car on pourrait annuler un transfert en cours
    popupWindow(newURL, 'Upload_'+type,'height=600,width='+width+',screenX=100,left=100,screenY=100,top=100,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1', orgName);
}

// VP 24/07/09 : ajout paramètre optionnel wName
function popupDoc(newURL,orgName,wName){
	if(!wName) wName='Document';
    popupWindow(newURL, wName,'height=600,width=900,screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=1,scrollbars=1,menubar=1,toolbar=1', orgName);
}

function popupLex(newURL,orgName){
    popupWindow(newURL, 'Lexique','height=500,width=600,screenX=350,left=350,screenY=150,top=150,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0', orgName);
}

function popupVal(newURL,orgName){
    popupWindow(newURL, 'Valeurs','height=500,width=500,screenX=350,left=350,screenY=150,top=150,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0', orgName);
}


function popupMat(newURL,orgName){
    popupWindow(newURL, 'Materiel','height=600,width=500,screenX=300,left=300,screenY=100,top=100,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0', orgName);
}

function popupUsager(newURL,orgName){
    popupWindow(newURL, 'Usager','height=600,width=500,screenX=300,left=350,screenY=100,top=100,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0', orgName);
}

function popupSelect(newURL,orgName){
    popupWindow(newURL, 'Select','height=500,width=500,screenX=250,left=250,screenY=150,top=150,fullscreen=0,resizable=0,scrollbars=auto,menubar=0,toolbar=0', orgName);
}

function popupAbout(newURL,orgName){
    popupWindow(newURL, 'About','height=200,width=200,screenX=350,left=350,screenY=250,top=150,fullscreen=0,resizable=0,scrollbars=auto,menubar=0,toolbar=0', orgName);
}

function popupSave(newURL,orgName){
    popupWindow(newURL, 'About','height=100,width=400,screenX=350,left=350,screenY=250,top=150,fullscreen=0,resizable=0,scrollbars=auto,menubar=0,toolbar=0', orgName);
}

function popupImage(img,txt) {

		window.open("popupImage.php?img="+img+"&amp;txt="+txt,'image','width=400,height=400,toolbar=no,scrollbars=no,resizable=yes');

//onblur='window.close()' onclick='window.close()'
}


function popupInfo(newURL,orgName){
    popupWindow(newURL, 'Info','height=400,width=790,screenX=100,left=100,screenY=100,top=100,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0', orgName);
}

function popupPersonne(newURL,orgName){
    popupWindow(newURL, 'Personne','height=550,width=800,screenX=150,left=150,screenY=100,top=100,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0', orgName);
}

function popupPlayerFlash(id){
    document.getElementById('player').src="popupPlayerFlash.php?id_doc="+id;
}

function popupModifLot(newURL,orgName,wName,context){
	if(!wName) wName='Document';
	if(!context) context='';

	var formSelector = "form#formPanier";
	if (context.indexOf("doc") == 0 || context == 'importView')
		formSelector = "form[name='documentSelection']";
		
	var ids = '';
	$j(formSelector + " input[name^='checkbox']").each(function () {
		if ($j(this).is(':checked') && parseInt($j(this).val()) !== 0 ) {
			if (ids != '')
				ids = ids+',';
			ids = ids + $j(this).val();
		}
	});
	if (ids != '')
		popupWindow(newURL + "&tIds=" + ids, wName,'height=600,width=900,screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=1,scrollbars=1,menubar=1,toolbar=1', orgName);
	else if(context=='importView')
		alert("Vous devez sélectionner au moins une notice");
	else
    	popupWindow(newURL, wName,'height=600,width=900,screenX=50,left=50,screenY=50,top=50,fullscreen=0,resizable=1,scrollbars=1,menubar=1,toolbar=1', orgName);
}

function copyValue(f1,f2) {
    if(f2.value=='') f2.value=f1.value;
}

/* Assigne une valeur à un champ, qq soit son type
*/
function setValue(val,field,matchcase) {
	val=val.toString();
	var i;
	if (field.type=='text' || field.type=='textarea') field.value=val;
	if (field.type=='select-one') {
		for(i=0;i<field.length;i++) {
			if (field.options[i].value==val && matchcase) {field.options[i].selected=true;return;}
			if (field.options[i].value.toLowerCase()==val.toLowerCase() && !matchcase)
			{	field.options[i].selected=true;return;}
		}
	}
	//to be continued...

}

function setValues(f1,f2) {
    if(f1.value!='') {
        f2.value=f1.value;
        f2.text=f1.text;
        }
}

function setValueBox(box,field) {
	var val="";
	for(i=0;i<box.length;i++){
		if(box[i].checked) {
			if(val=='') val= box[i].value;
			else val+=", "+box[i].value;
			}
	}
    field.value=val;
}

function getCheckedValue(field) {
	var val="";
	for(i=0;i<field.length;i++){
		if(field[i].checked) {
			if(val=='') val= field[i].value;
			else val+=", "+field[i].value;
			}
	}
    return val;
}

function selectMenu(f,url,src) {
    if(f.selectedIndex==2){
    //Selection
    popupSelect(url, src)
    }
    else if(f.selectedIndex==3) {
    //Suppression
        f.options[0].value='';
        f.options[0].text=''
        }
    f.selectedIndex=0;
}

function selectMenuAndSend(f,url,src) {
    if(f.selectedIndex==2){
    //Selection
    popupSelect(url, src)
    }
    else if(f.selectedIndex==3) {
    //Suppression
        f.options[0].value='';
        f.options[0].text=''
        f.selectedIndex=0;
        }
    f.selectedIndex=0;
        document.form1.commande.value='SELECT'
        document.form1.submit();
}

//update VG 09/2011 : ajout de la prise en compte du format
function formatDate(srcField,format) {
	if(typeof(format) == 'undefined' || format == '') {
	    var value_date = srcField.value;
	    if ( (value_date.length == 4) && (value_date.indexOf("-") == -1) ) {
	        value_date = value_date  + "-";
	        srcField.value = value_date;
	    }
	    if ( (value_date.length == 7) && (value_date.lastIndexOf("-") == 4) ) {
	        value_date = value_date  + "-";
	        srcField.value = value_date;
	    }
	     p = "--";
	    if (value_date.match(p)) {
	        value_date = value_date.replace(p, "-");
	        srcField.value = value_date;
	    }
	} else if (format = "jj/mm/aaaa") {
	    var value_date = srcField.value;
	    if ( (value_date.length == 2) && (value_date.indexOf("/") == -1) ) {
	        value_date = value_date  + "/";
	        srcField.value = value_date;
	    }
	    if ( (value_date.length == 5) && (value_date.lastIndexOf("/") == 2) ) {
	        value_date = value_date  + "/";
	        srcField.value = value_date;
	    }
	     p = "//";
	    if (value_date.match(p)) {
	        value_date = value_date.replace(p, "/");
	        srcField.value = value_date;
	    }
	}

}

function basename(str) {
    var base = new String(str).substring(str.lastIndexOf('/') + 1);
    if(base.lastIndexOf(".") != -1)
       base = base.substring(0, base.lastIndexOf("."));
   return base;
}

function formatNum(n) {
if(n<10) return "0"+n;
else return n;
}

//update VG 22/04/2010 : suppression du formatage des heures, + prise en compte de la saisie de ':' par l'utilisateur {Evol #46}
//update VG 11/05/2010 : On maintient toute valeur de temps (minute, seconde) � une longueur de 2, except� les heures, en d�coupant la chaine {Evol #46}
function formatTime(srcField) {

    var value_tc = srcField.value;

    //update VG 21/04/2010
    var p = "::";
    if (value_tc.match(p)) {
        value_tc = value_tc.replace(p, ":");
        srcField.value = value_tc;
    }

/*    if ( (value_tc.length == 1) && (value_tc.indexOf("0") == -1) && (value_tc.indexOf("1") == -1) && (value_tc.indexOf("2") == -1) ) {
       srcField.value = "";
        return;
    }*/
    //update VG 11/05/10
	var aTime = value_tc.split(':');
	//On ne bloque � 2 chiffres que pour les minutes et les secondes
	for  (var i=1; i<aTime.length; i++)  {
		aTime[i] = aTime[i].substr(0,2);
	}
	value_tc = aTime.join(':');
	srcField.value = value_tc;

    if ( (value_tc.length == 2) && (value_tc.indexOf(":") == -1) ) {
        value_tc = value_tc  + ":";
        srcField.value = value_tc;
        return;
    }
    if ( (value_tc.length == (value_tc.lastIndexOf(":")+3) ) && (value_tc.lastIndexOf(":") < 3) ) {
        value_tc = value_tc  + ":";
        srcField.value = value_tc;
        return;
    }
}


function formatTC(srcField) {
    var value_tc = srcField.value;
    if ( (value_tc.length == 1) && (value_tc.indexOf("0") == -1) && (value_tc.indexOf("1") == -1) && (value_tc.indexOf("2") == -1) ) {
       srcField.value = "";
        return;
    }
    if ( (value_tc.length == 2) && (value_tc.indexOf(":") == -1) ) {
        value_tc = value_tc  + ":";
        srcField.value = value_tc;
        return;
    }
    if ( (value_tc.length == 5) && (value_tc.lastIndexOf(":") == 2) ) {
        value_tc = value_tc  + ":";
        srcField.value = value_tc;
        return;
    }
    if ( (value_tc.length == 8) && (value_tc.lastIndexOf(":") == 5) ) {
        value_tc = value_tc  + ":";
        srcField.value = value_tc;
        return;
    }
    p = "::";
    if (value_tc.match(p)) {
        value_tc = value_tc.replace(p, ":");
        srcField.value = value_tc;
    }

}

//update VG 09/2011 : ajout de la prise en compte du format
function checkDate(srcField, format) {
    if(srcField.value !='') {
    	if(typeof(format) == 'undefined' || format == '') {
	        dat = srcField.value;
	        dat = dat.replace(/\//gi,'-');
	        datret = dat;
	        x = dat.indexOf("-");
	        if(x>0) {
	            a=parseInt(dat.substring(0,x),10);
	            y = dat.indexOf("-",x+1);
	            if(y==-1) {
	                m=parseInt(dat.substring(x+1),10);
	                if(isNaN(m))m=0;
					if(m==0) datret = formatNum(a)
	                else datret = formatNum(a) + "-" + formatNum(m);
	            } else {
	                m=parseInt(dat.substring(x+1,y),10);
	                j=parseInt(dat.substring(y+1),10);
	                if(isNaN(j))j=0;
	                if(isNaN(m))m=0;
	                datret = formatNum(a) + "-" + formatNum(m) + "-" + formatNum(j);
	            }
	        }
    	} else if (format=="jj/mm/aaaa"){
	        dat = srcField.value;
	        dat = dat.replace(/-/gi,'/');
	        datret = dat;
	        x = dat.indexOf("/");
	        if(x>0) {
	            j=parseInt(dat.substring(0,x),10);
	            y = dat.indexOf("/",x+1);
	            if(y==-1) {
	                m=parseInt(dat.substring(x+1),10);
	                if(isNaN(m))m=0;
					if(m==0) datret = formatNum(j)
	                else datret = formatNum(j) + "/" + formatNum(m);
	            } else {
	                m=parseInt(dat.substring(x+1,y),10);
	                a=parseInt(dat.substring(y+1),10);
	                if(isNaN(j))j=0;
	                if(isNaN(m))m=0;
	                datret = formatNum(j) + "/" + formatNum(m) + "/" + formatNum(a);
	            }
	        }
    	}
        srcField.value = datret;

        //alert("Format : AAAA-MM-JJ");
    }
}


//Fonction permettant de vérifier si une date est valide (par ex : 31/02/2010 est invalide)
function validDate(srcField, format) {
    if(srcField.value !='') {
    	if(typeof(format) == 'undefined' || format == '') {
	        dat = srcField.value;
	        dat = dat.replace(/\//gi,'-');
	        datret = dat;
	        x = dat.indexOf("-");
	        if(x>0) {
	            a=parseInt(dat.substring(0,x),10);
	            y = dat.indexOf("-",x+1);
	            if(y==-1) {
	                m=parseInt(dat.substring(x+1),10);
	                if(isNaN(m))m=0;
	            } else {
	                m=parseInt(dat.substring(x+1,y),10);
	                j=parseInt(dat.substring(y+1),10);
	                if(isNaN(j))j=0;
	                if(isNaN(m))m=0;
	            }
	        }
    	} else if (format=="jj/mm/aaaa"){
	        dat = srcField.value;
	        dat = dat.replace(/-/gi,'/');
	        datret = dat;
	        x = dat.indexOf("/");
	        if(x>0) {
	            j=parseInt(dat.substring(0,x),10);
	            y = dat.indexOf("/",x+1);
	            if(y==-1) {
	                m=parseInt(dat.substring(x+1),10);
	                if(isNaN(m))m=0;
	            } else {
	                m=parseInt(dat.substring(x+1,y),10);
	                a=parseInt(dat.substring(y+1),10);
	                if(isNaN(j))j=0;
	                if(isNaN(m))m=0;
	            }
	        }
    	}

    	a=formatAn(a);
		//Vérification de validité de date
		oDate = new Date();
		if(a != '' && a>0)
			oDate.setFullYear(a);
		if(m != '' && m>0)
			oDate.setMonth((m - 1));
		if(j != '' && j>0)
			oDate.setDate(j);

		if(m != '' && m>0) {
			m = parseInt(m);
			m2 = parseInt((oDate.getMonth() + 1));

			if(m!=m2)
				return false;
		}

		if(j != '' && j>0) {
			j = parseInt(j);
			j2 = parseInt(oDate.getDate());
			if(j!=j2)
				return false;
		}
    }
	return true;
}

//update VG 22/04/2010 : les minutes et les secondes d�passant 59 sont remises � 59 {Evol #46}
//update VG 11/05/2010 : En cas de champ vide on met simplement � 0, pour �viter un NaN, ainsi que tous les autres traitements, inutiles alors{Evol #46}
//update VG 17/05/2010 : Pour chaque particule on met � 0 si NaN{Evol #46}
//update VG 24/05/2010 : "if(h<10) h +="0"; " devient "if(h<10) h ="0" + h;"
function checkTime(srcField) {
    var t = srcField.value;

	if(t=="") {
		srcField.value = '00:00:00';
		return;
	}

    x = t.indexOf(":");
    var h=0;
    var m=0;
    var s=0;
    if(x==-1) h=parseInt(t,10);
    else {
        h=parseInt(t.substring(0,x),10);
        y = t.indexOf(":",x+1);
        if(y==-1) m=parseInt(t.substring(x+1),10);
        else {
            m=parseInt(t.substring(x+1,y),10);
            s=parseInt(t.substring(y+1),10);
        }
    }

	if(isNaN(h)) h = 0;
	if(isNaN(m)) m = 0;
	if(isNaN(s)) s = 0;

    if (m>59) {
    	m = 59;
    }

    if (s>59) {
    	s = 59;
    }

	if(h<10) h ="0" + h;
    ret= h + ":";
    if(m<10)  ret +="0";
    ret += m + ":";
    if(s<10) ret +="0";
    srcField.value =  ret + s;

}


function checkTC(srcField) {
    t = srcField.value;
    x = t.indexOf(":");
    h=0;
    m=0;
    s=0;
    f=0;
    if(x==-1) {
        h=parseInt(t.substring(0,2),10);
        m=parseInt(t.substring(2,4),10);
        s=parseInt(t.substring(4,6),10);
        f=parseInt(t.substring(6,8),10);
    } else {
        h=parseInt(t.substring(0,x).substring(0,2),10);
        y = t.indexOf(":",x+1);
        z = t.indexOf(":",y+1);
        if(y==-1) m=parseInt(t.substring(x+1).substring(0,2),10);
        else {
            m=parseInt(t.substring(x+1,y).substring(0,2),10);
            if(z==-1) s=parseInt(t.substring(y+1).substring(0,2),10);
            else {
                s=parseInt(t.substring(y+1,z).substring(0,2),10);
                f=parseInt(t.substring(z+1).substring(0,2),10);
            }
        }
    }
    if(isNaN(h)) h=0;
    if(isNaN(m)) m=0;
    if(isNaN(s)) s=0;
    if(isNaN(f)) f=0;
	// VP 14/10/10 : ajout bornes max pour minutes, secondes et frames
	if(m>59) m=0;
	if(s>59) s=0;
	if(f>24) m=0;
    srcField.value =  formatNum(h) + ":" + formatNum(m) + ":" + formatNum(s)+ ":" + formatNum(f);
}

// VP (6/10/08) : ajout fonction convDate
//update VG 09/2011 : ajout prise en compte du format
function convDate(dat,fmt,admitOnlyYear) {
 	x = dat.indexOf("/");
	if(x>0) {
		j=parseInt(dat.substring(0,x),10);
		y = dat.indexOf("/",x+1);
		if(y==-1) {
			a=parseInt(dat.substring(x+1),10);
			m=parseInt(dat.substring(0,x),10);
			j=0;
		} else {
			j=parseInt(dat.substring(0,x),10);
			m=parseInt(dat.substring(x+1,y),10);
			a=parseInt(dat.substring(y+1),10);
		}
	} else {
		x = dat.indexOf("-");
		if(x>0) {
			a=parseInt(dat.substring(0,x),10);
			y = dat.indexOf("-",x+1);
			if(y==-1) {
				m=parseInt(dat.substring(x+1),10);
				j=0;
			} else {
				m=parseInt(dat.substring(x+1,y),10);
				j=parseInt(dat.substring(y+1),10);
			}
		} else {
			a=parseInt(dat,10);
			m=0;
			j=0;
		}
	}
	if(isNaN(a)) a=0;
	if(isNaN(m)) m=0;
	if(isNaN(j)) j=0;
	if(m==0 && j==0 && admitOnlyYear == true) return formatAn(a);
    else if(a==0 && m==0 && j==0) return "";
	else if(fmt=='aaaa-mm-jj') return formatAn(a) + "-" + formatNum(m) + "-" + formatNum(j);
	else if(fmt=='jj/mm/aaaa') return formatNum(j) + "/" + formatNum(m) + "/" + formatAn(a);
	else if(fmt=='mm/jj/aaaa') return formatNum(m) + "/" + formatNum(j) + "/" + formatAn(a);
	else return dat;
}

// VP 4/12/08 : ajout fonction formatAn
// VP 20/4/10 : ajout parseInt et test NaN
function formatAn(n) {
	n=parseInt(n,10);
	if(isNaN(n)||(n==0)) return "0000";
	else if(n<1000) return "0"+n;
	else if(n>9999) return "9999";
	else return n;
}

// VP (6/10/08) : ajout fonction secToTime (idem php)
function secToTime(temps){
	h = Math.floor(temps / 3600);
	m = Math.floor((temps - (h * 3600)) / 60);
	s = temps - (h * 3600) - (m * 60);
	return formatNum(h) + ":" + formatNum(m) + ":" + formatNum(s);
}

// VP (6/10/08) : ajout fonction tcToSec (idem php)
function tcToSec(tc){
    if (tc=='') return 0;
    tab_duree = tc.split(":");
    h=parseInt(tab_duree[0],10);
    m=parseInt(tab_duree[1],10);
    s=parseInt(tab_duree[2],10);
    fr=parseInt(tab_duree[3],10);
    if(fr)
    	return (h*3600 + m*60 + s + Math.floor(fr/25));
	else 
		return (h*3600 + m*60 + s);}

// VP (10/9/09) : ajout fonction tcToFrame (idem php)
function tcToFrame(tc){
    if (tc=='') return 0;
    tab_duree = tc.split(":");
    h=parseInt(tab_duree[0],10);
    m=parseInt(tab_duree[1],10);
    s=parseInt(tab_duree[2],10);
    fr=parseInt(tab_duree[3],10);
    return (((h*60 + m)*60 + s)*25+fr);
}

// VP (10/9/09) : ajout fonction frameToTC (idem php)
function frameToTC(fr){
	_sec=Math.floor(fr/25);
	_tc=secToTime(_sec);
	_i=fr - (_sec*25);
	return _tc + ":" +formatNum(_i);
}

// VP (10/9/09) : ajout fonction diffTC
function diffTC(tcout,tcin){
	return frameToTC(tcToFrame(tcout)-tcToFrame(tcin));
}

function showAndInitRechInd2(titre_index,valeur,id_lang,champ,id_input_result){
	var lex_aff=0;
	var type_lex="";
	if(document.getElementById("thes_"+champ)) {
		lex_aff=1;
		type_lex=document.getElementById("thes_"+champ).value;
	}
	showAndInitRechInd(titre_index,valeur,id_lang,champ,id_input_result,lex_aff,type_lex);
}


function showAndInitRechInd(titre_index,valeur,id_lang,champ,id_input_result,lex_aff,type_lex){
    var url = "chercheIndex.php?";
    url +='titre_index='+titre_index+'&id_lang='+id_lang+'&valeur='+valeur+'&champ='+champ+'&id_input_result='+id_input_result;
	if(lex_aff) url+='&lex_aff='+lex_aff;
	if(type_lex) url+='&type_lex='+type_lex;
    iframeChrchInd.location.href=url;

}



// JC 01/06/05 : Pour mettre � jour les champs du formulaire du document parent, depuis l'iframe qu'il incorpore.
//(utilis�e par exemple dans l'iframe chercheIndex.php, incorpor�e dans cherche2.php (dans cherche2.inc.php) ).
function MaJChamp(id_champ, valeur){

	var valChamp = window.parent.document.getElementById(id_champ).value;
	if(valChamp.indexOf('"'+valeur+'"')==-1){
		if (valChamp!=""){
			window.parent.document.getElementById(id_champ).value+=',"'+valeur+'"';
		}else{
			window.parent.document.getElementById(id_champ).value='"'+valeur+'"';
		}
	}
}

// JC 17/06/05 : utile uniquement pour la d�mo
function doConnect(login, password){
    document.getElementById('login').value=login;
    document.getElementById('password').value=password;
    document.connexion.submit();


}


// Permet de cacher/montrer l'�lement "id_objet" (utilis� pour les storyboards)
    function cache(id) {
        if (document.getElementById) { // DOM3 = IE5, NS6
            document.getElementById(id).style.display = 'none';
        }
        else {
            if (document.layers) { // Netscape 4
                document.id.display = 'none';
            }
            else { // IE 4
                document.all.id.style.display = 'none';
            }
        }
    }

    function affiche(id) {
        if (document.getElementById) { // DOM3 = IE5, NS6
            document.getElementById(id).style.display = 'block';
        }
        else {
            if (document.layers) { // Netscape 4
                document.id.display = 'block';
            }
            else { // IE 4
                document.all.id.style.display = 'block';
            }
        }
    }

// Permet de cacher/montrer l'�lement "id_objet" (utilis� pour les storyboards)
function affiche_cache(id) {
    if (document.getElementById) { // DOM3 = IE5, NS6
        if (document.getElementById(id).style.display == 'none'){
            document.getElementById(id).style.display = 'block';
        }else{
            document.getElementById(id).style.display = 'none';
        }
    }
    else {
        if (document.layers) { // Netscape 4
            if (document.id.display == 'none'){
                document.id.display = 'block';
            }else{
                document.id.display = 'none';
            }
        }
        else { // IE 4
            if (document.all.id.style.display == 'none'){
                document.all.id.style.display = 'block';
            }else{
                document.all.id.style.display = 'none';
            }
        }
    }
}

function alterne_texte(objet,text1,text2) {
        if (objet.value == text1){
            objet.value = text2;
        }else{
            objet.value = text1;
        }
}


// R�alise un explode de 'item' selon 'delimiter'
function explodeArray(item,delimiter) {
  tempArray=new Array(1);
  var Count=0;
  var tempString=new String(item);

  while (tempString.indexOf(delimiter)>0) {
    tempArray[Count]=tempString.substr(0,tempString.indexOf(delimiter));
    tempString=tempString.substr(tempString.indexOf(delimiter)+1,tempString.length-tempString.indexOf(delimiter)+1);
    Count=Count+1
  }

  tempArray[Count]=tempString;
  return tempArray;
}

// Formate une durée saisie en minute en h:mm si celle-ci dépasse 60 minutes
function calcDuree(fld) {

	if (isNaN(fld.value)) return;

	hours=parseInt(fld.value/60);
	mins=fld.value-60*hours;
	fld.value=hours+":"+mins;
	checkTime(fld);
}

// Ajout de la fonction TRIM aux strings : retire les espaces au début et à la fin d'une chaine.
// usage : mytrimmedString=myString.Trim()
if (typeof String.prototype.Trim == "undefined") {
	String.prototype.Trim = function () {
		var s = this.replace(/^\s*/, "");
		return s.replace(/\s*$/, "");
	}
}

/*if (typeof HTMLSelectElement.prototype.selectedValue== "undefined") {
	HTMLSelectElement.prototype.selectedValue= function (_val) {
		for (i=0;i<this.options.length;i++) {
			if (this.options[i].value==_val) this.options[i].selected=true;
		}
	}
}*/


function encodeQuotes(str) {
	//str.replace(/"/,'&quot;');
	return str;
}

function Quote(str) {
	str=str.split("'").join("\\'");
	return str;
}


//Fonction de formulaire : vérifie si un champ est vide
function formCheckNonEmpty(val) {
	if (val.Trim()=='') {return false;} else {return true;}
}

//Fonction de formulaire : vérifie si une adresse mail est valide
function formCheckEmail(val) {
	val=val.trim();
    re=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //from Quirksmode.org
	if (val.search(re)!=-1) {return true;} else {return false;}
}

function formCheckMultiEmail(val) {
	var vals = val.split(',');
	var valid = true;
	for (var i = 0; i < vals.length; i++) {
		m = vals[i];
		console.log(m);
		if(!formCheckEmail(m)){
			valid = false;
		}
	}
	return valid;
}

function check_passwd_normes_cnil(password){
	if(password.length>=8 && password.length<=30
	&& password.match(/[A-Z]/) && password.match(/[a-z]/) && password.match(/[0-9$*?!,.()-+]/) && !password.match(/[ \t]/)){
		return true;
	}else{
		return false ;
	}
}


//Fonction de formulaire : vérifie si 2 valeurs sont identiques, ex: ctrl password.
function formCheckSameVal(val1,val2) {
	if (val1===val2 && val1.Trim()!='') {return true;} else {return false;}
}

function formCheckDate(val) {
	re = /^([0-9]){2}(\/|-){1}([0-9]){2}(\/|-)([0-9]){4}$/;
	if (val.search(re)!=-1) {return true;} else {return false;}
}

function formCheckInt(val) {
	if (isNaN(parseInt(val))) return false; else return true;
}

function formCheckFloat(val) {

	if (isNaN(parseFloat(val))) return false; else return true;
}


// FONCTIONS AJAX

function getHTTPObject(fonc,addPrms) {

	var xmlhttp=false;
	if (typeof(addPrms)=='undefined') {
		addPrms=null;
	}
	/* Compil addit IE */
	/*@cc_on
	@if (@_jscript_version >= 5)

		try {
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			} catch (E) {
				xmlhttp=false;
				}
		}
		@else
			xmlhttp=false;
	@end @*/

	/* création objet si pas encore fait */
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		try {
			xmlhttp=new XMLHttpRequest();
		} catch (e) {xmlhttp=false;}
	}

	if (xmlhttp) {
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4) { /* COMPLETE */
				if (xmlhttp.status==200) { /*HTTP PAGE OK */

					eval('window.'+fonc+'(xmlhttp.responseText,addPrms);'); //by LD, transfo window.parent en window
				}
			}
		}
	}
	return xmlhttp;
}

// MS 03/07/2012 support requete cross domain pour IE
function getXDR(fonc){
	var xdr=false;
	if(window.XDomainRequest){
		try {
			xdr=new XDomainRequest();
		} catch (e) {xmlhttp=false;}
	}

	if(xdr){
		xdr.onload = function(){
			eval('window.'+fonc+'(xdr.responseText);');
		}
	}
	return xdr;
}

function sendData(method,url,data,fonc,cross_domain,async,additionalParams) {

	if (typeof(fonc)=='undefined') fonc='dspResult';
	if (typeof(cross_domain)=='undefined') {
		cross_domain=false;
	}
	if (typeof(additionalParams)=='undefined') {
		additionalParams=null;
	}
	if (typeof(async)=='undefined') async=true;
	if(cross_domain == true && window.XDomainRequest ){
		var xmlhttp= getXDR(fonc);
	}else{
		var xmlhttp=getHTTPObject(fonc,additionalParams);
	}
	if (!xmlhttp) {alert('pas xml');return false;}

	if (method=='GET') {
		if (data=='null') {

			xmlhttp.open("GET",url,async); //ouverture async
		} else {
			xmlhttp.open("GET",url+"?"+data,async);
		}
		xmlhttp.send(null);
	} else if (method=='POST') {
		//alert(data);
		xmlhttp.open("POST",url,async);
		//xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencode');

		xmlhttp.send(data);
	}
	return true;
}

function importXML(str)
{ //Transforme une chaine XML en objet XML

	if (document.implementation && document.implementation.createDocument)
	{
	  var parser=new DOMParser();
	  var xmlDoc=parser.parseFromString(str,"text/xml");
	}
	else if (window.ActiveXObject)
	{
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
 		xmlDoc.loadXML(str);
 	}
	else
	{
		alert('Your browser can\'t handle this script');
		return;
	}
	return xmlDoc;
}

function getPageDimensions() {
		var x,y;
		var test1 = document.body.scrollHeight;
		var test2 = document.body.offsetHeight
		if (test1 > test2) // all but Explorer Mac
		{
			x = document.body.scrollWidth;
			y = document.body.scrollHeight;
		}
		else // Explorer Mac;
		     //would also work in Explorer 6 Strict, Mozilla and Safari
		{
			x = document.body.offsetWidth;
			y = document.body.offsetHeight;
		}
	return {x:x,y:y};

}

function getBrowserDimensions() {
		var x,y;
		if (window.innerWidth) // FF / SF
		{

			x = window.innerWidth;
			y = window.innerHeight;
		}
		else //IE
		{
			x = document.documentElement.clientWidth;
			y = document.documentElement.clientHeight;
		}
	return {x:x,y:y};

}

function getScrollDimensions() {
	var x,y;
	if (self.pageYOffset) // all except Explorer
	{
		x = self.pageXOffset;
		y = self.pageYOffset;
	}
	else if (document.documentElement && document.documentElement.scrollTop)
		// Explorer 6 Strict
	{
		x = document.documentElement.scrollLeft;
		y = document.documentElement.scrollTop;
	}
	else if (document.body) // all other Explorers
	{
		x = document.body.scrollLeft;
		y = document.body.scrollTop;
	}
	return {x:x,y:y};
}


function choose(fromElement, params, classname, ww, hh) {

    if (!ww)
        ww = 300;
    if (!hh)
        hh = 400;
    w = fromElement.offsetWidth;
    h = fromElement.offsetHeigth;
    myDiv = document.getElementById('chooser');
    myDiv.position = 'absolute';
    if (document.getElementById('frmchoose')) {
        document.getElementById('frmchoose').parentNode.removeChild(document.getElementById('frmchoose'));
    }

    myIFrame = document.createElement('iframe');
    if (classname != '') {
        myIFrame.className = classname;
    } else {
        myIFrame.style.border = "2px solid black";
    }

    myIFrame.id = 'frmchoose';
    myIFrame.name = 'frmchoose';
    myIFrame.width = (ww - 4) + 'px';
    myIFrame.height = (hh) + 'px';
    myIFrame.style.backgroundColor = 'white';
    myDiv.style.width = ww + 'px';
    myDiv.style.height = hh + 'px';
    myDiv.appendChild(myIFrame);
    myDiv.style.display = 'block';
    // on remet la src après tout, sinon il y a un bug safari
    myIFrame.src = "palette.php?urlaction=chercheIndex2&" + params + "&id_input_result=" + fromElement.id + "&w=" + ww + "&h=" + hh;
    x = getLeft(fromElement) + w;
    y = getTop(fromElement);
    //console.log('avant y '+y);
    // console.log('avant x '+x);


    /* si fromElement est contenu dans un conteneur (ex: div) susceptible d'avoir un scroll , il faut réajuster la position de la palette suivant ce scroll */
    var containerScroll = document.getElementById('desc');
    if (containerScroll !== null && containerScroll !== undefined) {

        x = x - containerScroll.scrollLeft;
        y = y - containerScroll.scrollTop;
        //console.log('scrollTopContainer'+containerScroll.scrollTop);
        //console.log('scrollLeftContainer'+containerScroll.scrollLeft);

    }

//console.log('apres y '+y);
//console.log('apres x '+x);

    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
    var win_scrollLeft;
    var win_scrollTop;
    if ((document.documentElement && document.documentElement.clientHeight)) {

//2014/12/18 rajout de chrome car meme si sous Chrome document.documentElement && document.documentElement.clientHeight renvoie TRUE, les scrolls récuperés = 0
        if (isChrome || isSafari) {
            win_scrollLeft = document.body.scrollLeft;
            win_scrollTop = document.body.scrollTop;
        } else {

            win_scrollLeft = document.documentElement.scrollLeft;
            win_scrollTop = document.documentElement.scrollTop;
        }
        winh = document.documentElement.clientHeight + win_scrollTop; //Hack Safari3

        winw = document.documentElement.clientWidth + win_scrollLeft;
        /* console.log('cas 1 clientHeight' + document.documentElement.clientHeight);
         console.log('cas 1 clientWidth' + document.documentElement.clientWidth);
         console.log('cas 1 scrollLeft' + win_scrollLeft);
         console.log('cas 1 scrollTop' + win_scrollTop);*/
    } else {
        winh = document.body.clientHeight + document.body.scrollTop;
        winw = document.body.clientWidth + document.body.scrollLeft;
        /*console.log('cas 2 clientHeight' + document.body.clientHeight);
         console.log('cas 2 clientWidth' + document.body.clientWidth);*/

        /*console.log('cas 2 scrollLeft' + document.body.scrollLeft);
         console.log('cas 2 scrollTop' + document.body.scrollTop);*/
    }



    if ((y + hh + 10) > winh) {
        y = winh - (hh + 10);
        //console.log('depassement en bas');
    }
    if ((x + ww + 10) > winw) {
        x = winw - (ww + 10);
        //console.log('depassement a droite');
    }


    myDiv.style.left = x + "px";
    myDiv.style.top = y + "px";
}

// fonction JS permettant de choisir des valeurs depuis basis
function chooseBasis(fromElement,params,classname,ww,hh) {

	if (!ww) ww=300;
	if (!hh) hh=400;

	w=fromElement.offsetWidth;
	h=fromElement.offsetHeigth;


	myDiv=document.getElementById('chooser');
	myDiv.position='absolute';

	if (document.getElementById('frmchoose')) {
	document.getElementById('frmchoose').parentNode.removeChild(document.getElementById('frmchoose'));
	}

	myIFrame=document.createElement('iframe');
	if (classname!='') {myIFrame.className=classname;} else {myIFrame.style.border="2px solid black";}

	myIFrame.id='frmchoose';
	myIFrame.name='frmchoose';
	myIFrame.width=(ww-4)+'px';
	myIFrame.height=(hh-4)+'px';
	myIFrame.style.backgroundColor='white';
	myDiv.style.width=ww+'px';
	myDiv.style.height=hh+'px';
	myDiv.appendChild(myIFrame);
	myDiv.style.display='block';
	// on remet la src après tout, sinon il y a un bug safari
	myIFrame.src=	"palette.php?urlaction=chercheIndexBasis&"+params+"&id_input_result="+fromElement.id+"&w="+ww+"&h="+hh;


	x=getLeft(fromElement)+w;
	y=getTop(fromElement);

	if (document.documentElement && document.documentElement.clientHeight ) {
	winh=document.documentElement.clientHeight+(document.documentElement.scrollTop||document.body.scrollTop); //Hack Safari3
	winw=document.documentElement.clientWidth+document.documentElement.scrollLeft;
	} else {
	winh=document.body.clientHeight+document.body.scrollTop;
	winw=document.body.clientWidth+document.body.scrollLeft;
	}
	if ((y+hh+10) > winh) y=winh-(hh+10);
	if ((x+ww+10) > winw) x=winw-(ww+10);

	myDiv.style.left=x+"px";
	myDiv.style.top=y+"px";
}

// fonction JS permettant de choisir des valeurs depuis Solr
function chooseSolr(fromElement,params,classname,ww,hh) {

	if (!ww) ww=300;
	if (!hh) hh=400;

		w=fromElement.offsetWidth;
		h=fromElement.offsetHeigth;


		myDiv=document.getElementById('chooser');
		myDiv.position='absolute';

	if (document.getElementById('frmchoose')) {
		document.getElementById('frmchoose').parentNode.removeChild(document.getElementById('frmchoose'));
	}

	myIFrame=document.createElement('iframe');
	if (classname!='') {myIFrame.className=classname;} else {myIFrame.style.border="2px solid black";}

	myIFrame.id='frmchoose';
	myIFrame.name='frmchoose';
	myIFrame.width=(ww-4)+'px';
	myIFrame.height=(hh-4)+'px';
	myIFrame.style.backgroundColor='white';
	myDiv.style.width=ww+'px';
	myDiv.style.height=hh+'px';
	myDiv.appendChild(myIFrame);
	myDiv.style.display='block';

	// on remet la src après tout, sinon il y a un bug safari
	myIFrame.src=	"palette.php?urlaction=chercheIndexSolr&"+params+"&id_input_result="+fromElement.id+"&w="+ww+"&h="+hh;


	x=getLeft(fromElement)+w;
	y=getTop(fromElement);

	if (document.documentElement && document.documentElement.clientHeight ) {
			winh=document.documentElement.clientHeight+(document.documentElement.scrollTop||document.body.scrollTop); //Hack Safari3
			winw=document.documentElement.clientWidth+document.documentElement.scrollLeft;
	} else {
			winh=document.body.clientHeight+document.body.scrollTop;
			winw=document.body.clientWidth+document.body.scrollLeft;
	}
	if ((y+hh+10) > winh) y=winh-(hh+10);
	if ((x+ww+10) > winw) x=winw-(ww+10);

	myDiv.style.left=x+"px";
	myDiv.style.top=y+"px";
}

//addValue en retour de palette pour les formulaires de recherche
function addValue(id_champ,valeur,idx) {
	arrType=id_champ.split('$');
	type=arrType[0];
	fld=document.getElementById(type);
	if (fld.value.Trim()!='') fld.value+=", ";
	fld.value+='"'+valeur+'"';
}

function getLeft(MyObject)
//Fonction permettant de connaître la position d'un objet
//par rapport au bord gauche de la page.
//Cet objet peut être à l'intérieur d'un autre objet.
    {
    if (MyObject.offsetParent)
        return (MyObject.offsetLeft + getLeft(MyObject.offsetParent));
    else
        return (MyObject.offsetLeft);
    }


function getTop(MyObject)
//Fonction permettant de connaître la position d'un objet
//par rapport au bord haut de la page.
//Cet objet peut être à l'intérieur d'un autre objet.
    {
    if (MyObject.offsetParent)
        return (MyObject.offsetTop + getTop(MyObject.offsetParent));
    else
        return (MyObject.offsetTop);
    }


// FONCTIONS DRAG'N'DROP PALETTE

function getMouseOffset(target, ev){
	ev = ev || window.event;
	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}
	left += e.offsetLeft;
	top  += e.offsetTop;
	return {x:left, y:top};
}

function _mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(typeof(dragObject)!='undefined' && dragObject){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y+"px";
		dragObject.style.left     = mousePos.x - mouseOffset.x+"px";

		if (typeof(dragObject.style.mozOpacity)!='undefined') dragObject.style.mozOpacity=0.7;
		if (typeof(dragObject.style.opacity)!='undefined')  dragObject.style.opacity=0.7;
		if (typeof(dragObject.style.filter)!='undefined') dragObject.style.filter = 'alpha(opacity=70)';
		window.status='x:'+(mousePos.x - mouseOffset.x)+'   y:'+(mousePos.y - mouseOffset.y);
		return false;
	}
}

// MS modifications de la gestion du drag n drop pour empecher un certain nb de bug des palettes
function _mouseUp(){
	if (typeof(dragObject)!='undefined' && dragObject) {
		if (typeof(dragObject.style.mozOpacity)!='undefined') dragObject.style.mozOpacity=1;
		if (typeof(dragObject.style.opacity)!='undefined')  dragObject.style.opacity=1;
		if (typeof(dragObject.style.filter)!='undefined') dragObject.style.filter = 'alpha(opacity=100)';
	}
	if(typeof $j != "undefined" && $j("div.dragging_overlay").length != 0){$j("div.dragging_overlay").remove();}
	if(document.addEventListener){
		document.removeEventListener("mouseup",_mouseUp,false);
	}else{
		document.onmouseup = null;
	}
	dragObject = null;
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function makeDraggable(item){
	if(!item) {return;}
	item.onmousedown = function(ev){
		dragObject  = this;
		if(typeof $j != "undefined"){
			if($j(item).find("iframe").length!=0 && $j("div.dragging_overlay").length==0){
				$j(item).prepend("<div class='dragging_overlay' style='position:absolute;top:0px;left:0px;right:0px;bottom:0px;'></div>");
			}
		}
		if(document.addEventListener){
			document.addEventListener("mouseup",_mouseUp,false);
		}else{
			document.onmouseup = _mouseUp;
		}
		mouseOffset = getMouseOffset(this, ev);
		return false;
	}
	document.onmousemove=_mouseMove;
}


function removeValue(id_div) {
		fld=document.getElementById(id_div);
		fld.parentNode.removeChild(fld);
}

function toggleVisibility(myDiv,imgArrow) {
 		if (myDiv.style.display=='block') {
 			myDiv.style.display='none';
 			if (imgArrow) imgArrow.src='design/images/arrow_right.gif';
 		} else {
 			myDiv.style.display='block';
 			if (imgArrow) imgArrow.src='design/images/arrow_down.gif';
 		}
 	}


/** flèches up/down présentes sur certaines pages **/
function moveUp(elt) {
		tab=elt.parentNode.parentNode;
		div=elt.parentNode;
		prv=div.previousSibling;
		//alert("mydiv: "+div.id+"  parent: "+tab.id+"  previous: "+prv.nodeName);
		if (prv.nodeName!='SCRIPT') tab.insertBefore(div,prv);
}

function moveDown(elt) {
		tab=elt.parentNode.parentNode;
		div=elt.parentNode;
		nxt=div.nextSibling;
		//alert("mydiv: "+div.id+"  parent: "+tab.id+"  next: "+nxt.nodeName);
		if ((nxt.nodeName=='DIV' || nxt.nodeName=='SPAN') && nxt.className!='miniButton' ) tab.insertBefore(div,nxt.nextSibling);
}


/** Scan des propriétés d'un objet JS, appelé par obj_show **/
function show_props_R (obj, obj_name, equ, delim, depth) {
  var result = ''
  depth--
  for (var i in obj) {
    if (parseInt (navigator.appVersion)==2)
      var typ= 'number'     // Netscape 2.0 doesn't know about typeof
    else
      var typ= typeof (obj[i])
    if (typ=='number' && i=='length') continue
    var del= (typ=='string') ? '"' : ''
    var suf= (typ!='string' && typ!='number' && typ!='object') ?
              ' ('+typ+')' : ''
    result += obj_name+'.'+i+equ+del+obj[i]+del+suf+delim
    if (depth>0)// && typ=='object')
      result += show_props_R (obj[i], obj_name+'.'+i, equ, delim, depth)
  }
  return result;
}

/** Fonctionne avec show_props : affiche tts les propriétés d'un objet JS **/
function objshow (object, depth) {
  var str= show_props_R (eval (object), object, '=</TD><TD VALIGN=TOP>',
                         '</TD></TR>\n<TR><TD VALIGN=TOP>', depth)
//Output everything in one go. Required for Netscape 3.0b5, which forgets
//everything once we start writing to the new page.
  document.writeln ('<TABLE>\n<TR><TD>',str,'</TABLE>')
//Finish off page (needed for Netscape 3.0b4 and before).
  document.close()
}

// Faire des fade-in fade-out !
function opacity(id, opacStart, opacEnd, millisec) {
    //speed for each frame
    var speed = Math.round(millisec / 100);
    var timer = 0;

    //determine the direction for the blending, if start and end are the same nothing happens
    if(opacStart > opacEnd) {
        for(i = opacStart; i >= opacEnd; i--) {
            setTimeout("changeOpac(" + i + ",'" + id + "')",(timer * speed));
            timer++;
        }
    } else if(opacStart < opacEnd) {
        for(i = opacStart; i <= opacEnd; i++)
            {
            setTimeout("changeOpac(" + i + ",'" + id + "')",(timer * speed));
            timer++;
        }
    }
}

//change the opacity for different browsers
function changeOpac(opacity, id) {

    var object = document.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + opacity + ")";
}

//Renvoie un enfant d'un noeud DOM en passant l'id de cet enfant
function getChildById(node,eltid) {
	if (!node.childNodes) return false;
	for (_nd=0;_nd<node.childNodes.length;_nd++) {
		if (node.childNodes[_nd].id==eltid) return node.childNodes[_nd];
	}
	return false;
}


/** UTILISE PROTOTYPE : affiche une alert en FADE IN / 5 sec / FADE OUT
**/

function showAlert(message,elt,mode,offsetX,offsetY,msgDuree) {
	if (typeof(elt)=='string') el=document.getElementById(elt);
	else el=elt;
	if (!el || typeof el == 'undefined') {alert(message);return;} //pas de div alertBox ? on revient à une alerte "bete"
	contentDiv=getChildById(el,'alertBoxContent');
	if (contentDiv) {
		contentDiv.innerHTML=message;
		contentDiv.onclick= function() {el.style.display='none';}
	}
	else {
		el.innerHTML=message; //S'il existe, on place dans le contenu
		el.onclick= function() {el.style.display='none';}
	}
	el.style.display='block';
	var sizeX=parseInt(el.offsetWidth)-1; //récupère la largeur de l'alertBox
	var sizeY=parseInt(el.offsetHeight)-1; //et la hauteur

	if (isNaN(sizeX)) sizeX=500;
	if (isNaN(sizeY)) sizeY=150;
	if (isNaN(offsetX)) offsetX=0;
	if (isNaN(offsetY)) offsetY=0;

	if (isNaN(msgDuree)) msgDuree=4; //affiché 4 secs par deft

	browserDim=getBrowserDimensions();
	_refy=(browserDim.y-sizeY)/2; //calculer le milieu d'écran en tenant compte du DIV
	_refx=(browserDim.x-sizeX)/2;

	el.style.display="block"; //Au cas où
	if (document.all) el.style.position="absolute"; else el.style.position="fixed"; //Si support du fixed on l'active

	if (!mode || mode=='') mode='slideLeft' //par déft, slideLeft, notamment pour compatibilité descendante

	//var queue = { scope: el.id, position: 'end',limit:2 };
	//queue.each(function(effect) {alert(effect);});

	if (mode=='slideLeft') { //slide depuis la gauche
		el.style.left=-(sizeX+10)+"px";
		el.style.top=_refy+offsetY+"px";
		new Effect.Move(el , { x: sizeX, duration: 0.5, queue: {position:'front',scope:'global',limit:2}});
		new Effect.Move(el , { delay: msgDuree, x: -sizeX, duration: 0.5, queue: {position:'end',scope:'global',limit:2} });
	}
	if (mode=='slideRight') { //slide depuis la gauche
		el.style.display="block";
		el.style.left=window.innerWidth+"px";
		el.style.top=_refy+offsetY+"px";
		new Effect.Move(el , { x: -sizeX, duration: 0.5, queue: {position:'front',scope:'global',limit:2}});
		new Effect.Move(el , { delay: msgDuree, x: +sizeX, duration: 0.5, queue: {position:'end',scope:'global',limit:2}, afterFinish: function(){
			el.style.display = 'none';
		}
		});
	}
	if (mode=='slideDown') { //slide depuis le haut
		el.style.top=-(sizeY+10)+"px";
		el.style.left=_refx+offsetX+"px";

		new Effect.Move(el , { y: sizeY, duration: 0.5, queue: {position:'front',scope:'global',limit:2}});
		new Effect.Move(el , { delay: msgDuree, y: -sizeY, duration: 0.5, queue: {position:'end',scope:'global',limit:2} });

	}
	if (mode=='fade') { //fade InOut
		el.style.top=_refy+offsetY+"px";
		el.style.left=_refx+offsetX+"px";
		new Effect.Opacity(el , { from: 0, to: 1, duration: 0.5, queue: {position:'front',scope:'global',limit:2} });
		new Effect.Opacity(el , { delay: msgDuree, from: 1, to: 0, duration: 1, queue: {position:'end',scope:'global',limit:2} });
	}

}


/** Dans un élément qui a plusieurs boutons, permet de mettre le bouton en highlight et afficher
la section div correspondante, ex: saisie matériel, consult doc
On mettra sur chaque bouton une propriété custom "related_div" alimentée par l'ID du DIV
à montrer.
Dans la page, on mettra dans le Name du bouton la même valeur que l'ID du DIV **/

function toggleDisplay(btn) {
	arrBtn=btn.parentNode.getElementsByTagName(btn.nodeName);
	for (i=0;i<arrBtn.length;i++) {
		if (arrBtn[i]==btn) {
			btn.className='selected';
			mes2=document.getElementById(btn.getAttributeNode('related_div').value);
			document.getElementById(btn.getAttributeNode('related_div').value).style.display='block';
			btns=btn.getElementsByTagName('img');
			if (btns[0]) btns[0].style.display='inline';
		} else {
			arrBtn[i].className='';
			arrBtn[i].getAttributeNode('related_div').value;
			document.getElementById(arrBtn[i].getAttributeNode('related_div').value).style.display='none';
			btns=btn.getElementsByTagName('img');
			if (btns[0]) btns[0].style.display='none';
		}
	}
}

/*
Compatible IE/FireFox, ce code trie les tableaux HTML par colonne, très simple à mettre en place.
Le script détecte et trie plusieurs types de données :
- Date : jj/mm/aaaa ou jj-mm-aaaa
- Nombre : (-) (0-9) (.) (0-9) ($£)
- Chaine de caractères
Voir une démonstration sur http://sortable.pass-x.com/
*/
var sortindex;
function  sort_int(p1,p2) { return p1[sortindex]-p2[sortindex]; }			//fonction pour trier les nombres
function sort_char(p1,p2) { return ((p1[sortindex]>=p2[sortindex])<<1)-1; }	//fonction pour trier les strings

function TableOrder(e,Dec)  //Dec= 0:Croissant, 1:Décroissant
{ //---- Détermine : oCell(cellule) oTable(table) sortindex(sortindex cellule) -----//
	var FntSort = new Array()
	if(!e) e=window.event
		for(oCell=e.srcElement?e.srcElement:e.target;oCell.tagName!="TD";oCell=oCell.parentNode);	//determine la cellule sélectionnée
	for(oTable=oCell.parentNode;oTable.tagName!="TABLE";oTable=oTable.parentNode);				//determine l'objet table parent
	for(sortindex=0;oTable.rows[0].cells[sortindex]!=oCell;sortindex++);									//determine l'index de la cellule

	//---- Copier Tableau Html dans Table JavaScript ----//
	var Table = new Array()
		for(r=1;r<oTable.rows.length;r++) Table[r-1] = new Array()

			for(c=0;c<oTable.rows[0].cells.length;c++)	//Sur toutes les cellules
			{	var Type;
				objet=oTable.rows[1].cells[c].innerHTML.replace(/<\/?[^>]+>/gi,"")
					if(objet.match(/^\d\d[\/-]\d\d[\/-]\d\d\d\d$/)) { FntSort[c]=sort_char; Type=0; } //date jj/mm/aaaa
				else if(objet.match(/^[0-9£$\.\s-]+$/))		{ FntSort[c]=sort_int;  Type=1; } //nombre, numéraire
				else											{ FntSort[c]=sort_char; Type=2; } //Chaine de caractère

				for(r=1;r<oTable.rows.length;r++)		//De toutes les rangées
				{	objet=oTable.rows[r].cells[c].innerHTML.replace(/<\/?[^>]+>/gi,"")
					switch(Type)
					{	case 0: Table[r-1][c]=new Date(objet.substring(6),objet.substring(3,5),objet.substring(0,2)); break; //date jj/mm/aaaa
					case 1: Table[r-1][c]=parseFloat(objet.replace(/[^0-9.-]/g,'')); break; //nombre
					case 2: Table[r-1][c]=objet.toLowerCase(); break; //Chaine de caractère
					}
					Table[r-1][c+oTable.rows[0].cells.length] = oTable.rows[r].cells[c].innerHTML
				}
			}

			//--- Tri Table ---//
			Table.sort(FntSort[sortindex]);
	if(Dec) Table.reverse();

	//---- Copier Table JavaScript dans Tableau Html ----//
	for(c=0;c<oTable.rows[0].cells.length;c++)	//Sur toutes les cellules
		for(r=1;r<oTable.rows.length;r++)		//De toutes les rangées
			oTable.rows[r].cells[c].innerHTML=Table[r-1][c+oTable.rows[0].cells.length];
}

//Retourne l'urk dans la page principale
function goBackToList(url) {
	try {
		if(!window.opener || window.opener.closed)	{
			if (!mainWindow || !mainWindow.focus)	{
				// alert('open new window');
				mainWindow = window.open(url,'main');
			}else mainWindow.location.href=url;
			mainWindow.focus();
		}	else	{
			window.opener.location.href=url;
			window.opener.focus();
		}
	}
	catch (e) {
		try {
			// alert('deal with it the raw way');
			mainWindow = window.open(url,'main');
			mainWindow.focus();
		}
			catch (e) {
		}
	}
}

//Retourne l'url dans la page principale
function goBackToListAndClose(url) {
	try {
		if(!window.opener || window.opener.closed)	{
			if (!mainWindow || !mainWindow.focus)	{
				mainWindow = window.open(url,'main');
				//update VG 13/05/2011
				window.close();
			}else mainWindow.location.href=url;
			mainWindow.focus();
		}	else	{
			window.opener.location.href=url;
			window.opener.focus();
			//update VG 13/05/2011
			window.close();
		}
	}
	catch (e) {
		try {
			// alert('deal with it the raw way');
			mainWindow = window.open(url,'main');
			mainWindow.focus();
		}
			catch (e) {
		}
	}
}

function json_decode (str_json) {
  var json = this.window.JSON;
  if (typeof json === 'object' && typeof json.parse === 'function') {
    try {
      return json.parse(str_json);
    } catch (err) {
      if (!(err instanceof SyntaxError)) {
        throw new Error('Unexpected error type in json_decode()');
      }
      this.php_js = this.php_js || {};
      this.php_js.last_error_json = 4; // usable by json_last_error()
      return null;
    }
  }

  var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
  var j;
  var text = str_json;

  cx.lastIndex = 0;
  if (cx.test(text)) {
    text = text.replace(cx, function (a) {
      return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
    });
  }

  if ((/^[\],:{}\s]*$/).
  test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
  replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
  replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

    j = eval('(' + text + ')');

    return j;
  }

  this.php_js = this.php_js || {};
  this.php_js.last_error_json = 4; // usable by json_last_error()
  return null;
}

function HTMLentitiesdecode (texte) {
	texte = texte.replace(/&amp;/g,'&'); // 38 26
	texte = texte.replace(/&quot;/g,'"'); // 34 22
	texte = texte.replace(/&lt;/g,'<'); // 60 3C
	texte = texte.replace(/&gt;/g,'>'); // 62 3E

	texte = texte.replace(/&cent;/g,'\242');
	texte = texte.replace(/&pound;/g,'\243');
	texte = texte.replace(/&euro;/g,'\€');
	texte = texte.replace(/&yen;/g,'\245');
	texte = texte.replace(/&deg;/g,'\260');
	texte = texte.replace(/&OElig;/g,'\274');
	texte = texte.replace(/&oelig;/g,'\275');
	texte = texte.replace(/&Yuml;/g,'\276');
	texte = texte.replace(/&iexcl;/g,'\241');
	texte = texte.replace(/&laquo;/g,'\253');
	texte = texte.replace(/&raquo;/g,'\273');
	texte = texte.replace(/&iquest;/g,'\277');
	texte = texte.replace(/&Agrave;/g,'\300');
	texte = texte.replace(/&Aacute;/g,'\301');
	texte = texte.replace(/&Acirc;/g,'\302');
	texte = texte.replace(/&Atilde;/g,'\303');
	texte = texte.replace(/&Auml;/g,'\304');
	texte = texte.replace(/&Aring;/g,'\305');
	texte = texte.replace(/&AElig;/g,'\306');
	texte = texte.replace(/&Ccedil;/g,'\307');
	texte = texte.replace(/&Egrave;/g,'\310');
	texte = texte.replace(/&Eacute;/g,'\311');
	texte = texte.replace(/&Ecirc;/g,'\312');
	texte = texte.replace(/&Euml;/g,'\313');
	texte = texte.replace(/&Igrave;/g,'\314');
	texte = texte.replace(/&Iacute;/g,'\315');
	texte = texte.replace(/&Icirc;/g,'\316');
	texte = texte.replace(/&Iuml;/g,'\317');
	texte = texte.replace(/&ETH;/g,'\320');
	texte = texte.replace(/&Ntilde;/g,'\321');
	texte = texte.replace(/&Ograve;/g,'\322');
	texte = texte.replace(/&Oacute;/g,'\323');
	texte = texte.replace(/&Ocirc;/g,'\324');
	texte = texte.replace(/&Otilde;/g,'\325');
	texte = texte.replace(/&Ouml;/g,'\326');
	texte = texte.replace(/&Oslash;/g,'\330');
	texte = texte.replace(/&Ugrave;/g,'\331');
	texte = texte.replace(/&Uacute;/g,'\332');
	texte = texte.replace(/&Ucirc;/g,'\333');
	texte = texte.replace(/&Uuml;/g,'\334');
	texte = texte.replace(/&Yacute;/g,'\335');
	texte = texte.replace(/&THORN;/g,'\336');
	texte = texte.replace(/&szlig;/g,'\337');
	texte = texte.replace(/&agrave;/g,'\340');
	texte = texte.replace(/&aacute;/g,'\341');
	texte = texte.replace(/&acirc;/g,'\342');
	texte = texte.replace(/&atilde;/g,'\343');
	texte = texte.replace(/&auml;/g,'\344');
	texte = texte.replace(/&aring;/g,'\345');
	texte = texte.replace(/&aelig;/g,'\346');
	texte = texte.replace(/&ccedil;/g,'\347');
	texte = texte.replace(/&egrave;/g,'\350');
	texte = texte.replace(/&eacute;/g,'\351');
	texte = texte.replace(/&ecirc;/g,'\352');
	texte = texte.replace(/&euml;/g,'\353');
	texte = texte.replace(/&igrave;/g,'\354');
	texte = texte.replace(/&iacute;/g,'\355');
	texte = texte.replace(/&icirc;/g,'\356');
	texte = texte.replace(/&iuml;/g,'\357');
	texte = texte.replace(/&eth;/g,'\360');
	texte = texte.replace(/&ntilde;/g,'\361');
	texte = texte.replace(/&ograve;/g,'\362');
	texte = texte.replace(/&oacute;/g,'\363');
	texte = texte.replace(/&ocirc;/g,'\364');
	texte = texte.replace(/&otilde;/g,'\365');
	texte = texte.replace(/&ouml;/g,'\366');
	texte = texte.replace(/&oslash;/g,'\370');
	texte = texte.replace(/&ugrave;/g,'\371');
	texte = texte.replace(/&uacute;/g,'\372');
	texte = texte.replace(/&ucirc;/g,'\373');
	texte = texte.replace(/&uuml;/g,'\374');
	texte = texte.replace(/&yacute;/g,'\375');
	texte = texte.replace(/&thorn;/g,'\376');
	texte = texte.replace(/&yuml;/g,'\377');
	return texte;
}

function htmlspecialchars(ch) {
    ch = ch.replace(/&/g,"&amp;");
    ch = ch.replace(/\"/g,"&quot;");
    ch = ch.replace(/\'/g,"&#039;");
    ch = ch.replace(/</g,"&lt;");
    ch = ch.replace(/>/g,"&gt;");
    return ch;
}

function confirmAllLinks(){
	var confirmLink = function(e){
		if (!confirm("Voulez vous quitter la page sans sauvegarder?")){
            return false;
        }
    };

	var n=document.links.length;
		for (var i=0; i<n; i++){
		document.links[i].onclick = confirmLink;
		}
}


// Ajout d'une version JS de normalisation des accents pour tri sur les multifields par exemple
function normalizeAccents(str){
	var charMap = {'ÀÁÂÃÃÄÅÆ' : 'A','àáâããäåæ': 'a', 'ÈÉÊË': 'E','èéêë': 'e', 'Ç': 'C','ç': 'c', 'ß': 'ss','Š':'S', 'š':'s', 'Ž':'Z', 'ž':'z', 'ÌÍÎÏ':'I','ìíîï':'i', 'Ñ':'N', 'ñ':'n', 'ÒÓÔÕÖØ':'O', 'ðòóôõöø':'o', 'ÙÚÛÜ':'U', 'ùúûü':'u', 'Ý':'Y', 'ýýÿ':'y', 'Þ':'B','þ':'b'};

	$j.each(charMap, function(chars, normalized) {
		var regex = new RegExp('[' + chars + ']', 'gi');
		str = str.replace(regex, normalized);
	  });

  return str;
}


String.prototype.nl2br = function()
{
    return this.replace(/\n/g, "<br />");
}
					
function switchSubtitle(lang) {
	//	console.log('switchSubtitle',lang);
	if (lang != 'off') {
		lang = lang.toUpperCase();
	}
	var buttonSubtitle = $j("#subtitles-" + lang);

	//		console.log('buttonSubtitle',buttonSubtitle,buttonSubtitle.length,$j("#ow_controllerpanel").length);

	// check if element exists
	if (buttonSubtitle.length) {
	//			console.log('buttonSubtitle.click2',buttonSubtitle);
		buttonSubtitle.click();
	}

	//on referme le menu
	var menuSubtitle = $j("#ow_bouton_srt ul");
	if (menuSubtitle.length) {
		menuSubtitle.hide();
	}
}
							
							
/**
*
* @param {type} id_doc
* @param {type} id_mat
* @param {type} lang
* @param {type} isFlv
* @param {type} gListePatternVisio ex: HD, SD (si est défini , on ne doit pas indiquer de id_mat spécifique)
* @returns {undefined}@author B.RAVI 30/05/17 15:35
*
*
*/
function changeSubtitlePlayer(id_doc, id_mat, lang, isFlv, gListePatternVisio) {

	gListePatternVisio = $j.trim(gListePatternVisio);

	_use_html = true;
	if (isFlv) {
		_use_html = false;
	}

	var pattern = '';
	if (gListePatternVisio == '') {
		player_options['addParamsToPHPScript']['id_mat'] = id_mat;

		player_options.onMovieReady = function () {
		setTimeout(function () {
				   switchSubtitle(lang);
				   }, 200);
		};
	} else {
		pattern = 'gListePatternVisio';
		player_options.onMovieReady = function () {
		if ($j("#ow_quality_switch select option:contains('" + gListePatternVisio + "')").length)

		{
		$j("#ow_quality_switch select option:contains('" + gListePatternVisio + "')").prop('selected', true);
		$j("#ow_quality_switch select").change();
		}

		setTimeout(function () {
				   switchSubtitle(lang);
				   }, 200);

		};


	}

	// Si le player n'est pas encore chargé et qu'on a un poster, alors on click sur le poster (=> action standard de lancement du player ..)
	if (typeof myVideo != 'undefined' && myVideo == null && $j("#poster").length > 0) {
		$j("#poster").click();
	} else {
		var current_id_mat = player_options["addParamsToPHPScript"]["id_mat"];
		//si on choisit de lire un autre matÃ©riel que le matÃ©riel qui est en cours de lecture , on relance la vidÃ©o depuis le debut
		//		console.log('id_mat : ' + id_mat);
		//		console.log('current_id_mat : ' + current_id_mat);
		if (id_mat != current_id_mat && !(typeof id_mat === 'undefined' || id_mat === null)) {
			//			console.log('relancé');
			loadOraoPlayer(id_doc, $j('#container').width(), pattern, player_options, true, 'document');
		}
	}


}

// Fonction générale permettant de transformer un objet JS "{ a : 1 , b :2}" en query string "a=1&b=2"
function JSObj2QueryString(obj, prefix){
	var str = [], p;
	for(p in obj) {
		if (obj.hasOwnProperty(p)) {
			var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
			str.push((v !== null && typeof v === "object") ?
			serialize(v, k) :
			encodeURIComponent(k) + "=" + encodeURIComponent(v));
		}
	}
	return str.join("&");
}



function requireCSS(url,callback){
	lien_css = document.createElement('link');
	lien_css.type = "text/css";
    lien_css.rel = "stylesheet";
    lien_css.href = url;
	already_included = false ; 
	$j("head link").each(function(idx,elt){
		if(($j(elt).get(0).href == lien_css.href)){
			already_included= true ; 
		}
	});
	if(!already_included){
		if(typeof callback == 'function'){
			lien_css.onload = callback;
		}
		document.getElementsByTagName("head")[0].appendChild(lien_css);
	}
	
	if(((typeof OW_env_user != 'undefined' && OW_env_user.browser=='Safari') || already_included) && typeof callback == 'function'){
		callback();
	}
}
