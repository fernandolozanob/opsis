
	function rightPanel (videoObject,IDs,params) {
		this.init(videoObject,IDs,params); }

	rightPanel.prototype.init = function (videoObject,IDs,params) {
	 		this.messages=new Array();
	 		this.video=videoObject;
	 		this.IDs=IDs;
	 		this.selectedRow='';
	 		this.showAlertMode='';
	 		this.videoLength=0;
	 		this._hasChanged=false;
	 		this.mySelect=document.getElementById('selectFolder');
	 		this.messages['titreDefaut']='No title';
	 		this.formName='form1';
	 		this.mode='extrait';
	 		this.defaultTCIN='00:00:00:00';
	 		this.displayBar=true;
	 		this.selectAllButton=true;
	 		this.addButton=true;
	 		this.TCButton=true;
	 		this.playButton=false;
	 		this.selectButton=false;
	 		this.removeAllButton=false;
	 		this.language='fr';
	 		this.content_id_lang='fr';
	 		this.toggleAllDeleted=false;
	 		this.xslForm='';
			this.id_container='sidecommande';

			this.onPanelSave = null ;
			this.onAddExtrait= null ;

			this.currentTCIN;
			this.currentTCOUT;

			//update VG 12/08/11 : position de la scrollbar
			this.scrollbarPosition = 'top';

			this.page=1;
			this.nbLignes=10;

			tooltips_fr				= {
				'rp_select_movie' 				: 'Déselectionner les extraits',
				'rp_add_new'					: 'Ajouter une ligne',
				'rp_set_tcin'					: 'Utiliser la position comme début',
				'rp_set_tcout'					: 'Utiliser la position comme fin',
				'rp_save'						: 'Sauver les changements',
				'rp_select_document'			: 'Sélectionner un document à ajouter',
				'rp_play_video'					: 'Voir la vidéo',
				'rp_capture_image'				: 'Capturer une image',
				//update VG 30/05/2011 : ajout de la fonctionnalité de capture et dl d'une image en pleine définition
				'rp_capture_dl_image'			: 'Capturer et télécharger une image pleine définition',
				'rp_loose_changes'				: 'Abandonner les modifications ?',
				'rp_remove_all'					: 'Supprimer/Restaurer tout',
				'rp_arrow_err_tcin'				: 'La position courante ne peut pas être définie comme position de début.',
				'rp_field_err_tcin'				: 'La position renseignée ne peut pas être définie comme position de début.',
				'rp_arrow_err_tcout'			: 'La position courante ne peut pas être définie comme position de fin.',
				'rp_field_err_tcout'			: 'La position renseignée ne peut pas être définie comme position de fin.'
			}

			tooltips_en				= {
				'rp_select_movie' 				: 'Unselect all',
				'rp_add_new'					: 'Add a new line',
				'rp_set_tcin'					: 'Use current position for start',
				'rp_set_tcout'					: 'Use current position for end',
				'rp_save'						: 'Save changes',
				'rp_select_document'			: 'Select a document to add',
				'rp_play_video'					: 'Watch video',
				'rp_capture_image'				: 'Capture image',
				'rp_loose_changes'				: 'Loose all changes ?',
				'rp_remove_all'					: 'Remove/Recover all',
				'rp_arrow_err_tcin'				: 'The current position cannot be defined as the start position.',
				'rp_field_err_tcin'				: 'The position you entered cannot be defined as the start position.',
				'rp_arrow_err_tcout'			: 'The current position cannot be defined as the end position.',
				'rp_field_err_tcout'			: 'The position you entered cannot be defined as the end position.'

			}
	 		if (params['videoLength']) 		this.videoLength=params['videoLength'];
	 		if (params['selectPanier']) 	this.mySelect=document.getElementById(params['selectPanier']);
	 		if (params['msgTitreDefaut']) this.messages['titreDefaut']=params['msgTitreDefaut'];
			if (params['formName']) 		this.formName=params['formName'];
			if (params['mode']) 		this.mode=params['mode'];
			if (params['defaultTCIN']) 		this.defaultTCIN=params['defaultTCIN'];
			if (params['defaultNameCote']) 		this.defaultNameCote=params['defaultNameCote'];
			if (typeof(params['displayBar'])!='undefined') this.displayBar=params['displayBar'];
			if (params['imgDir']) this.imgDir=params['imgDir'];
			if (typeof(params['selectAllButton'])!='undefined') this.selectAllButton=params['selectAllButton'];
			if (typeof(params['addButton'])!='undefined') this.addButton=params['addButton'];
			if (typeof(params['TCButton'])!='undefined') this.TCButton=params['TCButton'];
			if (typeof(params['playButton'])!='undefined') this.playButton=params['playButton'];
			if (typeof(params['selectButton'])!='undefined') this.selectButton=params['selectButton'];
			if (typeof(params['captureButton'])!='undefined') this.captureButton=params['captureButton'];
			//update VG : ajout du bouton de capture et de téléchargement pleine définition
			if (typeof(params['captureAndDlButton'])!='undefined') this.captureAndDlButton=params['captureAndDlButton'];
			if (typeof(params['removeAllButton'])!='undefined') this.removeAllButton=params['removeAllButton'];
			if (params['language']) this.language=params['language'];
				this.content_id_lang=this.language;
			if (params['content_id_lang']) this.content_id_lang=params['language'];
			//update VG 08/09/2010 : ajout de cette ligne, pour �viter que ce soit tout le temps en fran�ais
			if (params['nbLignes']) this.nbLignes=params['nbLignes'];
			if (params['xslForm']) this.xslForm=params['xslForm'];
			if (params['scrollbarPosition']) this.scrollbarPosition=params['scrollbarPosition'];
			//if (!document.getElementById('sidecommande')) alert('no sidecommande !');
			if (params['onPanelSave']) 		this.onPanelSave=params['onPanelSave'];
			if (params['showAlertMode']) 		this.showAlertMode=params['showAlertMode'];

			// on ajoute la possibilité de choisir l'ID du conteneur (c'est toujours plus facile à integrer !)
			if (typeof(params['id_container'])!='undefined')
				this.id_container=params['id_container'];

		if (this.language.toLowerCase() == 'fr')
			this.tooltips = tooltips_fr;
		else
			this.tooltips = tooltips_en;
	}

	rightPanel.prototype.dspBar = function () {

			if (this.displayBar==true) {

				myBar=document.createElement('div');
				myBar.className='icon_bar';
				myBar.id='icon_bar';

				document.getElementById(this.id_container).appendChild(myBar);

				if (this.selectAllButton) {
				_btn=document.createElement('img');
				_btn.id='barAllMovie';
				_btn.src=this.imgDir+'b_film_entier_petit.gif';
				_btn.title=this.tooltips['rp_select_movie'];
				_btn.alt=this.tooltips['rp_select_movie'];
				myBar.appendChild(_btn);
				// by ld 10/11/08 remplacÈ -1 par ''
				_btn.onclick=function () {myPanel.selectExtrait('');}
				// ANNULE - MS 19/12/2012 - "selectionner tout le film" redéfini toute la vidéo comme extrait => nouvelle fonction
				// _btn.onclick=function () {myPanel.selectFullMovie();}
				}

				if (this.addButton){
				_btn2=document.createElement('img');
				_btn2.id='barNew';
				_btn2.src=this.imgDir+'b_nouv_chap_petit.gif';
				_btn2.title=this.tooltips['rp_add_new'];
				_btn2.alt=this.tooltips['rp_add_new'];
				myBar.appendChild(_btn2);
				_btn2.onclick=function () {myPanel.addExtrait();}
				}

				if (this.TCButton) {
				_btn3=document.createElement('img');
				_btn3.id='barTCIN';
				_btn3.src=this.imgDir+'b_definir_debut_petit.gif';
				_btn3.title=this.tooltips['rp_set_tcin'];
				_btn.alt=this.tooltips['rp_set_tcin'];
				myBar.appendChild(_btn3);
				_btn3.onclick=function () {myPanel.setTC('in');}

				_btn=document.createElement('img');
				_btn.id='barTCOUT';
				_btn.src=this.imgDir+'b_definir_fin_petit.gif';
				_btn.title=this.tooltips['rp_set_tcout'];
				_btn.alt=this.tooltips['rp_set_tcout'];
				myBar.appendChild(_btn);
				_btn.onclick=function () {myPanel.setTC('out');}
				}

				if (this.removeAllButton) {
				_btn=document.createElement('img');
				_btn.id='barRemoveAll';
				_btn.src=this.imgDir+'b_remove_all_petit.gif';
				_btn.title=this.tooltips['rp_remove_all'];
				_btn.alt=this.tooltips['rp_remove_all'];
				myBar.appendChild(_btn);
				_btn.onclick=function () {javascript:myPanel.removeAll();}
				}

				if (this.captureButton) {
				_btn=document.createElement('img');
				_btn.id='barCapture';
				_btn.src=this.imgDir+'b_capt_img_petit.gif';
				_btn.title=this.tooltips['rp_capture_image'];
				_btn.alt=this.tooltips['rp_capture_image'];
				myBar.appendChild(_btn);
				_btn.onclick=function () {javascript:capturePicture();}
				}


				if (this.captureAndDlButton) {
				_btn=document.createElement('img');
				_btn.id='barCaptureAndDl';
				_btn.src=this.imgDir+'b_capt_download_img_petit.gif';
				_btn.title=this.tooltips['rp_capture_dl_image'];
				_btn.alt=this.tooltips['rp_capture_dl_image'];
				myBar.appendChild(_btn);
				_btn.onclick=function () {javascript:downloadPhotoShoot();}
				}

				_btn=document.createElement('img');
				_btn.id='barSave';
				_btn.src=this.imgDir+'b_disquette_petit.gif';
				_btn.title=this.tooltips['rp_save'];
				_btn.alt=this.tooltips['rp_save'];
				myBar.appendChild(_btn);
				_btn.onclick=function () {myPanel.save();}


				if (this.selectButton) {
				_btn=document.createElement('img');
				_btn.id='barSelect';
				_btn.src=this.imgDir+'b_select.gif';
				_btn.title=this.tooltips['rp_select_document'];
				_btn.alt=this.tooltips['rp_select_document'];
				myBar.appendChild(_btn);
				_btn.onclick=function () {javascript:choose(this,'titre_index=Document&id_lang='+this.language+'&valeur=&champ=DOC&rtn=addDoc');}
				}


				if (this.playButton) {
				_btn=document.createElement('img');
				_btn.id='barPlay';
				_btn.src=this.imgDir+'b_play_petit.gif';
				_btn.title=this.tooltips['rp_play_video'];
				_btn.alt=this.tooltips['rp_play_video'];
				myBar.appendChild(_btn);
				_btn.onclick=function () {loadVideo();}
				}

			}
			//if (document.getElementById('iframeSauve')) return;
			// 07/04/11 : modification d'un test sur IE (6.0) qui rendait incompatible rightPanel avec IE 9 (reste compatible avec IE  7 et 8)
			if (/msie 6.0/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent)) { //Special IE6 qui n'accepte pas de setAttribute name
			_fr= document.createElement("<iframe name='iframeSauve'></iframe>");
			} else {
			_fr = document.createElement("iframe");			}
			_fr.src='';
			_fr.id='iframeSauve';
			_fr.name='iframeSauve';
			_fr.style.width='0px';
			_fr.style.height='0px';
			_fr.style.visibility='hidden';
			//_fr.innerHTML='&#160;';
			document.getElementById(this.id_container).appendChild(_fr);

			myBar=document.createElement('div');
			myBar.id='zoneSaisie';
			document.getElementById(this.id_container).appendChild(myBar);
	}


	rightPanel.prototype.getTCOutFromVideo = function () {
		if (this.video) this.videoLength=this.video.GetCurrentSelectionEnd();
	}

	rightPanel.prototype.refreshContent = function(){
		if(typeof this.video !="undefined"&&this.video && this.video.movie_options && typeof this.video.movie_options.tcMosaicExact != "undefined" && this.video.movie_options.tcMosaicExact == true){
			this.video.movie_options.tcMosaicExact = false ;
		}

		if (this._hasChanged)
			if (!confirm(this.tooltips['rp_loose_changes'])){
				for (var id = 0; id < this.mySelect.options.length; id++)
					if (this.mySelect.options[id].value == idPanier)
						this.mySelect.selectedIndex = id;
				return;
			}
			if (document.getElementById('zoneSaisie')) document.getElementById('zoneSaisie').innerHTML="<center><img src='design/images/wait30trans.gif'></center>";

			if (this.mode=='extrait') {
				if (!this.mySelect ) return;
				idPanier=this.mySelect.options[this.mySelect.selectedIndex].value;
				if (this.xslForm=='') this.xslForm='oraoweb_extrait';
				return !sendData('GET','blank.php','xmlhttp=1&urlaction=processPanier&id_panier='+idPanier+'&commande=view&xsl='+this.xslForm+'&items='+this.IDs,'myPanel.getCart');
			}
			if (this.mode=='sequence') {
				if (this.xslForm=='') this.xslForm='oraoweb_sequence';
				return !sendData('GET','blank.php','xmlhttp=1&urlaction=processSequence&id='+this.IDs+'&commande=view&xsl='+this.xslForm,'myPanel.getCart');
			}

			if (this.mode=='materiel') {
				if (this.xslForm=='') this.xslForm='oraoweb_document';
				return !sendData('GET','blank.php','xmlhttp=1&urlaction=processDocument&id='+this.IDs+'&commande=view&xsl='+this.xslForm,'myPanel.getCart');
			}

			if (this.mode.indexOf('storyboard')!=-1) {
				storymode=this.mode.substring(this.mode.indexOf(':')+1);
				if (this.xslForm=='') this.xslForm='oraoweb_storyboard';

				return !sendData('GET','blank.php','xmlhttp=1&urlaction=processStoryboard&id='+this.IDs+'&commande=view&xsl='+this.xslForm+'&from='+storymode+'&page='+this.page+'&nbLignes='+this.nbLignes+'&id_lang='+this.content_id_lang,'myPanel.getCart');
			}

	}

	rightPanel.prototype.getCart = function (xml) {
		document.getElementById('zoneSaisie').innerHTML=xml;
		// MS (extraits) si création d'un nouveau panier => update du select des paniers (detection de la chaine 'value="<id_panier>' ou pour IE8/IE7 : 'value=<id_panier>' )
		if(document.getElementById('id_panier') && typeof(this.mySelect)!="undefined" && this.mode=='extrait'
		&& ((this.mySelect.innerHTML.indexOf('<option value="') ==0 && this.mySelect.innerHTML.indexOf('value="'+document.getElementById('id_panier').value) == -1 )
		|| (this.mySelect.innerHTML.indexOf('<OPTION') ==0 && this.mySelect.innerHTML.indexOf('value='+document.getElementById('id_panier').value) == -1))){
			// this.mySelect.innerHTML += '<option value="'+document.getElementById('id_panier').value+'">'+document.getElementById('pan_titre').value+'</option>'
			new_opt = document.createElement('option');
			new_opt.value = document.getElementById('id_panier').value;
			new_opt.innerHTML = document.getElementById('pan_titre').value;

			this.mySelect.appendChild(new_opt);
			this.mySelect.selectedIndex = this.mySelect.getElementsByTagName('option').length -1 ;
			// Si refreshFolders existe => on l'appelle pour updater les affichages paniers / sélections
			if(window.refreshFolders){
				refreshFolders();
			}
		}

		//update VG : gestion de 2 positions de la scrollbar : en haut, ou en bas
		if(this.scrollbarPosition == 'top')
			document.getElementById('zoneSaisie').scrollTop = 0;
		else
			document.getElementById('zoneSaisie').scrollTop = document.getElementById('zoneSaisie').scrollHeight;
	}

	rightPanel.prototype.resetContent= function () {
		document.getElementById(this.id_container).innerHTML='';
	}

	rightPanel.prototype.hasChanged= function (row) {
		document.getElementById('barSave').src=this.imgDir+'b_disquette_petit_anim.gif';
		// row.style.border='1px solid #66FF66';
		if(row.className.indexOf("hasChanged") == -1){
			row.className+=" hasChanged ";
		}
		this._hasChanged=true;

	}
	rightPanel.prototype.setCurr=function(row){
		_tcin=getChildById(row,'tcin$');
		_tcout=getChildById(row,'tcout$');
		_titre=getChildById(row,'ext_titre$');
		this.currentTCIN = _tcin.value;
		this.currentTCOUT = _tcout.value;

	}

	rightPanel.prototype.checkLine = function (row,dontSetSelection) {

		if(typeof dontSetSelection == "undefined"){dontSetSelection = false ; }
		_tcin=getChildById(row,'tcin$');
		_tcout=getChildById(row,'tcout$');
		_titre=getChildById(row,'ext_titre$');

		retour = true;

		if (_tcin.value=='' || _tcin.value<this.defaultTCIN ) {
			new Effect.Pulsate(_tcin);_tcin.value=this.defaultTCIN;
			_tcin.focus();

			retour = false ;
		}
		if( this.currentTCOUT &&_tcin.value>this.currentTCOUT){
			_tcin.value = this.currentTCIN;
			alert(this.tooltips['rp_field_err_tcin']);
			return false ;
		}

		if (_tcout.value=='' || _tcout.value>this.videoLength) {
			_tcout.focus();
			new Effect.Pulsate(_tcout);_tcout.value=this.videoLength;

			retour = false ;
		}

		if(this.currentTCIN &&  _tcout.value<this.currentTCIN){
			_tcout.value = this.currentTCOUT;
			alert(this.tooltips['rp_field_err_tcout']);
			return false ;
		}

		if ((this.mode =='sequence' || this.mode == 'extrait') && _titre.value.Trim()==''  ) {
			if(typeof(_titre.style.opacity) == 'undefined' || _titre.style.opacity == '' ) {
				new Effect.Pulsate(_titre);
			}
			// MS ajout index au titre par défaut
			var k=1, e=row;
			while (e = e.previousSibling) {if(e.nodeName == 'DIV' && e.id.indexOf('row$')==0 ){++k;}}
			_titre.value=this.messages['titreDefaut']+' '+k;
			_titre.focus();

			retour = false ;
		}

		if((this.mode =='sequence' || this.mode == 'extrait') && this.video && !dontSetSelection && row == this.selectedRow){
			// MS l'appel à video.SetSelection n'est fait que si le row en cours de check est le row selectionné par l'utilisateur
			//  On empeche l'appel SetSelection lors des checklines appelés pendant la sauvegarde.
			this.video.SetSelection(_tcin.value,_tcout.value,false);
		}

		this.currentTCIN = null;
		this.currentTCOUT = null;

		return retour;
	}

	rightPanel.prototype.save =function () {
		var ok = true;
		alldiv=document.getElementById('zoneSaisie').getElementsByTagName('div');
		for (idx=0;idx<alldiv.length;idx++) {
			ligneAction = getChildById(alldiv[idx],'action$');
			if (alldiv[idx].id.indexOf('row$')==0 && alldiv[idx].id.indexOf('row$blank') == '-1' && ligneAction.value != 'suppr' ) {

				// MS 01/02/13 => reset des currentTCIN / currentTCOUT.
				// ces variables sont utilisées lorsqu'on modifie un extrait, mais ne devraient pas avoir d'influence sur la partie contrôle final
				// (en réalité on ne devrait plus avoir de contrôle final si on contrôle les inputs au préalable)


				lineOk = this.checkLine(alldiv[idx],true);

				if(!lineOk) {
					ok = false;
				}
			}
		}
		if(!ok) {
			return false;
		}
		document.getElementById('barSave').src=this.imgDir+'b_disquette_petit.gif';
		if(this.mode =='sequence' || this.mode == 'extrait') {
			this.selectExtrait('');
		}

		if(typeof document.forms[this.formName].submit != "undefined"){
			document.forms[this.formName].submit();
		}else{
			if(typeof document.form1.length != "undefined" && document.form1.length>1){
				document.form1[0].submit();
			}else{
				document.form1.submit();
			}
		}
		this._hasChanged=false;
		if(typeof this.onPanelSave == "function"){
			try{
				idPanier=this.mySelect.options[this.mySelect.selectedIndex].value;
				this.onPanelSave(idPanier);
			}catch(e){
				console.log("fail on callback onPanelSave \n"+e);
			}
		}


	}

	rightPanel.prototype.showResultInParent=function (elt) {
		//GËre le retour de la sauvegarde depuis l iframe / processPanier
		//Attention, on utilise une astuce pour contourner un bug FF
		//Explications : refreshContent va dÈtruire et recrÈer l iframe depuis laquelle cette fonction est lancÈe
		//Or FF ne permet pas l appel d une fonction (a fortiori AJAX) depuis un object en cours de destruction
		//Donc on effectue un lancement indirect de la fonction via un "faux" timeout

		//alert(elt.innerHTML);
		showAlert(elt.innerHTML,'alertBox',this.showAlertMode,0,190); //affiche le rÈsultat + dÈcalage vertical pour afficher sous la vidÈo

   		window.setTimeout(function () {myPanel.refreshContent();}, 0);
	}


	rightPanel.prototype.selectExtrait=function(thisrow) {
		if (this.selectedRow!='' && this.selectedRow==thisrow) { //toggle selected/off
			this.selectExtrait('');
			return;
		}
		this.selectedRow=thisrow;
		alldiv=document.getElementById('zoneSaisie').getElementsByTagName('div');
		for (i=0;i<alldiv.length;i++) { //raz styles
			if (alldiv[i].id.indexOf('row$')==0) alldiv[i].className = alldiv[i].className.replace('row_extrait_selected','row_extrait');
			if (alldiv[i].id.indexOf('handle$')==0) alldiv[i].className =  alldiv[i].className.replace('ext_handler_selected','ext_handler');
		}
		if (thisrow!='') { //highlight sur ligne sÈlectionnÈe
			_n=getChildById(thisrow,'action$');
			if (!_n || _n.value=='suppr') return; //on n active pas une ligne marquÈe pour suppr
			thisrow.className= thisrow.className.replace('row_extrait','row_extrait_selected');
			_n=getChildById(thisrow,'handle$')
			if (_n) _n.className= _n.className.replace('ext_handler','ext_handler_selected');
			_n=getChildById(thisrow,'tcin$');
			if (_n) _tcin=_n.value;
			_n=getChildById(thisrow,'tcout$');
			if (_n) _tcout=_n.value;
			
			if (this.video) this.video.SetSelection(_tcin,_tcout,true);
		} else { //rewind au dÈbut
			if (this.video) {
				//by ld 10/11/08 : on ne retourne plus au dÈbut
				//this.video.GoToBegSelection();
				this.video.UnSetSelection();}

		}
	}

	rightPanel.prototype.addExtrait= function() {
	 	blankDiv=document.getElementById('row$blank');
	 	myNewRow=blankDiv.cloneNode(true);
	 	myNewRow.id='row$cloned'; //chgt id car il ne faut qu'un seul row$blank
	 	//21/10/08 : clone rattachÈ au formulaire et non au row$blank.parentNode
	 	document.getElementById(this.formName).appendChild(myNewRow);
	 	myNewRow.style.display='block'; //on rend visible le clone
		// VP 31/08/10 : positionnement tcin et tcout en fct de la sélection utilisateur
	 	if (this.video) {
			_tcin=this.video.GetCurrentSelectionBeginning();
			_tcout=this.video.GetCurrentSelectionEnd();
			_tc=this.video.GetCurrentLongTimeCode();

			// MS fonctionnement standard :
			// Si aucun extrait actif & que les curseurs de sélection sont dans leur état par défaut => Nouvel extrait in : pos° courante, out : fin de la vidéo
			// Si aucun extrait actif & que les curseurs de sélection sont sets => Nouvel extraiton utilise les curseurs pour définir l'extrait
			// Si un extrait actif 	=> Nouvel extrait in : pos° courante, out : fin de la vidéo
			if((_tcin == _tcout && _tcin.substr(0,8)==this.defaultTCIN.substr(0,8) )|| this.selectedRow != '' ){//_tc < _tcin || _tc > _tcout || this.video.flag_sub_selection_set!=undefined
				_tcin=_tc;
				_tcout=(this.videoLength!=0?this.videoLength:'00:00:00:00');
			}
		}else{
			_tcin=this.defaultTCIN;
			_tcout=(this.videoLength!=0?this.videoLength:'00:00:00:00');
		}

	 	//if (this.video) _tcin=this.video.GetCurrentLongTimeCode(); else _tcin=this.defaultTCIN;
		//videoLength est maj par le load video mais parfois il est ‡ 0 car la video n'est pas chargÈe complËtement
		//if (this.videoLength==0 && this.video) Video ok mais TC out=0 ? on retente de rÈcup le tc out
		// VP 12/12/09 : quid de ce TC ?
		//_tcout=(this.videoLength!=0?this.videoLength:TC);
		_n=getChildById(myNewRow,'tcin$');
		if (_n) _n.value=_tcin;
		_n=getChildById(myNewRow,'tcout$');
		if (_n) _n.value=_tcout;
		_n=getChildById(myNewRow,'ext_titre$');
		if (_n) {
			// MS ajout index au titre par défaut
			var k=1, e=myNewRow;
			while (e = e.previousSibling) {if(e.nodeName == 'DIV' && e.id.indexOf('row$')==0 && e.id != 'row$blank' ){++k;}}
			_n.value=this.messages['titreDefaut']+' '+k;
		}
		_n=getChildById(myNewRow,'ext_cote$');
		if (_n) {
			if(!this.defaultNameCote){
				// MS ajout index au titre par défaut
				var k=1, e=myNewRow;
				num = null ; 
				while (e = e.previousSibling) {
					if(e.nodeName == 'DIV' && e.id.indexOf('row$')==0 && e.id != 'row$blank' ){
						sibling_ext_cote = getChildById(e,'ext_cote$'); 
						tmp_num = sibling_ext_cote.value.match(/[0-9]+$/i);
						if(num == null || parseInt(tmp_num[0],10) > num){
							num =  parseInt(tmp_num[0],10);
						}
						++k;
					}
				}
				k = Math.max(++num,k); 
				if (k < 10) k = "0"+k;
				_n.value=_n.value+k;
			}else _n.value=this.defaultNameCote;
		}
		this.hasChanged(myNewRow);
	 	if (this.mode.indexOf('storyboard') == -1)
			this.selectExtrait(myNewRow); //pas de selection auto en mode sb
		 // VP 10/09/09 : Calcul de la durée
		_n=getChildById(myNewRow,'duration$');
		if (_n){
			_duration=diffTC(_tcout,_tcin);
			_n.value=_duration;
		}
	 	// VP 10/09/09 : Scroll div zoneSaisie
		if (document.getElementById('zoneSaisie')) {
			document.getElementById('zoneSaisie').scrollTop=document.getElementById('zoneSaisie').scrollHeight;
		}

		if(typeof this.onAddExtrait == "function"){
			try{
				this.onAddExtrait(myNewRow);
			}catch(e){
				console.log("fail on callback onAddExtrait \n"+e);
			}
		}

		this.selectedRow = myNewRow;
		return myNewRow;
	}

	rightPanel.prototype.removeExtrait = function(row) {

		new Effect.Opacity(row,{from:1,to:0.4,duration:0.3});

		_n=getChildById(row,'trash$');
		if (_n) {
			_n.onclick=function(){myPanel.reactivateRow(row);};
			_n.src=this.imgDir+'button_drop_restore.gif';
		}
		_n=getChildById(row,'action$');
		if (_n) _n.value='suppr';
		// this.hasChanged(row);

		if (row==this.selectedRow) {this.selectExtrait('');}

	}
	rightPanel.prototype.removeAll = function() {
		alldiv=document.getElementById('zoneSaisie').getElementsByTagName('div');


		for (idx=0;idx<alldiv.length;idx++) {
			if (alldiv[idx].id=='row$') {
				if (this.toggleAllDeleted) this.reactivateRow(alldiv[idx]); else this.removeExtrait(alldiv[idx]);
			}
		}
		this.toggleAllDeleted=!this.toggleAllDeleted;

		/*
		_n=getChildById(row,'trash$');
		if (_n) {
			_n.onclick=function(){myPanel.reactivateRow(row);};
			_n.src=this.imgDir+'button_drop_restore.gif';
		}
		_n=getChildById(row,'action$');
		if (_n) _n.value='suppr';
		this.hasChanged(row);

		if (row==this.selectedRow) {this.selectExtrait('');}*/

	}


	rightPanel.prototype.reactivateRow=function(row) {

		new Effect.Opacity(row,{from:0.4,to:1,duration:0.3});

		_n=getChildById(row,'trash$');
		if (_n) {
			_n.onclick=function(){myPanel.removeExtrait(row);};
			_n.src=this.imgDir+'button_drop.gif';
		}
		_n=getChildById(row,'action$');
		if (_n) _n.value='edit';
	}

	rightPanel.prototype.positionCursor=function(tc) {
		if (!tc || !this.video) return;
			//t = this.video.LongTimeCodeToQTTime(tc);
			//this.video.theVideo.SetTime(t);

		if (typeof(this.video.GoToLongTimeCode)!='undefined')
			this.video.GoToLongTimeCode(tc);
		else
		{
			var element=tc.split(':');
			var tc_sec=parseInt(element[0])*3600+parseInt(element[1])*60+parseInt(element[2]);
			this.video.gotoTC(tc_sec);
		}
	}

	rightPanel.prototype.setTC=function (inout) {
		if (typeof(this.selectedRow)=='undefined' || this.selectedRow=='') return;
		if (this.video)
		{
			//by ld 03 12 08 : on stoppe la video quand on place un tcout
			if (inout=='out') this.video.StopTheVideo();
			TC=this.video.GetCurrentLongTimeCode();

			// VP 31/08/10 : ajout contrôle des bornes
			_tcin=this.video.GetCurrentSelectionBeginning();
			_tcout=this.video.GetCurrentSelectionEnd();
			// if(inout=='out' && TC<_tcin) inout='in';
			// else if(inout=='in' && TC>_tcout) inout='out';

			// BUG si creation extrait avant demarrage player
			// les curseurs tcin et tcou du player sont au position 00:00:00:00 et 00:00:00:00
			if (inout=='in' && TC>_tcout && _tcout=='00:00:00:00' && _tcin=='00:00:00:00')
			{
				//on met le tcout a la fin de la video
				_tcout=this.videoLength;
			}

			// MS 19/12/12 : setTc in et setTc out sont maintenant strictes
			// Si new_tc_in > tc_out || new_tc_out < tc_in  ==> message d'erreur
			if(inout=='in' && TC>_tcout){
				alert(this.tooltips['rp_arrow_err_tcin']);
				return false;
			}else if(inout=='out' && TC<_tcin){
				alert(this.tooltips['rp_arrow_err_tcout']);
				return false;
			}



			_n=getChildById(this.selectedRow,'tc'+inout+'$');
			if (_n) _n.value=TC;

		}
		this.hasChanged(this.selectedRow);

		_n=getChildById(this.selectedRow,'tcin$');
		if (_n) _tcin=_n.value;
		_n=getChildById(this.selectedRow,'tcout$');
		if (_n) _tcout=_n.value;
		if(_tcin > _tcout){
			_tc=_tcin;
			_tcin=_tcout;
			_tcout=_tc;
			_n=getChildById(this.selectedRow,'tcin$');
			if (_n) _n.value=_tcin;
			_n=getChildById(this.selectedRow,'tcout$');
			if (_n) _n.value=_tcout;
		}
		_n=getChildById(this.selectedRow,'duration$');
		if (_n){
			_duration=diffTC(_tcout,_tcin);
			_n.value=_duration;
		}

		if (this.video) this.video.SetSelection(_tcin,_tcout,false);

	}


	rightPanel.prototype.updateSelection=function () {
		if (typeof(this.selectedRow)=='undefined' || this.selectedRow=='') return;
		if (this.video)
		{
			_tcin=this.video.GetCurrentSelectionBeginning();
			_tcout=this.video.GetCurrentSelectionEnd();
			_n=getChildById(this.selectedRow,'tcin$');
			if (_n) _n.value=_tcin;
			_n=getChildById(this.selectedRow,'tcout$');
			if (_n) _n.value=_tcout;
			this.hasChanged(this.selectedRow);
			_n=getChildById(this.selectedRow,'duration$');
			if (_n){
				_duration=diffTC(_tcout,_tcin);
				_n.value=_duration;
			}
			//XB
			if(typeof transcript_seqsaisie != "undefined"){
				_tcin = myVideo.timecodeToSecs(_tcin);
				_tcout = myVideo.timecodeToSecs(_tcout);
				highlightSequence(_tcin,_tcout)
			}

		}
	}

	// rightPanel.prototype.selectFullMovie=function () {

	//// MS 19/12/2012 - "selectionner tout le film" redéfini toute la vidéo comme extrait

	// _tcin = this.defaultTCIN;
	// _tcout = this.videoLength;

	// this.video.SetSelection(_tcin,_tcout);
	// _n=getChildById(this.selectedRow,'tcin$');
	// if (_n) _n.value=_tcin;
	// _n=getChildById(this.selectedRow,'tcout$');
	// if (_n) _n.value=_tcout;
	// _n=getChildById(this.selectedRow,'duration$');
	// if (_n){
		// _duration=diffTC(_tcout,_tcin);
		// _n.value=_duration;
	// }

	//// this.selectExtrait(this.selectedRow);
	// }

	rightPanel.prototype.debug=function () {

	}



