/*************************/
// Call-back JWPlayer
/*************************/
var currentTime=0;

function playerReady(obj) {
	//alert(obj['id']);
	moviePlayer = document.getElementById('ply');
	if(moviePlayer) moviePlayer.addModelListener('TIME', 'timeMonitor');
};


function timeMonitor(obj){
	currentTime=obj.position;
}

/*************************/
// Debut code OJW_player
/*************************/
function OJW_player(container, player_options)
{
	this.typeMime = '';
	this.controlbar='none';
	this.simpleController = false ;
	this.showFlashController = false ;
	this.dock='true';
	this.icons='true';
	this.autoplay=true;
	this.preload = true ;
	this.muted = false ;
	this.fullscreenOpt=true;
	this.url_pattern='';
	this.url_format='';
	this.enable_streaming=false;
	this.player_id='';
	this.skin="";
	this.tc_in='00:00:00:00';
	this.resizeCustom = false ;
	this.provider;
	this.offset_controlbar_height = 0;
	this.retardSeek = 0;
	this.flashRetardSeek = 0 ;
	this.canSeek = true ;
	this.fastSteppingOn = null;
    this.fastSteppingDirection = 0;

	this.dontCallOnChange = false ;
	this.pauseAfterSeek = false ;

	this.loadedMetadata = false ;

	// variables timeline (en réalité deux sliders séparés)
	this.timeline;			// timeline
	this.timelineSel;		// timeline de sélections

	this.ClickOnTimeline = false;   //flag permettant d'identifier la source d'un onChange timeline comme une action de l'utilisateur

	// variables de selection
	this.BegSelect = 0;
	this.EndSelect = 0;
	this.selectionExists= false;
	this.SelSlide = false;
	this.selectionSliderDisabled = false ;
	//lecture en boucle
	this.loopVideo=false;

	// variables boutons extraits :
	this.extsBtns = false ;


	this.currentPlyId;

	//  MS 21/05/2013 : PLAYLIST
	this.playlist=false;
	this.current_track=-1;
	this.nb_tracks=0;
	this.duration=0;
	this.BegTrack=0;
	this.EndTrack=0;
	this.url='';
	this.track_init_events = new Array();
	this.track_location=new Array();
	this.track_start=new Array();
	this.track_end=new Array();
	this.track_duration=new Array();
	this.track_media=new Array();
	this.track_start_offset=new Array();
	this.playNextFlag = false ;
	this.currentTrackEnded = false ;

	this.display_buff = true ;


	// VP 27/01/12 : initialisation movie_options
	this.movie_options={
	//parametres principaux
		url : '',
		xml : '', // xml playlist & flag d'utilisation des comportements playlist
		playlist : false,

		//plusieurs qualités de médias
		urls : '',
		typesMime : '',
		quality_names : '',

		width : 400,
		height : 300,

		//imageRootURL : "images/",			// Interet a demander a vincent vu que toutes les images se parametrent dans le css.
		typeMime : "",
		name : "ply",
		autoplay : true,
		image : null,
		noPoster : false,
		language : 'fr',
		debug_mode : 'false',
		stepping_frequency : 0.1,

		// Display timeline
		hauteur_track :10,		// Hauteur en pixel de la track a definir via movie_options si custom
		hauteur_bg_track : 4,		// Hauteur en pixel du background / des divs de bufferisation
		span_time_passed : false,


		previewOverTimeline : false ,	// Active le preview imagette en rollover sur la timeline
		mosaic_path : null ,			// chemin du fichier mosaique du storyboard
		mosaic_step : 10,				// écart entre chaque image de la mosaique
		tcMosaicExact : false ,
		offset_mouseEvent_timeline : 3, // du au fonctionnement du prototype slider, on doit légèrement ajuster la valeur rapportée par le mouseEvent pour que les temps affichés sur la timeline & dans le preview soit cohérents
		// variable liste des timecodes des images de la mosaique fournie dans mosaic_path
		mosaic_pics_index : null,
		mosaic_use_index : false,

		selectionHidden : true,

		extsBtnsOnStart : false,
		extsContext : "montage",
		extsBtnsDisabled : true ,

		xStand : false, // Cas particulier xStand => non détectable car basé sur Safari => paramètre spécial pr le detecter & pouvoir workaround

		// TC
		timecode_lag : null  ,      // option d'offset
		timecode_IN :   null,
		timecode_OUT :   null,
		timecode_with_frame : false  ,
		timecode_display_format : 'hh:mm:ss:ff',
		show_tc_with_lag : true,

		//mode affichage :
		//scale : 'aspect' ,			// on ne peut pas le parametrer en html 5 , la video garde toujours son ratio normal
		fit_height_container : false,
		movie_background_color : 'black',

		show_poster_on_ended : false ,

		force_ratio_ref : 16/9,

		//fonctions callback :
		onMovieFullScreen : null,
		onMovieEndFullScreen : null,
		onPlayerReady : null,
		onMovieFinished : null,
		onTrackChange : null,
		onTimecodeChange : null,
		onExtSave : null,
		onSelectionSliderChange : null,
		urlOfEndImage : '',

		displayLoopButton:false,

		//template :
		force_width_controller : false,
		controller_on_video : false,
		code_controller :  '<!-- Controller Design -->														' +
		'       <div id = ow_id_progress_slider>	</div>				' +
		'       <div id="ow_first_row_controller">		' +
		'			<div id="ow_id_jog_slider"></div><p id="joginfo">0x</p> 		' +
		'      		<p id="timecodeDisplay">00:00:00:00</p>                                                             ' +
		'      		<div id="ow_quality_switch"></div>                                                             ' +
		'     		<div id="ow_boutons_extraits">				' +
						'<a class = "btn" id ="ow_bouton_ext_in"></a>' +
						'<a class = "btn" id="ow_bouton_ext_out"></a>' +
						'<a class = "btn" style="display:none;" id = "ow_bouton_ext_new"></a>' +
						'<a class = "btn" style="display:none;" id="ow_bouton_ext_save"></a>' +
						'<a class = "btn" id = "ow_bouton_ext_cancel"></a>' +
		'			</div>                                                         ' +
		'       </div>                                                         ' +
		//'		<div id = "jogcontainer"> <p id="joginfo">0x</p> <div id = ow_id_jog_slider></div>	</div>			' +
		//'       <p id="timecodeDisplay">00:00:00:00</p>                                                             ' +
		'		 <div id = "ow_boutons">																			' +
		'		<div id = "containSound"><div id = "ow_id_sound_slider"></div></div>																	' +
        '<a class = "btn" id = "ow_bouton_mute_sound"></a>'+
        '<a class = "btn" id = "ow_bouton_go_to_beginning"></a>' +
        '<a class = "btn" id = "ow_bouton_fast_rewind"></a>' +
        '<a class = "btn" id = "ow_bouton_step_rewind"></a>' +
        '<a class = "btn play" id = "ow_bouton_play_pause" ></a>' +
        '<a class = "btn" id = "ow_bouton_step_forward"></a>' +
        '<a class = "btn" id = "ow_bouton_fast_forward"></a>' +
        '<a class = "btn" id = "ow_bouton_go_to_end"></a>' +
        '<a class = "btn" id = "ow_bouton_fullscreen"></a>' +

		'<a class="btn" id="ow_bouton_loop"></a>'+


		'</div>'
		+'<div>																			' +
		     // Tous les éléments de class temp sont des éléments utilisés pour le dev
		'		<input type = "button" id = "boutonInfo" class = "temp" value = "infos">										' +
		'		<input type = "button" id = "boutonInfo2" class = "temp" value = "show">									' +
		'		<p class = "" id = "infobuff"> </p>													' +
		'		<p class = "temp" id = "infobuff2"> </p>													' +
		'		<p class = "temp" id = "infobuff3"> </p>													' +
		'		<p class = "temp" id = "infobuff4"> </p>													' +
		'		<p class = "temp" id = "infobuff5"> </p>													' +
		'</div>'  ,


		basic_function_button : {
			'ow_bouton_play_pause' : this.ToggleVideo.bind(this),
			'ow_bouton_mute_sound' : this.SetMuteState.bind(this),
			// 'ow_bouton_fullscreen' : this.ToggleFullScreen.bind(this),
			'ow_bouton_go_to_beginning' :  this.GoToBegAvailable.bind(this),
			'ow_bouton_go_to_end' :  this.GoToEndAvailable.bind(this),
			//'ow_bouton_step_forward' : this.SlowPlaying(1), //MSTEST function(){jwplayer().seek(this.timeline.value+10);}.bind(this),
			// 'ow_bouton_step_rewind' : this.SlowPlaying(-1),
			'ow_bouton_fast_rewind' :  this.FastPlaying(-1),                                             //this.InitStepping.bind(this,"fast",-1), (pour la vidéo uniquement, remplacé par une detection
			'ow_bouton_fast_forward' : this.FastPlaying(1),                                        //this.InitStepping.bind(this,"fast",1),   et une association en fonction du media)
			'ow_bouton_ext_in' : this.extMarkTC.bind(this,"in"),
			'ow_bouton_ext_out' : this.extMarkTC.bind(this,"out"),
			'ow_bouton_ext_cancel' : this.cancelExt.bind(this),
			'ow_bouton_ext_new' : this.newExt.bind(this),
			'ow_bouton_ext_save' : this.saveExt.bind(this)


			// Boutons tests pour le développement
			// 'boutonInfo' : this.HideSelectionSlider.bind(this),
			// 'boutonInfo2' : this.ShowSelectionSlider.bind(this)
		},
		//tooltips
		tooltips_fr : {
			'ow_id_timecode' 				: 'Time Code (HH:mm:ss)',
			'ow_id_timecodeLong' 			: 'Time Code (HH:mm:ss.frame)',
			'ow_id_sound_slider'			: 'Volume sonore',
			'ow_id_progress_slider'			: 'Barre de progression',
			'ow_id_jog_slider'				: 'Jog Shuttle',
			'ow_bouton_play' 				: 'Lecture',
			'ow_bouton_pause' 				: 'Pause',
			'ow_bouton_play_pause' 			: 'Lecture ou Pause (suivant contexte)',
			'ow_bouton_play_again'			: 'Relance la visionneuse',
			'ow_bouton_fast_rewind' 		: 'Retour rapide',
			'ow_bouton_fast_forward' 		: 'Avance rapide',
			'ow_bouton_step_rewind'			: 'Reculer d\'une image',
			'ow_bouton_step_forward'		: 'Avancer d\'une image',
			'ow_bouton_go_to_beginning'		: 'Retourner au début du film',
			'ow_bouton_go_to_end'			: 'Aller a la fin',
			'ow_bouton_fullscreen'			: 'Active ou desactive le mode plein ecran',
			'ow_bouton_mute_sound' 			: 'Coupe le son',
			'ow_bouton_loop'				: 'Lecture en boucle',
			'ow_bouton_download'			: 'Téléchargement',
			'ow_bouton_add2cart'			: 'Ajouter au panier',
			'ow_bouton_removefromcart'		: 'Retirer du panier'
		},
		tooltips_en : 	{
			'ow_id_timecode' 				: 'Time Code (HH:mm:ss)',
			'ow_id_timecodeLong' 			: 'Time Code (HH:mm:ss.frame)',
			'ow_id_sound_slider'			: 'Sound volume',
			'ow_id_progress_slider'			: 'Progress bar',
			'ow_id_jog_slider'				: 'Jog Shuttle',
			'ow_bouton_play' 				: 'Play',
			'ow_bouton_pause' 				: 'Pause',
			'ow_bouton_play_pause' 			: 'Play or Pause (switch)',
			'ow_bouton_play_again'			: 'Restart the player',
			'ow_bouton_fast_rewind' 		: 'Fast rewind',
			'ow_bouton_fast_forward' 		: 'Fast forward',
			'ow_bouton_step_rewind'			: 'Back up one image',
			'ow_bouton_step_forward'		: 'Forward by one image',
			'ow_bouton_go_to_beginning'		: 'Go to the beginning',
			'ow_bouton_go_to_end'			: 'Go to the end',
			'ow_bouton_fullscreen'			: 'Toggle fullscreen',
			'ow_bouton_mute_sound' 			: 'Sound off',
			'ow_bouton_loop'				: 'Loop playing'
		}
	};



	this.init(container,player_options);							//appel fonction d'initialisation du player

}

OJW_player.prototype.init=function(container,player_options)
{

	this.player=player_options.player;
	if (player_options.url!=null)
		this.url = player_options.url;
	this.width=player_options.width;
	this.height=player_options.height;

	this.fullscreen = false ;


	// Permet d'update les paramètres par défaut à partir de l'objet player_options passé en argument
	for (o in player_options){		// Pour chaque clé dans player_options
		if(this.movie_options.hasOwnProperty(o)){		// Si movie_options possède une même clé
			this.movie_options[o] = player_options[o];	// Mise a jour de movie_options à partir de player_options
		}
	}

	if(player_options.typeMime !=null)
		this.typeMime = player_options.typeMime;

	this.mediaType = this.mediaTypeDetection(this.url,this.typeMime);


	if (player_options.playlist!=null)
		this.playlist=player_options.playlist;

	if (player_options.image!=null)
		this.image=player_options.image;

	if (player_options.logo!=null)
		this.logo=player_options.logo;

	if (player_options.autoplay!=null)
		this.autoplay=player_options.autoplay;

	if (player_options.preload!=null)
		this.preload=player_options.preload;

	if (player_options.fullscreen!=null)
		this.fullscreenOpt=player_options.fullscreen;

	if(player_options.skin!=null)
		this.skin=player_options.skin;

	if(player_options.controlbar!=null)
		this.controlbar=player_options.controlbar;

	if(player_options.simpleController!=null)
		this.simpleController=player_options.simpleController;

	if(player_options.showFlashController!=null)
		this.showFlashController=player_options.showFlashController;

	if(player_options.dock!=null)
		this.dock=player_options.dock;

	if(player_options.icons!=null)
		this.icons=player_options.icons;

	if(player_options.url_pattern!=null)
		this.url_pattern=player_options.url_pattern;

	if(player_options.url_format!=null)
		this.url_format=player_options.url_format;

	if(player_options.enable_streaming!=null)
		this.enable_streaming=player_options.enable_streaming;

	if(player_options.player_id!=null)
		this.player_id='_'+player_options.player_id;

	if(player_options.tc_in!=null)
		this.tc_in='_'+player_options.tc_in;

	if(player_options.muted !=null){
		this.muted=player_options.muted;
	}

	if(player_options.provider !=null){
		this.provider=player_options.provider;
	}

	if(player_options.resizeCustom !=null){
		this.resizeCustom=player_options.resizeCustom;
	}

	if ((!this.simpleController && !this.showFlashController ) && this.controlbar != 'none'){
		this.controlbar = 'none';
	}

	if(this.controlbar =="bottom" && typeof(player_options.offset_controlbar_height)=="undefined" && typeof(player_options.skin)=="undefined"){
		this.offset_controlbar_height = 24;
	}else if (this.controlbar == "bottom" && player_options.offset_controlbar_height!=null){
		this.offset_controlbar_height = player_options.offset_controlbar_height;
	}

	if(this.playlist || ( this.showFlashController==true && this.simpleController == true )){
		this.display_buff = false ;
	}

	if(this.display_buff){
	// Init variables d'affichage de la bufferisation :
		this.initRange();
		this.displayBufferedRangesDivs=[];
	}

	if(this.mediaType == "audio"){
		document.getElementById(container).className+=" audio";
	}

	this.durationCurrentItem = null;
	this.AttachPlayer(container);
	// this.AttachMovieController(this.movie_options);

	if(this.movie_options['width'] && this.movie_options['height'] && !this.movie_options['fit_height_container'] && this.controllerpanel && this.mediaType == "video"){
		this.container.style.minHeight = (this.movie_options['height']+this.controllerpanel.offsetHeight)+"px" ;
	}

}

OJW_player.prototype.AttachMovieController =function(){

	if(this.movie_options.code_controller){
		this.controllerpanel = document.createElement('div');					//création div controllerpanel
		this.controllerpanel.id = 'ow_controllerpanel';
		this.controllerpanel.className="flash";
		this.controllerpanel.innerHTML = this.movie_options.code_controller;	//update controllerpanel depuis le code_controller
		if (this.movie_options.force_width_controller == false)
			this.controllerpanel.style.width=this.width+'px' ;
		this.container.appendChild(this.controllerpanel);						//ajout du controllerpanel au container
	}


	if(!this.movie_options.extsBtnsDisabled && (typeof myPanel != "undefined" || this.movie_options.extsBtnsOnStart)){
		this.showExtsButtons();
	}else{
		this.hideExtsButtons();
	}

	if(document.getElementById('ow_id_progress_slider')){
		backgroundTimeline = document.createElement('div');
		backgroundTimeline.id = "backgroundTimeline";

		progresstrack = document.createElement('div');
		progresstrack.id = "progress_track";

		progresshandle = document.createElement('div');
		progresshandle.id = "progress_handle";


		if(this.display_buff){
			bufferedDisplay = document.createElement('div');
			bufferedDisplay.id = "0bufferedDisplay";
			this.displayBufferedRangesDivs.push(bufferedDisplay);
			bufferedDisplay.className = "bufferedDisplayDivs";
		}

		progresstrack.appendChild(progresshandle);

		if(!this.selectionSliderDisabled){
		selection = document.createElement('div');
		selection.id = "selection";

		selecttrack = document.createElement('div');
		selecttrack.id = "select_track";

		handleBegSel = document.createElement('div');
		handleBegSel.id = "handle_BegSel";
		handleBegSel.className = "handles_select";
		handleEndSel = document.createElement('div');
		handleEndSel.id = "handle_EndSel";
		handleEndSel.className = "handles_select";

		selecttrack.appendChild(selection);
		selecttrack.appendChild(handleBegSel);
		selecttrack.appendChild(handleEndSel);
		}

		ow_progress_slider = document.getElementById("ow_id_progress_slider");
		ow_progress_slider.appendChild(backgroundTimeline);
		if(this.movie_options.span_time_passed){
			spanpassed = document.createElement('div');
			spanpassed.id = "ow_time_passed";
			ow_progress_slider.appendChild(spanpassed);
		}
		ow_progress_slider.appendChild(progresstrack);

		//MSTEST
		if(this.display_buff){
			bufferedDisplayContainer = document.createElement('div');
			bufferedDisplayContainer.id = "bufferedDisplayContainer";
			bufferedDisplayContainer.style.marginTop = "-"+(this.movie_options['hauteur_track']/2+this.movie_options['hauteur_bg_track']/2)+"px";
			ow_progress_slider.appendChild(bufferedDisplayContainer);
			bufferedDisplayContainer.appendChild(bufferedDisplay);
		}
		else
		{
			bufferedDisplayContainer = document.createElement('div');
			bufferedDisplayContainer.id = "bufferedDisplayContainer";
			bufferedDisplayContainer.style.marginTop = "-"+(this.movie_options['hauteur_track']/2+this.movie_options['hauteur_bg_track']/2)+"px";
			ow_progress_slider.appendChild(bufferedDisplayContainer);
			bufferedDisplayContainer.style.height=backgroundTimeline.offsetHeight+'px';
		}

		if(!this.selectionSliderDisabled){
			ow_progress_slider.appendChild(selecttrack);
		}

		progresstrack.style.marginTop = "-" + (this.movie_options['hauteur_bg_track'] + (this.movie_options['hauteur_track'] - this.movie_options['hauteur_bg_track'])/2)+"px";

		//this.controllerpanel.style.width = document.getElementById('ply').offsetWidth+'px';
		this.InitTimeline();
	}

	// init slider volume sonore
	if(document.getElementById('ow_id_sound_slider')){
		handlesound = document.createElement("div");
		handlesound.id = "ow_sound_handle";
		document.getElementById("ow_id_sound_slider").appendChild(handlesound);
		this.InitSoundSlider();
	}

	//init du sélecteur de qualité
	if( Object.prototype.toString.call(this.movie_options.urls) === '[object Array]' && this.movie_options.urls.length > 0) {
		var code= '<select onchange="if (myVideo) myVideo.SwitchMovie(myVideo.movie_options.urls[this.value], myVideo.movie_options.typesMime[this.value]);">';
		for (key in this.movie_options.quality_names) {
			if (this.movie_options.quality_names.hasOwnProperty(key) && this.movie_options.urls.hasOwnProperty(key)){
				if (typeof str_lang !="undefined" && typeof str_lang[this.movie_options.quality_names[key]]=="string"){
					quality_label = str_lang[this.movie_options.quality_names[key]];
				} else {
					quality_label = this.movie_options.quality_names[key];
				}
				code += "<option value='"+key+"'>"+quality_label+ "</option>";
			}
		}
		code += "</select>";
		$j('#ow_quality_switch').html(code);
	}
	else $j('#ow_quality_switch').hide();

	// init bouton fullscreen
	if(document.getElementById('ow_bouton_fullscreen')){
		if(this.mediaType == "audio"){
			$("ow_bouton_fullscreen").addClassName("audioHide");
		}

		if(window.addEventListener){
			document.getElementById('ow_bouton_fullscreen').addEventListener('click',this.ToggleFullScreen.bind(this),false);
		}else{
			document.getElementById('ow_bouton_fullscreen').attachEvent('onclick',this.ToggleFullScreen.bind(this));
		}
	}

	//MS ==============Fonctionnalités non implémentées pour le player flash
	// Si jog container défini dans le code_controller => on le cache pour l'instant
	if(document.getElementById('ow_id_jog_slider')){
		document.getElementById('ow_id_jog_slider').style.display = "none";
		document.getElementById('joginfo').style.display = "none";
	}
	// Si step_forward , step_rewind défini dans le code_controller => on les cache
	if(document.getElementById('ow_bouton_step_forward')){
		document.getElementById('ow_bouton_step_forward').style.display = "none";
	}
	if(document.getElementById('ow_bouton_step_rewind')){
		document.getElementById('ow_bouton_step_rewind').style.display = "none";
	}
	if(document.getElementById('ow_bouton_loop')){
		document.getElementById('ow_bouton_loop').style.display = "none";
	}

	//===================

	this.StepVideoMethod =  this.LimitSeekStepVideo;

	//Fonction qui autorise le seeking pour Chrome
	this.AllowSeeking = function() {
		this.canSeek = true ;
	}
	//Fonction qui interdit le seeking pour Chrome
	this.BlockSeeking = function(arg1,arg2) {
		if(!this.playlist){
			e = arg1;
		}else {
			player_id = arg1;
			e = arg2;
		}

		// console.log('call BlockSeeking on Seek , id arg : '+ player_id+" current "+this.currentPlyId+" offset : "+e.offset+" "+e.position);

		if(typeof(e.offset)!='undefined' && (!this.playlist || (this.playlist && player_id==this.currentPlyId))){
			this.canSeek = false ;
			this.flashRetardSeek = parseFloat(e.offset);
			// console.log('	new flashRetardSeek : '+this.flashRetardSeek);


		}
	}


	// Association des handlers aux boutons existants en fonction de l'objet basic_function_button
	for (o in this.movie_options.basic_function_button){
		if(document.getElementById(o)){
			if(o=="ow_bouton_fullscreen"){
				document.getElementById(o).onclick = this.movie_options.basic_function_button[o];
			}else{
			document.getElementById(o).onmousedown = this.movie_options.basic_function_button[o];
			}
		}
	}

	if(!this.selectionSliderDisabled){
		this.BegSelect = this.timelineSel.values[0];
		this.EndSelect = this.timelineSel.values[1];
	}


	if(this.movie_options.selectionHidden && !this.selectionSliderDisabled){
        this.HideSelectionSlider();
	}


}

OJW_player.prototype.SwitchMovie = function(url,typeMime){
	var time = this.timeline.value;
	player = document.getElementById(this.movie_options.name);
	player.sendEvent('LOAD', url);
	//this.timeline.setValue(time);
}

OJW_player.prototype.gotoTC=function (tc)
{
	var element=this.tc_in.split(':');
	var tc_in_sec=parseInt(element[0])*3600+parseInt(element[1])*60+parseInt(element[2]);

	//document.getElementById('ply'+this.player_id).sendEvent('SEEK',tc-tc_in_sec);
	jwplayer().seek(tc-tc_in_sec);
}

OJW_player.prototype.setVideoUrl=function(id_doc,callback)
{
	var address='';

	address='xmlhttp=1&urlaction=prepareVisu&method=QT&action=visu&id='+ id_doc+'&id_lang=fr';
	if (this.url_pattern!='')
		address=address+'&pattern='+this.url_pattern;
	if (this.url_format!='')
		address=address+'&format='+this.url_format;

	return !sendData('GET','blank.php',address,callback);
}

OJW_player.prototype.AttachPlayer=function(container)
{
	// Init structure container
	this.container = document.getElementById(container);
	// créa élément parent du player "ow_main_container"
	this.container.innerHTML='';
	var main_container=document.createElement('div');
	main_container.id='ow_main_container';
	this.container.appendChild(main_container);
	this.container = document.getElementById('ow_main_container');
	$(this.container.id).addClassName("containerNS");

	this.container.innerHTML='';





	// reset players existants (empeche des bugs jwplayer de reset trop lent)
	// pour un reset plus "propre", utiliser la méthode "destroy" avant de relancer le chargement.
	var players = jwplayer.getPlayers();
	jQuery.each(players, function(i, v){
		jwplayer.api.destroyPlayer(v.container.name);
	});

	// définition des options du player flash
	this.jwPlayerOpts = {
	flashplayer : this.player,
	file : this.url,
	height : this.height+((typeof this.offset_controlbar_height!='undefined' && this.offset_controlbar_height!=0)? this.offset_controlbar_height:""),
	width : this.width,
	skin : this.skin,
	controlbar : this.controlbar,
	mute : this.muted,
	volume : 100,
	icons : false,
	// MSTEST variable de taille de buffer requise avant de commencer la lecture,
	//permet de mettre en place une sécurité au niveau de la bufferisation, permettant d'assurer une lecture continue.
	// à voir si fixe, ou modifiable via le player_options ==> dépendra beaucoup des perfs en prod
	bufferlength : 0.1
	}

	// MS workaround autobuffer => on lance le player en autoplay toujours à true, si l'autoplay=false défini dans le player_opts ==> on pausera après le chargement des metadata
	//  => voir jwplayer().onMeta()
	// MS 23/08/13 => il va falloir permettre de réellement mettre autoplay à false pour empecher que le poster disparaisse (contexte du player exportable)

	if(this.autoplay || this.preload){
		this.jwPlayerOpts['autoplay']=true;
	}
	if( (!this.movie_options['noPoster'] || this.movie_options['show_poster_on_ended'] ) && this.image){
		if(this.mediaType=="audio"){
			this.jwPlayerOpts['image']=this.image.replace('&ol=player','');
		}else{
			this.jwPlayerOpts['image']=this.image;
		}
	}
	// MS 16/10/12
	//	provider = type de media detecté par le typeMime transmis, video sinon
	if (typeof(this.mediaType)!='undefined'){
		this.jwPlayerOpts['provider']=this.mediaType;
	}

	// MS ajout provider "http" et http.startparam : starttime pour activer le pseudo-streaming,
	// necessite un module de streaming server side
	if (this.enable_streaming==true && this.mediaType == "video")
	{
		this.jwPlayerOpts['provider']="http";
		this.jwPlayerOpts["http.startparam"] = 'start';
	}


	if(this.playlist){
		// traitement du xml en entrée
		if(this.movie_options["xml"]!='') {
			var parser;
			var xmlDoc;
			if (typeof(DOMParser)!='undefined')
			{
				parser=new DOMParser();
				xmlDoc=parser.parseFromString(this.movie_options["xml"],"text/xml");
			}
			else
			{
				xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async=false;
				xmlDoc.loadXML(this.movie_options["xml"]);
			}
			this.traiterFluxXml(xmlDoc);
		} else if (this.movie_options["url"]!='') {
			$ajax=new Ajax.Request(this.movie_options["url"],
								   {
								   asynchronous:false,
								   onSuccess : this.traiterFluxXml.bind(this)
								   });
		}

		this.AttachPlaylist();

	}else{
		// init élément host du plugin
		var ply = document.createElement('div');
		this.currentPlyId = 'ply';
		ply.id=this.currentPlyId;
		this.container.appendChild(ply);
		
		
		// ajout selecteur qualité pour player flash (simpleController)
		// V1
		/*if(this.simpleController &&  Object.prototype.toString.call(this.movie_options.urls) === '[object Array]' && this.movie_options.urls.length > 0) {
			// var code= '<select onchange="if (myVideo) myVideo.SwitchMovie(myVideo.movie_options.urls[this.value], myVideo.movie_options.typesMime[this.value]);">';
			delete this.jwPlayerOpts['file'];
			// var sources = [];
			this.jwPlayerOpts['levels'] =[];
			for (key in this.movie_options.quality_names) {
				if (this.movie_options.quality_names.hasOwnProperty(key) && this.movie_options.urls.hasOwnProperty(key)){
					if (typeof str_lang !="undefined" && typeof str_lang[this.movie_options.quality_names[key]]=="string"){
						quality_label = str_lang[this.movie_options.quality_names[key]];
					} else {
						quality_label = this.movie_options.quality_names[key];
					}
					this.jwPlayerOpts['levels'].push({
						file : this.movie_options.urls[key]
					});
					// code += "<option value='"+key+"'>"+quality_label+ "</option>";
				}
			}
			// this.jwPlayerOpts['sources'] = sources ; 
			// code += "</select>";
			// $j('#ow_quality_switch').html(code);
		}*/
		// V2
		if(this.simpleController &&  Object.prototype.toString.call(this.movie_options.urls) === '[object Array]' && this.movie_options.urls.length > 0) {
			// var code= '<select onchange="if (myVideo) myVideo.SwitchMovie(myVideo.movie_options.urls[this.value], myVideo.movie_options.typesMime[this.value]);">';
			
			file_hd =  this.jwPlayerOpts['file']; 
			delete this.jwPlayerOpts['file'];
			// var sources = [];
			// this.jwPlayerOpts['levels'] =[];
			for (key in this.movie_options.quality_names) {
				if (this.movie_options.quality_names.hasOwnProperty(key) && this.movie_options.urls.hasOwnProperty(key)){
					if (typeof str_lang !="undefined" && typeof str_lang[this.movie_options.quality_names[key]]=="string"){
						quality_label = str_lang[this.movie_options.quality_names[key]];
					} else {
						quality_label = this.movie_options.quality_names[key];
					}
					file_sd = this.movie_options.urls[key];
					/*this.jwPlayerOpts['levels'].push({
						file : this.movie_options.urls[key]
					});*/
					// code += "<option value='"+key+"'>"+quality_label+ "</option>";
				}
			}
			this.jwPlayerOpts['plugins'] = {'hd' : {file : file_hd}};
			this.jwPlayerOpts['file'] = file_sd;
			// this.jwPlayerOpts['sources'] = sources ; 
			// code += "</select>";
			// $j('#ow_quality_switch').html(code);
		}
		
		
		jwplayer('ply').setup(this.jwPlayerOpts);

		// initialisation de certains controles apres l'initialisation du player
		function initJsInteraction(){
			// console.log("InitJSInteraction player unique");
			// control skin des boutons de lecture / pause
			jwplayer().onPause(this.changeButtonPlay.bind(this));
			jwplayer().onPlay(this.changeButtonPlay.bind(this));
			jwplayer().onIdle(this.changeButtonPlay.bind(this));
			jwplayer().onMeta(function(event){
				if(!this.loadedMetadata && !this.autoplay && this.preload){
					jwplayer().pause(true);
				}
				if(!this.mediaSeekpoints){
					this.initMediaSeekpoints(event.metadata);
				}
				if(this.durationCurrentItem == null){
					this.durationCurrentItem = parseInt(event.metadata.duration) ;
					this.setTimeline();
					if(document.getElementById('timecodeDuration')){
						document.getElementById('timecodeDuration').innerHTML = this.DisplayTimeCode(this.durationCurrentItem);
					}
				}

				this.loadedMetadata = true ;
			}.bind(this));
			if(!this.showFlashController){
				jwplayer().onBufferChange(function(event){
					this.updateCurrentRange();
					this.displayBufferedRange();
				}.bind(this));
			}

			// initialisation du callback onMovieFinished sur le jwplayer.onComplete
			if(this.movie_options.onMovieFinished!=null && typeof(this.movie_options.onMovieFinished)=='function'){
				jwplayer().onComplete(function(){
					this.movie_options.onMovieFinished();
				}.bind(this));
			}

			// control skin du bouton Mute dans certains cas ( video muted, retour au son via le slider ...)
			jwplayer().onMute(this.defaultControlVolumeChange.bind(this));

			// update de l'affichage en fonction de la progression de la lecture
			jwplayer().onTime(this.UItest.bind(this));

			// bufferisation dev infos
			// jwplayer().onTime(this.bufferinfos.bind(this));
			jwplayer().onSeek(this.BlockSeeking.bind(this));


			if(window.addEventListener){
				document.getElementById('ply_wrapper').addEventListener("click",function(){ if (this.fastSteppingOn != null ){this.canSeek = true ; }}.bind(this),false);
			}else{
				document.getElementById('ply_wrapper').attachEvent("onclick",function(){ if (this.fastSteppingOn != null ){this.canSeek = true ; }}.bind(this));
			}
			if(this.movie_options.fit_height_container){
				this.adjustSize();
			}
			if(this.movie_options.mosaic_path != null  && this.movie_options.mosaic_path != ""  ){
				if(typeof this.movie_options.mosaic_pics_index == "string"){
					this.movie_options.mosaic_pics_index = this.movie_options.mosaic_pics_index.split(',');
				}

				if(this.movie_options.previewOverTimeline && document.getElementById("ow_prevOverContainer").className=="" && this.mediaType!="audio"){
					if((jwplayer().getWidth()/jwplayer().getWidth())>1.5 ){
						document.getElementById("ow_prevOverContainer").className="form_16_9";
					}else{
						document.getElementById("ow_prevOverContainer").className="form_4_3";
					}
				}
				this.setMosaicPath();
			}
		}
		jwplayer().onReady(initJsInteraction.bind(this));

		// MSTEST
		new PeriodicalExecuter(this.bufferinfos.bind(this),0.5);
	}

	if(this.resizeCustom){
		this.resizeListener = this.adjustSize.bind(this)
		if(window.addEventListener){
			window.addEventListener('resize',this.resizeListener,false);
		}else{
			window.attachEvent('onresize',this.resizeListener);
		}
	}

	if(!this.simpleController){
		this.AttachMovieController(this.movie_options);
	}

	if(this.resizeCustom){
		// on appelle à l'initialisation le resizeListener, ce qui permet de resize correctement le player flash dès l'initialisation.
		// Si le resize n'est pas correct, alors le problème se reposera a chaque resize / retour du fullscreen, il y a donc probablement une erreur de configuration des player_options
		this.resizeListener();
	}
}

//  ===================== Fonctions lecture

// Fonction qui met en pause la lecture
OJW_player.prototype.StopTheVideo = function() {
	// console.log('call StopTheVideo , currentId : '+this.currentPlyId);
	if(this.fastSteppingOn!= null){		// si on est en avance/recul rapide
		this.StopFastStepping();		// on stoppe la repetition des sauts
	}

	if(this.playlist){
		jwplayer(this.currentPlyId).pause(true);
	}else{
		jwplayer().pause(true);
	}
}

OJW_player.prototype.PlayTheVideo = function(){
	// console.log('call PlayTheVideo , currentId : '+this.currentPlyId);
	if(this.playlist){
		jwplayer(this.currentPlyId).play(true);
	}else{
		jwplayer().play(true);
	}
}

OJW_player.prototype.ToggleVideo = function(){
	if (((this.playlist && (jwplayer(this.currentPlyId).getState() == 'PAUSED'|| jwplayer(this.currentPlyId).getState() =='IDLE' || jwplayer(this.currentPlyId).getState() == "BUFFERING" ))
		|| (!this.playlist && (jwplayer().getState() == 'PAUSED'|| jwplayer().getState() =='IDLE' || jwplayer().getState() == "BUFFERING" )))
		&& this.fastSteppingOn == null){
		this.PlayTheVideo();
	} else {
		this.StopTheVideo();

	}
}
//  ===================== FIN Fonctions lecture


//  ===================== Fonctions stepping

OJW_player.prototype.InitStepping = function (styleOfStepping, stepvalue){
	// console.log('call InitStepping');
	if(!this.loadedMetadata){
		return 0;
	}

	// def de variables (this ne fonctionne pas dans les PeriodicalExec)
	var self = this;
	var StepVideo = this.StepVideoMethod;					// Fonction d'étape correspondant au browser
	var freq = this.movie_options['stepping_frequency'];	// frequence de modification, passage seconde >> millisecondes
	// var JogSteppingFilter = this.JogSteppingFilter;

    tempFastSteppingDirection = this.fastSteppingDirection;

	// Si la vidéo est en lecture ou si un fastStepping est en cours
	if (((!this.playlist && jwplayer().getState() == "PLAYING" ) || (this.playlist &&  jwplayer(this.currentPlyId).getState() == "PLAYING")) || this.fastSteppingOn != null){
		this.StopTheVideo();	// on arrete la video et le stepping
	}

	this.canSeek = true;


	// MSTEST => Pour l'instant desactivé pour flash
	if ( false && styleOfStepping=="frame"){
		// ************FRAME BY FRAME******************
		var sync = this.CalcSyncIfNeeded();
		StepVideo(stepvalue,self,sync);
		// initialisation éxecution périodique de la fonction step limitée
		this.slowSteppingOn = new PeriodicalExecuter(function(){StepVideo(stepvalue,self,sync);},0.10);
        var tempSlowStepp = this.slowSteppingOn;
		// init du listener qui stoppe l'execution periodique quand on lache la souris
		window.onmouseup = function(){
            if(tempSlowStepp != null){
                tempSlowStepp.stop();
                tempSlowStepp = null;
			}
            $("ow_bouton_step_forward").removeClassName("ClickBouton");
            $("ow_bouton_step_rewind").removeClassName("ClickBouton");
		}


	} else if (styleOfStepping == "fast"){
		// ************FAST FORWARD/REWIND**************

        if(stepvalue > 0){
            this.fastSteppingDirection = 1;
        } else {
            this.fastSteppingDirection = -1;
        }

		if(this.playlist && this.flashRetardSeek == null){
			this.flashRetardSeek = jwplayer(this.currentPlyId).getPosition();
		}else if(!this.playlist && this.flashRetardSeek == null){
			this.flashRetardSeek = jwplayer().getPosition();
		}

		// console.log("InitStepping, flashRetardSeek : "+this.flashRetardSeek);

		this.retardSeek = 0;

        if(this.fastSteppingDirection != tempFastSteppingDirection){

			if(this.playlist){
				if(!jwplayer(this.currentPlyId).getMute()){
					this.soundVolume = jwplayer(this.currentPlyId).getVolume();
					for (i=0; i <this.track_media.length ; i++){
						jwplayer(this.track_media[i].id).setMute();
					}
				}
			}else{
				if(!jwplayer().getMute()){
					this.soundVolume = jwplayer().getVolume();
					jwplayer().setMute(true);
				}
			}

            if(this.fastSteppingDirection >0){
                $("ow_bouton_fast_forward").addClassName("ClickBouton");
            }else{
                $("ow_bouton_fast_rewind").addClassName("ClickBouton");
            }

            // if(this.outOfSelectionTest(this.GetCurrentTime())){
                // this.GoToBegSelection(); // on se place au debut de la selection
            // }
			// console.log('init periodical executer,'+this.currentPlyId+' flashRetardSeek : '+this.flashRetardSeek);

            this.fastSteppingOn = new PeriodicalExecuter(function(exec){StepVideo(stepvalue,self);},freq);
            this.changeButtonPlay();
        } else {
            this.fastSteppingDirection = 0;
            this.StopTheVideo();
		}


	} else if (styleOfStepping == "jog"){
		//***************JOG STEPPING************************
		this.jogSteppingOn = new PeriodicalExecuter(function(){JogSteppingFilter(self,StepVideo);},freq);

	}
}

// Fonction permettant d'attributer la bonne fonction de vitesse variable aux boutons suivant le cas d'un lecteur video ou audio
OJW_player.prototype.FastPlaying = function(direction) {

        if(direction == 1){
            return this.InitStepping.bind(this,"fast",1);
        } else {
            return this.InitStepping.bind(this,"fast",-1);
        }
   // }  //else {
        // if(direction == 1 ){
            // return this.varSpeedPlaying.bind(this,10);
        // } else {
            // return this.varSpeedPlaying.bind(this,-10);
        // }
    // }

}


OJW_player.prototype.LimitSeekStepVideo = function (stepvalue, self,sync){
	// Si on peut seek
	if (self.canSeek){

		seekTo = stepvalue + self.retardSeek ;
		seekTo += self.flashRetardSeek;

		// console.log("stepvalue : "+stepvalue+" retardSeek : "+self.retardSeek+" flashRetardSeek"+self.flashRetardSeek+"\nseekTo : "+seekTo +" currentPlyId : "+self.currentPlyId+" BegTrack : "+self.BegTrack+" seekTo calculé playlist : "+(seekTo+self.BegTrack-self.track_start_offset[self.current_track]));


		if(self.playlist){
			if((self.selectionExists && (seekTo+self.BegTrack-self.track_start_offset[self.current_track]) <= self.BegSelect) || (!self.selectionExists && (seekTo+self.BegTrack-self.track_start_offset[self.current_track])<= 0)){
				self.GoToBegAvailable();
				// console.log('	gotoBegavailable');
				// self.StopTheVideo();
			}else if ((self.selectionExists && (seekTo+self.BegTrack-self.track_start_offset[self.current_track]) >= self.EndSelect) ||  (!self.selectionExists && (seekTo+self.BegTrack-self.track_start_offset[self.current_track]) >= self.timeline.range.end)){
				self.GoToEndAvailable();
				// console.log('	gotoEndavailable');
				// self.StopTheVideo();
			}else{
				// console.log("valeur actuelle dans al vidéo : "+(seekTo+self.BegTrack-self.track_start_offset[self.current_track])+" begtrack "+self.BegTrack+" endtrack :  "+self.EndTrack)
				if(seekTo+self.BegTrack-self.track_start_offset[self.current_track] < self.BegTrack){
					self.retardSeek = 0;
					self.canSeek = true ;
					jwplayer(self.track_media[self.current_track-1].id).seek(self.track_start_offset[self.current_track-1]+self.track_duration[self.current_track-1]-1);
					self.flashRetardSeek = self.track_start_offset[self.current_track-1]+self.track_duration[self.current_track-1]-1;
					self.ChangeTrack(self.current_track-1);
					// console.log('changetrack from limitseekstep, position prec : '+self.flashRetardSeek+", position normalement cherchée : "+(self.track_start[self.current_track-1]-self.track_start_offset[self.current_track-1]));
				}else if (seekTo+self.BegTrack-self.track_start_offset[self.current_track] > self.EndTrack){
					self.canSeek = true ;
					self.retardSeek = 0;
					jwplayer(self.track_media[self.current_track+1].id).seek(self.track_start_offset[self.current_track+1]);
					self.flashRetardSeek = self.track_start_offset[self.current_track+1];
					self.ChangeTrack(self.current_track+1);
					// console.log('cshangetrack from limitseekstep, position suivante : '+self.flashRetardSeek+", position normalement cherchée : "+(self.track_start[self.current_track-1]-self.track_start_offset[self.current_track-1]));
				}else{
					jwplayer(self.currentPlyId).seek(seekTo);
				}
			}
		}else{
			if((self.selectionExists && seekTo <= self.BegSelect) || (!self.selectionExists && seekTo <= self.timeline.range.start)){
				// alert(self.selectionExists +" "+seekTo+" "+self.BegSelect+" "+)
				self.GoToBegAvailable();
				// self.StopTheVideo();
			}else if ((self.selectionExists && seekTo >= self.EndSelect) ||  (!self.selectionExists && seekTo >= self.timeline.range.end)){
				self.GoToEndAvailable();
				// self.StopTheVideo();
			}else{
				jwplayer().seek(seekTo);
			}
			// self.timeline.setValue(seekTo);
			// MS - FLASH : vu la précision du seeking, la synchronisation n'est surement pas nécessaire
			// if(sync != null){
				// self.timelineSel.setValue(self.media.currentTime+self.retardSeek, sync);
			// }
			//self.flashRetardSeek = 0;
			self.retardSeek = 0;				// RaZ du retard
		}
		// Si le seek est bloqué
	} else {
		// stockage du retard
		self.retardSeek += stepvalue;
	}

}

// Fonction permettant de stopper le stepping
OJW_player.prototype.StopFastStepping = function (){
	// si le stepping est en cours
	if (this.fastSteppingOn != null){
        if(this.mediaType == "video"){
            this.fastSteppingOn.stop();	// on stop le stepping
        }else if(this.mediaType=="audio"){
			this.media.playbackRate=1;
		}
        this.fastSteppingOn = null;	// on réinitialise le flag de stepping
        this.fastSteppingDirection = 0;
		// this.flashRetardSeek = jwplayer().getPosition();
		// this.retardSeek = 0;

		if( !$('ow_bouton_mute_sound').hasClassName('ClickBouton')){
			if(this.playlist){
				for(i=0; i < this.track_media.length; i++){
					jwplayer(this.track_media[i].id).setVolume(this.soundVolume);
				}
			}else{
				jwplayer().setVolume(this.soundVolume);
			}
		}

		this.changeButtonPlay();

        if($("ow_bouton_fast_forward").hasClassName("ClickBouton")){
            $("ow_bouton_fast_forward").removeClassName("ClickBouton");
        } else {
            $("ow_bouton_fast_rewind").removeClassName("ClickBouton");
        }

		// alert(this.dontCallOnChange+"  ");
		//****************************RANGES "MAISON"
		if(this.display_buff){		// Si on utilise la méthode des ranges
			this.changeRangeCheck(jwplayer().getPosition());		// test de range (current ? création?)
            this.updateCurrentRange();
        }
	}
}

//  ===================== FIN Fonctions stepping


// ====================== boutons aller début / aller fin
// Fonction aller au début
OJW_player.prototype.GoToBegAvailable = function (){
    this.StopTheVideo();
	if (this.selectionExists){			// Si on a une selection
		this.GoToBegSelection();			// aller au début de la selection
	}else{								// sinon
		// aller au début de la vidéo
		this.canSeek = true ;
		this.pauseAfterSeek = true;
		if(this.playlist){
			this.ChangeTrack(0);
			this.flashRetardSeek = this.timeline.range.start+this.track_start_offset[this.current_track];
			jwplayer(this.currentPlyId).seek(this.flashRetardSeek);
			this.UItest(this.currentPlyId,{position : this.flashRetardSeek});
		}else{
			jwplayer().seek(this.timeline.range.start);
			this.flashRetardSeek = this.timeline.range.start;
			this.UItest({position : this.timeline.range.start});
		}
	}
}

// Fonction aller en fin
OJW_player.prototype.GoToEndAvailable = function (){
    this.StopTheVideo();
	if (this.selectionExists){					// si la selection existe
		this.GoToEndSelection();					// aller en fin de selection
	}else{										// sinon
		this.canSeek = true ;
		this.pauseAfterSeek = true;
		// console.log("GoToEndAvailable, seek : "+(this.track_start_offset[this.nb_tracks-1]+this.track_duration[this.nb_tracks-1]));

		if(this.playlist){
			this.ChangeTrack(this.nb_tracks-1);
			jwplayer(this.currentPlyId).seek(this.track_start_offset[this.nb_tracks-1]+this.track_duration[this.nb_tracks-1]);
			this.flashRetardSeek = this.track_start_offset[this.nb_tracks-1]+this.track_duration[this.nb_tracks-1];
			this.UItest(this.currentPlyId,{position : (this.track_start_offset[this.nb_tracks-1]+this.track_duration[this.nb_tracks-1])});
		}else{
			jwplayer().seek(this.timeline.range.end);
			this.flashRetardSeek = this.timeline.range.end;
			this.UItest({position : (this.timeline.range.end)});
		}
	}
}

//  ===================== Fonctions selection
// Fonction permettant de cacher la barre d'extrait (pour l'onglet description Opsis)
OJW_player.prototype.HideSelectionSlider = function(){
    this.movie_options.selectionHidden = true;
    $("selection").addClassName("hideselection");
    $("select_track").addClassName("hideselection");
    document.getElementById("ow_id_progress_slider").style.height = document.getElementById('progress_track').offsetHeight;//"4px";
   // this.media.webkitEnterFullScreen();
    }

// Fonction permettant de réafficher la barre d'extrait (pour l'onglet extrait d'Opsis)
OJW_player.prototype.ShowSelectionSlider = function(){
    this.movie_options.selectionHidden = false;
    $("selection").removeClassName("hideselection");
    $("select_track").removeClassName("hideselection");
    document.getElementById("ow_id_progress_slider").style.height = document.getElementById('progress_track').offsetHeight;//"8px";
    }

// Fonction permettant de positionner la vidéo au début de la selection
OJW_player.prototype.GoToBegSelection = function (){
	this.StopTheVideo();
    this.dontCallOnChange = false;
	// this.timeline.setValue(this.BegSelect);

	this.canSeek = true ;
	this.pauseAfterSeek = true;
	jwplayer().seek(this.BegSelect);
	//this.GetCurrentTime() = Math.round(this.BegSelect*100)/100;

}

// Fonction permettant de positionner la video à la fin de la selection
OJW_player.prototype.GoToEndSelection = function(){
	this.StopTheVideo();
    this.dontCallOnChange = false;
	//this.timeline.setValue(this.EndSelect);
	//this.GetCurrentTime() = Math.round(this.EndSelect*100)/100;

	this.canSeek = true ;
	this.pauseAfterSeek = true;
	jwplayer().seek(this.EndSelect);
}


// Fonction qui retourne le début de la selection (A voir pour les questions de formats timecode / secondes)
OJW_player.prototype.GetCurrentSelectionBeginning = function(){

	var offset = 0;
	if(this.movie_options.timecode_IN){
		offset = this.timecodeToSecs(this.movie_options.timecode_IN);
	}


	return this.secsToTimeCode(this.BegSelect+offset);
}

// Fonction qui retourne la fin de la selection (A voir pour les questions de formats timecode / secondes)
OJW_player.prototype.GetCurrentSelectionEnd = function(){

	var offset = 0;
	if(this.movie_options.timecode_IN){
		offset = this.timecodeToSecs(this.movie_options.timecode_IN);
	}


	return this.secsToTimeCode(this.EndSelect+offset);
}

// Fonction permettant de parametrer la selection
OJW_player.prototype.SetSelection = function(timecode_debut, timecode_fin){
    // Variables bornes de la selection en secondes
    tc_in = this.timecodeToSecs(timecode_debut)-this.timecodeToSecs(this.movie_options.timecode_lag);
    tc_out = this.timecodeToSecs(timecode_fin)-this.timecodeToSecs(this.movie_options.timecode_lag);
    switchAutreExtrait = false;

    // test de validité des valeurs en entrée (tc_debut < tc_fin) et correction
    if(this.timecodeToSecs(timecode_debut)>this.timecodeToSecs(timecode_fin)){
        tc_in = this.timecodeToSecs(timecode_fin)-this.timecodeToSecs(this.movie_options.timecode_lag);;
        tc_out = this.timecodeToSecs(timecode_debut)-this.timecodeToSecs(this.movie_options.timecode_lag);;
    }

    this.dontCallOnChange = true;
	if(tc_out<this.BegSelect){
		this.timelineSel.setValue(tc_in, 0);
		this.timelineSel.setValue(tc_out,1);
	}else{
		this.timelineSel.setValue(tc_out,1);
		this.timelineSel.setValue(tc_in, 0);
	}
    this.dontCallOnChange = false;

    if(this.selectionExists && (jwplayer().getPosition()<this.BegSelect-0.04) || (jwplayer().getPosition()>this.EndSelect+0.04)){
		this.timeline.setValue(this.BegSelect);
	}

}

// Fonction permettant de supprimer le parametrage de selection (== mettre le debut et la fin a 0, inactif)
OJW_player.prototype.UnSetSelection = function(){
	this.dontCallOnChange = true;
    this.timelineSel.setValue(this.timelineSel.range.start,0);
    this.timelineSel.setValue(this.timelineSel.range.start,1);
    this.dontCallOnChange = false;
	this.BegSelect = this.timelineSel.values[0];
	this.EndSelect = this.timelineSel.values[1];

}

// Fonction permettant de tester si la selection existe et si on est en dehors (true si oui)
OJW_player.prototype.outOfSelectionTest = function(valToCompare) {
	if(this.selectionExists && (Math.round(valToCompare*100)/100<Math.round(this.BegSelect*100)/100-0.02 || Math.round(valToCompare*100)/100>Math.round(this.EndSelect*100)/100)){
		return true;
	} else{
		return false;
	}
}

OJW_player.prototype.inTheSelectionTest = function(valToCompare) {
	if(this.selectionExists && (Math.round(valToCompare*100)/100>=Math.round(this.BegSelect*100)/100 && Math.round(valToCompare*100)/100<=Math.round(this.EndSelect*100)/100)){
		return true;
	} else{
		return false;
	}
}

//===================== FIN Fonctions selection



//==========================================TIMELINE

// Fonction init timeline
OJW_player.prototype.InitTimeline = function(){
	waspaused = false ;
	// TIMELINE PROGRESSION
	// Fonction onSlide de la timeline progression
	updateTimeline = function(value){
		/*if(this.outOfSelectionTest(value) && jwplayer().getState()!='PAUSED'){
			this.StopTheVideo();
		}*/

		// positionnement a changer
		//this.media.currentTime = value;		// positionne la vidéo à la valeur du curseur
		if(this.playlist) {
			//window.status="updateTimeline track="+this.current_track+" : "+this.BegTrack+" < "+value+" < "+this.EndTrack;
			if(value<this.BegTrack) {
				tmp_track = this.current_track;
				while(value<this.track_start[tmp_track]){
					tmp_track = tmp_track -1;
				}
				jwplayer(this.track_media[tmp_track].id).seek((value+this.track_start_offset[tmp_track])-this.track_start[tmp_track]);
				this.ChangeTrack(tmp_track);
			} else if(value>this.EndTrack) {
				tmp_track = this.current_track;
				while(value>this.track_end[tmp_track]){
					tmp_track = tmp_track +1;
				}
				jwplayer(this.track_media[tmp_track].id).seek((value+this.track_start_offset[tmp_track])-this.track_start[tmp_track]);
				this.ChangeTrack(tmp_track);
			}else if(this.currentPlyId){
				jwplayer(this.currentPlyId).seek((value+this.track_start_offset[this.current_track])-this.BegTrack);
			}

		}
		else jwplayer().seek(value);
	}
	// Fonction onChange de la timeline progression
	changeTimeline = function(value){

		// console.log( "call changeTimeline "+value+", dontCallOnChange : "+this.dontCallOnChange+" currentTrackEnded "+this.currentTrackEnded+" value : "+value+ " this.EndTrack : "+this.EndTrack );
		/*if(this.outOfSelectionTest(value)){
			if(this.fastSteppingOn!= null || jwplayer().getState()!='PAUSED' ){
				this.StopTheVideo();
				if(!this.ClickOnTimeline){
					value<this.BegSelect? this.GoToBegSelection() : this.GoToEndSelection();
					this.dontCallOnChange = true;
				}
			}

			if (this.loopVideo==true)
			{
				//this.StopTheVideo();
				this.GoToBegAvailable();
				this.dontCallOnChange = true;
				this.PlayTheVideo();
			}
		}*/

		//MSTEST
		if(this.display_buff){
			if(!this.SelSlide){
				this.changeRangeCheck(value);
			}
			this.displayBufferedRange();
		}
		if(this.playlist && ((this.dontCallOnChange && value>this.EndTrack)|| this.currentTrackEnded) ){
			this.dontCallOnChange = true ;
			// console.log("changetimeline call playnext");
			this.PlayNext();
			this.currentTrackEnded = false ;
		}
		if(!this.dontCallOnChange && (this.canSeek || this.ClickOnTimeline)){
			if(this.playlist) {
				//window.status="updateTimeline track="+this.current_track+" : "+this.BegTrack+" < "+value+" < "+this.EndTrack;
				if(value<this.BegTrack) {
					tmp_track = this.current_track;
					while(value<this.track_start[tmp_track]){
						tmp_track = tmp_track -1;
					}
					jwplayer(this.track_media[tmp_track].id).seek((value+this.track_start_offset[tmp_track])-this.track_start[tmp_track]);
					this.flashRetardSeek = (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
					// console.log( "<br /> call changetrack("+tmp_track+") ( - ) : value "+value+" depuis changetimeline");
					this.ChangeTrack(tmp_track);
				} else if(value>this.EndTrack) {
					tmp_track = this.current_track;
					while(value>this.track_end[tmp_track]){
						tmp_track = tmp_track +1;
					}
					jwplayer(this.track_media[tmp_track].id).seek((value+this.track_start_offset[tmp_track])-this.track_start[tmp_track]);
					this.flashRetardSeek = (value+this.track_start_offset[tmp_track])-this.track_start[tmp_track];
					 // console.log( "<br /> call changetrack("+tmp_track+") ( + ): value "+value+" depuis changetimeline");
					this.ChangeTrack(tmp_track);
				}else if(this.currentPlyId){
					jwplayer(this.currentPlyId).seek((value+this.track_start_offset[this.current_track])-this.BegTrack);
				}

			}
			else jwplayer().seek(value);
		}
		this.dontCallOnChange = false;
		this.ClickOnTimeline = false;
	}
	timelineIsClicked = function(){
        this.ClickOnTimeline = true ;
		if(this.playlist){
			state = jwplayer(this.currentPlyId).getState();
		}else{
			state = jwplayer().getState();
		}
		// console.log('timeline Clicked : state of player  : '+state);
		if( state == "PAUSED" || state == "IDLE" ){
			this.pauseAfterSeek = true ;
		}else if (state == "PLAYING" || state == "BUFFERING"){
			this.pauseAfterSeek = false  ;
		}
	}

    if(window.addEventListener){
		document.getElementById("progress_track").addEventListener("mousedown", timelineIsClicked.bind(this),false);
    }else{
		document.getElementById("progress_track").attachEvent("onmousedown", timelineIsClicked.bind(this));
	}

		// implémentation rollover storyboard (necessite une mosaic générée & les paramètres movie_options.mosaic_* renseignés )
    if(this.movie_options.previewOverTimeline && this.mediaType!="audio"){
		if(!document.getElementById("ow_prevOverContainer")){
			prevOverContainer = document.createElement("div");
			prevOverContainer.id = "ow_prevOverContainer";
			prevOverContainer.style.display="none";

			img_prevOverContainer = document.createElement("div");
			img_prevOverContainer.id = "ow_img_prevOverContainer";
			img_prevOverContainer.style.width="100px";

			label_prevOverContainer = document.createElement("div");
			label_prevOverContainer.id = "ow_lab_prevOverContainer";

			// suppression du tooltip sur la barre de progression (un peu genant en + du preview)
			document.getElementById("ow_id_progress_slider").removeAttribute("title");
			document.getElementById("ow_id_progress_slider").appendChild(prevOverContainer);
			prevOverContainer.appendChild(img_prevOverContainer);
			prevOverContainer.appendChild(label_prevOverContainer);

		}

		this.hide_preview_timeout = null;

		document.getElementById("ow_id_progress_slider").addEventListener("mouseover", function(mouseEvent){
			document.getElementById("ow_prevOverContainer").style.display = "";
			if(this.hide_preview_timeout != null){
				// console.log("clearTimeout from mouse-enter");
				clearTimeout(this.hide_preview_timeout);
				this.hide_preview_timeout = null;
			}
		}.bind(this),false);

	document.getElementById("ow_id_progress_slider").addEventListener("mousemove", function(mouseEvent){
			if((typeof mouseEvent.srcElement != "undefined" && mouseEvent.srcElement.id != "ow_id_progress_slider" && mouseEvent.srcElement.id != "progress_track" && mouseEvent.srcElement.id != "progress_handle")
			|| (typeof mouseEvent.target != "undefined" && mouseEvent.target.id != "ow_id_progress_slider" && mouseEvent.target.id != "progress_track" && mouseEvent.target.id != "progress_handle")){
				return 0 ;
			}

			if((typeof mouseEvent.srcElement != "undefined" && mouseEvent.srcElement.id == "progress_handle") || (typeof mouseEvent.target != "undefined" && mouseEvent.target.id=="progress_handle")){
				if(typeof mouseEvent.srcElement != "undefined"){
					value = this.timeline.translateToValue(parseInt(document.getElementById("progress_handle").style.left,10)+mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = parseInt(document.getElementById("progress_handle").style.left,10)+(mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}else if(typeof mouseEvent.target != "undefined"){
					value = this.timeline.translateToValue(parseInt(document.getElementById("progress_handle").style.left,10)+mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = parseInt(document.getElementById("progress_handle").style.left,10)+(mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}
			}else{
				if(typeof mouseEvent.srcElement != "undefined"){
					value = this.timeline.translateToValue(mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = (mouseEvent.offsetX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}else if(typeof mouseEvent.target != "undefined"){
					value = this.timeline.translateToValue(mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline);
					document.getElementById("ow_prevOverContainer").style.left = (mouseEvent.layerX-this.movie_options.offset_mouseEvent_timeline)+"px";
				}
			}

			if(value < this.timeline.range.start){
				value = this.timeline.range.start;
			}else if (value > this.timeline.range.end){
				value = this.timeline.range.end;
			}


			if(this.movie_options.timecode_IN){
				value_pic = value-(this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag));
			}else{
				value_pic = value;
			}

			if(this.movie_options.mosaic_use_index){
				this.getPreviewPicFromIndex(value_pic);
			}else{
				this.getPreviewPic(value_pic);
			}

			if(!this.movie_options.tcMosaicExact){
				document.getElementById("ow_lab_prevOverContainer").innerHTML = this.DisplayTimeCode(value);
			}
		}.bind(this),false);

		document.getElementById("ow_id_progress_slider").addEventListener("mouseout", function(mouseEvent){
			if(this.hide_preview_timeout == null){
				this.hide_preview_timeout = setTimeout(function(){
					document.getElementById("ow_prevOverContainer").style.display = "none";
					this.hide_preview_timeout = null;
				}.bind(this),250);
			}
		}.bind(this),false);
	}


	if(this.movie_options.span_time_passed){
		span = "ow_time_passed";
	}else{
		span = 'none';
	}

	// création du slider timeline de progression
	this.timeline = new Control.Slider(["progress_handle"],"progress_track", {
									   alignY: 10,
									   range : $R(0,100),						// initialisation d'un range par défaut
									   onSlide: updateTimeline.bind(this),		//association des évenements aux fonctions
									   onChange: changeTimeline.bind(this),
									   startSpan : span
									   });


	if(!this.selectionSliderDisabled){
		// Fonction onSlide de la timeline selection
		slideSel = function (values){
			if(this.BegSelect != this.EndSelect && values[0]==this.BegSelect && this.EndSelect == values[1]){
				return 0;
			}

			this.BegSelect = values[0];						// Mise a jour de la variable début de selection grâce aux valeurs des curseurs
			this.EndSelect = values[1];					// Mise a jour de la variable fin de selection grâce aux valeurs des curseurs

			this.timeline.setValue(Math.round(values[this.timelineSel.activeHandleIdx]*100)/100);		// positionnement de la vidéo au niveau du curseur actif

			if(this.display_buff){
				this.SelSlide = true;
				this.stopUpdateRange = true;
			}

			if(values[0]==values[1]){
				if($("handle_BegSel").hasClassName('selected')){
					this.timelineSel.activeHandle = document.getElementById("handle_EndSel");
					this.timelineSel.activeHandleIdx =1;
					$("handle_BegSel").removeClassName('selected');
					$("handle_EndSel").addClassName('selected');

				}else{
					this.timelineSel.activeHandle = document.getElementById("handle_BegSel");
					this.timelineSel.activeHandleIdx =0;
					$("handle_EndSel").removeClassName('selected');
					$("handle_BegSel").addClassName('selected');
				}
			}
			if(this.movie_options.onSelectionSliderChange != null){
				this.movie_options.onSelectionSliderChange();
			}


			if (this.movie_options.onSelectionSliderChange){this.movie_options.onSelectionSliderChange();   }
		}

		// Fonction onChange de la timeline selection
		changeSel = function(values){
			this.SelSlide = false;
			this.stopUpdateRange = false;


			//this.timeline.setValue(values[this.timelineSel.activeHandleIdx]); ???
			// this.BegSelect = values[0];
			this.BegSelect = values[0];
			this.EndSelect = values[1];
			if(!this.dontCallOnChange){
				this.canSeek = true ;
				this.timeline.setValue(Math.round(values[this.timelineSel.activeHandleIdx]*100)/100);
			}
			if (this.BegSelect!=this.EndSelect){ this.selectionExists = true;}
			else{this.selectionExists = false; }


		}


		clickSelHandle = function (){
			if(this.timeline.value != this.timelineSel.values[this.timelineSel.activeHandleIdx]){
				this.StopTheVideo();
				this.timeline.setValue(this.timelineSel.values[this.timelineSel.activeHandleIdx]);
				// ou alors on test activeHandleIdx et on applique la méthode GoToBegSelection ou GoToEndSelection suivant le resultat
			}
		}

		opts_timeline_sel = {
		  range : $R(0,100),				// initialisation d'un range par défaut
		  spans : ["selection"],			// referencement du span permettant de colorer la selection
		  restricted : true,				// pour maintenir les valeurs debut selection et fin selection cohérente
		  sliderValue : [0,0],			// initialisation des valeurs des curseurs par défaut
		  onSlide:slideSel.bind(this),	// association des évenements aux fonctions
		  onChange : changeSel.bind(this)
		  };
		  if(typeof this.mediaSeekPoints != "undefined"){
			 opts_timeline_sel.values = this.mediaSeekpoints;
		  }


		// création du slider timeline de selection
		this.timelineSel = new Control.Slider(["handle_BegSel","handle_EndSel"],"select_track",opts_timeline_sel);
		this.timelineSel.track.stopObserving("mousedown", this.timelineSel.eventMouseDown);
		document.getElementById("handle_BegSel").onclick = clickSelHandle.bind(this);
		document.getElementById("handle_EndSel").onclick = clickSelHandle.bind(this);
	}
}

OJW_player.prototype.setTimeline = function (){
	// Paramétrage des timeline de progression et de selection
	// parametrage des range max des timeline
	//*****************TEST TC IN TC OUT
	if(!this.playlist){
		old_state = jwplayer().getState();
		old_time = jwplayer().getPosition();
	}

	if(this.movie_options.timecode_IN){
		this.timeline.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);
		if(!this.selectionSliderDisabled){this.timelineSel.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);}
	}




	if(!this.selectionSliderDisabled && !this.selectionExists ){
		this.dontCallOnChange = true ;
		this.timelineSel.setValue(this.timelineSel.range.start,0);
		this.timelineSel.setValue(this.timelineSel.range.start,1);
	}

	this.adjustTimeline();

	if(!this.playlist){
		jwplayer().seek(old_time);
		if(old_state == "PLAYING"){
			jwplayer().play(true);
		}else{
			jwplayer().pause(true);
		}
	}

	/*if(this.movie_options.timecode_IN){
		this.timeline.setValue(this.timeline.range.start);
	}*/
}


OJW_player.prototype.adjustTimeline = function(){
	// console.log('call adjustTimeline TC_IN '+this.movie_options.timecode_IN+" TC_OUT "+this.movie_options.timecode_OUT+" DUREE "+this.duration);
	if(!this.showFlashController){
	var val=this.timeline.value;			// enregistrement de la derniere valeur timeline progression
	if(!this.selectionSliderDisabled){
		var sel = this.timelineSel.values;		// enregistrement des dernieres valeurs timeline selection
	}

   selectionIsHidden = this.movie_options.selectionHidden;

   if(selectionIsHidden  && !this.selectionSliderDisabled){
	      this.ShowSelectionSlider();
	}
	this.timeline.dispose();			// suppression de la timeline et de ses listeners
	if(!this.selectionSliderDisabled){
		this.timelineSel.dispose();
	}
	this.InitTimeline();				// réinitialisation de la nouvelle timeline
	//*****************TEST TC IN TC OUT
	if(this.movie_options.timecode_IN){
		this.timeline.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);
		if(!this.selectionSliderDisabled){this.timelineSel.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);}
	}

	if(this.movie_options.timecode_OUT){
		this.timeline.range.end = this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag);
		if(!this.selectionSliderDisabled){this.timelineSel.range.end = this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag);}

	}else{
		// if (!this.duration){
			// this.duration = jwplayer().getDuration();
		// }
		// this.timeline.range.end = this.duration;		//DURATIONBUG this.duration normalement
		// if(!this.selectionSliderDisabled){this.timelineSel.range.end = this.duration}
		if( jwplayer().getDuration() != -1){
			this.timeline.range.end = jwplayer().getDuration();		//DURATIONBUG this.duration normalement
			if(!this.selectionSliderDisabled){this.timelineSel.range.end = jwplayer().getDuration();}
		}
	}


	this.dontCallOnChange = true;		// flag empechant les instructions de l'évenement onChange (évite les saccades)
	if(!this.selectionSliderDisabled){
		this.timelineSel.setValue(sel[1],1);	// positionnement de la nouvelle timeline selection aux anciennes valeurs
		this.timelineSel.setValue(sel[0],0);
	}

	this.timeline.setValue(val);		// positionnement de la nouvelle timeline progression à l'ancienne position
	// console.log('adjustTimeline timeline.range : '+this.timeline.range.start+" "+this.timeline.range.end);
	if(selectionIsHidden && !this.selectionSliderDisabled){
		// alert("adjustTimeline > hideselection");
        this.HideSelectionSlider();
	}

		//MS update de l'affichage des range bufferisés apres un reset timeline (pour forcer un recalcul des divs dans le cas ou toute la video se charge avant que les calculs soient faits
	//	if (this.controlSeek == "Ranges"){
	//		this.displayBufferedRange();
	//	}
	//	setTimeout(this.correctScrolls.bind(this),1);	// planification d'un test en cas de bug d'affichage dus à la place necessaire aux scrolls
	}
}

//============================== FIN TIMELINE

//============================== SKINS BOUTONS // affichage timecode

// Fonction gérant les modifications de l'apparence du bouton play/pause en changeant la classe de l'élément
OJW_player.prototype.changeButtonPlay = function(){
	// Le changement de skin du bouton play / pause se fait uniquement en changeant la classe de l'élément,
	// deux css sont définis pour ce bouton, un #ow_id_bouton_play_pause.play et un #ow_id_bouton_play_pause.pause.
	// Cela permet une plus grande flexibilité au niveau des choix graphiques (hover etc ...)
	// NB : les fonctionnalités sont gérés de façon totalement indépendante de l'affichage
	if(this.playlist && this.currentPlyId){
		state = jwplayer(this.currentPlyId).getState();
	}else{
		state = jwplayer().getState();
	}

	if ((state == 'PAUSED' || state == 'IDLE' ) && this.fastSteppingOn == null){
		$("ow_bouton_play_pause").removeClassName("pause");
		$("ow_bouton_play_pause").addClassName("play");
	} else {
		$("ow_bouton_play_pause").removeClassName("play");
		$("ow_bouton_play_pause").addClassName("pause");
	}
}

OJW_player.prototype.UItest = function(arg1,arg2){
	//MS renommage de "event" en "event_" car visiblement causait des problèmes sur IE ...

	if(!this.playlist){
		event_ = arg1;
	}else {
		player_id = arg1;
		event_ = arg2;
	}

	// console.log("onTime : id_arg "+player_id+" currentPlyId "+this.currentPlyId+" offset : "+event_.position);
	if(this.playlist && this.currentPlyId != player_id){
		return ;
	}



	if(!this.canSeek ){
		this.canSeek=true;
	}
	// MS voir comment on déclare la première initialisation timeline dans le cas d'une playlist (car sinon pause pb lors des change_track)
	/*if (event_ && event_.duration != '-1' && this.durationCurrentItem!= parseInt(event_.duration)){
		//alert(event_.duration);

		console.log('ONLY SET TIMELINE ????');
		this.durationCurrentItem = parseInt(event_.duration) ;
		this.setTimeline();
		if(document.getElementById('timecodeDuration')){
			document.getElementById('timecodeDuration').innerHTML = this.DisplayTimeCode(event_.duration);
		}
	}*/
	if (event_){
		if (this.movie_options.timecode_OUT && this.GetCurrentTime(event_.position)>(this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag))){
			this.timeline.setValue(this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag));
			this.StopTheVideo();
			// console.log('uitest blocage par TCOUT  ');
		}else {
			this.dontCallOnChange=true;
			if(this.playlist){
				time = this.GetCurrentTime(event_.position);
				// console.log( "  "+player_id+" time called : "+time);
				this.timeline.setValue(time);
			}
			else{this.timeline.setValue(event_.position);}
		}
	}

	if(document.getElementById('timecodeDisplay')){
		if(this.playlist){
			time = this.GetCurrentTime(event_.position);
			document.getElementById('timecodeDisplay').innerHTML = this.DisplayTimeCode(time);
			if (typeof(this.movie_options.onTimecodeChange) == "function"){ this.movie_options.onTimecodeChange(time);}
			else if (typeof(onTimecodeChange) == "function") onTimecodeChange(time);
		}else{
			document.getElementById('timecodeDisplay').innerHTML =  this.DisplayTimeCode(event_.position);
			if (typeof(this.movie_options.onTimecodeChange) == "function"){ this.movie_options.onTimecodeChange(event_.position);}
			else if (typeof(onTimecodeChange) == "function") onTimecodeChange(event_.position);

		}
	}

	if(this.pauseAfterSeek){
		this.StopTheVideo();
		this.canSeek=true;
		this.pauseAfterSeek = false ;
	}

	if(this.fastSteppingOn){
		// MS Pause juste après un seek en mode fastStepping n'est pas possible sur IE8-7
		// Le stepping sera effectué mais l'image ne sera jamais updaté ,
		// Pour IE on laisse donc le comportement standard "play after seek" du jwplayer ...
		if(window.addEventListener){
			jwplayer().pause(true);
		}
		this.canSeek=true;
	}
}

OJW_player.prototype.bufferinfos = function(e){
	//MSTEST
	// document.getElementById("infobuff").innerHTML = jwplayer().getState()+" "+jwplayer().getPosition();
}

// Méthode affichage du timecode courant
OJW_player.prototype.DisplayTimeCode = function(secs){
	if(secs <0 ){
		secs = 0;
	}
	var offset = 0;
	if(this.movie_options.timecode_lag && this.movie_options.show_tc_with_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}

	var tc=this.secsToTimeCode(secs+offset);

	if(this.movie_options.timecode_display_format =='hh:mm:ss:ff'){
		return tc;
	}else{
		var str ;
		tc = tc.split(':');
		if(this.movie_options.timecode_display_format=='hh:mm:ss'){
			str = tc[0]+':'+tc[1]+':'+tc[2];
		}else if(this.movie_options.timecode_display_format=='mm:ss'){
			m = (parseInt(tc[0])*60+parseInt(tc[1]));
			if(m<10){ m= '0'+m ; }
			str = m+':'+tc[2];
		}
	}
	return str;
}

// fonction de passage d'une valeur en secs à une chaine format timecode
OJW_player.prototype.secsToTimeCode = function(secs){
	var seconds = Math.round(secs*100)/100;

	var h=Math.floor(seconds/3600)<10?"0"+Math.floor(seconds/3600):Math.floor(seconds/3600);
	var m=Math.floor((seconds-h*3600)/60)<10?"0"+Math.floor((seconds-h*3600)/60):Math.floor((seconds-h*3600)/60);
	var s=Math.floor(seconds-(h*3600)-(m*60))<10?"0"+Math.floor(seconds-(h*3600)-(m*60)):Math.floor(seconds-(h*3600)-(m*60));
	// var f=Math.floor((seconds-(h*3600)-(m*60)-s)/0.04)<10?"0"+Math.floor((seconds-(h*3600)-(m*60)-s)/0.04):Math.floor((seconds-(h*3600)-(m*60)-s)/0.04);
	var f=Math.round(((seconds-(h*3600)-(m*60)-s)/0.04))<10?"0"+Math.round((seconds-(h*3600)-(m*60)-s)/0.04):Math.round((seconds-(h*3600)-(m*60)-s)/0.04);
	/*document.getElementById("infobuff").innerHTML= (seconds-(h*3600)-(m*60)-s)+" "+((seconds-(h*3600)-(m*60)-s)/0.04)+"<br />";
	document.getElementById("infobuff").innerHTML+=f+"<br />";
	document.getElementById("infobuff").innerHTML+=Math.round(((seconds-(h*3600)-(m*60)-s)/0.04)*100)/100<10?"0"+Math.round((seconds-(h*3600)-(m*60)-s)/0.04*100)/100:Math.round((seconds-(h*3600)-(m*60)-s)/0.04*100)/100;*/
	return h+":"+m+":"+s+":"+f;
}

// fonction de passage d'une chaine format timecode à une valeur en secs
OJW_player.prototype.timecodeToSecs = function(timecode){
	if(timecode == null || typeof(timecode) == "undefined")
		return 0;

	arrayTC = timecode.split(":");
	h = parseFloat(arrayTC[0]);
	m = parseFloat(arrayTC[1]);
	s = parseFloat(arrayTC[2]);
	f = parseFloat(arrayTC[3]);

	return h*3600+m*60+s+f*0.04;
}

// Fonction qui retourne le timecode actuel au format long (+offset si nécessaire)
OJW_player.prototype.GetCurrentLongTimeCode = function(){
    offset = 0;
    if(this.movie_options.timecode_lag){
        offset = this.timecodeToSecs(this.movie_options.timecode_lag);
    }
    return this.secsToTimeCode(jwplayer().getPosition()+offset);
}


OJW_player.prototype.GoToLongTimeCode = function(timecode)
{
	// GT 28/09/11 : prise en compte de l'offset
	var offset=0;
	if(this.movie_options.timecode_lag)
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);

	jwplayer().seek(this.timecodeToSecs(timecode)-offset-this.BegTrack);
}


//============================== FIN SKINS BOUTONS // affichage timecode


// =================== Fonctions fullscreen
// gestion fullscreen, toggle et les deux fonctions

OJW_player.prototype.ToggleFullScreen = function() {
	if(!this.loadedMetadata){
		return 0;
	}
	if (!this.fullscreen){
        this.SwitchToFullScreen();
	}
	else{
        this.FromFullScreenToPopUp();
	}
}

OJW_player.prototype.SwitchToFullScreen = function(){
	this.fullscreen = true;
	try{
	window.moveTo(0,0);
	window.resizeTo(screen.availWidth,screen.availHeight);
	}catch(err){}

	// ajout de classes aux éléments afin de modifier leur affichage
	$(this.controllerpanel.id).addClassName("controllerpanelFS");



	$$('body')[0].addClassName("hideover");
	$(this.container.id).removeClassName("containerNS");
	$(this.container.id).addClassName("containerFS"); //ESSAI DE CHANGEMENT DE FS

	if(this.playlist){
		for (i =0 ;i < this.track_media.length; i ++){
			$(this.track_media[i].id+"_wrapper").addClassName("capsuleVideoFS"); //ESSAI DE CHANGEMENT DE FS
			document.getElementById(this.track_media[i].id).className+=" videoFS";
		}
	}else{
		$("ply_wrapper").addClassName("capsuleVideoFS"); //ESSAI DE CHANGEMENT DE FS
		document.getElementById("ply").className+=" videoFS";
	}

	this.adjustSize();
	$("ow_id_progress_slider").style.overflow = "visible";		// Pour empecher un bug d'affichage du curseur de la timeline

	$('ow_controllerpanel').style.top = "";


}


OJW_player.prototype.FromFullScreenToPopUp = function() {
	this.fullscreen = false;

	// retrait des classes afin de rétablir l'affichage initial
	$$('body')[0].removeClassName("hideover");



	if(this.playlist){
		for (i =0 ;i < this.track_media.length; i ++){
			$(this.track_media[i].id+"_wrapper").removeClassName("capsuleVideoFS"); //ESSAI DE CHANGEMENT DE FS
			$(this.track_media[i].id).removeClassName(" videoFS");
		}
	}else{
		$("ply_wrapper").removeClassName("capsuleVideoFS"); //ESSAI DE CHANGEMENT DE FS
		document.getElementById('ply').className = document.getElementById('ply').className.replace(" videoFS","");
	}
	$(this.controllerpanel.id).removeClassName("controllerpanelFS");

	$(this.container.id).removeClassName("containerFS");
	$(this.container.id).addClassName("containerNS");
	// $("ply_wrapper").removeClassName("capsuleVideoFS"); //ESSAI DE CHANGEMENT DE FS


	//document.getElementById('fiche_info').setAttribute('style','visibility : visible ;');

//	if($('fiche_info')){
//		document.getElementById('fiche_info').style.visibility = "visible";
//	}

	// ajustement de la timeline
	this.adjustSize();
	//	$("ow_id_progress_slider").style.overflow = "hidden"; 		// Pour empecher l'affichage du curseur hors de la timeline en cas de bug graphique
}

// =================== FIN Fonctions fullscreen

// ======================= Fonctions contrôle volume / mute / son .... =============
OJW_player.prototype.InitSoundSlider = function() {

	updateSoundSlider = function(value){
		if(this.playlist){
			player = jwplayer(this.currentPlyId);
		}else{
			player = jwplayer();
		}
        if( player.getMute() && value != 0){
			player.setMute(false);
		}

		player.setVolume(value);
	}


	changeSoundSlider = function(value) {
        if(this.playlist){
			player = jwplayer(this.currentPlyId);
		}else{
			player = jwplayer();
		}

		if( player.getMute() && value != 0){
			player.setMute(false);
			if(this.playlist){
				for(i=0; i < this.track_media.length ; i++){
					jwplayer(this.track_media[i].id).setMute(false);
				}
			}
		}

		if(!(player.getMute() && value == 0)){
			player.setVolume(value);
			if(this.playlist){
				for(i=0; i < this.track_media.length ; i++){
					jwplayer(this.track_media[i].id).setVolume(value);
				}
			}
		}
	}

	if(this.muted == true){
		initSliderVal = "0";
	}else{
		initSliderVal = "100";
	}


	this.soundslider = new Control.Slider("ow_sound_handle","ow_id_sound_slider",{
										  range : $R(0,100),
										  sliderValue : initSliderVal,
										  onChange :  changeSoundSlider.bind(this) ,
										  onSlide :	updateSoundSlider.bind(this)
										  });

}

OJW_player.prototype.SetMuteState = function (){
	// console.log("call setMuteState ");
	if(this.fastSteppingOn == null){
		if(this.playlist){
			state = jwplayer(this.currentPlyId).getMute();
			// console.log("state current player ("+this.currentPlyId+") : "+state);

			for(i =0;i < this.track_media.length ; i++){
				jwplayer(this.track_media[i].id).setMute(!state);
			}
			mute = jwplayer(this.currentPlyId).getMute();
			// console.log("new state current player ("+this.currentPlyId+") : "+mute);

		}else{
			jwplayer().setMute(!jwplayer().getMute());
			mute = jwplayer().getMute()
		}
		if(mute){
			$("ow_bouton_mute_sound").addClassName("ClickBouton");
			this.soundVolume = jwplayer().getVolume();
			this.soundslider.setValue(0);
		}else {
			$("ow_bouton_mute_sound").removeClassName("ClickBouton");
			this.soundslider.setValue(this.soundVolume);
		}
	}
}

OJW_player.prototype.defaultControlVolumeChange = function(){
	if(this.playlist && this.currentPlyId){
		mute = jwplayer(this.currentPlyId).getMute();
	}else{
		mute = jwplayer().getMute()
	}

    if (!mute && document.getElementById('ow_bouton_mute_sound')){
        $("ow_bouton_mute_sound").removeClassName("ClickBouton");
	}
}

// ======================= FIN Fonctions contrôle volume / mute / son .... =============

// Fonction de resize du player
OJW_player.prototype.adjustSize = function(){

	// document.getElementById("infobuff").innerHTML += this.container.id+" ";
	container_width = this.container.parentNode.offsetWidth;
	new_container_height =  (container_width*(this.movie_options['height'])/this.movie_options['width']);
	if (typeof this.controllerpanel != 'undefined' && !this.simpleController && this.movie_options.force_width_controller == false){
		this.controllerpanel.style.width = container_width+"px";
	}
	this.adjustTimeline();

	if(this.movie_options.fit_height_container && new_container_height > (this.container.parentNode.offsetHeight-(typeof this.controllerpanel != 'undefined' && typeof this.controllerpanel.offsetHeight!='undefined'?this.controllerpanel.offsetHeight:0))){
        ratio = this.movie_options.width / this.movie_options.height;
		if (this.movie_options.controller_on_video == false)
			new_container_height = this.container.parentNode.offsetHeight-this.controllerpanel.offsetHeight;
		else
			new_container_height = this.container.parentNode.offsetHeight;
		old_container_width = container_width;
		if (!this.movie_options.force_width_controller && this.mediaType == "video"){
			container_width =new_container_height*ratio;
		}

		// margin_for_video = (old_container_width - container_width)/2;
		if(this.playlist){

		}else{
			$j("#ply_wrapper").css("margin","0 auto");
		}
	}

	if(this.playlist){
		for (i = 0 ; i < this.track_media.length; i ++){
			jwplayer(this.track_media[i].id).resize(container_width,new_container_height+this.offset_controlbar_height);
		}
	}else{
		jwplayer().resize(container_width,new_container_height+this.offset_controlbar_height);
	}
	// console.log("container_width "+container_width+" new_container_height:"+new_container_height);


	if(!this.simpleController && this.movie_options.fit_height_container){
		if (this.movie_options.force_width_controller == false)
			this.controllerpanel.style.width = container_width+"px";
		if(!this.showFlashController || !this.simpleController ){
			this.adjustTimeline();
			if(this.display_buff)
				this.displayBufferedRange();
		}
	}

	if(this.mediaType == "video"  && !this.movie_options['fit_height_container'] && !this.fullscreen){
		 this.container.style.minHeight = ((parseInt(document.getElementById(this.container.parentNode.id).offsetWidth)*this.movie_options['height']/this.movie_options['width'])+((typeof this.controllerpanel!='undefined')?this.controllerpanel.offsetHeight:''))+"px";
	}
}

OJW_player.prototype.mediaTypeDetection = function(url, typeMime){




    if(typeMime != ""){
        splits = typeMime.split("/");
        mediaType = splits[0];
	} else{
        splits = url.split(".");
        extension = splits[splits.length-1];

        switch (extension){
            case "mp4":
			case "MP4":
			case "m4v":
                mediaType = "video";
                break;
            case "mp3":
			case "mpa":
			case "MPA":
			case "m4a":
                mediaType = "audio";
                break;
			default :
				mediaType = "video";
			break;
		}
	}
    return mediaType ;
}


// ====TIMELINE BUFFERISATION
/*
OJW_player.prototype.testBuff = function () {

	if(this.controlSeek == "Ranges" && this.fastSteppingOn == null ){  //  && this.jogSteppingOn == null && !this.stopUpdateRange
		this.updateCurrentRange();
	}
	// lorsque la video charge, on actualise le range courant de myBuffered
}*/

OJW_player.prototype.initRange = function(){
	//************************** Objet remplacant l'attribut .buffered des vidéos
	//nom de l'objet
	this.myBuffered = {
		ranges : new Array([0,0]), // array contenant l'ensemble des ranges de video chargée
		currentRange : [],
		currentIdx : 0			// int contenant l'index du range courant
	}
	this.myBuffered.currentRange = this.myBuffered.ranges[0];
	//************************************
}

// Fonction qui actualise le range courant
OJW_player.prototype.updateCurrentRange = function(){
	if(this.fastSteppingOn == null && !this.stopUpdateRange){
		percent_buff = jwplayer().getBuffer()+this.myBuffered.currentRange[0];
		/*console.log("percent_buff : "+percent_buff);
		if(this.movie_options.timecode_IN){
			percent_buff = percent_buff-(this.timecodeToSecs(this.movie_options.timecode_IN)*100/jwplayer().getDuration());
			console.log("% offset : "+(this.timecodeToSecs(this.movie_options.timecode_IN)*100/jwplayer().getDuration())+" percent_buff : "+percent_buff);
		}*/
		// MS - filtre empechant la valeur "infinity" comme retour du getBuffer
		if(!isFinite(percent_buff)){return ;}

		if(percent_buff>100) percent_buff = 100;
		// alert("update_curr_range test : "+jwplayer().getBuffer()+" "+this.myBuffered.currentRange[1]);
		if( percent_buff >= this.myBuffered.currentRange[1]){
			this.myBuffered.currentRange[1] = percent_buff;
		/*if (this.movie_options.timecode_OUT)
				if (this.media.buffered.end(0)>(this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag)))
					this.myBuffered.currentRange[1]=this.timecodeToSecs(this.movie_options.timecode_OUT)-this.timecodeToSecs(this.movie_options.timecode_lag);*/
		}


		if(this.myBuffered.currentIdx < this.myBuffered.ranges.length-1){	// Si le range courant n'est pas le dernier,
			if (this.myBuffered.currentRange[1] >= this.myBuffered.ranges[this.myBuffered.currentIdx+1][0]){		 // si la fin du range courant est supérieure au début du range suivant
				this.myBuffered.ranges[this.myBuffered.currentIdx][1] = this.myBuffered.ranges[this.myBuffered.currentIdx+1][1];	// val de fin range courant reçoit val de fin range suivant
				this.myBuffered.ranges.splice(this.myBuffered.currentIdx+1,1);                      // suppression du range suivant
			}
		}

		this.displayBufferedRange();


		// this.StopPeriodicalUpdateRanges();
	}
}

//Fonction permettant de compléter la méthodes de tri de l'array pour les ranges
OJW_player.prototype.sortRange = function(a,b){
	return a[0]-b[0];			// Permet de classer les ranges dans l'ordre chrono de leurs valeurs de début
}

// Fonction pour recuperer l'index du range courant
OJW_player.prototype.getCurrentRangeIdx = function(){
	i = 0;
	while (this.myBuffered.currentRange[0] != this.myBuffered.ranges[i][0]){
		i++;
	}
	return i ;		// renvoi l'index du range courant;
}

//Fonction pour se reperer dans les ranges lorsqu'on change la timeline
OJW_player.prototype.changeRangeCheck = function(value){
	percent_value = Math.round((value*100/ this.durationCurrentItem)*100)/100;

	if(!isFinite(percent_value)){return ;}

	// Si pas de stepping en cours et qu'on est hors des ranges actuels
	if( this.fastSteppingOn == null && this.outOfBufferedRange(percent_value) ){ // this.jogSteppingOn == null &&
		this.myBuffered.ranges.push([percent_value,percent_value]);						// Creation d'un nouveau range, valeurs débuts et fins = valeur actuelle.
		this.myBuffered.currentRange = this.myBuffered.ranges[this.myBuffered.ranges.length-1];		// Le currentRange référence le nouveau range
		this.myBuffered.ranges.sort(this.sortRange);					// classement des ranges dans l'ordre chrono
		this.myBuffered.currentIdx = this.getCurrentRangeIdx();			// recuperation du nouvel index du range courant
	}

	// this.BeginPeriodicalUpdateRanges();

}

// Fonction test hors des ranges déja créés
OJW_player.prototype.outOfBufferedRange = function(value){
	for (i = 0 ; i< this.myBuffered.ranges.length; i ++){		// pour chaque range créé
		if (value >=this.myBuffered.ranges[i][0] && value <= this.myBuffered.ranges[i][1]){		// si value est bornée par les valeurs de début et de fin d'un range
			this.myBuffered.currentRange = this.myBuffered.ranges[i];								// actualisation du range courant
			this.myBuffered.currentIdx = i ;														// actualisation de l'index du range courant
			return false;																			// retourne false (on est dans un range déjà existant)
		}
	}
	return true;	// sinon, retourne true, on est hors des ranges existants
}

//Fonction de test de la position par rapport au range courant :
OJW_player.prototype.outOfCurrentRange = function(valueToCompare){
	if(valueToCompare < this.myBuffered.currentRange[0] || valueToCompare > this.myBuffered.currentRange[1]){
		return true;
	} else{
		return false;
	}
}

//Fonction qui gère l'affichage des zones bufferisées sous Safari
OJW_player.prototype.displayBufferedRange=function(){
	// Si le nombre de divs représentant les ranges est différent du nombre de range (cas de jump ou de merge)
	if(this.displayBufferedRangesDivs.length != this.myBuffered.ranges.length){
		// Si il y a moins de divs d'affichage que de ranges
		if(this.displayBufferedRangesDivs.length < this.myBuffered.ranges.length){
			//Création, paramétrage et stockage dans la liste d'un nouveau div
			newBufferedDisplay = document.createElement("div");
			newBufferedDisplay.id = this.displayBufferedRangesDivs.length+"bufferedDisplay";
			newBufferedDisplay.className = "bufferedDisplayDivs";
			//newBufferedDisplay.style.marginTop = "-"+document.getElementById("backgroundTimeline").clientHeight+"px";
			newBufferedDisplay.style.width = "0%";
			document.getElementById("bufferedDisplayContainer").appendChild(newBufferedDisplay);
			this.displayBufferedRangesDivs.push(newBufferedDisplay);
		}else{
			//Si il y a plus de divs que de ranges (cas des merges de ranges)
			//On supprime le dernier div de la page et de la liste
			document.getElementById("bufferedDisplayContainer").removeChild(this.displayBufferedRangesDivs[this.displayBufferedRangesDivs.length-1]);
			this.displayBufferedRangesDivs.pop();
		}

		// Actualisation des divs existants, les affichages correspondent aux ranges de l'objet myBuffered
		for(i=0;i<this.displayBufferedRangesDivs.length;i++){	// Pour chaque div
			// On place le début du bloc à la position correspondant au début du range
			try{
			document.getElementById(i+"bufferedDisplay").style.left = this.myBuffered.ranges[i][0] + "%";
			// On règle la largeur du bloc en fonction de la taille du range
			document.getElementById(i+"bufferedDisplay").style.width = (this.myBuffered.ranges[i][1]-this.myBuffered.ranges[i][0]) + "%";
			}catch(err){
				// alert(err+" \n"+ this.myBuffered.ranges.join("//"));
			}

		}

	} else{// Si le nombre de div == le nombre de ranges , on update le range courant
		if(this.movie_options.timecode_IN){
			offset_prct = ((this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag))*100/jwplayer().getDuration());
		}else{
			offset_prct = 0;
		}
		try{
			document.getElementById(this.myBuffered.currentIdx+'bufferedDisplay').style.width = (this.myBuffered.currentRange[1]-(this.myBuffered.currentRange[0]-offset_prct))+ "%";
			document.getElementById(this.myBuffered.currentIdx+'bufferedDisplay').style.left = (this.myBuffered.currentRange[0]-offset_prct)<0?0:(this.myBuffered.currentRange[0]-offset_prct)+ "%";
		}catch(err){
			// alert(err+" \n"+ this.myBuffered.ranges.join("//"));
		}

	}
}




// MS 21/05/13 : PLAYLIST

OJW_player.prototype.traiterFluxXml=function(str)
{
	var elements_track;
	var track_duration;

	if (str.responseXML==null)
		elements_track=str.getElementsByTagName('track');
	else
		elements_track=str.responseXML.getElementsByTagName('track');

	for(i=0;i<elements_track.length;i++)
	{
		if (elements_track[i].getElementsByTagName('location')[0]!=null){

			this.track_location.push(elements_track[i].getElementsByTagName('location')[0].firstChild.nodeValue);

			if (elements_track[i].getElementsByTagName('duration')[0]!=null){
				track_duration=parseFloat(elements_track[i].getElementsByTagName('duration')[0].firstChild.nodeValue);
			}else
				track_duration=3600;

			if(elements_track[i].getElementsByTagName('start_offset')[0]!=null){
				track_start_offset = parseFloat(elements_track[i].getElementsByTagName('start_offset')[0].firstChild.nodeValue);
			}else{
				track_start_offset = 0;
			}

			this.track_duration.push(track_duration);

			this.track_start.push(this.duration);
			this.track_start_offset.push(track_start_offset);
			this.duration+=track_duration;
			this.track_end.push(this.duration);

			this.nb_tracks++;
		}
	}

}


// Fonction attribuant une vidéo au player à partir d'une URL et d'un type.
OJW_player.prototype.AttachPlaylist = function(){
    var track_media, text;


	this.jwPlayerOpts.autoplay = true ;
	this.jwPlayerOpts.image = null;
	// this.jwPlayerOpts.mute = true ;

	for(i=0;i<this.nb_tracks;i++){

		var ply = document.createElement('div');
		this.currentPlyId = this.movie_options['name']+i;
		ply.id=this.currentPlyId;
		if(typeof this.controllerpanel!="undefined"){
			this.container.insertBefore(ply,this.controllerpanel);
		}else{
			this.container.appendChild(ply);
		}

		// setup du player
		if((typeof this.jwPlayerOpts.height == "undefined" || isNaN(this.jwPlayerOpts.height)) && this.movie_options.force_ratio_ref && this.jwPlayerOpts.width){
			this.movie_options['height'] = this.jwPlayerOpts.width*(1/this.movie_options.force_ratio_ref);
			this.jwPlayerOpts.height = this.movie_options['height'];
		}


		this.jwPlayerOpts.file = this.track_location[i];

		jwplayer(ply.id).setup(this.jwPlayerOpts);


		function initJsInteraction(id){
			// console.log("InitJSInteraction player multiples, id : "+id);
			// control skin des boutons de lecture / pause
			index = parseInt(id.substring(3),10);
			if(this.track_init_events[index] == false){
				jwplayer(id).onPause(this.changeButtonPlay.bind(this));
				jwplayer(id).onPlay(this.changeButtonPlay.bind(this));
				jwplayer(id).onIdle(this.OnEndTrack.bind(this));
				/*jwplayer(id).onIdle(this.changeButtonPlay.bind(this));*/

				jwplayer(id).onMeta(function(event){
					if(typeof this.mediaSeekpoints =="undefined" || typeof this.mediaSeekpoints[id.substring(this.movie_options['name'].length)] =="undefined" ){
						this.initMediaSeekpoints(event.metadata,id.substring(this.movie_options['name'].length));
					}
					if(!this.loadedMetadata && !this.autoplay){
						jwplayer(id).pause(true);
					}
					this.loadedMetadata = true ;

					}.bind(this));


				/* désactivé pour les playlist pour l'instant
				jwplayer().onBufferChange(function(event){
					this.updateCurrentRange();
					this.displayBufferedRange();
					// document.getElementById('infobuff3').innerHTML = event.bufferPercent;
					}.bind(this));
				*/

				// control skin du bouton Mute dans certains cas ( video muted, retour au son via le slider ...)
				jwplayer(id).onMute(this.defaultControlVolumeChange.bind(this));

				// update de l'affichage en fonction de la progression de la lecture
				jwplayer(id).onTime(this.UItest.bind(this,id));

				// bufferisation dev infos
				jwplayer(id).onSeek(this.BlockSeeking.bind(this,id));
				this.track_media[index] = (document.getElementById(id));
				if(document.getElementById(id+"_wrapper").className.indexOf("wrappers") == -1 ){
					//MS test remplacement du toggle display par une regle qui ne réinitialise pas le problème
					// document.getElementById(id+"_wrapper").style.display = "none";
					document.getElementById(id+"_wrapper").style.position = "absolute";
					document.getElementById(id+"_wrapper").style.left = "3000px";
					//==

					document.getElementById(id+"_wrapper").className += "wrappers";
					document.getElementById(id).className += "plys";
				}
				jwplayer(id).pause(true);
				/*if(window.addEventListener){
					document.getElementById('ply_wrapper').addEventListener("click",function(){ if (this.fastSteppingOn != null ){this.canSeek = true ; }}.bind(this),false);
				}else{
					document.getElementById('ply_wrapper').attachEvent("onclick",function(){ if (this.fastSteppingOn != null ){this.canSeek = true ; }}.bind(this));
				}*/

				// initialisation du callback onMovieFinished sur le jwplayer.onComplete de la dernière tracks
				if(index == (this.track_media.length-1) && this.movie_options.onMovieFinished!=null && typeof(this.movie_options.onMovieFinished)=='function'){
					jwplayer(id).onComplete(function(){
						this.movie_options.onMovieFinished();
					}.bind(this));
				}

				this.track_init_events[index] = true;



				// Lancement changeTrack(0) à la fin de la dernière initialisation => plante le player sinon
				//if(id.substring(this.movie_options['name'].length) == (this.nb_tracks-1)){
				if(this.track_init_events.length == this.nb_tracks){
					test_all_track_init = true;
					for(i = 0 ; i<this.track_init_events.length;  i ++ ){
						test_all_track_init = test_all_track_init  && this.track_init_events[i];
					}
					if(test_all_track_init){
						if(id.substring(this.movie_options['name'].length) == (this.nb_tracks-1) && this.durationCurrentItem == null){
							this.durationCurrentItem = parseInt(this.duration) ;
							if(!this.simpleController){
							this.setTimeline();
							if(document.getElementById('timecodeDuration')){
								document.getElementById('timecodeDuration').innerHTML = this.DisplayTimeCode(this.duration);
							}
							}
						}

						this.currentPlyId = this.movie_options['name']+"0";
						// document.getElementById('infobuff').innerHTML += "<br /> call changetrack(0) : depuis initJsInteraction";
						this.ChangeTrack(0);
						if(this.autoplay){
							this.PlayTheVideo();
						}
					}
				}

				if(this.movie_options.mosaic_path != null  && this.movie_options.mosaic_path != ""  ){
					if(typeof this.movie_options.mosaic_pics_index == "string"){
						this.movie_options.mosaic_pics_index = this.movie_options.mosaic_pics_index.split(',');
					}

					if(this.movie_options.previewOverTimeline && document.getElementById("ow_prevOverContainer").className=="" && this.mediaType!="audio"){
						if((jwplayer().getWidth()/jwplayer().getWidth())>1.5 ){
							document.getElementById("ow_prevOverContainer").className="form_16_9";
						}else{
							document.getElementById("ow_prevOverContainer").className="form_4_3";
						}
					}

					this.setMosaicPath();
				}

			}
		}
		jwplayer(ply.id).onReady(initJsInteraction.bind(this,ply.id));

		/*this.capsulevideo.appendChild(track_media);*/
		// this.track_media.push(ply);
		this.track_init_events.push(false);
	}
}

OJW_player.prototype.OnEndTrack = function() {
	// console.log("onEndTrack, access "+this.timeline.value+0.2);
	this.currentTrackEnded = true ;
	this.timeline.setValue(this.timeline.value+0.2);

}
// Fonction qui joue le fichier suivant
OJW_player.prototype.PlayNext = function() {
	// console.log("call playnext");
	if((this.current_track+1) >=this.nb_tracks){
		this.StopTheVideo();
	}else{
		this.playNextFlag = true ;
		jwplayer(this.track_media[this.current_track+1].id).seek(this.track_start_offset[this.current_track+1]);
		this.ChangeTrack(this.current_track+1);
	}
}

// Fonction qui joue le fichier track
OJW_player.prototype.ChangeTrack = function(no_track) {
	if(no_track <this.nb_tracks && no_track >=0 && no_track!=this.current_track){
		var playState = false;
		var fastStepState = 0 ;
		var slowStepState = 0 ;
		var jogStepState = 0 ;
		var varSpeedState = 0 ;
		var volume = 1;
		var muteState = false ;

		if(document.getElementById(this.currentPlyId)){

			volume =  jwplayer(this.currentPlyId).getVolume();
			muteState = jwplayer(this.currentPlyId).getMute();


			if(this.fastSteppingOn || this.slowSteppingOn ){
				playState = false ;
			}else if (this.playNextFlag){
				playState = true ;
			}else {
				state = jwplayer(this.currentPlyId).getState();
				// console.log('state of player  : '+state);
				if( state == "PAUSED" || state == "IDLE" ){
					playState = false ;
				}else if (state == "PLAYING" || state == "BUFFERING"){
					playState = true ;
				}

			}
			// document.getElementById('infobuff').innerHTML =("playState : "+playState+" media.paused : "+this.media.paused+" playnextflag : "+this.playNextFlag+" fastSteppignOn : "+this.fastSteppingOn+" slowSteppingOn : "+this.slowSteppingOn);

			if(this.slowSteppingOn != null ){
				slowStepState = this.slowStepValue;
			}
			if(this.fastSteppingOn != null ){
				fastStepState = this.fastSteppingDirection;
			}


			this.StopTheVideo();
		}

		this.current_track=no_track;
		this.url=this.track_location[this.current_track];
		this.mediaType= this.mediaTypeDetection(this.url,"");
		this.BegTrack=this.track_start[this.current_track];
		this.EndTrack=this.track_end[this.current_track];
		// this.media=this.track_media[this.current_track];
		this.currentPlyId=this.track_media[this.current_track].id; // MSTEST


		for(i=0;i<this.nb_tracks;i++){
			if(i!=this.current_track){
				//MS test remplacement du toggle display par une regle qui ne réinitialise pas le problème
				document.getElementById(this.movie_options["name"]+i+"_wrapper").style.position = "absolute";
				document.getElementById(this.movie_options["name"]+i+"_wrapper").style.left = "3000px";
			}else{
				// alert(this.movie_options["name"]+i+"_wrapper");
				document.getElementById(this.movie_options["name"]+i+"_wrapper").style.left = "";
				document.getElementById(this.movie_options["name"]+i+"_wrapper").style.position = "relative";
			}
		}

		this.mediaFullyLoaded = false;
		this.stopUpdateRange = false;


		/*if(!playState && !fastStepState){
			setTimeout(this.StopTheVideo.bind(this),100);
		}

		if(this.playNextFlag && playState){
			setTimeout(this.PlayTheVideo.bind(this),10);
			this.playNextFlag = false ;
		}

		playState = false ;

		this.pauseAfterSeek = false ; */



		if(slowStepState != 0){
			this.InitStepping('frame',slowStepState);
			slowStepState = 0;
		}
		// console.log("fastStepState : "+fastStepState);
		if(fastStepState != 0){
			this.InitStepping('fast',fastStepState);
			fastStepState = 0;
		}

		if(this.jogIsUsed ){
			setTimeout(function(){
				this.ShuttleMove();
			}.bind(this),100);
		}

		if(varSpeedState !=0){
			setTimeout(this.varSpeedPlaying.bind(this,varSpeedState),10);
			varSpeedState=0;
		}

		if(this.media){
			 this.media.volume = volume ;
			this.media.muted = muteState ;
		}

		if(this.media && $('ow_controllerpanel') && this.resizeCustom){
			// this.resizeVideoKeepRatio();
			this.adjustSize();
		}

		if(this.movie_options.onTrackChange != null){
            this.movie_options.onTrackChange(no_track);
		}
		this.currentTrackEnded = false ;

		this.changeButtonPlay();

	}

}

// Fonction de stockage des seek points (les temps en secondes des keyFrames, où le player flash peut seek de façon précise)
// appelé via jwplayer().onMeta(),
// cas standard & playlist
OJW_player.prototype.initMediaSeekpoints = function(metadata,index) {
	if(typeof index == "undefined"){
		index = 0;
	}
	if(typeof this.mediaSeekpoints == "undefined"){
		this.mediaSeekpoints = new Array();
	}


	if(this.playlist){
		// console.log("call initmediaseekpoints , index : "+index+" ");
		if(typeof index == "undefined"){
			return ;
		}
		this.mediaSeekpoints[index] = new Array();
		for (i=0; i < metadata.seekpoints.length;i++){
			this.mediaSeekpoints[index].push((metadata.seekpoints[i].time+0.04));
		}
	}else{
		this.mediaSeekpoints = new Array();
		for (i=0; i < metadata.seekpoints.length;i++){
			this.mediaSeekpoints.push((metadata.seekpoints[i].time+0.04));
		}
	}

	/*
	meta = '' ;
	for (i=0; i<this.mediaSeekpoints.length ; i++){
		meta +=' , '+this.mediaSeekpoints[i];
	}

	document.getElementById('infobuff').innerHTML = meta ;
	*/

}

// Fonction qui retourne le temps en seconde du seekpoint précédent le plus proche de la valeur passée en argument.
OJW_player.prototype.getSeekpoint = function(value, index){
	// console.log("call getSeekPoint");
	if(!this.mediaSeekpoints){
		return value;
	}
	if(this.playlist){
		if(typeof index == "undefined"){
			return value;
		}
		i=0;
		// console.log("input :  value : " +value+" index"+index);
		while (i < this.mediaSeekpoints[index].length &&  this.mediaSeekpoints[index][i] <= value ){
			i++;
		}
		i-- ;
		// console.log("output : valeur : "+this.mediaSeekpoints[index][i]+" id : "+i);
		return (this.mediaSeekpoints[index][i]);

	}else{
		i=0;
		while (i < this.mediaSeekpoints.length &&  this.mediaSeekpoints[i] <= value ){
			i++;
		}
		i-- ;
		// alert('return corrected value : '+this.mediaSeekpoints[i]);
		return (this.mediaSeekpoints[i]);
	}
}




// Fonction qui renvoi le temps courant

OJW_player.prototype.GetCurrentTime = function(target) {
	if(target == null || typeof target == "undefined" ){
		target = jwplayer(this.currentPlyId).getPosition();
	}
	// console.log( "begtrack : "+this.BegTrack+" target : "+target);
	if(this.playlist) return (this.BegTrack  + target -this.track_start_offset[this.current_track]);
	else return target ;
}

// fonction permettant la destruction "propre" du player

OJW_player.prototype.destroy = function(){
	this.StopTheVideo();

	if(window.addEventListener){
		window.removeEventListener('resize',this.resizeListener,false);
	}else{
		window.detachEvent('onresize',this.resizeListener);
	}

	if (this.playlist){
		// console.log("call destroy playlist");
		for (i = 0 ; i< this.track_media.length ; i++){

			// document.getElementById("infobuff").innerHTML +=this.track_media[i].id;
			jwplayer(this.track_media[i].id).remove();
			// console.log(" destroy called : "+this.track_media[i].id);
		}


	}else{
		// console.log("call destroy simple");
		jwplayer().remove();
	}
}


// Fonctions définition d'extraits

OJW_player.prototype.showExtsButtons = function(){
	if(document.getElementById('ow_boutons_extraits') && !this.movie_options.extsBtnsDisabled){
		this.extsBtns = true;
		document.getElementById('ow_boutons_extraits').style.display = "";
	}
	if(this.movie_options.extsContext == "montage"){
		document.getElementById("ow_bouton_ext_save").style.display = "";
	}
	if(typeof $j != 'undefined' && !$j(this.container).hasClass("exts_btns_active")){
		$j(this.container).addClass("exts_btns_active");
	}
}
OJW_player.prototype.hideExtsButtons = function(){
	if(document.getElementById('ow_boutons_extraits')){
		this.extsBtns = false;
		document.getElementById('ow_boutons_extraits').style.display = "none";
	}
	if(typeof $j != 'undefined' && $j(this.container).hasClass("exts_btns_active")){
		$j(this.container).removeClass("exts_btns_active");
	}
}

OJW_player.prototype.extMarkTC= function(type) {
	TC=this.GetCurrentTime();
	if(this.movie_options.timecode_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}

    this.dontCallOnChange = true;
	if(type=="in" && this.BegSelect == this.EndSelect ){
		this.timelineSel.setValue(this.timelineSel.range.end,1);
		this.timelineSel.setValue(this.getSeekpoint(TC), 0);
	}else if (type=="in" && TC < this.EndSelect){
		this.timelineSel.setValue(this.getSeekpoint(TC), 0);
	}else if (type=="out" && TC>this.BegSelect){
		this.timelineSel.setValue(this.getSeekpoint(TC),1);
	}

	 this.dontCallOnChange = false;


	if (this.movie_options.onSelectionSliderChange)         {       this.movie_options.onSelectionSliderChange();   }
}

OJW_player.prototype.newExt = function(){
	if(typeof myPanel != "undefined"){
		myPanel.addExtrait();
	}
	if(this.selectionIsHidden){
		this.ShowSelectionSlider();
	}
	/* -- code prévu pour une rotation click "new" => définition d'ext => "save", rejeté pour l'instant
	if(document.getElementById("ow_bouton_ext_save") && document.getElementById("ow_bouton_ext_save").style.display =="none"){
		document.getElementById("ow_bouton_ext_save").style.display = "";
		document.getElementById("ow_bouton_ext_new").style.display = "none";
	}*/
}

OJW_player.prototype.saveExt = function(){
	if(this.movie_options.onExtSave){
		this.movie_options.onExtSave();
	}

	this.cancelExt();
}


OJW_player.prototype.cancelExt = function(){
	/* -- code prévu pour une rotation click "new" => définition d'ext => "save", rejeté pour l'instant
	if(document.getElementById("ow_bouton_ext_new") && document.getElementById("ow_bouton_ext_new").style.display =="none"){
		document.getElementById("ow_bouton_ext_new").style.display = "";
		document.getElementById("ow_bouton_ext_save").style.display = "none";
	}*/

	this.UnSetSelection();
	if(typeof myPanel != "undefined"){
		try{
		myPanel.selectExtrait();
		}catch(e){
			console.log("Erreur communication cancelExt visionneuse vers rightPanel");
		}
	}
}

//********************* FONCTIONS MOSAIC PREVIEW

OJW_player.prototype.setMosaicPath = function(){
	if(this.movie_options.mosaic_path && typeof this.movie_options.mosaic_path != "undefined"){
		document.getElementById("ow_img_prevOverContainer").style.backgroundImage = "";
		document.getElementById("ow_img_prevOverContainer").style.backgroundImage = "url('"+this.movie_options.mosaic_path+"')";
		this.sec_mosaic = 0;
	}

}

OJW_player.prototype.getPreviewPic = function(secs){
	if(document.getElementById("ow_img_prevOverContainer").style.backgroundImage && this.movie_options.mosaic_path ){
		pos = secs/this.movie_options.mosaic_step ; // on divise par la valeur du pas entre deux images de la mosaique
		pos = pos/10; // on divise par le nombre de vignette par ligne
		row = parseInt(pos,10);
		col = Math.floor((pos % 1)*10);
		// console.log("coordonnée image : "+row+" "+col);
		document.getElementById("ow_img_prevOverContainer").style.backgroundPosition = "-"+col*document.getElementById("ow_img_prevOverContainer").clientWidth+"px -"+row*document.getElementById("ow_img_prevOverContainer").clientHeight+"px";

		if(this.movie_options.tcMosaicExact ){
			offset = 0
			if(this.movie_options.timecode_IN){
				offset = (this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag));
			}
			this.sec_mosaic = col*this.movie_options.mosaic_step+row*10*this.movie_options.mosaic_step+offset;
			document.getElementById("ow_lab_prevOverContainer").innerHTML = this.DisplayTimeCode(this.sec_mosaic);
		}
	}
}

OJW_player.prototype.getPreviewPicFromIndex = function(secs){
	if(document.getElementById("ow_img_prevOverContainer").style.backgroundImage && this.movie_options.mosaic_path && this.movie_options.mosaic_pics_index != null){
		i = 0;
		index_pics = this.movie_options.mosaic_pics_index;
		offset=0;
		index_pics = this.movie_options.mosaic_pics_index;
		if(this.movie_options.timecode_IN){
			offset = (this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag));
		}
		var tc=this.secsToTimeCode(secs+offset);

		while (tc > index_pics[i] && i< index_pics.length){
			i++;
		}
		i--;
		row = parseInt(i/10,10);
		col = i-row*10;
		document.getElementById("ow_img_prevOverContainer").style.backgroundPosition = "-"+col*document.getElementById("ow_img_prevOverContainer").clientWidth+"px -"+row*document.getElementById("ow_img_prevOverContainer").clientHeight+"px";
		if(this.movie_options.tcMosaicExact ){
			/*offset = 0
			if(this.movie_options.timecode_IN){
				offset = (this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag));
			}*/
			this.sec_mosaic = this.timecodeToSecs(index_pics[i]);

			document.getElementById("ow_lab_prevOverContainer").innerHTML = this.DisplayTimeCode(this.sec_mosaic);
		}
	}
}






