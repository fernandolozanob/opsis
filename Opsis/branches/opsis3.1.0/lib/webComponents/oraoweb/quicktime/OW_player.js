
function voidFunc()
{
	//alert("test");
}


// Constructeur OW_player
function OW_player(container, player_options)
{
	///////********* BRICOLAGE ********////////////
	window.setTimeout("voidFunc()",5000);
	//appel fonction d'initialisation du player
	this.init(container,player_options)
}
	
	
	
//Fonction d'initialisation du OW_player
OW_player.prototype.init = function(container,player_options){
	// Detection browser (pur copié collé, voir comment le gerer avec vincent)============================
	// Detection browser ============================
	if(typeof player_options['OW_env_user'] == 'undefined'){
		this.BrowserDetect = {
		init: function () {
			this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
			this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
			this.OS = this.searchString(this.dataOS) || "an unknown OS";
		},
		searchString: function (data) {
			for (var i=0;i<data.length;i++)	{
				var dataString = data[i].string;
				var dataProp = data[i].prop;
				this.versionSearchString = data[i].versionSearch || data[i].identity;
				if (dataString) {
					if (dataString.indexOf(data[i].subString) != -1)
						return data[i].identity;
				}
				else if (dataProp)
					return data[i].identity;
			}
		},
		searchVersion: function (dataString) {
			var index = dataString.indexOf(this.versionSearchString);
			if (index == -1) return;
			return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
		},
		dataBrowser: [
					  {
					  string: navigator.userAgent,
					  subString: "Chrome",
					  identity: "Chrome"
					  },
					  { 	string: navigator.userAgent,
					  subString: "OmniWeb",
					  versionSearch: "OmniWeb/",
					  identity: "OmniWeb"
					  },
					  {
					  string: navigator.vendor,
					  subString: "Apple",
					  identity: "Safari",
					  versionSearch: "Version"
					  },
					  {
					  prop: window.opera,
					  identity: "Opera"
					  },
					  {
					  string: navigator.vendor,
					  subString: "iCab",
					  identity: "iCab"
					  },
					  {
					  string: navigator.vendor,
					  subString: "KDE",
					  identity: "Konqueror"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "Firefox",
					  identity: "Firefox"
					  },
					  {
					  string: navigator.vendor,
					  subString: "Camino",
					  identity: "Camino"
					  },
					  {		// for newer Netscapes (6+)
					  string: navigator.userAgent,
					  subString: "Netscape",
					  identity: "Netscape"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "MSIE",
					  identity: "Explorer",
					  versionSearch: "MSIE"
					  },
					  {// IE 11
					  string: navigator.userAgent,
					  subString: "Trident",
					  identity: "Explorer",
					  versionSearch: "rv"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "Gecko",
					  identity: "Mozilla",
					  versionSearch: "rv"
					  },
					  { 		// for older Netscapes (4-)
					  string: navigator.userAgent,
					  subString: "Mozilla",
					  identity: "Netscape",
					  versionSearch: "Mozilla"
					  }
					  ],
			dataOS : [
					  {
					  string: navigator.platform,
					  subString: "Win",
					  identity: "Windows"
					  },
					  {
					  string: navigator.platform,
					  subString: "Mac",
					  identity: "Mac"
					  },
					  {
					  string: navigator.userAgent,
					  subString: "iPhone",
					  identity: "iPhone/iPod"
					  },
					  {
					  string: navigator.platform,
					  subString: "Linux",
					  identity: "Linux"
					  }
					  ]
			
		};
		this.BrowserDetect.init();
		this.browser = this.BrowserDetect.browser;
		this.browserVersion = this.BrowserDetect.version;
		this.user_OS= this.BrowserDetect.OS;
	}else{
		this.browser = player_options['OW_env_user'].browser;
		this.browserVersion = player_options['OW_env_user'].browserVersion;
		this.user_OS= player_options['OW_env_user'].env_OS;
	}
	//====================================================================================================
	
	this.StepVideoMethod;
    this.shuttleRatioSteppPlaybackrate = null;
    
    this.plugin_status = '';
	
	//En fonction du browser on initialise différentes variables==========================================

	//if (this.browser == "Safari"){				// Si le browser est safari
		
		this.controlSeek = "Ranges";				// méthode de controle des seeks par les myBufferedRange
		this.initRange();							//méthode d'initialisation des ranges "maison"
		this.delayRangeSeek = 0;					// compteur pour delayer les seeks hors des ranges
		this.StepVideoMethod = this.LimitRangeStepVideo;	// attribution de la méthode de step correspondant aux spécificités du navigateur
		this.displayBufferedRangesDivs=[];			// Initialisation du tableau des divs représentants les ranges
		this.stopUpdateRange = false;				// Flag permettant de bloquer l'update des ranges
		this.reloading = false;						// Flag permettant de savoir si la vidéo est en train de faire un reload (cas du bug bufferisation safari
		this.mediaFullyLoaded = false;
		this.shuttleRatioSteppPlaybackrate = function () {
			if(this.jogspeed >=0.5 && this.jogspeed <= 2){
				return true;
			} else {
			return false;
			}
		}
		
	
	/*} else if (this.browser == "Chrome"){		// Si le browser est Chrome
		this.canSeek= true;
		
		this.controlSeek = "Events"					// méthode de controle des seeks en utilisant les events 
		this.retardSeek = 0;						// variable contenant le retard cumulé dû aux seeks sous chrome pour le stepping
		this.StepVideoMethod = this.LimitSeekStepVideo; // attribution de la méthode de step correspondant aux spécificités du navigateur
		this.allowAdjustTimelineOnResize = true;	// flag pour limiter l'execution parallele des fonctions gerant le resize de la fenetre 

		//Fonction qui autorise le seeking pour Chrome
		this.AllowSeeking = function() {
			this.canSeek = true ;
		}
		//Fonction qui interdit le seeking pour Chrome
		this.BlockSeeking = function() {
			this.canSeek = false ;
		}
   		this.shuttleRatioSteppPlaybackrate = function (){
            if(this.jogspeed >= 0){
                return true;
            } else {
                return false;
            }
        }
                

		
	
	}*/

	


//=====================variables membres=====================
	this.orig_container=container;
	this.canAdjustSize = true ;
	this.container = document.getElementById(container);							//div conteneur
	this.container.innerHTML="";
	var main_container=document.createElement('div');
	main_container.id='ow_main_container';
	this.container.appendChild(main_container);
	this.container = document.getElementById('ow_main_container');
	$(this.container.id).addClassName("containerNS");
	
	//Variables video
	this.media;																		//élément video (ou audio)
    this.mediaType = this.mediaTypeDetection(player_options.url,player_options.typeMime);
    
	this.erreur=false; // true en cas d'erreur
	
    this.rateWasChanged = false;
		
	this.fullscreen = false;
	
    this.useNativeFullscreen = true;                // Paramètre activant l'utilisation du fullscreen natif de Safari. 
    
	this.dontCallOnChange=false;
	
	this.fastSteppingOn = null;
    this.fastSteppingDirection = 0;
    this.slowSteppingOn = null;
    
    this.goingToEnd = false;
	
	this.time_line_did_resize=false;
    
	
	// variables timeline (en réalité deux sliders séparés)
	this.timeline;			// timeline 
	this.timelineSel;		// timeline de sélections
	
    
    
    
    
    this.ClickOnTimeline = false;   //flag permettant d'identifier la source d'un onChange timeline comme une action de l'utilisateur
	
    this.bugAfterRateChange = false;        // Flag permettant d'activer des tests une fois que le playbackrate a été modifié (ce qui cause un leger bug
                                            //      de retour a la keyframe la plus proche avant de lancer une lecture ) 
	this.periodicalUpdateRangeOn = null;    // Flag permettant d'identifier si un periodical executer est actuellement utilisé pour controler l'update des 
                                            //      ranges de chargements
	// variables jog
	this.jogshuttle;
	this.jogspeed = 0;
	this.jogIsUsed = false;
	
    
    
	
	//variable sound slider
	this.soundslider;
	
	// variables de selection
	this.BegSelect = 0;
	this.EndSelect = 0;
	this.selectionExists= false;
	this.SelSlide = false;

	//lecture en boucle
	this.loopVideo=false;
	
	
	
	this.movie_options = {
		//parametres principaux
		url : '',
		typeMime : "",
		name : "video",
		autoplay : false,
		language : 'fr',
		debug_mode : 'false',
		stepping_frequency : 0.10,
		
		fit_height_container : false,
		
		// largeur et hauteur de la video (nécessaire pour quicktime)
        
        selectionHidden : true,
		extsBtnsOnStart : false,
		//extsContext : "montage",
		extsBtnsDisabled : true ,
		
		refreshCalcOffsets : false,
		
		// Display timeline 
		hauteur_track :10,		// Hauteur en pixel de la track a definir via movie_options si custom
		hauteur_bg_track : 4,		// Hauteur en pixel du background / des divs de bufferisation
		span_time_passed : false,
		
		// TC
		timecode_lag : null  ,      // option d'offset
		timecode_IN :   null,
		timecode_OUT :   null,
		timecode_with_frame : false  ,
		width : null,
		height : null,
		
		//mode affichage : 
		//scale : 'aspect' ,			// on ne peut pas le parametrer en html 5 , la video garde toujours son ratio normal
		movie_background_color : 'black',
		
		//fonctions callback :
		onMovieFullScreen : null,
		onMovieEndFullScreen : null,
		onPlayerReady : null,
		onMovieFinished : null,
		onSelectionSliderChange : null,
		onTimecodeChange : null,
		onExtSave : null ,
		urlOfEndImage : '',
		
		displayLoopButton:false,
		
		
		//template : 
		code_controller :  '<!-- Controller Design -->														' +
		'       <div id="ow_id_progress_slider">	</div>				' +
		'       <div id="ow_first_row_controller">		' +
		'			<div id="ow_id_jog_slider"></div><p id="joginfo">0x</p> 		' +
		'      		<p id="timecodeDisplay">00:00:00:00</p>                                                             ' +
		'     		<div id="ow_boutons_extraits">				' +
						'<a class = "btn" id ="ow_bouton_ext_in"></a>' +
						'<a class = "btn" id="ow_bouton_ext_out"></a>' +
						'<a class = "btn" style="display:none;" id = "ow_bouton_ext_new"></a>' +
						'<a class = "btn" style="display:none;" id="ow_bouton_ext_save"></a>' +
						'<a class = "btn" id = "ow_bouton_ext_cancel"></a>' +
		'			</div>                                                         ' +
		'       </div>                                                         ' +
		'		 <div id = "ow_boutons">																			' +
		'		<div id = "containSound"><div id = "ow_id_sound_slider"></div></div>																	' + 
        '<a class = "btn" id = "ow_bouton_mute_sound"></a>'+
        '<a class = "btn" id = "ow_bouton_go_to_beginning"></a>' +
        '<a class = "btn" id = "ow_bouton_fast_rewind"></a>' +
        '<a class = "btn" id = "ow_bouton_step_rewind"></a>' +
        '<a class = "btn play" id = "ow_bouton_play_pause" ></a>' +
        '<a class = "btn" id = "ow_bouton_step_forward"></a>' +
        '<a class = "btn" id = "ow_bouton_fast_forward"></a>' +
        '<a class = "btn" id = "ow_bouton_go_to_end"></a>' +
        '<a class = "btn" id = "ow_bouton_fullscreen"></a>' +
		
		'<a class="btn" id="ow_bouton_loop"></a>'+
		
		
		'</div><div>																			' +
		//      Tous les éléments de class temp sont des éléments utilisés pour le dev
		'		<input type = "button" id = "boutonInfo" class = "temp" value = "infos">										' +
		'		<input type = "button" id = "boutonInfo2" class = "temp" value = "show">									' +
		'		<p class = "" id = "infobuff"> </p>													' +
		'		<p class = "" id = "infobuff2"> </p>													' +
		'</div>'  , 

        
		basic_function_button : {
			'ow_bouton_play_pause' : this.ToggleVideo.bind(this),
			'ow_bouton_mute_sound' : this.SetMuteState.bind(this),
			'ow_bouton_fullscreen' : this.ToggleFullScreen.bind(this),
			'ow_bouton_go_to_beginning' :  this.GoToBegAvailable.bind(this),
			'ow_bouton_go_to_end' :  this.GoToEndAvailable.bind(this),
			'ow_bouton_step_forward' : this.SlowPlaying(1),
			'ow_bouton_step_rewind' : this.SlowPlaying(-1),
			'ow_bouton_fast_rewind' :  this.FastPlaying(-1),                                             //this.InitStepping.bind(this,"fast",-1), (pour la vidéo uniquement, remplacé par une detection
			'ow_bouton_fast_forward' : this.FastPlaying(1),                                             //this.InitStepping.bind(this,"fast",1),   et une association en fonction du media)
			
			'ow_bouton_loop' : this.toggleLoopVideo.bind(this),
			'ow_bouton_ext_in' : this.extMarkTC.bind(this,"in"),
			'ow_bouton_ext_out' : this.extMarkTC.bind(this,"out"),
			'ow_bouton_ext_cancel' : this.cancelExt.bind(this),
			'ow_bouton_ext_new' : this.newExt.bind(this),
			'ow_bouton_ext_save' : this.saveExt.bind(this),
			
			// Boutons tests pour le développement
			'boutonInfo' : this.HideSelectionSlider.bind(this),
			'boutonInfo2' : this.ShowSelectionSlider.bind(this)
		},
			
		
		
		
		//tooltips
		tooltips_fr : {
			'ow_id_timecode' 				: 'Time Code (HH:mm:ss)',
			'ow_id_timecodeLong' 			: 'Time Code (HH:mm:ss.frame)',
			'ow_id_sound_slider'			: 'Volume sonore',
			'ow_id_progress_slider'			: 'Barre de progression',
			'ow_id_jog_slider'				: 'Jog Shuttle',
			'ow_bouton_play' 				: 'Lecture',
			'ow_bouton_pause' 				: 'Pause',
			'ow_bouton_play_pause' 			: 'Lecture ou Pause (suivant contexte)',
			'ow_bouton_play_again'			: 'Relance la visionneuse',
			'ow_bouton_fast_rewind' 		: 'Retour rapide',
			'ow_bouton_fast_forward' 		: 'Avance rapide',
			'ow_bouton_step_rewind'			: 'Reculer d\'une image',
			'ow_bouton_step_forward'		: 'Avancer d\'une image',
			'ow_bouton_go_to_beginning'		: 'Retourner au début du film',
			'ow_bouton_go_to_end'			: 'Aller a la fin',
			'ow_bouton_fullscreen'			: 'Active ou desactive le mode plein ecran',
			'ow_bouton_mute_sound' 			: 'Coupe le son',
			'ow_bouton_ext_in' 				: 'Marquer début extrait',
			'ow_bouton_ext_out'				: 'Marquer fin extrait',
			'ow_bouton_ext_cancel' 			: 'Annuler définition de l\'extrait ',
			'ow_bouton_ext_save' 			: 'Sauver l\'extrait',
			'ow_bouton_loop'				: 'Lecture en boucle'
		},
		
		tooltips_en : 	{	
			'ow_id_timecode' 				: 'Time Code (HH:mm:ss)',
			'ow_id_timecodeLong' 			: 'Time Code (HH:mm:ss.frame)',
			'ow_id_sound_slider'			: 'Sound volume',
			'ow_id_progress_slider'			: 'Progress bar',
			'ow_id_jog_slider'				: 'Jog Shuttle',
			'ow_bouton_play' 				: 'Play',
			'ow_bouton_pause' 				: 'Pause',
			'ow_bouton_play_pause' 			: 'Play or Pause (switch)',
			'ow_bouton_play_again'			: 'Restart the player',
			'ow_bouton_fast_rewind' 		: 'Fast rewind',
			'ow_bouton_fast_forward' 		: 'Fast forward',
			'ow_bouton_step_rewind'			: 'Back up one image',
			'ow_bouton_step_forward'		: 'Forward by one image',
			'ow_bouton_go_to_beginning'		: 'Go to the beginning',
			'ow_bouton_go_to_end'			: 'Go to the end',
			'ow_bouton_fullscreen'			: 'Toggle fullscreen',
			'ow_bouton_mute_sound' 			: 'Sound off',
			'ow_bouton_ext_in' 				: 'Mark beginning of selection',
			'ow_bouton_ext_out'				: 'Mark ending of selection',
			'ow_bouton_ext_cancel' 			: 'Cancel the current selection',
			'ow_bouton_ext_save' 			: 'Save the current selection',
			'ow_bouton_loop'				: 'Loop playing'
		},
		my_panel:null
	};
	
	
	// Permet d'update les paramètres par défaut à partir de l'objet player_options passé en argument 
	for (o in player_options){		// Pour chaque clé dans player_options
		if(this.movie_options.hasOwnProperty(o)){		// Si movie_options possède une même clé
			this.movie_options[o] = player_options[o];	// Mise a jour de movie_options à partir de player_options
		}
	}
	
	//Variables controller
	this.play_pause_btn;
	
	
	// MS 12/02/12
	// Variables activation des fonctions de style
	this.resizeCustom = player_options['resizeCustom'];
	this.verticalFixed = player_options['verticalFixed'];
	
	
	// Initialisation du player
	this.AttachPlayer();
	//if(this.mediaType == 'video'){
	//	this.defRatio(false);	// fonction qui determine le ratio largeur / hauteur d'un media (en th�orie)
	//}

}

	//Fonction d'initialisation du player 
OW_player.prototype.AttachPlayer = function(){
	this.erreur=false;
	
	if (this.movie_options["url"]=='')
		this.erreur=true;
	
	this.AttachMovieController();
	this.AttachMovie(this.movie_options["url"],this.movie_options["typeMime"]); 			//Initialisation de la video
	
	var tcUpdate = this.UItest.bind(this);
	this.thread_uitest=new PeriodicalExecuter(function(){tcUpdate();},0.5);
}		
	
OW_player.prototype.setError = function()
{
	this.erreur=true;
}
	
	// Fonction attribuant une vidéo au player à partir d'une URL et d'un type.
OW_player.prototype.AttachMovie = function(url,typeMime){
	if(document.getElementById('capsVid')){					//Si l'élément video existe déjà :
		this.container.removeChild(this.capsulevideo);				//		On le supprime.
	}
    
    
    if(this.mediaType == "audio" ){                 // Si le media est audio, on regle le test ShuttleMove en consequence (sur chrome, le playbackRate négatif est ignoré, ce n'est donc pas fonctionnel)
   		this.shuttleRatioSteppPlaybackrate =function (){
            if(this.jogspeed >=-10 && this.jogspeed <= 10) {
                return true;
            } else {
                return false;
            }
        }
    } 

	
	this.capsulevideo = document.createElement('div');
	this.capsulevideo.id = "capsVid";
	
	if (this.mediaType == "audio" ){
		this.capsulevideo.setAttribute('style','height : 0px; width : 100%');
		
	}
	// MS on set la hauteur minimum container pour éviter décalage du controller après load de la video
	if(this.movie_options['width'] && this.movie_options['height'] && !this.movie_options['fit_height_container']){
		this.container.style.minHeight = (this.movie_options['height']+this.controllerpanel.offsetHeight)+"px" ;
	}
	
	
	this.container.insertBefore(this.capsulevideo,this.controllerpanel);	// ajout de la capsule video au container

	
	//var bodyBGColor =window.getComputedStyle(document.body,"").getPropertyValue('background-color');
	//alert(bodyBGColor);
	
	// VP 17/08/2011 : test sur Explorer seulement
	//if (this.browser=='Explorer' || this.browser == "Safari")
	if (this.browser=='Explorer')
	{
		/*this.capsulevideo.innerHTML='<embed src="'+url+'" pluginspage="http://www.apple.com/quicktime/download/" postdomevents="True" enablejavascript="True" bgcolor="black" controller="False" autoplay="'+this.movie_options['autoplay']+'" scale="Aspect" id="'+this.movie_options["name"]+'emb" height="100%" width="100%" />'+
		
		//'<!--[if IE]><object id="qt_event_source" classid="clsid:CB927D12-4FF7-4a9e-A169-56E4B8A75598"></object><![endif]-->'+

		'<object width="100%" height="100%" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab"'+
			'id="qtmovie_object"  style="behavior:url(#qt_event_source);" >'+
			'<param name="src" value="'+url+'">'+
			'<param name="autoplay" value="'+this.movie_options['autoplay']+'">'+
			'<param name="controller" value="true">'+
			'<param name="postdomevents" value="true">'+
			'<param name="scale" value="aspect">'+
			'<param name="bgcolor" value="black">'+
			'<embed  id="'+this.movie_options["name"]+'emb" '+
			'postdomevents="true" '+
			'autoplay="'+this.movie_options['autoplay']+'" '+
			'width = "100%" '+
			'height = "100%" '+
			'controller = "false" '+
			'scale = "aspect" '+
			'bgcolor="black" '+
			'src="'+url+'" />'+
		'</object>'+
		'';*/
		
																					//BGCOLOR','black','WMODE','transparent'
		this.capsulevideo.innerHTML=QT_GenerateOBJECTText(url,'100%','100%','','TYPE',this.movie_options['typeMime'],'POSTDOMEVENTS','true','ENABLEJAVASCRIPT','true','BGCOLOR',"black",'CONTROLLER','false','AUTOPLAY', this.movie_options['autoplay'], 'SCALE', 'aspect', 'obj#id', this.movie_options["name"] , 'obj#name', this.movie_options["name"]+'emb');
	}
	else																				//BGCOLOR,'black','WMODE','transparent',
		this.capsulevideo.innerHTML=QT_GenerateOBJECTText(url,'100%','100%','','TYPE',this.movie_options['typeMime'],'POSTDOMEVENTS','true','ENABLEJAVASCRIPT','true','BGCOLOR',"black",'CONTROLLER','false','AUTOPLAY', this.movie_options['autoplay'], 'SCALE', 'aspect', 'emb#id', this.movie_options["name"]+'emb');
	

	this.media=this.getMediaObject();
// ======= Set des listeners propres à la video

	if (this.media.addEventListener) // on ajout les evenement pour tous les navigateurs
	{
		this.media.addEventListener("click", this.ToggleVideo.bind(this),false);
		//this.media.addEventListener("qt_durationchange",this.setTimeline.bind(this),false);
		this.media.addEventListener("qt_timechanged", this.UItest.bind(this),false);
		this.media.addEventListener("qt_play", this.changeButtonPlay.bind(this),false);
		this.media.addEventListener("qt_pause", this.changeButtonPlay.bind(this),false);
		this.media.addEventListener("qt_progress",this.testBuff.bind(this),false);
		//this.media.addEventListener("ratechange", this.rateHasChanged.bind(this), false);
		//this.media.addEventListener("qt_volumechange", this.defaultControlVolumeChange.bind(this), false);
		this.media.addEventListener("qt_ended",this.StopTheVideo.bind(this),false);
		this.media.addEventListener("qt_error",this.setError.bind(this),false);
		if(this.mediaType=='video'){
			//this.media.addEventListener("qt_begin", this.timedDefRatio.bind(this));
			this.defRatio();
		}
		
		
	}
	else // evenements pour IE
	{
		this.media.attachEvent("onclick", this.ToggleVideo.bind(this));
		//this.media.attachEvent("qt_durationchange",this.setTimeline.bind(this));
		this.media.attachEvent("onqt_timechanged", this.UItest.bind(this));
		this.media.attachEvent("onqt_play", this.changeButtonPlay.bind(this));
		this.media.attachEvent("onqt_pause", this.changeButtonPlay.bind(this));
		this.media.attachEvent("onqt_progress",this.testBuff.bind(this));//
		//this.media.attachEvent("ratechange", this.rateHasChanged.bind(this),);
		//this.media.attachEvent("qt_volumechange", this.defaultControlVolumeChange.bind(this));
		this.media.attachEvent("onqt_ended",this.StopTheVideo.bind(this));
		this.media.attachEvent("onqt_error",this.setError.bind(this));
		
		if(this.mediaType=='video'){
			this.defRatio();
			//this.media.attachEvent("qt_canplay", this.timedDefRatio.bind(this,true));
		//	this.media.onqt_canplay=this.timedDefRatio();
		}
		
		
	}
	
	
	if(this.mediaType == 'video' && this.resizeCustom){
		this.resizeListener = this.adjustSize.bind(this)
	}else if (this.mediaType == "audio"){
		this.resizeListener = this.adjustTimeline.bind(this)
	}
	if(typeof this.resizeListener != "undefined"){
		if(window.addEventListener){
			window.addEventListener('resize',this.resizeListener,false);
		}else{
			window.attachEvent('onresize',this.resizeListener);
		}
	}
	
	
	
	

	//this.mediaFullyLoaded = false;
	this.stopUpdateRange = false;
	//this.defRatio(true);

}

OW_player.prototype.getMediaObject = function ()
{	
	var obj=document.getElementById(this.movie_options["name"]);
	if (!obj)
		return document.getElementById(this.movie_options["name"]+'emb');
	else
		return document.getElementById(this.movie_options["name"]);
}

OW_player.prototype.AttachMovieController = function(){
	
	//Controller
	this.controllerpanel = document.createElement('div');					//création div controllerpanel
	this.controllerpanel.id = 'ow_controllerpanel';
	this.controllerpanel.innerHTML = this.movie_options.code_controller;	//update controllerpanel depuis le code_controller


	this.container.appendChild(this.controllerpanel);						//ajout du controllerpanel au container

	
	if(this.movie_options['autoplay']){
		document.getElementById('ow_bouton_play_pause').className = 'btn pause';
	}
 
	if (this.movie_options.displayLoopButton==false)
	{
		$j('#ow_bouton_loop').hide();
	}

	if(!this.movie_options.extsBtnsDisabled && (typeof myPanel != "undefined" || this.movie_options.extsBtnsOnStart)){
		this.showExtsButtons();
	}else{
		this.hideExtsButtons();
	}
	
	
	// creation des éléments internes aux sliders
	handlesound = document.createElement("div");
	handlesound.id = "ow_sound_handle";
	document.getElementById("ow_id_sound_slider").appendChild(handlesound);
	
	handlejog = document.createElement('div');
	handlejog.id = 'ow_id_jog_handle';
	document.getElementById("ow_id_jog_slider").appendChild(handlejog);
	
	backgroundTimeline = document.createElement('div');
	backgroundTimeline.id = "backgroundTimeline";
	
	progresstrack = document.createElement('div');
	progresstrack.id = "progress_track";
	
	progresshandle = document.createElement('div');
	progresshandle.id = "progress_handle";
	
	if(this.controlSeek == "Ranges"){
		bufferedDisplay = document.createElement('div');
		bufferedDisplay.id = "0bufferedDisplay";
		this.displayBufferedRangesDivs.push(bufferedDisplay);
		bufferedDisplay.className = "bufferedDisplayDivs";
		bufferedDisplay.style.width='0%';
	}
	
	progresstrack.appendChild(progresshandle);

    
	
	selection = document.createElement('div');
	selection.id = "selection";

	selecttrack = document.createElement('div');
	selecttrack.id = "select_track";
	
	handleBegSel = document.createElement('div');
	handleBegSel.id = "handle_BegSel";
	handleBegSel.className = "handles_select";
	handleEndSel = document.createElement('div');
	handleEndSel.id = "handle_EndSel";
	handleEndSel.className = "handles_select";
	
	selecttrack.appendChild(handleBegSel);
	selecttrack.appendChild(handleEndSel);
	
	ow_progress_slider = document.getElementById("ow_id_progress_slider");
	ow_progress_slider.appendChild(backgroundTimeline);
	
	if(this.movie_options.span_time_passed){
		spanpassed = document.createElement('div');
		spanpassed.id = "ow_time_passed";
		ow_progress_slider.appendChild(spanpassed);
	}
	
	ow_progress_slider.appendChild(progresstrack);
	
	if(this.controlSeek == "Ranges"){
        bufferedDisplayContainer = document.createElement('div');
        bufferedDisplayContainer.id = "bufferedDisplayContainer";
        bufferedDisplayContainer.style.marginTop = "-"+(this.movie_options['hauteur_track']/2+this.movie_options['hauteur_bg_track']/2)+"px";
	    ow_progress_slider.appendChild(bufferedDisplayContainer);
		bufferedDisplayContainer.appendChild(bufferedDisplay);
	}
	ow_progress_slider.appendChild(selection);
	ow_progress_slider.appendChild(selecttrack);

    progresstrack.style.marginTop = "-" + (this.movie_options['hauteur_bg_track'] + (this.movie_options['hauteur_track'] - this.movie_options['hauteur_bg_track'])/2)+"px";
  //alert((backgroundTimeline.clientHeight + (progresstrack.clientHeight - backgroundTimeline.clientHeight)/2));
	// init des sliders scriptaculous
	this.InitTimeline();
	this.InitJog();
	this.InitSoundSlider();

	
	// Association des handlers aux boutons existants en fonction de l'objet basic_function_button
	for (o in this.movie_options.basic_function_button){
		if(document.getElementById(o)){
			//document.getElementById(o).onmousedown = this.movie_options.basic_function_button[o];
			if (document.getElementById(o).addEventListener)
				document.getElementById(o).addEventListener('mousedown',this.movie_options.basic_function_button[o],false);
			else
				document.getElementById(o).attachEvent('onmousedown',this.movie_options.basic_function_button[o]);
		}
	}
	
	// MS neutralisation du drag n drop présent par défaut sur les images(meme les background) sur Firefox
	// necessaire pour le fonctionnement souhaité sur l'avance image par image
	if(this.browser == 'Firefox'){
		if(document.getElementById('ow_bouton_step_forward')){
			document.getElementById('ow_bouton_step_forward').addEventListener('mousedown', function(event){if(event.preventDefault){event.preventDefault();}},false);
		}
		if(document.getElementById('ow_bouton_step_rewind')){
			document.getElementById('ow_bouton_step_rewind').addEventListener('mousedown', function(event){if(event.preventDefault){event.preventDefault();}},false);
		}
	}
    
    
    if(this.mediaType == "audio"){
		if(document.getElementById('ow_bouton_fullscreen')){
			$("ow_bouton_fullscreen").addClassName("audioHide");
		}
    } 

    if (document.getElementById("ow_bouton_step_forward").addEventListener) // on ajout les evenement pour tous les navigateurs
	{
		document.getElementById("ow_bouton_step_forward").addEventListener('mousedown', function(){$("ow_bouton_step_forward").addClassName("ClickBouton");},false);
		document.getElementById("ow_bouton_step_rewind").addEventListener('mousedown', function(){$("ow_bouton_step_rewind").addClassName("ClickBouton");},false);

		if(document.getElementById('boutonInfo')){
			document.getElementById("boutonInfo").addEventListener('click', this.PrintInfos.bind(this),false);
		}
    }
	else // pour IE
	{
		document.getElementById("ow_bouton_step_forward").attachEvent('onmousedown', function(){$("ow_bouton_step_forward").addClassName("ClickBouton");});
		document.getElementById("ow_bouton_step_rewind").attachEvent('onmousedown', function(){$("ow_bouton_step_rewind").addClassName("ClickBouton");});
		if(document.getElementById('boutonInfo')){
			document.getElementById("boutonInfo").attachEvent('onclick', this.PrintInfos.bind(this));
		}

	}
	
	//this.adjustTimeline();
	
	
	if(this.movie_options.language == 'fr'){
		tooltips = this.movie_options.tooltips_fr;
	} else if (this.movie_options.language == 'en'){
		tooltips = this.movie_options.tooltips_en;
	}
	
	for (o in tooltips){
		if(document.getElementById(o)){
			document.getElementById(o).title = tooltips[o];
		}
	}

	
	
	if(this.mediaType == 'audio'){
		$('ow_controllerpanel').style.top ='0px ';
		$('ow_controllerpanel').style.bottom ='';
	}

	//**********************************************************
	//MAJ 19/01 (associations Onresize obsolete dans ce lecteur (normalement pas de cas possible pour safari ou chrome) 
	// Association de la fonction d'ajustement des timelines à l'event resize (à limiter si jamais la visionneuse a une taille fixe)
	//alert(this.browser);
	/*if(this.browser == "Chrome"){
		limitForChrome = function(){
			if(this.allowAdjustTimelineOnResize){
				this.allowAdjustTimelineOnResize=false;
				this.adjustTimeline();
				this.allowAdjustTimelineOnResize = true;
			}
		}
		window.onresize = limitForChrome.bind(this);
	} else {
		limitForSafari = function(){
			this.adjustTimeline();
			this.displayBufferedRange();
		}
		window.onresize = limitForSafari.bind(this);
	}*/
    
    
    
   //    if(this.verticalFixed){
//	this.verticalFixedBehavior();	// init du comportement fixed en vertical, absolute en horizontal pour le container 
 ///   }
    
    
    
    if(this.movie_options.selectionHidden){
        this.HideSelectionSlider();
        }
    
}









OW_player.prototype.rateHasChanged = function(){
    this.rateWasChanged = true;

    }



OW_player.prototype.toggleLoopVideo=function()
{
	if (this.loopVideo==false)
	{
		$('ow_bouton_loop').addClassName('activated');
		this.loopVideo=true;
	}
	else
	{
		$('ow_bouton_loop').removeClassName('activated');
		this.loopVideo=false;
	}
}



// Fonction utilisée tres (trop) souvent pour afficher les valeurs des différents attributs importants de la vidéo lors de la prog // pourra etre utile pour le debugage ?
OW_player.prototype.PrintInfos = function(){
   //alert(document.getElementById("progress_track").clientHeight);


}


//==========================================PLAY / PAUSE
// Fonction qui lance la lecture
OW_player.prototype.PlayTheVideo = function() {
	
	if (this.plugin_status != 'OK')
		return;
	
	if (this.fastSteppingOn!=null){				// Si avance/recul rapide 
		this.StopTheVideo();						// stoppe avant la reprise
	}
	
//	if (this.selectionExists &&(this.media.currentTime < this.BegSelect || this.media.currentTime>=this.EndSelect)){
	if(this.outOfSelectionTest(this.media.GetTime()/this.media.GetTimeScale())|| (this.selectionExists && Math.round((this.media.GetTime()/this.media.GetDuration())*100)/100 == Math.round(this.EndSelect*100)/100)){
		// si la selection existe et que l'on est hors des bornes de selection
		this.GoToBegSelection();				// on renvoi au début de la selection
	}
    
    if(this.rateWasChanged){
        if(Math.round((this.media.GetTime()/this.media.GetDuration())*100)/100 == Math.round(this.BegSelect*100)/100){
            this.bugAfterRateChange = true; 
            }
        }

	this.media.Play();			// lecture de la vidéo	
}


// Fonction qui met en pause la lecture (et stoppe avance/recul rapide)
OW_player.prototype.StopTheVideo = function() {
	if (this.plugin_status != 'OK')
		return;
	
	if(this.fastSteppingOn!= null){		// si on est en avance/recul rapide
		this.StopFastStepping();		// on stoppe la repetition des sauts
      	}
    if(this.slowSteppingOn != null){
        this.slowSteppingOn.stop();
        this.slowSteppingOn = null;
        }
    
    
    
	this.media.Stop();			// mise en pause de la video 
}
// Fonction qui assure le switch lecture / pause
OW_player.prototype.ToggleVideo = function(){
	if (this.plugin_status != 'OK')
		return;
	//if ((this.media.ended || this.media.paused) && this.fastSteppingOn == null){
	if (typeof(this.media.GetRate)!='undefined')
	{
		if (this.media.GetRate()==0 && this.fastSteppingOn == null){
			this.PlayTheVideo();
		} else {
			this.StopTheVideo();
		}
		
			
		if (this.browser=="Explorer" || (this.browser == "Firefox" && this.mediaType == 'audio'))//un bug de IE qui ne gere pas les evenement qt_play
		{													// MS - player Quicktime sous firefox avec un fichier audio ne lance pas les events "qt_pause" > trigger manuel
			this.changeButtonPlay();
		}
	}
}


//==========================================STEP BY STEP

OW_player.prototype.removeStepping = function ()
{	if (this.plugin_status != 'OK')
		return;
	
	if(this.slowSteppingOn != null)
	{
		this.slowSteppingOn.stop();
		this.slowSteppingOn = null;
	}
	$("ow_bouton_step_forward").removeClassName("ClickBouton");
	$("ow_bouton_step_rewind").removeClassName("ClickBouton");
}

// Init Steppings 
OW_player.prototype.InitStepping = function (styleOfStepping, stepvalue){
	if (this.plugin_status != 'OK')
		return;
	
	// def de variables (this ne fonctionne pas dans les PeriodicalExec)
	var self = this;
	var StepVideo = this.StepVideoMethod;					// Fonction d'étape correspondant au browser
	var freq = this.movie_options['stepping_frequency'];	// frequence de modification, passage seconde >> millisecondes
	var JogSteppingFilter = this.JogSteppingFilter;
	
    tempFastSteppingDirection = this.fastSteppingDirection;
	
	// Si la vidéo est en lecture ou si un fastStepping est en cours
	if (!(this.media.GetRate()==0) || this.fastSteppingOn != null){
		this.StopTheVideo();	// on arrete la video et le stepping
	}
	
	if (styleOfStepping=="frame"){
// ************FRAME BY FRAME******************	
		var sync = this.CalcSyncIfNeeded();
		StepVideo(stepvalue,self,sync);
		// initialisation éxecution périodique de la fonction step limitée
		this.slowSteppingOn = new PeriodicalExecuter(function(){StepVideo(stepvalue,self,sync);},0.50);
		var tempSlowStepp = this.slowSteppingOn;
		// init du listener qui stoppe l'execution periodique quand on lache la souris
		if(document.addEventListener){
			document.addEventListener("mouseup",this.removeStepping.bind(this),false);
		}else{
			document.attachEvent("onmouseup",this.removeStepping.bind(this));
			}
			

	} else if (styleOfStepping == "fast"){
// ************FAST FORWARD/REWIND**************	
		
        if(stepvalue > 0){
            this.fastSteppingDirection = 1;
        } else {
            this.fastSteppingDirection = -1;
        }
        if(this.fastSteppingDirection != tempFastSteppingDirection){
            if(this.fastSteppingDirection >0){
                $("ow_bouton_fast_forward").addClassName("ClickBouton");
            }else{
                $("ow_bouton_fast_rewind").addClassName("ClickBouton");
            }
            
            if(this.outOfSelectionTest(this.media.GetTime()/this.media.GetTimeScale())){
                this.GoToBegSelection(); // on se place au debut de la selection
            }
            this.fastSteppingOn = new PeriodicalExecuter(function(){StepVideo(stepvalue,self);},freq);
            this.changeButtonPlay();
        } else {
            this.fastSteppingDirection = 0;
            this.StopTheVideo();
            }

	
	} else if (styleOfStepping == "jog"){
//***************JOG STEPPING************************
		this.jogSteppingOn = new PeriodicalExecuter(function(){JogSteppingFilter(self,StepVideo);},freq);
	}
}

// Fonction permettant de stopper le stepping 
OW_player.prototype.StopFastStepping = function (){
	if (this.plugin_status != 'OK')
		return;
	// si le stepping est en cours
	if (this.fastSteppingOn != null){
        if(this.mediaType == "video"){
            this.fastSteppingOn.stop();	// on stop le stepping 
        }
        this.fastSteppingOn = null;	// on réinitialise le flag de stepping
        this.fastSteppingDirection = 0;
		this.changeButtonPlay();
		
        if(document.getElementById("ow_bouton_fast_forward").hasClassName("ClickBouton")){
            $("ow_bouton_fast_forward").removeClassName("ClickBouton");
        } else {
            $("ow_bouton_fast_rewind").removeClassName("ClickBouton");
        }
        
        
        
       // alert(this.dontCallOnChange+"  ");
		//****************************RANGES "MAISON"
		if(this.controlSeek == "Ranges"){		// Si on utilise la méthode des ranges
			this.changeRangeCheck(this.media.GetTime()/this.media.GetTimeScale());		// test de range (current ? création?)
            this.updateCurrentRange();
        }
	}
}


// Fonction permettant d'attributer la bonne fonction de vitesse variable aux boutons suivant le cas d'un lecteur video ou audio
OW_player.prototype.FastPlaying = function(direction) {
    
    if(this.mediaType == "video"){
        if(direction == 1){
            return this.InitStepping.bind(this,"fast",1);
        } else {
            return this.InitStepping.bind(this,"fast",-1);
        }
    } else {
        if(direction == 1 ){
            return this.varSpeedPlaying.bind(this,10);
        } else {
            return this.varSpeedPlaying.bind(this,-10);
        }
    }
    
}





// Fonction de vitesse variable utilisée pour le faststepping en mode audio
OW_player.prototype.varSpeedPlaying = function(playbackRate){
    
    if(this.media.GetRate()!=0 && this.media.playbackRate == playbackRate){
        this.StopTheVideo();
        if(playbackRate > 0){
            $("ow_bouton_fast_forward").removeClassName("ClickBouton");
        } else {   
            $("ow_bouton_fast_rewind").removeClassName("ClickBouton");
        }

        
    } else{
        this.StopTheVideo();
        this.media.playbackRate = playbackRate;

        this.PlayTheVideo();
		
		this.media.SetRate(playbackRate);

        this.fastSteppingOn = 1;
        if(playbackRate > 0){
            $("ow_bouton_fast_forward").addClassName("ClickBouton");
        } else {   
            $("ow_bouton_fast_rewind").addClassName("ClickBouton");
        }

    }
}
    
    
// Fonction permettant d'attributer la fonction de stepping aux boutons avec les bons parametres suivant le cas d'un lecteur video ou audio
OW_player.prototype.SlowPlaying = function(direction){
    if(this.mediaType == "video"){
        if(direction == 1){
            return this.InitStepping.bind(this,"frame",0.04);
        } else {
            return this.InitStepping.bind(this,"frame",-0.04);
        }
    } else {
        if(direction == 1 ){
            return this.InitStepping.bind(this,"frame",1);
        } else {
            return this.InitStepping.bind(this,"frame",-1);
        }
    }
}









// Fonction assurant l'avancée step by step de la stepvalue en argument et avec self = this (probleme de setInterval)
OW_player.prototype.LimitRangeStepVideo = function(stepvalue,self,sync) {

	//self.outOfBufferedRange(self.media.currentTime);
	if(!self.outOfCurrentRange(self.media.currentTime)){
		if (self == null){			// si aucune valeur self n'est rentré, 
			self = this;			// initialisation a this (pas possible lors d'un setInterval)
		}
		//self.timeline.setValue(self.timeline.value + stepvalue);		// saut dans la vidéo
		self.media.Step(stepvalue*25);
		if (sync != null){			// Si l'on doit synchroniser le deplacement avec un des curseurs de selection
			self.timelineSel.setValue(self.media.currentTime, sync);	// synchronisation (sync contient l'index du cvurseur a déplacer)
		}
	}else{
		if (self.delayRangeSeek<10){
			self.delayRangeSeek++;
		} else{
			//self.timeline.setValue(self.timeline.value + stepvalue*10);
			self.media.Step(10*stepvalue*25);
			self.delayRangeSeek = 0;
		}
		self.outOfBufferedRange(self.media.currentTime);

	}
}

// Fonction filtre selection pour le stepping jog
OW_player.prototype.JogSteppingFilter = function(self, StepVideo){
	if (!self.selectionExists || self.inTheSelectionTest(self.media.currentTime +(self.jogspeed/10))){
		StepVideo(self.jogspeed/10,self,null);
	} else{
		(self.media.currentTime+(self.jogspeed)/10) < Math.round(self.BegSelect*100)/100? self.GoToBegSelection() : self.GoToEndSelection(); 
	}
}

// Fonction assurant si besoin la synchronisation du stepping image par image avec les curseurs de selection
OW_player.prototype.CalcSyncIfNeeded = function(){
	var sync = null;
    if(this.selectionExists){
        if (Math.round(this.media.currentTime*100)/100 == Math.round(this.timelineSel.value*100)/100){
            sync = 0;
        } else if(Math.round(this.media.currentTime*100)/100 == Math.round(this.timelineSel.values[1]*100)/100){
            sync = 1;
        }
    }
	return sync;
}


// ====================== boutons aller début / aller fin 
// Fonction aller au début
OW_player.prototype.GoToBegAvailable = function (){
    this.StopTheVideo();
	if (this.selectionExists){			// Si on a une selection
		this.GoToBegSelection();			// aller au début de la selection
	}else{								// sinon
		this.timeline.setValue(this.timeline.range.start);			// aller au début de la vidéo
	}
}

// Fonction aller en fin
OW_player.prototype.GoToEndAvailable = function (){
    this.StopTheVideo();
	if (this.selectionExists){					// si la selection existe
		this.GoToEndSelection();					// aller en fin de selection
	}else{										// sinon
		this.timeline.setValue(this.timeline.range.end);// aller en fin de video
	}
}

//========================Jog/Shuttle
// Initialisation du jogshuttle
OW_player.prototype.InitJog = function(){
    this.resetjogIDtemp = this.ResetJog.bind(this);
	shuttleUpdate = function(value){
		//document.getElementById('mon_player_html').innerHTML += Math.round(this.jogspeed*100)/100+"x // ";	// actualisation de l'affichage de la vitesse de nav
		
		var lef_jog_button=((value+2)/4)*document.getElementById('ow_id_jog_slider').clientWidth-(document.getElementById('ow_id_jog_handle').clientWidth/2)
		//document.getElementById('ow_id_jog_handle').style.left=lef_jog_button+'px';
		this.jogshuttle.setValue(value);
		this.jogIsUsed = true;		// flag d'utilisation du jog à true
		this.CalcJogValue(value);							//  Calcul de la vitesse de lecture
		this.ShuttleMove();
	}
		
	shuttleChange = function(value){
        //alert("shuttlechange, currentValue =  "+this.jogshuttle.value+' // ' + this.jogIsUsed);
		if(value != 0 && this.jogIsUsed){		// si jog a été utilisé et que la valeur est differente de 0 
			this.ResetJog();		// on reset le jog
		} else {
			this.CalcJogValue(value);		// calcul de la vitesse de lecture
			if(value !=0)// si la valeur est differente de 0
			{
				this.jogIsUsed = true;	// flag d'utilisation du jog à true  
				this.ShuttleMove();

				if(document.addEventListener)
					document.addEventListener("mouseup",this.resetjogIDtemp,false);
				else
					document.attachEvent("onmouseup",this.resetjogIDtemp);
            }
		}

	}
	
	var delayInitJog = function(){		// MS on delai un peu l'initialisation du slider scriptaculous car si le css n'est pas encore récupéré au moment de l'init, le slider s'initialise sur une largeur de track fausse (1px) 
	this.jogshuttle = new Control.Slider("ow_id_jog_handle","ow_id_jog_slider",{
		range : $R(-2,2),

		onSlide : shuttleUpdate.bind(this),
		onChange : shuttleChange.bind(this)

	});
	
	
	this.jogshuttle.setValue(0);
	}
	
	setTimeout(delayInitJog.bind(this),1);
	
	
	
}

// calcul de la variable jogspeed à partir du jogshuttle
OW_player.prototype.CalcJogValue = function(value){
	if (value<-1.5){
		this.jogspeed = 15*value+20;
	} else if (value<-1){
		this.jogspeed = 2*value +1;
	} else if (value <= 1){
		this.jogspeed = value;
	} else if (value < 1.5){
		this.jogspeed = 2*value-1;
	} else {
		this.jogspeed = 15*value -20; 
	}
}

// Fonction effectuant les déplacements dans la video à partir de jogspeed
OW_player.prototype.ShuttleMove = function() {
// ******************* Implementation en playbackrate uniquement (ne fonctionne nulle part actuellement)
//	if(this.media.paused){
//		this.media.play();
//	}
//	if(!this.outOfSelectionTest()){
//		this.media.playbackRate = this.jogspeed;
//	} else {
//		this.media.playbackRate = 0;
//	}
//	document.getElementById('joginfo').innerText = Math.round(this.jogspeed*100)/100+"x";	// actualisation affichage vitesse de nav
	
	
	
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Implementation jogshuttle stepping & playbackRate<<<<<<<<<<<<<<<<<<<<<<<<
// *************************SAFARI ******************************
	/*if(this.shuttleRatioSteppPlaybackrate()){			// si valeur positive, navigation par vitesse de lecture
		this.StopFastStepping();
		if(this.jogSteppingOn !=null){
			this.jogSteppingOn.stop();
			this.jogSteppingOn = null;
		}
		if(this.media.GetRate()==0 ){		// si video n'est pas en lecture
			this.rateWasChanged = true;
				if(Math.round(this.media.currentTime*100)/100 == Math.round(this.BegSelect*100)/100){
					this.bugAfterRateChange = true; 
				}


			this.PlayTheVideo();			// on la lance
			this.jogIsUsed = true;

		}
		if  (Math.round(this.media.currentTime*100)/100!=Math.round(this.EndSelect*100)/100 || !this.selectionExists){
			this.media.playbackRate = this.jogspeed;	// vitesse de lecture = jogspeed
		} else {
			this.media.playbackRate = 0;
		}
    } else {					// si val négative, navigation par saut
		this.StopTheVideo();	// on stoppe la lecture 
		if(this.jogSteppingOn == null){		// si pas de stepping jog en cours
			this.InitStepping("jog",0);					// on en lance un 

			}
	}*/
	
	this.media.SetRate(this.jogspeed);
	
	document.getElementById("joginfo").style.visibility = "visible";

	document.getElementById('joginfo').innerHTML = ((Math.round(this.jogspeed*100))/100)+"x";	// actualisation affichage vitesse de nav

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}

// Fonction RaZ du jogShuttle lorsqu'on le relache
OW_player.prototype.ResetJog = function(){
	this.jogspeed = 0;

	this.jogshuttle.setValue(0);
   
  	this.jogIsUsed = false;

	this.media.playbackRate = "1";
	this.StopTheVideo();
	if(this.jogSteppingOn !=null){
		this.jogSteppingOn.stop();
		this.jogSteppingOn = null;
	}
	
	//************TEST RANGES "maison"
	if(this.controlSeek == "Ranges"){	// Si on utilise les ranges
		this.changeRangeCheck(this.media.currentTime);		// test de range (current ? creation ?)
	}
	
    if (document.removeEventListener)
		document.removeEventListener("mouseup", this.resetjogIDtemp, false);
	else
		document.detachEvent("onmouseup", this.resetjogIDtemp);

	document.getElementById("ow_id_jog_handle").onmouseup = null;
    document.getElementById("joginfo").style.visibility = "hidden";

}



//==========================================TIMELINE
// Fonction assurant la mise a jour du max de la barre quand la video est chargée
OW_player.prototype.setTimeline = function (){
	// Paramétrage des timeline de progression et de selection
		// parametrage des range max des timeline
	//*****************TEST TC IN TC OUT

	if(this.movie_options.timecode_IN){
		this.timeline.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);
		this.timelineSel.range.start = this.timecodeToSecs(this.movie_options.timecode_IN)-this.timecodeToSecs(this.movie_options.timecode_lag);
	}
	

	
		// reinitialisation des valeurs par défaut
	this.timeline.setValue(this.timeline.range.start);
	if(!this.selectionExists){
		this.timelineSel.setValue(this.timelineSel.range.start,0);
		this.timelineSel.setValue(this.timelineSel.range.start,1);
	} else {}
	
		// ajustement des timelines a la taille de la fenetre
	this.adjustTimeline();
}

// fonction permettant d'initialiser la timeline
OW_player.prototype.InitTimeline = function(){
	// TIMELINE PROGRESSION 
	// Fonction onSlide de la timeline progression 
	updateTimeline = function(value){
		if(this.outOfSelectionTest(value) && this.media.GetRate()!=0){
			this.StopTheVideo();
		}
		if(typeof(this.media.SetTime)!="undefined")
			this.media.SetTime(value*this.media.GetTimeScale());		// positionne la vidéo à la valeur du curseur
		if(this.controlSeek == "Ranges"){
			this.stopUpdateRange = true;
		}
	}
	// Fonction onChange de la timeline progression
	changeTimeline = function(value){
	
		if(this.outOfSelectionTest(value)){
			if(this.fastSteppingOn!= null || this.media.GetRate()!=0 ){
				if(!(this.rateWasChanged && this.bugAfterRateChange)){
					this.StopTheVideo();
					if(!this.ClickOnTimeline){
						if(this.loopVideo){
							this.GoToBegAvailable();
							this.dontCallOnChange = true;
							this.PlayTheVideo();
						}else{
							((value/this.timeline.range.end)*100)<this.BegSelect? this.GoToBegSelection() : this.GoToEndSelection();
							this.dontCallOnChange = true;
						}
					}
				}
			}
			
		} //&& this.media.currentTime < this.EndSelect
		
		
		if(this.controlSeek == "Ranges" && !this.mediaFullyLoaded && (!this.dontCallOnChange || this.stopUpdateRange)){
			//this.flagTestBugSafari = (this.myBuffered.currentIdx == this.myBuffered.ranges.length-1) && (this.myBuffered.ranges[this.myBuffered.currentIdx][1]>((this.media.GetDuration()/this.media.GetTimeScale())-1));
            //alert(this.flagTestBugSafari+"  "+ this.media.networkState + " "+ this.media.readyState);
            
            
			if(!this.SelSlide){
				this.changeRangeCheck(value);			// On effectue les tests de changements de ranges
			}
			this.displayBufferedRange();
		}
		if(!this.dontCallOnChange && typeof(this.media.SetTime)!="undefined"){
			//alert((value*this.media.GetDuration()) +' // '+ this.media.GetTime());
			this.media.SetTime(Math.floor(value*this.media.GetTimeScale()));
		}
		this.dontCallOnChange = false;
		if(this.controlSeek == "Ranges" && !this.mediaFullyLoaded && !this.SelSlide && !this.ClickOnTimeline && !this.goingToEnd){
			if(this.stopUpdateRange){
				this.stopUpdateRange = false;
				this.updateCurrentRange();
			} 
			this.testBugSafari();
		}
		this.ClickOnTimeline = false;
		
	}
	
	timelineIsClicked = function(){
    
		this.ClickOnTimeline = true ; 

	}
	
	if (document.getElementById("progress_track").addEventListener)
		document.getElementById("progress_track").addEventListener("mousedown", timelineIsClicked.bind(this),false);
	else
		document.getElementById("progress_track").attachEvent("onmousedown", timelineIsClicked.bind(this));
	//document.getElementById("progress_track").mousedown=timelineIsClicked.bind;

	if(this.movie_options.span_time_passed){
		span = "ow_time_passed";
	}else{
		span = 'none';
	}
	// création du slider timeline de progression
	this.timeline = new Control.Slider(["progress_handle"],"progress_track", {
        alignY: 10,
		range : $R(0,100),						// initialisation d'un range par défaut
		onSlide: updateTimeline.bind(this),		//association des évenements aux fonctions
		onChange: changeTimeline.bind(this),
		startSpan:span

	});
	
	
	// Fonction onSlide de la timeline selection
	slideSel = function (values)
	{		
		this.BegSelect = values[0];						// Mise a jour de la variable début de selection grâce aux valeurs des curseurs
		this.EndSelect = values[1];					// Mise a jour de la variable fin de selection grâce aux valeurs des curseurs 

		//alert(this.timeline.range.end*(values[this.timelineSel.activeHandleIdx]/100));

		this.timeline.setValue((this.timeline.range.end)*values[this.timelineSel.activeHandleIdx]/100);		// positionnement de la vidéo au niveau du curseur actif

		if(this.controlSeek == "Ranges")
		{
			this.SelSlide = true;
			this.stopUpdateRange = true;
		}

		if(values[0]==values[1])
		{
			if(document.getElementById("handle_BegSel").hasClassName('selected'))
			{
				this.timelineSel.activeHandle = document.getElementById("handle_EndSel");
				this.timelineSel.activeHandleIdx =1;
				$("handle_BegSel").removeClassName('selected');
				$("handle_EndSel").addClassName('selected');
			}
			else
			{
				this.timelineSel.activeHandle = document.getElementById("handle_BegSel");
				this.timelineSel.activeHandleIdx =0;
				$("handle_EndSel").removeClassName('selected');
				$("handle_BegSel").addClassName('selected');
			}
		}
		if(this.movie_options.onSelectionSliderChange != null)
		{
			this.movie_options.onSelectionSliderChange();
		}
		
		if (typeof(this.movie_options.my_panel)!='undefined' && this.movie_options.my_panel != null)
		{
			elt_tcin=getChildById(this.movie_options.my_panel.selectedRow,'tcin$');
			elt_tcout=getChildById(this.movie_options.my_panel.selectedRow,'tcout$');
			elt_tcin.value=this.secsToTimeCode(this.BegSelect/100*this.timeline.range.end);
			elt_tcout.value=this.secsToTimeCode(this.EndSelect/100*this.timeline.range.end);
		}
	}
	
	// Fonction onChange de la timeline selection
	changeSel = function(values){
		this.SelSlide = false;
		this.stopUpdateRange = false;

		//this.timeline.setValue(values[this.timelineSel.activeHandleIdx]); ???
		this.BegSelect = values[0];
		this.EndSelect = values[1];
		if(!this.dontCallOnChange){
			this.timeline.setValue(this.timeline.range.end*values[this.timelineSel.activeHandleIdx]/100);
		}
		if (this.BegSelect!=this.EndSelect){ this.selectionExists = true;}
		else{this.selectionExists = false; }
        
        
	}
	
	
	clickSelHandle = function (){
		if(this.timeline.value != this.timelineSel.values[this.timelineSel.activeHandleIdx]){
			this.StopTheVideo();
			this.timeline.setValue(this.timeline.range.end*this.timelineSel.values[this.timelineSel.activeHandleIdx]/100);
			// ou alors on test activeHandleIdx et on applique la méthode GoToBegSelection ou GoToEndSelection suivant le resultat
		}
	}
	
	
	// création du slider timeline de selection
	this.timelineSel = new Control.Slider(["handle_BegSel","handle_EndSel"],"select_track",{
		range : $R(0,100),				// initialisation d'un range par défaut
		spans : ["selection"],			// referencement du span permettant de colorer la selection
		restricted : true,				// pour maintenir les valeurs debut selection et fin selection cohérente
		sliderValue : [0,0],			// initialisation des valeurs des curseurs par défaut
		onSlide:slideSel.bind(this),	// association des évenements aux fonctions 
		onChange : changeSel.bind(this)
	});
	this.timelineSel.track.stopObserving("mousedown", this.timelineSel.eventMouseDown);
	document.getElementById("handle_BegSel").onclick = clickSelHandle.bind(this);
	document.getElementById("handle_EndSel").onclick = clickSelHandle.bind(this);
	

}


// fonction permettant d'ajuster la timeline lors d'un resize
OW_player.prototype.adjustTimeline = function(){
	var val=this.timeline.value;// enregistrement de la derniere valeur timeline progression
	var min=this.timeline.range.start;
	var max=this.timeline.range.end;
	var sel = this.timelineSel.values;		// enregistrement des dernieres valeurs timeline selection
	
    selectionIsHidden = this.movie_options.selectionHidden;

    if(selectionIsHidden){
        this.ShowSelectionSlider();
        }
        
	this.timeline.dispose();			// suppression de la timeline et de ses listeners
	this.timelineSel.dispose();
	

	this.InitTimeline();				// réinitialisation de la nouvelle timeline
	
	//*************************************
	//this.timeline.range.end = this.media.duration;		// réinitialisation du range de la timeline (hors de init, pour des raisons de chargement video)
	//this.timelineSel.range.end = this.media.duration;
	this.dontCallOnChange = true;		// flag empechant les instructions de l'évenement onChange (évite les saccades)
	this.timelineSel.setValue(sel[1],1);	// positionnement de la nouvelle timeline selection aux anciennes valeurs
	this.timelineSel.setValue(sel[0],0);
	
	this.timeline.range.start=min ;
	this.timeline.range.end=max ;
	this.timeline.setValue(val);		// positionnement de la nouvelle timeline progression à l'ancienne position
    if(selectionIsHidden){
        this.HideSelectionSlider();
        }


	//setTimeout(this.correctScrolls.bind(this),1);	// planification d'un test en cas de bug d'affichage dus à la place necessaire aux scrolls
}

// Fonction permettant de corriger l'éventuel probleme de la place reservée aux scrolls (Safari)
OW_player.prototype.correctScrolls = function(){
	val = this.timeline.value;			// enregistrement de l'ancienne valeur timeline	
	sel = this.timelineSel.values;
	// si la taille de la track de la timeline est differente de la taille effective du div timeline
	if (this.timeline.trackLength!= document.getElementById("ow_id_progress_slider").clientWidth){
		this.timeline.trackLength = document.getElementById("ow_id_progress_slider").clientWidth;	// mise a jour de la longueur timeline à partir de la taille effective du div
		this.timelineSel.trackLength = this.timeline.trackLength;
		
		this.dontCallOnChange = true;			// flag empechant les instructions de onChange (évite les saccades)
		this.timelineSel.setValue(sel[1],1);
		this.timelineSel.setValue(sel[0],0);
		this.timeline.setValue(val);			// mise a jour de la nouvelle timeline avec l'ancienne valeur enregistrée
		
		
	}
}





// TEST 19/01 Ensemble des modifs pour m�moire :
// On essai de laisser le CSS gerer la largeur et de ne modifier que la hauteur de la video, via l'utilisation du ratio . 
// On lance cette fonction sur un trigger et non plus constament (gourmand)
// On r�cupere le ratio une seule fois pour chaque video charg�e, ca evite l'appel r�current � GetRectangle.
// 



OW_player.prototype.ratioFromVideo = function(){

		try{		// MS 14/02/12 => si resize tres important (comme lors du switch fullscreen) quicktime reload la video, l'acces aux fonctions sur l'objet peut causer un plantage general, 
			 var size=this.media.GetRectangle();	// on try / catch donc les erreurs en rentrant des valeurs par défaut si besoin.
			 size=size.split(',');
			this.movie_options["width"]=parseInt(size[2])-parseInt(size[0]);
			this.movie_options["height"]=parseInt(size[3])-parseInt(size[1]);
		 }catch(err){
			/* la recuperation des width / height depuis la video directement a échoué*/
		 }
		 
		
		 this.defRatio();
		 
}


OW_player.prototype.defRatio= function(){
	var largeur_video;
	var hauteur_video;
	
	
	
	if(this.movie_options["width"] !=null && this.movie_options['height'] != null ){
		
		largeur_video = this.movie_options["width"];
		hauteur_video = this.movie_options['height'];
		
	}else{
		setTimeout(this.ratioFromVideo.bind(this),500);
	}
	
	this.largeur_video = largeur_video;
	this.hauteur_video = hauteur_video;
	
	this.ratioMediaHxL = hauteur_video/largeur_video;
	this.ratioMedia = largeur_video/hauteur_video;

	setTimeout(function(){this.adjustSize();}.bind(this), 10);
	
}




// Fonction application des modifications de temps de la video aux éléments d'affichage
OW_player.prototype.UItest = function(){
	try{
	if (typeof(this.media)=='undefined' ||  typeof(this.media.GetPluginStatus)=='undefined' || (this.media.GetPluginStatus()!='Playable' && this.media.GetPluginStatus()!='Complete'))
		return;
	else
		this.plugin_status = 'OK';
	}catch(e){
		return;
	}
	if (!this.time_line_did_resize)
	{
		this.adjustTimeline();
		this.time_line_did_resize=true;
	}

	if (this.erreur)
		this.thread_uitest.stop();

	this.dontCallOnChange=true;
    
	if(this.fastSteppingOn != null && this.fastSteppingDirection == -1 && this.media.currentTime == 00){
		this.StopTheVideo();
	}


	if(this.rateWasChanged && (Math.round((this.media.GetTime()/this.media.GetDuration())*100)/100 > Math.round(this.BegSelect*100)/100)){
		this.bugAfterRateChange = false;
	}
	

		this.timeline.setValue(this.media.GetTime()/this.media.GetTimeScale());
		this.timeline.range.end=this.media.GetDuration()/this.media.GetTimeScale();
	
	

		
	this.updateCurrentRange()

	document.getElementById('timecodeDisplay').innerHTML =  this.DisplayTimeCode(this.media.GetTime()/this.media.GetTimeScale());
	if (typeof(this.movie_options.onTimecodeChange) == "function"){ this.movie_options.onTimecodeChange(this.media.GetTime()/this.media.GetTimeScale());}
	else if (typeof(onTimecodeChange) == "function") onTimecodeChange(this.media.GetTime()/this.media.GetTimeScale());
	
}

// Fonction permettant d'avancer la vidéo jusqu'au pourcentage donné en parametre (A voir si le format est XX% ou 0,XX)
OW_player.prototype.GoToPercent= function(percent){
	if(percent <100){
		this.timeline.setValue(percent/100 * (this.media.GetDuration()/this.media.GetTimeScale())); //)/100
	} else if (percent == 100){
		this.timeline.setValue(this.media.GetDuration()/this.media.GetTimeScale()); // this.media.duration
	}
}


//====================================== Selection

// Fonction permettant de positionner la vidéo au début de la selection
OW_player.prototype.GoToBegSelection = function (){
	this.StopTheVideo();
    this.dontCallOnChange = false;
	//this.media.currentTime = Math.round(this.BegSelect*100)/100;
    this.timeline.setValue(this.timeline.range.end*this.BegSelect/100);
}

// Fonction permettant de positionner la video à la fin de la selection
OW_player.prototype.GoToEndSelection = function(){
	this.StopTheVideo();
    this.dontCallOnChange = false;

    this.timeline.setValue(this.timeline.range.end*this.EndSelect/100);
	//this.media.currentTime = Math.round(this.EndSelect*100)/100;
}






// Fonction permettant de parametrer la selection (A voir pour les questions de formats timecode / secondes)
OW_player.prototype.SetSelection = function(timecode_debut, timecode_fin){
    // Variables bornes de la selection en secondes
    tc_in = this.timecodeToSecs(timecode_debut)-this.timecodeToSecs(this.movie_options.timecode_lag);
    tc_out = this.timecodeToSecs(timecode_fin)-this.timecodeToSecs(this.movie_options.timecode_lag);
    switchAutreExtrait = false;


    // test de validité des valeurs en entrée (tc_debut < tc_fin) et correction
    if(this.timecodeToSecs(timecode_debut)>this.timecodeToSecs(timecode_fin)){
        tc_in = this.timecodeToSecs(timecode_fin)-this.timecodeToSecs(this.movie_options.timecode_lag);;
        tc_out = this.timecodeToSecs(timecode_debut)-this.timecodeToSecs(this.movie_options.timecode_lag);;
    }
    
//    if(tc_in != this.BegSelect && tc_out != this.EndSelect){
  //      switchAutreExtrait = true;
    //    }
    

    // Parametrage des variables timecode debut et fin de selection internes a la visionneuse
	
	// on passe les tc_in et tc_out en pourcentage
	tc_out = (tc_out/(this.media.GetDuration()/this.media.GetTimeScale()))*100;
	tc_in = (tc_in/(this.media.GetDuration()/this.media.GetTimeScale()))*100;
	
	// this.BegSelect = tc_in;
	// this.EndSelect = tc_out;
    // Mise a jour des curseurs de selection
    this.dontCallOnChange = true;
	if(tc_out<this.BegSelect){
		this.timelineSel.setValue(tc_in, 0);
		this.timelineSel.setValue(tc_out,1);
	}else{
		this.timelineSel.setValue(tc_out,1);
		this.timelineSel.setValue(tc_in, 0);
	}


    this.dontCallOnChange = false;
    
    
    
  //  if(switchAutreExtrait){
    // VP 14/01/2013 : suppression calcul hors sélection par arrondi, dans tous les cas on prend une marge d'une image (0.04 = 1/25)
    //      if(this.selectionExists && (Math.round((this.media.GetTime()/this.media.GetTimeScale())*100)/100<Math.round(this.BegSelect*100)/100-0.04 || Math.round((this.media.GetTime()/this.media.GetTimeScale())*100)/100>=Math.round(this.EndSelect*100)/100+0.04)){
    if(this.selectionExists && this.media.GetMaxTimeLoaded()/this.media.GetTimeScale() > this.BegSelect*this.timeline.range.end/100-0.04 && 
		(((this.media.GetTime()/this.media.GetTimeScale())<this.BegSelect*this.timeline.range.end/100-0.04) 
		|| ((this.media.GetTime()/this.media.GetTimeScale())>this.EndSelect*this.timeline.range.end/100+0.04))){
			
			this.timeline.setValue(this.timeline.range.end*this.BegSelect/100);
    }
    //}
          
	
	//if(this.movie_options.timecode_lag){
	//	this.BegSelect = this.BegSelect - this.timecodeToSecs(this.movie_options.timecode_lag);
	//	this.EndSelect = this.EndSelect - this.timecodeToSecs(this.movie_options.timecode_lag);
	//}
	
}

// Fonction permettant de supprimer le parametrage de selection (== mettre le debut et la fin a 0, inactif)
OW_player.prototype.UnSetSelection = function(){
    this.dontCallOnChange = true;
    this.timelineSel.setValue(0,0);
    this.timelineSel.setValue(0,1);
    this.dontCallOnChange = false;
	this.BegSelect = 0;
	this.EndSelect = 0;
}


// Fonction permettant de cacher la barre d'extrait (pour l'onglet description Opsis)
OW_player.prototype.HideSelectionSlider = function(){
    this.movie_options.selectionHidden = true;
    $("selection").addClassName("hideselection");
    $("select_track").addClassName("hideselection");
    document.getElementById("ow_id_progress_slider").style.height = "4px";
   // this.media.webkitEnterFullScreen();
    }
    
// Fonction permettant de réafficher la barre d'extrait (pour l'onglet extrait d'Opsis)
OW_player.prototype.ShowSelectionSlider = function(){
    this.movie_options.selectionHidden = false;
    $("selection").removeClassName("hideselection");
    $("select_track").removeClassName("hideselection");
    document.getElementById("ow_id_progress_slider").style.height = "8px";

    }
    


// Fonction qui retourne le timecode actuel au format long (+offset si nécessaire)

OW_player.prototype.GetCurrentLongTimeCode = function(){
    offset = 0;
    if(this.movie_options.timecode_lag){
        offset = this.timecodeToSecs(this.movie_options.timecode_lag);
    }

    return this.secsToTimeCode((this.media.GetTime()/this.media.GetTimeScale())+offset);
    }


OW_player.prototype.GoToLongTimeCode = function(timecode) {
    // MS 14/03/12 : prise en compte de l'offset
	var offset=0;
	if(this.movie_options.timecode_lag)
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);

    this.media.SetTime((this.timecodeToSecs(timecode)-offset)*this.media.GetTimeScale());
    }




OW_player.prototype.GetTCMax = function() {
    return this.secsToTimeCode(this.timecodeToSecs(this.movie_options.timecode_lag)+(this.media.GetDuration/this.media.GetTimeScale()));
    }





// Fonction qui retourne le début de la selection (A voir pour les questions de formats timecode / secondes)
OW_player.prototype.GetCurrentSelectionBeginning = function(){
	
	var offset = 0;
	if(this.movie_options.timecode_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}
	
	return this.secsToTimeCode((this.BegSelect/100*this.timeline.range.end)+offset);
}

// Fonction qui retourne la fin de la selection (A voir pour les questions de formats timecode / secondes)
OW_player.prototype.GetCurrentSelectionEnd = function(){
	
	var offset = 0;
	if(this.movie_options.timecode_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}

	
	return this.secsToTimeCode((this.EndSelect/100*this.timeline.range.end)+offset);
}

// Fonction permettant de tester si la selection existe et si on est en dehors (true si oui)
OW_player.prototype.outOfSelectionTest = function(valToCompare) {
	valToCompare=valToCompare/this.timeline.range.end*100
	if(this.selectionExists && (Math.round(valToCompare*100)/100<Math.round(this.BegSelect*100)/100-0.02 || Math.round(valToCompare*100)/100>Math.round(this.EndSelect*100)/100)){
		return true;
	} else{
		return false;
	}
}

OW_player.prototype.inTheSelectionTest = function(valToCompare) {
	if(this.selectionExists && (Math.round(valToCompare*100)/100>=Math.round(this.BegSelect*100)/100 && Math.round(valToCompare*100)/100<=Math.round(this.EndSelect*100)/100)){
		return true;
	} else{
		return false;
	}
}

// ====TIMELINE BUFFERISATION

OW_player.prototype.testBuff = function () {

	if(this.controlSeek == "Ranges" && this.fastSteppingOn == null && this.jogSteppingOn == null && !this.stopUpdateRange ){ 
		this.updateCurrentRange();
	}
	// lorsque la video charge, on actualise le range courant de myBuffered

	


}


//=========================================FULLSCREEN
//Fonction d'alternance window/fullscreen
OW_player.prototype.ToggleFullScreen = function() {
	if (!this.fullscreen){
		this.SwitchToFullScreen();
	}
	else{
		this.FromFullScreenToPoUp();
	}
}

// Fonction assurant le passage au mode fullscreen
OW_player.prototype.SwitchToFullScreen = function (){
	
	this.canAdjustSize = false ;  // flag bloquant l'execution du resize manuel (fait via css pr le FS ) ;

	// ajout de classes aux éléments afin de modifier leur affichage
	$(this.container.id).removeClassName("containerNS");
	
	// MS mise en overflow hidden, remontée en haut de la page
	if(document.documentElement.scrollTop !=0){
		document.documentElement.scrollTop = 0;
	}
	document.body.style.overflow = "hidden";
	
	// modifications pour maximiser le container dans la fenetre
	$('container').addClassName("containerFS");
	
	// MS changement de méthode de fullscreen, 
	// On affiche en le container en grand, et correctement placé(top : 0 left : 0 , width : width fenetre, height : height : fenetre)
	
	true_pos = this.calculateOffsetToMaximizeContainer();
	
	// On annule les offsets pour ramener le container en 0,0 sans utiliser "fixed"
	$('container').style.top = "-"+true_pos[1]+"px";
	$('container').style.left = "-"+true_pos[0]+"px";

	
	// calculs taille fenetre 
	window_width = window.innerWidth || document.documentElement.clientWidth;
	window_height = window.innerHeight || document.documentElement.clientHeight;
	$('container').style.width =window_width+"px";
	$('container').style.height =window_height+"px";

	document.getElementById('capsVid').addClassName("capsuleVideoFS");
	document.getElementById('ow_controllerpanel').addClassName("controllerpanelFS");
	
    if(this.browser =="Explorer"){
        document.getElementById('capsVid').style.height= document.body.offsetHeight+"px";
    }
    
	$('ow_controllerpanel').style.top =  $('capsVid').offsetHeight+10+"px"; //On parametre le top dans le cas resizeCustom = true pour le cas hauteur >> largeur.
	this.fullscreen = true;
	this.adjustSize();

	// ajustement de la timeline à la nouvelle taille disponible
	$("ow_id_progress_slider").style.overflow = "visible";		// Pour empecher un bug d'affichage du curseur de la timeline
	
}

// Fonction assurant le retour au mode window
OW_player.prototype.FromFullScreenToPoUp = function (){

	//document.body.removeClassName("hideover");
	

	this.canAdjustSize = true ;
	$('container').removeClassName("containerFS");
	$(this.container.id).addClassName("containerNS");
	
	
	document.getElementById('capsVid').removeClassName("capsuleVideoFS");
	document.getElementById('ow_controllerpanel').removeClassName("controllerpanelFS");

	this.fullscreen = false;
	document.body.style.overflow = "auto";
	$('container').style.top = "";
	$('container').style.left ="";
	$('container').style.width = "";
	$('container').style.height ="";
	if(this.mediaType == 'video'){
		this.adjustSize();
	}
}


//==========================================SOUND


OW_player.prototype.defaultControlVolumeChange = function(){
    if (this.media.GetVolume()!=0){
		if(document.getElementById('ow_bouton_mute_sound')){
			$("ow_bouton_mute_sound").removeClassName("ClickBouton");
        }
    }
    this.soundslider.setValue(this.media.GetVolume());
    }



// Fonction assurant la fonction mute 
OW_player.prototype.SetMuteState = function ()
{
	if(!this.media.GetMute())
	{
		$("ow_bouton_mute_sound").addClassName("ClickBouton");
		this.soundVolume = this.media.GetVolume();
		this.soundslider.setValue(0);
		this.media.SetMute(true);
	}
	else
	{
		$("ow_bouton_mute_sound").removeClassName("ClickBouton");
		this.soundslider.setValue(this.soundVolume);
		this.media.SetMute(false);
	}
}

//Fonction permettant de creer le slider de son
OW_player.prototype.InitSoundSlider = function() {
	
	updateSoundSlider = function(value){
		if(!this.media.GetMute() && value != 0)
		{
			this.media.SetMute(false);
			if(document.getElementById('ow_bouton_mute_sound')){
				$("ow_bouton_mute_sound").removeClassName("ClickBouton");
			}
		}
		this.media.SetVolume(value);
	}
	
	
	changeSoundSlider = function(value) {
		
		if(this.media.GetMute() && value != 0)
		{
			this.media.SetMute(false);
			if(document.getElementById('ow_bouton_mute_sound')){
				$("ow_bouton_mute_sound").removeClassName("ClickBouton");
			}
		}
        this.media.SetVolume(value);
	}
	
	
	this.soundslider = new Control.Slider("ow_sound_handle","ow_id_sound_slider",{
		range : $R(0,256),
		sliderValue : "256",
		onChange :  changeSoundSlider.bind(this) ,
		onSlide :	updateSoundSlider.bind(this)
		});
	
}
	
	

//=====================================SKINS BOUTONS


// Fonction gérant les modifications de l'apparence du bouton play/pause en changeant la classe de l'élément 
OW_player.prototype.changeButtonPlay = function(){
	// Le changement de skin du bouton play / pause se fait uniquement en changeant la classe de l'élément,
	// deux css sont définis pour ce bouton, un #ow_id_bouton_play_pause.play et un #ow_id_bouton_play_pause.pause.
	// Cela permet une plus grande flexibilité au niveau des choix graphiques (hover etc ...)
	// NB : les fonctionnalités sont gérés de façon totalement indépendante de l'affichage
	if (typeof(this.media.GetRate)!='undefined') {
		if (this.media.GetRate()==0 && this.fastSteppingOn == null){
			$("ow_bouton_play_pause").removeClassName("pause");
			$("ow_bouton_play_pause").addClassName("play");
		} else {
			$("ow_bouton_play_pause").removeClassName("play");
			$("ow_bouton_play_pause").addClassName("pause");
			
		}
	}
}

// ==================================RANGES 
OW_player.prototype.initRange = function(){
	//************************** Objet remplacant l'attribut .buffered des vidéos (qui ne fonctionne pas sur safari)
	//nom de l'objet
	this.myBuffered = {
		ranges : new Array([0,0]), // array contenant l'ensemble des ranges de video chargée
		currentRange : [],		
		currentIdx : 0			// int contenant l'index du range courant
	}
	this.myBuffered.currentRange = this.myBuffered.ranges[0];
	//************************************
}

//Fonction pour se reperer dans les ranges lorsqu'on change la timeline
OW_player.prototype.changeRangeCheck = function(value){
	// Si pas de stepping en cours et qu'on est hors des ranges actuels
	if( this.jogSteppingOn == null && this.fastSteppingOn == null&&this.outOfBufferedRange(value) ){
		//this.myBuffered.ranges.push([value,value]);						// Creation d'un nouveau range, valeurs débuts et fins = valeur actuelle. 
		this.myBuffered.currentRange = this.myBuffered.ranges[this.myBuffered.ranges.length-1];		// Le currentRange référence le nouveau range
		this.myBuffered.ranges.sort(this.sortRange);					// classement des ranges dans l'ordre chrono
		this.myBuffered.currentIdx = this.getCurrentRangeIdx();			// recuperation du nouvel index du range courant
	}
	
	this.BeginPeriodicalUpdateRanges();
	
}


//Fonction de test de la position par rapport au range courant : 
OW_player.prototype.outOfCurrentRange = function(valueToCompare){
	if(valueToCompare < this.myBuffered.currentRange[0] || valueToCompare > this.myBuffered.currentRange[1]){
		return true;
	} else{
		return false;
	}
}

	


// Fonction qui actualise le range courant 
OW_player.prototype.updateCurrentRange = function(){
	try{
	if(typeof(this.media.GetEndTime) != "undefined" && typeof(this.media.GetTimeScale)!="undefined" && (this.media.GetEndTime()/this.media.GetTimeScale())>= this.myBuffered.currentRange[1]){
		this.myBuffered.currentRange[1] = (this.media.GetEndTime()/this.media.GetTimeScale());		// Actualisation de la fin du range via video.buffered
	}
	}catch(err){}
	if(this.myBuffered.currentIdx < this.myBuffered.ranges.length-1){	// Si le range courant n'est pas le dernier,
		if (this.myBuffered.currentRange[1] >= this.myBuffered.ranges[this.myBuffered.currentIdx+1][0]){		 // si la fin du range courant est supérieure au début du range suivant 
			this.myBuffered.ranges[this.myBuffered.currentIdx][1] = this.myBuffered.ranges[this.myBuffered.currentIdx+1][1];	// val de fin range courant reçoit val de fin range suivant
			this.myBuffered.ranges.splice(this.myBuffered.currentIdx+1,1);                      // suppression du range suivant
		}
	}
    
 	this.displayBufferedRange();

    
	this.StopPeriodicalUpdateRanges();
	//document.getElementById("infobuff").innerHTML = this.myBuffered.ranges.length +" : "+ this.myBuffered.ranges.join("///");	// Info des ranges;
}

//Fonction permettant de compléter la méthodes de tri de l'array pour les ranges 
OW_player.prototype.sortRange = function(a,b){
	return a[0]-b[0];			// Permet de classer les ranges dans l'ordre chrono de leurs valeurs de début
}

// Fonction pour recuperer l'index du range courant
OW_player.prototype.getCurrentRangeIdx = function(){
	i = 0;
	while (this.myBuffered.currentRange[0] != this.myBuffered.ranges[i][0]){
		i++;
	}
	return i ;		// renvoi l'index du range courant;
}

// Fonction test hors des ranges déja créés
OW_player.prototype.outOfBufferedRange = function(value){
	for (i = 0 ; i< this.myBuffered.ranges.length; i ++){		// pour chaque range créé
		if (value >=this.myBuffered.ranges[i][0] && value <= this.myBuffered.ranges[i][1]){		// si value est bornée par les valeurs de début et de fin d'un range
			this.myBuffered.currentRange = this.myBuffered.ranges[i];								// actualisation du range courant
			this.myBuffered.currentIdx = i ;														// actualisation de l'index du range courant
			return false;																			// retourne false (on est dans un range déjà existant)
		}
	}
	return true;	// sinon, retourne true, on est hors des ranges existants
}

//Fonction qui gère l'affichage des zones bufferisées sous Safari
OW_player.prototype.displayBufferedRange=function(){
	// Si le nombre de divs représentant les ranges est différent du nombre de range (cas de jump ou de merge)

	if(this.displayBufferedRangesDivs.length != this.myBuffered.ranges.length)
	{
		// Si il y a moins de divs d'affichage que de ranges
		if(this.displayBufferedRangesDivs.length < this.myBuffered.ranges.length){
			//Création, paramétrage et stockage dans la liste d'un nouveau div
			newBufferedDisplay = document.createElement("div");
			newBufferedDisplay.id = this.displayBufferedRangesDivs.length+"bufferedDisplay";
			newBufferedDisplay.className = "bufferedDisplayDivs";
			newBufferedDisplay.style.marginTop = "-"+document.getElementById("backgroundTimeline").clientHeight+"px";
			newBufferedDisplay.style.width = "0%";
			document.getElementById("bufferedDisplayContainer").appendChild(newBufferedDisplay);
			this.displayBufferedRangesDivs.push(newBufferedDisplay);
		}else{
		//Si il y a plus de divs que de ranges (cas des merges de ranges)
			//On supprime le dernier div de la page et de la liste
			document.getElementById("bufferedDisplayContainer").removeChild(this.displayBufferedRangesDivs[this.displayBufferedRangesDivs.length-1]);
			this.displayBufferedRangesDivs.pop();
		}
		
		// Actualisation des divs existants, les affichages correspondent aux ranges de l'objet myBuffered
		for(i=0;i<this.displayBufferedRangesDivs.length;i++){	// Pour chaque div
			// On place le début du bloc à la position correspondant au début du range 
			
			if (!isNaN((this.myBuffered.currentRange[0]-this.timeline.range.start)*100/(this.timeline.range.end-this.timeline.range.start))) // pour eviter un bug NaN
			{
				document.getElementById(i+"bufferedDisplay").style.left = ((this.myBuffered.ranges[i][0]-this.timeline.range.start)*100/(this.timeline.range.end-this.timeline.range.start)) + "%";
				// On règle la largeur du bloc en fonction de la taille du range
				document.getElementById(i+"bufferedDisplay").style.width = ((this.media.GetMaxTimeLoaded()/this.media.GetTimeScale())*100/(this.timeline.range.end-this.timeline.range.start)) + "%";//(this.myBuffered.ranges[i][1]-this.myBuffered.ranges[i][0])
			}
		}
	}
	else
	{// Si le nombre de div == le nombre de ranges , on update le range courant
		if (typeof(this.media.GetMaxTimeLoaded)!='undefined' && typeof(this.media.GetTimeScale)!='undefined' && !isNaN((this.myBuffered.currentRange[0]-this.timeline.range.start)*100/(this.timeline.range.end-this.timeline.range.start)) && this.media!=null) // pour eviter un bug NaN
		{
			document.getElementById(this.myBuffered.currentIdx+'bufferedDisplay').style.width = ((this.media.GetMaxTimeLoaded()/this.media.GetTimeScale())*100/(this.timeline.range.end-this.timeline.range.start))+ "%";
			document.getElementById(this.myBuffered.currentIdx+'bufferedDisplay').style.left = ((this.myBuffered.currentRange[0]-this.timeline.range.start)*100/(this.timeline.range.end-this.timeline.range.start))+ "%";
		}
	}
}

//Fonction qui gère le lancement d'un periodical executer pour actualiser les divs quand on ne peut plus utiliser video.progress
OW_player.prototype.BeginPeriodicalUpdateRanges = function(){
	//if(this.periodicalUpdateRangeOn == null && (this.myBuffered.ranges[this.myBuffered.ranges.length -1][1] >= this.media.duration -1 )&& this.myBuffered.currentRange[1] < this.media.duration){
		var bufferUpdate = this.testBuff.bind(this);
		this.periodicalUpdateRangeOn = new PeriodicalExecuter(function(){bufferUpdate();},0.5);
	//}
}

//Fonction qui gere l'arret du periodical executer d'actualisation des divs
OW_player.prototype.StopPeriodicalUpdateRanges = function () {
	if(typeof(this.media.GetTimeScale) != "undefined" && this.periodicalUpdateRangeOn !=null && (this.myBuffered.currentRange[1]>= (this.media.GetDuration()/this.media.GetTimeScale())-1)){
		this.periodicalUpdateRangeOn.stop();
		this.periodicalUpdateRangeOn = null;
	}
}

//Fonction permettant de recharger la vidéo depuis le cache (pour correction bug safari)
OW_player.prototype.reloadVideoFromCacheSafari= function(){
	//		Cette fonction sert à recharger la vidéo depuis le cache sur Safari.
	//		Cela permet de régler le bug qui empeche la lecture dans certaines zones
	//		de la vidéo lors de sauts pendant la bufferisation.
	//		La méthode media.load() utilisée est ne chargera depuis le cache que si
	//		video.currentTime == 0 ce qui n'est pas tout à fait normal et sera peut être corrigé
	
	var valeur = this.media.GetTime()/this.media.GetTimeScale();		// On stocke la position actuelle
	this.timeline.setValue(0);					// set de la position actuelle à 0
	reloadVideo = function(){					// definition de la fonction dans une variable pour l'utiliser dans un setTimeout
		this.media.load();						// appel à la fonction load (déclenche l'algorithme de chargement du media, voir la spec HTML5)
		backToPos=function(){					// definition de la fonction de retour à la position précédente (pour utilisation comme event handler)
			this.resetRanges();					// reset des ranges
			this.updateCurrentRange();			// update des ranges apres reset (normalement recrée un range qui représente toute la vidéo si tout est OK)
			this.timeline.setValue(valeur);		// retour à la valeur initiale
			this.reloading = false;
		}

		
	//		La fonction backToPos doit être appelée une fois que la vidéo a été chargée, sinon les instructions sont ignorées
	//		On place donc la fonction de retour à la position initiale comme event handler de l'evenement media.onloadedmetadata (quand le document a connaissance de la durée totale de la vidéo etc...)
		if (this.media.addEventListener)
			this.media.addEventListener("loadedmetadata",backToPos.bind(this),false);
		else
			this.media.attachEvent("loadedmetadata",backToPos.bind(this));
	}
	
	//		L'appelle à la fonction reloadVideo est delayé pour que le seek vers 0 soit effectué et terminé avant 
	setTimeout(reloadVideo.bind(this),1);
}


	

// Fonction permettant de reset les ranges fabriqués lors du reload de la video (bug safari) ou du changement de vidéo.
OW_player.prototype.resetRanges = function(){
	this.stopUpdateRange = true;		// On bloque les updates des ranges
	this.initRange();					// On initialise à nouveau l'objet myBuffered
	for (i = 0 ; i < this.displayBufferedRangesDivs.length; i ++){		// Pour chaque div représentant les ranges	
		document.getElementById("bufferedDisplayContainer").removeChild(this.displayBufferedRangesDivs[i]);		// on les supprime du document
	}
	this.displayBufferedRangesDivs = [];		// On vide le tableau des divs
	this.stopUpdateRange = false ;				// On autorise à nouveau l'update des ranges
}

//Fonction de test du bug safari (video en cache mais lecture impossible)
OW_player.prototype.testBugSafari = function(){
	var testbug = function(){		// definition de la fonction dans une variable pour pouvoir l'utiliser dans un timeout
        if(this.flagTestBugSafari){		// Si flagTestBugSafari est vrai ( c a d si on se trouvait dans le dernier range et que celui ci avait bufferisé jusqu'à la fin de la vidéo)
			if(this.media.networkState <= 2 && this.media.readyState <= 2 && !this.reloading){		// Si networkState est NETWORK_LOADING et readyState est HAVE_CURRENT_METADATA
				
				
				//Si Safari version 4- >>> la lecture sera impossible dans certaines zones et certaines zones n'apparaissent pas comme chargée dans le default controller
				if(this.browserVersion <= 4){
					this.reloading = true;				// On signale qu'on est en train de reload la vidéo
					this.reloadVideoFromCacheSafari();	// appel à la fonction qui gère le reload rapide depuis le cache
					this.mediaFullyLoaded = true;		// flag indiquant que la vidéo est completement chargée à true
                   	if(this.timeoutTestBugSafari){
                        window.clearTimeout(this.timeoutTestBugSafari);
                        this.timeoutTestBugSafari;
                    }
				
				// Si Safari version >4  >>> la lecture est OK sur toute la vidéo mais l'affichage de la bufferisation bug encore dans certaines zones
				} else if(!this.mediaFullyLoaded){
					this.resetRanges();										// On reset les ranges persos
					this.myBuffered.currentRange[1] = this.media.GetDuration()/this.media.GetTimeScale();	// on update le range courant afin qu'il représente toute la vidéo
					this.displayBufferedRange();							// on affiche le nouveau range
					//document.getElementById("infobuff").innerText = this.myBuffered.ranges.length +" : "+ this.myBuffered.ranges.join("///");	// Info des ranges;
					this.stopUpdateRange=true;								// On empeche les éventuels updates des ranges
					this.mediaFullyLoaded = true;							// flag indiquant que la video est completement chargée à true
				}
			}
        }

    }
	

	// appel du test en setTimeout pour ne pas avoir de problèmes avec les network et ready states
	

	
	
	this.timeoutTestBugSafari=setTimeout(testbug.bind(this),1000);
}

//=====================================TOOLS
// fonction de passage d'une valeur en secs à une chaine format timecode
OW_player.prototype.secsToTimeCode = function(secs){

	var s = Math.floor(secs);
	var f=Math.round((secs-s)/0.04);
	
	//on calcule le nombre de minutes
	var m = Math.floor(s/60)
	s=s%60
	
	//on calcule le nombre d' heures
	var h= Math.floor(m/60);
	m=m%60;
	
	if (h<10)
		h='0'+h
	if (m<10)
		m='0'+m
	if (s<10)
		s='0'+s
	if (f<10)
		f='0'+f
	
	return h+":"+m+":"+s+":"+f;
}

// fonction de passage d'une chaine format timecode à une valeur en secs
OW_player.prototype.timecodeToSecs = function(timecode){
	if (timecode==null)
		return 0;

	arrayTC = timecode.split(":");
	h = parseFloat(arrayTC[0]);
	m = parseFloat(arrayTC[1]);
	s = parseFloat(arrayTC[2]);
	f = parseFloat(arrayTC[3]);
	
	return h*3600+m*60+s+f*0.04;
}


OW_player.prototype.DisplayTimeCode = function(secs){
	var offset = 0;
	if(this.movie_options.timecode_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}
	return this.secsToTimeCode(secs+offset);
}
	




// TESTS
// Fonction de detection du style de media (audio / video) en fonction du TypeMime lors de l'appel du player, sinon grâce à l'extension (switch case à completer eventuellement)
OW_player.prototype.mediaTypeDetection = function(url, typeMime){


    if(typeMime != ""){
        
        splits = typeMime.split("/");
        mediaType = splits[0];




        } else{
        
        splits = url.split(".");
        extension = splits[splits.length-1];
        
        switch (extension){
            case "mp4":
                mediaType = "video";
                break;
            case "m4v" : 
                mediaType = "video";
                break;
            case "mp3":
                mediaType = "audio";
                break;
            case "m4a" : 
                mediaType = "audio";
                break;
            }
        
        }
    
    return mediaType ; 
    
    }



// Fonction test de changement de vidéo en cours de lecture, fonctionne sous cette forme en théorie (peut être reset du controller à envisager (au moins des ranges)
OW_player.prototype.testLoadNewVideo = function (source){
	this.media.src = source;
	this.media.load();
    this.mediaFullyLoaded = false;
}


// cette fonction permet de calculer les offset négatifs à appliquer au container pour placer le player dans le coin haut gauceh de l'écran
OW_player.prototype.calculateOffsetToMaximizeContainer = function(){
	
	// MS workaround getComputedStyle pour IE8
	if (!window.getComputedStyle) {
		window.getComputedStyle = function(el, pseudo) {
			this.el = el;
			this.getPropertyValue = function(prop) {
				var re = /(\-([a-z]){1})/g;
				if (prop == 'float') prop = 'styleFloat';
				if (re.test(prop)) {
					prop = prop.replace(re, function () {
						return arguments[2].toUpperCase();
					});
				}
				return el.currentStyle[prop] ? el.currentStyle[prop] : null;
			}
			return this;
		}
	}
	
	
	function findPos(obj) {	// Fonction permettant de recupérer les offsets réels de l'élément container
		var curleft = curtop = 0;
		if (obj.offsetParent) {
		do {
			if(window.getComputedStyle(obj).getPropertyValue('position')!=""){
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
				}
			} while (obj = obj.offsetParent);
		}
		return [curleft,curtop];	
	}
	
	if(document.getElementById(this.orig_container) && document.getElementById(this.orig_container).offsetParent){
		if(document.getElementById(this.orig_container).offsetParent.nodeName != "BODY" && window.getComputedStyle(document.getElementById(this.orig_container)).getPropertyValue('position')!="relative" ){	// Si il existe parent décalant l'offset, 
			true_pos = findPos(document.getElementById(this.orig_container).offsetParent);	// On calcule les offsets réels de l'élément container 
		}else if( window.getComputedStyle(document.getElementById(this.orig_container)).getPropertyValue('position')=="relative"){	// Si il existe parent décalant l'offset, 
			true_pos = findPos(document.getElementById(this.orig_container));	// On calcule les offsets réels de l'élément container 
		}else{
			true_pos = [0,0];														// Sinon, on placera le container en 0,0
		}
	}else{
		true_pos=[0,0];
	}
	
	return true_pos;
}


OW_player.prototype.adjustSize = function(){		// NEWLAYOUTVISIO
	var wasplaying = false ;
	var ratioVid = Math.round(10000*this.ratioMedia)/10000;											// "120" est la hauteur fixe du controller pannel, a changé suivant le controller
	var ratioCapsVid = $('ow_main_container').offsetWidth/($('ow_main_container').offsetHeight-($('ow_controllerpanel').offsetHeight+20));//$('capsVid').offsetWidth / $('capsVid').offsetHeight; //  dans la paranthese 120+145
	ratioCapsVid = Math.round(10000*ratioCapsVid)/10000;
	var tempheight,tempwidth;
	

	if(this.fullscreen == true ){
		if(this.movie_options.refreshCalcOffsets){
			// A activer si les offsets left & top sont variable (typiquement site à largeur fixe centré au milieu de la page)
			true_pos = this.calculateOffsetToMaximizeContainer();
			$(this.orig_container).style.top = (parseInt($(this.orig_container).style.top,10) - true_pos[1])+"px";
			$(this.orig_container).style.left =  (parseInt($(this.orig_container).style.left,10) - true_pos[0])+"px";
		}
	
		window_width = window.innerWidth || document.documentElement.clientWidth;
		window_height = window.innerHeight || document.documentElement.clientHeight;
		$('container').style.width =window_width+"px";
		$('container').style.height =window_height+"px";
	}else{
		if(this.mediaType == "video"  && !this.movie_options['fit_height_container'] && !this.fullscreen){
			 this.container.style.minHeight = ((parseInt(document.getElementById(this.container.parentNode.id).offsetWidth)*this.movie_options['height']/this.movie_options['width'])+this.controllerpanel.offsetHeight)+"px";
		}
	}
	
	// si le ratioVid >= ratioCapsVid (cas hauteur >>> largeur)
	// MS Le cas hauteur > largeur ne doit pas être activé pour le mode FullScreen (sinon adaptation de la video à la largeur fenetre => dépasse largement le bas de la fenetre)
	if (!this.fullscreen && ratioVid >= ratioCapsVid){		
		//if (this.browser == "Explorer"){
			//$('capsVid').style.width 
			tempwidth = "100%";
		//	}
		//$('capsVid').style.height 
		tempheight=(($('ow_main_container').offsetWidth * this.hauteur_video)/ this.largeur_video)+"px";	// On regle la hauteur de la video 
		
		if(this.browser =="Firefox"){
			$('capsVid').setAttribute("style","height : "+tempheight+"; width : "+tempwidth+";");
		}else{
			$('capsVid').style.height = tempheight ;
			$('capsVid').style.width = tempwidth;
			
		}
		$('ow_controllerpanel').style.width = $('capsVid').offsetWidth +"px";	// on ajuste le controller panel a la meme largeur, et il est centré via css
		//$('ow_controllerpanel').style.top = ($('capsVid').offsetHeight)+"px";	
	// sinon cas largeur >>> hauteur 
	} else{	

											// "120" est la hauteur fixe du controller pannel, a changé eventuellement	
		//alert($('ow_main_container').offsetHeight)
		tempheight = ($('ow_main_container').offsetHeight-($('ow_controllerpanel').offsetHeight))+ "px";	// la hauteur de l'element video est limité a la hauteur de la capsvid $('ow_controllerpanel').offsetHeight
		tempwidth = ( parseInt(tempheight) * this.largeur_video / this.hauteur_video)+"px"	// le controller panel est set a 100% de la width de la video

		
		
		if(this.browser == "Firefox"){
			$('capsVid').setAttribute("style","height : "+tempheight+"; width : "+tempwidth+"; margin-left : auto ; margin-right : auto; left : 0px; right : 0px ; ");
		} else{
			$('capsVid').style.height = tempheight;
			$('capsVid').style.width = tempwidth;
			$('capsVid').style.marginLeft = "auto";
			$('capsVid').style.marginRight = "auto";
			$('capsVid').style.right = "0px";
			$('capsVid').style.left	 = "0px";			
		}
		
		$('ow_controllerpanel').style.width = (parseInt(tempheight) * this.largeur_video  /this.hauteur_video)+"px"	// le controller panel est set a 100% de la width de la video

	}

	

	// on reajuste l'empalcement du controller par rapport a l'endroit ou on est 
	$('ow_controllerpanel').style.top = (document.getElementById('capsVid').offsetHeight)+"px";		// optimisationResi ze

	this.adjustTimeline();
	
}



// MS 12/02/12
// Fonction permettant d'avoir le comportement fixed en scroll vertical (cas usuel : la visionneuse reste a l'écran autant que possible)
// tout en conservant un comportement absolute sur le scroll horiazontal
OW_player.prototype.verticalFixedBehavior = function () {
	var leftdefault = document.getElementById('container').offsetLeft;		// Initialisation des valeurs défaut en fonction de l'offset reglé
	var topdefault = document.getElementById('container').offsetTop;		// => devrait fonctionner qqsoit le placement du container 
	var rightdefault = parseInt(window.getComputedStyle($('container')).getPropertyValue('right')); // valeur du right parametré dans le css
	var tempTop=0;		// on garde en memoire le top courant, on ne le change que si on a scroll 
	var getScrollTop, getScrollLeft;
	
	if(this.browser == "Safari" || this.browser == "Chrome" ){
		getScrollTop = function(){return document.body.scrollTop;};
		getScrollLeft = function(){return document.body.scrollLeft;};
	} else if (this.browser =="Explorer" || this.browser == "Firefox"){
		getScrollTop = function(){return document.documentElement.scrollTop;};
		getScrollLeft = function(){return document.documentElement.scrollLeft;};
	}
	
	document.getElementById('container').style.position = "fixed";
	//document.body.scrollTop document.documentElement.scrollTop
	
	
	window.onscroll = function(){	// sur l'evenement scroll	
	
			//document.getElementById('container').style.top = (getScrollTop()+100)+"px";
		
	
	
		//if(getScrollTop() < topdefault-9){	// si on est en haut de la page (+/- taille entete)
				//document.getElementById('container').style.position = "absolute";	// on passe en mode absolute
				//document.getElementById('container').style.top = topdefault+"px";		// fixation de la video comme sur la demo
				//document.getElementById('container').style.left = leftdefault+"px";
				//document.getElementById('container').style.right = rightdefault+"px";

		//}else {						// Sur le reste de la page : 	
				


				//document.getElementById('container').style.position = "fixed";	// visionneuse fixed pour deplacement vertical
				//if(tempTop != getScrollTop()){		//On ne reinitialise la valeur que si on a changé de scroll vertical
				//	document.getElementById('container').style.top = "10px";	// calée en haut de la fenetre
				//}
				document.getElementById('container').style.left = (leftdefault-getScrollLeft()) +"px";	//  on parametre le left par rapport au décalage de la fenetre avec le doc
				document.getElementById('container').style.right = (rightdefault+getScrollLeft())+"px";
		//}


		//tempTop = getScrollTop();	// maj du scrollTop courant
		
	}	
}

// ========================================Fonctions définition d'extraits 
// pour l'instant encore copié collé rapide de la version HTML5, il faudra surement retester chacune de ces fonctionnalités avant mise en service sur QT
OW_player.prototype.showExtsButtons = function(){
	if(document.getElementById('ow_boutons_extraits') && !this.movie_options.extsBtnsDisabled){
		this.extsBtns = true;
		document.getElementById('ow_boutons_extraits').style.display = "";
	}
	if(this.movie_options.extsContext == "montage"){
		document.getElementById("ow_bouton_ext_save").style.display = "";
	}
}

OW_player.prototype.hideExtsButtons = function(){
	if(document.getElementById('ow_boutons_extraits')){
		this.extsBtns = false;
		document.getElementById('ow_boutons_extraits').style.display = "none";
	}
}

OW_player.prototype.extMarkTC= function(type) {
	TC=this.GetCurrentTime();
	if(this.movie_options.timecode_lag){
		offset = this.timecodeToSecs(this.movie_options.timecode_lag);
	}
	
    this.dontCallOnChange = true;

	if(type=="in" && this.BegSelect == this.EndSelect ){
		this.timelineSel.setValue(this.duration,1);
		this.timelineSel.setValue(TC, 0);
	}else if (type=="in" && TC < this.EndSelect){
		this.timelineSel.setValue(TC, 0);
	}else if (type=="out" && TC>this.BegSelect){
		this.timelineSel.setValue(TC,1);
	}
	
	 this.dontCallOnChange = false;

	
	if (this.movie_options.onSelectionSliderChange)         {       this.movie_options.onSelectionSliderChange();   }
}

OW_player.prototype.newExt = function(){
	if(typeof myPanel != "undefined"){
		myPanel.addExtrait();
	}
	if(this.selectionIsHidden){
		this.ShowSelectionSlider();
	}
	/* -- code prévu pour une rotation click "new" => définition d'ext => "save", rejeté pour l'instant
	if(document.getElementById("ow_bouton_ext_save") && document.getElementById("ow_bouton_ext_save").style.display =="none"){
		document.getElementById("ow_bouton_ext_save").style.display = "";
		document.getElementById("ow_bouton_ext_new").style.display = "none";
	}*/
}

OW_player.prototype.saveExt = function(){
	if(this.movie_options.onExtSave){
		this.movie_options.onExtSave();
	}
	this.cancelExt();
}


OW_player.prototype.cancelExt = function(){
	/* -- code prévu pour une rotation click "new" => définition d'ext => "save", rejeté pour l'instant
	if(document.getElementById("ow_bouton_ext_new") && document.getElementById("ow_bouton_ext_new").style.display =="none"){
		document.getElementById("ow_bouton_ext_new").style.display = "";
		document.getElementById("ow_bouton_ext_save").style.display = "none";
	}*/
	
	this.UnSetSelection();
	if(typeof myPanel != "undefined"){
		try{
		myPanel.selectExtrait();
		}catch(e){
			console.log("Erreur communication cancelExt visionneuse vers rightPanel");
		}
	}
}



