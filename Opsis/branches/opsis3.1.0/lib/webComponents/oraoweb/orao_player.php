<?php 
$id_lang= (!empty($_GET['id_lang']))? $_GET['id_lang'] : '';
?>
<script language="Javascript" type="text/javascript">

var _video_id;
var _visu_type = 'document';
var image_default;
var _video_width = 480;
// var _use_html = false ; 
// var _use_QT = false ; 

if(typeof _use_html == 'undefined'){
	var _use_html = true;
}
if(typeof _use_QT == 'undefined'){
	var _use_QT = true;
}
var _useGetVisioUrl = true;
var myVideo=null;
var _player_options={};
// var ua = navigator.userAgent.toLowerCase(); 
var lien_css=null;
// var ref_var  = null;
var _pattern = "_vis";
var allow_multiple_files_by_pattern = false ; 


if(typeof method_visu == 'undefined'){
	var method_visu ='QT';
}


function loadOraoPlayer(video_id, video_width, pattern, player_options, useGetVisioUrl, visu_type,ref_var_name) {	// MS ajout de visutype en argument pour les materiels sur demo
	
	if(typeof OW_env_user == 'undefined' && typeof detectEnv != 'object' ){
		var xmlhttp;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.open("GET","<?=libUrl?>webComponents/opsis/detectEnv.js",false);
		xmlhttp.send();
		eval(xmlhttp.responseText);
		OW_env_user = new detectEnv(true);
	}


	// permet de rediriger vers la version simple du player via le player options 
	if((typeof player_options != 'undefined' && player_options != null && typeof player_options['use_simple_player'] != 'undefined' && player_options['use_simple_player'])
	|| (typeof OW_env_user != 'undefined' && (OW_env_user.env_OS == 'iPhone'))){
		loadSimpleOraoPlayer(video_id, video_width, pattern,player_options,visu_type);
		return 1 ; 
	}
	_pattern = pattern;
	<? if (defined("gListePatternVisio")) { ?>
		if (_pattern == "gListePatternVisio") _pattern = <?=json_encode(array_keys(unserialize(gListePatternVisio)));?>;
	<? } else if (defined("gAllowMultipleVisFiles") && gAllowMultipleVisFiles){?>
		allow_multiple_files_by_pattern = true ; 
	<?}?>

	if(visu_type){		// test visu_type parametre >>> si il n'est pas défini on prend le default (document)
		_visu_type = visu_type;
	}

	if (video_id)
		_video_id = video_id;

	if (video_width)
		_video_width = video_width;


	if (useGetVisioUrl)
		_useGetVisioUrl = useGetVisioUrl;
	else
		_useGetVisioUrl = false;

	if (player_options)
		_player_options = player_options;
	else
		_player_options={};

	try{
		_player_options['OW_env_user'] = OW_env_user;
	}catch(e){
		console.log("fail passage OW_env_user dans player_options");
	}
	
	if(typeof player_options['visu_flag_decoup'] !='undefined' &&  player_options['visu_flag_decoup']){
		_visu_type += "&decoup="+player_options['visu_flag_decoup'];
	}
		
	addPrms = {};
	if(ref_var_name){
		addPrms['ref_var'] = ref_var_name;
	}
	
	if(typeof player_options['visu_flag_decoup'] !='undefined' ){
		if(typeof player_options['visu_flag_decoup'] == 'boolean'){
			player_options['visu_flag_decoup'] = player_options['visu_flag_decoup'].toString();
		}
		_visu_type += "&decoup="+player_options['visu_flag_decoup'];
	}
	
	if (typeof(hash)!= 'undefined' && hash!='') {
		str_hash = '&h='+hash;
	} else {
		str_hash = '';
	}
        
//       B.RAVI 2016-04-08 permet d'ajouter des parametres $_GET supplémentaires à prepareVisu.php
        var str_add_params='';
        for (var prop in player_options['addParamsToPHPScript']) {
//            console.log(prop+' '+typeof(player_options['addParamsToPHPScript'][prop]));
            if (typeof(player_options['addParamsToPHPScript'][prop])== 'string' && player_options['addParamsToPHPScript'][prop]!='') {
                str_add_params+='&'+prop+'='+player_options['addParamsToPHPScript'][prop];
            }
        }
        
	// NB 06 08 2015 langue du player pour affichage des tooltips dans la bonne langue notemment
	<?php if (isset($_SESSION['langue']) && $_SESSION['langue']!=''){?>
		player_options.language='<?=$_SESSION['langue']?>'.toLowerCase();
	<?php } ?>

	if(player_options &&  player_options['container_id'] && typeof $j != 'undefined'){
		image_default=$j('#'+player_options['container_id']+" #poster").attr('src');
		try{
			player_options['poster_width'] = $j('#'+player_options['container_id']+" #poster").width() ; 
			player_options['poster_height'] = $j('#'+player_options['container_id']+" #poster").height() ; 
		}catch(e){}
		$j('#'+player_options['container_id']+" #poster").attr('src','design/images/wait4video.gif');
		if($j('#'+player_options['container_id']+" #poster").css("visibility") == 'hidden'){
			$j('#'+player_options['container_id']+" #poster").css("visibility","");
		}
	}else if(_video_id && document.getElementById('poster')){
		image_default=document.getElementById('poster').src;
		
		player_options['poster_width'] = document.getElementById('poster').width ; 
		player_options['poster_height'] = document.getElementById('poster').height ; 
		
		$j('#'+player_options['container_id']+'.audio_poster').removeClass('audio_poster');
		
		document.getElementById('poster').src=''; // MS - 15.09.16 - on passe d'abord la source à "" car on utilise src=wait4video pour changer le css, mais la modif css arrive avant que l'image soit chargée, on vide donc l'image précédente avant de changer la src de façon à ce que la nouvelle regle css s'applique à une image vide le temps que l'image soit chargée. 
		document.getElementById('poster').src='design/images/wait4video.gif';
		if($j("#poster").css("visibility") == 'hidden'){
			$j("#poster").css("visibility","");
		}
	}
	if (_use_html && OW_env_user.techs.html5){
		// lien_css = document.createElement('link');
		if(player_options && player_options['css_path']){
			css_path = player_options['css_path']+"/ow-html5.css";
		}else{
			css_path = "<?=libUrl;?>webComponents/oraoweb/css/ow-html5.css";
		}
		
		// On clone le player_options dans les paramètres additionnels
		addPrms['player_options'] = $j.extend({},player_options);
		
		addPrms['player_options']['infos'] = {
			'id' : _video_id , 
			'visu_type' : _visu_type 
		}

		requirejs(['oraoweb/html5/OWH_player'],function(){
			requireCSS(css_path,function(){
				return !sendData('GET','empty.php','xmlhttp=1&urlaction=prepareVisu&method='+method_visu+'&action=visu&pattern='+pattern<?= (!empty($id_lang))?"+'&id_lang=".$id_lang."'":""; ?>+'&id='+_video_id+'&type='+_visu_type+str_hash+str_add_params,'displayVideoHTML',false,true,addPrms);
			});
		});
	}
	else if (_use_QT && OW_env_user.techs.qt){
	
		// lien_css = document.createElement('link'); 
		if( player_options && player_options['css_path']){
			css_path = player_options['css_path']+"/ow-quicktime.css";
		}else{
			css_path = "<?=libUrl;?>webComponents/oraoweb/css/ow-quicktime.css";
		}
		
		// On clone le player_options dans les paramètres additionnels
		addPrms['player_options'] = $j.extend({},player_options);
		addPrms['player_options']['infos'] = {
			'id' : _video_id , 
			'visu_type' : _visu_type 
		}
		requirejs(['oraoweb/quicktime/AC_QuickTime','oraoweb/quicktime/OW_player'],function(){
			requireCSS(css_path,function(){
				return !sendData('GET','empty.php','xmlhttp=1&urlaction=prepareVisu&method='+method_visu+'&action=visu&pattern='+pattern<?= (!empty($id_lang))?"+'&id_lang=".$id_lang."'":""; ?>+'&id='+_video_id+'&type='+_visu_type+str_hash+str_add_params,'displayVideoQT',false,true,addPrms);
			});
		});
	}
	else { // if( OW_env_user.techs.flash) // MS - on devrait check la présence de flash et ajouter un cas default expliquant qu'une des technos est necessaire (html5 / qt / flash )

		$j.getScript('<?=libUrl?>webComponents/oraoweb/flash/swfobject.js');
	
		if(player_options && player_options['css_path']){
			css_path = player_options['css_path']+"/ow-jwplayer.css";
		}else{
			css_path  = "<?=libUrl;?>webComponents/oraoweb/css/ow-jwplayer.css";
		}


		if(player_options && player_options['noControllerFlash'] && player_options['noControllerFlash']==true){
			player_options['code_controller'] = null;
		}

		// On clone le player_options dans les paramètres additionnels
		addPrms['player_options'] = $j.extend({},player_options);
		
		requirejs(['oraoweb/flash/jwplayer','oraoweb/flash/OJW_player'],function(){
			requireCSS(css_path,function(){
				return !sendData('GET','empty.php','xmlhttp=1&urlaction=prepareVisu&method='+method_visu+'&action=visu&pattern='+pattern<?= (!empty($id_lang))?"+'&id_lang=".$id_lang."'":""; ?>+'&id='+_video_id+'&type='+_visu_type+str_hash+str_add_params,'displayVideoFlash',false,true,addPrms);
			});
		});

	}
}

function loadSimpleOraoPlayer(video_id, video_width, pattern,player_options,visu_type)
{
	_pattern = pattern;
	<? if (defined("gListePatternVisio")) { ?>
		if (_pattern == "gListePatternVisio") _pattern = <?=json_encode(array_keys(unserialize(gListePatternVisio)));?>;
	<? } else if (defined("gAllowMultipleVisFiles") && gAllowMultipleVisFiles){?>
		allow_multiple_files_by_pattern = true ; 
	<?}?>
	
	if(visu_type){		// test visu_type parametre >>> si il n'est pas défini on prend le default (document)
		_visu_type = visu_type;
	}


	if (video_id)
		_video_id = video_id;

	if (video_width)
		_video_width = video_width;

	if (player_options)
		_player_options = player_options;
	else
		_player_options={};

	var addPrms = {};
		
	if (_use_html && OW_env_user.techs.html5)
	{
		// MS - 10.07.17 pas besoin d'updater les lien css si on affiche du displaySimpleVideoHTML étant donné qu'on utilise le controleur natif (ou pas)
		// Je laisse pour l'exemple si jamais on devait un jour ajouter un comportement similaire pour charger une version spéciale de la css destinée à ce cas précis (par ex ow-simple-html5.css);
		/*lien_css = document.createElement('link');
		if(player_options && player_options['css_path']){
			lien_css.href = player_options['css_path']+"/ow-html5.css";
		}else{
			lien_css.href = "<?=libUrl;?>webComponents/oraoweb/css/ow-html5.css";
		}		
		lien_css.rel = "stylesheet";
		lien_css.type = "text/css";
		already_included = false ; 
		$j("head link").each(function(idx,elt){
			if(($j(elt).get(0).href == lien_css.href)){
				already_included= true ; 
			}
		});
		if(!already_included){
			document.getElementsByTagName("head")[0].appendChild(lien_css);
		}*/
		// On clone le player_options dans les paramètres additionnels
		addPrms['player_options'] = $j.extend({},player_options);
		return !sendData('GET','empty.php','xmlhttp=1&urlaction=prepareVisu&method='+method_visu+'&action=visu&pattern='+_pattern+'&id='+_video_id+'&type='+_visu_type,'displaySimpleVideoHTML',false,true,addPrms);
	}
	else
	{
		// On clone le player_options dans les paramètres additionnels
		addPrms['player_options'] = $j.extend({},player_options);
		//affichage du player flash
		return !sendData('GET','empty.php','xmlhttp=1&urlaction=prepareVisu&method='+method_visu+'&action=visu&pattern='+_pattern+'&id='+_video_id+'&type='+_visu_type,'displaySimpleVideoFlash',false,true,addPrms);
	}
}

function displaySimpleVideoHTML(str,additionalParams)
{
	if (typeof(additionalParams)=='undefined') {
		additionalParams=null;
	}

	// Le player_options est maintenant cloné puis passé depuis loadOraoPlayer via additionalParams => permet d'éviter de modifications concurrentes du player_options
	if(typeof additionalParams == 'object' && additionalParams!=null &&  typeof additionalParams['player_options'] == 'object'){
		var local_player_options=additionalParams['player_options'];
	}else{
		var local_player_options=_player_options;
	}
	myDom=importXML(str);
	myBalise=myDom.getElementsByTagName('mediaurl');
	
	if (myBalise[0].childNodes.length!=0)
	{
		try {
			video_mediaurl = "";
			var videos_mediaurl = [];
			if (_pattern != null && typeof(_pattern) == "object") {
				for(var i = 0; i < _pattern.length; i++) {
					var obj = myBalise[0].getElementsByTagName(_pattern[i]);
					if (obj[0]) {
						videos_mediaurl[i] = obj[0].firstChild.nodeValue;
						if (video_mediaurl == "") video_mediaurl = obj[0].firstChild.nodeValue;
					}
					else delete _pattern[i];
				}
			}else if(allow_multiple_files_by_pattern && myBalise[0].childNodes.length>0){
				j=0;
				list_files = [];
				// console.log(myBalise[0].childNodes);
				for(var i = 0; i < myBalise[0].childNodes.length; i++) {
					if(myBalise[0].childNodes[i].nodeName!='#text'){
						list_files [j] = myBalise[0].childNodes[i].nodeName;
						videos_mediaurl[j] = myBalise[0].childNodes[i].childNodes[0].nodeValue;
						if (video_mediaurl == "") video_mediaurl =videos_mediaurl[0];
						j++;
					}
				}
			}
			if (video_mediaurl == "" && typeof videos_mediaurl[0] != 'undefined'){
				video_mediaurl = videos_mediaurl[0];
			}else if (video_mediaurl == ""){
				video_mediaurl=myBalise[0].firstChild.nodeValue;	
			}
		}
		catch (e) {
			video_mediaurl='';
		}
		myBalise=myDom.getElementsByTagName('width');
		my_width=myBalise[0].firstChild.nodeValue;
		myBalise=myDom.getElementsByTagName('height');
		my_height=myBalise[0].firstChild.nodeValue/my_width*_video_width;
	}
	else
	{
		video_mediaurl='';
		myBalise=myDom.getElementsByTagName('width');
		my_width=400;
		myBalise=myDom.getElementsByTagName('height');
		my_height=300/my_width*_video_width;
	}

	if (local_player_options['width'] > 0)
		_video_width = local_player_options['width'];
	if (local_player_options['height'] > 0)
		my_height = local_player_options['height'];

	if(local_player_options['muted']== true){
		muted=" muted='true' ";
	}else{
		muted='';
	}

	 var controls= " controls='controls' ";
	if(local_player_options['controls'] == false  ){
		controls='';
	}

	// var player_options=_player_options;

	var autoplay='';

	if (local_player_options['autoplay']==true)
		autoplay=' autoplay="autoplay" ';

	myBalise=myDom.getElementsByTagName('vignette');

	if(typeof local_player_options['image'] == 'undefined'){
		local_player_options['image']=image_default;
	}
	
	if(typeof local_player_options['res_vignette_width'] != 'undefined'){
		img_width = local_player_options['res_vignette_width'];
	}else if(typeof local_player_options['width'] != 'undefined'){
		img_width = local_player_options['width'];
	}else{
		img_width = 400 ; 
	}
	
	if (typeof local_player_options['image'] == 'undefined' && myBalise[0].firstChild!=null)
		local_player_options['image']='makeVignette.php?image='+myBalise[0].firstChild.nodeValue+'&w='+img_width+'&kr=1'; 
		// MS - si on est dans le cas du player simple, si on use le poster, on ne souhaite pas avoir l'overlay poster (&ol=player) car redondant avec affichage par défaut du controller html5
	
	if(local_player_options &&  local_player_options['container_id']){
		container_id = local_player_options['container_id'];
	}else{
		container_id ='container';
	}
	if(typeof local_player_options['preroll'] != 'undefined' && typeof local_player_options['preroll_url'] && local_player_options['preroll'] &&  local_player_options['preroll_url'] != ''){
		queueVideo = function(event) {
				video_elem = $j("#"+container_id+" video").get(0);
				video_elem.poster=null;
				video_elem.autoplay = 'true';
				video_elem.src=video_mediaurl;
				video_elem.removeEventListener('ended',queueVideo,false);
			}
		$j("#"+container_id).html('<video id="player_html_simple" style="background-color:black;" poster="'+local_player_options['image']+'" width="'+_video_width+'" '+muted+'height="'+my_height+'"'+controls+autoplay+'>'+
		'<source src="'+local_player_options['preroll_url']+'" />'+
		'</video>');
		$j("#"+container_id+" video").get(0).addEventListener('ended',queueVideo,false);
	}else{
		$j("#"+container_id).html('<video id="player_html_simple" style="background-color:black;" poster="'+local_player_options['image']+'" width="'+_video_width+'" '+muted+'height="'+my_height+'"'+controls+autoplay+'>'+
		'<source src="'+video_mediaurl+'" />'+
		'</video>');
	}
	if(typeof local_player_options['onPlayerReady'] == 'function' && $j("#"+container_id+" #player_html_simple").length>0){
		$j("#"+container_id+" #player_html_simple").get(0).addEventListener('loadedmetadata',function(){
			if(typeof local_player_options.onPlayerReady == 'function'){
				try{
					local_player_options.onPlayerReady();
				}catch(e){
					console.log("orao loadPlayerSimpleHTML - crash callback onPlayerReady :"+e);
				}
			}
		},false)
	}
	
	local_player_options['url']=video_mediaurl;
	local_player_options['player']='<?=libUrl?>webComponents/oraoweb/flash/player5-licensed.swf';
	local_player_options['width']=_video_width;
	local_player_options['height']=my_height;

	local_player_options['onSelectionSliderChange'] = function(){if(typeof myPanel != "undefined") myPanel.updateSelection();};
	finalizePlayer(local_player_options);

	//MS -17/04/2012 muted en attribut de <video> sur safari ne marche pas >> on regle par un timeout
	if(local_player_options['muted']==true){
		setTimeout(function(){document.getElementById('player_html_simple').muted=true; },1);
	}
}

function displaySimpleVideoFlash(str,additionalParams){
	if (typeof(additionalParams)=='undefined') {
		additionalParams=null;
	}
	
	if(_player_options &&  _player_options['container_id'] && typeof $j != 'undefined'){
		$j('#'+_player_options['container_id']+" #poster").css('display','none');
	}else if(document.getElementById('poster')){
		document.getElementById('poster').style.display='none';
	}
	myDom=importXML(str);

	myBalise=myDom.getElementsByTagName('mimetype');
	video_mimetype=myBalise[0].firstChild.nodeValue;

	try {
		myBalise=myDom.getElementsByTagName('mediaurl');
		video_mediaurl=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_mediaurl='';
	}

	try {
		myBalise=myDom.getElementsByTagName('width');
		my_width=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		my_width=0;
	}

	try {
		myBalise=myDom.getElementsByTagName('height');
		my_height=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		my_height=0;
	}


	var player_options=_player_options;

	player_options['url'] = video_mediaurl;

	player_options['typeMime']= video_mimetype;

	player_options['player']='<?=libUrl?>webComponents/oraoweb/flash/player5-licensed.swf';
	if (!player_options['width'] > 0)
		player_options['width']=_video_width;
	if (!player_options['height'] > 0)
		player_options['height']=(my_height/my_width)*_video_width;

	if (!player_options['controlbar'])
		player_options['controlbar']='bottom';

	player_options.simpleController = true;

	try {
		myBalise=myDom.getElementsByTagName('tcin');
		video_tcin=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_tcin='00:00:00:00';
	}

	try {
		myBalise=myDom.getElementsByTagName('tcout');
		video_tcout=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_tcout='00:00:00:00';
	}

	if(_player_options['muted']){
		player_options['muted']=_player_options['muted'];
	}

	if(typeof player_options['res_vignette_width'] != 'undefined'){
		img_width = player_options['res_vignette_width'];
	}else if(typeof player_options['width'] != 'undefined'){
		img_width = player_options['width'];
	}else{
		img_width = 400 ; 
	}
	
	myBalise=myDom.getElementsByTagName('vignette');
	if (myBalise[0].firstChild!=null)
		player_options['image']='makeVignette.php?image='+myBalise[0].firstChild.nodeValue+'&w='+img_width+'&kr=1';


	if(player_options &&  player_options['container_id']){
		container_id = player_options['container_id'];
	}else{
		container_id ='container';
	}

	player=new OJW_player(container_id,player_options);

	myVideo = player;
	if(typeof myPanel != 'undefined'){
		myPanel.video=myVideo;
		myPanel.defaultTCIN=video_tcin;
		myPanel.videoLength=video_tcout;
		if(myPanel.mode=="extrait" || myPanel.mode=="sequence" || myPanel.mode=="document") myVideo.ShowSelectionSlider();
	}
	else{
		if(typeof parent != "undefined" ){
			try{
				parent.myVideo=myVideo;
				if (typeof parent.myPanel != "undefined") {
					parent.myPanel.video=myVideo; 
					parent.myPanel.defaultTCIN=video_tcin; 
					parent.myPanel.videoLength=video_tcout; 
					if(parent.myPanel.mode=="extrait" || parent.myPanel.mode=="sequence" || parent.myPanel.mode=="document") myVideo.ShowSelectionSlider();
				}
			}catch(e){
				console.log("crash alimentation données player frame parent : ");
				console.log(e);
			}
		}
	}

	finalizePlayer(player_options);
}


function displayVideoFlash(str,additionalParams) {
	if (typeof(additionalParams)=='undefined') {
		additionalParams=null;
	}
	
	if(_player_options &&  _player_options['container_id'] && typeof $j != 'undefined'){
		$j('#'+_player_options['container_id']+" #poster").css('display','none');
	}else if(document.getElementById('poster')){
		document.getElementById('poster').style.display='none';
	}
	myDom=importXML(str);

	try {
		myBalise=myDom.getElementsByTagName('mimetype');
		video_mimetype = "";
		var videos_mimetype = [];
		if (_pattern != null && typeof(_pattern) == "object") {
			for(var i = 0; i < _pattern.length; i++) {
				var obj = myBalise[0].getElementsByTagName(_pattern[i]);
				if (obj[0]) {
					videos_mimetype[i] = obj[0].firstChild.nodeValue;
					if (video_mimetype == "") video_mimetype = obj[0].firstChild.nodeValue;
				}
			}
		}
		if (video_mimetype == "") video_mimetype=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_mimetype='';
	}

	try {
		myBalise=myDom.getElementsByTagName('mediaurl');
		video_mediaurl = "";
		var videos_mediaurl = [];
		if (_pattern != null && typeof(_pattern) == "object") {
			for(var i = 0; i < _pattern.length; i++) {
				var obj = myBalise[0].getElementsByTagName(_pattern[i]);
				if (obj[0]) {
					videos_mediaurl[i] = obj[0].firstChild.nodeValue;
					if (video_mediaurl == "") video_mediaurl = obj[0].firstChild.nodeValue;
				}
				else delete _pattern[i];
			}
		}else if(allow_multiple_files_by_pattern && myBalise[0].childNodes.length>0){
			j=0;
			list_files = [];
			// console.log(myBalise[0].childNodes);
			for(var i = 0; i < myBalise[0].childNodes.length; i++) {
				if(myBalise[0].childNodes[i].nodeName!='#text'){
					list_files [j] = myBalise[0].childNodes[i].nodeName;
					videos_mediaurl[j] = myBalise[0].childNodes[i].childNodes[0].nodeValue;
					if (video_mediaurl == "") video_mediaurl =videos_mediaurl[0];
					j++;
				}
			}
		}
		if (video_mediaurl == "") video_mediaurl=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		console.log(e);
		video_mediaurl='';
	}

	try {
		myBalise=myDom.getElementsByTagName('width');
		my_width=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		my_width=0;
	}

	try {
		myBalise=myDom.getElementsByTagName('height');
		my_height=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		my_height=0;
	}

	try {
		myBalise=myDom.getElementsByTagName('playlist');
		xml_playlist=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		xml_playlist='';
	}


		//mosaic
	try {
		myBalise=myDom.getElementsByTagName('mosaic_path');
		mosaic_path=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		mosaic_path='';
	}
	try {
		myBalise=myDom.getElementsByTagName('mosaic_pics_index');
		mosaic_pics_index=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		mosaic_pics_index='';
	}
	
	try {
		myBalise=myDom.getElementsByTagName('waveform_path');
		waveform_path=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		waveform_path='';
	}

	try {
		myBalise=myDom.getElementsByTagName('subtitles');
		if(typeof myBalise[0] != 'undefined' && myBalise[0] != null ){
			if( typeof myBalise[0].outerHTML == 'undefined'){
				subtitles= $j('<div>').append(myBalise[0]).html();
			}else{
				subtitles=myBalise[0].outerHTML;
			}
		}else{
			subtitles = "" ; 
		}
	}
	catch (e) { 
		subtitles='';
	}

	
	if(typeof additionalParams == 'object' && additionalParams!=null && typeof additionalParams['player_options'] == 'object'){
		var local_player_options=additionalParams['player_options'];
	}else{
		var local_player_options=_player_options;
	}

	if(xml_playlist != ""){
		local_player_options['playlist'] = true;
		local_player_options['xml'] = xml_playlist;
	}else{
		local_player_options['url']=video_mediaurl;
	}

	local_player_options['typeMime']= video_mimetype;

	if( Object.prototype.toString.call(videos_mediaurl) === '[object Array]' ) {
		local_player_options['urls']= videos_mediaurl;
		local_player_options['typesMime']= videos_mimetype;
		if((allow_multiple_files_by_pattern && typeof list_files != 'undefined')){
			local_player_options['quality_names']= list_files;
		}
		<? if (defined("gListePatternVisio")) { ?>
			else if (typeof (_pattern)=='object'){
				local_player_options['quality_names']= _pattern;			
			}
		<? } ?>
	}

	local_player_options['player']='<?=libUrl?>webComponents/oraoweb/flash/player5-licensed.swf';
	if (!local_player_options['width'] > 0)
		local_player_options['width']=_video_width;
	if (!local_player_options['height'] > 0)
		local_player_options['height']=(my_height/my_width)*_video_width;


	try {
		myBalise=myDom.getElementsByTagName('tcin');
		video_tcin=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_tcin='00:00:00:00';
	}

	try {
		myBalise=myDom.getElementsByTagName('tcout');
		video_tcout=myBalise[0].firstChild.nodeValue;
	}catch (e) {
		video_tcout='00:00:00:00';
	}

	try {
		myBalise=myDom.getElementsByTagName('offset');
		video_offset=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_offset='00:00:00:00';
	}


	if(_player_options['muted']){
		local_player_options['muted']=_player_options['muted'];
	}
	
	if(typeof player_options['res_vignette_width'] != 'undefined'){
		img_width = player_options['res_vignette_width'];
	}else if(typeof player_options['width'] != 'undefined'){
		img_width = player_options['width'];
	}else{
		img_width = 400 ; 
	}
	
	myBalise=myDom.getElementsByTagName('vignette');
	if (myBalise[0].firstChild!=null){
		player_options['image']='makeVignette.php?image='+myBalise[0].firstChild.nodeValue+'&w='+img_width+'&kr=1';
		if(typeof player_options['image_with_ol'] != 'undefined' && player_options['image_with_ol']){
			 player_options['image']=player_options['image']+"&ol=player";
		}
	}
	
	if(mosaic_path != "" && mosaic_pics_index != "" ){
		if(typeof local_player_options['previewOverTimeline'] == "undefined"){
			local_player_options['previewOverTimeline'] = true;
		}
		if(typeof local_player_options['mosaic_use_index'] == "undefined"){
			local_player_options['mosaic_use_index'] = true;
		}
		if(typeof local_player_options['tcMosaicExact'] == "undefined"){
			// player_options['tcMosaicExact'] = true;
		}
		local_player_options['mosaic_path'] = mosaic_path;
		local_player_options['mosaic_pics_index'] = mosaic_pics_index;
	}
	if(waveform_path != ''){
		local_player_options['waveform_path'] = waveform_path;
	}
	
	if(!local_player_options['timecode_lag']){
		local_player_options['timecode_lag'] = video_offset;
	}
	if(!local_player_options['timecode_IN']){
		local_player_options['timecode_IN'] = video_tcin;
	}
	if(!local_player_options['timecode_OUT']){
		local_player_options['timecode_OUT'] = video_tcout;
	}

	if(subtitles != null)
		local_player_options['subtitles'] = subtitles;

	local_player_options['onSelectionSliderChange'] = function(){if(typeof myPanel != "undefined") myPanel.updateSelection();};
	
	if(local_player_options &&  local_player_options['container_id']){
		container_id = local_player_options['container_id'];
	}else{
		container_id ='container';
	}
	
	if(typeof additionalParams == 'object' && additionalParams!=null && typeof additionalParams['ref_var']!= 'undefined'){
		// a utiliser plutot pour des cas spéciaux, conserver myVideo comme var player par defaut
		eval(additionalParams['ref_var']+" = new OJW_player(container_id, local_player_options);");
	}else{
		// (pour l'instant on conserve le duplicata player / myVideo car certains sites se basent sur le nom de la variable pr determiner quel type de player on utilise)
		myVideo=new OJW_player(container_id,local_player_options);
	}
	
	if(typeof myPanel != 'undefined'){
		myPanel.video=myVideo;
		myPanel.defaultTCIN=video_tcin;
		myPanel.videoLength=video_tcout;
		if(myPanel.mode=="extrait" || myPanel.mode=="sequence" || myPanel.mode=="document") myVideo.ShowSelectionSlider();
	}
	else{
		if(typeof parent != "undefined"){
			try{
				parent.myVideo=myVideo;
				if (typeof parent.myPanel != "undefined") {
					parent.myPanel.video=myVideo; 
					parent.myPanel.defaultTCIN=video_tcin; 
					parent.myPanel.videoLength=video_tcout; 
					if(parent.myPanel.mode=="extrait" || parent.myPanel.mode=="sequence" || parent.myPanel.mode=="document") myVideo.ShowSelectionSlider();
				}
			}catch(e){
				console.log("crash alimentation données player frame parent : ");
				console.log(e);
			}
		}
	}
	finalizePlayer(local_player_options);
}

function displayVideoHTML(str,additionalParams) {
	if (typeof(additionalParams)=='undefined') {
		additionalParams=null;
	}
	
	// Le player_options est maintenant cloné puis passé depuis loadOraoPlayer via additionalParams => permet d'éviter de modifications concurrentes du player_options
	if(typeof additionalParams == 'object' && additionalParams!=null &&  typeof additionalParams['player_options'] == 'object'){
		var local_player_options=additionalParams['player_options'];
	}else{
		var local_player_options=_player_options;
	}
	
	if(local_player_options &&  local_player_options['container_id'] && typeof $j != 'undefined'){
		$j('#'+local_player_options['container_id']+" #poster").css('display','none');
	}else if(document.getElementById('poster')){
		document.getElementById('poster').style.display='none';
	}
	myDom=importXML(str);

	try {
		myBalise=myDom.getElementsByTagName('mediaurl');
		video_mediaurl = "";
		var videos_mediaurl = [];
		if (_pattern != null && typeof(_pattern) == "object") {
			for(var i = 0; i < _pattern.length; i++) {
				var obj = myBalise[0].getElementsByTagName(_pattern[i]);
				if (obj[0]) {
					videos_mediaurl[i] = obj[0].firstChild.nodeValue;
					if (video_mediaurl == "") video_mediaurl = obj[0].firstChild.nodeValue;
				}
				else delete _pattern[i];
			}
		}else if(allow_multiple_files_by_pattern && myBalise[0].childNodes.length>0){
			j=0;
			list_files = [];
			// console.log(myBalise[0].childNodes);
			for(var i = 0; i < myBalise[0].childNodes.length; i++) {
				if(myBalise[0].childNodes[i].nodeName!='#text'){
					list_files [j] = myBalise[0].childNodes[i].nodeName;
					videos_mediaurl[j] = myBalise[0].childNodes[i].childNodes[0].nodeValue;
					if (video_mediaurl == "") video_mediaurl =videos_mediaurl[0];
					j++;
				}
			}
		}
		if (video_mediaurl == "") video_mediaurl=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_mediaurl='';
	}

	try {
		myBalise=myDom.getElementsByTagName('width');
		video_width=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_width=0;
	}

	try {
		myBalise=myDom.getElementsByTagName('height');
		video_height=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_height=0;
	}

    try {
        myBalise=myDom.getElementsByTagName('scale');
        video_scale=myBalise[0].firstChild.nodeValue;
	}
	catch (e) { 
		video_scale=0;
	}	


	try {
        myBalise=myDom.getElementsByTagName('subtitles');
		if(typeof myBalise[0] != 'undefined' && myBalise[0] != null ){
			if( typeof myBalise[0].outerHTML == 'undefined'){
				subtitles= $j('<div>').append(myBalise[0]).html();
			}else{
				subtitles=myBalise[0].outerHTML;
			}
		}else{
			subtitles ="" ; 
		}
	}
	catch (e) {
		console.log(e);
		subtitles='';
	}

	try {
		myBalise=myDom.getElementsByTagName('mimetype');
		video_mimetype = "";
		var videos_mimetype = [];
		if (_pattern != null && typeof(_pattern) == "object") {
			for(var i = 0; i < _pattern.length; i++) {
				var obj = myBalise[0].getElementsByTagName(_pattern[i]);
				if (obj[0]) {
					videos_mimetype[i] = obj[0].firstChild.nodeValue;
					if (video_mimetype == "") video_mimetype = obj[0].firstChild.nodeValue;
				}
			}
		}else if(allow_multiple_files_by_pattern && myBalise[0].childNodes.length>0){
			j=0;
			for(var i = 0; i < myBalise[0].childNodes.length; i++) {
				if(myBalise[0].childNodes[i].nodeName!='#text'){
					videos_mimetype[j] = myBalise[0].childNodes[i].childNodes[0].nodeValue;
					if (video_mimetype == "") video_mimetype = videos_mimetype[j];
					break;
				}
			}
		}
		if (video_mimetype == "") video_mimetype=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_mimetype='';
	}

	try {
		myBalise=myDom.getElementsByTagName('playlist');
		xml_playlist=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		xml_playlist='';
	}

	try {
		myBalise=myDom.getElementsByTagName('offset');
		video_offset=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_offset='00:00:00:00';
	}

	try {
		myBalise=myDom.getElementsByTagName('tcin');
		video_tcin=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_tcin='00:00:00:00';
	}

	try {
		myBalise=myDom.getElementsByTagName('tcout');
		video_tcout=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_tcout='00:00:00:00';
	}

	//mosaic
	try {
		myBalise=myDom.getElementsByTagName('mosaic_path');
		mosaic_path=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		mosaic_path='';
	}
	try {
		myBalise=myDom.getElementsByTagName('mosaic_pics_index');
		mosaic_pics_index=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		mosaic_pics_index='';
	}
	
	try {
		myBalise=myDom.getElementsByTagName('waveform_path');
		waveform_path=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		waveform_path='';
	}
	
	try {
		myBalise=myDom.getElementsByTagName('sequences');
		xml_seq=$j('<div>').append(myBalise[0]).html();
	}
	catch (e) {
		console.log("crash recuperation xml_seq");
		console.log(e);
		xml_seq='';
	}

	var autoplay = true;
	
	if(xml_playlist != ""){
		local_player_options['playlist'] = true;
		local_player_options['xml'] = xml_playlist;
	}else{
		local_player_options['url']=video_mediaurl;
	}
	local_player_options['typeMime']= video_mimetype;

    if( Object.prototype.toString.call(videos_mediaurl) === '[object Array]' ) {
		local_player_options['urls']= videos_mediaurl;
		local_player_options['typesMime']= videos_mimetype;
		if((allow_multiple_files_by_pattern && typeof list_files != 'undefined')){
			local_player_options['quality_names']= list_files;
		}
		<? if (defined("gListePatternVisio")) { ?>
			else if (typeof (_pattern)=='object'){
				local_player_options['quality_names']= _pattern;			
			}
		<? } ?>
	}

	if(typeof(_player_options['autoplay'])=='undefined'){
		local_player_options['autoplay']= autoplay;
	}

	if(video_height!=0 && video_width !=0){
		//Redimensionnement de la vidÃ©o
		video_height=Math.round(video_height*_video_width/video_width);
		video_width=_video_width;

		local_player_options['width']= video_width;
		local_player_options['height']= video_height;
	}else if (local_player_options['force_ratio_ref']){
		local_player_options['width'] = _video_width;
		local_player_options['height'] = _video_width*(1/(local_player_options['force_ratio_ref']));

	}
	try{
        myBalise=myDom.getElementsByTagName('vignette');
        	
		if(typeof local_player_options['res_vignette_width'] != 'undefined'){
			img_width = local_player_options['res_vignette_width'];
		}else if(typeof local_player_options['width'] != 'undefined'){
			img_width = local_player_options['width'];
		}else{
			img_width = 400 ; 
		}
		
        if (typeof local_player_options['image'] == "undefined" && myBalise[0].firstChild!=null){
            local_player_options['image']='makeVignette.php?image='+myBalise[0].firstChild.nodeValue+'&w='+img_width+'&kr=1';
        }else if (typeof local_player_options['image'] == "undefined" && typeof image_default !='undefined'){
			local_player_options['image']=image_default;
		}
		if(typeof local_player_options['image_with_ol'] != 'undefined' && local_player_options['image_with_ol']){
			 local_player_options['image']=local_player_options['image']+"&ol=player";
		}
	}
	catch (e) { 
		if(typeof image_default != 'undefined' && image_default != ''){
			local_player_options['image']=image_default;
		}else{
			local_player_options['image']='';
		}
	}


	if(mosaic_path != "" && mosaic_pics_index != "" ){

		if(typeof local_player_options['previewOverTimeline'] == "undefined"){
			local_player_options['previewOverTimeline'] = true;
		}
		if(typeof local_player_options['mosaic_use_index'] == "undefined"){
			local_player_options['mosaic_use_index'] = true;
		}
		if(typeof local_player_options['tcMosaicExact'] == "undefined"){
			local_player_options['tcMosaicExact'] = true;
		}
		local_player_options['mosaic_path'] = mosaic_path;
		local_player_options['mosaic_pics_index'] = mosaic_pics_index;
	}
	if(waveform_path != ''){
		local_player_options['waveform_path'] = waveform_path;
	}

	if(!local_player_options['timecode_lag']){
		local_player_options['timecode_lag'] = video_offset;
	}
	if(!local_player_options['timecode_IN']){
		local_player_options['timecode_IN'] = video_tcin;
	}
	if(!local_player_options['timecode_OUT']){
		local_player_options['timecode_OUT'] = video_tcout;
	}
	
	if(xml_seq != ''){
		local_player_options['xml_sequences'] = xml_seq ; 
	}
	
	
	local_player_options['onSelectionSliderChange'] = function(){if(typeof myPanel != "undefined") myPanel.updateSelection();};

	if(subtitles != null)
		local_player_options['subtitles'] = subtitles;
	
	if(myVideo != null){
		if(myVideo.resetRanges){
			myVideo.resetRanges();
		}
		myVideo.media=null;
		myVideo = null;
	}
	if(local_player_options &&  local_player_options['container_id']){
		container_id = local_player_options['container_id'];
	}else{
		container_id ='container';
	}
	// Le player_options est maintenant cloné puis passé depuis loadOraoPlayer via additionalParams => permet d'éviter de modifications concurrentes du player_options
	if(typeof additionalParams == 'object' && additionalParams!=null && typeof additionalParams['ref_var']!= 'undefined'){
		// pour l'instant a utiliser plutot pour des cas spéciaux, conserver myVideo comme var defaut
		eval(additionalParams['ref_var']+" = new OWH_player(container_id, local_player_options);");
	}else{
		myVideo = new OWH_player(container_id, local_player_options);
	}

	if(typeof myPanel != 'undefined'){
		myPanel.video=myVideo;
		myPanel.defaultTCIN=video_tcin;
		myPanel.videoLength=video_tcout;
		if(myPanel.mode=="extrait" || myPanel.mode=="sequence" || myPanel.mode=="document") myVideo.ShowSelectionSlider();
	}
	else{
		if(typeof parent != "undefined"){
			try{
				parent.myVideo=myVideo;
				if (typeof parent.myPanel != "undefined") {
					parent.myPanel.video=myVideo; 
					parent.myPanel.defaultTCIN=video_tcin; 
					parent.myPanel.videoLength=video_tcout; 
					if(parent.myPanel.mode=="extrait" || parent.myPanel.mode=="sequence" || parent.myPanel.mode=="document") myVideo.ShowSelectionSlider();
				}
			}catch(e){
				console.log("crash alimentation données player frame parent : ");
				console.log(e);			
			}
		}
	}
	finalizePlayer(local_player_options);
}

function displayVideoQT(str,additionalParams) {
	if (typeof(additionalParams)=='undefined') {
		additionalParams=null;
	}
	if(_player_options &&  _player_options['container_id'] && typeof $j != 'undefined'){
		$j('#'+_player_options['container_id']+" #poster").css('display','none');
	}else if(document.getElementById('poster')){
		document.getElementById('poster').style.display='none';
	}
	myDom=importXML(str);
	myBalise=myDom.getElementsByTagName('mediaurl');
	video_mediaurl=myBalise[0].firstChild.nodeValue;

	myBalise=myDom.getElementsByTagName('width');
	video_width=myBalise[0].firstChild.nodeValue;

	myBalise=myDom.getElementsByTagName('height');
	video_height=myBalise[0].firstChild.nodeValue;

	myBalise=myDom.getElementsByTagName('scale');
	video_scale=myBalise[0].firstChild.nodeValue;

	myBalise=myDom.getElementsByTagName('mimetype');
	video_mimetype=myBalise[0].firstChild.nodeValue;

	try {
		myBalise=myDom.getElementsByTagName('offset');
		video_offset=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_offset='00:00:00:00';
	}

	try {
		myBalise=myDom.getElementsByTagName('tcin');
		video_tcin=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_tcin='00:00:00:00';
	}

	try {
		myBalise=myDom.getElementsByTagName('tcout');
		video_tcout=myBalise[0].firstChild.nodeValue;
	}
	catch (e) {
		video_tcout='00:00:00:00';
	}

	//Redimensionnement de la vidÃ©o
	video_height=Math.round(video_height*_video_width/video_width);
	video_width=_video_width;

	var autoplay = true;
	if (video_mimetype.indexOf("audio") == 0){
		autoplay = false;
		video_height = 0;
	}

	//On passe par getVisioUrl pour Ã©viter le bug du progressif download
	<?php
		$randomURL = gmdate("jHBsi",time());
		$videoURL = "getVisioUrl.php?R=" . $randomURL;
	?>
	if (_useGetVisioUrl)
		video_mediaurl = '<? echo $videoURL; ?>';

	var player_options=_player_options;

	player_options['url']=video_mediaurl;
	player_options['typeMime']= video_mimetype;
	player_options['autoplay']= autoplay;
	player_options['width']= video_width;
	player_options['height']= video_height;
	if(typeof player_options['image'] == 'undefined'){
		player_options['image']=image_default;
	}
	player_options['timecode_lag'] = video_offset;
	player_options['timecode_IN'] = video_tcin;
	player_options['timecode_OUT'] = video_tcout;
	player_options['onSelectionSliderChange'] = function(){if(typeof myPanel != "undefined") myPanel.updateSelection();};

	if(player_options &&  player_options['container_id']){
		container_id = player_options['container_id'];
	}else{
		container_id ='container';
	}

	myVideo = new OW_player(container_id, player_options);
	if(typeof myPanel != 'undefined'){
		myPanel.video=myVideo;
		myPanel.defaultTCIN=video_tcin;
		myPanel.videoLength=video_tcout;
		if(myPanel.mode=="extrait" || myPanel.mode=="sequence" || myPanel.mode=="document") myVideo.ShowSelectionSlider();
	}
	else{
		if(typeof parent != "undefined"){
			try {
				parent.myVideo=myVideo;
				if (typeof parent.myPanel != "undefined") {
					parent.myPanel.video=myVideo; 
					parent.myPanel.defaultTCIN=video_tcin; 
					parent.myPanel.videoLength=video_tcout; 
					if(parent.myPanel.mode=="extrait" || parent.myPanel.mode=="sequence" || parent.myPanel.mode=="document") myVideo.ShowSelectionSlider();
				}
			}catch(e){
				console.log("crash alimentation données player frame parent : ");
				console.log(e);
			}
		}
	}

	finalizePlayer(player_options);
}
// MS - 17.08.17 - on n'overwrite pas la fonction JS finalizePlayer si elle a été définie ailleurs (en théorie côté site, donc)
// cette fonction default ne fait d'ailleurs plus rien depuis un moment (je viens de supprimer les commentaires outdated).
// Je la laisse tout de même pour mémoire qu'elle peut être redéfinie et sera appelée lors de l'initialisation du player, après l'appel à l'objet OWH, mais pas forcément après le chargement du média.
if(typeof finalizePlayer == 'undefined'){
	function finalizePlayer(player_options){
	}
}






</script>
