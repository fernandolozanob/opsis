<?php
include_once(libDir."class_visualisation.php");
include_once(libDir."class_chercheDoc.php");
include_once(modelDir.'model_materiel.php');

function copy_directory($source, $destination)
{
	if ($source==$destination)
		return false;
	
	if (is_dir( $source ))
	{
		if (!file_exists($destination))
			mkdir($destination);
		
		$directory=dir($source);
		while (FALSE !== ($readdirectory=$directory->read()))
		{
			if ($readdirectory == '.' || $readdirectory == '..')
				continue;
			
			$PathDir = $source.'/'.$readdirectory;
			
			if (is_dir( $PathDir ))
			{
				copy_directory($PathDir,$destination.'/'.$readdirectory);
				continue;
			}
			
			copy($PathDir,$destination.'/'.$readdirectory);
		}
		
		$directory->close();
	}
	else
	{
		copy($source, $destination);
	}
}


class VisualisationDiaporama extends Visualisation {
	
	var $typeVisu;
	var $arrDocId;
	var $tri;
	
	function __construct ($type , $precis = false) {
		
		$this->typeVisu=$type;
		$this->arrFiles=array();
		$this->precis=$precis;
		$this->tri='';
		
	}
	
	
	
	/** Récupère et prépare le visionnage et le découpage en fonction du type de visionnage
	 */
	
	function prepareVisu($id,$id_lang,$flag_decoup = true,$hash=null,$id_mat=null) {
		global $db;
		
		//Appel depuis une recherche, note : l'id est inutile ici
		
		
		if ($this->typeVisu=='mat')
		{
			$this->objMat= new Materiel();
			$this->objMat->t_mat['ID_MAT'] = $_GET['id'];
			$this->objMat->getMat();
			$this->id_mat= $id;
			
			$this->makeFileName(true);
			$this->fileOut="material/original/".$this->fileOut;
			$ok=$this->copyMovie(); //lien symbolique vers la photo
			
			//Ajout xml diaporama
			$this->XML.="
			<Image>
			<Title value=\"".str_replace($search,$replace,$this->objMat->t_mat['MAT_TITRE'])."\"  />
			<Filename value=\"".basename($this->fileOut)."\" />
			</Image>
			";
			$gotOne=true;
			return true;
			unset($cast);
			
		}
		else
		{
			if ($this->typeVisu=='recherche_DOC') {
				$sql = $_SESSION['recherche_DOC']['sql'];
				$refine = $_SESSION['recherche_DOC']['refine'];
				$tri = $_SESSION['recherche_DOC']['tri'];
				
				$sql.= $refine.$tri;
				$limit='';
				$page = $_REQUEST["page"];
				$nb = $_REQUEST["nb"];
				// VP 6/05/10 : ajout test empty($nb) 
				if (!empty($nb) && $nb!="all"){
					$i_min = ($page - 1) * $nb;
					if($i_min<0){
						$i_min=0;
					}
					$limit=' LIMIT '.$nb.' OFFSET '.$i_min.' ';
				}
				$docs=$db->getAll($sql.$limit);
				foreach ($docs as $d)
				{
					$id[]=$d['ID_DOC'];
				}
				$this->arrDocId = $id;
				$ok=$this->makeXML();
				return $ok;
				
			} else if ($this->typeVisu=='panier') {
				//Panier => récup des ID_DOC dans le panier (avec tri), l'id = id_panier
				$tri=$_SESSION['tri_panier_doc']?$_SESSION['tri_panier_doc']:" ORDER BY PDOC_ORDRE,ID_LIGNE_PANIER ASC";
				$docs=$db->getAll("SELECT ID_DOC from t_panier_doc where id_panier=".intval($id)." ".$tri);
				foreach ($docs as $d) {$tmp[]=$d['ID_DOC'];}
				$this->arrDocId = $tmp;
				unset($tmp);
				$ok=$this->makeXML();
				return $ok;
				
			} else if ($this->typeVisu=='reportage') {
				$id=array($id);
				$this->arrDocId = $id;
				$ok=$this->makeXML();
				return $ok;
			} else if (isset($_GET['id']) && !empty($_GET['id'])) {
				if(!empty($_SESSION['recherche_DOC']['sql'])) {
					$sql = $_SESSION['recherche_DOC']['sql'];
				} else {
					$search = new RechercheDoc();
					$search->prepareSQL();
					$search->finaliseRequete();
					$sql = $search->sql;
				}
				$sql.= ' AND t1.ID_DOC='.intval($_GET['id']);
				//echo $sql;
				$docs=$db->getAll($sql);
			}
			
			if(empty($this->arrDocId)){
				$this->arrDocId = $id;
			}
			
			include_once(modelDir.'model_doc.php');
			
			$gotOne=false;
			
			//Création du répertoire temp et extraction du pack diaporama dans ce répertoire
			// VP 11/05/10 : test si diaporama sous forme non zippée
			/*if(file_exists(kCheminLocalMedia."/diaporama.zip")){
			 $ok=$this->makeDir();
			 if ($ok) $zip=new ZipArchive;
			 if ($zip->open(kCheminLocalMedia."/diaporama.zip") === TRUE) {
			 $ok=$zip->extractTo($this->tempDir);
			 $zip->close();
			 }
			 if (!$ok) {$this->dropError(kErrorDiaporamaExtraction);return false;}
			 }elseif(file_exists(kCheminLocalMedia."/diaporama")){
			 $ok=$this->makeDir();
			 copy_directory(kCheminLocalMedia."/diaporama",$this->tempDir);
			 }else {$this->dropError(kErrorDiaporamaNotFound);return false; }*/
			
			
			$search = array(" ' ",'"',"&","<",">");
			$replace = array("'","&quot;","&amp;","&lt;","&gt;");
			
			if(!is_array($id)) $id=array($id);
			
			//Boucle sur les id_docs à inclure
			foreach ($id as $_id) {
				$this->objDoc=new Doc();
				$this->objDoc->t_doc['ID_DOC']=$_id;
				$this->objDoc->t_doc['ID_LANG']=strtoupper($id_lang);
				$this->objDoc->getDoc();
				$this->objDoc->getPersonnes();
				$this->objDoc->getMats(); //récup ID séquences liées
				
				foreach ($this->objDoc->t_doc_mat as $key => $dm) {
					$tmpsort[$key]=$dm["ID_MAT"];
				}
				array_multisort($tmpsort,SORT_ASC,$this->objDoc->t_doc_mat);
				unset($tmpsort);		
				//debug($this->objDoc->t_doc_mat,'gold');
				
				// on recupere les formats de visionnage
				//$formats=$this->getFormatsForVisu();
				// VP 09/05/11 : ajout paramètres pattern et format pour choisir de préférence un fichier
				if (isset($_GET['format'])) {$formats = array($_GET['format']);}
				else $formats=$this->getFormatsForVisu();
				$gotOne=false;
				//$dm=$this->getMatForVisio($id_lang,$formats,$_GET['pattern']);
				
				
				//Boucle sur les matériels du document
				foreach ($this->objDoc->t_doc_mat as $dm) {
					// Seulement les fichiers Image sauf TIFF
					if ((in_array($dm['MAT']->t_mat['MAT_FORMAT'],$formats) 
						 && strpos(strtolower($dm['MAT']->t_mat['MAT_FORMAT']),'tiff')===false // on exclue le tiff, non supporté par Flash
						 && $dm['MAT']->hasfile==1 && empty($id_mat)) 
						|| (!empty($id_mat) && $dm['ID_MAT'] == $id_mat)) {
						//Note : on peut avoir PLUSIEURS MATERIELS (=> playlist de photos);
						$this->objMat=$dm['MAT'];
						$this->id_mat=$dm['ID_MAT'];
						
						$this->makeFileName(true);
						
						$this->fileOut="material/original/".$this->fileOut;
						$ok=$this->copyMovie(); //lien symbolique vers la photo
						
						if ($this->objDoc->vignette!='') { //lien symbolique pour la vignette
							copy(kCheminLocalMedia.$this->objDoc->vignette,$this->tempDir."material/thumb/".basename($this->objDoc->vignette));
						}
						
						//Récupération des personnes, à adapter par la suite pour plus de flexibilité ???
						foreach ($this->objDoc->t_pers_lex as $pers) {
							$cast[]=$pers['ROLE']." : ".($pers['PERS_PRENOM']!=''?$pers['PERS_PRENOM']." ":'').
							($pers['PERS_PARTICULE']!=''?$pers['PERS_PARTICULE']." ":'').$pers['PERS_NOM'];
						}
						
						//Ajout xml diaporama
						$this->XML.="
						<Image>
						<Title value=\"".str_replace($search,$replace,$this->objDoc->t_doc['DOC_TITRE'])."\"  />
						<Filename value=\"".basename($this->fileOut)."\" />
						<Note  value=\"".$this->objDoc->t_doc['DOC_NOTES']."\" />
						<UploaderName value=\"".@implode(", ",$cast)."\" />
						<UploadTime value=\"".$this->objDoc->t_doc['DOC_DATE_CREA']."\" />
						<Thumb value=\"".basename($this->objDoc->vignette)."\" />
						</Image>
						";
						$gotOne=true;
						unset($cast);
						if(strpos(strtolower($dm['MAT']->t_mat['MAT_FORMAT']),'pdf')!==false) break;
					}
				}
				unset ($this->objDoc);
			}
		}
		
		if (!$gotOne) {
			$this->dropError(kErrorVisioNoMedia);
			return false;
		}
		else {
			// if ($ok) $ok=$this->makeXML(); // génération du XML
			// else {
				if ($this->objDoc) logAction("VIS",Array("ID_DOC"=>$this->objDoc->t_doc['ID_DOC'], "ACT_REQ" => "DOC"));
				else logAction("VIS",array("ID_DOC"=>$this->objMat->t_mat['ID_MAT'], "ACT_REQ" => "MAT"));
				return true;
			// }
		}
	}
	
	
	/** Création et remplissage du fichier XML interprété par show.swf
	 *  Selon les types de visio et le découpage, le contenu du fichier diffère
	 *  IN : objet Visualisation
	 *  OUT : fichier XML écrit dans le répertoire temporaire + true si succès / false si échec
	 */
	
	function makeXML() {
		global $db;
		$content="";
		if(isset($this->arrDocId) && $this->arrDocId != ''){
			$donnees_vignettes=$db->getAll('SELECT  ID_DOC,IM_CHEMIN,IM_FICHIER 
										   FROM t_image i
										   INNER JOIN t_doc d ON d.DOC_ID_IMAGE = i.ID_IMAGE 
										   WHERE d.ID_DOC in ('.implode(",", $this->arrDocId).')
										   GROUP BY ID_DOC,IM_CHEMIN,IM_FICHIER ');
			$content.=$prefix."<slideshow>";
			foreach($this->arrDocId as $id_doc_ordre) // ce foreach permet de conserver l'ordre dans lequel les ids sont triés dans $this->arrDocId
			{
				foreach($donnees_vignettes as $data)
				{
					if ($data['ID_DOC']==$id_doc_ordre)
					{
						$content.="<item>";
						$content.="<image>".kCheminHttp.'/makeVignette.php?kr=1&amp;h=800&amp;image='.substr(storyboardChemin,strlen(kCheminHttpMedia)).'/'.$data['IM_CHEMIN'].'/'.$data['IM_FICHIER']."</image>";
						$content.="<thumb>".kCheminHttp.'/makeVignette.php?kr=1&amp;w=67&amp;h=50&amp;image='.substr(storyboardChemin,strlen(kCheminHttpMedia)).'/'.$data['IM_CHEMIN'].'/'.$data['IM_FICHIER']."</thumb>";
						
						if (!empty($data['IM_TEXT']))
							$content.="<legend>".$data['IM_TEXT']."</legend>";
						
						$content.="</item>";
					}
				}
			}
			$content.=$prefix."</slideshow>";
			$this->XML = $content;
			
			return true;
		}
		else return false;
	}
	
	//*Ecriture du XML interprété par la visionneuse ORAO*/
	function xml_export($entete=0,$encodage=0,$prefix="",$print=true)
	{
		require_once(modelDir.'model_doc.php');
		global $db;
		$content="";
		if ($this->typeVisu=='recherche_DOC' || $this->typeVisu=='panier') {
			$donnees_vignettes=$db->getAll('SELECT  IM_CHEMIN,IM_FICHIER,ID_DOC,DOC_ID_MEDIA 
										   FROM t_image i
										   INNER JOIN t_doc d ON d.DOC_ID_IMAGE = i.ID_IMAGE 
										   WHERE d.ID_DOC in ('.implode(",", $this->arrDocId).')
										   GROUP BY ID_DOC,IM_CHEMIN,IM_FICHIER,DOC_ID_MEDIA ORDER BY IM_FICHIER');
		}
		elseif ($this->typeVisu=='reportage') {
			//Ajout d'un tri si besoin, modification liée à maeb
			if ($this->tri != '')
			{
				$supGroupBy = ','.$this->tri;
				$supOrderBy = $this->tri.',';
			} else {
				$supGroupBy = '';
				$supOrderBy = '';
			}
			$donnees_vignettes=$db->getAll("select IM_CHEMIN,IM_FICHIER ,ID_DOC,DOC_ID_MEDIA 
											from t_doc doc 
											INNER JOIN t_doc_lien dl ON dl.ID_DOC_SRC=doc.id_doc 
											INNER JOIN t_image i ON i.ID_IMAGE = doc.DOC_ID_IMAGE
											where dl.ID_DOC_DST in (".implode(",", $this->arrDocId).") and doc.ID_LANG='".$_SESSION['langue']."' 
											GROUP BY ID_DOC,IM_CHEMIN,IM_FICHIER,dl.dd_ordre,DOC_ID_MEDIA $supGroupBy
											order by $supOrderBy dl.dd_ordre");
		}
		else
			$donnees_vignettes=$db->getAll('SELECT * FROM t_image WHERE ID_LANG=\''.$this->objMat->id_lang.'\' AND ID_IMAGEUR=\''.$this->objMat->t_mat['MAT_ID_IMAGEUR'].'\' ORDER BY IM_CHEMIN,IM_FICHIER');
		
		trace('sql donnees vignettes : SELECT * FROM t_image WHERE ID_LANG=\''.$this->objMat->id_lang.'\' AND ID_IMAGEUR=\''.$this->objMat->t_mat['MAT_ID_IMAGEUR'].'\' ORDER BY IM_CHEMIN,IM_FICHIER');
		
		if ($entete!=0)
			$content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

		$content.=$prefix."<slideshow>\n";
		// VP 9/05/11 : utilisation de makeVignette pour le tag <image> du diaporama
		foreach($donnees_vignettes as $data)
		{
			$myDoc=new Doc();
			$myDoc->t_doc['ID_DOC'] =$data['ID_DOC'];
			$myDoc->getDoc();
			$id_visu = $myDoc->getMaterielVisu();
			$content.="\n\t<item>\n";
			//$content.="\t\t<image>".kCheminHttpMedia.'/storyboard/'.$data['IMAGEUR'].'/'.$data['IM_CHEMIN']."</image>\n";
			if($data['DOC_ID_MEDIA'] == 'P'){
				if($this->precis){
					$content.="\t\t<image>".kCheminHttp.'/makeVignette.php?kr=1&amp;w=2000&amp;image='.$id_visu."&amp;type=video</image>\n";
				}else{
					$content.="\t\t<image>".kCheminHttp.'/makeVignette.php?kr=1&amp;h=800&amp;image='.urlencode(substr(storyboardChemin,strlen(kCheminHttpMedia)).'/'.$data['IM_CHEMIN'].'/'.$data['IM_FICHIER'])."</image>\n";
				}
			}
			else
			{
				if($this->precis){
					$content.="\t\t<image>".kCheminHttp.'/makeVignette.php?kr=1&amp;w=2000&amp;image='.substr(storyboardChemin,strlen(kCheminHttpMedia)).'/'.$data['IM_CHEMIN'].'/'.$data['IM_FICHIER']."</image>\n";
				}else{
					$content.="\t\t<image>".kCheminHttp.'/makeVignette.php?kr=1&amp;h=800&amp;image='.urlencode(substr(storyboardChemin,strlen(kCheminHttpMedia)).'/'.$data['IM_CHEMIN'].'/'.$data['IM_FICHIER'])."</image>\n";
				}
				
			}
			$content.="\t\t<thumb>".kCheminHttp.'/makeVignette.php?kr=1&amp;w=67&amp;h=50&amp;image='.urlencode(substr(storyboardChemin,strlen(kCheminHttpMedia)).'/'.$data['IM_CHEMIN'].'/'.$data['IM_FICHIER'])."</thumb>\n";
			if (!empty($data['IM_TEXT']))
				$content.="\t\t<legend>".$data['IM_TEXT']."</legend>\n";

			$content.="\t</item>\n";
		}
		$content.=$prefix."</slideshow>\n";

		if($encodage!=0)
			$content=mb_convert_encoding($content,$encodage,"UTF-8");

		if ($print)
			echo $content;
		else
			return $content;
	}

	function getMatForVisio($id_lang, $formats, $pattern="") 
	{
		// VP 22/07/10 : ajout paramètre pattern pour choisir un fichier de préférence 
		// Tri du tableau
		foreach ($this->objDoc->t_doc_mat as $key => $dm)
			$tmpsort[$key]=$dm["ID_MAT"];
		
		array_multisort($tmpsort,SORT_ASC,$this->objDoc->t_doc_mat);
		unset($tmpsort);
		// Recherche du premier matériel visionnable avec la bonne langue
		foreach ($this->objDoc->t_doc_mat as $dm)
		{
			//Récupération du matériel pour la visio
			// VP 16/10/09 : ajout critère DMAT_INACTIF
			trace($dm["ID_MAT"]." : ".$dm['MAT']->t_mat['MAT_FORMAT']." : ".$dm['MAT']->hasfile." : ".$dm['DMAT_INACTIF']);
			if (in_array($dm['MAT']->t_mat['MAT_FORMAT'],$formats) && $dm['MAT']->hasfile==1  && $dm['DMAT_INACTIF']!='1')
			{
				//Ce matériel est visionnable (format + existence fichier)
				//Certains matériels (peu) sont versionnés, dans ce cas, on choisit ce matériel si c'est la même langue
				if (strtoupper($dm['DMAT_ID_LANG'])==strtoupper($id_lang)) 
					$_dm=$dm;
				else if (!empty($pattern) && strstr($dm["ID_MAT"],$pattern))
					return $dm; //on choisit le fichier qui correspond au pattern
				else if (!isset($_dm))
					$_dm=$dm; //Sinon on garde le premier qui convient
			}
		}
		return $_dm;
	}
}
?>