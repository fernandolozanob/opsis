<?php

require_once('class_jobProcess.php');
require_once('interface_Process.php');

require_once('exception/exception_directoryException.php');

require_once(libDir.'class_matInfo.php');

class JobProcess_story extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('story');
		
		$this->required_xml_params=array
		(
			
		);
		
		$this->optional_xml_params=array
		(
			'largeur'=>'int',
			'hauteur'=>'int',
			'cutdetection'=>'int',
			'makemosaic'=>'int',
			'iteration'=>'int',
			'mosaic_width'=>'int'
			
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{
		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		$hauteur_storyboard=320;
		$largeur_storyboard=240;
		
		if ((isset($params_job['largeur']) && !empty($params_job['largeur'])) && (isset($params_job['hauteur']) && !empty($params_job['hauteur'])))
		{
			$hauteur_storyboard=intval($params_job['hauteur']);
			$largeur_storyboard=intval($params_job['largeur']);
		}
		else if ((isset($params_job['largeur']) && !empty($params_job['largeur'])) && !(isset($params_job['hauteur']) && !empty($params_job['hauteur'])))
		{
			// si on a une largeur mais pas de hauteur
			$mat_info=MatInfo::getMatInfo($this->getFileInPath());
			$src_w=intval($mat_info['display_width']);
			$src_h=intval($mat_info['display_height']);
			
			$largeur_storyboard=intval($params_job['largeur']);
			//calcul de la hauteur
			$hauteur_storyboard=intval(round(($params_job['largeur']*$src_h)/$src_w));
		}
		else if (!(isset($params_job['largeur']) && !empty($params_job['largeur'])) && (isset($params_job['hauteur']) && !empty($params_job['hauteur'])))
		{
			// si ona une hauteur mais pas de largeur
			$mat_info=MatInfo::getMatInfo($this->getFileInPath());
			$src_w=intval($mat_info['display_width']);
			$src_h=intval($mat_info['display_height']);
			
			$hauteur_storyboard=intval($params_job['hauteur']);
			$largeur_storyboard=intval(round(($params_job['hauteur']*$src_w)/$src_h));
		}
		else
		{
			// sinon on a ni la hauteur ni la largeur
			$mat_info=MatInfo::getMatInfo($this->getFileInPath());
			$hauteur_storyboard=intval($mat_info['display_height']);
			$largeur_storyboard=intval($mat_info['display_width']);
		}
		
		if (is_dir(dirname($this->getFileOutPath()))===false)
		{
			if (mkdir(dirname($this->getFileOutPath()))===false)
				throw new DirectoryException('failed to create storyboard directory ('.dirname($this->getFileOutPath()).')');
		}
		
		if (strpos(kUtilStoryboardPath,"MovToJpg")>0 || strpos(kUtilStoryboardPath,"multijpg")>0){ //MovToJpg et multijpg.sh
			$this->shellExecute(kUtilStoryboardPath,escapeQuoteShell($this->getFileInPath()).' '.$params_job['iteration'].' '.$largeur_storyboard.' '.$hauteur_storyboard.' "'.$this->getFileOutPath().'"');
		}else
		{
            // VP 1/07/2016 : ajout scale
			if (isset($params_job['cutdetection']) && !empty($params_job['cutdetection']) && $params_job['cutdetection']==1){
				$this->shellExecute(kUtilStoryboardPath,'-i '.escapeQuoteShell($this->getFileInPath()).' -vf "select=eq(pict_type\,1),scale=min(in_w\,in_h*dar*sar+1):min(in_h\,in_w/(dar*sar)+1),showinfo" -pix_fmt yuvj420p -vsync 0 -f image2 -qscale 5 "'.dirname($this->getFileOutPath()).'/%4d.jpg"',false);
			}else{
				$this->shellExecute(kUtilStoryboardPath,'-i '.escapeQuoteShell($this->getFileInPath()).' -vf "select=not(mod(t\,'.$params_job['iteration'].')),scale=min(in_w\,in_h*dar*sar+1):min(in_h\,in_w/(dar*sar)+1),showinfo" -pix_fmt yuvj420p -vsync 0 -f image2 -qscale 5 "'.dirname($this->getFileOutPath()).'/%4d.jpg"',false);
			}
			
			do
			{
				sleep(1);
				$this->updateProgress();
			}
			while($this->checkIfStoryRunning());
		}
		if ((strpos(kUtilStoryboardPath,"ffmbc")>0 || strpos(kUtilStoryboardPath,"ffmpeg")>0) && is_file($this->tmp_dir_path.'/stderr.txt')) {
			require_once(modelDir.'model_imageur.php');
			Imageur::renameImagesFFMBC($this->tmp_dir_path.'/stderr.txt',dirname($this->getFileOutPath()),basename($this->getFileOutPath()));
		}
		if(isset($this->params_job['makemosaic']) && $this->params_job['makemosaic']==1 && strpos(kUtilStoryboardPath,"ffmbc")>0 ){
			// VP 31/05/2018 : pas de génération de mosaïque si le fichier fait moins de 60 secondes car generateMosaic plante
			if(!isset($mat_info)){
				$mat_info=MatInfo::getMatInfo($this->getFileInPath());
			}
			$dur = $mat_info['duration'];
			if($dur>60){
				require_once(modelDir.'model_imageur.php');
				$this->writeJobLog("début makemosaic");
				if(isset($this->params_job['mosaic_width'])){
					$width = $this->params_job['mosaic_width'] ; 
				}else{
					$width = null ; 
				}
				Imageur::generateMosaic($this->getFileInPath(),$this->getFileOutPath(),null ,$this,$width);
				$this->writeJobLog("fin makemosaic");
			}
		}
		$this->setProgression(100,'Génération storyboard');
		$this->writeOutLog('Fin traitement');
	}
	
	public function updateProgress()
	{
		$donnees_stderr=file_get_contents($this->getTmpDirPath().'/stderr.txt');
        // VP 8/04/13 : compatibilité Mac OS X
		$donnees_stderr=str_replace("\r","\n",$donnees_stderr);
		
		//preg_match_all("/frame=(.*) fps=(.*) q=(.*) Lsize=(.*) time=(.*) bitrate=(.*)/", $donnees_stderr, $infos_encodage);
		preg_match_all('/frame=([ 0-9]+) fps=([ 0-9]+) q=([- 0-9.]+) Lsize=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=(.*)/', $donnees_stderr, $infos_encodage);
		
		if (count($infos_encodage[1])>0)
			$this->setProgression(100,'Génération storyboard');
		else
		{
			//preg_match_all("/frame=(.*) fps=(.*) q=(.*) size=(.*) time=(.*) bitrate=(.*) eta=(.*)/", $donnees_stderr, $infos_encodage);
			preg_match_all('/frame=([ 0-9]+) fps=([ 0-9]+) q=([- 0-9.]+) size=([ 0-9]+)kB time=([ 0-9:.]+) bitrate=([ 0-9.a-z\/]+) eta=(.*)/', $donnees_stderr, $infos_encodage);
			
			$eta_initial=$infos_encodage[7][0];
			$eta_courant=$infos_encodage[7][count($infos_encodage[7])-1];
			
			if (!empty($eta_initial) && !empty($eta_courant))
			{
				$progress=(timeToSec($eta_initial)-timeToSec($eta_courant))/timeToSec($eta_initial)*100;
				
				if ($this->getProgression()>$progress)
					$progress=$this->getProgression();
				
				$this->setProgression($progress,'Génération storyboard');
			}
		}
	}
	
	private function checkIfStoryRunning()
	{
		//if ($this->nb_frames_encodees<$this->nb_frames)
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$this->getPid().'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1)
				return true;
			else
				return false;
		}
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
}

?>
