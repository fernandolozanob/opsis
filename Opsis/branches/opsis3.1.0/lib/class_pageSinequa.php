<?php

/**
 *  Classe Page
 * Version : 1.0 (PHP4)
 * Author : L. Desjardins 02/08/2006
 */
include_once(libDir."class_page.php");


class PageSinequa extends Page {


private static $instance;
var $urlaction;
var $page;
var $template;
var $langue;
var $titre;
var $includePath;

var $prmAction, $prmPage, $prmSort, $prmNbLignes; // Noms des params GET comprenant l'action, la page de résultat, le tri

// Variables pour le Pager
var $max_rows; // max rows by page ($this->max_rows)
var $rows; // number of rows in pages ($this->rows)
var $fleches= array("prev"=>"&lt;","next"=>"&gt;","first"=>"&lt;&lt;","last"=>"&gt;&gt;"); //flèches par défaut : tableau
var $tab_options = array(5=>"5",10 => "10", 20 => "20", 50 => "50", 100 => "100", "all" => kTous);
var $found_rows;
var $result=array(); // Tableau de résultat de la requête
var $PagerLink;
var $nbLignes;
var $error_msg;

// Variables pour le tri
var $order; //tri récupéré depuis l'url
var $triInvert; //sens inverse (passé aux url du tableau pour la bascule ASC / DESC
var $col; //colonne de tri
var $params4XSL=array(); //Paramètres de base passés aux feuilles XSL.

	/** Singleton : une seule classe PageSinequa  en même temps !*/
   public static function getInstance()
   {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c;
       }
       return self::$instance;
   }

	/**
	 * Initialisation du Pager.
	 * IN : SQL, new_max (opt, nb de lignes max),
	 * 		$altVar = autre mode d'initialisation de la page en cours (ex : session),
	 * 			NOTE: cette variable n'est utile que si le sélecteur du nb de lignes est mis dans la XSL.
	 * 			Si ce sélecteur est appelé dans la page PHP, la var de session est déjà initialisée
	 * 		$addGETVars = variables additionnelles à passer aux liens du Pager
	 * OUT : var de classe : Result (resultat de requete "tronqué"),
	 * 		 Rows (nb de lignes "tronqué"), Found_Rows (nb lignes totales), PagerLink (url passée de page en page)
	 */
    function initPager($sql, $new_max = 10,$altVar=null,$addGETVars=null,$secstocache=0)
    {

        global $db;
   		require_once(libDir."sinequa/fonctions.inc");
		require_once(libDir."sinequa/Intuition.inc");     
		$deftNbLignes=defined("gNbLignesDocListeDefaut")?gNbLignesDocListeDefaut:10;

        
        $iSession=new iSession;  		
  		// VP (23/10/08) : changement max_answers_count et connexion par méthode connect
		$prms= array ('host' => sinequa_host,
						'port' => sinequa_port,
						'read_only' => 1,							 		       
						'charset' => in_UTF8,
						'page_size' => empty($this->nbLignes)?$this->nbLignes:10,	
						'max_answers_count' => 500000,	       
						'default-language' => $_SESSION['langue']
				);

		//$link = $iSession->in_connect($prms);	
		$link = $iSession->connect($prms);	
	
	   if (!isset($this->nbLignes)) $this->max_rows = $new_max; else $this->max_rows=$this->nbLignes;

       	$this->getPageFromURL();
        if ($this->page=="" && $altVar!=null) $this->page=$altVar;
        if (round($this->page)!=$this->page || $this->page<=0 || !is_numeric($this->page) ) $this->page=1;

        if($this->max_rows == "all"){ //on retourne tout
        	$i=0;
            $iQuery=$iSession->in_query($sql);		

            if (!$iQuery) { //0 résultats retournés !
            	$this->rows=0;
            	$this->found_rows=0;
            	$this->page=1;
            	$this->PagerLink="";
            	$this->result=array();
            	return false;
            	}

            $this->max_rows = $iQuery->in_num_rows();


            if ($this->max_rows>kMaxRows) $this->max_rows=kMaxRows; // précaution : si trop de lignes, on revient à un mode paginé pour ne pas surcharger le serveur
            else {

            for($i=0;$i<$this->max_rows;$i++) $this->result[$i]=$iQuery->in_fetch_array();
            $this->rows =$this->max_rows;
            $this->found_rows=$this->max_rows;
            $this->page=1;
            if(isset($iQuery))$iQuery->close();
            }
        }

		if ($this->max_rows!="all")
		{ //pagination
	        $i=0;
	        
	        $sql.=" SKIP ".(($this->page-1)*($this->nbLignes?$this->nbLignes:$deftNbLignes))." COUNT ".($this->nbLignes?$this->nbLignes:$deftNbLignes);
			//debug($sql); 
		//debug(str_ireplace(array(',','join','WHERE','in '),array(','.chr(10),'join'.chr(10),'WHERE'.chr(10),'in '.chr(10)),$sql),'pink');
			
	        $iQuery=$iSession->in_query($sql);
	        
	     // debug($iQuery,'orange',true); //attention : débug actif => stop des window.onload
	     
		    if ($iQuery) {
		        $this->found_rows = $iQuery->in_num_rows(); 
		        $i_min = ($this->page - 1) * $this->max_rows;
		        if ($i_min <=0 || $i_min >=$this->found_rows) $i_min=0;
		       	
		       	$linesToShow=$iQuery->the_available_tuples_count;
                for($i=0;$i<$linesToShow;$i++) 
                {$this->result[$i]=$iQuery->in_fetch_array();
                	//j'ai viré les balises XML encadrantes, pour les mettre dans la XSL
                	$this->result[$i]['colonne_xml']=trim($this->result[$i]['colonne_xml']);
                	//Pour contourner un pb sinequa, et récupérer le champ colonne XML avec du XML dedans,
                	//on retourner chercher le champ source document-content et on le transforme pour pouvoir
                	//l'utiliser
                	$this->result[$i]['document-content']=hex_str($this->result[$i]['document-content']); //décodage hexa
                	$this->result[$i]['document-content']=str_replace('<?xml version="1.0" encoding="utf-8"?>',"",$this->result[$i]['document-content']); //suppression entete xml
                	$this->result[$i]['document-content']="<XML>".$this->result[$i]['document-content']."</XML>"; //encadrement par des balises
                	
		        }
		        $this->rows =$linesToShow;
		        
		        /*Ensuite, récupération concepts et corrélations
		         */
		        $this->conceptsSinequa=parseConcepts( $iQuery->in_fetch_attribute("concepts"));
		        //debug($this->conceptsSinequa,'lightgreen');
		        $arrParamsSinequa=unserialize(gParamsRechercheSinequa);
		        foreach ($arrParamsSinequa["correlationGroups"] as $correlation) {
		        	$_tmp=$iQuery->in_fetch_attribute("cor_".$correlation['entity']);
		        	if ($_tmp) $this->correlationsSinequa[$correlation['entity']]=parseCorrelations($_tmp);		        
		     	}
		        // VP 20/11/08 : Suppression chr(10) 
				foreach($this->correlationsSinequa as &$cor){
					foreach($cor as &$val){
						$val["label"]=str_replace(chr(10)," ",$val["label"]);
					}
				}
		        
		       //debug($this->correlationsSinequa,'lightblue');
		        
				if(isset($iQuery))$iQuery->close();

		    } else {$this->found_rows=0;$this->page=1;$this->error_msg="Problem with request.";}
		}
        $this->PagerLink=$this->getName()."?urlaction=".$this->getActionFromUrl().$this->addUrlParams($addGETVars)."&page=";
        if ($this->page>$this->num_pages()) $this->page=$this->num_pages();
        $this->params4XSL['nb_pages']=$this->num_pages();
        $this->params4XSL['nb_rows']=$this->found_rows;
    
   		if(isset($iSession))$iSession->in_close();
    	
    	//debug($this,'gold',true);
    
    	
    
    }



	




} // fin de classe PAGE
?>