<?php
// VP 31/8/09 : création fichier
include_once(libDir."class_visualisation.php");

class VisualisationFlashVideo extends Visualisation {

	var $typeVisu;
	var $mediaUrl; // url fichier media
	var $mediaPath; //path media sur disk

	function __construct ($type) {
		$this->typeVisu=$type;
	}


	/** R√©cup√®re et pr√©pare le visionnage et le d√©coupage en fonction du type de visionnage
	 * et lance les traitements
	 * ID => ID de l'entité à visionner : doc, matériel, etc.
	 * ID_LANG => langue de l'entité à visionner
	 * 
	 */

	function prepareVisu($id,$id_lang) {
		
		switch ($this->typeVisu) {
			case 'materiel' :
				include_once(modelDir.'model_materiel.php');
				$this->objMat= new Materiel();
				$this->objMat->t_mat['ID_MAT']=$id;

				$this->objMat->getMat();
				$this->tcin=$this->objMat->t_mat['MAT_TCIN'];
				$this->tcout=$this->objMat->t_mat['MAT_TCOUT'];
				$this->offset=$this->objMat->t_mat['MAT_TCIN'];
				$this->id_mat=$this->objMat->t_mat['ID_MAT'];
				$this->objMat->id_lang=$id_lang;
				$ok=$this->objMat->hasfile;
				$this->calcResolution($this->objMat->t_mat['MAT_INFO']);
				$this->MimeType=getMimeType($this->objMat->t_mat['ID_MAT']);
				$ok=$this->makeDir();
				if ($ok){ 
				$ok=$this->makeFileName(true);
				$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
			$this->mediaPath=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
				}

				//if ($ok) $ok=$this->copyRefFiles();
				$ok=$this->copyMovie();

				if ($ok) {
					$this->XML.="<item><media:content url=\"".$this->mediaUrl."\"/></item>\n";
				}
				else {$this->dropError(kErrorVisioNoMedia);}
			break;

			default :
				// Type document
				include_once(modelDir.'model_doc.php');

				$search = array(" ' ",'"',"&","<",">");
				$replace = array("'","&quot;","&amp;","&lt;","&gt;");

				$this->objDoc=new Doc();
				$this->objDoc->t_doc['ID_DOC']=$id;
				$this->objDoc->t_doc['ID_LANG']=$id_lang;
				$this->objDoc->getDoc();
				$this->objDoc->getMats(); //r√©cup ID s√©quences li√©es
				$this->objDoc->getChildren(); //r√©cup mat√©riel

				//A partir de l√†, on peut extraire tous les TC n√©cessaires.
				//D'abord il faut r√©cup√©rer le mat√©riel de visualisation
				// Format
				if (isset($_GET['format'])) {$formats = array($_GET['format']);}
				else $formats=$this->getFormatsForVisu();

				// VP 10/03/09 : ajout fonction de recherche du matériel à visionner
				$gotOne=false;
				$arr_dm=$this->getMatForVisio($id_lang,$formats);
			

				if(!empty($arr_dm)){
					$ok=$this->makeDir();
					foreach ($arr_dm as $dm) {
						$this->id_mat=$dm['ID_MAT'];
					
						if (!$this->tcin) $this->tcin=$dm['DMAT_TCIN'];
						if (!$this->tcout) $this->tcout=$dm['DMAT_TCOUT'];
						$this->objMat=$dm['MAT'];
						$this->MimeType=getMimeType($this->objMat->t_mat['ID_MAT']);
						$numer=tcToSec($this->tcout)-tcToSec($this->tcin);  //dur√©e de l'extrait
						$denom=tcToSec($this->objMat->t_mat['MAT_TCOUT'])-tcToSec($this->objMat->t_mat['MAT_TCIN']); //dur√©e totale du mat√©riel
						trace("ratio decoup :".$numer."/".$denom);
						// cas de d√©coupage : pas de dur√©e du mat√©riel (dans le doute on d√©coupe) ou bien ratio inf√©rieur √† la limite)
						// sinon on ne d√©coupe pas : ratio sup√©rieur, dur√©e extrait inconnue,...
						if (($denom==0  || $numer/$denom<0.95 ) && $numer!=0 ) $decoup=true;
						if ($this->typeVisu=='commande') $decoup=true; //commande ? on force le d√©coupage

						$this->offset=(empty($this->objMat->t_mat['MAT_TCIN'])?"00:00:00:00":$this->objMat->t_mat['MAT_TCIN']);
					

						
						//Pr√©paration du d√©coupage
						if ($ok && $decoup ) {
							// VP 5/05/10 : appel des outils de découpage dans méthode sliceMovie
							$ok=$this->sliceMovie();
							$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
							$this->mediaPath=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
						} else { //Pas de d√©coupage, on fait une copie simple du media
							
							$this->makeFileName(true);
							$ok=$this->copyMovie();
							//$this->mediaUrl=videosServer.$this->id_mat;
							$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/".$this->fileOut;
							$this->mediaPath=kCheminLocalVisionnage.$this->tempDirShort."/".$this->fileOut;
						}
						
						$this->calcResolution($this->objMat->t_mat['MAT_INFO']);
						
					
						// Ajout playList
						$this->XML.="<item><media:content url=\"".$this->mediaUrl."\"/></item>\n";
						$gotOne=true;
					}
					
				}
				if (!$gotOne) {$this->dropError(kErrorVisioNoMedia);}
			break;
		}

		if ($ok) $ok=$this->makeXML(); // génération du XML
		if (!$ok) return false; else {
				if ($this->objDoc) logAction("VIS",Array("ID_DOC"=>$this->objDoc->t_doc['ID_DOC'], "ACT_REQ" => "DOC"));
				else logAction("VIS",array("ID_DOC"=>$this->objMat->t_mat['ID_MAT'], "ACT_REQ" => "MAT"));
				return true;
				}
	}

	// VP 10/03/09 : ajout fonction de recherche du matériel à visionner
	/** Choisit le meilleur matériel parmi le tableau des matériels d'un document
		*  IN : var de classe : tableau t_doc_mat du doc, $id_lang, $formats
		* 	OUT : la ligne de t_doc_mat correspondant au meilleur matériel
		*  NOTE : par meilleur matériel on entend :
		* 		- matériel au format visionnable (ex : mpeg4)
		* 		- matériel avec un fichier en ligne
		* 		- et SI une version existe, matériel dont la version correspond à la langue demandée (svt : la langue de l'appli)
		*/
	protected function getMatForVisio($id_lang, $formats) {
		// Tri du tableau
		foreach ($this->objDoc->t_doc_mat as $key => $dm) {
			$tmpsort[$key]=$dm["ID_MAT"];
		}
		array_multisort($tmpsort,SORT_ASC,$this->objDoc->t_doc_mat);
		unset($tmpsort);
		// Recherche du premier matériel visionnable avec la bonne langue
		foreach ($this->objDoc->t_doc_mat as $dm) {
			//Récupération du matériel pour la visio
			if (in_array($dm['MAT']->t_mat['MAT_FORMAT'],$formats) && $dm['MAT']->hasfile==1) {
				//Ce matériel est visionnable (format + existence fichier)
				//Certains matériels (peu) sont versionnés, dans ce cas, on choisit ce matériel si c'est la même langue
				if(empty($dm['DMAT_ID_LANG']) || (strtoupper($dm['DMAT_ID_LANG'])==strtoupper($id_lang))) $arr_dm[]=$dm;
			}	
		}
		return $arr_dm;
	}
	
/** Création et remplissage du fichier XML interprété par ORAO
*  Selon les types de visio et le découpage, le contenu du fichier diffère
*  IN : objet Visualisation
*  OUT : fichier XML écrit dans le répertoire temporaire + true si succès / false si échec
*/

function makeXML() {
	
	$handle=fopen($this->tempDir."liste.xml",w);
	if (!$handle) {
		$this->dropError(kErrorVisioCreationXML);
		return false;
	}

	fwrite($handle,'<rss version="2.0" 
		   xmlns:media="http://search.yahoo.com/mrss/" 
		   xmlns:jwplayer="http://developer.longtailvideo.com/trac/wiki/FlashFormats">
		   <channel>
		   ');
	fwrite($handle,$this->XML);
	fwrite($handle,'</channel>
		   </rss>');
	fclose($handle);
	$this->mediaUrl=kVisionnageUrl.$this->tempDirShort."/"."liste.xml";
	return true;
}


/** Cr√©ation du code HTML pour lancer le visionnage
	 *  IN :
	 * 	OUT :
	 */
	function renderComponent($print=true) {

		ob_start();
		include(getSiteFile("designDir","visualisationFlashVideo.inc.php"));
		$html=ob_get_contents();
		ob_end_clean();

		if ($print) echo $html; else return $html;

	}
	
    function xml_export($entete=0,$encodage=0,$prefix="",$print=true){
        $content="";
        $search = array(" ' ",'&','<','>');
        $replace = array("'",'&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
        
        $content.=$prefix."\t<visionnage>";
 		$content.=$prefix."\t\t<mediapath>".$this->mediaPath."</mediapath>";
		$content.=$prefix."\t\t<mediaurl>".$this->mediaUrl."</mediaurl>";  
		$content.=$prefix."\t\t<tcin>".$this->tcin."</tcin>";  
		$content.=$prefix."\t\t<tcout>".$this->tcout."</tcout>";  		
		$content.=$prefix."\t\t<offset>".$this->offset."</offset>";  
		$content.=$prefix."\t\t<id_mat>".$this->id_mat."</id_mat>";  
		$content.=$prefix."\t\t<width>".$this->width."</width>";  
		$content.=$prefix."\t\t<height>".$this->height."</height>";  
		$content.=$prefix."\t\t<scale>".$this->scale."</scale>";  						
		$content.=$prefix."\t\t<mimetype>".$this->MimeType."</mimetype>";  	
        $content.=$prefix."\t</visionnage>";
        if($encodage!=0){
            $content=mb_convert_encoding($content,$encodage,"UTF-8");
        }
        if ($print) echo $content; else return $content;
        	
	}
	

	
}
?>
