<?php

require_once('class_jobProcess.php');
require_once('class_matInfo.php');
require_once('interface_Process.php');

class JobProcess_audiowaveform extends JobProcess implements Process
{
	function __construct($input_job,$engine,$syncJobProcess=null)
	{
		parent::__construct($syncJobProcess);
		
		$this->setModuleName('audiowaveform');
		
		$this->required_xml_params=array
		(
		);
		
		$this->optional_xml_params=array
		(	
		);
		
		
		$this->setInput($input_job);
		$this->setEngine($engine);
	}
	
	public function doProcess()
	{

		$this->writeOutLog('Debut traitement');
		$params_job=$this->getXmlParams();
		$options = "-i ".escapeQuoteShell($this->getFileInPath())." ";
		// Ajout option "-b" à 8 => l'encodage est forcément en 8 bit pour pouvoir fonctionner avec peaks.js 
		// à voir si on le passe un jour en variable.
		$options .= "-b 8 "; 
		
		$tmp_file=stripExtension($this->getFileOutPath()).'_'.time().'_'.rand(100000,999999).'.'.getExtension($this->getFileOutPath());
		
		$options .= "-o ".escapeQuoteShell($tmp_file)." ";
		
		$this->shellExecute(kAudioWaveFormPath,$options,false);
		
		do
		{
			sleep(1);
			$this->updateProgress();
		}
		while($this->checkIfUtilRunning());
		
		rename($tmp_file,$this->getFileOutPath());
		
		$this->setProgression(100);
		$this->writeOutLog('Fin traitement');
	}
	
	public function finishJob()
	{
	
		parent::finishJob();
	}
	
	public function updateProgress()
	{
		// progression avec ffmbc
		$donnees_stdout=file_get_contents($this->getTmpDirPath().'/stdout.txt');
		$this->WriteOutLog('updateProgress donnees_stdout : '.print_r($donnees_stdout,true));
		
		// VP 8/04/13 : compatibilité Mac OS X
		preg_match_all("/Done: ([ 0-9]+)%/", $donnees_stdout, $infos_audiowaveform);
		$percent_progress = intval($infos_audiowaveform[1][count($infos_audiowaveform[1])-1]);
		$this->setProgression($percent_progress);
	}
	
	public static function kill($module,$xml_file)
	{
		JobProcess::kill($module,$xml_file);
	}
	
	
	private function checkIfUtilRunning()
	{
		if(eregi('WINNT',PHP_OS)){
			return !(strpos(shell_exec('tasklist /FI "PID eq '.$this->getPid().'"'), "INFO: No tasks are running which match the specified criteria.") === 0);
		} else {
			if (exec('ps -p '.$this->getPid().' -o comm= | wc -l')==1){
				$this->writeJobLog('checkIfUtilRunning : still running');	
				return true;
			}else{
				$this->writeJobLog('checkIfUtilRunning : done');	
				return false;
			}
		}
	}
}

?>
