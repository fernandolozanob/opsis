<?php
	// Vérification des paramètres
	foreach($this->args as $key => $val) {
		
		switch ($key) { 
			case 'verb':
				break;
				
			default:
				$this->dropError ('badArgument', $key, $val);
		}
	}

	if (!empty($this->error_msg)) return;
	
	// VP 10/02/11 : gestion OAI festival et personne (ajout constante oaiSets)
	if(defined("oaiSets")) $oaiSets=explode(",",oaiSets);
	else $oaiSets=array("doc");
	
	echo("<ListSets>");
	foreach($oaiSets as $set){
		switch($set){
			case "doc": $setName=kDocuments;break;
			case "fest": $setName=kFestivals;break;
			case "pers": $setName=kPersonnes;break;
		}
		echo "<set>
		<setSpec>".$set."</setSpec>
		<setName>".$setName."</setName>
		</set>";
	}
	echo("</ListSets>");
	
?>
