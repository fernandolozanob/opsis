<?php
require_once(modelDir.'model.php');
class Message extends Model
{
	public $t_message;
	
	var $to;
	var $from;
	var $sujet;
	var $contenu;
	var $type;
	var $boundary;
	var $add_headers;
	var $attachment="";
	var $eol = PHP_EOL;
	var $cc_to;
	var $reply_to;
	var $cc_to_sender = false;
    var $error_msg;
	
	//*** Constructeur
    function __construct($id=null,$version=""){
		// parent::__construct('t_message',$id,$version);
	}
	
	// a priori pas besoin de refonte getData, rien de specifique à la fonction getMessage
	function getMessage($arrMsg=array()){
		return $this->getData($arrMsg) ; 
	}
	/*function getMessage($arrMsg=array())
	{
		global $db;
		if (!$arrMsg) $arrVals=array($this->t_message['ID_MESSAGE']); else $arrVals=(array)$arrMsg;
		$arrCrit['ID_MESSAGE']=$arrVals;

		$this->t_message=getItem("t_message",null,$arrCrit,null,0);
		if (count($this->t_message)==1 && !$arrMsg) {
				$this->t_message=$this->t_message[0];
		}
		else {
            $arrMsgs=array();
			foreach ($this->t_message as $idx=>$Msg) {
				$arrMsgs[$idx]= new Message();
				$arrMsgs[$idx]->t_message=$Msg;
			}
			unset($this); // plus besoin
			return $arrMsgs;
		}
	}*/
	
	function updateFromArray ($tab_valeurs) {

	 	if (empty($this->t_message)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_message[strtoupper($fld)])) $this->t_message[strtoupper($fld)]=$val;
		}

	}

	function save() {
		global $db;
		if (empty($this->t_message['ID_MESSAGE'])) {return $this->create();}
		
		/*$arrFieldToSave=array();
		foreach ($this->t_message as $field=>$val)
			if (strpos($field, "MES_") === 0)
				$arrFieldToSave[] = $field.'='.$db->Quote($val);
				
		$ok=$db->Execute('UPDATE t_message SET '.implode(", ", $arrFieldToSave).' WHERE ID_MESSAGE='.$db->Quote($this->t_message['ID_MESSAGE']));*/
		$rs = $db->Execute("SELECT * FROM t_message WHERE ID_MESSAGE=".intval($this->t_message['ID_MESSAGE']));
		$sql = $db->GetUpdateSQL($rs, $this->t_message);
		if (!empty($sql)) $ok = $db->Execute($sql);

		if (!$ok && !empty($sql)) {$this->dropError(kErrorMessageSauve);return false;}
		return true;
	}

	function create() {
		global $db;
		if (!empty($this->t_message['ID_MESSAGE'])) {return $this->save();}
		
		$this->t_message['MES_DATE_CREA']=date('Y-m-d H:i:s');
		
		$ok = $db->insertBase("t_message","id_message",$this->t_message);
		if (!$ok) {$this->dropError(kErrorMessageCreation);return false;}
		//$this->t_message['ID_MESSAGE']=$db->Insert_ID('t_message');

		return true;
	}
	
	function delete() {
		global $db;
		if (empty($this->t_message['ID_MESSAGE'])) {return;}
		
		$ok=$db->Execute('DELETE FROM t_message WHERE ID_MESSAGE='.intval($this->t_message['ID_MESSAGE']));

		if (!$ok) {$this->dropError(kErrorMessageDelete);return false;}
		return true;
	}
	
	//MS 08-07-13, ajout fonction d'attachement de fichiers aux pièces jointes.
	// arr_files : array des fichiers à joindre au mail : 
	// - array index_num=>chemin_complet_fichier,
	// OU
	// - array nouveau_nom=>chemin_complet_fichier
	function attachFiles($arr_files){
		if (empty($arr_files) || !isset($this->boundary) || !isset($this->type) || $this->type != "multipart/mixed"){
			$this->dropError("Impossible d'attacher des pièces jointes, variables necessaires non définies");
			return false;
		}
		$size = 0;
		foreach ($arr_files as $key=>$file){
			if (!is_file($file)){
				continue;
			}
			$filepath = $file;
			$filehandle = fopen($filepath, 'rb');
			$size_tmp = filesize($filepath);
			
			$size += $size_tmp;
			
			// a remplacer par une constante de conf ? 
			if($size >=  2097152){ 
				$this->dropError("Ajout impossible - taille des fichiers joints supérieure à 2M");
				return false;
			}
			
			$name= (is_string ($key)?$key:basename($file));
			
			$this->attachment .= "--".$this->boundary.$this->eol;
			$this->attachment .= "Content-Type: application/octet-stream; name=\"".$name."\"".$this->eol;
			$this->attachment .= "Content-Transfer-Encoding: base64".$this->eol;
			$this->attachment .= "Content-Disposition: attachment".$this->eol.$this->eol;
			
			$data = fread($filehandle, filesize($filepath));
			fclose($filehandle);                
			$encoded_file = chunk_split(base64_encode($data)); 
			$this->attachment .= $encoded_file.$this->eol.$this->eol;
			unset($encoded_file);
		}
		
		if(!empty($this->attachment)){
			return true;
		}else{
			return false;
		}
	}
	
	
	// Reprise fonction send_mail tel qu'elle existait auparavant, 
	// nouvelle var add_headers permet d'ajouter des headers, attention à la syntaxe
	// nouvelle var arr_files permet d'ajouter une liste de fichier à joindre (voir fonction attachFiles) 
	// => necessite type="multipart/mixed" & boundary définie
	function send_mail($to, $from, $sujet, $contenu, $type, $boundary="", $add_headers="", $arr_files=null) {
		global $db;
		
		if (!defined("gDontSendMail") || gDontSendMail!="1") {
			if(self::_isStatic()){
				$msg = new Message();
				return $msg->send_mail($to, $from, $sujet, $contenu, $type, $boundary, $add_headers, $arr_files);
			}
			
			//if(!isset($this->to)){
				$this->to = $to;
		//	}
			if(!isset($this->from)){
				if (empty($from))
					$from = gMail;
				$this->from = $from;
			}
			if(!isset($this->sujet)){
				$this->sujet = $sujet;
			}
			if(!isset($this->contenu)){
				$this->contenu = $contenu;
			}
			if(!isset($this->type)){
				$this->type = $type;
			}
			
			if(!isset($this->boundary)){
				$this->boundary = $boundary;
			}
			
			if(!isset($this->add_headers)){
				$this->add_headers = $add_headers;
			}
			
			if(!isset($this->eol)){
				$this->eol = PHP_EOL;
			}
			
			if($this->cc_to_sender){
				$this->cc_to = (!empty($this->cc_to)?$this->cc_to.", ".$this->from:$this->from);
			}
			
			// MS - l'ordre des headers pour les closes From,Reply-to, CC, BCC etc est important
			// il faudrait donc éventuellement retravailler la gestion des headers pour inclure plus facilement des headers contenus dans addHeaders à leur emplacement correct - TODO ...
			$headers="From:".$this->from."\n";
			if(isset($this->cc_to) && !empty($this->cc_to)){
				$headers.="CC:".$this->cc_to."\n";	
			}
			if(isset($this->reply_to) && !empty($this->reply_to)){
				$headers.="Reply-To:".$this->reply_to."\n";
			}
			
			if (!empty($this->boundary)){
				$headers.="MIME-Version: 1.0\nContent-Type: ".$this->type."; boundary=\"".$this->boundary."\"";
			}else{
				$headers.="MIME-Version: 1.0\nContent-Type: ".$this->type."; charset=\"UTF-8\";";
			}
			if(!empty($this->add_headers)) $headers.= $this->add_headers;
						

			if($this->type == "multipart/mixed" && !empty($this->boundary)){
				$content = "--".$this->boundary.$this->eol;
				$content .= "Content-Type: text/html; charset=\"UTF-8\"".$this->eol;
				$content .= "Content-Transfer-Encoding: 8bit".$this->eol.$this->eol;
				$content .= $this->contenu.$this->eol.$this->eol;
				$this->contenu = $content;
				$this->contenu = preg_replace("/([^\n]{870,900}\s*)([^\n]{1,30})/","\\1\r\n\\2",$this->contenu);
				
				unset($content);
			
				if(isset($arr_files) && is_array($arr_files) && !empty($arr_files)){
					$ok = $this->attachFiles($arr_files);
					if(!$ok){
						$this->dropError("Echec lors de l'ajout des pièces jointes");
						return false ;
					}
				}
				
				if(isset($this->attachment) && !empty($this->attachment)){
					$this->contenu .= $this->attachment;
				}
				
				$this->contenu	.= "--".$this->boundary."--";
			}
			
			if(in_array($this->type, array("multipart/alternative", "text/html"))) {
				//Découpage des lignes trop grandes pour un mail. Le saut de ligne n'est pas visible
				$this->contenu = preg_replace("/([^\n]{870,900}\s*)([^\n]{1,30})/","\\1\r\n\\2",$this->contenu);
			}
			
			trace("Send mail to ".$this->to." : ".$this->sujet);
		//	trace("Content mail : ".$this->contenu);
			
			return mail($this->to, "=?ISO-8859-1?B?".base64_encode(utf8_decode($this->sujet))."?=", $this->contenu, $headers);
			//return mb_send_mail($to, $sujet, $contenu, $headers);
		}
		
		$this->t_message['MES_OBJET'] = utf8_encode($sujet);
		if (preg_match("#<html>(.*)</html>#s",$contenu,$matche))
			$contenu = $matche[0];
		$this->t_message['MES_CONTENU'] = utf8_encode($contenu);
		$this->t_message['MES_LU'] = 0;
		$idFrom = $db->getOne("SELECT ID_USAGER from t_usager WHERE US_SOC_MAIL like '$from';");
		if ($idFrom)
			$this->t_message['MES_ID_USAGER_FROM'] = $idFrom;
		//recherche destinataires
		$emails = explode(",", $to);
		foreach ($emails as $email){
			$idsTo = $db->getAll("SELECT ID_USAGER from t_usager WHERE US_SOC_MAIL like '".trim($email)."';");
			if (count($idsTo) > 0)
				foreach ($idsTo as $idTo) {
					trace("Send mail to ".$to." : ".$contenu);
					$this->t_message['MES_ID_USAGER_TO'] = $idTo['ID_USAGER'];
					$this->create();
				}
		}
		return true;
	}
	
		//envoyer un mail à un groupe
	function send_mail_groupe($id_groupe,$from, $sujet, $contenu, $type, $boundary="", $add_headers="", $arr_files=null) {
		global $db;
		
		$usager = $db->getAll("SELECT US_SOC_MAIL from t_usager_groupe left join t_usager on t_usager.id_usager=t_usager_groupe.id_usager WHERE ID_GROUPE=".intval($id_groupe));
			foreach($usager as $idx=>$mail){
				$this->send_mail($mail['US_SOC_MAIL'], gMail, $sujet, $contenu, $type);
			}
	}
	
	
	function xml_export($entete="",$encodage="",$indent="") 
	{
		global $db;
		$xml='<t_message>';
		
		foreach($this->t_message as $id=>$val)
		{
			$xml.='<'.strtoupper($id).'>'.xml_encode($val).'</'.strtoupper($id).'>';
		}
		if (!empty($this->t_message["MES_ID_USAGER_FROM"])){
			$us_from = $db->getOne("SELECT ".$db->Concat('UF.US_PRENOM',"' '","UF.US_NOM")." FROM t_usager UF WHERE ID_USAGER = ".intval($this->t_message["MES_ID_USAGER_FROM"]));
			$xml.='<USAGER_FROM>'.xml_encode($us_from).'</USAGER_FROM>';
		}
		$xml.='</t_message>';
		
		return $xml;
	}
	
	// Fonction utilisée pour assurer la non-regression lors de l'appel de send_mail en environnement static
	private static function _isStatic() {
        $backtrace = debug_backtrace();
        return $backtrace[1]['type'] == '::';
    }
	
}

?>
