<?php

require_once(modelDir.'model_tape.php');
require_once(modelDir.'model.php');

class LibrairieElement extends Model 
{
	private $id_element;
	private $device;
	private $type;
	private $numero;
	private $tape;
	private $from_slot;
	
	private $tape_obj;
	
	private $arr_elements;
	
	public function __construct($id_elt=0)
	{
		$this->id_element=0;
		$this->device='';
		$this->type='';
		$this->numero=-1;
		$this->tape='';
		$this->from_slot='';
		
		$this->tape_obj=null;
		
		$this->arr_elements=array();
		
		if (!empty($id_elt))
			$this->getElement($id_elt);
	}
	
	public function getElement($id_elt)
	{
		global $db;
		
		$res=$db->Execute('SELECT * FROM t_librairie_element WHERE ID_ELEMENT='.intval($id_elt).'')->getRows();
		
		$this->id_element=$id_elt;
		$this->device=$res[0]['LE_DEVICE'];
		$this->type=$res[0]['LE_TYPE'];
		$this->numero=$res[0]['LE_NUMERO'];
		$this->tape=$res[0]['LE_ID_TAPE'];
		$this->from_slot=$res[0]['LE_FROM_SLOT'];
	}
	
	public function getElementFromIdTape($id_tape)
	{
		global $db;
		
		$res=$db->Execute('SELECT * FROM t_librairie_element WHERE LE_ID_TAPE=\''.$id_tape.'\'')->getRows();
		
		$this->id_element=$id_elt;
		$this->device=$res[0]['LE_DEVICE'];
		$this->type=$res[0]['LE_TYPE'];
		$this->numero=$res[0]['LE_NUMERO'];
		$this->tape=$res[0]['LE_ID_TAPE'];
		$this->from_slot=$res[0]['LE_FROM_SLOT'];
	}
	
	public function setDevice($dev)
	{
		$this->device=$dev;
	}
	
	public function getDevice()
	{
		return $this->device;
	}
	
	public function setType($typ)
	{
		$typ=substr(trim($typ),0,5);
		if ($typ=='drive')
			$this->type='drive';
		else if ($typ=='mail')
			$this->type='mail';
		else
			$this->type='slot';
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function setNumero($ref)
	{
		$this->numero=intval($ref);
	}
	
	public function getNumero()
	{
		return $this->numero;
	}
	
	public function setIdTape($id)
	{
		$this->tape=$id;
	}
	
	public function getIdTape()
	{
		return $this->tape;
	}
	
	public function setFromSlot($id_elt)
	{
		$this->from_slot=$id_elt;
	}

	public function getFromSlot()
	{
		return $this->from_slot;
	}
	
	public function setTapeObject($obj)
	{
		$this->tape_obj=$obj;
	}
	
	public function getTapeObject()
	{
		return $this->tape_obj;
	}
	
	public function isElementEmpty()
	{
		return empty($this->tape);
	}
	
	public function load($lecteur)
	{
		$this->execMtxCmd('load '.$this->getRefElement().' '.$lecteur->getRefElement());
	}
	
	public function unload($lecteur)
	{
		$this->execMtxCmd('unload '.$this->getRefElement().' '.$lecteur->getRefElement());
	}
	
	public function transfert($destination)
	{
		if ($destination->isElementEmpty())
		{
			$this->execMtxCmd('transfer '.$this->getNumero().' '.$destination->getNumero());
			return true;
		}
		else
			return false;
	}
	
	public function ouvrirTiroir()
	{
		$this->execMtxCmd('eepos 0 transfer '.$this->getNumero().' '.$this->getNumero());
	}
	
	public function updateLibElements()
	{
		// recuperation en ligne de commande
		
		$data_status=$this->execMtxCmd('status');
		$arr_status=explode("\n",$data_status);
		
		$this->arr_elements=array();
		
		foreach ($arr_status as $idx=>$ligne)
		{
			if ($idx==0)
				continue;
			
			$ligne=trim($ligne);
			
			if (!empty($ligne))
			{
				$arr_ligne=explode(' ',$ligne,5);
				$this->arr_elements[$idx]=array();
				$this->arr_elements[$idx]['device']=$this->device;
				$this->arr_elements[$idx]['numero']='';
				$this->arr_elements[$idx]['id_tape']='';
				//var_dump($ligne);
				//var_dump($arr_ligne);

				if ($arr_ligne[0]=='Storage' && $arr_ligne[1]=='Element')
				{
					$arr_slot=explode(':',$arr_ligne[2]);
					// emplacement cartouche
					$this->arr_elements[$idx]['numero']=$arr_slot[0];
					
					if (!empty($arr_slot[1]))
					{
						$this->arr_elements[$idx]['type']='slot';
						
						if ($arr_slot[1]=='Full')
						{
							$data_volume=explode('=',$arr_ligne[3]);
							$this->arr_elements[$idx]['id_tape']=trim($data_volume[1]);
						}
					}
					else
					{
						$arr_slot=explode(':',$arr_ligne[3]);
						
						$this->arr_elements[$idx]['type']='mail';
						
						if ($arr_slot[1]=='Full')
						{
							$data_volume=explode('=',$arr_ligne[4]);
							$this->arr_elements[$idx]['id_tape']=trim($data_volume[1]);
						}
					}
					//var_dump($arr_ligne[3]);
				}
				else if ($arr_ligne[0]=='Data' && $arr_ligne[1]=='Transfer' && $arr_ligne[2]=='Element')
				{
					$arr_slot=explode(':',$arr_ligne[3]);
					
					$this->arr_elements[$idx]['type']='drive';
					$this->arr_elements[$idx]['numero']=$arr_slot[0];
					$this->arr_elements[$idx]['from_slot']=0;
					
					if (!empty($arr_ligne[4]))
					{
						$data_volume=explode('=',$arr_ligne[4]);
						$this->arr_elements[$idx]['id_tape']=trim($data_volume[1]);
						
						$data_volume=explode(' ',trim($arr_ligne[4]));
						$this->arr_elements[$idx]['from_slot']=$data_volume[2];
					}
				}
			}
		}
		
		$this->save();
		
		
		return $this->getLibElement();
	}
	
	public function getLibElement()
	{
		return $this->arr_elements;
	}
	
	public function getLibData()
	{
		// recuperation de ma liste des element d'une device
		global $db;
		
		return $db->Execute('SELECT * FROM t_librairie_element WHERE LE_DEVICE=\''.$this->getDevice().'\'')->getRows();
	}
	
	public function save()
	{
		global $db;
		
		//difference entre les donnees getLibData et getLibElements
		//$this->updateLibElements();
		foreach ($this->getLibElement() as $elts)
		{
			$row_found=false;
			foreach ($this->getLibData() as $donnees_sql)
			{
				if ($elts['device']==$donnees_sql['LE_DEVICE'] && $elts['type']==$donnees_sql['LE_TYPE'] && $elts['numero']==$donnees_sql['LE_NUMERO'])//device, type, numero
				{
					$row_found=true;
					if ($elts['id_tape']!=$donnees_sql['LE_ID_TAPE']) // mise a jour
					{
						if (isset($elts['from_slot']))
							$db->Execute('UPDATE t_librairie_element SET LE_ID_TAPE=\''.$elts['id_tape'].'\',LE_FROM_SLOT=\''.$elts['from_slot'].'\' WHERE LE_DEVICE=\''.$elts['device'].'\' AND LE_TYPE=\''.$elts['type'].'\' AND LE_NUMERO=\''.intval($elts['numero']).'\'');
						else
							$db->Execute('UPDATE t_librairie_element SET LE_ID_TAPE=\''.$elts['id_tape'].'\' WHERE LE_DEVICE=\''.$elts['device'].'\' AND LE_TYPE=\''.$elts['type'].'\' AND LE_NUMERO=\''.intval($elts['numero']).'\'');
					}
					
					break;
				}
			}
			
			if ($row_found===false)
			{
				// si pas present dans les donnees SQL -> insertion
				if (isset($elts['from_slot']))
					$db->Execute('INSERT INTO t_librairie_element(LE_DEVICE,LE_TYPE,LE_NUMERO,LE_ID_TAPE,LE_FROM_SLOT) VALUES (\''.$elts['device'].'\',\''.$elts['type'].'\',\''.intval($elts['numero']).'\',\''.$elts['id_tape'].'\',\''.$elts['from_slot'].'\')');
				else
					$db->Execute('INSERT INTO t_librairie_element(LE_DEVICE,LE_TYPE,LE_NUMERO,LE_ID_TAPE) VALUES (\''.$elts['device'].'\',\''.$elts['type'].'\',\''.intval($elts['numero']).'\',\''.$elts['id_tape'].'\')');
			}
		}
	}
	
	private function execMtxCmd($params)
	{
		// pour "reproduire" le temps d'execution des commandes envoyee a la librairie
		//sleep(10);
		$cmd=sprintf(kMtxPath,'-f /dev/'.$this->getDevice().' '.$params);
		trace($cmd);
		return shell_exec($cmd);
	}
	
	public static function getElementFromDevice($dev)
	{
		global $db;
		
		$result=$db->Execute('SELECT * 
			FROM t_librairie_element 
				LEFT JOIN t_tape ON t_librairie_element.LE_ID_TAPE=t_tape.ID_TAPE 
				LEFT JOIN t_tape_set ON t_tape.ID_TAPE_SET=t_tape_set.ID_TAPE_SET
		WHERE LE_DEVICE=\''.$dev.'\' order by LE_NUMERO')->getRows();
		
		$arr_elts=array();
		
		foreach ($result as $res)
		{
			$tmp=new LibrairieElement();
			$tmp->setDevice($res['LE_DEVICE']);
			$tmp->setType($res['LE_TYPE']);
			$tmp->setNumero($res['LE_NUMERO']);
			$tmp->setIdTape($res['LE_ID_TAPE']);
			$tmp->setFromSlot($res['LE_FROM_SLOT']);
			
			$tmp_tape=new Tape();
			

			$tmp_tape->t_tape=array
			(
				'ID_TAPE'=>$res['ID_TAPE'],
				'ID_TAPE_SET'=>$res['ID_TAPE_SET'],
				'TAPE_STATUS'=>$res['TAPE_STATUS'],
				'TAPE_CAPACITE'=>$res['TAPE_CAPACITE'],
				'TAPE_UTILISE'=>$res['TAPE_UTILISE'],
				'TAPE_FULL'=>$res['TAPE_FULL'],
				'TAPE_ID_USAGER_CREA'=>$res['TAPE_ID_USAGER_CREA'],
				'TAPE_ID_USAGER_MOD'=>$res['TAPE_ID_USAGER_MOD'],
				'TAPE_DATE_CREA'=>$res['TAPE_DATE_CREA'],
				'TAPE_DATE_MOD'=>$res['TAPE_DATE_MOD'],
				'TAPE_LOCALISATION'=>$res['TAPE_LOCALISATION']
			);
			
			
			$tmp_tape->t_tape_set=array
			(
				'ID_TAPE_SET'=>$res['ID_TAPE_SET'],
				'TS_DEVICE'=>$res['TS_DEVICE'],
				'TS_DRIVE'=>$res['TS_DRIVE'],
				'TS_MEDIA'=>$res['TS_MEDIA'],
				'TS_NB_TAPES'=>$res['TS_NB_TAPES'],
				'TS_ID_TAPE_SET_MIROIR'=>$res['TS_ID_TAPE_SET_MIROIR'],
				'TS_ID_GROUPE_MAT'=>$res['TS_ID_GROUPE_MAT'],
				'TS_DESCRIPTION'=>$res['TS_DESCRIPTION'],
				'TS_EXTERNE'=>$res['TS_EXTERNE']
			);
			
			$tmp->setTapeObject($tmp_tape);
			
			$arr_elts[]=$tmp;
		}
		
		return $arr_elts;
	}
}

?>