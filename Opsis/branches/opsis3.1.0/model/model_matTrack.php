<?php
require_once(modelDir.'model.php');
class MatTrack extends Model
{
	var $t_mat_track;
	var $t_mat_track_champs;
	
	public function __construct($id_mat_track=0)
	{
		$this->t_mat_track_champs=array
		(
			'ID_MAT_TRACK'=>'int',
			'ID_MAT'=>'int',
			'MT_INDEX'=>'int',
			'MT_TYPE'=>'string',
			'MT_FORMAT'=>'string',
			'MT_FORMAT_LONG'=>'string',
			'MT_FORMAT_PROFILE'=>'string',
			'MT_FORMAT_GOP'=>'string',
			'MT_FORMAT_WRAPPING'=>'string',
			'MT_FORMAT_VERSION'=>'string',
			'MT_CODEC_ID'=>'string',
			'MT_CODEC_LONG'=>'string',
			'MT_DURATION'=>'float',
			'MT_START_TIME'=>'float',
			'MT_HEIGHT'=>'int',
			'MT_WIDTH'=>'int',
			'MT_DISPLAY_HEIGHT'=>'int',
			'MT_DISPLAY_WIDTH'=>'int',
			'MT_DAR'=>'string',
			'MT_SAR'=>'string',
			'MT_BIT_RATE'=>'int',
			'MT_PIX_FMT'=>'string',
			'MT_FRAME_RATE'=>'float',
			'MT_BIT_DEPTH'=>'int',
			'MT_LANGUAGE'=>'string',
			'MT_TIMECODE'=>'string',
			'MT_CHANNELS'=>'int',
			'MT_SAMPLE_RATE'=>'float',
            'MT_SCAN_TYPE'=>'string',
            'MT_SCAN_ORDER'=>'string'
		);
		
		$this->t_mat_track=array();
		
		if (!empty($id_mat_track) && is_numeric($id_mat_track))
			$this->getMatTrack(intval($id_mat_track));
	}
	
	public function getMatTrack($id_mat_track)
	{
		global $db;
		
		$result=$db->Execute('SELECT * FROM t_mat_track WHERE ID_MAT_TRACK=\''.intval($id_mat_track).'\'')->getRows();
		$result[0]['MT_DURATION_TIME']=secToTime($result[0]['MT_DURATION']);
		$result[0]['MT_BIT_RATE_KBS']=($result[0]['MT_BIT_RATE']/1024).' kb/s';
		$this->t_mat_track=$result[0];
	}
	
	public function set($var,$value)
	{
		if (isset($this->t_mat_track_champs[$var]) && !empty($this->t_mat_track_champs[$var]) && $var!='ID_MAT_TRACK')
		{
			switch($this->t_mat_track_champs[$var])
			{
				case 'int':
					$this->t_mat_track[$var]=intval($value);
					break;
				case 'float':
					$this->t_mat_track[$var]=floatval($value);
					break;
				case 'string':
					$this->t_mat_track[$var]=addslashes(htmlentities($value));
					break;
			}
		}
	}
	
	public function get($var)
	{
		if (isset($this->t_mat_track[$var]) && !empty($this->t_mat_track[$var]))
			return $this->t_mat_track[$var];
		
		return null;
	}
	
	public function save()
	{
		global $db;
		
		if (isset($this->t_mat_track['ID_MAT_TRACK']) && !empty($this->t_mat_track['ID_MAT_TRACK']) && intval($this->t_mat_track['ID_MAT_TRACK'])!=0)
		{
			// mise a jour ne la ligne en base
			$data_set='';
			$first_elt=true;
			foreach($this->t_mat_track_champs as $champ=>$type)
			{
				if ($champ!='ID_MAT_TRACK')
				{
					if ($first_elt)
						$first_elt=false;
					else
						$data_set.=',';
					
					switch ($type)
					{
						case 'int':
							$data_set.=$champ.'='.intval($this->t_mat_track[$champ]);
							break;
						case 'float':
							$data_set.=$champ.'='.floatval($this->t_mat_track[$champ]);
							break;
						case 'string':
							$data_set.=$champ.'="'.$this->t_mat_track[$champ].'"';
							break;
					}
				}
			}
			
			$sql='UPDATE t_mat_track SET '.$data_set.' WHERE ID_MAT_TRACK='.intval($this->t_mat_track['ID_MAT_TRACK']).'';
			echo $sql;
			$db->Execute($sql);
		}
		else
		{
			//creation de la ligne en base
			$str_fields='';
			$str_values='';
			$first_elt=true;
			
			foreach ($this->t_mat_track_champs as $champ=>$type)
			{
				if ($champ!='ID_MAT_TRACK')
				{
					if ($first_elt)
						$first_elt=false;
					else
					{
						$str_values.=',';
						$str_fields.=',';
					}
					
					$str_fields.=$champ;
					
					switch ($type)
					{
						case 'int':
							$str_values.=intval($this->t_mat_track[$champ]);
							break;
						case 'float':
							$str_values.=floatval($this->t_mat_track[$champ]);
							break;
						case 'string':
							$str_values.="'".$this->t_mat_track[$champ]."'";
							break;
					}
				}
			}
			
			$sql='INSERT INTO t_mat_track('.$str_fields.') VALUES ('.$str_values.')';
			$res=$db->Execute($sql);
			// recuperer ligne courante 
			$this->getMatTrack($db->Insert_ID());
		}
	}
	
	public function delete()
	{
		global $db; 
		
		$db->Execute('DELETE FROM t_mat_track WHERE ID_MAT_TRACK='.intval($this->t_mat_track['ID_MAT_TRACK']).'');
		unset($this->t_mat_track['ID_MAT_TRACK']);
	}
	
	
	public static function getAllMatTrack($id_mat)
	{
		global $db;
		
		$list_tracks=$db->Execute('SELECT ID_MAT_TRACK FROM t_mat_track WHERE ID_MAT='.intval($id_mat))->getRows();
		
		$arr_tracks=array();
		
		foreach($list_tracks as $id_track)
		{
			$arr_tracks[]=new MatTrack($id_track['ID_MAT_TRACK']);
		}
		
		return $arr_tracks;
	}

 	/** Export XML de l'objet
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	public function xml_export($entete="",$encodage="",$indent="") {
        
        $content="";
        
		if (!empty($entete)) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

        $content.=$indent."<t_mat_track>\n";
        foreach ($this->t_mat_track as $fld=>$val) {
            $content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
        }
        $content.=$indent."</t_mat_track>\n";

        // Encodage si demandé
        if(!empty($encodage)){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }

}

?>