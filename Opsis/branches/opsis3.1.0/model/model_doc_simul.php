<?

/**
 * Classe de simulation de sauvegarde d'un document :
 * les ordres SQL d'update / insert ne sont pas exécutés.
 * 
 */
require_once(modelDir.'model_doc.php');
class Doc_Simul extends Doc {

	var $debrief; //tableau de debrief des opérations

 	// Par ex: certains clients considèrent DOC_COTE comme obligatoire & unique, d'autres pas.

 	private $arrFieldsToSaveWithIdLang = array(

 	      "DOC_TITRE" , "DOC_SOUSTITRE" , "DOC_RES","DOC_SEQ", "DOC_NOTE_INTERNE"
 	      , "DOC_TITRE_COL","DOC_AUTRE_TITRE", "DOC_RECOMP", "DOC_ACC", "DOC_IM_ARCH",
 	      "DOC_COMMENT","DOC_COMMISSION", "DOC_COPYRIGHT", "DOC_PROCAV","DOC_RES_CAT", "DOC_VISA","DOC_AUD"
 	      ,"DOC_ACCES", "DOC_ID_ETAT","DOC_ID_ETAT_DOC", "DOC_XML", "DOC_VAL", "DOC_LEX");

// VP 26/06/09 : ajout champ DOC_DATE_ETAT
 	private $arrFieldsToSaveWithoutIdLang= array(
												 
		 "DOC_TITREORI", "DOC_ORI_TYPE_SON","DOC_ORI_FORMAT","DOC_ORI_COULEUR" ,"DOC_ID_FONDS","DOC_ID_MEDIA",
		 "DOC_DIFFUSEUR" ,"DOC_ID_GENRE","DOC_ID_TYPE_DOC" , "DOC_ID_GEN","DOC_TITREORI",
		 "DOC_TYPE","DOC_DUREE", "DOC_NB_EPISODES","DOC_COTE_ANC", "DOC_COTE","DOC_DATE_DIFF","DOC_DATE_DIFF_FIN","DOC_DROITS",
		 "DOC_DATE","DOC_DATE_ETAT","DOC_DATE_PROD","DOC_DATE_PV_FIN", "DOC_DATE_PV_DEBUT","DOC_NOTES", "DOC_VI","DOC_AUD"
		 , "DOC_AUD_PDM","DOC_ID_USAGER_MODIF","DOC_DATE_MOD","DOC_LIEU_PV", "DOC_NUM", "DOC_TCIN", "DOC_TCOUT");
	

    private $arrTextFields=array("doc_titre","doc_soustitre","doc_titre_col","autre_titre",
			"doc_res","doc_seq","doc_notes","doc_recomp","doc_acc",
			"doc_comment","doc_procav","doc_note_interne"); // liste des champs de type texte dont il faut traiter les apostrophes

    // Constructeur

    function __construct($id=null,$version=""){
    	parent::__construct($id,$version);
    	$this->debrief=array();
		if (defined('gDocUniqueFields')) $this->uniqueFields=(array)unserialize(gDocUniqueFields);
    }




    function updateChampsXML($type,$arrLangues=null){
        global $db;
        	return true;
    }


    /**
     * Création en base d'un nouveau doc avec les infos de base (initialisées à la créa objet)
     * IN : id_mat (si on crée des doc_mat dans la foulée)
     * OUT : nv doc créé en base, var de classe ID_DOC renseignée avec nv ID
     *
     */

    function initDoc($id_mat=""){
        global $db;
        if (empty($this->t_doc['DOC_TITRE'])) return false;
		$this->t_doc['DOC_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];
		$this->t_doc['DOC_ID_USAGER_MODIF']=$_SESSION['USER']['ID_USAGER'];
		$this->debrief['doc']['new']++;
        if($id_mat!=""){
            $this->insererDocMat($id_mat);
        }
        $this->linkToAnotherDoc();	//Si on crée un nv document depuis les liens doc.
    	return true;
    }


	/**
	 * Note : OBSOLETE ! A retirer partout, utiliser updateDocNum à la place
	 */

	function metAJourDocNum($id_mat){
	    global $db;
	    include_once(modelDir.'model_materiel.php');
		$mat=new Materiel();
		$mat->t_mat['ID_MAT']=$id_mat;
		$mat->getMat();
	    if (is_file($mat->getFilePath())){
	        return TRUE;
	    }else{
	        return FALSE;
	    }
	}

	/** Met à jour le champ DOC_NUM indiquant qu'un doc est visionnable
	 *  Pour cela, on récupère la liste des matériels liés puis on vérifie qu'au moins un fichier existe,
	 *  qu'il est d'un format visionnable
	 *  IN : tab de classe t_doc_mat
	 *  OUT : maj DOC_NUM de l'objet
	 */

	function updateDocNum() {
		global $db;
		$doc_num='0';
		require_once(libDir."class_visualisation.php");
		if (empty($this->t_doc_mat) || !$this->t_doc_mat[0]['MAT']) $this->getMats();
		//debug($this->t_doc_mat,'violet');
		foreach ($this->t_doc_mat as $idx=>$dm) {
			if ($dm['MAT']->hasfile=='1' && $doc_num=='0' && in_array($dm['MAT']->t_mat['MAT_FORMAT'],Visualisation::getFormatsForVisu())) $doc_num='1';
		}
		$this->t_doc['DOC_NUM']=$doc_num;
		
	}


    /**
     * Duplication du document
     * IN : this
     * OUT : nouveau document dupliqué.
     */
    function duplicateDoc(){
    global $db;

		if (empty($this->arrVersions)) $this->getVersions(); // Récup des infos si pas encore loadées
		if (empty($this->t_doc_lex)) $this->getLexique();
		if (empty($this->t_doc_val)) $this->getValeurs();

        $dupDoc=new Doc_Simul();
        $dupDoc=$this; // Recopie des infos
        unset($dupDoc->t_doc['ID_DOC']); // pour éviter les effets de bord ? xxx utile ds le cas de simul ?
        $dupDoc->doc_trad=1; // pour générer les autres versions
        $dupDoc->initDoc(); // récupération du nouvel ID
        $dupDoc->save(1); // Sauvegarde avec désactivation du checkExist.

        $dupDoc->saveDocLex();
        $dupDoc->saveDocVal();
        return $dupDoc;
    }


	
	
    /**
     * Sauve le doc en base
     * 1. Si créa d'un nv doc, vérifie si un doc existe (d'après DOC_COTE) et abandonne si oui
     * 2. Sinon, crée un nv document provisoire et récupère l'ID.
     * 3. QQ soit le mode, les valeurs des champs sont updatées en base...
     * 4. ... en deux phases : update pour la langue demandée, puis update pr ttes les langues dispo
     *
     * IN : langue (version à sauver), flag dupliquer O/N,
     * OUT : infos sauvées en base ou retour à false en cas d'erreur.
     */
    function save($dupliquer=0,$failIfExists=true){
        global $db;

		if ($dupliquer==0) {
			$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
		}
		// VP 8/11/2011 : test existence sur autres champs que DOC_TITRE
        if (empty($this->t_doc['DOC_TITRE']) && empty($this->t_doc['DOC_SOUSTITRE'])
        	&& empty($this->t_doc['DOC_TITREORI']) && empty($this->t_doc['DOC_AUTRE_TITRE']) && empty($this->t_doc['DOC_COTE'])) return false;
		
        if (empty($this->t_doc['ID_DOC']) || $this->uniqueFields[0]=='ID_DOC' ) {
        	if ($dupliquer==0) {
	           if (!empty($this->uniqueFields)) { //Le test d'unicité se base sur le param config uniqueFields qui contient les champs constituant la clé unique
					$sql="SELECT ID_DOC FROM t_doc WHERE 1=1 ";
					foreach ($this->uniqueFields as $fld) $sql.=" AND ".strtoupper($fld)."=".$db->Quote($this->t_doc[strtoupper($fld)]);
					if (!in_array('ID_DOC',$this->uniqueFields)) $sql.=" AND ID_DOC!=".intval($this->t_doc['ID_DOC']);
		            //debug($sql,'pink');
		            $existDeja=$db->GetOne($sql);
					debug($existDeja,'orange');
		            if (!$existDeja){
		                $this->initDoc();
		            }
		            else {
		            	$this->t_doc['ID_DOC']=$existDeja;
		                if ($failIfExists) {
		                	$this->dropError(kErrorCoteExistant);
			                return FALSE;
		                }
	            	}
	           } else $this->initDoc(); //pas de test d'unicité => on crée systématiquement un nv ID_DOC
        	}
        }
		$this->t_doc['DOC_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_doc['DOC_ID_USAGER_MODIF']=$_SESSION['USER']['ID_USAGER'];
		$sql="UPDATE t_doc SET ";
		
		foreach ($this->t_doc as $fld=>$val) {

		if (strpos($fld,'DOC_')===0 && in_array($fld,$this->arrFieldsToSaveWithIdLang))
				$tab[]= " ".strtoupper($fld)."=".$db->Quote($val)." ";
		}
		if (isset($tab)) { //il y a des champs ? on execute l'ordre
			$sql.=implode(",",$tab);
			$sql.=" WHERE ID_DOC=".intval($this->t_doc['ID_DOC'])." AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
	       
	        //$db->Execute($sql);
		}
        unset($tab);

		$sql="UPDATE t_doc SET ";

		foreach ($this->t_doc as $fld=>$val) {

		if (strpos($fld,'DOC_')===0 && in_array($fld,$this->arrFieldsToSaveWithoutIdLang))
				$tab[]= " ".strtoupper($fld)."=".$db->Quote($val)." ";
		}
		if (isset($tab)) { //il y a des champs ? on execute l'ordre
			$sql.=implode(",",$tab);
			$sql.=" WHERE ID_DOC=".intval($this->t_doc['ID_DOC']);
	      
	        //$db->Execute($sql);
		}
		$this->debrief['doc']['update']++;
        // MAJ de DOC_SEQ_TC du document parent (s'il y en a un) (i.e. : liste des titres des s�quences associ� � un doc)
        if (!empty($this->t_doc['DOC_ID_GEN'])){$this->updateParentTC();}

        // I.5. G�n�ration des versions traduites. On ne g�n�re pas de version traduite pour les documents dupliqu�s
        if($this->doc_trad == 1 && $dupliquer==0){ $this->saveVersions();}


        // By LD : ajout du lien entre doc. Si on est en train de créer un document fils ou parent
        // depuis la page de liens, on crée directement le lien.
        // doc2link contient l'id du doc à lier au doc en cours de création.
        // relation détermine le sens du lien : doc parent ou doc fils.
        if ($dupliquer== 0) {
			$this->linkToAnotherDoc();
	      } // if dupliquer =0
    return true;
    }


	/** Sauve les informations des DOC_LEX
	 *  IN : tableau DOC_LEX
	 *  OUT : base mise à jour
	 */
	function saveDocLex($purge=true) {
		global $db;
		// if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		 if (empty($this->t_doc_lex)) return false;
		
		foreach($this->t_doc_lex as $idx=>$docLex) {

			if ($docLex['LEXIQUE']) unset($docLex['LEXIQUE']); //On ôte l'objet encapsulé s'il existe

			// On notera que ID_DOC est celui en cours et pas celui contenu dans le tableau.
			// Effectivement, en cas de duplication, on récupère le tableau avec l'ID_DOC de l'original
			//$docLex['ID_DOC']=$this->t_doc['ID_DOC'];


			if ($docLex['ID_LEX'] || $docLex['ID_PERS']) {
				$this->debrief['doc_lex']++;
	
				if (!empty($docLex['LEX_PRECISION'])) {
				$this->debrief['doc_lex_precision']++;
				}
			}
		}
		return true;
	}
	
	
	function saveDocFest($purge=true) {
		global $db;
		// if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		 if (empty($this->t_doc_fest)) return false;
		$db->StartTrans();
		// Purge générale ? Utile si on crée une fiche en mode édition. Pas en mode import
		
		foreach ($this->t_doc_fest as $idx=>&$docFest) {
			
			if ($docFest['ID_FEST']=='' && $docFest['FEST_LIBELLE']!='' && $docFest['FEST_ANNEE']) //Récup d'ID, par exemple, en cas d'import, où pas d'ID
				$docFest['ID_FEST']=$db->GetOne('SELECT ID_FEST from t_festival WHERE FEST_LIBELLE LIKE '.$db->Quote($docFest['FEST_LIBELLE']).
											  ' AND FEST_ANNEE='.$db->Quote($docFest['FEST_ANNEE']));
			
			if ($docFest['ID_FEST'] && $docFest['ID_FEST']!='') { //vérif existence id_fest=> peut-être vide si l'on choisit une valeur parmi une dropliste qui
										 // qui contient une valeur vide.
				//Précaution en cas de duplication : on spécifie bien que l'ID_DOC est celui en cours.
				// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_DOC originel dedans.
				$docFest['ID_DOC']=$this->t_doc['ID_DOC'];
				
				$this->debrief['doc_fest']++;
				

			

			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocVal);trace($sql); return false;} else return true;
	}		
	

	/**
	 * Sauve les informations d'un lien DOC / VALEUR
	 * IN : tableau t_doc_val, $purge=>vidage t_doc_val avant full insert
	 * OUT : base mise à jour
	 * NOTE : si on a des champs VALEUR et TYPE_VAL mais pas d'ID_VAL (par exemple pour les imports)
	 * 	on va tenter de le récupérer
	 */
	function saveDocVal($purge=true) {
		global $db;
		 //if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		 if (empty($this->t_doc_val)) return false;

		foreach ($this->t_doc_val as $idx=>$docVal) {
			
			if ($docVal['ID_VAL']=='' && $docVal['VALEUR']!='') //Récup d'ID, par exemple, en cas d'import, où pas d'ID
				$docVal['ID_VAL']=$db->GetOne('SELECT ID_VAL from t_val WHERE VALEUR LIKE '.$db->Quote($docVal['VALEUR']).
											  ' AND VAL_ID_TYPE_VAL='.$db->Quote($docVal['ID_TYPE_VAL']));
			
			if ($docVal['ID_VAL'] && $docVal['ID_VAL']!='') {
										 //vérif existence id_val=> peut-être vide si l'on choisit une valeur parmi une dropliste qui
										 // qui contient une valeur vide.
				//Précaution en cas de duplication : on spécifie bien que l'ID_DOC est celui en cours.
				// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_DOC originel dedans.
				$docVal['ID_DOC']=$this->t_doc['ID_DOC'];
				$this->debrief['doc_val']++;
			} else $this->dropError(kErreurDocSauveDocValNoId);
		}
		return true;
	}

	function saveDocRedif() {
		// if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		 if (empty($this->t_doc_redif)) return false;
		global $db;

		foreach ($this->t_doc_redif as $idx=>$docRed) {

			//Précaution en cas de duplication : on spécifie bien que l'ID_DOC est celui en cours.
			// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_VAL originel dedans.
			$docRed['ID_DOC']=$this->t_doc['ID_DOC'];

			$sql="INSERT INTO t_doc_redif (";

			$sql.=implode(",",array_keys($docRed));
			$sql.=") values ('";
			$tmp=implode("§",array_values($docRed));
			$sql.=str_replace("§","','",simpleQuote($tmp));
			$sql.="')";
			//$ok=$db->Execute($sql);
			$this->debrief['doc_redif']++;
		}
		return true;
	}

/** Supprime tous les liens Doc_Mat d'un document en base
 *  NOTE : ne touche pas à la structure t_doc_mat
 */
	function purgeDocMat() {
		return true;			
	}

/**
	 * Sauve les associations DOCUMENT / MATERIEL ainsi que les VALEURS associées à cette association
	 * IN : tableau de classe t_doc_mat (et son sous tableau t_doc_mat_val), withMats pour créer les mat à la volée
	 * OUT : base mise à jour
	 */
	function saveDocMat($withMats=true) {
		global $db;
		// if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
		 if (empty($this->t_doc_mat)) return false;
		require_once(modelDir.'model_materiel.php');
		$db->StartTrans();


		foreach ($this->t_doc_mat as $idx=>&$docMat) {
			if (empty($docMat['ID_DOC'])) $docMat['ID_DOC']=$this->t_doc['ID_DOC'];
			if (!empty($docMat['ID_DOC'])) {

				if ($withMats) {
					// Cas "normal" => on sauve le matériel systématiquement parce qu'il faut au moins sauver le format.
					$mat=new Materiel();
					$mat->t_mat['ID_MAT']=$docMat['ID_MAT'];
					$mat->t_mat['MAT_FORMAT']=$docMat['MAT_FORMAT']; 
					if (!$mat->checkExist()) { //Nvelle ligne dans le formulaire doc_mat => insert
							$this->debrief['mat']['new']++;
					} else {
							$mat->id_mat_ref=$docMat['ID_MAT']; //Mat. existant => update
							$this->debrief['mat']['update']++;
					}
					$this->dropError($mat->error_msg);
					unset($mat);
				}	
				
				// Note sur le champ MAT_FORMAT : il est systématique présent dans le formulaire doc_mat
				// mais il ne relève pas du lien doc_mat mais du matériel. Il faut donc le retirer après sauv. matériel.
				// C'est pas super propre.
				unset($docMat['MAT_FORMAT']);

				$docMat['ID_DOC']=$this->t_doc['ID_DOC']; // Synchro ID_DOC dans doc_mat et ID_DOC du DOC
				$dmVal=$docMat['t_doc_mat_val'];unset($docMat['t_doc_mat_val']);
				$docMat['ID_DOC_MAT']=$this->checkDocMatExists($docMat); //Récup de l'ID_DOC_MAT s'il existe

				if (empty($docMat['ID_DOC_MAT'])) {
					
					$this->debrief['doc_mat']['new']++;
				
				} else {
					$this->debrief['doc_mat']['update']++;			
				}

				if (!empty($dmVal)) { //sauvegarde des valeurs associées
					$this->debrief['doc_mat_val']++;
				}
				
			}
		}
		return true;
	}
	
	/**
	 * Actualise le lien DOC_MAT de séquences avec les DOCTCIN et DOCTCOUT de ces séquences.
	 * Utilisé en finupload pour créer les liens DOC_MAT des séquences. C'est notamment le cas pour la 
	 * LNR où les séquences sont créées par import et le matériel uploadé APRES.
	 * IN : id_doc PARENT, id_mat PARENT
	 */
	function saveDocMatForSequences($id_mat) {
		global $db;
		$sql="replace t_doc_mat (id_mat,id_doc,dmat_tcin,dmat_tcout) 
				select dm.id_mat,ds.id_doc,ds.doc_tcin, ds.doc_tcout
				from t_doc_mat dm, t_doc ds, t_doc dp
				where dm.id_doc=dp.id_doc
				and dp.id_doc=ds.doc_id_gen
				and dp.id_lang='fr' and ds.id_lang='fr'
				and dp.id_doc=".intval($this->t_doc['ID_DOC'])."
				and dm.id_mat=".intval($id_mat)."
				";
		//$ok=$db->Execute($sql);	
	}
	


	function linkToAnotherDoc()  {
	        global $db;
	        if (!empty($this->id_doc2link)) {
    		if ($this->relation=="SRC") {$fils="DST";$parent="SRC";} else {$fils="SRC";$parent="DST";}

        	$sql="INSERT IGNORE INTO t_doc_lien (ID_DOC_".$fils.",ID_DOC_".$parent.",DD_DUREE)
						select ".intval($this->t_doc['ID_DOC']).",".intval($this->id_doc2link).",'".$this->t_doc['DOC_DUREE']."'";
			$this->debrief['doc_lien'][$this->relation]++;
			return true;
   			}
	}
	function updateParentTC() {
	        return true;
	}

	/** Sauve un document dans les autres langues disponibles dans OPSIS
	 *  IN : tableau de classe t_doc, $tabLangues (opt) = tableau des langues que l'on veut sauver
	 *  OUT : bdd màj avec insertion t_doc des nv langues avec contrôle d'existence
	 * 		  + recopie des t_doc_lex_precision pour ces nouvelles versions
	 *  NOTES : pas d'update, juste insertion (contrôle si version existe)
	 * 			se base sur le tableau des champs dans t_doc -> on a retiré l'insertion de ts les champs (15/02/08)
	 */
	function saveVersions($tabLangues='') {
	        global $db;
			//si on ne passe pas de tableau, on utilise le tableau de ttes les langues en session
			if ($tabLangues=='') $tabLangues=$_SESSION['arrLangues']; 
	        //if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
			$ok=true;
	        
	            foreach($tabLangues as $lg){
                // I.5.a. On s'assure que la version � cr�er n'existe pas d�j�
                $existVersion=$db->Execute("SELECT ID_DOC FROM t_doc WHERE ID_DOC='".intval($this->t_doc['ID_DOC'])."' AND ID_LANG=".$db->Quote($lg));
                 if($existVersion->RecordCount()==0){   
                    $this->debrief['doc_version'][$lg]++;
                }
            }
    if (!$ok) return false;
	return true;
	}



    /**
     * Supprime un document de la base ou bien juste une version (si id_lang est passé)
     * En cas de suppression totale du doc, celui-ci est effacé + liens au lexique, matériel, valeurs et panier
     * IN : id_lang (opt, pour supprimer juste une version)
     * OUT : update BDD
     */

    function delete($id_lang=""){
        global $db;

        //if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}

        if (empty($id_lang)) {
	        // II.1.C. Suppression des enregistrements li�s
	    	$this->debrief['doc_delete'][$id_lang]++;
        } else {
        	if ($this->t_doc['DOC_ID_GEN']!=0) {
	            $this->updateParentTC();
	        }
	    	$this->debrief['doc_delete']++;
        }
        return true;
    }

	/** Supprime les séquences ainsi que les liens de ces séquences : matériel, valeur, lex, etc
	 * 	IN : array t_doc_seq de l'objet (chargé automatiquement si vide)
	 *  OUT : update BDD puis vidage tableau t_doc_seq
	 */
	function deleteSequences() {
		global $db;

		if (empty($this->t_doc_seq)) $this->getSequences();
		foreach ($this->t_doc_seq as $seq) {
	        if (!empty($seq['ID_DOC'])) {
		       $this->debrief['doc_seq_delete']++;
		       }
		}
		unset ($this->t_doc_seq);
		
			
	}



	/**
	 * Insère une séquence d'un document en base + lien avec matériel + image représentative + maj doc_seq parents
	 * IN : TC IN, TC OUT, DUREE, TITRE SEQUENCE, LANGUE
	 * OUT : Insertion en base
	 */
    function insertSequence($doc) {
    	global $db;
    	$mySeq=new Doc_Simul();

    	$mySeq->updateFromArray($doc,false);
    	$mySeq->t_doc['DOC_DUREE']=secToTime(tcToSec($doc["DOC_TCOUT"])-tcToSec($doc["DOC_TCIN"]));
    	$mySeq->t_doc['DOC_ID_GEN']=$this->t_doc['ID_DOC'];    	
    	$mySeq->t_doc['DOC_ID_MEDIA']=$this->t_doc['DOC_ID_MEDIA']; //Héritage automatique
    	$mySeq->t_doc['DOC_ID_FONDS']=$this->t_doc['DOC_ID_FONDS'];
    	$mySeq->t_doc['DOC_DATE_PROD']=$this->t_doc['DOC_DATE_PROD'];
    	$mySeq->t_doc['IMAGEUR']=$this->t_doc['IMAGEUR'];
    	$mySeq->t_doc['DOC_NUM']=$this->t_doc['DOC_NUM'];
      	$mySeq->t_doc['ID_LANG']=$this->t_doc['ID_LANG'];
      	
      	// By LD 24/04/08 => ajout récup tableaux LEX et VAL
      	$FORM=$_POST; //Pour le moment, on analyse directement le POST, il faudrait faire passer le POST dans $doc 
      					//dans frame/processSequence
		if (!empty($FORM['t_doc_lex']))
     	foreach ($FORM['t_doc_lex'] as $idx=>$lex) {
			if (isset($lex['ID_LEX']) || isset($lex['ID_PERS'])) {$lastID=$idx;$FORM['t_doc_lex'][$idx]['ID_DOC']=$mySeq->t_doc['ID_DOC'];}
			if (isset($lex['DLEX_ID_ROLE'])) {$FORM['t_doc_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM['t_doc_lex'][$idx]);}
			if (isset($lex['DLEX_ID_ROLE'])) {$FORM['t_doc_lex'][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM['t_doc_lex'][$idx]);}
			if (isset($lex['ID_TYPE_LEX'])) {$FORM['t_doc_lex'][$lastID]['ID_TYPE_LEX']=$lex['ID_TYPE_LEX'];unset($FORM['t_doc_lex'][$idx]);}
			if (isset($lex['DLEX_REVERSEMENT'])) {$FORM['t_doc_lex'][$lastID]['DLEX_REVERSEMENT']=$lex['DLEX_REVERSEMENT'];unset($FORM['t_doc_lex'][$idx]);}
			if (isset($lex['LEX_PRECISION'])) {$FORM['t_doc_lex'][$lastID]['LEX_PRECISION']=$lex['LEX_PRECISION'];unset($FORM['t_doc_lex'][$idx]);}
			if (isset($lex['ID_TYPE_DESC'])) {$FORM['t_doc_lex'][$lastID]['ID_TYPE_DESC']=$lex['ID_TYPE_DESC'];unset($FORM['t_doc_lex'][$idx]);}
		}

		if (!empty($FORM['t_doc_val']))
		foreach ($FORM['t_doc_val'] as $idx=>$val) {
			if (isset($val['ID_VAL'])) {$lastID=$idx;$FORM['t_doc_val'][$idx]['ID_DOC']=$mySeq->t_doc['ID_DOC'];}
			if (isset($val['ID_TYPE_VAL'])){
			if(!isset($FORM['t_doc_val'][$lastID]['ID_TYPE_VAL'])) {$FORM['t_doc_val'][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];}
				unset($FORM['t_doc_val'][$idx]);
			}
		}
		//Source import (mapFields) ? on la prend, sinon on prend la source formulaire
		if (!empty($doc['t_doc_lex']))	$mySeq->t_doc_lex=$doc['t_doc_lex']; else $mySeq->t_doc_lex=$FORM['t_doc_lex']; 
		if (!empty($doc['t_doc_val']))  $mySeq->t_doc_val=$doc['t_doc_val']; else $mySeq->t_doc_val=$FORM['t_doc_val'];
		// Fin des modifs du 24/04/08

    	
    	//trace(print_r($mySeq,true));    	
    	$rtn=$mySeq->save(0,false);

    	$mySeq->saveDocLex();
    	$mySeq->saveDocVal();
    	//$mySeq->saveVersions(); pas utile car on crée déjà les versions au moment du SAVE
    	$mySeq->updateChampsXML('DOC_LEX');$mySeq->updateChampsXML('DOC_VAL');$mySeq->updateChampsXML('DOC_XML');$mySeq->updateChampsXML('DOC_CAT');
		$this->dropError($mySeq->error_msg); //transmission 
		
		$this->debrief['doc_seq'][]=$mySeq->debrief; //héritage simulation séquence
		$i=count($this->debrief['doc_seq'])-1;

		/*Vignette automatique, by LD 27/12/07
		On récupère la liste des id_mat du parent, car c'est les mêmes que pour le fils.
		NOTE : à voir si cette dernière règle est vraie, stt en cas de chevauchement de matériels sur les TC
		pour le moment, on considère que c'est le cas... */

		if (empty($this->t_doc_mat)) $this->getMats();
		foreach ($this->t_doc_mat as $dmat) $matListe[]=$dmat['ID_MAT'];
		
		if (!empty($matListe)) {
			$sql="SELECT DISTINCT i.ID_IMAGE
						FROM t_image i, t_mat m
						WHERE i.ID_IMAGEUR=m.MAT_ID_IMAGEUR AND i.IM_TC>'".$doc["DOC_TCIN"]."' AND m.ID_MAT IN ('".implode("','",$matListe)."')
						ORDER BY i.IM_TC";

			$id_img=$db->getOne($sql); //On récupère juste l'id de la première image trouvée
			//trace('VIGNETTE SEQUENCE '.$id_img.' >>> '.$sql);
			if (!empty($id_img)) $this->debrief['doc_seq'][$i]['vignette']='1';
		}

		//Maj DOC_SEQ
		$this->updateParentTC();
		unset ($mySeq); 
		return 1; //on renvoie un id faux, mais qui n'est pas 0 pour dire que tt va bien.
    }

    function updateSequence($doc) { //OBSOLETE
  		$this->debrief['doc_seq']['update']++;
    }

	/**
	 * Sauvegarde des séquences par appel de insertSequence() ou updateSequence() suivant le cas
	 * IN :
	 * OUT : Insertion en base
	 */
    function saveSequences() {
      //  if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
    	global $db;
		$doc_seq_tc="";
		//$db->Execute("UPDATE t_doc SET DOC_ID_GEN='0' WHERE DOC_ID_GEN=".$db->Quote($this->t_doc['ID_DOC']));
		foreach ($this->t_doc_seq as $idx=>$doc) {
			if (!empty($doc['DOC_TITRE'])) { //by LD 27/12=> controle doc_titre pour les imports
				$doc_seq_tc.=$doc["DOC_TITRE"].";";
					$idExtrait=$this->insertSequence($doc);
			}
		}
		return true;
    }


    function updateMateriels() {
    	global $db;
    	foreach ($this->t_doc_mat as $key=>$mat) {
    		if ($mat['action']=='update') {
    			$this->debrief['doc_mat']['update']++;
    		}
    	}
    }


    function saveDocLies() {
    	// if (empty($this->t_doc['ID_DOC'])) {$this->dropError(kErrorDocNoId);return false;}
    	
		$ordre=0;
		foreach ($this->arrDocLiesSRC as $idx=>&$dl) {

			if (!empty($dl['ID_DOC_DST'])) {
				$this->debrief['doc_lien']['src']++;
			}
		}
		$ordre=0;
		foreach ($this->arrDocLiesDST as $idx=>&$dl) {
			if (!empty($dl['ID_DOC_SRC'])) {
				$this->debrief['doc_lien']['dst']++;
			}
		}

		return true;

    }

	/**
	 * Sauve l'association à un imageur
	 */
	function saveImageur($imageur) {
		$this->debrief['doc_imageur']++;
    	return true;
	}

    
	function saveVignette($from,$id) {
		if ($from=='doc_acc')
			{$fld='DOC_ID_DOC_ACC';
			 $fld2='DOC_ID_IMAGE';
			}
			else
			{$fld='DOC_ID_IMAGE';
			 $fld2='DOC_ID_DOC_ACC';
			}
	    $this->debrief['vignette']=1;
		$this->t_doc[$fld]=$id;
	}


	/** Effectue le mappage entre un tableau et des champs de doc
	 *
	 */
	function mapFields($arr) {

		require_once(modelDir.'model_personne.php');
		require_once(modelDir.'model_lexique.php');						
		require_once(modelDir."model_val.php");		
		require_once(modelDir.'model_materiel.php');
		require_once(modelDir.'model_festival.php');
		
								
		foreach ($arr as $idx=>$val) {

				
				$myField=$idx;
				// analyse du champ
				if ($myField=='ID_DOC') $this->t_doc['ID_DOC']=$val;
				$arrCrit=split("_",$myField);
				switch ($arrCrit[0]) {
					case 'DOC': //champ table DOC
						$val=trim($val); //Important, car souvent, les tags ID3 ont des espaces supplémentaires
						$flds=split(",",$myField);
						if (!empty($val)) foreach ($flds as $_fld) $this->t_doc[$_fld]=$val;
					break;

					case 'L': //lexique
						if (!$val!=='') {
							if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
							
							foreach($val as $thisVal) {
								$myLex=new Lexique();
								if ($thisVal['LEX_TERME'])
									$myLex->t_lexique['LEX_TERME']=$thisVal['LEX_TERME']; else
									$myLex->t_lexique['LEX_TERME']=$thisVal['VALEUR'];
								$myLex->t_lexique['LEX_ID_TYPE_LEX']=$arrCrit[3]; //par déft, extrait du champ
								
								if ($thisVal['ID_LEX']) $myLex->t_lexique['ID_LEX']=$thisVal['ID_LEX'];
								if ($thisVal['LEX_ID_TYPE_LEX']) $myLex->t_lexique['LEX_ID_TYPE_LEX']=$thisVal['LEX_ID_TYPE_LEX'];
								
								$myLex->t_lexique['ID_LANG']=$this->t_doc['ID_LANG'];
								
								if ($myLex->checkExist(false)) $this->debrief['lexique']['update']++; else
								{
									$this->debrief['lexique']['new']++;
									foreach ($_SESSION['arrLangues'] as $lg) {
										if ($lg!=$this->t_doc['ID_LANG']) $arrVersions[$lg]=$myLex->t_lexique;
										$this->debrief['lexique'][$lg]++;
									}
								}
		
								//Lien doc_lex
								$this->t_doc_lex[]=array('DLEX_ID_ROLE'=>($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]),
														 'ID_TYPE_DESC'=>($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]),
														 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
														 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
														 'ID_LEX'=>$myLex->t_lexique['ID_LEX']);
							}
						}
					break;


					case 'P': //personne
						if ($val!=='') {
							if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
							foreach($val as $thisVal) {
							
								$myPers=new Personne();
								$myPers->t_personne['PERS_NOM']=$thisVal['VALEUR'];
								$myPers->t_personne['PERS_ID_TYPE_LEX']=$arrCrit[3]; //par déft
	
								if ($thisVal['ID_PERS']) {
										$myPers->t_personne['ID_PERS']=$thisVal['ID_PERS'];
										$myPers->t_personne['ID_LANG']=$this->t_doc['ID_LANG'];
										$exists=$myPers->checkExistId();
										if ($exists) $this->debrief['personne']['new']++;
										}
								if ($thisVal['LEX_ID_TYPE_LEX']) $myPers->t_personne['LEX_ID_TYPE_LEX']=$thisVal['LEX_ID_TYPE_LEX'];
													
								$myPers->t_personne['ID_LANG']=$this->t_doc['ID_LANG'];
								if (!$thisVal['ID_PERS']) //Pas d'ID dans le XML, on check l'existence en base 
									$myPers->t_personne['ID_PERS']=$myPers->checkExist(false); 
								if (!empty($myPers->t_personne['ID_PERS'])) $this->debrief['personne']['update']++;
								else $this->debrief['personne']['new']++;
								$this->t_doc_lex[]=array('DLEX_ID_ROLE'=>($thisVal['DLEX_ID_ROLE']?$thisVal['DLEX_ID_ROLE']:$arrCrit[2]),
														 'ID_TYPE_DESC'=>($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]),
														 'LEX_PRECISION'=>$thisVal['LEX_PRECISION'],
														 'DLEX_REVERSEMENT'=>$thisVal['DLEX_REVERSEMENT'],
														 'ID_PERS'=>$myPers->t_personne['ID_PERS']);
							}
						}
					break;

					case 'V': //valeur
						if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;}
						foreach($val as $thisVal) {
							$myVal=new Valeur();
							$myVal->t_val['VALEUR']=$thisVal['VALEUR'];
							$myVal->t_val['VAL_ID_TYPE_VAL']=$arrCrit[1];
							if (isset($thisVal['VAL_ID_TYPE_VAL'])) $myVal->t_val['VAL_ID_TYPE_VAL']=$thisVal['VAL_ID_TYPE_VAL'];						
							
							$myVal->t_val['ID_LANG']=$this->t_doc['ID_LANG'];
							$_val=$myVal->checkExist();
							if ($_val) {
								$myVal->t_val['ID_VAL']=$_val;
								$this->debrief['valeur']['update']++; }
							else {
								$myVal->t_val['ID_VAL']=999; //fictif
								$this->debrief['valeur']['new']++;
							}
							$this->t_doc_val[]=array('ID_TYPE_VAL'=>$arrCrit[1]!=''?$arrCrit[1]:$myVal->t_val['VAL_ID_TYPE_VAL'],'ID_VAL'=>$myVal->t_val['ID_VAL']);
						}
					break;

					case 'MAT': //materiel
						foreach ($val as $thisMat) {
						$myMat=new Materiel();
						$myMat->updateFromArray($thisMat);
						if ($myMat->checkExist()) {
							$this->debrief['materiel']['update']++;
							}
						else {
							$this->debrief['materiel']['new']++;
						}
						//debug($myMat,'yellow');
						$this->t_doc_mat[]=array('ID_MAT'=>$myMat->t_mat['ID_MAT'],
												 'ID_DOC'=>$this->t_doc['ID_DOC'],
												 'DMAT_TCIN'=>$thisMat['DMAT_TCIN'],
												 'DMAT_TCOUT'=>$thisMat['DMAT_TCOUT'],
												 'DMAT_LIEU'=>$thisMat['DMAT_LIEU']);
						}
					break;
					
					case 'SEQ' : //séquences
						foreach($val as $thisVal) {							
							if (strpos($thisVal['DOC_TCIN'],":")===false) //juste des minutes ou des secs
								$thisVal['DOC_TCIN']=secToTc($thisVal['DOC_TCIN']);
							if (strpos($thisVal['DOC_TCOUT'],":")===false) //juste des minutes ou des secs
								$thisVal['DOC_TCOUT']=secToTc($thisVal['DOC_TCOUT']);
							if (!empty($thisVal['DOC_TITRE'])) {
							
							foreach ($thisVal as $_f=>$_v) {
								if (!is_array($_v)) $mySeq[$_f]=$_v; //champ "simple"
								else { //array
								}	
							}
							$mySeq["t_doc_lex"]=$thisVal['DOC_LEX'];	//Récupère le DOC LEX pour la séquence
							$mySeq["t_doc_val"]=$thisVal['DOC_VAL'];
							}
							//debug($mySeq,'lightblue');
							$this->t_doc_seq[]=$mySeq;
							unset($mySeq);
						}					
					break;
					
					//by LD 20 02 09 => import de storyboard
					case 'IMG': //image, storyboard
						foreach ($val as $thisImg) {
							$myImg=new Image();
							$myImg->updateFromArray($thisImg);
							if ($myImg->checkExist()) {
								$this->debrief['image']['update']++;
							}
							else {
								$this->debrief['image']['new']++;
							}										
						}
					break;
					
					
					//by LD 20 02 09 => import de doc acc
					case 'DA': //doc acc
						foreach ($val as $thisDA) {
							$myDA=new DocAcc();
							$myDA->updateFromArray($thisDA);
							if ($myDA->checkExist()) {
								$this->debrief['docacc']['update']++;
							}
							else {
								$this->debrief['docacc']['new']++;
							}										
						}
					break;
					
					
				//by LD 20 03 09 => import de festival
					case 'FEST': // festival

						foreach ($val as $thisFest) {
							$myFest=new Festival();
							$myFest->updateFromArray($thisFest);

							if($myFest->checkExist()) {
								$this->debrief['festival']['update']++;
							}else {
								$this->debrief['festival']['new']++;
							}	
							
							$this->t_doc_fest[]=array('ID_FEST'=>$myFest->t_fest['ID_FEST'],'DOC_FEST_VAL'=>$thisFest['DOC_FEST_VAL']);
				
							unset($myFest);										
						}
					break;
					
					
										
				}
		}
	}
	

	// VP 22/06/09 : ajout méthode saveDocAcc, utilisée dans importXML
	function saveDocAcc(){
		return true;
	}
	
	function insererDocMat($id_mat) { //OBSOLETE, à supprimer quand on aura abandonné Orao
	    return false;

	}
	
	/**
	 * Indexation de la fiche pour le moteur Sinequa
	 * IN : objet Doc, tableau de champs à mettre à jour, path script indexation sinequa sur serveur
	 * OUT : true / false, MAJ DB Sinequa
	 * NOTES : l'indexation utilise deux modes
	 * 	1. si un tableau de champs est passé et que ce champ appartient aux colonnes SINEQUA, alors UPDATE champ
	 *  2. dans les autres cas, on procède à une réindexation complète de la fiche
	 * 		- récupération du script d'indexation sineque sur le serveur et mise en forme
	 * 		- export XML des données du document, puis ingestion par le xsl d'export sinequa
	 * 		- insert dans la base de la ligne, en mode replace
	 *  (Attention, le mode replace crée des ghosts en base, il faut donc réindexer régulièrement)
	 */
	function saveSinequa($arrFlds=array()) {
		global $db;
		require_once(libDir."sinequa/fonctions.inc");
		require_once(libDir."sinequa/Intuition.inc");
		
		if (!defined("useSinequa") || !defined("kPathSinequaScriptCreation") || useSinequa==false) {
			$this->dropError(kErrorDocSinequaParametres);
			return false;
		}
		//Récupération du script qui d'indexation qui définit les options d'indexation
		$content=file_get_contents(kPathSinequaScriptCreation); //ouverture du script d'indexation sur le serveur
		if (empty($content)) {
			$this->dropError(kErrorDocSinequaLectureScriptCreation);return false;
		}
		$content=split("\n",$content); //split par line
		foreach ($content as $line) {
			$line=trim($line,"\r");
			if ($line[0]!=';' && !empty($line)) //si contenu et pas commentaire
			$options.=" ".trim($line); //concaténation
			$_dumb=preg_match("/--xml-fields=(.*)=(.*)/i",$line,$out); //récupération de la liste des colonnes
			if ($out[2]) $colonnesXML[]=$out[2];
		}	
		//on est dans le mode 1 => colonnes				
		if (!empty($arrFlds) && is_array($arrFlds)) {
			$fields2update=array_intersect($arrFlds,$colonnesXML); //on obtient la liste des champs pour màj
			
			if (!empty($fields2update)) {
			$sql="UPDATE ".sinequa_database." SET ";
			foreach ($fields2update as $fld) $sql.=" ".$fld."=".$db->Quote($this->t_doc[$fld]).",";
			$sql=substr($sql,0,-1)." WHERE ID_DOC=".$db->Quote($this->t_doc['ID_DOC'])." 
				AND ID_LANG=".$db->Quote($this->t_doc['ID_LANG']);
			$this->debrief['sinequa']['update']++;	
			return true; // on ne continue pas en mode complet
			} //note : si on n'a pas de champs (param arrFlds), on continue dans le mode réindex complète

		}
		//Mode 2 : réindex complète
		// Données => export xml et mise en forme puis passage par la XSL d'import/export Sinequa
		$xml=$this->xml_export();
		$xml=str_replace(array("||","’","&amp;","&lt;","&gt;","&#38;","&#60;","&#62;"),
					 array("","'","&amp;amp;","&amp;lt;","&amp;gt;","&amp;amp;","&amp;lt;","&amp;gt;"),$xml);
		$xml="<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";

		debug($xml,'plum',true);
		$xml2=TraitementXSLT($xml,getSiteFile("designDir","print/exportSinequa.xsl"),null);
	
		if (empty($xml2)) {$this->dropError(kErrorDocImportXMLSinequaVide);return false;}
		debug($xml2,'tan',true);
		$sql="INSERT INTO ".sinequa_database." (replace,language,format,options,text) 
		VALUES('true','".strtolower($this->t_doc['ID_LANG'])."','xml',".$db->Quote($options).",'".str_replace(array("'","\r","\n","\t"),array("''","","",""),$xml2)."');";
	
		//debug(str_ireplace(array(',','<','WHERE','--'),array(','.chr(10),'<'.chr(10),'WHERE'.chr(10),'--'.chr(10)),$sql),'pink');
		$this->debrief['sinequa']['new']++;
		return true;
	}
	
	function saveSolr()
	{}
	
	function deleteSolr()
	{}
	
} // fin classes
?>