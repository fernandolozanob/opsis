<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- D�veloppeurs : Loic Desjardins, basé sur class_usager.php ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

// LD : 18 01 2006, première mouture
// Pour info, les personnes sont initialement alimentées avec les données du lexique
// de type GEN/PP

//************************************************** USAGER ***************************************************************
include_once(modelDir.'model_lexique.php');
include_once(modelDir.'model_docAcc.php');
include_once(libDir."class_user.php");

require_once(modelDir.'model.php');
class Personne extends Model
{


	var $t_personne;
	
	var $type_lex; // Type lexique, en toutes lettres

	var $arrDocs;
	var $arrVersions;

	var $t_pers_lex;
	var $t_pers_val;
	var $t_doc_acc;
	var $t_pers_roles;
	var $arrFather= array();
	var $arrChildren= array();
	var $arrSyno=array(); //tableau ID synonymes

	var $temporary;
// VP 11/02/10 : ajout champ PERS_ID_TYPE_LEX
	// VP 22/06/11 : corrections champs arrFieldsToSaveWithIdLang
	private $arrFieldsToSaveWithIdLang= array("PERS_NOM","PERS_PRENOM","PERS_PARTICULE","PERS_ORGANISATION","PERS_CONTACT","PERS_BIOGRAPHIE","PERS_NOTES","PERS_DIFFUSION","PERS_PHOTO");

	private $arrFieldsToSaveWithoutIdLang= array("PERS_ID_GEN","PERS_ID_TYPE_LEX","PERS_DATE_NAISSANCE","PERS_LIEU_NAISSANCE","PERS_DATE_DECES","PERS_LIEU_DECES","PERS_TEL_PERSO","PERS_GSM_PERSO",
												 "PERS_TEL_PROF","PERS_GSM_PROF","PERS_MAIL_PROF","PERS_MAIL_PERSO","PERS_FAX","PERS_ADRESSE_PERSO","PERS_ADRESSE_PROF","PERS_ZIP_PROF","PERS_VILLE_PROF","PERS_CONTACT_PRENOM","PERS_PHOTO","PERS_WEB","PERS_DATE_CREA","PERS_DATE_MOD","PERS_ID_USAGER_MOD","PERS_STATUT","PERS_ID_ETAT_LEX","PERS_CODE","PERS_PSEUDO");

   private $arrTextFields=array("pers_nom","pers_prenom","pers_particule","pers_organisation"); // liste des champs de type texte dont il faut traiter les apostrophes

	function __construct($id=null,$version='') {
		parent::__construct('t_personne',$id,$version);
	}

	/**
	 * Initialise l'objet en récupérant les champs depuis la base
	 * 
	 * 
	 * @update VG 19/04/11 : ajout de l'initaialisation des champs "users"
	 */
	function init() {
		global $db;
		parent::init();
		$this->t_personne['ID_LANG']=$_SESSION['langue']; // par déft, langue en cours
		$this->t_personne['ID_USAGER']=User::getInstance()->UserID;
		$this->t_personne['USAGER']=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_personne['ID_USAGER']));

	}


	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 * 
	 * @update VG 27/04/11 : ajout de la gestion des parents
	 */
 	function updateFromArray ($tab_valeurs,$allFields=true) {
	 	if (empty($this->t_personne)) $this->init();

	 	foreach ($tab_valeurs as $fld=>$val) {
			// VP 30/06/09 : remplacement isset par array_key_exists à cause des valeurs nulles
			//if (isset($this->t_personne[strtoupper($fld)])) $this->t_personne[strtoupper($fld)]=$val;
			if (array_key_exists(strtoupper($fld),$this->t_personne)) $this->t_personne[strtoupper($fld)]=stripControlCharacters($val);
		}

		//BY LD 25/03/09 => si pas allFields, on n'importe que les champs présents dans le tableau, comme pour doc
		if (!$allFields) {
			//préparation d'un tableau contenant juste les clés uppercased de tab_valeurs
			$arrUpperKeys=array_keys(array_change_key_case($tab_valeurs,CASE_UPPER));
			foreach ($this->t_personne as $fld=>$_v) { //07/04/08 ai viré le test && empty($_v) car une valeur vide EST significative !
				if (!in_array($fld,$arrUpperKeys) && !in_array($fld,$this->arrPrimaryKeys)) {
					unset($this->t_personne[$fld]);
				}
			}
		}
		
		if ($tab_valeurs['arrFather']) $this->t_personne['PERS_ID_GEN']=$tab_valeurs['arrFather'][0]['ID_PERS'];
		$this->arrChildren=$tab_valeurs['arrChildren'];
		$this->arrFather=$tab_valeurs['arrFather'];
		$this->arrVersions=$tab_valeurs['version'];
		
		if (isset($tab_valeurs['arrSyno']))
			$this->arrSyno=$tab_valeurs['arrSyno'];
		else
			$this->arrSyno=array();

	}

	/**
	 * Récupère les infos sur un ou plusieurs personnes.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets pers, sinon màj de l'objet en cours
	 * 
	 * 
	 * @update VG 28/04/11 : ajout de la récupération de la vignette
	 */
	
	function getPersonne($arrPers=array(), $getUsagersSaisie=false) {
		return $this->getData($arrPers,'',$getUsagersSaisie) ; 
	}
	function getData($arrData=array(),$version="", $getUsagersSaisie=false) {
		global $db;
		$arrPers = parent::getData($arrData,$version);
		if (empty($arrPers) && !empty($this->t_personne) && !$arrData) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->spaceQuotes(array(" ' "," ’ "),array("'","’"));
				$this->type_lex=GetRefValue("t_type_lex",$this->t_personne["PERS_ID_TYPE_LEX"],$_SESSION['langue']);
				if($getUsagersSaisie) {
					if(!empty($this->t_personne['PERS_ID_USAGER_CREA'])) {
						$this->user_crea=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_personne['PERS_ID_USAGER_CREA']));
					}
					if(!empty($this->t_personne['PERS_ID_USAGER_MOD'])) {
						$this->user_modif=$db->GetOne('SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_personne['PERS_ID_USAGER_MOD']));
					}
				}
				if (!empty($this->t_personne['PERS_ID_GEN'])) {
					$this->arrFather=$db->GetAll("SELECT *,".$db->Concat('PERS_NOM',"' '",'PERS_PRENOM')." AS NOM FROM t_personne WHERE ID_PERS=".intval($this->t_personne['PERS_ID_GEN'])." AND ID_LANG=".$db->Quote($this->t_personne['ID_LANG']));
				}
				$this->getVignette();
				$this->getSyno();
		} else {
			foreach ($arrPers as $idx=>$Pers) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrPers[$idx]->spaceQuotes(array(" ' "," ’ "),array("'","’"));
				$arrPers[$idx]->type_lex=GetRefValue("t_type_lex",$arrPers[$idx]->t_personne["PERS_ID_TYPE_LEX"],$_SESSION['langue']);
			}
			unset($this); // plus besoin
			return $arrPers;
		}
	}
	
	function getSyno() {
		global $db;

		$sql="SELECT * FROM t_personne WHERE PERS_ID_SYN=".intval($this->t_personne['ID_PERS'])." AND ID_LANG=".$db->Quote($this->t_personne['ID_LANG']);
		$this->arrSyno=$db->GetAll($sql);

	}

	
	function getPossibleSolrFields(){
		global $db;
		if(!defined("useSolr") || !useSolr || empty($this->t_personne['ID_PERS']) || empty($this->t_personne['PERS_ID_TYPE_LEX'])){
			return false ; 
		}
		if(empty($this->arrDocs)){
			$this->getDoc();
		}
		if(empty($this->arrDocs)){
            return false;
        }
		$this->solr_fields = array();
		$arrIDs = array();
		foreach ($this->arrDocs as $doc)
			$arrIDs[] = $doc['ID_DOC'];
		
		if(defined("kDbType") && strpos(kDbType,'postgre')!==false){
			$sql = "SELECT string_agg(id_type_desc,',') as ARR_DESC, string_agg(dlex_id_role,',') as ARR_ROLE from t_doc_lex where id_doc in (".implode(',',$arrIDs).") and ID_PERS=".$db->Quote($this->t_personne['ID_PERS']).";";
		}else{
			$sql = "SELECT group_concat(id_type_desc,',') as ARR_DESC, group_concat(dlex_id_role,',') as ARR_ROLE from t_doc_lex where id_doc in (".implode(',',$arrIDs).") and ID_PERS=".$db->Quote($this->t_personne['ID_PERS']).";";
		}
		$dumb=$db->GetRow($sql);
		if(!empty($dumb) && !empty($dumb['ARR_DESC']) && !empty($dumb['ARR_ROLE'])){
			$desc_types =  explode(',',$dumb['ARR_DESC']); 
			$role_types = explode(',',$dumb['ARR_ROLE']);
			unset($dumb);
			
			foreach($desc_types as $idx=>$desc){
				$this->solr_fields[] = trim(strtolower("p_".$desc."_".$role_types[$idx]."_".$this->t_personne['PERS_ID_TYPE_LEX']),"_");
			}
			$this->solr_fields = array_unique($this->solr_fields);
		}
		return true ; 
	}


	/** Récupère les libellés pour toutes les langues de l'appli.
	 *  IN : rien (objet)
	 * 	OUT : tableau de classe arrVersions
	 *  NOTE : si le lexique existe déjà en base, on ramènes ttes les langues même si le lib n'existe pas encore (right join)
	 * 		   si le lexique  n'existe pas (création), on ramène toutes les langues.
	 */
	function getVersions () {
		global $db;

		if (!empty($this->t_personne['ID_PERS'])) {
			// VP 22/06/2011 : reprise requête pour récupérer tous les champs dépendant de la langue
			$sql="SELECT t_lang.ID_LANG, ID_PERS";
			foreach($this->arrFieldsToSaveWithIdLang as $fld) {$sql.=", ".$fld;}
			$sql.=" FROM t_personne
					INNER join t_lang on (t_lang.ID_LANG=t_personne.ID_LANG)
					WHERE t_personne.ID_LANG<>".$db->Quote($this->t_personne['ID_LANG'])." AND ID_PERS=".intval($this->t_personne['ID_PERS']);
			
			$this->arrVersions=$db->GetAll($sql);
			
			// Suppression quotes
			$from=array(" ' "," ’ ");
			$to=array("'","’");
			foreach($this->arrVersions as &$version){
				foreach ($this->arrTextFields as $fld) {
					$version[strtoupper($fld)]=str_replace($from, $to, $version[strtoupper($fld)]);
				}
			}
		}
	}

	/**
	 * Récupère les documents d'accompagnement, toutes versions confondues.
	 */
	function getDocAcc() {
		global $db;

		unset($this->t_doc_acc);
		$sql="SELECT ID_DOC_ACC,ID_LANG FROM t_doc_acc WHERE ID_PERS=".intval($this->t_personne['ID_PERS']);
		$sql.="AND ID_LANG=".$db->Quote($this->t_personne['ID_LANG']);
		$sql.="	ORDER BY ID_DOC_ACC,ID_LANG";
		$rs=$db->GetAll($sql);
		foreach ($rs as $idx=>$fld) {
			$docAcc= new DocAcc();
			$docAcc->t_doc_acc['ID_DOC_ACC']=$fld['ID_DOC_ACC'];
			$docAcc->t_doc_acc['ID_LANG']=$fld['ID_LANG'];
			$docAcc->getDocAcc();
			$docAcc->getValeurs();
			$this->t_doc_acc[]=$docAcc;
		}
		//debug($this->t_doc_acc,'yellow');
	}



    /**
     * Création en base d'une nouvelle pers avec les infos de base (initialisées à la créa objet)
     * OUT : nv pers créé en base, var de classe ID_PERS renseignée avec nv ID
     *
     */
    function initPersonne($idpers=0){

        global $db;
		$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
        // Insert avec langue de saisie
		// VP 25/01/11 : ajout champ PERS_PRENOM
		if ($idpers) $record["ID_PERS"] = $idpers;
		$record["ID_LANG"] = $this->t_personne['ID_LANG'];
		$record["PERS_ID_GEN"] = $this->t_personne['PERS_ID_GEN'];
		$record["PERS_ID_TYPE_LEX"] = $this->t_personne['PERS_ID_TYPE_LEX'];
		$record["PERS_NOM"] = $this->t_personne['PERS_NOM'];
		$record["PERS_PRENOM"] = $this->t_personne['PERS_PRENOM'];
		$record["PERS_DATE_CREA"] = str_replace("'","",$db->DBTimeStamp(time()));
		$record["PERS_ID_USAGER_CREA"] = intval($_SESSION['USER']['ID_USAGER']);
		$record["PERS_STATUT"] = 0;
		$record["PERS_CODE"] = $this->t_personne['PERS_CODE'];
		$ok = $db->insertBase("t_personne","id_pers",$record);
        if (!$ok) {$this->dropError(kErrorPersonneCreation);return false;}
		
        $this->t_personne['ID_PERS']=($idpers==0?$ok:$idpers);
        $this->t_personne['PERS_DATE_CREA']=$record["PERS_DATE_CREA"];
		$this->temporary=true;
        return true;
    }
    

    /**
     * update VG 29/08/2011 : création de la fonction
     */
	function createFromArray($tab_valeurs, $create = true, $update = true, $saveVersion = true) {
		if(!empty($this->t_personne['ID_PERS'])) $this->getPersonne();
		//$this->updateFromArray($tab_valeurs);
		$this->mapFields($tab_valeurs);
		//Si la valeur existe déjÃ , on retourne juste son id
		$exist = $this->checkExist(false);
		if ($exist) {
			$this->t_personne['ID_PERS'] = $exist;
			//$this->t_lexique['ID_LEX'] = $exist;
			return $this->t_personne['ID_PERS'];
		}

		//Sinon on la créé ou on la sauvegarde
		//update VG 01/09/2011 : ajout du test sur la présence d'ID_PERS...
		if(!empty($this->t_personne['ID_PERS']))
			$existId = $this->checkExistId();
		if($existId) {
			if(!$update) return false;
			$ok = $this->save();

		} else {
			if (!$create) return false;
			$ok = $this->initPersonne();
			$this->save();
		}

		if (!$ok) return false;

		if($saveVersion) {
			$aLang = getOtherLanguages();
			$persVersions = array();
			foreach ($aLang as $lang) {
				$persVersions[$lang] = array(
											'PERS_NOM' =>	$this->t_personne['PERS_NOM'],
											'PERS_PRENOM' =>	$this->t_personne['PERS_PRENOM']
										);
			}
		}
		if(!empty($persVersions)) $this->saveVersions($persVersions);

		if ($ok) return $this->t_personne['ID_PERS'];
		else return false;
	}
	
	
    /**
     * Duplication de la personne
     * IN : this
     * OUT : nouveau document dupliqué.
     */
    function duplicate(){
   		global $db;

		if (empty($this->arrVersions)) $this->getVersions(); // Récup des infos si pas encore loadées
		if (empty($this->t_pers_lex)) $this->getLexique();
		if (empty($this->t_pers_val)) $this->getVal();

        $dupPers=new Personne();
        $dupPers=$this; // Recopie des infos
        unset($dupPers->t_personne['ID_PERS']); // pour éviter les effets de bord
        $dupPers->pers_trad=1; // pour générer les autres versions
        $dupPers->initPersonne(); // récupération du nouvel ID
        $dupPers->save(1); // Sauvegarde avec désactivation du checkExist.

        $dupPers->savePersLex();
        $dupPers->savePersVal();
        return $dupPers;

    }

    /**
     * Sauve le doc en base
     * 1. Si créa d'un nv doc, vérifie si un doc existe (d'après DOC_COTE) et abandonne si oui
     * 2. Sinon, crée un nv document provisoire et récupère l'ID.
     * 3. QQ soit le mode, les valeurs des champs sont updatées en base...
     * 4. ... en deux phases : update pour la langue demandée, puis update pr ttes les langues dispo
     *
     * IN : langue (version à sauver), flag dupliquer O/N,
     * OUT : infos sauvées en base ou retour à false en cas d'erreur.
     */
    function save($dupliquer=0){
        global $db;
        if (empty($this->t_personne['PERS_NOM'])) return false;
        $db->StartTrans();

        if (empty($this->t_personne['ID_PERS']) || $this->checkExistId()) {
        	$ok=$this->initPersonne();
 			if (!$ok) { $this->dropError(kErrorPersonneCreation);
						}
        }

		$this->spaceQuotes(array("'","’"),array(" ' "," ’ "));
		$this->t_personne['PERS_STATUT']=1;
		if ($this->t_personne['PERS_DATE_NAISSANCE']) $this->t_personne['PERS_DATE_NAISSANCE']=fDate($this->t_personne['PERS_DATE_NAISSANCE']);
		if ($this->t_personne['PERS_DATE_DECES']) $this->t_personne['PERS_DATE_DECES']=fDate($this->t_personne['PERS_DATE_DECES']);
    	//update VG 13/07/11
        if($this->t_personne['PERS_ID_GEN'] == $this->t_personne['ID_PERS']) {
        	$this->t_personne['PERS_ID_GEN'] = '0';
        }
        // VP 5/05/2015 : affectation auto date et user modif
		$this->t_personne['PERS_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_personne['PERS_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
        if(empty($this->t_personne['PERS_DATE_CREA'])) unset($this->t_personne['PERS_DATE_CREA']);
        if(empty($this->t_personne['PERS_ID_USAGER_CREA'])) unset($this->t_personne['PERS_ID_USAGER_CREA']);
		
 		$rs = $db->Execute("SELECT * FROM t_personne WHERE ID_PERS=".intval($this->t_personne['ID_PERS'])." AND ID_LANG=".$db->Quote($this->t_personne['ID_LANG']));
		foreach ($this->t_personne as $fld=>$val) {
			if (strpos($fld,'PERS_')===0 && in_array($fld,$this->arrFieldsToSaveWithIdLang))
				$tab[$fld]= $val;
		}
		$sql = $db->GetUpdateSQL($rs, $tab);
		if (!empty($sql)) $ok = $db->Execute($sql);

		$sql = "";$tab=array();
		$rs = $db->Execute("SELECT * FROM t_personne WHERE ID_PERS=".intval($this->t_personne['ID_PERS']));
		foreach ($this->t_personne as $fld=>$val) {
			if (strpos($fld,'PERS_')===0 && in_array($fld,$this->arrFieldsToSaveWithoutIdLang))
				$tab[$fld]= $val;
		}
		$sql = $db->GetUpdateSQL($rs, $tab);
		if (!empty($sql)) $ok = $db->Execute($sql);
		
		$this->saveSyno();

        // I.5. G�n�ration des versions traduites. On ne g�n�re pas de version traduite pour les documents dupliqu�s
        if($this->pers_trad== 1 && $dupliquer==0){
			// VP 22/06/2011 : traitement cas où il n'y a pas de données en POST
			if(!isset($_POST['version'])){
				$aLang = getOtherLanguages();
				$versions = array();
				foreach ($aLang as $lang) {
				   $versions[$lang] = array('PERS_NOM' =>	$this->t_personne['PERS_NOM'], "arrSyno" => $this->arrSyno, "arrFather" => $this->arrFather);
				}
			}else $versions=$_POST['version'];
			if(!empty($versions)) $this->saveVersions($versions);
		}

       if (!$db->CompleteTrans()) {$this->dropError(kErrorPersonneSauve); return false;}

		return TRUE;
    }
	
	function saveSyno() {

		global $db;
		$rs = $db->Execute("SELECT * FROM t_personne WHERE PERS_ID_SYN=".intval($this->t_personne['ID_PERS']));
		$sql = $db->GetUpdateSQL($rs, array("PERS_ID_SYN" => 0));
		if (!empty($sql)) $db->Execute($sql);
		foreach ($this->arrSyno as $idx=>$syno) {
			if (empty($syno['ID_PERS'])) {
				if(!empty($syno['PERS_NOM'])) {
					$terme=$syno['PERS_NOM'];

					$newSyno=new Personne();
					$newSyno->t_personne['PERS_NOM']=$terme;
					$newSyno->t_personne['PERS_ID_SYN']=$this->t_personne['ID_PERS'];


					$newSyno->t_personne['LEX_ID_ETAT_LEX']=1;
					$newSyno->t_personne['LEX_ID_TYPE_LEX']=$this->t_personne['LEX_ID_TYPE_LEX'];
					$newSyno->t_personne['ID_LANG']=$this->t_personne['ID_LANG'];
					$ok=$newSyno->create();
					
					foreach ($_SESSION['arrLangues'] as $lang) {
						if($lang != strtoupper($this->t_personne['ID_LANG'])){
							$newSyno->t_personne['ID_LANG']=$lang;
							if(empty($syno[$lang]['PERS_NOM'])) $newSyno->t_personne['PERS_NOM']=$terme;
							else $newSyno->t_personne['PERS_NOM']=$syno[$lang]['PERS_NOM'];
							$ok=$newSyno->create();
						}
					}
					if (!$ok) {$this->dropError(kErrorLexiqueCreationSyno.": ".$newSyno->error_msg);}
					unset($newSyno);
				}
			} else {

				$rs = $db->Execute("SELECT * FROM t_personne WHERE ID_PERS=".intval($syno['ID_PERS']));
				$sql = $db->GetUpdateSQL($rs, array("PERS_ID_SYN" => intval($this->t_personne['ID_PERS'])));
				if (!empty($sql)) $ok=$db->Execute($sql);
				if (!$ok) {$this->dropError(kErrorLexiqueSauveSyno);return false;}
			}
		}
	}


	/** Sauve les informations des PERS_LEX
	 *  IN : tableau PERS_LEX
	 *  OUT : base mise à jour
	 */
	function savePersLex() {
		global $db;
		if (empty($this->t_pers_lex)) return false;
		$db->StartTrans();
		$ok=$db->Execute('DELETE FROM t_pers_lex WHERE ID_PERS='.intval($this->t_personne['ID_PERS']));
		if (!$ok) trace($db->ErrorMsg());

		foreach($this->t_pers_lex as $idx=>$persLex) {

			if ($persLex['LEXIQUE']) unset($persLex['LEXIQUE']); //On ôte l'objet encapsulé s'il existe

			// On notera que ID_DOC est celui en cours et pas celui contenu dans le tableau.
			// Effectivement, en cas de duplication, on récupère le tableau avec l'ID_DOC de l'original
			if ($persLex['ID_LEX']!='') {
				$ok = $db->insertBase("t_pers_lex","id_lex",array("ID_PERS" => intval($this->t_personne['ID_PERS']), "ID_LEX" => intval($persLex['ID_LEX']), "ID_TYPE_DESC" => $persLex['ID_TYPE_DESC']));
				if (!$ok) trace($db->ErrorMsg());
			}
		}

		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauvePersLex);trace($sql); return false;} else return true;
	}


	/**
	 * Sauve les informations d'un lien PERSONNE / VALEUR
	 * IN : tableau PERS_VAL
	 * OUT : base mise à jour
	 */
	function savePersVal() {
		global $db;
		if (empty($this->t_pers_val)) return false;
		$db->StartTrans();
		$db->Execute('DELETE FROM t_pers_val WHERE ID_PERS='.intval($this->t_personne['ID_PERS']));

		foreach ($this->t_pers_val as $idx=>$persVal) {

			//Précaution en cas de duplication : on spécifie bien que l'ID_PERS est celui en cours.
			// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_VAL originel dedans.
			$persVal['ID_PERS']=$this->t_personne['ID_PERS'];

			unset($persVal['VALEUR']);
			//XXX : ce qui suit est très moche et est dû à un pb de nomenclature en base
			if ($persVal['VAL_ID_TYPE_VAL']) { $persVal['ID_TYPE_VAL']=$persVal['VAL_ID_TYPE_VAL'];unset($persVal['VAL_ID_TYPE_VAL']);}


			if ($persVal['ID_VAL']!='') {
			
				// $rs = $db->Execute("SELECT * FROM t_pers_val WHERE id_pers=-1");
				// $sql = $db->GetInsertSQL($rs, $persVal);
				// $ok = $db->Execute($sql);
				$ok = $db->insertBase("t_pers_val","id_pers",$persVal);

				if (!$ok) trace($db->ErrorMsg());
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauvePersVal);trace($sql); return false;} else return true;
	}


	function saveVersions($tabVersions='') {
		global $db;
		
		if ($tabLangues=='') $tabLangues=$_SESSION['arrLangues'];
                
                // B.RAVI 2015-10-29 Eviter Warning: Invalid argument supplied for foreach()
                if (is_object($tabVersions) || is_array($tabVersions)){
                
                    // VP 22/06/11 : possibilité de sauver des valeurs différentes
                    foreach ($tabVersions as $lang=>$tabVersion) {
                            $version= new Personne;
                            $version->t_personne= $this->t_personne; //recopie de ts les champs par déft
                            $version->updateFromArray($tabVersion); //mise à jour champs
                            $version->t_personne['ID_LANG']=$lang; //mise à jour langue
                            if ($version->checkExistId()) $version->initPersonne($version->t_personne['ID_PERS']);
                            $ok=$version->save();	 		
                            if ($ok==false && $version->error_msg) {$this->dropError(kErrorPersonneVersionSauve.' : '.$version->error_msg);return false;}
                            unset($version);
                    }
                
                }
		
		//		foreach ($_SESSION['arrLangues'] as $idx=>$lang) {
//
//			if ($lang!=$this->t_personne['ID_LANG']) {
//				$version= new Personne;
//				$version->t_personne= $this->t_personne;
//				$version->t_personne['ID_LANG']=$lang;
//
//				if ($version->checkExistId()) {$ok=$version->initPersonne($version->t_personne['ID_PERS'])&&$version->save();}
//
//				if ($ok==false && $version->error_msg) {$this->dropError(kErrorPersonneVersionSauve.' : '.$version->error_msg);return false;}
//				unset($version);
//			}
//		}
		return true;
	}

	function checkExistId() {
		global $db;
		$sql="SELECT ID_PERS FROM t_personne WHERE ID_PERS=".intval($this->t_personne['ID_PERS']);
		$sql.=" AND ID_LANG=".$db->Quote($this->t_personne['ID_LANG']);
		$rs=$db->GetOne($sql);
		debug($sql, "red", true);
		if (!$rs) return true; else return false;
	}

	/** Vérifie l'existence d'une personne dans la base sur la base NOM/ PRENOM / TYPE / LANGUE
	 *  IN : tab classe t_personne, $exact(true/false) recherche par = ou LIKE,
	 * 		$arrCharsReplace=tableau de caractères à remplacer par % dans le cas d'une recheche en like
	 *
	 * @update VG 23/06/2010 : Pour les chaines, on met LIKE, plutot que de laisser le choix
	 */
	function checkExist($exact=true,$arrCharsReplace=null,$checkOnId=false) {
		global $db;
		//LD 18/03/09 : test inclus la particule (nv champ)
		if (!$exact && $arrCharsReplace!==null) {
			$nom=str_replace($arrCharsReplace,'%',$this->t_personne['PERS_NOM']);
			$prenom=str_replace($arrCharsReplace,'%',$this->t_personne['PERS_PRENOM']);
			$particule=str_replace($arrCharsReplace,'%',$this->t_personne['PERS_PARTICULE']);
		} else {$nom=$this->t_personne['PERS_NOM'];$prenom=$this->t_personne['PERS_PRENOM'];$particule=$this->t_personne['PERS_PARTICULE'];
		}

		// VP 01/07/2011 : Suppression passage en minuscule pour la comparaison
//		$sql="SELECT ID_PERS from t_personne WHERE TRIM(LOWER(PERS_NOM)) LIKE ".$db->Quote(strtolower($nom))."
//			AND TRIM(LOWER(PERS_PRENOM)) LIKE ".$db->Quote(strtolower($prenom))."
//			AND TRIM(LOWER(PERS_PARTICULE)) LIKE ".$db->Quote(strtolower($particule))."
//			AND PERS_ID_TYPE_LEX=".$db->Quote($this->t_personne['PERS_ID_TYPE_LEX'])." AND ID_LANG=".$db->Quote($this->t_personne['ID_LANG']);
		$sql="SELECT ID_PERS from t_personne WHERE PERS_ID_TYPE_LEX=".$db->Quote($this->t_personne['PERS_ID_TYPE_LEX'])." AND ID_LANG=".$db->Quote($this->t_personne['ID_LANG']);
		if(empty($prenom)){
			$sql.=" AND (TRIM(concat(PERS_NOM, ' ',PERS_PRENOM))  ".$db->like(trim($nom))." OR TRIM(concat(PERS_PRENOM, ' ',PERS_NOM))  ".$db->like(trim($nom)).")";
		}else{
			$sql.=" AND TRIM(PERS_NOM)  ".$db->like(trim($nom))." AND TRIM(PERS_PRENOM)  ".$db->like(trim($prenom))." AND TRIM(PERS_PARTICULE)  ".$db->like(trim($particule));
		}
		if (!empty($this->t_personne['ID_PERS']) && $checkOnId) $sql.=" AND ID_PERS!=".intval($this->t_personne['ID_PERS']);
		//trace($sql);
		return $db->GetOne($sql); //Note, si homonymie, renvoie juste le premier

	}

	/**
	 * Récupère les termes de lexique + précisions + rôles associés à une personne
	 * IN : ID_PERS et ID_LANG,
	 * OUT : tableau t_pers_lex (classe)
	 */
	function getLexique () {
		    global $db;
		  $sql="SELECT t_pers_lex.*, t_lexique.* FROM t_pers_lex
				INNER JOIN t_lexique ON t_pers_lex.id_lex=t_lexique.id_lex
			    WHERE t_pers_lex.id_pers=".intval($this->t_personne['ID_PERS'])
			    ." AND t_lexique.id_lang=".$db->Quote($this->t_personne['ID_LANG']);
          $this->t_pers_lex=$db->GetAll($sql);

	}

	/**
	 * Récupères les valeurs liées à une personne
	 * IN : ID_PERS et ID_LANG
	 * OUT : tableau t_pers_val (classe)
	 */
	function getValeurs() {
			global $db;
			// VP 22/06/2011 : changement requête sql pour récupérer ID_TYPE_VAL
		    //$sql = "SELECT t_val.VALEUR,t_val.ID_VAL,t_val.VAL_ID_TYPE_VAL FROM t_pers_val, t_val where t_pers_val.ID_VAL=t_val.ID_VAL AND t_pers_val.ID_PERS=".$db->Quote($this->t_personne['ID_PERS'])." AND t_val.ID_LANG=".$db->Quote($this->t_personne['ID_LANG']);
			$sql = "SELECT t_val.*,t_pers_val.ID_TYPE_VAL FROM t_pers_val
					INNER JOIN t_val ON t_pers_val.ID_VAL=t_val.ID_VAL
					where t_pers_val.ID_PERS=".intval($this->t_personne['ID_PERS'])." AND t_val.ID_LANG=".$db->Quote($this->t_personne['ID_LANG']);
            $this->t_pers_val=$db->GetAll($sql);
	}


	function getFestival() {
		   global $db;
		   $sql= "select *
				from t_doc_lex dl 
				INNER join t_festival f ON dl.id_fest=f.id_fest
				left OUTER join t_doc_lex_precision dlp on (dlp.ID_DOC_LEX=dl.ID_DOC_LEX and dlp.ID_LANG=f.ID_LANG)
				left OUTER join t_role r on (dl.DLEX_ID_ROLE=r.ID_ROLE and f.ID_LANG=r.ID_LANG)
				where dl.id_pers=".intval($this->t_personne['ID_PERS'])." and f.id_lang=".$db->Quote($this->t_personne['ID_LANG']);
            return $this->t_fest=$db->GetAll($sql);
	}


    /**
     * Supprime une personne de la base ou bien juste une version (si id_lang est passé)
     * En cas de suppression totale de la personne, celle-ci est effacée + liens au lexique &  valeurs
     * IN : id_lang (opt, pour supprimer juste une version)
     * OUT : update BDD
     */
    function delete($id_lang=""){
        global $db;
        if ($this->t_personne['ID_PERS']=='0' || empty($this->t_personne['ID_PERS'])) { $this->dropError(kErrorPersonneSuppr);return false; }

        if (empty($id_lang)) {
	        // II.1.C. Suppression des enregistrements li�s
	        deleteSQLFromArray(array("t_personne","t_pers_lex","t_pers_val","t_doc_lex"),"ID_PERS",$this->t_personne['ID_PERS']);
        } else {
	        deleteSQLFromArray(array("t_personne"),array("ID_PERS","ID_LANG"),array($this->t_personne['ID_PERS'],$id_lang));
        }
    }


	/** Effectue le mappage entre un tableau et des champs de pers
	 *
	 */
	function mapFields($arr) {
		
		require_once(modelDir."model_val.php");
		require_once(modelDir."model_lexique.php");
		
		foreach ($arr as $idx=>$val) {
			
			$myField=$idx;
			// analyse du champ
			if ($myField=='ID_PERS') $this->t_personne['ID_PERS']=$val;
			if ($myField=='ID_LANG') $this->t_personne['ID_LANG']=$val;
			$arrCrit=explode("_",$myField);
			switch ($arrCrit[0]) {
				case 'PERS': //champ table PERS
					$val=trim($val);
					$flds=explode(",",$myField);
					if ($val==='0' || !empty($val)) foreach ($flds as $_fld) $this->t_personne[$_fld]=$val;
					break;
					
				case 'V': //valeur
					if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
					
					foreach($val as $thisVal) {
						if(empty($thisVal['VALEUR'])) {
							continue;
						}
						$myVal=new Valeur();
						$thisVal['VAL_ID_TYPE_VAL']=isset($thisVal['VAL_ID_TYPE_VAL'])?$thisVal['VAL_ID_TYPE_VAL']:$arrCrit[1];
						$thisVal['ID_LANG']=$this->t_pers['ID_LANG'];
						$myVal->createFromArray($thisVal, true, true);
						
						if (isset($thisVal['ID_TYPE_VAL'])){
							$val_id_type_val=$thisVal['ID_TYPE_VAL'];
						}elseif ($arrCrit[2]!=''){
							$val_id_type_val=$arrCrit[2];
						}else {
							$val_id_type_val=$myVal->t_val['VAL_ID_TYPE_VAL'];
						}
						
						$this->t_pers_val[]=array('ID_TYPE_VAL'=>$val_id_type_val,'ID_VAL'=>$myVal->t_val['ID_VAL']);
					}
					
					break;
					
				case 'L': //lexique
					if ($val!=='') {
						if (!is_array($val)) {$_x[0]['VALEUR']=$val;$val=$_x;} //pas de tableau ? on en crée un minimaliste
						
						foreach($val as $thisVal) {
							
							$myLex=new Lexique();
							if (!$thisVal['LEX_TERME']){
								$thisVal['LEX_TERME']=$thisVal['VALEUR'];
								//par defaut état valide
								$thisVal['LEX_ID_ETAT_LEX']=1;
							}
							if(!$thisVal['LEX_ID_TYPE_LEX'])
								$thisVal['LEX_ID_TYPE_LEX']=$arrCrit[2]; //par déft, extrait du champ
							
							$thisVal['ID_LANG']=$this->t_pers['ID_LANG'];
							
							$myLex->createFromArray($thisVal, true, true);
							
							$id_type_desc = ($thisVal['ID_TYPE_DESC']?$thisVal['ID_TYPE_DESC']:$arrCrit[1]);
							
							//Lien t_pers_lex
							$this->t_pers_lex[]=array('ID_TYPE_DESC'=>$id_type_desc,
													 'ID_LEX'=>$myLex->t_lexique['ID_LEX']);
						}
					}
					break;
					
			}
		}
	}
    //*** XML
    function xml_export($entete=0, $encodage=0, $indent = ''){


        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
        $content.="<t_personne id_pers=\"".$this->t_personne['ID_PERS']."\">\n";

            // I. On liste les propri�t� de l'objet
            // I.1. Propri�t�s relatives � t_doc (sauf les propri�t�s de gestion "tab_verif" et "erreur")
                $list_class_vars = get_class_vars(get_class($this));
                foreach ($this->t_personne as $name => $value) {
                    $content .= "\t<".strtoupper($name).">".str_replace($search,$replace,$value)."</".strtoupper($name).">\n";
                    //$content .= "\t<".strtoupper($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".strtoupper($name).">\n";
                }
                $content .= "\t<VIGNETTE>".$this->vignette."</VIGNETTE>\n";
				$content .= "\t<US_CREA>".$this->user_crea."</US_CREA>\n";
				$content .= "\t<US_MODIF>".$this->user_modif."</US_MODIF>\n";
				$content .="\t<TYPE_LEX>".GetRefValue("t_type_lex",$this->t_personne["PERS_ID_TYPE_LEX"],$_SESSION['langue'])."</TYPE_LEX>\n";
		
            if (!empty($this->t_pers_lex)) {
			// I.2. Propri�t�s relatives � t_doc_lex
            $content.="\t<t_pers_lex nb=\"".count($this->t_pers_lex)."\">\n";
                // Pour chaque type de lexique

            	// Constitution du tableau hiérarchique TYPE_DESC => ROLE => lexique
                foreach ($this->t_pers_lex as $idx => $lex) {

                    $content.="\t\t<t_lex id='".$lex['ID_LEX']."'>\n";
                    foreach ($lex as $fld=>$val) {
                    	$content.="\t\t\t<".$fld.">".$val."</".$fld.">\n";
                    }

                    $content.="\t\t</t_lex>";
                    }
            $content.="\t</t_pers_lex>\n";
            }

            if (!empty($this->t_pers_val)) {
            // I.3. Propri�t�s relatives � t_doc_val
            $content.="\t<t_pers_val nb=\"".count($this->t_pers_val)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_pers_val as $name => $value) {
                    $content .= "\t\t<TYPE_VAL VAL_ID_TYPE_VAL=\"".$value['VAL_ID_TYPE_VAL']."\" >";
                    // Pour chaque valeur

                        $content .= "\t\t<t_val ID_VAL=\"".$value['ID_VAL']."\">\n";
                        $content .= "\t\t\t<ID_VAL>".$value['ID_VAL']."</ID_VAL>\n";
                        $content .= "\t\t\t<VALEUR>".str_replace($search,$replace, $value['VALEUR'])."</VALEUR>\n";
                        $content .= "\t\t</t_val>\n";

                    $content .= "</TYPE_VAL>\n";
                }
            $content.="\t</t_pers_val>\n";
            }

            if (!empty($this->t_fest)) {
            // I.3. Propri�t�s relatives � t_doc_val
            $content.="\t<t_fest nb=\"".count($this->t_fest)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_fest as $name => $fest) {
                    $content .= "\t\t<FEST>";
                    // Pour chaque valeur
                	foreach ($fest as $name => $v) {
                		$content .= "\t<".strtoupper($name).">".str_replace($search,$replace, $v)."</".strtoupper($name).">\n";
                    	//$content .= "\t<".strtoupper($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$v))."</".strtoupper($name).">\n";
                    }
                    $content .= "</FEST>\n";
                }
            $content.="\t</t_fest>\n";
            }

        	  if (!empty($this->t_doc_acc)) {
        	 $content.="\t<t_doc_acc nb=\"".count($this->t_doc_acc)."\">\n";
        	 	foreach ($this->t_doc_acc as $docacc) {
        	 		$content.=$docacc->xml_export(0,0,chr(9));
        	 	}
        	  $content.="\t</t_doc_acc>";
        	 }

        	if (empty($this->arrDocs)) $this->getDoc();
            if (!empty($this->arrDocs)) {
			// I.2. Propri�t�s relatives � t_doc_lex
            $content.="\t<t_pers_doc nb=\"".count($this->arrDocs)."\">\n";
                // Pour chaque type de lexique

            	// Constitution du tableau hiérarchique TYPE_DESC => ROLE => lexique
                foreach ($this->arrDocs as $idx => $doc) {

                    $content.="\t\t<t_doc id='".$doc['ID_DOC']."'>\n";
                    foreach ($doc as $fld=>$val) {
						if(substr(strtoupper($val),0,5)=="<XML>" || strtoupper($fld) == "DOC_TRANSCRIPT")
							$content .= "\t\t\t\t<".strtoupper($fld).">".str_replace(array(" ' ","&"),array("'","&amp;"),$val)."</".strtoupper($fld).">\n";
						else
							$content.="\t\t\t<".$fld.">".str_replace($search,$replace,$val)."</".$fld.">\n";
                    }

                    $content.="\t\t</t_doc>";
                    }
            $content.="\t</t_pers_doc>\n";
            }
            
            // I.3. Propriétés relatives aux droits_preexistants
    		if (!empty($this->t_pers_prex)) {
            	$content.="\t<t_pers_prex nb=\"".count($this->t_pers_prex)."\">\n";
                foreach ($this->t_pers_prex as $idx => $prex) {

                    $content.="\t\t<t_prex id='".$prex['ID_DOC_PREX']."'>\n";
                    foreach ($prex as $fld=>$val) {
                    	if (!is_array($val) && !is_object($val))
                    		$content.="\t\t\t<".$fld.">".str_replace($search,$replace,$val)."</".$fld.">\n";
                    }

                    $content.="\t\t</t_prex>";
                    }
            $content.="\t</t_pers_prex>\n";
            }
    		// I.4. Propriétés relatives aux documents liés aux droits_preexistants
    		if (!empty($this->t_pers_doc)) {
            	$content.="\t<t_pers_prods nb=\"".count($this->t_pers_prex)."\">\n";
                foreach ($this->t_pers_doc as $idx => $prod) {

                    $content.="\t\t<t_doc id='".$prod['ID_DOC']."'>\n";
                    foreach ($prod as $fld=>$val) {
                    	if (!is_array($val) && !is_object($val))
                    		$content.="\t\t\t<".$fld.">".str_replace($search,$replace,$val)."</".$fld.">\n";
                    }

                    $content.="\t\t</t_doc>";
                    }
            $content.="\t</t_pers_prods>\n";
            }
            
		if (!empty($this->arrFather)) {
			$content.=$indent."\t<t_personne_father count='".count($this->arrFather)."'>";
			foreach ($this->arrFather as $idx=>$_pers) {
				$content.=$indent."\t\t<t_personne>\n";
				foreach ($_pers as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_personne>\n";
			}
			$content.=$indent."\t</t_personne_father>";
		}
		
		if (!empty($this->arrChildren)) {
			$content.=$indent."\t<t_personne_children count='".count($this->arrChildren)."'>";
			foreach ($this->arrChildren as $idx=>$_pers) {
				$content.=$indent."\t\t<t_personne>\n";
				foreach ($_pers as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_personne>\n";
			}
			$content.=$indent."\t</t_personne_children>";
		}
		
		if (!empty($this->arrSyno)) {
			$content.=$indent."\t<t_personne_syno count='".count($this->arrSyno)."'>";
			foreach ($this->arrSyno as $idx=>$_pers) {
				$content.=$indent."\t\t<t_personne>\n";
				foreach ($_pers as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_personne>\n";
			}
			$content.=$indent."\t</t_personne_syno>";
		}
		
		if (!empty($this->arrSynoPref)) {
			$content.=$indent."\t<t_personne_syno_pref count='".count($this->arrSynoPref)."'>";
			foreach ($this->arrSynoPref as $idx=>$_pers) {
				$content.=$indent."\t\t<t_personne>\n";
				foreach ($_pers as $fld=>$val) {
					$content.=$indent."\t\t\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
				}
				$content.=$indent."\t\t</t_personne>\n";
			}
			$content.=$indent."\t</t_personne_syno_pref>";
		}
		
	   $content.="</t_personne>";
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }
	
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){
		global $db;
		
		if($mode == null){
			$mode=array();
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		if(strpos(strtolower($critere),'order by') === false){
			$critere.=" order by id_pers asc ";
		}
	
		$limit="";
		if ($nb!="all"){
			$limit = $db->limit($nb_offset,$nb);
		}
	
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		$xml_file_path = kServerTempDir."/export_opsis_thesaurus_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";
		
		$sql_pers = "SELECT P.* , TL.TYPE_LEX
		FROM t_personne P 
		LEFT OUTER JOIN t_type_lex TL ON P.PERS_ID_TYPE_LEX = TL.ID_TYPE_LEX AND P.ID_LANG=TL.ID_LANG 
		".$critere.$limit;
		
		//trace("sql_pers:".$sql_pers);
		

		$results_pers = $db->Execute($sql_pers);
		$ids = implode(',',$db->GetCol($sql_pers));
		if($ids == ''){
			return false;
		}
		
		
		
		$sql_pers_lex="SELECT t_pers_lex.*, t_lexique.* FROM t_pers_lex
					INNER JOIN t_lexique ON t_pers_lex.id_lex=t_lexique.id_lex
					WHERE t_pers_lex.id_pers in ($ids) AND t_lexique.id_lang=".$db->Quote($_SESSION['langue']);
		$results_pers_lex = $db->Execute($sql_pers_lex);	 
		unset($sql_pers_lex);
		
		$sql_pers_val="SELECT t_val.*,t_pers_val.ID_TYPE_VAL FROM t_pers_val
					INNER JOIN t_val ON t_pers_val.ID_VAL=t_val.ID_VAL
					where t_pers_val.ID_PERS in ($ids) AND t_val.ID_LANG=".$db->Quote($_SESSION['langue']);
		$results_pers_val = $db->Execute($sql_pers_val);	 
		unset($sql_pers_val);
		
		$sql_pers_doc="SELECT distinct doc.*,
			doc.DOC_VAL,doc.DOC_LEX,doc.DOC_XML,
			dt.TYPE_DOC,f.FONDS,r.ROLE,dt.TYPE_DOC,f.FONDS,r.ROLE,d.TYPE_DESC
			from t_doc doc 
				 inner join t_doc_lex on doc.ID_DOC=t_doc_lex.ID_DOC 
				 left join t_type_doc dt on doc.DOC_ID_TYPE_DOC=dt.ID_TYPE_DOC and dt.id_lang=".$db->Quote($_SESSION['langue'])." 
				 left join t_fonds f on doc.DOC_ID_FONDS=f.ID_FONDS 
			     left join t_role r on t_doc_lex.DLEX_ID_ROLE=r.ID_ROLE and r.id_lang=".$db->Quote($_SESSION['langue'])." 
			     left join t_type_desc d on t_doc_lex.ID_TYPE_DESC=d.ID_TYPE_DESC and d.id_lang=".$db->Quote($_SESSION['langue'])." 
			where 
				doc.id_lang=".$db->Quote($_SESSION['langue'])."
				and t_doc_lex.id_pers in ($ids)";
		$results_pers_doc = $db->Execute($sql_pers_doc);	 
		unset($sql_pers_doc);

		$sql_pers_docacc = "SELECT t_doc_acc.*
		FROM t_doc_acc 
		WHERE ID_PERS in ($ids) and t_doc_acc.id_lang=".$db->Quote($_SESSION['langue']);
		$results_pers_docacc = $db->Execute($sql_pers_docacc);	 
		unset($sql_pers_docacc);
		
		
		$sql_fest= "select *
				from t_doc_lex dl 
				INNER join t_festival f ON dl.id_fest=f.id_fest
				left OUTER join t_doc_lex_precision dlp on (dlp.ID_DOC_LEX=dl.ID_DOC_LEX and dlp.ID_LANG=f.ID_LANG)
				left OUTER join t_role r on (dl.DLEX_ID_ROLE=r.ID_ROLE and f.ID_LANG=r.ID_LANG)
				where dl.id_pers in ($ids) and f.id_lang=".$db->Quote($_SESSION['langue']);
		$results_fest = $db->Execute($sql_fest);	 
		unset($sql_fest);
		
		while($pers_lex = $results_pers_lex->FetchRow()){
			$results_pers_lex_ref[$pers_lex['ID_PERS']][]=$results_pers_lex->CurrentRow();
		}
		while($pers_val = $results_pers_val->FetchRow()){
			$results_pers_val_ref[$pers_val['ID_PERS']][]=$results_pers_val->CurrentRow();
		}
		while($pers_doc = $results_pers_doc->FetchRow()){
			$results_pers_doc_ref[$pers_doc['ID_PERS']][]=$results_pers_doc->CurrentRow();
		}
		while($pers_docacc = $results_pers_docacc->FetchRow()){
			$results_pers_docacc_ref[$pers_docacc['ID_PERS']][]=$results_pers_docacc->CurrentRow();
		}
		while($fest = $results_fest->FetchRow()){
			$results_fest_ref[$fest['ID_PERS']][]=$results_fest->CurrentRow();
		}
		
		foreach($results_pers as $pers){
			$xml .= "\t<t_personne>\n";
			foreach($pers as $fld=>$val){
				$xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
			}
			
			
			//==========GESTION PERS LEX==========
			if(isset($results_pers_lex_ref[$pers['ID_PERS']][0])){
				$results_pers_lex->Move($results_pers_lex_ref[$pers['ID_PERS']][0]);
				$array_pers_lex = $results_pers_lex->GetRows(count($results_pers_lex_ref[$pers['ID_PERS']])-1);
			}else{
				$array_pers_lex=array();
			}

			if(count($array_pers_lex)>0){
				$xml.="\t\t<t_pers_lex nb=\"".count($array_pers_lex)."\" >\n";

				foreach($array_pers_lex as $lex){
					$xml.="\t\t\t<t_lex>\n";
					foreach($lex as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_lex>\n";
				}
				$xml.="\t\t</t_pers_lex>\n";
			}
			
			//==========GESTION PERS VAL==========
			if(isset($results_pers_val_ref[$pers['ID_PERS']][0])){
				$results_pers_val->Move($results_pers_val_ref[$pers['ID_PERS']][0]);
				$array_pers_val = $results_pers_val->GetRows(count($results_pers_val_ref[$pers['ID_PERS']])-1);
			}else{
				$array_pers_val=array();
			}

			if(count($array_pers_val)>0){
				$xml.="\t\t<t_pers_val nb=\"".count($array_pers_val)."\" >\n";

				foreach($array_pers_val as $valeur){
					$xml.="\t\t\t<t_val>\n";
					foreach($valeur as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_val>\n";
				}
				$xml.="\t\t</t_pers_val>\n";
			}
			
			//==========GESTION PERS FEST==========
			if(isset($results_fest_ref[$pers['ID_PERS']][0])){
				$results_fest->Move($results_fest_ref[$pers['ID_PERS']][0]);
				$array_pers_fest = $results_fest->GetRows(count($results_fest_ref[$pers['ID_PERS']])-1);
			}else{
				$array_pers_fest=array();
			}

			if(count($array_pers_fest)>0){
				$xml.="\t\t<t_fest nb=\"".count($array_pers_fest)."\" >\n";

				foreach($array_pers_fest as $fest){
					$xml.="\t\t\t<FEST>\n";
					foreach($fest as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</FEST>\n";
				}
				$xml.="\t\t</t_fest>\n";
			}
			
			//==========GESTION PERS DOCACC==========
			if(isset($results_pers_docacc_ref[$pers['ID_PERS']][0])){
				$results_pers_docacc->Move($results_pers_docacc_ref[$pers['ID_PERS']][0]);
				$array_pers_docacc = $results_pers_docacc->GetRows(count($results_pers_docacc_ref[$pers['ID_PERS']])-1);
			}else{
				$array_pers_docacc=array();
			}

			if(count($array_pers_docacc)>0){
				$xml.="\t\t<t_doc_acc nb=\"".count($array_pers_docacc)."\" >\n";

				foreach($array_pers_docacc as $docacc){
					$xml.="\t\t\t<t_doc_acc>\n";
					foreach($docacc as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_doc_acc>\n";
				}
				$xml.="\t\t</t_doc_acc>\n";
			}
			//==========GESTION PERS DOCS==========
			if(isset($results_pers_doc_ref[$pers['ID_PERS']][0])){
				$results_pers_doc->Move($results_pers_doc_ref[$pers['ID_PERS']][0]);
				$array_pers_doc = $results_pers_doc->GetRows(count($results_pers_doc_ref[$pers['ID_PERS']])-1);
			}else{
				$array_pers_doc=array();
			}

			if(count($array_pers_doc)>0){
				$xml.="\t\t<t_pers_doc nb=\"".count($array_pers_doc)."\" >\n";

				foreach($array_pers_doc as $doc){
					$xml.="\t\t\t<t_doc>\n";
					foreach($doc as $fld=>$val){
						if(substr(strtoupper($val),0,5)=="<XML>" || strtoupper($fld) == "DOC_TRANSCRIPT")
							$xml .= "\t\t\t\t<".strtoupper($fld).">".str_replace(array(" ' ","&"),array("'","&amp;"),$val)."</".strtoupper($fld).">\n";
						else
							$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					$xml.="\t\t\t</t_doc>\n";
				}
				$xml.="\t\t</t_pers_doc>\n";
			}
			
			
			
			$xml .= "\t</t_personne>\n";
			fwrite($xml_file,$xml);
			$xml = "";
			
		}
		
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
		flock($xml_file, LOCK_UN);
		fclose($xml_file);
	
		return $xml_file_path;
	}
	
	


//update VG 24/06/2011 : changement de la requête : ajout des left join, plutôt que des unions dans la clause where
 function getDoc() {
 	global $db;
 	require_once(libDir."class_user.php");

/* 	$sql="SELECT distinct doc.*,
			doc.DOC_VAL,doc.DOC_LEX,doc.DOC_XML,
			dt.TYPE_DOC,f.FONDS,r.ROLE,dt.TYPE_DOC,f.FONDS,r.ROLE
			from t_doc doc,
				 t_doc_lex,
				 t_type_doc dt,
				 t_fonds f,
			     t_role r
			where doc.doc_id_type_doc=dt.id_type_doc
			and doc.doc_id_fonds=f.id_fonds
			and doc.id_lang=".$db->Quote($this->t_personne['ID_LANG'])."
			and dt.id_lang=".$db->Quote($this->t_personne['ID_LANG'])."
			and r.id_lang=".$db->Quote($this->t_personne['ID_LANG'])."
			and  doc.id_doc=t_doc_lex.id_doc
			and t_doc_lex.dlex_id_role=r.id_role
			and t_doc_lex.id_pers=".$db->Quote($this->t_personne['ID_PERS']);*/
 	$sql="SELECT distinct doc.*,
			dt.TYPE_DOC,f.FONDS,dt.TYPE_DOC,f.FONDS,r.ROLE,d.TYPE_DESC
			from t_doc doc 
				 inner join t_doc_lex on doc.ID_DOC=t_doc_lex.ID_DOC 
				 left join t_type_doc dt on doc.DOC_ID_TYPE_DOC=dt.ID_TYPE_DOC and dt.id_lang=".$db->Quote($this->t_personne['ID_LANG'])." 
				 left join t_fonds f on doc.DOC_ID_FONDS=f.ID_FONDS 
			     left join t_role r on t_doc_lex.DLEX_ID_ROLE=r.ID_ROLE and r.id_lang=".$db->Quote($this->t_personne['ID_LANG'])." 
			     left join t_type_desc d on t_doc_lex.ID_TYPE_DESC=d.ID_TYPE_DESC and d.id_lang=".$db->Quote($this->t_personne['ID_LANG'])." 
			where 
				doc.id_lang=".$db->Quote($this->t_personne['ID_LANG'])."
				and t_doc_lex.id_pers=".intval($this->t_personne['ID_PERS']);
	if(User::getInstance()->getTypeLog() <= kLoggedNorm) $sql.=" AND doc.DOC_ACCES='1' "; //Si pas admin/doc, on retire les doc non valides
	if (!empty($this->sqlOrder))  $sql.=$this->sqlOrder; //ajouté par LD 15/11/2007 pour gérer un ordre.
	else $sql.=" ORDER BY DOC_TITRE ASC"; //LD 260808 ordre par défaut sur n° ligne

 	$this->arrDocs=$db->GetAll($sql);
 	}

    /**
     * Gestion avancée de la fusion entre personnes
     *
     */
    function fusion($id2fusion) {

	   	global $db;

		if ($this->t_personne['ID_PERS'] == $id2fusion) {
			trace('fusion de personne '.$this->t_personne['ID_PERS'].' vers '.$id2fusion.' suspendue');
			return; //@update VG 30/07/2010 : si id et infos identiques, pas besoin de faire le reste. De plus, il en résulterait une suppression de personne...
		}
	   	//Récupération des infos de la personne en cours...
	  	$this->getPersonne();
	  	$this->getValeurs();
	  	$this->getLexique();

    	//Récup des infos de la personne à fusionner
    	$pers2fusion=new Personne();
    	$pers2fusion->t_personne['ID_PERS']=$id2fusion;
    	$pers2fusion->t_personne['ID_LANG']=$this->t_personne['ID_LANG'];
	  	$pers2fusion->getPersonne();
	  	$pers2fusion->getValeurs();
	  	$pers2fusion->getLexique();


    	//Fusion des tableaux annexes
    	$this->t_pers_lex=mergeArrays($this->t_pers_lex,$pers2fusion->t_pers_lex);
    	$this->t_pers_val=mergeArrays($this->t_pers_val,$pers2fusion->t_pers_val);
    	$this->t_doc_acc=mergeArrays($this->t_doc_acc,$pers2fusion->t_doc_acc);


    	//Pour les champs, c'est au cas par cas...
    	//1. Champs que l'on fusionne
    	$this->t_personne['PERS_NOTES'].=$pers2fusion->t_personne['PERS_NOTES'];
    	$this->t_personne['PERS_CONTACT'].=$pers2fusion->t_personne['PERS_CONTACT'];
    	$this->t_personne['PERS_BIOGRAPHIE'].=$pers2fusion->t_personne['PERS_BIOGRAPHIE'];
    	$this->t_personne['PERS_DIFFUSION'].=$pers2fusion->t_personne['PERS_DIFFUSION'];
    	$this->t_personne['PERS_ORGANISATION'].=$pers2fusion->t_personne['PERS_ORGANISATION'];

    	//2. Champs "à valeur unique", que l'on compare... Si une valeur existe des deux cotés, c'est le "nouveau" qui l'emporte
    	$this->t_personne['PERS_DATE_NAISSANCE']=mergeFields($this->t_personne['PERS_DATE_NAISSANCE'],$pers2fusion->t_personne['PERS_DATE_NAISSANCE']);
    	$this->t_personne['PERS_DATE_DECES']=mergeFields($this->t_personne['PERS_DATE_DECES'],$pers2fusion->t_personne['PERS_DATE_DECES']);
    	$this->t_personne['PERS_LIEU_NAISSANCE']=mergeFields($this->t_personne['PERS_LIEU_NAISSANCE'],$pers2fusion->t_personne['PERS_LIEU_NAISSANCE']);
    	$this->t_personne['PERS_LIEU_DECES']=mergeFields($this->t_personne['PERS_LIEU_DECES'],$pers2fusion->t_personne['PERS_LIEU_DECES']);
    	$this->t_personne['PERS_TEL_PERSO']=mergeFields($this->t_personne['PERS_TEL_PERSO'],$pers2fusion->t_personne['PERS_TEL_PERSO']);
    	$this->t_personne['PERS_TEL_PROF']=mergeFields($this->t_personne['PERS_TEL_PROF'],$pers2fusion->t_personne['PERS_TEL_PROF']);
    	$this->t_personne['PERS_GSM_PERSO']=mergeFields($this->t_personne['PERS_GSM_PERSO'],$pers2fusion->t_personne['PERS_GSM_PERSO']);
    	$this->t_personne['PERS_GSM_PROF']=mergeFields($this->t_personne['PERS_GSM_PROF'],$pers2fusion->t_personne['PERS_GSM_PROF']);
    	$this->t_personne['PERS_FAX']=mergeFields($this->t_personne['PERS_FAX'],$pers2fusion->t_personne['PERS_FAX']);
    	$this->t_personne['PERS_MAIL_PERSO']=mergeFields($this->t_personne['PERS_MAIL_PERSO'],$pers2fusion->t_personne['PERS_MAIL_PERSO']);
    	$this->t_personne['PERS_MAIL_PROF']=mergeFields($this->t_personne['PERS_MAIL_PROF'],$pers2fusion->t_personne['PERS_MAIL_PROF']);
    	$this->t_personne['PERS_PHOTO']=mergeFields($this->t_personne['PERS_PHOTO'],$pers2fusion->t_personne['PERS_PERS_PHOTO']);
    	$this->t_personne['PERS_ADRESSE_PERSO']=mergeFields($this->t_personne['PERS_ADRESSE_PERSO'],$pers2fusion->t_personne['PERS_ADRESSE_PERSO'],true);
    	$this->t_personne['PERS_ADRESSE_PROF']=mergeFields($this->t_personne['PERS_ADRESSE_PROF'],$pers2fusion->t_personne['PERS_ADRESSE_PROF'],true);

    	// trace(print_r($this,true));


    	$this->save();
    	$this->savePersVal();
    	$this->savePersLex();

    	//Pour les doc acc, c'est bcp plus simple d'utiliser SQL !
    	$db->Execute("UPDATE t_doc_acc SET ID_PERS=".intval($this->t_personne['ID_PERS'])." WHERE ID_PERS=".intval($id2fusion));
    	//Pareil pour les emissions
   		$db->Execute("UPDATE t_doc_lex SET ID_PERS=".intval($this->t_personne['ID_PERS'])." WHERE ID_PERS=".intval($id2fusion));

    	$pers2fusion->delete();

    }

	function getVignette() {
		global $db;
		if (!empty($this->t_personne['PERS_PHOTO'])) {
			// VP 10/06/11 : récupération vignette depuis doc_acc
			$vignetteChemin=$db->GetOne('SELECT '.$db->Concat('DA_CHEMIN','DA_FICHIER').' FROM t_doc_acc WHERE ID_DOC_ACC='.intval($this->t_personne['PERS_PHOTO']));
			$relativePathFromMedia=str_replace(kCheminHttpMedia,'',kDocumentUrl);
			if ($vignetteChemin) return $this->vignette= (strpos($vignetteChemin,'http://')===0?$vignetteChemin:$relativePathFromMedia.$vignetteChemin);
		}
	}
	
	
	function saveVignette($from,$id) {

		global $db;
		$sql="UPDATE t_personne SET PERS_PHOTO=".intval($id)." WHERE ID_PERS=".intval($this->t_personne['ID_PERS']);
		$db->Execute($sql);
		$this->t_personne['PERS_PHOTO']=$id;

	}

	/**
	 * Récupère les ID des doc liés à ce terme
	 * IN : rien
	 * OUT : tab de classe arrDoc
	 */
	function getDocs($withDocLex=true) {
		global $db;
		// VP 9/9/10 : initialisation tableau arrDoc
		$this->arrDoc=array();
		$this->arrDocLex=array();
		if(!empty($this->t_personne['ID_PERS'])){
			// tableau d'ID documents
			$sql="SELECT DISTINCT ID_DOC from t_doc_lex WHERE ID_PERS=".intval($this->t_personne['ID_PERS']);
			$this->arrDoc = $db->GetCol($sql);
			if($withDocLex){
				// tableau de documents
				$sql="SELECT dl.* ,DOC_COTE, DOC_TITRE, DOC_TITRE_COL, DOC_DATE_PROD, TYPE_DOC
				from t_doc_lex dl, t_doc d
				LEFT OUTER JOIN t_type_doc td ON td.ID_TYPE_DOC=d.DOC_ID_TYPE_DOC and td.ID_LANG=d.ID_LANG
				WHERE ID_PERS=".intval($this->t_personne['ID_PERS'])." and dl.id_doc=d.ID_DOC and d.ID_LANG=".$db->Quote($this->t_personne['ID_LANG'])." order by DOC_TITRE";
				$this->arrDocLex=$db->GetAll($sql);
			}
		}
	}
	//VP 6/07/2018
	function updateDocChampsXML($saveSolr=false) {
		require_once(modelDir.'model_doc.php');
		if(empty($this->arrDoc)){
			$this->getDocs(false);
		}
		foreach ($this->arrDoc as $idDoc) {
			$oDoc = new Doc();
			$oDoc->t_doc['ID_DOC'] = $idDoc;
			$oDoc->updateChampsXML("DOC_LEX");
			if($saveSolr && ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))){
				$oDoc->saveSolr();
			}
			unset($oDoc);
		}
	}



}

?>
