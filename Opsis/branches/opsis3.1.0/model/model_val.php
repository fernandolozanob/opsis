<?php
require_once(modelDir."/model.php");

class Valeur extends Model {

var $t_val;
var $arrFather; // tableau ID VAL père (unique)
var $arrChildren; // tableau ID VAL liées à cette val
var $arrDoc; // tableau ID docs liés à cette val
var $arrDocVal; // tableau docs liés à cette val
var $arrPers; //tableau ID personnes liées à cette val
var $arrMat; //tableau ID matériels liés à cette val
var $arrVersions; //tableau des ID/VALEURS pour les autres langues
var $fusionVal; // Valeur si fusion en cours (1) sinon null
var $type_val; // Type de valeur, en toutes lettres

	function __construct($id=null, $version="") {
		parent::__construct("t_val",$id, $version);
	}

	/**
	 * Initialise l'objet en récupérant les champs depuis la base
	 */
	function init() {
		global $db;
		$cols=$db->MetaColumns('t_val');
		foreach ($cols as $i=>$col) $this->t_val[$i]='';
		$this->t_val['ID_LANG']=$_SESSION['langue']; // par déft, langue en cours
	}

	/**
	 * Récupère les infos sur une ou plusieurs valeurs.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets valeur, sinon màj de l'objet en cours
	 */
	function getVal($arrVal=array(),$version="") {
		return $this->getData($arrVal,$version);
	}
	function getData($arrData=array(),$version="") {
		$arrVals=parent::getData($arrData,$version); 
		
		if (empty($arrVals) && !empty($this->t_val) && !$arrData) { 
			$this->type_val=GetRefValue("t_type_val",$this->t_val["VAL_ID_TYPE_VAL"],$this->t_val['ID_LANG']);
			if ($this->t_val['VAL_ID_GEN'])
				$this->arrFather[]=$this->t_val['VAL_ID_GEN'];
		}else{
			foreach ($arrVals as $idx=>$Valeur){ // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx]->type_val=GetRefValue("t_type_val",$arrVals[$idx]->t_val["VAL_ID_TYPE_VAL"],$this->t_val['ID_LANG']);
			}
			unset($this); // plus besoin
			return $arrVals;
		}
	}



	/** Récupère les libellés pour toutes les langues de l'appli.
	 *  IN : rien (objet)
	 * 	OUT : tableau de classe arrVersions
	 *  NOTE : si la valeur existe déjà en base, on ramènes ttes les langues même si le lib n'existe pas encore (right join)
	 * 		   si la valeur n'existe pas (création), on ramène toutes les langues.
	 */
	function getVersions () {
		global $db;
		$tabLg=$_SESSION['tab_langue'];
		unset($tabLg[$_SESSION['langue']]); // retrait de la langue en cours.
		if (!empty($this->t_val['ID_VAL'])) {
		$sql="SELECT t_lang.ID_LANG,VALEUR,LANG,ID_VAL FROM t_val
				right join t_lang on (t_lang.ID_LANG=t_val.ID_LANG)
				WHERE
				t_val.ID_LANG<>".$db->Quote($this->t_val['ID_LANG'])." AND ID_VAL=".intval($this->t_val['ID_VAL']);
		$this->arrVersions=$db->GetAll($sql);
		}
		if (empty($this->arrVersions)) {
		$sql="SELECT *,'' as VALEUR, null as ID_VAL FROM t_lang
				WHERE
				ID_LANG<>".$db->Quote($this->t_val['ID_LANG']);
		$this->arrVersions=$db->GetAll($sql);
		}
	}



	/** Export XML de l'objet valeur
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }


		$content.=$indent."<t_val>\n";
		// VALEUR
		foreach ($this->t_val as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}
		$content.=$indent."\t<TYPE_VAL>".GetRefValue("t_type_val",$this->t_val["VAL_ID_TYPE_VAL"],$_SESSION['langue'])."</TYPE_VAL>\n";
		$content.=$indent."</t_val>\n";



		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}

	/** Vérifie si une valeur existe déjà en base
	 *  IN : objet lieu, $version (opt) Si spécifié cherche la version, sinon se base sur la langue en cours
	 *  OUT : TRUE si existe, FALSE si libre
	 */
	function checkExist() {
		global $db;

        // VP 10/01/14 : utilisation de $db->like
		// MS 19/05/14 : utilisation de $db->getNormalizedLike qui permet d'ajouter les unaccent à la requete en version postgres (pour être similaire avec fonctionnement mysql)
		$sql="SELECT ID_VAL FROM t_val WHERE  ".str_replace('%','\%',$db->getNormalizedLike('VALEUR',trim($this->t_val['VALEUR'])))." AND ID_LANG=".$db->Quote($this->t_val['ID_LANG'])." AND VAL_ID_TYPE_VAL=".$db->Quote($this->t_val['VAL_ID_TYPE_VAL']);
			//si le lieu existe déjà en base, on l'exclue de la recherche de doublon
		if (!empty($this->t_val['ID_VAL'])) $sql.=" AND ID_VAL<>".intval($this->t_val['ID_VAL']);
		$rs=$db->GetOne($sql);
		return $rs;
	}

/** Vérifie si une valeur existe déjà en base par son code
 *  IN : objet lieu, $version (opt) Si spécifié cherche la version, sinon se base sur la langue en cours
 *  OUT : TRUE si existe, FALSE si libre
 */
function checkExistCode() {
    global $db;
    
    $sql="SELECT ID_VAL FROM t_val WHERE  VAL_CODE ".$db->like($this->t_val['VAL_CODE'])." AND ID_LANG=".$db->Quote($this->t_val['ID_LANG'])." AND VAL_ID_TYPE_VAL=".$db->Quote($this->t_val['VAL_ID_TYPE_VAL']);
    if (!empty($this->t_val['ID_VAL'])) $sql.=" AND ID_VAL<>".intval($this->t_val['ID_VAL']);
    $rs=$db->GetOne($sql);
    return $rs;
}


function checkExistId() {
		global $db;
		$sql="SELECT ID_VAL FROM t_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']);
		$sql.=" AND ID_LANG=".$db->Quote($this->t_val['ID_LANG']);
		return $db->GetOne($sql);
	}

	/**
	 * Sauve en base un lieu (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save ($failIfExists=false) {
		global $db;
		if (empty($this->t_val['ID_VAL'])) {$this->create();return 'crea';} //Protection : redirection de sauvegarde
		$existingVal=$this->checkExist();
		if ($existingVal) {$this->dropError(kErrorValExisteDeja);$this->fusionVal=$existingVal;return false;}
		$this->t_val['VAL_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_val['VAL_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		if (empty($this->t_val['VALEUR'])) return false;
    	//update VG 13/07/11
        if($this->t_val['VAL_ID_GEN'] == $this->t_val['ID_VAL']) {
        	$this->t_val['VAL_ID_GEN'] = '0';
        }
        
		$rs = $db->Execute("SELECT * FROM t_val WHERE ID_VAL=".intval($this->t_val['ID_VAL'])." AND ID_LANG=".$db->Quote($this->t_val['ID_LANG']));
		$sql = $db->GetUpdateSQL($rs, $this->t_val);
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok && !empty($sql)) {$this->dropError(kErrorValSauve);return false;}
		
		//VP 30/6/18 : déplacement appel updateDocChampsXML dans valSaisie
		//$this->updateDocChampsXML(true);
		
		return true;

	}

	/**
	 * @update VG 01/06/2010
	 * @update VG 15/06/2010 : ajout de paramètres
	 *
	 * Créé une nouvelle occurence si la valeur n'existe pas, avec ses versions, et retourne l'idVal
	 */
	function createFromArray($tab_valeurs, $create = true, $update = true, $saveVersion = true) {
		$this->updateFromArray($tab_valeurs);

		//Si la valeur existe déjà, on retourne juste son id
        if(!empty($this->t_val['VALEUR'])){
            $exist = $this->checkExist();
            if ($exist) {
                //@update VG 15/06/2010
                $this->t_val['ID_VAL']=$exist;
                return $exist;
            }
        }elseif(!empty($this->t_val['VAL_CODE'])){
            $exist = $this->checkExistCode();
            if ($exist) {
                $this->t_val['ID_VAL']=$exist;
                return $exist;
            }
        }

		//Sinon on la créé ou on la sauvegarde
		$existId = $this->checkExistId();
		if($existId) {
			if(!$update) return false;
			$ok = $this->save();

		} else {
			if (!$create) return false;
            // VAL_CODE par défaut
            if(empty($this->t_val['VALEUR'])) $this->t_val['VALEUR']=$this->t_val['VAL_CODE'];
			$ok = $this->create(false);
		}

		if (!$ok) return false;

		//On duplique alors aussi pours les versions en langues étrangères
		if($saveVersion) {
			$aLang = getOtherLanguages();
			$valVersions = array();
			foreach ($aLang as $lang) {
				$valVersions[$lang] = $this->t_val['VALEUR'];
			}
		}
		$ok = $this->saveVersions($valVersions);

		if ($ok) return $this->t_val['ID_VAL'];
		else return false;
	}


	/** Sauve les différentes versions
	 *  IN : tableau des versions, MODE = retour de la fonction save appelée avant. Si retour=1=>création
	 * 	OUT : màj base
	 */
	function saveVersions($tabVersions='') {
		global $db;
        if($tabVersions){
            foreach ($tabVersions as $lang=>$valeur) {
                if (!empty($valeur)) {
                
                // MS - 04/04/13 ajout possibilité d'extraire la valeur si la valeur transmise est un array
                if(is_array($valeur) && isset($valeur['VALEUR'])){$valeur = $valeur['VALEUR'];}
                
                $version= new Valeur;
                $version->t_val= $this->t_val; //recopie de tous les champs par deft
                $version->t_val['VALEUR']=$valeur; //update valeur
                $version->t_val['ID_LANG']=$lang; //update langue

                if ($version->checkExistId()) $ok=$version->save(); else $ok=$version->create();

                if ($ok==false && $version->error_msg) {$this->dropError(kErrorValeurVersionSauve.' : '.$version->error_msg);return false;}
                unset($version);
                }
            }
        }
        return true;
	}

	// MS 13.04.15 - pour info : la valeur dont l'id est fusionValId est détruite, les liens entre la valeur détruite et les autres entités sont modifiés pour etre rattachés à la valeur courante ($this)
	function fusion($fusionValId) {
		global $db;
		$db->StartTrans();
		if (empty($fusionValId) || !is_numeric($fusionValId)) {$this->dropError(kErrorValFusionPbId); return false;}
		
		trace("fusion de valeur ".$fusionValId."(sera supprimée) vers ".$this->t_val['ID_VAL']);
		
		$db->Execute("DELETE FROM t_val WHERE id_val=".$fusionValId);

		$this->save();
		
		foreach (array('id_doc'=>'t_doc_val','id_pers'=>'t_pers_val','id_mat'=>'t_mat_val','id_doc_acc'=>'t_doc_acc_val','id_doc_mat'=>'t_doc_mat_val') as $key_column=>$table) { //copie des tables liées
			// MS - 13.04.15 - modification des requetes de fusion : 
			// on update uniquement les liens entre des entités et l'ancienne valeur (fusionValId) qui ne sont pas redondant avec des liens déja existant entre la valeur courante ($this) et une entité donnée
			$sql="UPDATE ".$table." SET ID_VAL=".intval($this->t_val['ID_VAL'])." WHERE ID_VAL=".intval($fusionValId)." and ".$key_column." not in (SELECT ".$key_column." from ".$table." where id_val=".intval($this->t_val['ID_VAL']).")";
			$ok=$db->Execute($sql);
			// les liens restants sont supprimés
			$sql_del="DELETE FROM ".$table." WHERE ID_VAL=".intval($fusionValId);
			$ok_del=$db->Execute($sql_del);
			if (!$ok || !$ok_del) {$this->dropError(kErrorValFusion); return false;}
		}


		return $db->CompleteTrans();
	}



	/**
	 * Crée un lieu en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj et objet màj avec l'ID val créé
	 */
// VP (21/01/10) : ajout paramètre checkDoublon lors de la création
	function create ($checkDoublon=true) {
		global $db;
		if ($checkDoublon && $this->checkExist()) {$this->dropError(kErrorValExisteDeja);return false;}

		$this->t_val['VAL_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_val['VAL_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_val['VAL_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		if (empty($this->t_val['ID_LANG'])) $this->t_val['ID_LANG']=$_SESSION['langue'];
		if (isset($this->t_val['ID_VAL']) && intval($this->t_val['ID_VAL']) == 0) unset($this->t_val['ID_VAL']);
		//$this->t_val['ID_VAL']=$idVal;


		$ok = $db->insertBase("t_val","id_val",$this->t_val);
		if (!$ok) {$this->dropError(kErrorValCreation);trace(kErrorValCreation.$sql);return false;}
		$this->t_val['ID_VAL']=$ok;
		//var_dump($this->t_val['ID_VAL']);
		return true;
	}



	/** Attache une valeur à un autre (relation père-fils). Cette relation est unique et non-bijective.
	 * IN : objet val, tableau de val à attacher (à voir si on utilise pas objet)
	 * OUT : màj de l'objet (PAS de sauvegarde).
	 */
	function attachTo ($valToAttach) {

		if ((int)$valToAttach['VAL_ID_GEN']!=0 ) {
			$this->dropError(kErrorValAttache);
			return false;
		}

		if ((int)$this->t_val['VAL_ID_GEN']!=0 ) {
			$this->dropError(kWarningValDejaAttachee);
		}
		$this->t_val['VAL_ID_GEN']=$plToAttach['ID_VAL'];
	}

	/**
	 * Détache la valeur de sn père
	 * IN : objet val
	 * OUT : objet val màj.
	 */
	function detach () {
		$this->t_val['VAL_ID_GEN']=0;
	}

	/**
	 * Détache tous les fils d'un objet.
	 * IN : objet
	 * OUT : mise à jour BDD;
	 */
	function detachChildren () {
		global $db;
		if (empty($this->t_val['ID_VAL'])) {$this->dropError(kErrorValNoId);return false;}
		$sql="UPDATE t_val SET VAL_ID_GEN=0 WHERE VAL_ID_GEN=".intval($this->t_val['ID_VAL']);
		$ok=$db->Execute($sql);
		if (!$ok) {$this->dropError(kErrorValDetacheTous);return false;} else return true;
	}


	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 *
	 */
	 //@update VG 02/06/2010 : ajout de trim()
	 //@update VG 02/07/2010 : retrait de init() et importation (à partir de init ) de la vérification de l'existence des champs en base
 	function updateFromArray ($tab_valeurs) {
	 	//if (empty($this->t_val)) $this->init();
	 	global $db;
		$cols=$db->MetaColumns('t_val');

	 	foreach ($tab_valeurs as $fld=>$val) {
			//if (isset($this->t_val[strtoupper($fld)])) $this->t_val[strtoupper($fld)]=trim(stripControlCharacters($val));
			if (array_key_exists(strtoupper($fld), $cols)) $this->t_val[strtoupper($fld)]=trim(stripControlCharacters($val));
		}
		// VP 14/12/10 : ajout langue au cas où
		if(empty($this->t_val['ID_LANG']))$this->t_val['ID_LANG']=$_SESSION['langue'];

	}

// 	function updateFromArray ($tab_valeurs) {
//	 	if (empty($this->t_val)) $this->init();
//	 	foreach ($tab_valeurs as $fld=>$val) {
//			if (isset($this->t_val[strtoupper($fld)])) $this->t_val[strtoupper($fld)]=trim(stripControlCharacters($val));
//		}
//	}


	function delete() {
		global $db;
		if (empty($this->t_val['ID_VAL'])) {$this->dropError(kErrorValNoId);return false;}
		$db->StartTrans();
		parent::delete();
		$db->Execute("DELETE FROM t_doc_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']));
		$db->Execute("DELETE FROM t_pers_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']));
		$db->Execute("DELETE FROM t_mat_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']));
		if (!$db->CompleteTrans()) {$this->dropError(kErrorValSuppr);return false;} else return true;
	}

	/**
	 * Récupère les ID des documents liés à cette val
	 * IN : rien
	 * OUT : tab de classe arrDoc
	 */
	function getDocs($withDocVal=true) {
		global $db;
        $this->arrDoc=array();
        $this->arrDocVal=array();
        if(!empty($this->t_val['ID_VAL'])){
            // tableau d'ID documents
            $sql="SELECT ID_DOC from t_doc_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']);
            $this->arrDoc = $db->GetCol($sql);
			if($withDocVal){
			   // tableau de documents
				$sql="SELECT dv.* ,DOC_COTE, DOC_TITRE, DOC_TITRE_COL, DOC_DATE_PROD, TYPE_DOC
				from t_doc_val dv, t_doc d
				LEFT OUTER JOIN t_type_doc td ON td.ID_TYPE_DOC=d.DOC_ID_TYPE_DOC and td.ID_LANG=d.ID_LANG
				WHERE ID_VAL=".intval($this->t_val['ID_VAL'])." and dv.id_doc=d.ID_DOC and d.ID_LANG=".$db->Quote($this->t_val['ID_LANG'])." order by DOC_TITRE";
				$this->arrDocVal = $db->GetAll($sql);
			}
        }
	}

	/**
	 * Récupère les ID des matériels liés à cette val
	 * IN : rien
	 * OUT : tab de classe arrMat
	 */
	function getMats() {
		global $db;
		$sql="SELECT ID_MAT from t_mat_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']);
		$dumb=$db->GetAll($sql);
		foreach ($dumb as $idx=>$evt) $this->arrMat[]=$evt['ID_MAT'];
	}

	/**
	 * Récupère les ID des personnes liées à cette val
	 * IN : rien
	 * OUT : tab de classe arrPers
	 */
	function getPers() {
		global $db;
		$sql="SELECT ID_PERS from t_pers_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']);
		$dumb=$db->GetAll($sql);
		foreach ($dumb as $idx=>$evt) $this->arrPers[]=$evt['ID_PERS'];
	}


	/**
	 * Récupère les ID des valeurs "enfants" liés à cette val
	 * IN : rien
	 * OUT : tab de classe arrChildren
	 */
	function getChildren() {
		global $db;
		if (!isset($this->t_val['ID_VAL']))
			$this->t_val['ID_VAL']=null;
		
		$sql="SELECT * from t_val WHERE ID_LANG=".$db->Quote($_SESSION['langue'])." AND VAL_ID_GEN!=0 AND VAL_ID_GEN=".intval($this->t_val['ID_VAL']);
		$this->arrChildren=$db->GetAll($sql);

	}


	/** Détache les docs liés
	 *  IN : objet
	 * 	OUT : maj base
	 */
	function detachLink($entity) {
		global $db;
		if (!$entity) return false; else $entity=strtolower($entity);
		if($entity=="doc"){
			require_once(modelDir.'model_doc.php');
			$this->getDocs();
		}
		
		// Sécurité, on vérifie d'abord l'existence de la table.
		$cols=$db->MetaColumns('t_'.$entity.'_val');
		if (!$cols) {$this->dropError('XXX : table existe pas');return false;}
		$sql="DELETE FROM t_".$entity."_val WHERE ID_VAL=".intval($this->t_val['ID_VAL']);
		$ok=$db->Execute($sql);
		
		
		if( $ok && $entity=="doc" && !empty($this->arrDoc)){
			foreach($this->arrDoc as $id_doc){
				$doc_lie = new Doc();
				$doc_lie->t_doc['ID_DOC'] = $id_doc;
				$doc_lie->updateChampsXML("DOC_VAL");
				
			}
			unset($this->arrDoc);
		}
		if (!$ok) {$this->dropError('XXX : pas possible coupier lien');return false;}
		return true;
	}


	//update VG 12/10/11
	function updateDocChampsXML($saveSolr=false) {
		require_once(modelDir.'model_doc.php');
		if(empty($this->arrDoc))
			$this->getDocs(false);
        if(!empty($this->arrDoc)){
            foreach ($this->arrDoc as $idDoc) {
                $oDoc = new Doc();
                $oDoc->t_doc['ID_DOC'] = $idDoc;
                $oDoc->updateChampsXML("DOC_VAL");
				if($saveSolr && ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))){
					$oDoc->saveSolr();
				}
               unset($oDoc);
            }
        }
	}
	
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){	
		global $db;
		
		if($mode == null){
			$mode=array();
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		if(strpos(strtolower($critere),'order by') === false){
			$critere.=" order by id_val asc ";
		}
		
		$limit="";
		if ($nb!="all"){
			$limit= $db->limit($nb_offset,$nb);
		}
		
		// définition des caractères à remplacer, 
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		// definition du fichier de sortie 
		$xml_file_path = kServerTempDir."/export_opsis_val_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";

		$sql_val = "SELECT v.*, tv.TYPE_VAL
			FROM t_val v
			LEFT OUTER JOIN t_type_val tv on v.VAL_ID_TYPE_VAL = tv.ID_TYPE_VAL and v.ID_LANG=tv.ID_LANG and v.ID_LANG=".$db->Quote($_SESSION['langue'])."
			".$critere.$limit;
			//trace($sql_val);
		$results_val = $db->Execute($sql_val);

		$arr_ids =  array();
		while($row = $results_val->FetchRow()){
			$arr_ids[] = $row['ID_VAL'];
		}
		$ids = implode(',',$arr_ids);
		if($ids == ''){
			flock($xml_file, LOCK_UN);
			fclose($xml_file);
			unlink($xml_file_path);
			return false;
		}
		
		foreach($results_val as $valeur){
			$xml .= "\t<t_val ID_VAL=\"".$valeur['ID_VAL']."\">\n";
			foreach($valeur as $fld=>$val){
				$xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
			}
			$xml .= "\t</t_val>\n";
            fwrite($xml_file,$xml);
            $xml = "";
		}
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
        flock($xml_file, LOCK_UN);
        fclose($xml_file);
        return $xml_file_path;
	}
	
}?>
