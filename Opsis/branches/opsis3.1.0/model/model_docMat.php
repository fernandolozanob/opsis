<?
require_once(modelDir.'model.php');
class DocMat extends Model
{
	var $error_msg;

	var $t_mat;
	var $t_doc;
	var $t_doc_mat;

	var $id_lang; // NOTE : id_lang n'est pas dans la table, mais il est intéressant pour récupérer les intitulés des tables liées
	var $user_crea,$user_modif; // nom des user de crea et modif

    private $arrDateFields=array("DMAT_DATE_COPIE","DMAT_DATE_ETAT","DMAT_DATE_ETAT_CONSERV");

	function __construct($id=null,$version=''){
		parent::__construct('t_doc_mat',$id, $version); 
	}

	
	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {
 		//@update VG 02/07/2010 : on évite de passer par init (initialisation de tous les champs) et on fait la vérification sur les noms des champs ici
	 	//if (empty($this->t_doc_mat)) $this->init();
		global $db;
		$cols=$db->MetaColumns('t_doc_mat');

	 	foreach ($tab_valeurs as $fld=>$val) {
			// VP 30/06/09 : remplacement isset par array_key_exists à cause des valeurs nulles
			//if (isset($this->t_doc_mat[strtoupper($fld)])) $this->t_doc_mat[strtoupper($fld)]=stripControlCharacters($val);
			if (array_key_exists(strtoupper($fld),$cols)) $this->t_doc_mat[strtoupper($fld)]=stripControlCharacters($val);
		}
	}

	/**
	 * Récupère les infos sur un ou plusieurs matériels.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets mat, sinon màj de l'objet en cours
	 */
	 
	function getDocMat($arrDocMats = array(), $version=""){
		return $this->getData($arrDocMats); 
	}
	
	function getData($arrData=array(),$version=""){
		$arrDocMats=parent::getData($arrData,$version); 
		if (empty($arrDocMats) && !empty($this->t_val) && !$arrData) { 
			if(!empty($this->t_doc_mat['DMAT_ID_USAGER_CREA'])){
				$this->user_crea=$db->CacheGetOne(120,'SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_doc_mat['DMAT_ID_USAGER_CREA']));
			}
			if(!empty($this->t_doc_mat['DMAT_ID_USAGER_MOD'])){
				$this->user_modif=$db->CacheGetOne(120,'SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($this->t_doc_mat['DMAT_ID_USAGER_MOD']));
			}
		}else{
			foreach ($arrDocMats as $idx=>$docmat){ // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
	   			if(!empty($arrDocMats[$idx]->t_doc_mat['DMAT_ID_USAGER_CREA'])){
					$arrDocMats[$idx]->user_crea=$db->CacheGetOne(120,'SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($arrDocMats[$idx]->t_doc_mat['DMAT_ID_USAGER_CREA']));
	   			}
				if(!empty($arrDocMats[$idx]->t_doc_mat['DMAT_ID_USAGER_MOD'])){
					$arrDocMats[$idx]->user_modif=$db->CacheGetOne(120,'SELECT '.$db->Concat('US_NOM',"' '",'US_PRENOM').' from t_usager WHERE ID_USAGER='.intval($arrDocMats[$idx]->t_doc_mat['DMAT_ID_USAGER_MOD']));
				}
			}
			unset($this); // plus besoin
			return $arrDocMats;
		}
	}


/**
 * Sélecteur d'entités à sauvegarder
 * IN : tableau des entités à sauver
 * OUT : sauvegardes
 * NOTE : utilisé pour les modifs par lots
 */
	function multiSave($arr2save=array()) {

		$this->save();
		if ($arr2save['VAL']) $this->saveDocMatVal();
		return true;
	}


	/**
	 * Sauve les informations d'un lien DOC_MAT / VALEUR
	 * IN : tableau DOC_MAT_VAL
	 * OUT : base mise à jour
	 */
	function saveDocMatVal() {
		global $db;
		$db->StartTrans();
		$db->Execute('DELETE FROM t_doc_mat_val WHERE ID_DOC_MAT='.intval($this->t_doc_mat['ID_DOC_MAT']));

		foreach ($this->t_doc_mat_val as $idx=>$dmVal) {
			if (!empty($dmVal['ID_VAL'])) {

				//Précaution en cas de duplication : on spécifie bien que l'id_doc_mat est celui en cours.
				// Effectivement, dans la dupli, on copie les tableaux de val DONC on a l'ID_VAL originel dedans.
				$dmVal['ID_DOC_MAT']=$this->t_doc_mat['ID_DOC_MAT'];
				if ($dmVal['VALEUR']) unset($dmVal['VALEUR']); //On ôte l'objet encapsulé s'il existe

				$record["ID_VAL"] = intval($dmVal['ID_VAL']);
				$record["ID_TYPE_VAL"] = $dmVal['ID_TYPE_VAL'];
				if (!empty($dmVal['ID_DOC_MAT'])) $record["ID_DOC_MAT"] = intval($dmVal['ID_DOC_MAT']);
				$ok = $db->insertBase("t_doc_mat_val","id_doc_mat",$record);
				if (!$ok) trace($db->ErrorMsg());

			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveDocMatVal);trace($sql); return false;} else return true;
	}

	function checkDocMatExists($dm=null) {

		global $db;
		return $db->GetOne('SELECT ID_DOC_MAT FROM t_doc_mat WHERE ID_DOC='.intval($dm===null?$this->t_doc_mat['ID_DOC']:$dm['ID_DOC']).'
				AND ID_MAT='.intval($dm===null?$this->t_doc_mat['ID_MAT']:$dm['ID_MAT']));
	}

	function checkExist($dm=null) {

		global $db;
		return $db->GetOne('SELECT ID_DOC_MAT FROM t_doc_mat WHERE ID_DOC_MAT='.intval($dm===null?$this->t_doc_mat['ID_DOC_MAT']:$dm['ID_DOC_MAT']));
	}



	/**
	 * Supression des liens DOC_MAT (+ t_doc_mat_val)
	 * IN : tab de classe t_doc_mat
	 * OUT : bdd maj et tableau t_doc_mat vidé
	 * NOTE : supprime juste le lien DOC_MAT, PAS le document
	 */
	function delete() {
		global $db;
	        deleteSQLFromArray(array("t_doc_mat","t_doc_mat_val"),"ID_DOC_MAT",$this->t_doc_mat['ID_DOC_MAT']);
	}


	/**
	 * Récupère les valeurs liées à un matériel
	 * IN : ID_DOC_MAT
	 * OUT : tableau t_doc_mat_val (classe)
	 */
	function getValeurs() {
			global $db;
			//VP 05/05/10 : ajout de t_doc_mat_val.ID_TYPE_VAL dans la clause SELECT
		    $sql = "SELECT t_val.*,t_doc_mat_val.ID_TYPE_VAL FROM t_doc_mat_val, t_val where t_doc_mat_val.ID_VAL=t_val.ID_VAL AND t_doc_mat_val.ID_DOC_MAT=".intval($this->t_doc_mat['ID_DOC_MAT'])." AND t_val.ID_LANG=".$db->Quote($this->id_lang);
            $this->t_doc_mat_val=$db->GetAll($sql);
	}


	/** Récupération du document lié
	 * IN : t_doc_mat
	 * OUT : t_doc avec un objet DOC
	 *
	 */

	function getDoc() {
		require_once(modelDir.'model_doc.php');
		global $db;


			$doc=new Doc();
			$doc->t_doc['ID_DOC']=$this->t_doc_mat['ID_DOC'];
			$doc->t_doc['ID_LANG']=$this->id_lang;
			$doc->getDoc();
			$this->t_doc['DOC']=$doc;
		unset($doc); // libération mémoire
	}

	function getMateriel() {
		require_once(modelDir.'model_materiel.php');
		global $db;


			$mat=new Materiel();
			$mat->t_mat['ID_MAT']=$this->t_doc_mat['ID_MAT'];
			$mat->getMat();
			$this->t_mat['MAT']=$mat;
		unset($mat); // libération mémoire
	}
	
	function convDateFields() {
		foreach ($this->t_doc_mat as $fld=>&$val) {
			if (strpos($fld,'DMAT_')===0 && in_array($fld,$this->arrDateFields))
				$val = convDateTime($val);
		}
	}

	/**
	 * Crée un doc mat en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function create ($failIfExist=false) {
		global $db;
		if (empty($this->t_doc_mat['ID_MAT'])) {$this->dropError(kErrorMaterielVide);return false;}
		if (empty($this->t_doc_mat['ID_DOC'])) {$this->dropError(kErrorDocumentIdVide);return false;}
		$exist=$this->checkDocMatExists();
		if ($exist) { //mat existe, on redirige donc vers une sauvegarde std (sans renommage)
			$this->t_doc_mat['ID_DOC_MAT']=$exist;
			if ($failIfExist) {$this->dropError(kErrorDocMatExisteDeja);return false;}
			else return $this->save();
			}
		// VP 16/03/09 : ajout user création par défaut
		$this->t_doc_mat['DMAT_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_doc_mat['DMAT_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];
		$this->convDateFields();
		
		$ok = $db->insertBase("t_doc_mat","id_doc_mat",$this->t_doc_mat);
		if (!$ok) {$this->dropError(kErrorDocMatCreation);trace($sql);return false;}

		$this->dropError(kSuccesDocMatCreation);

		return true;
	}

	/**
	 * Sauve en base un lien document / matériel (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save () {
		global $db;

		if (empty($this->t_doc_mat['ID_DOC_MAT'])) {
			//$this->dropError(kErrorDocMatVide);return false;
			return $this->create();
		}

		$this->t_doc_mat['DMAT_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_doc_mat['DMAT_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		unset($this->t_doc_mat['DMAT_DATE_CREA']);
		unset($this->t_doc_mat['DMAT_ID_USAGER_CREA']);
		$this->convDateFields();

		$rs = $db->Execute("SELECT * FROM t_doc_mat WHERE ID_DOC_MAT=".intval($this->t_doc_mat['ID_DOC_MAT']));
		$sql = $db->GetUpdateSQL($rs, $this->t_doc_mat);
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok && !empty($sql)) {$this->dropError(kErrorDocMatSauve);trace($sql);return false;}

		$this->dropError(kSuccesDocMatSauve);
		return true;
	}


	/**
	 * Crée un matériel en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	 /*
	function create () {
		global $db;
		if (empty($this->t_mat['ID_MAT'])) {$this->dropError(kErrorMaterielVide);return false;}
		$exist=$this->checkExist();
		if ($exist) { //mat existe, on redirige donc vers une sauvegarde std (sans renommage)
			$this->t_mat['ID_MAT']=$exist;$this->id_mat_ref=$exist;return $this->save();} //by ld 01/04/08 retour mis this->save au lieu de false


		$this->t_mat['MAT_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));

		$sql="INSERT INTO t_mat (";
		$sql.=implode(",",array_keys($this->t_mat));
		$sql.=") values ('";
		$tmp=implode("§",array_values($this->t_mat));
		$sql.=str_replace("§","','",simpleQuote($tmp));
		$sql.="')";
		$ok=$db->Execute($sql);
		if (!$ok) {$this->dropError(kErrorMaterielCreation);trace($sql);return false;}

		$this->dropError(kSuccesMaterielCreation);
		$this->hasfile=$this->checkFileExists(); // On regarde si le fichier n'existe pas déjà sur le disque
		// NOTE : cela peut être le cas si le fichier a déjà été transféré (via FTP ou copie) ou en cas de reprise de données

		return true;
	}*/

    //*** XML
     function xml_export($entete=0,$encodage=0,$indent=""){

        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

        if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
        $content.=$indent."<t_doc_mat id_doc_mat=\"".$this->t_doc_mat['ID_DOC_MAT']."\">\n";

            // D'abord l'objet

                foreach ($this->t_doc_mat as $name => $value) {
                    //$content .= $indent."\t<".strtoupper($name).">".str_replace($search,$replace, ereg_replace("\n|\r","|",$value))."</".strtoupper($name).">\n";
                    $content .= $indent."\t<".strtoupper($name).">".str_replace($search,$replace, $value)."</".strtoupper($name).">\n";
                    }

                $content .= "\t<US_CREA>".$this->user_crea."</US_CREA>\n";
                $content .="\t<US_MODIF>".$this->user_modif."</US_MODIF>\n";

            if (!empty($this->t_doc_mat_val)) {
            // I.3. Propri�t�s relatives � t_doc_val
            $content.="\t<t_doc_mat_val nb=\"".count($this->t_doc_mat_val)."\">\n";
                // Pour chaque type de valeur
                foreach ($this->t_doc_mat_val as $name => $value) {
                    $content .= "\t\t<TYPE_VAL ID_TYPE_VAL=\"".$value['VAL_ID_TYPE_VAL']."\" >";
                    // Pour chaque valeur

                        $content .= "\t\t<t_val ID_VAL=\"".$value['ID_VAL']."\">\n";
                        $content .= "\t\t\t<ID_VAL>".$value['ID_VAL']."</ID_VAL>\n";
                        $content .= "\t\t\t<VALEUR>".str_replace($search,$replace, $value['VALEUR'])."</VALEUR>\n";
                        $content .= "\t\t</t_val>\n";

                    $content .= "</TYPE_VAL>\n";
                }
            $content.="\t</t_doc_mat_val>\n";
            }

		   if (!empty($this->t_doc))
		   {
		   		$content.="\t<t_doc><DOC>\n";
		   		$content.=$this->t_doc['DOC']->xml_export("","","\t\t".$indent);
		   		$content.="\t</DOC></t_doc>\n";
		   }

		   if (!empty($this->t_mat))
		   {
		   		$content.="\t<t_mat><MAT>\n";
		   		$content.=$this->t_mat['MAT']->xml_export("","","\t\t".$indent);
		   		$content.="\t</MAT></t_mat>\n";
		   }

 		$content.=$indent."</t_doc_mat>\n";
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }




    function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){
        global $db;

        if($mode == null){
            $mode=array('doc'=>1,'mat'=>1,'val'=>1);
        }

        $critere = "" ;
        if(isset($sql) && !empty($sql)){
            $parsed = parseSimpleSQL($sql);
            foreach($parsed as $part){
                if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
                    $critere .=$part['segment'] ;
                }
            }
        }
        if(strpos(strtolower($critere),'order by') === false){
            $critere.=" order by dm.id_doc_mat asc ";
        }

        $limit="";
        if ($nb!="all"){
            $limit= $db->limit($nb_offset,$nb);
        }

        // définition des caractères à remplacer,
        $search_chars = array(" ' ","’","œ","\n");
        $replace_chars = array("'","'","oe","\r\n");

        if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
            $search_chars = array_merge($search_chars,$search_sup);
            $replace_chars = array_merge($replace_chars,$replace_sup);
        }

        // definition du fichier de sortie
        $xml_file_path = kServerTempDir."/export_opsis_docmat_".microtime(true).".xml";
        $xml_file=fopen($xml_file_path,"w");
        flock($xml_file, LOCK_EX);
        fwrite($xml_file,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        fwrite($xml_file,"<EXPORT_OPSIS>\n");
        $xml="";

        $sql_docmat = "SELECT dm.*, ".$db->Concat('t2.US_NOM',"' '",'t2.US_PRENOM')." as US_CREA, ".$db->Concat('t3.US_NOM',"' '",'t3.US_PRENOM')." as US_MODIF
            FROM t_doc_mat dm
            LEFT OUTER JOIN t_usager t2 ON dm.DMAT_ID_USAGER_CREA= t2.ID_USAGER
            LEFT OUTER JOIN t_usager t3 ON dm.DMAT_ID_USAGER_MOD = t3.ID_USAGER
            ".$critere.$limit;
                           
        $results_docmat = $db->Execute($sql_docmat);
        $arr_ids =  array();
        while($row = $results_docmat->FetchRow()){
            $arr_ids[] = $row['ID_DOC_MAT'];
        }
        $ids = implode(',',$arr_ids);
        if($ids == ''){
            flock($xml_file, LOCK_UN);
            fclose($xml_file);
            unlink($xml_file_path);
            return false;
        }

        if($mode['val']){
            $sql_val="SELECT distinct t_doc_mat_val.*, t_val.* from t_doc_mat_val
                LEFT OUTER JOIN t_val on t_doc_mat_val.ID_VAL=t_val.ID_VAL AND ID_LANG=".$db->Quote($_SESSION['langue'])."
                where t_doc_mat_val.ID_DOC_MAT in ($ids) ORDER BY  t_doc_mat_val.ID_DOC_MAT ASC";
            $results_val = $db->Execute($sql_val);
            unset($sql_val);
        }

        if($mode['doc']){
            $sql_doc="SELECT distinct t_doc_mat.ID_DOC_MAT,t_doc.* from t_doc_mat
                LEFT OUTER JOIN t_doc on t_doc_mat.ID_DOC=t_doc.ID_DOC AND ID_LANG=".$db->Quote($_SESSION['langue'])."
                where t_doc_mat.ID_DOC_MAT in ($ids) ORDER BY t_doc_mat.ID_DOC_MAT ASC";
            $results_doc = $db->Execute($sql_doc);
            unset($sql_doc);
        }

        if($mode['mat']){
            $sql_mat="SELECT distinct t_doc_mat.ID_DOC_MAT,t_mat.* from t_doc_mat
                LEFT OUTER JOIN t_mat on t_doc_mat.ID_MAT=t_mat.ID_MAT
                where t_doc_mat.ID_DOC_MAT in ($ids) ORDER BY t_doc_mat.ID_DOC_MAT ASC";
            $results_mat = $db->Execute($sql_mat);
            unset($sql_mat);
        }
                           
                           
        while($mode['val'] && !empty($results_val) && $val = $results_val->FetchRow()){
            $results_val_ref[$val['ID_DOC_MAT']][]=$results_val->CurrentRow()-1;
        }
        while($mode['doc'] && !empty($results_doc) && $doc = $results_doc->FetchRow()){
            $results_doc_ref[$doc['ID_DOC_MAT']][]=$results_doc->CurrentRow()-1;
        }
        while($mode['mat'] && !empty($results_mat) && $mat = $results_mat->FetchRow()){
            $results_mat_ref[$mat['ID_DOC_MAT']][]=$results_mat->CurrentRow()-1;
        }
                           
        foreach($results_docmat as $docmat){
            $xml .= "\t<t_doc_mat ID_DOC_MAT=\"".$docmat['ID_DOC_MAT']."\">\n";
            foreach($docmat as $fld=>$val){
                //on ajoute pas de CDATA sur les champs  qui contiennent du XML car on veut pouvoir les traiter dans le xsl, si ils sont CDATA, ils ne sont pas parcourable via xpath
                $xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
            }

            //======== GESTION DOC_MAT_VAL ==============

            if(isset($results_val_ref[$docmat['ID_DOC_MAT']][0])){
                $results_val->Move($results_val_ref[$docmat['ID_DOC_MAT']][0]);
                $array_val = $results_val->GetRows(count((array)$results_val_ref[$docmat['ID_DOC_MAT']]));
            }else{
                $array_val= array();
            }
            if(count($array_val)>0){
                $xml.="\t\t<t_doc_mat_val>\n";
                foreach($array_val as $dmat_val){
                    $xml.="\t\t\t<t_val ID_VAL=\"".$dmat_val['ID_VAL']."\" ID_TYPE_VAL=\"".$dmat_val['ID_TYPE_VAL']."\">\n";
                    foreach($dmat_val as $fld=>$val){
                        $xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                    }
                    $xml.="\t\t\t</t_val>\n";
                }
                $xml.="\t\t</t_doc_mat_val>\n";
            }
                           
            //======== GESTION DOC ==============

            if(isset($results_doc_ref[$docmat['ID_DOC_MAT']][0])){
                $results_doc->Move($results_doc_ref[$docmat['ID_DOC_MAT']][0]);
                $array_doc = $results_doc->GetRows(count((array)$results_doc_ref[$docmat['ID_DOC_MAT']]));
            }else{
                $array_doc= array();
            }
            if(count($array_doc)>0){
                foreach($array_doc as $doc){
                    $xml.="\t\t<t_doc ID_DOC=\"".$doc['ID_DOC']."\">\n";
                    foreach($doc as $fld=>$val){
                       //on ajoute pas de CDATA sur les champs qui contiennent du XML car on veut pouvoir les traiter dans le xsl, si ils sont CDATA, ils ne sont pas parcourable via xpath
                       if(in_array($fld,array('DOC_CAT','DOC_VAL','DOC_LEX','DOC_XML','DOC_TRANSCRIPT')) || strpos($val,'XML') !== false){
                            if($fld=='DOC_TRANSCRIPT')
                               $val=str_replace(array('<VIRG>','&'),array('<VIRG/>','&amp;'),$val);
                            $xml .= "\t\t\t<".strtoupper($fld).">".str_replace($search_chars,$replace_chars,$val)."</".strtoupper($fld).">\n";
                       }else {
                           $xml .= "\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                       }
                    }
                    $xml.="\t\t</t_doc>\n";
                }
            }

            //======== GESTION MAT ==============

            if(isset($results_mat_ref[$docmat['ID_DOC_MAT']][0])){
                $results_mat->Move($results_mat_ref[$docmat['ID_DOC_MAT']][0]);
                $array_mat = $results_mat->GetRows(count((array)$results_mat_ref[$docmat['ID_DOC_MAT']]));
            }else{
                $array_mat= array();
            }
            if(count($array_mat)>0){
                foreach($array_mat as $mat){
                    $xml.="\t\t<t_mat ID_MAT=\"".$mat['ID_MAT']."\">\n";
                    foreach($mat as $fld=>$val){
                        //on ajoute pas de CDATA sur les champs qui contiennent du XML car on veut pouvoir les traiter dans le xsl, si ils sont CDATA, ils ne sont pas parcourable via xpath
                        if(in_array($fld,array('MAT_VAL','MAT_LEX','MAT_XML','MAT_INFO'))){
                            $xml .= "\t\t\t<".strtoupper($fld).">".str_replace($search_chars,$replace_chars,$val)."</".strtoupper($fld).">\n";
                        }else {
                            $xml .= "\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
                        }
                    }
                    $xml.="\t\t</t_mat>\n";
                }
            }

                           
            $xml .= "\t</t_doc_mat>\n";
            fwrite($xml_file,$xml);
            $xml = "";
        }

        fwrite($xml_file,"</EXPORT_OPSIS>\n");
        flock($xml_file, LOCK_UN);
        fclose($xml_file);

        return $xml_file_path;
                           
    }
                           
    /** Effectue le mappage entre un tableau et des champs de doc_mat
	 *
	 */
	function mapFields($arr) {

		require_once(modelDir."model_val.php");

		foreach ($arr as $idx=>$val) {

				$myField=$idx;
				// analyse du champ
				if ($myField=='ID_DOC_MAT') $this->t_doc_mat['ID_DOC_MAT']=$val;
				if ($myField=='ID_DOC') $this->t_doc_mat['ID_DOC']=$val;
				if ($myField=='ID_MAT') $this->t_doc_mat['ID_MAT']=$val;
				$arrCrit=split("_",$myField);
				switch ($arrCrit[0]) {
					case 'DMAT': //champ table DOC
						$val=trim($val); //Important, car souvent, les tags ID3 ont des espaces supplémentaires
						$flds=split(",",$myField);
						if (!empty($val)) foreach ($flds as $_fld) $this->t_doc_mat[$_fld]=$val;
					break;

					case 'V': //valeur
						if (empty($val)) break;
						$myVal=new Valeur();
						$myVal->t_val['VALEUR']=$val;
						$myVal->t_val['VAL_ID_TYPE_VAL']=$arrCrit[1];
						$myVal->t_val['ID_LANG']=$_SESSION['langue'];
						$_val=$myVal->checkExist();

						if ($_val) {
							$myVal->t_val['ID_VAL']=$_val;
							$myVal->save(); }
						else {
							$myVal->create(); //création dans la langue
							foreach ($_SESSION['arrLangues'] as $lg) {
								if ($lg!=$_SESSION['langue']) $arrVersions[$lg]=$val;
							}
							$myVal->saveVersions($arrVersions); //sauve dans autres langues
							unset($arrVersions);
						}

						$this->t_doc_mat_val[]=array('ID_TYPE_VAL'=>$arrCrit[1],'ID_VAL'=>$myVal->t_val['ID_VAL']);
					break;

				}
		}
	}

	static function setAutoIncrement() {
		global $db;
		$db->Execute("ALTER TABLE t_doc_mat AUTO_INCREMENT=0"); //Attention, ne fonctionne que ss mysql (p-ê)
	}


}
?>
