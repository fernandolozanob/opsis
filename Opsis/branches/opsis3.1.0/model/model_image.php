<?php
require_once(modelDir.'model.php');
class Image extends Model{

	var $t_image;
	var $error_msg;
	var $callingImageur; //objet imageur appelant

	// MS - N'A PAS ETE MIS A JOUR APRES PASSAGE EN MODEL, A REVOIR
	function __construct($callingImageur=null) { //Passage de l'imageur appelant à la construction
		$this->callingImageur=$callingImageur;
	}

	/**
	 * Initialise l'objet en récupérant les champs depuis la base
	 */
	function init() {
		global $db;
		$cols=$db->MetaColumns('t_image');
		foreach ($cols as $i=>$col) $this->t_image[$i]='';
		$this->t_image['ID_LANG']=$_SESSION['langue']; // par déft, langue en cours
	}


	/**
	 * Récupération d'images depuis la base, selon différents modes "mixables"
	 * IN : tableau des critères arrCrit (cf notes)
	 * OUT : tab de class t_image
	 * NOTES : structure de tabCrit :
	 * 	[ID_LANG]=> langue, commune à tous les modes, unique. Si absente, la session est utilisée
	 * 	[ID_IMAGE] => array de ID d'images pour récupération directe (mode image)
	 *	[IMAGEUR][n]=> array de n imageurs dont on va récupérer les images (mode imageur)
	 *	[LIMITS][n]=> array de n paires de [TCIN],[TCOUT] pour désigner les limites de TC pour chaque IMAGEUR (optionnel, uniquement en mode imageur)
	 * Les deux modes peuvent être utilisés conjointement (ie [IMAGEUR] et [ID_IMAGE] remplis
	 * En mode imageur, les images sont triées par TC asc, en mode image, par ID asc
	 */
	function getImage($arrCrit = null) {
		return $this->getData($arrCrit) ; 
	}
	// MS - pour l'instant j'ai juste renommé la fonction, à voir pour une refonte plus propre 
	function getData($arrCrit = null,$version=""){
		global $db;

		if (!$arrCrit['ID_IMAGE'] && isset($this->t_image['ID_IMAGE']) && !empty($this->t_image['ID_IMAGE'])){
			$arrCrit['ID_IMAGE']=array($this->t_image['ID_IMAGE']);
		}
		if (!$arrCrit['ID_LANG']){
			if(!empty($version)){
				$arrCrit['ID_LANG']=$version;
			}else{
				$arrCrit['ID_LANG']=$_SESSION['langue'];
			}
		}
		$this->t_image=array();
		
		if(isset($arrCrit['ID_IMAGEUR'])){
			foreach ($arrCrit['ID_IMAGEUR'] as $idx=>$imageur) { // Mode sélection d'images par imageur (et TC en option)
				$sql="SELECT * FROM t_image WHERE ID_LANG=".$db->Quote($arrCrit['ID_LANG'])."
				and ID_IMAGEUR=".intval($imageur);
				if (!empty($arrCrit['LIMITS'][$idx]) &&
					!empty($arrCrit['LIMITS'][$idx]['TCIN']) &&
					!empty($arrCrit['LIMITS'][$idx]['TCOUT']) &&
					$arrCrit['LIMITS'][$idx]['TCIN']<$arrCrit['LIMITS'][$idx]['TCOUT']
					) { //On a spécifié des TC
				$sql.=" AND IM_TC>=".$db->Quote($arrCrit['LIMITS'][$idx]['TCIN'])." AND IM_TC<=".$db->Quote($arrCrit['LIMITS'][$idx]['TCOUT']);
				}
				// VP 22/01/2013 : tri par TC puis Chemin
				$sql.=" ORDER BY IM_TC, IM_FICHIER";
				$this->t_image=array_merge($this->t_image,$db->GetAll($sql));
			}
		}

		if (isset($arrCrit['ID_IMAGE']) && $arrCrit['ID_IMAGE']) { // Mode sélection d'images par ID
			$sql="SELECT * FROM t_image WHERE ID_LANG=".$db->Quote($arrCrit['ID_LANG'])."
			and ID_IMAGE IN (".implode(',',$arrCrit['ID_IMAGE']).") ORDER BY ID_IMAGE ASC";
			$this->t_image=array_merge($this->t_image,$db->GetAll($sql));
		}
		foreach ($this->t_image as &$img) {//Cette image est la vignette du doc (param issu de l'imageur)
			if ($img['ID_IMAGE']==$this->callingImageur->selectedPic) $img['selected']=true; //ajout d'un champ selected
		}

		if (count($this->t_image)==1 && !$arrCrit['ID_IMAGEUR']) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->t_image=$this->t_image[0];
		}
		else {
			$_arrIDs=array();
			$arrImgs=array();
			
			foreach ($this->t_image as $idx=>$Img) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats

				if (!in_array($Img['ID_IMAGE'],$_arrIDs)) { //dédoublonnage des images renvoyées
					$_arrIDs[]=$Img['ID_IMAGE'];
					$arrImgs[$idx]= new Image();
					$arrImgs[$idx]->t_image=$Img;
				}
			}
			unset($this); // plus besoin
			return $arrImgs;
		}

	}

	public function getFilePath()
	{
		return $this->t_image['IM_CHEMIN'].'/'.$this->t_image['IM_FICHIER'];
	}
	
	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 *
	 */
 	function updateFromArray ($tab_valeurs) {
	 	if (empty($this->t_image)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_image[strtoupper($fld)])) $this->t_image[strtoupper($fld)]=$val;
		}

	}


/** Création d'une image en base
 * 	IN : $idImg => ID_IMAGE, 0 si pure création, mais peut avoir une valeur lorsque l'on crée les versions d'une même image
 * 	OUT : màj base et objet t_image
 *
 * 	NOTE : le seul moyen de créer des images est de passer par l'upload.
 */
	function create($idImg=0) {
		global $db;
		if (!empty($this->t_image['ID_IMAGE']) && $idImg==0 ) {if (!$this->save()) return false; else return 'save';} //Protection : redirection de sauvegardes
		$exImg=$this->checkExist();
		if ($exImg) {$this->t_image['ID_IMAGE']=$exImg;$this->dropError(kErrorImageExisteDeja);return false;}
		
		$this->t_image['IM_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		if (empty($this->t_image['ID_LANG'])) $this->t_image['ID_LANG']=$_SESSION['langue'];
		if (!empty($idImg)) $this->t_image['ID_IMAGE']=$idImg;
		else unset($this->t_image['ID_IMAGE']);
		
		$ok = $db->insertBase("t_image","id_image",$this->t_image);
		
		if (!$ok) {$this->dropError(kErrorImageCreation);trace(kErrorImageCreation.$sql);return false;}
		$this->t_image['ID_IMAGE']=($idImg?$idImg:$ok);
		return true;
	}

	/** Sauve les différentes versions
	 *  IN : tableau des versions (langues), MODE = crea pour création, sinon update.
	 * 	OUT : màj base.
	 */
	function saveVersions($arrLangues='', $mode=true) {
		global $db;
		if ($mode===false) return false;
		foreach ($arrLangues as $lang) { //boucle sur chaque langue
			if ($lang!=$_SESSION['langue']) {
				$version= new Image;
				$version=$this;
				$version->t_image['ID_LANG']=$lang;
				if ($mode=='crea') //On vient d'une création -> on va créer les différentes versions
					{$ok=$version->create($version->t_image['ID_IMAGE']);}
					else
					{$ok=$version->save();} //id image existe -> version existe déjà
				if ($ok==false && $version->error_msg) {$this->dropError(kErrorVersionSauve.' : '.$version->error_msg);return false;}
				unset($version);
			}
		}
		return true;
	}

	/**
	 * Sauve en base une image (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save () {
		global $db;
		
		if (empty($this->t_image['ID_IMAGE']))
		{
			if (!$this->create()) 
				return false; 
			else 
				return 'crea';
		} //Protection : redirection de sauvegarde

		$existingImg=$this->checkExist();
		if ($existingImg) 
		{
			$this->t_image['ID_IMAGE']=$existingImg;
			$this->dropError(kErrorImageExisteDeja);
			return false;
		}

		$rs = $db->Execute("SELECT * FROM t_image WHERE ID_IMAGE=".intval($this->t_image['ID_IMAGE'])." AND ID_LANG=".$db->Quote($this->t_image['ID_LANG']));
		$sql = $db->GetUpdateSQL($rs, $this->t_image);
		
		if (!empty($sql))
			$ok = $db->Execute($sql);
		
		if (!$ok && !empty($sql)) 
		{
			$this->dropError(kErrorImageSauve);
			return false;
		}
		
		return true;

	}

	/** Vérifie si l'ID d'une image existe en base
	IN : obj image
	OUT : id image si existe ou false si n'existe pas
	NOTE : créé le 23 02 09 pour l'import d'images
	**/
	function checkExistId() {
		global $db;
		
		if (empty($this->t_image['ID_IMAGE']))
			return false;
		
		return $db->GetOne("SELECT ID_IMAGE from t_image WHERE ID_IMAGE=".intval($this->t_image['ID_IMAGE']));
	}

	/** Vérifie si une image existe déjà en base, se base sur le nom d'image et la langue
	 *  IN : objet t_image
	 *  OUT : ID_IMAGE doublon si existe, FALSE si libre
	 */
	function checkExist() {
		global $db;

		//$sql="SELECT ID_IMAGE FROM t_image WHERE TRIM(LOWER(IM_FICHIER))=".$db->Quote(trim(strtolower($this->t_image['IM_FICHIER'])));
		$sql="SELECT ID_IMAGE FROM t_image WHERE IM_CHEMIN=".$db->Quote($this->t_image['IM_CHEMIN'])." AND IM_FICHIER=".$db->Quote($this->t_image['IM_FICHIER']);

		if ($this->t_image['ID_IMAGEUR']!='')
			$sql.=" AND ID_IMAGEUR=".intval($this->t_image['ID_IMAGEUR']);
		//$sql.="		AND ID_LANG=".$db->Quote($this->t_image['ID_LANG']) ;

		if (!empty($this->t_image['ID_IMAGE']))
			$sql.=" AND ID_IMAGE<>".$this->t_image['ID_IMAGE'];
		
		//trace($sql);
		$rs=$db->GetOne($sql);

		return $rs;
	}

	/**
   * supprime une image de la base (voire du disque)
   * IN : tab classe image + delete_file (bool, opt) suppression de l'image du disque ?
   * OUT : màj base + disque si demandé. false si erreur
   */
    function deleteImage($delete_file=true){
		global $db;
		$erreur="";

		if($delete_file ) {
			if (empty($this->t_image['IM_FICHIER']))
				$this->getImage(array("ID_IMAGE"=>array($this->t_image['ID_IMAGE'])));

			$path_image=kStoryboardDir.$this->getFilePath();
			
			if (is_file($path_image)){
				trace ('remove image : '.$path_image);
				if(!unlink($path_image)){
					$this->dropError(kErrorImageurDeleteImageAbsente);
					return false;
				}
			}
		}
		// VP 8/04/10 : initialisation DOC_ID_IMAGE
		$sql="UPDATE t_doc SET DOC_ID_IMAGE=0 WHERE DOC_ID_IMAGE=".intval($this->t_image['ID_IMAGE']);
		$ok=$db->Execute($sql);
		
		if (!$ok)
			return false;

		deleteSQLFromArrayOfCrit("t_image",array("ID_IMAGE"=>$this->t_image['ID_IMAGE']));

		return true;
    }


	/**
	 * Ajoute un message d'erreur pour la sortie
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function dropError($errLib) { //: A ETOFFER EN TRY CATCH ?
		$this->error_msg.=$errLib."<br/>";
	}


    //*** XML
    function xml_export($entete=0,$encodage=0,$indent=''){

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

        $content="";
	                $content.=$indent."<t_image ID_IMAGE=\"".$this->t_image["ID_IMAGE"]."\">\n";
					foreach ($this->t_image as $fld=>$val) {
						$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
					}

                $content.=$indent."</t_image>\n";
		if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
    }
	
}

?>
