<?php

require_once(modelDir."/model.php");
class Config extends Model
{
	private $parametre;
	private $valeur;
	
	function __construct($id=null,$version='')
	{
		$this->parametre='';
		$this->valeur='';
		parent::__construct('t_config',$id, $version); 
	}
	
	function getData($arrData=array(),$version=""){
		$arrConfig = parent::getData($arrData,$version);
		if(empty($arrConfig)){
			$this->parametre = $this->t_config['PARAMETRE'];
			$this->valeur = $this->t_config['VALEUR'];
		}else{
			foreach($arrConfig as $idx=>$config){
				$arrConfig[$idx]->parametre = $arrConfig[$idx]->t_config['PARAMETRE'];
				$arrConfig[$idx]->valeur = $arrConfig[$idx]->t_config['VALEUR'];
			}
		}
		return $arrConfig;
	}	
	
	public function getConfig($param){
		return $this->getData($param);
	}
	
	// getters
	public function getParamName()
	{
		return $this->parametre;
	}
	
	public function getValue()
	{
		return $this->valeur;
	}
	
	// setters
	public function setParamName($name)
	{
		$this->parametre=htmlentities($name);
	}
	
	public function setValue($val)
	{
		$this->valeur=htmlentities($val);
	}
	
	public function save()
	{
		global $db;
		
		$result=$db->Execute('SELECT COUNT(*) as compte FROM t_config WHERE PARAMETRE=\''.$this->parametre.'\'')->GetRows();
		
		if ($result[0]['compte']==0)
			$this->create();
		else
		{
			$db->Execute('UPDATE t_config SET VALEUR=\''.$this->valeur.'\' WHERE PARAMETRE=\''.$this->parametre.'\'');
		}
	}
	
	function create()
	{
		global $db;
		
		$db->Execute('INSERT INTO t_config VALUES (\''.$this->parametre.'\',\''.$this->valeur.'\')');
	}
	
	public static function getValueFromParamName($nom)
	{
		global $db;
		
		$result=$db->Execute('SELECT * FROM t_config WHERE PARAMETRE=\''.addslashes($nom).'\'')->GetRows();
		
		if (count($result)==0)
			return null;
		else
			return $result[0]['VALEUR'];
	}
}

?>