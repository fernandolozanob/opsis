<?php
require_once(modelDir.'model.php');
class EtatArch extends Model
{
	private $t_etat_arch;
	
	function __construct($id_etat_arch='',$id_lang='')
	{
		$this->t_etat_arch=array();
		if (isset($id_etat_arch) && !empty($id_etat_arch) && isset($id_lang) && !empty($id_lang))
		{
			$this->getEtatArch($id_etat_arch,$id_lang);
		}
	}
	
	// MS - TODO refonte model + refonte getData
	public function getEtatArch($id_etat_arch,$id_lang)
	{
		global $db;
		
		if (!empty($id_etat_arch) && is_numeric($id_etat_arch) && !empty($id_lang))
		{
			$res=$db->Execute("SELECT * FROM t_etat_arch WHERE ID_ETAT_ARCH=".intval($id_etat_arch)." AND ID_LANG='$id_lang'")->getRows();
			$this->t_etat_arch['ID_ETAT_ARCH']=intval($res[0]['ID_ETAT_ARCH']);
			$this->t_etat_arch['ID_LANG']=$res[0]['ID_LANG'];
			$this->t_etat_arch['ETAT_ARCH']=$res[0]['ETAT_ARCH'];
			return true;
		}
		
		return false;
	}
	
	public function setNomEtat($nom_etat)
	{
		$this->t_etat_arch['ETAT_ARCH']=addslashes(htmlentities($nom_etat));
	}
	
	public function setIdLang($id_lang)
	{
		$this->t_etat_arch['ID_LANG']=addslashes(htmlentities($id_lang));
	}
	
	public function getIdEtatArch()
	{
		return $this->t_etat_arch['ID_ETAT_ARCH'];
	}
	
	public function getIdLang()
	{
		return $this->t_etat_arch['ID_LANG'];
	}
	
	public function getNomEtat()
	{
		return $this->t_etat_arch['ETAT_ARCH'];
	}
	
	public function save()
	{
		global $db;
		// PROBLEME DE CONCEPTION : il y a deux cles primaires dans la table
		if (isset($this->t_etat_arch['ID_ETAT_ARCH']) && !empty($this->t_etat_arch['ID_ETAT_ARCH']))
			$res=$db->Execute('SELECT COUNT(*) AS compte FROM t_etat_arch WHERE ID_ETAT_ARCH='.$this->t_etat_arch['ID_ETAT_ARCH']." AND ID_LANG='".$this->t_etat_arch['ID_LANG']."'")->getRows();
		else
			$res[0]['compte']=0;
		
		if ($res[0]['compte'])
		{
			// modification
			$db->Execute('UPDATE t_etat_arch SET ETAT_ARCH='.$db->Quote($this->t_etat_arch['ETAT_ARCH'])." WHERE ID_ETAT_ARCH=".intval($this->t_etat_arch['ID_ETAT_ARCH'])." AND ID_LANG='".$this->t_etat_arch['ID_LANG']."'");
			return true;
		}
		else
			return $this->create();
	}
	
	function create()
	{
		global $db;
		
		// si la cle primaire est prescisee
		if (isset($this->t_etat_arch['ID_ETAT_ARCH']) && !empty($this->t_etat_arch['ID_ETAT_ARCH']))
			$db->Execute('INSERT INTO t_etat_arch(ID_ETAT_ARCH,ID_LANG,ETAT_ARCH) VALUES ('.intval($this->t_etat_arch['ID_ETAT_ARCH']).",'".$this->t_etat_arch['ID_LANG']."','".$this->t_etat_arch['ETAT_ARCH']."')");
		else
			$db->Execute("INSERT INTO t_etat_arch(ID_LANG,ETAT_ARCH) VALUES ('".$this->t_etat_arch['ID_LANG']."','".$this->t_etat_arch['ETAT_ARCH']."')");
		
		$this->t_etat_arch['ID_ETAT_ARCH']=$db->getOne("select max(ID_ETAT_ARCH) FROM t_etat_arch");
		
		return true;
	}
	
	
	
	
	
	
	
	public static function getAllEtatArch($id_lang)
	{
		global $db;
		
		if (isset($id_lang) && !empty($id_lang))
		{
			$res=$db->Execute("SELECT ID_ETAT_ARCH,ID_LANG FROM t_etat_arch WHERE ID_LANG='".$id_lang."'")->getRows();
			$list_etat_arch=array();
			
			foreach ($res as $etat_arch)
			{
				$tmp_etat_arch=new EtatArch($etat_arch['ID_ETAT_ARCH'],$etat_arch['ID_LANG']);
				$list_etat_arch[]=$tmp_etat_arch;
			}
			
			return $list_etat_arch;
		}
		
		return null;
	}
}

?>