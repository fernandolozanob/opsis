<?php
require_once(modelDir."/model.php");

class Role extends Model {

var $t_role;

	function __construct($id=null, $version="") {
		parent::__construct("t_role",$id, $version);
	}

	/** Vérifie existance en base
	 *  IN : objet
	 *  OUT : TRUE si existe, FALSE si libre
	 */
	function checkExist() {
		global $db;
		
		$sql="SELECT ID_ROLE FROM t_role WHERE  ".$db->getNormalizedLike('ROLE',trim($this->t_role['ROLE']))." AND ID_LANG=".$db->Quote($this->t_role['ID_LANG'])." AND ID_TYPE_DESC=".$db->Quote($this->t_role['ID_TYPE_DESC']);
		//si le lieu existe déjà en base, on l'exclue de la recherche de doublon
		if (!empty($this->t_role['ID_ROLE'])) $sql.=" AND ID_ROLE<>".$db->Quote($this->t_role['ID_ROLE']);
		$rs=$db->GetOne($sql);
		return $rs;
	}
	
	/**
	 * Vérification existence par ID
	 * IN : objet
	 * OUT
	 */
	function checkExistId() {
		global $db;
		
		if(!$this->table || !$this->key)
			return false;
		
		$sql="SELECT $this->key FROM $this->table WHERE $this->key=".$db->Quote($this->{$this->table}[$this->key]);
		if($this->haslang)
			$sql.=" AND ID_LANG=".$db->Quote($this->{$this->table}['ID_LANG']);
		$id=$db->GetOne($sql);
		return $id;
	}
	
	/**
	 *
	 * Crée un nouvel objet en base
	 */
	function create() {
		global $db;
		
		$ok = $db->insertBase("t_role","id_role",$this->t_role);
		return $ok;
	}
	/**
	 *
	 * Crée un nouvel objet s'il n'existe pas, et retourne l'id
	 */
	function createFromArray($tab_valeurs, $create = true) {
		if(empty($tab_valeurs['ROLE'])){
			return false;
		}
		$this->updateFromArray($tab_valeurs);
		//Si la valeur existe déjà , on retourne juste son id
		$exist = $this->checkExist();
		if ($exist) {
			$this->t_role['ID_ROLE'] = $exist;
			return $this->t_role['ID_ROLE'];
		}
		
		if(!$create) {
			return false;
		}
		// Génération id_role
		$root = substr(strtoupper(replaceAccents($this->t_role['ROLE'])), 0, 3);
		$this->t_role['ID_ROLE'] = $root;
		if($this->checkExistId()){
			$existId=true;
			$i=0;
			while($existId && $i<3) {
				$root = substr($root, 0, -1);
				$j = 0;
				$max = pow(10, $i+1);
				while($existId && $j<$max){
					$this->t_role['ID_ROLE'] = sprintf($root.'%0'.($i+1).'d', $j);
					if(!$this->checkExistId()){
						$existId = false;
					}
					$j++;
				}
				$i++;
			}
		}
		// Sauvegarde
		if(!$existId){
			foreach ($_SESSION['arrLangues'] as $lg) {
				$this->t_role['ID_LANG']=$lg;
				$ok = $this->create();
			}
			return $this->t_role['ID_ROLE'];
		}
	}
	
}?>
