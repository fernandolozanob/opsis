<?
//************************************************** TAPE ***************************************************************

// VP : 6/01/2009 : Création du fichier, définition de la classe Tape


//************************************************** TAPE ***************************************************************
require_once(modelDir.'model.php');
class Tape extends Model 
{

	var $t_tape;
	var $t_tape_file;
	var $t_tape_set;
	var $error_msg;
	var $status;
	
	function __construct($id=null,$version='') {
		parent::__construct('t_tape',$id,$version);
	}

	
	/** Récupère un objet tape ou un tableau d'objet
		* 	IN : tableau
		* 	OUT : objet(s)
		*/
	function getTape($arrTape=array()) {
		global $db;
		if (!$arrTape) $arrVals=array($this->t_tape['ID_TAPE']); else $arrVals=(array)$arrTape;
		$arrCrit['ID_TAPE']=$arrVals;

		$this->t_tape=getItem("t_tape",null,$arrCrit,null,0); 
		
		if (count($this->t_tape)==1 && !$arrTape) { // Un seul résultat : on affecte le résultat à la valeur en cours
			$this->t_tape=$this->t_tape[0];
			$this->getStatus();
		}
		else {
            $arrVals=array();
			foreach ($this->t_tape as $idx=>$Tape) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx]= new Tape();
				$arrVals[$idx]->t_tape=$Tape;
				$arrVals[$idx]->getStatus();
			}
			unset($this); // plus besoin
			return $arrVals;
		}
	}

	/** Récupère le statut
		* 	IN : objet
		* 	OUT : status mis à jour
		*/
	function getStatus() {
		$this->status=($this->t_tape['TAPE_STATUS']=='1'?kOnline:kOffline);
	}



	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {
	 	if (empty($this->t_tape)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_tape[strtoupper($fld)])) $this->t_tape[strtoupper($fld)]=$val;
			if($fld=="t_tape_file" && is_array($val)){
				foreach($val as $i=>$x){
					foreach($x as $j=>$y){
						if(!empty($y)) $this->t_tape_file[$j][$i]=$y;
					}
				}
			}
		}
	}

	/** Met à jour l'objet avec les données provenant du fichier info
		* 	IN : tableau
		* 	OUT : objet màj
		*/
 	function updateFromInfoFile () {
		
		$infoFile=kBackupInfoDir.'/'.$this->t_tape['ID_TAPE'].".xml";
		if(file_exists($infoFile)){
			$_xml = file_get_contents($infoFile);
			$_tab=xml2tab($_xml);
			$c=trim(str_replace(array("KB","MB","GB","Ko","Mo","Go"),array("*1024","*1048576","*1073741824","*1024","*1048576","*1073741824"),$_tab['DATA'][0]['INFOS'][0]['CAPACITY'])); 
			eval("\$x= $c ;");
			$this->t_tape['TAPE_CAPACITE']=$x;
			return true;
		} else {
			$this->dropError(kErrorTapeInfoFile." : ".$infoFile);
			return false;
		}
		
	}
	
	/**
		* Vérification existence tape 
	 * IN : objet tape
	 * OUT
	 */
	function checkExist() {
		global $db;
		$idTape=$db->GetOne("SELECT ID_TAPE from t_tape WHERE ID_TAPE=".$db->Quote($this->t_tape['ID_TAPE']));
		return $idTape; //false si n'existe pas
	}
	

	/**
		* Sauvegarde objet tape 
	 * IN : objet tape
	 * OUT : base mise à jour
	 */
	function save($failIfExists=false) {
		global $db;
		
		if (empty($this->t_tape['ID_TAPE'])) {$this->dropError(kErrorTapeNoId);return false;}
		if (empty($this->id_tape_ref)) {return $this->create($failIfExists);} //redirection vers la creation, retour = create au lieu de true
		if ($this->id_tape_ref!=$this->t_tape['ID_TAPE'] &&  $this->checkExist()) {$this->dropError(kErrorTapeExisteDeja);return false;}
		
		$this->t_tape['TAPE_DATE_MOD']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_tape['TAPE_ID_USAGER_MOD']=$_SESSION['USER']['ID_USAGER'];
		unset($this->t_tape['TAPE_DATE_CREA']);
		unset($this->t_tape['TAPE_ID_USAGER_CREA']);
		
		$rs = $db->Execute("SELECT * FROM t_tape WHERE ID_TAPE=".$db->Quote($this->id_tape_ref));
		$sql = $db->GetUpdateSQL($rs, $this->t_tape);
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok && !empty($sql)) {$this->dropError(kErrorTapeSauve);trace($sql);return false;}
		
		if ($this->t_tape['ID_TAPE']!=$this->id_tape_ref) {
			
			//Mise à jour t_tape_file
			$sql="UPDATE t_tape_file SET ID_TAPE=".$db->Quote($this->t_tape['ID_TAPE'])." WHERE ID_TAPE=".$db->Quote($this->id_tape_ref);
			$db->Execute($sql);
		}
		
		$this->dropError(kSuccesTapeSauve);
		return true;
	}
	
	/**
		* Creation tape 
	 * IN : objet tape
	 * OUT : base mise à jour
	 */
	function create ($failIfExists=false) {
		global $db;
		if (empty($this->t_tape['ID_TAPE'])) {$this->dropError(kErrorTapeNoId);return false;}
		$exist=$this->checkExist();
		if ($exist) { //tape existe, on redirige donc vers une sauvegarde std (sans renommage)
			if ($failIfExists) { // cas de failifexists
				$this->dropError(kErrorTapeExisteDeja);return false;
			} else {
				$this->t_tape['ID_TAPE']=$exist;$this->id_tape_ref=$exist;return $this->save();}
		} 		
		
		$this->t_tape['TAPE_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		$this->t_tape['TAPE_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];

		$db->insertBase("t_tape","id_tape",$this->t_tape);
		
		$this->dropError(kSuccesTapeCreation);
		
		return true;
	}
	
	/**
	 * Suppression tape 
	 * IN : objet tape
	 * OUT : base mise à jour
	 */
	function delete() {
		global $db;
    	if (empty($this->t_tape['ID_TAPE'])) {$this->dropError(kErrorTapeNoId);return false;}
        $ok=deleteSQLFromArray(array("t_tape","t_tape_file"),"ID_TAPE",$this->t_tape['ID_TAPE']);
		if (!$ok) {$this->dropError(kErrorTapeSuppr);return false;}
		return true;
	}

	/**
		* Positionne le flag TAPE_FULL à 1
	 * IN : objet tape
	 * OUT : base mise à jour
	 */
	function setTapeFull() {
		global $db;
		$sql="UPDATE t_tape SET TAPE_FULL=1 WHERE ID_TAPE=".$db->Quote($this->t_tape['ID_TAPE']);
		$ok=$db->Execute($sql);
		return $ok;
	}
	
	/**
	 * Récupère les liens TAPE / FICHIER 
	 * IN 
	 * OUT : tableau de classe t_tape_file
	 */
	function getTapeFile() {
		global $db;
		$sql="SELECT * FROM t_tape_file WHERE ID_TAPE=".$db->Quote($this->t_tape['ID_TAPE']);
		if(defined('kMaxRows') && kMaxRows) {
			$sql.=" LIMIT ".kMaxRows;
		}

		$this->t_tape_file=$db->GetAll($sql);
	}

	/**
		* Récupère les liens TAPE / FICHIER 
	 * IN : ID Ligne
	 * OUT : tableau de classe t_tape_file
	 */
	function getOneTapeFile($id_tape_file) {
		global $db;
		$sql="SELECT * FROM t_tape_file WHERE ID_TAPE_FILE=".$db->Quote($id_tape_file);
		$tab=$db->GetAll($sql);
		return $tab[0];
	}
	/**
		* Récupère les liens TAPE / SET 
	 * IN 
	 * OUT : tableau de classe t_tape_file
	 */
	function getTapeSet($id_tape_set="") {
		global $db;
		if(!empty($id_tape_set)) $sql="SELECT * FROM t_tape_set WHERE ID_TAPE_SET=".intval($id_tape_set);
		// VP 5/07/11 : chanagement requête sql
		//else $sql="SELECT * FROM t_tape_set WHERE ID_TAPE=".$db->Quote($this->t_tape['ID_TAPE']);
		else $sql="SELECT * FROM t_tape_set,t_tape WHERE t_tape.ID_TAPE_SET=t_tape_set.ID_TAPE_SET and t_tape.ID_TAPE=".$db->Quote($this->t_tape['ID_TAPE']);
		$this->t_tape_set=$db->GetAll($sql);
	}
	
	
	/**
	 * Sauve les liens TAPE / FICHIER 
	 * IN : tableau de classe t_tape_file
	 * OUT : base mise à jour
	 */
	function saveTapeFile($purge=true) {
		global $db;
		if (empty($this->t_tape_file)) return;
		$db->StartTrans();
		if ($purge) $ok=$db->Execute("DELETE FROM t_tape_file WHERE ID_TAPE =".$db->Quote($this->t_tape['ID_TAPE']));;
		foreach ($this->t_tape_file as $idx=>&$tapeFile) {
			$tapeFile['ID_TAPE']=$this->t_tape['ID_TAPE'];
			if (!empty($tapeFile['TF_FICHIER'])) {
				if ($purge || empty($tapeFile['ID_TAPE_FILE'])) { // INSERTION				
					$id_tape_file = $db->insertBase("t_tape_file","id_tape_file",$tapeFile);
					$tapeFile['ID_TAPE_FILE']=$id_tape_file; //maj objet
					//var_dump($tapeFile['ID_TAPE_FILE']);
					
				} else { // UPDATE
					$rs = $db->Execute("SELECT * FROM t_tape_file WHERE ID_TAPE_FILE=".intval($tapeFile['ID_TAPE_FILE']));
					$sql = $db->GetUpdateSQL($rs, $tapeFile);
					if (!empty($sql)) $ok = $db->Execute($sql);
				}
			}
		}
		if (!$db->CompleteTrans()) {$this->dropError(kErrorSauveTapeFile); return false;} else return true;
	}

	/**
		* Supression des liens TAPE / FICHIER 
	 * IN : tab de classe t_tape_file
	 * OUT : bdd maj et tableau t_tape_file vidé
	 */
	function deleteTapeFile() {
		global $db;
		foreach ($this->t_tape_file as $tm) {
	        deleteSQLFromArray(array("t_tape_file"),"ID_TAPE_FILE",$tm['ID_TAPE_FILE']);
		}
		unset ($this->t_tape_file);		
	}
	
	/**
		* Mise à jour du champs id_tape_set
	 * IN : tab de classe t_tape_file
	 * OUT : bdd maj
	 */
	function updateSet($id_tape_set) {
		global $db;
		$ok=$db->Execute("Update t_tape set ID_TAPE_SET = ".intval($id_tape_set)." WHERE ID_TAPE =".$db->Quote($this->t_tape['ID_TAPE']));
		if ($ok)
			$db->Execute("Update t_tape_file set ID_TAPE_SET = ".intval($id_tape_set)." WHERE ID_TAPE =".$db->Quote($this->t_tape['ID_TAPE']));
	}
	
 	/** Export XML de l'objet 
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {


        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_tape>\n";
		foreach ($this->t_tape as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}


       if (!empty($this->t_tape_file)) {
        // I.4. Propri�t�s relatives
        $content.="\t<t_tape_file nb=\"".count($this->t_tape_file)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_tape_file as $name => $value) {
                $content .= "\t\t<TAPE_FILE ID_TAPE_FILE=\"".$this->t_tape_file[$name]["ID_TAPE_FILE"]."\">\n";
                // Pour chaque valeur
                foreach ($this->t_tape_file[$name] as $val_name => $val_value) {
                    if (is_object($val_value)) {$content.=$val_value->xml_export();}
                    else {
                    	$content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                    }
                }
                $content .= "\t\t</TAPE_FILE>\n";
            }
        $content.="\t</t_tape_file>\n";
       }

		$content.=$indent."</t_tape>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
	
	
	function getXMLList($sql,$nb_offset=0,$nb="all",$mode=null,$search_sup=array(),$replace_sup=array()){	
		global $db;
		
		if($mode == null){
			$mode=array('tape_file'=>1);
		}
		
		$critere = "" ; 
		if(isset($sql) && !empty($sql)){
			$parsed = parseSimpleSQL($sql);
			foreach($parsed as $part){
				if($part['segment']!==false && ($part['keyword'] == 'where' || $part['keyword'] == 'order by')){
					$critere .=$part['segment'] ;
				}
			}
		}
		
		if(strpos(strtolower($critere),'order by') === false){
			$critere.=" order by id_tape ";
		}
		
		$limit="";
		if ($nb!="all"){
			$limit= $db->limit($nb_offset,$nb);
		}
		
		// définition des caractères à remplacer, 
		$search_chars = array(" ' ","’","œ","\n");
		$replace_chars = array("'","'","oe","\r\n");
		
		if(!empty($search_sup) && is_array($search_sup) && is_array($replace_sup) && !empty($replace_sup)){
			$search_chars = array_merge($search_chars,$search_sup);
			$replace_chars = array_merge($replace_chars,$replace_sup);
		}
		
		// definition du fichier de sortie 
		$xml_file_path = kServerTempDir."/export_opsis_tape_".microtime(true).".xml";
		$xml_file=fopen($xml_file_path,"w");
		flock($xml_file, LOCK_EX);
		fwrite($xml_file,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		fwrite($xml_file,"<EXPORT_OPSIS>\n");
		$xml="";

		$sql_tape = "SELECT t1.*
			FROM t_tape t1
			".$critere.$limit;
		$results_tape = $db->Execute($sql_tape);

		$arr_ids =  array();
		while($row = $results_tape->FetchRow()){
			$arr_ids[] = $row['ID_TAPE'];
		}
		$ids = implode(',',$arr_ids);
		if($ids == ''){
			flock($xml_file, LOCK_UN);
			fclose($xml_file);
			unlink($xml_file_path);
			return false;
		}
		
		if($mode['tape_file'] ){	
			$ids_tapes = "'".implode("','",$arr_ids)."'";
		
			$sql_tapefile="SELECT t1.* 
				FROM t_tape_file t1
				WHERE ID_TAPE in ($ids_tapes) ORDER BY ID_TAPE,ID_TAPE_FILE ASC";
				trace("sql_tapefile : ".$sql_tapefile);
				
			$results_tapefile = $db->Execute($sql_tapefile);
			trace("sql_tapefile:".$sql_tapefile);
			unset($sql_tapefile);
		}
		
		
		while($mode['tape_file'] && $tape_file = $results_tapefile->FetchRow()){
			$results_tapefile_ref[$tape_file['ID_TAPE']][]=$results_tapefile->CurrentRow()-1;
		}
		
		foreach($results_tape as $tape){
			$xml .= "\t<t_tape ID_TAPE=\"".$tape['ID_TAPE']."\">\n";
			foreach($tape as $fld=>$val){
				$xml .= "\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
			}
			if(isset($results_tapefile_ref[$tape['ID_TAPE']][0])){
				$results_tapefile->Move($results_tapefile_ref[$tape['ID_TAPE']][0]);
				$array_tapefile = $results_tapefile->GetRows(count((array)$results_tapefile_ref[$tape['ID_TAPE']]));
			}else{
				$array_tapefile=array();
			}
			if(count($array_tapefile)>0){
				$xml.="\t\t<t_tape_file nb=\"".count($array_tapefile)."\" ID_TAPE=\"".$tape['ID_TAPE']."\">\n";
				foreach($array_tapefile as $tapefile){
					$xml.="\t\t\t<TAPE_FILE ID_TAPE_FILE=\"".$tapefile['ID_TAPE_FILE']."\">\n";
					foreach($tapefile as $fld=>$val){
						$xml .= "\t\t\t\t<".strtoupper($fld)."><![CDATA[".str_replace($search_chars,$replace_chars,$val)."]]></".strtoupper($fld).">\n";
					}
					 $xml .= "\t\t\t</TAPE_FILE>\n";
				}
				$xml.="\t\t</t_tape_file>\n";
			}
			
			$xml .= "\t</t_tape>\n";
            fwrite($xml_file,$xml);
            $xml = "";
		}
		fwrite($xml_file,"</EXPORT_OPSIS>\n");
        flock($xml_file, LOCK_UN);
        fclose($xml_file);
        
        return $xml_file_path;
	}
}

?>