<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- D�veloppeurs : Vincent Prost, Fran�ois Duran, J�rome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

// XS : 26/10/2005 : Cr�ation du fichier, d�finition de la classe REQUETTE

//************************************************** REQUETTE ***************************************************************
require_once(libDir."class_user.php");
require_once(modelDir.'model.php');
class Requete extends Model
{

    //V2
    var $t_requete;
    var $error_msg;

	function __construct($id=null,$version='') {
		parent::__construct('t_requete',$id,$version);
    	$this->t_requete['ID_USAGER']=User::getInstance()->UserID; // par déft, user en cours
    }

	/**
	 * Initialise l'objet en récupérant les champs depuis la base
	 */
	function init() {
		parent::init();
		$this->t_requete['ID_USAGER']=User::getInstance()->UserID; // par déft, user en cours
	}

	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 *
	 */
 	function updateFromArray ($tab_valeurs) {
	 	global $db;
	 	$cols=array_keys($db->MetaColumns('t_requete'));

	 	foreach ($tab_valeurs as $fld=>$val) {
			if (in_array(strtoupper($fld),$cols)) $this->t_requete[strtoupper($fld)]=$val;
		}
	}


	/**
	 * Récupère les infos sur une ou plusieurs requetes.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets requetes, sinon màj de l'objet en cours
	 */
	function getRequete($arrVal=array(),$id_usager="") {
		return $this->getData($arrVal,'',$id_usager);
	}
	
	// MS - pour l'instant pas de refonte getData car je ne sais pas trop comment gérer la notion du critère id_usager.(devrait probablement être une fonction à part.);
	function getData($arrVal=array(),$version="",$id_usager="") {
		if (empty($arrVal)) $arrVals=array($this->t_requete['ID_REQ']); else $arrVals=$arrVal;
		$arrCrit['ID_REQ']=$arrVals;
		if (!empty($id_usager) && $id_usager!=-1) $arrCrit['ID_USAGER'] = $id_usager;
		//by ld 141108 désactivation du filtrage usager pr gaumont
		//elseif ($id_usager!=-1) $arrCrit['ID_USAGER']=$this->t_requete['ID_USAGER'];

		$this->t_requete=getItem("t_requete",null,$arrCrit,null,0); //AJOUTER GESTION DES ORDRES


		if (count($this->t_requete)==1 && empty($arrVal)) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->t_requete=$this->t_requete[0];
				$this->t_requete['REQ_PARAMS']=unserialize($this->t_requete['REQ_PARAMS']);
				}
		else {
            $arrVals=array();
			foreach ($this->t_requete as $idx=>$Req) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx]= new Requete();
				$arrVals[$idx]->t_requete=$Req;
				
				$arrVals[$idx]->t_requete['REQ_PARAMS']=unserialize($arrVals[$idx]->t_requete['REQ_PARAMS']);

			}
			unset($this); // plus besoin
			return $arrVals;
		}
	}
	
	static function croiserRequetes($requetes,$operateur)
	{
		if 
		(
			isset($requetes) && !empty($requetes) &&
			isset($operateur) && !empty($operateur) &&
			is_array($requetes) && is_string($operateur)
		)
		{
			$list_where=array();
			$req_lib=array();
			foreach ($requetes as $req)
			{
				$query=new Requete();
				$query->t_requete['ID_REQ']=intval($req);
				$query->getRequete();
				
				$str_req=$query->t_requete['REQ_SQL'];		

				//si on a une clause where on continue le traitement de la requete
				if ($where=stristr($str_req,'where'))
				{
					$where=trim(substr($where,5));
					
					if (stristr($where,'group by')!=false)
						$where=trim(stristr($where,'group by',true));
					
					if (stristr($where,'order by')!=false)
						$where=trim(stristr($where,'order by',true));
					
					if (stristr($where,'limit')!=false)
						$where=trim(stristr($where,'limit',true));
					
					if (!empty($where))
					{
						$list_where[]='('.$where.')';
						$req_lib[]=$query->t_requete['REQ_LIBRE'];
					}
				}		
				
				if($query->t_requete['REQ_ENGINE'] == 'Solr'){
					$list_where[]=$query->t_requete['REQ_SQL'];
					$req_lib[]=$query->t_requete['REQ_LIBRE'];
				}
			}
						
			if (!empty($list_where))
			{
				$query=new Requete();
				$query->t_requete['ID_REQ']=intval($requetes[0]);
				$query->getRequete();
				
				$req_intersection=new Requete();
				$req_intersection->t_requete['ID_USAGER']=$query->t_requete['ID_USAGER'];
				$req_intersection->t_requete['REQ_DATE_EXEC']=date('Y-m-d');
				$req_intersection->t_requete['SID']=$query->t_requete['SID'];
				$req_intersection->t_requete['REQ_DB']=$query->t_requete['REQ_DB'];
				$req_intersection->t_requete['REQ_ENGINE']=$query->t_requete['REQ_ENGINE'];
			
				
				
				if ($operateur=='AND')
					$req_intersection->t_requete['REQ_LIBRE']=implode(" ".kEt." ",$req_lib);
				else if ($operateur == 'AND NOT')
					$req_intersection->t_requete['REQ_LIBRE']=implode(" ".kSauf." ",$req_lib);
				else if ($operateur == 'OR')
					$req_intersection->t_requete['REQ_LIBRE']=implode(" ".kOu." ",$req_lib);
				
				
				$str_req=$query->t_requete['REQ_SQL'];
				
				// MS - 15.04.15 - On récupère maitenant les fields lists de la première requete.
				// on récuperait la partie "from" depuis la première requete, mais pas les fields lists. 
				// sur pgsql, on a besoin de recopier la field list à l'identique car sinon on se retrouvait avec des champs vides car on join 2 t_doc, 
				// et le resultset contenait alors des rows contenant 2x les mêmes fields, avec NULL dans la deuxième occurence de la colonne. 
				// => affichait le resultset avec des doc_titre vide etc ...
				$fld_list = substr($str_req,strlen('select'),(stripos($str_req,'from')-strlen('select')));
								
				if ($from=stristr($str_req,'from'))
				{
					$from=trim(stristr(substr($from,4),'where',true));
				}
				
				if($query->t_requete['REQ_ENGINE'] == 'Solr'){
					$new_req=implode(' '.$operateur.' ',$list_where);
					$req_intersection->t_requete['REQ_NB_DOC']=$query->rows;
					$req_intersection->t_requete['REQ_SQL']=$new_req;
					$req_intersection->save();
									
				
				}
				else {
					if(!isset($fld_list) || empty($fld_list)){
						$fld_list = "*";
					}
					
					$new_req='SELECT '.$fld_list.' FROM '.$from.' WHERE '.implode(' '.$operateur.' ',$list_where);
					$req_intersection->t_requete['REQ_SQL']=$new_req;
					$req_intersection->save();
				}
				
				return $req_intersection;
			}
			
			return NULL;
		}
		
		return NULL;
	}


	/** Vérifie si une requete existe déjà en base
	 *  IN : objet lieu, $id_usager (opt) Si spécifié cherche pour cet usager
	 *  OUT : TRUE si existe, FALSE si libre
	 */
	function checkExist() {
		global $db;


		// $sql="SELECT ID_REQ FROM t_requete WHERE REQ_SQL=".$db->Quote(trim(strtolower($this->t_requete['REQ_SQL'])));
		//$sql="SELECT ID_REQ FROM t_requete WHERE REQ_LIBRE=".$db->Quote(trim(strtolower($this->t_requete['REQ_LIBRE'])));
		$sql="SELECT ID_REQ FROM t_requete WHERE REQ_LIBRE=".$db->Quote(trim($this->t_requete['REQ_LIBRE']));
		
		
		if (!empty($this->t_requete['REQ_ENGINE']))
			$sql.=" AND REQ_ENGINE=".$db->Quote($this->t_requete['REQ_ENGINE']);
		
		if (!empty($this->t_requete['REQ_DB']))
			$sql.=" AND REQ_DB=".$db->Quote($this->t_requete['REQ_DB']);
		else
			$sql.=" AND SID=".$db->Quote(session_id());
			//si la requete existe déjà en base, on l'exclue de la recherche de doublon
		if (!empty($this->t_requete['ID_REQ'])) $sql.=" AND ID_REQ<>".$this->t_requete['ID_REQ'];
		
		//debug($sql,'orange');
		//by Ld 21/11/08, suppr du cache de 120sec car pose problème qd on exécute des requetes ds cet intervalle : créa n doublons
		$rs=$db->GetOne($sql); 
		//debug('>>>'.$rs,'green');
		if ($rs===false) return false; else return $rs;
	}

	/**
	 * Sauve en base une requete (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj
	 */
	function save ($chkRequired=true) {
		global $db;

		if (empty($this->t_requete['ID_REQ']))
		{
			if ($chkRequired) $existingReq=$this->checkExist(); else $existingReq=0;
			if ($existingReq) {$this->t_requete['ID_REQ']=$existingReq;} else {return $this->create();}
		}

		//by ld 24/09/07 j'ai viré le test d'existence pour serializer systématiquement meme si vide (causait un warning)
		$this->t_requete['REQ_PARAMS']=serialize($this->t_requete['REQ_PARAMS']);
		$this->t_requete['SID']=session_id();
		if(!empty(User::getInstance()->UserID)) $this->t_requete['ID_USAGER']=User::getInstance()->UserID;
		//NOTE : On resauve l'ID_USAGER CAR une requete peut être créé avec un userid à 0 (user non loggé)
		//PUIS après login, être sauvée par l'user : il faut donc la réattribuer à ce user

		//debug($this,'gold');

		$rs = $db->Execute("SELECT * FROM t_requete WHERE ID_REQ=".intval($this->t_requete['ID_REQ']));
		$sql = $db->GetUpdateSQL($rs, $this->t_requete);
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok && !empty($sql)) {$this->dropError(kErrorRequeteSauve);return false;}

	}


	/**
	 * Crée une requete en base (avec vérif de doublons)
	 * IN : objet
	 * OUT : base màj et objet màj avec l'ID REQ créé
	 */
	function create ($idReq=0) {
		global $db;
		if (empty($this->t_requete['REQ_SQL'])) return;
		
		//by ld 21/11/08, désactivation check car on vient toujours ici par un save (voire un saveSearch, donc le check est déjà fait avt
		//if (!empty($this->t_requete['ID_REQ']) && $idReq==0 ) {$this->save();return 'save';} //Protection : redirection de sauvegardes
		//if ($this->checkExist()) {$this->dropError(kErrorRequeteExisteDeja);return false;}

		if (empty($this->t_requete['ID_USAGER'])) $this->t_requete['ID_USAGER']=User::getInstance()->UserID;
		if ($idReq) $this->t_requete['ID_REQ']=$idReq;
		
		//HYPER SPECIAL : simpleQuote est perdu à partir d'un certain nombre de \ ce qui est le cas
		// lorsque l'on a des ' dans les valeurs de champ FULLTEXT. On lui force donc la main.
		$this->t_requete['REQ_SQL']=str_replace("\\\\\\","\\\\\\\\\\",$this->t_requete['REQ_SQL']);
		$this->t_requete['REQ_PARAMS']=serialize($this->t_requete['REQ_PARAMS']);
		$this->t_requete['SID']=session_id();

		//debug($this,'silver');

		$ok = $db->insertBase("t_requete","id_req",$this->t_requete);
		if (!$ok) {$this->dropError(kErrorRequeteCreation);trace(kErrorRequeteCreation.$sql);return false;}
		$this->t_requete['ID_REQ']=$ok;
	}



	/** Export XML de l'objet requete
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_requete>\n";
		// VALEUR
		foreach ($this->t_requete as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}
		$content.=$indent."</t_requete>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}



	static function recupere_histo($type, $db_cible='',$tri='REQ_DATE_EXEC DESC'){
		//Récupère l'historique pour la session en cours, sauf requetes sauvées.
		global $db;
		require_once(libDir."class_user.php");

		// MS 08/06/2012 - ajout critère ID_USAGER= usager en cours sur la récupération de l'historique

		$sql="SELECT * FROM t_requete where SID='".session_id()."' and ID_USAGER=".intval(User::getInstance()->UserID)." and REQ_SQL<>'' ";
		
		if($db_cible !=''){
			$sql.=" and REQ_DB='".$db_cible."' ";
		}
		
		$sql.=" ORDER BY ".$tri;

		

		if ($type=='full' && !empty(User::getInstance()->UserID)) { // Historique complet de l'utilisateur
			$sql="SELECT * FROM t_requete where ID_USAGER=".intval(User::getInstance()->UserID)." and REQ_SQL<>''  ORDER BY ".$tri;
			}
		$result=$db->Execute($sql);
		
		$tab_histo=array();
		
		while($row=$result->FetchRow())
			{
				$row["REQ_PARAMS"]=unserialize($row["REQ_PARAMS"]);
				$tab_histo[]=$row;
			}
			return $tab_histo;
	}


	function insert_histo($tab_params) {
		//Insère dans l'historique la recherche qui vient d'être faite. Renvoie l'ID nouvellement créé.
		global $db;


		if ($this->checkExist()) return false;


		$sql="INSERT INTO t_requete (ID_USAGER,REQUETE,REQ_LIBRE,REQ_SQL,
									REQ_NB_DOC,REQ_ALERTE,REQ_DATE_EXEC,SID,REQ_PARAMS,REQ_SAVED)
				Values (".intval($_SESSION["USER"]["ID_USAGER"]).",'',
						".$db->Quote($tab_params["REQ_LIBRE"]).",".$db->Quote($tab_params["REQ_SQL"]).",".intval($tab_params["REQ_NB_DOC"]).",0,NOW(),'".session_id()."',
						".$db->Quote(serialize($tab_params["REQ_PARAMS"])).",0)";

		$db->Execute($sql);
		return $db->getOne("SELECT max(id_req) FROM t_requete");
	}
	
	// Supprime toutes les requetes associées à un SessID et un usager
	static function delete_all_histo(){
		global $db;
		deleteSQLFromArray(array("t_requete"),array("SID"), array(session_id()));
	}


    //*** Suppression
    function deleteRequete(){
        deleteSQLFromArray(array("t_requete"),"ID_REQ",$this->t_requete['ID_REQ']);
    }


}

?>