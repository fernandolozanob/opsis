<?
//************************************************** MODEL ***************************************************************

// VP : 15/03/2016 : Création de la classe Model

//************************************************** MODEL ***************************************************************
class Model
{

	var $error_msg;
    var $key;
    var $table;
	var $arrPrimaryKeys;
    var $hasLang;
	var $entity;
	var $entity_code;
	var $loaded_from_db;
    var $arrVersions; //tableau des ID/VALEURS pour les autres langues


	function __construct($table="", $id=null, $version="") {
        global $db;
		$this->error_msg = '';
		$this->loaded_from_db = false;
        $this->hasLang = false;
        if(!empty($table)){
            $this->table = $table;
            $keys = $db->MetaPrimaryKeys($this->table);
            if($keys !== false && count($keys)>0){
                foreach($keys as $i=>$k){
                    if($i == 0)
                        $this->key = strtoupper($k);
                    if(strtoupper($k)=='ID_LANG') // à rendre paramétrable par constante éventuellement
                        $this->hasLang = true;
                }
            }
			if($this->hasLang && !empty($version)){
				$this->{$this->table}['ID_LANG'] = $version ;
			}
            if (isset($id) && !empty($id) && $id!=null){
				$this->{$this->table}[$this->key] = $id;
                $this->getData();
			}
			if(empty($this->entity)){
				$this->entity = (strpos($table,'t_')===0?substr($table,2):$table);
			}
			if(empty($this->entity_code)){
				$this->entity_code = (strpos($this->key,'ID_')===0?substr($this->key,3):$this->key);
			}
		}
	}
	
	function getEntityFilenamePart(){
		if(!isset($this->entity) || empty($this->entity) ){
			return false;
		}
		
		$entity_tmp = explode('_',$this->entity);
		$entity_filename_format = "";
		foreach($entity_tmp as $idx=>$part){
			$part = strtolower($part); 
			if($idx != 0){
				$part = ucfirst($part);
			}
			$entity_filename_format.=$part;
		}
		unset($entity_tmp);
		return $entity_filename_format;
	}

    
    /**
     * Vérification existence (identique à checkExistId dans cette classe générique)
     * IN : objet
     * OUT
     */
    function checkExist() {
        
        return $this->checkExistId();
    }
    
    
    /**
     * Vérification existence par ID
     * IN : objet
     * OUT
     */
    function checkExistId() {
        global $db;
        
        if(!$this->table || !$this->key)
            return false;
        
        $sql="SELECT $this->key FROM $this->table WHERE $this->key=".intval($this->{$this->table}[$this->key]);
        if($this->haslang)
            $sql.=" AND ID_LANG=".$db->Quote($this->{$this->table}['ID_LANG']);
        
        $id=$db->GetOne($sql);
        return $id;
    }

    
    /**
     * Creation
     * IN : objet
     * OUT : base mise à jour
     */
    function create () {
        global $db;
        if(!$this->table || !$this->key)
            return false;
        
        if (!empty($this->{$this->table}[$this->key]) && $this->checkExist()) {return $this->save();}
        
        
        unset($this->{$this->table}[$this->key]); //Pour ne pas faire apparaitre la colonne dans le SQL ET générer un ID
        $ok = $db->insertBase($this->table, $this->key, $this->{$this->table});
        if (!$ok) {
            $this->dropError(kErrorCreation);
            trace($sql);
            return false;
        }
        $this->{$this->table}[$this->key]=$ok;
        
        $this->dropError(kSuccesCreation);
        return true;
    }

    
    /**
     * Suppression
     * IN : objet
     * OUT : base mise à jour
     */
    function delete() {
        global $db;
        if(!$this->table || !$this->key)
            return false;
        
        if (empty($this->{$this->table}[$this->key])) {
            $this->dropError(kErrorNoId);
            return false;
        }
        $ok=deleteSQLFromArray(array($this->table),$this->key,simpleQuote($this->{$this->table}[$this->key]));
        if (!$ok) {
            $this->dropError(kErrorSuppr);
            return false;
        }
        return true;
    }

    
	/**
		* Ajoute un message d'erreur pour la sortie
	 * IN : message (string)
	 * OUT : var objet mise à jour
	 */
	function dropError($errLib) {
		$this->error_msg.=$errLib."<br/>";
	}

    
    /** Récupère tous les objets
     * 	IN : version
     * 	OUT : objet(s)
     */
    public function getAll($version="")
    {
        global $db;
        if(!$this->table || !$this->key)
            return false;
        
        $result = array();
        $id_lang = $version;
        if($this->hasLang){
            if (empty($version) && empty($this->{$this->table}['ID_LANG'])) $id_lang = $_SESSION['langue'];
            elseif (!empty($version)) $id_lang = $version;
            else $id_lang = $this->{$this->table}['ID_LANG'];
            $rows = $db->GetAll("SELECT $this->key FROM $this->table where ID_LANG=".$db->Quote($id_lang)." order by 1");
        }else{
            $rows = $db->GetAll("SELECT $this->key FROM $this->table order by 1");
        }
        if (isset($rows) && !empty($rows)) {
            foreach($rows as $row) {
				$current_class = get_class($this);
                $result[]=new $current_class($this->table, $row[$this->key], $id_lang);
            }
        }
        return $result;
    }
    
    
	/** Récupère un objet ou un tableau d'objet
		* 	IN : tableau
		* 	OUT : objet(s)
			 Ex d'appels : 
		getData() récupère l'objet et alimente la table principale de l'objet courant
			=> il faut que l'objet contienne les informations (id_doc / id_lang) necessaires
		getData(<array ids docs>) // MSVERIF - A terme je pense pas que cette partie de la fonctionnalité soit conservée, devrait être une fonction statique à part
			=> renvoi un array d'objets 
		*/
	function getData($arrData=array(), $version="") {
		global $db;
        if(!$this->table || !$this->key)
            return false;

        if (!$arrData){
            $arrVals=array($this->{$this->table}[$this->key]);
        }else{
            $arrVals=(array)$arrData;
        }
		$arrCrit[$this->key]=$arrVals;
		
        if($this->hasLang){
            if (empty($version) && empty($this->{$this->table}['ID_LANG'])) $arrCrit['ID_LANG']=$_SESSION['langue'];
            elseif (!empty($version)) $arrCrit['ID_LANG']=$version;
            else $arrCrit['ID_LANG']=$this->{$this->table}['ID_LANG'];
        }
       
		$items=getItem($this->table,null,$arrCrit,null,0);
		if (count($items)==1 && !$arrData) { // Un seul résultat ET pas d'arrData en input : on affecte le résultat à la valeur en cours
			$this->{$this->table}=$items[0];
			$this->loaded_from_db = true ; 
		}else {
            $arrVals=array();
			foreach ($items as $idx=>$Data){ // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$current_class = get_class($this);
				if($current_class=='Model'){	
					$arrVals[$idx]= new $current_class($this->table);
				}else{
					$arrVals[$idx]= new $current_class();
				}
				$arrVals[$idx]->{$arrVals[$idx]->table}=$Data;
				$arrVals[$idx]->loaded_from_db = true; // devrait peut-être être indiqué légerement différement que le cas d'au dessus, puisqu'on a pas l'objet complet ... 
			}
			unset($this);
			return $arrVals;
		}
	}

    
    /** Récupère les libellés pour toutes les langues de l'appli.
     *  IN : rien (objet)
     * 	OUT : tableau de classe arrVersions
     */
    function getVersions(){
        global $db;
        if(!$this->table || !$this->key || !$this->hasLang)
            return false;
       
        if (!empty($this->{$this->table}[$this->key])) {
            $sql="SELECT t_lang.*, $this->table.* FROM $this->table join t_lang on (t_lang.ID_LANG=$this->table.ID_LANG) 
                    WHERE $this->table.ID_LANG<>".$db->Quote($this->{$this->table}['ID_LANG'])." AND $this->key=".intval($this->{$this->table}[$this->key]);
            
            $this->arrVersions=$db->GetAll($sql);
        }
    }

    
    /** Initialisation objet
     * 	IN
     * 	OUT : objet màj
     */
     function init() {
        global $db;
        $cols=$db->MetaColumns($this->table);
        foreach ($cols as $i=>$col){
			$this->{$this->table}[$i]='';
			if ($col->primary_key==1) $this->arrPrimaryKeys[]=$col;
		}
    }

    
	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {
        if(!$this->table || !$this->key)
            return false;
        
	 	if (empty($this->{$this->table})) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->{$this->table}[strtoupper($fld)]))
                $this->{$this->table}[strtoupper($fld)]=$val;
		}
	}


    /**
     *
     * Crée un nouvel usager s'il n'existe pas, et retourne l'id
     */
    function createFromArray($tab_valeurs, $create = true) {
        $this->updateFromArray($tab_valeurs);
        //Si la valeur existe déjà , on retourne juste son id
        $exist = $this->checkExist();
        if ($exist) {
            $this->$this->table[$this->key] = $exist;
            return $this->$this->table[$this->key];
        }
        
        if(!$create) {
            return false;
        }
        $ok = $this->save();
        return $this->$this->table[$this->key];
    }
    
    /**
		* Sauvegarde objet
	 * IN : objet
	 * OUT : base mise à jour
	 */
	function save() {
		global $db;
		
        if(!$this->table || !$this->key)
            return false;

        if (empty($this->{$this->table}[$this->key]) || !$this->checkExist()) {return $this->create();}
		
		$rs = $db->Execute("SELECT * FROM $this->table WHERE $this->key=".intval($this->{$this->table}[$this->key]));
		$sql = $db->GetUpdateSQL($rs, $this->{$this->table});
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok && !empty($sql)) {$this->dropError(kErrorSauve);trace($sql);return false;}
		
		$this->dropError(kSuccesSauve);
		return true;
	}

    
    /** Sauve les différentes versions
     *  IN : tableau des versions, MODE = retour de la fonction save appelée avant. Si retour=1=>création
     * 	OUT : màj base
     */
    function saveVersions($tabVersions='') {
        global $db;
        
        if(!$this->table || !$this->key || !$this->haslang)
            return false;
        
        if ($tabVersions=='')
			$tabVersions=$_SESSION['arrLangues'];
        
        if (is_object($tabVersions) || is_array($tabVersions)){
            foreach ($tabVersions as $lang=>$tabVersion) {
                $current_class = get_class($this);
				$version= new $current_class($this->table);
                $version->{$this->table}= $this->{$this->table}; //recopie de ts les champs par déft
                $version->updateFromArray($tabVersion); //mise à jour champs
                $version->{$this->table}['ID_LANG']=$lang; //mise à jour langue
                if ($version->checkExistId()) $version->init($version->{$this->table}[$this->key]);
                $ok=$version->save();
                if ($ok==false && $version->error_msg) {
                    $this->dropError(kErrorSauveVersion.' : '.$version->error_msg);
                    return false;
                }
                unset($version);
            }
        }
        
        return true;
    }

	/**
	* Ajoute ou supprime des espaces autour des quotes pour les champs texte
	 * IN : FROM (chaine source), TO (chaine dest) et tableau class arrTextFields (liste des champs texte)
	 * OUT : var de classes processée pour ces champs texte.
	 *
	 */
	function spaceQuotes($from,$to) {
		// MS 24-04-14 - kDbType postgres & kDbType postgres7 doivent se comporter de la même manière.  
		if(kDbType != 'postgres' && kDbType != 'postgres7'){
			if(!empty($this->arrTextFields)){
				foreach ($this->arrTextFields as $fld) {
					if (isset($this->{$this->table}[strtoupper($fld)]))
						$this->{$this->table}[strtoupper($fld)]=str_replace($from, $to, $this->{$this->table}[strtoupper($fld)]);
				}
			}
		}
	}
	
    
 	/** Export XML de l'objet
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {

        if(!$this->table || !$this->key)
            return false;

        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<".$this->table.">\n";
		foreach ($this->{$this->table} as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}

		$content.=$indent."</".$this->table.">\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
}

?>
