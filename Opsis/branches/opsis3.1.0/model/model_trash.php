<?php
//************************************************** TRASH ***************************************************************
require_once(libDir."class_user.php");
require_once(modelDir.'model.php');
class Trash extends Model
{

    var $t_trash;
    var $error_msg;
    var $_Object;

   function __construct($id=null,$version='') {
		parent::__construct('t_trash',$id,$version);
    	$this->t_trash['TRA_ID_USAGER']=User::getInstance()->UserID; // par déft, user en cours
    }

	/**
	 * Initialise l'objet en récupérant les champs depuis la base
	 */
	function init() {
		parent::init(); 
		$this->t_trash['TRA_ID_USAGER']=User::getInstance()->UserID; // par déft, user en cours
	}


	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 *
	 */
 	function updateFromArray ($tab_valeurs) {
	 	global $db;
	 	$cols=array_keys($db->MetaColumns('t_trash'));

	 	foreach ($tab_valeurs as $fld=>$val) {
			if (in_array(strtoupper($fld),$cols)) $this->t_trash[strtoupper($fld)]=stripControlCharacters($val);
		}
	}


	/**
	 * Récupère les infos sur une ou plusieurs trashs.
	 * IN : array de ID (opt).
	 * OUT : si array spécifié -> tableau d'objets trashs, sinon màj de l'objet en cours
	 */
	function getTrash($arrVal=array(),$id_usager="") {

		if (empty($arrVal)) $arrVals=array($this->t_trash['ID_TRASH']); else $arrVals=$arrVal;
		$arrCrit['ID_TRASH']=$arrVals;
		if (!empty($id_usager) && $id_usager!=-1) $arrCrit['TRA_ID_USAGER'] = $id_usager;
		$this->t_trash=getItem("t_trash",null,$arrCrit,null,0); //AJOUTER GESTION DES ORDRES


		if (count($this->t_trash)==1 && empty($arrVal)) { // Un seul résultat : on affecte le résultat à la valeur en cours
				$this->t_trash=$this->t_trash[0];
				}
		else {
            $arrVals=array();
			foreach ($this->t_trash as $idx=>$Trash) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx]= new Trash();
				$arrVals[$idx]->t_trash=$Trash;
			}
			unset($this); // plus besoin
			return $arrVals;
		}
	}

	/** Récupération corbeilles de l'usager
	 *  IN : entity
	 *  OUT : Tableau de corbeilles
	 */
	static function getTrashUser($entity="doc") {
		global $db;
		require_once(libDir."class_user.php");
		
		$sql="SELECT * FROM t_trash where TRA_ID_USAGER=".intval(User::getInstance()->UserID)."  ORDER BY TRA_DATE_CREA DESC";
		$tab=$db->GetAll($sql);
		return $tab;
	}

	/** Récupération nb corbeilles de l'usager
	 *  IN : entity
	 *  OUT : Tableau de corbeilles
	 */
	static function getNbTrash($entity="doc") {
		global $db;
		require_once(libDir."class_user.php");
		
		$sql="SELECT count(*) FROM t_trash where TRA_ID_USAGER=".intval(User::getInstance()->UserID);
		$nbElt=$db->GetOne($sql);
		return $nbElt;
	}
	
	/**
	 * Sauve en base une corbeille 
	 * IN : objet
	 * OUT : base màj
	 */
	function save () {
		global $db;
		if (empty($this->t_trash['TRA_XML'])) return;
		
		// Normalement on ne devrait pas avoir de ID_TRASH
		if(isset($this->t_trash['ID_TRASH'])) unset($this->t_trash['ID_TRASH']);
		if (empty($this->t_trash['TRA_ID_USAGER'])) $this->t_trash['TRA_ID_USAGER']=User::getInstance()->UserID;
		$this->t_trash['TRA_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		
		$ok = $db->insertBase("t_trash","id_trash",$this->t_trash);
		if (!$ok) {$this->dropError(kErrorTrashCreation);trace(kErrorTrashCreation.$sql);return false;}
		$this->t_trash['ID_TRASH']=$ok; 
		return $ok;
	}


	/** Suppression corbeille
	 *  IN : 
	 *  OUT : 
	 */
    function delete(){
		// Suppression en base
        deleteSQLFromArray(array("t_trash"),"ID_TRASH",$this->t_trash['ID_TRASH']);
		// Suppression répertoire(s)
		$trashDir=kTrashDir.$this->t_trash['ID_TRASH'];
		if(is_dir($trashDir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if(is_file($file)) unlink($trashDir."/".$file);
					elseif(is_dir($file)) rmdir($trashDir."/".$file);
				}
				closedir($dh);
			}
			rmdir($trashDir);
		}
		return true;
    }



	/** Déplacement fichiers
	 *  IN : fichiers à déplacer
	 *  OUT : T/F
	 */
	function putFiles($tab_fichier) {
		// Création réperoire
		$trashDir=kTrashDir.$this->t_trash['ID_TRASH'];
		$okdir=is_dir($trashDir);
		if(!$okdir) $okdir=mkdir($trashDir);
		if(!$okdir) {$this->dropError(kErreurCreerRepertoire);return false;}
		// Déplacement fichiers dans dossier doc_acc, mat et storyboard
		foreach($tab_fichier as $ligne_fichier){
			$type=$ligne_fichier['type'];
			$fichier=$ligne_fichier['file'];
			if(!empty($type) && !empty($fichier)){
				if(!is_dir($trashDir."/".$type))
					mkdir($trashDir."/".$type);
				if($type=="storyboard"){
					$destDir=$trashDir.substr($fichier,strpos($fichier, "/storyboard/"));
					if(!is_dir($destDir))
						mkdir($destDir, 0777, true);
					mv_rename($fichier, $destDir);
				}else{
					mv_rename($fichier, $trashDir."/".$type."/".basename($fichier));
				}
			}
		}
		return true;
		
	}
	

	/** Restoration corbeille
	 *  IN : 
	 *  OUT : T/F
	 */
	function restore() {
		require_once(modelDir.'model_doc.php');
		require_once(modelDir.'model_materiel.php');
		require_once(modelDir.'model_imageur.php');
		global $db;
		// Création document correspondant à partir des données XML de la colonne TRASH_XML et de la méthode mapFields
		$ok=false;
		switch($this->t_trash['TRA_ENTITE']){
			case "doc":
				$datas=xml2tab($this->t_trash['TRA_XML']);
				foreach ($datas['T_DOC'][0]['IMAGEUR'] as $key => $aImageur) {
					$oImageur = new Imageur();
					$oImageur->t_imageur['ID_IMAGEUR'] = $aImageur['ID_IMAGEUR'];
					$oImageur->t_imageur['IMAGEUR'] = $aImageur['IMAGEUR'];
					$oImageur->create();
				}
				
				$this->getObjectTrashed();
				$myDoc = $this->_Object;
				if(!empty($myDoc->t_doc['ID_DOC']) && !$myDoc->checkExistId()) {
					$ok=$myDoc->initDoc();
					if($ok)
						$ok=$myDoc->save(1);
				} else {
					unset($myDoc->t_doc['ID_DOC']);
					$ok=$myDoc->save();
				}
				
				if ($ok) {
					$myDoc->saveDocLex(true); 
					$myDoc->saveDocVal(true); 
					$myDoc->saveDocFest(true);
					$myDoc->saveDocMat(false);
					$myDoc->updateChampsXML('DOC_LEX');
					$myDoc->updateChampsXML('DOC_VAL');
					$myDoc->updateChampsXML('DOC_XML');
					$myDoc->updateChampsXML('DOC_CAT');
					$myDoc->saveSequences();
					$myDoc->saveDocAcc();
					if(!empty($myDoc->t_doc['DOC_ID_DOC_ACC']))
						$myDoc->saveVignette('doc_acc',$myDoc->t_doc['DOC_ID_DOC_ACC']);
					else
						$myDoc->saveVignette('',$myDoc->t_doc['DOC_ID_IMAGE'], $myDoc->t_doc['DOC_ID_IMAGEUR']);
						
					if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true))
						$myDoc->saveSolr();
						
				}
				break;
			case "mat" : 
				$this->getObjectTrashed();
				$myMat = $this->_Object;
				$myMat->id_mat_ref=$myMat->t_mat['MAT_NOM'];
				if(!empty($myMat->t_mat['ID_MAT']) ) {
					$ok=$myMat->create();
				}
				if($ok){
					$myMat->saveDocMat();
					$myMat->saveMatVal();
					$myMat->saveMatTrack();					
				}
				
				break ; 
		}
		

		if(!$ok) return false;
		// Déplacement des fichiers liés au bon endroit (doc_acc, mat et storyboard)
		$trashDir=kTrashDir.$this->t_trash['ID_TRASH'];
		// doc_acc
		$dir=$trashDir."/doc_acc";
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if($file[0]!='.') mv_rename($dir."/".$file,kDocumentDir.$file);
				}
				closedir($dh);
			}
		}
		// storyboard
		$dir=$trashDir."/storyboard";
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if($file[0]!='.') {
						if($dhStory = opendir($trashDir."/storyboard/".$file)) {
							while(($imageFile=readdir($dhStory)) !== false) {
								if($imageFile[0]=='.')
									continue;
								$oImage = new Image();
								$oImage->t_image['IM_CHEMIN'] = $file;
								$oImage->t_image['IM_FICHIER'] = $imageFile;
								$oImage->t_image['ID_IMAGEUR'] = $myDoc->t_doc['DOC_ID_IMAGEUR'];
								$oImage->create();
								$oImage->saveVersions($_SESSION['arrLangues'],'crea');
								unset($oImage);
							}
						}
						
						mv_rename($dir."/".$file,kStoryboardDir.$file);
					}
					
					unset($oImage);
				}
				closedir($dh);
			}
		}

		// mat
		$dir=$trashDir."/mat";
		if($this->t_trash['TRA_ENTITE'] == "doc"){
			if (is_dir($dir)) {
				if ($dh = opendir($dir)) {
					while (($file = readdir($dh)) !== false) {
						if($file[0]!='.') {
							$myMat=new Materiel();
							$myMat->ingest($dir."/".$file);
							$datas=xml2tab($this->t_trash['TRA_XML']);
							foreach ($datas['T_DOC'][0]['MAT'] as $key => $aMat) {
								if (strcmp($aMat['MAT_NOM'],$file) == 0)
									break;
							}
							$myMat->saveImageur($datas['T_DOC'][0]['MAT'][$key]['MAT_ID_IMAGEUR']);
							if($myMat->checkFileExists()) $myMat->t_mat['MAT_NUM'] = 1;
							$myMat->save();
							unset($myMat);
						}
					}
					closedir($dh);
				}
			}
		}else if ($this->t_trash['TRA_ENTITE'] == 'mat'){
			mv_rename($dir."/".basename($myMat->getFilePath()),dirname($myMat->getFilePath()));
		}
		
		// Suppression corbeille
		$this->delete();
		
		return true;
		
	}
	
	function getObjectTrashed() {
		if(empty($this->t_trash))
			$this->getTrash();
		
		switch($this->t_trash['TRA_ENTITE']){
			case "doc":
				$datas=xml2tab($this->t_trash['TRA_XML']);
				
				$doc=$datas['T_DOC'][0];
				$myDoc=new Doc();
				$myDoc->mapFields($doc);
				$this->_Object = $myDoc;
				unset($myDoc);
			break;
			case "mat":
				$datas=xml2tab($this->t_trash['TRA_XML']);
				
				
				$mat=$datas['T_MAT'][0];
				$myMat=new Materiel();
				$myMat->mapFields($mat);
				$this->_Object = $myMat;
				unset($myMat);
			break;
		}
	}
	
	/** Export XML de l'objet Trash
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete=0,$encodage=0,$indent="",$root="") {
		
		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }
		if(!empty($root)) $content.="<".$root.">";
		$content.=$indent."<t_trash>\n";
		// VALEUR
		foreach ($this->t_trash as $fld=>$val) {
			if($fld=="TRA_XML") $content .= $indent."\t<".strtoupper($fld).">".str_replace(array(" ' ","&"),array("'","&amp;"),$val)."</".strtoupper($fld).">\n";
			else $content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}
		$content.=$indent."</t_trash>\n";
		if(!empty($root)) $content.="</".$root.">";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
	

}

?>
