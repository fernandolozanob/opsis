<?
//************************************************** TAPE_SET ***************************************************************

// PC : 26/09/2012 : Création du fichier, définition de la classe TapeSet


//************************************************** TAPE_SET ***************************************************************
require_once(modelDir.'model.php');
class TapeSet extends Model
{

	var $t_tape_set;
	var $t_tape;
	var $error_msg;


	function __construct($id=null,$version='') {
		parent::__construct('t_tape_set',$id,$version);
	}

	/** Récupère un objet tape ou un tableau d'objet
		* 	IN : tableau
		* 	OUT : objet(s)
		*/
	function getTapeSet($arrTapeSet=array()) {
		global $db;
		if (!$arrTapeSet) $arrVals=array($this->t_tape_set['ID_TAPE_SET']); else $arrVals=(array)$arrTapeSet;
		$arrCrit['ID_TAPE_SET']=$arrVals;

		$this->t_tape_set=getItem("t_tape_set",null,$arrCrit,null,0); 
		
		if (count($this->t_tape_set)==1 && !$arrTapeSet) { // Un seul résultat : on affecte le résultat à la valeur en cours
			$this->t_tape_set=$this->t_tape_set[0];
		}
		else {
            $arrVals=array();
			foreach ($this->t_tape_set as $idx=>$TapeSet) { // Plusieurs résultats : on renvoie un tableau d'objet contenant les résultats
				$arrVals[$idx]= new TapeSet();
				$arrVals[$idx]->t_tape_set=$TapeSet;
			}
			unset($this);
			return $arrVals;
		}
	}


	/** Met à jour l'objet avec les données provenant d'un tableau (ex: POST)
	 * 	IN : tableau
	 * 	OUT : objet màj
	 */
 	function updateFromArray ($tab_valeurs) {
	 	if (empty($this->t_tape_set)) $this->init();
	 	foreach ($tab_valeurs as $fld=>$val) {
			if (isset($this->t_tape_set[strtoupper($fld)])) $this->t_tape_set[strtoupper($fld)]=$val;
			if($fld=="t_tape" && is_array($val)){
				foreach($val as $i=>$x){
					foreach($x as $j=>$y){
						if(!empty($y)) $this->t_tape[$j][$i]=$y;
					}
				}
			}
		}
	}

	/**
		* Vérification existence tape 
	 * IN : objet tape
	 * OUT
	 */
	function checkExist() {
		global $db;
		$idTape=$db->GetOne("SELECT ID_TAPE_SET from t_tape_set WHERE ID_TAPE_SET=".intval($this->t_tape_set['ID_TAPE_SET']));
		return $idTape; //false si n'existe pas
	}
	

	/**
		* Sauvegarde objet tape 
	 * IN : objet tape
	 * OUT : base mise à jour
	 */
	function save($failIfExists=false) {
		global $db;
		
		if (empty($this->t_tape_set['ID_TAPE_SET']) || !$this->checkExist()) {return $this->create($failIfExists);}
		
		$rs = $db->Execute("SELECT * FROM t_tape_set WHERE ID_TAPE_SET=".intval($this->t_tape_set['ID_TAPE_SET']));
		$sql = $db->GetUpdateSQL($rs, $this->t_tape_set);
		if (!empty($sql)) $ok = $db->Execute($sql);
		if (!$ok && !empty($sql)) {$this->dropError(kErrorTapeSetSauve);trace($sql);return false;}
		
		$this->dropError(kSuccesTapeSetSauve);
		$this->updateTapeSetMiroir();
		return true;
	}
	
	/**
		* Creation tape 
	 * IN : objet tape
	 * OUT : base mise à jour
	 */
	function create ($failIfExists=false) {
		global $db;
		if (!empty($this->t_tape_set['ID_TAPE_SET']) && $this->checkExist()) {return $this->save($failIfExists);}
		
		//$this->t_tape_set['TAPE_DATE_CREA']=str_replace("'","",$db->DBTimeStamp(time()));
		//$this->t_tape_set['TAPE_ID_USAGER_CREA']=$_SESSION['USER']['ID_USAGER'];
		
		unset($this->t_tape_set['ID_TAPE_SET']); //Pour ne pas faire apparaitre la colonne dans le SQL ET générer un ID
		$ok = $db->insertBase("t_tape_set","id_tape_set",$this->t_tape_set);
		if (!$ok) {$this->dropError(kErrorTapeSetCreation);trace($sql);return false;}
		$this->t_tape_set['ID_TAPE_SET']=$ok;
		
		$this->dropError(kSuccesTapeSetCreation);
		$this->updateTapeSetMiroir();
		return true;
	}
	
	function updateTapeSetMiroir() {
		global $db;
		
		if (isset($this->t_tape_set['TS_ID_TAPE_SET_MIROIR']) && is_numeric($this->t_tape_set['TS_ID_TAPE_SET_MIROIR'])){
			$setMiroir = new TapeSet();
			$setMiroir->t_tape_set['ID_TAPE_SET']=$this->t_tape_set['TS_ID_TAPE_SET_MIROIR'];
			$setMiroir->getTapeSet();
			if ($setMiroir->t_tape_set['TS_ID_TAPE_SET_MIROIR'] != $this->t_tape_set['ID_TAPE_SET']){
				$setMiroir->t_tape_set['TS_ID_TAPE_SET_MIROIR']=$this->t_tape_set['ID_TAPE_SET'];
				$setMiroir->save();
			}
			unset($setMiroir);
		}
		else {
			$sql = "UPDATE t_tape_set SET TS_ID_TAPE_SET_MIROIR = null WHERE TS_ID_TAPE_SET_MIROIR = ".intval($this->t_tape_set['ID_TAPE_SET']);
			$db->execute($sql);
		}
	}
	
	/**
	 * Suppression tape 
	 * IN : objet tape
	 * OUT : base mise à jour
	 */
	function delete() {
		global $db;
    	if (empty($this->t_tape_set['ID_TAPE_SET'])) {$this->dropError(kErrorTapeSetNoId);return false;}
        $ok=deleteSQLFromArray(array("t_tape_set"/*,"t_tape"*/),"ID_TAPE_SET",simpleQuote($this->t_tape_set['ID_TAPE_SET']));
		if (!$ok) {$this->dropError(kErrorTapeSetSuppr);return false;}
		return true;
	}

	/**
	 * Récupère les liens SET / TAPE 
	 * IN 
	 * OUT : tableau de classe t_tape
	 */
	function getTapes() {
		global $db;
		$sql="SELECT t_tape.*, count(tf.ID_TAPE) as NB FROM t_tape 
				LEFT OUTER JOIN t_tape_file tf ON tf.ID_TAPE = t_tape.ID_TAPE
				WHERE  t_tape.ID_TAPE_SET=".intval($this->t_tape_set['ID_TAPE_SET'])." 
				GROUP BY  t_tape.ID_TAPE, t_tape.id_tape_set, t_tape.tape_status, t_tape.tape_capacite, t_tape.tape_utilise, t_tape.tape_full, t_tape.tape_id_usager_crea, t_tape.tape_date_crea, t_tape.tape_id_usager_mod, t_tape.tape_date_mod, t_tape.tape_localisation
				ORDER BY  t_tape.ID_TAPE";
		$this->t_tape=$db->GetAll($sql);
		
		//Calcul du nombre de cartouches
		$nb_tape = count($this->t_tape);
		if ($this->t_tape_set['TS_NB_TAPES'] != $nb_tape)
			$this->updateNbTapes($nb_tape);
		$this->t_tape_set['TS_NB_TAPES'] = $nb_tape;
	}
	
	function updateNbTapes($nb_tape) {
		global $db;
		
		if (empty($this->t_tape_set['ID_TAPE_SET']) || !$this->checkExist() || !is_numeric($nb_tape)) {return false;}
		
		$sql="UPDATE t_tape_set 
				SET TS_NB_TAPES = $nb_tape 
				WHERE ID_TAPE_SET=".intval($this->t_tape_set['ID_TAPE_SET']);
		
		$ok=$db->Execute($sql);
		return true;
	}

 	/** Export XML de l'objet 
	 *  IN : entete XML(O/N) (opt), encodage(opt), chaine d'indentation(opt)
	 *  OUT : XML au format UTF8.
	 */
	function xml_export($entete="",$encodage="",$indent="") {


        $content="";

        $search = array('&','<','>');
        $replace = array('&#38;','&#60;','&#62;');

		if ($entete!=0) { $content.="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; }

		$content.=$indent."<t_tape_set>\n";
		foreach ($this->t_tape_set as $fld=>$val) {
			$content.=$indent."\t<".strtoupper($fld).">".xml_encode($val)."</".strtoupper($fld).">\n";
		}


       if (!empty($this->t_tape)) {
        $content.="\t<t_tape nb=\"".count($this->t_tape)."\">\n";
            // Pour chaque type de valeur
            foreach ($this->t_tape as $name => $value) {
                $content .= "\t\t<TAPE ID_TAPE=\"".$this->t_tape[$name]["ID_TAPE"]."\">\n";
                // Pour chaque valeur
                foreach ($this->t_tape[$name] as $val_name => $val_value) {
                    if (is_object($val_value)) {$content.=$val_value->xml_export();}
                    else {
                    	$content .= "\t\t\t<".$val_name.">".str_replace($search,$replace, $val_value)."</".$val_name.">\n";
                    }
                }
                $content .= "\t\t</TAPE>\n";
            }
        $content.="\t</t_tape>\n";
       }

		$content.=$indent."</t_tape_set>\n";
		// Encodage si demandé
        if($encodage!=0){
            return mb_convert_encoding($content,$encodage,"UTF-8");
        }
        else{
            return $content;
        }
	}
}

?>