<?php
require_once(modelDir.'model.php');
class GroupeMat extends Model
{
	private $t_groupe_mat;
	
	public function __construct($id_groupe=null,$version='')
	{
		parent::__construct('t_groupe_mat',$id_groupe,$version);
	}
	
	// rien de spécifique à la fonction getGroupeMat (a part sa rédaction très étrange) , se repose donc sur getData 
	function getGroupeMat($id_groupe){
		return $this->getData($id_groupe) ; 
	}
	
	/*
	public function getGroupeMat($id_groupe)
	{
		global $db;
		
		$id_groupe=intval($id_groupe);
			
		if (is_numeric($id_groupe))
		{
			$res=$db->Execute('SELECT * FROM t_groupe_mat WHERE ID_GROUPE_MAT='.intval($id_groupe))->GetRows();
			
			if (count($res)==1)
			{
				$this->t_groupe_mat['ID_GROUPE_MAT']=intval($res[0]['ID_GROUPE_MAT']);
				$this->t_groupe_mat['GROUPE_MAT']=$res[0]['GROUPE_MAT'];
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}*/
	
	public function setNomGroupe($nom)
	{
		if (isset($nom) && !empty($nom))
		{
			$nom=htmlentities($nom);
			$nom=addslashes($nom);
			$this->t_groupe_mat['GROUPE_MAT']=$nom;
		}
	}
	
	public function getIdGroupe()
	{
		return $this->t_groupe_mat['ID_GROUPE_MAT'];
	}
	
	public function getNomGroupe()
	{
		return $this->t_groupe_mat['GROUPE_MAT'];
	}
	
	public function save()
	{
		global $db;
		
		if (isset($this->t_groupe_mat['ID_GROUPE_MAT']) && !empty($this->t_groupe_mat['ID_GROUPE_MAT']))
		{
			$db->Execute('UPDATE t_groupe_mat SET GROUPE_MAT=\''.$this->t_groupe_mat['GROUPE_MAT'].'\' WHERE ID_GROUPE_MAT='.intval($this->t_groupe_mat['ID_GROUPE_MAT']));
		}
		else
			$this->create();
	}
	
	function create()
	{
		global $db;
		
		$db->Execute('INSERT INTO t_groupe_mat(GROUPE_MAT) VALUES (\''.$this->t_groupe_mat['GROUPE_MAT'].'\')');
		$this->t_groupe_mat['ID_GROUPE_MAT']=$db->getOne("SELECT max(ID_GROUPE_MAT) FROM t_groupe_mat WHERE GROUPE_MAT=".$db->Quote($this->t_groupe_mat['GROUPE_MAT']));
	}
	
	
	
	public static function getAllGroupes()
	{
		global $db;
		
		$result=array();
		
		$list_id_groupe_mat=$db->Execute('SELECT ID_GROUPE_MAT FROM t_groupe_mat')->GetRows();
		if (isset($list_id_groupe_mat) && !empty($list_id_groupe_mat))
		{
			foreach($list_id_groupe_mat as $id_gm)
			{
				$result[]=new GroupeMat($id_gm['ID_GROUPE_MAT']);
			}
		}
		
		return $result;
	}
}

?>