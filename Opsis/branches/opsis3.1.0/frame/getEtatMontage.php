﻿<?php
	// rappel états montage en session : 
	// SESSION[etat_montage][state] => "init", "prep", "encod", "ended", "error"
	// SESSION[etat_montage][current] => fichier en cours de prep, SESSION[etat_montage][nb_seqs] => nombre de fichiers à encoder avant montage

	// var_dump($_SESSION['etat_montage']);
	
	if(isset($_SESSION['etat_montage'])){
		switch($_SESSION['etat_montage']['state']){
			case "init" : 
				echo kMsgMontageInit;
			break; 
			case "prep":
				echo kMsgMontagePrep.$_SESSION["etat_montage"]["current"]."/".$_SESSION["etat_montage"]["nb_seqs"];
			break;
			
			case "encod" : 
				echo kMsgMontageEncod;
			break;
			
			case "ended" : 
				echo kMsgMontageEnded."<br /><a id='download_link' href='empty.php?urlaction=downloadMontageWrapper' alt='lien_telechargement'>".kTelecharger."</a>";

			break;
			
			case "error" : 
				echo kMsgMontageError;
			break;
		}
	}
	
	
	
?>