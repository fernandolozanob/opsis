<?
global $db;
$parentId=$_REQUEST['parentId'];
$type_cat=$_REQUEST['id_type_cat'];
$newName=$_REQUEST['newName'];
$langue=$_SESSION['langue'];
$date=date('Y-m-d H:i:s');
// error_reporting(E_ALL);
if(!empty($newName) && !empty($parentId) && !empty($type_cat)){
	if($parentId == '-1') $parentId=0;
	require_once(modelDir.'model_categorie.php');
	$cat = new Categorie();
	$cat->t_categorie['ID_LANG'] = $_SESSION['langue'];
	$cat->getCategorie() ; 
	$cat->t_categorie['CAT_ID_GEN'] = $parentId;
	$cat->t_categorie['CAT_ID_TYPE_CAT'] = $type_cat;
	$cat->t_categorie['CAT_NOM'] = $newName;
	$versions = array() ; 
	// MS - reconstitution du tableau de versions permettant de créer les nouvelles versions (cf formulaire catSaisie)
	// ne me plait pas trop ... il faudrait éventuellement trouver une version plus élégante 
	foreach($_SESSION['arrLangues'] as $lang){
		if($lang != $_SESSION['langue']){
			$versions[$lang] = $newName;
		}
	}
	$cat->create() ; 
	if(!empty($versions)){
		$cat->saveVersions($versions) ; 
	}
	echo $cat->t_categorie['ID_CAT'];
}
?>