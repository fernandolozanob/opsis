<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require_once(modelDir.'model_doc.php');
require_once(modelDir.'model_materiel.php');



//sleep(10); pour tester #dialog_loading dans docSousTitres.inc.php

$id_mat = (int) $_REQUEST['id_mat'];
$tab_id_mat = $_REQUEST['tab_id_mat'];
$id_doc = (int) $_REQUEST['id_doc'];
$id_val = (int) $_REQUEST['id_val'];
$id_mat_subtitle = (int) $_REQUEST['id_mat_subtitle'];
$id_job = (int) $_REQUEST['id_job'];
$matName = filter_var(trim($_REQUEST['matName']), FILTER_SANITIZE_STRING);
global $db;


//$subtitle_lang = filter_var(trim(strtoupper($_REQUEST['subtitle_lang'])), FILTER_SANITIZE_STRING);


if (defined('kSousTitrage_lang')) {
	$tab_subtitleLang_VAL = unserialize(kSousTitrage_lang);

	if (is_array($tab_subtitleLang_VAL) && !empty($tab_subtitleLang_VAL)) {
		if (trim($tab_subtitleLang_VAL['id_type_val']) != '') {
			$id_type_val = strtoupper(trim($tab_subtitleLang_VAL['id_type_val']));
		} else {
			header("HTTP/1.0 404 Not Found");
			header("Warning: identifiant valeur de langue du sous-titrage non defini");
			exit;
		}
	}
	$aLangSupported = array_keys($tab_subtitleLang_VAL['supported']);
	
} else {
	header("HTTP/1.0 404 Not Found");
	header("Warning: constante de langue du sous-titrage non defini");
	exit;
}



if (isset($_REQUEST['testSession'])) {

	echo '<pre>';
	var_dump($_SESSION['sousTitrageEditionVolee'][$id_doc]);
	echo '</pre>';
	exit;
}


if (isset($_REQUEST['isFileExists'])) {


	$file = kCheminLocalPrivateMedia . '/videos/' . filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);

	if (file_exists($file)) {
		echo 'true';
	} else {
		echo 'false';
	}

	exit;
}




//dans le cas ou on ajoute un ligne de sous-titre, et qu'on vient de switch de matériel, comme la page docSousTitres.inc.php n'est pas rafraichie
//une utilisation de $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc_int]['id_mat'] dans une fonction js risque d'afficher l'ancien id_mat
if (empty($id_mat) && !empty($id_doc)) {
	if ($matName != '') {
		//on va essayer de trouver l'id mat si on a un nom de matériel (utilisé dans bouton importer pour restore le mat , quand la cliente écrase un matériel à partir d'un csv de même nom
		$sql = "SELECT t_mat.id_mat FROM t_mat INNER JOIN t_doc_mat ON t_mat.id_mat=t_doc_mat.id_mat AND t_doc_mat.id_doc=" . $id_doc . " AND t_mat.mat_nom='" . $matName . "'";
		$id_mat = (int) $db->GetOne($sql);
	} elseif (!empty($_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc]['id_mat'])) {
		$id_mat = (int) $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc]['id_mat'];
	}
}


$id_ligne = filter_var($_REQUEST['id_ligne'], FILTER_SANITIZE_STRING); //ligne du sous-titre
//Essaye de determiner le + rapidement (sans devoir parcourir toutes les lignes du tableau) si l'utilisateur a fait des modifications sur le fichier de sous-titre d'origine

function detectModifSubtitle($tabOrigin, $tab) {

	if (count($tabOrigin) != count($tab)) {

		return true;
	}

	//même taille, mais est ce que les clés tcin ont été modifiés ?

	if (array_diff_assoc($tabOrigin, $tab)) {
//        echo 'CLE différente';
		return true;
	}
	//même taille, même clés, mais est le contenu a été modifié ?
//    On parcourt le tableau d'origine, ligne par ligne, on sort dès que trouvé différence

	foreach ($tabOrigin as $key => $value) {
//        echo 'key ' . $key . '<br />';
		if (array_diff($tabOrigin[$key], $tab[$key])) {
			return true;
		}
	}

	return false;
}

function parseFromSubtitleFile($id_doc, $id_mat, $path_mat, $type = 'session') {

	$file_as_array = file($path_mat, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

	/* Attention on met === et pas juste == car on veut detecter le cas ou file() a echoué (c'est a dire bool FALSE)
	  or dans un fichier existant mais vide , on obtient $file_as_array=array(0)==false */
	if ($file_as_array === FALSE) {
		echo "Erreur : impossible d'ouvrir le fichier<br />";
		exit;
	}


	if ($type == 'session') {
		$_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat] = array();
	} elseif ($type == 'return') {
		$tab_return = array();
	}




	foreach ($file_as_array as $f) {

		if (!(preg_match("/^\x{feff}*\d+$/u", $f) || preg_match("/^\\x{feff}*WEBVTT$/u", $f))) { //\\x{feff}* : permet de matcher la présence d'un BOM
//            echo 'debug_affich ' . $f . '<br/>';

			if (preg_match("/^(\d{2}:\d{2}:\d{2}(\.|,)\d{3}) --> (\d{2}:\d{2}:\d{2}(\.|,)\d{3})/", $f, $match)) {

				$flag_lineIsText = false;


				//Important on prend le tcin( qui est toujours unique) comme CLE d'une ligne de sous-titre, cela permet de simplifier l'insertion d'1 ligne de sous-titre entre 2 sous-titres


				$key_current = str_replace(',', '.', $match[1]); //en cas de fichier SRT (on génére quand meme un format VTT) mais à l'ecrasement du matériel, on générera un SRT

				$tc_out = array('tcout' => str_replace(',', '.', $match[3]));

				if ($type == 'session') {
					$_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat][$key_current] = $tc_out;
				} else if ($type == 'return') {
					$tab_return[$key_current] = $tc_out;
				}
			} else {

				/*   -Si 1 ligne de texte, on ne rajoute pas de \n
				 *   ex:
				 *   00:00:00.200 --> 00:00:07.810
				 *   You.
				 *   00:00:10.000 --> 00:00:12.700
				 *   Me. 
				 *   
				 *   -Si plusieurs ligne de texte, cela doit faire
				 *   00:00:29.180 --> 00:00:33.720
				 *   You know i need one pure
				 *   \nalmost all these ladies know sick (on rajoute le \n ici car comme on parcourt le fichier ligne par ligne, c'est seulement à ce moment là qu'on sait que la ligne precedente est du text)
				 *   00:00:33.920 --> 00:00:38.580
				 * 
				 */

				$sep = null;
				if ($flag_lineIsText) {
					$sep = "\n";
				}

				if ($type == 'session') {
					$_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat][$key_current]['text'].=$sep . $f; //Attention, le texte peut être sur plusieurs lignes !
				} else if ($type == 'return') {
					$tab_return[$key_current]['text'].=$sep . $f; //Attention, le texte peut être sur plusieurs lignes !
				}

				$flag_lineIsText = true;
			}
		}
	}

	if ($type == 'return') {
		return $tab_return;
	}
}

if (isset($_REQUEST['job_generateMatDiffSubtitled'])) {

	$tab_response = null;


	if (!empty($tab_id_mat) && !empty($id_mat_subtitle)) {
		
		$tab_list_mat_diff=array();

		require_once(modelDir.'model_processus.php');

		$matSousTitre = new Materiel($id_mat_subtitle);
		$matSousTitre->getValeurs();


		$suffixeLang = NULL;
		foreach ($matSousTitre->t_mat_val as $value) {

			if (strtoupper($value["VAL_ID_TYPE_VAL"]) == $id_type_val && !empty($value["VAL_CODE"])) {

				$suffixeLang = strtolower($value["VAL_CODE"]);
			}
		}


		if (!empty($suffixeLang)) {

			foreach ($tab_id_mat as $v) {

				$procObj = New Processus();
				$procObj->t_proc["ID_PROC"] = 99;
				$procObj->getProc();
				$procObj->getProcEtape();


				$matObj = new Materiel();
				$matObj->t_mat['ID_MAT'] = $v;
				$matObj->getMat(array(), false);
				
				
				$procObj->t_proc["PROC_NOM"]=kSousTitrage_generateDiffMatIncrustedMailSubject;


				$procObj->t_proc_etape[0]["ETAPE"]->t_etape["ETAPE_OUT"] = '_subtitled_' . $suffixeLang . '.mp4';

//	$myParams['dynamic_param']['ID_MAT_SUBTITLE']=array($myPanier->t_panier_doc[0]["ID_LIGNE_PANIER"]=>$_GET["id_mat_subtitle"]);
				$job_param = '<param><send_mail>1</send_mail><ID_MAT_SUBTITLE>' . $id_mat_subtitle . '</ID_MAT_SUBTITLE></param>';

				$priorite = 2;
				$bypass_etapes = array(); //sinon in_array() expects parameter 2 to be array, null given in <b>/var/www/html/dev/babou/opsis/opsis3.0.2/lib/class_processus.php</b> on line <b>211<
//				$procObj->createJobs($matObj->t_mat['ID_MAT'], $matObj->t_mat['MAT_NOM'], $job_param, $jobObj, $mat['FILE_NAME_MASK'], $superJobId, 0, $bypass_etapes, $priorite, $job_plateforme, $date_lancement_job);


				$tab_list_mat_diff[]= substr($matObj->t_mat['MAT_NOM'], 0, strrpos($matObj->t_mat['MAT_NOM'], '.')) . $procObj->t_proc_etape[0]["ETAPE"]->t_etape["ETAPE_OUT"] ;
//			echo '<div style="position:relative;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_affich $procObj<pre>';
//			var_dump($procObj);
//			echo '</pre></div><br />';
			}
			$tab_response['OK']=implode(', ',$tab_list_mat_diff);
			
		} else {
			$tab_response['KO'] = 'code de la langue manquant';
		}
	} else {
		$tab_response['KO'] = 'id_mat ou id_mat subtitle manquant';
	}


	echo json_encode($tab_response);
	exit;
}







if (isset($_REQUEST['publish_isExistsOtherSubtitlePUBLISHEDInThisLanguage'])) {
//1.On determine la langue de l'id mat qu'on veut publier
	$lang = NULL;
	if (is_array($_SESSION['sousTitrageEditionVolee']['AllInCatLang'][$id_doc])) {
		foreach ($_SESSION['sousTitrageEditionVolee']['AllInCatLang'][$id_doc] as $k => $v) {

			if (in_array($id_mat, $v)) {
				$lang = $k;
				break;
			}
		}
	}


	if (empty($lang)) {
		echo 'error';
		exit;
	}


	//Attention, si il n'y a AUCUN sous-titre publié alors $_SESSION['sousTitrageEditionVolee']['lastInCatLangPUBLISHED'][$id_doc] est NULL ET Warning: array_key_exists() expects parameter 2 to be array, null given

	if (empty($_SESSION['sousTitrageEditionVolee']['lastInCatLangPUBLISHED'][$id_doc])) {

		echo 'false';
	} else if (!array_key_exists($lang, $_SESSION['sousTitrageEditionVolee']['lastInCatLangPUBLISHED'][$id_doc])) {
//		Aucun sous-titre publié dans cette langue
		echo 'false';
	} else {

		//Il ne peut avoir qu'1 sous-titre publié dans 1 catégorie de langue, c'est pourquoi la ligne ci-dessous suffit
		if ($_SESSION['sousTitrageEditionVolee']['lastInCatLangPUBLISHED'][$id_doc][$lang] == $id_mat) {
			echo 'false';
		} else {
			echo 'true';
		}
	}

	exit;
}

//Pour la fenêtre d'upload de sous-titre, on regarde pour cette notice s'il existe deja un sous-titre  (qu'il soit publié ou pas) avec ce mat_val VERS
if (isset($_REQUEST['upload_isExistsSubtitleInThisLanguage'])) {
	$myDoc = new Doc();
	$myDoc->t_doc['ID_DOC'] = $id_doc;

	$myDoc->getMats(true);


	$tab_extSubtitle = array('SRT', 'VTT');
	if (defined('gSubtitleTypes')) {
		$gSubtitleTypes = unserialize(gSubtitleTypes);
		if (is_array($gSubtitleTypes) && !empty($gSubtitleTypes)) {
			$tab_extSubtitle = array_map("strtoupper", $gSubtitleTypes);
			unset($tab_extSubtitle['csv']);
		}
	}


	foreach ($myDoc->t_doc_mat as $value) {

		if (in_array($value["MAT"]->t_mat["MAT_FORMAT"], $tab_extSubtitle)) {
//		if (in_array($value["MAT"]->t_mat["MAT_FORMAT"], $tab_extSubtitle) && empty($value["DMAT_INACTIF"])) {//quand on upload même si etat non publié, on considere qu'il y en a 1, et on peut l'ecraser


			foreach ($value["MAT"]->t_mat_val as $value2) {
				if ($value2["ID_VAL"] == $id_val && $value2["VAL_ID_TYPE_VAL"] == $id_type_val) {
					echo 'true';
					exit;
				}
			}
		}
	}


	echo 'false';
	exit;
}





if (isset($_REQUEST['getActiveMat'])) {

	echo $_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc]['id_mat'];
	exit;
}

if (isset($_REQUEST['mailTranscriptionFinished'])) {
	if (empty($id_job)) {
		die('id_job manquant');
	}

//	echo '<pre>';
//	var_dump($_SESSION["USER"]["ID_USAGER"]);
//		echo '</pre>';


	require_once(modelDir.'model_job.php');
	$j = new Job();
	$j->t_job['ID_JOB'] = $id_job;
	$j->getJob();

	//Attention ILIKE ne passe pas sur mysql !
//	$sql = "SELECT id_module FROM t_module WHERE module_nom ILIKE '%authot%'";
	$sql = "SELECT id_module FROM t_module WHERE LOWER(module_nom) LIKE '%authot%'";

	//pour éviter qu'un petit malin relance la page ajax plusieurs fois pour spammer de mail, on verifie AVANT d'envoyer un mail que c'est un job terminé, job de transcription + job_id_usager correspond au user connecté
	if ($db->getOne($sql) == $j->t_job["JOB_ID_MODULE"] && $j->t_job["JOB_ID_ETAT"] == 9 && $j->t_job["JOB_PROGRESSION"] == 100 && $j->t_job["JOB_ID_USAGER"] == $_SESSION["USER"]["ID_USAGER"]) {
		$sujet = kSousTitrage_transcription_subjectMail;
		$dest = $j->get_mail_user(); //on ne met pas null car cela envoie dans ce cas aussi à gMailJob par défaut
		//abdandon de job->send_mail car contenu du mail vide...., on va passer par la classe Message
		require_once(modelDir.'model_message.php');
		$msg = new Message();

		$xsl = "print/jobMailTranscriptionFinished.xsl";
		$xml = $j->xml_export(0, 0, '', 0);
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>" . $xml . "\n</EXPORT_OPSIS>";

		$frontiere = "_----------=_parties_" . md5(uniqid(rand()));
		$param = array("boundary" => $frontiere);
		$message = TraitementXSLT($xml, getSiteFile("designDir", $xsl), $param, 1);

		ob_start();
		eval("?>" . str_replace(array('||', " ' "), array(chr(10), "'"), $message) . "<?");
		$message = ob_get_contents();
		ob_end_clean();


//		die(var_dump($message));


		if ($msg->send_mail($dest, gMail, $sujet, $message, "text/plain")) {
//		if ($j->send_mail($dest, $sujet, "print/jobMailTranscriptionFinished.xsl")) {
			echo 'Mail envoyé';
		} else {
			echo 'Mail problem';
		}
	}

//	die(var_dump($j->xml_export(0,0,'',0)));
//	send_mail($dest="",$sujet="",$xsl="print/jobMail.xsl",$withChilds=0){
//	echo '<pre>';
//	var_dump($j);
//	echo '</pre>';



	exit;
}





if (empty($id_mat)) {

	echo '<span style="display:none">Erreur : identifiant du matériel manquant<br /></span>'; //il peut se produire cet  affichage sur checkVideoPlayerReadyBeforeAddTMPtrack() donc on cache le message au client(tout en ayant l'information dans la console de debuggage du navigateur)
	exit;
}


if (isset($_REQUEST['isMatAlreadyTranscripted'])) {

	if(!empty($_REQUEST['lang'])) {
		$langChecked = $_REQUEST['lang'];
		//Sécurité SQL par whitelist. Si le paramètre passé est différent, on arrête avant la requête et on bloque la demande.
		if(!in_array(strtolower($langChecked), $aLangSupported)) {
			echo "true";
			exit;
		}
		$where_lang = "AND j.job_param like '%<LANG>".strtoupper($langChecked)."</LANG>%'";
	} else {
		$where_lang = '';
	}
	//Attention ILIKE ne passe pas sur mysql !
	//$sql = "SELECT id_job FROM t_job j JOIN t_module m ON j.job_id_module=m.id_module WHERE module_nom ILIKE '%authot%' AND j.job_id_etat=9 AND j.job_id_mat=" . $id_mat . " ORDER BY id_job DESC LIMIT 1";
	$sql = "SELECT id_job, job_out FROM t_job j JOIN t_module m ON j.job_id_module=m.id_module WHERE LOWER(module_nom) LIKE '%authot%' AND j.job_id_etat in (0, ".jobAttente.", ".jobEnCours.", ".jobErreur.", ".jobFini.") AND j.job_id_mat=". $id_mat. ' '. $where_lang." ORDER BY id_job DESC LIMIT 1";

//	echo '<div style="position:relative;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_affich $sql<pre>';
//	var_dump($sql);
//	echo '</pre></div><br />';
	$r = $db->getAll($sql);

	if (empty($r)) {
		echo 'false';
	} else {
		$sql2 = "SELECT lieu_path FROM t_lieu WHERE LOWER(lieu)='vtt' LIMIT 1";
		//on regarde si le fichier n'a pas été supprimé physiquement de la liste de matériels
		$r2 = $db->getOne($sql2);

		if (file_exists($r2 . $r[0]['job_out'] . '.vtt')) {
			echo 'true';
		} else {
			echo 'false';
		}
	}
	
	exit;
}


if (empty($id_doc)) {

	echo 'Erreur : identifiant de la notice manquant<br />';
	exit;
}



if (isset($_REQUEST['show'])) {


	$mat = new Materiel ();
	$mat->t_mat['ID_MAT'] = $id_mat;
	$mat->getMat(array(),'', false);

	$tab_mat = explode('.', $mat->t_mat['MAT_NOM']);
	$ext_mat = strtoupper(array_pop($tab_mat));


	$tab_nom_mat = explode('_', $tab_mat[0]);
//	$lang_mat = strtoupper(array_pop($tab_nom_mat));
//	var_dump($lang_mat);

	if (!in_array($ext_mat, array('SRT', 'VTT'))) {

		echo "Erreur : ce matériel n'a pas une extension de sous-titre<br />";
		exit;
	}


	//OBSOLETE on efface les autres materiels qu'on edite plus (par notice), on garde à moins qu'on clique sur le bouton Annuler toutes les modifications
//    foreach ($_SESSION['sousTitrageEditionVolee'][$id_doc] as $k => $v) {
//        if ($k != $id_mat) {
//            unset($_SESSION['sousTitrageEditionVolee'][$id_doc][$k]);
//        }
//    }

	if (empty($_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat])) {
		parseFromSubtitleFile($id_doc, $id_mat, $mat->getFilePath());
	}



	$_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc]['id_mat'] = $id_mat;
	$_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc]['type'] = $ext_mat;

//	if (strlen($lang_mat) == 2) {
//		$_SESSION['sousTitrageEditionVolee']['activeSubtitle'][$id_doc]['lang'] = $lang_mat;
//	}
//    echo '<pre>';
//    print_r($_SESSION['sousTitrageEditionVolee'][$id_mat]);
//    echo '</pre>';



	$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><subtitles></subtitles>');
	$xml->addChild('active_id_mat', $id_mat);

	ksort($_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat]);


	foreach ($_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat] as $key => $value) {


		$node = $xml->addChild('subtitle');
		$node->addChild('tcin', $key);
		$node->addChild('tcout', $value["tcout"]);
		$node->addChild('text', $value["text"]);
	}



//	$xsl = getSiteFile("designDir", "form/subtitleEditVolee.xsl");
	$xsl = getSiteFile("formDir", "subtitleEditVolee.xsl");


	$contentExport = TraitementXSLT($xml->asXML(), $xsl, array(
		"profil" => User::getInstance()->getTypeLog(),
		"gestPers" => gGestPers,
		"loggedIn" => loggedIn(),
		"id_doc" => $id_doc,
		"id_mat" => $id_mat,
			)
			, 0);








	ob_start();
	$contentExport = eval("?" . chr(62) . $contentExport . chr(60) . "?");
	ob_end_flush();

//    echo '<table>';
//    echo '<tr><th>' . kDebut . '</th><th>' . kFin . '</th><th>' . kSousTitre . '</th><th></th><th></th><th></th></tr>';
//    foreach ($_SESSION['sousTitrageEditionVolee'][$id_mat] as $key => $value) {
//        echo '<tr>';
//        echo '<td></td>';
//        echo '<td></td>';
//        echo '<td></td>';
//
//
//        echo '</tr>';
//    }
//    echo '</table>';
//    echo '<pre>';
//    var_dump($_SESSION['sousTitrageEditionVolee']);
//    echo '</pre>';
} elseif (isset($_REQUEST['modif'])) {


	$tcin = filter_var(trim($_REQUEST['tcin']), FILTER_SANITIZE_STRING);
	$tcout = filter_var(trim($_REQUEST['tcout']), FILTER_SANITIZE_STRING);
//	$text = filter_var(trim($_REQUEST['text']), FILTER_SANITIZE_STRING);//cela encode les caractères html mais cela pose ensuite probleme pour l'acion detectModif
	$text = trim($_REQUEST['text']);


//	Si le texte d'une ligne de sous-titre contient une ligne vide (espace + LF ou LF) , cela foire l'incrustation du sous-titre dans la video, on remplace donc par tab
	$tabText = explode("\n", $text);
	foreach ($tabText as $k_Text => $v_Text) {
		if (trim($v_Text) == '') {
			$tabText[$k_Text] = "\t"; //les lignes vides (espace + LF ou LF) foire l'incrustation de la video
		}
	}

	$text = implode("\n", $tabText);

	$pattern_timeCode = "/^\d{2}:\d{2}:\d{2}\.\d{3}$/";
	if (!preg_match($pattern_timeCode, $tcin)) {
		echo "le temps de Début n'est pas au bon format<br />";
		exit;
	}

	if (!preg_match($pattern_timeCode, $tcin)) {
		echo "le temps de Fin n'est pas au bon format<br />";
		exit;
	}




//Si le client modifie le tcin , il modifie la CLE de la ligne de sous-titre, on insère donc une nouvelle ligne
	if ($tcin != $id_ligne) {
		$_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat][$tcin] = array('tcout' => $tcout, 'text' => $text);
		unset($_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat][$id_ligne]);
	} else {
		$_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat][$id_ligne] = array('tcout' => $tcout, 'text' => $text);
	}


//    foreach ($_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat] as $key => $value) {
//
//        echo $key . '<pre>' . print_r($value, true) . '</pre><br />';
//    }
} elseif (isset($_REQUEST['delete'])) {
//pas d'inquiètude même si $id_ligne est vide
	unset($_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat][$id_ligne]);
//    foreach ($_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat] as $key => $value) {
//
//        echo $key . '<pre>' . print_r($value, true) . '</pre><br />';
//    }
} elseif (isset($_REQUEST['restore'])) {
	$mat = new Materiel ();
	$mat->t_mat['ID_MAT'] = $id_mat;
	$mat->getMat(array(),'', false);
	parseFromSubtitleFile($id_doc, $id_mat, $mat->getFilePath());
} elseif (isset($_REQUEST['save'])) {

	$mat = new Materiel();
	$mat->t_mat['ID_MAT'] = $id_mat;
	$mat->getMat(array(),'', false);
	$path_origin = $mat->getFilePath();
	$tab_mat = explode('.', $mat->t_mat['MAT_NOM']);
	$ext_mat = strtoupper(array_pop($tab_mat));

	$subtitlesMat = $_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat];

	if (is_array($subtitlesMat) && !empty($subtitlesMat)) {

		ob_start();
		/* Attention ! mettre les \n entre guillemets doubles (et pas guillemet simple '\n') pour que cela soit correctement interpreté !!!! */
		if ($ext_mat == 'VTT') {
			echo "WEBVTT\n";
		}

		$i = 0;
		foreach ($subtitlesMat as $k => $v) {
			if ($ext_mat == 'SRT' && $i == 0) {
				echo "$i\n";
			} else {
				echo "\n$i\n";
			}

			if ($ext_mat == 'SRT') {
				$k = str_replace('.', ',', $k);
				$tcout = str_replace('.', ',', $v["tcout"]);
			} else {
				$tcout = $v["tcout"];
			}
			echo "$k --> " . $tcout . "\n";



			//	Si le texte d'une ligne de sous-titre contient une ligne vide (espace + LF ou LF) , cela foire l'incrustation du sous-titre dans la video, on remplace donc par tab
			$tabText = explode("\n", $v["text"]);
			foreach ($tabText as $k_Text => $v_Text) {
				if (trim($v_Text) == '') {
					$tabText[$k_Text] = "\t"; //les lignes vides (espace + LF ou LF) foire l'incrustation de la video, attention même si on enregistre dans la session en tab (action=MODIF), on le laisse quand même ici au cas ou une ligne ne serait pas modifié)
				}

				//attention, ne pas mettre ENT_QUOTES sinon detectModifSubtitle() va foirer (dans le fichier ' sera transcrit par le code ascii 39, alors que dans la session, ca sera le caractère ascii 38) , m
//				echo html_entity_decode($tabText[$k_Text], ENT_QUOTES, 'UTF-8') . "\n"; //html_entity_decode pour éviter ex: L&#39;A54, une des autoroutes
				echo html_entity_decode($tabText[$k_Text], ENT_NOQUOTES, 'UTF-8') . "\n"; //html_entity_decode pour éviter ex: L&#39;A54, une des autoroutes
			}






			$i++;
		}


		$modifs = ob_get_contents();
		ob_end_clean();


//        var_dump($modifs);
		// Let's make sure the file exists and is writable first.
		if (is_writable($path_origin)) {


			if (!$handle = fopen($path_origin, 'w')) {
				trace("subtitle : Cannot open file ($path_origin)");
				echo "Cannot open file";
				exit;
			}

			// Write $modifs to our opened file.
			if (fwrite($handle, $modifs) === FALSE) {
				trace("subtitle :Cannot write to file ($path_origin)");
				echo "Cannot write to file";
				exit;
			}

			echo "OK";

			fclose($handle);
		} else {
			trace("subtitle : The file $path_origin is not writable");
			echo "The file is not writable";
		}
	} else {
		echo 'Modifications vides';
	}
} else if (isset($_REQUEST['info_isPublish'])) {
	$tab_response = null;
	require_once(modelDir.'model_docMat.php');

	$mydMatSubtitle = new DocMat();
	$mydMatSubtitle->t_doc_mat['ID_DOC'] = $id_doc;
	$mydMatSubtitle->t_doc_mat['ID_MAT'] = $id_mat;

	$id_doc_mat = (int) $mydMatSubtitle->checkDocMatExists();
	if ($id_doc_mat) {
		$mydMatSubtitle->t_doc_mat['ID_DOC_MAT'] = $id_doc_mat;
		$mydMatSubtitle->getDocMat();
		if ($mydMatSubtitle->t_doc_mat['DMAT_INACTIF'] || !isset($mydMatSubtitle->t_doc_mat['DMAT_INACTIF'])) {
			$tab_response['OK'] = array('etat' => 'unpublished', 'lib' => kPublier);
//			echo kPublier;
		} else {
			$tab_response['OK'] = array('etat' => 'published', 'lib' => kDepublier);
//			echo kDepublier;
		}
	} else {
		$tab_response['KO'] = 'KO';
	}

	echo json_encode($tab_response);
} else if (isset($_REQUEST['info_isModified'])) {

	$mat = new Materiel();
	$mat->t_mat['ID_MAT'] = $id_mat;
	$mat->getMat(array(),'', false);


	$originFile = parseFromSubtitleFile($id_doc, $id_mat, $mat->getFilePath(), 'return');

//    echo $id_mat.'<pre>';
//    var_dump($originFile);
//    echo '</pre>';

	if (detectModifSubtitle($originFile, $_SESSION['sousTitrageEditionVolee'][$id_doc][$id_mat])) {
		if (!empty($_REQUEST['ask_id_mat'])) {//car dans la fonction de xavier on a besoin de l'id_mat et le mettre ici m'évite à avoir à faire 2 requêtes
			echo $id_mat;
		} else {
			echo 'true';
		}
	} else {
		echo 'false';
	}
} else if (isset($_REQUEST['export'])) {

	$mat = new Materiel();
	$mat->t_mat['ID_MAT'] = $id_mat;
	$mat->getMat(array(),'', false);


	$filename = filter_var($mat->t_mat['MAT_NOM'], FILTER_SANITIZE_STRING) . '.csv';
	ob_start();
//    header('Content-Type: application/csv; charset=utf-8');
	header('Content-Type: application/csv; charset=iso-8859-1');
	header('Content-Disposition: attachment; filename="' . $filename . '";');
	$out = fopen('php://output', 'w');
	$originFile = parseFromSubtitleFile($id_doc, $id_mat, $mat->getFilePath(), 'return');
	foreach ($originFile as $key => $value) {
		fputcsv($out, array(utf8_decode($key), utf8_decode($value['tcout']), utf8_decode($value['text'])), ';'); //on encode en iso car la cliente ouvre les csv sous Excel (qui ne comprend que l'iso)
//        fputcsv($out, array($key, $value['tcout'], $value['text']), ';');
	}
	fclose($out);
	ob_end_flush();
} else if (isset($_REQUEST['download_file'])) {

	$mat = new Materiel();
	$mat->t_mat['ID_MAT'] = $id_mat;
	$mat->getMat(array(),'', false);


	$file = $mat->getFilePath();
	require_once(frameDir.'downloadFile.php');
}
