<?php
    $myUser=User::getInstance();

    if($myUser->Type >= kLoggedAdmin){
        header('Content-type: text/plain');
        
        $max = 1048576; // 1Mo
        if(isset($_GET['f']))
            $file=$_GET['f'];
        
        if(isset($_GET['max']))
            $max=$_GET['max'];
        
        if(!empty($file)){
            $file_log = new Logger($file, "r");
            $data = $file_log->Read($max);
            $file_log->Close();
        }
        echo $data;
        exit;
    }else{
        echo "<?= kAccesReserve ?>";
    }

?>