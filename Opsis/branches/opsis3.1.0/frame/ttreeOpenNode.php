<?php
if(!empty($_REQUEST['entity']) && $_REQUEST['entity'] == 'PERS') {
	include_once(libDir."class_cherchePers.php");
	$rl=new RecherchePersonne();
} else if(!empty($_REQUEST['entity']) && $_REQUEST['entity'] == 'FON') {
	include_once(libDir."class_chercheFonds.php");
	$rl=new RechercheFonds();
} else if(!empty($_REQUEST['entity']) && $_REQUEST['entity'] == 'VAL') {
    include_once(libDir."class_chercheVal.php");
    $rl=new RechercheVal();
} else if(!empty($_REQUEST['entity']) && $_REQUEST['entity'] == 'CAT') {
    include_once(libDir."class_chercheCat.php");
    $rl=new RechercheCat();
} else {
	include_once(libDir."class_chercheLex.php");
	$rl=new RechercheLex();
}
include_once(libDir."class_tafelTree.php");

$rl->treeParams=unserialize($_REQUEST['treeParams']);
$rl->getFather($_REQUEST['branch_id'],true,$arrNodes);
$rl->getChildren($_REQUEST['branch_id'],$arrNodes);
$tt=new tafelTree($rl,$_REQUEST['form_name']);
$tt->JSReturnFunction=$_REQUEST['jsFunction'];
$tt->JSlink=urldecode($_REQUEST['jsLink']);
$tt->arrNodes=$arrNodes;
//trace(print_r($arrNodes,true));
$tt->id_input_result=$_REQUEST['id_input_result'];
echo "[";
$tt->makeTree($_REQUEST['branch_id']);
echo "]";
?>