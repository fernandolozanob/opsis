<?php
global $db;
//header('Content-Type: text/xml;charset=utf-8');
header('Content-Type: text/xml');

$myUsr=User::getInstance();

$xml="<options>\n";
if (isset($_GET['debut'])) {
    $valeur=$_GET['debut'];
    //$debut = mb_convert_encoding($debut,"ISO-8859-1","UTF-8");
    //$debut = mb_convert_encoding($debut,"UTF-8","ISO-8859-1");
} else {
    $valeur = "";
}
//$debut = strtolower($debut);
	// VP 7/04/11 : ajout paramètre séparateur
	$prefix="";
	if(isset($_GET['sep'])){
		$pos=strrpos($valeur,$_GET['sep']);
		if($pos>0){
			$prefix=substr($valeur,0,$pos+1);
			$valeur=trim(substr($valeur,$pos+1));
		}
	}

$id_champ_appelant=""; // Type de recherche (L, V_DIF, etc...)
if(isset($_REQUEST['type'])) $id_champ_appelant = trim($_REQUEST['type']); // equivalent variable champ dans chercheIndex2
if (isset($_REQUEST["currentId"])) $currentId=$_REQUEST["currentId"];
if (isset($_REQUEST["mode"])) $mode=$_REQUEST["mode"];

if($id_champ_appelant!=""){
	$desc_champs_appelant = explode("_",$id_champ_appelant);

	//XB: a améliorer,  pour passer un nombre d'occurence a retourner
	if(!empty($desc_champs_appelant[4])){
		$limit =$desc_champs_appelant[4];
		unset($desc_champs_appelant[4]);
		$id_champ_appelant=str_replace('_'.$limit,'', $id_champ_appelant);
	}
	else{
		$limit = '10';
	}
	//PC 02/12/10 : Ajout de la gestion des droits pour l'autocomplétion
	//MS 16/04/2015 - Ajout d'un seuil customisable pour activer ou non appliqueDroits
	if ((defined('gSeuilAppliqueDroits') && $myUsr->Type < gSeuilAppliqueDroits ) ||(!defined('gSeuilAppliqueDroits') && $myUsr->Type < kLoggedDoc)) {
		require_once(libDir."class_chercheDoc.php");
	    $mySearch=new RechercheDoc();
	    $mySearch->appliqueDroits();
	    $sql_droit =  $mySearch->sqlRecherche;
	    unset($mySearch);
	}
	else 
		$sql_droit = '';
	if ($myUsr->Type <= kLoggedNorm)
		$sql_droit .= " AND t1.DOC_ACCES='1' ";
	
	switch ($desc_champs_appelant[0]){

		// Index issu de T_DOC :
		//PC 18/01/11 : limitation par DOC_ACCES limité au profils inférieurs à Doc
		case "C":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			$sql= "SELECT distinct $champ as VALEUR from t_doc t1 where $champ ".$db->like($valeur.'%')."
				AND ID_LANG=".$db->Quote($_SESSION['langue'])." $sql_droit ORDER BY $champ ASC LIMIT ".$limit;
			break;

		case "CF":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			$sql= "SELECT distinct $champ as VALEUR from t_doc t1 where ".$db->getFullTextSQL(array($valeur."*"), array($champ))."
				AND ID_LANG=".$db->Quote($_SESSION['langue'])." $sql_droit ORDER BY $champ ASC LIMIT ".$limit;
			break;

		case "FTI":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			$sql= "SELECT distinct MOT as VALEUR from t_mot where MOT ".$db->like($valeur.'%');
			if (isset($_GET['char_min'])) {
				$sql .= " and char_length(MOT) > ".intval($_GET['char_min']);
			}
			$sql.= "	 ORDER BY VALEUR ASC  LIMIT ".$limit;
			break;

		// Index issu de T_DOC_LEX et T_LEXIQUE :
		case "L":
			
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			if(isset($desc_champs_appelant[1])) $id_type_desc=$desc_champs_appelant[1];
			if(isset($desc_champs_appelant[2])) $dlex_id_role=$desc_champs_appelant[2];
			if(isset($desc_champs_appelant[3])) $lex_id_type_lex=$desc_champs_appelant[3];

			// VP 14/9/09 : suppression jointure t_doc
			if($id_type_desc || $dlex_id_role){
				$sql = "select distinct LEX_TERME as VALEUR from t_lexique l";
				$sql .=" INNER JOIN t_doc_lex dl ON (dl.ID_LEX=l.ID_LEX";
				if ($id_type_desc) $sql .= " and dl.ID_TYPE_DESC=".$db->Quote($id_type_desc);
				if ($dlex_id_role) $sql .= " and dl.DLEX_ID_ROLE=".$db->Quote($dlex_id_role);
				$sql .=") INNER JOIN t_doc t1 ON (t1.ID_DOC = dl.ID_DOC ".$sql_droit." ) ";
			}else{
				$sql = "select LEX_TERME as VALEUR from t_lexique l";
			}
			$sql .= " where l.ID_LANG=".$db->Quote($_SESSION['langue'])." and l.LEX_TERME ".$db->like($valeur.'%');
			if ($lex_id_type_lex) $sql .= " and l.LEX_ID_TYPE_LEX=".$db->Quote($lex_id_type_lex);
			if (isset($_GET['char_min'])) {
				$sql .= " and char_length(l.LEX_TERME) > ".intval($_GET['char_min']);
			}
			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;
			break;

		case "LF":
		case "LFP":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			if(isset($desc_champs_appelant[1])) $id_type_desc=$desc_champs_appelant[1];
			if(isset($desc_champs_appelant[2])) $dlex_id_role=$desc_champs_appelant[2];
			if(isset($desc_champs_appelant[3])) $lex_id_type_lex=$desc_champs_appelant[3];

			if($id_type_desc || $dlex_id_role){
				$sql = "select distinct LEX_TERME as VALEUR from t_lexique l";
				$sql .=" INNER JOIN t_doc_lex dl ON (dl.ID_LEX=l.ID_LEX";
				if ($id_type_desc) $sql .= " and dl.ID_TYPE_DESC=".$db->Quote($id_type_desc);
				if ($dlex_id_role) $sql .= " and dl.DLEX_ID_ROLE=".$db->Quote($dlex_id_role);
				$sql .=") INNER JOIN t_doc t1 ON (t1.ID_DOC = dl.ID_DOC ".$sql_droit." ) ";
			}else{
				$sql = "select LEX_TERME as VALEUR from t_lexique l";
			}
			$sql .=" where l.ID_LANG=".$db->Quote($_SESSION['langue'])." and ".$db->getFullTextSQL(array($valeur."*"), array("l.LEX_TERME"));
			if ($lex_id_type_lex) $sql .= " and l.LEX_ID_TYPE_LEX=".$db->Quote($lex_id_type_lex);
			if($desc_champs_appelant[0]=='LFP') $sql.=" AND LEX_ID_SYN=0";
			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;
			trace($sql);
			break;

		// Index issu de T_LEXIQUE
		case "LX":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+2);
			if(isset($desc_champs_appelant[1])) $lex_id_type_lex=$desc_champs_appelant[1];
			
			$sql = "select LEX_TERME as VALEUR from t_lexique l
					where l.ID_LANG=".$db->Quote($_SESSION['langue'])." and l.LEX_TERME ".$db->like($valeur.'%');

			if ($lex_id_type_lex) $sql .= " and l.LEX_ID_TYPE_LEX=".$db->Quote($lex_id_type_lex);
			if (isset($mode)) {
				switch($mode){
						case "CHILD":
							 if(!isset($currentId)) $currentId="0";
							 $sql .= "and l.LEX_ID_SYN=0 and l.LEX_ID_GEN=".intval($currentId);
						break;
				}
			}
			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;

			break;


		// Index issu de T_DOC_LEX et T_PERSONNE :
		//update VG 20/05/2011 : utilisation de concat_ws à la place de concat pour éviter une valeur finle nulle si l'une des valeur de départ l'est
		//update VG 09/06/2011 : traitement du type_lex
		case "P":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			if(isset($desc_champs_appelant[1])) $id_type_desc=$desc_champs_appelant[1];
			if(isset($desc_champs_appelant[2])) $dlex_id_role=$desc_champs_appelant[2];
			if(isset($desc_champs_appelant[3])) $id_type_lex=$desc_champs_appelant[3];
			elseif($_GET['type_lex']) $id_type_lex = htmlentities($_GET['type_lex']);

			$aId_type_lex = explode("_", $id_type_lex);
			$sqlTypesLex = "";
			foreach($aId_type_lex as $typeLex) {
				if(!empty($typeLex)) {
					$sqlTypesLex .= $db->Quote($typeLex).",";
				}
			}
			$sqlTypesLex = substr($sqlTypesLex, 0, -1);
			
			if($id_type_desc || $dlex_id_role){
				$sql = "select distinct ".$db->Concat('PERS_NOM',"' '",' PERS_PRENOM')." as VALEUR from t_personne p";
				$sql .=" INNER JOIN t_doc_lex dl ON (dl.ID_PERS=p.ID_PERS";
				if ($id_type_desc) $sql .= " and dl.ID_TYPE_DESC=".$db->Quote($id_type_desc);
				if ($dlex_id_role) $sql .= " and dl.DLEX_ID_ROLE=".$db->Quote($dlex_id_role);
				$sql .=") INNER JOIN t_doc t1 ON (t1.ID_DOC = dl.ID_DOC ".$sql_droit." ) ";
			}else{
				$sql = "select ".$db->Concat('PERS_NOM',"' '",' PERS_PRENOM')." as VALEUR from t_personne p";
			}
			$sql .= " where p.ID_LANG=".$db->Quote($_SESSION['langue'])." and p.PERS_NOM ".$db->like($valeur.'%');

			if (!empty($sqlTypesLex)) $sql .= " and p.PERS_ID_TYPE_LEX in (".$sqlTypesLex.")";

			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;

			break;

		//update VG 20/05/2011 : utilisation de concat_ws à la place de concat pour éviter une valeur finle nulle si l'une des valeur de départ l'est
		case "PF":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			if(isset($desc_champs_appelant[1])) $id_type_desc=$desc_champs_appelant[1];
			if(isset($desc_champs_appelant[2])) $dlex_id_role=$desc_champs_appelant[2];
			if(isset($desc_champs_appelant[3])) $id_type_lex=$desc_champs_appelant[3];
			elseif($_GET['type_lex']) $id_type_lex = htmlentities($_GET['type_lex']);

			$aId_type_lex = explode("_", $id_type_lex);
			$sqlTypesLex = "";
			foreach($aId_type_lex as $typeLex) {
				if(!empty($typeLex)) {
					$sqlTypesLex .= $db->Quote($typeLex).",";
				}
			}
			$sqlTypesLex = substr($sqlTypesLex, 0, -1);

			if($id_type_desc || $dlex_id_role){
				$sql = "select distinct ".$db->Concat('PERS_NOM',"' '",' PERS_PRENOM')." as VALEUR from t_personne p ";
				$sql .=" INNER JOIN t_doc_lex dl ON (dl.ID_PERS=p.ID_PERS";
				if ($id_type_desc) $sql .= " and dl.ID_TYPE_DESC=".$db->Quote($id_type_desc);
				if ($dlex_id_role) $sql .= " and dl.DLEX_ID_ROLE=".$db->Quote($dlex_id_role);
				$sql .=") INNER JOIN t_doc t1 ON (t1.ID_DOC = dl.ID_DOC ".$sql_droit." ) ";
			}else{
				$sql = "select  ".$db->Concat('PERS_NOM',"' '",' PERS_PRENOM')." as VALEUR from t_personne p ";
			}
			$sql .=" where p.ID_LANG=".$db->Quote($_SESSION['langue'])." and ".$db->getFullTextSQL(array($valeur."*"), array("PERS_NOM", "PERS_PRENOM"), "p.");
            //(p.PERS_NOM || ' ' || p.PERS_PRENOM) @@ ('".simpleQuote($valeur)."')";

			if (!empty($sqlTypesLex)) $sql .= " and p.PERS_ID_TYPE_LEX in (".$sqlTypesLex.")";

			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;

			break;
		
			// Index issu de T_PERSONNE
		//update VG 20/05/2011 : utilisation de concat_ws à la place de concat pour éviter une valeur finle nulle si l'une des valeur de départ l'est
		case "PX":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+2);
			if(isset($desc_champs_appelant[1])) $pers_id_type_lex=$desc_champs_appelant[1];
			
			
			$sql = "select ".$db->Concat('PERS_NOM',"' '",' PERS_PRENOM')." as VALEUR from t_personne p
			where p.ID_LANG=".$db->Quote($_SESSION['langue'])." and p.PERS_NOM ".$db->like($valeur.'%');
			
			// VP 2/12/09 : correcion bug $pers_id_type_lex à la place de $lex_id_type_lex
			if ($pers_id_type_lex) $sql .= " and p.PERS_ID_TYPE_LEX=".$db->Quote($pers_id_type_lex);
			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;
			
			break;
		
			
		// Index issu de T_DOC_LEX et T_PERSONNE, avec une colonne MEDIA :
		case "PMEDIA":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			if(isset($desc_champs_appelant[1])) $id_type_desc=$desc_champs_appelant[1];
			if(isset($desc_champs_appelant[2])) $dlex_id_role=$desc_champs_appelant[2];
			if(isset($desc_champs_appelant[3])) $id_type_lex=$desc_champs_appelant[3];
			elseif($_GET['type_lex']) $id_type_lex = htmlentities($_GET['type_lex']);

			$aId_type_lex = explode("_", $id_type_lex);
			$sqlTypesLex = "";
			foreach($aId_type_lex as $typeLex) {
				if(!empty($typeLex)) {
					$sqlTypesLex .= $db->Quote($typeLex).",";
				}
			}
			$sqlTypesLex = substr($sqlTypesLex, 0, -1);
			
			$sql = "select distinct ".$db->Concat('PERS_NOM',"' '", 'PERS_PRENOM',"'		--		'",'m.MEDIA')." as VALEUR from t_personne p";
			if($id_type_desc || $dlex_id_role){
				$sql .=" INNER JOIN t_doc_lex dl ON (dl.ID_PERS=p.ID_PERS";
				if ($id_type_desc) $sql .= " and dl.ID_TYPE_DESC=".$db->Quote($id_type_desc);
				if ($dlex_id_role) $sql .= " and dl.DLEX_ID_ROLE=".$db->Quote($dlex_id_role);
				$sql .=") INNER JOIN t_doc t1 ON (t1.ID_DOC = dl.ID_DOC ".$sql_droit." ) ";
				$sql .=" INNER JOIN t_media =ON m.ID_MEDIA = t1.DOC_ID_MEDIA";
			}
			$sql .="where p.ID_LANG=".$db->Quote($_SESSION['langue'])." and p.PERS_NOM ".$db->like($valeur.'%');

			if (!empty($sqlTypesLex)) $sql .= " and p.PERS_ID_TYPE_LEX in (".$sqlTypesLex.")";

			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;

			break;

		case "PERS":
			$sql = "select distinct $id_champ_appelant as VALEUR
			from t_personne
			where ID_LANG=".$db->Quote($_SESSION['langue'])." and $id_champ_appelant ".$db->like($valeur.'%')."
			ORDER BY 1 ASC  LIMIT ".$limit;
			
			break;
			
        case "US":
            $sql = "select distinct $id_champ_appelant as VALEUR
            from t_usager
            where $id_champ_appelant ".$db->like($valeur.'%')."
            ORDER BY 1 ASC  LIMIT ".$limit;
            
            break;

        case "V":
			$champ=substr($id_champ_appelant,strlen($desc_champs_appelant[0])+1);
			//if(isset($desc_champs_appelant[1])) $id_type_val=$desc_champs_appelant[1];

			$sql = "select distinct VALEUR
							from t_val
							inner join t_doc_val dv ON dv.ID_VAL = t_val.ID_VAL
							inner join t_doc t1 on t1.ID_DOC = dv.ID_DOC 
							where t_val.ID_LANG=".$db->Quote($_SESSION['langue'])." $sql_droit
							and t_val.VALEUR ".$db->like($valeur.'%');

			if ($champ) $sql .= " and t_val.VAL_ID_TYPE_VAL='$champ'";
			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;

			break;

        case "VX":
			if(isset($desc_champs_appelant[1])) $champ=$desc_champs_appelant[1];
			if(isset($desc_champs_appelant[2])) $code=$desc_champs_appelant[2];
            
			$sql = "select distinct VALEUR from t_val
            where t_val.ID_LANG=".$db->Quote($_SESSION['langue'])." 
            and t_val.VALEUR ".$db->like($valeur.'%');
            
			if ($champ) $sql .= " and t_val.VAL_ID_TYPE_VAL='$champ'";
			if ($code) $sql .= " and t_val.VAL_CODE ".$db->like($code);
			$sql .= " ORDER BY VALEUR ASC  LIMIT ".$limit;
            
			break;


	}
	trace($sql);
	$liste = $db->GetAll($sql);

	//trace(print_r($liste,true));
	foreach ($liste as $element) {
		$xml.=str_replace(" ' ","'","<option>".$prefix.str_replace('&','&amp;',$element['VALEUR'])."</option>\n");
	}
}
$xml.="</options>\n";
//trace($xml);
echo $xml;
?>
