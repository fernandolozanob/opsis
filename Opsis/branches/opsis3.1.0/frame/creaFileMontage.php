﻿<script type="text/javascript" src="<?=libUrl?>webComponents/jquery/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?=libUrl?>webComponents/opsis/fonctionsJavascript.js"></script>
<div style="border : 1px solid black; margin : 5px; padding : 20px 30px; width:400px; text-align : center; ">
<div id="msg_info"><?= kMsgMontageBegin ?></div>
<div style="text-align : center;" id="msg_progress"><?= kMsgMontageInit?></div>
<img id="img_status" src="<?=imgUrl?>/wait30trans.gif" alt="loader" />
</div>
<script>
	var $j=jQuery.noConflict();

	ids = "<?=$_GET['id']?>";
	montage_ratio = "<?= $_GET['ratio'] ?>"
	prep_url = "empty.php?xmlhttp=1&urlaction=prepareVisu&method=QT&action=visu&pattern=_vis&id="+ids+"&type=playlist&decoup=true&montage_ratio="+montage_ratio;
	var downloaded = false ; 
	filename = "<?=$_GET['name']?>";
	
	<? if (isset($_GET['noGen'])){
		echo "downloaded=false;";
		echo "getEtatMontage();";
		/*echo "triggerDownload();";*/
	}else{
		echo "triggerNewGen();";
	}
	?>
	
	var udpate_etat = window.setInterval(getEtatMontage,1000);

	
	function triggerDownload(){
		$j("#img_status").css("display","none");
		$j('#msg_info').css("display","none");
		
		if(!downloaded){
			window.location = "empty.php?urlaction=downloadMontageWrapper&name="+encodeURIComponent(filename);
		}
		window.clearInterval(udpate_etat);
		setTimeout(function(){getEtatMontage();},1000);
		downloaded = true ; 
	}
	
	function triggerNewGen(){
		$j.ajax({
			context : document,
			url : prep_url
		}).done(function(str){
			// console.log("results principale");
			// si le retour est vide, c'est que le prepareVisu a échoué, => il s'agit normalement du cas où une autre génération est en cours,
			// => on ne lance pas le downloadWrapper, on continue a interroger getEtatMontage dans l'espoir de récupérer les infos sur le traitement en cours
			if(typeof str != 'undefined' && str!='' && str != null ){
				triggerDownload();
			}else{
				$j('#msg_info').text("<?= kMsgMontageReprise ?>");
			}
		});
	}
	
	
	function getEtatMontage(){
		$j.ajax({
			url : "empty.php?urlaction=getEtatMontage"
		}).done(function(str){
			$j("#msg_progress").html(str);
			if($j("#download_link").length == 1 ){
				// console.log("call endUpdateEtat from getEtat");
				$j("#download_link").attr("href",$j("#download_link").attr("href")+"&name="+encodeURIComponent(filename));
				triggerDownload();
			}
		});
		
	}
	
	
</script>

