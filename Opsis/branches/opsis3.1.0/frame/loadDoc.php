<?php
global $db;
require_once(modelDir.'model_doc.php');
$myUsr=User::getInstance();

// header("Content-type: text/xml; http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
// error_reporting(E_ALL);
// ini_set('display_errors',1);
$id_doc=urldecode($_REQUEST['id_doc']);

if ($id_doc) {
	$myDoc = new Doc;
	$myDoc->t_doc['ID_DOC'] = $id_doc;
	$myDoc->getDocFull((isset($_GET['loadLex'])?intval($_GET['loadLex']):0),(isset($_GET['loadVal'])?intval($_GET['loadVal']):0),(isset($_GET['loadMat'])?intval($_GET['loadMat']):0),0,(isset($_GET['loadImg'])?intval($_GET['loadImg']):0),(isset($_GET['loadSeq'])?intval($_GET['loadSeq']):0),0,0,0,0,0,0,(isset($_GET['loadFes'])?intval($_GET['loadFes']):0));
	$str=$myDoc->xml_export();
}
else {
	$str="";
}

if(isset($_GET['xsl']) && !empty($_GET['xsl'])){

	function isJson($string) {
		$json = json_decode($string,true);
		if(json_last_error() != JSON_ERROR_NONE){
			return false ; 
		}else{
			return $json;
		}
	}

	if(isset($_GET['params']) && !empty($_GET['params']) && $json = isJson($_GET['params'])){
		$params = $json;
	}else{
		$params = null ; 
	}
	
	// récupération du niveau de privilèges de l'utilisateur sur ce document
	if ($myUsr->Type!=kLoggedAdmin){
		$params['id_priv_us_doc']=0;
		foreach($myUsr->Groupes as $droits){
			if($droits["ID_FONDS"]==$myDoc->t_doc["DOC_ID_FONDS"]) $params['id_priv_us_doc']=$droits["ID_PRIV"];
		}
	} else $params['id_priv_us_doc']=99;
	$xsl_file = getSiteFile("listeDir",$_GET['xsl'].".xsl");
	if(strpos($xsl_file,'index.php') === false ){
		$str=TraitementXSLT($str,$xsl_file,$params,0,$str,null);
		echo ( $str);
	}
}else{
	header("Content-Type: text/xml; charset=utf-8;");
	echo $str;
}?>
