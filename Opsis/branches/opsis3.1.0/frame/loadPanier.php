<?php
// VP 5/01/10 : ajout paramètre noHTML pour afficher ou non les entêtes HTML

if (!isset($_REQUEST['noHTML'])) header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
$myUsr=User::getInstance();

if ($myUsr->loggedIn()) {
	require_once(modelDir.'model_panier.php');
	$myPanier=new Panier;
	$folders=Panier::getFolders();
	if(defined("gShareSelGrp") && gShareSelGrp && isset($_REQUEST['getGroupes']) && $_GET['getGroupes']== "1"){
		$folders=array_merge($folders,Panier::getGroupeFolders(false,true));
		$myPage->params4XSL['groupeSel']=1;
	}
} else
{
	require_once(modelDir.'model_panier_session.php');
	$myPanier=new PanierSession;
	$folders=PanierSession::getFolders();
}



global $db;

$myPage=Page::getInstance();


if (isset($_REQUEST['action']))
	$action=urldecode($_REQUEST['action']);
else
	$action='';

if (isset($_REQUEST['id']))
	$id_panier=urldecode($_REQUEST['id']);
else
	$id_panier='';

if (isset($_REQUEST['id_lang']))
	$id_lang=urldecode($_REQUEST['id_lang']);
else
	$id_lang='';

if (is_numeric($id_panier) || $id_panier=='_session') $_SESSION['FOLDERS'][$id_panier]['MODE']=$action;

//@update VG 11/08/10 : $_GET => $_REQUEST
if (isset($_REQUEST['display']) && $_REQUEST['display']=='hierarchique') {

	//constitution du tableau pour affichage hiérarchique
	if (!isset($_REQUEST['noHTML'])) {
		echo "<html ><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
		echo "</head><body>";
		echo"<script type='text/javascript' src='".libUrl."webComponents/prototype/prototype.js'></script>
		<script type='text/javascript' src='".libUrl."webComponents/prototype/scriptaculous.js'></script>
		<script type='text/javascript' src='".libUrl."webComponents/tafelTree/Tree.js'></script>
                <script type='text/javascript' src='".libUrl."webComponents/opsis/detectEnv.js'></script>
                <script type='text/javascript' > var OW_env_user = new detectEnv(false);</script>";

		if(defined("designUrl") && is_file(designDir."css/tree.css")){
			echo "<style TYPE='text/css'>@import url('".designUrl."css/tree.css');</style>";;
		}else{
			echo "<style TYPE='text/css'>@import url('".libUrl."webComponents/tafelTree/tree.css');</style>";
		}
	}

	include_once(libDir."class_tafelTree.php");
	include_once(libDir."class_cherchePanier.php");

	$mySearch=new RecherchePanier();
	$mySearch->noAjax=true;
	$mySearch->useCookies=false;
	$mySearch->imgBase='imgfolder.gif';


	if ($_REQUEST['onlyDossier'])
	$mySearch->onlyDossier=true; //on n'affiche que des dossiers
	else $mySearch->onlyDossier=false; //on n'affiche pas que les dossiers

	if ($_REQUEST['includeRoot'])
	$mySearch->includeRoot=true; //on n'affiche la racine
	else $mySearch->includeRoot=false; //on n'affiche pas la racine

	if ($_REQUEST['includePublic'])
	$mySearch->includePublic=$_REQUEST['includePublic']; //on n'affiche que des dossiers publics
	else $mySearch->includePublic=0; //on n'affiche des dossiers	publics

	
	// VP 27/8/09 : nouveaux paramètres affichage hiérarchique des paniers (excludePanier et excludeId)
	if ($_REQUEST['excludePanier'])
		$mySearch->excludePanier=$_REQUEST['excludePanier']; //on n'affiche pas le panier
	else $mySearch->excludePanier=0; //on n'affiche panier et sélections

	if ($_REQUEST['excludeId']) $mySearch->excludeId=$_REQUEST['excludeId']; //Id Panier à exclure

	if ($_REQUEST['imgNode']) $mySearch->imgNode=$_REQUEST['imgNode']; //Id Panier à exclure


	$myTree=new TafelTree($mySearch,'form1');

	if($mySearch->noAjax) $myTree->noAjax=$mySearch->noAjax;
	if($mySearch->useCookies) $myTree->useCookies=$mySearch->useCookies;
	if($mySearch->imgBase) $myTree->imgBase=$mySearch->imgBase;
	if($mySearch->includeRoot)	$myTree->includeRoot=$mySearch->includeRoot;
	 $myTree->onDropAjax="[funcDrop,'empty.php?urlaction=dragAndDropTree.php']";
	 $myTree->behaviourDrop="'sibling'";

	//BY LD 29/01/08 => pour rester sur une xsl de gestion des dossiers et pas le panier
	// dans le cas de la gestion des dossiers publics (cnrs)
	//ce n'est pas élégant et je cherche une meilleure façon de procéder !
	if ($mySearch->includePublic) $addPrms="&xsl=dossier";

	// VP 22/01/10 : ajout paramètre JSlink et imgBaseUrl
	if ($_REQUEST['JSlink']) $myTree->JSlink=$_REQUEST['JSlink'];
	// else $myTree->JSlink="
	// 	if(typeof  dontClick === 'undefined')  {  dontClick=0;}
	// 		if( dontClick == 0 ) top.location.href='index.php?urlaction=panier".$addPrms."&id_panier=%3\$s';
	// 		else dontClick = 0;";	

	// else $myTree->JSlink="
	// 	 top.location.href='index.php?urlaction=panier".$addPrms."&id_panier=%3\$s'";
		
		else $myTree->JSlink="
		if(OW_env_user.browser== 'Explorer'){
			top.location.href='index.php?urlaction=panier".$addPrms."&id_panier=%3\$s';
		}
		else{		
			if(typeof  dontClick === 'undefined')  {  dontClick=0;}
				if( dontClick == 0 ) top.location.href='index.php?urlaction=panier".$addPrms."&id_panier=%3\$s';
				else dontClick = 0;	
		}			
";
	if ($_REQUEST['imgBaseUrl']) $myTree->imgBaseUrl=$_REQUEST['imgBaseUrl'];

	// VP 23/02/10 : limitation taille affichage des termes (paramètre termeSize)
	if ($_REQUEST['termeSize']) $myTree->termeSize=$_REQUEST['termeSize'];

	$myTree->JSReturnFunction=$jsFunction;
	$myTree->makeRoot();

	if ($_REQUEST['JSlink']) $myTree->JSlink=$_REQUEST['JSlink'];
	if (is_numeric($id_panier))$myTree->revealNode($id_panier);
	$myTree->renderOutput($id_input_result,true,$url_edit,0,true);
	if (!isset($_REQUEST['noHTML'])) echo "</body></html>";
}
elseif (isset($_REQUEST['display']) && $_REQUEST['display']=='none') {//@update VG 11/08/10 : $_GET => $_REQUEST
	// MS - 12.10.15 - On a besoin de pouvoir paramétrer le niveau user à partir duquel on inclus les PAN_PUBLIC, et ce autrement que par des variables passées en POST ou en GET. Si on se base uniquement sur des variables passées à l'appel ajax, la complexité repose sur les fonctions d'appel à loadPanier...
	// Pour l'instant cette constante n'est utilisée que pour ce cas précis, mais devrait éventuellement être étendue aux autres appels
	if(defined('gSeuilUsrIncludePanPublic') && (!$myUsr->loggedIn() || $myUsr->getTypeLog() < gSeuilUsrIncludePanPublic)){
		$folders2 = array() ;
		foreach($folders as $folder){
			if(!isset($folder['PAN_PUBLIC']) || $folder['PAN_PUBLIC'] != 1 ){
				$folders2[] = $folder;
			}
		}
		$folders = $folders2 ; 
	}

	echo "<xml>".array2xml($folders,'cart')."</xml>";

} else { //affichage en liste -> juste un div + traitement XSL
	$prec_folder = null;
	foreach ($folders as $folder) {

		if (isset($folder['PAN_PUBLIC']) && $folder['PAN_PUBLIC']==1 && !isset($_REQUEST['includePublic'])) continue; //dossier public mais on ne veut que du privé

		//MS 20/12/2012 - Report du fonctionnement excludePanier sur le cas général (et plus uniquement hierarchique)
		if($folder['PAN_ID_ETAT'] && isset($_REQUEST['excludePanier'])) continue;
		
		
		if(isset($_GET['getGroupes']) && $_GET['getGroupes']){
			if(empty($prec_folder)){
				//afficher new titre
				echo '<div class="grpFolderLabel">'.$folder['GROUPE']."</div>";
				$prec_folder = $folder['PAN_ID_GROUPE'];
			}else if($prec_folder != $folder['PAN_ID_GROUPE']){
				//afficher new titre
				echo '<div class="grpFolderLabel">'.$folder['GROUPE']."</div>";
				$prec_folder = $folder['PAN_ID_GROUPE'];
			}
		}
		
	 	$_SESSION['FOLDERS'][$folder['ID_PANIER']]['ETAT']=$folder['PAN_ID_ETAT'];
	 	 if ($folder['PAN_ID_ETAT']==1) $style='cart'; else $style='folder';
		 echo "<div class='".$style."' id='classeur".$folder['ID_PANIER']."' >"; //renommé en classeur car interférence avec aff hiérarchiqe lexique
		 echo "<a  href='?urlaction=panier&id_panier=".$folder['ID_PANIER']."'>";
		 if ($folder['PAN_ID_ETAT']==1) {
				if(file_exists(designDir."/images/".$style.".png")){
					echo "<img src='".imgUrl.$style.".png' border='0' />&nbsp;";
				}else{
					echo "<img src='".imgUrl.$style.".gif' border='0' />&nbsp;";
		 		}
				if( isset($_REQUEST['label_current_pan']) && !empty($_REQUEST['label_current_pan']) && defined($_REQUEST['label_current_pan'])){
					echo constant($_REQUEST['label_current_pan']);
				}else{
					echo kPanier;
				}
		 	}
		 elseif (!empty($folder['PAN_TITRE'])) {
		 		echo "<img src='".imgUrl.$style.".gif'  border='0' />&nbsp;";
		 		echo $folder['PAN_TITRE'];
		 		}
		 else  {
		 		echo "<img src='".imgUrl.$style.".gif' border='0' />&nbsp;";
		 		echo kSelection;
		 		}

		 echo "</a> (<div style='display:inline' id=folder_count".$folder['ID_PANIER']."'>".$folder['NB']."</div>)";
		 echo "<a href='#' id='folder_button".$folder['ID_PANIER']."' onclick='javascript:refreshFolders(\"".$folder['ID_PANIER']."\")' class='".(isset($_SESSION['FOLDERS'][$folder['ID_PANIER']]['MODE']) && $_SESSION['FOLDERS'][$folder['ID_PANIER']]['MODE']=='expand'?"collapseFolder":"expandFolder")."'><img src='".imgUrl.(isset($_SESSION['FOLDERS'][$folder['ID_PANIER']]['MODE']) && $_SESSION['FOLDERS'][$folder['ID_PANIER']]['MODE']=='expand'?"mini_arrow_down.gif":"mini_arrow_right.gif")."' /></a>";
		 echo "</div>";

		 if (isset($_SESSION['FOLDERS'][$folder['ID_PANIER']]['MODE']) && $_SESSION['FOLDERS'][$folder['ID_PANIER']]['MODE']=='expand') {

			$myPanier->t_panier['ID_PANIER']=$folder['ID_PANIER'];
			$myPanier->t_panier['ID_LANG']=$id_lang;
			$myPanier->getPanier();
			$myPanier->getDocs();
				if (!empty($myPanier->t_panier_doc))
					tabToXSL($myPanier->t_panier_doc,'t_panier_doc',getSiteFile("listeDir",'minipanier.xsl'),$myPage->params4XSL);
		 		echo "</div>";
		 }
	 }
}
//fin
unset($myPanier);

?>
