<?

require_once("conf/conf.inc.php");
require_once(libDir . "session.php");
require_once(libDir . "fonctionsGeneral.php");
require_once(libDir . "class_page.php");
require_once(libDir . "class_cherchePanier.php");
global $db;
// Les variables disponibles sont :
// - drag = JSON de la branche qui a bougé
// - drop = JSON de la branche sur laquelle on a déposé le drag
// - drag_id = id de la branche qui a bougé
// - drop_id = id de la branche sur laquelle on a déposé le drag_id
// - treedrag_id = id de l'arbre d'où est tiré la branche qui a bougé
// - treedrop_id = id de l'arbre qui accueille la branche qui a bougé
// - sibling = 0 si on le drop comme fils, 1 s'il est comme frère
// var_dump(ini_get('display_errors'));

$debug = false; // mode debug
$desactive = false; //mettez à true et aucune requete ne sera faite en base de donnée



if (isset($_POST['drop'])) {



    // Récupération des ids (élément drag et drop)
    $dragID = (int) $_POST['drag_id'];
    $dropID = (int) $_POST['drop_id'];//on peut dropper sur root1 (qui n'est pas un int), provoquera alors 1 erreur SQL


    $tab_drag = $tab_drop = $tab_FRERE_drop = $tab_FRERE_dropCLEAN = $tab_FILS_drop = $tab_FILS_dropCLEAN = array();

    $REQ_pan_parent_drag = "SELECT PAN_ID_GEN,PAN_DOSSIER FROM t_panier WHERE ID_PANIER=" . $dragID;
    $pan_parent_drag = $db->getOne($REQ_pan_parent_drag);


    $REQ_pan_parent_drop = "SELECT PAN_ID_GEN FROM t_panier WHERE ID_PANIER=" . $dropID;
    $pan_parent_drop = $db->getOne($REQ_pan_parent_drop);




    if ($debug) {
        trace('$dragID : ' . $dragID);
        trace('REQ $pan_parent_drag ' . $REQ_pan_parent_drag);
        trace('$pan_parent_drag ' . $pan_parent_drag);
        trace('$dropID : ' . $dropID);
        trace('REQ $pan_parent_drop ' . $REQ_pan_parent_drop);
        trace('$pan_parent_drop ' . $pan_parent_drop);
    }

//    if (empty($pan_parent_drop)) {
//        $pan_parent_drop = $dropID;
//        trace('$pan_parent_drop = $dropID');
//    }


    $REQ_pan_dossier_drop = "SELECT PAN_DOSSIER FROM t_panier WHERE ID_PANIER=" . $dropID;
    $pan_dossier_drop = $db->getOne($REQ_pan_dossier_drop);



    $REQ_fils_drop = 'SELECT COUNT(*) FROM t_panier where PAN_ID_GEN=' . $dropID;
    $fils_drop = $db->getOne($REQ_fils_drop);


//    if (!empty($pan_parent_drop)) { attention parfois peut etre $pan_parent_drop = 0 
    $REQ_tab_frere_drop = "SELECT ID_PANIER FROM t_panier WHERE PAN_PUBLIC=1 AND PAN_ID_GEN=" . (int)$pan_parent_drop . " ORDER BY PAN_ORDRE ASC";
    $tab_FRERE_drop = $db->getAll($REQ_tab_frere_drop);

    foreach ($tab_FRERE_drop as $idx => $row) {
        $tab_FRERE_dropCLEAN[$idx] = $row['ID_PANIER'];
    }
//    }


    if ($fils_drop > 0) {
        $REQ_tab_frere_drop = "SELECT ID_PANIER FROM t_panier WHERE PAN_PUBLIC=1 AND PAN_ID_GEN=" .(int)$dropID . " ORDER BY PAN_ORDRE ASC";
        $tab_FILS_drop = $db->getAll($REQ_tab_frere_drop);

        foreach ($tab_FILS_drop as $idx => $row) {//nb: on commence par $tab_FILS_drop[0]
            $tab_FILS_dropCLEAN[$idx] = $row['ID_PANIER'];
        }
    }




    if ($debug) {
        trace('REQ $pan_dossier_drop : ' . $REQ_pan_dossier_drop);
        trace('$pan_dossier_drop ' . $pan_dossier_drop);
        trace('REQ $fils_drop : ' . $REQ_fils_drop);
        trace('$fils_drop : ' . $fils_drop);
        trace('$tab_pan_parent_drop : ' . print_r($tab_pan_parent_drop, true));
    }






    if ($dropID == 'root1') {   //CAS 1 : Si on drop à la racine
        $REQ_action1 = 'UPDATE t_panier SET PAN_ORDRE=0,PAN_ID_GEN=0 WHERE ID_PANIER=' . $dragID;
        if ($debug) {
            trace('CAS 1 ' . $REQ_action1);
        }
        if (!$desactive) {
            $db->getOne($REQ_action1);
        }
    } elseif ($fils_drop > 0 || $pan_dossier_drop) { /* CAS 2 : Si on drop sur 1 élément qui a des enfants (ou qui est PAN_DOSSIER) , on met le drop A L'INTERIEUR */


        if ($debug) {
            trace('CAS 2 ');
        }
        //on rajoute au frères de l'élements ou aux fils de l'elements , l'élement qu'on drag



        $tab_FILS_dropCLEAN[$fils_drop] = $dragID;

        foreach ($tab_FILS_dropCLEAN as $idx => $row) {

            $REQ_action2 = 'UPDATE t_panier SET PAN_ID_GEN=' . $dropID . ' , PAN_ORDRE=' . $idx . ' WHERE   ID_PANIER=' . $row;

            if ($debug) {
                //dans tous les cas, devient les fils du drop
                trace($REQ_action2);
            }

            if (!$desactive) {

                $db->getOne($REQ_action2);
            }
        }
    } else { /* CAS  drop sur 1 élement( qui peut ne PAS avoir de parent, donc $pan_parent_drop=0)  n'ayant pas d'enfants, on met au même niveau en changeant l'ordre */

        if ($debug) {
            trace('CAS 3 ');
        }


        $tab_tri = array();
        $key_drag = array_search($dragID, $tab_FRERE_dropCLEAN);
        $key_drop = array_search($dropID, $tab_FRERE_dropCLEAN);

        foreach ($tab_FRERE_dropCLEAN as $idx => $row) {
            if ($key_drop > $key_drag) {
                if ($key_drag == $idx) {
                    $tab_tri[$key_drop] = $row;
                } elseif ($key_drag < $idx && $idx <= $key_drop) {
                    $tab_tri[$idx - 1] = $row;
                } else {
                    $tab_tri[$idx] = $row;
                }
            } else {
                if ($key_drag == $idx) {
                    $tab_tri[$key_drop] = $row;
                } elseif ($key_drop <= $idx && $idx < $key_drag) {
                    $tab_tri[$idx + 1] = $row;
                } else {
                    $tab_tri[$idx] = $row;
                }
            }
        }
        foreach ($tab_tri as $idx => $row) {


            $REQ_action3 = 'UPDATE t_panier SET PAN_ID_GEN=' . $pan_parent_drop . ' , PAN_ORDRE=' . $idx . ' WHERE   ID_PANIER=' . $row;
            if ($debug) {
                trace('$REQ_action3 ' . $REQ_action3);
            }
            if (!$desactive) {
                $db->getOne($REQ_action3);
            }
        }


        //si on drag 1 element qu'on drop sur 1 élément (dans 1 dossier par ex) à 1 autre niveau
//attention on pourrait drag un element de la racine ! (donc empty($pan_parent_drag)=TRUE)
        if (/* !empty($pan_parent_drop) && !empty($pan_parent_drag) && */ $pan_parent_drop != $pan_parent_drag) {
            $REQ_action3BIS = 'UPDATE t_panier SET PAN_ORDRE=' . count($tab_FRERE_dropCLEAN) . ',PAN_ID_GEN=' . $pan_parent_drop . ' WHERE ID_PANIER=' . $dragID;

            if ($debug) {
                trace('$REQ_action3BIS ' . $REQ_action3BIS);
            }

            if (!$desactive) {
                $db->getOne($REQ_action3BIS);
            }
        }
    }
//a etoffer avec la variable okdrop si besoin en fonction de update reussi ou non
    $okDrop = 1;
    $msg = 'ok';
    if ($okDrop) {
        echo "({";
        echo "'ok' : true,";
        echo "'msg' : '" . $msg . "'";
        echo "})";
    } else {
        echo "({";
        echo "'ok' : false,";
        echo "'msg' : '" . $msg . "'";
        echo "})";
    }
}
?>