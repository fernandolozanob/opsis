<?php
/*
require_once("../conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 
	*/

global $db;
$id_grp=urldecode($_REQUEST['id_grp']);
$id_mat=urldecode($_REQUEST['id_mat']);



if ($id_grp) {
	$sql="SELECT * FROM t_mat WHERE ";
	$sql.=" MAT_GROUPE=".$db->Quote($id_grp);
	$sql.=" ORDER BY MAT_ORDRE ASC";
	$rs=$db->GetAll($sql);
	
	$id_mat_in_list=0;
	
	if (count($rs)>0) {
		$str.="<table cellpadding='0' cellspacing='0' width='100%' ><caption style='text-align:left;font-size:16px;font-family:\"Trebuchet MS\",sans-serif;font-weight:bold;' >".kGroupe." : $id_grp</caption>";
		$str.="<th style='background-color:#BBBBBB;height:25px'>".kOrdre."</th>";
		$str.="<th style='background-color:#BBBBBB;'>".kMateriel."</th>";
		$str.="<th style='background-color:#BBBBBB;'>".kDeplacer."</th>";
		
		
		foreach ($rs as $idx=>$mat) {
			
		if ($mat['ID_MAT']==$id_mat) $id_mat_in_list++;
		$str.="<tr>";
		
		$str.="<td width='30' align='middle' valign='top' style='border-bottom:4px solid #dddddd;background-color:#666666;color:#FFFFFF;font-weight:bold;font-size:18px;text-align:center;'>".($idx+1)."</td>";
		$str.="<td style='border-bottom:4px solid #dddddd;font-size:14px' id='row§".str_replace('.','_',$mat['ID_MAT'])."'>";
			if ($mat['ID_MAT']!=$id_mat) $str.="<a href=\"javascript:saveAndGo(getSiteRoot()+'?urlaction=matSaisie&id_mat=".urlencode($mat['ID_MAT'])."')\"><img style='padding:3px;' src='".imgUrl."crayon.gif' title='".kModifier."' alt='".kModifier."' border='0'/></a><b>".$mat['ID_MAT']."</b> (".$mat['MAT_FORMAT'].")";
		$str.="<input type='hidden' name='arrOrdre[]' value='".$mat['ID_MAT']."'/>";
		$str.="</td>";
		
		$str.="<td style='border-bottom:4px solid #dddddd;background-color:#f5F5F5' align='middle' valign='top'>";
		$str.="<img src='".imgUrl."arrow_moveup.gif' title=\"".kFlecheMonter."\" alt=\"".kFlecheMonter."\" class='miniTrash' onClick=\"moveUp(document.getElementById('row§".str_replace('.','_',simpleQuote($mat['ID_MAT']))."'))\" />";
		$str.="<img src='".imgUrl."arrow_movedown.gif' title=\"".kFlecheDescendre."\" alt=\"".kFlecheDescendre."\" class='miniTrash' onClick=\"moveDown(document.getElementById('row§".str_replace('.','_',simpleQuote($mat['ID_MAT']))."'))\" />";
		$str.="</td>";
		$str.="</tr>";
		}
		
		if ($id_mat_in_list==0) { //pas d'id mat dans la liste, on l'ajoute à la fin, pour constituer un tableau où il se trouve
			$str.="<tr>";
			$str.="<td align='middle' valign='top' style='border-bottom:4px solid #dddddd;background-color:#666666;color:#FFFFFF;font-weight:bold;font-size:18px'>".($idx+2)."</td>";
			$str.="<td style='border-bottom:4px solid #dddddd' id='row§".str_replace('.','_',simpleQuote($id_mat))."'>";
			$str.="<input type='hidden' name='arrOrdre[]' value='".simpleQuote($id_mat)."'/></td>";
			$str.="<td style='border-bottom:4px solid #dddddd;background-color:#f5F5F5' valign='top'>";
			
			$str.="<img src='".imgUrl."arrow_moveup.gif' title=\"".kFlecheMonter."\" alt=\"".kFlecheMonter."\" class='miniTrash' onClick='moveUp(this.parentNode)' />";
			$str.="<img src='".imgUrl."arrow_movedown.gif' title=\"".kFlecheDescendre."\" alt=\"".kFlecheDescendre."\" class='miniTrash' onClick='moveDown(this.parentNode)' />";			
			$str.="</td>";
			$str.="</tr>";
		}
		
		$str.="</table>";
	} 
}
else {
	$str="";
}

echo $str;

?>
