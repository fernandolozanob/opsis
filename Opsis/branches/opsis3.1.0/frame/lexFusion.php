<?php
/*
require_once("../conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
*/
require_once(libDir."class_lexique");

global $db;

header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");

$idLex=$_REQUEST['idLex'];
$idLex2fusion=$_REQUEST['idLex2fusion'];
if (empty($idLex) || empty($idLex2fusion)) return false;

$myLex=new Lexique();
$myLex->t_lexique['ID_LEX']=$idLex;
$ok=$myLex->fusion($idLex2fusion);
if ($ok) echo $idLex."£_£fusion"; // Note : format de sortie compatible avec le DSP result de la page lexSaisie !

?>
