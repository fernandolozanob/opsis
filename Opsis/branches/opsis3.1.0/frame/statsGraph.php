<?php
if(isset($_SESSION["stat_sql"]["sqlGraph"]) && isset($_GET['cols'])) {
	$sql=$_SESSION["stat_sql"]["sqlGraph"];
	global $db;
	set_time_limit(0);
	
	require_once (libDir.'webComponents/jpgraph/jpgraph.php');
	require_once (libDir.'webComponents/jpgraph/jpgraph_bar.php');
	
	$result = $db->getAll($sql);
	
	$colTmp = explode("/", $_GET['cols']);
	if (count($colTmp) > 0) {
		$col = array("", "", "", "");
		$colsName=array_keys($result[0]);
		foreach ($colTmp as $k=>$c)
			if (in_array($c, $colsName)) $col[$k] = $c;
		
		if (isset($result[0]) && $colsName[0] == "PRECIS") {
			$min=$_SESSION["stats"]['stat_date_deb'];
			$max=$_SESSION["stats"]['stat_date_fin'];
			$result=zeroFillResult($result,$min,$max,strlen($result[0]["PRECIS"]));
		}
      
		from3ColumArrayToGraph($result,$col[0], $col[1], $col[2], 900, 600, true,$col[3]);
	}
}

//ex d'appel : cols=/DOC_COTE/NB/DOC_TITRE  pour afficher en x DOC_COTE, DOC_TITRE et en y NB
function from3ColumArrayToGraph($theSourceArray, $xAxe = "date", $yAxe = "critere", $valueAxe = "value", $imageWidth = 600, $imageHeight = 500, $RoundValues = TRUE, $yAxe2 = null) {
	$dates = array();
	$criteres = array();
	foreach ($theSourceArray as $line)	{
		if(isset($dates[$line[$xAxe]]))
			$dates[$line[$xAxe]]++;
		else
			$dates[$line[$xAxe]]=1;
                
                if (!empty($yAxe2)){
                  $criteres[$line[$yAxe]]=array('yAxe2'=>$line[$yAxe2]);  
                    
                }else{
                
		if(isset($criteres[$line[$yAxe]]))
			$criteres[$line[$yAxe]]++;
		else
			$criteres[$line[$yAxe]]=1;
                }
	}
        
               
	ksort($dates);
	if (!empty($xAxe)) ksort($criteres);
        	
	$theUsedArray = array();
	foreach ($criteres as $crit => $iter) $theUsedArray[$crit] = array();
	foreach ($theSourceArray as $line) {
		if(!isset($theUsedArray[$line[$yAxe]][$line[$xAxe]]))
			$theUsedArray[$line[$yAxe]][$line[$xAxe]] = $line[$valueAxe];
	}
	foreach ($criteres as $crit => $iter) ksort($theUsedArray[$crit]);
	foreach ($criteres as $crit => $iter)	{
		foreach ($dates as $date => $itery)	{
			if ( ! isset($theUsedArray[$crit][$date]) )	{
				$theUsedArray[$crit][$date] = 0;
			}
		}
	}
	
	$toBeDrawn = array();
	foreach ($criteres as $crit => $iter)	{
		ksort($theUsedArray[$crit]);
		$toBeDrawn[$crit] = array();
		foreach ($dates as $date => $itery)	{
			$toBeDrawn[$crit][] = $theUsedArray[$crit][$date];
		}	
	}
	$numberOfCriteria = count($criteres);
	$numberOfHorizontalSetOfBar = count($dates);
	
	//Creation of the Graph
	$graph = new Graph($imageWidth,$imageHeight);
	$graph->SetScale("textlin");
	
	$graph->SetBox(false);
	//$graph->SetMargin(60, 220, 10, 40); 
	//$graph->legend->SetFont(FF_ARIAL);
	$graph->SetMarginColor('white');
	$graph->SetMargin(60, 250, 40, 40); 
	
	$graph->yaxis->SetTitle(getFieldLabel($valueAxe));
	$graph->yaxis->HideTicks(false,false);
	$graph->yaxis->SetTitlemargin(50); 
	$graph->yaxis->scale->SetGrace(3);
	$graph->xaxis->SetTickLabels(array_keys($dates));
	$graph->xaxis->SetTitle(getFieldLabel($xAxe)); 
	$graph->xaxis->SetTextTickInterval((floor($numberOfHorizontalSetOfBar/8))+1,0);
	
	if (!empty($yAxe)) {
		//$graph->legend->SetColumns(1); 
		$graph->legend->SetAbsPos(670, 30,"left","top"); 
		$graph->legend->SetLayout(LEGEND_VERT);
		
		$txt = new Text();
		$txt->Set(getFieldLabel($yAxe)); 
		$txt->SetPos(680, 10, 'left', 'top'); 
		$txt->SetFont(FF_DV_SANSSERIF, FS_BOLD, 10);
		$graph->AddText($txt);
	}
	else $graph->SetMargin(60, 30, 40, 40); 
	
	$listOfBarPlot = array();
	$k = 0;
	foreach ($criteres as $crit => $iter)	{
		$fcrit= $crit;
		$barPlot = new BarPlot($toBeDrawn[$crit]);
		$listOfBarPlot[$k] = $barPlot;
		$k++;
	}
	$ybplot = new GroupBarPlot($listOfBarPlot);
	$graph->Add($ybplot);
        	
	$k = 0;
	if (!empty($yAxe))
		foreach ($criteres as $crit => $iter)	{
			$lib = trim(html_entity_decode(htmlspecialchars_decode($crit.' '.$iter['yAxe2'])));
			if (empty($lib)) $lib = getEmptyLegend($yAxe);
			if (empty($lib)) $lib = " - ";
			if (strlen($lib) > 30) $lib = substr($lib, 0, 28)."...";
			
			if (count($criteres) > 1 || !empty($lib))
				$graph->legend->add($lib, $listOfBarPlot[$k]->fill_color);
			$k++;
		}
	
	
	$graph->legend->Stroke($graph->img);
	$graph->Stroke();
}

function getEmptyLegend($value) {
	$arrLib = $_SESSION["stat_sql"]["EMPTYLEGEND"];
	return (isset($arrLib[$value])?$arrLib[$value]:"");
}

function getFieldLabel($value) {
	$arrLib = $_SESSION["stat_sql"]["LIB"];
	return (isset($arrLib[$value])?$arrLib[$value]:$value);
}
	
function zeroFillResult($result,$min=null,$max=null,$length=10){
	$col=array_keys($result[0]);
	$date_key=$col[0];
	
	$absMin = $result[count($result)-1][$date_key];
	if(empty($min)/*||$min<$absMin*/) $min=$absMin;
	if(empty($max)||$max>date("Y-m-d")) $max=date("Y-m-d");
	
	if($date_key!='V'){
		$rowBlank=$result[0];
		//if (isset($rowBlank[$col[count($col)-2]]) && count($col)-2 > 0) $rowBlank[$col[count($col)-2]]="";
		$rowBlank[$col[count($col)-1]]=0;
		switch($length){
			case 4:
				$format="Y";
				$y=1;
				$m=-1;
				$d=-1;
				break;
			case 7:
				$format="Y-m";
				$y=0;
				$m=1;
				$d=-1;
				break;
			case 10:
				$format="Y-m-d";
				$y=0;
				$m=0;
				$d=1;
				break;
		}
		$parseDate=explode("-",$min);
		$minDate=date($format,mktime(0,0,0,$parseDate["1"],$parseDate["2"],$parseDate["0"]));
		$parseDate=explode("-",$max);
		$maxDate=date($format,mktime(0,0,0,$parseDate["1"],$parseDate["2"],$parseDate["0"]));
		// Ajout date fin si non existante
		$minDateRes = $result[count($result)-1][$date_key];
		if($minDateRes!=$minDate) {
			$rowBlank[$date_key]=$minDate;
			$result[]=$rowBlank;
		}
		
		$nextDate=$maxDate;
		foreach($result as $idx=>$row){
			$date=$row[$date_key];
			$i=0;// securite pour boucle sans fin
			while($nextDate>$date && $i<1000){
				$i++;
				$rowBlank[$date_key]=$nextDate;
				$res[]=$rowBlank;
				$parseDate=explode("-",$nextDate);
				$nextDate=date($format,mktime(0,0,0,$parseDate["1"]-$m,$parseDate["2"]-$d,$parseDate["0"]-$y));
			}
			$res[]=$row;
			$parseDate=explode("-",$date);
			$nextDate=date($format,mktime(0,0,0,$parseDate["1"]-$m,$parseDate["2"]-$d,$parseDate["0"]-$y));
		}
	}else{
		$res=$result;
	}
	return $res;
}
?>
