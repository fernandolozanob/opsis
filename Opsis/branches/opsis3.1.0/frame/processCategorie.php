<?php

require_once(modelDir.'model_categorie.php');

global $db;

header('Content-type:text/plain');

$items=$_GET['items'];
$commande=$_GET['commande'];
$id_cat=$_GET['id_cat'];

if ($id_cat=='-1')
{
	$my_cat=new Categorie();
	if ($_GET['cat_nom'])
	{
		$selection_titre=$_GET['cat_nom'];
	}
	else
	{
		$cnt=count($my_cat->getCats());
		$selection_titre=kSelection.' '.$cnt;
	}
	
	$my_cat->set('CAT_NOM',$selection_titre);
	$my_cat->set('ID_LANG',$_SESSION['langue']);
	$my_cat->save();
	$data_cat=$my_cat->getValues();
	$id_cat=$data_cat['ID_CAT'];
	
	/*$myCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$pan_titre,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID, "PAN_DOSSIER" => $pan_dossier);
	$myCart->createPanier();*/
}


$arr_docs=array_filter(explode('$',$items));

if (!empty($_GET['remove_docs']) && $_GET['remove_docs']==1)
{
	$type_cat=$db->Execute('SELECT * FROM t_categorie WHERE ID_CAT='.intval($id_cat))->GetRows();
	$type_cat=$type_cat[0]['CAT_ID_TYPE_CAT'];
	
	foreach($arr_docs as $id_doc)
	{
		$res=$db->Execute('SELECT COUNT(*) AS compte 
						FROM t_categorie 
						LEFT JOIN t_doc_cat ON t_categorie.ID_CAT=t_doc_cat.ID_CAT
						WHERE t_categorie.CAT_ID_TYPE_CAT='.$db->Quote($type_cat).' AND t_doc_cat.ID_DOC='.intval($id_doc))->GetRows();
		
		//on recupere la categorie ou est actuellement enregistre le document
		$id_cat_current=$db->Execute('SELECT t_categorie.ID_CAT 
						FROM t_categorie 
						LEFT JOIN t_doc_cat ON t_categorie.ID_CAT=t_doc_cat.ID_CAT
						WHERE t_categorie.CAT_ID_TYPE_CAT='.$db->Quote($type_cat).' AND t_doc_cat.ID_DOC='.intval($id_doc))->GetRows();
		
		if ($res[0]['compte']>0)
			$db->Execute('DELETE FROM t_doc_cat WHERE ID_CAT='.intval($id_cat_current[0]['ID_CAT']).' AND ID_DOC='.intval($id_doc));
	}
}

$my_cat=new Categorie($id_cat);
$saveCat = false;

switch($commande) //ajout d'une ligne
{ 
	case 'add': //ajout
		$my_cat->addDoc($arr_docs);
		$saveCat = true;
		break;
									 
	 case 'export':
		 $my_cat->getCategorie();
									 
		 header("http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
		 echo "<xml>";
		 echo $my_cat->xml_export();
		 echo "</xml>";
		 break;
									 
	 case 'remove':
		$my_cat->removeDoc($arr_docs);
		
		if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true)) {
			require_once(modelDir.'model_doc.php');
			
			if (!is_array($arr_docs)) {
				$tmp_id_doc=$arr_docs;
				$arr_docs=array($tmp_id_doc);
			}
		
			foreach ($arr_docs as $id) {
				$newDoc = new Doc();
				$newDoc->t_doc['ID_DOC'] = $id;
				$newDoc->saveSolr();
			}
		}
		$saveCat = true;
		break;
}
if($saveCat){
	$my_cat->save();
}
?>
