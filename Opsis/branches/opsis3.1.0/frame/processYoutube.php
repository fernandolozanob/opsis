<?
	require_once("conf/conf.inc.php");
	require_once(libDir."session.php");
	require_once(libDir."fonctionsGeneral.php"); 	
	require_once(libDir."class_page.php");
	require_once(libDir."class_youtube.php");

	global $db;

	$youtube = new Youtube() ;	
	
	
	if(!isset($_GET['action'])){
		echo "Erreur : aucune action";
		exit ; 
	}else{
		$action = $_GET['action'];
	}
	
	if(isset($_GET['etape_yt']) && !empty($_GET['etape_yt'])){
		$youtube->getLogParamsFromEtape(intval($_GET['etape_yt']));
	}else{
		echo "Erreur pas d'etape fournie";
		exit ; 
	}
	
	if(isset($_GET['type_v_pl_yt']) && !empty($_GET['type_v_pl_yt'])){
		$youtube->setTypeValPl($_GET['type_v_pl_yt']);
	}else{
		// echo "Erreur pas de type de valeur défini fournie";
		// exit ; 
	}
	
	
	
	$ok = $youtube->connectYt();
	
	
	
	if ($ok ) {	
		switch($action){
			case "refresh_val_to_pl" : 
				set_time_limit(0);
				if(isset($_GET['id_val'])){
					$retour = $youtube->export_val_to_playlist_yt($_GET['id_val']);
				}else{
					$retour = $youtube->export_val_to_playlist_yt();
				}
				
				if($retour){
					echo "true";
				}else{
					echo "false";
				}
				
			break ; 
			case "refresh_pl_to_val": 
				set_time_limit(0);
				$retour = $youtube->import_playlist_yt_to_val();
				if($retour){
					echo "true";
				}else{
					echo "false";
				}
			break ; 
			case "import_pl_links_to_val": 
				set_time_limit(0);
				$retour = $youtube->import_playlist_yt_to_val();
				$retour2 = $youtube->import_links_playlist_video();
				if($retour && $retour2){
					echo "true";
				}else{
					echo "false";
				}
			break ; 
			case "update_yt_video":
				if(!isset($_GET['id_doc']) || empty($_GET['id_doc'])){
					echo "Erreur update_video no id_doc";
					exit ; 
				}

				$retour = $youtube->update_video_yt($_GET['id_doc'],1,1);
				if($retour){
					echo "true";
				}else{
					echo "false";
				}
			break ; 	
			
			case "get_status_yt" : 
				if(!isset($_GET['id_doc']) || empty($_GET['id_doc'])){
					echo "Erreur update_video no id_doc";
					exit ; 
				}
				
				if(isset($_GET['type_v_status_yt']) && !empty($_GET['type_v_status_yt'])){
					$youtube->setTypeValStatus($_GET['type_v_status_yt']);
				}else{
					// echo "Erreur pas de type de valeur défini fournie";
					// exit ; 
				}
				
				
				$retour = $youtube->get_status_video_xml($_GET['id_doc']);
				if($retour){
					echo "true";
				}else{
					echo "false";
				}
			break ;
		}
	}
?>
