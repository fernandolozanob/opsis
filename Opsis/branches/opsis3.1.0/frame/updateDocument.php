<?php
/**
 * Batch process de documents, à partir d'une liste d'ID, d'un résultat de recherche,
 * ou d'un répertoire de travail.
 */
 global $db;
 require_once(modelDir.'model_doc.php');

 $action=$_GET['commande'];
 
 if ($_GET['id_doc']) $arrIds=@split(',',$_GET['id_doc']); //Par get id_doc


 if ($_GET['search'] ) { //by LD 15/12, uniquement si on vient de résultat de rech.
 	 if ( $_SESSION[$_GET['search']]["sql"]) {
 	 	
 	 	$sql=$_SESSION[$_GET['search']]["sql"];
 	 	$sql.=$_SESSION[$_GET['search']]["refine"];
 	 	$sql.=$_SESSION[$_GET['search']]["tri"];
 	 	
		
 	 	if (isset($_GET['rang'])) {
			if(defined("useSolr") && useSolr && strpos($sql,"solr_params")!== false ){
				$solr_opts=unserialize(kSolrOptions);
	
				if (!empty($_SESSION['DB']))
				{
					$solr_opts['path']=$_SESSION['DB'];
				}
				
				$solr_client=new SolrClient($solr_opts);
				$sql_obj=new SolrQuery();
				$sql_obj->unserialize($sql);
				$sql_obj->setStart($_GET['rang']);
				$sql_obj->setRows(1);
				
				$result_solr=$solr_client->query($sql_obj);
				$result=array();
				foreach ($result_solr->getResponse()->response->docs as $doc_solr)
				{
					$tmp=array();
					$tmp['ID_DOC']=intval($doc_solr->id_doc);
					$tmp['ID_LANG']=$doc_solr->id_lang;
					$result[]=$tmp;
				}
				$arrRes = $result;
			}else if (defined("useSinequa") && useSinequa && $_GET['search']=='recherche_DOC') {
				//Mode sinequa && recherche doc --> utilisation sinequa
				
				require_once(libDir."sinequa/fonctions.inc");
				require_once(libDir."sinequa/Intuition.inc");    
	 	 		$sql.=" SKIP ".((int)$_GET['rang'])." COUNT 1";
	 	 		$iSession=new iSession;  		
				$prms= array ('host' => sinequa_host,
								'port' => sinequa_port,
								'read_only' => 1,							 		       
								'charset' => in_UTF8,
								'page_size' => 100000,	
								'max_answers_count' => 100000,	       
								'default-language' => $_SESSION['langue']
						);
				$link = $iSession->in_connect($prms);	           	
				$iQuery=$iSession->in_query($sql);	
				$rows=$iQuery->the_available_tuples_count;	
			    for($i=0;$i<$rows;$i++) $arrRes[$i]=$iQuery->in_fetch_array();
			    if(isset($iQuery))$iQuery->close();  
			    if(isset($iSession))$iSession->close();
 	 		
 	 		} else { // sinon : pas sinequa ou on vient depuis une autre source (panier_doc par ex);
	 	 		$sql.=" LIMIT 1 OFFSET ".intval($_GET['rang']);
	 	 		$arrRes=$db->GetAll($sql);
 	 		}
 	 	} else {$arrRes=$db->GetAll($sql);}

 	 //	if (empty($arrRes)) exit;	
 	 	foreach ($arrRes as $doc) $arrIds[]=$doc['ID_DOC']?$doc['ID_DOC']:$doc['id_doc'];
 	 	unset($arrRes);
 	 }
 }
 
 if ($_GET['id_panier']) {
 	require(modelDir.'model_panier.php');
 	$myPanier=new Panier;
 	$myPanier->t_panier['ID_PANIER']=intval($_GET['id_panier']);
 	$myPanier->getPanierDoc();
 	foreach ($myPanier->t_panier_doc as $doc) $arrIds[]=$doc['ID_DOC'];
 	unset($myPanier); 	
 }

if (empty($arrIds)) exit;

//Action

switch ($action) {

	case 'delete':
	case 'suppr':
	 	 	$docDeleted=0;
	 	 	foreach ($arrIds as $_doc) {
	 	 		$myDoc=new Doc();
	 	 		$myDoc->t_doc['ID_DOC']=$_doc;
	 	 		if ($myDoc->delete()) $docDeleted++; //NOTE : pour le moment, pas de gestion des versions
	 	 		unset($myDoc);
	 	 	}
	 	 	if ($docDeleted>0) $msg=$docDeleted." ".kSuccesSupprDoc;
	 	 	else $msg=kErrorSupprDoc;
	break;

	case 'getDoc': //exporte le document, (le premier si on a passé une liste);
	case 'getPanier_doc':
		// VP 1/04/10 : export lexique aussi
		$myDoc=new Doc();
		$myDoc->t_doc['ID_DOC']=$arrIds[0];
		$myDoc->getDoc();
		$myDoc->getValeurs(); 
		$myDoc->getLexique();
		$myDoc->getPersonnes();
		$myDoc->getVersions();
		$out=$myDoc->xml_export();
	break;

}

  
 //Message dans la fenêtre principale.
 //Refresh fenêtre principale.

if ($msg) {
	header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">"); 
	echo "<msg_out>".$msg."</msg_out>"; 
}

if ($out) {

	header("http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
	header('Content-type: text/xml');
	echo $out;
}
?>