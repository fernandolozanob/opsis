<?php

// Pour masquer tous les messages (ex : Deprecated...)
// error_reporting(E_ALL);
error_reporting(0);

header('Content-Type: text/xml');

switch ($_GET['method']) {

    case 'ORAO' :
        require_once(libDir . 'class_visualisationORAO.php');
        $myVis = new VisualisationORAO($_GET['type']);
        break;

    case 'Flash' :
        require_once(libDir . 'class_visualisationFlash.php');
        $myVis = new VisualisationFlash($_GET['type']);
        break;

    case "limelight" :
        if (defined("kCheminLimelight") && kCheminLimelight) {
            require_once(libDir . 'class_visualisationQT.php');
            $_GET["decoup"] = 'false';
            $myVis = new VisualisationQT($_GET['type'], 'limelight');
        }
        break;
    case 'QT' :
        require_once(libDir . 'class_visualisationQT.php');
        $myVis = new VisualisationQT($_GET['type']);
        break;

    case 'Diaporama':
        require_once(libDir . 'class_visualisationDiaporama.php');
        if (isset($_GET['precis']) && !empty($_GET['precis']) && $_GET['precis'] == '1') {
            $diapo_precis = true;
        } else {
            $diapo_precis = false;
        }

        $myVis = new VisualisationDiaporama($_GET['type'], $diapo_precis);
        break;
}

if (isset($_GET['id_lang']))// GT pour prendre en compte la langue de la video
    $langue = $_GET['id_lang'];
else
    $langue = $_SESSION['langue'];

if (isset($_GET["decoup"]) && strtolower($_GET["decoup"]) == "true") {
    $decoup = true;
} else {
    $decoup = false;
}

if (isset($_GET["getDocSeq"]) && !empty($_GET["getDocSeq"])) {
    $myVis->getDocSeq = $_GET['getDocSeq'];
}

if(isset($_GET['montage_xml'])){
	try{
		$myVis->montage_xml = simplexml_load_string($_GET['montage_xml']);
	}catch(Exception $e){
		trace("prepareVisu crash loading montage_xml");
	}
}

//Ajout d'un tri si existe, modification liée à maeb
if (isset($_GET['tri'])){
    $myVis->tri = $_GET['tri'];
}

$ok = $myVis->prepareVisu($_GET['id'], $langue, $decoup, $_GET['h'], (isset($_REQUEST['id_mat'])?(int) $_REQUEST['id_mat']:null));

// VP 6/10/11 : ajout variable session videoUrl
$_SESSION['videoUrl'] = $myVis->mediaUrl;
$_SESSION['videoPath'] = $myVis->mediaPath;
$_SESSION['videoPlayer'] = "opsis"; // authorisation visionnage via Opsis

$_SESSION['xml_vis'] = $myVis->xml_export(0, 0, "", false);

if ($ok)
    print $_SESSION['xml_vis'];
?>
