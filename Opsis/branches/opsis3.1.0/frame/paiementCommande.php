<?php
/**
 * Page de retour automatique de Paybox. Cette page enregistre la transaction en cas de succès
 * et passe la commande à l'état payé.
 * NOTE : Cette page est volontairement muette (pas de sortie écran)
 */
    require_once(modelDir.'model_panier.php');
	require_once(modelDir.'model_usager.php');
	  
	$idTrans=$_GET['trans']; //id transaction PAYBOX
	
	$myPanier=new Panier();
	//les params sont : ID= ID de commande, OUT = code retour de PAYBOX, ID_TRANS = id de transaction fourni par Paybox
	
	if ($_GET['auto']) { //paiement autorisé
	
			$myPanier->t_panier['ID_PANIER']=$_REQUEST["id"];
			$myPanier->getPanier();	
			$myPanier->getDocs();	
			$myPanier->met_a_jour_lignes('PDOC_ID_ETAT',3); //passage à l'état PAYE et sauvegarde
			$myPanier->t_panier['PAN_ID_ETAT']=3;
			$myPanier->t_panier['PAN_ID_TRANS']=$idTrans;
			$myPanier->t_panier['PAN_DATE_COM']=date("Y-m-d H:i:s");	
			$myPanier->getTarifs();			
			$myPanier->save();
			$myPanier->saveAllPanierDoc();
			
			$myUser=new Usager();
			$myUser->t_usager['ID_USAGER']=$myPanier->t_panier['PAN_ID_USAGER']; //get usager depuis la BDD
			$myUser->getUsager();
			if (!empty($myUser->t_usager['US_SOC_MAIL'])) {
					$myPanier->User->Mail=$myUser->t_usager['US_SOC_MAIL'];
					$myPanier->User->UserID=$myUser->t_usager['ID_USAGER'];
			}
			unset($myUser);				
			
			trace(print_r($myPanier,true));
			
			
			if(!empty($myPanier->User->Mail))
				$ok=$myPanier->send_mail_usager(1); //5/2/08 param viré $myPanier->User->UserID car pas présent coté panier. pb synhro fichiers ?
			trace('ENVOI MAIL :'.$myPanier->User->Mail.' RESULTAT:'.$ok);
	}
	unset($myPanier);
	trace('URL RETOUR PAIEMENT= '.$_SERVER['QUERY_STRING']); //on trace l'url de retour complet
?>