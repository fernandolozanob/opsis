<?
global $db;

if (isset($_GET['action']) && isset($_GET['id_doc']) && isset($_GET['comment'])) {
	$action = $_GET['action'];
	$id_doc = $_GET['id_doc'];
    $id_comment = $_GET['id_comment'];
    $comm = $_GET['comment'];
	if (isset($_GET['autor'])) {
		$autor =$_GET['autor'];
 	}
	$dc_note = $_GET['total']; // Note totale
	$doc_note = $_GET['doc_note']; //variable de calcul total de notes
	
	//trace(print_r($_GET, true));
	if (isset($action) && $action = "add") {
		require_once(modelDir.'model_doc.php');
		$user_id = 0;
		$myUsr=User::getInstance();
		if ($myUsr->UserID) {
			$user_id = $myUsr->UserID;
			$autor = $myUsr->Prenom . " " . $myUsr->Nom;
		}

		$myDoc = new Doc();
		$myDoc->t_doc['ID_DOC'] = $id_doc;
		$date=str_replace("'","",$db->DBTimeStamp(time()));


	// NB 15 04 2015 processComment pour Système de vote ( Filmsandcompanies / DGA )

		if ($dc_note != '' && $doc_note != '' && isset($_GET['mainCrit'])) { //doc_note est la variable calculé dans le site pour stockage dans t_doc
			$updateDocNote = 0;
			if (isset($_GET['updateDocNote'])){$updateDocNote = str_replace("'","",$db->Quote($_GET['updateDocNote']));}

			$comment_val = array();
			foreach ($_GET['mainCrit'] as $key => $crit) {
				$idVal= str_replace('dcv_note_','',$crit[0]);
				$critIntVal = $crit[1];
				array_push($comment_val,array('ID_VAL'=>$idVal,'DCV_NOTE'=>$critIntVal));
			}

			if(!empty($_GET['optionalCrit'])) {
				foreach ($_GET['optionalCrit'] as $key => $crit) {
					$idVal= str_replace('dcv_note_','',$crit[0]);
					$critIntVal = $crit[1];
					array_push($comment_val,array('ID_VAL'=>$idVal,'DCV_NOTE'=>$critIntVal));
				}
			}

			$myDoc->getDocComments(true);

			//si le juré a déjà noté ce film, mettre a jour la notation (mettre a jour les notes, la remarque et la date de modif si id_comment alreadyexist)
			if(!empty($myDoc->t_doc_comment)) {
				foreach ($myDoc->t_doc_comment as $key => $value) {
					if ($updateDocNote && $value['DC_ID_USAGER_CREA'] == $user_id) {
						$myDoc->t_doc_comment[$key]['DC_COMMENT'] = $comm;// apréciation du juré
						$myDoc->t_doc_comment[$key]['DC_NOTE'] = $dc_note;
						$myDoc->t_doc_comment[$key]['DC_DATE_MOD'] = $date;
						$myDoc->t_doc_comment[$key]['DOC_COMMENTS_VAL']= $comment_val;
						break;
					} elseif ($updateDocNote==0) {
						$comment = array('ID_DOC' => $id_doc,
										'DC_AUTEUR' => $autor,
										'DC_COMMENT' => $comm,	// apréciation du juré
										'DC_NOTE' => $dc_note,
										'DC_DATE_CREA' => $date,
										'DC_DATE_MOD' => $date,
										'DC_ID_USAGER_CREA' => $user_id,
										'DOC_COMMENTS_VAL' => $comment_val);
						$myDoc->t_doc_comment[] = $comment;
						break;
					}
				}
			} else {
				$comment = array('ID_DOC' => $id_doc,
								'DC_AUTEUR' => $autor,
								'DC_COMMENT' => $comm,// apréciation du juré
								'DC_NOTE' => $dc_note,
								'DC_DATE_CREA' => $date,
								'DC_DATE_MOD' => $date,
								'DC_ID_USAGER_CREA' => $user_id,
								'DOC_COMMENTS_VAL' => $comment_val);
				$myDoc->t_doc_comment[] = $comment;
			}
			$myDoc->saveDocComments(true,$doc_note, $id_comment, $_GET['updateDocNote']);
			// Fin du système de vote
		} else {
			// Cas classique pour les commentaires classiques
            $comment_val = array();
            foreach ($_GET['comments_val'] as $key => $crit) {
                $idVal= str_replace('dcv_note_','',$crit[0]);
                $critIntVal = $crit[1];
                array_push($comment_val,array('ID_VAL'=>$idVal,'DCV_NOTE'=>$critIntVal));
            }
            $myDoc->getDocComments(true);
			$comment = array("ID_DOC" => $id_doc,
                                'DC_AUTEUR' => $autor,
								'DC_COMMENT' => $comm,
								'DC_DATE_CREA' => $date,
								'DC_DATE_MOD' => $date,
								'DC_ID_USAGER_CREA' => str_replace("'","",$user_id),
								'DC_ID_USAGER_MOD' => str_replace("'","",$user_id),
								'DC_NOTE' => $dc_note,
								'DOC_COMMENTS_VAL' => $comment_val
                             );
            $found_dc = false;
            if(!empty($id_comment)){
                $comment['ID_COMMENT']=$id_comment;
                foreach($myDoc->t_doc_comment as &$dc){
                    if($dc['ID_COMMENT']==$id_comment){
                        $dc=$comment;
                        $found_dc=true;
                    }
                }
            }
            if(!$found_dc) {
                $myDoc->t_doc_comment[] = $comment;
            }
            //trace(print_r($myDoc->t_doc_comment,true));
			$ok=$myDoc->saveDocComments(true, $doc_note, $id_comment, $_GET['updateDocNote']);
			if ($ok) {
				?>
				<fieldset id="<?= $id_comment ?>" class="ui-widget ui-widget-content ui-corner-all" style="margin-left:10px;">
					<legend><?=$autor;?> : <?=$date;?> <span style="color:red;font-weight:normal">(<?=kAttenteValidation;?>)</span></legend>
					<span><?=$comm;?></span>
				</fieldset>
				<?
			}
		}
	}
}
?>
