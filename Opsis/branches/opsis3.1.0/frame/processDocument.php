<?php

require_once(libDir."class_user.php");
require_once(modelDir.'model_materiel.php');
require_once(modelDir.'model_doc.php');

$myMat= new Materiel();
if (empty($_REQUEST['id'])) return false;
$myMat->t_mat['ID_MAT']=$_REQUEST['id'];
$commande=htmlspecialchars(urldecode($_REQUEST['commande']));
$myMat->getMat();
$myMat->getValeurs();
$myMat->getDocs();

global $db;


switch($commande) {
case 'add': //ajout
		// pas utilisé	pour le moment
break;
case 'export':
			header("http-equiv=\"Content-type\" content=\"text/xml; charset=utf-8\">");
			echo "<xml>";
			echo $myMat->xml_export();
			echo "</xml>";
break;
case 'view':
			header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
			$xml=$myMat->xml_export();
 			$xml = "<?xml version='1.0' encoding='UTF-8' ?".">\n<select>".$xml."</select>";
			//trace($xml);
			$arrProfil=array("id_lang"=>$_SESSION['langue']);
			$html=TraitementXSLT($xml,getSiteFile("listeDir",$_GET['xsl'].'.xsl'),$arrProfil,0,$xml);
			echo $html;
			
break;

case 'saveDocuments' :
			echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8 \" /> </head><body>";
			//debug($_REQUEST,'green',true);
			foreach ($_POST['ID_DOC_MAT'] as $idx=>$idDM) { //parcours du tableau des extraits, infos de base
				$myLine=array(	'ID_DOC_MAT'			=> $idDM,
								'ID_MAT'		=> $myMat->t_mat['ID_MAT'],
								'ID_DOC'		=> $_POST['ID_DOC'][$idx]													
				);
				foreach ($_POST as $_fld=>$_val) {
					if (is_array($_val) && strpos($_fld,'DMAT_')===0) $myLine[$_fld]=$_val[$idx];
				
					if (is_array($_val) && $_fld=='t_doc') { 
						foreach ($_val as $_i=>$_doc) {
							$myLine[$_fld][$_i]=$_doc[$idx];
						}
					}
					
					if (is_array($_val) && $_fld=='t_doc_mat_val') { //t_doc_mat_val
						foreach ($_val as $_i=>$_dmVal) {
							foreach ($_dmVal as $col=>$_v) $myLine[$_fld][$_i][$col]=$_v[$idx];
						}
					}					
				}
				
				if ($_POST['ligne_action'][$idx]=='edit' && !empty($_POST['t_doc']['DOC_TITRE'][$idx]) ) { //mode edit + titre => save
					$docMat[]=$myLine;
				} elseif ($_POST['ligne_action'][$idx]=='edit' && !empty($idDM) ) { //mode edit sans titre => update
					$docMat[]=$myLine;
				} elseif (!empty($idDM) && $_POST['ligne_action'][$idx]=='suppr')  { //mode suppr + n° ligne => delete
					$docMat2Del[]=$myLine;
				}
			}
			
			//debug($docMat,'lightblue',true);
			//debug($docMat2Del,'pink',true);
			
			if (!empty($docMat)) {$myMat->t_doc_mat=$docMat;$myMat->saveDocMat();}
			if (!empty($docMat2Del)) {
				$myMat->t_doc_mat=$docMat2Del;
				$myMat->deleteDocMat();
				foreach($docMat2Del as $dm) {
					Doc::checkVignette($dm['ID_DOC'], $docMat2Del);
				}
			}
			
			//On génère la sortie de la fonction d'abord en encapsulant le message dans un DIV
			//Puis on lance un script dans la frame appelante.
			echo "<div id='output'>";
			if (trim($myMat->error_msg,'<br/>')=='') echo kSuccesDocSauve; else echo $myMat->error_msg;
			echo "</div>";
			echo "<script>if(parent.myPanel) parent.myPanel.showResultInParent(document.getElementById('output'));</script>";
			echo "</body></html>";
break;

}

?>