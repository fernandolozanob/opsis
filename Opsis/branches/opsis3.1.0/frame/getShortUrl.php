<?
require_once(modelDir.'model_shortUrl.php');
global $db;
$myPage=Page::getInstance();
$myUser=User::getInstance();
//renvoie url longue à partir de l'url court


//extraire l'url courte 
 $path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
 $url=basename($path);
// $pos=strpos(kCheminHttp,"/",8);
// $var = substr(kCheminHttp,$pos);	
// $suffixe = explode($var,$path); 	  
// $param = explode("/", $suffixe[1]);
// $url=$param[1];

	if(!empty($url)){
		$shortUrl = new ShortUrl();

		try {
			$url = $shortUrl->shortCodeToUrl($url);
			 header("Location: " . $url);
			exit;
		}
		catch (Exception $e) {
			header("HTTP/1.0 404 Not Found");
			exit;
		}
	}

 ?>
