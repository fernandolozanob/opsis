<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link type="text/css" href="<?=libUrl?>webComponents/tinymce/skins/lightgray/skin.min.css" rel="stylesheet" />	
<style type="text/css">

body .mce-abs-layout-item, .mce-abs-end {
    position: relative;
}
.tab
{
	border: 1px solid #CECECE;
}

 .warning {
	border: 1px solid;
	margin: 10px 0px;
	padding:15px 10px 15px 50px;
	background-repeat: no-repeat;
	background-position: 10px center;
	color: #9F6000;
	background-color: #FEEFB3;
	background-image: url('<?=libUrl?>webComponents/tinymce/skins/lightgray/img/warning.png');
}
.entete {

	background-color: #F0F0F0;
	background-image: linear-gradient(to bottom, #FDFDFD, #DDDDDD);
	background-repeat: repeat-x;
	border-bottom: 1px solid #9E9E9E;
	overflow: hidden;
}


 



<!--
        #uploadFrame { display: none; }
//-->
</style>

</head>

<script type='text/javascript'>

//quand on choisit une image on envoie son url ?a page parente et on ferme le pop up
function submit_url(URL) {
URL=URL.replace(/ /g,"");
window.parent.document.getElementById('<?php echo trim($_REQUEST['field']); ?>').value = URL;
//ferme le pop up !!! ! ! ! !!
top.tinymce.activeEditor.windowManager.close();
}

function delete_file(URL,field) {
			var answer = confirm("<?=kUploadEffacerFichier?>");
			if (answer){
		  	document.location.href='empty.php?urlaction=uploadTinymce&deletefile='+URL+'&field='+field+'&type=<?php echo $_REQUEST['type'];?>&currentFolder=<?php echo $_REQUEST['currentFolder'];?>';
			} 
		}
		
function delete_rep(URL,field) {
			var answer = confirm("<?=kUploadEffacerDossier?>");
			if (answer){
		  	document.location.href='empty.php?urlaction=uploadTinymce&deletefolder='+URL+'&field='+field+'&type=<?php echo $_REQUEST['type'];?>&currentFolder=<?php echo $_REQUEST['currentFolder'];?>';
			} 
		}
		
function create_folder() {
			var name=prompt("<?=kUploadCreeDossier?>","<?=kUploadCreeDossier?>");
			if (name!=null && name!=""){
				document.location.href='empty.php?urlaction=uploadTinymce&createfolder='+name+'&field=<?php echo $_REQUEST['field']; ?>&currentFolder=<?php echo $_REQUEST['currentFolder'];?>&type=<?php echo $_REQUEST['type'];?>';
			}
		}		
</script>


<?php



 $error = '';
$filename = NULL;
$path_upload=kTinymceUploadPath.'/';
$relative_path=kTinymceUploadPathRelativ;
$maxsize=2097152;

//extension autoris? selon le type
if($_REQUEST['type']=='image')
$allowed_types = array("jpeg","png","gif","bmp","jpg");
if($_REQUEST['type']=='media')
$allowed_types = array("avi","mov","mp4","flv");
if($_REQUEST['type']=='file')
$allowed_types = array("txt","rtf","pdf","doc","docx","xls","xlsx","ppt","pptx");

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
///////////////// UPLOAD /////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

if(isset($_FILES['uploadFile']) && $_FILES['uploadFile']['error'] == 0) {
//si le repertoire change
	if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '')
		{
		$targetpath = $path_upload.$_REQUEST['currentFolder'];
		}
	else{
		 $targetpath =$path_upload;
	}
	//taille
	if ($_FILES['uploadFile']['size'] > $maxsize) $error = kFichierTropGros;
	$filename   = $_FILES['uploadFile']['name'];
	$filename=str_replace(' ','',$filename);
	//extension
	$extension_upload = strtolower(  substr(  strrchr($_FILES['uploadFile']['name'], '.')  ,1)  );
	if (!in_array($extension_upload,$allowed_types)) $error =kErrorExtensionNonAutorisee;
	//renommons le fichier
	 //$name_file = md5( uniqid('H', 5) ).'.'.$extension_upload;

	
	if($error == ''){
	if(move_uploaded_file($_FILES['uploadFile']['tmp_name'], $targetpath.$filename)) {
		 $error = kUploadMaterielSucces; 
	  } 
	else {
		 $error = kUploadEchec; 
		} 
	}
		
//redirection apres upload
if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '') {		
?>
<script>
document.location.href='empty.php?urlaction=uploadTinymce&currentFolder&currentFolder=<?php echo $_REQUEST['currentFolder']; ?> &field=<?php echo $_REQUEST['field']; ?>&type=<?php echo $_REQUEST['type']; ?>&error=<?php echo $error; ?>';
</script>
<?php				
}
else 
?>
<script>
document.location.href='empty.php?urlaction=uploadTinymce&field=<?php echo $_REQUEST['field']; ?>&type=<?php echo $_REQUEST['type']; ?>&error=<?php echo $error; ?>';
</script>
<?php
			} 
			
//fonction pour supprimer un repertoire et son contenu
 function sup_repertoire($chemin) {

        // v?fie si le nom du repertoire contient "/" ?a fin
        if ($chemin[strlen($chemin)-1] != '/') // place le pointeur en fin d'url
           { $chemin .= '/'; } // rajoute '/'

        if (is_dir($chemin)) {
             $sq = opendir($chemin); // lecture
             while ($f = readdir($sq)) {
             if ($f != '.' && $f != '..')
             {
             $fichier = $chemin.$f; // chemin fichier
             if (is_dir($fichier))
             {sup_repertoire($fichier);} // rapel la fonction de mani? r?rsive
             else
             {unlink($fichier);} // sup le fichier
             }
                }
                closedir($sq);
                rmdir($chemin); // sup le r?rtoire
                             }
        else {
                unlink($chemin);  // sup le fichier
             }
       }
		

//fonction pour pour corriger le nom de dossier	
function format_filename($filename) {
	$bads = array(' ','a','c','e','g','i','k','l','n','r','�','u','�','A','C','E','G','I','K','L','N','R','�','U','�','$','&','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','??','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','??','?','?','?','?','?','?');
	$good = array('-','a','c','e','g','i','k','l','n','r','s','u','z','A','C','E','G','I','K','L','N','R','S','U','Z','s','and','A','B','V','G','D','E','J','Z','Z','I','J','K','L','M','N','O','P','R','S','T','U','F','H','C','C','S','S','T','T','E','Ju','Ja','a','b','v','g','d','e','e','z','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','c','c','s','t','t','y','z','e','ju','ja');
	$filename = str_replace($bads,$good,trim($filename));
	$allowed = "/[^a-z0-9\\.\\-\\_\\\\]/i";
	$filename = preg_replace($allowed,'',$filename);
	return $filename;
}
		
 	//remove unnecessary files
	if(isset($_REQUEST['deletefile'])) {
		if(!file_exists($_REQUEST['deletefile'] )) {
			$erreur =  kSupressionFichierFailed;
		} else {
			if(unlink($_REQUEST['deletefile'] )) {
				$erreur =  '';
			} else {
				$erreur = kSupressionErreur;
			}
		}
	}
	
	//r?p? l extension du fichier
function get_file_ext($file)
	{
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		return $ext;
	}

//supprime le dossier
	if(isset($_REQUEST['deletefolder'])) {
	sup_repertoire($_REQUEST['deletefolder']);
	$erreur =  '';

	}


	//cr? r?rtoire
if(isset($_GET['createfolder'])) {
    $new_title = format_filename($_GET['createfolder']);
	if(isset($_REQUEST['currentFolder'])){
		$chemin=$path_upload.$_REQUEST['currentFolder'];
		}
	else $chemin = 	$path_upload;
	  if(!is_dir($chemin.$_REQUEST['createfolder'])) {
			if(mkdir($chemin.$_REQUEST['createfolder'], 0777)) {
				$erreur = '';
			} else {
				$erreur = kCreationRepertoireErreur;
			}
		} else {
			$erreur = kRepertoireExiste;
		}
	}

?>
 



 
<div id="uploadDiv" style="padding: 4px;">
        <form id="uploadForm" enctype="multipart/form-data"  action="empty.php?urlaction=uploadTinymce"   method="post">

                <div class="center">
						<input type="button" class= "mce-widget mce-btn mce-last mce-abs-layout-item"  style=" height: 28px;" value="<?=kUploadCreeDossier?>" onclick="create_folder();">
						<?php 
						if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '')
							echo '<input type="hidden"  name="currentFolder"  value="'.$_REQUEST['currentFolder'].'">';
						?> 		
						<input type="hidden"  name="field"  value="<?php if(isset($_REQUEST['field'])) echo $_REQUEST['field'];?>">
						<input type="hidden"  name="type"  value="<?php if(isset($_REQUEST['type'])) echo $_REQUEST['type'];?>">	
	                    <input id="uploadFile" class= "mce-widget mce-btn mce-last mce-abs-layout-item" style=" height: 28px;" name="uploadFile" type="file" />						
                        <input id="uploadSubmit" class="mce-widget mce-btn mce-primary mce-first mce-abs-layout-item"  style=" height: 28px;" type="submit" value="Upload" />
					
                </div>
				
				<?php if(isset($_REQUEST['error']) && $_REQUEST['error']!='' ) {echo '<br><div class="warning" style="width:250px;margin-left:150px;">'.$_REQUEST['error'].'</div>';}
				if(isset($erreur) && $erreur !='' ) {echo '<br><div class="warning" style="width:250px;margin-left:150px";>'.$erreur.'</div>';}
				
				?>

        </form>
</div>




 <?php
// include(libDir."webComponents/tinymce/upload/importFolder.php");
 ?>
</body>
</html>

<?php

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
/////////////////////// ARBORESCENCE FICHIER ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '')
	$relative_path=$relative_path.'/'.$_REQUEST['currentFolder'];
		
$import_dir=$path_upload;
$url_import_folder='';

if (isset($_REQUEST['currentFolder']) && !empty($_REQUEST['currentFolder']))
{
	$url_path=str_replace('../','',$_REQUEST['currentFolder']);
	$import_dir=trim($import_dir.$url_path);
}
$dh=opendir($import_dir);
if ($dh!=false)
{

	$nb_files=0;
	$fichiers=array();
	$repertoires=array();
// VP 10/10/12 : on ne prend pas en compte les fichiers commen?t par '.'
	while (($file=readdir($dh))!==false)
	{
		//if ($file!='.' && $file!='..' && is_file($import_dir.$file))
		if ($file[0]!='.' && is_file($import_dir.$file))
		{
			$nb_files++;
			$fichiers[]=$file;
			$fichier_reel[]=$import_dir.$file;
		}
		//else if ($file!='.' && $file!='..' && is_dir($import_dir.$file))
		else if ($file[0]!='.' && is_dir($import_dir.$file))
		{
			$nb_files++;
			$repertoires[]=$file;
			$repertoires_reel[]=$import_dir.$file;
		}
	}

	closedir($dh);
}
else
	echo kErreurOuvertureRepertoire;

// if ($nb_files==0 && (!isset($url_path) || empty($url_path)))
	// echo kErreurAucunFichierDossierTrouve;
// else
	// include(getSiteFile('formDir','importFolder.inc.php'));

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
/////////////////////// IMPORT FOLDER INC ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


// {

?>







<span style="float:left;height:520px; width: 120px;" class='tab' >
<table>
		<?php
		
		if (!empty($url_path))
		{
			$rep_parent=dirname($url_path);
			
			if ($rep_parent=='.')
				$rep_parent='';
			
			
			echo '<tr>';

			
			if (empty($rep_parent))
				echo '<td><a href="empty.php?urlaction=uploadTinymce&'.$url_import_folder.'&field='.$_REQUEST['field'].'&type='.$_REQUEST['type'].'"><img src="'.libUrl.'webComponents/tinymce/skins/lightgray/img/fleche_precedent.jpg" alt="precedent" />&nbsp;Retour</a></td>';
			else
				echo '<td><a href="empty.php?urlaction=uploadTinymce&'.$url_import_folder.'&type='.$_REQUEST['type'].'&field='.$_REQUEST['field'].'&currentFolder='.urlencode(dirname($url_path).'/').'"><img src="'.libUrl.'webComponents/tinymce/skins/lightgray/img/fleche_precedent.jpg" alt="precedent" />&nbsp;Retour</a></td>';
			
			echo '</tr>';
		
		}
		
		$i=0;
		$j=0;
		$cpt=0;
		
		foreach ($repertoires as $rep)
		{
	
			$tmp_rep = $rep;
			$subtmp = substr($tmp_rep, 0, '9');			
			if (strlen($tmp_rep) > strlen($subtmp))
				{
				$tmp_rep = $subtmp.'...';
			}
			$i++;
			echo '<tr>';
		//	echo '<td class="resultsCorps"><input type="checkbox" name="file_'.$i.'" /></td>';
			echo '<td>';
			echo '<span ><img src="'.libUrl.'webComponents/tinymce/skins/lightgray/img/folder_magnify.png" alt="repertoire" /></span>';
			echo '<span>';
			echo '<a title="'.$rep.'" href="empty.php?urlaction=uploadTinymce&'.$url_import_folder.'&field='.$_GET['field'].'&type='.$_REQUEST['type'].'&currentFolder='.urlencode($url_path.$rep.'/').'">'.$tmp_rep.'</a>';
			echo '</td>';
			echo '<input type="hidden" name="file_'.$i.'_path" value="'.$repertoires_reel[$j].'" />';
			echo '<input type="hidden" name="file_'.$i.'_original" value="'.basename($repertoires_reel[$j]).'" />';
			echo '</span>';
			echo '<td>';
			echo '<span><img src="'.libUrl.'webComponents/tinymce/skins/lightgray/img/trash.gif" alt="effacer" onclick="delete_rep(\''.$repertoires_reel[$j].'\',\''.$_REQUEST['field'].'\')"/></span>';
			echo '</td>';echo '</tr>';
			$j++;
		}
		echo '</table></span>';
		
		?>
		
			<div  class='tab' style="float:left;width:500px;height:20px;">
			<span>
			<?php 
			if(isset($_REQUEST['currentFolder']) && $_REQUEST['currentFolder'] != '')
				echo $_REQUEST['currentFolder'];
			else
				echo '/';
			?>
			</span>
			</div>
	
		<?php
		$j=0;

		echo '<span class="tab" style="float:left;width:500px;height:500px;">';
		echo '<table  width="500px" cellpadding="12"  border=1 frame=void rules=rows align="center">';
		echo'<tr class="entete"><th>'.kNom.'</th><th>'.kType.'</th><th>'.kUploadTaille.'</th><th>'.kDateModification.'</th><th></th></tr>';
		foreach ($fichiers as $fich)
		{
		//on voit le fichier que si c est un fichier image
		$ext=strtolower(get_file_ext($fich));
		if(in_array($ext, $allowed_types))
		{
			$i++;
			$cpt++;
			$size =  round(filesize($fichier_reel[$j])/1000,2);
			$date =  filemtime($fichier_reel[$j]);
			//concatene le chemin relatif et le nom
			$path= $relative_path.'/'.$fich;
			$extension= strtolower(  substr(  strrchr($fich, '.')  ,1)  );
			//tronque le nom du fichier
			$tmp = $fich;
			$text=str_replace(' ','',$text);
			$subtmp = substr($tmp, 0, '15');			
			if (strlen($tmp) > strlen($subtmp))
				{
				$fich = $subtmp.'...';
			}
 
		
			
			 echo '<tr>';
			 echo '<td style="border:0px;"  title="'.$tmp.'" onclick="submit_url(\''.$path.'\')" >'.$fich.'</td>';
			 echo '<td style="border:0px;">'.$extension.'</td>';
			 echo '<td style="border:0px;text-align : center;">'.$size.'</td>';
			 echo '<td style="border:0px;">'.date ("d/m/Y H:i:s.",$date).'</td>';
			echo '<td><img src="'.libUrl.'webComponents/tinymce/skins/lightgray/img/trash.gif"  alt="effacer" onclick="delete_file(\''.$fichier_reel[$j].'\',\''.$_REQUEST['field'].'\')"/></td>';
			
			 echo '</tr>';
			$j++;
		}	
		else {$i++;$j++;}	
		}
		// if($cpt == 0)
			// echo '<tr><td>Il n\'y a pas de fichiers dans ce r�pertoire</td></tr>';

		
		echo '</table>';
		echo '</span>';


// }



  ?>