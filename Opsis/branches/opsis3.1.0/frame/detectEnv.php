<?php


require_once('conf/conf.inc.php');
require_once(libDir.'session.php');

if(isset($_GET['action'])){
	
	switch ($_GET['action']){
		case 'store_env': 
			if(isset($_POST['xml']) && !empty($_POST['xml'])){
				$xml = $_POST['xml'];
				try{
					$tab = xml2tab($xml);
					$_SESSION['env_user']['browser'] = $tab['XML'][0]['BROWSER'];
					$_SESSION['env_user']['browserVersion'] = $tab['XML'][0]['BROWSERVERSION'];
					$_SESSION['env_user']['env_OS'] = $tab['XML'][0]['ENV_OS'];
					$_SESSION['env_user']['techs'] = $tab['XML'][0]['TECHS'][0];
					session_write_close();
					echo "true - done";
				}catch(Exception $e){
					echo "false - crash while storing env : \n".$e;
				}
			}else{
				echo "false - missing xml params";
			}
		break ; 
		case 'unset_env': 
			if(isset($_SESSION['env_user']) && !empty($_SESSION['env_user'])){
				unset($_SESSION['env_user']);
			}else{
				echo 'false - $_SESSION[env_user] est déja vide ou indéfini';
			}
		break ; 
		case 'get_env': 
			if(!isset($_SESSION['env_user']) || empty($_SESSION['env_user'])){
				echo "empty ".session_id();
				exit;
			}
			try{
				$xml = "<xml>".
				"	<browser>".$_SESSION['env_user']['browser']."</browser>".
				"	<browserVersion>".$_SESSION['env_user']['browserVersion']."</browserVersion>".
				"	<env_OS>".$_SESSION['env_user']['env_OS']."</env_OS>".
				"	<techs>".
				"	<html5>".$_SESSION['env_user']['techs']['HTML5']."</html5>".
				"	<qt>".$_SESSION['env_user']['techs']['QT']."</qt>".
				"	<flash>".$_SESSION['env_user']['techs']['FLASH']."</flash>".
				"</techs></xml>";
				header('Content-Type: application/xml; charset=utf-8');
				echo $xml;
			}catch(Exception $e){
				echo "false - crash while getting env : \n".$e;
			}
		break ; 
		
	}
	
	
}





?>