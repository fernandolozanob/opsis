<?php
/**
 * Batch process de documents, à partir d'une liste d'ID, d'un résultat de recherche,
 * ou d'un répertoire de travail.
 */
 global $db;
 require_once(modelDir.'model_materiel.php');

 $action=$_GET['commande'];

 if ($_GET['id_mat']) $arrIds=@split(',',$_GET['id_mat']); //Par get id_mat


 if ($_GET['search']) {
 	 if ( $_SESSION[$_GET['search']]["sql"]) {

 	 	$sql=$_SESSION[$_GET['search']]["sql"];
 	 	$sql.=$_SESSION[$_GET['search']]["tri"];
 	 	if ($_GET['rang']) $sql.=" LIMIT 1 OFFSET ".$_GET['rang'];

 	 	$arrRes=$db->GetAll($sql);

 	 	foreach ($arrRes as $mat) $arrIds[]=$mat['ID_MAT'];
 	 	unset($arrRes);
 	 }



 }

if (empty($arrIds)) exit;

//Action
// VP 13/07/09 : ajout cas update
switch ($action) {

	case 'delete':
	case 'suppr':
	 	 	$matDeleted=0;
	 	 	foreach ($arrIds as $_mat) {
	 	 		$myMat=new Materiel();
	 	 		$myMat->t_mat['ID_MAT']=$_mat;
	 	 		if ($myMat->delete()) $matDeleted++; //NOTE : pour le moment, pas de gestion des versions
	 	 		unset($myMat);
	 	 	}
	 	 	if ($matDeleted>0) $msg=$matDeleted." ".kSuccesSupprMateriel;
	 	 	else $msg=kErrorSupprMateriel;
	break;


	case 'getMat': //exporte le document, (le premier si on a passé une liste);
	case 'getPanier_mat'://@update VG 15/04/2010
		$myMat=new Materiel();
		$myMat->t_mat['ID_MAT']=$arrIds[0];
		$myMat->getMat();
		$myMat->getValeurs(); //pour l'instant que Valeurs
		echo $myMat->xml_export();
	break;

	case 'update': //met à jour les champ contenus dans le tableau $_GET
		foreach ($arrIds as $_mat) {
			$myMat=new Materiel();
			$myMat->t_mat['ID_MAT']=$_mat;
			$myMat->getValeurs();
			$myMat->mapFields($_GET);
			$myMat->save();
			$myMat->saveMatVal();
			unset($myMat);
		}
		$msg=kSuccesMaterielSauve;

		break;

}


 //Message dans la fenêtre principale.
 //Refresh fenêtre principale.
header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">");
if ($msg) echo "<msg_out>".$msg."</msg_out>";
?>
