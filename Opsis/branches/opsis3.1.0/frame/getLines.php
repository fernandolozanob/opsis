<?php
global $db;
header('Content-Type: text/html');
$myFrame=Page::getInstance();

$id=str_replace('§',',',$_GET['id']); //transfo des § en virgule pour être interprétable dans le sql_op de la recherche
$filter=str_replace('§',',',$_GET['filter']); //on peut ajouter des valeurs 'interdites' par filter
$type=$_GET['type'];

switch($type) {

	case 'usager':
		include_once(libDir."class_chercheUsager.php");
		$mySearch=new RechercheUsager;
		$xsl=getSiteFile("listeDir","usagerGroupeListe.xsl");
		$fld=strtoupper("ID_".$type);
	break;

	case 'fonds':
		include_once(libDir."class_chercheFonds.php");
		$mySearch=new RechercheFonds;
		$xsl=getSiteFile("listeDir","fondsGroupeListe.xsl");
		$fld=strtoupper("ID_".$type);
	break;

	case 'groupe':
		include_once(libDir."class_chercheGroupe.php");
		$mySearch=new RechercheGroupe;
		$xsl=getSiteFile("listeDir","groupeFondsListe.xsl");
		$fld=strtoupper("ID_".$type);
	break;

}

$mySearch->useSession=false;
$mySearch->prepareSQL();

$mySearch->tab_recherche=array(1=>
							array('FIELD'=>$fld,
								  'VALEUR'=>$id,
								   'TYPE'=>'C',
								   'OP'=>'AND')
								  );

if (!empty($filter)) 	$mySearch->tab_recherche[]=array('FIELD'=>$fld,
								  'VALEUR'=>$filter,
								   'TYPE'=>'C',
								   'OP'=>'AND NOT');

$mySearch->makeSQL();
$mySearch->finaliseRequete();
$sql=$mySearch->sql;
$myFrame->initPager($sql,'all'); // init splitter object
$myFrame->addParamsToXSL($param);
$myFrame->afficherListe("t_".$type,$xsl);

?>
