<?php
// var_dump($_SESSION['etat_montage']);
if (isset($_SESSION['etat_montage']['state']) && $_SESSION['etat_montage']['state']=="ended" && isset($_SESSION['etat_montage']['file']) && file_exists($_SESSION['etat_montage']['file'])){
	$file = $_SESSION['etat_montage']['file'];
	if(defined("libDir")){
        require_once(libDir."fonctionsGeneral.php");
        $len = getFileSize($file);
    }else $len = filesize($file);
	trace($len." ".$_SESSION['etat_montage']['file']);
	
	if(isset($_GET['name']) && !empty($_GET['name'])){
		$filename = removeTrickyChars($_GET['name']);
	}else{
		$filename = "montage";
	}
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: public"); 
	header("Content-Description: File Transfer");
	header("Content-Type: application/force-download");
	///Force the download
	$header="Content-Disposition: attachment; filename=".$filename.".mp4;";
	header($header);
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".$len);
	
	readfile($file);
	exit;
}


?>