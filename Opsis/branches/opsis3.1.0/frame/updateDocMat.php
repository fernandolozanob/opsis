<?php
/**
 * Batch process de documents, à partir d'une liste d'ID, d'un résultat de recherche,
 * ou d'un répertoire de travail.
 */
 global $db;
 require_once(modelDir.'model_docMat.php');

 $action=$_GET['commande'];
 
 if ($_GET['id_doc_mat']) $arrIds=@split(',',$_GET['id_doc_mat']); //Par get id_mat

 
 if ($_GET['search']) {
 	 if ( $_SESSION[$_GET['search']]["sql"]) {
 	 	
 	 	$sql=$_SESSION[$_GET['search']]["sql"];
 	 	$sql.=$_SESSION[$_GET['search']]["tri"];
 	 	if ($_GET['rang']) $sql.=" LIMIT 1 OFFSET ".$_GET['rang'];
 	 	
 	 	$arrRes=$db->GetAll($sql);
 	 	
 	 	foreach ($arrRes as $dmat) $arrIds[]=$dmat['ID_DOC_MAT'];
 	 	unset($arrRes);
 	 }
 	

 	 
 }

if (empty($arrIds)) exit;

//Action

switch ($action) {

	case 'delete':
	case 'suppr':
	 	 	$dmatDeleted=0;
	 	 	foreach ($arrIds as $_dmat) {
	 	 		$mydMat=new DocMat();
	 	 		$mydMat->t_doc_mat['ID_DOC_MAT']=$_dmat;
	 	 		if ($mydMat->delete()) $dmatDeleted++; //NOTE : pour le moment, pas de gestion des versions
	 	 		unset($mydMat);
	 	 	}
	 	 	if ($dmatDeleted>0) $msg=$dmatDeleted." ".kSuccesSupprDocMat;
	 	 	else $msg=kErrorSupprDocMat;
	break;

	case 'getDocmat':
	case 'getDocMat': //exporte le document, (le premier si on a passé une liste);
		$mydMat=new DocMat();
		$mydMat->t_doc_mat['ID_DOC_MAT']=$arrIds[0];
		$mydMat->getDocMat();
		$mydMat->getValeurs(); //pour l'instant que Valeurs
		echo $mydMat->xml_export();
	break;


}

  
 //Message dans la fenêtre principale.
 //Refresh fenêtre principale.
header("http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\">"); 
if ($msg) echo "<msg_out>".$msg."</msg_out>"; 
?>
