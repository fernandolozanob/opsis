<?php
	/*
	 Recherche d'adresse dans référentiel d'adresses national
	 */
	header('Content-type: text/xml');
	
	if (isset($_GET['debut']) && !empty($_GET['debut'])) {
		$url="https://api-adresse.data.gouv.fr/search/?q=".urlencode($_GET['debut'])."&limit=20";
		$curl=curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		$json_resp = curl_exec($curl);
		curl_close($curl);
		$resp = json_decode($json_resp);
		if(count($resp->features)>0){
			echo '<options>';
			foreach ($resp->features as $feature) {
				echo '<option>'.$feature->properties->label.'</option>';
			}
			echo '</options>';
		}
	}

?>
