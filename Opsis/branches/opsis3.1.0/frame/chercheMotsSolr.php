<?php

require_once('conf/conf.inc.php');
require_once(libDir.'session.php');

header('Content-type: text/xml');

$solr_query=new SolrQuery();

$solr_query->setTerms(true);
$solr_query->setTermsLimit(-1);

if ($_GET['type']=='FTI')
	// $field_name='text';
	$field_name=strtolower('ac_text_'.$_SESSION['langue']);
else
	$field_name=$_GET['type'];

$arr_field_name=explode(',', $field_name);

foreach ($arr_field_name as $key => $value) {
	$solr_query->addParam("terms.fl",$value);
}

//$solr_query->setTermsField($field_name);
//$solr_query->set("terms.regex","(.*)".$_GET['debut']."(.*)");
$solr_query->set("terms.regex",replaceAccentsListed($_GET['debut'])."(.*)");
$solr_query->set("terms.regex.flag","case_insensitive");
if (defined('gLimitMotSolr') && gLimitMotSolr != '' )
	$solr_query->setTermsLimit(gLimitMotSolr);
else
	$solr_query->setTermsLimit(20);
$solr_opts=unserialize(kSolrOptions);

if (!empty($_SESSION['DB']))
{
	$solr_opts['path']=$_SESSION['DB'];
}

$solr_client=new SolrClient($solr_opts);
$result=$solr_client->query($solr_query);
//var_dump($result->getResponse());
//$result=$result->getResponse()->terms->offsetGet($field_name);

echo '<options>';

//$reponse=array();
foreach ($arr_field_name as $value) {
	$result_tmp=$result->getResponse()->terms->offsetGet($value);
	//var_dump($result_tmp);
	//$reponse=array_merge($result_tmp,$reponse);
	$list_key=array();

	if (!empty($result_tmp)) {
		foreach ($result_tmp as $key=>$res) {
			// suppression de la ponctuation
			//MS - 15.10.2015 - Cette suppression de la ponctuation a peu de sens, l'autocomplete �tant utilis� :
			// - sur les champs full text qui sont capables de g�rer les ponctuations (apostrophe,tiret,etc...)
			// - sur les champs de recherche exacte de termes, auquel cas on souhaite avoir des suggestions qui permettent de retrouver les termes exactes, donc qui incluent la ponctuation du terme
			// pour l'instant nous avons permis la conservation des chars tiret apostrophe & double quote, � voir pour les autres ...
			$key=mb_strtolower(trim(str_replace(array('(',')','[',']',',','.',';',':','/'),' ',$key)),'UTF-8'); // ,'_','-','\'','"',
			if (!in_array($key,$list_key)) // suppression des doublons apres filtre sur la ponctuation
			{
				echo '<option>'.str_replace('&','&amp;', preg_replace("/<.*?>/", "", $key)).'</option>';
				$list_key[]=$key;
			}
		}
	}

}

echo '</options>';

?>
