<?  

     
    $user=User::getInstance();
    $myPage=Page::getInstance();
 
    
    if ($user->loggedIn()) {
    	require_once(modelDir.'model_panier.php');
    	$myPanier = New Panier();
		$listSelections=Panier::getFolders(true);
    } else {
		require_once(modelDir.'model_panier_session.php');
    	$myPanier = New PanierSession();   		
    }
    	
   
   
    require_once(modelDir.'model_usager.php');
    global $db;


	$myPanier->t_panier['ID_PANIER']=$_REQUEST['id_panier'];

    
    // I. Traitement des actions
    if(!isset($_REQUEST["commande"]) || empty($_REQUEST["commande"])) {	// Pas de cmd, lecture d'un panier par défaut, RAZ des statuts précédemment renseignés	

		$myPanier->getPanier();			
	}
    else
    {
        

        $action=$_REQUEST["commande"];
        $ligne=trim($_REQUEST["ligne"]);

      
        $myPanier->t_panier['ID_PANIER']=$_REQUEST["id_panier"];
        $myPanier->currentLine=(integer)trim($_REQUEST["ligne"])-1;
        
		
		if ((strcmp($action,'LIVRAISON')==0)) {
		
			require_once(libDir.'class_livraison.php');
			
			$myPanier->getPanier();
			// VP (19/3/08) : ajout pour livraison directe depuis le panier
			$myPanier->met_a_jour($_POST);
	
	        //debug($myPanier,'red');
	
			$nomDossierCourt=preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()); //nom de dossier défini par microtime
		
			$myLivraison=new Livraison();
			$myLivraison->setDir($nomDossierCourt);
			$myLivraison->arrDocs=$myPanier->t_panier_doc;
			// VP 23/11/10 : prise en compte paramètre $_POST['zipFile']
			if(!empty($_POST['zipFile']) || !empty($_GET['zipFile'])) $myLivraison->zipFile=(!empty($_POST['zipFile']) ? $_POST['zipFile'] : $_GET['zipFile']);
			$myLivraison->doLivraison();
			$myPanier->t_panier_doc=$myLivraison->arrDocs; //pour contrer un bug bizarre qui fait disparaitre la dernière ligne

		}
		else if ((strcmp($action,'LIVRAISON_LIGNE')==0)) {
		
			require_once(libDir.'class_livraison.php');
			
			$myPanier->getPanier();
			// VP (19/3/08) : ajout pour livraison directe depuis le panier => on peut changer des params "à la volée"
			$myPanier->met_a_jour($_POST);
			
			// VP (27/01/11) : récupération lignes panier au cas où elles aurait été effacées par met_a_jour
			if(empty($myPanier->t_panier_doc)) $myPanier->getPanierDoc();
			
	        foreach ($myPanier->t_panier_doc as $idx=>&$pdoc){
			// On ne commande que les lignes qui sont cochées
				if($ligne==$pdoc['ID_LIGNE_PANIER']) {
					$nomDossierCourt=preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()); //nom de dossier défini par microtime

					$myLivraison=new Livraison();
					$myLivraison->setDir($nomDossierCourt);
			
					
					$myLivraison->arrDocs[0]=$pdoc;
					// VP 23/11/10 : prise en compte paramètre $_POST['zipFile']
					if(!empty($_POST['zipFile'])) $myLivraison->zipFile=$_POST['zipFile'];
					
					if (isset($_POST['MAT_TYPE']) && !empty($_POST['MAT_TYPE']))
						$myLivraison->doLivraison(false,'',false,$_POST['MAT_TYPE']);
					else
						$myLivraison->doLivraison(false,'',false);
					
					$pdoc=$myLivraison->arrDocs[0];
				}
			}

		}
		
    }

	$myPanier->getDocs();
	$myPanier->getTitre();
/*
	debug($_POST,'blue',true);
	debug($myPanier->t_panier,'lightgrey',true);
	debug($myPanier->t_panier_doc,'beige',true);	
	*/
	//debug($myLivraison);

	
		echo "<div class='feature'>";
		
		include(getSiteFile("designDir","livraison.inc.php"));

		echo "</div>";
		
		
		
		

            ?>
 