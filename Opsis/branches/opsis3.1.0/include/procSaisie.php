<?
 	//trace(print_r($_POST,true));
    require_once(modelDir.'model_processus.php');
	global $db;

	debug($_POST,'cyan');

	$myUser=User::getInstance();
	$myPage=Page::getInstance();
	$myPage->nomEntite = kProcessus;
	$myProc=New Processus();
	
	if (isset($_POST["commande"]))
		$action=$_POST["commande"];



// II. Traitement des actions
if(isset($_POST["bOk"]) || !empty($action))
{
    // II.1. Suppression enregistrement
    if(strcmp($action,"SUP")==0) { 
    	$myProc->t_proc['ID_PROC']=$_REQUEST["id_proc"];
        $myProc->delete();
        if(!empty($_POST["page"])){
              $myPage->redirect($_POST["page"]);
        }else $myPage->redirect($myPage->getName()."?urlaction=procListe");
    }
    // II.2. Sauvegarde enregistrement
    else if(isset($_POST["bOk"]) || $action=="SAVE" || $action=="DUP_PROC")
    {
		$myProc->updateFromArray($_POST);
		debug($myProc,'pink');
        $saved=$myProc->save();
        $FORM=$_POST;
        if ($saved && !empty($FORM['t_proc_etape'])) {
   
		   	foreach ($FORM['t_proc_etape'] as $idx=>$etape) {
			if (isset($etape['PE_ORDRE']) ) {$lastID=$idx;$FORM['t_proc_etape'][$idx]['ID_PROC']=$myProc->t_proc['ID_PROC'];}
			if (isset($etape['PE_ID_ETAPE_PREC'])) {$FORM['t_proc_etape'][$lastID]['PE_ID_ETAPE_PREC']=$etape['PE_ID_ETAPE_PREC'];unset($FORM['t_proc_etape'][$idx]);}
			if (isset($etape['ID_ETAPE'])) {$FORM['t_proc_etape'][$lastID]['ID_ETAPE']=$etape['ID_ETAPE'];unset($FORM['t_proc_etape'][$idx]);}
		   	}		
        
        $myProc->t_proc_etape=$FORM['t_proc_etape'];
        $myProc->saveEtapes();
        }
        
        // II.2.C. Duplication de l'enregistrement dans la langue courante
        if (strcmp($action,"DUP_PROC")==0){
            $newProc=$myProc->duplicate();
            $myProc=$newProc;
        }
        

    }
} else $myProc->t_proc['ID_PROC']=$_REQUEST["id_proc"];

$myProc->getProc();
$myProc->getProcEtape();

$myPage->titre = (isset($myProc->t_proc['PROC_NOM']) && !empty($myProc->t_proc['PROC_NOM'])?$myProc->t_proc['PROC_NOM']:'');
$myPage->setReferrer(true);

//debug($myProc,'beige');

require_once(getSiteFile("formDir",'procSaisie.inc.php'));
?>