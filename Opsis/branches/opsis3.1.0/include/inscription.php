<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2004/2005                                                                   ---*/
/*--- D�veloppeurs : Vincent Prost, Fran�ois Duran, J�rome Chauvin, Xavier Sirven ---*/
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/

// VP : 06/12/2005 : Test unicité login, mail et soc_mail
// VP : 23/11/2005 : Suppression UsagerBienvenue
// VP : 07/11/2005 : Affectation groupe par défaut
// XS : 10/10/2005 : Cr�ation et utilisation de la classe Usager pour le traitement
// XS : 19/09/2005 : Simplification du formulaire et du traitement + envoie du mail
// VP : 23/05/2005 : insertion de US_ID_TYPE_USAGER et US_ID_ETAT_USAGER par defaut
// FD : 7/12/2004 : utilisation de la classe BD (var ) pour traitement


    require_once(modelDir.'model_usager.php');
    require_once(modelDir.'model_groupe.php');
	
	require_once(libDir.'/captcha/securimage.php');

	$myPage=Page::getInstance();
	$myPage->titre = kInscription;
	$myPage->setReferrer(true);

    $checkPerso = false;
    $myUsager = New Usager();
    $erreurUs="";

    $usr=User::getInstance();
    global $db;

    // I. Traitement des actions
    if(isset($_POST["envoyer"]) || isset($_POST["bAnnuler"]) || isset($_POST["commande"]) || isset($_REQUEST['resetPassword']))
    {

		$action=$_POST["commande"];
		

        // I.A. Annulation d'une action utilisateur
        if(strcmp($action,"CANCEL")==0) {
            $myUsager->getUsager();
        }
        // I.B. Suppression de l'usager
        else if($_POST["supprimer"]) {
            $myUsager->t_usager['ID_USAGER']=$_REQUEST['ID_USAGER'];
            $myUsager->delete();
            $usr->Logout();
        }

		// I.C. Sauvegarde/Mise � jour d'un enregistrement
		// 08/06/2018 ajout de la contrainte UNSUBSCRIBE pour bouton désinscription
        else if(isset($_POST["envoyer"]) || $action=='SAVE' || $action== 'UNSUBSCRIBE')
        {
			$modif_pwd_pb=false;
			
			if (isset($_SESSION['USER']['ID_USAGER']) && !empty($_SESSION['USER']['ID_USAGER']))
			{
				$myUsager->t_usager['ID_USAGER']=$_SESSION['USER']['ID_USAGER'];
				$myUsager->getUsager();
				//cas de modification de mot de passe
				if ($_POST['old_password'] != '' || $_POST['new_password'] != '' || $_POST['new_password2'] != '') {
					if ($myUsager->t_usager['US_PASSWORD'] != getPasswordHash($_POST['old_password'],$myUsager->t_usager['US_SALT'])){
						$myUsager->dropError(kErreurMotDePasseIncorrect);
						$modif_pwd_pb = true;
					} elseif ($_POST['new_password'] != $_POST['new_password2']) {
						$myUsager->dropError(kErreurMotDePasseIdentique);
						$modif_pwd_pb = true;
					} elseif (!check_passwd_normes_cnil($_POST['new_password'])) {
						if($myUsager->error_msg!=''){
							$myUsager->error_msg.='<br />';
						}
						$myUsager->dropError(kErreurMdpNonConformeCNIL);
						$modif_pwd_pb = true;
					} else {

						unset($_POST['id_usager']);
						unset($_POST['us_id_type_usager']);
						unset($_POST['old_password']);
						unset($_POST['new_password2']);
						$_POST['us_password'] = getPasswordHash($_POST['new_password'],$myUsager->t_usager['US_SALT']);
						unset($_POST['new_password']);
						// I.C.1. Sauvegarde/Mise � jour
						$myUsager->updateFromArray($_POST);
					}
				} else {
					unset($_POST['id_usager']);
					unset($_POST['us_id_type_usager']);
					// I.C.1. Sauvegarde/Mise � jour
					$myUsager->updateFromArray($_POST);
				}
				/*if($action== 'UNSUBSCRIBE'){
					Page::getInstance()->redirect($myPage->getName());
				}*/
				
			}
			else
			{
				
				// pas d'ID en session => creation 
				unset($_POST['id_usager']);
				unset($_POST['us_id_type_usager']);
				$_POST['us_id_type_usager']=1;
				
				if(defined("gCreateUsagerDefaultStatut") && is_numeric(gCreateUsagerDefaultStatut)){
					$_POST['US_ID_ETAT_USAGER']=gCreateUsagerDefaultStatut;
				}
				// I.C.1. Sauvegarde/Mise � jour
				$myUsager->updateFromArray($_POST);
			}

			if(empty($myUsager->t_usager['ID_USAGER']))
			{
				if (defined('gUseCaptchaInscription') && gUseCaptchaInscription==true)
				{
					$captcha=new Securimage();
					if ($captcha->check($_POST['captcha_inscription']))
						$captcha_valid=true;
					else
						$captcha_valid=false;
				}
				else
					$captcha_valid=true;
				
				$new=true;
			}
			else
				$new=false;
					
					
			// possibilité de définir une constante permettant la suppression d'un utilisateur inactif depuis "gAllowEraseInactiveUserAfterXDays" jours,
			// => permet d'autoriser nouvelle inscription depuis adresse mail déjà inscrite pour un compte mais jamais validée
			if($new && defined('gAllowEraseInactiveUserAfterXDays') && $myUsager->checkExist()){
				$existing_usager = new Usager() ; 
				$existing_usager->t_usager['ID_USAGER'] = $myUsager->t_usager['ID_USAGER'];
				$existing_usager->getUsager();

				if(!empty($existing_usager->t_usager['US_DATE_CREA']) 
				&& $existing_usager->t_usager['US_ID_ETAT_USAGER'] == Usager::getIdEtatUsrFromCode('INACTIF') 
				&& (microtime(true) - strtotime($existing_usager->t_usager['US_DATE_CREA']))/(3600*24) >  gAllowEraseInactiveUserAfterXDays){
					$existing_usager->delete();
				}
				// Si checkExist a trouvé un usager déjà existant, alors id_usager a été alimenté
				// => pb car déclenchera une sauvegarde sur l'utilisateur trouvé lors de l'appel à create, et ce sans hash le mot de passe 
				$myUsager->t_usager['ID_USAGER'] = null ;
			}
			if ($myUsager->checkFilledFields() && ($new===false || ($new===true && $captcha_valid===true)) && $myUsager->create() && !$modif_pwd_pb){
				// I.C.1.a Saisie effectu�, envoie d'un message � l'administrateur + redirection
				if($new) {
					$myUsager->send_mail_administrateur();
					if($myUsager->t_usager['US_ID_ETAT_USAGER']==Usager::getIdEtatUsrFromCode('ACTIF'))
						$myUsager->send_mail_usager();

					if((defined("gInscriptionRequiresMailValidation") && gInscriptionRequiresMailValidation)
						|| isset($_POST['send_mail_validation'])){
						$myUsager->sendMailUsagerValidation();
					}
				}
				// VP 24/02/10 : Compatibilité class_form (id_groupe au lieu de ID_GROUPE)
				// MS 27.01.16 : Ajout possibilité de définir sous forme de constante (gCreateUsagerDefaultGroupe) un groupe par défaut pr la création d'usager 
				if(isset($_POST['ID_GROUPE']) && !empty($_POST['ID_GROUPE'])){
					$idGroupe=$_POST['ID_GROUPE'];
				}else if(isset($_POST['id_groupe']) && !empty($_POST['id_groupe'])){
					$idGroupe=$_POST['id_groupe'];
				}else if($new && defined("gCreateUsagerDefaultGroupe") && is_numeric(gCreateUsagerDefaultGroupe)){
					$idGroupe=gCreateUsagerDefaultGroupe;
				}
				if(!empty($idGroupe)){
					$ok = $myUsager->saveUsagerGroupe($idGroupe);
				}
				
				if($new){
					$usr->Login($myUsager->t_usager['US_LOGIN'],$myUsager->t_usager['US_PASSWORD']);
				}else{
					$usr->Login($myUsager->t_usager['US_LOGIN'],$myUsager->t_usager['US_PASSWORD'],'',false,true);
				}

				//if($new) $myUsager->dropError(kBienvenue);
				//else $myUsager->dropError(kSuccesSauveUsager);
				if (!$new)
					$myUsager->dropError(kSuccesSauveUsager);


				//Permette la désinscription avec la function js desinscription() dans main.js utiliser sur le bouton de la page Mon profil du CNES avec la redirection sur la page d'accuiel
				if($action== 'UNSUBSCRIBE'){
					Page::getInstance()->redirect($myPage->getName());
				}
			}else{
				
				if($new && !empty($myUsager->t_usager['ID_USAGER'])){
					// MS 20.01.15 - Si la création d'un nouvel utilisateur a échoué, et qu'on a un ID_USAGER rempli, c'est alors qu'on a trouvé un autre utilisateur avec le même login
					// dans ce cas, on supprime l'ID_USAGER (aide au choix de l'interface dans inscription.inc.php  & à ne pas pouvoir utiliser l'id d'un autre usager avec le même login)
					// => fait suite à la modification de 2010 de Usager::checkExist par VP, qui récupère l'ID de l'usager trouvé ayant le même login pour faciliter les imports 
					$myUsager->t_usager['ID_USAGER'] = null ; 
				}
			}


        }else if(isset($_GET['resetPassword']) && !empty($_GET['resetPassword']) || (isset($_POST['resetPassword']) && !empty($_POST['resetPassword']))){
			// vérification validité du token avant d'autoriser l'enregistrement d'un nouveau mot de passe
			// ("validité" => moins de 24h d'écart entre l'heure actuelle et l'heure où le token a été généré, l'utilisateur doit exister )
			require_once(libDir."class_RSA.php");
			$rsa = new RSA(tokenRSAModulus,tokenRSAExponentPrivate);
			$resetPassword_token = $rsa->decrypt(base64_decode($_REQUEST['resetPassword']));
			$resetPassword_token = explode("|",$resetPassword_token) ;
			$resetPassword_user = $resetPassword_token[1];
			$resetPassword_time = $resetPassword_token[0];
			
			$myUsager = new Usager() ; 
			$myUsager->t_usager['ID_USAGER'] = $resetPassword_user;
			$myUsager->getUsager();
			
			$resetPassword_time = (microtime(true) - $resetPassword_time );
			$resetPassword_time = ($resetPassword_time / 3600);
			
			if(isset($_POST['resetPassword']) && !empty($_POST['resetPassword']) && isset($_POST['new_password']) && isset($_POST['new_password2'])){
				$modif_pwd_pb = false;
				if(!(isset($myUsager->t_usager['US_LOGIN']) && isset($myUsager->t_usager['US_PASSWORD'])) || $resetPassword_time>24){
					$myUsager->dropError(kResetPasswordWrongToken);
					$modif_pwd_pb = true;
				}
				if ($_POST['new_password'] != $_POST['new_password2']) {
					if($myUsager->error_msg!=''){
						$myUsager->error_msg.='<br />';
					}
					$myUsager->dropError(kErreurMotDePasseIdentique);
					$modif_pwd_pb = true;
				}
				if (!check_passwd_normes_cnil($_POST['new_password'])) {
					if($myUsager->error_msg!=''){
						$myUsager->error_msg.='<br />';
					}
					$myUsager->dropError(kErreurMdpNonConformeCNIL);
					$modif_pwd_pb = true;
				}
				if(!$modif_pwd_pb){
					//enregistrement du nouveau mot de passe, le hash est généré dans myUsager->save(true) (premier paramètre à true !)
					$myUsager->t_usager['US_PASSWORD'] = $_POST['new_password'];
					$myUsager->save(true);
					
					$password_has_been_changed = true ; 
				}
			}else{
				$modif_pwd_pb = true ; 
			}
		}

    }

    // II. Affichage d'un Usager /!\ faille de sécurité
	// GT : FAILLE DE SECURITE : possibilité de choisir son ID et modifier les informations de n'importe quel utilisateur
	/*else if(isset($_GET["id_usager"])) {
		$myUsager->t_usager['ID_USAGER']=$_REQUEST['id_usager'];
		$myUsager->getUsager();
    }*/
    else if(!empty($usr->UserID)) {
        $myUsager = New Usager();
        //$myUsager->t_usager['ID_USAGER']=$_REQUEST['ID_USAGER'];
        $myUsager->t_usager['ID_USAGER']=$usr->UserID;
        $myUsager->getUsager();
    }
	
	if(isset($_REQUEST['resetPassword']) && !empty($_REQUEST['resetPassword'])){
		include(getSiteFile("formDir","resetPassword.inc.php"));
	}else{
		include(getSiteFile("formDir","inscription.inc.php"));
	}
	?>
