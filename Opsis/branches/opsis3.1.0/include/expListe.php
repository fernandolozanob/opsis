<?
require_once(libDir."class_chercheExp.php");
global $db;

$myUsr=User::getInstance();
$myPage=Page::getInstance();
$myPage->titre=kExploitation;
$myPage->setReferrer(true);
$mySearch=new RechercheExp();

if(isset($_GET["init"]))
	$mySearch->initSession();

if(isset($_POST["F_exp_form"]))
{
    $mySearch->prepareSQL();
	$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
	$mySearch->finaliseRequete(); //fin et mise en session
}

if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
$mySort=$myPage->getSort(true,$deft);
if ($deft==false) $_SESSION[$mySearch->sessVar]["tri"]=$mySort;
if ($deft==true && $_SESSION[$mySearch->sessVar]["tri"]=='') $_SESSION[$mySearch->sessVar]["tri"]=$mySort;



include_once(getSiteFile("formDir","chercheExp.inc.php"));


    if(isset($_POST["commande"])){
        $id_exp = urldecode($_POST["ligne"]);
        if(strcmp($_POST["commande"],"SUP")==0) {
        require_once(modelDir.'model_exploitation.php');
            $mon_objet = New Exploitation();
            $mon_objet->t_exploitation['ID_EXP'] = $id_exp;
            $mon_objet->delete();
        }
    }

    if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql = $_SESSION[$mySearch->sessVar]["sql"];
	    
	    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];
	
	    //Sauvegarde du nombre de lignes de recherche � afficher :
	    if($_REQUEST['nbLignes'] != '') {
	    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}
	
	    if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;
	    $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object
	
		$_SESSION[$mySearch->sessVar]["rows"] = $myPage->found_rows;
	
	    print("<div class='errormsg'>".$myPage->error_msg."</div>");
	    $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	    $param["page"] = $myPage->page;
	        $param["id_doc"]=$_REQUEST['id_doc'];
		$param["xmlfile"]=getSiteFile("listeDir","xml/expListe.xml");
	
		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
	    $myPage->addParamsToXSL($param);
	    $myPage->afficherListe("exp",getSiteFile("listeDir","expListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	}

?>