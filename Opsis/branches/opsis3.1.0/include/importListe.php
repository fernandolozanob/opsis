<?php
require_once(libDir."class_chercheImp.php");
global $db;
$myPage=Page::getInstance();
$myUser=User::getInstance();
$myPage->titre=kImports;
$myPage->setReferrer(true);
$mySearch=new RechercheImp();

	if(isset($_GET["init"])) $mySearch->initSession(); // réinit recherche

	if (!isset($_SESSION[$mySearch->sessVar])) {
    	$mySearch->prepareSQL();
    	if ( (defined('gSeuilAppliqueDroits') && $myUser->Type < intval(gSeuilAppliqueDroits) ) 
			||(!defined('gSeuilAppliqueDroits') && $myUser->Type<kLoggedDoc ) ){
			 $mySearch->appliqueDroits();
		}
	    $mySearch->makeSQL();
		$mySearch->finaliseRequete();
	}
    if(isset($_POST["F_imp_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
		if ( (defined('gSeuilAppliqueDroits') && $myUser->Type < intval(gSeuilAppliqueDroits) ) 
			||(!defined('gSeuilAppliqueDroits') && $myUser->Type<kLoggedDoc ) ){
			 $mySearch->appliqueDroits();
		}
 		$mySearch->finaliseRequete(); //fin et mise en session
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	$mySort=$myPage->getSort(true,$deft,array('COL'=>array('ID_IMPORT'),'DIRECTION'=>array('DESC')));
	if ($deft==false) $_SESSION[$mySearch->sessVar]["tri"]=$mySort;
	if ($deft==true && $_SESSION[$mySearch->sessVar]["tri"]=='') $_SESSION[$mySearch->sessVar]["tri"]=$mySort;
	
	// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
	//require_once(getSiteFile("formDir","valListe.inc.php"));
	if (file_exists(formDir."impListe.inc.php")) require_once(formDir."impListe.inc.php");
	elseif (file_exists(formDirCore."impListe.inc.php")) require_once(formDirCore."impListe.inc.php");
	else {
		require_once(libDir."class_formCherche.php");
		$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheImp.xml"));
		$myForm=new FormCherche;
		$myForm->chercheObj=$mySearch;
		$myForm->entity="imp";
		$myForm->classLabel="label_champs_form";
		$myForm->classValue="val_champs_form";
		$myForm->display($xmlform);
	}
	

if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql=$_SESSION[$mySearch->sessVar]["sql"];
	    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

	    //Sauvegarde du nombre de lignes de recherche � afficher :
	    if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
	    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

		// VP 21/05/10 : ajout paramètre gNbLignesDefaut par défaut
		$nbDeft=(defined("gNbLignesListeDefaut")?gNbLignesListeDefaut:10);
		if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = $nbDeft;
	    $myPage->InitPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

	    print("<div class='errormsg'>".$myPage->error_msg."</div>");
	    $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	    $param["page"] = $myPage->page;
	    $param["titre"] = urlencode(kImport);
		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		// VP 08/04/10 : ajout paramètre xmlfile 
		$param["xmlfile"]=getSiteFile("listeDir","xml/impListe.xml");
	    $myPage->addParamsToXSL($param);
	    $myPage->afficherListe("t_import",getSiteFile("listeDir","impListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	}
	
	
?>