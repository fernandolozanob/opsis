<?
require_once(modelDir.'model_exploitation.php');
require_once(libDir."browser_detection.php");
global $db;

$myUser = User::getInstance();
$myPage = Page::getInstance();
$myExp = New Exploitation();
if(isset($_GET["id_exp_gen"])){
    $_POST['id_exp_gen'] = $_GET["id_exp_gen"];
}
$myExp->updateFromArray($_POST);

$action = $_POST["commande"];
$myExp->t_exploitation['ID_EXP'] = $_REQUEST["id_exp"];

if(isset($_POST["bOk"]) || !empty($action))
{
	if(strcmp($action,"SUP_EXP") == 0) {
        $myExp->delete();
        //echo "<script>window.close();</script>";
        $myPage->redirect($myPage->getName()."?urlaction=expListe");
    }
    else if($action == "SAVE" || $action == "DUP_EXP")
    {
    	$FORM = $_POST;
    	
    	$saved = $myExp->save();
    	
	    if ($saved) {
            if ($action == "DUP_EXP"){
                $newExp=$myExp->duplicate();
                $myExp=$newExp;
            }
		    elseif(isset($_GET["id_doc"]))
			{
				$myExp->t_exp_doc[]['ID_DOC'] = intval($_GET["id_doc"]);
				$myExp->saveExpDoc();
				echo "<script>window.opener.location.reload();
						window.location = '" . $myPage->getName() . "?urlaction=expSaisie&id_exp=" . $myExp->t_exploitation['ID_EXP'] . "';
						</script>";
			}
	    	elseif (isset($_GET["id_exp_gen"]))
			{
				echo "<script>window.opener.location.reload();
						window.location = '" . $myPage->getName() . "?urlaction=expSaisie&id_exp=" . $myExp->t_exploitation['ID_EXP'] . "';
						</script>";
			}
	    }
    }
    else if(strcmp($action, "SAVE_PROD") == 0){
    	$FORM = $_POST;
    	$myExp->t_exp_doc = $FORM['t_exp_doc'];
    	$myExp->saveExpDoc();
    }
	else if(strcmp($action, "SAVE_EXP") == 0){
    	$FORM = $_POST;
    	$myExp->t_exp_exp = $FORM['t_exp_exp'];
    	$myExp->saveChildExp();
    }
    //PC 07/10/10 : Ajout demande SDRM
	else if(strcmp($action, "GENERE_SDRM") == 0){
		$sql = "select e.*
        		from t_exploitation e
        		where e.ID_EXP = " . intval($myExp->t_exploitation['ID_EXP']);
		$res = $db->GetAll($sql);
		$content = TableauVersXML ($res, "SDRM_DATA", 0, "EXPORT_OPSIS", 0);
		$params = array(
			"xmlfile" 	=> getSiteFile("designDir","print/xml/export_SDRM.xml")
		);
		$contentExport = TraitementXSLT($content,getSiteFile("designDir","print/export_SDRM.xsl"),$params,0);
		
		//Log xml
		$log = new Logger("testSDRM.xml");
		$log->Log($contentExport);
		
		$filename="exportSDRM.xml";
	    $mime_type = 'text/plain';
	  	header('Content-Type: ' . $mime_type);
	  	header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	  	$browser=browser_detection('browser');
		if (strpos($browser,'ie')===0 || strpos($browser,'op')===0) {
			header('Content-Disposition: inline; filename="' . $filename . '"');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
		} else {
			header('Content-Disposition: attachment; filename="' . $filename . '"');
			header('Pragma: no-cache');
		}
  		echo $contentExport;
    }
    
	if(!empty($_POST["page"])){
		$params = split('&',$_POST["page"]);
		foreach($params as $idx=>$prm) {
			list($key,$val) = split('=',$prm);
			$urlparams[$key]=$val;
		}
		foreach($urlparams as $idx=>$val)
			$url.=$idx."=".$val."&";
		if($urlparams['urlaction']!=$_GET['urlaction'] || $urlparams['id_exp']!=$myDoc->t_exploitation['ID_EXP'])
			$myPage->redirect($url);
	}
	$myExp->getExpFull();
}
elseif(isset($_GET["id_exp"]))
{
    $myExp->t_exploitation['ID_EXP'] = $_GET["id_exp"];
    $myExp->getExpFull();
}
else
{
    if(empty($myExp->t_exploitation['ID_EXP'])){

    }
}

if ($_REQUEST['form'] && is_file(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'))) {
	require_once(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'));
}
else
	require_once(getSiteFile("formDir",'expSaisie.inc.php'));
?>

