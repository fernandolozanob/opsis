<?

    require_once(modelDir.'model_docMat.php');
 
	global $db;
	$myPage=Page::getInstance();


// Initialisation
$myDmat = New DocMat();

$myDmat->t_doc_mat['ID_DOC_MAT']=urldecode($_REQUEST["id_doc_mat"]);
// Note: en soi, id_lang ne sert pas à t_doc_mat mais il sert à récupérer le doc lié
if ($_REQUEST["id_lang"]) $myDmat->id_lang=urldecode($_REQUEST["id_lang"]);
$myDmat->getDocMat();


// I. Traitement des actions
if(isset($_POST["bOk"]) || isset($_POST["bAnnuler"]) || isset($_POST["commande"])){


	$FORM=$_POST;

    $action=$_POST["commande"];
    $myDmat->updateFromArray($_POST);

	foreach ($FORM['t_doc_mat_val'] as $idx=>$val) {
		if (isset($val['ID_VAL'])) {$lastID=$idx;$FORM['t_doc_mat_val'][$idx]['ID_DOC_MAT']=$myDmat->t_doc_mat['ID_DOC_MAT'];}
		if (isset($val['ID_TYPE_VAL'])) {$FORM['t_doc_mat_val'][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];unset($FORM['t_doc_mat_val'][$idx]);}
	}

    // I.A. Suppression de l'enregistrement
    if(strcmp($action,"SUP")==0) {
        $myDmat->delete();

    }

    // I.C. Sauvegarde/Mise � jour d'un enregistrement
    //else if(isset($_POST["bOk"])){
    else if((isset($_POST["bOk"]))||(strcmp($action,"SAVE")==0)){

        $ok=$myDmat->save();

        if ($ok) {
        	$myDmat->t_doc_mat_val=$FORM['t_doc_mat_val'];
	        $myDmat->saveDocMatVal();

        } 

    }



    // I.C. Redirection si il n'y pas d'erreur et que l'on ne souhaite pas cr�er de duplicat

         if(strlen($_POST["page"])>0 ){
           print "<script>window.location.replace('".$_POST["page"]."')</script>";
        }

}


$myDmat->getValeurs();
$myDmat->getDoc();
$myDmat->getMateriel();

debug($myDmat->t_doc_mat,'beige');


if ($myDmat->error_msg) print('<script>showAlert("<div class=\'error\'>'.$myDmat->error_msg.'</div>","alertBox","slideDown",200,0);</script>');


	if ($_REQUEST['form'] && is_file(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'))) {
		require_once(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'));
	}
	else require_once(getSiteFile("formDir",'docMatSaisie.inc.php'));

?>