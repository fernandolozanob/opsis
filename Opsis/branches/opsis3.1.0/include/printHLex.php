<?php

//@author B.RAVI 2016-08-09 Car besoin spécifique sur CNRS, ou le client veut que l'arbre hiérarchique soit déplié
if (file_exists(designDir . "/include/pre_printHLex.inc.php")) {
	include(designDir . "/include/pre_printHLex.inc.php");
}


require_once(libDir."class_chercheLex.php");
$mySearch=new RechercheLex();
$id_type_lex=$_REQUEST['type_lex'];
$id_input_result='';
$mySearch->treeParams=array(
    		"ID_TYPE_LEX"=>$id_type_lex,
    		"ID_TYPE_DESC"=>'',
    		"ID_ROLE"=>'',
    		"MODE"=>'');

echo "

			<script>
				function dspClock (toggle) {

					if (toggle) {
						document.getElementById('clock').style.display='block';
						if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='none';
						}
					else {
						document.getElementById('clock').style.display='none';
						if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='block';
						}
					return true;
				}

			</script>
			<div id='clock' style='display:none;text-align:center'>".kPatientez."<br/><img src='".imgUrl."wait30trans.gif'></div>

			<div align='center'  style='width:600px;border:1px solid #999999;' >
			<script>dspClock(true)</script>
			";

			require_once(libDir."class_tafelTree.php");
            
            echo "<script>dspClock(true)</script>";
			
			$btnEdit=''; //Mode simple, on réinit le btnEdit sinon on le transmet à l'arbre

			$title=GetRefValue('t_type_lex',$id_type_lex,$_SESSION['id_lang']);
			
			$myAjaxTree=new tafelTree($mySearch,'form1');
			//$myAjaxTree->JSlink="window.parent.".$jsFunction."(\"%s\",\"%s\",%s)";
			 if($mySearch->noAjax) $myAjaxTree->noAjax=$mySearch->noAjax;
			 if($mySearch->useCookies) $myAjaxTree->useCookies=$mySearch->useCookies;
			 if($mySearch->imgBase) $myAjaxTree->imgBase=$mySearch->imgBase;
 			 if($mySearch->includeRoot)	$myAjaxTree->includeRoot=$mySearch->includeRoot;
			
			$myAjaxTree->JSReturnFunction=$jsFunction;
			$myAjaxTree->makeRoot();


            if ($valeur!='') { //En plus, on a une recherche sur un terme
            	$rst=$db->GetAll($sql); // Lancement de la requete.
	            if (!empty($rst)) {
					foreach ($rst as $row) { //Pour chaque résultat...
					   switch ($desc_champs_appelant[0]) {
						   case 'LF':
						   case 'L':
						   case 'LC' :
						   case 'LFC':
							   if($row['LEX_ID_ETAT_LEX']>=2) { //... on ne garde que les termes valides
								  if ($row['LEX_ID_SYN']==0) {//s'il y a un synonyme préférentiel, c'est lui qu'on retient
									$myAjaxTree->revealNode($row['ID_VAL']);
									 }
								  else $myAjaxTree->revealNode($row['LEX_ID_SYN']);
							   }
						   break;
						   case 'PAN' :
						   		$myAjaxTree->revealNode($row['ID_VAL']);
						   break;
						   case 'PF':
						   case 'P':
						   case 'PC' :
						   case 'PFC':
								$myAjaxTree->revealNode($row['ID_VAL']);
							break;							
					   }

					}
				echo "<div class='story'>".count($rst)." ".kResultats."</div>";
	            } else echo "<div class='story'>".kAucunResultat."</div>";
            }
            if ($_POST['id_asso']) $myAjaxTree->revealNode(intval($_POST['id_asso']),'rebond');
			//debug($btnEdit);
            $myAjaxTree->renderOutput($id_input_result,true,$btnEdit);			
			echo "<script>dspClock(false)</script>";



?>
