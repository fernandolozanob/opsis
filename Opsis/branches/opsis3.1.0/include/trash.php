<?php
	global $db;
	$myUser=User::getInstance();
	$myPage=Page::getInstance();

    require_once(modelDir.'model_trash.php');

    $myTrash= new Trash();
	// Traitement formulaire
	if(isset($_POST)){
		$ok=false;
		$myTrash->t_trash['ID_TRASH']=intval($_POST["id_trash"]);
		switch ($_POST['commande']) {
			// SUP : appel méthodes delete de l’objet trash
			case "SUP":
				$ok=$myTrash->delete();
				break;
				
			// RESTORE : appel méthodes restore de l’objet trash
			case "RESTORE":
				$myTrash->getTrash();
				$ok=$myTrash->restore();
				break;
		}
		// Redirection sur trashListe
		if($ok) $myPage->redirect($myPage->getReferrer());
	}
		
		// Affichage
    if(isset($_GET["id_trash"])){
		$myTrash->t_trash['ID_TRASH']=intval($_GET["id_trash"]);
		$myTrash->getTrash();
	   
		// Affichage formulaire
		include(libDir."class_form.php");
		$xmlform=file_get_contents(getSiteFile("designDir","form/xml/trashSaisie.xml"));
		$myForm=new Form();
		$myForm->display($xmlform);
		
		// Affichage données
		$content= $myTrash->xml_export(1,"","","EXPORT_OPSIS");
		//debug($content,pink,true);

		$xsl=getSiteFile("designDir","detail/trash.xsl");
		$xml=getSiteFile("designDir","detail/xml/docAff.xml");
		$contentExport =TraitementXSLT($content,$xsl,array("profil"=>User::getInstance()->getTypeLog(),
														   "loggedIn"=>loggedIn(),
														   "xmlfile"=>$xml
														   ),0);

		$contentExport = eval("?".chr(62).$contentExport.chr(60)."?");
		echo $contentExport;
    }
?>