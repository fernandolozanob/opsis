<?

require_once(modelDir.'model_usager.php');

$myPage=Page::getInstance();
$myPage->nomEntite = kUtilisateur;
$myUser = New Usager();
try {
	// Vérification de base : sur l'admin peut utiliser un id autre que le sien.
	$me=User::getInstance();
	// VP 10/9/10 : ajout protection sur ID=1
	//if (($me->getTypeLog()!=kLoggedAdmin && $me->UserID!=$_REQUEST['id_usager']) || $_REQUEST['id_usager']=="1") {
	// VP 14/10/11 : suppression protection sur profil admin
	$_REQUEST['id_usager']=intval((isset($_REQUEST['id_usager']))?$_REQUEST['id_usager']:'0');


    // I. Traitement des actions
    if(isset($_POST["bOk"]) || isset($_POST["bAnnuler"]) || isset($_POST["commande"]))
    {
        $action=$_POST["commande"];
        $id_usager = intval($_POST["id_usager"]);

        // I.B. Suppression de l'usager
       if(strcmp($_POST["commande"],"SUP")==0) {
            $myUser->t_usager['ID_USAGER']=$id_usager;
            $myUser->delete();
			if(empty($_POST['no_redirection'])) {
				Page::getInstance()->redirect($myPage->getName()."?urlaction=usagerListe");
			}
        }

        // I.C. Sauvegarde/Mise � jour d'un enregistrement
		// VP 5/06/09 : ajout sauvegarde avec commande=SAVE
	   else if(isset($_POST["bOk"]) || (strcmp($_POST["commande"],"SAVE")==0))
        {
			// VP 10/12/08 : modification sauvegarde (analogue inscription.php)
            $password_has_been_changed = false ; 
		   
            //$myUser->t_usager['ID_USAGER']=(int)$_REQUEST["id_usager"];//testé, $_POST["id_usager"] prendra le pas sur le $_GET["id_usager"]
			if(!empty($id_usager)){
				$myUser->t_usager['ID_USAGER']=$id_usager;
				$myUser->getUsager();
			}
			
//               echo '<pre>'; 
//             var_dump($myUser);
//             echo '</pre>';
//             die();
            
            $modif_pwd_pb=false;
            //cas de modification de mot de passe
            if ($_POST['new_password'] != '' || $_POST['new_password2'] != '') { 
			   if ($_POST['new_password'] != $_POST['new_password2']) {
					if($myUser->error_msg!=''){
						$myUser->error_msg.='<br />';
					}
					$myUser->dropError(kErreurMotDePasseIdentique);
					$modif_pwd_pb = true;
				}
				if (!check_passwd_normes_cnil($_POST['new_password'])) {
					if($myUser->error_msg!=''){
						$myUser->error_msg.='<br />';
					}
					$myUser->dropError(kErreurMdpNonConformeCNIL);
					$modif_pwd_pb = true;
				}
				
				if(!$modif_pwd_pb){
					unset($_POST['new_password2']);
                    $_POST['us_password'] = $_POST['new_password'];
                    unset($_POST['new_password']);
                    $myUser->updateFromArray($_POST);
					$password_has_been_changed = true ; 
                }
            } else {
                $myUser->updateFromArray($_POST);
            }
			if ((!empty($myUser->t_usager['ID_USAGER']) && $myUser->save($password_has_been_changed)) 
			|| (empty($this->t_usager['ID_USAGER']) && $myUser->create())){
				if($_POST['id_groupe'] || $_POST['ID_GROUPE']){
					$id_groupe = (!empty($_POST['id_groupe'])? $_POST['id_groupe']:$_POST['ID_GROUPE']);
					$myUser->saveUsagerGroupe($id_groupe);
				}else if(isset($_POST['t_usager_groupe'])){
					$myUser->saveUsagerGroupe($_POST['t_usager_groupe']);
				}
				if(isset($_POST["us_send_mail"])){
					//PC 07/02/11 : Corrections de 1 en "1" car email jamais envoyé
					if($_POST["us_send_mail"]=="1" && $myUser->isActif()){
						$myUser->send_mail_usager();
					}
					if(empty($_POST['no_redirection']) && !$modif_pwd_pb) {
						if (trim($myUser->error_msg)==''){//B.RAVI 2016-09-05 si on fait 1 redirection, on ne verra pas les messages d'erreurs, donc on redirige que si on a pas d'erreurs
						$myPage->redirect($myPage->getName()."?urlaction=usagerListe");
						}
					}
                }
                $myUser->getUsagerGroupe();
			}
			
			// MS - code commenté car empechait récupération de l'ID_USAGER d'un usager tout juste créé
			/*si on n'a pas d'id usager existant dans les donnees du formulaire*/
			/*if (!isset($_POST['id_usager']) || empty($_POST['id_usager']))
				unset($myUser->t_usager['ID_USAGER']);
			*/
			$myUser->getUsagerModif();//@update VG 25/06/2010
			
				/*
			if ($myUser->save()){
				if(isset($_POST["us_send_mail"]) && isset($_POST['US_ID_ETAT_USAGER'])){
					if($_POST["us_send_mail"]==1 && $_POST['US_ID_ETAT_USAGER']==Usager::getIdEtatUsrFromCode('ACTIF')){
						$myUser->send_mail_usager();
					}
                    $myPage->redirect($myPage->getName()."?urlaction=usagerListe");
                }
                $myUser->getUsagerGroupe();
            }
			*/
        }
		else if(isset($_POST["bAnnuler"])) (Page::getInstance()->redirect($myPage->getName()."?urlaction=usagerListe"));

    }

    // II. Affichage d'un Usager
    else if(isset($_GET["id_usager"])) {

        $myUser->t_usager['ID_USAGER']=$_GET["id_usager"];
        $myUser->getUsager();
        $myUser->getUsagerGroupe();
        $myUser->getUsagerModif();//@update VG 10/06/2010
    }
	$nom = (isset($myUser->t_usager['US_NOM']) && !empty($myUser->t_usager['US_NOM'])?$myUser->t_usager['US_NOM']:'');
	$prenom = (isset($myUser->t_usager['US_PRENOM']) && !empty($myUser->t_usager['US_PRENOM'])?$myUser->t_usager['US_PRENOM']:'');
	$myPage->titre = $nom." ".$prenom;
	$myPage->setReferrer(true);
	
	require(getSiteFile("formDir","usagerSaisie.inc.php"));


}
catch (Exception $e) {
	echo $e->getMessage();
}

?>
