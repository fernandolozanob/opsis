<?

    $user=User::getInstance();
    $myPage=Page::getInstance();

    require_once(modelDir.'model_panier.php');
    $myPanier = New Panier();
    $listSelections=Panier::getFolders(true);

	
	$xsl='panierPublic.xsl';
	if (isset($_REQUEST['xsl']) && $_REQUEST['xsl'] && getSiteFile("listeDir",$_REQUEST['xsl'].'.xsl')!=siteDirCore."index.php") $xsl=$_REQUEST['xsl'].'.xsl';

	$myPanier->sqlOrder=$myPage->getSort(false); //Pas de tri par défaut !
    if (!empty($myPanier->sqlOrder)) $_SESSION['tri_panier_doc']=$myPanier->sqlOrder;
    else unset($_SESSION['tri_panier_doc']);

    if(isset($_REQUEST["id_panier"])) $id_panier=intval($_REQUEST["id_panier"]);
	else $id_panier="";

    $myPanier->t_panier['ID_PANIER']=$id_panier;
    $myPanier->getPanier();

    if($myPanier->t_panier["PAN_PUBLIC"]!=1){
        echo "<div class='error'>".kAccesReserve."</div>";return;
        exit;
    }

    // Traitement des actions
    if(!isset($_POST["commande"]) || empty($_POST["commande"])) {
        switch($_POST["commande"]){
                
        case"SEND_MAIL":
            if(isset($_POST['email_to_send'])){
                
                $email=$_POST['email_to_send'];
        
                $frontiere = "_----------=_parties_".md5(uniqid (rand()));
                $param = 	array (
                                "etat" => $myPanier->panier['PAN_ID_ETAT'],
                                "playerVideo" => gPlayerVideo,
                                "imgurl"=>kCheminHttp."/design/images/",
                                "senderName"=>$user->Prenom . ' ' . $user->Nom,
                                "boundary"=>$frontiere,
                                "corpsTexte"=>$_POST['corpsMail']
                            );
        
                foreach ($myPanier->t_panier_doc as $key=>$doc){
                    if (isset($doc['ID_DOC'])){
                        $res = $db->getAll("SELECT ID_MAT from t_doc_mat where ID_DOC = " . $db->Quote($doc['ID_DOC']));
                        foreach ($res as &$mat){
                            $ext = substr($mat['ID_MAT'], strrpos($mat['ID_MAT'], '.'));
                            if (strcmp($ext, ".mp4") == 0)
                                $myPanier->t_panier_doc[$key]['VISIOFLASH'] = $mat['ID_MAT'];
                        }
                        $myPanier->t_panier_doc[$key]['URLHTTP'] = kCheminHttp;
                    }
                }
                
                $xml=$myPanier->xml_export();
                $xml.=$myPanier->xml_export_panier_doc();
                $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";
                
                $message= TraitementXSLT($xml,getSiteFile("designDir","print/selectionMail.xsl"),$param,1);
                ob_start();
                eval("?>". str_replace(array('||'," ' "),array(chr(10),"'"),$message)."<?");
                $message=ob_get_contents();
                ob_end_clean();
                $message=str_replace(array("€","’"),array("&euro;","'"),$message);
                $headers="From:".$user->Mail."\nMIME-Version: 1.0\nContent-Type: multipart/alternative; boundary=\"".$frontiere."\"";
                if(!empty($_POST['copyForExp']) && $_POST['copyForExp'] == '1')
                $headers .= "\nCc: ".$user->Mail;
                /*if(defined("kMailSujetEnvoi")) $sujet=constant("kMailSujetEnvoi").gSite;
                else $sujet="Sélection sur ".gSite;*/
                
                //$sujet = utf8_decode($sujet);
                //$message = utf8_decode($message);
                $sujet = $_POST['objetMail'];
                //$rtn=mail($email, "=?ISO-8859-1?B?".base64_encode(utf8_decode($sujet))."?=",utf8_decode($message),$headers);
                include_once(modelDir.'model_message.php');
                $msg = new Message();
                $rtn=$msg->send_mail($email, $user->Mail,"=?ISO-8859-1?B?".base64_encode(utf8_decode($sujet))."?=", utf8_decode($message), "multipart/alternative", $frontiere);
                
                if ($rtn)
                    $myPanier->dropError(kSuccesSendEmail);
                else
                    $myPanier->dropError(kFailSendEmail);
                    
                unset($msg);	
            }
            break;
        }
    }

// Affichage
	$xmlPanier=$myPanier->xml_export(0,0);
	
	include(getSiteFile("designDir","menuPanier.inc.php"));
	
    if(isset($_GET["page"])){
        $page= $_GET["page"];
    }
    else $page="1";

    $nbDeft=(defined("gNbLignesPanierDefaut")?gNbLignesPanierDefaut:10);
    $myPage->nbLignes=((isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'])?$_REQUEST['nbLignes']:$nbDeft);
    $myPage->page=isset($_REQUEST['page'])?$_REQUEST['page']:'';
    $myPage->initPagerFromArray($myPanier->t_panier_doc);
    
    $param["print"] = ((isset($_GET['print']) && $_GET['print'])?htmlentities($_GET['print']):0);
    $param["nbLigne"] = $myPage->nbLignes;
    $param["page"] = $myPage->page;
    $param["playerVideo"]=gPlayerVideo;
    $param["pager_link"]=$myPage->PagerLink;
    $param["offset"] =  $myPage->nbLignes*($myPage->page-1);
    $param["cartList"]=isset($cartList)?$cartList:'';
    $param["urlparams"]=$myPage->addUrlParams();
    $param["titre"]=$myPanier->titre;
    $param["frameurl"]=frameUrl;
    $param["error_msg"]=$myPanier->error_msg; //by LD 20 11 08, envoi msg erreur
    if (!isset($param["xmlfile"]) || empty($param["xmlfile"]))
        $param["xmlfile"]=getSiteFile("listeDir","xml/panierDoc.xml");
    $myPage->addParamsToXSL($param);

    if (isset($myLivraison) && $myLivraison) $xmlLivraison=$myLivraison->xml_export(0,0);
    $allXML=$xmlPanier.$xmlLivraison.array2xml($listSelections,'cart_list')."\n".array2xml($_SESSION['USER']['US_GROUPES'],'privileges');

    $myPage->afficherListe('t_panier_doc',getSiteFile("listeDir",$xsl),false,null,$allXML);

?>
