<?
require_once(libDir."class_chercheDocMat.php");
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();

$myPage->titre=kDocument." / ".kMateriel;;
$mySearch=new RechercheDocMat();

	if(isset($_GET["init"])) {$mySearch->initSession(); }// réinit recherche

    if(isset($_POST["F_dmat_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION[$mySearch->sessVar]["tri"]=$myPage->getSort();



// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
//include_once(getSiteFile("formDir","chercheDocMat.inc.php"));
if (file_exists(formDir."chercheDocMat.inc.php")) require_once(formDir."chercheDocMat.inc.php");
else {
	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheDocMat.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="DMAT";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
}


    // III. Suppression d'un imageur, des images li�es et mise � jour de t_doc et t_mat
    if(isset($_POST["commande"])){
        $id_doc_mat=urldecode($_POST["ligne"]);
        if(strcmp($_POST["commande"],"SUP")==0) {
        	require_once(modelDir.'model_docmat.php');
            $mon_objet = New DocMat();
            $mon_objet->t_doc_mat['ID_DOC_MAT']=$id_doc_mat;
            $mon_objet->delete();
        }
    }

	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
		$sql=$_SESSION[$mySearch->sessVar]["sql"];
		debug($sql, "gold", true);
		if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

		//Sauvegarde du nombre de lignes de recherche � afficher :
		if($_REQUEST['nbLignes'] != '') {
				$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

		if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = 10;
		$myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

		//@update VG 09/04/2010 : mise en session du nombre de ligne
		$_SESSION[$mySearch->sessVar]["rows"] = $myPage->found_rows;

		print("<div class='errormsg'>".$myPage->error_msg."</div>");
		$param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
		$param["page"] = $myPage->page;

		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);

		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		// VP 08/04/10 : ajout paramètre xmlfile
		$param["xmlfile"]=getSiteFile("listeDir","xml/docMatListe.xml");
		$myPage->addParamsToXSL($param);
		// VP 05/05/10 : changement "dmat" en "docmat"
		$myPage->afficherListe("docmat",getSiteFile("listeDir","docMatListe.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight']);
	}

 ?>
