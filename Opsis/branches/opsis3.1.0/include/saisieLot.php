<?php
/*
 * Created on 26 avr. 10
 *
 */

global $db;
//On gère 3 entités : les documents, les matériels et doc_mat
require_once(libDir."class_modifLot.php");

$myPage=Page::getInstance();

/**
 * Ajoute une valeur (type doc_val, mat_val...) à un tableau de paramètres au format compréhensible par la classe modifLot
 */
function SL_addValeur(&$value, $valueToConcat) {
	$value .=', "'.$valueToConcat.'"';
}

/**
 * Ajoute un id de valeur (type doc_val, mat_val...) à un tableau de paramètres au format compréhensible par la classe modifLot
 *
 * @param (array) $arrParams : tableau de paramètres dans un format compréhensible à la classe modifLot
 * @param $id_val
 */
function SL_addParamId(&$arrParams, $id) {
	if (empty($arrParams['id'])) {
		$arrParams['id'] = array();
	}
	$arrParams['id'][] = $id;
}

/**
 * Ajoute un id de valeur (type doc_val, mat_val...) à un tableau de paramètres au format compréhensible par la classe modifLot
 *
 * @param (array) $arrParams : tableau de paramètres dans un format compréhensible à la classe modifLot
 * @param $id_val
 */
function SL_addParamLexId(&$arrParams, $id, $persOrLex = 'ID_LEX') {
	if (empty($arrParams['id'])) {
		$arrParams['id'] = array();
	}
	$arrParams['id'][] = array($persOrLex => $id);
}


/**
 * @param (string) $type
 * @param (array) $data : tableau de données format classes Doc, Mat...
 * @return (array) : tableau qui contient tous les tableaux paramètres dans un format compréhensible à la classe modifLot
 */
function SL_createArrParam($type, $data) {
	$param = array();
	switch ($type) {
		case "valeur" :
						$param['field'] = 'V_'.$data['ID_TYPE_VAL'];
						if(isset($data['COMMANDE']) && !empty($data['COMMANDE'])){
							$param['commande'] =$data['COMMANDE'] ; 
						}
						if(!empty($data['ID_VAL'])){
							$param['id'] = array($data['ID_VAL']);
						} elseif(!empty($data['VALEUR'])) {
							$param['value'] = $data['VALEUR'];
						} else return false;
						//$param['commande'] = 'ADDVAL';
		break;
		case "cat" :
						$param['field'] = 'CAT_'.$data['CAT_ID_TYPE_CAT'];
						if(isset($data['COMMANDE']) && !empty($data['COMMANDE'])){
							$param['commande'] =$data['COMMANDE'] ; 
						}
						if(!empty($data['ID_CAT'])){
							$param['id'] = array($data['ID_CAT']);
						} elseif(!empty($data['CAT_NOM'])) {
							$param['value'] = $data['CAT_NOM'];
						} else return false;
						//$param['commande'] = 'ADDCAT';
		break;
		case "lex" :
						$param['field'] = 'L_'.$data['ID_TYPE_DESC'].'_'.$data['DLEX_ID_ROLE'].'_'.$data['ID_TYPE_LEX'];
						if(isset($data['COMMANDE']) && !empty($data['COMMANDE'])){
							$param['commande'] =$data['COMMANDE'] ; 
						}
						if(!empty($data['ID_LEX'])){
							$param['id'] = array(array("ID_LEX" => $data['ID_LEX']));
						} elseif(!empty($data['ID_PERS'])) {
							$param['id'] = array(array("ID_PERS" => $data['ID_PERS']));
						} elseif(!empty($data['LEX_TERME'])) {
							$param['value'] = $data['LEX_TERME'];
						} elseif(!empty($data['PERS_NOM'])){
							$param['field'] = 'P_'.$data['ID_TYPE_DESC'].'_'.$data['DLEX_ID_ROLE'].'_'.$data['ID_TYPE_LEX'];
							$param['value'] = $data['PERS_NOM'];
                                                        if(!empty($data['PERS_PRENOM'])){
                                                            $param['value_prenom'] = $data['PERS_PRENOM'];
                                                        }
						} else return false;
						//$param['commande'] = 'ADDVAL';

		break;
		case "pers" :
			$param['field'] = 'P_'.$data['ID_TYPE_DESC'].'_'.$data['DLEX_ID_ROLE'].'_'.$data['ID_TYPE_LEX'];
			if(isset($data['COMMANDE']) && !empty($data['COMMANDE'])){
				$param['commande'] =$data['COMMANDE'] ;
			}
			if(!empty($data['ID_PERS'])) {
				$param['id'] = array(array("ID_PERS" => $data['ID_PERS']));
			} elseif(!empty($data['PERS_NOM'])){
				$param['field'] = 'P_'.$data['ID_TYPE_DESC'].'_'.$data['DLEX_ID_ROLE'].'_'.$data['ID_TYPE_LEX'];
				$param['value'] = $data['PERS_NOM'];
				if(!empty($data['PERS_PRENOM'])){
					$param['value_prenom'] = $data['PERS_PRENOM'];
				}
			} else return false;
			//$param['commande'] = 'ADDVAL';
			
			break;
		case "lien_src" :
						$param['field'] = 'LIENS_SRC';
						if(!empty($data['ID_DOC_DST'])){
							$param['id'] = array(array("ID_DOC_DST" => $data['ID_DOC_DST']));
						} else return false;
						//$param['commande'] = 'ADDVAL';

		break;
		case "lien_dst" :
						$param['field'] = 'LIENS_DST';
						if(!empty($data['ID_DOC_SRC'])){
							$param['id'] = array(array("ID_DOC_SRC" => $data['ID_DOC_SRC']));
						} else return false;
						//$param['commande'] = 'ADDVAL';

		break;
		default :		$param['field'] = key($data);
						$param['value'] = current($data);
						if(isset($data['COMMANDE']) && !empty($data['COMMANDE'])){
							$param['commande'] =$data['COMMANDE'] ; 
						}
						//update VG 06/09/2011 : on teste avec  == '' plutôt que empty(), afin de pouvoir setter la valeur à 0
						if ($param['value'] == '') {
							return false;
						}
		break;
	}

	return $param;
}


$entite=$_REQUEST['entite'];
$commande=$_REQUEST['commande'];
$newModifLot =(empty($commande))?true:false;
$sql = '';
$xmlAff=getSiteFile("formDir",'xml/saisieLot_'.strtolower($entite).'.xml');
//by LD 11 12 08 : possibilité de passer un ID PANIER
//dans ce cas, on créée une "fausse" recherche panier_doc
if ($_REQUEST['id_panier']) {

	$_SESSION['recherche_PANIER_DOC']['sql']="SELECT t_doc.* 
		from t_panier_doc left join t_doc on t_doc.ID_DOC = t_panier_doc.ID_DOC where ID_PANIER=".intval($_REQUEST['id_panier'])." and t_doc.ID_LANG = ".$db->Quote($_SESSION['langue']);
	$_SESSION['recherche_PANIER_DOC']['refine']='';
	$_SESSION['recherche_PANIER_DOC']['tri']='';
	//si on vient par l iframe de modif lot avec un panier on veut pas que l entite soit doc ...
	if (!empty($_REQUEST['tIds'])) {
		$entite='panier_doc';
	}
}

if (!empty($_REQUEST['tIds'])) {

	$ids = $_REQUEST['tIds'];
	$aIds = explode(",",$ids);
	$aIds = array_map(intval,$aIds);
	$strSqlAdd = implode(",",$aIds);

	if ($entite == 'doc') {
		$sql = "SELECT *
			from t_doc where ID_DOC IN(".$strSqlAdd.")";
		if(!empty($_SESSION['langue']))
			$sql.= " AND ID_LANG = '".$_SESSION['langue']."' order by ID_DOC";
			
		$entite = 'doc';
	}

	if ($entite == 'mat' ) {
		$sql = "select M.* from t_mat M WHERE M.ID_MAT IN (".$strSqlAdd.") ";
		if(!empty($_SESSION['langue']))
			$sql.= " AND ID_LANG = '".$_SESSION['langue']."' order by ID_DOC";
	}
	
	if ($entite == 'panier_doc' ) {
		$sql = "SELECT t_doc.*
		from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=".intval($_REQUEST['id_panier'])." AND ID_LIGNE_PANIER IN (".$strSqlAdd."))"; 
		if(!empty($_SESSION['langue']))
			$sql.= " AND t_doc.ID_LANG = ".$db->Quote($_SESSION['langue'])." order by ID_DOC";
	}
	
	if ($entite == 'doc_seq' ) {
		$sql = "SELECT *
			from t_doc where DOC_ID_GEN IN(".$strSqlAdd.")";
		if(!empty($_SESSION['langue']))
			$sql.= " AND ID_LANG = '".$_SESSION['langue']."' order by ID_DOC";
			
		$entite = 'doc';
	}

} else if($_REQUEST['id_panier']) {
	$sql = "SELECT t_doc.*
	from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=".intval($_REQUEST['id_panier']).") and t_doc.ID_LANG = ".$db->Quote($_SESSION['langue'])." order by ID_DOC";
	$entite=  'panier_doc';
}

switch(strtolower($entite)) {
	case 'panier_doc':
		//Ce sont en effet un ou des objets doc que l'on souhaite modifier via le formulaire
		$entiteForm = 'DOC';
		$entiteListe = 'DOC';
		$xmlAff=getSiteFile("formDir",'xml/saisieLot_'.strtolower($entiteForm).'.xml');

	case 'doc':
		require_once(modelDir.'model_doc.php');
		$myObj = new Doc();
		$myObj->t_doc['ID_LANG'] = $_SESSION['langue'];
		$champEntiteVal = 't_doc_val';
		$champEntiteLex = 't_doc_lex';
		$champEntiteCat = 't_doc_cat';
		$champEntiteLiensDST = 't_doc_liesDST';
		$champEntiteLiensSRC = 't_doc_liesSRC';
		$prefix = array("doc");
		
		foreach($_SESSION['arrLangues'] as $langue){
			if($langue != $_SESSION['langue']){
				$myObj->arrVersions[] = array("ID_LANG" => $langue);
			}
		}

		$entiteForm = 'DOC';
		$entiteListe = 'DOC';
		
		break;
	case 'mat':
		require_once(modelDir.'model_materiel.php');
		$myObj=new Materiel();
		$champEntiteVal = 't_mat_val';
		$champEntiteLex = 't_mat_pers';
		$prefix = array("mat");
		break;
	case 'docmat':
		require_once(modelDir.'model_docMat.php');
		$myObj=new DocMat();
		$champEntiteVal = 't_doc_mat_val';
		$prefix = array("dmat", "mat", "doc");
		break;
}
trace($sql);


/**
 * @author B.RAVI 16/06/17 11:21
 * Permet de choisir un xml preferentiel (ex : saisieLot_doc_typeMediaV.xml )suivant le type de média (Photo ou Video) si :
 * - les notices sont de même media_type
 * -ET si le fichier xml preferentiel(ex : saisieLot_doc_typeMediaV.xml ) existe physiquement
 */
if ((!empty($_REQUEST['tIds']) && $entite == 'doc') || (!empty($_REQUEST['id_panier']) )) {
	$tIds_meme_type_media = true;
	if(!empty($_REQUEST['tIds'])) {
		$tIdsNew = $_REQUEST['tIds'];
	} elseif(!empty($_REQUEST['id_panier'])) {
		$aIds = $db->GetCol("select id_ligne_panier from t_panier_doc where id_panier = " . (int) $_REQUEST['id_panier'] );
		$tIdsNew = implode(',',$aIds);
	}
	
	if (!empty($tIdsNew)) {
		foreach (explode(',', $tIdsNew) as $value) {
			if ($entite == 'panier_doc') {
				$sql_type_mediaTID = "SELECT doc_id_media
											from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=" . (int) $_REQUEST['id_panier'] . " AND ID_LIGNE_PANIER=" . (int) $value . ")";
			} else {
				$sql_type_mediaTID = 'SELECT doc_id_media FROM t_doc WHERE id_doc=' . (int) $value;
			}
			

			$type_mediaTID = $db->getOne($sql_type_mediaTID);
			//			var_dump($type_mediaTID);
			if ($type_mediaTID_PREC != $type_mediaTID && $type_mediaTID_PREC !== null) {
				$tIds_meme_type_media = false;
				break;
			}
			$type_mediaTID_PREC = $type_mediaTID;
		}
	}

	if ($tIds_meme_type_media) {
		$xmlPathType = constant("formDir") . 'xml/saisieLot_doc_typeMedia' . $type_mediaTID . '.xml';
		
		//on regarde si le fichier existe (SEULEMENT au niveau du site)
		if (is_file($xmlPathType)) {
			$xmlAff = $xmlPathType;
		}
	}
} else {
	$tIds_meme_type_media = false;
}


$myMod=new ModifLot(strtolower($entite),$xmlAff);
$myMod->getFields();
$myMod->setSearchSQL($newModifLot, $sql);
$result=$myMod->getResults();

if ($result) {

	//On a une commande (action) + pas en train de naviguer (step=0)
	if ($commande === "INIT" || $commande === "INITEMPTY") {
		//Paramètres généraux
		$paramsGen = array(
			'urlaction' => 'saisieLot',
			'commande' => $commande
		);

		$FORM = $_REQUEST;

		$multiArrParams = $paramsGen;
		if(isset($champEntiteLex) && !empty($FORM[$champEntiteLex])) {
			$lastID=-1;
			foreach ($FORM[$champEntiteLex] as $idx=>$lex) {
				if (isset($lex['ID_LEX']) || isset($lex['ID_PERS'])) {
					$lastID=$idx;
					$FORM[$champEntiteLex][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
					if(isset($lex_terme)) {$FORM[$champEntiteLex][$idx]['LEX_TERME']=$lex_terme;$FORM[$champEntiteLex][$idx]['ID_LEX']=0;unset($lex_terme);}
					if(isset($pers_nom)) {$FORM[$champEntiteLex][$idx]['PERS_NOM']=$pers_nom;$FORM[$champEntiteLex][$idx]['ID_PERS']=0;unset($pers_nom);}
				}
				if (isset($lex['LEX_TERME'])) {$lex_terme=$lex['LEX_TERME'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['PERS_NOM'])) {$pers_nom=$lex['PERS_NOM'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['PERS_PRENOM'])) {$FORM[$champEntiteLex][$lastID]['PERS_PRENOM']=$lex['PERS_PRENOM'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['DLEX_ID_ROLE'])) {$FORM[$champEntiteLex][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['ID_TYPE_LEX'])) {$FORM[$champEntiteLex][$lastID]['ID_TYPE_LEX']=$lex['ID_TYPE_LEX'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['DLEX_REVERSEMENT'])) {$FORM[$champEntiteLex][$lastID]['DLEX_REVERSEMENT']=$lex['DLEX_REVERSEMENT'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['LEX_PRECISION'])) {$FORM[$champEntiteLex][$lastID]['LEX_PRECISION']=$lex['LEX_PRECISION'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['ID_TYPE_DESC'])) {$FORM[$champEntiteLex][$lastID]['ID_TYPE_DESC']=$lex['ID_TYPE_DESC'];unset($FORM[$champEntiteLex][$idx]);}
				if (isset($lex['COMMANDE'])) {$FORM[$champEntiteLex][$lastID]['COMMANDE']=$lex['COMMANDE'];unset($FORM[$champEntiteLex][$idx]);}
			}
		}


		// MS 20/02/13 mise en place du support des catégories pour la saisie par lot (recopié à partir du fonctionnement des valeurs)
		if(isset($champEntiteCat) && !empty($FORM[$champEntiteCat])) {
			foreach ($FORM[$champEntiteCat] as $idx=>$cat) {
				if (isset($cat['ID_CAT']) ) {
					$lastID=$idx;
					$FORM[$champEntiteCat][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
					if(isset($cat_nom)) {$FORM[$champEntiteCat][$idx]['CAT_NOM']=$cat_nom;$FORM[$champEntiteCat][$idx]['ID_CAT']=0;unset($cat_nom);}
				}
				if (isset($cat['CAT_NOM'])) {$cat_nom=$cat['CAT_NOM'];unset($FORM[$champEntiteCat][$idx]);}
				if (isset($cat['CAT_ID_TYPE_CAT'])) {
					if(!isset($FORM[$champEntiteCat][$lastID]['CAT_ID_TYPE_CAT']))
						$FORM[$champEntiteCat][$lastID]['CAT_ID_TYPE_CAT']=$cat['CAT_ID_TYPE_CAT'];
					unset($FORM[$champEntiteCat][$idx]);
				}
				if (isset($cat['ID_TYPE_CAT'])) {
					if (!isset($FORM[$champEntiteCat][$lastID]['CAT_ID_TYPE_CAT'])) 
						$FORM[$champEntiteCat][$lastID]['CAT_ID_TYPE_CAT']=$cat['ID_TYPE_CAT'];
					unset($FORM[$champEntiteCat][$idx]);
				}
				if (isset($cat['CAT_CODE'])) {$FORM[$champEntiteCat][$lastID]['CAT_CODE']=$cat['CAT_CODE'];unset($FORM[$champEntiteCat][$idx]);}
				if (isset($cat['COMMANDE'])) {$FORM[$champEntiteCat][$lastID]['COMMANDE']=$cat['COMMANDE'];unset($FORM[$champEntiteCat][$idx]);}
			}
			if(isset($lastID)) {

				/*for($idx=$lastID;$idx>=0;$idx--){
					if(isset($FORM[$champEntiteCat][$idx])){
						if(empty($FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'])) $FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT']=$lastTypeCat;
						else $lastTypeCat=$FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'];
					}
				}*/
				for($idx=0;$idx<=$lastID;$idx++){
					if(isset($FORM[$champEntiteCat][$idx])){
						if(empty($FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'])) $FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT']=$lastTypeCat;
						else $lastTypeCat=$FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'];
						
						if (intval($FORM[$champEntiteCat][$idx]['ID_CAT']) <= 0)
							unset($FORM[$champEntiteCat][$idx]);
					}
				}
				
				$paramsIndex = array();//sauvegarde les typeCats mis en paramètre

				for($idx=$lastID;$idx>=0;$idx--){
					if ($FORM[$champEntiteCat][$idx] == null)
						continue;
						
					//On construit le tableau de paramètres pour la classe modifLot
					//On a déjà créé un paramètre pour ce champ, on le met donc simplement à jour en ajoutant les données
					$lastTypeCat = $FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'];
					if (isset($paramsIndex[$lastTypeCat])) {
						if (!empty($FORM[$champEntiteCat][$idx]['ID_CAT'])) {
							SL_addParamId($multiArrParams[$paramsIndex[$lastTypeCat]], $FORM[$champEntiteCat][$idx]['ID_CAT']);
						} else {
							SL_addValeur($multiArrParams[$paramsIndex[$lastTypeCat]]['value'], $FORM[$champEntiteCat][$idx]['CAT_NOM']);
						}
					//On créé un tableau de paramètre dans un format compréhensible par la classe modifLot
					} else {
						$arrParams = SL_createArrParam('cat', $FORM[$champEntiteCat][$idx]);
						if($arrParams) {
							$multiArrParams[] = $arrParams;
							end($multiArrParams);
							$paramsIndex[$lastTypeCat] = key($multiArrParams);
						}
					}
				}
			}
		}

		if(isset($champEntiteVal)) {
			if (!empty($FORM[$champEntiteVal])){
				//On retravaille les données reçues de manière à avoir une structure correcte
				$lastID=-1;
				foreach ($FORM[$champEntiteVal] as $idx=>$val) {
					if (isset($val['ID_VAL'])) {
						$lastID=$idx;
						$FORM[$champEntiteVal][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
						if(isset($valeur)) {$FORM[$champEntiteVal][$idx]['VALEUR']=$valeur;$FORM[$champEntiteVal][$idx]['ID_VAL']=0;unset($valeur);}
					}
					if (isset($val['VALEUR'])) {$valeur=$val['VALEUR'];unset($FORM[$champEntiteVal][$idx]);}
					if (isset($val['ID_TYPE_VAL'])){						
						if(!isset($FORM[$champEntiteVal][$lastID]['ID_TYPE_VAL'])) {
								$FORM[$champEntiteVal][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];
						}					
						unset($FORM[$champEntiteVal][$idx]);
					}
                                     /*  B.RAVI 2015-04-28 Bug les champs de types valeur(VI) ne prenaient pas en compte le paramètre commande (qui override la valeur de <commande> generale du form ex: INIT) du champ dans le                                         xml ex: <element>
                                                    <chFields>PAYS</chFields>
                                                    <multi_type>VI</multi_type>
                                                    <commande>ADDVAL</commande> 
                                                 </element>
                                      */
                                        if (isset($val['COMMANDE'])) {$FORM[$champEntiteVal][$lastID]['COMMANDE']=$val['COMMANDE'];unset($FORM[$champEntiteVal][$idx]);}
				}
				if(isset($lastID)) {

					for($idx=$lastID;$idx>=0;$idx--){

						if(isset($FORM[$champEntiteVal][$idx])){
							if(empty($FORM[$champEntiteVal][$idx]['ID_TYPE_VAL'])) $FORM[$champEntiteVal][$idx]['ID_TYPE_VAL']=$lastTypeVal;
							else $lastTypeVal=$FORM[$champEntiteVal][$idx]['ID_TYPE_VAL'];
						}

					}

					$paramsIndex = array();//sauvegarde les typeVals mis en paramètre

					for($idx=$lastID;$idx>=0;$idx--){
						//On construit le tableau de paramètres pour la classe modifLot
						//On a déjà créé un paramètre pour ce champ, on le met donc simplement à jour en ajoutant les données
						$lastTypeVal = $FORM[$champEntiteVal][$idx]['ID_TYPE_VAL'];
						if (isset($paramsIndex[$lastTypeVal])) {							
							if (!empty($FORM[$champEntiteVal][$idx]['ID_VAL'])) {
								SL_addParamId($multiArrParams[$paramsIndex[$lastTypeVal]], $FORM[$champEntiteVal][$idx]['ID_VAL']);
							} else {
								SL_addValeur($multiArrParams[$paramsIndex[$lastTypeVal]]['value'], $FORM[$champEntiteVal][$idx]['VALEUR']);
							}
						//On créé un tableau de paramètre dans un format compréhensible par la classe modifLot
						} else {
							$arrParams = SL_createArrParam('valeur', $FORM[$champEntiteVal][$idx]);

							if($arrParams) {
								$multiArrParams[] = $arrParams;
								end($multiArrParams);
								$paramsIndex[$lastTypeVal] = key($multiArrParams);
							}
						}
					}
				}
			}
		}
		if (isset($champEntiteLex) && !empty($FORM[$champEntiteLex])) {
			//On créé les paramètres type lex au format compréhensible de la classe modifLot
			$paramsIndex = 	array();
			foreach ($FORM[$champEntiteLex] as $idx => $lex) {
				$arrParams = SL_createArrParam('lex', $lex);
				if (isset($paramsIndex[$arrParams['field']])) {

					$index = $paramsIndex[$arrParams['field']];
					if (isset($lex['ID_LEX']) && !empty($lex['ID_LEX'])) {
						SL_addParamLexId($multiArrParams[$index], $lex['ID_LEX'], "ID_LEX");

					} elseif (isset($lex['ID_PERS']) && !empty($lex['ID_PERS'])) {
						SL_addParamLexId($multiArrParams[$index], $lex['ID_PERS'], "ID_PERS");

					} elseif (isset($lex['LEX_TERME'])) {
						SL_addValeur($multiArrParams[$index]['value'], $lex['LEX_TERME']);

					} elseif (isset($lex['PERS_NOM'])) {
						SL_addValeur($multiArrParams[$index]['value'], $lex['PERS_NOM']);
						/**
						 * @author B.RAVI 22/06/17 16:55
						 * bug constaté sur mémorial, il faut alimenter nom et prenom (et pas soit nom, soit prenom)
						 */
						if (isset($lex['PERS_PRENOM'])) {
							SL_addValeur($multiArrParams[$index]['value_prenom'], $lex['PERS_PRENOM']);
						}
						
					}

				} else {
					if($arrParams) {
						$multiArrParams[] = $arrParams;
						end($multiArrParams);
						$paramsIndex[$arrParams['field']] = key($multiArrParams);
					}
				}
			}
		}
		
		if (isset($champEntiteLiensSRC) && !empty($FORM[$champEntiteLiensSRC])) {
			$paramsIndex = 	array();
			foreach ($FORM[$champEntiteLiensSRC] as $idx => $lien) {
				$arrParams = SL_createArrParam('lien_src', $lien);
				if (isset($paramsIndex[$arrParams['field']])) {
					$index = $paramsIndex[$arrParams['field']];
					if (isset($lien['ID_DOC_DST'])) {
						SL_addParamId($multiArrParams[$index], $lien['ID_DOC_DST']);
					}
				} else {
					if($arrParams) {
						$multiArrParams[] = $arrParams;
						end($multiArrParams);
						$paramsIndex[$arrParams['field']] = key($multiArrParams);
					}
				}
			}
		}
		if (isset($champEntiteLiensDST) && !empty($FORM[$champEntiteLiensDST])) {
			$paramsIndex = 	array();
			foreach ($FORM[$champEntiteLiensDST] as $idx => $lien) {
				$arrParams = SL_createArrParam('lien_dst', $lien);
				if (isset($paramsIndex[$arrParams['field']])) {
					$index = $paramsIndex[$arrParams['field']];
					if (isset($lien['ID_DOC_SRC'])) {
						SL_addParamId($multiArrParams[$index], $lien['ID_DOC_SRC']);
					}
				} else {
					if($arrParams) {
						$multiArrParams[] = $arrParams;
						end($multiArrParams);
						$paramsIndex[$arrParams['field']] = key($multiArrParams);
					}
				}
			}
		
		}

		//On créé les paramètres type régulier au format compréhensible de la classe modifLot
		foreach ($FORM as $myField => $value) {
			$arrCrit = explode("_",$myField);
			foreach ($prefix as $onePrefix) {
				if(strtolower($arrCrit[0]) === strtolower($onePrefix)) {
					$arrParams = SL_createArrParam('', array($myField => $value));
					if($arrParams) {
						$multiArrParams[] = $arrParams;
					}
				}
			}
		}
		
		if (isset($FORM['version'])){
			$multiArrParams[] = array('field'=>'version','value'=>$FORM['version']);
		}
				
		
		

		debug($multiArrParams, "red", true);
		$myMod->updateFromArray($multiArrParams, true);
		
		
		

		$myMod->applyAllModifs();
	}
	else if ($commande === "SUP_DOC")
	{
		// suppression des documents
		foreach ($result as $doc)
		{
			$tmp_doc=new Doc();
			$tmp_doc->t_doc['ID_DOC']=intval($doc['ID_DOC']);
			$tmp_doc->getDoc();
			$tmp_doc->delete();
		}
		
		// on reinitialise 
		$myMod->setSearchSQL($newModifLot, $sql);
		$result=$myMod->getResults();
	}
	//-------------------------------------//
	//		  AFFICHAGE DE LA LISTE        //
	//-------------------------------------//
	if(gDisplayListeModifLot==true){
		//Fichiers de traitement et d'affichage
		$xslAffListe = "lot.xsl";//XXXXXXXXXX------REMPLACER------XXXXXXXXXX//
		$xmlDataListe=getSiteFile("listeDir","xml/lot_".strtolower($entiteForm).".xml");

		//Mise de Paramètres en session
		//$_SESSION['recherche_'.strtoupper($entite)]['docListeXSL'] = $xslAffListe;
		//Sauvegarde du nombre de lignes de recherche � afficher :

		$myPage->nbLignes = (defined(gNbLignesSaisieLotDefaut))?gNbLignesSaisieLotDefaut:"100";
		$myPage->page = 1;
		//Prépare les données
		$myPage->initPagerFromArray($result); // init splitter object
		$param["nbLigne"] = $myPage->nbLignes;
		$param["page"] = $myPage->page;
		$param["nbRows"]=$myPage->found_rows;
		$param['xmlfile'] = $xmlDataListe;
		$param["urlparams"]=$myPage->addUrlParams();
		$param["offset"] = "0";

		$myPage->addParamsToXSL($param);
		$myPage->afficherListe(strtolower(!empty($entiteForm)?$entiteForm:$entite),getSiteFile("listeDir",$xslAffListe));
	}
}

foreach ($result as $res) {
	require_once(modelDir.'model_doc.php');
	if (strtolower($entite) == "doc" && !empty($res['ID_DOC']) && defined("gReportToHeritFields") && !empty($commande)) {
		$myDoc = new Doc;
		$myDoc->t_doc['ID_DOC']=intval($res['ID_DOC']);
		$myDoc->t_doc['ID_LANG']=$_SESSION['langue'];
		$myDoc->heritFieldsToReport();
	}
}

	//-------------------------------------//
	//		 AFFICHAGE DU FORMULAIRE       //
	//-------------------------------------//

	include (getSiteFile("formDir",'saisieLot.inc.php'));

?>
