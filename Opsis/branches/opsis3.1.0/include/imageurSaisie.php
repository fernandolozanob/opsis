<?
/*
$affichage=1;
if (isset($_REQUEST["affichage"])){$affichage = trim($_REQUEST["affichage"]);}
*/

debug($_POST);

global $db;
require_once(libDir."class_upload.php");
require_once(modelDir.'model_imageur.php');
$myPage=Page::getInstance();
$myPage->nomEntite = kStoryboard;

// I. Initialisation des variables
$myImageur = New Imageur();
$imageur="";
$id_mat="";
$id_doc="";
$id_lang="";

if (isset($_REQUEST["id_mat"])){$id_mat = trim($_REQUEST["id_mat"]);}
if (isset($_REQUEST["id_doc"])){$id_doc = trim($_REQUEST["id_doc"]);}

if(isset($_REQUEST["id_lang"])) $id_lang=$_REQUEST["id_lang"];
else $id_lang = strtoupper($_SESSION["langue"]);

//debug($_POST,'cyan',true);

$myImageur->t_imageur['ID_IMAGEUR']=$_REQUEST['id_imageur'];


// II. Ouverture d'un storyboard
        // II.B. Traitement des actions BDD
        if(!empty($_POST["commande"]) || $_POST['bOk'])
        {
            $action=$_POST["commande"];
            $ligne=$_POST["ligne"];
            $id_image=$_POST["id_image".$ligne];

			if (isset($myDoc))
			{
				$myMat=new Materiel();
				$myMat->t_mat['ID_MAT']=$myDoc->getMaterielVisu();
				$myMat->getMat();
			}
			
            if(strcmp($action,"SELECT")==0) { //Selection d'une association IMAGEUR / MAT ou DOC


                if (isset($myMat)) { // appel depuis un matériel
                	$myMat->saveImageur($_POST['id_imageur']);
                	$myImageur->dropError(kStoryboard." : ".$myMat->error_msg);
                }

                if (isset($myDoc)) { // appel depuis un document
                	$myDoc->saveImageur($_POST['id_imageur']);
                	$myImageur->dropError(kStoryboard." : ".$myDoc->error_msg);
                }
            }

            else if (strcmp($action,'VIGNETTE')==0) { //Choix d'une vignette pour un document

            	if ($myDoc) {
            	$myDoc->saveVignette('DOC_ID_IMAGE',$_POST['vignette']);
            	$myDoc->getVignette();
            	}
            }

			else if (strcmp($action,'GENERER')==0 && $myMat) { //Génération automatique du storyboard

				// BY LD : plus de get des dimensions car fait dans la classe
				$rate=$_POST['sb_rate'];
				if ($_POST['sb_number']) { //si un nb d'image a été renseigné
					$number=$_POST['sb_number'];
					$secs=timeToSec($myMat->t_mat['MAT_DUREE']);
					if ($number>0) $rate=round($secs/$number,3); //intervalle en sec entre images
				}

				$myImageur->generateStoryboard($myMat,$rate,$_dumb);


			}

            else if (strcmp($action,"SUP")==0) { //Suppression d'images

				// VP 6/04/10 : correction bug suppression collectif à la place du mode individuel
            	if ($_POST['img2del']) { //Mode delete individuel
					$myImg= new Image;
            		$myImg->t_image['ID_IMAGE']=$_POST['img2del'];
            		$myImg->deleteImage();
            		unset ($myImg);

            	} else if ($_POST['checked']) { // Mode delete collectif par cases à cocher
	            	foreach ($_POST['checked'] as $idx=>$img) {
	            		$myImg= new Image;
	            		$myImg->t_image['ID_IMAGE']=$idx;
	            		$myImg->deleteImage();
	            		unset ($myImg);
	            	}
	            }
			}
			else if ($action=='SUP_ALL')// GT : suppression de toutes le images du storyboard
			{
				$myImageur->deleteAllImages();//on supprimme toutes les images du storyboard
			}


            // Sauvegarde imageur + images
            else if(strcmp($action,"SAVE")==0 || $_POST['bOk']) {
            	$myImageur->updateFromArray($_POST);
            	$ok=$myImageur->save();



            	foreach ($_POST['ID_IMAGE'] as $idx=>$img) { //sauvegarde de toutes les images affichées

            		$myImg = new Image;
            		$myImg->t_image['ID_IMAGE']=$img;
            		$myImg->t_image['IM_TC']=$_POST['IM_TC'][$idx];
            		$myImg->t_image['IM_TEXT']=$_POST['IM_TEXT'][$idx];
            		$myImg->t_image['IM_CHEMIN']=$_POST['IM_CHEMIN'][$idx];
            		$myImg->t_image['IM_FICHIER']=$_POST['IM_FICHIER'][$idx];
            		$myImg->t_image['ID_LANG']=$_POST['ID_LANG'][$idx];

            		$myImg->save();
            		unset($myImg);


            	}

				//A ne faire que pour cas d'action "select"
            	/*if ($ok >1) { //il y a eu sauvegarde et/ou création ? Il faut mettre à jour l'asso IMAGEUR / DOC ou MAT

	                if (isset($myMat)) { //depuis un matériel? => on met à jour MAT_IMAGEUR
						$myMat->saveImageur($myImageur->t_imageur['ID_IMAGEUR']);
						$myImageur->dropError($myMat->error_msg);
	                }

	                if (isset($myDoc)) { //depuis un un doc ? => on met à jour DOC_IMAGEUR

	                	$myDoc->saveImageur($myImageur->t_imageur['ID_IMAGEUR']);
						$myImageur->dropError($myDoc->error_msg);
	                }

            	}*/


           		if(strlen($_POST["page"])>0){ $myPage->redirect($_POST["page"]); }

			}

     } //fin de la partie traitement

        // partie affichage
        if(isset($myMat) && !isset($myDoc)) { //visu SB depuis l'édition des matériels

        	$myImageur->t_imageur['ID_IMAGEUR']=$myMat->t_mat['MAT_ID_IMAGEUR'];
            $myImageur->getImageur(null,$id_lang);
            $titre=$myMat->t_mat['MAT_NOM']; //by VP 29/04/08
            $addXML=$myMat->xml_export(0,0);

        } else if(isset($myDoc)) { //visu SB depuis les documents

        	if (!empty($myDoc->t_doc['DOC_ID_IMAGEUR'])) {  // NOTE : préférence est donnée à l'imageur directement associé au doc.
        											 // Ceci est fait stt pour assurer la compatibilité avec certains sites (CNRS)
        		$arrImg[]=$myDoc->t_doc['DOC_ID_IMAGEUR'];
        		$arrLimits[]=array("TCIN"=>$myDoc->t_doc['DOC_TCIN'],"TCOUT"=>$myDoc->t_doc['DOC_TCOUT']); //TC du DOC
        		$myImageur->t_imageur['ID_IMAGEUR']=$myDoc->t_doc['DOC_ID_IMAGEUR'];

        	}
	        else { //récup des imageurs de chaque MAT
	        	foreach ($myDoc->t_doc_mat as $dm) {
	        		if (($_POST['DMAT_ID_MAT']=='' && !empty($dm['MAT']->t_mat['MAT_ID_IMAGEUR'])) || $_POST['DMAT_ID_MAT']==$dm['MAT']->t_mat['ID_MAT']) {
	        		$arrImg[$dm['MAT']->t_mat['ID_MAT']]=$dm['MAT']->t_mat['MAT_ID_IMAGEUR'];
	        		$arrLimits[$dm['MAT']->t_mat['ID_MAT']]=array("TCIN"=>$dm['DMAT_TCIN'],"TCOUT"=>$dm['DMAT_TCOUT']); //TC de DOC_MAT
	        		$arrAddParams[$dm['MAT']->t_mat['ID_MAT']]=array("ID_MAT"=>$dm['ID_MAT'],'DMAT_ID_MAT'=>$_POST['DMAT_ID_MAT']);
	        		}
	        	}
        	}

            $myImageur->selectedPic=$myDoc->t_doc['DOC_ID_IMAGE'];  //affectation de la vignette
			
            $myImageur->getImageur($arrImg,$id_lang,$arrLimits,$arrAddParams); //récup imageur(s) + images
 			$addXML=$myDoc->xml_export(0,0);
            $titre=kStoryboard;

	        echo "<div class='pageName' id='menuDocSaisie'>"; //Menu haut (pour le doc)
			include(getSiteFile("designDir","menuDocSaisie.inc.php"));
			echo "</div>";


        } else { // mode edition de storyboard depuis admin

            $myImageur->getImageur(null,$id_lang);
            $titre=kStoryboard;
        }

	if(!isset($retour)) $retour=$myPage->getName()."?urlaction=imageurListe&amp;id_doc=".$id_doc."&amp;id_mat=".$id_mat;

	//debug($myImageur,'gold',true);

	$myPage->titre = (isset($myImageur->t_imageur['IMAGEUR']) && !empty($myImageur->t_imageur['IMAGEUR'])?$myImageur->t_imageur['IMAGEUR']:'');
	$myPage->setReferrer(true);

?>
    <div id="pageTitre" class="title_bar">
		<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>

		<?= $titre ?>
    </div>
  <br/>              <div class='error'><?=$myImageur->error_msg ?></div>
                    <?

                    if(isset($_GET["page"])){
			            $page= $_GET["page"];
			        }
			        else $page="1";

						$myPage->nbLignes=($_REQUEST['nbLignes']?$_REQUEST['nbLignes']:12);
						$myPage->page=$_REQUEST['page'];
						if ($myPage->page=='') $myPage->page=$_REQUEST['rang']; //BY LD XXX expérimental
						$myPage->initPagerFromArray($myImageur->t_images);

			            $param["nbLigne"] = $myPage->nbLignes;
			            $param["page"] = $myPage->page;
			            $param["pager_link"]=$myPage->PagerLink;
						$param["offset"] =  $myPage->nbLignes*($myPage->page-1);
			            $param["urlparams"]=$myPage->addUrlParams();
			            $param["storyboardChemin"]=storyboardChemin;
			            $param["imgurl"]=imgUrl;
			            $param["id_lang"]=$id_lang;
			            $myPage->addParamsToXSL($param);
			            // En plus de la liste des images, on ajoute au XML la liste des imageurs et la liste des langues
						$myPage->afficherListe('t_images',getSiteFile("listeDir",'imageur.xsl'),false,null,$myImageur->xml_export().array2xml($_SESSION['arrLangues'],'t_langues').$addXML);


 ?>


