<?php
require_once(libDir."class_backupJob.php");
require_once(modelDir.'model_tape.php');

if(!function_exists("displayMsg")){

	function displayMsg($str) {
		$time=date('H:i:s',time());
		echo $time." : ".$str."\n";
		if(php_sapi_name()!="cli") {
			echo "<script>if (document.getElementById('myClock')) document.getElementById('myClock').parentNode.removeChild(document.getElementById('myClock'));
	</script><br/><img src='".imgUrl."wait30trans.gif' id='myClock' />"; //on raffiche une pendule à chaque ligne
		}
	}
}

$myPage=Page::getInstance();
if(isset($_POST["action"])){
	
	displayMsg(kDebutTraitement);
	
	switch($_POST["action"]){
		case "backup":
			// Création Job Backup
			$myBackupJob=new BackupJob();
			// Listing cartouches 
			displayMsg(kListageCartouches);
			// VP 20/07/09 : initArrayTapes est appelé depuis updateTapeStatus
			//$ok=$myBackupJob->initArrayTapes();
			$ok=$myBackupJob->updateTapeStatus();
			// VP 5/05/10 : test sur présence cartouche
			if(!empty($myBackupJob->arrayTapes)){
				// Listing fichiers
				displayMsg(kRechercheFichierASauver);
				// Recherche des fichiers créés ou modifiés
				if(!empty($_POST["id_tape_set"])) {
					$sets[]=$_POST["id_tape_set"];
					foreach($sets as $set){
						// VP 3/12/09 : modif appel initArrayFiles
						$myBackupJob->initArrayFiles($set);
						$myBackupJob->updateArrayFiles($set);
						if(count($myBackupJob->arrayFiles)==0) {
							displayMsg(kRienASauvegarder);
						}else{
							// Sauvegarde des fichiers
							$id_job_prec="0";
							foreach($myBackupJob->arrayFiles as $file){
								// Recherche cartouche courante
								//$myBackupJob->getCurrentTape($file);
								// Sauvegarde
                                // VP 18/10/12 : ajout paramètre id_mat
                                // VP 2/07/14 : priorité à 3
								$myBackupJob->createJob($jobObj, "backup", "", 0, $set, "", $file['id'], $file['name'], $file['media'],$id_job_prec,0,3);
								$id_job_prec=$jobObj->t_job['ID_JOB'];
							}
						}
					}
				}else displayMsg(kErrorBackupJobJeuVide);
			}else displayMsg(kErrorBackupJobNoTape);
			break;
			
        case "listing":
            $myBackupJob=new BackupJob();
            $ok=$myBackupJob->updateTapeStatus();
            break;
            
		default:
			// Création Job Backup
			$myBackupJob=new BackupJob();
            // VP 18/10/12 : ajout paramètre id_mat
			$ok=$myBackupJob->createJob($jobObj, $_POST["action"], $_POST["device"], $_POST["id_tape_file"], $_POST["id_tape_set"], $_POST["id_tape"], $_POST["id_mat"], $_POST["fichier"], $_POST["media"]);
			if($ok){
				if(isset($_POST["wait"])){
					if(!$jobObj->waitForJob($_POST["wait"])) displayMsg($jobObj->error_msg);
				}
			} else displayMsg($myBackupJob->error_msg);
			break;
	}

	displayMsg(kFinTraitement);
	if(php_sapi_name()!="cli") {
		echo ( "<script>if (document.getElementById('myClock')) document.getElementById('myClock').parentNode.removeChild(document.getElementById('myClock'));</script>");
        ob_end_flush();
	}

	// VP 17/03/09 : Redirection éventuelle si tout va bien
	if($ok && isset($_POST["suite"])){
		$myPage->redirect($myPage->getName()."?urlaction=".$_POST["suite"]);
	}
	

}else{
	// Dialogue configuration
	$xmlform="<form>
	<fieldset>
		<element>
			<type>hidden</type>
			<name>media</name>
		</element>
		<element>
			<type>hidden</type>
			<name>suite</name>
			<value>jobListe</value>
		</element>
		<element>
			<type>hidden</type>
			<name>id_tape_file</name>
		</element>
		<element>
			<type>select</type>
			<name>action</name>
			<label>Action</label>
			<select_option>listing,backup,restore</select_option>
			<select_lib>listing,backup,restore</select_lib>
			<trtags>both</trtags>
		</element>
		<element>
			<type>select</type>
			<name>device</name>
			<label>Device</label>
			<select_type>sql</select_type>
			<select_sql>select distinct TS_DEVICE as ID,TS_DEVICE as VAL from t_tape_set order by ID</select_sql>
			<trtags>both</trtags>
		</element>
		<element>
			<type>select</type>
			<name>id_tape_set</name>
			<label>Jeu</label>
			<select_type>sql</select_type>
			<select_sql>select distinct ID_TAPE_SET as ID,ID_TAPE_SET as VAL from t_tape_set order by ID</select_sql>
			<trtags>both</trtags>
		</element>
		<element>
			<type>text</type>
			<name>fichier</name>
			<label>Fichier</label>
			<trtags>both</trtags>
		</element>
		<element>
			<type>text</type>
			<name>id_tape</name>
			<label>Cartouche</label>
			<trtags>both</trtags>
		</element>
		<element>
			<type>submit</type>
			<value>OK</value>
			<trtags>both</trtags>
		</element>
	</fieldset>
</form>";
	include(libDir."class_form.php");
	$myForm=new Form;
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
	
}

?>
