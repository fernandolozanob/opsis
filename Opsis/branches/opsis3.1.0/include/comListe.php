<?
require_once(libDir."class_cherchePanier.php");
global $db;

$myPage=Page::getInstance();
$myUser=User::getInstance();
$myPage->titre=kGestionCommandes;;
$myPage->setReferrer(true);
$mySearch=new RecherchePanier();

	if(isset($_GET["init"])) $mySearch->initSession(); // réinit recherche


	
    if(isset($_POST["F_panier_form"])) //Recherche par formulaire
    {
    	$mySearch->prepareSQL();
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
 		if(strpos($_SESSION[$mySearch->sessVar]["sql"], 'GROUP BY')===false){
			$_SESSION[$mySearch->sessVar]["sql"].=" GROUP BY t_panier.id_panier";
		}
    }else if(isset($_GET["defaultSearch"])){
		$mySearch->prepareSQL();
		if(isset($_GET['min_etat_pan']) && !empty($_GET['min_etat_pan']) && is_numeric($_GET['min_etat_pan'])){
			$mySearch->ajouteCritere("AND","PAN_ID_ETAT>='".$_GET['min_etat_pan']."'"); 
		}
		$mySearch->ajouterTri("ID_PANIER","DESC"); 
		$mySearch->finaliseRequete(); //fin et mise en session
	}
    

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION[$mySearch->sessVar]["tri"]=$myPage->getSort();

	
//B.RAVI 2016-10-11 besoin sur sncf pour gérer une liste de commande particulière (bordereau) avec un pan_id_type_commande particulier
//attention ! ne pas oublier de mettre un <input type="hidden" name="type_commande" value="Bordereau"> dans le formulaire par ex
$type_commande=$_REQUEST['type_commande'];
	

// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
//include_once(getSiteFile("formDir","chercheCommande.inc.php"));
if (file_exists(formDir."chercheCommande$type_commande.inc.php")) require_once(formDir."chercheCommande$type_commande.inc.php");
else if (file_exists(formDirCore."chercheCommande$type_commande.inc.php")) require_once(formDirCore."chercheCommande$type_commande.inc.php");
else {
	echo "<div id='pageTitre' class='title_bar'>".kListeCommandes."</div>";

	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheCommande.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="PAN";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
}


    // III. Suppression d'un imageur, des images li�es et mise � jour de t_doc et t_mat
    if(isset($_POST["commande"])){
        $id_panier=urldecode($_POST["ligne"]);
        if(strcmp($_POST["commande"],"SUP")==0) {
        	require_once(modelDir.'model_panier.php');
            $mon_objet = New Panier();
            $mon_objet->t_panier['ID_PANIER']=$id_panier;
            $mon_objet->deletePanier();
        }
    }


       if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql=$_SESSION[$mySearch->sessVar]["sql"];


	    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

	    //Sauvegarde du nombre de lignes de recherche � afficher :
	    if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
	    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

	    if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = gNbLignesListeDefaut;
	    $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

	    print("<div class='errormsg'>".$myPage->error_msg."</div>");

	    $param["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	    $param["page"] = $myPage->page;
        $param["id_doc"]=isset($_REQUEST['id_doc'])?$_REQUEST['id_doc']:null;
		// VP 16/06/09 : ajout paramètre xmlfile
		$param["xmlfile"]=getSiteFile("listeDir","xml/comListe$type_commande.xml");

		// VP 10/07/09 : ajout liste paniers
		require_once(modelDir.'model_panier.php');
		$carts=Panier::getFolders(true);
		$additionnalXML=array2xml($carts,'cart_list');

		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $param["offset"] = "0";
		else $param["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
	    $myPage->addParamsToXSL($param);
	    $myPage->afficherListe("panier",getSiteFile("listeDir","comListe$type_commande.xsl"),false,$_SESSION[$mySearch->sessVar]['highlight'],$additionnalXML);

		//@update VG 07/04/2010 : pour l'ajout d'un lien de retour
	    $myPage->setReferrer(true);

	}

 ?>
