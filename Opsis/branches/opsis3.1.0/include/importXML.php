<?php
    ini_set('memory_limit', '4096M');
    set_time_limit(0);

  $myPage=Page::getInstance();
  $myUser=User::getInstance();

  //fonction d'import et de conversion du contenu d'un fichier
  //par une XSL selon un encodage
  function importFichier($file,$xsl,$file_org,$encodage,$stream='') {

	  if ($stream=='') { //si on a un flux de données, on bypasse la lecture / analyse csv
		  trace("Lecture fichier : $file");
		  $data = file_get_contents($file);
		  //by ld 12 11 08 => traitement de l'extension et parsing txt vers xml si nécessaire
		  $ext = getExtension($file_org);
		  $entite=explode("import",$xsl);$entite=strtoupper($entite[1]); //détection de l'entité sur le nom de la XSL
		  //ATTENTION, ceci requiert une xsl de nom import<Entite>.xsl
          //B.RAVI 2015-06-01 permet de rajouter des sous-types d'xsl pour chaque type de notice tout en recuperant bient l'entité ex:importDOC_TYPEDOC1, importDOC_TYPEDOC2...
          $type_doc=explode('_',$entite);
          $entite=$type_doc[0];
		  
		  // VP 30/11/10 : ajout séparateur tab et changement split en explode
		  if(isset($_POST['csv_separator']) && !empty($_POST['csv_separator'])){
			$separator = $_POST['csv_separator'];
		  }else{
              // Détection séparateur sur première ligne
              if (strpos($data, "\r")) $CR = "\r";
              elseif (strpos($data, "\n")) $CR = "\n";
              $header=substr($data,0,strpos($data,$CR));
              $cntComma=explode(",",$header);$cntComma=count($cntComma);
              $cntSemiColon=explode(";",$header);$cntSemiColon=count($cntSemiColon);
              $cntTab=explode("\t",$header);$cntTab=count($cntTab);
			  $cntMax=max($cntComma,$cntSemiColon,$cntTab);
			  switch($cntMax){
				  case $cntComma:$separator=",";break;
				  case $cntSemiColon:$separator=";";break;
				  case $cntTab:$separator="\t";break;
			  }
		  }
		  //		if ($cntComma>$cntSemiColon) $separator=","; else $separator=";"; //détection du séparateur
		  //		if ($cntTab>$cntSemiColon) $separator=","; else $separator=";"; //détection du séparateur
		  if ($ext=='csv' || $ext=='txt') {
			  trace("Début conversion CSV");
			  $data=csv2xml($file,'DATA',$entite,$separator);
			  $data="<?xml version=\"1.0\" encoding=\"utf-8\"?>".$data;
		  }

	} else $data=$stream;



    if (stripos($data,'ISO-8859-1')!== false || $encodage=='ISO-8859-1') {
        // VP 8/01/13 : encodage caractères spéciaux … ‘ —
        $data=str_replace(array(chr(133),chr(146),chr(150)),array("...","'","-"),$data);
        $data=utf8_encode($data); //Feuilles de match en iso-latin ? on encode en utf8
        $data=str_ireplace('ISO-8859-1','UTF-8',$data);
    }
    if (stripos($data,'MAC-ROMAN')!== false || $encodage=='MAC-ROMAN') {
        $data=macRoman_to_utf8($data); //Feuilles de match en mac-roman ? on encode en utf8
        $data=str_ireplace('MAC-ROMAN','UTF-8',$data);
    }

	//Log xml
	$log = new Logger("data_import.xml");
	$log->Log($data);
	debug($data,'pink');

	//debug($xsl,'red');
    if (!empty($xsl)) {
		// VP 13/10/11 : ajout xml_file_name pour compatibilité finUpload
		$param = array('filename' => stripExtension($file_org), 'xml_file_name' => $file_org);
		
		// B.RAVI 2016-08-10 Eviter Warning: Invalid argument supplied for foreach()
		if (is_object($_POST['params']) || is_array($_POST['params'])) {
			foreach ($_POST['params'] as $fld => $val) {
				$param[$fld] = $val;
			}
		}
		
		//LD 28 05 09 : appel en mode sortie XML (non HTML) pour garder l'encodage des caractères interdits <,>,& etc
		$data = TraitementXSLT($data, getSiteFile("designDir", "print/" . $xsl . ".xsl"), $param, 0, $data, null, false);
	}

	debug($data,'gold');

    return $data;
  }

// FIN importFichier

  // 1ère méthode, on scrute le contenu du répertoire FCPWatchFolder à la recherche de fichiers XML
  if (defined("FCPWatchFolder") && is_dir(FCPWatchFolder)) {
// VP 13/10/11 : initialisation $tab
	$tab=array();
    searchFile(FCPWatchFolder , $tab , ".xml");
    if(count($tab)>0){
      foreach ($tab as $_tab) {
          $file[]=$_tab["pathFile"];
          $file_org[]=basename($_tab["pathFile"]);
      }
      if (defined("FCPWatchFolderImportXSL")) $xsl=FCPWatchFolderImportXSL; 	//1. Constante
      elseif ($_POST['xsl']) $xsl=$_POST['xsl'];								//2. Sinon, param POST
      else $xsl="importXML";													//3. Sinon ImportXML (classique)
    }

  }

  //2ème méthode si la première n'a rien renvoyé, on traite les fichiers envoyés par formulaire
  if(empty($file) && isset($_POST["bOK"]) && $_FILES["importFichier"]["tmp_name"]!='') { //Envoi formulaire avec fichier
  // sinon récupération dialogue POST
    if(($_FILES["importFichier"]["size"]>0)&& ($_POST["xsl"]!="")) {
      $file[]=$_FILES["importFichier"]["tmp_name"];
      $file_org[]=$_FILES["importFichier"]["name"];
      $xsl=$_POST["xsl"];
    }
  }

  //3ème méthode : depuis 1 ou plusieurs url
 if(empty($file) && isset($_POST["bOK"]) && $_POST["url"]) { //Envoi formulaire avec fichier

 	 	$stream='';
	 	foreach($_POST["url"] as $url) {
	 	 	if ($_REQUEST["merge"]) {
	 	 	$content=file_get_contents($url);
	 	 	$content=str_replace("<?xml version=\"1.0\" encoding=\"utf-8\" ?>","",$content);
	 	 	$stream.=$content;
	 	 	//debug($content,'pink');
	 	 	} else {
	 	 		 $file[]=$url;
	 	 		 $file_org[]=$url;
	 	 	}
	 	}
	 	if ($_REQUEST["merge"]) {$file[]='stream'; $file_org[]='Stream';$stream="<?xml version=\"1.0\" encoding=\"utf-8\" ?><xml>".str_replace("&amp;","&amp;amp;",$stream)."</xml>";}
	 	$xsl=$_POST["xsl"];
 }


  debug($file,'beige');
  //Ici la liste des fichiers à importer est faite


  //Mode de traitement d'import : réel ou simulation ?
  if ($_REQUEST['simul']) {
    require_once(modelDir.'model_materiel_simul.php');
    require_once(modelDir.'model_doc_simul.php');
    require_once(modelDir.'model_docMat_simul.php');
    $simul=true;
  } else {
    require_once(modelDir.'model_materiel.php');
    require_once(modelDir.'model_doc.php');
    require_once(modelDir.'model_docMat.php');
    require_once(modelDir.'model_lexique.php');
  }


 	//Cas des doublons, abandon ou remplacement ?
 	if (isset($_REQUEST['fail_if_doublon'])) $fail_if_doublon=true; else $fail_if_doublon=false;


  // On a au moins un fichier & une xsl d'import ?
  if(isset($file) && isset($xsl)) {

    //Affichage "patientez"
	if(php_sapi_name()!="cli"){
		echo "<div id='waiter'>".kPatientez."<br/>";
		echo "<img src='".imgUrl."wait30trans.gif' id='clock' /></div>";
	}

	//Pour chaque fichier
    foreach ($file as $_idx=>$_file) {

	//Si on a dépassé la limite d'import, on arrête
	//Cette limite est utilisée pour ne pas importer trop de fichiers d'un coup
	// =>expiration de script et/ou saturation mySQL
	if (defined("gImportXMLMaxFileCount") && $_idx > gImportXMLMaxFileCount ) continue;

    $cntDoc=0;
    $cntMat=0;
    $cntDMat=0;
	$cntPers=0;
	$cntPrex=0;
	$cntVal=0;
	$cntLex=0;
		
    $xmldata=importFichier($_file,$xsl,$file_org[$_idx],$_REQUEST['encodage'],$stream);
    $datas=xml2tab($xmldata);
	debug($datas,'yellow');

	//VP 5/06/09 : cas où DATA est vide
	if(empty($datas['DATA'])) $datas['DATA']=$datas[''];

	//VP 2/10/09 : ajout possibilité de purge
	if (isset($_POST['purge'])) $purge=$_POST['purge'];
	else $purge=false;

	// Import Documents
    foreach ($datas['DATA'][0]['DOC'] as $i=>$doc) {
      if ($simul){
		$myDoc=new Doc_Simul();
	}else{
		$myDoc=new Doc();
	}
	 if(isset($doc['ID_LANG']) && !empty($doc['ID_LANG'])){
		$myDoc->t_doc['ID_LANG'] = $doc['ID_LANG'];
	  }else{
		$myDoc->t_doc['ID_LANG']=$_SESSION['langue'];
      }
	  if(!$fail_if_doublon && isset($doc['ID_DOC']) && isset($_POST['maj_existant']) && $_POST['maj_existant']==1){
		$myDoc->t_doc['ID_DOC'] = $doc['ID_DOC'];
		$myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1);
		// MS 12.01.15 - puisqu'on récupère l'ensemble des valeurs / lexiques / mat etc ... on set purge=true car on va de toute facon réenregistrer les valeurs actuelles récupérées par getDocFull
		if (!isset($_POST['purge'])) {$purge = true ; }
	  }


      $myDoc->mapFields($doc);
      $myDoc->doc_trad=1; //ttes versions
      $ok=$myDoc->save(0,$fail_if_doublon); //Ici, on ne sauve que les chmaps présents dans mapFields

      if ($ok) {
      	//debug($myDoc,'#'.substr(md5(microtime()), 0, 6));
        $myDoc->saveDocLex($purge); //sans purge par défaut
        $myDoc->saveDocVal($purge); //sans purge par défaut
        $myDoc->saveDocFest($purge); //sans purge par défaut
		$myDoc->saveDocCat($purge); //sans purge par défaut
        $myDoc->saveDocMat($purge); //pas la peine de sauver les mat à la volée, car c'est fait dans mapFields au dessus
        $myDoc->updateChampsXML('DOC_LEX');$myDoc->updateChampsXML('DOC_VAL');$myDoc->updateChampsXML('DOC_XML');$myDoc->updateChampsXML('DOC_CAT');
        $myDoc->saveSequences();
		$myDoc->saveDocAcc();
        $cntDoc++;
      }
      $debrief[$i]['doc']['simul']=$myDoc->debrief;
      $debrief[$i]['doc']['file']=$file_org[$_idx];
      $debrief[$i]['doc']['titre']=($myDoc->t_doc['DOC_TITRE']!=''?$myDoc->t_doc['DOC_TITRE']:$myDoc->t_doc['DOC_TITREORI']);
      $debrief[$i]['doc']['id_doc']=$myDoc->t_doc['ID_DOC'];
      if ($ok && $myDoc->t_doc_seq) $debrief[$i]['doc']['doc_seq']=count($myDoc->t_doc_seq);
      if ($ok && $myDoc->t_doc_mat) $debrief[$i]['doc']['doc_mat']=count($myDoc->t_doc_mat);
      if ($ok && $myDoc->t_doc_val) $debrief[$i]['doc']['doc_val']=count($myDoc->t_doc_val);
      if ($ok && $myDoc->t_doc_lex) $debrief[$i]['doc']['doc_lex']=count($myDoc->t_doc_lex);
      if ($ok && $myDoc->t_doc_fest) $debrief[$i]['doc']['doc_fest']=count($myDoc->t_doc_fest);
      if ($ok && $myDoc->t_doc_acc) $debrief[$i]['doc']['doc_acc']=count($myDoc->t_doc_acc);
      if ($ok && $myDoc->t_images) $debrief[$i]['doc']['image']=count($myDoc->t_images);


	if (!$simul){
      if (defined("useSinequa") && useSinequa) $myDoc->saveSinequa();
      if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true)) $myDoc->saveSolr();
    }

      $debrief[$i]['doc']['error_msg']=(trim($myDoc->error_msg,'<br/>')!=''?$myDoc->error_msg:kSuccesDocSauve);
      unset($myDoc);
    }
	//if ($datas['DATA'][0]['DOC'] && !$simul) Doc::setAutoIncrement();

	// B.RAVI 2016-08-10 Eviter Warning: Invalid argument supplied for foreach()
	if (is_object($datas['DATA'][0]['MAT']) || is_array($datas['DATA'][0]['MAT'])) {
		//Import Matériels
		foreach ($datas['DATA'][0]['MAT'] as $i => $mat) {

			if ($simul)
				$myMat = new Materiel_Simul();
			else
				$myMat = new Materiel();
			$myMat->mapFields($mat);
			$ok = $myMat->save(); //Ici, on ne sauve que les chmaps présents dans mapFields
			if ($ok) {
				//debug($myMat,'#'.substr(md5(microtime()), 0, 6));
				$myMat->saveMatVal();
				$myMat->saveDocMat();
				$cntMat++;
			}
			$debrief[$i]['mat']['simul'] = $myMat->debrief;
			$debrief[$i]['mat']['file'] = $mat['FILENAME'];
			$debrief[$i]['mat']['id_mat'] = $myMat->t_mat['ID_MAT'];
			if ($ok && $myMat->t_doc_mat)
				$debrief[$i]['mat']['doc_mat'] = count($myMat->t_doc_mat);
			if ($ok && $myMat->t_mat_val)
				$debrief[$i]['mat']['mat_val'] = count($myMat->t_mat_val);
			$debrief[$i]['mat']['error_msg'] = (trim($myMat->error_msg, '<br/>') != '' ? $myMat->error_msg : kSuccesMaterielSauve);
			unset($myMat);
		}
	}


	//Import Valeurs
    if (is_object($datas['DATA'][0]['VAL']) || is_array($datas['DATA'][0]['VAL'])) {
        foreach ($datas['DATA'][0]['VAL'] as $i=>$val) {

            $myVal=new Valeur();
            $ok = $myVal->createFromArray($val);
            if ($ok != false) {
                $cntVal++;
            }
            $debrief[$i]['val']['type']=$val['VAL_ID_TYPE_VAL'];
            $debrief[$i]['val']['valeur']=$val['VALEUR'];
            $debrief[$i]['val']['res']=$ok;
          
            unset($myVal);
        }
    }

	//Import Lexique
    if (is_object($datas['DATA'][0]['LEX']) || is_array($datas['DATA'][0]['LEX'])) {
        $lastFatherId = array();
        foreach ($datas['DATA'][0]['LEX'] as $i=>$lex) {
            $level = intval ($lex["LEVEL"]);
            if (isset($lex["PRE_FATHER"]) && $lex["PRE_FATHER"] = "1" && isset($lastFatherId[$level-1]))
                $lex['arrFather'][0]['ID_LEX'] = $lastFatherId[$level-1];
            if (!isset($lex["LEX_ID_ETAT_LEX"]))
                $lex['LEX_ID_ETAT_LEX'] = 2;
            $myLex=new Lexique();
            $ok = $myLex->createFromArray($lex);
            if ($ok) {
                $cntLex++;
                $lastFatherId[$level] = $ok;
            }
            $debrief[$i]['lex']['type']=$lex['LEX_ID_TYPE_LEX'];
            $debrief[$i]['lex']['terme']=$lex['LEX_TERME'];
            $debrief[$i]['lex']['res']=$ok;
            
            unset($myLex);
        }
    }
	
	// Import Doc Mat (aka Supports)
	// B.RAVI 2016-08-10 Eviter Warning: Invalid argument supplied for foreach()
	if (is_object($datas['DATA'][0]['DMAT']) || is_array($datas['DATA'][0]['DMAT'])) {
		foreach ($datas['DATA'][0]['DMAT'] as $i => $dmat) {

			if ($simul)
				$myDMat = new DocMat_Simul();
			else
				$myDMat = new DocMat();
			$myDMat->mapFields($dmat);
			$ok = $myDMat->create($fail_if_doublon);


			if ($ok) {
				$myDMat->saveDocMatVal();
				$cntDMat++;
			}
			$debrief[$i]['dmat']['simul'] = $myDMat->debrief;
			$debrief[$i]['dmat']['file'] = $dmat['FILENAME'];
			$debrief[$i]['dmat']['id_doc_mat'] = $myDMat->t_doc_mat['ID_DOC_MAT'];
			if ($ok && $myDMat->t_doc_mat_val)
				$debrief[$i]['dmat']['doc_mat_val'] = count($myDMat->t_doc_mat_val);
			$debrief[$i]['dmat']['error_msg'] = (trim($myDMat->error_msg, '<br/>') != '' ? $myDMat->error_msg : kSuccesDocMatSauve);
			unset($myDMat);
		}
	}
		
	// Import Personne
	if (is_object($datas['DATA'][0]['PERS']) || is_array($datas['DATA'][0]['PERS'])) {
		foreach ($datas['DATA'][0]['PERS'] as $i => $pers) {
			require_once(modelDir.'model_personne.php');
			if (!$simul) {
				$myPers = new Personne();
				$ok = $myPers->createFromArray($pers);
				if ($ok) {
					$cntPers++;
				}
				/*
				$myPers->mapFields($pers);
				$ok = $myPers->save();
				if ($ok) {
					$myPers->savePersLex();
					$myPers->savePersVal();
					$cntPers++;
				}*/
				unset($myPers);
			}
		}
	}

    //PC 19/11/10 : Import Droits Préexistants
	// B.RAVI 2016-08-10 Eviter Warning: Invalid argument supplied for foreach()
	if (is_object($datas['DATA'][0]['PREX']) || is_array($datas['DATA'][0]['PREX'])) {
		foreach ($datas['DATA'][0]['PREX'] as $i => $prex) {
			require_once(modelDir.'model_doc_prex.php');
			if (!$simul) {
				$myPrex = new DroitPrex();
				if (isset($_POST['ID_DOC']))
					$myPrex->t_prex['ID_DOC'] = $_POST['ID_DOC'];
				$myPrex->mapFields($prex);
				$ok = $myPrex->save();
				if ($ok) {

					$myPrex->savePersonne();
					$myPrex->saveValeurs();
					$cntPrex++;
				}
				unset($myPrex);
			}
		}
	}
		//Fin
		// VP 17/03/10 : pas de renommage du fichier si url ou post
	if(empty($_FILES["importFichier"]["tmp_name"]) && empty($_POST["url"])){
		@mv_rename($_file,$_file.".old"); //renommage pour éviter un éternel réimport...
	}
  }
  //fin de la boucle

  //LD 20 02 09 => copie de fichiers demandée (et pas en mode simul)
	if ($_REQUEST['copie'] && !$simul ) {
		if (defined("kExportDir") && is_dir(kExportDir)) {
		$typecopy=split(",",$_REQUEST['copie']);

			if (in_array('doc_acc',$typecopy)) { //on copie les doc acc

				$da_copy=array();
				searchFile(kExportDir."doc_acc" , $da_copy);
				debug($da_copy,'pink');
				foreach ($da_copy as $_da) {
					if (copy($_da['pathFile'],kDocumentDir.basename($_da['pathFile']))) {
						unlink($_da['pathFile']);
						$debrief['file_copy']['doc_acc']++;
					}
				}
				unset($da_copy);
			}

			if (in_array('mat',$typecopy)) { //on copie les doc acc
				$da_copy=array();
				searchFile(kExportDir."videos" , $da_copy);
				debug($da_copy,'pink');
				foreach ($da_copy as $_da) {
					$lieu=Materiel::setLieuByIdMat(basename($_da['pathFile']));
					if (!is_dir($lieu['LIEU_PATH'])) mkdir($lieu['LIEU_PATH']);
					if (mv_rename($_da['pathFile'],$lieu['LIEU_PATH'].basename($_da['pathFile']))) {
						$debrief['file_copy']['mat']++;
					}
				}
				unset($da_copy);
			}

			if (in_array('image',$typecopy)) { //on copie les doc acc
				$da_copy=array();
				searchFile(kExportDir."storyboard" , $da_copy);
				debug($da_copy,'pink');
				foreach ($da_copy as $_da) {
					$chemin=str_replace(kExportDir."storyboard/","",$_da['pathFile']);
					debug($chemin,'cyan');
					list($imageur,$image)=split("\/",$chemin);
					//echo $imageur."    ".$image;
					if (!is_dir(kStoryboardDir.$imageur)) mkdir(kStoryboardDir.$imageur);
					if (copy($_da['pathFile'],kStoryboardDir.$imageur."/".$image)) {
						unlink($_da['pathFile']);
						rmdir(kExportDir."storyboard/".$imageur); //note : ne supprimera réellement que si rép. vide
						$debrief['file_copy']['image']++;
					}
				}
			}

		} else $errormsg=kErrorExportDirNonDefinie;
	}

	//on kill le waiter
	if(php_sapi_name()!="cli") echo "<script>if (document.getElementById('waiter')) document.getElementById('waiter').parentNode.removeChild(document.getElementById('waiter'))</script>";

	}
	/* MS - 03.05.16 - déplacé dans importXML.inc.php 
	else {
		if(php_sapi_name()!="cli") print "<div class='error'>Merci de sélectionner un fichier.</div>";
	}*/

// VP 22/6/09 : ajout test sur php_sapi_name
 if(php_sapi_name()!="cli") include(getSiteFile("formDir",'importXML.inc.php'));
?>
