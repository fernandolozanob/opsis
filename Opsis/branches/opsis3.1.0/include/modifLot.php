<?php
/*
 * Created on 26 avr. 10
 *
 */

global $db;

//On gère 3 entités : les documents, les matériels et doc_mat
require_once(libDir."class_modifLot.php");
$myUsr = User::getInstance();

$myPage = Page::getInstance();


$entite=$_REQUEST['entite'];
$commande=$_REQUEST['commande'];
$newModifLot =(empty($commande))?true:false;
$sql = '';


	if($_REQUEST['action']=='modifLot'){
		if (!empty($_REQUEST['tIds'])) {

			$strSqlAdd = $_REQUEST['tIds'];

			if ($entite == 'doc') {
				$sql = "SELECT distinct ID_DOC,'" . $_SESSION['langue'] . "' as ID_LANG
					from t_doc where ID_DOC IN(" . $strSqlAdd . ")";
			}

			if ($entite == 'panier_doc') {
				$sql = "SELECT t_doc.*
				from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=" . $_REQUEST['id_panier'] . " AND ID_LIGNE_PANIER IN (" . $strSqlAdd . "))";
				if (!empty($_SESSION['langue']))
					$sql.= " AND t_doc.ID_LANG = " . $db->Quote($_SESSION['langue']);

				$entite = 'doc';
			}

			if ($entite == 'mat') {
				$sql = "select M.* from t_mat M WHERE M.ID_MAT IN (" . $strSqlAdd . ") ";
				//$entite=  'panier_mat';
			}

		//	$_SESSION['recherche_'.strtoupper($entite)]['sql']=$sql;
		//	$_SESSION['recherche_'.strtoupper($entite)]['refine']='';
		//	$_SESSION['recherche_'.strtoupper($entite)]['tri']='';
		} elseif ($_REQUEST['id_panier']) {
			$sql = "SELECT distinct ID_DOC,'" . $_SESSION['langue'] . "' as ID_LANG
			from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=" . $_REQUEST['id_panier'] . ")";
			$entite = 'doc';
		}elseif ($_REQUEST['id_import']) {
			$sql = "SELECT distinct ID_DOC,'" . $_SESSION['langue'] . "' as ID_LANG
			from t_doc where DOC_ID_IMPORT=".$db->Quote($_REQUEST['id_import']);
			$entite = 'doc';
		}
		$xmlPath = getSiteFile("formDir", 'xml/modifLot_' . strtolower($entite) . '.xml');



		/* B.RAVI 2016-09-15 Besoin sur le site du CNRS (ou la valeur d'une session determine le type de xml qu'on doit prendre (dans ce cas, on a pas besoin de check que les doc soit tous du même type doc */

		if (!empty($_REQUEST['type_doc']) &&  $entite == 'doc')  {
			$xmlPathType = constant("formDir") . 'xml/modifLot_doc_' . $_REQUEST['type_doc'] . '.xml';
			if (is_file($xmlPathType)) {
				$xmlPath = $xmlPathType;
				goto xmlPathFINISHED;
			}
		}

		/* B.RAVI 2016-05-19 Besoin sur le site Mémorial de la Shoah pouvoir changer le xml suivant le doc_id_type_doc
		 * Si les notices selectionnés sont tous du même type (doc_id_type_doc) et que le fichier modifLot_doc_type[doc_id_type_doc] existe, on prend celui-ci de préference (attention ne pas changer la structure de nom du xml car autres sites) */

	//																+

		/* B.RAVI 2016-10-03 Besoin sur le site SNCF pouvoir changer le xml suivant le doc_id_media
		 * Si les notices selectionnés sont tous du même type (doc_id_media) et que le fichier modifLot_doc_typeMedia[doc_id_media] existe, on prend celui-ci de préference (attention ne pas changer la structure de nom du xml car autres sites) */


		global $db;


		if (!empty($_REQUEST['tIds']) && ($entite == 'doc' || ($entite == 'panier_doc' && !empty($_REQUEST['id_panier']) ) )) {
			$tIds_meme_type_doc = true;
			$tIds_meme_type_media = true;
			//des qu'il y 1 doc_id_type_doc différent du précédent,on break; cela veut dire qu'ils ont pas tous du même type
			foreach (explode(',', $_REQUEST['tIds']) as $value) {
	//			if ($entite == 'panier_doc') {//on ne peut pas utiliser ceci car $entite = 'doc'; au haut du fichier
				if (!empty($_REQUEST['id_panier'])){
					$sql_type_docTID = "SELECT doc_id_type_doc
										from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=" . (int) $_REQUEST['id_panier'] . " AND ID_LIGNE_PANIER=" . (int) $value . ")";
				} else {
					$sql_type_docTID = 'SELECT doc_id_type_doc FROM t_doc WHERE id_doc=' . (int) $value;
				}

				$type_docTID = (int) $db->getOne($sql_type_docTID);

				if ($type_docTID_PREC != $type_docTID && $type_docTID_PREC !== null) {
					$tIds_meme_type_doc = false;
					break;
				}
				$type_docTID_PREC = $type_docTID;
			}


			foreach (explode(',', $_REQUEST['tIds']) as $value) {
				//			if ($entite == 'panier_doc') {//on ne peut pas utiliser ceci car $entite = 'doc'; au haut du fichier
				if (!empty($_REQUEST['id_panier'])){
					$sql_type_mediaTID = "SELECT doc_id_media
										from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=" . (int) $_REQUEST['id_panier'] . " AND ID_LIGNE_PANIER=" . (int) $value . ")";
				} else {
					$sql_type_mediaTID = 'SELECT doc_id_media FROM t_doc WHERE id_doc=' . (int) $value;
				}
				
				
				$type_mediaTID = $db->getOne($sql_type_mediaTID);
				if ($type_mediaTID_PREC != $type_mediaTID && $type_mediaTID_PREC !== null) {
					$tIds_meme_type_media = false;
					break;
				}
				$type_mediaTID_PREC = $type_mediaTID;
			}


			if ($tIds_meme_type_doc) {
				$xmlPathType = constant("formDir") . 'xml/modifLot_doc_type' . $type_docTID . '.xml';
	//			echo '$xmlPathType '.$xmlPathType.'<br />';
				//on regarde si le fichier existe (SEULEMENT au niveau du site)
				if (is_file($xmlPathType)) {
					$xmlPath = $xmlPathType;
				}
			}
			
			if ($tIds_meme_type_media) {
				$xmlPathType = constant("formDir") . 'xml/modifLot_doc_typeMedia' . $type_mediaTID . '.xml';
	//			echo '$xmlPathType '.$xmlPathType.'<br />';
				//on regarde si le fichier existe (SEULEMENT au niveau du site)
				if (is_file($xmlPathType)) {
					$xmlPath = $xmlPathType;
				}
			}
		} else {
			//on met ici pour pour pouvoir utiliser à la suite dans modifLot.inc.php
			$tIds_meme_type_doc = false;
			$tIds_meme_type_media = false;
		}

	xmlPathFINISHED:

	//echo '$xmlPathType FINAL' . $xmlPath . '<br />';

	$myMod = new ModifLot($entite, $xmlPath);

	$myMod->getFields();
	$myMod->setSearchSQL($newModifLot, $sql);

	//Héritage des champs sur les séquences

	if (defined("gInheritedFields") && in_array($_REQUEST['field'], explode('/', gInheritedFields))) {
		$sqlFrom = "SELECT t1.ID_DOC " . substr($myMod->sql, strpos($myMod->sql, "from"));
		$sqlFrom = substr($sqlFrom, 0, strpos($sqlFrom, "ORDER"));
		$sql = str_replace("WHERE ", "WHERE t1.DOC_ID_GEN in ($sqlFrom) OR ", $myMod->sql);
		$myMod->sql = $sql;
	}
	$hasResult = $myMod->getResults();
	if ($hasResult) {
		// enregistrement de  la modification par lot ( initialiser / ajouter valeur ect)
		if ($commande && $_REQUEST['modif_lot'] && $_REQUEST['step'] == '0') {

			$myMod->updateFromArray($_REQUEST);
			$myMod->applyModif();
		}

		$rang = $_REQUEST['rang'] ? $_REQUEST['rang'] : 0;
		$rang+=$_REQUEST['step'];
	}

	//SAISIE LOT


	$xmlAff=getSiteFile("formDir",'xml/saisieLot_'.strtolower($entite).'.xml');
	//by LD 11 12 08 : possibilité de passer un ID PANIER
	//dans ce cas, on créée une "fausse" recherche panier_doc
	if ($_REQUEST['id_panier']) {

		$_SESSION['recherche_PANIER_DOC']['sql']="SELECT t_doc.* 
			from t_panier_doc left join t_doc on t_doc.ID_DOC = t_panier_doc.ID_DOC where ID_PANIER=".intval($_REQUEST['id_panier'])." and t_doc.ID_LANG = ".$db->Quote($_SESSION['langue']);
		$_SESSION['recherche_PANIER_DOC']['refine']='';
		$_SESSION['recherche_PANIER_DOC']['tri']='';
		//si on vient par l iframe de modif lot avec un panier on veut pas que l entite soit doc ...
		if (!empty($_REQUEST['tIds'])) {
			$entite='panier_doc';
		}
	}

	if (!empty($_REQUEST['tIds'])) {
		$ids = $_REQUEST['tIds'];
		$aIds = explode(",",$ids);
		$aIds = array_map(intval,$aIds);
		$strSqlAdd = implode(",",$aIds);

		if ($entite == 'doc') {
			$sql = "SELECT t_doc.*, t_doc_acc.DA_FICHIER, t_doc_acc.DA_CHEMIN,t_image.IM_CHEMIN, t_image.IM_FICHIER
				from t_doc 
				left join t_image  on t_image.ID_IMAGE=t_doc.DOC_ID_IMAGE and t_image.ID_LANG=t_doc.ID_LANG
				left join t_doc_acc  on t_doc_acc.ID_DOC_ACC=t_doc.DOC_ID_DOC_ACC and t_doc_acc.ID_LANG=t_doc.ID_LANG
				where t_doc.ID_DOC IN(".$strSqlAdd.")";
			if(!empty($_SESSION['langue']))
				$sql.= " AND t_doc.ID_LANG = '".$_SESSION['langue']."'";
				
			$entite = 'doc';
		}
		if ($entite == 'mat' ) {
			$sql = "select M.* from t_mat M WHERE M.ID_MAT IN (".$strSqlAdd.") ";
			if(!empty($_SESSION['langue']))
				$sql.= " AND ID_LANG = '".$_SESSION['langue']."'";
		}
		
		if ($entite == 'panier_doc' ) {
			$sql = "SELECT t_doc.*, t_doc_acc.DA_FICHIER, t_doc_acc.DA_CHEMIN,t_image.IM_CHEMIN, t_image.IM_FICHIER
			from t_doc
			left join t_image  on t_image.ID_IMAGE=t_doc.DOC_ID_IMAGE and t_image.ID_LANG=t_doc.ID_LANG
			left join t_doc_acc  on t_doc_acc.ID_DOC_ACC=t_doc.DOC_ID_DOC_ACC and t_doc_acc.ID_LANG=t_doc.ID_LANG
			where t_doc.ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=".intval($_REQUEST['id_panier'])." AND ID_LIGNE_PANIER IN (".$strSqlAdd."))"; 
			if(!empty($_SESSION['langue']))
				$sql.= " AND t_doc.ID_LANG = ".$db->Quote($_SESSION['langue']);
		}
		
		if ($entite == 'doc_seq' ) {
			$sql = "SELECT *
				from t_doc where DOC_ID_GEN IN(".$strSqlAdd.")";
			if(!empty($_SESSION['langue']))
				$sql.= " AND ID_LANG = '".$_SESSION['langue']."'";
				
			$entite = 'doc';
		}

	} else if($_REQUEST['id_panier']) {
		$sql = "SELECT t_doc.*
		from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=".intval($_REQUEST['id_panier']).") and t_doc.ID_LANG = ".$db->Quote($_SESSION['langue']);
		$entite=  'panier_doc';
	}

	switch(strtolower($entite)) {
		case 'panier_doc':
			//Ce sont en effet un ou des objets doc que l'on souhaite modifier via le formulaire
			$entiteForm = 'DOC';
			$entiteListe = 'DOC';
			$xmlAff=getSiteFile("formDir",'xml/saisieLot_'.strtolower($entiteForm).'.xml');

		case 'doc':
			require_once(modelDir.'model_doc.php');
			$myObj = new Doc();
			$myObj->t_doc['ID_LANG'] = $_SESSION['langue'];
			$champEntiteVal = 't_doc_val';
			$champEntiteLex = 't_doc_lex';
			$champEntiteCat = 't_doc_cat';
			$champEntiteFest = 't_doc_fest';
			$champEntiteLiensDST = 't_doc_liesDST';
			$champEntiteLiensSRC = 't_doc_liesSRC';
			$champEntiteTextLex = 'text_doc_lex';
			$champEntiteTextVal = 'text_doc_val';
			$prefix = array("doc");
			
			foreach($_SESSION['arrLangues'] as $langue){
				if($langue != $_SESSION['langue']){
					$myObj->arrVersions[] = array("ID_LANG" => $langue);
				}
			}

			$entiteForm = 'DOC';
			$entiteListe = 'DOC';
			
			break;
		case 'mat':
			require_once(modelDir.'model_materiel.php');
			$myObj=new Materiel();
			$champEntiteVal = 't_mat_val';
			$prefix = array("mat");
			break;
		case 'docmat':
			require_once(modelDir.'model_docMat.php');
			$myObj=new DocMat();
			$champEntiteVal = 't_doc_mat_val';
			$prefix = array("dmat", "mat", "doc");
			break;
	}

	if (!empty($_REQUEST['tIds']) && ($entite == 'doc' || ($entite == 'panier_doc' && !empty($_REQUEST['id_panier']) ) )) {
		$tIds_meme_type_media = true;
		$tIds_meme_type_media = true;
		foreach (explode(',', $_REQUEST['tIds']) as $value) {

			if ($entite == 'panier_doc') {
				$sql_type_mediaTID = "SELECT doc_id_media
											from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=" . (int) $_REQUEST['id_panier'] . " AND ID_LIGNE_PANIER=" . (int) $value . ")";
			} else {
				$sql_type_mediaTID = 'SELECT doc_id_media FROM t_doc WHERE id_doc=' . (int) $value;
			}
			

			$type_mediaTID = $db->getOne($sql_type_mediaTID);
			//			var_dump($type_mediaTID);
			if ($type_mediaTID_PREC != $type_mediaTID && $type_mediaTID_PREC !== null) {
				$tIds_meme_type_media = false;
				break;
			}
			$type_mediaTID_PREC = $type_mediaTID;
		}
		
		foreach (explode(',', $_REQUEST['tIds']) as $value) {
		//			if ($entite == 'panier_doc') {//on ne peut pas utiliser ceci car $entite = 'doc'; au haut du fichier
			if (!empty($_REQUEST['id_panier'])){
				$sql_type_docTID = "SELECT doc_id_type_doc
									from t_doc where ID_DOC IN (select distinct ID_DOC from t_panier_doc where ID_PANIER=" . (int) $_REQUEST['id_panier'] . " AND ID_LIGNE_PANIER=" . (int) $value . ")";
			} else {
				$sql_type_docTID = 'SELECT doc_id_type_doc FROM t_doc WHERE id_doc=' . (int) $value;
			}

			$type_docTID = (int) $db->getOne($sql_type_docTID);

			if ($type_docTID_PREC != $type_docTID && $type_docTID_PREC !== null) {
				$tIds_meme_type_doc = false;
				break;
			}
			$type_docTID_PREC = $type_docTID;
		}
		if ($tIds_meme_type_doc) {
			$xmlPathType = constant("formDir") . 'xml/saisieLot_doc_type' . $type_docTID . '.xml';
//			echo '$xmlPathType '.$xmlPathType.'<br />';
			//on regarde si le fichier existe (SEULEMENT au niveau du site)
			if (is_file($xmlPathType)) {
				$xmlAff = $xmlPathType;
			}
		}
		if ($tIds_meme_type_media) {
			$xmlPathType = constant("formDir") . 'xml/saisieLot_doc_typeMedia' . $type_mediaTID . '.xml';
			
			//on regarde si le fichier existe (SEULEMENT au niveau du site)
			if (is_file($xmlPathType)) {
				$xmlAff = $xmlPathType;
			}
		}
	} else {
		//on met ici pour pour pouvoir utiliser à la suite dans modifLot.inc.php
		$tIds_meme_type_doc = false;
		$tIds_meme_type_media = false;
	}
	$myModSaisieLot=new ModifLot(strtolower($entite),$xmlAff);
	$myModSaisieLot->getFields();
	$myModSaisieLot->setSearchSQL($newModifLot, $sql);
	$result=$myModSaisieLot->getResults();

	if ($result) {
		//On a une commande (action) + pas en train de naviguer (step=0)
		if ($commande === "INIT" || $commande === "INITEMPTY") {
			//Paramètres généraux
			$paramsGen = array(
				'urlaction' => 'saisieLot',
				'commande' => $commande
			);

			$FORM = $_REQUEST;

			$multiArrParams = $paramsGen;
			if(isset($champEntiteLex) && !empty($FORM[$champEntiteLex])) {
				$lastID=-1;
				foreach ($FORM[$champEntiteLex] as $idx=>$lex) {
					if (isset($lex['ID_LEX']) || isset($lex['ID_PERS'])) {
						$lastID=$idx;
						$FORM[$champEntiteLex][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
						if(isset($lex_terme)) {$FORM[$champEntiteLex][$idx]['LEX_TERME']=$lex_terme;$FORM[$champEntiteLex][$idx]['ID_LEX']=0;unset($lex_terme);}
						if(isset($pers_nom)) {$FORM[$champEntiteLex][$idx]['PERS_NOM']=$pers_nom;$FORM[$champEntiteLex][$idx]['ID_PERS']=0;unset($pers_nom);}
					}
					if (isset($lex['LEX_TERME'])) {$lex_terme=$lex['LEX_TERME'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['PERS_NOM'])) {$pers_nom=$lex['PERS_NOM'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['PERS_PRENOM'])) {$FORM[$champEntiteLex][$lastID]['PERS_PRENOM']=$lex['PERS_PRENOM'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['DLEX_ID_ROLE'])) {$FORM[$champEntiteLex][$lastID]['DLEX_ID_ROLE']=$lex['DLEX_ID_ROLE'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['ID_TYPE_LEX'])) {$FORM[$champEntiteLex][$lastID]['ID_TYPE_LEX']=$lex['ID_TYPE_LEX'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['DLEX_REVERSEMENT'])) {$FORM[$champEntiteLex][$lastID]['DLEX_REVERSEMENT']=$lex['DLEX_REVERSEMENT'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['LEX_PRECISION'])) {$FORM[$champEntiteLex][$lastID]['LEX_PRECISION']=$lex['LEX_PRECISION'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['ID_TYPE_DESC'])) {$FORM[$champEntiteLex][$lastID]['ID_TYPE_DESC']=$lex['ID_TYPE_DESC'];unset($FORM[$champEntiteLex][$idx]);}
					if (isset($lex['COMMANDE'])) {$FORM[$champEntiteLex][$lastID]['COMMANDE']=$lex['COMMANDE'];unset($FORM[$champEntiteLex][$idx]);}
				}
			}

			if(!empty($FORM[$champEntiteTextLex])){
				$lastID=$idx;
				$params_text_lex = array();
				foreach($FORM[$champEntiteTextLex] as $idx_=>$text_input){
					// définition séparateur de la saisie texte
					if(isset($text_input['sep']) && !empty($text_input['sep'])){ $separator = $text_input['sep']; }else {$separator = ',';}
					
					// stockage des paramètres à ajouter à chaque terme extrait du texte
					if (isset($text_input['DLEX_ID_ROLE'])) {$params_text_lex['DLEX_ID_ROLE']=$text_input['DLEX_ID_ROLE'];}
					if (isset($text_input['ID_TYPE_LEX'])) {$params_text_lex['ID_TYPE_LEX']=$text_input['ID_TYPE_LEX'];}
					if (isset($text_input['PERS_ID_TYPE_LEX'])) {$params_text_lex['PERS_ID_TYPE_LEX']=$text_input['PERS_ID_TYPE_LEX'];}
					if (isset($text_input['ID_TYPE_DESC'])) {$params_text_lex['ID_TYPE_DESC']=$text_input['ID_TYPE_DESC'];}
					if (isset($text_input['COMMANDE'])) {$params_text_lex['COMMANDE']=$text_input['COMMANDE'];}
					
					// traitement du texte
					if(isset($text_input['text']) && !empty($text_input['text'])){
						if(array_key_exists('ID_TYPE_LEX',$params_text_lex) && !empty($params_text_lex['ID_TYPE_LEX'])){
							$type_insert = 'l';
						}else if(array_key_exists('PERS_ID_TYPE_LEX',$params_text_lex) && !empty($params_text_lex['PERS_ID_TYPE_LEX'])){
							$type_insert = 'p';
						}else{
							continue ;
						}
						// split selon separator
						$text_split = explode($separator,$text_input['text']);
						foreach($text_split as $idx=>$term){
							// chaque terme est ajouté à t_doc_lex avec comme index lastID,
							$lastID++;
							// suivant le type de valeurs à insérer :
							if($type_insert == 'l'){
								$FORM[$champEntiteLex][$lastID]['LEX_TERME'] = trim($term);
							}else if($type_insert == 'p'){
								$arr_term = explode(' ',trim($term));
								if(count($arr_term)>1){
									if(defined("gPersFirstTerm") && gPersFirstTerm == 'prenom'){
										$FORM[$champEntiteLex][$lastID]['PERS_PRENOM'] = $arr_term[0];
										unset($arr_term[0]);
										$FORM[$champEntiteLex][$lastID]['PERS_NOM'] = implode(' ',$arr_term);
									}else if(!defined("gPersFirstTerm") || gPersFirstTerm == 'nom'){
										$FORM[$champEntiteLex][$lastID]['PERS_PRENOM'] = $arr_term[count($arr_term)-1];
										unset($arr_term[count($arr_term)-1]);
										$FORM[$champEntiteLex][$lastID]['PERS_NOM'] = implode(' ',$arr_term);
									}
								}else{
									$FORM[$champEntiteLex][$lastID]['PERS_NOM'] = $term;
								}
							}
							// l'ensemble des params est appliqué à chacun des termes
							if(!empty($params_text_lex)){
								foreach($params_text_lex as $key=>$param){
									$FORM[$champEntiteLex][$lastID][$key] = $param;
								}
							}
						}
						// les params sont remis à 0 pour le champ suivant
						$params_text_lex = array() ;
					}
				}
				// Mise à jour $idx pour cas suivant
				$idx=$lastID;
			}

			// MS 20/02/13 mise en place du support des catégories pour la saisie par lot (recopié à partir du fonctionnement des valeurs)
			if(isset($champEntiteCat) && !empty($FORM[$champEntiteCat])) {
				foreach ($FORM[$champEntiteCat] as $idx=>$cat) {
					if (isset($cat['ID_CAT']) ) {
						$lastID=$idx;
						$FORM[$champEntiteCat][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
						if(isset($cat_nom)) {$FORM[$champEntiteCat][$idx]['CAT_NOM']=$cat_nom;$FORM[$champEntiteCat][$idx]['ID_CAT']=0;unset($cat_nom);}
					}
					if (isset($cat['CAT_NOM'])) {$cat_nom=$cat['CAT_NOM'];unset($FORM[$champEntiteCat][$idx]);}
					if (isset($cat['CAT_ID_TYPE_CAT'])) {$FORM[$champEntiteCat][$lastID]['CAT_ID_TYPE_CAT']=$cat['CAT_ID_TYPE_CAT'];unset($FORM[$champEntiteCat][$idx]);}
					if (isset($cat['ID_TYPE_CAT'])) {$FORM[$champEntiteCat][$lastID]['CAT_ID_TYPE_CAT']=$cat['ID_TYPE_CAT'];unset($FORM[$champEntiteCat][$idx]);}
					if (isset($cat['CAT_CODE'])) {$FORM[$champEntiteCat][$lastID]['CAT_CODE']=$cat['CAT_CODE'];unset($FORM[$champEntiteCat][$idx]);}
					if (isset($cat['COMMANDE'])) {$FORM[$champEntiteCat][$lastID]['COMMANDE']=$cat['COMMANDE'];unset($FORM[$champEntiteCat][$idx]);}
				}
			
				if(isset($lastID)) {

					/*for($idx=$lastID;$idx>=0;$idx--){
						if(isset($FORM[$champEntiteCat][$idx])){
							if(empty($FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'])) $FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT']=$lastTypeCat;
							else $lastTypeCat=$FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'];
						}
					}*/
					for($idx=0;$idx<=$lastID;$idx++){
						if(isset($FORM[$champEntiteCat][$idx])){
							if(empty($FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'])) $FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT']=$lastTypeCat;
							else $lastTypeCat=$FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'];
							
							if (intval($FORM[$champEntiteCat][$idx]['ID_CAT']) <= 0)
								unset($FORM[$champEntiteCat][$idx]);
						}
					}
					
					$paramsIndex = array();//sauvegarde les typeCats mis en paramètre

					for($idx=$lastID;$idx>=0;$idx--){
						//On construit le tableau de paramètres pour la classe modifLot
						//On a déjà créé un paramètre pour ce champ, on le met donc simplement à jour en ajoutant les données
						$lastTypeCat = $FORM[$champEntiteCat][$idx]['CAT_ID_TYPE_CAT'];
						if (isset($paramsIndex[$lastTypeCat])) {
							if (!empty($FORM[$champEntiteCat][$idx]['ID_CAT'])) {
								$myMod->SL_addParamId($multiArrParams[$paramsIndex[$lastTypeCat]], $FORM[$champEntiteCat][$idx]['ID_CAT']);
							} else {
								$myMod->SL_addValeur($multiArrParams[$paramsIndex[$lastTypeCat]]['value'], $FORM[$champEntiteCat][$idx]['CAT_NOM']);
							}
						//On créé un tableau de paramètre dans un format compréhensible par la classe modifLot
						} else {
							$arrParams = $myMod->SL_createArrParam('cat', $FORM[$champEntiteCat][$idx]);
							if($arrParams) {
								$multiArrParams[] = $arrParams;
								end($multiArrParams);
								$paramsIndex[$lastTypeCat] = key($multiArrParams);
							}
						}
					}
				}
			}

			// VP 20/06/18 mise en place du support des fetivals pour la saisie par lot (recopié à partir du fonctionnement des catégories)
			if(isset($champEntiteFest) && !empty($FORM[$champEntiteFest])) {
				foreach ($FORM[$champEntiteFest] as $idx=>$fest) {
					if (isset($fest['ID_FEST']) ) {
						$lastID=$idx;
						$FORM[$champEntiteFest][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
						if(isset($fest_libelle)) {$FORM[$champEntiteFest][$idx]['FEST_LIBELLE']=$fest_libelle;$FORM[$champEntiteFest][$idx]['ID_FEST']=0;unset($fest_libelle);}
					}
					if (isset($fest['DOC_FEST_VAL'])) {
						$valKey = array_keys($fest['DOC_FEST_VAL']);
						$valKey = $valKey[0];
						if (!isset($FORM[$champEntiteFest][$lastID]['DOC_FEST_VAL'][$valKey])) $FORM[$champEntiteFest][$lastID]['DOC_FEST_VAL'][$valKey]=$fest['DOC_FEST_VAL'][$valKey];
						else $FORM[$champEntiteFest][$lastID]['DOC_FEST_VAL'][$valKey]=array_merge($FORM[$champEntiteFest][$lastID]['DOC_FEST_VAL'][$valKey], $fest['DOC_FEST_VAL'][$valKey]);
						unset($FORM[$champEntiteFest][$idx]);
					}
					if (isset($fest['COMMANDE'])) {$FORM[$champEntiteFest][$lastID]['COMMANDE']=$fest['COMMANDE'];unset($FORM[$champEntiteFest][$idx]);}
				}
				
				if(isset($lastID)) {
					
					for($idx=$lastID;$idx>=0;$idx--){
						//On construit le tableau de paramètres pour la classe modifLot
						//On a déjà créé un paramètre pour ce champ, on le met donc simplement à jour en ajoutant les données
						$arrParams = $myMod->SL_createArrParam('fest', $FORM[$champEntiteFest][$idx]);
						if($arrParams) {
							$multiArrParams[] = $arrParams;
						}
					}
				}
			}
			
			if(isset($champEntiteVal)) {
				if (!empty($FORM[$champEntiteVal])){
					//On retravaille les données reçues de manière à avoir une structure correcte
					$lastID=-1;
					foreach ($FORM[$champEntiteVal] as $idx=>$val) {
						if (isset($val['ID_VAL'])) {
							$lastID=$idx;
							$FORM[$champEntiteVal][$idx]['ID_DOC']=$myDoc->t_doc['ID_DOC'];
							if(isset($valeur)) {$FORM[$champEntiteVal][$idx]['VALEUR']=$valeur;$FORM[$champEntiteVal][$idx]['ID_VAL']=0;unset($valeur);}
						}
						if (isset($val['VALEUR'])) {$valeur=$val['VALEUR'];unset($FORM[$champEntiteVal][$idx]);}
						if (isset($val['ID_TYPE_VAL'])){						
							if(!isset($FORM[$champEntiteVal][$lastID]['ID_TYPE_VAL'])) {
									$FORM[$champEntiteVal][$lastID]['ID_TYPE_VAL']=$val['ID_TYPE_VAL'];
							}					
							unset($FORM[$champEntiteVal][$idx]);
						}
	                                     /*  B.RAVI 2015-04-28 Bug les champs de types valeur(VI) ne prenaient pas en compte le paramètre commande (qui override la valeur de <commande> generale du form ex: INIT) du champ dans le                                         xml ex: <element>
	                                                    <chFields>PAYS</chFields>
	                                                    <multi_type>VI</multi_type>
	                                                    <commande>ADDVAL</commande> 
	                                                 </element>
	                                      */
	                                        if (isset($val['COMMANDE'])) {$FORM[$champEntiteVal][$lastID]['COMMANDE']=$val['COMMANDE'];unset($FORM[$champEntiteVal][$idx]);}
					}
					if(isset($lastID)) {

						for($idx=$lastID;$idx>=0;$idx--){

							if(isset($FORM[$champEntiteVal][$idx])){
								if(empty($FORM[$champEntiteVal][$idx]['ID_TYPE_VAL'])) $FORM[$champEntiteVal][$idx]['ID_TYPE_VAL']=$lastTypeVal;
								else $lastTypeVal=$FORM[$champEntiteVal][$idx]['ID_TYPE_VAL'];
							}

						}

						$paramsIndex = array();//sauvegarde les typeVals mis en paramètre

						for($idx=$lastID;$idx>=0;$idx--){
							//On construit le tableau de paramètres pour la classe modifLot
							//On a déjà créé un paramètre pour ce champ, on le met donc simplement à jour en ajoutant les données
							$lastTypeVal = $FORM[$champEntiteVal][$idx]['ID_TYPE_VAL'];
							if (isset($paramsIndex[$lastTypeVal])) {							
								if (!empty($FORM[$champEntiteVal][$idx]['ID_VAL'])) {
									$myMod->SL_addParamId($multiArrParams[$paramsIndex[$lastTypeVal]], $FORM[$champEntiteVal][$idx]['ID_VAL']);
								} else {
									$myMod->SL_addValeur($multiArrParams[$paramsIndex[$lastTypeVal]]['value'], $FORM[$champEntiteVal][$idx]['VALEUR']);
								}
							//On créé un tableau de paramètre dans un format compréhensible par la classe modifLot
							} else {
								$arrParams = $myMod->SL_createArrParam('valeur', $FORM[$champEntiteVal][$idx]);

								if($arrParams) {
									$multiArrParams[] = $arrParams;
									end($multiArrParams);
									$paramsIndex[$lastTypeVal] = key($multiArrParams);
								}
							}
						}
					}
				}
			}
			if(!empty($FORM[$champEntiteTextVal])){
				$lastID=$idx;
				$params_text_lex = array();
				foreach($FORM[$champEntiteTextVal] as $idx_=>$text_input){
					// définition séparateur de la saisie texte
					if(isset($text_input['sep']) && !empty($text_input['sep'])){ $separator = $text_input['sep']; }else {$separator = ',';}
					
					// stockage des paramètres à ajouter à chaque terme extrait du texte
					if (isset($text_input['VAL_ID_TYPE_VAL'])) {$params_text_lex['VAL_ID_TYPE_VAL']=$text_input['VAL_ID_TYPE_VAL'];}
					if (isset($text_input['ID_TYPE_VAL'])) {$params_text_lex['ID_TYPE_VAL']=$text_input['ID_TYPE_VAL'];}
					if (isset($text_input['COMMANDE'])) {$params_text_lex['COMMANDE']=$text_input['COMMANDE'];}
					
					// traitement du texte
					if(isset($text_input['text']) && !empty($text_input['text'])){
						// split selon separator
						$text_split = explode($separator,$text_input['text']);
						foreach($text_split as $idx=>$term){
							// chaque terme est ajouté à t_doc_val avec comme index lastID,
							$lastID++;
							$FORM[$champEntiteVal][$lastID]['VALEUR'] = trim($term);
							// l'ensemble des params est appliqué à chacun des termes
							if(!empty($params_text_lex)){
								foreach($params_text_lex as $key=>$param){
									$FORM[$champEntiteVal][$lastID][$key] = $param;
								}
							}
						}
						// les params sont remis à 0 pour le champ suivant
						$params_text_lex = array() ;
					}
				}
				// Mise à jour $idx pour cas suivant
				$idx=$lastID;
			}

			if (isset($champEntiteLex) && !empty($FORM[$champEntiteLex])) {
				//On créé les paramètres type lex au format compréhensible de la classe modifLot
				$paramsIndex = 	array();
				foreach ($FORM[$champEntiteLex] as $idx => $lex) {
					$arrParams = $myMod->SL_createArrParam('lex', $lex);
					if (isset($paramsIndex[$arrParams['field']])) {

						$index = $paramsIndex[$arrParams['field']];
						if (isset($lex['ID_LEX']) && !empty($lex['ID_LEX'])) {
							$myMod->SL_addParamLexId($multiArrParams[$index], $lex['ID_LEX'], "ID_LEX");

						} elseif (isset($lex['ID_PERS']) && !empty($lex['ID_PERS'])) {
							$myMod->SL_addParamLexId($multiArrParams[$index], $lex['ID_PERS'], "ID_PERS");

						} elseif (isset($lex['LEX_TERME'])) {
							$myMod->SL_addValeur($multiArrParams[$index]['value'], $lex['LEX_TERME']);

						} elseif (isset($lex['PERS_NOM'])) {
							$myMod->SL_addValeur($multiArrParams[$index]['value'], $lex['PERS_NOM']);
						}elseif (isset($lex['PERS_PRENOM'])) {
							$myMod->SL_addValeur($multiArrParams[$index]['value_prenom'], $lex['PERS_PRENOM']);
						}

					} else {
						if($arrParams) {
							$multiArrParams[] = $arrParams;
							end($multiArrParams);
							$paramsIndex[$arrParams['field']] = key($multiArrParams);
						}
					}
				}
			}

			if (isset($champEntiteLiensSRC) && !empty($FORM[$champEntiteLiensSRC])) {
				$paramsIndex = 	array();
				foreach ($FORM[$champEntiteLiensSRC] as $idx => $lien) {
					$arrParams = $myMod->SL_createArrParam('lien_src', $lien);
					if (isset($paramsIndex[$arrParams['field']])) {
						$index = $paramsIndex[$arrParams['field']];
						if (isset($lien['ID_DOC_DST'])) {
							$myMod->SL_addParamId($multiArrParams[$index], $lien['ID_DOC_DST']);
						}
					} else {
						if($arrParams) {
							$multiArrParams[] = $arrParams;
							end($multiArrParams);
							$paramsIndex[$arrParams['field']] = key($multiArrParams);
						}
					}
				}
			}
			if (isset($champEntiteLiensDST) && !empty($FORM[$champEntiteLiensDST])) {
				$paramsIndex = 	array();
				foreach ($FORM[$champEntiteLiensDST] as $idx => $lien) {
					$arrParams = $myMod->SL_createArrParam('lien_dst', $lien);
					if (isset($paramsIndex[$arrParams['field']])) {
						$index = $paramsIndex[$arrParams['field']];
						if (isset($lien['ID_DOC_SRC'])) {
							$myMod->SL_addParamId($multiArrParams[$index], $lien['ID_DOC_SRC']);
						}
					} else {
						if($arrParams) {
							$multiArrParams[] = $arrParams;
							end($multiArrParams);
							$paramsIndex[$arrParams['field']] = key($multiArrParams);
						}
					}
				}
			
			}

			//On créé les paramètres type régulier au format compréhensible de la classe modifLot
			foreach ($FORM as $myField => $value) {
				$arrCrit = explode("_",$myField);
				foreach ($prefix as $onePrefix) {
					if(strtolower($arrCrit[0]) === strtolower($onePrefix)) {
						$arrParams = $myMod->SL_createArrParam('', array($myField => $value));
						if($arrParams) {
							$multiArrParams[] = $arrParams;
						}
					}
				}
			}
			
			if (isset($FORM['version'])){
				$multiArrParams[] = array('field'=>'version','value'=>$FORM['version']);
			}
					
			
			

			debug($multiArrParams, "red", true);
			$myMod->updateFromArray($multiArrParams, true);
			$myMod->applyAllModifs();
		}
		else if ($commande === "SUP_DOC")
		{
			// suppression des documents
			foreach ($result as $doc)
			{
				$tmp_doc=new Doc();
				$tmp_doc->t_doc['ID_DOC']=intval($doc['ID_DOC']);
				$tmp_doc->getDoc();
				$tmp_doc->delete();
			}
			
			// on reinitialise 
			$myMod->setSearchSQL($newModifLot, $sql);
			$result=$myMod->getResults();
		}

	}

	foreach ($result as $res) {
		require_once(modelDir.'model_doc.php');
		if (strtolower($entite) == "doc" && !empty($res['ID_DOC']) && defined("gReportToHeritFields") && !empty($commande)) {
			$myDoc = new Doc;
			$myDoc->t_doc['ID_DOC']=intval($res['ID_DOC']);
			$myDoc->t_doc['ID_LANG']=$_SESSION['langue'];
			$myDoc->heritFieldsToReport();
		}
	}
	//

}




	

//possiblité de rajouter une page custom a la place de modiflot
if (!empty($_REQUEST['form']) && file_exists(designDir . '/form/' . $_REQUEST['form'] . '.inc.php')) {
	include(getSiteFile("formDir", $_REQUEST['form'] . '.inc.php'));
} else {
	include (getSiteFile("formDir", 'modifLot.inc.php'));
}


?>
