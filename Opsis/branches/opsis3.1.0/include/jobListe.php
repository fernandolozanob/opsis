<?php

	require_once(libDir."fonctionsGeneral.php");
	
	require_once(libDir."class_chercheJob.php");
	require_once(modelDir.'model_job.php');
	require_once(modelDir.'model_processus.php');
	require_once(modelDir.'model_materiel.php');

	global $db;

	$myPage=Page::getInstance();
	$myUser=User::getInstance();
	$myPage->setReferrer(true);
	$myPage->titre=kListeTraitements;
	$mySearch=new RechercheJob();

	
	if(isset($_GET["init"])) $mySearch->initSession(); // réinit recherche

	if(isset($myPrms["lancement"])){

		debug($myPrms,'red');
		debug($mats,'blue');
		
		// On choisit la priorité du job avec le parametre priority du formulaire
		if (isset($myPrms['priority']) && !empty($myPrms['priority']))
			$priorite=$myPrms['priority'];
		else
			$priorite=2;

        // VP 10/05/12 : ajout paramètre plateforme pour gérer plusieurs frontaux
        // VP 28/05/12 : ajout flag $otherJobPlateforme pour programmer des étapes sur un serveur sans vérifier l'existence des fichiers
        $otherJobPlateforme=false;
        if(defined("kJobPlateforme")){
            $job_plateforme=(empty($myPrms['job_plateforme'])?kJobPlateforme:$myPrms['job_plateforme']);
            if($job_plateforme!=kJobPlateforme) $otherJobPlateforme=true;
        }else{
            $job_plateforme="";
        }
		
		if (isset($myPrms['date_lancement']) && !empty($myPrms['date_lancement']))
		{
			$arr_date_launch=explode(' ',$myPrms['date_lancement']);
			$date_lancement_job=convDate($arr_date_launch[0]).' '.$arr_date_launch[1];
		}
        
		
		$param_mail="";
        $mails = '';
        $arrUsager = array();
        $arrGrp = array();
        if($myPrms["send_mail"]=="1" || $myPrms["send_mail"]=="true") {
			$param_mail.="<send_mail>1</send_mail>";
			require_once(modelDir.'model_usager.php');
			require_once(modelDir.'model_groupe.php');
			// upload non java
			if(is_array($myPrms['mail_id_usager'])) $arrUsager=$myPrms['mail_id_usager'];
			elseif(is_array($myPrms['mail_id_groupe'])) $arrGrp=$myPrms['mail_id_groupe'];
			else{// upload java
				foreach ($myPrms as $_key=>$_prm) {
					if(strpos($_key,"mail_id_usager_")===0) $arrUsager[]=$_prm;
					elseif(strpos($_key,"mail_id_groupe_")===0) $arrGrp[]=$_prm;
				}
			}
			if(count($arrGrp)>0){
				$grpObj=new Groupe();
				foreach($arrGrp as $grp){
					$grpObj->t_groupe['ID_GROUPE']=$grp;
					$grpObj->getusagerGroupe();
					foreach($grpObj->t_usager_groupe as $us) $arrUsager[]=$us['ID_USAGER'];
				}
				unset($grpObj);
			}
			if(count($arrUsager)>0){
				$usObj=new Usager();
				$usObj->getUsager($arrUsager);
				foreach($usObj->t_usager as $us) $mails.=",".$us['US_SOC_MAIL'];
				unset($usObj);
			}
			if(!empty($myPrms["mails"])) $mails.=",".$myPrms["mails"];
			// VP 22/02/2011 : filtrage caractères interdits dans adresses mails
			if(!empty($mails)) $param_mail.="<mails>".str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),substr($mails,1))."</mails>";
			if(!empty($myPrms["no_send_mail_usager"])){
				$param_mail.="<no_send_mail_usager>1</no_send_mail_usager>";
			}
			if(!empty($myPrms["no_send_mail_admin"])){
				$param_mail.="<no_send_mail_admin>1</no_send_mail_admin>";
			}
			if(!empty($myPrms["mail_fin_traitement_sujet"])){
				$param_mail.="<mail_fin_traitement_sujet>".str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),$myPrms['mail_fin_traitement_sujet'])."</mail_fin_traitement_sujet>";
			}
		}
		
		
		if(isset($myImp)){
			$myImp->createJobImport($param_mail);
		}
		
		
		// Lancement des traitements
		foreach($mats as $idx => $mat){
			//update 01/03/12 : doit corriger le bug des mails en doublons
			$mails = '';
			//PC 13/06/12 : correction du bug en entier
			$arrUsager = array();
			$arrGrp = array();
            
            // VP 4/07/13 : traitement sur DA
            if(isset($mat["ID_DOC_ACC"])){
                require_once(modelDir.'model_docAcc.php');
                $myDA= new DocAcc;
                $myDA->t_doc_acc['ID_DOC_ACC']=$mat["ID_DOC_ACC"];
                $myDA->getDocAcc();
                if(!empty($myPrms["id_etape"])|| !empty($mat["id_etape"])){
					// traitement par une étape sur un matériel
                    require_once(modelDir.'model_etape.php');
                    $etapeObj = New Etape();
                    $etapeObj->t_etape["ID_ETAPE"]=$myPrms["id_etape"];
                    $etapeObj->getEtape();
                    $job_param="<param><entity>doc_acc</entity></param>";
                    $id_job_valid=0;
                    $etapeObj->createJob($myDA->t_doc_acc['ID_DOC_ACC'],$myDA->t_doc_acc['DA_FICHIER'],$myImp->t_import['IMP_ID_JOB'],0,$id_job_valid,$jobObj,$job_param,'',$priorite,$job_plateforme,$date_lancement_job);
                    unset($etapeObj);
				}
                unset($myDA);
                
            }elseif(isset($mat["ID_MAT"])){
                $id_mat=$mat["ID_MAT"];
                // VP 16/09/09 : traitement si fichier localisé sur un autre serveur (ex : frontal CNRS)
                $matObj= new Materiel;
                $matObj->t_mat['ID_MAT']=$mat['ID_MAT'];
                $matObj->getMat();
                $cMatTrack = $matObj->getMatTrack();
                // VP 10/11/09 : utilisation méthode TCwithOffset
                if(isset($mat['TCIN'])) $mat['TCIN']=$matObj->TCwithOffset($mat['TCIN']);
                if(isset($mat['TCOUT'])) $mat['TCOUT']=$matObj->TCwithOffset($mat['TCOUT']);
                // VP 6/10/10 : utilisation de FOLDER_IN pour le path du fichier
                if($mat['FOLDER_IN']) $path=$mat['FOLDER_IN'];
                else $path=$matObj->getLieuByIdMat($mat['ID_MAT'],$server);
                debug($matObj->getFilePath());
                if(($server!="localhost")||file_exists($matObj->getFilePath())||($myPrms["type"]=="imageur")||($myPrms["type"]=="num")||($myPrms["type"]=="doc_acc")|| $otherJobPlateforme || !empty($myPrms["job_without_mat"])){
                	
                    // VP 24/03/10 : définitions des adresses mails destinataires avec balise <mails>
                    // VP 25/05/12 : prise en compte paramètre job_param
                    //$param="";
                    
                	$param=$myPrms["job_param"];
					if(isset($myPrms['job_without_mat']) && !empty($myPrms['job_without_mat'])){
						$param.= '<job_without_mat>'.$myPrms['job_without_mat'].'</job_without_mat>';
					}
					if(!empty($myPrms["JOB_OUT"]))
						$jobObj->t_job["JOB_OUT"]=$myPrms["JOB_OUT"];
					if(isset($matObj->t_mat['MAT_FORMAT']) && strpos($matObj->t_mat['MAT_FORMAT'],'DCP-CPL')!==false){
						$param.="<dcp>".$matObj->t_mat['MAT_INFO']."</dcp>";
						if(isset($mat['ID_DOC'])){
							$param.="<ID_DOC>".$mat['ID_DOC']."</ID_DOC>";
						}
					}
					if(isset($matObj->t_mat['MAT_FORMAT']) && strpos($matObj->t_mat['MAT_FORMAT'],'DPX')!==false){
						$param.="<dpx>".$matObj->t_mat['MAT_INFO']."</dpx>";
						if(isset($mat['ID_DOC'])){
							$param.="<ID_DOC>".$mat['ID_DOC']."</ID_DOC>";
						}
					}
					
					$param.=$param_mail;
					
                   	// VP (16/10/08) : création superJob
                    // VP 25/05/10 : changement mode affectation JOB_PARAM
                    // VP 15/06/10 : correction bug sur affectation JOB_PARAM
                    if(!empty($myPrms["super_job"]) && empty($superJobId)){
                        $job_param=$param;
                        if ($myPrms['id_panier']) $job_param.="<ID_PANIER>".$myPrms['id_panier']."</ID_PANIER>";
                        // VP 5/10/10 : changement FOLDER en FOLDER_OUT
                        if ($myPrms['folder']) $job_param.="<FOLDER_OUT>".$myPrms['folder']."</FOLDER_OUT>";
                        $job_param="<param>".$job_param."</param>";
                        $superJobObj=new Job;
                        $superJobObj->t_job["JOB_ID_GEN"]=$myImp->t_import['IMP_ID_JOB'];
                        $superJobObj->t_job["JOB_NOM"]=$myPrms["super_job"];
                        $superJobObj->t_job["JOB_PARAM"]=$job_param;
                        $superJobObj->t_job["JOB_DATE_CREA"]=date("Y-m-d H:i:s");
                        $superJobObj->t_job["JOB_ID_SESSION"]=session_id();
                        $superJobObj->t_job["JOB_PRIORITE"]=$priorite;
                        $superJobObj->t_job["JOB_PLATEFORME"]=$job_plateforme;
                        $superJobObj->create();
                        $superJobId=$superJobObj->t_job["ID_JOB"];
                    }else{
						$superJobId = $myImp->t_import['IMP_ID_JOB'];
					}

                    // VP (16/10/08) : ajout possibilité de processus différents pour chaque ligne de traitement
                    //print_r($myPrms);
                    if(!empty($myPrms["id_proc"]) || !empty($mat["id_proc"])){
                        // VP 23/04/10 : ajout paramètre $bypass_etapes pour sauter des étapes
                        $bypass_etapes=explode(",",$myPrms["bypass_etapes"]);
                        if(($myPrms["id_proc"]=='*') && !empty($myPrms["proc_critere"])){
                            
                            if (!is_array($myPrms["proc_critere"]))
                            {
                                // VP 29/07/2015 : Suppression stripcslashes car les regexp peuvent en contenir (cf LNR)
                                //$critere=json_decode(stripcslashes($myPrms["proc_critere"]),true);
                                $critere=json_decode($myPrms["proc_critere"],true);
                            }else{
                                $critere=$myPrms["proc_critere"];
                            }
                            // VP (16/5/08) Choix du processus en fonction de critères, l'indice du tableau étant l'id du processus à appliquer
                            // VP 3/09/12 : on sort au premier cas trouvé par break
                            foreach($critere as $idx => $crit){
								$reg_valid = false ; 
                                if(preg_match("|</(.*)>|",$crit,$matches)) $field=$matches[1];
                                if(preg_match("|<".$field.">(.*)</".$field."+>|",$crit,$matches)) $val=$matches[1];
								if(preg_match("#".$val."#i",$matObj->t_mat[$field])){$reg_valid = true ; }
                                // VP 20/10/11 : test sur chaine contenue plutôt que égal
								// PC 11/03/14 : ajout d'un cas par défaut "*"
                                if(strpos($matObj->t_mat[$field],$val) !== false || $reg_valid || $val == "*" || ( strpos($val,'|regex|') === 0  && preg_match($val,$matObj->t_mat[$field])) ) {
                                    //debug("proc= $idx field=$field val=$val mat=".$matObj->t_mat[$field]);
                                    $procObj = New Processus();
                                    $procObj->t_proc["ID_PROC"]=$idx;
                                    $procObj->getProc();
                                    $procObj->getProcEtape();
                                    //if ($param!="") $param="<param>".$param."</param>";
                                    $job_param="<param>".$param."</param>";
                                    
                                    // VP 1/07/09 : ajout paramètre FILE_NAME_MASK
                                    $procObj->createJobs($matObj->t_mat['ID_MAT'],$matObj->t_mat['MAT_NOM'],$job_param,$jobObj,$mat['FILE_NAME_MASK'],$superJobId,0,$bypass_etapes,$priorite,$job_plateforme,$date_lancement_job);

                                    //debug($procObj);
                                    break;
                                } else {
                                	foreach ($cMatTrack as $oTrack) {
                                		if(strpos($oTrack->get($field),$val) !== false) {
                                			$procObj = New Processus();
                                			$procObj->t_proc["ID_PROC"]=$idx;
                                			$procObj->getProc();
                                			$procObj->getProcEtape();
                                			//if ($param!="") $param="<param>".$param."</param>";
                                			$job_param="<param>".$param."</param>";
                                			
                                			// VP 1/07/09 : ajout paramètre FILE_NAME_MASK
                                			$procObj->createJobs($matObj->t_mat['ID_MAT'],$matObj->t_mat['MAT_NOM'],$job_param,$jobObj,$mat['FILE_NAME_MASK'],$superJobId,0,$bypass_etapes,$priorite,$job_plateforme,$date_lancement_job);
                                		}
                                	}
                                }
                            }
                        }else{
                    		// traitement par processus sur un matériel
                        	if($mat["id_proc"]=='*') {
	                        	if (!is_array($mat["proc_critere"]))
	                        	{
	                        		$critere=json_decode(stripcslashes($mat["proc_critere"]),true);
	                        	}else{
	                        		$critere=$mat["proc_critere"];
	                        	}
	                        		
	                        	// VP (16/5/08) Choix du processus en fonction de critères, l'indice du tableau étant l'id du processus à appliquer
	                        	// VP 3/09/12 : on sort au premier cas trouvé par break
	                        	foreach($critere as $idx => $crit){
	                        		if(preg_match("|</(.*)>|",$crit,$matches)) $field=$matches[1];
	                        		if(preg_match("|<".$field.">(.*)</".$field."+>|",$crit,$matches)) $val=$matches[1];
	                        		// VP 20/10/11 : test sur chaine contenue plutôt que égal
	                        		if(strpos($matObj->t_mat[$field],$val) !== false || ( strpos($val,'|regex|') === 0 && preg_match(str_replace('|regex|','',$val),$matObj->t_mat[$field])) ) {
	                        			$idProc = $idx;
	                        			break;
	                        		}
	                        	}
                        	} else {
                        		$idProc = $mat['id_proc'];
                        	}
                        	
                            $procObj = New Processus();
                            if(!empty($mat['id_proc'])) $procObj->t_proc["ID_PROC"]=$idProc;
                            else $procObj->t_proc["ID_PROC"]=$myPrms["id_proc"];
                            $procObj->getProc();
                            $procObj->getProcEtape();
                            $job_param=$param;
                            if ($mat['TCIN'] && $mat['TCOUT']) $job_param.="<TcIn>".$mat['TCIN']."</TcIn><TcOut>".$mat['TCOUT']."</TcOut>";
                            if ($mat['ID_LANG'] && $mat['ID_LANG']) $job_param.="<ID_LANG>".$mat['ID_LANG']."</ID_LANG>";
                            if ($mat['ID_DOC']) $job_param.="<ID_DOC>".$mat['ID_DOC']."</ID_DOC>";
                            // VP 5/10/10 : Inclusion updatePanier dans paramètres jobs
                            if ($mat['updatePanier']) $job_param.="<updatePanier>".$mat['updatePanier']."</updatePanier>";
                            if ($mat['ID_LIGNE_PANIER']) $job_param.="<ID_LIGNE_PANIER>".$mat['ID_LIGNE_PANIER']."</ID_LIGNE_PANIER>";
                            if ($mat['FOLDER_IN']) $job_param.="<FOLDER_IN>".$mat['FOLDER_IN']."</FOLDER_IN>";
                            // VP 5/10/10 : changement FOLDER en FOLDER_OUT
                            if ($mat['FOLDER']) $job_param.="<FOLDER_OUT>".$mat['FOLDER']."</FOLDER_OUT>";
                            if ($mat['MAT_COTE_ORI']) $job_param.="<MAT_COTE_ORI>".$mat['MAT_COTE_ORI']."</MAT_COTE_ORI>";
                            //if ($param!="") $param="<param>".$param."</param>";
                            $job_param="<param>".$job_param."</param>";
                            
                            $procObj->createJobs($matObj->t_mat['ID_MAT'],$matObj->t_mat['MAT_NOM'],$job_param,$jobObj,$mat['FILE_NAME_MASK'],$superJobId,0,$bypass_etapes,$priorite,$job_plateforme, $date_lancement_job);
                        }
                    } else if(!empty($myPrms["id_etape"])|| !empty($mat["id_etape"])){
                        // traitement par une étape sur un matériel
                        // si on a plusieurs materiel (sous forme d'array)
    //                    $tmp_id_etape=$myPrms["id_etape"];
    //                    if (!is_array($myPrms["id_etape"]))
    //                    {
    //                        $myPrms["id_etape"]=array();
    //                        $myPrms["id_etape"][]=$tmp_id_etape;
    //                    }
    //                    
    //                    foreach ($myPrms["id_etape"] as $id_etape)
                        if(!empty($mat['id_etape'])) $tmp_id_etape=$mat['id_etape'];
                        else $tmp_id_etape=$myPrms["id_etape"];

                        if (!is_array($tmp_id_etape)){
                            $arrEtapes=array();
                            $arrEtapes[]=$tmp_id_etape;
                        }else{
                            $arrEtapes=$tmp_id_etape;
                        }
                        
                        foreach ($arrEtapes as $id_etape)
                        {
                            require_once(modelDir.'model_etape.php');
                            $etapeObj = New Etape();
                            $etapeObj->t_etape["ID_ETAPE"]=$id_etape;
                            $etapeObj->getEtape();
                            $job_param=$param;
                            // By VP (23/9/08) : modif $xml_params (cas "doc")
                            if ($mat['TCIN'] && $mat['TCOUT']) $job_param.="<TcIn>".$mat['TCIN']."</TcIn><TcOut>".$mat['TCOUT']."</TcOut>";
                            if ($mat['ID_DOC']) $job_param.="<ID_DOC>".$mat['ID_DOC']."</ID_DOC>";
                            // VP 5/10/10 : Inclusion updatePanier dans paramètres jobs
                            if ($mat['updatePanier']) $job_param.="<updatePanier>".$mat['updatePanier']."</updatePanier>";
                            if ($mat['ID_LIGNE_PANIER']) $job_param.="<ID_LIGNE_PANIER>".$mat['ID_LIGNE_PANIER']."</ID_LIGNE_PANIER>";
                            if ($mat['FOLDER_IN']) $job_param.="<FOLDER_IN>".$mat['FOLDER_IN']."</FOLDER_IN>";
                            if ($mat['FOLDER']) $job_param.="<FOLDER_OUT>".$mat['FOLDER']."</FOLDER_OUT>";
                            //if ($param !="") $param="<param>".$param."</param>";
                            $job_param="<param>".$job_param."</param>";
                        // VP 27/11/09 : ajout paramètre $id_job_valid initialisé à 0
                            $id_job_valid=0;
							$id_job_prec=0;
							if(!empty($myPrms['id_job_prec'])) $id_job_prec=$myPrms['id_job_prec'];
							
                            $etapeObj->createJob($matObj->t_mat['ID_MAT'],$matObj->t_mat['MAT_NOM'],$myImp->t_import['IMP_ID_JOB'],$id_job_prec,$id_job_valid,$jobObj,$job_param,$mat['FILE_NAME_MASK'],$priorite,$job_plateforme,$date_lancement_job);
                        }
                    } else if(!empty($myPrms["id_module"])){
                        // traitement manuel sur un matériel
                        foreach ($myPrms as $_fld=>$_val) { //Filtrage, on n'envoie que les paramètres liés au transcodage
                            if (strpos($_fld,'trans_')===0) {
                                $_fld=split('_',$_fld,2); //on se débarrasse du préfixe (trans_)
                                if(is_array($_val)){
                                    foreach ($_val as $_fld2=>$_val2){
                                        $param.="<".$_fld[1].">".$_val2."</".$_fld[1].">";
                                    }
                                } else {
                                    $param.="<".$_fld[1].">".$_val."</".$_fld[1].">";
                                }
                            }
                        }
                        
                        //if ($param !="") $param="<param>".$param."</param>";
                        $job_param="<param>".$param."</param>";
                        $jobObj=new Job;
                        $jobObj->t_job["JOB_ID_MODULE"]=$myPrms["id_module"];
                        $jobObj->t_job["JOB_NOM"]="Traitement ".$id_mat;
                        $jobObj->t_job["JOB_IN"]=$matObj->t_mat['MAT_NOM'];
                        $jobObj->t_job["JOB_ID_MAT"]=$matObj->t_mat['ID_MAT'];
                        //$jobObj->t_job["JOB_OUT"]=str_replace($this->t_etape["ETAPE_IN"],$this->t_etape["ETAPE_OUT"],$id_mat);
                        $jobObj->setOut($matObj->t_mat['MAT_NOM'],$myPrms["in"],$myPrms["out"]);
                        $jobObj->t_job["JOB_PARAM"]=$job_param;
                        $jobObj->t_job["JOB_DATE_CREA"]=date("Y-m-d H:i:s");
                        $jobObj->t_job["JOB_ID_SESSION"]=session_id();
                        $jobObj->t_job["JOB_PRIORITE"]=$priorite;
                        $jobObj->t_job["JOB_PLATEFORME"]=$job_plateforme;
						$jobObj->t_job["JOB_ID_ETAT"]=1;
						$jobObj->getModule();
                        $jobObj->create();
                    }
                }
                unset($matObj);
            }
		}// end foreach

		// VP (16/10/08) : modification test sur création de traitement
		//if(empty($myPrms["id_proc"])&&empty($myPrms["id_etape"])&&empty($myPrms["id_module"])) {
		//XB noDisplay pas d affichage
	
			if(empty($jobObj) && empty($_REQUEST['redirectImportView'])) {
				echo "<br/>".$message."<br/>".kUploadFinTraitement."<br/><br/>";
				if(!isset($myPrms['noDisplay'])) {
					echo "<center><a href='javascript:self.close()' class='label_champs_form'>".kFermer."</a></center>";
				}
				return;
			}
		
		
		// VP 30/11/09 : affichage ou non de la fenêtre de suivi
			if(isset($myPrms["noDspProcs"])){
				if(php_sapi_name() == "cli"){
					echo "\n".$message."\n".kUploadFinTraitementCLI."\n\n";
				}else{
					if(!isset($myPrms['noDisplay'])) {
						echo "<br/>".$message."<br/>".kUploadFinTraitementSuivi."<br/><br/>";
						echo "<center><a href='javascript:self.close()' class='label_champs_form'>".kFermer."</a></center>";
					}
				}
				if(isset($myPrms['closePopup']) && $myPrms['closePopup']){
					echo "<script type='text/javascript'>";
						if(isset($myPrms['reloadWindow']) && $myPrms['reloadWindow']){echo "if(typeof window.opener != 'undefined'){ window.opener.location = window.opener.location ;}";}
						echo "window.close();
						</script>";
				}
				return;
			}
		
		// VP 30/09/10 : Paramétrage recherche jobListe suite création jobs
		$_POST["F_job_form"]="1";
		$_POST['chFields'][0]="JOB_ID_SESSION";
		$_POST['chTypes'][0]="C";
		$_POST['chValues'][0]=session_id();
		
	}

	// Lancement ou mise à jour des traitements -> lancement en ligne de commande
	//include(frameDir."updateJobs.php");
	
	// VP 1/10/10 : changement $myPrms par $_POST
    if(isset($_POST["commande"])){

        if(strcmp($_POST["commande"],"SUP")==0) {
            $myJob = New Job();
            $myJob->t_job['ID_JOB']=$_POST["ligne"];
			$myJob->getJob();
			$myJob->makeDeleteXml();
            $myJob->delete();
        }

        else if(strcmp($_POST["commande"],"INIT")==0) {
            $myJob = New Job();
            $myJob->t_job['ID_JOB']=$_POST["ligne"];
            $myJob->initJob();
//				$myJob->getJob();
//				$myJob->t_job['JOB_DATE_DEBUT']='';
//				$myJob->t_job['JOB_DATE_FIN']='';
//				$myJob->t_job['JOB_MESSAGE']='';
//				$myJob->setStatus(0);
//				$myJob->save();
        }
		else if ($_POST["commande"]=='CANCEL')
		{
			$myJob = New Job();
			$myJob->t_job['ID_JOB']=intval($_POST["ligne"]);
			$myJob->getJob();
			// changement de l'etat du job
			$myJob->t_job['JOB_ID_ETAT']=jobEnDemandeAnnulation;
			$myJob->save();
            $myJob->deleteXmlFile();
            $myJob->deleteLog();
			$myJob->makeCancelXml();
		}
    }
	
	//PC 21/03/13 Récupération et mise en session du paramêtre job_aff (alphabétique/hiérarchique)
	if (isset($_POST['job_aff'])) $_SESSION[$mySearch->sessVar]["job_aff"]=$_POST['job_aff'];
	$job_aff = 1;
	if (!empty($_SESSION[$mySearch->sessVar]["job_aff"])) {
		$_REQUEST['job_aff'] = $_SESSION[$mySearch->sessVar]["job_aff"];
		$job_aff = $_SESSION[$mySearch->sessVar]["job_aff"];
	}

    if(isset($_POST["F_job_form"])) //Recherche par formulaire
    {
		$aff_hierarchique = ($job_aff == 2);
    	$mySearch->prepareSQL($aff_hierarchique);
		$mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs']);
 		$mySearch->finaliseRequete(); //fin et mise en session
		
		$mySearch->finaliseRequeteChildren();
    }

	if ($myPage->getPageFromURL()!="") $_SESSION[$mySearch->sessVar]["page"]=$myPage->page;
	if ($myPage->getSortFromURL()!="") $_SESSION[$mySearch->sessVar]["tri"]=$myPage->getSort();

	

	
// MS - ajout possibilité de neutraliser l'affichage, utile pour les ajax de génération de jobs.
// le formulaire est soumis dans une iframe, mais on n'a pas besoin de faire l'affichage dans l'iframe puisqu'elle est cachée
if(isset($_GET['noDisplay']) && !empty($_GET['noDisplay']) && $_GET['noDisplay']){
	exit ;
}else if(isset($_REQUEST['redirectImportView']) && $_REQUEST['redirectImportView']){
	
	$redirectImportViewURL = 'index.php';
	if (preg_match('/\.php$/', $_REQUEST['redirectImportView']) && defined('kCheminLocalSurServeur')) {
		if (file_exists(kCheminLocalSurServeur . '/' . $_REQUEST['redirectImportView'])) {
			$redirectImportViewURL = $_REQUEST['redirectImportView'];
		}
	}

	if(isset($myImp) && !empty($myImp->t_import['ID_IMPORT'])){
		header('Location: '.$redirectImportViewURL.'?urlaction=importView&id_import='.$myImp->t_import['ID_IMPORT']);
	}else{
		header('Location: '.$redirectImportViewURL.'?urlaction=importView');	
	}
}else{
//Affichage message suite upload
	if(isset($message)) $_POST["message"]=$message;
	if(isset($_POST["message"])) echo "<br/>".$_POST["message"]."<br/>";

echo "<div id='pageTitre'><?= kListeTraitements ?></div><div class='feature'>";

// VP 10/03/10 : utilsation de formulaire XML à la place de formulaire inclus
//include_once(getSiteFile("formDir","jobListe.inc.php"));
if (file_exists(formDir."jobListe.inc.php")) require_once(formDir."jobListe.inc.php");
else require_once(getSiteFile("formDir","chercheJob.inc.php"));

	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql=$_SESSION[$mySearch->sessVar]["sql"];
	    if(isset($_SESSION[$mySearch->sessVar]["tri"])) $sql.= $_SESSION[$mySearch->sessVar]["tri"];

	    //Sauvegarde du nombre de lignes de recherche � afficher :
	    if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
	    		$_SESSION[$mySearch->sessVar]['nbLignes'] = $_REQUEST['nbLignes'];}

	    if(!isset($_SESSION[$mySearch->sessVar]['nbLignes'])) $_SESSION[$mySearch->sessVar]['nbLignes'] = (defined("gNbLignesListeDefaut")?gNbLignesListeDefaut:10);
	    $myPage->initPager($sql, $_SESSION[$mySearch->sessVar]['nbLignes'],$_SESSION[$mySearch->sessVar]["page"]); // init splitter object

	    print("<div class='errormsg'>".$myPage->error_msg."</div>");
		
		$aParams = array();
	    $aParams["nbLigne"] = $_SESSION[$mySearch->sessVar]['nbLignes'];
	    $aParams["page"] = $myPage->page;
	    $aParams["titre"] = urlencode(kTraitement);
		$aParams["jobEnCours"] = jobEnCours;
		$aParams["jobFini"] = jobFini;
		$aParams["urlparams"]=$myPage->addUrlParams();
		if($_SESSION[$mySearch->sessVar]['nbLignes']=="all") $aParams["offset"] = "0";
		else $aParams["offset"] = $_SESSION[$mySearch->sessVar]['nbLignes']*($myPage->page-1);
		$_SESSION[$mySearch->sessVar]['val_rows']=$myPage->found_rows;
		// VP 08/04/10 : ajout paramètre xmlfile
		$aParams["xmlfile"]=getSiteFile("listeDir","xml/jobListe.xml");
	    $myPage->addParamsToXSL($aParams);

		
		
		//PC 21/03/13 : prise en compte du job_aff dans le nom du xsl
		$xsl = "jobListe.xsl";
		if ($job_aff > 1) {
			$xsl = "jobListe$job_aff.xsl";
			//Récupération enfants
			//$param["children"]
			$res = $mySearch->searchChildren();
			$xml = TableauVersXML($res,"t_job_child",4,"select",0);
			$myPage->afficherListe("t_job",getSiteFile("listeDir",$xsl),false,$_SESSION[$mySearch->sessVar]['highlight'], $xml);
		}
		else $myPage->afficherListe("t_job",getSiteFile("listeDir",$xsl),false,$_SESSION[$mySearch->sessVar]['highlight']);

		if (!empty($_SESSION[$mySearch->sessVar]["sqlSource"])) $_SESSION[$mySearch->sessVar]["sql"] = $_SESSION[$mySearch->sessVar]["sqlSource"];
	}

echo "</div>";
}


?>
