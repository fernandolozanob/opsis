<?php
global $db;

    // VP 5/12/12 : pas de limite de temps
ini_set('max_execution_time','0');
 
/********************************************************/
/*						FONCTIONS						*/
/********************************************************/

/************************************************/
/*		FONCTIONS : traitement des données		*/
/************************************************/
/**
 * Récupérations des fichiers non référencés en base par dossier
 * @param string $dir
 * @param array $aFileSearch
 * @param string $filter
 * @return array $arrMissingFile
 */
function getMissedFiles($dir,$aFileSearch,$filter="") {
	$arrMissingFile = array();
	$exclude = array('xml','db');
	$file_list = array();
	$stack[] = $dir;
	
	while ($stack) {
		$current_dir = array_pop($stack);
		if ($dh = opendir($current_dir)) {
			while (($file = readdir($dh)) !== false) {
				if ($file[0] !== '.') {
					$current_file = "{$current_dir}/{$file}";
					if ((is_file($current_file) && !in_array(getExtension($file),$exclude) && (!is_array($filter) || in_array(getExtension($file),$filter))) ||(is_dir($current_file) && substr($current_file, -3,3) == 'HLS') ) {
						$file = $current_file;
						$file=str_replace('//','/',$file); //Précaution car il peut arriver que l'on ait des "double /"...
						$filename=basename($file);
						$key = dirname($file);
						if(!isset($aFileSearch[$key]) || !in_array($file,$aFileSearch[$key])) {
							$arrMissingFile[]=array('FILE'=>$file,'SIZE'=>@filesize($file),'DATE'=>filectime($file));
						}
					} elseif (is_dir($current_file)) {
						$stack[] = $current_file;
					}
				}
			}
			closedir($dh);
		}
	}

	return $arrMissingFile;
}


/**
 * Récupération des matériels non référencés en base
 * @return array $arrMissingMat
 */
function getMissedMats() {
	global $db;
	// VP 17/07/09 : optimisation récupération lieu
	$sql="SELECT ID_MAT,MAT_NOM,MAT_CHEMIN,LIEU_PATH from t_mat left join t_lieu on (MAT_LIEU=LIEU) ORDER BY ID_MAT ASC";
	$rsMats=$db->Execute($sql);
	$arrMats=array();
	
	//VG 30/11/2012 : retrait du fetchAll, qui causait un dépassement de la mémoire.
	//Attention ! Il reste néanmoins toujours le tableau $arrMats, moins volumineux car il n'a besoin que du chemin du fichier
	//Ajout d'une deuxième dimension au tableau $arrMats, afin de gagner en temps de traitement pour la recherche plus bas
	while ($mat=$rsMats->FetchRow()) {
		// $file= (empty($mat['LIEU_PATH'])?kVideosDir:$mat['LIEU_PATH']);
		$file=(empty($mat['LIEU_PATH'])?kVideosDir:((substr($mat['LIEU_PATH'],0,1)!='/')?kCheminLocalPrivateMedia.'/'.$mat['LIEU_PATH']:$mat['LIEU_PATH']));
		$file.= (!empty($mat['MAT_CHEMIN'])?$mat['MAT_CHEMIN']."/":"");
		$file.= $mat['MAT_NOM'];
		$file= str_replace('//','/',$file);
	
		//VG 30/11/2012 : j'ai commenté, on ne s'en sert plus
		//if (!file_exists($file)) {
		//$arrMissingFile[]=$mat;
		//}
	
		$arrMats[dirname($file)][] = $file;
	}
	$rsMats->Close();
	
	// 2. Récupération de la liste des fichiers
	
	// LD 22 05 09 passé en récursif pour les ss-répertoires, en excluant les fichiers xml et thumbs.db
	// VP 17/07/09 : exclusion fichiers commençant par .
	$arrMissingMat = getMissedFiles(kVideosDir,$arrMats);
	
	// VP 2/02/10 : liste des fichiers inclus dans les répertoires lieux
	$sql="SELECT * from t_lieu";
	$rows=$db->GetAll($sql);
	foreach ($rows as $row) {
		if(substr($row['LIEU_PATH'],0,1)!='/'){
			$row['LIEU_PATH'] = kCheminLocalPrivateMedia.'/'.$row['LIEU_PATH'];
		} 
		// MS - 23.06.15 - évite les doublons si plusieurs lieux pointent vers le même répertoire
		$arrMissingMat =  array_unique(array_merge($arrMissingMat, getMissedFiles($row['LIEU_PATH'],$arrMats)),SORT_REGULAR);
	}
	
	return $arrMissingMat;
}

/**
 * Récupération des documents d'accompagnement non référencés en base
 * @return array/boolean $arrMissingDocAcc
 */
function getMissedDACC() {
	global $db;
	// Doc Acc
	$sql="SELECT * from t_doc_acc ORDER BY DA_FICHIER ASC";
	$rsDAs=$db->Execute($sql);
	while ($da=$rsDAs->FetchRow()) {
		$key = substr(basename($da['DA_FICHIER']),0,3);
		$arrDAs[$key][]=$da['DA_FICHIER'];
	}
	$rsDAs->Close();
	
	if ($handle = opendir(kDocumentDir)) {
	
		while (false !== ($file = readdir($handle))) {
			if ($file[0] === '.' || is_dir(kDocumentDir.$file)) {
				continue;
			}

			$key = substr($file,0,3);
			if (!isset($arrDAs[$key]) || !in_array($file,$arrDAs[$key])) {
				$arrMissingDocAcc[]=array('FILE'=>$file,'SIZE'=>@filesize(kDocumentDir.$file));
			}
		}
		
		return $arrMissingDocAcc;
	} else
		return false;
}

/**
 * Récupération des imageurs et de leurs images non référencés en base
 * @return array $arrMissingImage
 */
function getMissedImageurs() {
	global $db;
	$sql="SELECT * from t_image,t_imageur WHERE t_image.ID_IMAGEUR=t_imageur.ID_IMAGEUR ORDER BY IM_CHEMIN ASC";
	$rsImgs=$db->Execute($sql);
	$arrImgs = array();
	while ($img=$rsImgs->FetchRow()) {
		$key=substr(kStoryboardDir.$img['IM_CHEMIN'],0,-1);
		$arrImgs[$key][]=str_replace('//','/',kStoryboardDir.$img['IM_CHEMIN']."/".$img['IM_FICHIER']);
	}
	
	$rsImgs->Close();
	$arrMissingImage = getMissedFiles(kStoryboardDir,$arrImgs,unserialize(gImageTypes));
	
	return $arrMissingImage;
}


/************************************************/
/*			FONCTIONS : Affichage				*/
/************************************************/

function showMatsListe($arrMissingMat) {
	echo "<br/>
					<h3>".kMateriel_s."</h3>
			<table id='tableMaintenanceFichiers' class='tableResults'>
				<th></th>
				<th></th>
				<th></th>
				<th><img src='".imgUrl."/button_drop_a.gif' style='cursor:pointer' onClick='deleteMat(\"all\")' /></th>
			";
	if (empty($arrMissingMat)) echo "<tr><td class='altern0'>".kErrorMaintenanceFichierNoFile."</td></tr>";
	else {
		$newIdx = 0;
		foreach ($arrMissingMat as $idx=>$misMat) {
				
			if ((isset($_POST['commande']) && $_POST['commande']=='deleteMat' && ($_POST['idx']==$idx || $_POST['idx']=='all') ) ) {
				unlink($misMat['FILE']);
				unset($arrMissingMat[$idx]);
			}
			else {
				echo "<tr class='altern".($newIdx%2)."'><td class='resultsCorps'>".$misMat['FILE']."</td>
								<td class='resultsCorps'>".date("d/m/Y H:i:s", $misMat['DATE'])."</td>
								<td class='resultsCorps'>".filesize_format($misMat['SIZE'])."</td>
								<td class=\"resultsCorps\"><img style='cursor:pointer' src='".imgUrl."/button_drop.gif' onClick='deleteMat(".$newIdx.")' /></td></tr>";
				$newIdx++;
			}
		}
	}
	echo "</table>";
	
	echo "<input type='button' value=\"".kGenererNoticeMaterielAssocie."\" onClick=\"javascript:popupUpload('indexPopupUpload.php?urlaction=importFCP&from=maintenanceMat','main')\" />";
}

function showDAsListe($arrMissingDocAcc) {
	echo "<br/>
				<h3>".kDocumentAcc."</h3>
				<table class='tableResults'>
					<th></th>
					<th></th>
					<th><img src='".imgUrl."/button_drop_a.gif' style='cursor:pointer' onClick='deleteDocAcc(\"all\")' /></th>
				";
	if (empty($arrMissingDocAcc)) echo "<tr><td class='altern0'>".kErrorMaintenanceFichierNoFile."</td></tr>";
	else {
		$newIdx = 0;
		foreach ($arrMissingDocAcc as $idx=>$misDocAcc) {
				
			if (($_POST['commande']=='deleteDocAcc' && ($_POST['idx']==$idx || $_POST['idx']=='all') ) ) {
				unlink(kDocumentDir.$misDocAcc['FILE']);
				unset($arrMissingDocAcc[$idx]);
			}
			else {
				echo "<tr class='altern".($newIdx%2)."'><td class='resultsCorps'><a href=\"".kDocumentUrl.$misDocAcc['FILE']."\" target='_blank'>".$misDocAcc['FILE']."</a></td><td class='resultsCorps'>".filesize_format($misDocAcc['SIZE'])."</td><td class=\"resultsCorps\"><img style='cursor:pointer' src='".imgUrl."/button_drop.gif' onClick='deleteDocAcc(".$newIdx.")' /></td></tr>";
				$newIdx++;
			}
	
		}
	}
	echo "</table>";
}


function showImageursListe($arrMissingImage) {
	echo "
			<br/>
				<h3>".kImageur."</h3>
			<table class='tableResults'>
				<th></th>
				<th></th>
				<th></th>
				<th><img src='".imgUrl."/button_drop_a.gif' style='cursor:pointer' onClick='deleteImage(\"all\")' /></th>
			";
	if (empty($arrMissingImage)) echo "<tr><td class='altern0'>".kErrorMaintenanceFichierNoFile."</td></tr>";
	else {
		$newIdx = 0;
		foreach ($arrMissingImage as $idx=>$misImg) {
	
			if (($_POST['commande']=='deleteImage' && ($_POST['idx']==$idx || $_POST['idx']=='all') ) ) {
				unlink($misImg['FILE']);
				unset($arrMissingImage[$idx]);
			}
			else {
				/*$url=implode('/',array_slice(explode('/',$misImg['FILE']),-2,2));
				echo kStoryboardDir.'<br />';
				echo $misImg['FILE'].'<br />';
				echo substr($misImg['FILE'],strlen(kStoryboardDir)).'<br />';*/
				$url=substr($misImg['FILE'],strlen(kStoryboardDir));
				echo "<tr class='altern".($newIdx%2)."'><td><img src='".htmlspecialchars(storyboardChemin.$url, ENT_QUOTES)."' width='40'/></td><td class='resultsCorps'><a href=\"javascript:popupImage('".addslashes(storyboardChemin.$url)."')\" >".$misImg['FILE']."</a></td><td class='resultsCorps'>".filesize_format($misImg['SIZE'])."</td><td class=\"resultsCorps\"><img style='cursor:pointer' src='".imgUrl."/button_drop.gif' onClick='deleteImage(".$newIdx.")' /></td></tr>";
				$newIdx++;
			}
				
		}
	}
	echo "</table>";
}


function getAndShowThumbsListe(){
	echo "
			<br/>
				<h3>".kCacheVignette."</h3>
			<table class='tableResults'>
				<th></th>
				<th></th>
				<th></th>
				<th><img src='".imgUrl."/button_drop_a.gif' style='cursor:pointer' onClick='deleteThumb(\"all\")' /></th>
			";
	
	$dir = kThumbnailDir;
	$filter = unserialize(gImageTypes);
	$arrMissingFile = array();
	$exclude = array('xml','db');
	$file_list = array();
	$stack[] = $dir;
	$idx = 0;
	$newIdx = 0;
	while ($stack) {
		$current_dir = array_pop($stack);
		if ($dh = opendir($current_dir)) {
			while (($file = readdir($dh)) !== false) {
				if ($file !== '.' && $file !== '..' ) {
					$current_file = "{$current_dir}/{$file}";
					if (is_file($current_file) && !in_array(getExtension($file),$exclude) && (!is_array($filter) || in_array(getExtension($file),$filter))) {
						$file = $current_file;
						$file=str_replace('//','/',$file); //Précaution car il peut arriver que l'on ait des "double /"...
						$filename=basename($file);
						if (($_POST['commande']=='deleteThumb' && ($_POST['idx']==$idx || $_POST['idx']=='all') ) ) {
							unlink($file);
						}
						else {
							$size = @filesize($file);
							//$url=implode('/',array_slice(explode('/',$file),-2,2));
							$url=substr($file,strlen(kCheminLocalMedia));
							echo "<tr class='altern".($newIdx%2)."'><td class=\"resultsCorps\"><img src='".kCheminHttpMedia."/".$url."' width='80'/></td><td class='resultsCorps'><a href=\"javascript:popupImage('".kCheminHttpMedia."/".$url."')\" >".$filename."</a></td><td class='resultsCorps'>".filesize_format($size)."</td><td class=\"resultsCorps\"><img style='cursor:pointer' src='".imgUrl."/button_drop.gif' onClick='deleteThumb(".$newIdx.")' /></td></tr>";
							$newIdx++;
						}
						$idx++;
	
					} elseif (is_dir($current_file)) {
						$stack[] = $current_file;
					}
				}
			}
			closedir($dh);
		}
	}
	
	if ($idx == 0 || ($idx>0 && $newIdx==0)) echo "<tr><td class='altern0'>".kErrorMaintenanceFichierNoFile."</td></tr>";
	echo "</table>";
}


/********************************************************/
/*						EXECUTION						*/
/********************************************************/
/************************************************/
/*		EXECUTION : Traitement des données		*/
/************************************************/

if (isset($_GET['clearstatcache']) && $_GET['clearstatcache'])
	clearstatcache();

if (!isset($_POST['filter']))
	$_POST['filter']=null;

$afficherMatsListe = ($_POST['filter']=='mat' || $_POST['filter']=='all' || !$_POST['filter']);
$afficherDAsListe = ($_POST['filter']=='doc_acc' || $_POST['filter']=='all');
$afficherImageursListe = ($_POST['filter']=='imageur' || $_POST['filter']=='all');
$afficherThumbsListe = ($_POST['filter']=='thumbs' || $_POST['filter']=='all');

// Récupération de la liste des matériels manquants
if ($afficherMatsListe) {
	$arrMissingMat = getMissedMats();
}

//Récupération de la liste des documents d'accompagnement manquants
if ($afficherDAsListe) {
	$arrMissingDocAcc = getMissedDACC();
	if($arrMissingDocAcc === false) {
		echo "Impossible de parcourir le répertoire";
	}
}

//Récupération de la liste des imageurs et de leurs images manquants
if ($afficherImageursListe) {
	$arrMissingImage = getMissedImageurs();
}





/************************************************/
/*			EXECUTION : Affichage				*/
/************************************************/
echo "<div id='pageTitre' class='title_bar'>".kAdminMaintenanceFichierOrphelins."</div>";


echo "
	  <script >
		function deleteMat(id) {
			if (!confirm('".kConfirmJSFichierSuppression."')) return false;
			document.form_maintenanceFichiers.idx.value=id;
			document.form_maintenanceFichiers.commande.value='deleteMat';
			document.form_maintenanceFichiers.submit();			
		}

		function deleteDocAcc(id) {
			if (!confirm('".kConfirmJSFichierSuppression."')) return false;
			document.form_maintenanceFichiers.idx.value=id;
			document.form_maintenanceFichiers.commande.value='deleteDocAcc';
			document.form_maintenanceFichiers.submit();
		}

		function deleteImage(id) {
			if (!confirm('".kConfirmJSFichierSuppression."')) return false;
			document.form_maintenanceFichiers.idx.value=id;
			document.form_maintenanceFichiers.commande.value='deleteImage';
			document.form_maintenanceFichiers.submit();
		}

		function deleteThumb(id) {
			if (!confirm('".kConfirmJSFichierSuppression."')) return false;
			document.form_maintenanceFichiers.idx.value=id;
			document.form_maintenanceFichiers.commande.value='deleteThumb';
			document.form_maintenanceFichiers.submit();
		}

		function filterBy(type) {
			document.form_maintenanceFichiers.filter.value=type;
			document.form_maintenanceFichiers.submit();
		}


	  </script>
	  <br/>
	  <table align='center'  class='contentMenu' border='0' cellspacing='0' cellpadding='5'>
 	  <tr valign='bottom'> 
      <td align='center'>
		  <a class='glink' href=\"javascript:filterBy('mat')\">".kMateriel_s."</a>
		  <a class='glink' href=\"javascript:filterBy('doc_acc')\">".kDocumentAcc."</a>	  
		  <a class='glink' href=\"javascript:filterBy('imageur')\">".kImageur."</a>
		  <a class='glink' href=\"javascript:filterBy('thumbs')\">".kCacheVignette."</a>
		  <a class='glink' href=\"javascript:filterBy('all')\">".kTous."</a>
      </td>
      </tr>
	  </table>
	  <div align='center' style='font-family:Georgia;font-size:12px;color:#666666;'>".kWarningMaintenanceFichierMessage."</div>
	  <div class='feature'>
	  <form name=form_maintenanceFichiers method=POST id=form_maintenanceFichiers action=".Page::getName()."?urlaction=maintenanceFichiers />
	  <input type='hidden' name='commande' value='' />
	  <input type='hidden' name='idx' value='' />
	  <input type='hidden' name='filter' value='".(isset($_REQUEST['filter'])?$_REQUEST['filter']:'')."' />
";
	

	if ($afficherMatsListe === true) {
		showMatsListe($arrMissingMat);
	} 

	if ($afficherDAsListe === true) {
		showDAsListe($arrMissingDocAcc);
	}

	if ($afficherImageursListe === true) {
		showImageursListe($arrMissingImage);
	} 

	if ($afficherThumbsListe === true) {
		//Ici, vu la quantité de fichiers possible, on évite de passer par un array intermédiare.
		getAndShowThumbsListe();
	}



echo "</form></div>";




?>
