<?
global $db;

$myUsr = User::getInstance();
$myPage = Page::getInstance();
?>
<div id="stats_webPage"><!--pour avoir un identifiant unique pour cette page et mettre en forme les �lements enfant via css -->
    <div id="pageTitre"> <?= kStatistiquesWeb ?></div>
    <?php
    if (defined("gUseOldStats") && gUseOldStats) {
        echo '<a href="?urlaction=stats" >' . kStatistiquesOpsis . '</a>';
    } else {
        echo '<a href="?urlaction=stats_app" >' . kStatistiquesOpsis . '</a>';
    }
    ?>

    <div class="print" onclick="window.frames['stats_web'].focus();
            window.frames['stats_web'].print();"><span ><?php echo kImprimer ?></span></div>


    <div class="feature">
        <?
        if (defined("gAwstatsUrl")) {
            if ($arr_awstats = unserialize(gAwstatsUrl)) {
                echo '<form method="POST" name="aw_stats_form" action="index.php?urlaction=stats_web">';
                echo '<select name="target_stats" onchange="aw_stats_form.submit();">';
                if (isset($_POST['target_stats'])) {
                    $target = $_POST['target_stats'];
                } else {
                    $target = null;
                }
                foreach ($arr_awstats as $name => $url) {
                    echo '<option value="' . $name . '" ' . (($target && $target == $name) ? 'selected="selected"' : '') . '>' . $name . "</option>\n";
                }
                echo "</select>";
                if (($target) && isset($arr_awstats[$target])) {
                    $target_url = $arr_awstats[$target];
                } else {
                    $target_url = reset($arr_awstats);
                }

                echo "<iframe id='stats_web' name='stats_web' src='" . $target_url . "' width='100%' style='border:0px solid black;min-height:750px' ></iframe>";
                echo "</form>";
            } else {


                echo "<iframe id='stats_web' name='stats_web' src='" . gAwstatsUrl . "' width='100%' style='border:0px solid black;min-height:750px' ></iframe>";
            }
        } else {
            echo "<div class='error'>" . kMsgNoAwstats . "</div>";
        }
        ?>
    </div>
</div>