<?


require_once(modelDir.'model_doc_prex.php');
global $db;

$myUser = User::getInstance();
$myPage = Page::getInstance();
$myPrex = New DroitPrex();

if(isset($_GET["id_doc"])){
	$_POST['ID_DOC'] = intval($_GET["id_doc"]);
}
if (isset($_GET['id_type']))
	$_POST['DP_TYPE'] = $_GET['id_type'];	
$myPrex->updateFromArray($_POST);

$action = $_POST["commande"];
$myPrex->t_prex['ID_DOC_PREX'] = $_REQUEST["id_prex"];

if(isset($_POST["bOk"]) || !empty($action))
{
	if(strcmp($action,"SUP_PREX") == 0) {
        $myPrex->delete();
        echo "<script>window.opener.location.reload();window.close();</script>";
    }
    else if(isset($_POST["bOk"]) || strcmp($action, "SAVE_PREX") == 0 || strcmp($action, "SAVE") == 0)
    {
    	$FORM = $_POST;
    	
    	if (isset($FORM['dp_audite']))
    		$FORM['dp_audite'] = "1";
    	else
    		$FORM['dp_audite'] = "0";
    	$myPrex->t_prex['DP_AUDITE'] = $FORM['dp_audite'];
    	$saved = $myPrex->save();
    	
	    if ($saved) {
	    	
        
		    if(isset($_GET["id_doc"]))
				{
					echo "<script>window.opener.location.reload();
							window.location = '" . $myPage->getName() . "?urlaction=prexSaisie&id_prex=" . $myPrex->t_prex['ID_DOC_PREX'] . "';
							</script>";
				}
	    	
	    	//construction tableau t_dp_pers pour la sauvegarde
	    	$cols = $db->MetaColumns('t_dp_pers');
	    	$fields = array();
	    	foreach ($cols as $idP => $valP){
	    		$fields[] = $idP;
                }  
                
                if (is_object($FORM['t_dp_pers']) || is_array($FORM['t_dp_pers'])){
                    foreach ($FORM['t_dp_pers'] as $idP => $valP){
                            foreach ($valP as $idPP => $valPP){
                                    if(in_array($idPP, $fields)){
                                    $myPrex->t_dp_pers[0][$idPP] = $valPP;}
                            }

                    }
                }
                
	    	//Nettoyage si ID_PERS non présent
                if (is_object($myPrex->t_dp_pers) || is_array($myPrex->t_dp_pers)){
                    foreach ($myPrex->t_dp_pers as $idP => &$valP){ 
                            if (!isset($valP['ID_PERS']) || empty($valP['ID_PERS'])){
                            unset($myPrex->t_dp_pers[$idP]);}
                    }
                }
    		//construction tableau t_dp_val pour la sauvegarde
	    	$arrVal = array();
                 if ($FORM['t_dp_val']){
			foreach ($FORM['t_dp_val'] as $idV => &$valV){
				if (isset($valV['ID_VAL'])){
					$arrVal[] = array('ID_VAL' => $valV['ID_VAL']);
				}
				if (isset($valV['ID_TYPE_VAL'])){
					$type_val = $valV['ID_TYPE_VAL'];
					foreach ($arrVal as &$row_val)
						if(!isset($row_val['ID_TYPE_VAL']))
							$row_val['ID_TYPE_VAL'] = $type_val;
				}
				//PC 16/11/10 : ajout champs DPV_NB
				if (isset($valV['DPV_NB']) && isset($arrVal[count($arrVal) - 1])){
					$arrVal[count($arrVal) - 1]['DPV_NB'] = $valV['DPV_NB'];
				}
			}
                 }
			$myPrex->t_dp_val = $arrVal;
	    				
    		$myPrex->savePersonne();
    		$myPrex->saveValeurs();
    		
    		$myPrex->getPrexFull();
	    }
    }
}
else if(isset($_GET["id_prex"]))
{
    $myPrex->t_prex['ID_DOC_PREX'] = $_GET["id_prex"];
    $myPrex->getPrexFull();
}
else
{
    if(empty($myPrex->t_prex['ID_DOC_PREX'])){

    }
}

if ($_REQUEST['form'] && is_file(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'))) {
	require_once(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'));
}
else{
	require_once(getSiteFile("formDir",'prexDetail.inc.php'));
}        
?>

