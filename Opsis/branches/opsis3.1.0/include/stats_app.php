<?
require_once(libDir."class_stats.php");
global $db;

$myStats = new Stats;
$myUsr=User::getInstance();
$myPage=Page::getInstance();

//mise en session des paramètres
if (!empty($_POST['bOK'])) $_SESSION["stats"]=$_POST;
if (isset($_SESSION["stats"]))
	foreach($_SESSION["stats"] as $var=>$val) {
		$$var=str_replace('"', '', $val);
	}

if ($myPage->getPageFromURL()!="") $_SESSION["stat_page"]=$myPage->page;
if ($myPage->getSortFromURL()!="") $_SESSION["stat_tri"]=$myPage->getSort();

include_once(getSiteFile("formDir",'stats_app.inc.php'));
if (($_POST['bOK'] || $_POST['page']) && isset($stat_sql) && isset($myStats->arrSql[$stat_sql]) && isset($stat_pres)) {
	$myStats->setParamForSearch($_SESSION["stats"]);
	$myStats->setParamForFilter($stat_dist, $stat_precis);
	$myStats->constructSql($stat_sql, $stat_pres);
	
	$sql = $myStats->getSql();
	unset($_SESSION["stat_sql"]);
	$_SESSION["stat_sql"]["sql"]=$sql;
	$_SESSION["stat_sql"]["sqlGraph"]=$sql.$myStats->getEndSql();
	$_SESSION["stat_sql"]["LIB"]=$myStats->arrLib;
	$_SESSION["stat_sql"]["EMPTYLEGEND"]=$myStats->arrEmptyLegend;
	
	if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') $_SESSION['stat_nbLignes'] = $_REQUEST['nbLignes'];
	if(!isset($_SESSION['stat_nbLignes'])) $_SESSION['stat_nbLignes'] = 10;

	$tmp_tri = explode(" ",trim($_SESSION['stat_tri']));
	if (!isset($tmp_tri[2])) $tmp_tri[2]=null;
	if (!isset($tmp_tri[3])) $tmp_tri[3]=null;
	
	$myPage->initPager($sql,$_SESSION['stat_nbLignes'],$_SESSION['stat_page']);
	$_SESSION["stat_sql"]['val_rows']=$myPage->found_rows;
	$param["nbLigne"] = $_SESSION['stat_nbLignes'];
	$param["page"] = $myPage->page;
	$param["tri"] = $tmp_tri[2].(($tmp_tri[3] == "DESC" )?"1":"0");
	$param["titre"] = $myStats->getTitle();
	if($_SESSION['stat_nbLignes']=="all") $param["offset"] = "0";
	else $param["offset"] = $_SESSION['stat_nbLignes']*($myPage->page-1);
	$param["graph"] = $myStats->getGraphParam($stat_pres);
	$libXml = array2xml($myStats->arrLib);

	$myPage->addParamsToXSL($param);
	$myPage->afficherListe("stat",getSiteFile("listeDir","stats_app.xsl"), false, null,$libXml);
}
?>
