<?
    require_once(modelDir.'model_docAcc.php');


    require_once(libDir."class_upload.php");
	global $db;
	$erreur=0;
	$myUser=User::getInstance();
	$myPage=Page::getInstance();


	if ($_REQUEST['id_lang']) $id_lang=$_REQUEST['id_lang']; else $id_lang=$_SESSION['langue'];


// I. Ouverture des s�quences d'un document
    if(isset($_REQUEST["id_doc"]) || isset($_REQUEST["id_pers"]) || isset($_REQUEST["id_fest"]))
    {
        // I.A. Initialisation
        if (!empty($_REQUEST["id_doc"])) {
    			require_once(modelDir.'model_doc.php');
				$myObject=New Doc();
			    $myObject->updateFromArray($_POST);
 				 if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myObject->t_doc['ID_LANG']=$_REQUEST['id_lang'];

			    $myObject->t_doc['ID_DOC']=$_REQUEST["id_doc"];

				//Test reportage
				require_once(modelDir.'model_docReportage.php');
				if (isset($myObject->t_doc['ID_DOC']) && !empty($myObject->t_doc['ID_DOC']) && Reportage::checkReportageWithId($myObject->t_doc['ID_DOC'])) {
					$id_doc = $myObject->t_doc['ID_DOC'];
					unset($myObject);
					$myObject = new Reportage();
					$myObject->updateFromArray($_POST);
					if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myObject->t_doc['ID_LANG']=$_REQUEST['id_lang'];
					if (empty($myObject->t_doc['ID_LANG'])) $myObject->t_doc['ID_LANG']=$_SESSION['langue'];
					$myObject->t_doc['ID_DOC']=intval($id_doc);
				}

				$myObject->getDocFull(1,1,1);
				$myObject->getDocAcc();
				$myObject->key="ID_DOC";
			    $myObject->key_value=$myObject->t_doc['ID_DOC'];
			    $lang=$myObject->t_doc['ID_LANG'];

        		if (!$myObject->checkAccess(4)) {throw new Exception("<b>".kAccesReserve."</b>");}

				$myDoc=$myObject; // XXX : idiot mais obligé de mettre ça pour que le menu marche, en attendant la refonte
        }
        if (!empty($_REQUEST["id_pers"])) {
    			require_once(modelDir.'model_personne.php');
				$myObject=New Personne();
			    $myObject->updateFromArray($_POST);
 				 if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myObject->t_personne['ID_LANG']=$_REQUEST['id_lang'];

			    $myObject->t_personne['ID_PERS']=$_REQUEST["id_pers"];
				$myObject->getPersonne();

				$myObject->getDocAcc();
				$myObject->key="ID_PERS";
			    $myObject->key_value=$myObject->t_personne['ID_PERS'];
			    $lang=$myObject->t_personne['ID_LANG'];

				$myPers=$myObject; // XXX : idiot mais obligé de mettre ça pour que le menu marche, en attendant la refonte
        }

       if (!empty($_REQUEST["id_fest"])) {
    			require_once(modelDir.'model_festival.php');
				$myObject=New Festival();
			    $myObject->updateFromArray($_POST);
 				 if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myObject->t_fest['ID_LANG']=$_REQUEST['id_lang'];

			    $myObject->t_fest['ID_FEST']=$_REQUEST["id_fest"];
				$myObject->getFest();

				$myObject->getDocAcc();
				$myObject->key="ID_FEST";
			    $myObject->key_value=$myObject->t_fest['ID_FEST'];
			    $lang=$myObject->t_fest['ID_LANG'];

				$myFest=$myObject; // XXX : idiot mais obligé de mettre ça pour que le menu marche, en attendant la refonte
        }



        // Liste des langues
        $t_lang=$_SESSION["arrLangues"];

        if(isset($_POST["btnSave"]) || $_POST['commande']=='SAVE')
        {
			$myDocAcc=new DocAcc();
			$arrDocAcc=$aIdsPost=array();

			$aIdsPost=array();
			// Réorganisation du tableau : comme ça les tableaux issus de l'objet ET du post ont la même structure
			foreach ($_POST["t_doc_acc"]["ID_DOC_ACC"] as $idx=>$val) { //ATTENTION les dimensions ne sont PAS quotées !!!

				// VP 2/07/13 : changement ordre tableau idem bdd
				$arrDocAcc[$idx]=array ('ID_DOC_ACC'=>$val,
									'ID_LANG'=>$_POST["t_doc_acc"]["ID_LANG"][$idx],
									'ID_DOC'=>($_POST["t_doc_acc"]["ID_DOC"][$idx]!=''?$_POST["t_doc_acc"]["ID_DOC"][$idx]:'0'),
									'ID_PERS'=>($_POST["t_doc_acc"]["ID_PERS"][$idx]!=''?$_POST["t_doc_acc"]["ID_PERS"][$idx]:'0'),
									'ID_PANIER'=>($_POST["t_doc_acc"]["ID_PANIER"][$idx]!=''?$_POST["t_doc_acc"]["ID_PANIER"][$idx]:'0'),
									'ID_FEST'=>($_POST["t_doc_acc"]["ID_FEST"][$idx]!=''?$_POST["t_doc_acc"]["ID_FEST"][$idx]:'0'),
									'ID_EXP'=>($_POST["t_doc_acc"]["ID_EXP"][$idx]!=''?$_POST["t_doc_acc"]["ID_EXP"][$idx]:'0'),
									'ID_CAT'=>($_POST["t_doc_acc"]["ID_CAT"][$idx]!=''?$_POST["t_doc_acc"]["ID_CAT"][$idx]:'0'),
									'DA_TITRE'=>$_POST["t_doc_acc"]["DA_TITRE"][$idx],
									'DA_FICHIER'=>$_POST["t_doc_acc"]["DA_FICHIER"][$idx],
									'DA_CHEMIN'=>$_POST["t_doc_acc"]["DA_CHEMIN"][$idx],
									'DA_TEXTE'=>$_POST["t_doc_acc"]["DA_TEXTE"][$idx],
									'DA_ORDRE'=>$_POST["t_doc_acc"]["DA_ORDRE"][$idx]
									);
				$aIdsPost[] = $val;
			}

			// Eviter Warning: Invalid argument supplied for foreach()
			if (is_object($_POST["t_doc_acc_val"]) || is_array($_POST["t_doc_acc_val"])) {
				foreach ($_POST["t_doc_acc_val"] as $type => $docacc) {
					foreach ($docacc as $idx => $val) {
						if (!empty($val)) {
							$arrDocAccVal[$idx][] = array('ID_TYPE_VAL' => $type,
								'ID_VAL' => $val,
								'ID_DOC_ACC' => $_POST["t_doc_acc"]["ID_DOC_ACC"][$idx]
							);
						}
					}
				}
			}

			$arrDAfromObjectIds=array();
			foreach ($myObject->t_doc_acc as $idx=>$docacc) { //il faut constituer un tableau ne contenant que le tableau t_doc_acc de CHAQUE object
				$arrDAfromObjectIds[]=$docacc->t_doc_acc['ID_DOC_ACC'];
			}


			$arrDel=array_diff($arrDAfromObjectIds,$aIdsPost);
			foreach ($myObject->t_doc_acc as $idx=>$docacc) {
				if(in_array($docacc->t_doc_acc['ID_DOC_ACC'],$arrDel)) {
					//Suppression du doc acc
					$docacc->delete();
				}
			}

			foreach($arrDocAcc as  $idx=>$docacc) {
				$myDocAcc=new DocAcc();
				$myDocAcc->updateFromArray($docacc);
				
				if(empty($docacc['ID_DOC_ACC'])) {
					//Création du doc acc
					$myDocAcc->t_doc_acc_val=$arrDocAccVal[$idx];
					$myDocAcc->create(false);
					$myDocAcc->saveDocAccVal();
					
				} else {
					$id = $myDocAcc->checkExistId();
					if($id) {
						//Sauvegarde du doc acc
						$myDocAcc->t_doc_acc_val=$arrDocAccVal[$idx];
						$myDocAcc->save();
						$myDocAcc->saveDocAccVal();
						
					} else {
						//Création du doc acc
						$myDocAcc->t_doc_acc_val=$arrDocAccVal[$idx];
						$myDocAcc->create(false);
						$myDocAcc->saveDocAccVal();
					}
				}
				unset($myDocAcc);
			}

			
            $myObject->saveVignette('doc_acc',$_POST['vignette']);
            if ($myDoc || $myFest) $myObject->getVignette();

            if(!empty($_POST["page"]))  {
               $myPage->redirect($_POST["page"]); //XXX : a fixer
			}

            $myObject->getDocAcc(); // Réactualisation. Pas hyper propre, mais efficace !

        }


    }
?>

<?
// VP 2/06/09 : suppression tags d'habillage
if ($_REQUEST['form'] && is_file(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'))) {
	require_once(getSiteFile("formDir",$_REQUEST['form'].'.inc.php'));
}
else{
	if ($myObject->key=="ID_PERS")
		include(getSiteFile("designDir","menuPersSaisie.inc.php"));
	elseif ($myObject->key=="ID_FEST")
		include(getSiteFile("designDir","menuFestSaisie.inc.php"));
	else
		include(getSiteFile("designDir","menuDocSaisie.inc.php"));

    // VP 29/4/2015 : $key et $value sont utilises dans le formulaire
    $key=$myObject->key;
    $value=$myObject->key_value;
	include(getSiteFile("formDir","docAccSaisie.inc.php"));
}
?>

</div>


