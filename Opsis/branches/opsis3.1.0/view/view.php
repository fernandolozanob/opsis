<?
/**
 *  Classe View
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(libDir."class_page.php");

class View {
    var $template;
    
   
    function __construct($template) {
        $this->setTemplate($template);
    }

    /** Affichage contenu principal
     * Appel méthode render
     * IN : template content
     * OUT : HTML
     */
    function dspContent(){
        if(isset($this->params['contentTemplate']) && !empty($this->params['contentTemplate'])){
            $tmpView = new View($this->params['contentTemplate']);
            $tmpView->setParams($this->params);
            $tmpView->render();
        }else{
            print $this->params['content'];
        }
    }

    /** Affichage template form
     * Appel méthode render
     * IN : template content
     * OUT : HTML
     */
    function dspFormTemplate(){
        if(isset($this->params['formTemplate']) && !empty($this->params['formTemplate'])){
            $tmpView = new View($this->params['formTemplate']);
            $tmpView->setParams($this->params);
            $tmpView->render();
        }
    }

    /** Affichage template
     * Appel méthode render
     * IN : template content
     * OUT : HTML
     */
    function dspTemplate($name){
        if (is_file(getSiteFile("designDir","templates/content/".$name.".html"))) {
            $tmpView = new View(getSiteFile("designDir","templates/content/".$name.".html"));
            $tmpView->setParams($this->params);
            $tmpView->render();
        }
    }
    
    /** Affichage détaillé en XSL
     * IN : $xsl, $xml
     * OUT : HTML
     */
    function dspDetail() {
		//trace("dspDetail");
        $myObj = $this->params['modelObj'];
        $entity = $this->params['entity'];
        $xmlFormFile = $this->params['xmlFormFile'];
        
        // Affichage détaillé par XSL
        $nomElement = $this->params['nomElement'];
        $xslFile = $this->params['xslFile'];
        $highlight = $this->params['highlight'];
        
		
        $this->params['params4XSL']["titre"] = urlencode($this->params['title']);
        $this->params['params4XSL']["loggedIn"] = loggedIn();
        $this->params['params4XSL']["profil"] = User::getInstance()->getTypeLog();
        $this->params['params4XSL']["storyboardChemin"] = storyboardChemin;
		if(isset($this->params['xmlFile']) && !empty($this->params['xmlFile'])){
			$this->params['params4XSL']['xmlfile'] =  $this->params['xmlFile'];
		}

		if(isset($this->params["contentXml"]) && !empty($this->params["contentXml"])){
			$content = $this->params["contentXml"];
			$contentnohighlight=$content;
		} else {
			$content = $myObj->xml_export();
            if(!empty($this->params['additionnalXML'])){
                $content.=$this->buildAdditionnalXML();
             }
			$content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$content."\n</EXPORT_OPSIS>";
			$contentnohighlight = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$contentnohighlight."\n</EXPORT_OPSIS>";
		}
		$log = new Logger("data_detail.xml");
		$log->Log($content);
		
		$this->setParams4XSL();
        $contentExport =TraitementXSLT($content, $xslFile, $this->params4XSL, 0, $contentnohighlight, $highlight);
        
        $contentExport = eval("?".chr(62).$contentExport.chr(60)."?");
        echo $contentExport;
    }
    
    /** Affichage formumlaire de recherche
     * IN : xml
     * OUT : HTML
     */
    function dspSearchForm(){
        require_once(libDir."class_formCherche.php");
        
        $mySearch = $this->params['searchObj'];
        $xmlFormFile = $this->params['xmlFormFile'];

        $entity = $mySearch->entity;
        $xmlform=file_get_contents($xmlFormFile);
        $myForm=new FormCherche;
        $myForm->chercheObj=$mySearch;
        $myForm->entity=$entity;
        $myForm->classLabel="label_champs_form";
        $myForm->classValue="val_champs_form";
        $myForm->display($xmlform);
    }

    /** Affichage formumlaire de saisie
     * IN : xml
     * OUT : HTML
     */
    function dspEditForm(){
        require_once(libDir."class_formSaisie.php");

        $myObj = $this->params['modelObj'];
        $entity = $this->params['entity'];
        $xmlFormFile = $this->params['xmlFormFile'];

        $xmlform=file_get_contents($xmlFormFile);
        $myForm=new FormSaisie;
        $myForm->saisieObj=$myObj;
        $myForm->entity=$entity;
        $myForm->classLabel="label_champs_form";
        $myForm->classValue="val_champs_form";
        $myForm->classHelp="miniHelp";
        $myForm->display($xmlform);
    }
    
    /** Affichage liste en XSL
     * IN : $xsl, $xml
     * OUT : HTML
     */
    function dspList() {
		//trace("dspList ".print_r($this->params,true));
        $mySearch = $this->params['searchObj'];
        $myRes = $this->params['searchRes'];
        $myPage = Page::getInstance() ; 
		
		if($mySearch->affichage=='HIERARCHIQUE') {
            // Affichage hiérarchique
            $myAjaxTree = $this->params['ajaxTree'];
            $nb_results = $this->params['nb_results'];
            $this->dspListTafelTree($myAjaxTree, $nb_results);
        } else {
            // Affichage liste
            $nomElement = $this->params['nomElement'];
            $xmlListFile = $this->params['xmlListFile'];
            $xslListFile = $this->params['xslListFile'];
            
            $this->params['params4XSL']["xmlfile"] = $xmlListFile;
            $this->params['params4XSL']["titre"] = urlencode($this->params['title']);
            $this->params['params4XSL']["sauvRequete"] = $this->params['sauvRequete'];

            if(isset($_GET['fromAjax']) && intval($_GET['fromAjax']) == 1){
                $this->params['params4XSL']['fromAjax'] = 1;
            }
			
			if ($mySearch != null) {
				print("<div class='errormsg'>".$mySearch->error_msg."</div>");
				if (get_class($mySearch) == "RechercheSolr") {
					$this->params['additionnalXML']['facet']=array2xml($mySearch->getArrayFacets(),'facet');
					if ($mySearch->solr_query->getHighlight()){
						$this->params['additionnalXML']['highlight_context']=array2xml($mySearch->getArrayHighlightContext(),'highlight_context');
					}
					$this->params['additionnalXML']['form']=array2xml($_SESSION[$mySearch->sessVar]['form'],'form');
				}elseif (get_class($mySearch) == "RechercheSinequa") {
					$this->params['additionnalXML']['concepts']=array2xml($mySearch->conceptsSinequa,'concepts');
					$this->params['additionnalXML']['correlations']=array2xml($mySearch->correlationsSinequa,'correlations');
				}
			    
				$this->setParams4XSL();
				$this->setParams4XSLFromSearch($mySearch);
				$additionnalXML = $this->buildAdditionnalXML();
				
				// trace(print_r($mySearch->result,true));
				$this->dspListXSL($mySearch->result, $nomElement, $xslListFile, $_SESSION[$mySearch->sessVar]['highlight'], $additionnalXML);
			}
			elseif (is_array($myRes)) {
				$this->setParams4XSL();
				$additionnalXML = $this->buildAdditionnalXML();
				$this->dspListXSL($myRes, $nomElement, $xslListFile, null, $additionnalXML);
			}
        }
    }
    
    /** Affiche une liste de résultats par TafelTree
     $myAjaxTree : objet TafelTree
     $nb_results : nb résultats si requête effectuée
     */
    function dspListTafelTree(&$myAjaxTree, $nb_results) {
        global $db;
        
        echo "
        <script>
        function dspClock (toggle) {
            
            if (toggle) {
                document.getElementById('clock').style.display='block';
                if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='none';
            }
            else {
                document.getElementById('clock').style.display='none';
                if (document.getElementById('blocDtree')) document.getElementById('blocDtree').style.display='block';
            }
            return true;
        }
        </script>
        <div id='clock' style='display:none;text-align:center'>".kPatientez."<br/><img src='".imgUrl."wait30trans.gif'></div>
        <link rel='stylesheet' href='".libUrl."webComponents/tafelTree/tree.css' type='text/css' />
        <script type='text/javascript' src='".libUrl."webComponents/prototype/prototype.js'></script>
        <script type='text/javascript' src='".libUrl."webComponents/prototype/scriptaculous.js'></script>
        <script type='text/javascript' src='".libUrl."webComponents/tafelTree/Tree.js'></script>
        <div align='center'  style='width:600px;border:1px solid #999999;' >
        <script>dspClock(true)</script>
        ";
        
        if($nb_results===0){
            echo "<div class='resultsMenu'>".kAucunResultat."</div>";
        }elseif($nb_results>0){
            echo "<div class='resultsMenu'>".$nb_results." ".kResultats."</div>";
        }

        $myAjaxTree->renderOutput('',true,$btnEdit);
        echo "<div id='myTree'></div>";
        echo "<script>dspClock(false)</script>
        </div>";
    }

    /** Affiche une liste de résultats par XSL (reprise afficherListe de classe Page)
     $tabResult : tableau de résultats
     $nomelement : nom de l'élement dans le xml
     $nomFichierXsl : nom du fichier xsl de transformation
     $params4XSL : paramètres pour XSL
     $options : 1 user, 2 doc, 3 admin
     $page : numéro de la page
     
     $ajoutPrivilegeDoc : Faut-il ajouter aux résultats le statuts de leurs privilèges (seulement utile pour la gestion des documents)
     */
    function dspListXSL(&$tabResult, $nomelement, $nomFichierXsl, $arrHighlight=null, $xml2merge='') {
        $xml = TableauVersXML($tabResult, $nomelement, 4, "select", 1, $xml2merge); 
		//classique
        $this->dspXSL($xml, $nomelement, $nomFichierXsl, $arrHighlight);
    }
	
    function dspXSL($xml, $nomelement, $nomFichierXsl, $arrHighlight=null) {
        $log = new Logger("data_$nomelement.xml");
        $log->Log($xml);
        
		// ini_set('display_errors',1);
		// error_reporting(E_ALL);
		
        //trace(print_r($this->params4XSL,true));
        $html=TraitementXSLT($xml, $nomFichierXsl, $this->params4XSL, 0, $xml, $arrHighlight);
       
        echo $html;
    }

    
    /** Retourne nom du script (reprise getName de classe Page)
     * IN : $_SERVER['SCRIPT_NAME']
     * OUT : fichier php
     */
    function getName() {
        return basename($_SERVER['SCRIPT_NAME']);
    }

    /** Mise à jour
     * IN : objet search, tableau params
     * OUT : params4XSL
     */
    function setParams4XSL($add_param = null) {
        $myPage=Page::getInstance();
		$this->params4XSL = array() ; 
		
		if(isset($this->params['params4XSL']) && !empty($this->params['params4XSL'])){
			$this->params4XSL = $this->params['params4XSL'];
		}
		
		if(isset($add_param) && !empty($add_param) && is_array($add_param)){
			$this->params4XSL = array_merge($this->params4XSL,$add_param);
		}
				
        $this->params4XSL["scripturl"]=$myPage->getName();
        $this->params4XSL["urlparams"]=$myPage->addUrlParams();
        $this->params4XSL["pageurl"]=$myPage->getUrlFromAction($myPage->urlaction, "","",$myPage->addUrlParams());
        $this->params4XSL["profil"]=User::getInstance()->getTypeLog();
        $this->params4XSL["userId"]=User::getInstance()->UserID;
        $this->params4XSL["frameurl"]=frameUrl;
		// transfert des options layout_opts de l'urlaction courante vers les param4XSL
		if(isset($_SESSION['USER']['layout_opts'][$myPage->urlaction]) && !empty($_SESSION['USER']['layout_opts'][$myPage->urlaction]) && is_array($_SESSION['USER']['layout_opts'][$myPage->urlaction])){
			
			foreach($_SESSION['USER']['layout_opts'][$myPage->urlaction] as $layout_opt_key=>$layout_opt_val){
				$this->params4XSL[$layout_opt_key] = $layout_opt_val;
			}
		}
	}

	function setParams4XSLFromSearch($mySearch){
        $this->params4XSL["nbLigne"] = $mySearch->nbLignes;
        $this->params4XSL["nb_rows"] = $mySearch->found_rows;
        $this->params4XSL["nbRows"] = $mySearch->found_rows;
        $this->params4XSL["page"] = $mySearch->page;
        $this->params4XSL["nb_pages"] = $mySearch->nb_pages;
        $this->params4XSL["tri"] = $mySearch->order;
        $this->params4XSL["ordre"] = $mySearch->triInvert;
        if($mySearch->nbLignes=="all") $this->params4XSL["offset"] = "0";
        else $this->params4XSL["offset"] = $mySearch->nbLignes*($mySearch->page-1);
        $this->params4XSL['fromOffset'] = (isset($mySearch->start_offset)?$mySearch->start_offset:null);
        $this->params4XSL["votre_recherche"]=htmlentities($mySearch->etape,ENT_COMPAT,'UTF-8');
        $this->params4XSL["pager_link"]=$mySearch->PagerLink;
        $this->params4XSL["refine"] = $mySearch->str_refine;
		
		
		// MSVERIF - pas sur que cela doive rester la, il s'agit d'un cas spécifique à la recherche solr 
		// (le refine juste au dessus est somewhat dans le meme cas) 
		if(defined('useSolr') && useSolr && get_class($mySearch) == 'RechercheSolr'){
			if(isset($mySearch->solr_facet_file_fullpath)){
				$this->params4XSL['solr_facet'] = $mySearch->solr_facet_file_fullpath;
			}else if (isset($_SESSION[$mySearch->sessVar]['solr_facet_file_fullpath'])){
				$this->params4XSL['solr_facet'] = $_SESSION[$mySearch->sessVar]['solr_facet_file_fullpath'];
			}
			if($mySearch->geojson){
				$this->params4XSL['geojson'] = $mySearch->geojson;
			}
		}
        //trace(print_r($this->params4XSL,true));
    }
	
	
	function buildAdditionnalXML(){
		$additionnalXML="";
		foreach($this->params['additionnalXML'] as $key => $add_xml){
			if(is_array($add_xml)){
				$additionnalXML.= array2xml($add_xml,$key);
			}elseif(is_string($add_xml)){
				$additionnalXML.=$add_xml;
			}
		}
		return $additionnalXML ; 
	}

    /** Calcule et renvoie le HTML d'une page
     * Analyse du template : extraction et traitement des balises spéciales
     * IN : template (init par méthode SetDesign)
     * OUT : HTML
     */
    function render($buffered=true) {
        try {
            //trace("render ".$this->template);
            $myPage=Page::getInstance();
            $src=file_get_contents($this->template);
            
            $regexp='`<php_param name="(.*)" />`siu';
            $src=preg_replace ($regexp,"<?=$1?>",$src); //Remplacement des balises <php_param... par la valeur
            
            $src=preg_replace ("`/\*.*\*/`siu","",$src);
            
            $motifbalise='`<include[^>]+/>`siu';
            $html=@preg_split($motifbalise,$src); // Pour le HTML hors balise
            
            @preg_match_all($motifbalise,$src,$includes,PREG_SET_ORDER); // Pour les balises include
            
            if ($buffered) ob_start();
            
            for ($i=0;$i<count($includes);$i++) {
                print($html[$i]);
                
                $attribs=get_attributes_from_tag($includes[$i][0],array("id","file","type","name","const_dir"));
                if (isset($attribs["file"][0])) { // include classique : test existence fichier puis include
					if(isset($attribs["const_dir"][0]) && !empty($attribs["const_dir"][0]) 
					&& basename($path = getSiteFile($attribs["const_dir"][0],$attribs['file'][0])) != 'index.php'
					&& file_exists($path)){
						include($path);
					}else if (file_exists($attribs["file"][0])) {
						include ($attribs["file"][0]);}
					else {print("Included file not found".$attribs["file"][0] );}
                }
                elseif (isset($attribs["id"][0])) { // include special : appel de méthode de classe
                    switch ($attribs["id"][0]) {
                            
                        case "content" :
                            $this->dspContent();
                            break;
                        default : print ("Included order not existing"); break;
                    }
                }
                elseif (isset($attribs["type"][0])) { // include special : appel de méthode de classe
                    switch ($attribs["type"][0]) {
                            
                        case "detail" :
                            $this->dspDetail();
                            break;
                            
                        case "formCherche" :
                            $this->dspSearchForm();
                            break;
                            
                        case "formSaisie" :
                            $this->dspEditForm();
                            break;
                            
                        case "liste" :
                             $this->dspList();
                            break;
                            
                        case "template" :
                            $this->dspTemplate($attribs["name"][0]);
                            break;

                        case "formTemplate" :
                            $this->dspFormTemplate();
                            break;

                        default : print ("Included order not existing"); break;
                    }
                }
                
            }
            print($html[$i]);
            
            
            if ($buffered) {
                $html=ob_get_contents(); // récupération du buffer
                ob_end_clean(); //vidage du buffer
                
				$log = new Logger("view_".basename($this->template));
				$log->Log($html);
					
				$htmlr= eval("?".chr(62).$html.chr(60)."?"); //eval du code pour interpréter le PHP embarqué.
                
                return $htmlr;
            }
        }
        
        catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    
    /** Affectation tableau paramètres
     * IN : tableau paramètres
     */
    function setParams($params){
        $this->params = $params;
    }

    /** Affectation template
     * IN : template
     */
    function setTemplate($template){
        $this->template = $template;
    }

}

?>
