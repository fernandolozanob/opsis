<?
/**
 *  Classe Controller_doc
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(modelDir.'model_doc.php');
require_once(libDir."class_page.php");

class Controller_doc extends Controller {
    
    /** Détail doc
     * IN : GET
     * OUT : objet entité
     */
    function do_doc($args=null) {
        global $db;
        
        $myPage=Page::getInstance();
        $myPage->nomEntite = kDocument;
        $myPage->setReferrer(true);
        
        $myUser=User::getInstance();
        
        // I. Ouverture enregistrement
        if (defined("useSinequa") && useSinequa==true && $_SESSION["useMySQL"]!="1") {
            $this->do_docSinequa(); // A IMPLEMENTER
        } else if (defined('useBasis') && useBasis==true) {
            $this->do_docBasis(); // A IMPLEMENTER
        } else if (defined('useSolr') && useSolr==true) {
            $this->do_docSolr();
        } else {
            
            if ($myUser->loggedIn()) require_once(modelDir.'model_panier.php');
            else require_once(modelDir.'model_panier_session.php');
            
            
            //si réecriture d'url, pas de get id_doc, il provient de la classe page
            if(!empty($myPage->id)){
                $id_doc_param = intval($myPage->id);
            }
            else if (!empty($_GET["id"]))
            {
                $id_doc_param = intval($_GET["id"]);
            }
            else if (!empty($_GET["id_doc"]))
            {
                $id_doc_param = intval($_GET["id_doc"]);
            }
            
            
            
            // I. Ouverture enregistrement
            $myDoc= new Doc();
            if (isset($_REQUEST['id_lang']) && $_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myDoc->t_doc['ID_LANG']=$_REQUEST['id_lang'];
            
            if(isset($id_doc_param)||isset($_GET["rang"])||isset($_GET['rang_seq']))
            {
                $id_lang=strtoupper($_SESSION["langue"]);
                
                // Mémorisation du paramètre rang
                //update VG 07/10/11 : ajout de la gestion des rangs pour la navigation parmi une liste de séquences issues d'un même père
                if(isset($_GET["rang_seq"]) && $_GET["rang_seq"] != '') { //Gestion de la navigation parmi les séquences d'un document
                    $rang_seq=intval($_GET["rang_seq"]);
                    
                    if(!empty($_GET["id_doc_pere"])) {
                        $docPere = new Doc();
                        $docPere->t_doc['ID_DOC'] = intval($_GET["id_doc_pere"]);
                        $docPere->t_doc['ID_LANG'] = $myDoc->t_doc['ID_LANG'];
                        $docPere->getChildren();
                        $id_doc = $docPere->arrChildren[$rang_seq]['ID_DOC'];
                        //update VG 28/05/12 : on redirige dans le cas où on ne peut PAS identifier le document.
                        //} elseif(!empty($_GET['id_doc'])) {
                    } elseif(empty($_GET['id_doc'])) {
                        $myPage->redirect($myPage->getName()."?urlaction=docListe");
                    }
                    
                } elseif(isset($_GET["rang"])) {
                    $rang=intval($_GET["rang"]);
                    
                    //update VG 07/10/11 : changement de place du traitement du rang, désormais plus haut dans le code
                    // Accès par rang sinon, en cherchant ID_DOC du résultat
                    if($rang==0) $rang=1; // Premier résultat par défaut
                    // VP (7/10/08) Navigation dans le panier
                    if(!empty($_GET["id_panier"])){
                        require_once(modelDir.'model_panier.php');
                        $myPanier = new Panier();
                        $myPanier->t_panier["ID_PANIER"]=intval($_GET["id_panier"]);
                        // MS - possibilité de conserver le tri du panier lors de la navigation inter-documents par rang
                        if(isset($_GET['tri'])){
                            $myPanier->sqlOrder=$myPage->getSort(false);
                        }
                        $myPanier->getPanierDoc();
						$max_rows = count($myPanier->t_panier_doc);
                        $id_doc=$myPanier->t_panier_doc[$rang-1]["ID_DOC"];
					} elseif(!empty($_GET["id_import"])) {
						require_once(modelDir.'model_import.php');
						$myImp = new Import();
						$myImp->t_import["ID_IMPORT"]=intval($_GET["id_import"]);
						$myImp->getImpDocs();
						$id_doc=$myImp->t_imp_docs[$rang-1]["ID_DOC"];
						$max_rows = count($myImp->t_imp_docs);
						unset($myImp);
					} elseif(!empty($_GET["id_reportage"])) {
						require_once(modelDir.'model_docReportage.php');
						$docRep = new Reportage();
						$docRep->t_doc['ID_DOC'] = intval($_GET["id_reportage"]);
						$docRep->t_doc['ID_LANG'] = $_SESSION['langue'];
						$arrIds = $docRep->getChildrenIds();
						$id_doc = $arrIds[$rang-1];
						$max_rows = count($arrIds);
						unset($docRep);
                    }else{
                        $sql=$_SESSION["recherche_DOC"]["sql"];
                        // VP 2/9/09 : déplacement test sql
                        if (!empty($sql)) {
                            if(isset($_SESSION["recherche_DOC"]["refine"])) $sql.= $_SESSION["recherche_DOC"]["refine"];
                            if(isset($_SESSION["recherche_DOC"]["tri"])) $sql.= $_SESSION["recherche_DOC"]["tri"];
                            //debug($sql,'lime');
                            $result=$db->Execute($sql." limit 1 OFFSET ".($rang-1));
                            if($result && $row=$result->FetchRow()){
                                $id_doc=$row["ID_DOC"];
                            }
                            $result->Close();
                            //update VG 28/05/12 : on redirige dans le cas où on ne peut PAS identifier le document.
                            //} elseif(!empty($_GET['id_doc'])){$myPage->redirect($myPage->getName()."?urlaction=docListe");}
                        } elseif(empty($_GET['id_doc'])){$myPage->redirect($myPage->getName()."?urlaction=docListe");}
                    }
                }
                
                // Accès par ID_DOC si existant
                if(isset($id_doc_param)) {
                    $id_doc=intval($id_doc_param);
                }
                
                //Conversion reportage
                require_once(modelDir.'model_docReportage.php');
                if (isset($id_doc) && !empty($id_doc) && Reportage::checkReportageWithId($id_doc)) {
                    unset($myDoc);
                    $myDoc = new Reportage;
                    if (isset($_REQUEST['id_lang']) && !empty($_REQUEST['id_lang']) && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myDoc->t_doc['ID_LANG']=$_REQUEST['id_lang'];
                }
                
                $myDoc->t_doc['ID_DOC']=$id_doc;
                
                $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1,1);
                if (!$myDoc->checkAccess(1)) {echo "<h3 class='error'>".kAccesReserve."</h3>";return;}
                
                $myDoc->getDocAcc();
                $myDoc->getFestival();
                $myDoc->getDocLiesDST();
                $myDoc->getDocLiesSRC();
                
                //NB 24 03 2015 Récupération des commentaire et de valeurs associées pour système de notation (initialement pour Projet DGA)
                $myDoc->getDocComments(true);
                
                
                // Log de l'action
                logAction("DOC",array("ID_DOC"=>$myDoc->t_doc['ID_DOC']));
                
				$this->init_showParam($myDoc, (isset($myDoc->t_doc['DOC_TITRE']) && !empty($myDoc->t_doc['DOC_TITRE'])?$myDoc->t_doc['DOC_TITRE']:''));
                $this->params['highlight'] = $_SESSION['recherche_DOC']['highlight'];
                
				// récupération du niveau de privilèges de l'utilisateur sur ce document
                if ($myUser->Type!=kLoggedAdmin){
                    $this->params['params4XSL']['id_priv_us_doc']=0;
                    foreach($myUser->Groupes as $droits){
                        if($droits["ID_FONDS"]==$myDoc->t_doc["DOC_ID_FONDS"]) $this->params['params4XSL']['id_priv_us_doc']=$droits["ID_PRIV"];
                    }//PC 13/12/10 : changement du privilège admin (99)
                } else $this->params['params4XSL']['id_priv_us_doc']=99;
			
				// ADDITIONAL XML 
				// Récupération de la liste des paniers visible pour l'utilisateur (somewhat obsolete, la liste des paniers est remise à jour via ajax)
                if ($myUser->loggedIn()){
					$this->params['additionnalXML']['cart_list']=Panier::getFolders(true); 
				}else{
					$this->params['additionnalXML']['cart_list']=PanierSession::getFolders();
                }
				if(isset($rang)){
					$this->params['rang'] = $rang;
					$this->params['max_rows'] = $max_rows;
				}
				if(isset($rang_seq)){
					$this->params['rang_seq'] = $rang_seq;
				}
				// Récupération de la liste des paniers associés au meme groupe que l'utilisateur
				if(defined("gShareSelGrp") && (gShareSelGrp) && $myUser->getTypeLog()>=kLoggedAdmin){
                    $this->params['additionnalXML']['grp_cart_list'] = Panier::getGroupeFolders(false);
                }
			}
        }
    }
    
    /** Détail doc solr
     * IN : GET
     * OUT : objet entité
     */
    function do_docSolr($args = null) {
		require_once(libDir."class_chercheSolr.php");
		$mySearch=new RechercheSolr();
        $myUser=User::getInstance();
        $myPage=Page::getInstance();
        if ($myUser->loggedIn()) require_once(modelDir.'model_panier.php');
        else require_once(modelDir.'model_panier_session.php');
        
		//si réecriture d'url, pas de get id_doc, il provient de la classe page	
		if(!empty($myPage->id)){
			// MS - 23.11.16 - Pour inadlsolr on a besoin de pouvoir gérer des ids non-entiers
			$id_doc_input = $myPage->id;
		}
		else if (!empty($_GET["id"]))
		{
			$id_doc_input = intval($_GET["id"]);
		}
		else if (!empty($_GET["id_doc"]))
		{
			$id_doc_input = intval($_GET["id_doc"]);
		}
		
        if(isset($id_doc_input) || isset($_GET["rang"]) || isset($_GET["key"]))
        {
            // necessaire pour afficher le formulaire de rang
            $rang=intval($_GET["rang"]);
            $max_rows=$_SESSION['recherche_DOC_Solr']['rows'];
            
            // affichage du document avec l'id
            
            $solr_opts=unserialize(kSolrOptions);
            
            if (!empty($_SESSION['DB']))
            {
                $solr_opts['path']=$_SESSION['DB'];
            }
            
            $client_solr=new SolrClient($solr_opts);
            $query_solr=new SolrQuery();
            
            // MS - 21.10.15 traitement rang effectué AVANT traitement id_doc uniquement :
            // L'idée est que si on accède à un document via un rang (et pas d'id_panier), il est issu de la recherche, même si un id_doc est aussi défini
            // -> on va donc ajouter des traitements relatifs aux highlights;
            // Si on accède au document avec uniquement son id_doc de défini, on considère qu'on ne vient pas de la recherche, il n'y aura donc pas de highlight
            if (isset($_GET["rang"]) && !empty($_GET["rang"]) && (!empty($_SESSION['recherche_DOC_Solr']['sql']) || (isset($_GET['id_panier']) && !empty($_GET['id_panier'])) ) )
            {
                if (isset($_GET['id_panier']) && !empty($_GET['id_panier']))
                {
                    require_once(modelDir.'model_panier.php');
                    $myPanier = new Panier();
                    $myPanier->t_panier["ID_PANIER"]=intval($_GET["id_panier"]);
                    if(isset($_GET['tri'])){
                        $myPanier->sqlOrder=$myPage->getSort(false);
                    }else if(isset($_SESSION['tri_panier_doc'])){
                        $myPanier->sqlOrder=$_SESSION['tri_panier_doc'];
                    }
                    $myPanier->getPanierDoc();
					$max_rows = count($myPanier->t_panier_doc);
					
                    $myDoc=new Doc();
                    $myDoc->t_doc['ID_DOC']=intval($myPanier->t_panier_doc[intval($_GET['rang'])-1]['ID_DOC']);
                    $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1,1);
                    $query_solr->setQuery('id_doc:'.intval($myDoc->t_doc['ID_DOC']).' AND id_lang:"'.$_SESSION['langue'].'"');
				} elseif(!empty($_GET["id_import"])) {
					require_once(modelDir.'model_import.php');
					$myImp = new Import();
					$myImp->t_import["ID_IMPORT"]=intval($_GET["id_import"]);
					$myImp->getImpDocs();
					$id_doc=$myImp->t_imp_docs[$rang-1]["ID_DOC"];
					$max_rows = count($myImp->t_imp_docs);
					unset($myImp);
					
					$myDoc=new Doc();
					$myDoc->t_doc['ID_DOC']=intval($id_doc);
					$myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1,1);
					$query_solr->setQuery('id_doc:'.intval($myDoc->t_doc['ID_DOC']).' AND id_lang:"'.$_SESSION['langue'].'"');
				
				} elseif(!empty($_GET["id_reportage"])) {
					require_once(modelDir.'model_docReportage.php');
					$docRep = new Reportage();
					$docRep->t_doc['ID_DOC'] = intval($_GET["id_reportage"]);
					$docRep->t_doc['ID_LANG'] = $_SESSION['langue'];
					$arrIds = $docRep->getChildrenIds();
					$id_doc = $arrIds[$rang-1];
					$max_rows = count($arrIds);
					unset($docRep);
					
					$myDoc=new Doc();
					$myDoc->t_doc['ID_DOC']=intval($id_doc);
					$myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1,1);
					$query_solr->setQuery('id_doc:'.intval($myDoc->t_doc['ID_DOC']).' AND id_lang:"'.$_SESSION['langue'].'"');
              } else {
                    
                    // récupération de la recherche Solr
                    // MS 2015-11-10 cette récupération via unserialize / serialize permet de créer un clone de la requete solr au lieu de récupérer la référence de l'objet en session => évite que les modifications réalisées sur query_solr ici ne soient sauvées en session
                    $query_solr = new SolrQuery();
                    $query_solr->unserialize($_SESSION['recherche_DOC_Solr']['sql']->serialize());
                    
                    // on récupère l'ensemble des champs pour l'affichage de la fiche
                    $query_solr->setParam('fl','*');
                    
                    $query_solr->setStart(intval($_GET["rang"])-1);
                    $query_solr->setRows(1);
                    
                    if (defined('gNomChampIdSolr'))
                        $query_solr->addField(gNomChampIdSolr);
                    else
                        $query_solr->addField('id_doc');
                    
                    $sort_fields = $query_solr->getSortFields();
                    if (!empty($sort_fields)){
                        foreach($sort_fields as $fld){
                            $query_solr->removeSortField(trim(str_replace(array('desc','asc'),'',$fld)));
                        }
                    }
                    
                    // MS - récupération tri de la recherche doc en cours
				  /*
                    if (isset($_SESSION['recherche_DOC_Solr']["tri"]) && is_array($_SESSION['recherche_DOC_Solr']["tri"])){
                        foreach ($_SESSION['recherche_DOC_Solr']["tri"] as $champ_tri){
                            if($champ_tri[1] == 1){
                                $champ_tri[1] = SolrQuery::ORDER_DESC;
                            }else{
                                $champ_tri[1] = SolrQuery::ORDER_ASC;
                            }
							$champ_tri[0] = $mySearch->checkFieldVersionDependant($champ_tri[0]);
                            $query_solr->addSortField($champ_tri[0],$champ_tri[1]);
                        }
                    }*/
					if (isset($_SESSION['recherche_DOC_Solr']["tabOrder"]) && is_array($_SESSION['recherche_DOC_Solr']["tabOrder"])){
					  $tabOrder=$_SESSION['recherche_DOC_Solr']["tabOrder"];
					  if($tabOrder['COL'][0]!='none'){
						  foreach ($tabOrder['COL'] as $idx=>$key) {
							  if ($tabOrder['DIRECTION'][$idx] && strtoupper($tabOrder['DIRECTION'][$idx]) == 'DESC') {
								  $order = SolrQuery::ORDER_DESC;
							  }else{
								  $order = SolrQuery::ORDER_ASC;
							  }
							  $key = strtolower(str_replace(' ',',',trim($key)));
							  $aFld = explode(',',$key);
							  foreach ($aFld as $fld) {
								  $fld = $mySearch->checkFieldVersionDependant($fld);
								  $query_solr->addSortField($fld, $order);
							  }
						  }
						  
					  }
					}
				  
                    $response=$client_solr->query($query_solr);
                    
                    $docSolr = $response->getResponse()->response->docs[0] ;
                    
                    if(defined('gNomChampIdSolr') && $docSolr->offsetExists(gNomChampIdSolr)){
                        $id_doc_by_rang = $docSolr->offsetGet(gNomChampIdSolr);
                    }else if(!empty($docSolr) && $docSolr->offsetExists('id_doc')){
                        $id_doc_by_rang =  $docSolr->offsetGet('id_doc');
                    }else if (!is_object($docSolr)){
                        $id_doc_by_rang= 0;
                    }
                    
                    if(is_object($docSolr) && isset($response->getResponse()->highlighting)){
                        if (defined('gNomChampIdSolr')){
                            $key = $docSolr->offsetGet(gNomChampIdSolr);
                            $highlight_params=$response->getResponse()->highlighting->offsetGet($key);
                        }else{
                            $key = $docSolr->offsetGet("id_doc")."_".$docSolr->offsetGet('id_lang');
                            $highlight_params=$response->getResponse()->highlighting->offsetGet($key);
                        }
                    }else{
                        $highlight_params = false;
                    }
                }
            }
            
            if (isset($_GET["key"]) && !empty($_GET["key"]) ) { // cas de visionnage par partage d'url
                // trace('first key');
                $id_doc_from_key=$db->GetOne("SELECT distinct ID_DOC FROM t_doc WHERE DOC_SHORT_URL = '".$_GET["key"]."'");
                // trace($id_doc_from_key);
                if (defined('gNomChampIdSolr'))
                    $query_solr->setQuery(gNomChampIdSolr.':"'.$id_doc_from_key.'"');
                else
                    $query_solr->setQuery('id_doc:'.($id_doc_from_key).' AND id_lang:"'.$_SESSION['langue'].'"');
                
                $query_solr->setStart(0);
                
                $myDoc=new Doc();
                $myDoc->t_doc['ID_DOC']=intval($id_doc_from_key);
                $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1);
                
                $rang=0;
            } else if (
                       isset($id_doc_input) && !empty($id_doc_input) && ( (!isset($_GET['rang']) || empty($_GET['rang'])) || (!isset($id_doc_by_rang) || $id_doc_by_rang != $id_doc_input) )
                       )
            {
                if (defined('gNomChampIdSolr'))
                    $query_solr->setQuery(gNomChampIdSolr.':"'.$id_doc_input.'"');
                else
                    $query_solr->setQuery('id_doc:'.($id_doc_input).' AND id_lang:"'.$_SESSION['langue'].'"');
                
                $query_solr->setStart(0);
                
                $myDoc=new Doc();
                $myDoc->t_doc['ID_DOC']=intval($id_doc_input);
                $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1);
            }
            
            //trace(print_r($myDoc, 1));
            
            
            $search = array(" ' ",'&','<','>');
            $replace = array("'",'&#38;','&#60;','&#62;');
            
            /* mlt */
            $query_solr->setMlt(true);
            $query_solr->addMltField('doc_titre');
            $query_solr->addMltField('doc_res');
            //$query_solr->addMltField('text');
            
            $query_solr->setMltCount(count($_SESSION['arrLangues'])*5);
            $query_solr->addMltQueryField('id_lang:"'.$_SESSION['langue'].'"',0);
            $query_solr->setMltMinWordLength(3);
            $query_solr->setMltMinDocFrequency(2);
            $query_solr->setMltMinTermFrequency(5);
            
            // trace($query_solr);
            
            $response=$client_solr->query($query_solr);
            
            if(!isset($myDoc)){
                if (defined('gNomChampIdSolr')){
                    $myDoc=new Doc();
                    $myDoc->t_doc['ID_DOC']=$response->getResponse()->response->docs[0]->offsetGet(gNomChampIdSolr);
                    // $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1);
                    $myDoc->getDoc();
                }else{
                    $myDoc=new Doc();
                    $myDoc->t_doc['ID_DOC']=intval($response->getResponse()->response->docs[0]->id_doc);
                    // $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1);
                    $myDoc->getDoc();
                }
            }

			//Conversion reportage
			require_once(modelDir.'model_docReportage.php');
			if (Reportage::isReportageEnabled() && ((isset($myDoc->t_doc['DOC_ID_TYPE_DOC']) && $myDoc->t_doc['DOC_ID_TYPE_DOC'] == gReportagePicturesActiv) || (!empty($myDoc->t_doc['ID_DOC']) && Reportage::checkReportageWithId($myDoc->t_doc['ID_DOC'])))) {
				$id_doc=$myDoc->t_doc['ID_DOC'];
				unset($myDoc);
				$myDoc = new Reportage;
				$myDoc->t_doc['ID_DOC']=intval($id_doc);
				// $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1);
				$myDoc->getDoc();
				if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myDoc->t_doc['ID_LANG']=$_REQUEST['id_lang'];
				if (empty($myDoc->t_doc['ID_LANG']))
					$myDoc->t_doc['ID_LANG']=$_SESSION['langue'];
			}

			// Ces champs étaient récupérés automatiquement dans la version solr de l'xml_export, je les remet donc ici pour l'instant :
			$myDoc->getDocAcc();
			// Méthode de test de l'existence de l'objet mat dans la table t_doc_mat
			// Le test est un peu tordu, mais il reste ainsi compatible avec php 5.3 en évitant les appels nested des fonctions natives php.
			if(empty($myDoc->t_doc_mat) || reset($myDoc->t_doc_mat) && !isset($myDoc->t_doc_mat[key($myDoc->t_doc_mat)]['MAT'])){
				$myDoc->getMats();
			}
			$myDoc->getUserCreaModif();
			$myDoc->getSequences();
			$myDoc->getRedif();
			$myDoc->getDocLiesDST();
			$myDoc->getDocLiesSRC();
			$myDoc->getFestival() ; 
			$myDoc->getDocComments(true);
			$myDoc->getDocLexDroit();
			$myDoc->getDocPrex();
			$myDoc->getDocFreres();
			
            $arrFieldsToSaveWithIdLang=$myDoc->getArrFieldsToSaveWithIdLang() ;
            $arrFieldsToSaveWithIdLang = array_map('strtolower',$arrFieldsToSaveWithIdLang);
            
            if (!$myDoc->checkAccess(1)){
                echo "<h3 class='error'>".kAccesReserve."</h3>";
                return;
            }
            
			
			// la partie composition du xml à partir de l'objet solr a été déplacée dans le model_doc, 
			// ici on alimente juste l'objet avec les éléments pertinents
			// ce qui permet de continuer à utiliser les fonctions par défaut (plus besoin de passer par un param[contentXml] spécifique
            if(!empty($response->getResponse()->response->docs)){
				$myDoc->solrObj = $response->getResponse()->response->docs[0];
				if($response->getResponse()->moreLikeThis){
					$myDoc->moreLikeThis = $response->getResponse()->moreLikeThis;
				}
			}

			if ($myUser->loggedIn()){
				$this->params['additionnalXML']['cart_list']=Panier::getFolders(true);
			}else{
				$this->params['additionnalXML']['cart_list']=PanierSession::getFolders();
            }
			// Ajout des roles
			$this->params['additionnalXML']['roles'] = $myDoc->getRoles();
			
			// Log de l'action
            logAction("DOC",array("ID_DOC"=>$myDoc->t_doc['ID_DOC']));
            
            // Paramètres pour view
			$this->init_showParam($myDoc, (isset($myDoc->t_doc['DOC_TITRE']) && !empty($myDoc->t_doc['DOC_TITRE'])?$myDoc->t_doc['DOC_TITRE']:''));
            $this->params['highlight'] = (isset($highlight_params)?$highlight_params:false);
            if(isset($rang)){
				$this->params['rang'] = $rang;
				$this->params['max_rows'] = $max_rows;
			}
        }
    }
    
    
    /** Liste doc
     * IN : GET, POST
     * OUT : objet cherche
     */
    function do_docListe($args=null) {
        global $db;
        
		require_once(modelDir.'model_requete.php');
		
        $myPage=Page::getInstance();
        $myPage->setReferrer();
        
        $myUsr=User::getInstance();
        
        // MS - 21.09.15 - Possibilité d'inclure un script php du site AVANT l'execution de la requete
        // entete.inc.php permettait ce genre de hack dans les versions opsis <3.0.4 puisqu'il était executé avant la recherche
        // facilitera les montées de versions des anciens sites et laissera cette possibilité de hack de la recherche, le tout en étant correctement séparé de l'affichage
        if(file_exists(designDir."/include/pre_docListe.inc.php")){
            include(designDir."/include/pre_docListe.inc.php");
        }
        
        // Choix moteur de recherche
        if (defined("useSolr") && useSolr==true) {
            require_once(libDir."class_chercheSolr.php");
            $mySearch=new RechercheSolr();
			// Facettes solr
			if ($_REQUEST['xml_solr_facet']){
				$mySearch->solr_facet_file = $_REQUEST['xml_solr_facet'];
			}         
		} else if (defined("useSinequa") && useSinequa==true) {
            require_once(libDir."class_chercheSinequa.php");
            $mySearch=new RechercheSinequa();
        } else if (defined("useBasis") && useBasis==true) {
            require_once(libDir."class_chercheBasis.php");
            $mySearch=new RechercheBasis();
        } else {
            require_once(libDir."class_chercheDoc.php");
            $mySearch=new RechercheDoc();
        }
        // Choix BDD
        if (!empty($_SESSION['DB'])) {
            $mySearch->setDatabase($_SESSION['DB']);
        }
        
        if (isset($_REQUEST['sessVar']) && !empty($_REQUEST['sessVar'])) $mySearch->sessVar = $_REQUEST['sessVar'];
        
        if ($myUsr->loggedIn()) require_once(modelDir.'model_panier.php');
        else require_once(modelDir.'model_panier_session.php');
        
        
        // I. Traitement des parametres, affichage d'une requ�te utilisateur
        
        if(isset($_POST["commande"])){
            // Suppression doc
            if($_POST["commande"]=="SUP" && !empty($_POST["ligne"])) {
                $myDoc = new Doc();
                $myDoc->t_doc['ID_DOC']=intval($_POST["ligne"]);
                if($myDoc->checkAccess(4)) {
                    $myDoc->delete($_SESSION["langue"]);
                    $_SESSION[$mySearch->sessVar]["no_cache_sql"] = "1";
                }
            }
            
        } elseif((isset($_GET["precherche"]) || isset($_POST["F_doc_form"]) ) && ( (!isset($_POST['refine']) || empty($_POST['refine'])) || (isset($_POST['reset_refine']) && !empty($_POST['reset_refine']) ))) {
            // MS - 17.09.15 : On (ré)initialise la recherche uniquement si :
            //		* Precherche ou F_doc_form sont définis,
            //		* ET si on a aucun refine défini OU si le flag reset du refine est défini & supérieur à 1.
            
            // GT 15-04-11 : calcul de la durée d'un action SQL
            $date_debut_requete_sql=microtime(true);
            
            if (!empty($_POST['most_viewed']))
                $mySearch->prepareSQL(true);
            else
                $mySearch->prepareSQL();
            
            $mySearch->saveHisto=true;
            $mySearch->AnalyzeFields($_POST['chFields'],$_POST['chValues'],$_POST['chTypes'],$_POST['chOps'],$_POST['chLibs'],$_POST['chValues2'],$_POST['chNoHLs'],$_POST['chNoHist'],$_POST['chOptions']);
            if ( (defined('gSeuilAppliqueDroits') && $myUsr->Type < intval(gSeuilAppliqueDroits) )
                ||(!defined('gSeuilAppliqueDroits') && $myUsr->Type<kLoggedDoc ) ){
                $mySearch->appliqueDroits();
            }
            
            $mySearch->finaliseRequete(); //fin et mise en session
            
			// pr rappel etait désactivé pour basis 
			$id_action = logAction("SQL",array("ACT_FORM"=>$_POST["F_doc_form"],"ACT_REQ"=>$mySearch->etape,"ACT_SQL"=>$mySearch->sql,'duree_req_sql'=>(microtime(true)-$date_debut_requete_sql)));
            
            
        } else if(isset($_GET["recherche"]) || isset($_GET["hist"])) {
            // Lecture d'une requête dans l'historique
            
            $id=(isset($_GET["hist"])?$_GET["hist"]:$_GET["recherche"]);
            $mySearch->getSearchFromDB($id);
            
        } else if(isset($_GET["croiserRequete"]) && isset($_GET["operateur"])) {
            // Croisement requetes
            include_once(modelDir.'model_requete.php');
            $_GET['croiserRequete']=unserialize($_GET['croiserRequete']);
            $list_id_requetes=array();
            foreach ($_GET['croiserRequete'] as $cb_req) {
                if (isset($cb_req) && !empty($cb_req))
                {
                    $list_id_requetes[]=intval($cb_req);
                }
            }
            $requete_fusion=Requete::croiserRequetes($list_id_requetes,$_GET['operateur']);
            $mySearch->getSearchFromDB($requete_fusion->t_requete['ID_REQ']);
            $mySearch->saveHisto = true;
            
        } elseif(isset($_GET["init"])) {
            // Initialisation
            $mySearch->initSession();
            
        } else {
            // Lecture session
            $mySearch->getSearchFromSession();
        }
        
        // Paramètres affichage
        $nbLignesDefaut = (defined("gNbLignesDocListeDefaut")?gNbLignesDocListeDefaut:10);
        $mySearch->setNbLignes($_REQUEST['nbLignes'], $nbLignesDefaut);
        $mySearch->setPage($_REQUEST['page']);
        
        // Tri
        $_SESSION[$mySearch->sessVar]['most_viewed']='0';
        if (isset($_POST['orderbydeft']) && $_POST['orderbydeft']) {
            $deft_field = $_POST['orderbydeft'];
        } elseif (isset($_POST['most_viewed']) && !empty($_POST['most_viewed'])) {
            $deft_field = array("COL" => array("NB_VIEW"), "PREFIX" => array(""), "DIRECTION" => array("DESC"));
            $_SESSION[$mySearch->sessVar]['most_viewed']='1';
        } elseif (defined('gOrderByDefault') && gOrderByDefault!="") {
            $deft_field = unserialize(gOrderByDefault);
        } else {
            $deft_field = array("COL" => array("DOC_DATE_CREA"), "PREFIX" => array("t1"), "DIRECTION" => array("DESC"));
        }
		// trace("mySearch before setOrderBy : ".print_r($mySearch,true));
        $mySearch->setOrderBy($_REQUEST['tri'], $deft_field);
        
		
		
		
		
        // Refine
        $refine = (isset($_REQUEST['refine'])?$_REQUEST['refine']:null);
        $reset_refine = (isset($_REQUEST['reset_refine'])?$_REQUEST['reset_refine']:null);
        $refine_facet = (isset($_REQUEST['refine_facet'])?$_REQUEST['refine_facet']:null);
        $mySearch->setRefine($refine, $reset_refine,$refine_facet);
		$mySearch->refine();
		
		// gestion param from_offset, en général permet de régler manuellement un offset dans la recherche (rechercher éléments à partir de l'index donné par from_offset)
		if(isset($_REQUEST['from_offset']) && !empty($_REQUEST['from_offset'])){
			$mySearch->setOffset(intval($_REQUEST['from_offset']));
		}
	
      
        
        // Exécution requête
        $mySearch->execute();
        
        //trace(print_r($mySearch, true));
        
        // Calcul nb de pages
        $mySearch->setNbPages();
        
        // Mise à jour log action
        if (isset($id_action) && intval($id_action) > 0) {
            updateAction($id_action, array("ACT_REQ_NB_DOC" => $mySearch->found_rows));
        }
        
        // Sauvegarde de la recherche en histo voire dans "mes recherches" si un titre est fourni.
        // Cette action met à jour l'objet mySearch avec les id de recherche.
        // Le JS met à jour d'éventuels champs de "nom de recherche" avec les infos
        if ($mySearch->saveHisto || !empty($_POST['REQUETE'])){
            // $mySearch->getSearchFromSession();
            // VP 2/07/14 : pas de mise à jour de l'historique dans le cas d'un refine
            if(empty($mySearch->str_refine)){
                $mySearch->saveSearchInDB(isset($_POST['REQUETE'])?$_POST['REQUETE']:'');
            }
        }
        
        
        // III. Affichage des resultats
        // Préparation affichage
        
        $this->params['searchObj'] = $mySearch;
        
        // Paniers
        if ($myUsr->loggedIn())
            $this->params['additionnalXML']['cart_list'] = Panier::getFolders(true);
        else
            $this->params['additionnalXML']['cart_list'] = PanierSession::getFolders();
        
        if(defined("gShareSelGrp") && (gShareSelGrp) && $myUsr->getTypeLog()>=kLoggedAdmin){
            $this->params['additionnalXML']['grp_cart_list'] = Panier::getGroupeFolders(false);
        }
        
        if (isset($myPanier)) {
            $this->params["titre"]= $myPanier->titre;
        } else {
            $this->params["titre"] = kListeDocuments;
        }
        
        // Catégories
        $isDocListeWithCategories = false;
        if (defined('gDocListeWithCategories')) $isDocListeWithCategories = gDocListeWithCategories;
        if ($myUsr->loggedIn() && $isDocListeWithCategories) {
			require_once(modelDir.'model_categorie.php');
            $tmp_cat=new Categorie();
            $this->params['additionnalXML']['cat_list'] = $tmp_cat->getCats();
        }
        
        
        
        if(isset($_REQUEST['style']) && $_REQUEST['style'] != '') {
            $docListeXSL = $_REQUEST['style'];
        } elseif(isset($_SESSION[$mySearch->sessVar]['docListeXSL'])) {
            $docListeXSL = $_SESSION[$mySearch->sessVar]['docListeXSL'];
        } else {
            $docListeXSL = "docListe.xsl";
        }
        
        if (isset($_REQUEST['xml_liste']) && $_REQUEST['xml_liste']!=''){
            $xml_liste=$_REQUEST['xml_liste'];
            if(strpos($xml_liste,'xml/')!==0){
                $xml_liste = "xml/".$xml_liste;
            }
        }elseif(isset($_SESSION[$mySearch->sessVar]['docListeXML'])){
            $xml_liste = $_SESSION[$mySearch->sessVar]['docListeXML'];
        } else {
            $xml_liste = "xml/docListe.xml";
        }
        
        $this->params['nomElement'] = "doc";
        $this->params['sauvRequete'] = '';
        $this->params['contentTemplate'] = getSiteFile("designDir","templates/content/docListe.html");
        $this->params['xmlFormFile'] = getSiteFile("designDir","form/xml/chercheDoc.xml");
        $this->params['xslListFile'] = getSiteFile("listeDir",$docListeXSL);
        $this->params['xmlListFile'] = getSiteFile("listeDir",$xml_liste);
        
        // Stockage session
        $_SESSION[$mySearch->sessVar]['page'] = $mySearch->page;
        $_SESSION[$mySearch->sessVar]['nbLignes'] = $mySearch->nbLignes;
        $_SESSION[$mySearch->sessVar]['tabOrder'] = $mySearch->tabOrder;
        $_SESSION[$mySearch->sessVar]['rows'] = $mySearch->found_rows;
        $_SESSION[$mySearch->sessVar]['docListeXML'] = $xml_liste;
		if((!isset($_POST['reset_refine']) || empty($_POST['reset_refine']) || $_POST['reset_refine'] == '2')){
			$_SESSION[$mySearch->sessVar]['refine'] = $mySearch->str_refine;
			
		}else{
			$_SESSION[$mySearch->sessVar]['refine'] = '';
		}
		if(!isset($_REQUEST['dontUpdateSessStyle']) || (isset($_REQUEST['dontUpdateSessStyle']) && !$_REQUEST['dontUpdateSessStyle'])){
			$_SESSION[$mySearch->sessVar]['docListeXSL'] = $docListeXSL;
		}
        if (defined('useSolr') && useSolr){
			// sauvegarde de la référence vers le fichier solr_facet qui peut eventuellement etre custom 
			if(isset($mySearch->solr_facet_file_fullpath) && !empty($mySearch->solr_facet_file_fullpath)){
				$_SESSION[$mySearch->sessVar]['solr_facet_file_fullpath'] = $mySearch->solr_facet_file_fullpath;
			}
			
            if(isset($_GET["precherche"]) || isset($_POST["F_doc_form"]) ){
                $_SESSION[$mySearch->sessVar]['highlight']=$mySearch->getPageSolrHighlight();
            }else{
                $_SESSION[$mySearch->sessVar]['highlight'] = $mySearch->mergeSolrObjects($_SESSION[$mySearch->sessVar]['highlight'],$mySearch->getPageSolrHighlight());
            }
            $_SESSION[$mySearch->sessVar]['noHls'] = $mySearch->champs_noHls;
            
            $_SESSION[$mySearch->sessVar]['facet'] = $mySearch->getPageSolrFacet();
        }
		
		if($_GET['mode']=="carto" || (isset($_SESSION['USER']['layout_opts']['docListe']) && $_SESSION['USER']['layout_opts']['docListe']['affMode']=="carto")){
			$geojson = array(
							 'type'      => 'FeatureCollection',
							 'features'  => array()
							 );
			$k = 0;
			foreach ($mySearch->getArrayFacets() as $facet){
				if (strpos($facet['field'], "coor_")===0) {
					$coor = explode (',', $facet['key']);
					$feature = array(
						'id' => $k++,
						'type' => 'Feature',
						'geometry' => array(
							'type' => 'Point',
							'coordinates' => array($coor[1],$coor[0])
						),
						'properties' => array(
							'count' => $facet['valeur']
						)
					);
					array_push($geojson['features'], $feature);
				}
				
			}
			$mySearch->geojson = json_encode($geojson, JSON_NUMERIC_CHECK);
		}
		
    }
    
    
    /** Saisie Doc
     * IN : GET, POST, objet Doc
     * OUT : objet Doc
     */
    function do_docSaisie($args=null){
		global $db;
		$myPage=Page::getInstance();
		$myPage->nomEntite = kDocument;
		// $myPage->setReferrer(true);

		$myDoc = new Doc();
		$myDoc->updateFromArray($_POST,false);
		if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myDoc->t_doc['ID_LANG']=$_REQUEST['id_lang'];
		if (empty($myDoc->t_doc['ID_LANG'])) $myDoc->t_doc['ID_LANG']=$_SESSION['langue'];
		
		if (isset($_POST["commande"]))
			$action=$_POST["commande"];
		else
			$action='';
		
		if(isset($_GET["rang_seq"]) && $_GET["rang_seq"] != '' ) { //Gestion de la navigation parmi les séquences d'un document
			//update VG 12/08/11 : ajout de la gestion des rang
			//update VG 07/10/11 : modif de la gestion des rangs		
			$rang_seq=intval($_GET["rang_seq"]);
			//debug("rang_seq", "red", true);
			if(!empty($_GET["id_doc_pere"])) {
				$docPere = new Doc();
				$docPere->t_doc['ID_DOC'] = intval($_GET["id_doc_pere"]);
				$docPere->t_doc['ID_LANG'] = $myDoc->t_doc['ID_LANG'];
				$docPere->getChildren();
				$id_doc = $docPere->arrChildren[$rang_seq]['ID_DOC'];
			//update VG 05/04/13 : on redirige dans le cas où on ne peut PAS identifier le document.
			//} elseif(!empty($_GET['id_doc'])) {
			} elseif(empty($_GET['id_doc'])) {
				$myPage->redirect($myPage->getName()."?urlaction=docListe");
			}
		} elseif(isset($_GET['rang'])) {
			$rang=intval($_GET["rang"]);
			if($rang==0) $rang=1;
			
			if(!empty($_GET["id_panier"])){
				require_once(modelDir.'model_panier.php');
				$myPanier = new Panier();
				$myPanier->t_panier["ID_PANIER"]=intval($_GET["id_panier"]);
				$myPanier->getPanierDoc();
				$id_doc=$myPanier->t_panier_doc[$rang-1]["ID_DOC"];
				$max_rows = count($myPanier->t_panier_doc);
			} elseif(!empty($_GET["id_import"])) {
				require_once(modelDir.'model_import.php');
				$myImp = new Import();
				$myImp->t_import["ID_IMPORT"]=intval($_GET["id_import"]);
				$myImp->getImpDocs();
				$id_doc=$myImp->t_imp_docs[$rang-1]["ID_DOC"];
				$max_rows = count($myImp->t_imp_docs);
				unset($myImp);
			} elseif(!empty($_GET["id_reportage"])) {
				require_once(modelDir.'model_docReportage.php');
				$docRep = new Reportage();
				$docRep->t_doc['ID_DOC'] = intval($_GET["id_reportage"]);
				$docRep->t_doc['ID_LANG'] = $_SESSION['langue'];
				$arrIds = $docRep->getChildrenIds();
				$id_doc = $arrIds[$rang-1];
				$max_rows = count($arrIds);
				unset($docRep);
			} else {
				if (defined('useSolr') && useSolr==true)
				{
					$solr_opts=unserialize(kSolrOptions);
		
					if (!empty($_SESSION['DB']))
					{
						$solr_opts['path']=$_SESSION['DB'];
					}
					
					$client_solr=new SolrClient($solr_opts);
					//$client_solr=new SolrClient(unserialize(kSolrOptions));
					$query_rech=new SolrQuery();
					if (isset($_SESSION['recherche_DOC_Solr']['sql']))
					{	
						// récupération de la recherche Solr
						// MS 2015-11-10 cette récupération via unserialize / serialize permet de créer un clone de la requete solr au lieu de récupérer la référence de l'objet en session => évite que les modifications réalisées sur query_solr ici ne soient sauvées en session
						$query_rech->unserialize($_SESSION['recherche_DOC_Solr']['sql']->serialize());
						$query_rech->setStart(intval($_GET["rang"])-1);
						$query_rech->setRows(1);
						$query_rech->addField('id_doc');
						$response=$client_solr->query($query_rech);
						
						$id_doc=$response->getResponse()->response->docs[0]->id_doc;
					}
					/*$myDoc=new Doc();
					$myDoc->t_doc['ID_DOC']=intval($response->getResponse()->response->docs[0]->id_doc);
					$myDoc->getDoc();*/
					
					//$query_solr->setQuery('id_doc:'.intval($response->getResponse()->response->docs[0]->id_doc).' AND id_lang:"'.$_SESSION['langue'].'"');
				}
				else
				{
					$sql=$_SESSION["recherche_DOC"]["sql"];
					// VP 2/9/09 : déplacement test sql
					if (!empty($sql)) {
						if(isset($_SESSION["recherche_DOC"]["refine"])) $sql.= $_SESSION["recherche_DOC"]["refine"];
						if(isset($_SESSION["recherche_DOC"]["tri"])) $sql.= $_SESSION["recherche_DOC"]["tri"];
						//debug($sql,'lime');
						$result=$db->Execute($sql." limit 1 OFFSET ".($rang-1));
						if($result && $row=$result->FetchRow()){
							$id_doc=$row["ID_DOC"];
						}
						$result->Close();
					//update VG 05/04/13 : on redirige dans le cas où on ne peut PAS identifier le document.
					//} elseif(!empty($_GET['id_doc'])) {$myPage->redirect($myPage->getName()."?urlaction=docListe");}
					} elseif(empty($_GET['id_doc'])) {$myPage->redirect($myPage->getName()."?urlaction=docListe");}
				}
			}
		}

		// Accès par ID_DOC si existant
		if(isset($_GET["id_doc"])) {
			$id_doc=intval($_GET["id_doc"]);
		}
				
		if(!empty($id_doc)) {
			$myDoc->t_doc['ID_DOC']=intval($id_doc);
		}
		
		//Test reportage
		require_once(modelDir.'model_docReportage.php');
		if (Reportage::isReportageEnabled() && ((isset($myDoc->t_doc['DOC_ID_TYPE_DOC']) && $myDoc->t_doc['DOC_ID_TYPE_DOC'] == gReportagePicturesActiv) || (!empty($myDoc->t_doc['ID_DOC']) && Reportage::checkReportageWithId($myDoc->t_doc['ID_DOC'])))) {
			unset($myDoc);
			$myDoc = new Reportage;
			$myDoc->updateFromArray($_POST,false);
			if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myDoc->t_doc['ID_LANG']=$_REQUEST['id_lang'];
			if (empty($myDoc->t_doc['ID_LANG'])) $myDoc->t_doc['ID_LANG']=$_SESSION['langue'];
			if(!empty($id_doc))
				$myDoc->t_doc['ID_DOC']=intval($id_doc);
		}
		
		/************************************/
		/*			GESTION D'ACCES			*/
		/************************************/
		$myDocAcces = clone $myDoc;
		$myDocAcces->getDoc();
		if (!$myDocAcces->checkAccess(4)) {throw new Exception("<b>".kAccesReserve."</b>");}
		
		debug($myDoc, "pink", true);
		$ligne=(isset($_POST["ligne"]))?$_POST["ligne"]:'';
			if(isset($_POST)){
				$action = $_REQUEST['commande'];
				if(isset($_POST["commande_additionnelle"]))
					$action.="-".$_POST["commande_additionnelle"];
				
				$FORM=$_POST;
				$id_doc = $myDoc->t_doc['ID_DOC'];
				
				switch ($action) {
						
					case "SUP_DOC":
						
						$myDoc->getDocLiesSRC();
						$arrSrc = $myDoc->arrDocLiesSRC;
						
						if(!empty($_POST["version2suppr"])) {
							//Ne supprimons les séquences que s'il n'existe pas d'autres versions de la fiche
							$myDocTest = clone $myDoc;
							$myDocTest->getVersions();
							if(empty($myDocTest->arrVersions)) {
								$myDoc->deleteSequences(); //by LD 27/02/08 suppr sequences ! a garder ?
							}
						}
						$myDoc->delete($_POST["version2suppr"],$_POST['delete_assoc_mats']);
						   
						if (Reportage::isReportageEnabled() && defined("gReportToHeritFields")) {
							if (isset($arrSrc) && !empty($arrSrc) && is_array($arrSrc))
								foreach ($arrSrc as $idx=>&$dl)
									if (!empty($dl['ID_DOC_DST']) && Reportage::checkReportageWithId($dl['ID_DOC_DST'])) {
										$myRep = new Reportage;
										$myRep->t_doc['ID_DOC']=intval($dl['ID_DOC_DST']);
										$myRep->heritAllFieldsToReport();
									}
						
							//Suppression vignette reportage
							foreach ($arrSrc as $src)
								if ($src['DOC_ID_IMAGE'] == $myDoc->t_doc['DOC_ID_IMAGE']) {
									$myRep = new Reportage;
									$myRep->t_doc['ID_DOC']=intval($src['ID_DOC_DST']);
									$myRep->saveVignette("doc_id_image", "");
								}
						}
						
						if(!empty($_POST["page"]) && empty($_POST["version2suppr"])){
							  $myPage->redirect($_POST["page"]);
						}
						break;
						
						// II.2. Sauvegarde enregistrement
					case "SAVE_DOC":
					case "SAVE":
					case "DUP_DOC" :
					case "SAVE-SAVE_DPREX":
						
						$myDoc->doc_trad=$_POST['doc_trad'];
						 if(empty($myDoc->t_doc['DOC_DATE_ETAT'])){
					
								$myDoc->t_doc['DOC_DATE_ETAT']='';
							}

						// MS 06/05/13 - ajout possibilité de sauvegarder les différentes versions d'un même champ (ex : DOC_RES) 
						// voir fonction class_doc getVersions et updateVersions
						if(isset($_POST['version'])){
							$myDoc->updateVersions($_POST['version']);
						}
							//Réorganisation des valeurs du POST pour leur donner la màªme structure de pour l'objet doc
						// VP 16/11/09 : ajout champ LEX_TERME et PERS_NOM pour saisie en liste
						if (!empty($FORM['t_doc_lex'])){
							$myDoc->t_doc_lex = $this->transformArrayMulti($FORM['t_doc_lex'], array('ID_LEX', 'ID_PERS'),'ID_TYPE_DESC');
						}
						// MS 25/07/14 ajout gestion des text_doc_lex, champ de lexiques en saisie libre 
						if(!empty($FORM['text_doc_lex'])){
                            end($myDoc->t_doc_lex);
							$lastID=key($myDoc->t_doc_lex);
							$params_text_lex = array();
							foreach($FORM['text_doc_lex'] as $idx_=>$text_input){
								// définition séparateur de la saisie texte
								if(isset($text_input['sep']) && !empty($text_input['sep'])){ $separator = $text_input['sep']; }else {$separator = ',';}
								
								// stockage des paramètres à ajouter à chaque terme extrait du texte
								if (isset($text_input['DLEX_ID_ROLE'])) {$params_text_lex['DLEX_ID_ROLE']=$text_input['DLEX_ID_ROLE'];}
								if (isset($text_input['ID_TYPE_LEX'])) {$params_text_lex['ID_TYPE_LEX']=$text_input['ID_TYPE_LEX'];}
								if (isset($text_input['PERS_ID_TYPE_LEX'])) {$params_text_lex['PERS_ID_TYPE_LEX']=$text_input['PERS_ID_TYPE_LEX'];}
								if (isset($text_input['ID_TYPE_DESC'])) {$params_text_lex['ID_TYPE_DESC']=$text_input['ID_TYPE_DESC'];}
								
								
								
								// traitement du texte
								if(isset($text_input['text']) && !empty($text_input['text'])){
									if(array_key_exists('ID_TYPE_LEX',$params_text_lex) && !empty($params_text_lex['ID_TYPE_LEX'])){
										$type_insert = 'l';
									}else if(array_key_exists('PERS_ID_TYPE_LEX',$params_text_lex) && !empty($params_text_lex['PERS_ID_TYPE_LEX'])){
										$type_insert = 'p';
									}else{
										continue ; 
									}
									// split selon separator
									$text_split = explode($separator,$text_input['text']);
									foreach($text_split as $idx=>$term){
										// chaque terme est ajouté à t_doc_lex avec comme index lastID, 
										$lastID++;
										// suivant le type de valeurs à insérer : 
										if($type_insert == 'l'){
											$myDoc->t_doc_lex[$lastID]['LEX_TERME'] = trim($term);
										}else if($type_insert == 'p'){
											$arr_term = explode(' ',trim($term));
											if(count($arr_term)>1){
												if(defined("gPersFirstTerm") && gPersFirstTerm == 'prenom'){
													$myDoc->t_doc_lex[$lastID]['PERS_PRENOM'] = $arr_term[0];
													unset($arr_term[0]);
													$myDoc->t_doc_lex[$lastID]['PERS_NOM'] = implode(' ',$arr_term);
												}else if(!defined("gPersFirstTerm") || gPersFirstTerm == 'nom'){
													$myDoc->t_doc_lex[$lastID]['PERS_PRENOM'] = $arr_term[count($arr_term)-1];
													unset($arr_term[count($arr_term)-1]);
													$myDoc->t_doc_lex[$lastID]['PERS_NOM'] = implode(' ',$arr_term);
												}
											}else{
												$myDoc->t_doc_lex[$lastID]['PERS_NOM'] = $term;
											}
										}
										// l'ensemble des params est appliqué à chacun des termes
										if(!empty($params_text_lex)){
											foreach($params_text_lex as $key=>$param){
												$myDoc->t_doc_lex[$lastID][$key] = $param;
											}
										}
									}
									// les params sont remis à 0 pour le champ suivant
									$params_text_lex = array() ; 
								}
							}
						}

						// VP 27/11/09 : ajout champ VALEUR pour saisie en liste
						if (!empty($FORM['t_doc_val'])){
							$myDoc->t_doc_val = $this->transformArrayMulti($FORM['t_doc_val'], array('ID_VAL'),'ID_TYPE_VAL');
						}
						// VP 29/07/14 ajout gestion des text_doc_val, champ de valeurs en saisie libre 
						if(!empty($FORM['text_doc_val'])){
							 end($myDoc->t_doc_val);
                            $lastID=key($myDoc->t_doc_val);
							$params_text_lex = array();
							foreach($FORM['text_doc_val'] as $idx_=>$text_input){
								// définition séparateur de la saisie texte
								if(isset($text_input['sep']) && !empty($text_input['sep'])){ $separator = $text_input['sep']; }else {$separator = ',';}
								
								// stockage des paramètres à ajouter à chaque terme extrait du texte
								if (isset($text_input['VAL_ID_TYPE_VAL'])) {$params_text_lex['VAL_ID_TYPE_VAL']=$text_input['VAL_ID_TYPE_VAL'];}
								if (isset($text_input['ID_TYPE_VAL'])) {$params_text_lex['ID_TYPE_VAL']=$text_input['ID_TYPE_VAL'];}
								
								// traitement du texte
								if(isset($text_input['text']) && !empty($text_input['text'])){
									// split selon separator
									$text_split = explode($separator,$text_input['text']);
									foreach($text_split as $idx=>$term){
										// chaque terme est ajouté à t_doc_val avec comme index lastID, 
										$lastID++;
										$myDoc->t_doc_val[$lastID]['VALEUR'] = trim($term);
										// l'ensemble des params est appliqué à chacun des termes
										if(!empty($params_text_lex)){
											foreach($params_text_lex as $key=>$param){
												$myDoc->t_doc_val[$lastID][$key] = $param;
											}
										}
									}
									// les params sont remis à 0 pour le champ suivant
									$params_text_lex = array() ; 
								}
							}
						}
						
						if (!empty($FORM['t_doc_cat']))
						{
							$myDoc->t_doc_cat = $this->transformArrayMulti($FORM['t_doc_cat'], array('ID_CAT'),'ID_TYPE_CAT');
						}


						if (!empty($FORM['t_doc_redif'])) {
							$myDoc->t_doc_redif = $this->transformArrayMulti($FORM['t_doc_redif'], array('REDIF_DIFFUSEUR'));
							
						}

						if(isset($FORM['t_doc_liesSRC'])) $myDoc->arrDocLiesSRC=$FORM['t_doc_liesSRC'];
						if(isset($FORM['t_doc_liesDST'])) $myDoc->arrDocLiesDST=$FORM['t_doc_liesDST'];
							
						//si duplication pas besoin de save ici
						if ($action != "DUP_DOC"){
							$saved=$myDoc->save();
						}
						if($saved) {
							$myDoc->saveDocLex();
							$myDoc->saveDocVal();
							$myDoc->saveDocCat();
							$myDoc->saveDocRedif();
					
					
							$myDoc->updateChampsXML('DOC_LEX');
							$myDoc->updateChampsXML('DOC_VAL');
							$myDoc->updateChampsXML('DOC_XML');
							$myDoc->updateChampsXML('DOC_CAT');
					
							$myDoc->saveDocLies(false);
						
							if(isset($FORM['t_doc_fest'])) {
								$myDoc->t_doc_fest=$this->transformArrayMulti($FORM['t_doc_fest'], array('ID_FEST'));
								$myDoc->saveDocFest();
							}
					
							if ((defined("saveDoc2Solr") && saveDoc2Solr==true ) || (defined('useSolr') && useSolr==true)) {
								$myDoc->saveSolr();
							} elseif (defined("useSinequa") && useSinequa==true) {
								$myDoc->saveSinequa();
							}
							
								
							//PC 28/12/12 : Héritage champs
							if (defined("gInheritedFields")){
								require_once(libDir."class_modifLot.php");
								$arrFields = explode('/', gInheritedFields);
								
								$myMod=new ModifLot("doc","");
								$myMod->setSearchSQL(true, "SELECT distinct ID_DOC,'".$_SESSION['langue']."' as ID_LANG from t_doc where DOC_ID_GEN = ".intval($myDoc->t_doc['ID_DOC']));
								$hasResult=$myMod->getResults();
								if ($hasResult)
									foreach ($arrFields as $field){
										$arrWF = explode("_", $field);
										switch ($arrWF[0]) {
											case "DOC":
												if (isset($myDoc->t_doc[$field])) {
													$myMod->updateFromArray(array("field" => $field, "value" => $myDoc->t_doc[$field], 'commande' => "INIT"));
													$myMod->applyModif(); }
												break;
											case "L" :
												$arrIds = array();
												foreach ($FORM['t_doc_lex'] as $docLex) if ($docLex['ID_TYPE_DESC'] == $arrWF[1] && (!isset($arrWF[2]) || $docLex['DLEX_ID_ROLE'] == $arrWF[2])) $arrIds[] = array('ID_LEX' => $docLex['ID_LEX'], "DLEX_ID_ROLE" => $docLex['DLEX_ID_ROLE'], "ID_TYPE_DESC" => $docLex['ID_TYPE_DESC']);
												if (count($arrIds) > 0 ) {
													$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => "INIT"));
													$myMod->applyModif(); }
												break;
											case "V" :
												$arrIds = array();
												foreach ($FORM['t_doc_val'] as $docVal) if ($docVal['ID_TYPE_VAL'] == $arrWF[1]) $arrIds[] = $docVal['ID_VAL'];
												if (count($arrIds) > 0 ) {
													$myMod->updateFromArray(array("field" => $field, "id" => $arrIds, 'commande' => "INIT"));
													$myMod->applyModif(); }
												break;
										}
									}
								unset($myMod);
							}
							
							if (Reportage::isReportageEnabled() && defined("gReportToHeritFields")) {
								$myDoc->heritFieldsToReport();
								/*$myDoc->getDocLiesSRC();
								if (isset($myDoc->arrDocLiesSRC) && !empty($myDoc->arrDocLiesSRC) && is_array($myDoc->arrDocLiesSRC))
									foreach ($myDoc->arrDocLiesSRC as $idx=>&$dl)
										if (!empty($dl['ID_DOC_DST']) && Reportage::checkReportageWithId($dl['ID_DOC_DST'])) {
											$myRep = new Reportage;
											$myRep->t_doc['ID_DOC']=intval($dl['ID_DOC_DST']);
											$myRep->heritReportFields();
										}*/
							}
						}
						// II.2.C. Duplication de l'enregistrement dans la langue courante
						if ($action == "DUP_DOC"){
							//update VG : duplicated=1 : permet de savoir que la notice vient d'être dupliquée.
							//Utile pour les habituelles fonctions de nettoyage/héritage spécifiques aux clients
							$newDoc=$myDoc->duplicateDoc();
							$myPage->redirect($myPage->getName()."?urlaction=docSaisie&id_doc=".$newDoc->t_doc['ID_DOC']."&id_lang=".$newDoc->t_doc['ID_LANG']."&duplicated=1");
							//$myDoc=$newDoc;
						}
						break;
						
					case "SAVE_SEQ" :
						
						$myDoc->t_doc_seq = $this->transformArrayMulti($FORM['t_doc_seq'], array('ID_DOC'));
						$myDoc->saveSequences();
						break;
						
					case "SUP_SEQ" :
				 // I.B.1. Suppression d'une séquence
						deleteSQLFromArray(array("t_doc","t_doc_lex","t_doc_mat","t_doc_val","t_panier_doc","t_action"),"ID_DOC",$ligne);
						break;
						
					case "SAVE_LIEN" :
						$myDoc->arrDocLiesSRC=$this->transformArrayMulti($FORM['t_doc_liesSRC'], array('ID_DOC_DST'));
						$myDoc->arrDocLiesDST=$this->transformArrayMulti($FORM['t_doc_liesDST'], array('ID_DOC_SRC'));
						$myDoc->saveDocLies();
						break;
						
						
						// SAUVEGARDE DES MATERIELS
					case "SAVE_MAT" :
						if(isset($FORM['t_doc_mat'])){
							$FORM['t_doc_mat'] = $this->transformArrayMulti($FORM['t_doc_mat'], array('ID_MAT'));
						}
						//BY LD 01 06 09 : reprise du process de sauvegarde pour ne pas avoir de delete/insert qui pose problème
						//car on perd les infos des champs pas en formulaire... or il y en a bcp dans doc_acc maintenant

						//On procède comme pour les doc_acc : récup de la liste des doc_mat originaux
						//et comparaison avec la liste fournie avec le doc
						//Grâce aux clés ID_MAT et ID_DOC_MAT, on peut connaitre les lignes à  ajouter, à  effacer et à  enlever.


						$myDoc->getDocMat();
						$arrDMObject=$myDoc->t_doc_mat;
						$dmDeleted = array();
						
						//Etape 1 => on parcours le post avec les originaux pour repérer les lignes déjà  présentes (save)
						// et celle qui ne le sont plus dans le post (delete);
						foreach ($arrDMObject as $idx=>$dmOrg) { //originaux
							$found=false;
							$delete_doc_mat=false;

							if(isset($FORM['t_doc_mat'])){
								foreach ($FORM['t_doc_mat'] as $idx2=>$dmPost) { //post
									if ($dmPost['ID_MAT']==$dmOrg['ID_MAT'] && $dmPost['ID_DOC_MAT']==$dmOrg['ID_DOC_MAT'])
									{
										$found=true;
										
										if (isset($dmPost['action']) && !empty($dmPost['action']) && $dmPost['action']=='suppr')
											$delete_doc_mat=true;
										break;
									}
								}
							}
							
							if ($found==true && $delete_doc_mat==true)
							{
								$dmOrg['action']='delete';
								$total[]=$dmOrg;
								$dmDeleted[] = $dmOrg;
							}
							else if ($found==true ) {
								$dmPost['action']='save';
								$total[]=$dmPost;
							}
							else {
								//$dmOrg['action']='delete';
								//$total[]=$dmOrg;
								//$dmDeleted[] = $dmOrg;
							}
						}

						//Etape 2 => sens inverse, on parcours le post pour voir les lignes non présentes en originaux (new)
						if(isset($FORM['t_doc_mat'])){
						foreach ($FORM['t_doc_mat'] as $idx=>$dmPost) { //post
							$found=false;
							foreach ($arrDMObject as $idx2=>$dmOrg) { //post
								if ($dmPost['ID_MAT']==$dmOrg['ID_MAT'] && $dmPost['ID_DOC_MAT']==$dmOrg['ID_DOC_MAT']) {$found=true;break;}
							}
							//Note, on en profite pour faire un test et ne pas aj des lignes avec id_mat vide
							if ($found==false && !empty($dmPost['MAT_NOM']) && strcmp($dmPost['action'],'suppr') !== 0) {
								$dmPost['action']='new';
								if(isset($dmPost['ID_MAT']) && (empty($dmPost['ID_MAT']) || $dmPost['ID_MAT'] == 0 )){unset($dmPost['ID_MAT']);}
								$total[]=$dmPost;
							}
						}
						}

						debug($total,'gold');
						$myDoc->t_doc_mat=$total;

						//02 06 09 : retrait de la purge
						 $myDoc->saveDocMat();
						 $myDoc->updateDocNum();
						//Pour chaque version du document, on met à jour la vignette n'est une image provenant d'un imageur d'un matériel que l'on a dissocié
						if(!empty($dmDeleted)) {
							Doc::checkVignette($myDoc->t_doc['ID_DOC'], $dmDeleted);
						}
						
						$arrVal = array();
						$i = -1;
						foreach ($FORM['t_mat_val'] as $idv=>$val) {
							if (isset($val['ID_VAL']) && !empty($val['ID_VAL'])){
								$i++;
								$arrVal[$i]['ID_VAL'] = $val['ID_VAL'];
							}
							else if (!empty($arrVal[$i]) && isset($val['ID_TYPE_VAL']))
								$arrVal[$i]['ID_TYPE_VAL'] = $val['ID_TYPE_VAL'];
							else if (!empty($arrVal[$i]) && isset($val['ID_MAT']))
								$arrVal[$i]['ID_MAT'] = $val['ID_MAT'];
							
						}
						foreach ($arrVal as $vmat){
							require_once(modelDir.'model_materiel.php');
							$mat = new Materiel();
							$mat->t_mat['ID_MAT'] = $vmat['ID_MAT'];
							$mat->getMat();
							//Sauvegarde t_mat_val
							$mat->t_mat_val[] = $vmat;
							$mat->saveMatVal();
							unset($mat);
						}
						break;
						
					case "SAVE_DMAT" :
						$id_doc = $myDoc->t_doc['ID_DOC'];
						$myDoc->getDocMat();
						
						//Création t_mat_val
						$arrVal = array();
						$i = -1;
						foreach ($FORM['t_mat_val'] as $idv=>$val) {
							if (isset($val['ID_ROW_TL'])){
								$i = $val['ID_ROW_TL'];
							}
							if (isset($val['ID_VAL']) && !empty($val['ID_VAL'])){
								$arrVal[$i][] = array('ID_VAL' => $val['ID_VAL']);
							}
							if (isset($val['ID_TYPE_VAL'])){
								$type_val = $val['ID_TYPE_VAL'];
								if(is_array($arrVal[$i])){
									foreach ($arrVal[$i] as &$row_val){
										if(!isset($row_val['ID_TYPE_VAL'])){
											$row_val['ID_TYPE_VAL'] = $type_val;
										}
									}
								}
							}
						}
						
						//Sauvegarde t_mat
						$okmat=array();
						$nokmat=array();
						foreach ($FORM['t_mat']as $idmat=>&$vmat){
							require_once(modelDir.'model_materiel.php');
							$mat = new Materiel();
							$mat->t_mat['ID_MAT'] = $vmat['ID_MAT'];
							//$mat->t_mat['MAT_FORMAT'] = $vmat['MAT_FORMAT'];
							$mat->getMat();
							$mat->mapFields($vmat);
							if (!$mat->checkExist()) {
								$mat->create();
								$id_mat = $vmat['ID_MAT'] = $mat->t_mat['ID_MAT'];
							} else {
								$id_mat = $mat->checkExist();
								$mat->id_mat_ref = $mat->t_mat['MAT_NOM'];
								$vmat['ID_MAT'] = $mat->t_mat['ID_MAT'] = $id_mat;
								$ok = $mat->save();
							}
							//Sauvegarde t_mat_val
							if ($ok && isset($arrVal[$idmat])){
								$mat->t_mat_val = $arrVal[$idmat];
							}
							$mat->saveMatVal();
							
							if($ok) $okmat[]=$vmat['ID_MAT'];
							else $nokmat[]=$vmat['ID_MAT'];
							
							unset($mat);
						}

						//Sauvegarde t_doc_mat
						//Enregistre les doc_mat déjà  trouvés, pour permettre d'ajouter un nouveau doc_mat lié à  un matériel déjà  lié au document
						$aDocMatFinded = array();
						foreach ($FORM['t_mat'] as $idm=>$mat){
							$id_dm = -1;
							foreach ($myDoc->t_doc_mat as $idx => $docMat){
								if ($docMat['ID_DOC'] == $id_doc && $docMat['ID_MAT'] == $mat['ID_MAT'] && !in_array($idx,$aDocMatFinded)) {
									$id_dm = $idx;
									$aDocMatFinded[] = $idx;
									break;
								}
							}
							if ($id_dm >= 0){
								
							}
							else{
								$myDoc->t_doc_mat[] = array('ID_DOC' => $id_doc, 'ID_MAT' => $mat['ID_MAT']);
								$aDocMatFinded[] = key($myDoc->t_doc_mat);
							}
						}
						
						//On parcours les deux tableaux pour trouver les t_doc_mat a supprimer
						$aFoundedMat = array();
						$dmDeleted = array();
						foreach ($myDoc->t_doc_mat as $idx => $docMat){
							$found = false;
							$id_dm = -1;
							foreach ($FORM['t_mat']as $idm=>$mat)
								if ($docMat['ID_DOC'] == $id_doc && $docMat['ID_MAT'] == $mat['ID_MAT'] && !in_array($idm, $aFoundedMat)){
									$found = true;
									$id_dm = $idm;
									$aFoundedMat[] = $idm;
									break;
								}

							if ($found==true && $FORM['t_doc_mat'][$idm]['action'] == 'suppr') {
                                $myDoc->t_doc_mat[$idx]['action'] = 'delete';
                                $dmDeleted[] = $myDoc->t_doc_mat[$idx];

							}
							else if ($found==true ){
                                $myDoc->t_doc_mat[$idx]['action'] = 'save';
                                if(is_array($FORM['t_doc_mat'][$id_dm])){
                                    foreach ($FORM['t_doc_mat'][$id_dm] as $fld=>$val){
                                        if (strpos($fld, "DMAT_") === 0){
                                            $myDoc->t_doc_mat[$idx][$fld] = $val;
                                        }
                                    }
                                }
							}
                         // else {
                         //        $myDoc->t_doc_mat[$idx]['action'] = 'delete';
                         //        $dmDeleted[] = $myDoc->t_doc_mat[$idx];
                         //    }
						}
						
						$myDoc->saveDocMat(false);
						//VP 8/06/11 : mise à  jour DOC_NUM
						$myDoc->updateDocNum();
						//Pour chaque version du document, on met à jour la vignette n'est une image provenant d'un imageur d'un matériel que l'on a dissocié
						if(!empty($dmDeleted)) {
							Doc::checkVignette($myDoc->t_doc['ID_DOC'], $dmDeleted);
						}
						if(count($nokmat)>0) $myDoc->dropError(kErrorMaterielsSauveCorrect." : ".implode($nokmat,', '));
						else $myDoc->dropError(kSuccesMaterielsSauve);
						break;
						
						//PC - 29/09/2010 : Sauvegarde des données juridiques
					case "SAVE_JUR" :
						$saved=$myDoc->save();
						
						if (!empty($FORM['t_doc_val'])){
							$myDoc->t_doc_val=$this->transformArrayMulti($FORM['t_doc_val'], array('ID_VAL'),'ID_TYPE_VAL');
							$myDoc->saveDocVal();
							$myDoc->updateChampsXML('DOC_VAL');
						}
						break;
						
					case "SAVE_DROIT" :
						$myDoc->getLexique();
						$myDoc->t_doc_lex_droit = $FORM['t_doc_lex_droit'];
			
						foreach ($FORM['t_doc_lex'] as $idDL => $valDL)
							if(!isset($valDL['ID_PERS']) || !isset($valDL['DLEX_ID_ROLE']) || !isset($valDL['ID_TYPE_DESC']))
								unset($FORM['t_doc_lex'][$idDL]);
						foreach ($FORM['t_doc_lex'] as $idtdl=>$rowtdl)
							if (!isset($myDoc->t_doc_lex[$idtdl]))
								$myDoc->t_doc_lex[$idtdl] = $rowtdl;
							else 
								$myDoc->t_doc_lex[] = $rowtdl;
						$myDoc->saveDocLex();
						
						foreach ($myDoc->t_doc_lex_droit as $idd=>$droit){
							foreach ($droit as $idv=>$dval) {
								$tmp = explode("_", $idv);
								if ($idv != "ID_DOC_LEX" && $tmp[0] != "DR")
									unset($myDoc->t_doc_lex_droit[$idd][$idv]);
								else if ($idv == "ID_DOC_LEX" && empty($dval)){
									if (isset($myDoc->t_doc_lex[$idd]["ID_DOC_LEX"]))
										$myDoc->t_doc_lex_droit[$idd][$idv] = $myDoc->t_doc_lex[$idd]["ID_DOC_LEX"];
									else 
										unset($myDoc->t_doc_lex_droit[$idd]);
								}
							}
						}
						$myDoc->saveDocLexDroit();
						$myDoc->getDocLexDroit();
						break;
						
					case "SAVE_DPREX" :
					case "SAVE-SAVE_DPREX" :
						
					
						$myDoc->t_doc_prex = $FORM['t_doc_prex'];
						
						$arrVal = array();
						$i = -1;
										
						// B.RAVI 20150127 Eviter Warning: Invalid argument supplied for foreach()
						if (is_object($FORM['t_dp_val']) || is_array($FORM['t_dp_val'])){
							foreach ($FORM['t_dp_val'] as $idV => &$valV){
									if (isset($valV['ID_DOC_PREX'])){
											$arrVal[++$i] = array();
									}
									if (isset($valV['ID_VAL'])){
											$arrVal[$i][] = array('ID_VAL' => $valV['ID_VAL']);
									}
									if (isset($valV['ID_TYPE_VAL'])){
											$type_val = $valV['ID_TYPE_VAL'];
											foreach ($arrVal[$i] as &$row_val)
													if(!isset($row_val['ID_TYPE_VAL']))
															$row_val['ID_TYPE_VAL'] = $type_val;
									}
							}
						}
						
						$i = 0;
						 // B.RAVI 20150127 Eviter Warning: Invalid argument supplied for foreach()
						if (is_object($myDoc->t_doc_prex) || is_array($myDoc->t_doc_prex)){
							foreach ($myDoc->t_doc_prex as $idP => &$valP){
									//construction tableau t_dp_pers
									if (isset($FORM['t_dp_pers'][$idP]['ID_PERS']) && $FORM['t_dp_pers'][$idP]['ID_PERS'] != "")
											$valP['t_dp_pers'][] = $FORM['t_dp_pers'][$idP];
											//construction tableau t_dp_val
									if (isset($arrVal[$i]))
											$valP['t_dp_val'] = $arrVal[$i];
									if (!isset($valP['DP_AUDITE']))
											$valP['DP_AUDITE'] = '0';
									$i++;
							}
						}

						
						$myDoc->saveDocPrex();
						$myDoc->getDroitPrex();
						break;
						
					case "SAVE_DEXP":
						if(isset($FORM['t_doc_exp'])) {
							foreach ($FORM['t_doc_exp'] as $dexp) {
								if (isset($dexp['ID_EXP']))
									$myDoc->t_doc_exp[]['ID_EXP'] = $dexp['ID_EXP'];
							}
							$myDoc->saveDocExp();
							$myDoc->getDocExp();
						}
						break;
						
					case "SAVE_FEST" :
						if(isset($FORM['t_doc_fest'])) {
							$myDoc->t_doc_fest = $this->transformArrayMulti($FORM['t_doc_fest'], array('ID_FEST'));
							$myDoc->saveDocFest();
						 }
						break;
						
						//URL courte >> lancement WS createShortUrl en curl
					case "SHORT_URL":
						// $ws_url=gWsCreateShortUrl.'/createShortUrl/'.$FORM['url_longue'];
						$post_data = array(
								'url' =>   $FORM['url_longue']
								);
								
						$ch = curl_init();
						// curl_setopt($ch, CURLOPT_URL, $ws_url);
						curl_setopt($ch, CURLOPT_URL,gWsCreateShortUrl);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$url_courte = curl_exec($ch);
						
						 if(preg_match('#<code>#',$url_courte)){
							$myDoc->t_doc['DOC_SHORT_URL'] = strip_tags($url_courte);
							$myDoc->save();
							}
						else {
							echo strip_tags($url_courte);	
						 }
						break;
						
					case "SAVE_DOCACC":
						require_once(modelDir.'model_docAcc.php');
						$myDocAcc=new DocAcc();
						$arrDocAcc=array();
						$aIdsPost=array();
						$myDoc->getDocAcc();
						// Réorganisation du tableau : comme ça les tableaux issus de l'objet ET du post ont la même structure
						foreach ($_POST["t_doc_acc"]["ID_DOC_ACC"] as $idx=>$val) { //ATTENTION les dimensions ne sont PAS quotées !!!

							// VP 2/07/13 : changement ordre tableau idem bdd
							$arrDocAcc[$idx]=array ('ID_DOC_ACC'=>$val,
												'ID_LANG'=>$myDoc->t_doc["ID_LANG"],
												'ID_DOC'=>$myDoc->t_doc["ID_DOC"],
												'ID_PERS'=>($_POST["t_doc_acc"]["ID_PERS"][$idx]!=''?$_POST["t_doc_acc"]["ID_PERS"][$idx]:'0'),
												'ID_PANIER'=>($_POST["t_doc_acc"]["ID_PANIER"][$idx]!=''?$_POST["t_doc_acc"]["ID_PANIER"][$idx]:'0'),
												'ID_FEST'=>($_POST["t_doc_acc"]["ID_FEST"][$idx]!=''?$_POST["t_doc_acc"]["ID_FEST"][$idx]:'0'),
												'ID_EXP'=>($_POST["t_doc_acc"]["ID_EXP"][$idx]!=''?$_POST["t_doc_acc"]["ID_EXP"][$idx]:'0'),
												'ID_CAT'=>($_POST["t_doc_acc"]["ID_CAT"][$idx]!=''?$_POST["t_doc_acc"]["ID_CAT"][$idx]:'0'),
												'DA_TITRE'=>$_POST["t_doc_acc"]["DA_TITRE"][$idx],
												'DA_FICHIER'=>$_POST["t_doc_acc"]["DA_FICHIER"][$idx],
												'DA_CHEMIN'=>$_POST["t_doc_acc"]["DA_CHEMIN"][$idx],
												'DA_TEXTE'=>$_POST["t_doc_acc"]["DA_TEXTE"][$idx],
												'DA_ORDRE'=>$_POST["t_doc_acc"]["DA_ORDRE"][$idx]
												);
							$aIdsPost[] = $val;
						}

						// Eviter Warning: Invalid argument supplied for foreach()
						if (is_object($_POST["t_doc_acc_val"]) || is_array($_POST["t_doc_acc_val"])) {
							foreach ($_POST["t_doc_acc_val"] as $type => $docacc) {
								foreach ($docacc as $idx => $val) {
									if (!empty($val)) {
										$arrDocAccVal[$idx][] = array('ID_TYPE_VAL' => $type,
											'ID_VAL' => $val,
											'ID_DOC_ACC' => $_POST["t_doc_acc"]["ID_DOC_ACC"][$idx]
										);
									}
								}
							}
						}

						$arrDAfromObjectIds=array();
						foreach ($myDoc->t_doc_acc as $idx=>$docacc) { //il faut constituer un tableau ne contenant que le tableau t_doc_acc de CHAQUE object
							$arrDAfromObjectIds[]=$docacc->t_doc_acc['ID_DOC_ACC'];
						}


						$arrDel=array_diff($arrDAfromObjectIds,$aIdsPost);
						foreach ($myDoc->t_doc_acc as $idx=>$docacc) {
							if(in_array($docacc->t_doc_acc['ID_DOC_ACC'],$arrDel)) {
								//Suppression du doc acc
								$docacc->delete();
							}
						}

						foreach($arrDocAcc as  $idx=>$docacc) {
							$myDocAcc=new DocAcc();
							$myDocAcc->updateFromArray($docacc);
							
							if(empty($docacc['ID_DOC_ACC'])) {
								//Création du doc acc
								$myDocAcc->t_doc_acc_val=$arrDocAccVal[$idx];
								$myDocAcc->create(false);
								$myDocAcc->saveDocAccVal();
								
							} else {
								$id = $myDocAcc->checkExistId();
								if($id) {
									//Sauvegarde du doc acc
									$myDocAcc->t_doc_acc_val=$arrDocAccVal[$idx];
									$myDocAcc->save();
									$myDocAcc->saveDocAccVal();
									
								} else {
									//Création du doc acc
									$myDocAcc->t_doc_acc_val=$arrDocAccVal[$idx];
									$myDocAcc->create(false);
									$myDocAcc->saveDocAccVal();
								}
							}
							unset($myDocAcc);
						}

						
						$myDoc->saveVignette('doc_acc',$_POST['vignette']);
						if ($myDoc) $myDoc->getVignette();

						break;

			 }
			
			if(!empty($_POST["page"])){

				  // Petite machine un peu compliquée destinée à  remplir le champ id_doc dans l'url de redirection si celui-ci est vide
				  // Ce cas peut arriver si à  la création d'un doc (id_doc vide) on clique directement sur un autre item du menu (saveAndGo...)
				  $params=split('&',$_POST["page"]);
				  foreach($params as $idx=>$prm) {list($key,$val)=split('=',$prm);$urlparams[$key]=$val;}
				  if (empty($urlparams['id_doc'])) $urlparams['id_doc']=$myDoc->t_doc['ID_DOC'];
				  foreach($urlparams as $idx=>$val) $url.=$idx."=".$val."&";
				  // VP 25/06/09 : redirection si changement urlaction ou id_doc
				  if (empty($urlparams['id_lang'])) $urlparams['id_lang']=$myDoc->t_doc['ID_LANG'];
				  if($urlparams['urlaction']!=$_GET['urlaction'] || $urlparams['id_doc']!=$myDoc->t_doc['ID_DOC'] || $urlparams['id_lang']!=$myDoc->t_doc['ID_LANG']) $myPage->redirect($url);
			}
		}

        // III. Ouverture d'un enregistrement
        if(isset($_GET["id_doc"])) {
            $id_doc = intval($_GET["id_doc"]);
        } else {
            // III.1. Creation d'un document par defaut
            if(empty($myDoc->t_doc['ID_DOC'])){
                // Media=V par defaut
                if(isset($_GET["doc_id_media"]))  $myDoc->t_doc['DOC_ID_MEDIA']=$_GET["doc_id_media"];
                else  $myDoc->t_doc['DOC_ID_MEDIA']='V';               
                 if(isset($_GET["doc_id_type_doc"]))  $myDoc->t_doc['DOC_ID_TYPE_DOC']=$_GET["doc_id_type_doc"];


            }else{
                $id_doc = intval($myDoc->t_doc['ID_DOC']);
            }
        }
        
        if(!empty($id_doc)){
            $myDoc->getDocFull(1,1,1,1,1,1,1,1,1,1,1,1);
            $myDoc->getVersions(); //
            $myDoc->getFestival();
        }
        
        // Paramètres pour view
		$this->init_editParam($myDoc, (isset($myDoc->t_doc['DOC_TITRE']) && !empty($myDoc->t_doc['DOC_TITRE'])?$myDoc->t_doc['DOC_TITRE']:''));
		
        if (isset($_REQUEST['form']) && $_REQUEST['form'] && strpos(getSiteFile("designDir","templates/content/".$_REQUEST['form'].".html"), 'index.php') === false) {
            $this->params['formTemplate'] = getSiteFile("designDir","templates/content/".$_REQUEST['form'].".html");
        } else {
            $this->params['formTemplate'] = getSiteFile("designDir","templates/content/docDetail.html");
        }
		if(isset($rang)){
			$this->params['rang'] = $rang;
			$this->params['max_rows'] = $max_rows;
		}
		if(isset($rang_seq)){
			$this->params['rang_seq'] = $rang_seq;
		}
    }
	
}
