<?
/**
 *  Classe Controller_panier
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(libDir."class_cherchePanier.php");
require_once(modelDir.'model_panier.php');
require_once(modelDir.'model_panier_session.php');
require_once(libDir."class_page.php");

class Controller_panier extends Controller {
    
    /** Liste paniers
     * IN : GET, POST
     * OUT : objet cherche
     */
    function do_panierListe($args=null) {
		global $db;
		$myPage=Page::getInstance();
		$myPage->setReferrer(true);


        $mySearch=new RecherchePanier();

        $mySearch->tab_recherche[500]=array('FIELD'=>'PAN_ID_USAGER',
                                  'VALEUR'=>intval(User::getInstance()->UserID),
                                  'TYPE'=>'CE',
                                  'OP'=>'AND',
                                  'LIB'=>kUsager);   

        $mySearch->tab_recherche[501]=array('FIELD'=>'PAN_ID_ETAT',
                                  'VALEUR'=>'1',
                                  'TYPE'=>'CVGT',
                                  'OP'=>'AND',
                                  'LIB'=>kEtat);

        $mySearch->tab_recherche[502]=array('FIELD'=>'PAN_ID_TYPE_COMMANDE',
                                  'VALEUR'=>'0',
                                  'TYPE'=>'CVGT',
                                  'OP'=>'AND',
                                  'LIB'=>kType);

		$this->init_list($mySearch, "F_panier_form", kMesCommandes, "panier");

		/*// VP (22/10/08) : modification test sur PAN_ID_ETAT de >=1 à >1 pour ne pas voir le panier courant
		$sql="SELECT t_panier.*,t_type_commande.TYPE_COMMANDE, count(distinct t_panier_doc.ID_DOC) as NBDOCUMENTS";
		$sql.=",sum(t_panier_doc.PDOC_EXTRAIT) as NBEXTRAITS, t_etat_pan.ETAT_PAN";
		$sql.=" from t_panier LEFT JOIN t_panier_doc ON t_panier.id_panier=t_panier_doc.id_panier";
		$sql.=" LEFT JOIN t_etat_pan ON t_panier.PAN_ID_ETAT=t_etat_pan.ID_ETAT_PAN and t_etat_pan.ID_LANG='".$_SESSION['langue']."'";
		$sql.=" LEFT JOIN t_type_commande ON t_panier.PAN_ID_TYPE_COMMANDE=t_type_commande.ID_TYPE_COMMANDE and t_type_commande.ID_LANG='".$_SESSION["langue"]."'";
		$sql.=" WHERE t_panier.pan_id_usager=".intval(User::getInstance()->UserID)." and t_panier.pan_id_etat > 1 and t_panier.PAN_ID_TYPE_COMMANDE>0 ";
		$sql.=" GROUP BY  t_panier.id_panier, t_panier.pan_id_gen, t_panier.pan_dossier, t_panier.pan_public, pan_date_mod, pan_id_usager_mod, pan_id_etat, pan_id_usager, pan_titre, pan_objet, pan_xml, pan_date_com, pan_date_crea, pan_id_type_commande, pan_total_ht, pan_frais_ht, pan_id_trans, pan_tva, pan_id_doc_acc, type_commande, etat_pan ";

		if ($myPage->getSortFromURL()!="") $sql.=$myPage->getSort();
		$myPage->getPageFromURL();

		$_SESSION["panierListe"]["sql"]=$sql;
		
		//Sauvegarde du nombre de lignes de recherche � afficher :
		if(!isset($_REQUEST['nbLignes']) || $_REQUEST['nbLignes'] == '') $_REQUEST['nbLignes']=10;

		//print("<div class='errormsg'>".$myPage->error_msg."</div>");
		$param["nbLigne"] = $_REQUEST['nbLignes'];
		$param["page"] = $myPage->page;
		if($myPage->nbLignes=='') $param["offset"] = "0";
		else $param["offset"] = $myPage->nbLignes*($myPage->page-1);
      
		
		$this->params['searchRes'] = $db->getAll($sql);
        $this->params['title'] = kListeCommandes;
        $this->params['nomElement'] = "panier";
        $this->params['contentTemplate'] = getSiteFile("designDir","templates/content/deftListe.html");
        $this->params['xslListFile'] = getSiteFile("listeDir","panierListe.xsl");        
        $this->params['xmlListFile'] = getSiteFile("listeDir","xml/panierListe.xml");
		$this->params['params4XSL'] = $param;*/
		

    }
    
    function do_panierPersoListe($args=null) {
		global $db;
		$myPage=Page::getInstance();
		$myPage->setReferrer(true);


        $mySearch=new RecherchePanier();
		$mySearch->sessVar='recherche_PANPERSO';

        $mySearch->tab_recherche[500]=array('FIELD'=>'t_panier.PAN_ID_USAGER',
                                  'VALEUR'=>intval(User::getInstance()->UserID),
                                  'TYPE'=>'CE',
                                  'OP'=>'AND',
                                  'LIB'=>kUsager);   

        $mySearch->tab_recherche[501]=array('FIELD'=>'PAN_ID_ETAT',
                                  'VALEUR'=>'2',
                                  'TYPE'=>'CVLT',
                                  'OP'=>'AND',
                                  'LIB'=>kEtat);


		$this->init_list($mySearch, "F_panier_form", kMesCommandes, "panier","","panierPerso");


		

    }
    /** Saisie panier
     * IN : GET, POST, objet panier
     * OUT : objet panier
     */
    function do_panier($args=null) {
        $myPage=Page::getInstance();
        $myPage->nomEntite = kPanier;
		$user=User::getInstance();
		
		if (isset($_GET['flush']) && $_GET['flush']) {unset($_SESSION['panier_session']); return;}
		
		//indique dans le xsl si c'est un AUTRE utilisateur qui essaye d'accéder au panier
		$param["acces_externe"] = 0;

		//qu'on SOIT loggé ou pas loggé, si on donne une clé RSA valide on a accès au panier (dont panier d'un autre utilisateur) en mode EXTERNE (cad on ne peut que action= copy selection, on peut pas supprimer...etc...voir plus bas)

		if (trim($_REQUEST['acces_externe'])!='') {
			require_once(libDir . "class_RSA.php");
			$rsa = new RSA(tokenRSAModulus_panier, tokenRSAExponentPrivate_panier, tokenRSAExponentPublic_panier);
			$origin = $rsa->decrypt(base64_decode($_REQUEST['acces_externe']));//on passe par une nouvel variable acces_externe (au lieu de réutiliser id_panier) car sinon quand l'utilisateur soumet le formulaire par ex, le hidden id_panier=705 écrasera le id=c1LTQ0MTU4NzUtNDM2N(clé rsa) de l'url

			if (ctype_digit($origin) && (int) $origin > 0 ) {
				require_once(modelDir.'model_panier.php');
				$myPanier = New Panier();
				$_REQUEST['id_panier'] = $origin;
				//TODO rajouter ctrl si panier existant + comment ca se passe si 1 id ressemble à une clé rsa (doit pas se produire normalement)
				$param["acces_externe"] = 1;
			}else{
				echo kErrorPanierInvalidIDpanier;
				return; // return fait sortir de de panier.php (on est sûr que l'user ne pourra faire aucune action sur le panier d'un autre même en rajoutant un formulaire par un outil de développement comme firebug)
			}
		} elseif ($user->loggedIn()) {
			$myPanier = New Panier();
			if(!empty($_REQUEST["id_panier"])){
				$myPanier->t_panier['ID_PANIER'] = intval($_REQUEST["id_panier"]);
			}elseif(!empty($myPage->id)){
				$myPanier->t_panier['ID_PANIER'] = intval($myPage->id);
			}
			$myPanier->getPanier(null, false); //besoin de getPanier() pour getProprioPanier()
			$myPanier->getProprioPanier();

			$listSelections=Panier::getFolders(true);
			if (defined("gShareSelGrp") && (gShareSelGrp)) {
				$grpCarts = Panier::getGroupeFolders(false);
				foreach($grpCarts as $grpCart){
					if($grpCart['ID_PANIER']==$myPanier->t_panier['ID_PANIER']){
						$canViewGrpCart=true;
						break;
					}
				}
			}
			
			//pour empêcher un utilisateur non admin d'accéder aux paniers des autres utilisateurs
			if ($user->UserID != $myPanier->owner->t_usager['ID_USAGER'] && $canViewGrpCart!=true) {
				$forbidViewAllPanier=false;
				if (defined('gSeuilUsrViewAllPanier')){
					$SeuilUsrViewAllPanier=gSeuilUsrViewAllPanier;
				}elseif (defined('kLoggedAdmin')){
					$SeuilUsrViewAllPanier=kLoggedAdmin;
				}else{
					$forbidViewAllPanier=true;
				}

				if ($user->getTypeLog() < $SeuilUsrViewAllPanier || $forbidViewAllPanier){
					echo kErrorPanierAccessDenied;
					return; // return fait sortir du controller (on est sûr que l'user ne pourra faire aucune action sur le panier d'un autre même en rajoutant un formulaire par un outil de développement comme firebug)
				}
			}

		} else {
			require_once(modelDir.'model_panier_session.php');
			$myPanier = New PanierSession();
		}
		
		$xsl='panier.xsl';
		if (isset($_REQUEST['xsl']) && $_REQUEST['xsl'] && getSiteFile("listeDir",$_REQUEST['xsl'].'.xsl')!=siteDirCore."index.php")
			$xsl=$_REQUEST['xsl'].'.xsl';
		
		 if (isset($_REQUEST['xmlfile']) && $_REQUEST['xmlfile']!=''){
            $xml_liste=$_REQUEST['xmlfile'];
            if(strpos($xml_liste,'xml/')!==0){
                $xml_liste = "xml/".$xml_liste;
            }
        } else {
            $xml_liste = "xml/panierDoc.xml";
        }
		
		$myPanier->t_panier['ID_PANIER']=$_REQUEST['id_panier'];
		$myPanier->sqlOrder=$myPage->getSort(false); //Pas de tri par défaut !

		// De quel panier s'agit-il ?
		if(isset($_REQUEST["id_panier"])) $id_panier=trim($_REQUEST["id_panier"]);
		else $id_panier="";
		if (isset($_REQUEST["tri"])) $arrOrders=tri2array($_REQUEST["tri"]); else $arrOrders=null;
		//by LD 18/11/08 : uniquement pour trier également les exports
		if (!empty($myPanier->sqlOrder)) $_SESSION['tri_panier_doc']=$myPanier->sqlOrder;
		else unset($_SESSION['tri_panier_doc']);
		//panier id origine
		$panier_id_origine=$myPanier->t_panier['ID_PANIER'];
		
		if(!isset($_POST["commande"]) || empty($_POST["commande"])) {	// Pas de cmd, lecture d'un panier par défaut ou chgt page ou tri, RAZ des statuts précédemment renseignés
			$myPanier->getPanier();
			//Utile pour le montage 
			if(!empty($_GET['getMats'])) 
				$myPanier->getDocsFull(0,0,1);
		} else {
			$action=$_POST["commande"];
			$ligne=trim($_POST["ligne"]);
			$id_ligne_panier=$_POST["id_ligne_panier".$ligne];
			$id_panier=$_REQUEST["id_panier"];

			$myPanier->t_panier['ID_PANIER']=$_REQUEST["id_panier"];
			$myPanier->currentLine=(integer)trim($_POST["ligne"])-1;

			// I.A. Suppression
			if(strcmp($action,"SUP")==0 && !$param["acces_externe"]) {

				// I.A.1. Suppression du panier
				if(strcmp($ligne,"")==0) {
					$myPanier->deletePanier();
					if($user->loggedIn()){
						$user->getPaniersUser();
					}
					// VP (16/10/08) : prise en compte paramètre POST refer pour redirection éventuelle
					if (!empty($_POST["refer"])) $myPage->redirect($_POST["refer"]);
					else $myPage->redirect($myPage->getReferrer());
				}
				// I.A.2. Suppression d'une ligne
				else {
					$myPanier->getPanier();
					$myPanier->met_a_jour($_POST);
					$myPanier->getPanierDoc();
					foreach ($myPanier->t_panier_doc as $pdoc){
					// On ne supprime que la ligne
						if($_POST["ligne"]==$pdoc['ID_LIGNE_PANIER']) $myPanier->deletePanierDoc($pdoc);
					}

					if ($myPanier->t_panier['PAN_ID_ETAT']>0) $myPanier->dropError(kSuccesPanierDocumentRetire); //succes
					else $myPanier->dropError(kSuccesFolderDocumentRetire); //distinction entre panier et folder (demande clients)
				}
			}
			else if (strcmp($action,"SUP_LIGNES")==0 && !$param["acces_externe"]) { //suppression lignes cochées

				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST);
				foreach ($myPanier->t_panier_doc as $pdoc){
	//                    echo 'DEBUG_affich_BR $pdoc <pre>'.print_r($pdoc,true).'</pre><br /><br />';
				// On ne commande que les lignes qui sont cochées
				//	VP 10/9/09 : changement test sur checkbox
				//	if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1') {

									//B.RAVI 2015-11-03 il peut se produire le cas ou checkbox0=0
	//                                if(!empty($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
					if( trim($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])!='') {
	//                                      echo 'DEBUG_affich_BR $pdoc <pre>'.print_r($pdoc,true).'</pre><br /><br />';
						$myPanier->deletePanierDoc($pdoc);
					}
				}
				$myPanier->getPanier();
				if ($myPanier->t_panier['PAN_ID_ETAT']>0) $myPanier->dropError(kSuccesPanierDocumentRetire); //succes
				else $myPanier->dropError(kSuccesFolderDocumentRetire); //distinction entre panier et folder (demande clients)
			}

			// I.B. Suppression de toutes les lignes
			else if(strcmp($action,"SUP_ALL")==0 && !$param["acces_externe"]) {
				$ok=$myPanier->deleteToutesLignes();
				$myPanier->getPanier();
				if ($ok) $myPanier->dropError(kSuccesPanierVider);
			}

			else if(strcmp($action,"SAVE")==0 && !$param["acces_externe"]) //Refresh du panier
			{
				$myPanier->getPanier();

				//PC 16/12/2010 : ajout de la variable prev_etat
				$prev_etat = $myPanier->t_panier["PAN_ID_ETAT"];

				$myPanier->met_a_jour($_POST);

				$myPanier->getVignette(); //si on a changé la vignette avec le post, il faut la récupérer ASAP
				//$myPanier->getTarifs();
				$ok=$myPanier->save();
				$ok2=$myPanier->saveAllPanierDoc();
				$myPanier->getPanierDoc();
				if(defined("kSaveVignetteAsHDForPanIds")){
					$arr_panHD_ids = explode(',',(string)(kSaveVignetteAsHDForPanIds));
					foreach($arr_panHD_ids as $panHD_id){
						if($panHD_id == $myPanier->t_panier['ID_PANIER']){
							require_once(modelDir.'model_imageur.php');
							Imageur::refreshImagesPanHD($myPanier->t_panier['ID_PANIER']);
						}
					}
				}

				//VP 26/8/09 : distinction message panier et sélection
				if ($ok && $ok2){
					$myPanier->dropError($myPanier->t_panier['PAN_ID_ETAT']>0?kSuccesPanierSauve:kSuccesSelectionSauve);
				}
			}

			else if(strcmp($action,"CHANGE_USER")==0 && !$param["acces_externe"]) //Tranfert du panier à un autre usager
			{
				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST);
				// VP 31/05/10 : sauvegarde avant changement user
				$ok=$myPanier->save();
				$ok2=$myPanier->saveAllPanierDoc();
				//$myPanier->getTarifs();
				$myPanier->saveRecursif(array("PAN_ID_USAGER"=>$myPanier->t_panier['PAN_ID_USAGER']));
				$myPage->redirect($myPage->getName()); //retour à l'accueil
			}
			//PC 15/10/10 duplication et transfert vers un nouvel user
			else if ($action=='DUPLIQUER_USER' && !$param["acces_externe"]) {

				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST,1,false);
				if (get_class($myPanier)=='Panier' ){
					//$newCart=$myPanier->duplicate($myPanier->t_panier['PAN_ID_GEN']);
					$newCart=$myPanier->duplicate(0);
					$newCart->getPanier();
					$ok=$newCart->saveRecursif(array("PAN_ID_USAGER"=>$myPanier->t_panier['PAN_ID_USAGER']));
					$ok=$newCart->saveRecursif(array("PAN_PUBLIC"=>$myPanier->t_panier['PAN_PUBLIC']));
					if ($ok) {
						$myPanier->error_msg='';
						$myPanier->dropError(kSuccesPanierDupliquer);
					}
				}
			}
			else if ($action=='TRANSFERT' && !$param["acces_externe"]) {

				$myPanier->getPanier();
	//                      $myPanier->met_a_jour($_POST);
	/* @author B.RAVI 2016-01-07 on utilise updateFromArray() à la place de met_a_jour() pour garder les elements qui ne seraient pas dans $_POST (exemple concret : si on ne POST pas PDOC_EXTRAIT, avec met_a_jour(), PDOC_EXTRAIT disparaît de $myPanier->t_panier_doc)
	mais comme updateFromArray ne rajoute que les element existant initialement dans t_panier, on ajoute $myPanier->t_                          panier['CARTLIST']=' ' (doit être différent de NULL) */
							$myPanier->t_panier['CARTLIST']=' ';
							$myPanier->updateFromArray($_POST);


				if ($myPanier->t_panier['CARTLIST']=='-1' && get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
					$newCart=new Panier();

					//MS 04/04/12 - si on transfere sur un nouveau panier, si le champ newCartName existe & est renseigné il devient le titre du nouveau panier
					if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
					else {$name= kSelection.' '.count($listSelections);}
					$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
					$newCart->createPanier();
					$myPanier->t_panier['CARTLIST']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}elseif ($myPanier->t_panier['CARTLIST']=='-2' && get_class($myPanier)=='Panier' ) { //Création à la volée d'une nouvelle commande sauf si panier session !
					$newCart=new Panier();
					$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
					$newCart->createPanier();
					$myPanier->t_panier['CARTLIST']=$newCart->t_panier['ID_PANIER'];
					unset($newCart);
				}
				$errDoublons=0;
	//                        echo '<div style="position:absolute;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_affich_BR $myPanier->t_panier<pre>';
	//                        var_dump($myPanier->t_panier_doc);
	//                        echo '</pre></div><br />';
				foreach ($myPanier->t_panier_doc as $pdoc){
				// On ne transfère que les lignes qui sont cochées
				//	VP 10/9/09 : changement test sur checkbox
					//if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1' ) {
					if(!empty($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
						$pdoc['ID_PANIER']=$myPanier->t_panier['CARTLIST'];
						if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on transfère avec check de doublons
					}
				}
	//                        echo 'debug_affich : $errDoublons'.$errDoublons;
				//by LD 20 11 08 : comptage des lignes en doublon et envoi d'erreur
				if ($errDoublons>0) {
					$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
				} else {
					$myPanier->dropError(kSuccesPanierTransfert);
					//VP 6/2/09 : affichage nouveau panier si demandé
					if($_POST['dspDest']=="1"){
						$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['CARTLIST'];
					}
				}
				$myPanier->getPanier();

				//Récup des paniers pour faire le compte des progs dedans
				//by ld 250708 commenté car inutile => comme on a un refresh de page, on a un déjà refreshFolders
				//echo '<script>refreshFolders();</script>';
			}

			//XB ajout des fonctions de copie et de deplacement
			else if ($action=='TRANSFERT_PANIER') {

				$myPanier->getPanier();
	//			$myPanier->met_a_jour($_POST);
							/* @author B.RAVI 2016-01-07 on utilise updateFromArray() à la place de met_a_jour() pour garder les elements qui ne seraient pas dans $_POST (exemple concret : si on ne POST pas PDOC_EXTRAIT, avec met_a_jour(), PDOC_EXTRAIT disparaît de $myPanier->t_panier_doc)
	mais comme updateFromArray ne rajoute que les element existant initialement dans t_panier, on ajoute $myPanier->t_                          panier['DESTINATION_PANIER']=' ' (doit être différent de NULL) */
							$myPanier->t_panier['DESTINATION_PANIER']=' ';
							$myPanier->updateFromArray($_POST);

	//                        echo '<div style="position:absolute;background-color:white;color:black;z-index:1000;border:1px solid red;height: inherit;">DEBUG_affich_BR $myPanier->t_panier<pre>';
	//                        var_dump($myPanier->t_panier);
	//                        echo '</pre></div><br />';

				//si action=deplacer
					if(isset($_POST['action_panier']) && ($_POST['action_panier']=="deplacer_selection")){
						if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
						$newCart=new Panier();

						//MS 04/04/12 - si on transfere sur un nouveau panier, si le champ newCartName existe & est renseigné il devient le titre du nouveau panier
						if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
						else {$name= kSelection.' '.count($listSelections);}
						if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
						if(!isset($_POST['pan_public'])) $_POST['pan_public']='0';
						if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
						$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen'],"PAN_PUBLIC"=>$_POST['pan_public']);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) { //Création à la volée d'une nouvelle commande sauf si panier session !
						$newCart=new Panier();
						$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}
					$errDoublons=0;
					foreach ($myPanier->t_panier_doc as $pdoc){
					// On ne transfère que les lignes qui sont cochées
					//	VP 10/9/09 : changement test sur checkbox
						//if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1' ) {
						if(!empty($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
							$pdoc['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
							if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on transfère avec check de doublons
						}
					}
					//by LD 20 11 08 : comptage des lignes en doublon et envoi d'erreur
					if ($errDoublons>0) {
						$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
					} else {
						$myPanier->dropError(kSuccesPanierTransfert);
						//VP 6/2/09 : affichage nouveau panier si demandé
						if($_POST['dspDest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						}
						//ajout de parametre pour afficher la popup au refresh et rediriger sur le bon panier si demandé
						$param["popup"] =1;
						$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
						if(isset($_POST['pan_public']) && $_POST['pan_public'] == "1") $param["popup_dossier"] =1;
					}
					//affichage de l ancien panier
					if($_POST['Dest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
						}

					$myPanier->getPanier();


					}


				//si action=deplacer tout
					if(isset($_POST['action_panier']) && ($_POST['action_panier']=="deplacer_selection_all")){
						if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
						$newCart=new Panier();

						//MS 04/04/12 - si on transfere sur un nouveau panier, si le champ newCartName existe & est renseigné il devient le titre du nouveau panier
						if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
						else {$name= kSelection.' '.count($listSelections);}
						if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
						if(!isset($_POST['pan_public'])) $_POST['pan_public']='0';
						if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
						$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen'],"PAN_PUBLIC"=>$_POST['pan_public']);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) { //Création à la volée d'une nouvelle commande sauf si panier session !
						$newCart=new Panier();
						$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}
					$errDoublons=0;
					foreach ($myPanier->t_panier_doc as $pdoc){
					// On ne transfère que les lignes qui sont cochées
					//	VP 10/9/09 : changement test sur checkbox
						//if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1' ) {

							$pdoc['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
							if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on transfère avec check de doublons

					}
					//by LD 20 11 08 : comptage des lignes en doublon et envoi d'erreur
					if ($errDoublons>0) {
						$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
					} else {
						$myPanier->dropError(kSuccesPanierTransfert);
						//VP 6/2/09 : affichage nouveau panier si demandé
						if($_POST['dspDest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						}
						//ajout de parametre pour afficher la popup au refresh et rediriger sur le bon panier si demandé
						$param["popup"] =1;
						$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
						if(isset($_POST['pan_public']) && $_POST['pan_public'] == "1") $param["popup_dossier"] =1;
					}
					//affichage de l ancien panier
					if($_POST['Dest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
						}

					$myPanier->getPanier();


					}

			//copie tout
			if(isset($_POST['action_panier']) && ($_POST['action_panier']=="cp_selection_all")){

				if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
						$newCart=new Panier();

						//MS 04/04/12 - si on transfere sur un nouveau panier, si le champ newCartName existe & est renseigné il devient le titre du nouveau panier
						if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
						else {$name= kSelection.' '.count($listSelections);}
						if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
						if(!isset($_POST['pan_public'])) $_POST['pan_public']='0';
						if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
						$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen'],"PAN_PUBLIC"=>$_POST['pan_public']);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) { //Création à la volée d'une nouvelle commande sauf si panier session !
						$newCart=new Panier();
						$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}
					$errDoublons=0;
					foreach ($myPanier->t_panier_doc as $pdoc){
					// On ne transfère que les lignes qui sont cochées
					//	VP 10/9/09 : changement test sur checkbox
						//if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1' ) {

							$pdoc['ID_LIGNE_PANIER']="";
							 $myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
							if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on transfère avec check de doublons

					}
					//by LD 20 11 08 : comptage des lignes en doublon et envoi d'erreur
					if ($errDoublons>0) {
						$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
					} else {
						$myPanier->dropError(kSuccesPanierTransfert);
						//VP 6/2/09 : affichage nouveau panier si demandé
						if($_POST['dspDest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						}
						//ajout de parametre pour afficher la popup au refresh et rediriger sur le bon panier si demandé
						$param["popup"] =1;
						$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
						if(isset($_POST['pan_public']) && $_POST['pan_public'] == "1") $param["popup_dossier"] =1;
					}
					//affichage de l ancien panier
					if($_POST['Dest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
						}

					$myPanier->getPanier();


					}

			//copie selection
			if(isset($_POST['action_panier']) && ($_POST['action_panier']=="cp_selection")){


				if ($myPanier->t_panier['DESTINATION_PANIER']=='-1' && get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
						$newCart=new Panier();
						//MS 04/04/12 - si on transfere sur un nouveau panier, si le champ newCartName existe & est renseigné il devient le titre du nouveau panier
						if(isset($_POST['newCartName']) && !empty($_POST['newCartName']) ){ $name= $_POST['newCartName'];}
						else {$name= kSelection.' '.count($listSelections);}
						if(!isset($_POST['pan_id_gen'])) $_POST['pan_id_gen']='';
						if(!isset($_POST['pan_public'])) $_POST['pan_public']='0';
						if($_POST['pan_id_gen']=="Dossier racine") $_POST='';
						$newCart->t_panier=array( "PAN_ID_ETAT"=>0,"PAN_TITRE"=>$name,"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID,"PAN_ID_GEN"=>$_POST['pan_id_gen'],"PAN_PUBLIC"=>$_POST['pan_public']);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}elseif ($myPanier->t_panier['DESTINATION_PANIER']=='-2' && get_class($myPanier)=='Panier' ) { //Création à la volée d'une nouvelle commande sauf si panier session !
						$newCart=new Panier();
						$newCart->t_panier=array( "PAN_ID_ETAT"=>'2',"PAN_TITRE"=>'',"PAN_ID_TYPE_COMMANDE"=>0,"PAN_ID_USAGER"=>User::getInstance()->UserID);
						$newCart->createPanier();
						$myPanier->t_panier['DESTINATION_PANIER']=$newCart->t_panier['ID_PANIER'];
						unset($newCart);
					}
					$errDoublons=0;
					foreach ($myPanier->t_panier_doc as $pdoc){
					// On ne transfère que les lignes qui sont cochées
					//	VP 10/9/09 : changement test sur checkbox
									// Dans le cas de panier session la valeur de checkbox peut etre = 0 (d'ou isset et non !empty)
										if(isset($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
							$pdoc['ID_LIGNE_PANIER']="";
							 $myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
							if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on transfère avec check de doublons
						}
					}
					//by LD 20 11 08 : comptage des lignes en doublon et envoi d'erreur
					if ($errDoublons>0) {
						$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
					} else {
						$myPanier->dropError(kSuccesPanierTransfert);
						//VP 6/2/09 : affichage nouveau panier si demandé
						if($_POST['dspDest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$myPanier->t_panier['DESTINATION_PANIER'];
						}
					//ajout de parametre pour afficher la popup au refresh et rediriger sur le bon panier si demandé
						$param["popup"] = 1 ;
						$param["destination_panier"] =$myPanier->t_panier['DESTINATION_PANIER'];
						if(isset($_POST['pan_public']) && $_POST['pan_public'] == "1") $param["popup_dossier"] =1;
					}
					//affichage de l ancien panier
					if($_POST['Dest']=="1"){
							$myPanier->t_panier['ID_PANIER']=$panier_id_origine;
						}

					$myPanier->getPanier();


					}

			}

			else if ($action=='CREER' && !$param["acces_externe"]) {
				//Création d'un panier => on prend les données du formulaire, corrigée si besoin et on crée
				if (get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
					$newCart=new Panier();
					$newCart->t_panier["PAN_ID_ETAT"]=0;
					$newCart->t_panier["PAN_FRAIS_HT"]=0;
					$newCart->t_panier["PAN_TOTAL_HT"]=0;
					$newCart->t_panier["PAN_TVA"]=0;
					$newCart->t_panier["PAN_OBJET"]='';
					$newCart->t_panier["PAN_XML"]='';
					$newCart->t_panier["PAN_ID_TYPE_COMMANDE"]=0;

					$newCart->met_a_jour($_POST);
					if(empty($newCart->t_panier["PAN_ID_USAGER"])){
						$newCart->t_panier["PAN_ID_USAGER"]=User::getInstance()->UserID;
					}
					debug($newCart,'cyan');
					$ok=$newCart->createPanier();
					/*
					//VP 28/01/10 : sauvegarde des lignes du panier
					if($ok && $_POST['creerLignes']=='1'){
						trace("panier.php : saveAllPanierDoc");
						$ok=$newCart->saveAllPanierDoc();
					}
					$newCart->getPanier();
					$myPanier=$newCart;
					if ($ok) $myPanier->dropError(kSuccesPanierCreation);*/
					//VP 28/01/10 : sauvegarde des lignes du panier
					if($ok && $_POST['creerLignes']=='1'){
						foreach ($newCart->t_panier_doc as $pdoc){ //transfert de toutes les lignes du panier dans le nouveau
							$pdoc['ID_LIGNE_PANIER']="";
							$pdoc['ID_PANIER']=$newCart->t_panier['ID_PANIER'];
							if (!$newCart->savePanierDoc($pdoc)) $err++;
						}
					}

					$newCart->getPanier(); //on reload et on passe sur le nouveau panier
					$myPanier = $newCart;
					if ($err==0) $myPanier->dropError(kSuccesPanierCreation);
				}

			}

			else if ($action=='DUPLIQUER' && !$param["acces_externe"]) {

				// VP (8/09/09) : utilisation paramètres $updatePanierDoc=false dans met_a_jour()
				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST,1,false);

				/*
				if (get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
					//VP 13/07/09 : dupliquer dossier thema (ajout champ PAN_PUBLIC)
					$newCart=new Panier();

					$newCart->t_panier=array(  //transfert des paramètres et copie des valeur du panier d'origine
							"PAN_ID_ETAT"=>($myPanier->t_panier['PAN_ID_ETAT']=="1"?"0":$myPanier->t_panier['PAN_ID_ETAT']),
							"PAN_ID_GEN"=>$myPanier->t_panier['PAN_ID_GEN'],
							"PAN_DOSSIER"=>$myPanier->t_panier['PAN_DOSSIER'],
							"PAN_PUBLIC"=>$myPanier->t_panier['PAN_PUBLIC'],
							"PAN_FRAIS_HT"=>$myPanier->t_panier['PAN_FRAIS_HT'],
							"PAN_TOTAL_HT"=>$myPanier->t_panier['PAN_TOTAL_HT'],
							"PAN_TVA"=>$myPanier->t_panier['PAN_TVA'],
							"PAN_OBJET"=>$myPanier->t_panier['PAN_OBJET'],
							"PAN_XML"=>$myPanier->t_panier['PAN_XML'],
							"PAN_TITRE"=>$myPanier->t_panier['PAN_TITRE'],
							"PAN_ID_TYPE_COMMANDE"=>$myPanier->t_panier['PAN_ID_TYPE_COMMANDE'],
							"PAN_ID_USAGER"=>User::getInstance()->UserID);
					$ok=$newCart->createPanier();
					if($myPanier->t_panier['PAN_ID_ETAT']=="0") $myPanier->t_panier['CARTLIST']=$newCart->t_panier['ID_PANIER'];
				}
				$err=0;

				foreach ($myPanier->t_panier_doc as $pdoc){ //transfert de toutes les lignes du panier dans le nouveau
						$pdoc['ID_LIGNE_PANIER']="";
						$pdoc['ID_PANIER']=$newCart->t_panier['ID_PANIER'];
						if (!$newCart->savePanierDoc($pdoc)) $err++;
				}
				*/
				if (get_class($myPanier)=='Panier' ){
					$dataToUpd = array();
					$id_gen = $myPanier->t_panier['PAN_ID_GEN'];
					if($_POST['action_panier'] == 'cp_vers_public') {
						$dataToUpd['PAN_PUBLIC'] = '1';
						$dataToUpd['PAN_ID_ETAT'] = '0';
						$id_gen = 0;
					}
					$newCart=$myPanier->duplicate($id_gen,$dataToUpd);
				}
				$newCart->getPanier(); //on reload et on passe sur le nouveau panier
				$myPanier=$newCart;

				if ($err==0) $myPanier->dropError(kSuccesPanierDupliquer);
				//Récup des paniers pour faire le compte des progs dedans
				echo '<script>refreshFolders();</script>';
			}

			else if ($action=='DUPLIQUER_LIGNES' && !$param["acces_externe"]) {
				$myPanier->t_panier['ID_PANIER']=intval($_POST['ID_PANIER']);
				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST);

				if (get_class($myPanier)=='Panier' ) { //Création à la volée d'un nouveau panier sauf si panier session !
					$newCart=new Panier();
					$newCart->t_panier=array(  //transfert des paramètres et copie des valeur du panier d'origine
											   "PAN_ID_ETAT"=>($myPanier->t_panier['PAN_ID_ETAT']=="1"?"0":$myPanier->t_panier['PAN_ID_ETAT']),
											   "PAN_ID_GEN"=>$myPanier->t_panier['PAN_ID_GEN'],
											   "PAN_DOSSIER"=>$myPanier->t_panier['PAN_DOSSIER'],
											   "PAN_FRAIS_HT"=>$myPanier->t_panier['PAN_FRAIS_HT'],
											   "PAN_TOTAL_HT"=>$myPanier->t_panier['PAN_TOTAL_HT'],
											   "PAN_TVA"=>$myPanier->t_panier['PAN_TVA'],
											   "PAN_OBJET"=>$myPanier->t_panier['PAN_OBJET'],
											   "PAN_XML"=>$myPanier->t_panier['PAN_XML'],
											   "PAN_TITRE"=>$myPanier->t_panier['PAN_TITRE'],
											   "PAN_ID_TYPE_COMMANDE"=>$myPanier->t_panier['PAN_ID_TYPE_COMMANDE'],
											   "PAN_ID_USAGER"=>User::getInstance()->UserID);
					$ok=$newCart->createPanier();
					if($myPanier->t_panier['PAN_ID_ETAT']=="0") $myPanier->t_panier['CARTLIST']=$newCart->t_panier['ID_PANIER'];
				}
				$err=0;
				foreach ($myPanier->t_panier_doc as $pdoc){
					// On ne duplique que les lignes qui sont cochées
					//	VP 10/9/09 : changement test sur checkbox
					//if($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']]=='1') {
					if(!empty($_POST["checkbox".$pdoc['ID_LIGNE_PANIER']])) {
						$pdoc['ID_LIGNE_PANIER']="";
						$pdoc['ID_PANIER']=$newCart->t_panier['ID_PANIER'];
						if (!$newCart->savePanierDoc($pdoc)) $err++;
					}
				}
				$newCart->getPanier(); //on reload et on passe sur le nouveau panier
				$myPanier=$newCart;
				if ($err==0) $myPanier->dropError(kSuccesPanierDupliquerLignes);
				//Récup des paniers pour faire le compte des progs dedans
				echo '<script>refreshFolders();</script>';
			}
			else if ($action=='COLLER_LIGNES' && !$param["acces_externe"]) {
				// VP 12/06/09 : ajout cas 'COLLER_LIGNES'
				$myPanier->getPanier();
				$myPanier->met_a_jour($_POST);

				$errDoublons=0;
				//VP 30/06/09 : mémorisation en session des numéros de lignes seulement
				/*
				 foreach ($_SESSION['scrap']['t_panier_doc'] as $pdoc){
					 $pdoc['ID_PANIER']=$myPanier->t_panier['ID_PANIER'];
					 if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on copie avec check de doublons
				 }
				 */
				// Ordre
				$_ordre=$myPanier->getMaxPdocOrdre();
				//VP 10/07/09 : test variable scrap
				if(isset($_SESSION['scrap']['lignes'])){
					$lignes=json_decode($_SESSION['scrap']['lignes']);
					if(is_array($lignes)){
						$myPanierSrc=new Panier();
						$myPanierSrc->t_panier['ID_PANIER']=$_SESSION['scrap']['id_panier'];
						$myPanierSrc->getPanierDoc();
						foreach ($myPanierSrc->t_panier_doc as $pdoc) { //parcours du tableau des lignes
							if(in_array($pdoc['ID_LIGNE_PANIER'],$lignes)) {
								$pdoc['ID_PANIER']=$myPanier->t_panier['ID_PANIER'];
								//PC 24/11/10 : ajout d'un controle pour voir si l'on copie un doublon
								$sql="SELECT ID_LIGNE_PANIER FROM t_panier_doc WHERE ID_PANIER=".intval($pdoc['ID_PANIER'])."
										AND ID_DOC=".intval($pdoc["ID_DOC"])." AND (PDOC_EXTRAIT=".intval($pdoc['PDOC_EXTRAIT'])." OR (PDOC_EXTRAIT is null AND '".$pdoc['PDOC_EXTRAIT']."' like ''))
										AND PDOC_EXT_TCIN='".$pdoc['PDOC_EXT_TCIN']."' AND PDOC_EXT_TCOUT='".$pdoc['PDOC_EXT_TCOUT']."'  ";
								$res = $db->GetOne($sql);
								if ($res!=false) {
									$errDoublons++;
								}
								else
								{
									$pdoc['ID_LIGNE_PANIER']="";
									// VP 28/07/09 : mise à jour statut lignes
									$pdoc['PDOC_ID_ETAT']=$myPanier->t_panier['PAN_ID_ETAT'];
									// VP 24/05/10 : mise à jour ordre
									$pdoc['PDOC_ORDRE']=$_ordre;
									if (!$myPanier->savePanierDoc($pdoc,null,true)) $errDoublons++; //on copie avec check de doublons
								}
							}
						}
					}
					// Comptage des lignes en doublon et envoi d'erreur
					if ($errDoublons>0) {
						$myPanier->dropError(sprintf(kErrorPanierTransfertDoublon,$errDoublons));
					} else {
						$myPanier->dropError(kSuccesPanierCopie);
					}
				}else $myPanier->dropError(kErrorPanierCopieLignesVides);
				$myPanier->getPanier();
			}
			else if ((strcmp($action,'LIVRAISON')==0) && !$param["acces_externe"]) {

				require_once(libDir.'class_livraison.php');

				$myPanier->getPanier();

				//debug($myPanier);


				$nomDossierCourt=preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()); //nom de dossier défini par microtime

				$myLivraison=new Livraison();
				$myLivraison->setDir($nomDossierCourt);
				$myLivraison->arrDocs=$myPanier->t_panier_doc;
				$myLivraison->doLivraison();
				$myPanier->t_panier_doc=$myLivraison->arrDocs; //pour contrer un bug bizarre qui fait disparaitre la dernière ligne

				foreach ($myLivraison->arrDocs as $doc) {

					$strFiles[]="<a href=\"download.php?livraison=".$myLivraison->idDir."/".$doc['fileName']."\">".$doc['fileName']."</a> (".$doc['fileSize'].")";
					//$doc['fileUrl'].'|'.$doc['fileName'].'|'.$doc['fileSize'];

				}

				echo $myLivraison->error_msg;
			}

			else if ((strcmp($action,'LIVRAISON_LIGNE')==0) && !$param["acces_externe"]) {

				require_once(libDir.'class_livraison.php');

				$myPanier->getPanier();
	//			$myPanier->met_a_jour($_POST);

				foreach ($myPanier->t_panier_doc as &$pdoc){
				// On ne commande que les lignes qui sont cochées
					if($ligne==$pdoc['ID_LIGNE_PANIER']) {

						$nomDossierCourt=preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()); //nom de dossier défini par microtime

						$myLivraison=new Livraison();
						$myLivraison->setDir($nomDossierCourt);
						$myLivraison->arrDocs[0]=$pdoc;
						$myLivraison->doLivraison();
						$pdoc=$myLivraison->arrDocs[0];

						$strFiles[]="<a href=\"download.php?livraison=".$myLivraison->idDir."/".$pdoc['fileName']."\">".$pdoc['fileName']."</a> (".$pdoc['fileSize'].")";
							//$doc['fileUrl'].'|'.$doc['fileName'].'|'.$doc['fileSize'];


					}
				}
				echo $myLivraison->error_msg;

			} else if ((strcmp($action,'CANCEL')==0)) {
				$myPanier->getPanier();
			}
			//envoi de mail
			else if(strcmp($action,"SEND_MAIL")==0 && isset($_POST['email_to_send']) && !$param["acces_externe"]){
				$myPanier->getPanier();

				$email=$_POST['email_to_send'];

				$frontiere = "_----------=_parties_".md5(uniqid (rand()));
				$param = 	array (
								"etat" => $myPanier->panier['PAN_ID_ETAT'],
								"playerVideo" => gPlayerVideo,
								"imgurl"=>kCheminHttp."/design/images/",
								"senderName"=>$user->Prenom . ' ' . $user->Nom,
								"boundary"=>$frontiere,
								"corpsTexte"=>$_POST['corpsMail']
							);

				foreach ($myPanier->t_panier_doc as $key=>$doc){
					if (isset($doc['ID_DOC'])){
						$res = $db->getAll("SELECT ID_MAT from t_doc_mat where ID_DOC = " . $db->Quote($doc['ID_DOC']));
						foreach ($res as &$mat){
							$ext = substr($mat['ID_MAT'], strrpos($mat['ID_MAT'], '.'));
							if (strcmp($ext, ".mp4") == 0)
								$myPanier->t_panier_doc[$key]['VISIOFLASH'] = $mat['ID_MAT'];
						}
						$myPanier->t_panier_doc[$key]['URLHTTP'] = kCheminHttp;
					}
				}

				$xml=$myPanier->xml_export();
				$xml.=$myPanier->xml_export_panier_doc();
				$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<EXPORT_OPSIS>".$xml."\n</EXPORT_OPSIS>";

				$message= TraitementXSLT($xml,getSiteFile("designDir","print/selectionMail.xsl"),$param,1);
				ob_start();
				eval("?>". str_replace(array('||'," ' "),array(chr(10),"'"),$message)."<?");
				$message=ob_get_contents();
				ob_end_clean();
				$message=str_replace(array("€","’"),array("&euro;","'"),$message);
				$headers="From:".$user->Mail."\nMIME-Version: 1.0\nContent-Type: multipart/alternative; boundary=\"".$frontiere."\"";
				if(!empty($_POST['copyForExp']) && $_POST['copyForExp'] == '1')
				$headers .= "\nCc: ".$user->Mail;
				/*if(defined("kMailSujetEnvoi")) $sujet=constant("kMailSujetEnvoi").gSite;
				else $sujet="Sélection sur ".gSite;*/

				//$sujet = utf8_decode($sujet);
				//$message = utf8_decode($message);
				$sujet = $_POST['objetMail'];
				//$rtn=mail($email, "=?ISO-8859-1?B?".base64_encode(utf8_decode($sujet))."?=",utf8_decode($message),$headers);
				include_once(modelDir.'model_message.php');
				$msg = new Message();

				$rtn=$msg->send_mail($email, $user->Mail,"=?ISO-8859-1?B?".base64_encode(utf8_decode($sujet))."?=", utf8_decode($message), "multipart/alternative", $frontiere);

				if ($rtn)
					$myPanier->dropError(kSuccesSendEmail);
				else
					$myPanier->dropError(kFailSendEmail);

				unset($msg);


			}

		}
		// MS - ajout possibilité de neutraliser l'affichage, utile pour le block panier quand on arrange l'ordre du panier via drag n drop par exemple.
		// le formulaire est soumis dans une iframe, mais on n'a pas besoin de faire l'affichage dans l'iframe puisqu'elle est cachée
		if(isset($_GET['noDisplay']) && !empty($_GET['noDisplay']) && $_GET['noDisplay']) return;
		
		// VP 21/3/14 : appel listSelections après traitements pour mise à jour
		if ($user->loggedIn())
			$listSelections=Panier::getFolders(true);
		
		$xmlPanier=$myPanier->xml_export(0,0);
		
		if(isset($_GET["page"])){
            $page= $_GET["page"];
        }
        else $page="1";
		$nbDeft=(defined("gNbLignesPanierDefaut")?gNbLignesPanierDefaut:10);
		// $nbLignes=((isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'])?$_REQUEST['nbLignes']:$nbDeft);
		//Sauvegarde du nombre de lignes de recherche � afficher :
		if(isset($_REQUEST['nbLignes']) && $_REQUEST['nbLignes'] != '') {
			if(!isset($_SESSION['nbLignesPanier'])){
				$_SESSION['nbLignesPanier']=$_REQUEST['nbLignes'];
			}

			// MS 02/10/2012 Si on change le nombre de lignes affichées par page, on retourne en page 1
			if($_REQUEST['nbLignes'] != $_SESSION['nbLignesPanier']){
				$_REQUEST['page']= 1;
				$_SESSION['nbLignesPanier']=$_REQUEST['nbLignes'];
				$_SESSION['doc_page']= 1;
			}
			$nbLignes=$_SESSION['nbLignesPanier'];
		}
		 if(isset($_SESSION['nbLignesPanier']))
			$nbLignes=$_SESSION['nbLignesPanier'];
		else $nbLignes=	$nbDeft;
		$page=isset($_REQUEST['page'])?$_REQUEST['page']:1;
		//$myPage->initPagerFromArray($myPanier->t_panier_doc);


		if(isset($_SESSION['recherche_PAN']['page']))
			$var_page='&page='.$_SESSION['recherche_PAN']['page'];
		else $var_page='&page=1';

		//update VG 30/06/11 : ajout du param print
        $param["print"] = ((isset($_GET['print']) && $_GET['print'])?htmlentities($_GET['print']):0);
        $param["nbLigne"] = $nbLignes;
        $param["page"] = $page;
        $param["playerVideo"]=gPlayerVideo;
        $param["pager_link"]=$pagerLink;
		$param["offset"] =  $nbLignes*($page-1);
        $param["cartList"]=isset($cartList)?$cartList:'';
        $param["urlparams"]=$myPage->addUrlParams(null, false);
        //$param["titre"]=$myPanier->titre;
        $param["frameurl"]=frameUrl;
        $param["page_comListe"]=$var_page;
        $param["error_msg"]=$myPanier->error_msg; //by LD 20 11 08, envoi msg erreur
		$param["tri"] = $myPage->order;
		$param["ordre"] = $myPage->triInvert;
        // PC 06/10/10 : ajout possibilité de spécifier xmlfile
		/*if (!isset($param["xmlfile"]) || empty($param["xmlfile"]))
			$param["xmlfile"]=getSiteFile("listeDir","xml/panierDoc.xml");*/
		
		if (isset($myLivraison) && $myLivraison){
			$xmlLivraison=$myLivraison->xml_export(0,0);
		}
		$this->params['additionnalXML']['cart_list']=$listSelections; 
		$this->params['additionnalXML']['privileges']=$_SESSION['USER']['US_GROUPES']; 
		
		if(isset($xmlPanier) && !empty($xmlPanier)){
			$this->params['additionnalXML']['panier']=$xmlPanier; 
		}
		if(isset($xmlLivraison) && !empty($xmlLivraison)){
			$this->params['additionnalXML']['livraison']=$xmlLivraison; 
		}
		// $allXML=$xmlPanier.$xmlLivraison;
		if(isset($grpCarts) && !empty($grpCarts)){
			 $this->params['additionnalXML']['grp_cart_list'] = $grpCarts;
		}
		
		$this->params["title"]= $myPanier->titre;
        $this->params['nomElement'] = "t_panier_doc";
        $this->params['searchRes'] = $myPanier->t_panier_doc;
        $this->params['contentTemplate'] = getSiteFile("designDir","templates/content/panier.html");
        $this->params['xslListFile'] = getSiteFile("listeDir",$xsl);
        $this->params['xmlListFile'] = getSiteFile("listeDir",$xml_liste);
        $this->params['params4XSL'] = $param;
    }
    
}

?>
