<?
/**
 *  Classe Controller_html
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(libDir."class_chercheHtml.php");
require_once(modelDir.'model_html.php');
require_once(libDir."class_page.php");

class Controller_html extends Controller {
    /** Détail HTML
     * IN : GET
     * OUT : objet html
     */
    function do_html($args=null) {
        
        $myHtml=new Html();
        if(!empty($_REQUEST["id_html"])){
            $myHtml->t_html['ID_HTML'] = intval($_REQUEST["id_html"]);
        }elseif(!empty($myPage->id)){
            $myHtml->t_html['ID_HTML'] = intval($myPage->id);
        }elseif(isset($_GET["html"]) && !empty($_GET["html"])){
            $myHtml->t_html['ID_HTML'] = intval($_GET["html"]);
        }
        $myHtml->getHtml();
        
        $nomFichierXsl=getSiteFile('designDir','htmlAff.xsl');
        if (preg_match('#index.php$#',$nomFichierXsl)!=0) // si getSiteFile retourne la page d'index alors la XSL n'est pas trouvée
        {
            $html = $myHtml->t_html['HTML_CONTENU'];
        }
        else
        {
            $myHtml->t_html['HTML_CONTENU']=str_replace('\'','&#39;',$myHtml->t_html['HTML_CONTENU']);
            $xml=$myHtml->xml_export(1);
            $params4XSL=array();
            foreach($params4XSL as $ind => $data) $tab[$ind] = $data;
            $html=TraitementXSLT($xml,$nomFichierXsl,$tab,0,$xml,$arrHighlight);
        }
		
        $this->init_showParam($myHtml, (isset($myHtml->t_html['HTML_TITRE']) && !empty($myHtml->t_html['HTML_TITRE'])?$myHtml->t_html['HTML_TITRE']:''), $html);
    }
    
    
    /** Liste HTML
     * IN : GET, POST
     * OUT : objet cherche
     */
    function do_htmlListe($args=null) {
        $mySearch=new RechercheHtml();
		$this->init_list($mySearch, "F_html_form", kListePages, "html");
        
        if($mySearch->affichage=='HIERARCHIQUE'){
            // Préparation affichage hiérachique
            require_once(libDir."class_tafelTree.php");
            $mySearch->treeParams=array("ID_TYPE_VAL"=>$mySearch->getValueForField(array('FIELD'=>'VAL_ID_TYPE_VAL','TYPE'=>'CE')));
            $btnEdit=$myPage->getName()."?urlaction=htmlSaisie&id_html=";
            $myAjaxTree=new tafelTree($mySearch,'form1');
            $myAjaxTree->JSReturnFunction=$jsFunction;
            
            $myAjaxTree->makeRoot();
            $myAjaxTree->JSlink="window.location.href='".$myPage->getName()."?urlaction=htmlSaisie&id_html=%3\$s'"; //Le lien sur les terme ouvrira l'édition
            
            if ($mySearch->getValueForField(array('FIELD'=>'HTML_TITRE','TYPE'=>'FT'))!=''
                || $mySearch->getValueForField(array('FIELD'=>'HTML_TITRE','TYPE'=>'C'))!='' ) { //En plus, on a une recherche sur un terme
                if (!empty($mySearch->result)) {
                    foreach ($mySearch->result as $row) { //Pour chaque résultat...
                        $myAjaxTree->revealNode($row['ID_HTML']);
                    }
                }
                $this->params['nb_results'] = $mySearch->found_rows;
            }
            $this->params['ajaxTree'] = $myAjaxTree;
        }
    }
    
    
    /** Saisie HTML
     * IN : GET, POST, objet html
     * OUT : objet html
     */
    function do_htmlSaisie($args=null) {

        $myPage=Page::getInstance();
        $myPage->nomEntite = kPage;
        $myPage->setReferrer(true);

        $myHtml=new Html();
		$myHtml->init();

        if(!empty($_REQUEST["id_html"])){
            $myHtml->t_html['ID_HTML'] = intval($_REQUEST["id_html"]);
        }elseif(!empty($myPage->id)){
            $myHtml->t_html['ID_HTML'] = intval($myPage->id);
        }
        $idLang=(!empty($_REQUEST['id_lang'])?$_REQUEST['id_lang']:$_SESSION['langue']);
        if(!empty($myHtml->t_html['ID_HTML'])){
            $myHtml->t_html['ID_LANG']=$idLang;
            $myHtml->getHtml(null,$idLang);
        }

        if(isset($_POST)){
            $commande="";
            if (isset($_POST['commande']))
                $commande = $_REQUEST['commande'];
            elseif (isset($_POST['action']))
            $commande = $_POST['action'];
            
            switch ($commande) {
                case "SAVE":  // Sauvegarde après modif infos
                    $myHtml->updateFromArray($_POST);
                    $ok=$myHtml->save(); // renvoie 'crea' si on crée, rien si on update
                    break;
                    
                case "SUP": // Supression
                case "SUPPR": // Retrocompatibilité
                    $myHtml->updateFromArray($_POST);
                    if ($myHtml->delete() ) $myPage->redirect($myPage->getName()."?urlaction=htmlListe");
                    break;
                    
                case "": // pas d'action, on préinitialise le type avec celui issu du get
                    $myHtml->updateFromArray($_POST);
                    break;
            }
        }
        
		$this->init_editParam($myHtml, (isset($myHtml->t_html['HTML_TITRE']) && !empty($myHtml->t_html['HTML_TITRE'])?$myHtml->t_html['HTML_TITRE']:''));
    }
}

?>