<?
/**
 *  Classe Controller_mat
 * Version : 1.0 (PHP5)
 * Author : Opsomai
 */
require_once(coreDir."/controller/controller.php");
require_once(libDir."class_chercheMat.php");
require_once(modelDir.'model_materiel.php');
require_once(libDir."class_page.php");

class Controller_mat extends Controller {
    
    /** Détail mat
     * IN : GET
     * OUT : objet entité
     */
    function do_mat($args=null) {
        global $db;
        
        $myPage=Page::getInstance();
        $myPage->nomEntite = kMateriel;
        $myPage->setReferrer(true);
        
        // I. Ouverture enregistrement
        $myMat= new Materiel();
        if ($_REQUEST['id_lang'] && in_array($_REQUEST['id_lang'],$_SESSION['arrLangues']) ) $myMat->id_lang=$_REQUEST['id_lang'];
        
        if(isset($_GET["id_mat"])||isset($_GET["rang"]))
        {
            $id_lang=strtoupper($_SESSION["langue"]);
            
            if(isset($_GET["rang"])) $rang=intval($_GET["rang"]);
            
            // Accès par ID_MAT si existant
            if(isset($_GET["id_mat"])) {
                $id_mat=intval($_GET["id_mat"]);
            } else {
                // Accès par rang sinon, en cherchant ID_MAT du résultat
                if($rang==0) $rang=1; // Premier résultat par défaut
                $sql=$_SESSION["recherche_MAT"]["sql"];
                if(isset($_SESSION["recherche_MAT"]["tri"])) $sql.= $_SESSION["recherche_MAT"]["tri"];
                if (!empty($sql)) {
                    $result=$db->Execute($sql." limit 1 OFFSET ".($rang-1));
                    if($result && $row=$result->FetchRow()){
                        $id_mat=$row["ID_MAT"];
                    }
                    $result->Close();
                } else {$myPage->redirect($myPage->getName()."?urlaction=matListe");}
            }
            
            $myMat->t_mat['ID_MAT']=$id_mat;
            
            $myMat->getMat();
            $myMat->getValeurs();
            $myMat->getPersonnes();
            $myMat->getDocs();
            $myMat->getChildren();
            $myMat->getTapeFile();
            $tracks=$myMat->getMatTrack();
            
            //@update VG 19/05/2010
            $tMatInfo = xml2tab($myMat->t_mat['MAT_INFO']);
            $tMatInfo = $tMatInfo['XML'][0]['FILE'][0];
            if ($tMatInfo['WIDTH'] && $tMatInfo['HEIGHT']) {
                $tDimImg = getMediaDim( gResolutionImageDPI, $tMatInfo['WIDTH'], $tMatInfo['HEIGHT']);
                $myMat->t_mat[key($tDimImg)] = current($tDimImg);
            }
            
			$this->init_showParam($myMat, (isset($myMat->t_mat['MAT_NOM']) && !empty($myMat->t_mat['MAT_NOM'])?$myMat->t_mat['MAT_NOM']:''));
            $this->params['highlight'] = $_SESSION['recherche_MAT']['highlight'];
            $this->params['params4XSL']['xmlfile'] = getSiteFile("designDir","detail/xml/matAff.xml");
        }
    }
    
    /** Liste mat
     * IN : GET, POST
     * OUT : objet cherche
     */
    function do_matListe($args=null) {
        $mySearch=new RechercheMat();
		$this->init_list($mySearch, "F_mat_form", kGestionMateriels, "mat", array("COL" => array("MAT_NOM"), "PREFIX" => array("M"), "DIRECTION" => array("ASC")));
        
        if (!isset($_POST["F_mat_form"]) && !isset($_GET["init"]) && isset($_POST["commande"])) {
			$id_mat=intval($_POST["ligne"]);
			if($_POST["commande"]=="SUP" && !empty($id_mat)) {
				$myMat = new Materiel();
				$myMat->t_mat['ID_MAT']=$id_mat;
				$myMat->delete();
				unset($myMat);
			}
		}
    }
    
    
    /** Saisie mat
     * IN : GET, POST, objet mat
     * OUT : objet mat
     */
    function do_matSaisie($args=null) {
        $myPage=Page::getInstance();
        $myPage->nomEntite = kMateriel;
        // $myPage->setReferrer(true);

        $myMat = new Materiel();

        if(!empty($_REQUEST["id_mat"])){
            $myMat->t_mat['ID_MAT'] = intval($_REQUEST["id_mat"]);
        }elseif(!empty($myPage->id)){
            $myMat->t_mat['ID_MAT'] = intval($myPage->id);
        }
         if(!empty($_REQUEST["id_mat_ref"])){
             $myMat->id_mat_ref =$_REQUEST["id_mat_ref"];
        }
        switch ($_REQUEST['commande']) {
            case "SAVE":  // Sauvegarde après modif infos
            case "DUP":
                $myMat->updateFromArray($_POST);
                

                // Traitement de l'ordre d'affichage.
                if ($_POST['arrOrdre']) {
                    foreach ($_POST['arrOrdre'] as $idx=>$id_mat) {
                        //if ($id_mat==$myMat->t_mat['ID_MAT'] || $id_mat==$myMat->id_mat_ref ) { // Au sein du tableau groupe, je pointe sur mon matériel
                        if ($id_mat==$myMat->t_mat['ID_MAT']) { // Au sein du tableau groupe, je pointe sur mon matériel
                            $myMat->t_mat['MAT_ORDRE']=$idx+1; //+1 pour démarrer les ordres à partir de 1.
                        }
                        else { // Une autre ligne du tableau groupe.
                            $myOtherMat=new Materiel;
                            $myOtherMat->t_mat['ID_MAT']=$id_mat;
                            //VG 24/05/2013 : Ajout de getMat pour récupérer le nom dans le cas où il n'est pas passé
                            if(empty($myOtherMat->t_mat['MAT_NOM'])) {
                                $myOtherMat->getMat();
                            }
                            $myOtherMat->id_mat_ref=$myOtherMat->t_mat['MAT_NOM'];
                            $myOtherMat->t_mat['MAT_ORDRE']=$idx+1;
                            $myOtherMat->save();
                        }
                    }
                    unset($myOtherMat);
                }
                else {
                    $myMat->t_mat['MAT_ORDRE']='1';
                }

                if ($action == "DUP"){
                    $myMat->t_mat['ID_MAT']='';
                    $myMat->id_mat_ref='';
                }
                $ok = $myMat->save(); // renvoie 'crea' si on crée, rien si on update
                
                if ($ok){
                    if (!empty($_POST['t_mat_val'])){
                        $myMat->t_mat_val = $this->transformArrayMulti($_POST['t_mat_val'], array('ID_VAL'),'ID_TYPE_VAL');
                        $myMat->saveMatVal();
                    }
                    
                    if (!empty($_POST['t_mat_pers'])){
                        $myMat->t_mat_pers = $this->transformArrayMulti($_POST['t_mat_pers'], array('ID_PERS'),'ID_TYPE_DESC');
                        $myMat->saveMatPers();
                    }
                    
                    // VG 09/09/10 : ajout getMat ( pour récupérer certaines données, certaines ayant ou pouvant être modifées/supprimées pour la sauvegarde)
                    $myMat->getMat();
                    $myMat->updateEtatArch();
                }
                
                // Redirection éventuelle
                if(isset($_POST["page"]) && !empty($_POST["page"]) ){
                    $myPage->redirect($_POST["page"]);
                }
                break;
                
            case "SUP": // Supression
                $myMat->delete();
				if($myPage->hasReferrer())
					$myPage->redirect($myPage->getReferrer());
                break;
				
            case "PURGE": // Suppression et purge
                $myMat->delete(true);
				if($myPage->hasReferrer())
					$myPage->redirect($myPage->getReferrer());
                break;
				
            case "PURGE_FICHIER": // purge fichier
                $myMat->purge();
                break;
                
            case "": // pas d'action, on préinitialise le type avec celui issu du get (cas de création d'une nv mat)
                $myMat->updateFromArray($_POST);
                break;
                
        }
	
	
        if(!empty($myMat->t_mat['ID_MAT'])){
            $myMat->getMat();
            $myMat->getValeurs();
            $myMat->getPersonnes();
            $myMat->getDocs();
            $myMat->getTapeFile();
            $myMat->getChildren();
        }
        
		$this->init_editParam($myMat, (isset($myMat->t_mat['MAT_NOM']) && !empty($myMat->t_mat['MAT_NOM'])?$myMat->t_mat['MAT_NOM']:''));
    }
}

?>
