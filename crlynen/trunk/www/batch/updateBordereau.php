<?php

// Adaptation pour import en ligne de commande
// Syntaxe : importShell.php fichier

    require_once("../conf/conf.inc.php");
    require_once(libDir."session.php");
    require_once(libDir."fonctionsGeneral.php");
    require_once(libDir."class_page.php");
    
    require_once(modelDir."model_usager.php");
    require_once(modelDir."model_panier.php");
   
    global $db;
    ini_set('memory_limit',-1);
    ini_set('max_execution_time',0);
    ini_set('auto_detect_line_endings',true);
    
if(isset($_POST["bOK"]) && $_FILES["importFichier"]["tmp_name"]!='' && $_FILES["importFichier"]["size"]>0) {
	$file=$_FILES["importFichier"]["tmp_name"];
	$file_org=$_FILES["importFichier"]["name"];
	updateBord($file);
	//importFilm(kCheminLocalSurServeur."/batch/importFilm/import8.csv");
} else {
	?> <form name="form1" id="form1" action="updateBordereau.php?<?=$_SERVER["QUERY_STRING"];?>" method="post" enctype="multipart/form-data" >
		<input name="importFichier" type="file" size="60" />
		<input name="bOK" type="submit" class='BUTTON' value="<?=kValider?>" />
	</form> <?
}
exit;	
	
    
function importFichier($file) {
    $rows=array();
    if(is_file($file)){
        $fd=fopen($file,"r");
        $separator=";";
        while (($data = fgetcsv($fd, 16000, $separator)) !== FALSE) {
            if(!isset($keys)){
                $keys=$data;
            }else{
                $row=array();
                foreach($keys as $i=>$key){
                    if(!empty($data[$i]) && $data[$i]!="null")
                       $row[$key]=trim(utf8_encode($data[$i]));
                }
                if(!empty($row))
                    $rows[]=$row;
            }
        }
        fclose($fd);
    }
    return $rows;
}


// Debut code import
function updateBord($file) {
	global $db;
	$bordereaux = importFichier($file);

	$etats_ligne = array("X","Disponible", "X", "Expédié", "X", "Retourné");
	
    foreach($bordereaux as $bordereau){
		// Panier
		$id_panier=$db->GetOne("select ID_PANIER from t_panier where PAN_OBJET=".$db->Quote($bordereau['PAN_OBJET']));
		
		// Lignes
		$id_doc=$db->GetOne("select ID_DOC from t_doc where DOC_COTE=".$db->Quote($bordereau['DOC_COTE']));
		$id_mat=$db->GetOne("select ID_MAT from t_mat where MAT_NOM=".$db->Quote($bordereau['MAT_NOM']));
		if(empty($id_panier) || empty($id_doc) || empty($id_mat)){
			echo "Not found id_panier=$id_panier id_doc=$id_doc DOC_COTE=".$db->Quote($bordereau['DOC_COTE'])." id_mat=$id_mat MAT_NOM=".$db->Quote($bordereau['MAT_NOM'])."\n<br />";
		}else{
			$panierDoc['ID_PANIER']=$id_panier;
			$panierDoc['ID_DOC']=$id_doc;
			$panierDoc['PDOC_ID_MAT']=$id_mat;
			//$panierDoc['PDOC_ID_ETAT']=array_search($bordereau['PDOC_ETAT'], $etats_ligne);
			$panierDoc['PDOC_ID_ETAT']=5;
			$ok = $db->insertBase("t_panier_doc","id_ligne_panier",$panierDoc);
			echo "Found id_panier=$id_panier id_doc=$id_doc DOC_COTE=".$db->Quote($bordereau['DOC_COTE'])." id_mat=$id_mat MAT_NOM=".$db->Quote($bordereau['MAT_NOM'])."\n<br />";
		}
		
    }
	unset($bordereaux);
	
	
}
?>
