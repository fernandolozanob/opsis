<?php
require_once("../conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php");
require_once(libDir."class_page.php");

require_once(modelDir."model_imageur.php");
require_once(modelDir."model_materiel.php");
require_once(modelDir."model_doc.php");
require_once(modelDir."model_usager.php");
require_once(modelDir."model_message.php");

header('Content-type: text/plain');

global $db;
    
    if($argc<3) {
        print "Syntaxe : importXML.php fichier xsl [encodage=ISO-8859-1]\n";
        exit;
    }

    $file[0]=$argv[1];
    $xsl=$argv[2];
	if(isset($argv[3])) $encodage=$argv[3];
    else $encodage="ISO-8859-1";

	$file_org=$file;
	$_REQUEST['encodage'] = $encodage;
	
	if(!is_file(designDir."/print/".$xsl.".xsl")) {
        print "Fichier xsl manquant : ".designDir."/print/".$xsl.".xsl \n";
        exit;
    }
    
    print "DEBUT ".date('H:i:s',time())."\n";
    print "Import du fichier ".$file[0]." xsl ".$xsl.".xsl\n";
    require_once(includeDir."importXML.php");

    print "FIN ".date('H:i:s',time())."\n";



?>
