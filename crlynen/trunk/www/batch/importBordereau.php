<?php

// Adaptation pour import en ligne de commande
// Syntaxe : importShell.php fichier

    require_once("../conf/conf.inc.php");
    require_once(libDir."session.php");
    require_once(libDir."fonctionsGeneral.php");
    require_once(libDir."class_page.php");
    
    require_once(modelDir."model_usager.php");
    require_once(modelDir."model_panier.php");
   
    global $db;
    ini_set('memory_limit',-1);
    ini_set('max_execution_time',0);
    ini_set('auto_detect_line_endings',true);
    
    
function importFichier($file) {
    $rows=array();
    if(is_file($file)){
        $fd=fopen($file,"r");
        $separator=";";
        while (($data = fgetcsv($fd, 16000, $separator)) !== FALSE) {
            if(!isset($keys)){
                $keys=$data;
            }else{
                $row=array();
                foreach($keys as $i=>$key){
                    if(!empty($data[$i]) && $data[$i]!="null")
                       $row[$key]=trim(utf8_encode($data[$i]));
                }
                if(!empty($row))
                    $rows[]=$row;
            }
        }
        fclose($fd);
    }
    return $rows;
}


// Debut code import
if($argc>2) {
	print "Import ".$argv[1]." ".$argv[2]."\n";
	$clients=importFichier($argv[1]);
	
    // Clients
    foreach($clients as $client){
        $newUser = new Usager();
        if(!empty($client['PAYS'])){
            $id_pays=$db->GetOne("select ID_PAYS from t_pays where PAYS".$db->like($client['PAYS']));
            if(empty($id_pays)){
                $pays['ID_PAYS']=substr($client['PAYS'], 0, 4);
                $pays['ID_LANG']="FR";
                $pays['PAYS']=$client['PAYS'];
                $id_pays = $db->insertBase("t_pays","id_pays",$pays);
            }
            $client['US_SOC_ID_PAYS'] = $id_pays;
        }
		if(!empty($client['FONCTION_USAGER'])){
			$id_fct=$db->GetOne("select ID_FONCTION_USAGER from t_fonction_usager where FONCTION_USAGER".$db->like($client['FONCTION_USAGER']));
			if(empty($id_fct)){
				$fct['ID_LANG']="FR";
				$fct['FONCTION_USAGER']=$client['FONCTION_USAGER'];
				$id_fct = $db->insertBase("t_fonction_usager","id_fonction_usager",$fct);
			}
			$client['ID_FONCTION_USAGER'] = $id_fct;
		}
		if(substr($client['ETAT_USAGER'],0,1)=='O'){
			$client['US_ID_ETAT_USAGER'] = 2;
		}
		$client['US_ID_TYPE_USAGER'] = 1;
		$client['US_SOC_ADRESSE'] = trim($client['ADRESSE_1']."\n".$client['ADRESSE_2']."\n".$client['ADRESSE_3'], "\n");
        $client['US_LOGIN'] = $client['US_CODE']."-".$client['US_NOM'];
		$client['US_PASSWORD'] = substr(md5(uniqid()), 0, 8);
		$client['US_DATE_CREA']=convDate($client['US_DATE_CREA']);
		$client['US_DATE_MOD']=convDate($client['US_DATE_MOD']);
        $newUser->createFromArray($client);
    }
	unset($clients);
	
    // Bordereaux
	$bordereaux=importFichier($argv[2]);
	
	$etats_bordereau = array("X","En Cours", "Programm�", "Exp�di�", "Retour Partiel", "Sold�", "Retour en attente", "En contentieux");
	$etats_ligne = array("X","Disponible", "X", "Exp�di�", "X", "Retourn�");
	
    foreach($bordereaux as $bordereau){
		// Panier
		$id_panier=$db->GetOne("select ID_PANIER from t_panier where PAN_OBJET=".$db->Quote($bordereau['PAN_OBJET']));
		
		if(empty($id_panier)){
			
			$id_usager=$db->GetOne("select ID_USAGER from t_usager where US_CODE=".$db->Quote($bordereau['US_CODE'])." AND US_NOM=".$db->Quote($bordereau['US_NOM'])." AND US_PRENOM=".$db->Quote($bordereau['US_PRENOM']));
			$bordereau['PAN_ID_TYPE_COMMANDE']=5;
			$bordereau['US_SOC_ADRESSE'] = trim($bordereau['COMM_1']."\n".$bordereau['COMM_2']."\n".$bordereau['COMM_3'], "\n");
			$bordereau['PAN_DATE_CREA']=convDate($bordereau['PAN_DATE_CREA']);
			$bordereau['PAN_DATE_MOD']=convDate($bordereau['PAN_DATE_MOD']);
			$bordereau['PAN_ID_ETAT']=array_search($bordereau['ETAT'], $etats_bordereau);
			$newPanier = new Panier();
			$newPanier->updateFromArray($bordereau);
			$newPanier->createPanier();
			$id_panier = $newPanier->t_panier['ID_PANIER'];
			if(!empty($id_panier)){
				// Mise � jour date crea et pan_id_usager
				$db->Execute("update t_panier set pan_date_crea=".$db->Quote($bordereau['PAN_DATE_CREA']).", pan_date_mod=".$db->Quote($bordereau['PAN_DATE_MOD']).", pan_id_usager=".intval($id_usager)." where id_panier=".$newPanier->t_panier['ID_PANIER']);
			}
			if(empty($id_usager)){
				echo "Not found id_panier=$id_panier US_CODE=".$db->Quote($bordereau['US_CODE'])." AND US_NOM=".$db->Quote($bordereau['US_NOM'])." AND US_PRENOM=".$db->Quote($bordereau['US_PRENOM'])."\n";
			}
		}
		
		// Lignes
		$id_doc=$db->GetOne("select ID_DOC from t_doc where DOC_COTE=".$db->Quote($bordereau['DOC_COTE']));
		$id_mat=$db->GetOne("select ID_MAT from t_mat where MAT_NOM=".$db->Quote($bordereau['MAT_NOM']));
		if(empty($id_doc) && empty($id_mat)){
			echo "Not found id_panier=$id_panier id_doc=$id_doc DOC_COTE=".$db->Quote($bordereau['DOC_COTE'])." id_mat=$id_mat MAT_NOM=".$db->Quote($bordereau['MAT_NOM'])."\n";
		}else{
			$panierDoc['ID_PANIER']=$id_panier;
			$panierDoc['ID_DOC']=$id_doc;
			$panierDoc['PDOC_ID_MAT']=$id_mat;
			$panierDoc['PDOC_ID_ETAT']=array_search($bordereau['PDOC_ETAT'], $etats_ligne);
			$ok = $db->insertBase("t_panier_doc","id_ligne_panier",$panierDoc);
		}
		
    }
	unset($bordereaux);
	
	
} else {
    print "Syntaxe : importBordereau.php Client.csv Bordereau.csv \n";
}
?>
