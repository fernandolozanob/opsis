<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- Developpeurs : Loic Desjardins											  ---*/
/*-----------------------------------------------------------------------------------*/
// NOTA : éè respecter cet ordre !!!
	error_reporting(E_PARSE | E_ERROR);
	//error_reporting(E_ALL);

    $time_start = microtime(true);

     require_once("conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php");
     require_once(libDir."class_page.php");


    $myFrame=Page::getInstance();
    $myFrame->setDesign(templateDir."empty.html");

	$myUsr=User::getInstance();
	if($myUsr->loggedAsGuest()){
		require_once(modelDir."model_partage.php");
		$partage = new Partage() ; 
		$partage->getPartageFromCode($myUsr->guestPartage);
		if($partage->isValid() && isset($_GET['id']) && $_GET['id'] == $partage->t_partage['PARTAGE_ID_ENTITE']){
			$myFrame->guestPartageAccess = true ; 
		}
	}
	

	$myFrame->includePath=frameDir;
	if (isset($_GET['where']) && $_GET['where']) $myFrame->includePath=coreDir."/".$_GET['where']."/";
	if (isset($_GET['upload']) && $_GET['upload']) $myFrame->includePath=upload_scriptDir;
	if (isset($_GET['include']) && $_GET['include'] )  $myFrame->includePath= designDir . "/include/".$_GET['include'].".inc.php";

  	$html=$myFrame->render();

	print($html);
	$time_end = microtime(true);
	$time=$time_end-$time_start;
	exit;
?>

