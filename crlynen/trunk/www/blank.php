<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- Developpeurs : Loic Desjardins											  ---*/
/*-----------------------------------------------------------------------------------*/
	
// NOTA : respecter cet ordre !!!
    $time_start = microtime(true);
     require_once("conf/conf.inc.php");
     require_once(libDir."session.php");
     require_once(libDir."fonctionsGeneral.php"); 	
     require_once(libDir."class_page.php");

    $myFrame=Page::getInstance();
	
	$myUsr=User::getInstance();

	if($myUsr->loggedAsGuest()){
		require_once(modelDir."model_partage.php");
		$partage = new Partage() ; 
		$partage->getPartageFromCode($myUsr->guestPartage);
		if($partage->isValid() && isset($_GET['id']) && $_GET['id'] == $partage->t_partage['PARTAGE_ID_ENTITE']){
			$myFrame->guestPartageAccess = true ; 
		}
	}
	
    $myFrame->setDesign(templateDir."blank.html");
    if (isset($_GET['noframe']) && $_GET['noframe']) {}
    else {$myFrame->includePath=frameDir;}
    

  	$html=$myFrame->render();  

	print($html);
	$time_end = microtime(true);
	$time=$time_end-$time_start;
	exit;
?> 

