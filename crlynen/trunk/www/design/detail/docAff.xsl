<?xml version="1.0" encoding="utf-8"?>
<!-- docAff.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:include href="../detail.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<xsl:param name="gestPers"/>
<xsl:param name="scripturl" />
<xsl:param name="xmlfile" />
<xsl:param name="profil" />

	<xsl:template match='EXPORT_OPSIS'>
		<xsl:variable name="xmllist" select="document($xmlfile)"/>
        <xsl:variable name="chemin_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,storyboardChemin); ?&gt;</xsl:variable>
        <xsl:variable name="doc_story">&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,kDocumentUrl); ?&gt;</xsl:variable>
        
        <!--div id="docBody">
			<div id="pageTitreResponsive">
				&lt;?php
					echo $myDoc->t_doc['DOC_TITRE'];
				?&gt;
				&#160;
			</div>
			<div id="leftBlock">
                &lt;? include (getSiteFile("designDir","mediaDisplay.inc.php")); ?&gt;
			</div>

			<script type="text/javascript">
			function finalizePlayer(player_options){
				$j("#media_space #container.video").css("overflow","visible");
			}
			</script-->

			<!--div id="fiche_info" class="&lt;?=($entity_has_media)?'':'no_media'; ?&gt;">
				<ul class="panelSelWrapper">
					<li id="sel_desc" class="panelSel selected" onclick="toggleMe('desc')"><xsl:processing-instruction name="php">print kDescription </xsl:processing-instruction></li>
					&lt;? if(!empty($myDoc-&gt;t_doc['DOC_ID_IMAGE']) &amp;&amp; $myDoc-&gt;t_doc['DOC_ID_MEDIA']!='P' &amp;&amp; $myDoc-&gt;t_doc['DOC_ID_TYPE_DOC']!='2'){ ?&gt;
					<li id="sel_story" class="panelSel" onclick="toggleMe('story','story')"><xsl:processing-instruction name="php">print kStoryboard ;</xsl:processing-instruction></li>
					&lt;? } ?&gt;
					&lt;? if(!empty($myDoc-&gt;t_doc['DOC_TRANSCRIPT'])){ ?&gt;
					<li id="sel_transcription" class="panelSel" onclick="toggleMe('transcription')"><xsl:processing-instruction name="php">print kTranscription; </xsl:processing-instruction></li>
					&lt;? } ?&gt;
					&lt;? if(!empty($myDoc-&gt;t_doc['DOC_NUM']) &amp;&amp; $myUsr->loggedIn() &amp;&amp; ($myDoc-&gt;t_doc['DOC_ID_MEDIA']=='V' || $myDoc-&gt;t_doc['DOC_ID_MEDIA']=='A' )){ ?&gt;
					<li id="sel_extraits" class="panelSel" onclick="toggleMe('extraits','extraits')"><xsl:processing-instruction name="php">print kExtraits; </xsl:processing-instruction></li>
					&lt;? } ?&gt;
					&lt;? if (count($arrReportPhotos) &gt; 0){ ?&gt;
					<li id="sel_report" class="panelSel" onclick="toggleMe('report','report')"><xsl:processing-instruction name="php">print kPhotosDuReportage ;</xsl:processing-instruction></li>
					&lt;? } ?&gt;
					&lt;? if(!empty($myDoc-&gt;t_doc_acc)){ ?&gt;
					<li id="sel_doc_acc" class="panelSel" onclick="toggleMe('doc_acc')"><xsl:processing-instruction name="php">print kDocumentAcc ;</xsl:processing-instruction></li>
					&lt;? } ?&gt;

				</ul-->
				<div id="desc">
					
                    <xsl:call-template name="displayView">
                        <xsl:with-param name="xmllist" select="$xmllist"/>
                    </xsl:call-template>
					
					<xsl:if test = "$xmllist/view/include != ''">
						<xsl:variable name="subfile"><xsl:value-of select="substring-before($xmlfile, 'design')" />design/<xsl:value-of select="$xmllist/view/include/folder" />/<xsl:value-of select="$xmllist/view/include/name" />.xml</xsl:variable>
						<xsl:call-template name="displayView">
							<xsl:with-param name="xmllist" select="document($subfile)"/>
						</xsl:call-template>
					</xsl:if>

                    <xsl:variable name="id_doc"><xsl:value-of select="t_doc/ID_DOC"/></xsl:variable>
                    <xsl:variable name="doc_titre"><xsl:value-of select="t_doc/DOC_TITRE"/></xsl:variable>
                    <xsl:variable name="doc_res">
                        <xsl:call-template name="nl2br">
                            <xsl:with-param name="stream">
                                <xsl:call-template name="internal-quote-replace">
                                    <xsl:with-param name="stream" select="t_doc/DOC_RES"/>
                                </xsl:call-template>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:variable>

					<xsl:if test="t_doc/t_seq/SEQ!=''">
						<div id="sequences" style="">
							<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="plans_assoc">
								<legend><xsl:processing-instruction name="php">print kSequences;</xsl:processing-instruction></legend>

								<table width="80%"  border="0" align="center" cellspacing="0" class="tableResults">
									<tr>
                                        <td class="resultsHead">&#160;</td>
										<td class="resultsHead"><xsl:processing-instruction name="php">print kNumero;</xsl:processing-instruction></td>
										<td class="resultsHead"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
										<td class="resultsHead"><xsl:processing-instruction name="php">print kType;</xsl:processing-instruction></td>
										<td class="resultsHead"><xsl:processing-instruction name="php">print kTCin;</xsl:processing-instruction></td>
										<td class="resultsHead"><xsl:processing-instruction name="php">print kTCout;</xsl:processing-instruction></td>
									</tr>
									<xsl:for-each select="t_doc/t_seq/SEQ">
										<xsl:variable name="j"><xsl:number/></xsl:variable>
                                        <xsl:variable name="vignette">
                                            <xsl:choose>
                                                <xsl:when test="string-length(IM_FICHIER)>3"><xsl:value-of select="concat($chemin_story,IM_CHEMIN,'/',IM_FICHIER)"/></xsl:when>
                                                <xsl:when test="string-length(DA_FICHIER)>3"><xsl:value-of select="concat($doc_story,DA_CHEMIN,DA_FICHIER)"/></xsl:when>
                                            </xsl:choose>
                                        </xsl:variable>
										<tr class="{concat('altern',$j mod 2)}">
                                            <td class="resultsCorps">
                                                <xsl:if test="$vignette!=''">
                                                    <a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}"><img src="makeVignette.php?image={$vignette}&amp;w=120&amp;h=90&amp;kr=1" border="0" title="{DOC_TITRE}" /></a>
                                                </xsl:if>&#160;
                                            </td>
                                            <td class="resultsCorps"><a href="?urlaction=doc&amp;id_doc={ID_DOC}"><xsl:value-of select="DOC_COTE" /></a></td>
											<td class="resultsCorps"><a href="?urlaction=doc&amp;id_doc={ID_DOC}"><xsl:value-of select="DOC_TITRE" /></a></td>
											<td class="resultsCorps"><xsl:value-of select="TYPE_DOC"/></td>
											<td class="resultsCorps"><xsl:value-of select="DOC_TCIN"/></td>
											<td class="resultsCorps"><xsl:value-of select="DOC_TCOUT"/></td>
										</tr>
									</xsl:for-each>
								</table>
							</fieldset>
						</div>
					</xsl:if>

					<xsl:if test="t_doc/*[name()='t_doc_lies_dst' or name()='t_doc_lies_src']/t_doc!=''">
						<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="field_description">
							<legend><xsl:processing-instruction name="php">print kDocumentsLies;</xsl:processing-instruction></legend>
							<table width="80%"  border="0" align="center" cellspacing="0" class="tableResults">
								<tr>
                                    <td class="resultsHead">&#160;</td>
                                    <td class="resultsHead"><xsl:processing-instruction name="php">print kCote;</xsl:processing-instruction></td>
									<td class="resultsHead"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
								</tr>
								<xsl:for-each select="t_doc/*[name()='t_doc_lies_dst' or name()='t_doc_lies_src']/t_doc">
                                    <xsl:variable name="vignette">
                                        <xsl:choose>
                                            <xsl:when test="string-length(IM_FICHIER)>3"><xsl:value-of select="concat($chemin_story,IM_CHEMIN,'/',IM_FICHIER)"/></xsl:when>
                                            <xsl:when test="string-length(DA_FICHIER)>3"><xsl:value-of select="concat($doc_story,DA_CHEMIN,DA_FICHIER)"/></xsl:when>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <xsl:variable name="j"><xsl:number/></xsl:variable>
									<tr class="{concat('altern',$j mod 2)}">
                                        <td class="resultsCorps">
                                            <xsl:if test="$vignette!=''">
                                                <a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}"><img src="makeVignette.php?image={$vignette}&amp;w=120&amp;h=90&amp;kr=1" border="0" title="{$doc_titre}" /></a>
                                            </xsl:if>&#160;
                                        </td>
                                        <td class="resultsCorps"><a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}"><xsl:value-of select="DOC_COTE"/></a></td>
										<td class="resultsCorps"><a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}"><xsl:value-of select="DOC_TITRE" /></a></td>
									</tr>
								</xsl:for-each>
							</table>
						</fieldset>
					</xsl:if>


					<xsl:if test="t_doc/t_doc_mat/DOC_MAT[t_mat/MAT_FORMAT='']!='' and $profil &gt; 1">
					<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="field_description">
					<legend><xsl:processing-instruction name="php">print kExemplaires;</xsl:processing-instruction></legend>
                    <xsl:variable name="doc_id_media"><xsl:value-of select="t_doc/DOC_ID_MEDIA"/></xsl:variable>
						<table width="80%"  border="0" align="center" cellspacing="0" class="tableResults">
							<tr>
								<td class="resultsHead"><xsl:processing-instruction name="php">print kCoteExemplaire;</xsl:processing-instruction></td>
								<td class="resultsHead"><xsl:processing-instruction name="php">print kTypeDeSupport;</xsl:processing-instruction></td>
								<td class="resultsHead"><xsl:processing-instruction name="php">print kEtat;</xsl:processing-instruction></td>
							</tr>
							<xsl:for-each select="t_doc/t_doc_mat/DOC_MAT[t_mat/MAT_FORMAT='']">
								<xsl:variable name="j"><xsl:number/></xsl:variable>
								<tr class="{concat('altern',$j mod 2)}">
									<td class="resultsCorps"><a href="index.php?urlaction=mat&amp;amp;id_mat={ID_MAT}"><xsl:value-of select="t_mat/MAT_NOM"/></a></td>
									<td class="resultsCorps"><xsl:value-of select="t_mat/t_mat_val/TYPE_VAL[@ID_TYPE_VAL='SUP']/t_val/VALEUR" /></td>
									<td class="resultsCorps"><xsl:value-of select="t_mat/t_mat_val/TYPE_VAL[@ID_TYPE_VAL='ETAT']/t_val/VALEUR"/></td>
								 </tr>
							</xsl:for-each>
						</table>
						</fieldset>
					</xsl:if>
					
					<xsl:if test="t_doc/t_doc_mat/DOC_MAT[t_mat/MAT_FORMAT!='']!='' and $profil &gt; 1">
					<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="field_description">
					<legend><xsl:processing-instruction name="php">print kMateriels;</xsl:processing-instruction></legend>
                    <xsl:variable name="doc_id_media"><xsl:value-of select="t_doc/DOC_ID_MEDIA"/></xsl:variable>
						<table width="80%"  border="0" align="center" cellspacing="0" class="tableResults">
							<tr>
								<td class="resultsHead"><xsl:processing-instruction name="php">print kReferenceMateriel;</xsl:processing-instruction></td>
								<td class="resultsHead"><xsl:processing-instruction name="php">print kFormat;</xsl:processing-instruction></td>
								<xsl:if test="not(contains($doc_id_media,'P'))">
								<td class="resultsHead"><xsl:processing-instruction name="php">print kTCin;</xsl:processing-instruction></td>
								<td class="resultsHead"><xsl:processing-instruction name="php">print kTCout;</xsl:processing-instruction></td>
								<!--<td class="resultsHead"><xsl:processing-instruction name="php">print kDuree;</xsl:processing-instruction></td>
								<td class="resultsHead"><xsl:processing-instruction name="php">print kLangue;</xsl:processing-instruction></td>-->
								</xsl:if>
							</tr>
							<xsl:for-each select="t_doc/t_doc_mat/DOC_MAT[t_mat/MAT_FORMAT!='']">
								<xsl:variable name="j"><xsl:number/></xsl:variable>
								<tr class="{concat('altern',$j mod 2)}">
									<td class="resultsCorps"><a href="index.php?urlaction=mat&amp;amp;id_mat={ID_MAT}"><xsl:value-of select="t_mat/MAT_NOM"/></a></td>
									<td class="resultsCorps"><xsl:value-of select="t_mat/MAT_FORMAT" /></td>
									<xsl:if test="not(contains($doc_id_media,'P'))">
										<td class="resultsCorps"><xsl:value-of select="DMAT_TCIN"/></td>
										<td class="resultsCorps"><xsl:value-of select="DMAT_TCOUT"/></td>
										<!--<td class="resultsCorps"><xsl:value-of select="DMAT_DUREE"/></td>
										<td class="resultsCorps"><xsl:value-of select="DMAT_ID_LANG"/></td>-->
									</xsl:if>
								 </tr>
							</xsl:for-each>
						</table>
						</fieldset>
					</xsl:if>

				</div>

				<div id="report" style="display:none">
					<xsl:for-each select="t_doc_freres">
						<xsl:if test="t_doc!=''">
							<fieldset class="ui-widget ui-widget-content ui-corner-all"  id="field_description">
								<legend><xsl:processing-instruction name="php">print kReportage;</xsl:processing-instruction> : <xsl:value-of select="titre"/></legend>
								<xsl:for-each select="t_doc">
									<xsl:variable name="j"><xsl:number/></xsl:variable>


									<xsl:variable name="vignette">
										<xsl:choose>
											<xsl:when test="string-length(IM_FICHIER)>3"><xsl:value-of select="concat($chemin_story,IM_CHEMIN,'/',IM_FICHIER)"/></xsl:when>
											<xsl:when test="string-length(DA_FICHIER)>3"><xsl:value-of select="concat($doc_story,DA_CHEMIN,DA_FICHIER)"/></xsl:when>
										</xsl:choose>
									</xsl:variable>
									<xsl:variable name="doc_titre">
										<xsl:call-template name="remove_html_tags">
											<xsl:with-param name="node" select="DOC_TITRE" />
										</xsl:call-template>
									</xsl:variable>

									<div class="resultsMos bloc_image" style="-o-text-overflow: ellipsis; text-overflow: ellipsis; height:auto">
										<xsl:if test="$vignette!=''">
											<div style="width:120px;height:90px;margin:auto">
												<img src="makeVignette.php?image={$vignette}&amp;w=120&amp;h=90&amp;kr=1" border="0" title="{$doc_titre}" />
											</div>
										</xsl:if>

										<div style="overflow: hidden; -o-text-overflow: ellipsis; text-overflow: ellipsis;">
											<a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}"><xsl:value-of select="DOC_COTE"/></a>
										</div>
										<div style="height:25px;overflow: hidden; -o-text-overflow: ellipsis; text-overflow: ellipsis;">
											<a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}" title="{$doc_titre}"><xsl:value-of select="DOC_TITRE"/></a>
										</div>
									</div>
								</xsl:for-each>
							</fieldset>
						</xsl:if>
					</xsl:for-each>
					&#160;
				</div>

				<div id="story" style="display:none">
					<div id='sidecommande' style="display:none">&#160;</div>
				</div>

				<div id="extraits" style="display:none">&#160;
					<xsl:if test="cart_list!=''">
						<select id='selectFolder' onchange='myPanel?myPanel.refreshContent():void(null)' >
							<xsl:for-each select="cart_list">
								<xsl:if test="PAN_DOSSIER!=1">
									<option value='{ID_PANIER}'>
										<xsl:if test="PAN_ID_ETAT=0"><xsl:value-of select="PAN_TITRE"/></xsl:if>
										<xsl:if test="PAN_ID_ETAT=1"><xsl:processing-instruction name="php">print kPanier;</xsl:processing-instruction></xsl:if>
									</option>
								</xsl:if>
							</xsl:for-each>
						</select>
						<xsl:processing-instruction name="php">if(isset($_GET['id_panier'])){</xsl:processing-instruction>
							<script>
								for ( i = 0 ; i &lt; document.getElementById('selectFolder').options.length ; i++){
									if(document.getElementById('selectFolder').options[i].value == '&lt;?= $_GET['id_panier'] ?&gt;') document.getElementById('selectFolder').options[i].selected = true;
								}
							</script>
						<xsl:processing-instruction name="php">}</xsl:processing-instruction>
					</xsl:if>



				</div>

				<xsl:if test="count(t_doc/t_doc_acc/t_doc_acc[ID_EXP='1'])>0">
				<div id="doc_acc" style="display:none">
					&#160;
					<xsl:if test="t_doc/t_doc_acc/t_doc_acc[ID_EXP='1']!=''">
						<table>
						<tr>
						<td class="resultsHead" width="100">&#160;</td>
						<td class="resultsHead"  width="100"><xsl:processing-instruction name="php">print kTitre;</xsl:processing-instruction></td>
						</tr>
						<xsl:for-each select="t_doc/t_doc_acc/t_doc_acc[ID_EXP='1']">
							<xsl:variable name="da_url">
								<xsl:choose>
									<xsl:when test="substring(DA_FICHIER,1,4)='http'">
										<xsl:value-of select="DA_FICHIER" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="concat('&lt;?php echo kDocumentUrl ?&gt;',DA_CHEMIN,DA_FICHIER)" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<tr>
								<td class="resultsCorps">
									<xsl:if test="(translate(substring-after(DA_FICHIER,'.'),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='gif' or translate(substring-after(DA_FICHIER,'.'),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='jpg' or translate(substring-after(DA_FICHIER,'.'),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='png' or translate(substring-after(DA_FICHIER,'.'),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='pdf') and substring(DA_FICHIER,1,4)!='http'">
										<a href="{$da_url}" target="_blank">
											<img src="{concat('makeVignette.php?h=90&amp;amp;w=120&amp;amp;kr=1&amp;amp;type=doc_acc&amp;amp;image=',DA_CHEMIN,DA_FICHIER)}" border="0" alt="vignette" />
										</a>
									</xsl:if>
								</td>
								<td class="resultsCorps">
									<a href="{$da_url}" target="_blank"><xsl:value-of select="DA_TITRE"/></a>
								</td>
							</tr>
						</xsl:for-each>
						</table>
					</xsl:if>
				</div>
				</xsl:if>

				<!--xsl:if test="t_doc/DOC_TRANSCRIPT!=''">
					<div id="transcription" style="display:none">
						<xsl:for-each select="t_doc/DOC_TRANSCRIPT/AudioDoc">
							<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
							<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
							<xsl:variable name="withaccent" select="'áàâäéèêëíìîïóòôöúùûü'" />
							<xsl:variable name="unaccent" select="'aaaaeeeeiiiioooouuuu'" />

							<form id="chercheForm" onsubmit="chercheText();return false;">
								<fieldset class="ui-corner-all ui-widget">
									<legend>&lt;?=kRecherche;?&gt;</legend>

									<input type="text" name="text" id="text" style="width:50%" />
									<button id="bChercher" type="submit" >search</button>
									&amp;nbsp;&amp;nbsp;
									<button id="bNextWord" type="button" onclick="goToNextWord()" >next</button>
								</fieldset>
							</form>
							<br />
							<div id="transcript">
								<xsl:for-each select="SegmentList/SpeechSegment">
									<div stime="{@stime}" etime="{@etime}" class="SpeechSegment">
										<xsl:for-each select="Word">
											<xsl:variable name="stime"><xsl:value-of select="number(@stime)"/></xsl:variable>
											<xsl:variable name="dur"><xsl:value-of select="number(@dur)"/></xsl:variable>
											<xsl:variable name="etime"><xsl:value-of select="$stime + $dur"/></xsl:variable>

											<span stime="{format-number($stime, '0.##')}" etime="{format-number($etime, '0.##')}" title="{translate(translate(normalize-space(.), $uppercase, $smallcase), $withaccent, $unaccent)}">
												<xsl:value-of select="."/>
											</span>
										</xsl:for-each>
									</div>
								</xsl:for-each>
							</div>
						</xsl:for-each>
						&#160;
					</div>
				</xsl:if-->

			<!--/div>
		</div-->


    </xsl:template>

</xsl:stylesheet>
