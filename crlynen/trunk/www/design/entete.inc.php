<?php

$myUsr=User::getInstance();
$myPage=Page::getInstance();
$link=$myPage->getName();

error_reporting(E_ERROR | E_PARSE);
// error_reporting(E_ALL);

if(strtoupper($_SESSION['langue'])=='AR')
{
	// si on est en arabe textarea de droite a gauche
	echo '<style type="text/css">';
	echo 'input, textarea';
	echo '{';
	echo 'direction:rtl;';
	echo '}';
	echo '</style>';
}
global $db;

// génération des flags de changement de langue dans la variable $langue_menu pour insertion dans l'entete un peu plus bas
$res = $db->Execute("select ID_LANG,LANG from t_lang");
$langue_menu = "" ;
while($row = $res->FetchRow()){
	if($row['ID_LANG'] == $_SESSION['langue']){
		$current_langue = $row['LANG'];
	}
	$langue_menu.="<a href=\"".$myPage->getName()."?langue=".$row["ID_LANG"]."\" ><img class='flag_chooser ".(($_SESSION['langue'] == $row['ID_LANG'])?'current':'')."' border='0' alt='".$row["LANG"]."'  title='".$row["LANG"]."' src='".designUrl."images/flag_".$row["ID_LANG"].".png'/></a>";

}
$res->Close();


?>


<div id="entete">
<div id="enteteCrlyne">
	<div id="header_logo_block" style="">
		<a href="https://www.paris.fr/" id="logo" class="rheader-logo">
			<img alt="logo opsomai" id="logo_opsomai" src="design/images/logo_ville_de_paris_blanc.png"  border="0" style="height:58px; border:none;"/>
			<img alt="logo opsomai" id="logo_opsomai_small" src="design/images/logo_ville_de_paris_blanc.png" border="0" style="height:67px; border:none;" />
		</a>
		<span id="headtiret"></span>
		<a href="http://www.cinemathequerobertlynen.paris"> 
			<img alt="logo opsomai" id="logo_opsomai" src="design/images/LogoLynen_blanc.png"  border="0" style="height:58px; border:none;margin-left: 38px;"/>
			<img alt="logo opsomai" id="logo_opsomai_small" src="design/images/LogoLynen_blanc.png" border="0" style="height:67px; border:none;margin-left: 38px;" />
		</a>
	</div>
	<div id="headRight">
		<div id="headRight" style="float: right;margin-right: 20%;color: #FFFFFF;">
			<h2 style="margin-bottom: 0px;margin-top: 6px; letter-spacing: 4px;">SCOPITONE</h2>
			<h4 style="margin: 0px;">CATALOGUE EN LIGNE</h4>
		</div>
	</div>
</div>
	<div id="header_top" class="headers">

		<div id="header_menu">
			<div id="header_quick_srch_block">
				<?php
				require_once(libDir."class_chercheSolr.php");

					if (!isset($mySearch) || !in_array($mySearch->sessVar,array('recherche_DOC','recherche_DOC_Solr'))){
						$mySearch=new RechercheSolr();
						$mySearch->getSearchFromSession();
					}
				require_once(libDir."class_formCherche.php");
				$xmlform=file_get_contents(getSiteFile("designDir","form/xml/cherche.xml"));




				//	Code lié à la gestion des multifields. si un critère a été ajouté, alors on a un compte_champs updaté qui est fourni,
				// la valeur de  compte_champs permet (apres remplacement dans le xml) de savoir combien de champs multifields on doit afficher,
				if (isset($_POST['compte_champs']) && !empty($_POST['compte_champs'])){
					// update valeur si compte_champs existe dans le post
					$_SESSION['compte_champs']=intval($_POST['compte_champs']);
				}else if (!isset($_SESSION['compte_champs'])){
					// si session[compte_champs] n'existe pas, on l'initialise, ici à 1
					$_SESSION['compte_champs']=1;
				}
				// remplacement dans le xml de la valeur nb_fields par la valeur de compte_champs (nb_fields régit le n)
				$xmlform=preg_replace('(<nb_fields>[0-9]+</nb_fields>)','<nb_fields>'.$_SESSION['compte_champs'].'</nb_fields>',$xmlform);

				$myForm=new FormCherche;
				$myForm->chercheObj=$mySearch;
				$myForm->entity="DOC";
				$myForm->classLabel="label_champs_form";
				$myForm->classValue="val_champs_form";
				$myForm->display($xmlform);
				?>
			</div>
			<div id="header_menus_block">
				<ul id="DesktopMenu">
					<? if($myUsr->loggedIn()){?>
					<li class="menu"><a href="index.php?urlaction=docListe"><?=kRecherche?></a></li>
					<?}?>
					<? if($myUsr->loggedIn()){?>
					<li class="menu" ><a onclick="togglePanBlock('full',true)" ><?=kPanier?></a></li>
					<?}?>
					<? if($myUsr->loggedIn() && $myUsr->getTypeLog()>=kLoggedGestionnaire){?>
						<li class="menu"><a href="index.php?urlaction=comListe&type_commande=5"><?=kBordereaux?></a></li>
					<?}?>
					<? if($myUsr->loggedIn() && $myUsr->getTypeLog()>=kLoggedInterne){?>
						<li class="menu"><a href="index.php?urlaction=importView"><?=kImports?></a></li>
					<?}?>
					<? if($myUsr->loggedIn() && $myUsr->getTypeLog()>=kLoggedInterne){ ?>
						<li class="menu"><a href="index.php?urlaction=admin"><?=kAdministration?></a></li>
					<?}?>
					<? if($myUsr->loggedIn() && ($myUsr->getTypeLog() == kLoggedStagiaire || $myUsr->getTypeLog() >= kLoggedInterne) && $myUsr->getTypeLog()<kLoggedInterne){ ?>
						<li class="menu"><a href="index.php?urlaction=docSaisie"><?=kCreerNotice?></a></li>
					<?}?>
					<?if($myUsr->loggedIn()){?>
						<li class="menu">
							<div id="menu_account" class="menu"><!-- <span class="login_block_icon login_icon"></span> --><?=kMonCompte?>
								<ul class="drop_down">
									<li class="noLink" style="font-weight:bold"><?=$myUsr->Nom." ".$myUsr->Prenom?></li>
									<li><a href="<?=$link?>?urlaction=inscription&amp;id_usager=<?=$myUsr->UserID?>&amp;edit=1"><?=kMonProfil?></a></li>
									<li><a href="index.php?urlaction=panierListe&tri=id_panier1" ><?=kMesCommandes?></a></li>
									<li><a href="index.php?urlaction=historique" ><?=kHistorique?></a></li>
									<!--li><a href="index.php?urlaction=histNav" ><?=kMaNavigation?></a></li-->
									<li><a href="index.php?deconnection" ><?=kDeconnexion?></a></li>
								</ul>
							</div>
						</li>
					<?}else{?>
						<li >
							<form  name='connexion' id='connect_form' method='post' action="index.php">
								<input type="text" id="login" name="login" placeholder="<?=kLogin?>" size="14" value="<?php
								if (isset($_POST["login"]) && !empty($_POST["login"]))
									echo $_POST["login"]
								?>" />
								<input type='password' id='password' name='password' placeholder="<?=kMotDePasse?>"  size='14' value='<?php
								if (isset($_POST["password"]) && !empty($_POST["password"]))
									echo $_POST["password"];
								?>' />
								<input name='refer' type='hidden' value='' />
								<input type='submit' name='bsubmitLogin' value='<?= kOk ?>' />
								<div class='error' style="display:none;"><? echo $myUsr->message; ?></div>
							</form>
							<script type="text/javascript">
								if($j("#connect_form div.error").html()!=''){
									$j("#connect_form div.error").click(function(){
										$j("#connect_form div.error").hide();
									});
									$j("#connect_form div.error").css('display','block');
								}
							</script>
						</li>
						<li class="menu"><a><?=kAide?></a>
							<ul id="login_actions"class="drop_down">
								<li><a class="login_link_signup" href="javascript:loadInscription();"  ><?= kPasEncoreInscrit ?></a></li> <!-- href="<?=$link?>?urlaction=inscription" -->
								<li><a class="login_link_forgotpwd" href="javascript:loadLoginDemande();" ><?= kMotDePasseOublie ?></a></li> <!-- href="<?=$link?>?urlaction=loginDemande" -->
							</ul>
						</li>

					<?}?>
					<!--li id="header_other_block" class="valign_helper">
						<?php // le block header_other_block est float : right, il est donc placé avant le bread_crumb pr alignement.
						print $langue_menu;?>
					</li-->
				</ul>
				<a title="Menu" href="#" class="navbar-toggle" id="bouton-menu-responsive" onclick="toggleMenuResponsive();">
  		 			<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
    			</a>
    			<div id="menu_responsive_wrapper" class="collapsed">
    				<ul id="ResponsiveMenu">
						<? if($myUsr->loggedIn()){?>
						<li class="menu"><a href="index.php?urlaction=docListe"><?=kRecherche?></a></li>
						<?}?>
						<? if($myUsr->loggedIn()){?>
						<li class="menu" ><a onclick="toggleMenuResponsive();togglePanBlock('full',true);" ><?=kPanier?></a></li>
						<?}?>
						<? if($myUsr->loggedIn() && $myUsr->getTypeLog()>kLoggedNorm){?>
							<li class="menu"><a href="index.php?urlaction=importView"><?=kImports?></a></li>
						<?}?>
						<? if($myUsr->loggedIn() && $myUsr->getTypeLog()>kLoggedNorm){ ?>
							<li class="menu"><a href="index.php?urlaction=admin"><?=kAdministration?></a></li>
						<?}?>
						<?if($myUsr->loggedIn()){?>
							<li class="menu" onclick="toggleSubMenuResponsive(this);">
								<div id="menu_account" class="menu"><?=kMonCompte?><!-- <span class="login_block_icon login_icon"></span> -->
								</div>
								<span id="menu_responsive_arrow">&nbsp;</span>
							</li>
							<li style="display:none">
									<ul class=""><!-- style="display:none" -->
										<li class="subMenu"><a href="<?=$link?>?urlaction=inscription&amp;id_usager=<?=$myUsr->UserID?>&amp;edit=1"><?=kMonProfil?></a></li>
										<li class="subMenu"><a href="index.php?urlaction=panierListe&tri=id_panier1" ><?=kMesCommandes?></a></li>
										<li class="subMenu"><a href="index.php?urlaction=historique" ><?=kMesRequetes?></a></li>
										<li class="subMenu"><a href="index.php?urlaction=histNav" ><?=kMaNavigation?></a></li>
										<li class="subMenu"><a href="index.php?deconnection" ><?=kDeconnexion?></a></li>
									</ul>
							</li>
						<?}else{?>
							<li >
								<form  name='connexion' id='connect_form' method='post' action="index.php">
									<input type="text" id="login" name="login" placeholder="<?=kLogin?>" size="14" value="<?php
									if (isset($_POST["login"]) && !empty($_POST["login"]))
										echo $_POST["login"]
									?>" autocapitalize="off" />
									<input type='password' id='password' name='password' placeholder="<?=kMotDePasse?>"  size='14' value='<?php
									if (isset($_POST["password"]) && !empty($_POST["password"]))
										echo $_POST["password"];
									?>' />
									<input name='refer' type='hidden' value='' />
									<input type='submit' name='bsubmitLogin' value='<?= kOk ?>' />
									<div class='error' style="display:none;"><? echo $myUsr->message; ?></div>
								</form>
								<script type="text/javascript">
									if($j("#connect_form div.error").html()!=''){
										$j("#connect_form div.error").click(function(){
											$j("#connect_form div.error").hide();
										});
										$j("#connect_form div.error").css('display','block');
									}
								</script>
							</li>
							<li class="menu" onclick="toggleSubMenuResponsive(this);"><a><?=kAide?></a>

							</li>
							<li style="display:none">
								<ul id="login_actions">
									<li class="subMenu"><a class="login_link_signup" href="javascript:loadInscription();"  ><?= kPasEncoreInscrit ?></a></li> <!-- href="<?=$link?>?urlaction=inscription" -->
									<li class="subMenu"><a class="login_link_forgotpwd" href="javascript:loadLoginDemande();" ><?= kMotDePasseOublie ?></a></li> <!-- href="<?=$link?>?urlaction=loginDemande" -->
								</ul>
							</li>

						<?}?>
						<li id="header_other_block" class="valign_helper">
							<?php // le block header_other_block est float : right, il est donc placé avant le bread_crumb pr alignement.
							print $langue_menu;?>
						</li>
					</ul>
    			</div>
		</div>
		</div>

		<div id="header_second_row" >
			<div id="header_slogan_opsis" style="position : relative;  ">
				<?= kSloganOpsis ; ?>
			</div>
			<div id="header_breadcrumb" >
				<a id="accueil_link" href="index.php"><?=kAccueil?></a><?
				//var_dump($_SESSION['breadcrumb']);
			if(isset($_SESSION['breadcrumb'][$_SERVER['SCRIPT_NAME']])){
				$arr_bc =$_SESSION['breadcrumb'][$_SERVER['SCRIPT_NAME']];
			}else{
				$arr_bc = Page::refererTab2BreadCrumb();
			}

			//flag affiche une seule fois l instance panier
			$panier_fil=0;
			foreach($arr_bc as $idx=>$bc_step){
				$bc_step_str = "" ;
				if(isset($bc_step['instances'][0]['str']) && !empty($bc_step['instances'][0]['str'])){
					$bc_step_str= ': '.$bc_step['instances'][0]['str'];
				}
				if(isset($bc_step['instances'][0]['url']) && !empty($bc_step['instances'][0]['url'])){
					$bc_step_url= $bc_step['instances'][0]['url'];
				}
				if(isset($bc_step['instances'][0]['titre']) && !empty($bc_step['instances'][0]['titre'])){
					$bc_step_titre= $bc_step['instances'][0]['titre'];
				}
				if(isset($bc_step['instances'][0]['id_panier']) && !empty($bc_step['instances'][0]['id_panier'])){
					$bc_step_panier= $bc_step['instances'][0]['id_panier'];
				}
				if(strpos($bc_step['key'],'_')!==false){
					$key = explode('_',$bc_step['key']);
					$key_type = $key[0];
					$key_id = $key[1];
				}else{
					$key_type = $bc_step['key'];
				}

				if(isset($bc_step_titre) && !empty($bc_step_titre)){
					if(!empty($bc_step_panier)){
						if($panier_fil==0){
							echo '<span class="breadcrumb_arrow"></span><a style="cursor:pointer;" onclick="togglePanBlock(\'full\');">'.kPanier.'</a>';
							$panier_fil=1;
						}
						echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.$bc_step_titre.'</a>';
					}
					else {
						switch($key_type){
							case 'docListe' :
								echo '<span class="breadcrumb_arrow"></span><a style="cursor:pointer;" onclick="removeFilter(0);">'.kRecherche;
								printSolrCriteres();
								echo '</a>';
								echo '<div id="refine_crits_wrapper"></div>';
							break ;
							default :
								if (strpos($bc_step_url, "urlaction=comListe&type_commande=5") > 0)
									echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kGestionBordereaux.'</a>';
								else
									echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.$bc_step_titre.'</a>';
						}
					}
				}else{
					if(empty($bc_step_panier)){
						switch($key_type){
							case 'admin' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=admin">'.kAdministration.'</a>';
							break ;
							case 'doc' :
								echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kDocument.$bc_step_str."</a>";
							break;
							case 'mat' :
								echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kMateriel.$bc_step_str."</a>";
							break;
							case 'lex' :
								echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kLexique.$bc_step_str."</a>";
							break;
							case 'val' :
								echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kValeur.$bc_step_str."</a>";
							break;
							case 'import' :
								echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kImport.$bc_step_str."</a>";
							break;
							case 'imageur' :
								echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kStoryboard.$bc_step_str."</a>";
							break;
							case 'tape' :
								echo '<span class="breadcrumb_arrow"></span><a href="'.$bc_step_url.'">'.kCartouche.$bc_step_str."</a>";
							break;
							case 'docListe' :
								echo '<span class="breadcrumb_arrow"></span><a style="cursor:pointer;" onclick="removeFilter(0);">'.kRecherche;
								printSolrCriteres();
								echo '</a>';
								echo '<div id="refine_crits_wrapper"></div>';
							break ;
							case 'matListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=matListe">'.kGestionMateriels.'</a>';
							break;
							case 'lexListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=lexListe">'.kGestionLexique.'</a>';
							break;
							case 'valListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=valListe">'.kGestionValeurs.'</a>';
							break;
							case 'usagerListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=usagerListe">'.kGestionUtilisateurs.'</a>';
							break;
							case 'importListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=importListe">'.kImports.'</a>';
							break;
							case 'imageurListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=importListe">'.kGestionStoryboards.'</a>';
							break;
							case 'tapeListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=tapeListe">'.kGestionSauvegardes.'</a>';
							break;
							case 'comListe' :
								echo '<span class="breadcrumb_arrow"></span><a href="index.php?urlaction=comListe">'.kGestionCommandes.'</a>';
							break;
						}
					}
				}
			}

		function printSolrCriteres(){
			if(isset($_SESSION['recherche_DOC_Solr']['form'])){
				$crit_actifs= '';
				$count = 0;
				foreach($_SESSION['recherche_DOC_Solr']['form'] as $idx=>$form){
					$form['VALEUR'] = htmlentities($form['VALEUR'], ENT_COMPAT | ENT_HTML401, 'UTF-8');
					if($form['VALEUR'] != ''){
						if($form['FIELD'] == 'doc_id_media'){
							if(!is_array($form['VALEUR'])){
								$form['VALEUR'] = array($form['VALEUR']);
							}
							foreach($form['VALEUR'] as &$val){
								if($val == 'V'){
									$val = kVideo;
								}else if($val == 'P'){
									$val = kPhoto;
								}else if($val == 'D'){
									$val = kDocument;
								}else if($val == 'R'){
									$val = kReportage;
								}else if($val == 'A'){
									$val = kAudio;
								}
							}
						}

						
						if ($form['TYPE'] == 'FT'){
							if(isset($form['LIB']) && !empty($form['LIB']) && ($form['LIB']!=kCritere)){
								$crit_actifs.= $form['LIB'];
							}else{
								$crit_actifs.= kTexte;
							}
							$crit_actifs.= ' : '.$form['VALEUR']; 	//'&nbsp;<a href="#" onclick="updateNbLignesForm();removeFacetSearch(\''.$form['FIELD'].'\');"><img alt="remove criteria" src="design/images/close.jpg" /></a>';
						} else if ($form['FIELD']=="doc_id_fonds" &&  $form['VALEUR'] != ''){
							switch($form['VALEUR']){
								case '1' :
									$crit_actifs.= $form['LIB'].' : '.kPhototheque;
								break ;
								case '2' :
									$crit_actifs.= $form['LIB'].' : '.kMultimedia;
								break ;
							}
						} else if ($form['FIELD']=="doc_id_type_doc" &&  $form['VALEUR'] != ''){
							$crit_actifs.= $form['LIB'];
						} else if ( is_array($form['VALEUR']) && !empty($form['VALEUR'])){

							$crit_actifs.= $form['LIB'].' : '.implode(',',$form['VALEUR']);
						} else if ($form['VALEUR'] != ''){
							$crit_actifs.= $form['LIB'].' : '.$form['VALEUR'];
						}


						$crit_actifs.=', ';

						$count  ++ ;
					}
				}
				if($crit_actifs!=''){
					$crit_actifs = substr($crit_actifs,0,-2);
					echo ' - '.$crit_actifs;
				}
			}
		}

		?>
		<script>
			form = getFormSolr();

			// récupération de l'objet depuis le champ refine du formulaire de recherche
			if(form != null && typeof form.refine != 'undefined' && form.refine.value!=""){
				obj_refine_solr = JSON.parse(form.refine.value);
			}else{
				obj_refine_solr=new Array();
			}

			// génération du fil d'ariane des critères de refine
			if($j("#refine_crits_wrapper").is(":last-child")){
				tokenize_refine_crits();
			}else{
				tokenize_refine_crits(true);
			}
			animateBreadcrumb() ;
		</script>
			</div>
		</div>
	</div>
</div>
	<?
		
	
		if(isset($_SESSION['USER']['layout_opts']['layout'])){
			$layout = $_SESSION['USER']['layout_opts']['layout'];
		}
		if(isset($_SESSION['USER']['layout_opts']['panListeRefToLastOrder'])){
			$panListeRefToLastOrder = $_SESSION['USER']['layout_opts']['panListeRefToLastOrder'];
		}
		// MS - Actuellement, dans le cas où on utilise les conf css designé pour des portables, 
		// On range automatiquement le menu gauche au chargement de la page si il est ouvert 
		// MAIS comme on travaille sur la largeur de l'affichage via JS, on ne peut faire ce repliage qu'après 
		// le rendering de la page, et on voit donc le menu se replier
		// on peut éventuellement modifier ceci pour cacher completement l'affichage si on arrive à 
		// déterminer en php qu'on est dans le cas où "les volets doivent tjs etre fermés au chargement dela page"
		
		
		?>


<script>

	var showLeftMenu = <?=($layout>=1 && $layout<3)?'true':'false';?>;
	var showRightMenu = <?=($layout>=2)?'true':'false';?>;
	var flagPagination = <?=($_SESSION['USER']['layout_opts']['docListe']['pagination']=='1')?'true':'false';?>;
	var panListeRefToLastOrder = <?=($panListeRefToLastOrder)?$panListeRefToLastOrder:0;?>;

	var veto_right_menu = false ;

	var layout = "<?=$layout?>";

	$j(document).ready(function(){

		// affichage du menu de Gauche en session (par layout) pour l'affichage en tablette
		/*if (layout === "1" && $j(window).width()<tablet_border) {
			$j('#main_block, #mainResultsBar, #importViewBar').css('left','194px');
		}*/
		if (showLeftMenu && $j(window).width()<mobil_border){
			changeLayout(0);
		}
		if (layout === "2" && $j(window).width()<tablet_border) {
			changeLayout(3);
		}

		// si le device est au max une tablette, on désactive l'affichage de preview en hover
		if ($j(window).width()<tablet_border && sessLayout.prev_pop_in){
			toggleFlagPrevPopin();
		}
	});



// Ces variables sont necessaires aux divers calculs d'affichage et sont updatés au cours des changements
if ($j(window).width() > mobil_border){
	var mos_unit_w = 210;
} else {
	var mos_unit_w = 190;
}
var mos_unit_h = 148;

var nb_mos_w;
var nb_mos_h;
var nb_mos_total;

var nb_lignes_deft = <?=(defined("gNbLignesDocListeDefaut")?gNbLignesDocListeDefaut:50) ?>;


// cette fonction gère la modification des affichages
function changeLayout(pres){
	 console.log("changeLayout");
	if(( showLeftMenu && (pres == '0' || pres == '3'))
      || !showLeftMenu&& (pres == '1' || pres == '2')){
		toggleLeftMenu("changeLayout");
	}
	if((!$j("#menuDroite").hasClass("collapsed") && (pres == '0' || pres == '1'))
      || $j("#menuDroite").hasClass("collapsed") && (pres == '2' || pres == '3')){
		toggleRightPreview("changeLayout");
	}

	$j("#main_block, #mainResultsBar").removeClass("pres_0 pres_1 pres_2 pres_3");
	$j("#main_block, #mainResultsBar").addClass("pres_"+pres);



	updateLayoutFlags();
}

// fonction de toggle de l'affichage du menu de gauche,
function toggleLeftMenu(from){
	//console.log("toggleLeftMenu");
	if ($j(window).width() > mobil_border) { //desktop et tablet
		//console.log("showLeftMenu desktop/tablet");
		if(showLeftMenu ){
			//fermeture du menu de gauche
			$j(function () {

				var widthMenuGauche = '';
				var leftMain = '';
				var leftPaddingFacet ='';

				// Variable de positons selon le type de device (Responsive)
				if ( $j(window).width() > tablet_border) {
					widthMenuGauche ='32px';//Desktop Layout
					leftMain = '33px';
					leftPaddingFacet = '8px';
				} else {
					widthMenuGauche ='56px';//Desktop Layout
					leftMain = '56px';
					leftPaddingFacet = '19px';
				}

				$j("#menuGauche .block_facet.open").slideUp('fast');
				$j("#menuGauche").addClass("collapsing"); // , #panGauche
				$j("#menuGauche").animate({					//, #panGauche
				   width: widthMenuGauche
				}, { duration: 200, queue: false });
				$j("#main_block, div.main_scrollableDiv.apply_layout").animate({ // #mainPan,
				   left: leftMain
				}, { duration: 200, queue: false });
				$j("#mainResultsBar, #importViewBar, div.main_scrollableDiv.apply_layout > .title_bar").animate({
				   left: leftMain
				}, { duration: 200, queue: false });
				$j("#menuGauche .type_facet").animate({
				   'padding-left': leftPaddingFacet
				}, { duration: 200, queue: false });
				showLeftMenu = false ;
				$j("#menuGauche").addClass("collapsed"); //, #panGauche
				$j("#menuGauche").removeClass("collapsing"); //, #panGauche
				if(typeof from == "undefined"){
					updateLayoutFlags();
				}
			});
		}else{
			//ouverure du menu de gauche
			$j("#menuGauche").addClass("expanding"); //, #panGauche
			$j("#menuGauche .block_facet.open").slideDown('fast');
			$j(function () {
				var widthMenuGauche = '';
				var leftMain = '';
				var leftPaddingFacet ='';

				// Variable de positons selon le type de device (Responsive)
				if ( $j(window).width() > tablet_border) {
					widthMenuGauche ='260px';//Desktop Layout
					leftMain = '261px';
					leftPaddingFacet = '40px';
				} else {
					widthMenuGauche ='193px'; // Tablet or Smaller Device Layout
					leftMain = '194px';
					leftPaddingFacet = '14px';
				}

				$j("#menuGauche").animate({ //, #panGauche
			    	width: widthMenuGauche
				}, { duration: 200, queue: false });
				$j("#main_block, div.main_scrollableDiv.apply_layout").animate({ // #mainPan,
				   left: leftMain
				}, { duration: 200, queue: false });
				$j("#mainResultsBar, #importViewBar, div.main_scrollableDiv.apply_layout > .title_bar").animate({
				   left: leftMain
				}, { duration: 200, queue: false });
				$j("#menuGauche .type_facet").animate({
				   'padding-left': leftPaddingFacet
				}, { duration: 200, queue: false });

				showLeftMenu = true;
				setTimeout(function(){
					$j("#menuGauche").removeClass("expanding"); //, #panGauche
				},100);
				if(typeof from == "undefined"){
					updateLayoutFlags();
				}
			});
			$j("#menuGauche").removeClass("collapsed"); //, #panGauche

			if ( $j(window).width() < tablet_border && $j(window).width() > mobil_border) {
				if ($j('#menuDroite').hasClass('collapsed') == false) {
					toggleRightPreview();
				}
			}

		}
	} else {
		//console.log("showLeftMenu mobile");
		// mobil device layout
		if(showLeftMenu ){
			//fermeture du menu de gauche
			$j(function () {
				$j("#menuGauche .block_facet.open").slideUp('fast');
				$j("#menuGauche").addClass("collapsing"); // , #panGauche
				$j("#menuGauche").animate({					//, #panGauche
				   width: '32px',
			       height: '56px'
				}, { duration: 200, queue: false });
				$j("#menuGauche #facet_wrapper").animate({					//, #panGauche
				   width: '0px'
				}, { duration: 200, queue: false });
				$j("#main_block, div.main_scrollableDiv.apply_layout").animate({ // #mainPan,
				   left: '0px'
				}, { duration: 200, queue: false });
				$j("#mainResultsBar, #importViewBar, div.main_scrollableDiv.apply_layout > .title_bar").animate({
				   left: '33px'
				}, { duration: 200, queue: false });
/*				$j("#menuGauche .type_facet").animate({
				   'padding-left': '8px'
				}, { duration: 200, queue: false });*/
				showLeftMenu = false ;
				$j("#menuGauche").addClass("collapsed"); //, #panGauche
				$j("#menuGauche").removeClass("collapsing"); //, #panGauche
				if(typeof from == "undefined"){
					updateLayoutFlags();
				}

			});
		}else{
			//ouverure du menu de gauche
			$j("#menuGauche").addClass("expanding"); //, #panGauche
			$j("#menuGauche .block_facet.open").slideDown('fast');
			$j(function () {
				var widthMenuGauche = '';
				var leftMain = '';
				var leftPaddingFacet ='';

				// Variable de positons selon le type de device (Responsive)

					widthMenuGauche ='193px'; // Tablet or Smaller Device Layout
					leftMain = '194px';
					leftPaddingFacet = '14px';

				$j("#menuGauche").animate({ //, #panGauche
				    	height: '100%'
				}, { duration: 200, queue: false });
				$j("#menuGauche,#menuGauche #facet_wrapper").animate({ //, #panGauche
				    	width: widthMenuGauche
				}, { duration: 200, queue: false });
				$j("#main_block, div.main_scrollableDiv.apply_layout").animate({ // #mainPan,
				   left: leftMain
				}, { duration: 200, queue: false });
				$j("#mainResultsBar, #importViewBar, div.main_scrollableDiv.apply_layout > .title_bar").animate({
				   left: leftMain
				}, { duration: 200, queue: false });
				$j("#menuGauche .type_facet").animate({
				   'padding-left': leftPaddingFacet
				}, { duration: 200, queue: false });

				showLeftMenu = true;
				setTimeout(function(){
					$j("#menuGauche").removeClass("expanding"); //, #panGauche
				},100);
				if(typeof from == "undefined"){
					updateLayoutFlags();
				}
			});
			$j("#menuGauche").removeClass("collapsed"); //, #panGauche
		}
	}
	setTimeout(function(){
		try{
			$j("#menuGauche,#facet_wrapper").css("width","");
			$j("#menuGauche").css("height","");
			$j("#main_block,#mainResultsBar,  #importViewBar, div.main_scrollableDiv.apply_layout, div.main_scrollableDiv.apply_layout > .title_bar").css("left","");
			$j("#menuGauche .type_facet").css("padding-left","");
			//console.log("toggleLeftMenu test unset left : "+$j("#main_block").css('left')+" "+$j("#main_block").css('left'));
			var event = document.createEvent("HTMLEvents");
			event.initEvent("resize",true,false);
			window.dispatchEvent(event);
		}catch(e){
			console.log("failed to dispatch resize event : "+e);
		}

	    // $j(".notice_results").css("width", "calc(100% - 476px)");
	},215);
}


// fonction de toggle de l'affichage du preview de droite,
function toggleRightPreview(from){
	// console.log("toggleRightPreview");
	if(!veto_right_menu ){
		if(!$j("#menuDroite").hasClass("collapsed")){

			var widthMenuGauche = '';
			var leftMain = '';
			var leftPaddingFacet ='';

			// Variable de positons selon le type de device (Responsive)
			if ( $j(window).width() > tablet_border) {
				widthMenuDroite ='32px';//Desktop Layout
				rightMain = '33px';
			} else {
				widthMenuDroite ='56px';//tablette ou mobile Layout
				rightMain = '57px';
			}

			$j(function () {
				$j("#menuDroite #prevMedia audio, #menuDroite #prevMedia video").each(function( idx,elt){
					$j(elt).get(0).pause();
				});
				$j("#menuDroite").addClass("collapsing");
				$j("#menuDroite").animate({
				   width: widthMenuDroite
				}, { duration: 200, queue: false });
				$j("#main_block, #panBlock").animate({
				   right: rightMain
				}, { duration: 200, queue: false });
				$j("#mainResultsBar").animate({
				   right: rightMain
				}, { duration: 200, queue: false });

				showRightMenu = false ;
				$j("#menuDroite").addClass("collapsed");
				$j("#menuDroite").removeClass("collapsing");
				if(typeof from == "undefined"){
					updateLayoutFlags();
				}

				if ($j("#notice_pan_result").css('display','none')) {
					$j("#notice_pan_result").css('display','block');
				}
			});
		}else{
			$j("#menuDroite").addClass("expanding");
			$j(function () {
				$j("#menuDroite").animate({
				   width: '320px'
				}, { duration: 200, queue: false });
				$j("#main_block, #panBlock").animate({
				   right: '321px'
				}, { duration: 200, queue: false });
				$j("#mainResultsBar").animate({
				   right: '321px'
				}, { duration: 200, queue: false });

				showRightMenu = true;
				$j("#menuDroite").removeClass("expanding");
				if(typeof from == "undefined"){
					updateLayoutFlags();
				}
			});
			$j("#menuDroite").removeClass("collapsed");

			if ( $j(window).width() < tablet_border && $j(window).width()>=mobil_border) {
				if ($j('#menuGauche').hasClass('collapsed') == false) {
					toggleLeftMenu();
				}
			}

		}
	}
	setTimeout(function(){
		try{
			$j("#menuDroite").css("width","");
			$j("#main_block, #panBlock,#mainResultsBar").css("right","");

			var event = document.createEvent("HTMLEvents");
			event.initEvent("resize",true,false);
			window.dispatchEvent(event);

		}catch(e){
			console.log("failed to dispatch resize event : "+e);
		}
	    // $j(".notice_results").css("width", "calc(100% - 476px)");
	},215);
	setTimeout(function(){
		if (parseInt($j("#mainPan .title_bar").css("width")) - parseInt($j("#mainPan .toolbar").css("width")) < 77 ){ // si le champ qui affiche le panier et ses éléments est trop petit -> on l'escamote
			$j("#notice_pan_result").css('display','none');
		} else {
	    	$j("#notice_pan_result").css("width", "calc(100% - 390px)");
		}
    },215);
}

function togglePanBlock(target,allowRedirect){

	// console.log("togglePanBlock");
	if(typeof allowRedirect !='undefined' && allowRedirect  && $j("#panBlock").length == 0 ){
		window.location.href="index.php?urlaction=docListe&showPanier";
	}

	if(!$j("#panBlock").hasClass("collapsed") && ( $j("#panBlock").hasClass('full') || (typeof target == 'undefined' || target != 'full'))){
		if($j("#panBlock").hasClass('full') && $j("#panBlock").hasClass('montage')){
			try{
				if(panHasChanged && confirmExit){
					if(!confirm(str_lang.kJSConfirmExit)){
						return false ;
					}else{
						window.onbeforeunload = null ;
					}
				}
			}catch(e){}
			retourSel();
		}
		$j(function () {
			$j("#panBlock").removeClass('full').addClass("collapsing");
			$j("#mainResultsBar .toolbar").removeClass('hidden');
			$j("#panBlock").animate({
			   height: '4px'
			}, { duration: 200, queue: false });
			$j("#panBlock .toolbar").animate({
			   opacity: '0'
			}, { duration: 200, queue: false });
			$j("#mainResultsBar .toolbar").animate({
			   opacity: '1'
			}, { duration: 200, queue: false });


			// showLeftMenu = false ;
			setTimeout(function(){
				$j("#panBlock").removeClass("expanded");
				$j("#panBlock").addClass("collapsed");
				$j("#panBlock").removeClass("collapsing");
				// cet updateLayoutFlags permet d'update le layout_opt panState, mais je ne suis pas sur qu'il soit encore utilisé (devait à l'origine assurer le maintien de l'état du panier après un reload eventuel => à vérifier)
				// updateLayoutFlags();
			},201);


		});
	}else{
		$j("#panBlock").addClass("expanding");
		$j(function () {
			if(typeof target != 'undefined' && target == 'full'){

				$j("#panBlock").animate({
				   height: '94%'
				}, { duration: 200, queue: false });
				$j("#panBlock #mainPan .title_bar").animate({
				   height: '22px',
				   marginTop : '0px',
				   padding : '7px 25px 5px 40px'
				}, {
					duration: 200,
					queue: false,
					complete : function(){
						$j("#panBlock #mainPan .title_bar").css('height','').css('margin-top','').css('padding','');
					}});
				$j("#panBlock .toolbar").animate({
				   opacity: '1'
				}, { duration: 200, queue: false });
				$j("#mainResultsBar .toolbar").animate({
				   opacity: '0'
				}, { duration: 200, queue: false });
				setTimeout(function(){
					$j("#panBlock").removeClass('expanding').removeClass('expanded').addClass("full");
					$j("#mainResultsBar .toolbar").addClass('hidden');
				},201);
			}else{
				$j("#panBlock").animate({
				   height: '215px'
				}, { duration: 200, queue: false });
				setTimeout(function(){
					$j("#panBlock").removeClass("expanding").addClass("expanded");
				},201);
			}

			setTimeout(function(){
				// cet updateLayoutFlags permet d'update le layout_opt panState, mais je ne suis pas sur qu'il soit encore utilisé (devait à l'origine assurer le maintien de l'état du panier après un reload eventuel => à vérifier)
				// updateLayoutFlags();
			},201);
		});
		$j("#panBlock").removeClass("collapsed");

		if($j("#panBlock div.mosWrapper").length>0){
			adjustMosaique();
		}

	}
	// event.stopPropagation();
}

// cette fonction gère le toggle du mode preview pop-in
function toggleFlagPagination(){
	if(!$j(".toggle_pagination.tool").hasClass("disabled")){
		if($j(".toggle_pagination.tool").hasClass("selected")){
			$j(".toggle_pagination.tool").removeClass("selected");
			flagPagination = 0 ;
		}else{
			$j(".toggle_pagination.tool").addClass("selected");
			flagPagination = 1 ;
		}
		updateLayoutOpts({'docListe' : {'pagination' : flagPagination}},function(){$j(".layout_chooser.selected").click()});
		// updateLayoutFlags(function(){$j(".layout_chooser.selected").click()});
	}
}

function toggleFlagPrevPopin(){
	if(!$j(".toggle_preview.tool").hasClass("disabled")){
		if($j(".toggle_preview.tool").hasClass("selected")){
			$j(".toggle_preview.tool").removeClass("selected");
			flagPreviewPopin = 0 ;
		}else{
			$j(".toggle_preview.tool").addClass("selected");
			flagPreviewPopin = 1 ;
		}
		updateLayoutOpts({'prev_pop_in' : flagPreviewPopin});
	}
}

// cette fonction met à jour l'état d'affichage des icones de changement de présentation
function updateLayoutFlags(callback){
	$j(".display_opt span").removeClass("selected").removeClass("disabled");
	//console.log("updateLayoutFlags");
	//console.log("updateLayoutFlags - showLeftMenu : "+showLeftMenu+" "+typeof showLeftMenu);
	//console.log("updateLayoutFlags - showRightMenu : "+showRightMenu+" "+typeof showRightMenu);
	if(showLeftMenu && showRightMenu){
		$j("#side2_icn").addClass("selected");
		current_layout = 2;
	}else if(showLeftMenu && !showRightMenu){
		$j("#side1_icn").addClass("selected");
		current_layout = 1;
	}else if(!showLeftMenu && showRightMenu){
		$j("#side3_icn").addClass("selected");
		current_layout = 3;
	}else if(!showLeftMenu && !showRightMenu){
		$j("#side0_icn").addClass("selected");
		current_layout = 0;
	}


	
	
	ajax_url = "updateSessLayout.php?layout="+current_layout;
	// console.log(ajax_url);
	/*if(typeof id_current_panier != 'undefined' && id_current_panier != null){
		ajax_url += "&id_current_panier="+id_current_panier;
	}*/

	
	var old_class = "" ;
	if($j("*[class*='pres_']").length > 0 ){
		$j.each($j("*[class*='pres_']").eq(0).attr('class').split(' '),function(idx,val){
			if(val.indexOf('pres_') === 0 ){
				old_class = val ; 
			}
		});
	}
	
	$j("*[class*='pres_']").each(function(idx,elt){
		$j(elt).removeClass(old_class).addClass('pres_'+current_layout);
	});

	$j.ajax({
		url : ajax_url,
		success : function(){
			try{
			if(typeof callback == 'function'){
				callback();
			}
			}catch(e){
				console.log("updateLayoutFlags - crash call to callback :"+e);
			}
		}
	});
}



// cette fonction permet d'estimer les différentes valeurs necessaires au remplissage dynamique de la mosaique
function calculateNbLignes(){
	if($j("#main_block").length>0){
		full_w = $j("#main_block").width();
		full_h = $j("#main_block").height();

		if(full_w<100){
			full_w = $j(window).width() - 500;
		}
		if(full_h < 100 ){
			full_h = $j(window).height() - 500
		}


		nb_mos_w = Math.floor(full_w/mos_unit_w);
		nb_mos_h = Math.floor(full_h/mos_unit_h);

		if(nb_mos_w<=0){
			nb_mos_w = 1;
		}
		if(nb_mos_h<=0){
			nb_mos_h = 1;
		}

		nb_mos_total = (nb_mos_w*nb_mos_h);

		return nb_mos_total;
	}
}

//cette fonction permet de reinitialiser le nombre optimal de résultats souhaités lors d'un lancement d'une nouvelle recherche en mode mosaique
function updateNbLignesForm(){
	if(typeof sessLayout == 'undefined' || sessLayout == null || sessLayout.res_affMode == 'mos'){
		nbLignes = calculateNbLignes();

	// if(nbLignes){
		nbLignes = nbLignes*10;
		if($j("#quick_search input#nbLignes").length==0){
			$j("#quick_search ").append('<input type="hidden" name="nbLignes" id="nbLignes" label="" value="">');
		}
		if($j("#chercheForm input#nbLignes").length==0){
			$j("#chercheForm ").append('<input type="hidden" name="nbLignes" id="nbLignes" label="" value="">');
		}
		$j("#quick_search input#nbLignes, #chercheForm input#nbLignes").val(nbLignes);
		return nbLignes ;
	}else{
		// default value pr l'instant :
		return nb_lignes_deft;
	}
}



function toggleAdvFormWrapper(){

	if($j("#adv_search_wrapper").hasClass("collapsed")){
		$j("#adv_search_button").addClass("active");
		$j("#bouton-menu-responsive").css("z-index","50");

		$j("#conteneur").addClass("disable_transition");

		$j("#adv_search_wrapper").removeClass("collapsed"); // remove etat collapsed
		$j("#adv_search_wrapper").css({"display":"block","visibility":"hidden","height":"auto"}); //
		tmpH = $j("#adv_search_wrapper").css("height");											//recup height
		$j("#adv_search_wrapper").css({"display":"","visibility":"","height":"0px"});				//

		$j("#adv_search_wrapper").animate({
		   height: tmpH
		}, {
			duration: 100,
			queue: false,
			complete : function(){
				$j("#adv_search_wrapper").css("height","auto");
			}
		});
		setTimeout( function(){
			$j("#adv_search_wrapper").css("height","auto");
			$j("#conteneur").addClass("disabled");
		},101);
		$j("#adv_search_wrapper  :input").attr("disabled",false);
	}else{
		$j("#conteneur").addClass("disable_transition");
		$j("#conteneur").removeClass("disabled");
		$j("#adv_search_wrapper").animate({
		   height: '0px'
		}, { duration: 100, queue: false });


		setTimeout( function(){
			$j("#adv_search_button").removeClass("active");
			$j("#conteneur").removeClass("disable_transition");
			$j("#bouton-menu-responsive").css("z-index","57");
		},100);
		$j("#adv_search_wrapper").addClass("collapsed");
		$j("#adv_search_wrapper :input").attr("disabled",true);
	}
}

var conf_popin = {
	loading_parent : "div#popupModal",
	result_element : "div#frameModal"
}

function toggleMenuResponsive(){

	if($j("#menu_responsive_wrapper").hasClass("collapsed")){
		$j("#bouton-menu-responsive").addClass("active");
		$j("#adv_search_button").css("z-index","50");

		$j("#conteneur").addClass("disable_transition");

		$j("#menu_responsive_wrapper").removeClass("collapsed"); // remove etat collapsed
		$j("#menu_responsive_wrapper").css({"display":"block","visibility":"hidden","height":"auto"}); //
		//$j("#bouton-menu-responsive").css({"display":"block","visibility":"hidden","height":"auto"}); //
		tmpH = $j("#menu_responsive_wrapper").css("height");											//recup height
		$j("#menu_responsive_wrapper").css({"display":"","visibility":"visible","height":"0px"});				//
		//$j("#chercheForm").css({"opacity":""});

		$j("#menu_responsive_wrapper").animate({
		   height: tmpH
		}, {
			duration: 100,
			queue: false,
			complete : function(){
				$j("#menu_responsive_wrapper").css("height","auto");
			}
		});
		$j("#menu_responsive_wrapper").animate({
		   opacity : 1
		}, { duration: 100, queue: false });
		setTimeout( function(){
			$j("#menu_responsive_wrapper").css("height","auto");
			$j("#conteneur").addClass("disabled");
		},101);
		$j("#menu_responsive_wrapper  :input").attr("disabled",false);
	}else{
		$j("#menu_responsive_wrapper").css({"visibility":"hidden"});


		if($j("#ResponsiveMenu ul").css("display") ==="block") { //si le sous menu de l'onglet "Mon Compte" est ouvert, on le referme aussi pour une future ouverture du menu
			$j("#ResponsiveMenu li.open").click();
		}
		$j("#conteneur").addClass("disable_transition");
		$j("#conteneur").removeClass("disabled");
		$j("#menu_responsive_wrapper").animate({
		   height: '0px'
		}, { duration: 100, queue: false });
		$j("#menu_responsive_wrapper").animate({
		   opacity : 0
		}, { duration: 100, queue: false });

		setTimeout( function(){
			$j("#bouton-menu-responsive").removeClass("active");
			$j("#conteneur").removeClass("disable_transition");
			$j("#adv_search_button").css("z-index","57");
		},100);
		$j("#menu_responsive_wrapper").addClass("collapsed");
		$j("#menu_responsive_wrapper :input").attr("disabled",true);
	}
}

function toggleSubMenuResponsive(elt){
	if($j(elt).hasClass("open")){
		$j(elt).removeClass("open");

		$j(elt).next("li").css("display","none");
	}else{
		$j(elt).next("li").css("display","block");
		$j("#ResponsiveMenu #menu_account ul").css("display","block");
		$j(elt).addClass("open");
	}
}

function loadLoginDemande(){
	$j("#popupModal").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=loginDemande&where=include&fromAjax',
		success : function(data){
			$j("#popupModal").css('display','block');
			$j("#frameModal").addClass('gate_popin');
			$j("#frameModal").html(data);
			$j("#popupModal").removeClass('loading');
			initSubmitToAjax("form#loginDemande_form",chkFormReinitPw,conf_popin);
		}
	});
}

function loadInscription(){
	$j("#popupModal").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=inscription&where=include',
		success : function(data){
			$j("#popupModal").css('display','block');
			$j("#frameModal").addClass('gate_popin');
			$j("#frameModal").css('width', "700px");
			$j("#frameModal").html(data);
			$j("#popupModal").removeClass('loading');
			initSubmitToAjax("form#inscription_form",chkFormInscription,conf_popin);
		}
	});
}

function loadResetPassword(){
	$j("#popupModal").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=inscription&where=include&fromAjax&resetPassword=<?=$_GET['resetPassword']?>',
		success : function(data){
			$j("#popupModal").css('display','block');
			$j("#frameModal").addClass('gate_popin');
			$j("#frameModal").html(data);
			$j("#popupModal").removeClass('loading');
			initSubmitToAjax("form#resetPassword_form",checkPwFields ,conf_popin);
		}
	});
}

function loadLoginValidate(){
	$j("#popupModal").addClass('loading');
	$j.ajax({
		url : 'empty.php?urlaction=loginValidate&where=include&id=<?=$_GET['loginValidate']?>',
		success : function(data){
			$j("#popupModal").css('display','block');
			$j("#frameModal").addClass('gate_popin');
			$j("#frameModal").html(data);
			$j("#popupModal").removeClass('loading');
			initSubmitToAjax("form#loginDemande_form",chkFormFields,conf_popin);
		}
	});
}


function returnFromAjaxPopin(){
	closePopup(true);
}


<?if(isset($_GET['resetPassword'])){
	echo "loadResetPassword();\n";
}else if (isset($_GET['loginValidate']) || (isset($_GET['urlaction']) && $_GET['urlaction'] == 'loginValidate' && isset($_GET['id']))){
	echo "loadLoginValidate();\n";
}else{
	echo "\$j('.gate_popin_corps').removeClass('loading');\n";
}?>



</script>


<form name="formRebond" id="formRebond" method="post" action="?urlaction=docListe&amp;cherche=1">
<input type='hidden' name='F_doc_form' value="0" />
<input type="hidden" id="chFields[100]" name="chFields[100]" value="" />
<input type="hidden" id="chValues[100]" name="chValues[100]" value="" />
<input type="hidden" name="chOps[100]" value="AND" />
<input type="hidden" id="chLibs[100]" name="chLibs[100]" value="" />
<input type="hidden" id="chTypes[100]" name="chTypes[100]" value="" />
</form>


