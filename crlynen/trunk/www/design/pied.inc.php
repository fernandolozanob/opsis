<!-- PIED DE PAGE -->
<?
if((!isset($_COOKIE['ops_accept_cookie']) && !isset($_SESSION['cookie_implicit_auth']))){
	$_SESSION['cookie_implicit_auth'] = 1;
	?>
	<div id="msg_info_cookie_box">
		<div id="msg_info_cookie_txt">
			<?=kMsgInfoCookies ?>
			<br/>
			<div class="msg_info_buttons">
			<button class="std_button_style" onclick="$j('#msg_info_cookie_box').hide();"><?=kOk?></button>
			<button class="std_button_style" onclick="showCGU('cookies');"><?=kPlusInformations?></button>
			</div>
		</div>
	</div>
	<?
}else if (isset($_SESSION['cookie_implicit_auth']) && $_SESSION['cookie_implicit_auth'] == 1){
	setcookie('ops_accept_cookie','true',strtotime('+5 years'));
	unset($_SESSION['cookie_implicit_auth']);
}?>
<div id="footer">
	<span id="footerlogo">
		<!--a href="https://www.paris.fr/"> <img src="design/images/logo-MDP-cadre_couleur.png" style="width: 178px;margin-bottom:9px"/></a>  
		<a href="http://www.cinemathequerobertlynen.paris"> <img src="design/images/LogoLynen_couleur.png" style="height: 40px;"/></a-->
	</span>
	<span id="CenterFooter">
		
	</span>
	<span id="footerright">
		<a href="https://fr-fr.facebook.com/cinemathequerobertlynen/" target="_blank"><img src="design/images/share_facebook.png"/></a>
		<a href=""  onclick="showPopin('empty.php?include=contact'); return false;"> > Contact</a>  <a href="javascript:showPop_inCGU(2);"> > <?=kMentionsLegales?></a>  <a href="javascript:showPop_inCGU(3);"> > <?=kPresentation ?></a>
	</span>
</div>
<?php
	if(isset($_POST['EnvoyerContact']) == true){
		$nom=stripslashes(htmlspecialchars($_POST["nom"]));
		$qualit=stripslashes(htmlspecialchars($_POST["qualit"]));
		$email=stripslashes(htmlspecialchars($_POST["email"]));
		$structure=stripslashes(htmlspecialchars($_POST["structure"]));
		$votrequestion=stripslashes(htmlspecialchars($_POST["votrequestion"]));
		
		if($_POST["structure"] == ""){
			$message = $nom."\n".$qualit."\n".$email."\n".$votrequestion;
		}else{
			$message = $nom."\n".$qualit."\n".$email."\n".$structure."\n".$votrequestion;
		}
		//echo gMailContact;
		mail(gMailContact ,"Nouveau message de client",$message);
		echo "<script>alert('Votre message a bien été envoyé')</script>";
	}

?>