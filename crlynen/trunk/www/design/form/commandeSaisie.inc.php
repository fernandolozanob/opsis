<script type="text/javascript">
	var formChanged = false;

	function selectAll(tf){

		checkedItems=document.getElementsByTagName('input');
		for (i=0;i<checkedItems.length;i++) {
			if (checkedItems[i].type=='checkbox' && checkedItems[i]!=tf ) {
				checkedItems[i].checked=tf.checked;
			}
		}
	}

	function refresh() {
		document.form1.commande.value="SAVE";
		document.form1.submit();
	}

	function livraison(id_job) {
		document.form1.commande.value="LIVRAISON";
		document.form1.action="index.php?urlaction=commandeSaisie&id_panier=<?=$myPanier->t_panier['ID_PANIER'];?>";
		if(id_job) document.form1.action+='&id_job='+id_job;
		if(document.getElementById('livraison')) document.getElementById('livraison').innerHTML='<?= kMsgPreparationCommande ?>';
		submitForm();
	}

	function livraisonLigne(i) {
		document.form1.commande.value="LIVRAISON_LIGNE";
		document.form1.action="index.php?urlaction=commandeSaisie&id_panier=<?=$myPanier->t_panier['ID_PANIER'];?>";
		document.form1.ligne.value=i;
		if (document.getElementById('livraison'))
			document.getElementById('livraison').innerHTML='<h2><?=kMsgPreparationCommande;?></h2>';
		document.form1.submit();
	}

    function livraison_job(myform){
        if($j('#panxml_id_proc').val() >= 190 && $j('#panxml_id_proc').val() < 200)
            myform.commande.value='LIVRAISON_DVD';
        else	
            myform.commande.value='LIVRAISON_JOB';
        
        myform.submit();
    }

	function commander(type) {
		document.form1.commande.value="INIT";
		document.form1.pan_id_type_commande.value=type;
		if (document.form1.id_panier.value!='_session') 
			document.form1.action="?urlaction=commande<xsl:value-of select='$urlparams'/>";
			else 
			document.form1.action="<xsl:value-of select='$scripturl'/>?urlaction=inscription&amp;type_commande="+type;
			document.form1.submit();
	}

	function updateMatLignes(obj){
		support=obj.options[obj.selectedIndex].text;
		if(support=="MPEG4") pattern="vis";
		else pattern="csv";
		selectItems=document.getElementsByTagName('select');
		for (i=0;i<selectItems.length;i++) {
		 if (selectItems[i].name.indexOf('ID_MAT')>0 ) {
			 opts=selectItems[i].options;
			 for (j=0;j<opts.length;j++) {
				 val=opts[j].value.toLowerCase();
				if(val.indexOf(pattern) > 0 && ((pattern=='csv' && val.indexOf('dvd')==-1) || pattern=='dvd') && val.indexOf('vis')==-1) opts.selectedIndex=j;
				if(pattern=='vis' && val.indexOf('mp4')>0) opts.selectedIndex=j;
			 }
		 }
		}
	 
	}

    function updateAllIdProcLignes(main_select,media_type){
        
        if($j(main_select).val() >=190 && $j(main_select).val() < 200){
            $j('#panxml_id_proc').val($j(main_select).val()); // Encodage DVD
            $j(".id_proc_lignes.media_type_"+media_type).each(function(idx,elt){
                                                              $j(elt).val("");
                                                              });
        } else {
            $j('#panxml_id_proc').val(''); // Encodage DVD
            $j("select.id_proc_lignes.media_type_"+media_type).each(function(idx,elt){
                                                          $j(elt).val($j(main_select).val());
                                                          });
        }
        $j("select.id_proc_lignes.media_type_"+media_type).each(function(idx,elt){
                                                          copyToSupportLiv($j(elt).eq(0));
                                                          });
    }

    function copyToSupportLiv(elt){
        $j(elt).parent().parent().find("input.copy_pdoc_support_liv").val($j(elt).val());
    }

    function updateAllIdProcFromSupportLiv(){
        $j("select.id_proc_lignes").each(function(idx,elt){
                                                          $j(elt).val($j(elt).parent().parent().find("input.copy_pdoc_support_liv").val());
                                                          });
    }
	
	function addExemplaireAJAX(str) { // Récupération du XML
		xmlMat=importXML(str);

		myBalise=xmlMat.getElementsByTagName('ID_MAT');
		var id_mat=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild

		myBalise=xmlMat.getElementsByTagName('MAT_NOM');
		if (myBalise[0].firstChild)
		var mat_nom=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
		else var mat_nom='';

		myBalise=xmlMat.getElementsByTagName('MAT_FORMAT');
		if (myBalise[0].firstChild)
		var mat_format=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
		else var mat_format='';

		myBalise=xmlMat.getElementsByTagName('MAT_TCIN');
		if (myBalise[0].firstChild)
		var mat_tcin=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
		else var mat_tcin ='';

		myBalise=xmlMat.getElementsByTagName('MAT_TCOUT');
		if (myBalise[0].firstChild)
		var mat_tcout=myBalise[0].firstChild.nodeValue; //ne PAS oublier le firstChild
		else var mat_tcout ='';
		
		//alert($j(str).find("ID_DOC").html());
		if ($j(str).find("ID_DOC").html() != undefined)
			var id_doc = $j(str).find("ID_DOC").html().replace("<!--[CDATA[", "").replace("]]-->", "");
		if ($j(str).find("DOC_COTE").html() != undefined)
			var doc_cote = $j(str).find("DOC_COTE").html().replace("<!--[CDATA[", "").replace("]]-->", "");
		if ($j(str).find("DOC_TITRE").html() != undefined)
			var doc_titre = $j(str).find("DOC_TITRE").html().replace("<!--[CDATA[", "").replace("]]-->", "");
		if ($j(str).find("DOC_DUREE").html() != undefined)
			var doc_duree = $j(str).find("DOC_DUREE").html().replace("<!--[CDATA[", "").replace("]]-->", "");
		if ($j(str).find("DOC_XML").html() != undefined)
			var fonds = $j($j(str).find("DOC_XML").html()).find("FONDS").html();
		if ($j("<xml>"+$j(str).find("t_val[ID_TYPE_VAL='SUP']").html()+"</xml>").find("VALEUR").html() != undefined)
			var sup = $j("<xml>"+$j(str).find("t_val[ID_TYPE_VAL='SUP']").html()+"</xml>").find("VALEUR").html().replace("<!--[CDATA[", "").replace("]]-->", "");
		if ($j("<xml>"+$j(str).find("t_val[ID_TYPE_VAL='ETAT']").html()+"</xml>").find("VALEUR").html() != undefined)
			var etat = $j("<xml>"+$j(str).find("t_val[ID_TYPE_VAL='ETAT']").html()+"</xml>").find("VALEUR").html().replace("<!--[CDATA[", "").replace("]]-->", "");

		myTab=document.getElementById('t_panier_doc$tab');

		for(i=0;i<myTab.childNodes.length;i++) { //vérif de la limite de DOC / matériel et des doublons
			chld=myTab.childNodes[i];

			if (chld.id=='t_panier_doc$DIV$'+id_mat) {
				alert(str_lang.kDOMnodouble); return false;
			}
		}

		$urlmat="index.php?urlaction=matSaisie&id_mat="+id_mat;

		arrRow=new Array('t_panier_doc$DIV$'+id_mat,id_mat,mat_nom,$urlmat,'',mat_format, mat_tcin, mat_tcout);
		//out=add_t_panier_doc(arrRow);
		
		out=add_t_panier_doc("t_panier_doc", 0, '', '', false);
		$row = $j("#t_panier_doc\\$"+(num_row_t_panier_doc - 1));
		
		$row.find("input[chfields='ID_DOC']").val(id_doc);
		$row.find("input[chfields='PDOC_ID_MAT']").val(id_mat);
		
		$row.find("span[chfields='DOC_COTE']").html(doc_cote);
		$row.find("span[chfields='DOC_TITRE']").html(doc_titre);
		$row.find("span[chfields='EXT_DUREE']").html(doc_duree);
		$row.find("span[chfields='MAT_NOM']").html(mat_nom);
		$row.find("span[chfields='FONDS']").html(fonds);
		$row.find("span[chfields='V_SUP']").html(sup);
		$row.find("span[chfields='V_ETAT']").html(etat);
		
		/*newFld=document.createElement('DIV');
		newFld.innerHTML=out[0];

		myBtn=document.getElementById('toolbar');
		if (myBtn) {myTab.insertBefore(newFld,myBtn);} else {myTab.appendChild(newFld);}*/

		xmlDoc=null; //vidage mémoire

	}
	
	function addExemplaire(idx) { 
		var newId = true;
		$j("input[chfields='PDOC_ID_MAT']").each (function () {
			if ($j(this).val() == idx)
				newId = false;
		});
		if (newId)
			return !sendData('GET','export.php','xmlhttp=1&id='+idx+'&id_lang=<?=$myDoc->t_doc['ID_LANG']?>&type=materiel&export=0&impression=screen','addExemplaireAJAX');		
		else
			alert("Exemplaire déjà présent");
	}
	
	function chooseMat () {
		//choose(this,'titre_index=Materiel&valeur=&champ=M&rtn=addExemplaire');
		$j("#popupModal,#frameModal").removeAttr('class');
		url_choice = "empty.php?include=chooseMat&id_panier=<?=$myPanier->t_panier["ID_PANIER"];?>";
		$j("#popupModal #frameModal").addClass("frame_menu_actions").css("width", "900px");
		
		$j.ajax({
			url : url_choice,
			success : function(data){
				$j("#frameModal.frame_menu_actions").html(data);
				$j("#popupModal").css('display','block');
			}
		});
	}
	
	function searchMat(form) {
		url_choice = "empty.php?include=chooseMat&id_panier=<?=$myPanier->t_panier["ID_PANIER"];?>";
		
		$j.post(url_choice, $j(form).serialize() ).done(function (data) {
			$j("#frameModal.frame_menu_actions").html(data);
		});
	}
	
	function changeStatutComm (new_etat) {
		if (new_etat == 1) {
			$j("input[chfields='PANXML_SORTIE']").val("");
			$j("input[chfields='PANXML_RETOUR']").val("");
		}
		
		if (new_etat == 3)
			$j("input[chfields='PANXML_SORTIE']").val(new Date().toJSON().slice(0,10));
		if (new_etat == 5)
			$j("input[chfields='PANXML_RETOUR']").val(new Date().toJSON().slice(0,10));
		
		//if (new_etat == 3 || new_etat == 5)
			$j("select[chfields='PDOC_ID_ETAT']").val(new_etat);
			
	}
	
	function updateEtatLignes (new_etat) {
		$j("select[chfields='PDOC_ID_ETAT']").val(new_etat);
		
		if (new_etat == 3 || new_etat == 5) {
			changeStatutComm (new_etat);
			$j("select#pan_id_etat").val(new_etat);
		}
	}
	
	function changeStatutLigne (new_etat) {
		var ok = true;
		if (new_etat == 3 || new_etat == 5) {
			$j("tr[id!='t_panier_doc$blank'] select[chfields='PDOC_ID_ETAT']").each(function () {
				if ($j(this).parent("td").parent("tr").attr("id") != "t_panier_doc$blank" && $j(this).val() != new_etat)
					ok = false;
			});
		}
		if (ok) {
			$j("select#pan_id_etat").val(new_etat);
			if (new_etat == 3)
				$j("input[chfields='PANXML_SORTIE']").val(new Date().toJSON().slice(0,10));
			if (new_etat == 5)
				$j("input[chfields='PANXML_RETOUR']").val(new Date().toJSON().slice(0,10));
		} else if (new_etat == 5) {
			$j("select#pan_id_etat").val(4);
		}
	}
	
	function updateCustomers (us_societe) {
		$j("#pan_id_usager options").remove();
		
		if (us_societe != '') {
			url_options = "empty.php?include=getCustomerSelect&us_societe="+encodeURI(us_societe.replace("#", "%23"));
			
			$j.ajax({
				url : url_options,
				success : function(data){
					$j("#pan_id_usager").html(data);
				}
			});
		}
	}
	
	function addUserFinal(field,text,id) {
		$j("#panxml_user").val(text);
		hideMe();
	}
	
	$j(document).ready(function () {
		//updateCustomers($j("#panxml_client").val());
	});
</script>
<div class="title_bar">
	<div id="backButton"><?=$myPage->renderReferrer(kRetourListeRecherche, 'class="icoBackSearch"');?></div>
	<div id="pageTitre"><?= kBordereau ?>&nbsp;</div>
	
	<?php if(!empty($myPanier->t_panier['ID_PANIER'])){ ?>
		<div class="toolbar">
			<div id="consult" onclick="saveIfAndGo('<?=$myPage->getName().'?urlaction=commande&id_panier='.$myPanier->t_panier['ID_PANIER']; ?>');" class="tool"><span class="tool_hover_text"><?=kConsultation?></span>&#160;</div>
		</div>
	<?php } ?>
</div>
<?
require_once(modelDir."model_materiel.php");
require_once(libDir."fonctionsGeneral.php");
$myUser = User::getInstance();

//Changement état
if ($_POST["pan_id_etat"] == "3") {

}


if (empty($myPanier->t_panier["PAN_DATE_CREA"]))
	$myPanier->t_panier["PAN_DATE_CREA"] = date("Y-m-d H:i:s");

foreach ($myPanier->t_panier_doc as $idx=>&$pdoc) {
	if ($pdoc['PDOC_EXTRAIT'] == "1")
		$pdoc['PDOC_EXT'] = $pdoc['PDOC_EXT_TITRE']."<br />".$pdoc['PDOC_EXT_TCIN']."<br />".$pdoc['PDOC_EXT_TCOUT'];
	
	$selectId = "t_panier_doc[".$idx."][PDOC_ID_MAT]";
	if (empty($pdoc['PDOC_ID_MAT'])) {
		$options = listOptions("select ID_MAT as ID, MAT_NOM as VAL from t_mat WHERE MAT_TYPE='EXEMPLAIRE' AND id_mat in (SELECT id_mat FROM t_doc_mat WHERE id_doc = ".intval($pdoc['ID_DOC']).")", Array("ID","VAL"), 0, false);
		//var_dump($selectId);
		?><script>
			$j(document).ready ( function () {
				$j('select[id="<?=$selectId;?>"]').html('<?echo $options;?>');
			});
		</script>
		<!--style>
			#t_panier_doc$tab tr td:nth-child(7) {
				display: none;
			}
		</style--><?
	} else {
		$mat = new Materiel();
		$mat->t_mat['ID_MAT'] = $pdoc['PDOC_ID_MAT'];
		$mat->getMat();
		$mat->getValeurs();
		$pdoc["MAT"] = $mat;
			
		$pdoc["MAT_NOM"] = $mat->t_mat['MAT_NOM'];
		$pdoc["MAT_TEXT_6"] = $mat->t_mat['MAT_TEXT_6'];
		$pdoc["MAT_TEXT_7"] = $mat->t_mat['MAT_TEXT_7'];
		$pdoc["V_SUP"] = $mat->t_mat_val['MAT_NOM'];
		foreach ($mat->t_mat_val as $val)
			if ($val["VAL_ID_TYPE_VAL"] == "SUP")
				$pdoc["V_SUP"] = $val["VALEUR"];
		foreach ($mat->t_mat_val as $val)
			if ($val["VAL_ID_TYPE_VAL"] == "ETAT")
				$pdoc["V_ETAT"] = $val["VALEUR"];
				
		?><script>
			$j(document).ready ( function () {
				$j('select[id="<?=$selectId;?>"]').parent("td").html('<?echo $mat->t_mat['MAT_NOM'];?>');
			});
		</script><?
	}
}

include(libDir."class_formSaisie.php");

if (empty($myPanier->t_panier["ID_PANIER"]) || $myPanier->t_panier["PAN_ID_TYPE_COMMANDE"] != "2") {
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/bordereauSaisie.xml"));
	
	if ($_POST["commande"] == "SAVE") {
		$res = $db->getAll("SELECT id_panier FROM t_panier WHERE pan_objet = ".$db->Quote($myPanier->t_panier["PAN_OBJET"])." AND id_panier < ".intval($myPanier->t_panier["ID_PANIER"]));
		if (count($res) > 0) {
			$lastRef = $db->getOne("SELECT max(pan_objet) FROM t_panier WHERE pan_id_type_commande not in (2) AND pan_objet like '$year%'");
			if ($lastRef == null)
				$lastRef = $year.'/0000';
			$lastId = str_pad((intval(explode("/", $lastRef)[1]) + 1), 4, '0', STR_PAD_LEFT);
			$newRef = explode("/", $lastRef)[0] . "/" . $lastId;
			
			$myPanier->t_panier["PAN_OBJET"] = $newRef;
			//var_dump($newRef); TODO : message ?
			$ok=$myPanier->save();
		}
	}
	
	else if (empty($myPanier->t_panier["ID_PANIER"])) {
		$year = intval(date("Y"));
		if (intval(date("n")) >= 8 )
			$year++;
		
		$lastRef = $db->getOne("SELECT max(pan_objet) FROM t_panier WHERE pan_id_type_commande in (3, 5) AND pan_objet like '$year%'");
		if ($lastRef == null)
			$lastRef = $year.'/0000';
		$lastId = str_pad((intval(explode("/", $lastRef)[1]) + 1), 4, '0', STR_PAD_LEFT);
		$newRef = explode("/", $lastRef)[0] . "/" . $lastId;
		
		$myPanier->t_panier["PAN_OBJET"] = $newRef;
		
		$myPanier->t_panier["ID_PANIER"] = "";
		//$myPanier->t_panier["PANXML_STATUT"] = "Nouveau";
		
		?><style>
			fieldset#docs, div.label_champs_form:last-of-type, button#bAdd {
				display: none !important;
			}
		</style><?
	}
		
	//$myPanier->t_panier["PAN_USAGER_CREA"] = $myPanier->t_panier["PAN_ID_USAGER"];
	//var_dump($myPanier->t_panier["XML"]["commande"]["PANXML_MRETOUR"]);
	if (empty($myPanier->t_panier["XML"]["commande"]["PANXML_IDCREA"])) {
		$myPanier->t_panier["XML"]["commande"]["PANXML_IDCREA"] = $myUser->UserID;
		$myPanier->t_panier["USAGER_CREA"] = $myUser->Nom . " " . $myUser->Prenom;
	}
	else
		$myPanier->t_panier["USAGER_CREA"] = $db->getOne("SELECT us_nom || ' ' || us_prenom FROM t_usager WHERE id_usager = ".intval($myPanier->t_panier["XML"]["commande"]["PANXML_IDCREA"]));
		
	if (!empty($myPanier->t_panier["PAN_ID_USAGER_MOD"]))
		$myPanier->t_panier["USAGER_MODIF"] = $db->getOne("SELECT us_nom || ' ' || us_prenom FROM t_usager WHERE id_usager = ".intval($myPanier->t_panier["PAN_ID_USAGER_MOD"]));
		
	//$myPanier->t_panier["XML"]["commande"]["PANXML_CLIENT"] = "FORUM DES IMAGES";
	if (!empty($myPanier->owner->t_usager ["US_SOCIETE"])) {
		$myPanier->t_panier["XML"]["commande"]["PANXML_CLIENT"] = $myPanier->owner->t_usager ["US_SOCIETE"];
		$xmlform=str_replace("WHERE 1=2", "WHERE us_id_type_usager = 1 AND us_societe = ".$db->Quote($myPanier->owner->t_usager ["US_SOCIETE"]), $xmlform);
	}
	
	$timeTot = 0;
	foreach ($myPanier->t_panier_doc as $idx=>$doc)
		$timeTot += timeToSec($doc["DOC_DUREE"]);
	$myPanier->t_panier["DUREE_TOT"] = secToTime($timeTot);
		
} else
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/commandeSaisie.xml"));

//Remplacement des valeurs dans le formulaire
$xmlform=str_replace("[PAN_ID_TYPE_COMMANDE]", $myPanier->t_panier["PAN_ID_TYPE_COMMANDE"], $xmlform);

$myForm=new FormSaisie;
$myForm->saisieObj=$myPanier;
$myForm->entity="PANIER";
$myForm->classLabel="label_champs_form";
$myForm->classValue="val_champs_form";
$myForm->classHelp="help_champs_form";
$myForm->display($xmlform);

echo "<script> updateAllIdProcFromSupportLiv(); \$j('#panxml_id_proc_video').val(\$j('#panxml_id_proc').val()); </script>";

if (isset($myLivraison))
{
	foreach ($myLivraison->arrDocs as $idx=>$doc) {
		if (!empty($doc['fileSize'])) { //le fichier a été demandé et bien livré (taille > 0)
			echo "<iframe src='download.php?livraison=".$myLivraison->idDir.'/'.$doc['fileName']."' name='frameCommande".$idx."' id='frameCommande".$idx."' width='0' height='0'>&#160;</iframe>";
		}
	}
}
?>
<button id="bAdd" class="ui-state-default ui-corner-all" type="button" onclick="javascript:chooseMat ();">
	<span class="ui-icon ui-icon-open"></span><?= kAjouter?>
</button>

<script type="text/javascript">
	$j(document).ready ( function () {
		requirejs(['jquery.ui.widget'],function(){
			if ($j("#panxml_datebloc_deb").val() != "") {
				dateD = $j.datepicker.parseDate( "yy-mm-dd", $j("#panxml_datebloc_deb").val());
				$j("#panxml_datebloc_fin").datepicker("option", "minDate", dateD);
			}
			if ($j("#panxml_datebloc_fin").val() != "") {
				dateF = $j.datepicker.parseDate( "yy-mm-dd", $j("#panxml_datebloc_fin").val());
				$j("#panxml_datebloc_deb").datepicker("option", "maxDate", dateF);
			}
			
			$j("#panxml_datebloc_deb").on( "change", function() {
				date = $j.datepicker.parseDate( "yy-mm-dd", $j(this).val());
				$j("#panxml_datebloc_fin").datepicker( "option", "minDate", date );
			});
			$j("#panxml_datebloc_fin").on( "change", function() {
				date = $j.datepicker.parseDate( "yy-mm-dd", $j(this).val());
				$j("#panxml_datebloc_deb").datepicker( "option", "maxDate", date );
			});
		});
	});
</script>

	
