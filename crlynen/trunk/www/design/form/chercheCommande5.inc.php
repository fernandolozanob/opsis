<div class="title_bar">
	<div id="backButton"><a class="icoBackSearch" href="index.php?urlaction=admin">
		<?=kRetourAdmin;?>
	</a></div>
	<?= kRechercheBordereaux ?>
	<div class="toolbar">
		<div  onclick="javascript:popupPrint('print.php?<?=$_SERVER['QUERY_STRING'];?>','main')" class=" print tool">
			<span class="tool_hover_text"><?echo kImprimer;?></span>
		</div>
	</div>
</div>
<div class="contentBody">
	<?
	$myPage->titre=kGestionBordereaux;
	//$myPage->setReferrer(true, '', true);
		
	require_once(libDir."class_formCherche.php");
	$xmlform=file_get_contents(getSiteFile("designDir","form/xml/chercheBordereaux.xml"));
	$myForm=new FormCherche;
	$myForm->chercheObj=$mySearch;
	$myForm->entity="PAN";
	$myForm->classLabel="label_champs_form";
	$myForm->classValue="val_champs_form";
	$myForm->display($xmlform);
	
	if(isset($_SESSION[$mySearch->sessVar]["sql"])) {
	    $sql=$_SESSION[$mySearch->sessVar]["sql"];
		$sql = str_replace("t_etat_pan.ETAT_PAN", "CASE WHEN pan_id_etat = 0 THEN 'Soldé'  WHEN pan_id_etat = 1 THEN 'En Cours' 
													WHEN pan_id_etat = 2 THEN 'Programmé' 
													WHEN pan_id_etat = 3 THEN 'Expédié' 
													WHEN pan_id_etat = 4 THEN 'Retour Partiel' 
													WHEN pan_id_etat = 5 THEN 'Soldé' 
													WHEN pan_id_etat = 6 THEN 'Retour en attente' 
													WHEN pan_id_etat = 7 THEN 'En contentieux' 
													ELSE '' END as ETAT_PAN", $sql);
		$_SESSION[$mySearch->sessVar]["sql"] = $sql;
		//var_dump($sql);
	}
	?>
</div>

<script>
function  changeValue(fieldName,idx,valeur) {
	$j("input[name='"+fieldName+"']").val(valeur);
}
</script>
