<?xml version="1.0" encoding="utf-8"?>
<!-- 
import FCP 
VP : 13/09/2007 : Création du fichier
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

	<xsl:template match='/DATA'>
		<data>
			<xsl:for-each select="LEX">
				<LEX>
					<LEX_ID_TYPE_LEX><xsl:value-of select="id_type_lex"/></LEX_ID_TYPE_LEX>
					<LEX_TERME><xsl:value-of select="terme"/></LEX_TERME>
					<LEX_TERME_GEN><xsl:value-of select="terme_TG"/></LEX_TERME_GEN>
					<xsl:if test="terme_EN!=''"><LEX_TERME_EN><xsl:value-of select="terme_EN"/></LEX_TERME_EN></xsl:if>
					<xsl:if test="terme_EP!=''"><LEX_TERME_SYNO><xsl:value-of select="terme_EP"/></LEX_TERME_SYNO></xsl:if>
					<!--xsl:if test="terme_TA!=''"><LEX_TERME_ASSO><xsl:value-of select="terme_TA"/></LEX_TERME_ASSO></xsl:if-->
					<xsl:if test="notes!=''"><LEX_NOTE><xsl:value-of select="notes"/></LEX_NOTE></xsl:if>
					<xsl:if test="lex_code!=''"><LEX_CODE><xsl:value-of select="lex_code"/></LEX_CODE></xsl:if>
				</LEX>
			</xsl:for-each>
		</data>
	</xsl:template>


</xsl:stylesheet>
