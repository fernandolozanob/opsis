<?xml version="1.0" encoding="utf-8"?>
<!-- 
import FCP 
VP : 13/09/2007 : Création du fichier
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="no"
        indent="yes"
/> 

<xsl:template match='/DATA'>
	<data>
		<xsl:for-each select="DOCFILM">
			<DOC>
				<DOC_ACCES>0</DOC_ACCES>
				<ID_LANG>FR</ID_LANG>
				<DOC_ID_MEDIA>V</DOC_ID_MEDIA>
				<DOC_ID_TYPE_DOC>1</DOC_ID_TYPE_DOC>
				
				<CT_FONDS>Fonds TMP</CT_FONDS>
				<DOC_TITRE><xsl:value-of select="normalize-space(FicheFilmTitre)"/></DOC_TITRE>
				<DOC_COTE><xsl:value-of select="normalize-space(FicheFilmCode)"/></DOC_COTE>
				<xsl:if test="normalize-space(FicheFilmDuree)!=''">
					<DOC_DUREE><xsl:value-of select="normalize-space(FicheFilmDuree)"/></DOC_DUREE>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmTitreValide)!=''">
					<DOC_TITREORI><xsl:value-of select="normalize-space(FicheFilmTitreValide)"/></DOC_TITREORI>
				</xsl:if>
				<DOC_RES>
					<xsl:if test="normalize-space(FicheFilmResume)!=''">
						<xsl:value-of select="normalize-space(FicheFilmResume)"/>\n
					</xsl:if>
					<xsl:if test="normalize-space(FicheFilmPellicule)!=''">
						<xsl:value-of select="normalize-space(FicheFilmPellicule)"/>\n
					</xsl:if>
					<xsl:if test="normalize-space(FicheFilmFichePedagogique)!=''">
						<xsl:value-of select="normalize-space(FicheFilmFichePedagogique)"/>
					</xsl:if>
					&amp;nbsp;
				</DOC_RES>
				<xsl:if test="normalize-space(FicheFilmAnneeDeRealisation)!=''">
					<DOC_DATE_PV_DEBUT>
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="normalize-space(FicheFilmAnneeDeRealisation)"/>
						</xsl:call-template>
					</DOC_DATE_PV_DEBUT>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmEntreeEnCinemLe)!=''">
					<DOC_DATE_ETAT>
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="normalize-space(FicheFilmEntreeEnCinemLe)"/>
						</xsl:call-template>
					</DOC_DATE_ETAT>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmNumeroSaf)!=''">
					<DOC_COPYRIGHT><xsl:value-of select="normalize-space(FicheFilmNumeroSaf)"/></DOC_COPYRIGHT>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmNumerisation)!=''">
					<DOC_TEXT2><xsl:value-of select="normalize-space(FicheFilmNumerisation)"/></DOC_TEXT2>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmMuetSonore)!=''">
					<V_SON><xsl:value-of select="normalize-space(FicheFilmMuetSonore)"/></V_SON>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmImpression)!=''">
					<V_COUL><xsl:value-of select="normalize-space(FicheFilmImpression)"/></V_COUL>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmSupport)!=''">
					<V_ORI><xsl:value-of select="normalize-space(FicheFilmSupport)"/></V_ORI>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmMetrageDOrigine)!=''">
					<DOC_TEXT7><xsl:value-of select="normalize-space(FicheFilmMetrageDOrigine)"/></DOC_TEXT7>
				</xsl:if>
				
				<xsl:if test="normalize-space(FicheFilmRealisateur)!=''">
					<P_CTB_REA_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmRealisateur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_REA_PP>
				</xsl:if>
				<!--xsl:if test="normalize-space(FicheFilmRealisateur)!=''">
					<P_CTB_PHO_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmRealisateur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_PHO_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmRealisateur)!=''">
					<P_CTB_EDI_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmRealisateur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_EDI_PP>
				</xsl:if-->
				<xsl:if test="normalize-space(FicheFilmProducteur)!=''">
					<P_CTB_PRD_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmProducteur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_PRD_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmDistributeur)!=''">
					<P_CTB_DIS_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmDistributeur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_DIS_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmAuteur)!=''">
					<P_CTB_AUT_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmAuteur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_AUT_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmImage)!=''">
					<P_CTB_IMG_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmImage)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_IMG_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmMusique)!=''">
					<P_CTB_MUS_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmMusique)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_MUS_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmMarque)!=''">
					<P_CTB_MAR_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmMarque)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_MAR_PP>
				</xsl:if>
				<!--xsl:if test="normalize-space(FicheFilmRealisateur)!=''">
					<P_CTB_ILL_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmRealisateur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_ILL_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmRealisateur)!=''">
					<P_CTB_INT_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmRealisateur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_INT_PP>
				</xsl:if>
				<xsl:if test="normalize-space(FicheFilmRealisateur)!=''">
					<P_CTB_IMP_PP>
						<xsl:choose>
							<PERS_NOM><xsl:value-of select="normalize-space(FicheFilmRealisateur)"/></PERS_NOM>
						</xsl:choose>
					</P_CTB_IMP_PP>
				</xsl:if-->
				
				<xsl:if test="normalize-space(FicheFilmTheme)!=''">
					<V_THM><xsl:value-of select="normalize-space(FicheFilmTheme)"/></V_THM>
				</xsl:if>
				
				
				
				<xsl:if test="normalize-space(NOTES)!=''">
					<DOC_RES><xsl:value-of select="normalize-space(NOTES)"/></DOC_RES>
				</xsl:if>
				<xsl:if test="normalize-space(DESCRIPTION)!=''">
					<DOC_NOTES><xsl:value-of select="normalize-space(DESCRIPTION)"/></DOC_NOTES>
				</xsl:if>
				<xsl:if test="normalize-space(DATE_CREATION)!=''">
					<DOC_DATE_PROD>
						<xsl:call-template name="format_date">
							<xsl:with-param name="chaine_date" select="substring-before(normalize-space(DATE_CREATION), ' ')"/>
						</xsl:call-template>
					</DOC_DATE_PROD>
				</xsl:if>
				<xsl:if test="normalize-space(COLLECTION)!=''">
					<V_COLL><xsl:value-of select="normalize-space(COLLECTION)"/></V_COLL>
				</xsl:if>
				<xsl:if test="normalize-space(AUTEUR)!=''">
					<P_AUT__PP>
						<xsl:choose>
							<xsl:when test="contains(AUTEUR,',')">
								<PERS_NOM><xsl:value-of select="substring-before(normalize-space(AUTEUR), ', ')"/></PERS_NOM>
								<PERS_PRENOM><xsl:value-of select="substring-after(normalize-space(AUTEUR), ', ')"/></PERS_PRENOM>
							</xsl:when>
							<xsl:otherwise>
								<PERS_NOM><xsl:value-of select="normalize-space(AUTEUR)"/></PERS_NOM>
							</xsl:otherwise>
						</xsl:choose>
						<DLEX_ID_ROLE><xsl:value-of select="substring(translate(normalize-space(ROLE), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3)"/></DLEX_ID_ROLE>
					</P_AUT__PP>
				</xsl:if>
				<xsl:if test="normalize-space(STATUT)!=''">
					<DOC_RECOMP><xsl:value-of select="normalize-space(STATUT)"/></DOC_RECOMP>
				</xsl:if>
				<xsl:if test="normalize-space(DROIT_AUTEUR)!=''">
					<V_DROI><xsl:value-of select="normalize-space(DROIT_AUTEUR)"/></V_DROI>
				</xsl:if>
				<xsl:if test="normalize-space(SUPPORT)!=''">
					<V_FMT><xsl:value-of select="normalize-space(SUPPORT)"/></V_FMT>
				</xsl:if>
				<xsl:if test="normalize-space(TECHNIQUE1)!=''">
					<V_COUL><VALEUR><xsl:value-of select="normalize-space(TECHNIQUE1)"/></VALEUR></V_COUL>
				</xsl:if>
				<xsl:if test="normalize-space(TECHNIQUE2)!=''">
					<V_COUL><VALEUR><xsl:value-of select="normalize-space(TECHNIQUE2)"/></VALEUR></V_COUL>
				</xsl:if>
				<xsl:if test="normalize-space(PARTICULARITES)!=''">
					<V_EXP><VALEUR><xsl:value-of select="normalize-space(PARTICULARITES)"/></VALEUR></V_EXP>
				</xsl:if>
				<xsl:if test="normalize-space(PARTICULARITES2)!=''">
					<V_EXP><VALEUR><xsl:value-of select="normalize-space(PARTICULARITES2)"/></VALEUR></V_EXP>
				</xsl:if>
				<xsl:if test="normalize-space(OBJET_ASSOCIE)!=''">
					<DOC_PROCAV><xsl:value-of select="normalize-space(OBJET_ASSOCIE)"/></DOC_PROCAV>
				</xsl:if>
				
				
				<xsl:if test="normalize-space(INDEX1_SUJET1)!=''">
					<L_TH__NC>
						<xsl:choose>
							<xsl:when test="contains(INDEX1_SUJET1, ':')">
								<VALEUR><xsl:value-of select="normalize-space(substring-before(INDEX1_SUJET1, ':'))"/></VALEUR>
								<LEX_PRECISION><xsl:value-of select="normalize-space(substring-after(INDEX1_SUJET1,':'))"/></LEX_PRECISION>
							</xsl:when>
							<xsl:otherwise>
								<VALEUR><xsl:value-of select="normalize-space(INDEX1_SUJET1)"/></VALEUR>
							</xsl:otherwise>
						</xsl:choose>
						</L_TH__NC>
				</xsl:if>
				
				<xsl:if test="normalize-space(INDEX1_PERSONNE1)!=''">
					<xsl:call-template name="splitPers2">
						<xsl:with-param name="pVal" select="INDEX1_PERSONNE1"/>
						<xsl:with-param name="pBal" select="'P_PER__PP'"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="normalize-space(INDEX1_LIEU_2)!=''">
					<L_LG__LG><VALEUR><xsl:value-of select="normalize-space(INDEX1_LIEU_2)"/></VALEUR></L_LG__LG>
				</xsl:if>
				
				<xsl:if test="normalize-space(INDEX1_EVENEMENT)!=''">
					<FEST><FEST_LIBELLE><xsl:value-of select="normalize-space(INDEX1_EVENEMENT)"/></FEST_LIBELLE></FEST>
				</xsl:if>

				<xsl:if test="normalize-space(NOTICE_CREEE_PAR)!=''">
					<DOC_RES_CAT><xsl:value-of select="normalize-space(substring-before(NOTICE_CREEE_PAR, ':'))"/></DOC_RES_CAT>
				</xsl:if>
				<xsl:if test="normalize-space(NOTICE_MODIFIEE_PAR)!=''">
					<DOC_LIEU_PV><xsl:value-of select="normalize-space(substring-before(NOTICE_MODIFIEE_PAR, ':'))"/></DOC_LIEU_PV>
				</xsl:if>
				<xsl:if test="normalize-space(NOTICE_CRE)!=''">
					<DOC_DATE_DIFF><xsl:value-of select="normalize-space(NOTICE_CRE)"/></DOC_DATE_DIFF>
				</xsl:if>
				<xsl:if test="normalize-space(NOTICE_MOD)!=''">
					<DOC_DATE_DIFF_FIN><xsl:value-of select="normalize-space(NOTICE_MOD)"/></DOC_DATE_DIFF_FIN>
				</xsl:if>
				<xsl:if test="normalize-space(VERIFICATEUR)!=''">
					<US_CREA><xsl:value-of select="normalize-space(VERIFICATEUR)"/></US_CREA>
				</xsl:if>
				
				<xsl:if test="normalize-space(INVENTAIRE)!=''">
					<MAT>
						<MAT_ID_MEDIA>S</MAT_ID_MEDIA>
						<xsl:if test="normalize-space(INVENTAIRE)!=''">
							<MAT_EMPRUNTEUR><xsl:value-of select="normalize-space(INVENTAIRE)"/></MAT_EMPRUNTEUR>
							<MAT_NOM><xsl:value-of select="normalize-space(INVENTAIRE)"/></MAT_NOM>
						</xsl:if>
						<xsl:if test="normalize-space(AUTRE_N)!=''">
							<MAT_USERBIT><xsl:value-of select="normalize-space(AUTRE_N)"/></MAT_USERBIT>
						</xsl:if>
						<xsl:if test="normalize-space(NOMBRE)!=''">
							<MAT_BULLETIN><xsl:value-of select="normalize-space(NOMBRE)"/></MAT_BULLETIN>
						</xsl:if>
						<xsl:if test="normalize-space(DATE_CREATION2)!=''">
							<MAT_DATE_RETOUR>
								<xsl:call-template name="format_date">
									<xsl:with-param name="chaine_date" select="substring-before(normalize-space(DATE_CREATION2), ' ')"/>
								</xsl:call-template>
							</MAT_DATE_RETOUR>
						</xsl:if>
						<xsl:if test="normalize-space(SUPPORT)!=''">
							<V_FMT><xsl:value-of select="normalize-space(SUPPORT)"/></V_FMT>
						</xsl:if>
						<xsl:if test="normalize-space(LOCALISATION)!=''">
							<V_LOC><xsl:value-of select="normalize-space(LOCALISATION)"/></V_LOC>
						</xsl:if>
						<xsl:if test="normalize-space(MESURES1)!=''">
							<V_MES><xsl:value-of select="normalize-space(MESURES1)"/></V_MES>
						</xsl:if>
						<xsl:if test="normalize-space(MESURES2)!=''">
							<V_MES><xsl:value-of select="normalize-space(MESURES2)"/></V_MES>
						</xsl:if>
						<xsl:if test="normalize-space(CONSTAT_ETAT)!=''">
							<MAT_TEXT_1><xsl:value-of select="normalize-space(CONSTAT_ETAT)"/></MAT_TEXT_1>
						</xsl:if>
						<xsl:if test="normalize-space(DATE_CONST)!=''">
							<MAT_DATE_ETAT>
								<xsl:call-template name="format_date">
									<xsl:with-param name="chaine_date" select="normalize-space(DATE_CONST)"/>
								</xsl:call-template>
							</MAT_DATE_ETAT>
						</xsl:if>
						<xsl:if test="normalize-space(DATE_LOCAL)!=''">
							<MAT_COMPRESSION>
								<xsl:call-template name="format_date">
									<xsl:with-param name="chaine_date" select="normalize-space(DATE_LOCAL)"/>
								</xsl:call-template>
							</MAT_COMPRESSION>
						</xsl:if>
					</MAT>
				</xsl:if>
			</DOC>
		</xsl:for-each>

	</data>
</xsl:template>

	<xsl:template match="text()" name="splitPers">
		<xsl:param name="pNom" select="."/>
		<xsl:param name="pPrenom" select="."/>
		<xsl:param name="pRole" select="."/>
		<xsl:param name="pBal" select="."/>
		
		<xsl:if test="string-length($pNom)">
			<xsl:element name="{$pBal}">
				<PERS_NOM><xsl:value-of select="normalize-space(substring-before(concat($pNom,';'),';'))"/></PERS_NOM>
				<PERS_PRENOM><xsl:value-of select="normalize-space(substring-before(concat($pPrenom,';'),';'))"/></PERS_PRENOM>
				<DLEX_ID_ROLE><xsl:value-of select="substring-before(concat(substring(translate(normalize-space($pRole), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3),';'),';')"/></DLEX_ID_ROLE>
			</xsl:element>
			<xsl:call-template name="splitPers">
				<xsl:with-param name="pNom" select="substring-after($pNom, ';')"/>
				<xsl:with-param name="pPrenom" select="substring-after($pPrenom, ';')"/>
				<xsl:with-param name="pRole" select="substring-after($pRole, ';')"/>
				<xsl:with-param name="pBal" select="$pBal"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="text()" name="splitPers2">
		<xsl:param name="pVal" select="."/>
		<xsl:param name="pBal" select="."/>
		
		<xsl:if test="string-length($pVal)">
			<xsl:element name="{$pBal}">
				<xsl:choose>
					<xsl:when test="contains($pVal,':') and contains(substring-before($pVal, ':'),',')">
						<PERS_NOM><xsl:value-of select="normalize-space(substring-before($pVal, ','))"/></PERS_NOM>
						<PERS_PRENOM><xsl:value-of select="normalize-space(substring-after(substring-before($pVal, ':'), ','))"/></PERS_PRENOM>
						<LEX_PRECISION><xsl:value-of select="normalize-space(substring-after($pVal, ':'))"/></LEX_PRECISION>
					</xsl:when>
					<xsl:when test="contains($pVal,',') and not(contains($pVal,':'))">
						<PERS_NOM><xsl:value-of select="normalize-space(substring-before($pVal, ','))"/></PERS_NOM>
						<PERS_PRENOM><xsl:value-of select="normalize-space(substring-after($pVal, ','))"/></PERS_PRENOM>
					</xsl:when>
					<xsl:otherwise>
						<PERS_NOM><xsl:value-of select="normalize-space($pVal)"/></PERS_NOM>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template match="text()" name="split">
		<xsl:param name="pText" select="."/>
		<xsl:param name="pBal" select="."/>
		
		<xsl:if test="string-length($pText)">
			<xsl:element name="{$pBal}">
				<VALEUR><xsl:value-of select="normalize-space(substring-before(concat($pText,';'),';'))"/></VALEUR>
			</xsl:element>
			<xsl:call-template name="split">
				<xsl:with-param name="pText" select="substring-after($pText, ';')"/>
				<xsl:with-param name="pBal" select="$pBal"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="text()" name="splitcomma">
		<xsl:param name="pText" select="."/>
		<xsl:param name="pBal" select="."/>
		
		<xsl:if test="string-length($pText)">
			<xsl:element name="{$pBal}">
				<VALEUR><xsl:value-of select="normalize-space(substring-before(concat($pText,','),','))"/></VALEUR>
			</xsl:element>
			<xsl:call-template name="splitcomma">
				<xsl:with-param name="pText" select="substring-after($pText, ',')"/>
				<xsl:with-param name="pBal" select="$pBal"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="text()" name="split2">
		<xsl:param name="pText" select="."/>
		<xsl:param name="pBal" select="."/>
		<xsl:param name="pSep" select="."/>
		
		<xsl:if test="string-length($pText)">
			<xsl:element name="{$pBal}">
				<VALEUR><xsl:value-of select="normalize-space(substring-before(concat($pText,$pSep),$pSep))"/></VALEUR>
			</xsl:element>
			<xsl:call-template name="split">
				<xsl:with-param name="pText" select="substring-after($pText, $pSep)"/>
				<xsl:with-param name="pBal" select="$pBal"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template match="text()" name="split_lex_pref">
		<xsl:param name="pText" select="."/>
		<xsl:param name="pBal" select="."/>
		
		<xsl:if test="string-length($pText)">
			<xsl:element name="{$pBal}">
				<VALEUR><xsl:value-of select="normalize-space(substring-before(concat(substring-before(concat($pText,';'),';'),'('),'('))"/></VALEUR>
				<LEX_PRECISION><xsl:value-of select="normalize-space(substring-before(substring-after($pText,'('),')'))"/></LEX_PRECISION>
			</xsl:element>
			<xsl:call-template name="split_lex_pref">
				<xsl:with-param name="pText" select="substring-after($pText, ';')"/>
				<xsl:with-param name="pBal" select="$pBal"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

<!-- 
FORMAT_DUREE : Formate une dur√©e HH:MM:SS
-->
    <xsl:template name="format_duree">
        <xsl:param name="chaine_duree"/>
            <!-- d√©coupage de la chaine -->
            <xsl:variable name="duree_hh"><xsl:text>00</xsl:text></xsl:variable>
            <xsl:variable name="duree_mm">
					<xsl:choose>
						<xsl:when test="contains($chaine_duree,'min')"><xsl:value-of select="format-number(normalize-space(substring-before($chaine_duree,'min')),'00')"/></xsl:when>
						<xsl:otherwise><xsl:text>00</xsl:text></xsl:otherwise>
					</xsl:choose>
			</xsl:variable>
            <xsl:variable name="duree_ss">
					<xsl:choose>
						<xsl:when test="contains($chaine_duree,'min')"><xsl:value-of select="format-number(normalize-space(substring-before(substring-after($chaine_duree,'min'),'sec')),'00')"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="format-number(normalize-space(substring-before($chaine_duree,'sec')),'00')"/></xsl:otherwise>
					</xsl:choose>
			</xsl:variable>

            <!-- Restitution -->
        	<xsl:value-of select="$duree_hh"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_mm"/><xsl:text>:</xsl:text><xsl:value-of select="$duree_ss"/>
            
    </xsl:template>

<!-- 
FORMAT_DATE : Formate une date jj/mm/aaaa en aaaa-mm-jj
-->
	<xsl:template name="format_date">
        <xsl:param name="chaine_date"/>
        <!-- découpage de la chaine -->
		<xsl:choose>
			<xsl:when test="contains($chaine_date, '/')">
				<xsl:variable name="j"><xsl:value-of select="substring-before($chaine_date,'/')"/></xsl:variable>
				<xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine_date,'/'),'/')"/></xsl:variable>
				<xsl:variable name="a"><xsl:value-of select="substring(substring-after(substring-after($chaine_date,'/'),'/'),1,4)"/></xsl:variable>
				<!--xsl:variable name="hms"><xsl:value-of select="substring-after($chaine_date,' ')"/></xsl:variable-->
				<xsl:if test="$a!='0000'">
					<xsl:value-of select="normalize-space($a)"/>
						<xsl:text>-</xsl:text></xsl:if>
					<xsl:if test="normalize-space($m)!='00' and normalize-space($m)!=''" ><xsl:value-of select="normalize-space($m)"/>
						<xsl:text>-</xsl:text></xsl:if>
					<xsl:if test="normalize-space($j)!='00' and normalize-space($j)!=''" ><xsl:value-of select="normalize-space($j)"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="normalize-space($chaine_date)"/>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>
	
    <xsl:template name="format_date_old">
        <xsl:param name="chaine_date"/>
		<!-- decoupage de la chaine -->
		<xsl:variable name="j"><xsl:value-of select="substring-before($chaine_date,'.')"/></xsl:variable>
		<xsl:variable name="m"><xsl:value-of select="substring-before(substring-after($chaine_date,'.'),'.')"/></xsl:variable>
		<xsl:variable name="a"><xsl:value-of select="substring-after(substring-after($chaine_date,'.'),'.')"/></xsl:variable>
        <xsl:value-of select="normalize-space($a)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($m)"/><xsl:text>-</xsl:text><xsl:value-of select="normalize-space($j)"/>
    </xsl:template>


</xsl:stylesheet>