<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" 
xmlns:exsl="http://exslt.org/common"
extension-element-prefixes="exsl" version="1.0"
 >
    
    <xsl:param name="hasheader" /><!-- défini si on a besoin de redéfinir les headers & les page-master  -->
    <xsl:param name="date_FR"/>
    <xsl:param name="date_US"/>
    <xsl:param name="layout"/> <!-- normal(1 notice par page pdf) ou mosaique(4 notices par page pdf) -->
    <xsl:param name="xmlfile" /><!-- abrege,complet, etc  -->
    <xsl:param name="type_mosaique" /><!-- light, cote(par défaut)  -->
    <xsl:param name="paysage" /><!-- paysage (mode paysage), ''(vide par défaut=> mode portrait)  -->
    <xsl:param name="custom_fields" /><!-- xml contenant les champs customisables choisis  -->
    
    <xsl:include href="../fonctions.xsl"/><!-- ne pas enlever, fonctions.xsl contient <xsl:template name="format_date"> -->

    <xsl:output 
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
    /> 
	
	<xsl:key name="t_lex-by-id_role" match="t_lex" use="concat(../../ID_DOC,'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE)"/>
	<xsl:key name="t_pers-by-id_role" match="t_personne" use="concat(../../ID_DOC,'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE)"/>

	
	
	
    
    <!-- ************************************Remplit 1 cellule du tableau suivant le type de mosaique(light ou cote (défaut)) ************************** -->
	<xsl:template name="columns">
		<xsl:param name="nb"/>
		<xsl:param name="i"/>
		<xsl:if test="$i &lt;= $nb">
			 <fo:table-column /> <!-- column-width="concat(number(100 div $nb),'%')" -->
			<xsl:if test="$i &lt;= $nb">
				<xsl:call-template name="columns">
					<xsl:with-param name="nb" select="$nb"/>
					<xsl:with-param name="i"><xsl:value-of select="number($i + 1)"/></xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>	
	<xsl:template name="mosaique_table_1cell">
        <xsl:param name="type_mosaique" />
        <xsl:param name="show_label" />
        <xsl:param name="mos_img_height" />
        <xsl:param name="xmllist" />
        <fo:block display-align="center" text-align="center">
            <xsl:choose>
				<xsl:when test="DOC_ID_DOC_ACC!='' and t_doc_acc/t_doc_acc[@ID_DOC_ACC=current()/DOC_ID_DOC_ACC]/DA_FICHIER != ''">
				   <fo:external-graphic src="&lt;?=kDocumentUrl?&gt;/{concat(t_doc_acc/t_doc_acc[@ID_DOC_ACC=current()/DOC_ID_DOC_ACC]/DA_CHEMIN,'/',t_doc_acc/t_doc_acc[@ID_DOC_ACC=current()/DOC_ID_DOC_ACC]/DA_FICHIER)}" width="100%" height="{concat(($mos_img_height),'mm')}" content-width="scale-to-fit" content-height="scale-to-fit" />
				</xsl:when>
				<xsl:when test="DOC_ID_IMAGE!='' and ./t_image/DOC_IMG/IM_FICHIER!=''">
					<fo:external-graphic src="&lt;?=kStoryboardDir?&gt;/{concat(t_image/DOC_IMG[@ID_IMAGE=current()/DOC_ID_IMAGE]/IM_CHEMIN,'/',t_image/DOC_IMG[@ID_IMAGE=current()/DOC_ID_IMAGE]/IM_FICHIER)}" width="100%" height="{concat($mos_img_height,'mm')}" content-width="scale-to-fit" content-height="scale-to-fit" />
				</xsl:when>
                <xsl:when test="VIGNETTE!=''">
                    <fo:external-graphic src="&lt;?=kCheminLocalMedia?&gt;/{VIGNETTE}"    width="100%" height="{concat(($mos_img_height),'mm')}" content-width="scale-to-fit" content-height="scale-to-fit"  />
                </xsl:when>
            
                <xsl:otherwise>
                    <fo:external-graphic src="&lt;?=imageDir ?&gt;/nopicture.gif"     width="100%" height="{concat(($mos_img_height),'mm')}" content-width="scale-to-fit" content-height="scale-to-fit"  />
                </xsl:otherwise>
            </xsl:choose>
        </fo:block>
                
        <fo:block-container overflow="hidden"><!-- width="100%" height="7.5mm"  -->                    
			<xsl:choose>
				<xsl:when test="$custom_fields != ''">
					<xsl:variable name="row" select="child::*"/>
					<xsl:for-each select="$xmllist//element">
						<xsl:call-template name="displayElement">
							<xsl:with-param name="row" select="$row"/>
							<xsl:with-param name="show_label" select="$show_label"/>
							<xsl:with-param name="wrap-opt">no-wrap</xsl:with-param>
							<xsl:with-param name="space-after">0.1pt</xsl:with-param>
						</xsl:call-template>
					</xsl:for-each><!-- element -->
				</xsl:when>
				<xsl:otherwise>
				<fo:block font-size="8.5pt" wrap-option="no-wrap" space-after.optimum="0.1pt">
					&lt;![CDATA[<xsl:value-of select="DOC_COTE"/>]]&gt;
				</fo:block>
				<fo:block font-size="8.5pt"  wrap-option="no-wrap" space-after.optimum="0.1pt">
					&lt;![CDATA[<xsl:value-of  select="DOC_TITRE"/>]]&gt;
				</fo:block>
				</xsl:otherwise>
			</xsl:choose>
        </fo:block-container>
           
                                
    </xsl:template>
    
       <!-- **************************************************template mosaique (4 notices par page pdf (si texte pas trop long)************************** -->
    <xsl:template name="mosaique">
		<xsl:param name="iteration"/>
		<xsl:param name="fin"/>
		<xsl:param name="i"/>
		<xsl:param name="nb_total_notices"/>
        <xsl:param name="type_mosaique" />
        <xsl:param name="xmllist" />
		<xsl:if test="$iteration &lt; $fin"><!-- &lt; inférieur à -->
            <fo:table  border-width="1mm" border-style="solid" border-color="white" table-layout="fixed" width="100%" border-collapse="separate" >
				<!-- définition dynamique du nombre de colonnes-->
				<xsl:call-template name="columns">
					<xsl:with-param name="nb" select="$nb_by_line"/>
					<xsl:with-param name="i" >1</xsl:with-param>
				</xsl:call-template>
				<xsl:variable name="mos_img_height">
					<xsl:choose>
						<xsl:when test="$img_block_shape != '' and $img_block_shape = 'square'">
							<xsl:value-of select="(number(number(210 - (2 * 15)) div $nb_by_line) - 2)"/>
						</xsl:when>
						<xsl:when test="$img_block_shape != '' and $img_block_shape = '4_3'">
							<xsl:value-of select="number((number(number(210 - (2 * 15)) div $nb_by_line) - 2) * 3 div 4)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="number((number(number(210 - (2 * 15)) div $nb_by_line) - 2) * 9 div 16)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="show_label">
				<xsl:choose>
					<xsl:when test="$nb_by_line &lt; 3 ">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				
				</xsl:variable>
				
			
				<fo:table-body>
					<fo:table-row page-break-inside="avoid">
						<xsl:for-each select="t_doc">
							<!-- tout les <nb_by_line> éléments, on recrée une nouvelle ligne. 
							Le calcul est un peu "bousculé" par le fait que position() débute à 1 au lieu de 0 et que cela décale le calcul via modulo. 
							On évite aussi de créer une ligne vide au début (quand (position()-1) == 0 ) -->
							<xsl:if test="position() != 1 and number(position() - 1) mod $nb_by_line = 0 ">
								<xsl:text>&lt;/fo:table-row&gt;</xsl:text>
								<xsl:text>&lt;fo:table-row page-break-inside="avoid"&gt;</xsl:text>
							</xsl:if>
							<fo:table-cell  border-width="1mm" border-style="solid" border-color="white"  >

								<xsl:call-template name="mosaique_table_1cell">
									<xsl:with-param name="show_label" select="$show_label"/>
									<xsl:with-param name="type_mosaique" select="$type_mosaique"/>
									<xsl:with-param name="mos_img_height" select="$mos_img_height"/>
									<xsl:with-param name="xmllist" select="$xmllist"/>
								</xsl:call-template>
							</fo:table-cell>
						</xsl:for-each>
					</fo:table-row>
                </fo:table-body>
            </fo:table> 
        </xsl:if>
    </xsl:template>
    
    
    

    <xsl:template name="boucle">
                <xsl:param name="xmllist" />
                <xsl:param name="entity" />
                <xsl:param name="get_vignette" />
            
      

           
            <xsl:variable name="row" select="child::*" />
            <!-- si le docAff.xml utilisé ne contient pas de tag element pour la vignette, on l'affiche quand meme (utile quand on utilise le docAff.xml default -->
			<xsl:if test="$entity='t_doc' and count($xmllist//element[type='image' and $field='VIGNETTE']) = 0 ">
				<fo:block text-align="center">
					<xsl:choose>
					   <xsl:when test="$row[name()='DOC_ID_DOC_ACC']!='' and $row[name()='t_doc_acc']/t_doc_acc[@ID_DOC_ACC=$row[name()='DOC_ID_DOC_ACC']]/DA_FICHIER != ''">
						   <fo:external-graphic src="&lt;?=kDocumentUrl?&gt;/{concat($row[name()='t_doc_acc']/t_doc_acc[@ID_DOC_ACC=$row[name()='DOC_ID_DOC_ACC']]/DA_CHEMIN,'/',$row[name()='t_doc_acc']/t_doc_acc[@ID_DOC_ACC=$row[name()='DOC_ID_DOC_ACC']]/DA_FICHIER)}" width="100%" height="55.6mm"   content-height="scale-to-fit" />
						</xsl:when>
						<xsl:when test="$row[name()='DOC_ID_IMAGE']!='' and $row[name()='t_image']/DOC_IMG[@ID_IMAGE=$row[name()='DOC_ID_IMAGE']]/IM_FICHIER!=''">
							<fo:external-graphic src="&lt;?=kStoryboardDir?&gt;/{concat($row[name()='t_image']/DOC_IMG[@ID_IMAGE=$row[name()='DOC_ID_IMAGE']]/IM_CHEMIN,'/',$row[name()='t_image']/DOC_IMG[@ID_IMAGE=$row[name()='DOC_ID_IMAGE']]/IM_FICHIER)}" width="100%" height="55.6mm"   content-height="scale-to-fit" />
						</xsl:when>
						<xsl:when test="$row[name()='VIGNETTE']!=''">
							<fo:external-graphic src="&lt;?=kCheminLocalMedia?&gt;/{$row[name()='VIGNETTE']}" width="100%" height="55.6mm"   content-height="scale-to-fit" />
						</xsl:when>
						<xsl:otherwise>
							<fo:external-graphic src="&lt;?=imageDir ?&gt;/nopicture.gif" width="100%" height="55.6mm"  content-height="scale-to-fit" />
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</xsl:if>
			
            <!-- pour chaque t_doc de EXPORT_OPSIS, boucle sur la liste xml suivant le modèle abregé, complet-->
            <xsl:for-each select="$xmllist/data">

                <xsl:for-each select="fieldset">
                    <xsl:if test="not(no_export_fop) or no_export_fop != '1'">
                        <xsl:if test="legend!=''">
                            <fo:block font-size="12.5pt"
                                      space-before.optimum="8pt"
                                      space-after.optimum="8pt"
                                      color="black"
                                      text-align="left"
									  border-bottom="solid 1pt #E8E8E8"
									  >
                                
                                <xsl:processing-instruction name="php">print <xsl:value-of select="legend"/>;</xsl:processing-instruction> 

                            </fo:block>
                        </xsl:if>
						
                        <xsl:for-each select="element">
							<xsl:call-template name="displayElement">
								<xsl:with-param name="row" select="$row"/>
								<xsl:with-param name="show_label" >1</xsl:with-param>
								<xsl:with-param name="get_vignette" select="$get_vignette"/>
								<xsl:with-param name="font-size">10pt</xsl:with-param>
								<xsl:with-param name="wrap-opt">wrap</xsl:with-param>
							</xsl:call-template>
                        </xsl:for-each><!-- element -->
                    </xsl:if>
                </xsl:for-each><!--fieldset-->
				<xsl:for-each select="element">
					<xsl:call-template name="displayElement">
						<xsl:with-param name="row" select="$row"/>
						<xsl:with-param name="show_label" >1</xsl:with-param>
						<xsl:with-param name="get_vignette" select="$get_vignette"/>
						<xsl:with-param name="font-size">10pt</xsl:with-param>
						<xsl:with-param name="wrap-opt">wrap</xsl:with-param>
					</xsl:call-template>
				</xsl:for-each><!-- element -->
            </xsl:for-each>
    </xsl:template>
	
	<xsl:template name="displayElement">

			<xsl:param name="row"/>
			<xsl:param name="show_label"/>
			<xsl:param name="get_vignette"/>
			<xsl:param name="wrap-opt"/>
			<xsl:param name="font-size"/>
			<xsl:param name="space-after"/>
			
			
			<xsl:variable name="field" >
				<xsl:choose>
					<xsl:when test="opsfield and opsfield != ''"><xsl:value-of select="opsfield"/></xsl:when>
					<xsl:when test="chFields and chFields != ''"><xsl:value-of select="chFields"/></xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="opsfield_split">
				<xsl:if test="opsfield and opsfield != ''">
					<xsl:call-template name="tokenize">
						<xsl:with-param name="pText" select="opsfield"/>
						<xsl:with-param name="delimitor"><xsl:text>_</xsl:text></xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="label" select="label"/>
			
			<xsl:variable name="ftsize">
				<xsl:choose>
					<xsl:when test="$font-size != ''"><xsl:value-of select="$font-size"/></xsl:when>
					<xsl:otherwise><xsl:text>8.5pt</xsl:text></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:variable name="labelfontcolor">
				<xsl:choose>
					<xsl:when test="labelfontcolor != ''">
						<xsl:value-of select="labelfontcolor"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>black</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="space_after_element_block">
				<xsl:choose>
					<xsl:when test="$space-after != ''"><xsl:value-of select="$space-after"/></xsl:when>
					<xsl:otherwise><xsl:text>0.3pt</xsl:text></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			
			<!--<fo:block  border="solid 1pt" space-before.optimum='3pt' > on pourrait mettre le block juste ici('apres la boucle sur les elements) pour alléger le code mais malheureusement, cela conduira à des bloc vide(car xml non rempli) avec un margin et des espacements variables --> 
			   

			<!--debug--> 
			<!--                               <fo:block>
				<xsl:value-of select="." />
			</fo:block>-->
				
			<xsl:variable name="id" select="link_id"/>
			
			<xsl:choose><!-- choose principal de <element> -->
				<xsl:when test="type='image' and $field='VIGNETTE'">
					<xsl:choose>
						 <xsl:when test="not($get_vignette) or $get_vignette!='no'"><!--on ne doit pas afficher l'image dans cette boucle en mode paysage (car on l'affiche à gauche) mais on rentre AVANT quand même dans = vignette sinon c'est otherwise qui sera appliqué (avec affichage textuel du chemin de la vignette) -->
					<fo:block text-align="center">
						<xsl:choose>
							<xsl:when test="$row[name()='DOC_ID_DOC_ACC']!='' and $row[name()='t_doc_acc']/t_doc_acc[@ID_DOC_ACC=$row[name()='DOC_ID_DOC_ACC']]/DA_FICHIER != ''">
							   <fo:external-graphic src="&lt;?=kDocumentUrl?&gt;/{concat($row[name()='t_doc_acc']/t_doc_acc[@ID_DOC_ACC=$row[name()='DOC_ID_DOC_ACC']]/DA_CHEMIN,'/',$row[name()='t_doc_acc']/t_doc_acc[@ID_DOC_ACC=$row[name()='DOC_ID_DOC_ACC']]/DA_FICHIER)}" width="100%" height="55.6mm"   content-height="scale-to-fit" />
							</xsl:when>
							<xsl:when test="$row[name()='DOC_ID_IMAGE']!='' and $row[name()='t_image']/DOC_IMG[@ID_IMAGE=$row[name()='DOC_ID_IMAGE']]/IM_FICHIER!=''">
								<fo:external-graphic src="&lt;?=kStoryboardDir?&gt;/{concat($row[name()='t_image']/DOC_IMG[@ID_IMAGE=$row[name()='DOC_ID_IMAGE']]/IM_CHEMIN,'/',$row[name()='t_image']/DOC_IMG[@ID_IMAGE=$row[name()='DOC_ID_IMAGE']]/IM_FICHIER)}" width="100%" height="55.6mm"   content-height="scale-to-fit" />
							</xsl:when>
							<xsl:when test="$row[name()='VIGNETTE']!=''">
								<fo:external-graphic src="&lt;?=kCheminLocalMedia?&gt;/{$row[name()=$field]}" width="100%" height="55.6mm"   content-height="scale-to-fit" />
							</xsl:when>
							<xsl:otherwise>
								<fo:external-graphic src="&lt;?=imageDir ?&gt;/nopicture.gif" width="100%" height="55.6mm"  content-height="scale-to-fit" />
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
						 </xsl:when>
						 <xsl:otherwise></xsl:otherwise>
					</xsl:choose>
					
				</xsl:when>
				<!--******************************************************************************************************-->
				<xsl:when test="type='MAT_INFO'">
					<xsl:if test="$row[name()='MAT_INFO']/xml/file/*[name()=$field]!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
							<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
								<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> :&#160;</fo:inline>
							</xsl:if>
							<fo:inline>
								<xsl:value-of select="$row[name()='MAT_INFO']/xml/file/*[name()=$field]"/>
						&#160;
							</fo:inline>
						</fo:block>			
					</xsl:if>
				</xsl:when>
				<!--******************************************************************************************************-->
				<xsl:when test="type='EXIF'">
					<xsl:if test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
							<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
								<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> :&#160;</fo:inline>
							</xsl:if>
							<fo:inline>
								<xsl:value-of select="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]"/>
								&#160;
							</fo:inline>
						</fo:block>			
					</xsl:if>
				</xsl:when>
					
				<!--******************************************************************************************************-->
					
				<xsl:when test="type='EXIF_EXPOSURE_PROG'">
					<xsl:if test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]!='' and $row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]!='0'">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> :&#160;</fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:choose>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='1'">
									<xsl:processing-instruction name="php">print kManual;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='2'">
									<xsl:processing-instruction name="php">print kNormalProgram;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='3'">
									<xsl:processing-instruction name="php">print kAperturePriority;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='4'">
									<xsl:processing-instruction name="php">print kShutterPriority;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='5'">
									<xsl:processing-instruction name="php">print kCreativeProgram;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='6'">
									<xsl:processing-instruction name="php">print kActionProgram;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='7'">
									<xsl:processing-instruction name="php">print kPortraitMode;</xsl:processing-instruction>
								</xsl:when>
								<xsl:when test="$row[name()='MAT_INFO']/xml/file/EXIF/EXIF/*[name()=$field]='8'">
									<xsl:processing-instruction name="php">print kLandscapeMode;</xsl:processing-instruction>
								</xsl:when>
							</xsl:choose>
					&#160;
						</fo:inline>
	</fo:block>			
					</xsl:if>
				</xsl:when>
					
				<!--******************************************************************************************************-->
					
					
				<xsl:when test="type='IFD0'">
					<xsl:if test="$row[name()='MAT_INFO']/xml/file/EXIF/IFD0/*[name()=$field]!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> :&#160;</fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:value-of select="$row[name()='MAT_INFO']/xml/file/EXIF/IFD0/*[name()=$field]"/>
					&#160;
						</fo:inline>
						</fo:block>			
					</xsl:if>
				</xsl:when>
					
				<!--******************************************************************************************************-->
					
				<xsl:when test="(type='PERS' and role!='')
				or(exsl:node-set($opsfield_split)/tag[position() = 1] = 'P' and exsl:node-set($opsfield_split)/tag[position() = 3] != '')">
					<xsl:variable name="role">
						<xsl:choose>
							<xsl:when test="exsl:node-set($opsfield_split)/tag[position() = 3]"><xsl:value-of select="exsl:node-set($opsfield_split)/tag[position() = 3]"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="role"/></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
					<xsl:if test="$row[name()='t_pers_lex']//t_personne[(ID_TYPE_DESC=$field or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]) and DLEX_ID_ROLE=$role]/PERS_NOM!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> :&#160;</fo:inline>
						</xsl:if>
						
						<fo:inline>
							<xsl:for-each select="$row[name()='t_pers_lex']//t_personne[(ID_TYPE_DESC=$field or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]) and DLEX_ID_ROLE=$role]">
									<xsl:value-of select="PERS_PRENOM"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="PERS_NOM"/>
									<xsl:if test="position()!=last()">
										<xsl:text>, </xsl:text>
									</xsl:if>
							</xsl:for-each>
						</fo:inline>
						</fo:block>
					</xsl:if>
				</xsl:when>
					
					
				<!--******************************************************************************************************-->

					
					
				<xsl:when test="type='PERS' or(exsl:node-set($opsfield_split)/tag[position() = 1] = 'P')">
					<xsl:if test="$row[name()='t_pers_lex']//t_personne[ID_TYPE_DESC=$field  or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]]/PERS_NOM!=''">
						<xsl:variable name="condition_role" select="not(dont_show_role) or dont_show_role=''"/>
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> :&#160;</fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:for-each select="$row[name()='t_pers_lex']//t_personne[ID_TYPE_DESC=$field or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]][(not($condition_role)) or (count(.|key('t_pers-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[1])=1)]">
								<xsl:if test="$condition_role and DLEX_ID_ROLE!=''"><xsl:value-of select="ROLE"/><xsl:text> : </xsl:text></xsl:if>
								<xsl:for-each select="current()[not($condition_role)] | key('t_pers-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[$condition_role]">
									<xsl:value-of select="PERS_PRENOM"/>
									<!--pour eviter d'avoir un blanc disgracieux en 1ère ligne alors que pers_prenom est vide-->
									<xsl:if test="PERS_PRENOM!=''" >
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="PERS_NOM"/>
									<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
								</xsl:for-each>
								<xsl:if test="position()!=last()">
									<xsl:choose>
										<xsl:when test="$condition_role">
											<xsl:text> - </xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>, </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
						</fo:inline>
						</fo:block>
					</xsl:if>
				</xsl:when>
				
			
				<!--******************************************************************************************************-->
					
					
					
				<xsl:when test="((type='DOC_LEX' or type='LEX') and role!='')
				or(exsl:node-set($opsfield_split)/tag[position() = 1] = 'L' and exsl:node-set($opsfield_split)/tag[position() = 3] != '')">
					<xsl:variable name="role">
						<xsl:choose>
							<xsl:when test="exsl:node-set($opsfield_split)/tag[position() = 3]"><xsl:value-of select="exsl:node-set($opsfield_split)/tag[position() = 3]"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="role"/></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<!--xsl:if test="$row[name()='t_doc_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$role]/t_lex/LEX_TERME!=''"-->
					<xsl:if test="$row[name()='t_doc_lex']//t_lex[(ID_TYPE_DESC=$field or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]) and DLEX_ID_ROLE=$role]/LEX_TERME!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
							<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
								<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction>  :&#160;</fo:inline>
							</xsl:if>
							<fo:inline>
								<!--xsl:for-each select="$row[name()='t_doc_lex']/TYPE_DESC[@ID_TYPE_DESC=$field]/t_rol[@DLEX_ID_ROLE=$role]">
									<xsl:for-each select="t_lex"-->
									<xsl:for-each select="$row[name()='t_doc_lex']//t_lex[(ID_TYPE_DESC=$field or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]) and DLEX_ID_ROLE=$role]">
										<xsl:variable name="id_lex">
											<xsl:value-of select="ID_LEX"/>
										</xsl:variable>
										<xsl:value-of select="LEX_TERME"/>
										<xsl:if test="LEX_PRECISION!=''" >
											<xsl:text>(</xsl:text>
											<xsl:value-of select="LEX_PRECISION"/>
											<xsl:text>)</xsl:text>
										</xsl:if>
										<xsl:choose>
											<xsl:when test="position()!=last()">
												<xsl:text>, </xsl:text>
											</xsl:when>
											<xsl:otherwise></xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
									<!--/xsl:for-each>
								</xsl:for-each-->
							</fo:inline>
						</fo:block>				
					</xsl:if>
				</xsl:when>
							
				<!--******************************************************************************************************-->

			 	<xsl:when test="(type='DOC_LEX' or type='LEX')
						or(exsl:node-set($opsfield_split)/tag[position() = 1] = 'L')">
					<xsl:if test="$row[name()='t_doc_lex']//t_lex[ID_TYPE_DESC=$field or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]]/LEX_TERME!=''">
						<xsl:variable name="condition_role" select="not(dont_show_role) or dont_show_role=''"/>
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> :&#160;</fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:for-each select="$row[name()='t_doc_lex']//t_lex[ID_TYPE_DESC=$field or ID_TYPE_DESC = exsl:node-set($opsfield_split)/tag[position() = 2]][(not($condition_role)) or (count(.|key('t_lex-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[1])=1)]">
								<xsl:if test="$condition_role and DLEX_ID_ROLE!=''"><xsl:value-of select="ROLE"/><xsl:text> : </xsl:text></xsl:if>
								<xsl:for-each select="current()[not($condition_role)] | key('t_lex-by-id_role',concat($row[name()='ID_DOC'],'_',ID_TYPE_DESC,'_',DLEX_ID_ROLE))[$condition_role]">
									<xsl:variable name="id_lex">
										<xsl:value-of select="ID_LEX"/>
									</xsl:variable>
									<xsl:value-of select="LEX_TERME"/>
									<xsl:if test="LEX_PRECISION!=''" >
										<xsl:text>(</xsl:text>
										<xsl:value-of select="LEX_PRECISION"/>
										<xsl:text>)</xsl:text>
									</xsl:if>
									<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
								</xsl:for-each>
								<xsl:if test="position()!=last()">
									<xsl:choose>
										<xsl:when test="$condition_role">
											<xsl:text>&#160;-&#160;</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>, </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</xsl:for-each>
						</fo:inline>
						</fo:block>
					</xsl:if>
				</xsl:when>
				
				
				<!--******************************************************************************************************-->

				<xsl:when test="type='DOC_XML'">
					<xsl:if test="$row[name()='DOC_XML']/XML/DOC/*[name()=$field]!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:value-of select="$row[name()='DOC_XML']/XML/DOC/*[name()=$field]"/>
						</fo:inline>
						</fo:block>
					</xsl:if>
				</xsl:when>
					
				<!--******************************************************************************************************-->	
				<xsl:when test="contains($field,'DOC_ID_') and $row[name()='DOC_XML']/XML/DOC/*[name()=substring-after($field,'DOC_ID_')]!=''">
					<xsl:if test="$row[name()='DOC_XML']/XML/DOC/*[name()=substring-after($field,'DOC_ID_')]!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:value-of select="$row[name()='DOC_XML']/XML/DOC/*[name()=substring-after($field,'DOC_ID_')]"/>
						</fo:inline>
						</fo:block>
					</xsl:if>
				</xsl:when>
					<!--******************************************************************************************************-->
					
				<xsl:when test="type='VAL' or (exsl:node-set($opsfield_split)/tag[position() = 1] = 'V')">
					<!-- ancienne structure -->
					<!--xsl:if test="$row[name()='t_doc_val']/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val/VALEUR!=''"-->
					<xsl:if test="$row[name()='t_doc_val']/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val/VALEUR!='' or $row[name()='t_doc_val']//t_val[ID_TYPE_VAL=exsl:node-set($opsfield_split)/tag[position() = 2]]!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					
						<fo:inline>
							<xsl:for-each select="$row[name()='t_doc_val']//t_val[ID_TYPE_VAL=$field  or ID_TYPE_VAL=exsl:node-set($opsfield_split)/tag[position() = 2]]">
								<xsl:value-of select="VALEUR"/>
								<xsl:if test="position()!=last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</fo:inline>
						</fo:block>				
					</xsl:if>
				</xsl:when> 
				<xsl:when test="type='DOC_VAL' or exsl:node-set($opsfield_split)/tag[position() = 1] = 'V'">
					<!-- ancienne structure -->
					<!--xsl:if test="$row[name()='t_doc_val']/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val/VALEUR!=''"-->
					<xsl:if test="$row[name()='DOC_VAL']/XML/*[name()=$field]!='' or $row[name()='DOC_VAL']/XML/*[name()=exsl:node-set($opsfield_split)/tag[position() = 2]]!=''">
						<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<fo:inline color="{$labelfontcolor}">
							<xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
					
						<fo:inline>
							<xsl:for-each select="$row[name()='DOC_VAL']/XML/*[name()=$field or name()=exsl:node-set($opsfield_split)/tag[position() = 2]]">
								<xsl:value-of select="VALEUR"/>
								<xsl:if test="position()!=last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>
						</fo:inline>
						</fo:block>				
					</xsl:if>
				</xsl:when>
					
				<!--******************************************************************************************************-->
							   
				<xsl:when test="type='MAT_VAL'">
					<xsl:if test="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='VISIO']/t_mat_val/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val/VALEUR!=''">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:for-each select="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='VISIO']/t_mat_val/TYPE_VAL[@ID_TYPE_VAL=$field]/t_val">
								<xsl:value-of select="VALEUR"/>
								<xsl:if test="position()!=last()">
									<xsl:text>, </xsl:text>
								</xsl:if>
							</xsl:for-each>&#160;
						</fo:inline>
						</fo:block>
					</xsl:if>
				</xsl:when>
					
					
				<!--******************************************************************************************************-->

					
				<xsl:when test="$field='DOC_ID_MEDIA' or contains(opsfield,'_ID_MEDIA')">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>
						<xsl:choose>
							<xsl:when test="$row[name()=$field]='V'">
								<xsl:processing-instruction name="php">echo  kVideo</xsl:processing-instruction>
							</xsl:when>
							<xsl:when test="$row[name()=$field]='A'">
								<xsl:processing-instruction name="php">print kAudio</xsl:processing-instruction>
							</xsl:when>
							<xsl:when test="$row[name()=$field]='P'">
								<xsl:processing-instruction name="php">print kPhoto</xsl:processing-instruction>
							</xsl:when>
							<xsl:when test="$row[name()=$field]='R'">
								<xsl:processing-instruction name="php">print kReportage</xsl:processing-instruction>
							</xsl:when>
							<xsl:when test="$row[name()=$field]='D'">
								<xsl:processing-instruction name="php">print kImprime</xsl:processing-instruction>
							</xsl:when>
						</xsl:choose>&#160;
					</fo:inline>
					</fo:block>
				</xsl:when>
					
					
				<!--******************************************************************************************************-->

					
				<xsl:when test="type='filesize_format'">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>
						<xsl:processing-instruction name="php">echo filesize_format(<xsl:value-of select="$row[name()=$field]" />);</xsl:processing-instruction>
					&#160;
					</fo:inline>
	</fo:block>			
				</xsl:when>
					
				<!--******************************************************************************************************-->

					
				<xsl:when test="type='booleen' and $field='DOC_ACCES'">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>
						<xsl:choose>
							<xsl:when test="contains($row[name()=$field],'1')">
								<xsl:processing-instruction name="php">print kPublie</xsl:processing-instruction>
							</xsl:when>
							<xsl:otherwise>
								<xsl:processing-instruction name="php">print kNonPublie</xsl:processing-instruction>
							</xsl:otherwise>
						</xsl:choose>
					&#160;
					</fo:inline>
	</fo:block>			
				</xsl:when>
					
					
				<!--******************************************************************************************************-->
					
					
				<xsl:when test="type='booleen'">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>
						<xsl:choose>
							<xsl:when test="contains($row[name()=$field],'1')">
								<xsl:processing-instruction name="php">print kOui</xsl:processing-instruction>
							</xsl:when>
							<xsl:otherwise>
								<xsl:processing-instruction name="php">print kNon</xsl:processing-instruction>
							</xsl:otherwise>
						</xsl:choose>
					&#160;
					</fo:inline>
				</fo:block>			
				</xsl:when>
					
					
				<!--******************************************************************************************************-->
					
					
				<xsl:when test="type='multi' and starts-with($field,'MAT_') and  $row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[local-name()=$field]!=''">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>&lt;![CDATA[<xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[local-name()=$field]"/>]]&gt;&#160;</fo:inline>
					</fo:block>
				</xsl:when>
					
				<!--******************************************************************************************************-->
					
					
					
				<xsl:when test="starts-with($field,'MAT_') and  $row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[local-name()=$field]!=''">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>&lt;![CDATA[<xsl:value-of select="$row[name()='t_doc_mat']/DOC_MAT/t_mat[MAT_TYPE='MASTER']/*[local-name()=$field]"/>]]&gt;&#160;</fo:inline>
					</fo:block>
				</xsl:when>
					
				<!--******************************************************************************************************-->
				   
					
				<xsl:when test="type='multi' and ($field='DOC_RES' or $field='DOC_RECOMP' or $field='DOC_COMMENT'or $field='DOC_NOTES') and $row[name()=$field]!='' ">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>&lt;![CDATA[<xsl:value-of select="$row[name()=$field]"/>]]&gt;&#160;</fo:inline>
					</fo:block>
				</xsl:when>
					
					
				<!--******************************************************************************************************-->
					
					
				<xsl:when test="type='multi' and $row[name()=$field]!='' ">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
					<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
						<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
					</xsl:if>
					<fo:inline>&lt;![CDATA[<xsl:value-of select="$row[name()=$field]"/>]]&gt;&#160;</fo:inline>
					</fo:block>
				</xsl:when>
					
				<!--******************************************************************************************************-->
					
					
					
				<xsl:when test=" contains($field,'DOC_DATE') and $row[name()=$field]!='' and $row[name()=$field]!='0000-00-00' and $row[name()=$field]!='00:00:00 ' and $row[name()=$field]!='--'">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
						<fo:inline>
							<xsl:call-template name="format_date">
								<xsl:with-param name="chaine_date">
									<xsl:value-of select="$row[name()=$field]"/>
								</xsl:with-param>
							</xsl:call-template>&#160;
						</fo:inline>
					</fo:block>			
				</xsl:when>
			
				<!--******************************************************************************************************-->
				<xsl:when test="type='count_chidren' and $row[name()=$field] != ''">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
						<fo:inline>&lt;![CDATA[<xsl:value-of select="count($row[name()=$field]/*)"/>]]&gt;&#160;</fo:inline>
					</fo:block>
				</xsl:when>
				<!--******************************************************************************************************-->
				<xsl:when test="$row[name()=$field]!='' and $row[name()=$field]!='0000-00-00' and $row[name()=$field]!='00:00:00 ' and $row[name()=$field]!='--'">
					<fo:block font-size="{$ftsize}" space-after.optimum="{$space_after_element_block}" wrap-option="{$wrap-opt}">
						<xsl:if test="$show_label != '0' and $show_label != '' and $label!=''">
							<fo:inline color="{$labelfontcolor}"><xsl:processing-instruction name="php">print <xsl:value-of select="$label"/>;</xsl:processing-instruction> : </fo:inline>
						</xsl:if>
					<fo:inline>&lt;![CDATA[<xsl:value-of select="$row[name()=$field]"/>]]&gt;&#160;</fo:inline>
					</fo:block>			
				</xsl:when>
					
				<!--******************************************************************************************************-->
					
			 
			</xsl:choose><!-- FIN choose principal de <element> -->
			<!--</fo:block>--> 
	</xsl:template>
	

    <!-- **************************************************template normal************************************************** -->

    <xsl:template name='normal'>
        <xsl:param name="xmllist" />
        <xsl:variable name="entity" select="$xmllist/data/entity"/>
        <!-- Boucle sur chaque t_doc de EXPORT_OPSIS, attention parfois on a dans EXPORT_OPSIS des occurrences vides , donc on met [@id_doc!=''] -->
	   <xsl:for-each select="child::*[name()=$entity]">
           <fo:block ><xsl:if test="position()!=1"><xsl:attribute name="break-before">page</xsl:attribute></xsl:if> <!--permet de passer à la page suivante pour chaque nouveau doc de la boucle-->
            <xsl:call-template name="boucle">
                <xsl:with-param name="xmllist" select="$xmllist"/>
                <xsl:with-param name="entity" select="$entity"/>
            </xsl:call-template>
              </fo:block>
        </xsl:for-each><!--         <xsl:for-each select="child::*[name()=$entity]">-->
    </xsl:template>
    
    <!-- **************************************************END template normal************************************************** --> 

    
     <!-- **************************************************template normal-paysage************************************************** -->

    <xsl:template name='normal-paysage'>
        <xsl:param name="xmllist" />
        
        
        <xsl:variable name="entity" select="$xmllist/data/entity"/>
        <!-- Boucle sur chaque t_doc de EXPORT_OPSIS, attention parfois on a dans EXPORT_OPSIS des occurrences vides , donc on met [@id_doc!=''] -->
        <xsl:for-each select="child::*[name()=$entity][@id_doc!='']">
            <!--table à 1 seule ligne à 2 colonne, vignette dans la colonne de gauche, texte dans la colonne de droite-->
            <fo:block space-before="5mm">

            <fo:table>
                <fo:table-column/>
                <fo:table-column/>

                <fo:table-body>

                    <fo:table-row>
                        <fo:table-cell><!--ATTENTION table-cell DOIT CONTENIR un fo:block (voir dynamiquement contenu dans le template appelé) sinon fop foire -->
                            <fo:block display-align="center" text-align="center">
            
                                <xsl:choose>
                                    <xsl:when test="VIGNETTE!=''">
                                        <fo:external-graphic src="&lt;?=kCheminLocalMedia?&gt;/{VIGNETTE}"    width="100%" height="50mm" content-width="scale-to-fit" content-height="scale-to-fit"  />
                                    </xsl:when>
            
                                    <xsl:otherwise>
                                        <fo:external-graphic src="&lt;?=imageDir ?&gt;/nopicture.gif"     width="100%" height="50mm" content-width="scale-to-fit" content-height="scale-to-fit"  />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </fo:block>
                        </fo:table-cell>
                    
                    
                        <fo:table-cell>
                        
                            <xsl:call-template name="boucle">
                                <xsl:with-param name="xmllist" select="$xmllist"/>
                                <xsl:with-param name="get_vignette" select='"no"'/>
                            </xsl:call-template>
                        
                        </fo:table-cell>
        


                    </fo:table-row>


                </fo:table-body>
          
          
            </fo:table>  
            </fo:block>

        </xsl:for-each><!--         <xsl:for-each select="child::*[name()=$entity]">-->
        
    </xsl:template>
    
    <!-- **************************************************END template normal-paysage************************************************** --> 
    
    <xsl:template match="EXPORT_OPSIS">
		<xsl:variable name="xmllist" select="document($custom_fields)[$custom_fields and $custom_fields!='']|document($xmlfile)[not($custom_fields and $custom_fields!='')]" />
        <xsl:variable name="paysage_choice">
            <xsl:choose>
                <xsl:when test="$paysage='1'">
                    <xsl:text>A4-paysage</xsl:text>
                </xsl:when>
                <xsl:otherwise><xsl:text>A4</xsl:text></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:if test="$hasheader= '1'">&lt;fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"&gt;
            <!-- **************************************************layout information************************************************** -->
            <fo:layout-master-set>
                
                <fo:simple-page-master master-name="A4"
                                       page-height="29.7cm"
                                       page-width="21cm"
                                       margin-top="1cm"
                                       margin-left="1.5cm"
                                       margin-right="1.5cm">
                    <fo:region-body margin-top="1.7cm" margin-bottom="1.5cm"/>

                    <fo:region-before extent="1.5cm"  margin-top="1cm"/>
                    <fo:region-after extent="1cm"/>
                </fo:simple-page-master>
                
                
                <fo:simple-page-master master-name="A4-paysage"
                                       page-height="21cm"
                                       page-width="29.7cm"
                                       margin-top="1cm"
                                       margin-left="1.5cm"
                                       margin-right="1.5cm">
                    <fo:region-body margin-top="3cm" margin-bottom="1.5cm"/>

                    <fo:region-before extent="3cm"/>
                    <fo:region-after extent="1cm"/>
                </fo:simple-page-master>
                
                
            </fo:layout-master-set>
            <!-- **************************************************end: defines page layout************************************************** -->
  
			
  
                        
            &lt;fo:page-sequence master-reference="<xsl:value-of select="$paysage_choice"/>"&gt;
                            
                 
                
                <!-- **************************************************footer ************************************************** -->
                <!-- *****ATTENTION doit être AVANT le xsl-region-body a cause de ref-id="end-of-document" ******* -->
                <fo:static-content flow-name="xsl-region-after">
                    <!--        <fo:block>
                    <fo:inline text-align="start">16-03-2015</fo:inline>
                    <fo:inline text-align="center">Mémorial de la Shoah</fo:inline>
                    <fo:inline text-align="end">page <fo:page-number/></fo:inline></fo:block>-->
                    <fo:block  font-size="8pt" >
                        <fo:table table-layout="fixed" width="100%">
                            <fo:table-column column-width="33.33%"/>
                            <fo:table-column column-width="33.33%"/>
                            <fo:table-column column-width="33.33%"/>

      
                            <fo:table-body>

                                <fo:table-row>
                                    <fo:table-cell text-align="start">
                                        <fo:block>
                                            <xsl:value-of select="$date_FR"/>
                                        </fo:block>
                                    </fo:table-cell>
									<xsl:if test="$name_site != ''">
										<fo:table-cell text-align="center" >
											<fo:block><xsl:value-of select="$name_site"/></fo:block>
										</fo:table-cell>
									</xsl:if>
                                    <fo:table-cell  text-align="end">
                                        <fo:block>page <fo:page-number/>/<fo:page-number-citation
                                                ref-id="last-page"/>
                                        </fo:block>
                                    </fo:table-cell>

                                </fo:table-row>


                            </fo:table-body>
          
          
                        </fo:table>  
                    </fo:block>
        
                </fo:static-content>
                <!-- ************************************************** END footer ************************************************** -->
                
                <!-- **************************************************header ************************************************** -->
				<xsl:if test="$logo_site != ''">
                <fo:static-content flow-name="xsl-region-before" height="15mm">
                    <fo:block  text-align="start">
                        <!--fo:external-graphic src="&lt;?=imageDir ?&gt;/{$logo_site}" width="100%" content-height="scale-to-fit" height="15mm" /-->
                        <fo:external-graphic src="&lt;?=imageDir ?&gt;/{$logo_site}" width="100%" content-height="scale-to-fit" height="15mm" >
						</fo:external-graphic>
                    </fo:block>
                </fo:static-content>
                </xsl:if>
				<!-- **************************************************END header ************************************************** -->         
                &lt;fo:flow flow-name="xsl-region-body"&gt;
		</xsl:if> 
                    <xsl:choose>
                        <xsl:when test="$layout='mosaique'">
                             <!--
							L'affichage dépends du nombre d'éléments par ligne souhaité (renseigné dans $nb_by_line)
                        
                            -Le mode mosaique, n'a pas besoin de xmllist car ce sont toujours le même nb limité de champs quelque soit le type de notice
                            -ex pour 5 notices,t_doc dans EXPORT_OPSIS, il va faire un loop de 3 (pour afficher 3 table à la suite, la dernière table n'affichant que le 1er td)
                            -->
                            
                            <xsl:variable name="nb_notices">
                                <xsl:value-of select="count(t_doc)"/>
                            </xsl:variable>
       
                            <xsl:variable name="nb_lignes_tableau">
                                <xsl:choose>
                                    <!-- Nombre pair -->
                                    <xsl:when test="$nb_notices mod 2 =0">
                                        <xsl:value-of select="$nb_notices div 2" />
                                    </xsl:when>
                                    <!-- Nombre impair -->
                                    <xsl:otherwise>
                                        <xsl:value-of select="floor($nb_notices div 2)+1" />
                                    </xsl:otherwise>
                                </xsl:choose>           
                            </xsl:variable>   
                              
        
                            <xsl:call-template name="mosaique">
                                <xsl:with-param name="iteration" select="0"/>
                                <xsl:with-param name="i" select="0"/>
                                <xsl:with-param name="fin" select="$nb_lignes_tableau"/>
                                <xsl:with-param name="nb_total_notices" select="$nb_notices"/>
                                <xsl:with-param name="type_mosaique" select="$type_mosaique"/>
                                <xsl:with-param name="xmllist" select="$xmllist"/>
                            </xsl:call-template>
                            
                        </xsl:when>
                        
                        
                        <xsl:otherwise><!-- layout normal-->
                            <xsl:choose>
                                <xsl:when test="$paysage='1'">
                                    <xsl:call-template name="normal-paysage">
                                        <xsl:with-param name="xmllist" select="$xmllist"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="normal">
                                        <xsl:with-param name="xmllist" select="$xmllist"/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose> 
    </xsl:template>
</xsl:stylesheet>
