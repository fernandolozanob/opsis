<?php
	require_once(coreDir."/controller/controller_doc.php");
	require_once(coreDir."/view/view.php");
	
	global $db;
	error_reporting(0);
	$myPage=Page::getInstance();
	
	$_REQUEST['sessVar'] = "recherche_EXT";
	$_POST["F_doc_form"] = 1;
	$_GET['mode'] = 'carto';
	$_SESSION["recherche_EXT"]=$_SESSION["recherche_DOC"];

	$_SESSION["recherche_EXT"]['docListeXSL'] = "docListeMini.xsl";
	$_SESSION["recherche_EXT"]['nbLignes'] = "5";
	$_REQUEST['style']= "";

	if (!empty($_GET['refine'])) {
		$_REQUEST['refine']=$_GET['refine'];
		foreach ($_SESSION["recherche_DOC"]["form"] as $k=>$field) {
			$_POST['chFields'][$k] = $field["FIELD"];
			$_POST['chValues'][$k] = $field["VALEUR"];
			$_POST['chTypes'][$k] = $field["TYPE"];
			$_POST['chOps'][$k] = $field["OP"];
			$_POST['chLibs'][$k] = $field["LIB"];
			$_POST['chValues2'][$k] = $field["VALEUR2"];
			$_POST['chNoHLs'][$k] = "1";
			$_POST['chNoHist'][$k] = "1";
		}

		$refine_ori = $_SESSION["recherche_DOC"]["refine"];

		$_REQUEST['reset_refine'] = "";
		$tmp = array(array("label_facet" => 'coor_lg',
						"type" => 'coor_lg',
						"value" => $_GET['refine'],
						"readable_value" => ""));
						
		$arrRef = json_decode($_SESSION["recherche_DOC"]["refine"], true);
		if (empty($arrRef)) $arrRef = array();
		$tmp = array_merge($tmp, $arrRef);
		$_REQUEST['refine'] = json_encode($tmp);

		// Pour récupération variable refine_title dans XSL
		$myPage->urlaction="docListe";
		$_SESSION['USER']['layout_opts'][$myPage->urlaction]['refine_title']=$db->GetOne("select lex_terme from t_lexique where lex_coor=".$db->Quote($_GET['refine']));
		
		$crtl = new Controller_doc();
		$crtl->do_docListe();
		
		$view = new View(null);
		$view->setParams($crtl->params);
		$view->dspList();
	}
