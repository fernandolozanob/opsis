<?

        global $db;
        $nbPage = 0;
        if (!empty($_GET['page']))
            $nbPage += intval(htmlentities($_GET['page'])) - 1;
        $nbLigneAttendu = 5;
		$offset = $nbLigneAttendu * $nbPage;
		
		global $field;
		if(!empty($_GET['field'])){
				$field = htmlentities($_GET['field']);
		}else{
			$field = "l.id_lex";
		}
		//var_dump($field);

		$tab = array(array("typL", "Type"),
					array("l.lex_terme", "Terme"),
					array("el.etat_lex", "Statut"),
					array("syn.lex_terme", "Synonymes"),
					array("par.lex_terme", "Parent"),
					array("nb_doc", "Doc.liés"));

		$res = $db->getAll ("SELECT l.id_lex , tl.type_lex AS typL , l.lex_terme ,el.etat_lex ,l.lex_id_syn , syn.lex_terme AS syn, par.lex_terme AS par , l.lex_id_gen , count(dl.id_lex) as nb_doc
							FROM t_lexique l
							LEFT OUTER JOIN t_etat_lex el 
							ON el.id_etat_lex =  l.id_lex 
							AND el.id_lang = l.id_lang

							LEFT OUTER JOIN t_lexique par
							ON l.lex_id_gen = par.id_lex 

							LEFT OUTER JOIN t_lexique syn 
							ON l.lex_id_syn  = syn.id_lex 

							LEFT OUTER JOIN t_doc_lex dl
							ON dl.id_lex = l.id_lex AND dl.id_doc > 0

							LEFT OUTER JOIN t_type_lex tl
							ON tl.id_type_lex = l.lex_id_type_lex

							WHERE l.lex_id_etat_lex = '1' 
							AND l.id_lang = 'FR'

							GROUP BY  l.id_lex , tl.type_lex , l.lex_terme ,el.etat_lex ,l.lex_id_syn , syn.lex_terme , par.lex_terme, l.lex_id_gen 
							ORDER BY $field
							LIMIT 5 OFFSET $offset;");
		$NbLigne = count($res);

		include_once(designDir . "/include/fonctionTable.inc.php");

	?>

<table class="tableResults" id="docListe" align="center" width="97%" cellspacing="0">
		<tr class="resultsHead_row">
			<?
			for($i = 0 ;$i <= count($tab);$i++){
				CreateHeader($tab[$i][0] ,$tab[$i][1],$field,"TH" );
			}
			$data = array();
			for($i = 0 ; $i <= $NbLigne-1 ; $i++){
				$data[$i] = array($res[$i]["TYPL"],
							'<a class="" href="index.php?urlaction=lexSaisie&id_lex='.$res[$i]["ID_LEX"].'">'.$res[$i]["LEX_TERME"] ,
							$res[$i]["ETAT_LEX"],
							$res[$i]["SYN"],
							$res[$i]["PAR"],
							$res[$i]["NB_DOC"],
							'<a class="" href="index.php?urlaction=lexSaisie&id_lex='.$res[$i]["ID_LEX"].'">
								<img src="design/images/icn-edit.png" style="height:13px;" title="Modifier" border="0">
							</a>');
				}
				CreateTable($data);	
		?>
</table>
