<?

$id_doc = (int) $_REQUEST['id_doc'];
$id_mat = (int) $_REQUEST['id_mat'];

$lang = trim(filter_var($_REQUEST['lang'], FILTER_SANITIZE_STRING));

if(kDatabaseName=="demo" && LOADED_APPLICATION_ENV=="production"){
	// ON NE FAIT PAS DE TRNASCRIPTION EN DEMO PROD
} elseif ($id_doc > 0 && $id_mat > 0 && $lang != '') {

    require_once(modelDir.'model_materiel.php');
    $myMatTrans = new Materiel;
    $myMatTrans->t_mat['ID_MAT'] = $id_mat;
    $myMatTrans->getMat();

    $path = kVideosDir . $myMatTrans->t_mat['MAT_CHEMIN'] . '/' . $myMatTrans->t_mat['MAT_NOM'];
    $path = stripExtension($path) . '_' . strtolower($lang);

    $mats[0]["ID_MAT"] = $id_mat;
    $mats[0]["ID_DOC"] = $id_doc;
    $mats[0]['FILE_NAME_MASK']['suffixe'] = '_' . strtolower($lang);
//    $myPrms["job_param"] = '<INHERIT_LANG>' . $lang . '</INHERIT_LANG>';
    $myPrms["job_param"] = '<LANG>' . $lang . '</LANG>';
    $myPrms['lancement'] = "proc";
    $myPrms["id_proc"] = 20;

    include(includeDir . "jobListe.php");
}

// if(!empty($path_tl)){
// $myPrms["job_param"]= '<INHERIT_FILETRANSCRIPT>'.$path_tl.'</INHERIT_FILETRANSCRIPT>';
// }	
// if($type == "audio" && !empty($path_tl)){
// $myPrms["id_proc"] = 9;
// }
// else if ($type == "video" && !empty($path_tl)){
// $myPrms["id_proc"] = 10;
// }
// else if($type == "audio"){
// $myPrms["id_proc"] = 7;
// }
// else if ($type == "video"){
// $myPrms["id_proc"] = 8;
// }
// if(isset($type) && $type=='delete' && !empty($id_doc)){
// global $db;
// $db->GetOne("UPDATE t_doc SET DOC_TRANSCRIPT='' WHERE ID_DOC=".$id_doc);
// $db->GetOne("DELETE  FROM t_doc_mat dm WHERE id_doc=".$id_doc." and id_mat IN (SELECT id_mat FROM t_mat WHERE mat_format='TXT' OR mat_format='XML' OR mat_format='SRT')");
// if(!empty($id_mat))
// $db->GetOne("DELETE  FROM t_job dm WHERE job_id_mat=".$id_mat." AND ((job_id_proc=7) OR (job_id_proc=8) OR (job_id_proc=9) OR (job_id_proc=10) OR (job_id_etape=502) OR (job_id_etape=501))");
// }
?>
