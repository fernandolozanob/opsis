<?php
 global $db;
 $nbPage = 0;
        if (!empty($_GET['page']))
            $nbPage += intval(htmlentities($_GET['page'])) - 1;
        $nbLigneAttendu = 5;
        $offset = $nbLigneAttendu * $nbPage;
        global $field;
		if(!empty($_GET['field'])){
				$field = htmlentities($_GET['field']);
		}else{
			$field = "id_usager";
		}
$titel = array(array("us_nom" , "Nom"),
                array("us_prenom" , "Prénom"),
                array("us_societe" , "Société"),
                array("us_login" , "Login"),
                array("TypeU" , "Profil"),
                array("EtatU" , "Statut"));

$res = $db->getAll ("SELECT id_usager, us_nom, us_prenom , us_societe , us_login ,tu.type_usager AS TypeU ,eu.etat_usager AS EtatU
                    FROM t_usager u
                    LEFT OUTER JOIN t_type_usager tu ON tu.id_type_usager =  u.us_id_type_usager
                    LEFT OUTER JOIN t_etat_usager eu ON eu.	id_etat_usager =  u.us_id_etat_usager
                    WHERE id_etat_usager = 0 AND eu.id_lang = 'FR'
                    ORDER BY $field
					LIMIT 5 OFFSET $offset;");
                    
$NbLigne = count($res);
include_once(designDir . "/include/fonctionTable.inc.php");
?>
<table class="tableResults" id="docListe" align="center" width="97%" cellspacing="0">
		<tr class="resultsHead_row">
			<?
			for($i = 0 ;$i <= count($titel);$i++){
				CreateHeader($titel[$i][0] ,$titel[$i][1],$field,"SR" );
            }
            $data = array();
			for($i = 0 ; $i <= $NbLigne-1 ; $i++){
				$data[$i] = array('<a class="" href="index.php?urlaction=usagerSaisie&amp;id_usager='.$res[$i]["ID_USAGER"].'&amp;rang=1">'.$res[$i]["US_NOM"],
							$res[$i]["US_PRENOM"],
							$res[$i]["US_SOCIETE"],
							$res[$i]["US_LOGIN"],
                            $res[$i]["TYPEU"],
                            $res[$i]["ETATU"],
                            '<a class="" href="index.php?urlaction=usagerSaisie&amp;id_usager='.$res[$i]["ID_USAGER"].'&amp;rang=1"><img src="design/images/icn-edit.png" style="height:13px;" title="Modifier" border="0"></a>');
			}
			CreateTable($data);	
		?>
</table>
<?php

?>