<?
if (isset($_POST["msg"])) {
	$obj = "Message de ".gSite . " : " . $_POST["obj"];
	$msg = "Un utilisateur de ".gSite." vous a envoyé un message.\n\nNom : ".$_POST["nom"]."\nPrénom : ".$_POST["prenom"]."\nAdresse Mail : ".$_POST["email"]."\nMessage : ".$_POST["msg"]."\n\nMessage envoyé automatiquement";
	//faut il utiliser $myUsr->Mail ou $_POST["email"] pour le from et le reply?
	$headers = 'From: ' . $myUsr->Mail . "\r\n" .
				'Reply-To: ' . $myUsr->Mail . "\r\n";
	$ok = mail(gMail, utf8_decode($obj), utf8_decode($msg), $headers);
	echo $ok;
	exit;
}
?>

<div class="title_bar">
	<?=kContact;?>
	<div id="close_export_menu" title="Fermer" onclick="hideMenuActions()" style="float:right;"></div>
</div>
	
<!--form action="" method="POST" name="formContact">

</form-->
<div id="contactForm">
	<label for="prenom" ><?=kPrenom;?></label>
	<input type="text" id="prenom" value="<?=$myUsr->Prenom;?>" />
	
	<label for="nom" ><?=kNom;?></label>
	<input type="text" id="nom" value="<?=$myUsr->Nom;?>" />
	
	<label for="email" ><?=kMail;?></label>
	<input type="text" id="email" value="<?=$myUsr->Mail;?>" />

	<label for="obj" ><?=Sujet;?></label>
	<input type="text" id="obj" value="" />
	
	<label for="msg" >Message</label>
	<textarea type="text" id="msg" />
	
	<button id="bOk" class="ui-state-default ui-corner-all" type="button" name="bOk" onclick="validContactForm()" style="margin: 10px auto;display: block;">
		<span class="ui-icon ui-icon-check"></span><?=kEnvoyer;?>
	</button>
</div>
<style>
#contactForm {
	padding: 10px;
    font-size: 16px;
    font-family: Arial,Helvetica,sans-serif;
}

#contactForm input, #contactForm textarea {
    width: calc(100% - 8px);
	max-width: calc(100% - 8px);
	min-width: calc(100% - 8px);
    border: none;
    background: #eceef3;
    padding: 4px;
	font-size: 16px;
    font-family: Arial,Helvetica,sans-serif;
}

#contactForm label {
	display: block;
}
</style>
<script>
function validContactForm() {
	hideMenuActions();
	$j.ajax({
		url : "empty.php?include=contact",
		method: "POST",
		data: { msg: $j("#contactForm #msg").val(), nom: $j("#contactForm #nom").val(), prenom: $j("#contactForm #prenom").val(), email: $j("#contactForm #email").val(), obj: $j("#contactForm #obj").val() },
		success : function(data){
			if (data == "1")
				alert("Message envoyé avec succès.");
			else
				alert("Erreur lors de l'envoi du message.");

		}
	});
}
</script>
<?

?>
