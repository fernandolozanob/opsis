<div class="title_bar">
	Sélection d'exemplaires
	<span class="html_close" onclick="hideMenuActions();"></span>
</div>

<div style="padding: 10px">
	<form id="chooseMatForm" name="chooseMatForm" action="" method="POST" onsubmit="searchMat(this);return false;" >
		<input type="hidden" name="page" value="<?=empty($_POST["page"]) ? 0 : $_POST["page"];?>" />
		<table>
			<tr>
				<td><label><?=kTypeDeMedia;?> : </label></td>
				<td>
					<select name="ID_MEDIA" >
						<option value=''></option>
						<?
							$fonds = $db->getAll("SELECT id_fonds, fonds FROM t_fonds WHERE id_lang=".$db->quote($_SESSION["langue"])." ORDER BY id_fonds");
							foreach ($fonds as $f) {
								echo "<option ".($_POST["ID_MEDIA"] == $f["ID_FONDS"] ? "selected='selected'" : "")." value='".$f["ID_FONDS"]."'>".$f["FONDS"]."</option>";
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td><label><?=kTypeDeSupport;?> : </label></td>
				<td>
					<select name="ID_SUP" >
						<option value=''></option>
						<?
							$sups = $db->getAll("SELECT id_val, valeur FROM t_val WHERE id_lang=".$db->quote($_SESSION["langue"])." AND val_id_type_val='SUP' ORDER BY valeur");
							foreach ($sups as $s) {
								echo "<option ".($_POST["ID_SUP"] == $s["ID_VAL"] ? "selected='selected'" : "")." value='".$s["ID_VAL"]."'>".$s["VALEUR"]."</option>";
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td><label><?=kTitre;?> : </label></td>
				<td>
					<select name="T_TITRE" >
						<option value='CNT' <? if ($_POST["T_TITRE"] == "CNT") echo "selected='selected'"; ?> >Contient</option>
						<option value='EXT' <? if ($_POST["T_TITRE"] == "EXT") echo "selected='selected'"; ?> >Exact</option>
						<option value='COM' <? if ($_POST["T_TITRE"] == "COM") echo "selected='selected'"; ?> >Commence par</option>
						<option value='TER' <? if ($_POST["T_TITRE"] == "TER") echo "selected='selected'"; ?> >Termine par</option>
					</select>
					<input name="TITRE" type="text" value="<?=$_POST["TITRE"];?>" />
				</td>
			</tr>
			<!--tr>
				<td><label><?=kCote;?> : </label></td>
				<td>
					<select name="T_COTE" >
						<option value='CNT' <? if ($_POST["T_COTE"] == "CNT") echo "selected='selected'"; ?> >Contient</option>
						<option value='EXT' <? if ($_POST["T_COTE"] == "EXT") echo "selected='selected'"; ?> >Exact</option>
						<option value='COM' <? if ($_POST["T_COTE"] == "COM") echo "selected='selected'"; ?> >Commence par</option>
						<option value='TER' <? if ($_POST["T_COTE"] == "TER") echo "selected='selected'"; ?> >Termine par</option>
					</select>
					<input name="COTE" type="text" value="<?=$_POST["COTE"];?>" />
				</td>
			</tr-->
			<tr>
				<td><label><?=kCoteExemplaire;?> : </label></td>
				<td>
					<select name="T_CEXP" >
						<option value='CNT' <? if ($_POST["T_CEXP"] == "CNT") echo "selected='selected'"; ?> >Contient</option>
						<option value='EXT' <? if ($_POST["T_CEXP"] == "EXT") echo "selected='selected'"; ?> >Exact</option>
						<option value='COM' <? if ($_POST["T_CEXP"] == "COM") echo "selected='selected'"; ?> >Commence par</option>
						<option value='TER' <? if ($_POST["T_CEXP"] == "TER") echo "selected='selected'"; ?> >Termine par</option>
					</select>
					<input name="CEXP" type="text" value="<?=$_POST["CEXP"];?>" />
				</td>
			</tr>
		</table>
		<input type="submit" value="Rechercher" />
	
		<span style="text-align:center;float:right">
			Lignes par page: <select name="nbLignes" onchange="searchMat(document.chooseMatForm)">
				<option <?=($_POST["nbLignes"] == 10 ? "selected='selected'" : "");?> value="10">10</option>
				<option <?=($_POST["nbLignes"] == 15 || empty($_POST["nbLignes"]) ? "selected='selected'" : "");?> value="15">15</option>
				<option <?=($_POST["nbLignes"] == 20 ? "selected='selected'" : "");?> value="20">20</option>
			</select>
		</span>
	</form>

	<?
	global $db;
	require_once(modelDir."model_panier.php");
	
	$id_pan = intval($_GET["id_panier"]);
	
	$myPanier = new Panier();
	$myPanier->t_panier["ID_PANIER"] = $id_pan;
	$myPanier->getPanier();
	$dateDeb = strtotime ($myPanier->t_panier["XML"]["commande"]["PANXML_DATEBLOC_DEB"]);
	$dateFin = strtotime ($myPanier->t_panier["XML"]["commande"]["PANXML_DATEBLOC_FIN"]);
	//var_dump($dateDeb);var_dump($dateFin);
	
	$mat_ids = null;
	if ($id_pan > 0) {
		$mat_ids = implode(",", $db->getCol("SELECT pdoc_id_mat FROM t_panier_doc WHERE id_panier = ".$id_pan));
	}
	
	$gnLignes = !empty($_POST["nbLignes"]) ? $_POST["nbLignes"] : 15;
	$page = (!empty($_POST["page"]) ? $_POST["page"] : 0);
	$offset = $page * $gnLignes;
	$count = $db->getOne("SELECT count(1) FROM t_mat m WHERE m.MAT_TYPE = 'EXEMPLAIRE';");
	/*$nbPage = ceil($count / $gnLignes);
	
	if ($nbPage > 1) {
		?><div style="text-align:center"> <?
		if ($page > 0) {
			?> <a href="javascript:changePage(-1)"><</a> <?
		}
		
		echo ($page + 1) . " / " . $nbPage;
		
		if ($page + 1 < $nbPage) {
			?> <a href="javascript:changePage(1)">></a> <?
		}
		?></div><?
	}*/
	$sql = "SELECT m.*, d.*, vEtat.VALEUR AS ETAT, vSup.VALEUR AS SUP
			FROM t_mat m
			LEFT OUTER JOIN t_doc_mat dm ON dm.id_mat = m.id_mat
			LEFT OUTER JOIN t_doc d ON d.id_doc = dm.id_doc AND d.id_lang='FR'
			LEFT OUTER JOIN t_mat_val mv ON mv.id_mat = m.id_mat AND mv.id_type_val = 'ETAT'
			LEFT OUTER JOIN t_val vEtat ON vEtat.id_val = mv.id_val AND vEtat.id_lang='FR'
			LEFT OUTER JOIN t_mat_val mvs ON mvs.id_mat = m.id_mat AND mvs.id_type_val = 'SUP'
			LEFT OUTER JOIN t_val vSup ON vSup.id_val = mvs.id_val AND vSup.id_lang='FR'
			WHERE m.MAT_TYPE = 'EXEMPLAIRE' ";
	
	if (!empty($_POST["TITRE"])) $sql .= getSqlCondition($_POST["T_TITRE"], "d.doc_titre", $_POST["TITRE"]);
	if (!empty($_POST["COTE"])) $sql .= getSqlCondition($_POST["T_COTE"], "d.doc_cote", $_POST["COTE"]);
	if (!empty($_POST["CEXP"])) $sql .= getSqlCondition($_POST["T_CEXP"], "m.mat_nom", $_POST["CEXP"]);
	
	if (!empty($_POST["ID_MEDIA"])) $sql .= " AND d.doc_id_fonds = ".intval($_POST["ID_MEDIA"]);
	if (!empty($_POST["ID_SUP"])) $sql .= " AND vSup.id_val = ".intval($_POST["ID_SUP"]);
	
	if ($mat_ids != null)
		$sql .= " AND m.id_mat not in ($mat_ids)";
		
	$sql .= " ORDER BY d.doc_cote, m.mat_nom";
	
	$sql .= " OFFSET $offset LIMIT $gnLignes";
	
	//echo $sql;
	
	$res = $db->getAll($sql);
	//var_dump($res);
	
	//$date_deb = ;
	$sqlCount = str_replace("m.*, d.*, vEtat.VALEUR AS ETAT, vSup.VALEUR AS SUP", "count(1)", substr($sql, 0, strpos($sql, "ORDER BY ")));
	$count = $db->getOne($sqlCount);
	$nbPage = ceil($count / $gnLignes);
	
	if ($nbPage > 1) {
		?><div style="text-align:center"> <?
		if ($page > 0) {
			?> <a href="javascript:changePage(-1)"><</a> <?
		}
		
		echo ($page + 1) . " / " . $nbPage;
		
		if ($page + 1 < $nbPage) {
			?> <a href="javascript:changePage(1)">></a> <?
		}
		?></div><?
	}

	?>
	
	<table style="border-spacing: 0px;color: #444;margin: auto;">
		<tr style="color: black;">
			<th><?=kTypeDeMedia;?></th>
			<th><?=kTitre;?></th>
			<!--th><?=kCote;?></th-->
			<th><?=kCoteExemplaire;?></th>
			<th><?=kTypeDeSupport;?></th>
			<th><?=kEtat;?></th>
			<th><?=kDuree;?></th>
			<th><?=kNbCollures;?></th>
			<th><?=kPerforations;?></th>
			<!--th><?=kPerformationReparees;?></th-->
			
			<th>Disponibilité</th>
		</tr>
	<?
	foreach ($res as $id=>$mat) {
		$dispo = true;
	
		$xmls = $db->getCol("SELECT pan_xml FROM t_panier WHERE id_panier in (SELECT id_panier FROM t_panier_doc WHERE pdoc_id_mat = ".intval($mat["ID_MAT"]).");");
		foreach ($xmls as $pxml) {
			$arr = xml2array($pxml)["commande"];
			$pdateDeb = strtotime (!empty($arr["PANXML_SORTIE"]) ? $arr["PANXML_SORTIE"] : $arr["PANXML_DATEBLOC_DEB"]);
			$pdateFin = strtotime (!empty($arr["PANXML_RETOUR"]) ? $arr["PANXML_RETOUR"] : $arr["PANXML_DATEBLOC_FIN"]);
			$pdateFin = strtotime ($arr["PANXML_DATEBLOC_FIN"]);
			//echo "$dateDeb / $dateFin => $pdateDeb / $pdateFin";
			//if (!($pdateDeb > $dateFin || $pdateFin < $dateDeb))
			if (($pdateDeb >= $dateDeb && $pdateDeb <= $dateFin) || ($pdateFin >= $dateDeb && $pdateFin <= $dateFin))
				$dispo = false;
		}
	
		echo "<tr id='rowMat".$mat["ID_MAT"]."' ".($dispo?"onclick='clickMat(".$mat["ID_MAT"].")'":"")." style='cursor:pointer' class='altern".(intval($id)%2).($dispo?"":" reserved")."'>";
		displayCell(xml2array($mat["DOC_XML"])["XML"]["DOC"]["FONDS"]);
		displayCell($mat["DOC_TITRE"]);
		//displayCell($mat["DOC_COTE"]);
		displayCell($mat["MAT_NOM"]);
		displayCell($mat["SUP"]);
		displayCell($mat["ETAT"]);
		//displayCell($mat["MAT_TEXT_7"]);
		displayCell($mat["DOC_DUREE"]);
		displayCell(str_replace(array(" collures", " collure"), "", $mat["MAT_TEXT_6"]), true);
		displayCell(str_replace(" perfs", "", $mat["MAT_TEXT_7"]), true);
		
		//displayCell($mat["MAT_ID_STATUT"] == "1" ? "Réservé" : ($mat["MAT_ID_STATUT"] == 2 ? "Disparu" : "Disponible"));
		displayCell($dispo ? "Disponible" : "Réservé");
		echo "</tr>";
	}

	function displayCell ($value, $center=false) {
		echo "<td style='padding:0px 10px;".($center ? "text-align:center" : "")."'>";
		echo $value;
		echo "</td>";
	}
	
	function getSqlCondition ($type, $field, $val) {
		global $db;
		if ($type == "CNT")
			return " AND $field ilike ".$db->Quote("%".htmlentities($val)."%");
		elseif ($type == "COM")
			return " AND $field ilike ".$db->Quote(htmlentities($val)."%");
		elseif ($type == "TER")
			return " AND $field ilike ".$db->Quote("%".htmlentities($val));
		else
			return " AND $field ilike ".$db->Quote(htmlentities($val));
	}
	?>
	</table>
</div>
<script>
function clickMat (id) {
	//hideMenuActions();
	addExemplaire(id);
	//$j("tr#rowMat"+id).remove();
}

function changePage (dir) {
	$j('input[name=page]').val(<?=($page);?> + dir);
	$j('#chooseMatForm').submit();
}
</script>

<style>
	tr.altern1 {
		background-color: #eee;
	}
	tr:hover {
		color: black;
	}
	tr.reserved, tr.reserved:hover {
		color: red;
	}
</style>