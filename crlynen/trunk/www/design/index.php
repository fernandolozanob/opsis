<?php
require_once(modelDir.'model_doc.php');

$myUsr=User::getInstance();


global $db;


if(isset($_GET['include']) && !empty($_GET['include']))
	require(designDir."include/".str_replace('../','',$_GET['include']).".inc.php");
else
{
	$myPage = Page::getInstance();
	$myPage->initReferrer();

?>
<div id="homepage_quick_srch_wrapper" class="headers"></div>
<div class="homepage_scrolldown_arrow"><a href="#homepage_new_docs" ></a></div>

<?
require_once(getSiteFile('designDir','/carrousel_acceuil.inc.php')) ;

$html = $db->getOne("SELECT html_contenu FROM t_html WHERE id_html = 1;");
echo "<div id='htmlContent' style='background-color: #F1F1F1;padding: 20px'>";
echo $html;
echo "</div>";

if ($myUsr->Type == kLoggedEdito) {
?>
	<!-- homepage nouveautés -->
	<div id="homepage_new_docs">
		<div id="homepage_new_docs_title" class="homepage_section_title">
			<span class="homepage_section_main_title"><?=kMsgAccueilNewDocsTitle?></span><br />
			<span class="homepage_section_sub_title"><?=kMsgAccueilNewDocsSubTitle?></span>
		</div>

		<div id="homepage_new_docs_mediatype">
		<?
			// récupération dynamiques des types de media, mais pose problème pr avoir un ordre choisi
			// $mediaTypes = getItem('t_media',array('id_media','media'),array('1'=>'1'),array('id_media'=>'desc'),false);
		?>
			<span onclick="showNewDocs('*',this);" class="selected" title="<?=kTous?>"><img class="icn_type_media" src="design/images/icnside-fond.png" /><span class="news_docs_mediatype_labels"><?=kTous?></span></span>
			<span onclick="showNewDocs('V',this);" title="<?=kVideos?>"><img class="icn_type_media" src="design/images/icninfo-video-grey.png" /><span class="news_docs_mediatype_labels"><?=kVideos?></span></span>
			<span onclick="showNewDocs('P',this);" title="<?=kImages?>"><img class="icn_type_media" src="design/images/icninfo-image-grey.png" /><span class="news_docs_mediatype_labels"><?=kImages?></span></span>
			<span onclick="showNewDocs('A',this);" title="<?=kAudios?>"><img class="icn_type_media" src="design/images/icninfo-audio-grey.png" /><span class="news_docs_mediatype_labels"><?=kAudios?></span></span>
			<span onclick="showNewDocs('D',this);" title="<?=kDocuments?>"><img class="icn_type_media" src="design/images/icninfo-file-grey.png" /><span class="news_docs_mediatype_labels"><?=kDocuments?></span></span>
			<span onclick="showNewDocs('R',this);" title="<?=kReportages?>"><img class="icn_type_media" src="design/images/icninfo-folder-grey.png" /><span class="news_docs_mediatype_labels"><?=kReportages?></span></span>
		</div>

		<div id="homepage_new_docs_grid">
			<div class="grid_1 new_doc_1"></div>
			<div class="grid_2">
				<div class="grid_1 new_doc_2"></div>
				<div class="grid_2">
					<div class="grid_1 new_doc_3"></div>
					<div class="grid_2 new_doc_4"></div>
				</div>
			</div>
		</div>
		<div class="homepage_actions_wrapper">
		<div class="homepage_scrolldown_arrow red inline"><a href="#homepage_thms" ></a></div>
		<div id="homepage_new_docs_goto" class="inline" onclick="goToCurrentNewDocs();"></div>
		<div class="homepage_scrollup_arrow red inline"><a href="#carrousel_accueil" ></a></div>
		</div>
	</div>
<?
}
?>
<div id="recapTab">
<? if(intval ($myUsr->Type) >= kLoggedDoc){ ?>
		<h2>Notices documentaires en attente</h2>
		<div id="container_tabdoc">
			<?php

				$res = $db->getAll ("SELECT id_doc 
				FROM t_doc d
				WHERE doc_acces!='1' AND d.id_lang = 'FR';");
				$NbLigneMax = count($res);
				if($NbLigneMax == 0){
					echo "Aucune notices documentaires en attente";
				}else{
					include (designDir . "/include/homeTable.inc.php");
					$NbPageMax = ceil ($NbLigneMax / $NbLigne);
		?></div><?
					CreateButton("ND" ,$NbPageMax );
				}
			?>

		<br/><br/>
		<h2>Liste des termes du thésaurus candidats</h2>
		<div id="container_tabThesaurus">
			<?
			$res = $db->getAll ("SELECT l.id_lex
								FROM t_lexique l	
								WHERE l.lex_id_etat_lex = '1' 
								AND l.id_lang = 'FR'
							;");
			$NbLigneMax = count($res);
			if($NbLigneMax == 0){
				echo "Aucune liste des termes du thésaurus candidats";
			}else{
				include (designDir . "/include/homeThesaurus.inc.php");
				$NbPageMaxTh = ceil ($NbLigneMax / $nbLigneAttendu);
		?></div><?
				CreateButton("TH" ,$NbPageMaxTh ); 
			}
	}
	if(intval ($myUsr->Type) == kLoggedAdmin){ ?>
		<br/><br/>
		<h2>Liste des demandes d’inscription en attente de validation</h2>
		<div id="container_tabSubRequest">
			<?

			$res = $db->getAll ("SELECT id_usager
								FROM t_usager u
								WHERE us_id_etat_usager = 0");
			$NbLigneMax = count($res);
			if($NbLigneMax == 0){
				echo "Aucune demande d'inscription en attente";
			}else{
				include (designDir . "/include/SubscriptionRequest.inc.php");
				$NbPageMaxSR = ceil ($NbLigneMax / $nbLigneAttendu);
		?></div><?
				CreateButton("SR" ,$NbPageMaxSR ); 
			}
	
	}
	?>
</div>

<script>
		var nbPageND = 1;
		var nbPageTH = 1;
		var nbPageSR = 1;
		var nbPageMaxND = <? echo intval($NbPageMax); ?>;
		var nbPageMaxTH = <? echo intval($NbPageMaxTh); ?>;
		var nbPageMaxSR = <? echo intval($NbPageMaxSR); ?>;
		var FieldND = 0;
		var FieldTH = 0;
		var FieldSR = 0;
		function nextPage (nb , type) {
			var nbPageMax = 0;
			var nbPage = 0;
			var MyId = "";
			if(type == "ND" | type =="TH" | type =="SR"){
				MyId = type+'Pager';
				nbPageMax = eval('nbPageMax'+type);
				eval('nbPage'+type+'+= nb;');
				if(nb == nbPageMax){
					eval('nbPage'+type+' = nbPageMax;');
				}
				if(nb == 0){
					eval('nbPage'+type+'= 1;');
				}
				nbPage = eval('nbPage'+type);
				if(type == "ND"){
					$j.ajax({
						url : "empty.php?include=homeTable&page="+nbPage+"&field="+FieldND,
						success: function(data){
							$j("#container_tabdoc").html(data);
							$j("#"+MyId+" #spanNbPage").html(nbPage);
						}	
					});
				}
				if(type == "TH"){
					$j.ajax({
						url : "empty.php?include=homeThesaurus&page="+nbPage+"&field="+FieldTH,
						success: function(data){
							$j("#container_tabThesaurus").html(data);
							$j("#"+MyId+" #spanNbPage").html(nbPage);
						}
					});
				}
				if(type == "SR"){
					$j.ajax({
						url : "empty.php?include=SubscriptionRequest&page="+nbPage+"&field="+FieldSR,
						success: function(data){
							$j("#container_tabSubRequest").html(data);
							$j("#"+MyId+" #spanNbPage").html(nbPage);
						}
					});
				}
			}	
			$j("#"+MyId+" #Next").css("display",nbPage >= nbPageMax ? "none" : "");
			$j("#"+MyId+" #MaxNext").css("display",nbPage >= nbPageMax ? "none" : "");
			$j("#"+MyId+" #Prev").css("display",nbPage >= 2 ? "" : "none");
			$j("#"+MyId+" #MaxPrev").css("display",nbPage >= 2 ? "" : "none");

		}

		function changeTri( Type , Field){

			if(Type == "ND"){
				if(FieldND == Field){
					FieldND = Field+" DESC";
				}else{
					FieldND = Field;
				}

			}
			else if(Type == "TH"){
				if(FieldTH == Field){
					FieldTH = Field+" DESC";
				}else{
					FieldTH = Field;
				}
			}
			else if(Type == "SR"){
				if(FieldSR == Field){
					FieldSR = Field+" DESC";
				}else{
					FieldSR = Field;
				}
			}
			nextPage( 0 , Type );
		}
	</script>
	
<!-- homepage thèmes -->
<div id="homepage_thms" >
	<?
		// récupération des thèmes de top niveau :
		require_once(libDir."class_chercheCat.php");
		$srchCat = new RechercheCat();
		$srchCat->prepareSQL() ;
		$srchCat->useSession = false ;
		$srchCat->tab_recherche[] =array (
									'FIELD' 	=>	'CAT_ID_GEN',
									'VALEUR'	=>	0,
									'TYPE'		=>	'C',
									'OP'		=>	'AND'
								);
		$srchCat->tab_recherche[] =array (
									'FIELD' 	=>	'CAT_ID_TYPE_CAT',
									'VALEUR'	=>	'THM',
									'TYPE'		=>	'C',
									'OP'		=>	'AND'
								);
		$srchCat->tab_recherche[] =array (
									'FIELD' 	=>	'CAT_INTRO',
									'VALEUR'	=>	'1',
									'TYPE'		=>	'CE',
									'OP'		=>	'AND'
								);
								
		$srchCat->tab_recherche[] =array (
									'FIELD' 	=>	'CAT_CODE',
									'VALEUR'	=>	'1,'.($myUsr->Type >= 5 ? "2" : "3"),
									'TYPE'		=>	'C',
									'OP'		=>	'AND'
								);

		$srchCat->makeSQL();
		$srchCat->finaliseRequete();
		$srchCat->ajouterTri('CAT_NOM','ASC');
		$themes = $db->GetArray($srchCat->sql.$srchCat->getTri().' Limit '.kMaxNbThemesHomepage);
		$themes_scnd_row = array_splice($themes,ceil(count($themes)/2));

		require_once(modelDir.'model_categorie.php');
		function draw_themes($arr_thm ){
			$str_themes = "" ;
			foreach($arr_thm as $thm){
				$cat = new Categorie();
				$cat->t_categorie['ID_CAT'] = $thm['ID_CAT'];
				$cat->getCategorie() ;
				$cat->getVignette() ;
				$str_themes .= '<div class="theme theme_'.$thm['ID_CAT'].'" 
					onclick="rebondSolr(\'0/'.str_replace("'","\\'",$cat->t_categorie['CAT_NOM']).'\',\'hier_ct_thm\',\''.kTheme.'\',\''.str_replace("'","\\'",$cat->t_categorie['CAT_NOM']).'\')" style="background-image : url('.kCheminHttp.'/design/images/degrade_vertical_60px_black_lighter.png), url('.kCheminHttp.'/makeVignette.php?image='.$cat->vignette.');"   
					onmouseout="$j(this).find(\'div\').removeClass(\'divOverlay\').addClass(\'hidden\')"
					onmouseover="$j(this).find(\'div\').removeClass(\'hidden\').addClass(\'divOverlay\')" >
					<div class="hidden"><span class="theme_desc">'.$cat->t_categorie['CAT_DESC'].'</span></div>';
				$str_themes .= '<span class="theme_titre">'.$cat->t_categorie['CAT_NOM'].'</span>';
				$str_themes .= '</div>';
			}
			return $str_themes;
		}


	?>
	<div id="homepage_thms_title" class="homepage_section_title">
		<span class="homepage_section_main_title"><?=kMsgAccueilDossiersThemTitle?></span><br />
		<span class="homepage_section_sub_title"><?=kMsgAccueilDossiersThemSubTitle?></span>
	</div>
	<div id="homepage_thms_grid">
		<div class="thm_row row1"><?=draw_themes($themes)?></div>
		<div class="thm_row row2"><?=draw_themes($themes_scnd_row)?></div>
	</div>
	<div class="homepage_scrollup_arrow red"><a href="#conteneur"></a></div>
</div>



<? include(getSiteFile('designDir','pied.inc.php')); ?>
<script type="text/javascript">
	var current_mediaType = "*";

	$j(document).ready(function(){
		$j("#homepage_new_docs_mediatype .selected").click();
	});

	var p_height = $j('#carrousel_accueil').outerHeight()+$j('#homepage_new_docs').outerHeight()+$j('#homepage_thms').outerHeight();
	var p_scroll = $j(window).scrollTop();

	$j(window).resize(function(){
		new_p_height = $j('#carrousel_accueil').outerHeight()+$j('#homepage_new_docs').outerHeight()+$j('#homepage_thms').outerHeight();
		console.log("p_height : "+p_height +" new_p_height "+new_p_height );
		console.log("p_scroll : "+p_scroll +" new_p_scroll "+(new_p_height*p_scroll/p_height) );
		progress_animation = true ;
		window.scrollTo(0,new_p_height*p_scroll/p_height) ;
		p_height = new_p_height;
		p_scroll = new_p_height*p_scroll/p_height;
	});

	function goToCurrentNewDocs(){
		current_mediatype_label = $j("#homepage_new_docs_mediatype span.selected").text();
		if(current_mediaType !='*'){
			rebondSolr(current_mediaType,'doc_id_media',current_mediatype_label);
		}else{
			reinitForm('quick_search',false );
			form = getFormSolr();

			form.refine.disabled=false;
			form.refine ="" ;
			form.reset_refine =2 ;
			obj_refine_solr = [];
			form.submit();
		}
	}


	function showNewDocs(mediaType,mediaType_button){
		// déplacement de la class selected
		$j("#homepage_new_docs_mediatype span").removeClass('selected');
		$j(mediaType_button).addClass('selected loading');
		$j("#homepage_new_docs_grid").addClass('loading');

		// mise à jour variable
		current_mediaType = mediaType;

		$j.ajax({
			url : 'service.php?urlaction=recherche&where=webservice&xsl=getXML',
			method : 'POST',
			data : {
				lang : '<?=$_SESSION['langue']?>',
				entite : 'doc',
				chFields : new Array('doc_id_media'),
				chTypes : new Array('C'),
				chOps : new Array('AND'),
				chValues : new Array(mediaType),
				chNoHLs : new Array(1),
				reset_refine : 1,
				nbLignes: 4,
				tri : 'doc_date_crea1'
			},
			success : function(data){
				try{
					$j(data).find('doc').each(function(idx,elt){
						vignette = $j(elt).find('vignette').text();
						id_media = $j(elt).find('DOC_ID_MEDIA').text();
						console.log("id_media :"+id_media);
						switch(id_media){
							case 'V' :
								str_media = '<img alt="ic&ocirc;ne vid&eacute;o"  src="design/images/100x100-icninfo-video.png" />';
							break;
							case 'P' :
								str_media = '<img alt="ic&ocirc;ne photo" src="design/images/100x100-icninfo-image.png" />';
							break;
							case 'D' :
								str_media = '<img alt="ic&ocirc;ne document" src="design/images/100x100-icninfo-file.png" />';
							break;
							case 'A' :
								str_media = '<img alt="ic&ocirc;ne audio" src="design/images/100x100-icninfo-audio.png" />';
							break;
							case 'R' :
								str_media = '<img alt="ic&ocirc;ne reportage" src="design/images/100x100-icninfo-dossier.png" />';
							break;
							default :
								str_media ="";
							break ;

						}

						$j(".new_doc_"+(idx+1)).html('<a href="index.php?urlaction=doc&id_doc='+$j(elt).find('ID_DOC').text()+'"><span class="new_doc_titre">'+str_media+$j(elt).find('DOC_TITRE').text()+'</span></a>');
						$j(".new_doc_"+(idx+1)).removeAttr('style');
						setTimeout(function(vignette){
							if(typeof vignette != 'undefined' && vignette != ''){
								$j(this).css({'backgroundImage' :"url(<?=kCheminHttp?>/design/images/degrade_vertical_60px_black_lighter.png), url(<?=kCheminHttp?>/makeVignette.php?image="+vignette+")"});
							}else{
								$j(this).css({'backgroundImage' :"url(<?=kCheminHttp?>/design/images/degrade_vertical_60px_black_lighter.png), url(<?=kCheminHttp?>/design/images/nopicture.gif)"});
							}
						}.bind($j(".new_doc_"+(idx+1)),vignette),1);
						
						if($j(elt).find('DOC_ID_MEDIA').text() == 'D'){
							$j(".new_doc_"+(idx+1)).addClass('media_doc');
						}else{
							$j(".new_doc_"+(idx+1)).removeClass('media_doc');
						}
					});
				}catch(e){
					console.log("crash mise à jour nouveaux docs - "+e);
				}
				$j(mediaType_button).removeClass('loading');
				$j("#homepage_new_docs_grid").removeClass('loading');
			}
		});
	}


$j(function() {
  $j('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $j(this.hash);
      target = target.length ? target : $j('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $j('html,body').animate({
          scrollTop: target.offset().top
		 },{
		  easing : 'easeInOutCubic',
		  duration : 700
        });
        return false;
      }
    }
  });
});


	var duration_homepage_quicksrch_fadein =70;
	$j(window).scroll(function(evt){
		duration_anim = duration_homepage_quicksrch_fadein ;

		if ($j(window).width()<mobil_border) { // mobile
			animation_scroll_constante = 2;
		} else { // tablette et desktop
			animation_scroll_constante = 4;
		}
		if($j(window).scrollTop() > $j(window).height()/parseInt(animation_scroll_constante) ){
			if(!$j("#entete").hasClass('scrolled') && !$j("#entete").hasClass('scrolling')){
				$j('#entete').addClass('scrolling');
				$j("#header_quick_srch_block").clearQueue().stop();
				$j("#header_quick_srch_block").animate({
					opacity : 0
				},{
					queue : false ,
					duration : duration_anim,
					complete: function(){
						$j("#header_menus_block").before($j("#header_quick_srch_block").detach());
						$j('#entete').removeClass('scrolling').addClass('scrolled');
						$j("#header_quick_srch_block").animate({
							opacity : 1
						},{
							queue : false ,
							duration : duration_anim
						});
					}
				});
			}
		}else{
			if($j("#entete").hasClass('scrolled') || $j("#entete").hasClass('scrolling') || $j("#header_quick_srch_block").css('opacity') ==0){
				$j("#header_quick_srch_block").clearQueue().stop();
				$j("#header_quick_srch_block").animate({
					opacity : 0
				},{
					queue : false,
					duration : duration_anim,
					complete: function(){
						$j("#header_quick_srch_block").detach().appendTo("#homepage_quick_srch_wrapper");
						$j('#entete').removeClass('scrolling').removeClass('scrolled');
						$j("#header_quick_srch_block").animate({
							opacity : 1
						},{
							queue : false ,
							duration : duration_anim
						});

					}
				});
			}
		}
	});

	var scrollMagnet_timeout = null ;
	var progress_animation = null ;

	$j(window).scroll(function(evt){
		p_scroll =  $j(window).scrollTop();

		//duration animation
		duration_anim = duration_homepage_quicksrch_fadein ;
		//éléments sur lesquels s'arrimer
		elements_to_magnet = $j("#carrousel_accueil,#homepage_new_docs,#homepage_thms"); //
		// seuil à partir duquel on veut s'arrimer à l'élément de elements_to_magnet le plus proche , ici 10% de la taille de la fenetre
		magnet_treshold = $j( window ).height()/5;
		progress_step = progress_animation ;
		progress_animation = null ;
		// console.log("tick scrollevent , progress_step : "+progress_step);


		// si un timeout est en cours d'attente et qu'on a un autre event de scroll => on cancel le timeout en attente
		if(progress_step == null && scrollMagnet_timeout != null){
			// console.log("clear timeout quand ne vient pas d'une anim en cours");
			clearTimeout(scrollMagnet_timeout);
			scrollMagnet_timeout = null ;
			$j('body').clearQueue();
			// $j('body').stop(true);
		}


		elements_to_magnet.each(function(idx,elt){
			// scrolltop proche (- de 10% de la taille de l'écran de différence)
			if(progress_step == null && Math.abs($j(window).scrollTop() - $j(elt).offset().top) <= magnet_treshold && Math.abs($j(window).scrollTop() - $j(elt).offset().top) != 0 ){
				// console.log("treshold reached, magnet to : "+$j(elt).get(0).id+" animation lancée dans 0.5s");
				scrollMagnet_timeout = window.setTimeout(function(){
					if(Math.abs($j(window).scrollTop() - $j(elt).offset().top) <= magnet_treshold){
						$j('body').animate({
							scrollTop : $j(elt).offset().top
						},{
							duration : duration_anim,
							queue :false,
							progress : function(){
								progress_animation = true;
								// console.log("tick progress animation");
							},
							done : function(){
								scrollMagnet_timeout = null ;
								// console.log("tick animation complete ");
							}
						});
					}
				},500);
			}
		});
	});

	$j(window).trigger('scroll');

</script>

	<?php
}
?>
