<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:variable name="affMode" select="'carto'"/>
	<xsl:variable name="pagination" select="'1'"/>
	
	<xsl:include href="docListe.xsl"/>
	


</xsl:stylesheet>
