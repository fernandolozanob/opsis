<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../fonctions.xsl"/>
<xsl:include href="../liste.xsl"/>
<xsl:output
        method="xml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="yes"
/> 

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="xmlfile" />
<xsl:param name="fromAjax" />
<xsl:param name="fromOffset" />
<xsl:param name="refine_title" />

<xsl:template match='/select'>

	<xsl:variable name="xmllist" select="document($xmlfile)"/>
	<xsl:variable name="xml_solr_facet" select="document($solr_facet)"/>
	<div id="frame_previewWrapper">
		<div id="previewContainer"></div>
	</div>
	<div id="frame_block">
		<div id="resultats" class="liste"  align="center">
			<table width="100%" align="center" cellspacing="0" cellpadding="0" class="tableResults" id="frame_list_doc" >
				 <xsl:for-each select="doc">
					<xsl:variable name="row" select="child::*"/>
					<xsl:variable name="j"><xsl:number/></xsl:variable>

					<xsl:variable name="chemin_story">
						&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,storyboardChemin); ?&gt;
					</xsl:variable>
					<xsl:variable name="doc_story">
						&lt;?php echo str_replace(kCheminHttpMedia,&#39;&#39;,kDocumentUrl); ?&gt;
					</xsl:variable>
					
					<xsl:variable name="vignette">
						<xsl:choose>
							<xsl:when test="string-length(IM_FICHIER)>3"><xsl:value-of select="$chemin_story" />&lt;? echo urlencode('<xsl:call-template name="replace"><xsl:with-param name="stream" select="concat(IM_CHEMIN,'/',IM_FICHIER)"/><xsl:with-param name="old">'</xsl:with-param><xsl:with-param name="new">\'</xsl:with-param></xsl:call-template>');?&gt;</xsl:when>
							<xsl:when test="string-length(DA_FICHIER)>3"><xsl:value-of select="$doc_story" />&lt;? echo urlencode('<xsl:call-template name="replace"><xsl:with-param name="stream" select="concat(DA_CHEMIN,DA_FICHIER)"/><xsl:with-param name="old">'</xsl:with-param><xsl:with-param name="new">\'</xsl:with-param></xsl:call-template>');?&gt;</xsl:when>
						</xsl:choose>
					</xsl:variable>
			<xsl:variable name="ol_type_media">
				<xsl:choose>
						<xsl:when test="DOC_ID_MEDIA='V'"><xsl:text>player</xsl:text></xsl:when>
						<xsl:when test="DOC_ID_MEDIA='A'"><xsl:text>audiovector</xsl:text></xsl:when>
				</xsl:choose>		
			</xsl:variable>
					<tr>
						<td class="cell_vignette">
							<input type="hidden" name="id_doc" class="hoverplayerid" value="{ID_DOC}" />
							<xsl:if test="$vignette!=''">
								<img class="frame_vignette" src="makeVignette.php?image={$vignette}&amp;w=107&amp;h=60&amp;kr=1&amp;amp;ol={$ol_type_media}" border="0" alt="vignette" />
							</xsl:if>
							<xsl:if test="$vignette='' and DOC_ID_MEDIA='A'">
								<img class="frame_vignette" src="design/images/vignette_audio.png" border="0" height="60px" alt="vignette" />
							</xsl:if>
							<xsl:if test="$vignette='' and (DOC_ID_MEDIA='V' or DOC_ID_MEDIA='P')">
								<img class="frame_vignette" src="design/images/nopicture.gif" border="0" height="60px" alt="vignette" />
							</xsl:if>
						</td>
						<td>
							<div class="cell_titre">
								<a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}"><xsl:value-of select="DOC_COTE"/> - <xsl:value-of select="DOC_TITRE"/></a>
							</div>
							<div>
								<xsl:value-of select="FONDS"/> - 
								<xsl:call-template name="format_date">
									<xsl:with-param name="chaine_date" select="DOC_DATE_PV_DEBUT"/>	
									<xsl:with-param name="prec_date" select="V_DATE"/>
								</xsl:call-template>
								<xsl:if test="DOC_DUREE!=''">
								 - <xsl:value-of select="DOC_DUREE"/>
								</xsl:if>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</div>
	
	<div class="frame_tri">
		<!--xsl:choose>
			<xsl:when test="$ordre=1">
				<button class="icon-asc" onclick="updateCartoListTri('doc_id_media1,'+$j('#select_tri').val()+'{$ordre}');">&amp;nbsp;</button>
			</xsl:when>
			<xsl:otherwise>
				<button class="icon-desc" onclick="updateCartoListTri('doc_id_media1,'+$j('#select_tri').val()+'{$ordre}');">&amp;nbsp;</button>
			</xsl:otherwise>
		</xsl:choose-->
		<select name="tri" id="select_tri" onchange="updateCartoListTri($j(this).val()+'{$ordre}');" >
			<option value=""><xsl:processing-instruction name="php">echo kTrierPar;</xsl:processing-instruction></option>
			<option value="sort_doc_titre"><xsl:if test="contains($tri, 'sort_doc_titre')"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">echo kTitre;</xsl:processing-instruction></option>
			<option value="doc_date"><xsl:if test="contains($tri, 'doc_date_pv_debut')"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">echo kDate;</xsl:processing-instruction></option>
<!-- 			<option value="doc_duree"><xsl:if test="contains($tri, 'doc_duree')"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:processing-instruction name="php">echo kDuree;</xsl:processing-instruction></option> -->
		</select>
	</div>
	
	<xsl:if test="$nb_pages>1">
		<div class="browser">
			<xsl:if test="$page>1"><a class="icon-nav icon-prev" href="#" onclick='javascript:updateCartoListPage({$page - 1});'>&lt;</a> </xsl:if>
			<xsl:value-of select="$page"/><xsl:value-of select="concat(' / ',$nb_pages)"/>
			<xsl:if test="$page &lt; $nb_pages"> <a class="icon-nav icon-next" href="#" onclick='javascript:updateCartoListPage({$page + 1});'>&gt;</a></xsl:if>
		</div>
	</xsl:if>
	
	<script>

	$j( "button.icon-asc" ).button({
		icons: {
			primary: "ui-icon-arrowthick-1-n"
		},
		text: false
	});
	$j( "button.icon-desc" ).button({
		icons: {
			primary: "ui-icon-arrowthick-1-s"
		},
		text: false
	});
	$j("#map_dialog").dialog( "option", "title", "<xsl:value-of select="concat($refine_title,' (',$nb_rows,')')"/>");
	</script>

</xsl:template>

</xsl:stylesheet>
