<?xml version="1.0" encoding="utf-8"?>
<!-- minipanier : utilisé pour afficher le contenu d'un folder en format très réduit (ex: frame de gauche, etc. ) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>
<xsl:param name="currentIdDoc"/>


<xsl:template match="/select">
<xsl:variable name="id_lang" ><xsl:value-of select="t_doc/ID_LANG"/></xsl:variable>


																				
    <form name="form2" id="form2" action="blank.php?urlaction=processSequence&amp;commande=saveSequences&amp;id={t_doc/ID_DOC}" method="POST" target="iframeSauve" >

	    <xsl:for-each select="t_doc/t_seq/SEQ">	      	
				<xsl:variable name="titre" >
					<xsl:call-template name="internal-dbquote-replace"><xsl:with-param	name="stream" select="DOC_TITRE"/></xsl:call-template>
				</xsl:variable>
				<xsl:variable name="vignette">
					<xsl:choose>
						<xsl:when test="string-length(IM_FICHIER)>3"><xsl:value-of select="concat('storyboard/',IM_CHEMIN,'/',IM_FICHIER)"/></xsl:when>
						<xsl:when test="string-length(DA_FICHIER)>3"><xsl:value-of select="concat('doc_acc/',DA_CHEMIN,DA_FICHIER)"/></xsl:when>
					</xsl:choose>
				</xsl:variable>

				<div id="row$" class="row_extrait" style="height:50px;">
					<div id="handle$" class="ext_handler" onClick="myPanel.selectExtrait(this.parentNode)" >&#160;</div>
					<input id="action$" type="hidden" name="ligne_action[]" value="edit" />
					<input type="hidden" name="ID_DOC[]" value="{@ID_DOC}" />
					<input type="hidden" name="DOC_ID_GEN[]" value="{DOC_ID_GEN}" />
					<input type="hidden" name="DOC_ID_MEDIA[]" value="{DOC_ID_MEDIA}" />
					<input type="hidden" name="DOC_ID_FONDS[]" value="{DOC_ID_FONDS}" />
					<input type="hidden" name="DOC_COTE[]" value="{DOC_COTE}" id="ext_cote$" />
					<input type="hidden" name="DOC_ID_TYPE_DOC[]" value="{DOC_ID_FONDS}" />

					
					

					<xsl:if test="$vignette!=''">
						<img src="makeVignette.php?image={$vignette}&amp;w=60" height="46" style="margin:2px 0px 0px 4px;float:left;left:10px;"/>
					</xsl:if>
					<input id="ext_titre$" type="text" size="35" name="DOC_TITRE[]" class="ext_titre" value="{$titre}" 
					onKeyPress="myPanel.hasChanged(this.parentNode);" onBlur="myPanel.checkLine(this.parentNode); " />
				
					&#160;<img id="trash$" src="design/images/button_drop.gif" style="display:block; float:right; margin:1px;" 
						onclick="myPanel.removeExtrait(this.parentNode)" />
					&#160;<a href="index.php?urlaction=doc&amp;id_doc={ID_DOC}" style="display:block; float:right; margin:1px;">
						<img id="gotodoc$" align="top" alt="&lt;?=kDocument?&gt;" title="&lt;?=kDocument?&gt;" style="right : 18px;" src="design/images/gotodoc.gif" border="0" />
					</a>
					<a href="index.php?urlaction=docSaisie&amp;id_doc={ID_DOC}" style="display:block; float:right; margin:1px;">
						<img id="edit$" align="top" title="&lt;?=kModifier?&gt;" alt="&lt;?=kModifier?&gt;" style="right : 36px;" src="design/images/button_modif.gif" border="0" />
					</a>
					
					<br />&lt;?=kTC?&gt;
					<input id="tcin$" type="text" name="DOC_TCIN[]" size="11" maxlength="11" class="ext_TC" 
						value="{DOC_TCIN}" onfocus="myPanel.setCurr(this.parentNode);" onBlur="checkTC(this); myPanel.checkLine(this.parentNode); " onKeyPress="myPanel.hasChanged(this.parentNode);" />
					<input id="tcout$" type="text" name="DOC_TCOUT[]" size="11" maxlength="11" class="ext_TC" 
						value="{DOC_TCOUT}" onfocus="myPanel.setCurr(this.parentNode);" onBlur="checkTC(this); myPanel.checkLine(this.parentNode);" onKeyPress="myPanel.hasChanged(this.parentNode);" style="margin-left:0px"/>
	
				</div>		
	    </xsl:for-each> 
	    
    </form>
    <div id="separateur" >&#160;</div><!-- Attention, tjs mettre du contenu sinon bug FF -->  
    <div id="row$blank" class="row_extrait" style="height:50px;display:none;">
			<div id="handle$" class="ext_handler" onClick="myPanel.selectExtrait(this.parentNode)" >&#160;</div>
			<input id="action$" type="hidden" name="ligne_action[]" value="edit" />
			<input type="hidden" name="ID_DOC[]" value="" />
			<input type="hidden" name="DOC_ID_GEN[]" value="{t_doc/ID_DOC}" />
			<input type="hidden" name="DOC_ID_MEDIA[]" value="{t_doc/DOC_ID_MEDIA}" />
			<input type="hidden" name="DOC_ID_FONDS[]" value="{t_doc/DOC_ID_FONDS}" />
			
			<input id="ext_cote$" type="hidden" name="DOC_COTE[]" value="{t_doc/DOC_COTE}_" />
			<input type="hidden" name="DOC_AUTRE_TITRE[]" value="{t_doc/DOC_AUTRE_TITRE}" />
			<input type="hidden" name="DOC_DUREE[]" value="{t_doc/DOC_DUREE}" />
			<input type="hidden" name="DOC_TITRE_COL[]" value="{t_doc/DOC_TITRE_COL}" />
			<input type="hidden" name="DOC_DATE_DIFF[]" value="{t_doc/DOC_DATE_DIFF}" />
			<input type="hidden" name="DOC_DATE_DIFF_FIN[]" value="{t_doc/DOC_DATE_DIFF_FIN}" />
			<input type="hidden" name="DOC_DIFFUSEUR[]" value="{t_doc/DOC_DIFFUSEUR}" />
			<input type="hidden" name="DOC_RES_CAT[]" value="{t_doc/DOC_RES_CAT}" />
			<input type="hidden" name="DOC_RES[]" value="{t_doc/DOC_RES}" />
			<input type="hidden" name="DOC_COMMISSION[]" value="{t_doc/DOC_COMMISSION}" />
			<input type="hidden" name="DOC_SEQ[]" value="{t_doc/DOC_SEQ}" />
			<input type="hidden" name="DOC_ACC[]" value="{t_doc/DOC_ACC}" />
			<input type="hidden" name="DOC_NOTES[]" value="{t_doc/DOC_NOTES}" />
			<input type="hidden" name="DOC_COMMENT[]" value="{t_doc/DOC_COMMENT}" />
			<input type="hidden" name="DOC_RECOMP[]" value="{t_doc/DOC_RECOMP}" />
			<input type="hidden" name="DOC_NB_EPISODES[]" value="{t_doc/DOC_NB_EPISODES}" />
			<input type="hidden" name="DOC_TRANSCRIPT[]" value="{t_doc/DOC_TRANSCRIPT}" />
			<input type="hidden" name="DOC_ID_TYPE_DOC[]" value="10" />

			
			<input type="hidden" name="inherit_pers_lex" value="1" />
			<input type="hidden" name="inherit_doc_lex" value="1" />
			<input type="hidden" name="inherit_doc_val" value="1" />
			
			<img id="trash$" src="design/images/button_drop.gif"  style="display:block; float:right; margin:1px;"
				onclick="myPanel.removeExtrait(this.parentNode)" />
			
			<input id="ext_titre$" type="text" size="35" name="DOC_TITRE[]" class="ext_titre" value="" onKeyPress="myPanel.hasChanged(this.parentNode);" onBlur="myPanel.checkLine(this.parentNode); " />
			
			<br />&lt;?=kTC?&gt;
			<input id="tcin$" type="text" name="DOC_TCIN[]" size="11" maxlength="11" class="ext_TC" 
				value="" onfocus="myPanel.setCurr(this.parentNode);" onBlur="checkTC(this); myPanel.checkLine(this.parentNode); " onKeyPress="myPanel.hasChanged(this.parentNode);" />
			<input id="tcout$" type="text" name="DOC_TCOUT[]" size="11" maxlength="11" class="ext_TC" 
				value="" onfocus="myPanel.setCurr(this.parentNode);" onBlur="checkTC(this); myPanel.checkLine(this.parentNode); " onKeyPress="myPanel.hasChanged(this.parentNode);" style="margin-left:0px"/>				
			
	</div>
</xsl:template>

	<xsl:template name="internal-quote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">'</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>\'<xsl:call-template
		name="internal-quote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
	</xsl:template>

<xsl:template name="internal-dbquote-replace">
		  <xsl:param name="stream" />
		  <xsl:variable name="simple-quote">"</xsl:variable>
	
			<xsl:choose>
	
			<xsl:when test="contains($stream,$simple-quote)">
		<xsl:value-of
		select="substring-before($stream,$simple-quote)"/>&quot;<xsl:call-template
		name="internal-dbquote-replace"><xsl:with-param name="stream"
		select="substring-after($stream,$simple-quote)"/></xsl:call-template>
			</xsl:when>
	
			<xsl:otherwise>
		<xsl:value-of select="$stream"/>
		  </xsl:otherwise>
	
			</xsl:choose>
	
</xsl:template>

</xsl:stylesheet>
