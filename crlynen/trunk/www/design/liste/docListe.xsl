<?xml version="1.0" encoding="utf-8"?>
<!-- docListe.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../liste.xsl"/>
<xsl:include href="../fonctions.xsl"/>
<xsl:output
        method="xml"
        encoding="utf-8"
        omit-xml-declaration="yes"
        indent="yes"
/>

<!-- recuperation de parametres PHP -->
<!-- profil=(1 user, 2 doc, 3 admin) -->
<xsl:param name="profil"/>
<xsl:param name="nbLigne"/>
<xsl:param name="titre"/>
<xsl:param name="ordre"/>
<xsl:param name="tri"/>
<xsl:param name="sauvRequete"/>
<xsl:param name="offset" />
<xsl:param name="scripturl"/>
<xsl:param name="cartList"/>
<xsl:param name="pager_link" />
<xsl:param name="votre_recherche" />
<xsl:param name="etat"/>
<xsl:param name="playerVideo"/>
<xsl:param name="page" />
<xsl:param name="urlparams" />
<xsl:param name="frameurl" />
<xsl:param name="nb_rows" />
<xsl:param name="xmlfile" />
<xsl:param name="fromAjax" />
<xsl:param name="fromOffset" />
<xsl:param name="solr_facet" />
<xsl:param name="pagination" />
<xsl:param name="affMode" />
<xsl:param name="geojson" />

<xsl:template match='/select'>
	<xsl:variable name="xmllist" select="document($xmlfile)"/>
	<xsl:variable name="xml_solr_facet" select="document($solr_facet)"/>
	
	<xsl:choose>
	<xsl:when test="$fromAjax='1'">
		<xsl:if test="number($nbLigne) = $nbLigne and number($fromOffset) = $fromOffset ">
			<xsl:variable name="newNbLigne"><xsl:value-of select="number($fromOffset) + number($nbLigne)"/></xsl:variable>
			<xsl:processing-instruction name="php">$_SESSION['recherche_DOC_Solr']['nbLignes']=<xsl:value-of select="$newNbLigne"/>;</xsl:processing-instruction>
		</xsl:if>
		
		<xsl:choose>
			<xsl:when test="$affMode = 'mos'">
				<xsl:call-template name="mosDoc">
					<xsl:with-param name="context">doc</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$affMode = 'liste'">
				<xsl:call-template name="displayListe">
					<xsl:with-param name="xmllist" select="$xmllist"/>
					<xsl:with-param name="fromOffset" select="$fromOffset"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:when>
	<xsl:otherwise>

		<script type="text/javascript" src="&lt;?=libUrl?&gt;/webComponents/opsis/liste.js">&amp;nbsp;</script>

		<!-- inclusion template qui genere le menu gauche -->
		<xsl:call-template name="rechercheLeftMenu" >
			<xsl:with-param name="solr_facet" select="$xml_solr_facet"/>
		</xsl:call-template>
		<div id="mainResultsBar"><xsl:attribute name="class">title_bar pres_&lt;?=$_SESSION['USER']['layout_opts']['layout']?&gt; <xsl:if test="$affMode='carto'">pres_1</xsl:if></xsl:attribute>
			<xsl:if test="$affMode='carto'"><xsl:attribute name="style">right:0px;</xsl:attribute></xsl:if>
			<span class="notice_results">
				<span class="num_results"><xsl:value-of select="$nb_rows"/></span>
				<xsl:processing-instruction name="php">print kResultats;</xsl:processing-instruction>
			</span>
			<xsl:call-template name="toolbar">
				<xsl:with-param name="context">doc_<xsl:value-of select="$affMode"/></xsl:with-param>
				<xsl:with-param name="entity" select="$xmllist/list/entity"/>
				<xsl:with-param name="pagination" select="$pagination"/>
			</xsl:call-template>
		</div>

		<!-- inclusion template qui genere la partie droite de preview-->
		<!-- réalisé avant l'affichage template central => permet d'avoir un chargement de la page plus propre -->
		<xsl:if test="$affMode!='carto'">
		<xsl:call-template name="rechercheRightPreview" />
		</xsl:if>

<div id="main_block"><xsl:attribute name="class">pres_&lt;?=$_SESSION['USER']['layout_opts']['layout']?&gt;</xsl:attribute>
	<xsl:if test="$affMode='carto'"><xsl:attribute name="style">right:0px;</xsl:attribute></xsl:if>
	
			<form name="documentSelection" method="post" onsubmit='updatePage()' action="{$scripturl}?urlaction=docListe{$urlparams}">
				<input type="hidden" name="page" value="{$page}"  />
				<input type="hidden" name="nbLignes" value="{$nbLigne}"  />
				<input type="hidden" name="style" value=""  />
				<input type="hidden" name="stylePanier" id="stylePanier" value=""  />
				<input type="hidden" name="tri" value="" />

				<xsl:choose>
					<xsl:when test="$pagination='1' and $affMode!='carto'">
						<xsl:call-template name="pagerListe">
							<xsl:with-param name="form">documentSelection</xsl:with-param>
							<xsl:with-param name="class">first</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<script type="text/javascript">
						var pagerForm=document.documentSelection;
						</script>
					</xsl:otherwise>
				</xsl:choose>

				<div id="resultats"  align="center">
					<xsl:attribute name="class"><xsl:value-of select="$affMode"/><xsl:text> </xsl:text><xsl:if test="$pagination='1'">paginee</xsl:if></xsl:attribute>
					<xsl:choose>
						<xsl:when test="$affMode = 'mos'">
							<xsl:call-template name="mosDoc">
								<xsl:with-param name="context">doc</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="$affMode = 'liste'">
							<xsl:call-template name="displayListe">
								<xsl:with-param name="xmllist" select="$xmllist"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="$affMode = 'carto'">
							<link rel="stylesheet" href="&lt;?=libUrl?&gt;/webComponents/leaflet/leaflet.css" />
							<script type="text/javascript" src="&lt;?=designUrl?&gt;js/carto.js">&amp;nbsp;</script>

							<div id="map" >&amp;nbsp;</div>
							<div id="map_dialog">&amp;nbsp;</div>
							&lt;?
							// récup geojson depuis l'objet view car la 3.1.0 empêche d'éditer l'objet avant qu'il soit passé à l'interpreteur xslt
							// on édite donc l'objet dans docListe.html et on le récupère ici au moment des evals php.
							$myPage = Page::getInstance() ;
							$mySearch = $myPage->view->params['searchObj'];
							?&gt;
							<script>
								var arrPlace;
								<xsl:if test="$geojson">
									arrPlace = &lt;?= $mySearch->geojson?&gt;;
								</xsl:if>
								
								requirejs(['leaflet/leaflet'], function(){
									initCartoListe();
								});
								
							</script>
						</xsl:when>
					</xsl:choose>
					<div class="pusher">&#160;</div>
				</div>
				<xsl:choose>
					<xsl:when test="$affMode='carto'">
					</xsl:when>
					<xsl:when test="$pagination='1'">
						<xsl:call-template name="pagerListe">
							<xsl:with-param name="form">documentSelection</xsl:with-param>
							<xsl:with-param name="class">last</xsl:with-param>
							<xsl:with-param name="noscript">1</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$nbRows and $nbLigne &lt; $nbRows">
							<div id="loadMoreRes" class="mod_auto auto_hidden" onclick="loadMoreResults();"><span><xsl:processing-instruction name="php">print kLoadMoreResults;</xsl:processing-instruction></span></div>
							<script type="text/javascript">
								initAutoLoadMoreResults();
							</script>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<script type="text/javascript">
					var totalNbResults = <xsl:value-of select="$nbRows"/>;
				</script>
			</form>
		</div>

	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
