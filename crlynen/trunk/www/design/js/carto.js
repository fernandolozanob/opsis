
function initCartoListe() {
	console.log("initCartoListe");
	if ($j("div#map").length == 1) {
		
		
		var map = L.map('map').setView([-0.8, 12], 6);
		var osmUrl='http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png';
		var osm = new L.TileLayer(osmUrl, {opacity:0.8, minZoom: 3, maxZoom: 14});
		map.setView(new L.LatLng(48.853, 2.349), 3); // Paris
		map.addLayer(osm);
		 
		/*
		var map = L.map('map').setView([48.853, 2.349], 12);
		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
					maxZoom: 18,
					attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
					'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
					'Imagery © <a href="http://mapbox.com">Mapbox</a>',
					id: 'mapbox.light'
					}).addTo(map);
		*/
		
		var geojsonMarkerOptions = {
			radius: 10,
			fillColor: "#F07407",
			color: "#CC3000",
			weight: 1,
			opacity: 1,
			fillOpacity: 1
		};
		var i = 0;
		
		$j("#map").append('<div id="map_infobulle"><div id="map_infobulle_texte"></div></div>');
		var infobulle = $j("#map_infobulle");
		var map_offset = $j("#map").offset() ;
		
		L.geoJson(arrPlace, {
			pointToLayer: function (feature, latlng) {
				i++;
				geojsonMarkerOptions.radius=Math.round(5+3*Math.log(feature.properties.count));
				mark = L.circleMarker(latlng, geojsonMarkerOptions);
				mark.on('click', function(e) {
						var arrXY=feature.geometry.coordinates;
						coor=arrXY[1]+","+arrXY[0];
					onMarkClick(coor, e);
				});
				mark.on('mouseover', function(e){
					//console.log("mouseover ",e);
					var name = (typeof feature.properties.name != 'undefined' && feature.properties.name!=null)?feature.properties.name:''; 
					if(infobulle.length >0){
						offset_layer_left = e.containerPoint.x - e.layerPoint.x  ; 
						offset_layer_top = e.containerPoint.y - e.layerPoint.y  ; 
						infobulle.find('#map_infobulle_texte').html(name+"<br />"+feature.properties.count) ;
						infobulle.css('left',map_offset.left + e.target._point.x - (infobulle.outerWidth() / 2 ) + offset_layer_left);
						infobulle.css('top',map_offset.top + e.target._point.y - infobulle.outerHeight() - 10   + offset_layer_top);
						infobulle.addClass('show');
					}
				});
				mark.on('mouseout', function(e){
					//console.log("mouseleave",e);
					if(infobulle.length >0){
						infobulle.removeClass('show');
						infobulle.find('#map_infobulle_texte').html("") ;
					}
				});
				return mark;
			},
			onEachFeature: function (feature, layer) {
				if (typeof(onEachMarker) == "function") onEachMarker(feature, layer);
			}
		 
		}).addTo(map);
		
		$j("#map_dialog").dialog({autoOpen: false, height: 440, width: 400});
		$j( "#map_dialog").on( "dialogclose", function( event, ui ) {
			$j("#map_dialog").removeClass('previewActive');
			
		} );
	}
}

function onEachMarker(feature, layer) {
}

var currentName = "";
var currentIdsDoc = "";
var currentPage = 1;
var currentTri = "";

function onMarkClick(data, e) {
	currentName = data;
	currentPage = 1;
	currentTri = "";
	currentIdsDoc = "";
	
	//$j("#map_dialog").dialog( "option", "title", "");
	$j("#map_dialog").dialog("open");
	$j("#map_dialog").dialog("option", { position: [e.originalEvent.pageX + 5, e.originalEvent.pageY - 200] });
	updateCartoList();
}

function updateCartoListPage (num_page) {
	if (currentName != "") {
		currentPage = num_page;
		updateCartoList();
	}
}

function updateCartoListTri (tri) {
	if (currentName != "") {
		currentTri = tri;
		updateCartoList();
	}
}

function updateCartoList() {
	if (currentName != "") {
		$j("#map_dialog").html("<img class='wait_img' src='design/images/wait4trans.gif' />");
		$j("#map_dialog").dialog( "option", "title", "");
		$j("#map_dialog").dialog( "option", "height", 150);
		$j.get("blank.php", { include: "getCartoData", refine: currentName , page: currentPage, tri: currentTri, ids: currentIdsDoc },
			function(data){
				$j("#map_dialog").html(data);
				$j("#map_dialog").dialog( "option", "height", 440);
			}
		);
	}
}
