$j(document).ready
(
	function()
	{
		//window.scrollTo(0, 1);

		// si petit écran, extension du champ de recherche textuel
/*		if ($j(window).width()<mobil_border){

			$j('#quick_cherche_txt').on('focus', function(event) {
				var largeur = $j('#header_menu').width() - 108;
				$j('#header_quick_srch_menu_block').css('width', largeur);
				$j('#header_quick_srch_menu_block').css('left', '50px');
				$j('#header_quick_srch_block').css('width', largeur);
				$j('#quick_cherche_txt').css('width', '100%');
			});
			$j('#quick_cherche_txt').blur(function(event) {
				$j('#header_quick_srch_menu_block').css('left', 'inherit');
				$j('#quick_cherche_txt').css('width', '90');
				$j('#header_quick_srch_block').css('width', '180');
				$j('#header_quick_srch_menu_block').css('width', '180');
			});
		}*/

		// NB 02.03.2015 - PANIER, pas d'état intermédiaire de panier (liste de résultat + panier) en responsive (tablette et mobile) : full ou collapsed
		if ($j(window).width()<tablet_border) {
			// $j("#panGauche .title_bar").attr("onclick","togglePanBlock('full');event.stopPropagation();");
			// $j("#panGauche .title_bar span").attr("onclick","togglePanBlock('full');event.stopPropagation();");

			//aération du menu de recherche avancée en tablette
			$j('#quick_search #adv_search_wrapper .adv_search_wrapper-1 table').attr("cellspacing","10");
			$j('#quick_search #adv_search_wrapper .adv_search_wrapper-1 table').attr("cellpadding","10");


			$j("#conteneur .title_bar .add.tool .drop_down li").each(function(idx,elt){
				elt.addEventListener('touchend', function () {
			    	$j(this).parent().parent().parent().trigger('touchend');
					$j(this).parent().parent().parent().trigger('mouseout');
				});
			});


		}



		$j('#main_block').on('click', function(event) {
			if ($j('#conteneur').hasClass('show-nav')){
				event.preventDefault();
				toggleMenu();
			}

		});

		// sortir du menu général si on clique en dehors
		$j('#conteneur').on('click', function(event) {
			if (!($j('#adv_search_button').hasClass('active'))) {
				if ($j('#conteneur').hasClass('disabled')){
					event.preventDefault();
					toggleMenuResponsive();
				}
			} else {
				toggleAdvFormWrapper();
			}
		});

		$j('#menuDroite').on('click', function(event) {
			if ($j('#conteneur').hasClass('show-nav')) {
				event.preventDefault();
				toggleMenu();
			}
		});

		
			var old_width=$j(window).width();
			var resizeId;
			$j(window).resize
			(
				function()
				{
					old_width=$j(this).width();
					clearTimeout(resizeId);
					resizeId = setTimeout(doneResizing, 500);



				}

			);


			// NB 01 06 2015 Hack pour fix bug d'affichage de prev Infos qui se superpose à la vignette du player lors du changement d'orientation du #menuDroite sur Iphone
			if ($j(window).width()<mobil_border){
				if ((($j('#menuDroite').css('display')) !='none') && navigator.userAgent.match(/iPhone/i)){
					var mql = window.matchMedia("(orientation: portrait)");
					// Add a media query change listener
					mql.addListener(function(m) {
						if(m.matches) {
							// Changed to portrait
							loadmobilesize();
						}
						else {
							// Changed to landscape
							loadmobilesize();
						}
					}, false);
				}
			}


			// Fix for IOS 7 devices on homepage (vh issue on iOS 7)
			var iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
			var iPhoneOS = navigator.userAgent.match(/(iPhone|iPod)/g) ? true : false;
			var MacOS = navigator.userAgent.match(/(Macintosh)/g) ? true : false;
			var iOSDevice = navigator.userAgent.match(/(OS 7)/g) ? true : false;

			if(iOS ){ // && iOSDevice
				/* toutes les regles "vh" sont reprises avec des %ages */
				$j('#homepage_quick_srch_wrapper').css('top', '48%');
				$j('body.homepage #homepage_scrolldown_arrow, body.homepage #homepage_scrollup_arrow').css('top', '75%');
				$j('#homepage_new_docs').css('height', '100%');
				$j('#homepage_thms').css('height', '100%');
				$j(' body.homepage #homepage_new_docs #homepage_new_docs_mediatype > span').css('height', '5%');
			}
			/*fix for little margin between bouton menu responsive and his menu On OSX and iOS*/
			//if (iOS || MacOS){
			if (iPhoneOS){
				$j('body.homepage #bouton-menu-responsive').css('margin-top','-2px');
			}

		

		
	}
);

function doneResizing(){
	if ($j(window).width()<tablet_border) {
		if (updateLayoutFlags()){
			updateLayoutFlags();
		}
		calcCarrouselLabelWidth()
		//alert('test');
	}
}

// NB 15 03 2016 - Fonction de calcul de la taille du label de l'image du carrousel sur la homepage
function calcCarrouselLabelWidth(){
	var window_width = parseInt($j(window).width());
	var carrousel_control_width = parseInt($j('#carrousel_control').width());
	if ($j(window).width()<mobil_border){
		var carrousel_label_width = window_width - carrousel_control_width - (window_width*3/100) - 18; // 18 = 10 + 8 de marge
	} else {
		var carrousel_label_width = window_width - carrousel_control_width - (window_width*3/100) - 28; // 28 = 20 + 8 de marge
	}

	$j(".carrousel_item_info").width(carrousel_label_width);
}

/*========================================
=            CUSTOM FUNCTIONS            =
========================================*/
function toggleMenu() {
	if ($j('#conteneur').hasClass('show-nav')) {
		// Do things on Nav Close
		$j('#conteneur').removeClass('show-nav');
	} else {
		// Do things on Nav Open
		$j('#conteneur').addClass('show-nav');
	}
}


function loadmobilesize() {
	setTimeout(function(){
		$j("#prevMedia").css('width', $j("#previewDoc").css('width'));
		w = parseInt($j("#previewDoc").css('width'),10);
		h = ( 9 * w ) / 16;
		$j("#prevWrapper").css('top',(h+parseInt($j('#prevTitre').outerHeight(),10)+20)+"px");
		$j("#prevMedia video").css('height',h+"px");
		$j("#prevMedia video").css('width',w+"px");
	}, 60);
}

