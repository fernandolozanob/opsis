<?

global $db;
$t_media=$db->getArray("SELECT ID_MEDIA FROM t_media order by 1");
$myUsr=User::getInstance();
$myPage=Page::getInstance();

//a commenter pour pannel admin
echo '<div id="pageTitre" class="title_bar"><?= kAdministration ?></div>';
if($myUsr->Type >= kLoggedInterne) {
?>
	<div class="contentBody admin" align="center">
		<div class="admin_column">
		<fieldset class='admin_zone'>
			<div class="main_admin_titre title_bar"><?=kMedias?></div>
			<ul class="main_admin_block">
				<li><a href="<?=$myPage->getName()?>?urlaction=docSaisie"><?=kCreerNotice?></a></li>
				<li><a href="<?=$myPage->getName()?>?urlaction=matListe"><?= kGestionMateriels ?></a></li>
				<?if($myUsr->Type == kLoggedAdmin){?>
					<li><a href="<?=$myPage->getName()?>?urlaction=imageurListe"><?= kGestionStoryboards ?></a></li>
					<li><a href="<?=$myPage->getName()?>?urlaction=tapeListe"><?= kGestionSauvegardes ?></a></li>
				<?}?>
				<li><a href="index.php?urlaction=importView"><?= kImportParChargement ?></a></li>
				<li><a href="index.php?urlaction=importView&import_type_form=folder"><?= kImportParDossier ?></a></li>
				<?if($myUsr->Type == kLoggedAdmin){?>
					<li><a href="<?=$myPage->getName()?>?urlaction=importXML"><?= kImportXML ?></a></li>
				<?}?>
				<li><a href="index.php?urlaction=importListe"><?= kTousImports ?></a></li>
				<li><a href="javascript:popupWindow('simple.php?urlaction=jobListe','Upload_mat','height=500,width=900,fullscreen=0,resizable=1,scrollbars=1,menubar=0,toolbar=0,status=1','main')"><?= kSuiviTraitements ?></a></li>
			</ul>
		</fieldset>
		<fieldset class='admin_zone'>
			<div class="main_admin_titre title_bar"><?=kLexiques?></div>
			<ul class="main_admin_block">
				<li><a href="<?=$myPage->getName()?>?urlaction=lexListe"><?= kThesaurus ?></a></li>
				<?if (gGestPers==1) echo "\n<li><a href=\"".$myPage->getName()."?urlaction=personneListe\">".kPersonnes."</a></li>\n";?>
				<?if (gGestCat==1 && $myUsr->Type >= kLoggedEdito) echo "\n<li><a href=\"".$myPage->getName()."?urlaction=catListe\">".kCategoriesEtThemes."</a></li>\n";?>
				<li><a href="<?=$myPage->getName()?>?urlaction=valListe"><?= kListeDeValeur ?></a></li>
			</ul>
		</fieldset>
		<? if($myUsr->Type >= kLoggedInterne) { ?>
		<fieldset class='admin_zone'>
			<div class="main_admin_titre title_bar"><?=kAdminUtilisateursDroits?></div>
			<ul class="main_admin_block">
				<? if($myUsr->Type == kLoggedAdmin) { ?>
					<li><a href="<?=$myPage->getName()?>?urlaction=usagerListe"><?= kUtilisateurs ?></a></li>
					<li><a href="<?=$myPage->getName()?>?urlaction=groupeListe"><?= kGroupesUtilisateurs ?></a></li>
					<li><a href="<?=$myPage->getName()?>?urlaction=fondsListe"><?= kGestionFonds ?></a></li>
					<li><a href="<?=$myPage->getName()?>?urlaction=comListe"><?=kCommandesUtilisateurs?></a></li>
				<?}?>
				<li><a href="<?=$myPage->getName()?>?urlaction=comListe&type_commande=5"><?=kBordereaux ?></a></li>
			</ul>
		</fieldset>
		<?}?>
		</div>
		<div class="admin_column">
			<? if($myUsr->Type >= kLoggedEdito) { ?>
				<fieldset class='admin_zone'>
					<div class="main_admin_titre title_bar"><?=kEditorial?></div>
					<ul class="main_admin_block">
						<li ><a href="<?=$myPage->getName()?>?urlaction=htmlListe"><?= kAdminPagesStatiques ?></a></li>
					</ul>
				</fieldset>
			<? } ?>		
			<fieldset class='admin_zone'>
				<div class="main_admin_titre title_bar"><?=kStatistiques?></div>
				<ul class="main_admin_block">
					<li><a href="<?=$myPage->getName()?>?urlaction=stats_app"><?=kStatistiquesOpsis?></a></li>
					<?php if(defined("gAwstatsUrl")){?>
						<li><a href="<?=$myPage->getName()?>?urlaction=stats_web"><?=kStatistiquesWeb?></a></li>
					<?}?>
				</ul>
			</fieldset>
			<? if($myUsr->Type == kLoggedAdmin) { ?>
			<fieldset class='admin_zone'>
				<div class="main_admin_titre title_bar"><?=kAdminSysteme?></div>
				<ul class="main_admin_block">
					<li><a href="<?=$myPage->getName()?>?urlaction=procListe"><?=kGestionProcessus?></a></li>
					<li><a href="<?=$myPage->getName()?>?urlaction=etapeListe"><?=kGestionEtapes?></a></li>
					<li><a href="<?=$myPage->getName()?>?urlaction=refListe"><?= kGestionTables ?></a></li>
					<li><a href="<?=$myPage->getName()?>?urlaction=maintenanceFichiers"><?= kAdminMaintenanceFichierOrphelins ?></a></li>
				</ul>
			</fieldset>
			<? } ?>		
		</div>
	</div><?
} else {
	echo "<h2><?= kAccesReserve ?></h2>";
	echo "<div align=\"center\"><a href=\"".$myPage->getName()."\"><?= kRetour ?></a></div>";
}
?>
