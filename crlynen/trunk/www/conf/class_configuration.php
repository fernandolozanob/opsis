<?php

class configuration
{
    public $config;
    
    public function __construct($filename)
    {
        if (!empty($filename)) {
           $config = $this->parse_ini_file_extended($filename);
			if(getenv('APPLICATION_ENV') !== false){
				$application_env = getenv('APPLICATION_ENV');
			}else if (defined('APPLICATION_ENV')){
				$application_env = APPLICATION_ENV;
			}else{
				$application_env = 'production';
			}
			// Ajout définition constant php LOADED_APPLICATION_ENV, car depuis qu'on utilise en priorité les variables d'environnement serveur par rapport aux constantes php pr définir APPLICATION_ENV, on peut avoir un APPLICATION_ENV différent de la conf réellement chargée. 
			define("LOADED_APPLICATION_ENV",$application_env);

			$this->config = $config[$application_env];
        }
    }
    
    private function parse_ini_file_extended($filename) {
        $p_ini = parse_ini_file($filename, true);
        $config = array();
        foreach($p_ini as $namespace => $properties){
            list($name, $extends) = array_merge( explode( ':', $namespace ), array( true ) );
            $name = trim($name);
            $extends = trim($extends);
            // create namespace if necessary
            if(!isset($config[$name])) $config[$name] = array();
            // inherit base namespace
            if(isset($config[$extends])){
                foreach($config[$extends] as $prop => $val){
                    if($prop == 'kChemin') $config[$name]['kCheminHttp'] = $this->makeHttpPath($val);
                    elseif($prop == 'kCheminMedia') $config[$name]['kCheminHttpMedia'] = $this->makeHttpPath($val);
                    elseif($prop == 'coreChemin') $config[$name]['coreUrl'] = $this->makeHttpPath($val);
                    else $config[$name][$prop] = $val;
                }
            }
            // overwrite / set current namespace values
            foreach($properties as $prop => $val){
                if($prop == 'kChemin') $config[$name]['kCheminHttp'] = $this->makeHttpPath($val);
                elseif($prop == 'kCheminMedia') $config[$name]['kCheminHttpMedia'] = $this->makeHttpPath($val);
                elseif($prop == 'coreChemin') $config[$name]['coreUrl'] = $this->makeHttpPath($val);
                else $config[$name][$prop] = $val;
            }
        }
        return $config;
    }
    
    private function makeHttpPath($val)
    {
        $path = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=="on")?"https://":"http://");
		
		if(isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])){
			$path .= $_SERVER['HTTP_HOST'];
		}else{
			$path .=  $_SERVER['SERVER_NAME'];
			if(isset($_SERVER['SERVER_PORT']) && !empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT']!='80'){
				$path .= ":".$_SERVER['SERVER_PORT'];
			}
		}
        $path .= $val;
        return $path;
    }
    
    public function defineConstantList()
    {
        foreach($this->config as $cle => $element) {
            define($cle,$element);
        }
    }
    
    public function defineDependentPath()
    {
        
        define('kLivraisonLocaleUrl',kCheminHttp.'/comLivraisonLocale.php');
        define('designUrl',kCheminHttp.'/design/');
        define('imgUrl',kCheminHttp.'/design/images/');
		//upload
		if(!defined("kTinymceUploadPath")) define("kTinymceUploadPath",kCheminLocalMedia."/html");
		if(!defined("kTinymceUploadPathRelativ")) define("kTinymceUploadPathRelativ",kCheminHttpMedia."/html");
        
        define('storyboardChemin',kCheminHttpMedia.'/storyboard/');
        define('kVisionnageUrl',kCheminHttpMedia.'/visionnage/');
        define('videosServer',kCheminHttpMedia.'/videos/');
        define('kDocumentUrl',kCheminHttpMedia.'/doc_acc/');
        define("kLivraisonUrl",kCheminHttpMedia."/livraison/");
        define("rapportServer",kCheminHttpMedia."/rapport/");
        define("kFckEditorRessourceDir",kCheminHttpMedia.'/html/');
        
		
        define('libUrl',coreUrl.'/lib/');
        define('frameUrl',coreUrl.'/frame/');
        define('upload_scriptUrl',coreUrl.'/upload/');
        define('designUrlCore',coreUrl.'/site/design/');
        define('imgUrlCore',coreUrl.'/site/design/images/');
        
        define('baseDir',kCheminLocalSurServeur);
        define('confDir',kCheminLocalSurServeur.'/conf/');
        define('designDir',kCheminLocalSurServeur.'/design/');
        define('imageDir',kCheminLocalSurServeur.'/design/images/');
        define('formDir',kCheminLocalSurServeur.'/design/form/');
        define('listeDir',kCheminLocalSurServeur.'/design/liste/');	
        define('templateDir',kCheminLocalSurServeur.'/design/templates/');
       
        define('kCheminLocalVisionnage',kCheminLocalMedia.'/visionnage/');
        define('klivraisonRepertoire', kCheminLocalMedia.'/livraison/');
        define('kStoryboardDir',kCheminLocalMedia.'/storyboard/');
        define('kDocumentDir',kCheminLocalMedia.'/doc_acc/');
        define('kThumbnailDir',kCheminLocalMedia.'/thumbnails/');
        define('tokenDir',kCheminLocalMedia.'/tokens/');
        define("kTrashDir",kCheminLocalMedia."/trash/");
        define("kRapportDir",kCheminLocalMedia."/rapport/");
        define("gFTdumpFile",kCheminLocalMedia."/ftdump.txt");
        define("kFckEditorRessourceAbsolutePath",kCheminLocalMedia.'/html/');
        
        define('kVideosDir',kCheminLocalPrivateMedia.'/videos/');
        define('kImagesDir', kCheminLocalPrivateMedia.'/images/');
        if(!defined('kImportDir')) define('kImportDir', kCheminLocalPrivateMedia.'/import/');
        if(!defined('FCPWatchFolder')) define('FCPWatchFolder',kCheminLocalPrivateMedia.'/import/');
        		
        define('libDir',coreDir.'/lib/');
		define('modelDir',coreDir.'/model/');

        define('includeDir',coreDir.'/include/');
        define('frameDir',coreDir.'/frame/');
        define('upload_scriptDir',coreDir.'/upload/');
        define('webserviceDir',coreDir.'/webservice/');
        define('batchDir',coreDir.'/batch/');
        define('oaiDir',coreDir.'/oai/');
        define('siteDirCore',coreDir.'/site/');
        define('confDirCore',siteDirCore.'conf/');
        define('designDirCore',siteDirCore.'design/');
        define('imageDirCore',designDirCore.'images/');
        define('formDirCore',designDirCore.'form/');
        define('listeDirCore',designDirCore.'liste/');	
        define('templateDirCore',designDirCore.'templates/');
        
        define('jobInDir',jobDir.'/in'); // ce repertoire contient les fichiers XML en attente de traitement
        define('jobRunDir',jobDir.'/run'); // ce repertoire contient les fichiers XML en cours de traitement
        define('jobDoneDir',jobDir.'/done');
        define('jobSyncDir',jobDir.'/synchrone');
        define('jobOutDir',jobDir.'/out'); // fichier de retour  par le frontal
        define('kBackupInfoDir',jobDir.'/info'); // "Dossier d’information utilisé par le backup"
        define('jobTmpDir',jobDir.'/tmp'); // repertoire temporaire
        define('jobCancelDir',jobDir.'/cancel'); // XML en attente d'annulation
        define('jobCancellingDir',jobDir.'/cancelling'); // XML en cours d'annulation
        define('jobCanceledDir',jobDir.'/canceled'); // XML des fichiers annulés
        define('jobDeleteDir',jobDir.'/delete'); // XML des traitements en attente de suppression
        define('jobDeletingDir',jobDir.'/deleting'); // XML des traitements en cours de suppression
        define('jobDeletedDir',jobDir.'/deleted'); // XML fichiers XML supprimmés
        define('jobErrorDir',jobDir.'/error'); // XML des erreurs de traitementdefine('jobInDir',jobDir.'/in'); // ce repertoire contient les fichiers XML en attente de traitement
        define('jobSettingDir',jobDir.'/etc/setting'); // fichiers utilisés par les traitements (images,polices ....)
        define('jobImageDir',jobDir.'/etc/images/');
        define('jobMusiqueDir',jobDir.'/etc/musique/');
        define('jobFontDir',jobDir.'/etc/fonts/');
		
		preg_match("/opsis([0-9.]+)$/",coreDir,$matches);
		define('coreVersion',$matches[1]);
    }
}
?>