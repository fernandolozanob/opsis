<?
// Expressions spécifiques à la démo
define("kMsgCommandeAchat","Merci de remplir ce bon de commande en indiquant le type de support que vous voudriez recevoir.<br/>Si vous choisissez le paiement en ligne, la page de test de notre partenaire PAYBOX de paiement par carte de crédit sera affichée.<br/>
<strong>Ce bon de commande n'est fourni qu'à titre de démonstration et n'entraînera à ce titre ni facturation ni livraison.</strong>");
define("kMsgCommandeAchatRecap","<strong>Ce bon de commande n'est fourni qu'à titre de démonstration et n'entraînera à ce titre ni facturation ni livraison.</strong>");
define("kMsgCommandeTelecharger","La validation de ce bon de commande donne accès à la page de téléchargement des éléments de votre commande.<br/>
Dans cette démonstration, le téléchargement est opérationnel et gratuit.");
define("kMsgCommandeTelechargerRecap","Vous pouvez télécharger chaque élément de votre commande de manière unitaire,
<strong>en cliquant sur la disquette</strong> de la ligne correspondante.");
define("kInscriptionEntete","Merci de remplir le formulaire d'inscription ci-dessous.<br/>Vos données personnelles ne seront pas conservées, car la base de données de cette démonstration est réinitialisée chaque nuit.");
define("kMsgEntete","Opsis Media, une suite logicielle Internet conçue pour la gestion,<br/> la communication et la commercialisation d'archives audiovisuelles");

// HTML5
define("kMsgTryHTML5","Essayez notre nouveau lecteur vidéo HTML5");

//Page d'accueil
define("kEveryDocV","Toutes les vidéos");
define("kEveryDocA","Tous les sons");
define("kEveryDocP","Toutes les images");
define("kEveryDocD","Tous les documents");
define("kRetourListeRecherche","Retour &agrave; la liste de recherche");

define("kVideos","Vidéos");
define("kImages","Images");
define("kAudios","Audios");
define("kDocuments","Documents");
define("kReportages","Reportages");


// Administration
define("kConfirmViderpanier","Confirmez-vous la suppression ?");

define("kParametreEntete",'Paramètres de l\'entête');
define("kParametres",'Paramètres');

define('kErrorGroupeSauveUsager','Erreur lors de l\'ajout de l\'usager au groupe');
define('kErrorGroupeSauveFonds','Erreur lors de l\'ajout du fond au groupe');

//formulaire de recherche
define("kDatePublication","Date de publication");

define("kAjouterDossierDoc","Ajouter dans le dossier thématique ...");

define('kUrl','URL');

define('kPartager','Partager');
define('kReseauxSociaux','Réseaux sociaux');
define('kFacebook','Facebook');
define('kTwitter','Twitter');
define('kLinkedIn','Linked in');
define('titre_trop_long_twitter','Le titre est trop long pour Twitter');
define('titre_trop_long_linkedin','Le titre est trop long pour Linked in');


// site mobile
define('kVoulezVousEtreRedirigeMobile','Voulez-vous etre redirigé vers le site mobile ?');

// Montage
define("kAjouterAuMontage","ajouter au montage");
define("kAtelierMontage","Atelier de montage");
define("kVoirDemo","Voir la démo");
define("kChutier","chutier");
define("kGlissezIci","Glissez ici");
define("kUnPlanDeMesSelections","un plan de « mes sélections »");
define("kAfficherAutresPlans","afficher les autres plans");
define("kCliquezGlissez","cliquez-glissez");
define("kPourChangerOrdrePlans","pour changer l’ordre des plans");
define("kMontage","montage");
define("kVisionneuse","visionneuse");
define("kCreerNouveau","créer nouveau");
define("kSansTitre","sans titre");
define("kJouerClip","jouer le clip");
define("kJouerMontage","jouer le montage");
define("kSupprimerDuMontage","supprimer du montage");
define("kSupprimerDuChutier","supprimer du chutier");
define("kJSMsgSelectClip","Sélectionner d'abord un clip !");
define("kSauvegarderMontage","sauvegarder montage");
define("kDefinirDebut","définir début");
define("kDefinirFin","définir fin");


define('kMentionslegales','Mentions légales');


define("kSloganOpsis","");
define("kImporter","Importer");
define("kRechercher","Rechercher");
define("kOk","OK");
define("kFiltres","Filtres");
define("kAfficherTout","Afficher Tout");
define("kAfficherTopResults","Afficher les filtres principaux");
define("kLoadMoreResults","Charger plus de résultats");
define("kPreview","Preview");
define("kVoirFicheDoc","Voir la notice");
define("kDropZoneText","Déposez un document ici pour l'ajouter à la sélection");
define("kQuitterMontage","Quitter le mode montage");

// Paramètres liés à la vérification anti-bots
define("kGatePopinTitle","Bienvenue sur Scopitone");

// Spécifique demo
define("kMsgConnexionRefuseeEtat","Votre compte a été désactivé.<br />Merci de contacter l’administrateur.");


//FieldRef_doc
//Identification
define("kTypeDeDocument","Type de document");
define("kTypeDeMedia","Type de média");
define("kTitreOri","Titre Valide");
define("kComplementDeTitre","Complément de titre");
define("kAncienneReference","Ancienne référence");
define("kResume","Résumé");
define("kComplementDescription","Complément de description");
define("kComplementDeDate","Complément de date");
define("kDateAcquisition","Date d'acquisition");
define("kNumeroSAF","Numéro SAF");
define("kImportanceMaterielle","Importance matérielle");
define("kAutreInscription","Autre inscription");

//Technique
define("kTechnique","Technique");
define("kNumerisation","Numérisation");
define("kCouleurs","Couleurs");
define("kSuppOrigine","Support d'origine");
define("kTypeObjet","Type d'objet");
define("kComplementDeType","Complément de type");
define("kFormatPVP","Format pellicule, vidéo, passe-vue");
define("kTechniqueImage","Technique d'image");
define("kProcede","Procédé");
define("kImpression","Impression");
define("kLongeur","Longueur");
define("kProfondeur","Profondeur");
define("kPoids","Poids (kg)");
define("kMateriaux","Matériaux");
define("kTechniqueRealisation","Techniques de réalisation");

//Contenu
define("kContenu","Contenu");
define("kNationalite","Nationalité");
define("kIntituleAtelier","Intitulé de l'atelier");
define("kŒuvresCitees","Œuvres citées");

//Indexation
define("kNomCommun","Nom commun");

//Droits
define("kDateDroitDebut","Date de début des droits");
define("kDateFinDroits","Date de fin des droits");
define("kAyantDroit","Ayant droit");
define("kAccesPublic","Accès public");
define("kAutreLocalisation","Autre localisation");

//Interne
define("kInterne","Interne");
define("kCommentaire","Commentaire");
define("kPublicationExpo","Publication / Expos");
define("kRefBibliographique","Ref Bibliographique");


//Matériel
define("kTypeDeSupport","Type de support");
define("kCoteExemplaire","Cote exemplaire");
define("kCodeBarre","Code barre");
define("kMetrageReel","Métrage réel");
define("kMetrageOri","Métrage d'origine");
define("kNbCollures","Nb collures");
define("kPerformationReparees","Perforations réparées (en m.)");
define("kNbBobines","Nb bobines");
define("kLieuxDeConservation","Lieux de conservation");
define("kFormatImage","Format image");
define("kDimension","Dimension");
define("kProvenance","Provenance");
define("kDateEntreeLaCinematheque","Date d'entrée à la cinématheque");
define("kRestaurateur","Restaurateur");
define("kDateDeRestauration","Date de restauration");
define("kTirageRealisePar","Tirage réalisé par");
define("kDateDeTirage","Date de tirage");
define("kDateDeNumerisation","Date de numérisation");
define("kNombreDeVues","Nombre de vues");
define("kMarqueDuSupport","Marque du support");
define("kNombreDeVues","Nombre de vues");
define("kNumeroDeSerie","Numéro de série");
define("kEtatDate","Etat date");
define("kVitesseDeRotation","Vitesse de rotation");
define("kLaquage","Laquage");
//define("","");
define("kDatePVDebutRe","Date de réalisation");

define("kMentionsLegales","Mentions légales");

define("kRechercheBordereaux","Recherche de bordereaux");
define("kBordereaux", "Bordereaux");
define("kGestionBordereaux","Gestion des bordereaux");
define ('kBordereau', 'Bordereau');
define ('kClient', 'Client');
define ('kUtilisateurFinal', 'Utilisateur final');
define ('kDateUtilisation', 'Date d\'utilisation');
define ('kComplementDate', 'Complément de date');
define ('kDateFinUtilisation', 'Date de fin d\'utilisation');
define ('kDateDebutBlocage', 'Date de début de blocage');
define ('kDateFinBlocage', 'Date de fin de blocage');
define ('kSortieReelle', 'Sortie réelle');
define ('kRetourReel', 'Retour réelle');
define ('kNbSpectateurs', 'Nombre de spectateurs');
define ('kNbSeances', 'Nombre de séances');
define ('kModeExpedition', 'Mode d\'expédition');
define ('kModeRetour', 'Mode de retour');
define ('kExemplaires', 'Exemplaires');
define("kCategoriesEtThemes", "Dossiers thématiques");
define("kCategorie", "Dossier thématique");
define("kGestionCategories", "Gestion des dossiers thématiques");
define("kRechercheCategorie", "Recherche de dossiers thématiques");
define("kMotifDemande", "Motif de la demande");
define("kAnneeNaissance", "Année de naissance");
define("kPanier","Sélection");
define("kPaniers","Sélections");
define("kNouveauPanier","Nouvelle sélection");
define("kConfirmPanierSupprimer","Confirmez-vous de supprimer Panier ?");
define("kConfirmPanierVider","Confirmez-vous de vider la sélection ?");
define("kErrorPanierLigneExisteDeja","Cette notice est déjà présente dans la sélection.");
define("kPanierVide","la sélection est vide");
define("kSelectionnerToutPanier","Sélectionner toute la sélection");
define("kSupprimerPanier","Supprimer Panier");
define("kViderPanier","Vider la sélection");
define("kAdministration","Gestion");
define("kMesCommandes","Mes demandes");
define("kDatePVFin", "Date de fin de réalisation");
define("kNotesJuridiques", "Complément des droits");
define("kNomsCommuns", "Noms communs");
define("kListeExemplaires", "Liste des exemplaires");
define("kAjouterTheme_long", " Attribuer les documents au dossier thêmatique");
define ("kDiffuse", "Diffusé");
define ("kFonds", "Fonds particulier");
define ("kAnneeReal", "Année de réalisation");
define ("kDimensionOri", "Dimension de l'original (en cm)");
define ("kModele", "Modèle");
define ("kDateFabrication", "Date de Fabrication");
define ("kDatePublication", "Date de publication");
define ("kDateParution", "Date de 1ere parution");
define ("kDateEnregistrement", "Date d'enregistrement");
define ("kDiametreBobine", "Diamètre bobine (cm)");
define ("kLieuEnregistrement", "Lieu d'enregistrement");
define ("kNotesEtat", "Notes sur l'état");
define ("kTypeSon", "Type de son");
define ("kTypeDePellicule", "Type de pellicule");
define ("kElementAssocie", "Element associé");
define ("kQualiteSon", "Qualité du son");
define ("kIntertitres", "Intertitres");
define ("kFacSRealisePar", "Fac-similé réalisé par");
define ("kDateDeFacS", "Date du Fac-similé");
define ("kDimensionOriginal", "Dimension de l'original (en cm)");
define ("kDureeFaceA", "Durée Face A");
define ("kDureeFaceB", "Durée Face B");
define ("kChangerEtat", "Changer l'état de tous les exemplaires à ");
define ("kModificationsToDo", "Modification à apporter");
define ("kLieuPublication", "Lieu de publication");
define ("kMediaExterneEmbarquee", "Média externe embarqué");
define ("kInterneExterne", "Interne et externe");
define ("kTexteExplicatif", "Texte explicatif");
define ("kNouvelExemplaire", "Nouvel exemplaire");
define ("kPerforations", "Perforations");
define ("kInfosBordereau", "Informations bordereau");
define ("kContenuBordereau", "Contenu du bordereau");
define ("kDureeTotale", "Durée totale");
define ("kDemandeConsultation", "Demande de consultation");
define ("kMsgCommandeDemande", "La validation de ce bon de commande donne accès à la page de téléchargement des éléments de votre commande.");
define ("kDateSouhaitee", "Date souhaitée");
define ("kModele", "Modèle");
define ("kModele", "Modèle");

define("kAffichageCarto","Affichage carte");

?>
