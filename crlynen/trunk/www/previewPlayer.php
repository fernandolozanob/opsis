<?php

	if(!isset($_GET['id']) || !isset($_GET['type'])){
		exit ;
	}

	error_reporting(0);
	if(isset($_GET['target']) && !empty($_GET['target'])){
		$target = $_GET['target'];
	}

	$time_start = microtime(true);
	require_once("conf/conf.inc.php");
	require_once(libDir."session.php");
	require_once(libDir."fonctionsGeneral.php");
	require_once(libDir."class_page.php");
	$myUsr=User::getInstance();

	if($_GET['type'] == 'doc'){
		require_once(modelDir.'model_doc.php');
		$myDoc = new Doc();
		$myDoc->t_doc['ID_DOC'] = $_GET['id'];
		$myDoc->t_doc['ID_LANG'] = $_SESSION['langue'];
		$count_init = count($myDoc->t_doc['t_doc']);
		$myDoc->getDoc();
		$myDoc->getMats();
	}else if ($_GET['type'] == 'pdoc'){
		require_once(modelDir.'model_panier.php');
		$row_pan = Panier::getLignePanier($_GET['id']);
		if(isset($row_pan['ID_DOC'])){
			require_once(modelDir.'model_doc.php');
			$myDoc = new Doc();
			$myDoc->t_doc['ID_DOC'] = $row_pan['ID_DOC'];
			$myDoc->t_doc['ID_LANG'] = $_SESSION['langue'];
			$count_init = count($myDoc->t_doc['t_doc']);
			$myDoc->getDoc();
			$myDoc->getMats();
		}
	}
	// echo intval(!isset($myDoc))." ".intval(count($myDoc->t_doc)<=$count_init)." ".$count_init." ".count($myDoc->t_doc);
	if(!isset($myDoc) || count($myDoc->t_doc)<=$count_init){
		echo "document not found ";
		exit ;
	}



	require_once(modelDir.'model_materiel.php');
	$visu_type="document";
	$id_visu  = $myDoc->t_doc['ID_DOC'];
	$vignette = $myDoc->vignette;

	$id_mat_vis=$myDoc->getMaterielVisu();
	
	
	if($target == 'frame_previewWrapper'){?>
		<div class="backbutton">
		<a class="icon-prev" href="#" onclick='hideHoverPlayer();$j("#frame_previewWrapper").removeAttr("class");$j(this).parents("#map_dialog.previewActive").removeClass("previewActive")'>&amp;nbsp;</a>
		</div>
		<script>
		$j( "a.icon-prev" ).button({
								   icons: {
								   primary: "ui-icon-carat-1-w"
								   },
								   text: false
								   });
		</script>
	<?}?>



<script type="text/javascript" >
	// Définition des options relatives au layout du site, envoyées lors de l'appel
	if(typeof(player_options) == "undefined"){
		var player_options = {};
	}
	player_options['resizeCustom'] = true;
	player_options['enable_streaming'] = true;
	player_options['span_time_passed'] = true;
	player_options['displayLoopButton'] = true;
	player_options['autoplay'] = true;
</script>
<!-- =================================CAS DOC VIDEO===========================================-->
<?php
$arrFormatVisio = array("MPEG4", "H264");

if(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="V")
	|| ($visu_type == "materiel" && $myMat->hasfile && in_array($myMat->t_mat['MAT_FORMAT'], $arrFormatVisio))){
	$img=(empty($vignette)?"design/images/vignette_blank.png":"makeVignette.php?image=".$vignette."&amp;w=480&amp;kr=1");
	?>

	<div id="media_space" class="video">
		<div id="<?=( $target == 'previewHoverWrapper')?'previewHover_container':"container";?>" style="visibility:hidden;">
		</div>
	</div>
	<script>
		<? if($_GET['type'] == 'pdoc'){?>
			_visu_type = 'visuExtrait';
			id = '<?=$_GET['id']?>';
			player_options['controls'] = false ;
		<?}else{?>
			id='<?=$id_visu?>';
			_visu_type = '<?=$visu_type?>';
		<?}?>


		<?if( $target == 'previewHoverWrapper'){?>
			player_options['controls'] = false;
			player_options['container_id'] = 'previewHover_container';
		<?}else{?>
			var player_options = {

			resizeCustom : true,
			controlbar : 'none',
			selectionSliderDisabled : true,
			autoplay : true,
			enable_streaming : true,
			previewOverTimeline : false,
			code_controller : '<div id="ow_id_progress_slider">	</div>				' +
			'	<div id = "ow_boutons">' +
			'		<span id = "ow_boutons_left">' +
			'			<a class = "btn play" id = "ow_bouton_play_pause" ></a>' +
			'			<p id="timecodeDisplay">00:00:00</p>' +
			'			<p id="timecodeDuration">/ 00:00:00</p>' +
			'		</span><span id = "ow_boutons_right">' +
			'			<a class = "btn" id = "ow_bouton_mute_sound"></a>'+
			'			<div id = "containSound"><div id = "ow_id_sound_slider"></div></div>' +
			'			<a class = "btn" id = "ow_bouton_fullscreen"></a>' +
			'			<a class="btn" id="ow_bouton_loop"></a>'+
			'		</span>' +
			'	</div>',
			hauteur_bg_track : 10,
			timecode_display_format : 'hh:mm:ss',
			controller_on_video : true,
			span_time_passed: true,
			dynamicController : true
	};
		
		if(typeof OW_env_user != 'undefined' && OW_env_user.browser == 'Safari' && OW_env_user.env_OS == 'Mac'){
			player_options['xStand'] = true;
		}


		player_options.skin="<?=kCheminHttp?>/design/css/skin_simple_player/skin_flash/skin_flash.xml";
		player_options.showFlashController = true;
		player_options.controlbar="over";
		player_options.offset_controlbar_height=34;
		player_options.simpleController = true ;
		<?}?>
		player_options['css_path']='design/css/skin_simple_player/';
		player_options['image'] = "<?=$img?>";// "makeVignette.php?image=<?=$vignette?>&w=480&kr=1";

		player_options.onPlayerReady = function(){
			try{
				current_h = $j("#previewHoverWrapper").get(0).offsetHeight ;
				delta_h = Math.max($j("#previewContainer").get(0).offsetHeight-current_h,0);
				if($j("#previewHoverWrapper").hasClass('fix_to_bot')){
					target_top =  parseInt($j("#previewHoverWrapper").css('top'),10)  - delta_h;
				}else{
					target_top = parseInt($j("#previewHoverWrapper").css('top'),10);
			}
			$j('#previewHoverWrapper').animate({
					top : target_top+"px",
				   'min-height': $j("#previewContainer").get(0).offsetHeight
				}, { duration: 100, queue: false });

			}
			catch(e){
				console.log("crash : "+e);
			}
			$j("#<?=( $target == 'previewHoverWrapper')?'previewHover_container':"container";?>").css('visibility','visible');
			$j("#previewHoverInfos").css('visibility','visible');
		};
		

		function finalizePlayer(opts){
			if(_visu_type == 'visuExtrait' && 	 document.getElementById('player_html_simple')!=null){
				document.getElementById('player_html_simple').addEventListener('loadedmetadata',function(){
					document.getElementById('player_html_simple').currentTime = 0;
				});
		}

		}
		
		try{
			<?if( $target == 'previewHoverWrapper'){?>
				loadSimpleOraoPlayer(id, $j('#previewHover_container').width(),'_vis',player_options,_visu_type);
			<?}else{?>
				loadOraoPlayer(id, $j('#container').width(),'_vis',player_options,null,_visu_type,'myVideo_prevDoc');
			<?}?>
		}catch(e){
			console.log('crash loading media ');
		}
		</script>

<!-- =================================CAS DOC REPORTAGE=========================================== -->
<?}elseif ($visu_type == "document" && get_class($myDoc) == "Reportage"){
	$id_visu = $myDoc->t_doc['ID_DOC']; ?>
	
	<style>
		#mon_slide #ow_boutons, #mon_slide #ow_legend, #mon_slide #ow_thumbs{
			display : none ;
		}
		#mon_slide #ow_obj_image{
			margin-top : 0px !important;
		}
		#mon_slide {
			height: 400px;
			width: 400px;
		}
	</style>

	<div id="media_space">
		<div id="container">
			<div id="mon_slide">
				<script type="text/javascript">
				function playSlideshowReport(id, type)
				{
					var options=
					{
						width:'100%',
						height:'700px',
						url:'blank.php?xmlhttp=1&urlaction=prepareVisu&method=Diaporama&action=visu&id='+id+'&id_lang=fr&pattern=vis&type='+type,
						loop:true,
						toggle_thumbs:true,
						stepping_frequency:3
					};
					requirejs(['oraoweb/slideshow/OW_slideshow'],function(){
						requireCSS('<?=libUrl?>/webComponents/oraoweb/css/ow-slideshow.css',function(){
							my_player=new OW_slideshow('mon_slide',options);
						});
					});
				}

				playSlideshowReport('<?= $id_visu; ?>','reportage');
				setTimeout(function(){my_player.playSlideshow()},100);
				$j("#previewHoverInfos").css('visibility','visible');

				</script>
			</div>
		</div>
	</div>

<!-- =================================CAS DOC PICTURE=========================================== -->
<?}elseif(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="P")
	|| ($visu_type == "materiel" && $myMat->hasfile &&( $myMat->t_mat['MAT_FORMAT']=='GIF' || $myMat->t_mat['MAT_FORMAT']=='JPEG' || $myMat->t_mat['MAT_FORMAT']=='JPG' || $myMat->t_mat['MAT_FORMAT']=='PNG') )){

	if($visu_type == "document"){
		$id_mat_visu=$myDoc->getMaterielVisu();
		$id_doc_mat=0;

		foreach ($myDoc->t_doc_mat as $mat)
		{
			if ($mat['ID_MAT']==$id_mat_visu)
				break;

			$id_doc_mat++;
		}

		// redéfinition de l'id_visu vers le materiel pour les images
		$id_visu = $myDoc->t_doc_mat[$id_doc_mat]["MAT"]->t_mat['ID_MAT'];
		$mat_visu =  $myDoc->t_doc_mat[$id_doc_mat]["MAT"];

	}else if ($visu_type == "materiel"){
		$mat_visu =$myMat;
		// $arrsize =  array($myMat->t_mat['MAT_WIDTH'],$myMat->t_mat['MAT_HEIGHT']);
	}

	if(isset($mat_visu->t_mat['MAT_WIDTH']) && $mat_visu->t_mat['MAT_WIDTH']!="0"
		&& isset($mat_visu->t_mat['MAT_HEIGHT'])  && $mat_visu->t_mat['MAT_HEIGHT']!="0"){
			$arrsize = array($mat_visu->t_mat['MAT_WIDTH'],$mat_visu->t_mat['MAT_HEIGHT']);
		}else{
			require_once(libDir."class_matInfo.php");
			$infos = MatInfo::getMatInfo($myDoc->t_doc_mat[$id_doc_mat]["MAT"]->getLieu().$myDoc->t_doc_mat[$id_doc_mat]["MAT"]->t_mat['MAT_NOM']);
			$arrsize = array($infos["width"],$infos["height"]);
			unset($infos);
		}

	$max_size = max($arrsize);
	if(intval($max_size)>2000){
		$max_size=2000;
	}
	?>

	<link href="<?=libUrl?>webComponents/Mapbox/css/map.css" type="text/css" rel="stylesheet" />

	<div id="media_space">
		<div id="container" class="mapwrapper" style="height:360px;">
			<div id="viewport" style="width:100%; height:100%;">
				<div id="divPart" style="width: 480px; height:360px;">
					<img src="makeVignette.php?image=<?= $id_visu ?>&type=video&w=<?=$max_size?>&h=<?=$max_size?>&kr=1" width="100%" height="100%" />
				</div>
				<div id="divComplete" style="width: 960px; height: 720px;">
					<img id="imgComplete" src="makeVignette.php?image=<?= $id_visu ?>&type=video&w=<?=$max_size?>&h=<?=$max_size?>&kr=1" alt="" />
					<div class="mapcontent">
					</div>
				</div>
			</div>
			<div id="control" style="<?=($target=='previewHoverWrapper')?'display:none;':''?>" class="zoom-control">
				<a href="#zoom" class="zoom">Zoom</a>
				<a href="#zoom_out" class="back">Back</a>
				<a href="#fullscreen" class="fullscreen">Fullscreen</a>
			</div>
		</div>
	</div>

	<script type ="text/javascript">

		var ratio = <?=$arrsize[1];?> / <?=$arrsize[0];?>; // var pour transmettre les éléments php > javascript pour mapboxresize
		
		requirejs(['Mapbox/js/mousewheel','Mapbox/js/mapbox'],function(){
			initMapboxResize();
			$j("#previewHoverInfos").css('visibility','visible');
		});
	</script>

<!-- =================================CAS DOC PDF==========================================-->
<?}elseif(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="D")
	|| ($visu_type == "materiel" && $myMat->hasfile && $myMat->t_mat['MAT_FORMAT']=='PDF')){
		// redéfinition de l'id_visu dans le cas "document"
		if($visu_type == "document"){
			$id_visu = $id_mat_vis;

		}?>
	<style>
		#mon_slide #ow_boutons, #mon_slide #ow_legend, #mon_slide #ow_thumbs{
			display : none ;
		}
		#mon_slide #ow_obj_image{
			margin-top : 0px !important;
		}
		#mon_slide{
			height: 260px;
			width: 400px;
		}
		#ow_image{
			height : 260px;
			width : 400px;
		}


	</style>
	<div id="media_space" style="margin-top : 8px;">
		<div id="container">
			<div id="mon_slide">
				<script type="text/javascript">

				if(typeof(options) == "undefined"){
					var options={};
				}
				if(typeof(options['width']) == "undefined") options['width'] = '400px';
				if(typeof(options['height']) == "undefined") 	options['height'] = '260px';
				if(typeof(options['loop']) == "undefined") options['loop'] = true;
				if(typeof(options['toggle_thumbs']) == "undefined") options['toggle_thumbs'] = true;
				if(typeof(options['stepping_frequency']) == "undefined") options['stepping_frequency'] = 3;


				function playSlideshow(id, type)
				{
					options['url'] = 'blank.php?xmlhttp=1&urlaction=prepareVisu&method=Diaporama&action=visu&id='+id+'&id_lang=fr&pattern=vis&type='+type;
					requirejs(['oraoweb/slideshow/OW_slideshow'],function(){
						requireCSS('<?=libUrl?>/webComponents/oraoweb/css/ow-slideshow.css',function(){
							my_player=new OW_slideshow('mon_slide',options);
						});
					});
				}

				playSlideshow('<?= $id_visu; ?>','mat');
				setTimeout(function(){my_player.playSlideshow()},100);
				$j("#previewHoverInfos").css('visibility','visible');

				</script>
			</div>
		</div>
	</div>

<!-- =================================CAS DOC AUDIO==========================================-->
<?}elseif(($visu_type == "document" && !empty($id_mat_vis) && $myDoc->t_doc['DOC_ID_MEDIA']=="A")
	|| ($visu_type == "materiel" && $myMat->hasfile && $myMat->t_mat['MAT_FORMAT']=='MP3' )){
	?>
		<div id="media_space" class="audio">
			<div id="<?=( $target == 'previewHoverWrapper')?'previewHover_container':"container";?>">
			</div>
		</div>
	<script type="text/javascript">
		player_options['resizeCustom'] = false;

		<? if($_GET['type'] == 'pdoc'){?>
			_visu_type = 'visuExtrait';
			id = '<?=$_GET['id']?>';
			// player_options['controls'] = false ;
		<?}else{?>
			id='<?=$id_visu?>';
			_visu_type = '<?=$visu_type?>';
		<?}?>


		<?if( $target == 'previewHoverWrapper'){?>
			player_options['controls'] = false;
			player_options['container_id'] = 'previewHover_container';
		<?}else{?>
			var player_options = {

			resizeCustom : true,
			controlbar : 'none',
			selectionSliderDisabled : true,
			autoplay : true,
			enable_streaming : true,
			previewOverTimeline : false,
			code_controller : '<div id="ow_id_progress_slider">	</div>				' +
			'	<div id = "ow_boutons">' +
			'		<span id = "ow_boutons_left">' +
			'			<a class = "btn play" id = "ow_bouton_play_pause" ></a>' +
			'			<p id="timecodeDisplay">00:00:00</p>' +
			'			<p id="timecodeDuration">/ 00:00:00</p>' +
			'		</span><span id = "ow_boutons_right">' +
			'			<a class = "btn" id = "ow_bouton_mute_sound"></a>'+
			'			<div id = "containSound"><div id = "ow_id_sound_slider"></div></div>' +
			'			<a class = "btn" id = "ow_bouton_fullscreen"></a>' +
			'			<a class="btn" id="ow_bouton_loop"></a>'+
			'		</span>' +
			'	</div>',
			hauteur_bg_track : 10,
			timecode_display_format : 'hh:mm:ss',
			span_time_passed: true,
			show_vignette_audio : true,
			res_vignette_width : "",
			dynamicController : true,
			controller_on_video : true , 
			poster_width : 286,
			poster_height : 215
	};
		<?}?>
		player_options['css_path']='design/css/skin_simple_player/';
		player_options.onPlayerReady = function(){
			try{
				current_h = $j("#previewHoverWrapper").get(0).offsetHeight ;
				delta_h = Math.max($j("#previewContainer").get(0).offsetHeight-current_h,0);
				if($j("#previewHoverWrapper").hasClass('fix_to_bot')){
					target_top =  parseInt($j("#previewHoverWrapper").css('top'),10)  - delta_h;
				}else{
					target_top = parseInt($j("#previewHoverWrapper").css('top'),10);
				}
			$j('#previewHoverWrapper').animate({
					top : target_top+"px",
				   'min-height': $j("#previewContainer").get(0).offsetHeight
				}, { duration: 100, queue: false });

			}
			catch(e){
				console.log("crash : "+e);
			}
			$j("#container").css('visibility','visible');
			$j("#previewHoverInfos").css('visibility','visible');
		};


		<?if (empty($vignette)) {?>
			player_options['image'] =  "design/images/audio.png";
		<?}else{?>
			player_options['image'] = "makeVignette.php?image=<?=$vignette?>&w=480&kr=1";
		<?}?>
		
		<?if( $target == 'previewHoverWrapper'){?>
			loadSimpleOraoPlayer(id, $j('#previewHover_container').width(),'.mp3',player_options,_visu_type);
		<?}else{?>
			loadOraoPlayer(id, $j('#container').width(),'.mp3',player_options,null,_visu_type,'myVideo_prevDoc');
		<?}?>


		// loadSimpleOraoPlayer('<?= $id_visu ?>',  $j('#container').width(),null,player_options,_visu_type);
		$j("#previewHoverInfos").css('visibility','visible');
	</script>
<? } elseif(!empty($vignette)){
?>
	<div id="media_space">
		<div id="container">
			<img id="poster" src="makeVignette.php?image=<?=$vignette?>&amp;w=480&amp;kr=1" width="100%"/>
		</div>
	</div>
<? }/*else{
		header("HTTP/1.0 404 Not Found");

	?>
		<script>if (document.getElementById('fiche_doc')) document.getElementById('fiche_doc').className+=" no_media";</script>
<? } */?>

<?if( $target == 'previewHoverWrapper' || $target=='frame_previewWrapper'){?>
<div id="previewHoverInfos" style="visibility : hidden ;">
	<?
	if(isset($myDoc->t_doc['DOC_TITRE']) && !empty($myDoc->t_doc['DOC_TITRE'])){
		echo "<div style=\"text-align:center;font-weight:bold;padding-bottom:4px;\">".$myDoc->t_doc['DOC_TITRE']."</div>";
	}

	echo "<div>";

	if(isset($myDoc->t_doc['DOC_DATE_PROD']) && !empty($myDoc->t_doc['DOC_DATE_PROD']) && $myDoc->t_doc['DOC_DATE_PROD'] != '--'){
		echo $myDoc->t_doc['DOC_DATE_PROD'];
	}

	$infos_sup = "";
	// récup matériel MASTER
	foreach($myDoc->t_doc_mat as $idx=>$dmat){
		if($dmat['MAT']->t_mat['MAT_TYPE'] == 'MASTER'){
			$mat_master = $myDoc->t_doc_mat[$idx]['MAT']->t_mat;
		}
	}
	// Si pas trouvé on prends juste le premier t_doc_mat
	if (!isset($mat_master)){
		$mat_master = $myDoc->t_doc_mat[0]['MAT']->t_mat;
	}


	// récupération des infos pertinentes en fonction du type de document
	if($myDoc->t_doc['DOC_ID_MEDIA'] == 'P'
	&& isset($mat_master['MAT_WIDTH']) && !empty($mat_master['MAT_WIDTH'])
	&& isset($mat_master['MAT_HEIGHT']) && !empty($mat_master['MAT_HEIGHT']) )
	{
 		$infos_sup = $mat_master['MAT_WIDTH']." x ".$mat_master['MAT_HEIGHT'];
	}else if($myDoc->t_doc['DOC_ID_MEDIA'] == 'D'  && isset($mat_master['MAT_NB_PAGES']) && !empty($mat_master['MAT_NB_PAGES'])){
 		$infos_sup = $mat_master['MAT_NB_PAGES'].' '.strtolower(kPages);
	}else if(isset($myDoc->t_doc['DOC_DUREE']) && !empty($myDoc->t_doc['DOC_DUREE'])){
		$infos_sup = $myDoc->t_doc['DOC_DUREE'];
	}

	if($infos_sup != '' && isset($myDoc->t_doc['DOC_DATE_PROD']) && !empty($myDoc->t_doc['DOC_DATE_PROD']) && $myDoc->t_doc['DOC_DATE_PROD'] != '--'){
		echo " - ";
	}

	echo $infos_sup;

	echo "</div>";

	if(isset($myDoc->t_doc['DOC_RES']) && !empty($myDoc->t_doc['DOC_RES'])){
		if(strlen($myDoc->t_doc['DOC_RES'])<300){
			echo "<div>".$myDoc->t_doc['DOC_RES']."</div>";
		}else{
			echo "<div>".substr($myDoc->t_doc['DOC_RES'],0,300)."...</div>";
		}
	}
	
	if(empty($vignette) && empty ($id_mat_visu)){
	?>
		<script type="text/javascript">
		$j("#previewHoverInfos").css('visibility','visible');
		</script>
	<?}?>

</div>
	
	<script type="text/javascript">
	console.log(window.location.search.substring(1),window.location.search.substring(1).indexOf('mode=carto')) ;
	if(window.location.search.substring(1).indexOf('mode=chrono') !== -1 ||window.location.search.substring(1).indexOf('mode=carto') !== -1 ){
		$j("span.previewActions a.prev_doc_link").attr('target','_blank');
	}
	
	</script>
<?}?>
