<?php
	require_once("conf/conf.inc.php");
	require_once(libDir."session.php");
	require_once(libDir."fonctionsGeneral.php"); 	

    if(isset($_GET["id_mat"]) && isset($_GET["id_doc"])) {
        require_once(libDir.'class_livraison.php');
        $myLivraison=new Livraison();
        $nomDossierCourt = preg_replace('/^0?(\S+) (\S+)$/X', '$2$1', microtime()); //nom de dossier défini par microtime
        $myLivraison->setDir($nomDossierCourt);
        
        $arrDocs=Array(Array("ID_DOC"=>$_GET["id_doc"],"ID_MAT"=>$_GET["id_mat"]));
        $myLivraison->setArray($arrDocs);
        $myLivraison->doLivraison();

        if(count($myLivraison->arrDocs)>0){
            $tab=$myLivraison->arrDocs[0];
            if(!empty($tab['fileSize'])){
                $file=$tab['fileDir'];
            }
        }
    }
    elseif(isset($_GET["id_mat"])){
        require_once(modelDir.'model_materiel.php');
        $file=Materiel::getLieuByIdMat($_GET["id_mat"]).$_GET["id_mat"];
    }
    elseif(isset($_GET["livraison"])){
		if(isset($_GET['id_doc'])){
			logAction('LIV',array("ID_DOC"=>intval($_GET['id_doc']),"ACT_REQ"=>klivraisonRepertoire.$_GET["livraison"]));
		}
        $file=klivraisonRepertoire.$_GET["livraison"];
	}elseif(isset($_GET["image"])){
		$file=kStoryboardDir.$_GET["image"];
	}
	
    if(is_file($file)){
        include(frameDir."downloadFile.php");
    }else if (is_dir($file)){
		include(frameDir."downloadZip.php");
	}
	
?>
