<?php
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- Developpeurs : Loic Desjardins											  ---*/
/*-----------------------------------------------------------------------------------*/
	
// NOTA : respecter cet ordre !!!
$time_start = microtime(true);
require_once("conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
require_once(libDir."class_page.php");

$_REQUEST["urlaction"] = "visualisation";
$_GET["method"] = "QT";


$myFrame=Page::getInstance();

$myFrame->setDesign(templateDir."video.html");
$myUsr=User::getInstance();
$html=$myFrame->render();  

print($html);
$time_end = microtime(true);
$time=$time_end-$time_start;
if (defined('debugMode') && debugMode==true)
	echo "<div style='text-align:right;font-size:9px;color:#880000'>".$time." sec.</div>";

exit;

?> 

