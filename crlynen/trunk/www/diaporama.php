<?php 
require_once("conf/conf.inc.php");
require_once(libDir."session.php");
require_once(libDir."fonctionsGeneral.php"); 	
require_once(libDir."class_page.php");
?>
<script type="text/javascript" src="<?=libUrl?>webComponents/js/fonctionsJavascript.js"></script>
<script type="text/javascript" src='<?=libUrl?>webComponents/js/prototype.js'></script>
<script type="text/javascript" src='<?=libUrl?>webComponents/js/slider.js'></script>
<script type="text/javascript" src='<?=libUrl?>webComponents/js/effects.js'></script>
<script type="text/javascript" src='<?=libUrl?>webComponents/js/scriptaculous.js'></script>
<script type="text/javascript" src='<?=libUrl?>webComponents/js/autocomplete-3-2.js'></script>
<script type="text/javascript" src='<?=libUrl?>webComponents/js/dropdown.js'></script>
<script type="text/javascript" src="<?=designUrl?>jquery/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=designUrl?>jquery/js/jquery-ui-1.7.2.custom.min.js"></script>

<script type="text/javascript">
var $j=jQuery.noConflict();
</script>
<script type="text/javascript" src='<?=libUrl?>oraoweb/slideshow/OW_slideshow.js'></script>
<link type="text/css" href="<?=libUrl?>oraoweb/slideshow/ow_slideshow.css" rel="stylesheet" />	

<center>
<div class="contenu_gal_simple_document" style="text-align: center; width : 480px;">
	<div id="mon_slide" class="pagination_gal_simple_document" style="text-align : center;">
		<script type="text/javascript">
			playSlideshow('<?=$id_mat_vis;?>','mat');
		</script>
	</div>
</div>
</center>