<?
/*-----------------------------------------------------------------------------------*/
/*---------------------------- OPSOMAI -- OPSIS -------------------------------------*/
/*---                                                                             ---*/
/*--- 2006                                                               		   ---*/
/*--- Developpeurs : Loic Desjardins											  ---*/
/*-----------------------------------------------------------------------------------*/

// Permet d'interdire le mode de compatibilit� sur IE et de le forcer au plus � la version 10
header('x-ua-compatible: ie=10');

// NOTA : respecter cet ordre !!!
	$time_start = microtime(true);
	require_once("conf/conf.inc.php");
	require_once(libDir."session.php");
	require_once(libDir."fonctionsGeneral.php");
	require_once(libDir."class_page.php");
	require_once(modelDir."model_partage.php");
	//require_once(designDir.'include/class_config.php');

    $myUsr=User::getInstance();

	// blocage en fonction du code partage ; 
	if(!isset($_GET['code']) || empty($_GET['code'])){
		exit ; 
	}
	$myPartage = new Partage() ; 
	$myPartage->getPartageFromCode($_GET['code']);
	if(!$myPartage->isValid()){
		echo $myPartage->error_msg;
		exit ; 
	}


    if (isset($_GET["deconnection"])) $myUsr->Logout(false, "index.php?disconnected=1");
    // Cette ligne est pour permettre l'appel d'une recherche depuis la page d'accueil (ex: fonds d'images)
	$addToParams='';


 	setlocale(LC_TIME,strtoupper($_SESSION["langue"]));

    $myPage=Page::getInstance();

	$myUsr->guestPartage = $myPartage->t_partage['PARTAGE_CODE'];
	$myUsr->putInSession();
	$myPage->guestPartageAccess = true;
	// on redéfinit la page de redirection depuis viewer.php vers index.php 
	$myPage->redirect_name = "index.php";
	

	switch($myPartage->t_partage['PARTAGE_TYPE_ENTITE']){
		case "doc":
			$myPage->urlaction = "doc";
			$myPage->id = $myPartage->t_partage['PARTAGE_ID_ENTITE'];
		break ; 
	}
	
	$myPage->setDesign(templateDir."general.html");
	// Pas d'autolog depuis la page viewer
	/*if( !isset($_POST["login"]) && defined('gAutoLogAs')){
		$myPage->autoLog($addToParams);
		$myUsr->getFromSession();
	}*/

	
	
  	$html=$myPage->render();

	print($html);

	$time_end = microtime(true);
	$time=$time_end-$time_start;
	if (defined('debugMode') && debugMode==true) echo "<div style='text-align:right;font-size:9px;color:#880000'>".$time." sec.</div>";
	exit;

?>

