-- -------------------------------------
-- Modif t_categorie : ajout cat_id_fonds
-- -------------------------------------
ALTER TABLE t_categorie ADD COLUMN cat_id_fonds bigint DEFAULT (0)::bigint NOT NULL;
CREATE INDEX t_categorie_cat_id_fonds_idx ON t_categorie USING btree (cat_id_fonds);

-- -------------------------------------
-- Modif t_lexique : ajout lex_id_fonds
-- -------------------------------------
ALTER TABLE t_lexique ADD COLUMN lex_id_fonds bigint DEFAULT (0)::bigint NOT NULL;
CREATE INDEX t_lexique_lex_id_fonds_idx ON t_lexique USING btree (lex_id_fonds);

-- -------------------------------------
-- Modif t_mat : ajout mat_id_fonds
-- -------------------------------------
ALTER TABLE t_mat ADD COLUMN mat_id_fonds bigint DEFAULT (0)::bigint NOT NULL;
CREATE INDEX t_mat_mat_id_fonds_idx ON t_mat USING btree (mat_id_fonds);

-- -------------------------------------
-- Modif t_personne : ajout t_mat
-- -------------------------------------
ALTER TABLE t_personne ADD COLUMN pers_id_fonds bigint DEFAULT (0)::bigint NOT NULL;
CREATE INDEX t_personne_pers_id_fonds_idx ON t_personne USING btree (pers_id_fonds);

-- -------------------------------------
-- Modif t_val : ajout val_id_fonds
-- -------------------------------------
ALTER TABLE t_val ADD COLUMN val_id_fonds bigint DEFAULT (0)::bigint NOT NULL;
CREATE INDEX t_val_val_id_fonds_idx ON t_val USING btree (val_id_fonds);

-- -------------------------------------
-- Modif t_doc_acc : ajout da_id_fonds
-- -------------------------------------
ALTER TABLE t_doc_acc ADD COLUMN da_id_fonds bigint DEFAULT (0)::bigint NOT NULL;
CREATE INDEX t_doc_acc_da_id_fonds_idx ON t_doc_acc USING btree (da_id_fonds);

ALTER TABLE t_doc_acc ADD COLUMN da_taille bigint;
ALTER TABLE t_doc_acc ADD COLUMN da_date_fichier timestamp without time zone;

-- -------------------------------------
-- Modif id_usager en bigint
-- -------------------------------------
ALTER TABLE t_import ALTER COLUMN imp_id_usager TYPE bigint;
ALTER TABLE t_partage ALTER COLUMN partage_id_usager_crea TYPE bigint;
ALTER TABLE t_requete ALTER COLUMN id_usager TYPE bigint;

-- -------------------------------------
-- Modification de la colonne FONDS_COL vers un type texte car on généralise les hierarchie de fonds de profondeur 2+
-- -------------------------------------
ALTER TABLE t_fonds ALTER COLUMN FONDS_COL TYPE text; 


TRUNCATE t_type_mat;
INSERT INTO t_type_mat VALUES ('COPIE');
INSERT INTO t_type_mat VALUES ('MASTER');
INSERT INTO t_type_mat VALUES ('VISIO');
INSERT INTO t_type_mat VALUES ('ORIGINAL');
INSERT INTO t_type_mat VALUES ('EXEMPLAIRE');

TRUNCATE t_type_commande;
INSERT INTO t_type_commande VALUES (1, 'FR', 'Projection');
INSERT INTO t_type_commande VALUES (2, 'FR', 'Téléchargement');
INSERT INTO t_type_commande VALUES (3, 'FR', 'Consultation');
--INSERT INTO t_type_commande VALUES (4, 'FR', 'Image DVD');


INSERT INTO t_type_val VALUES ('ETAT', 'FR', 'Etat exemplaire', null);