delete from t_panier_doc;

update t_doc_lex dl set id_lex=l1.id_lex from t_lexique l1, t_lexique l2 where  l1.lex_terme=l2.lex_terme and l1.id_lex!=l2.id_lex and dl.id_lex=l2.id_lex and l1.lex_id_etat_lex=2 and l2.lex_id_etat_lex=1;
delete from t_lexique where lex_id_etat_lex=1 and id_lex not in (select distinct id_lex from t_doc_lex where id_lex>0);
