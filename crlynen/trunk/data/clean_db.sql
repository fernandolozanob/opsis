--Suppression
delete from t_mat_val where id_mat in (select dm.id_mat from t_doc_mat dm, t_mat m where dm.id_mat=m.id_mat and dm.id_doc in (select id_doc from t_doc));
delete from t_mat_pers where id_mat in (select dm.id_mat from t_doc_mat dm, t_mat m where dm.id_mat=m.id_mat and dm.id_doc in (select id_doc from t_doc));
--delete from t_mat where id_mat in (select dm.id_mat from t_doc_mat dm, t_mat m where dm.id_mat=m.id_mat and dm.id_doc in (select id_doc from t_doc));
delete from t_mat;

delete from t_doc;
delete from t_doc_mat where id_doc not in (select id_doc from t_doc where id_lang='FR');
delete from t_doc_val where id_doc not in (select id_doc from t_doc where id_lang='FR');
delete from t_doc_lex where id_doc not in (select id_doc from t_doc where id_lang='FR');
delete from t_mat_val where id_mat not in (select id_mat from t_mat);


--Nettoyage tables
delete from t_personne where pers_id_etat_lex=1 and id_pers not in (select distinct id_pers from t_doc_lex where id_pers>0) and id_pers not in (select distinct id_pers from t_mat_pers where id_pers>0) ;

delete from t_val where val_id_etat_lex=1 and id_val not in (select distinct id_val from t_doc_val where id_val>0) and  id_val not in (select distinct id_val from t_mat_val where id_val>0);

delete from t_lexique where lex_id_etat_lex=1 and id_lex not in (select distinct id_lex from t_doc_lex where id_lex>0);

update t_val set valeur=trim(valeur);
update t_lexique set lex_terme=trim(lex_terme);
update t_personne set pers_nom=trim(pers_nom),pers_prenom=trim(pers_prenom);
