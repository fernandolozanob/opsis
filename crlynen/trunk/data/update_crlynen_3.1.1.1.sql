-- -------------------------------------
-- ajout de colonnes dans t_doc
-- -------------------------------------
ALTER TABLE t_doc ADD DOC_TEXT_1 text;
ALTER TABLE t_doc ADD DOC_TEXT_2 text;
ALTER TABLE t_doc ADD DOC_TEXT_3 text;
ALTER TABLE t_doc ADD DOC_TEXT_4 text;
ALTER TABLE t_doc ADD DOC_TEXT_5 text;
ALTER TABLE t_doc ADD DOC_TEXT_6 text;
ALTER TABLE t_doc ADD DOC_TEXT_7 text;
ALTER TABLE t_doc ADD DOC_TEXT_8 text;
ALTER TABLE t_doc ADD DOC_TEXT_9 text;


-- -------------------------------------
-- Ajout champs à t_mat
-- -------------------------------------
ALTER TABLE t_mat ADD COLUMN mat_date_1  character varying(20);
ALTER TABLE t_mat ADD COLUMN mat_date_2  character varying(20);
ALTER TABLE t_mat ADD COLUMN mat_date_3  character varying(20);
ALTER TABLE t_mat ADD COLUMN mat_date_4  character varying(20);
ALTER TABLE t_mat ADD COLUMN mat_date_5  character varying(20);

-- -------------------------------------
-- Init t_type_lex
-- -------------------------------------

TRUNCATE t_type_lex;
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('LG', 'FR', 'Lieu',1, 0);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('NC', 'FR', 'Nom commun',1, 0);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('PE', 'FR', 'Personnalité',1, 0);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('PP', 'FR', 'Personne',0, 1);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('AY', 'FR', 'Ayant-droit',0, 1);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('DE', 'FR', 'Déposant',0, 1);

-- -------------------------------------
-- Init t_fonds
-- -------------------------------------
truncate t_fonds;
INSERT INTO t_fonds select ID_TYPE_DOC, 'FR', TYPE_DOC, 0, TYPE_DOC, '','' from t_type_doc where id_type_doc < 10;
SELECT setval('t_fonds_id_fonds_seq', (SELECT max(id_fonds) FROM t_fonds));

-- -------------------------------------
-- Init t_type_commande
-- -------------------------------------
TRUNCATE t_type_commande;
INSERT INTO t_type_commande VALUES (2, 'FR', 'Téléchargement');
INSERT INTO t_type_commande VALUES (3, 'FR', 'Consultation');
INSERT INTO t_type_commande VALUES (5, 'FR', 'Projection');
