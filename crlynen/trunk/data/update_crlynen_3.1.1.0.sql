DELETE FROM t_etape WHERE id_etape IN (1,10,2,5);
INSERT INTO t_etape VALUES (1, 'Encodage MP4 vis', 22, '.', '_vis.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <r>25</r> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>auto</aspect_ratio>  <deinterlace>1</deinterlace> <pix_fmt>yuv420p</pix_fmt> <profile>main</profile> <height>720</height> <b>1500</b> <logo_noresize>1</logo_noresize><logo>logoCrlynen50x50.jpg</logo><logo_position>top-right</logo_position> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>128</ab> <MAT_TYPE>VISIO</MAT_TYPE> </param>');
INSERT INTO t_etape VALUES (10, 'Encodage MP4 proj', 22, '.', '_proj.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <r>25</r> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>auto</aspect_ratio>  <deinterlace>1</deinterlace> <pix_fmt>yuv420p</pix_fmt> <profile>high</profile> <b>10000</b> <logo_noresize>1</logo_noresize><logo>logoCrlynen50x50.jpg</logo><logo_position>top-right</logo_position> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>96</ab> <MAT_TYPE>VISIO</MAT_TYPE> </param>');
INSERT INTO t_etape VALUES (2, 'Storyboard', 1, '.', '', '<param><cutdetection>1</cutdetection><makemosaic>1</makemosaic></param>');
INSERT INTO t_etape VALUES (5, 'Encodage JPEG', 25, '.', '_vis.jpg', '<param><use_embed>1</use_embed><logo_marge>10</logo_marge><logo_noresize>1</logo_noresize><logo>logoCrlynen200x200.jpg</logo><logo_position>top-right</logo_position><MAT_TYPE>VISIO</MAT_TYPE><width>2000</width><height>2000</height><kr>1</kr></param>');


DELETE FROM t_proc_etape WHERE id_proc = 1;
INSERT INTO t_proc_etape VALUES (1, 10, 1 , 0);
INSERT INTO t_proc_etape VALUES (1, 1, 2, 0);
INSERT INTO t_proc_etape VALUES (1, 2, 3 , 10);
