
DELETE FROM t_type_lex;
INSERT INTO t_type_lex VALUES ('LG', 'FR', 'Lieu', 1, 0);
INSERT INTO t_type_lex VALUES ('NC', 'FR', 'Nom commun', 1, 0);
INSERT INTO t_type_lex VALUES ('PE', 'FR', 'Personnalité', 1, 0);
INSERT INTO t_type_lex VALUES ('PP', 'FR', 'Personne', 0, 1);
INSERT INTO t_type_lex VALUES ('AY', 'FR', 'Ayant-droit', 0, 1);
INSERT INTO t_type_lex VALUES ('DE', 'FR', 'Déposant', 0, 1);
INSERT INTO t_type_lex VALUES ('PM', 'FR', 'Entité morale', 0, 1);


