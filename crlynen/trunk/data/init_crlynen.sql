TRUNCATE t_action;
TRUNCATE t_categorie;
TRUNCATE t_couleur;
TRUNCATE t_doc;
TRUNCATE t_doc_acc;
TRUNCATE t_doc_acc_val;
TRUNCATE t_doc_cat;
TRUNCATE t_doc_comment;
TRUNCATE t_doc_comment_val;
TRUNCATE t_doc_fest;
TRUNCATE t_doc_fest_val;
TRUNCATE t_doc_lex;
TRUNCATE t_doc_lex_droit;
TRUNCATE t_doc_lex_precision;
TRUNCATE t_doc_lien;
TRUNCATE t_doc_mat;
TRUNCATE t_doc_mat_val;
TRUNCATE t_doc_prex;
TRUNCATE t_doc_publication;
TRUNCATE t_doc_redif;
TRUNCATE t_doc_val;
TRUNCATE t_exp_doc;
TRUNCATE t_exp_vente;
TRUNCATE t_exploitation;
TRUNCATE t_fest_val;
TRUNCATE t_festival;
TRUNCATE t_fonds;
TRUNCATE t_html;
TRUNCATE t_image;
TRUNCATE t_imageur;
TRUNCATE t_import;
TRUNCATE t_job;
TRUNCATE t_lexique;
TRUNCATE t_lexique_asso;
TRUNCATE t_lieu;
TRUNCATE t_mat;
TRUNCATE t_mat_track;
TRUNCATE t_mat_val;
TRUNCATE t_mot;
TRUNCATE t_panier;
TRUNCATE t_panier_doc;
TRUNCATE t_partage;
TRUNCATE t_personne;
TRUNCATE t_pers_lex;
TRUNCATE t_pers_val;
TRUNCATE t_requete;
TRUNCATE t_session_ado;
TRUNCATE t_session2_ado;
TRUNCATE t_tape;
TRUNCATE t_tape_file;
TRUNCATE t_tape_set;
TRUNCATE t_tarifs;
TRUNCATE t_trash;
TRUNCATE t_val;

TRUNCATE t_categorie;
INSERT INTO t_categorie (id_cat, id_lang, cat_id_gen, cat_id_type_cat, cat_nom) VALUES (1, 'FR', 0, 'UNE', 'Carrousel');
INSERT INTO t_categorie (id_cat, id_lang, cat_id_gen, cat_id_type_cat, cat_nom) VALUES (1, 'EN', 0, 'UNE', 'Carrousel');

TRUNCATE t_config;
INSERT INTO t_config VALUES ('ID_GROUPE', '2');

TRUNCATE t_etape;
-- ENCODAGES IMPORTS
INSERT INTO t_etape VALUES (1, 'Encodage MP4 vis', 22, '.', '_vis.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <r>25</r> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>auto</aspect_ratio>  <deinterlace>1</deinterlace> <pix_fmt>yuv420p</pix_fmt> <profile>main</profile> <height>720</height> <b>1500</b> <logo_noresize>1</logo_noresize><logo>logoCrlynen50x50.jpg</logo><logo_position>top-right</logo_position> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>128</ab> <MAT_TYPE>VISIO</MAT_TYPE> </param>');
-- <logo>logoCrlynen.jpg</logo>
INSERT INTO t_etape VALUES (2, 'Storyboard', 1, '.', '', '<param><cutdetection>1</cutdetection><makemosaic>1</makemosaic></param>');
INSERT INTO t_etape VALUES (3, 'Extraction PDF', 15, '.', '', '<param><hauteur>2500</hauteur><largeur>0</largeur><logo_marge>10</logo_marge><logo_position>center</logo_position></param>');
-- <logo>logoCrlynen.jpg</logo>
INSERT INTO t_etape VALUES (4, 'Encodage PDF', 8, '.', '.pdf', '<param><format>pdf</format><MAT_TYPE>VISIO</MAT_TYPE></param>');
INSERT INTO t_etape VALUES (5, 'Encodage JPEG', 25, '.', '_vis.jpg', '<param><use_embed>1</use_embed><logo_marge>10</logo_marge><logo_noresize>1</logo_noresize><logo>logoCrlynen.jpg</logo><logo_position>top-right</logo_position><MAT_TYPE>VISIO</MAT_TYPE><width>2000</width><height>2000</height><kr>1</kr></param>');
-- <logo>logoCrlynen.jpg</logo>
INSERT INTO t_etape VALUES (6, 'Vignette JPEG', 26, '_vis/.', '', '<param><use_embed>1</use_embed><largeur>600</largeur></param>');
INSERT INTO t_etape VALUES (7, 'OCR', 23, '.', '_ocr.xml', '<param><langue>fra</langue></param>');
INSERT INTO t_etape VALUES (8, 'Encodage MP3', 3, '.', '_vis.mp3', '<param>    <acodec>libmp3lame</acodec>    <ac>2</ac>    <ab>128</ab>    <ar>44100</ar>    <MAT_TYPE>VISIO</MAT_TYPE></param>');
INSERT INTO t_etape VALUES (9, 'Encodage vis DCP', 3,'.', '_vis.mp4', '<param><movflags>faststart</movflags><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>auto</aspect_ratio>  <deinterlace>1</deinterlace> <pix_fmt>yuv420p</pix_fmt> <profile>main</profile> <width>640</width> <b>700</b> <logo_position>center</logo_position> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>128</ab> <MAT_TYPE>PROJ</MAT_TYPE> </param>');
-- <logo>logoCrlynen.jpg</logo>

INSERT INTO t_etape VALUES (10, 'Encodage MP4 proj', 22, '.', '_proj.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <r>25</r> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>auto</aspect_ratio>  <deinterlace>1</deinterlace> <pix_fmt>yuv420p</pix_fmt> <profile>high</profile> <b>10000</b> <logo_noresize>1</logo_noresize><logo>logoCrlynen50x50.jpg</logo><logo_position>top-right</logo_position> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>96</ab> <MAT_TYPE>VISIO</MAT_TYPE> </param>');


-- ENCODAGES / LIVRAISONS
-- Encodage pivot montage
insert into t_etape (id_etape,etape_nom,etape_id_module,etape_in,etape_out,etape_param) values (90,'Encodage - Pivot Montage - ProResHQ',3,'.','.mov','<param><flag_etape>montage_pivot</flag_etape><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>3</profile> <aspect_mode>letterbox</aspect_mode> <pix_fmt>yuv422p10le</pix_fmt> <height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');

INSERT INTO t_etape VALUES (100, 'Cut', 20, '.', '%s', '<param><use_link>1</use_link></param>');
INSERT INTO t_etape VALUES (101, 'Encodage HD - MOV - DNXHD120', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>dnxhd</vcodec> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>1920</width><height>1080</height> <b>120000</b> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff></param>');
INSERT INTO t_etape VALUES (102, 'Encodage HD - MOV - DNXHD185x', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder><vcodec>dnxhd</vcodec> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>1920</width><height>1080</height> <b>185000</b> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');
INSERT INTO t_etape VALUES (103, 'Encodage HD - MOV - HDV', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>mpeg2video</vcodec> <vtag>hdv3</vtag> <g>12</g> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1440</width><height>1080</height> <minrate>25000k</minrate> <maxrate>25000k</maxrate> <b>25000</b> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (104, 'Encodage HD - MOV - ProRes', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>std</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');
INSERT INTO t_etape VALUES (105, 'Encodage HD - MOV - ProResHQ', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>hq</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');
INSERT INTO t_etape VALUES (106, 'Encodage HD - MOV - XDCAM EX', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>mpeg2video</vcodec> <vtag>xdvc</vtag> <b>35000</b> <maxrate>35000k</maxrate> <bufsize>25485834</bufsize> <flags>+ildct</flags> <flags2>+ivlc+non_linear_q</flags2> <bf>2</bf> <qmin>1</qmin> <lmin>''1*QP2LAMBDA''</lmin> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');
INSERT INTO t_etape VALUES (107, 'Encodage HD - MOV - XDCAM HD', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>mpeg2video</vcodec> <vtag>xdv3</vtag> <b>35000</b> <maxrate>35000k</maxrate> <bufsize>25485834</bufsize> <flags>+ildct</flags> <flags2>+ivlc+non_linear_q</flags2> <bf>2</bf> <qmin>1</qmin> <lmin>''1*QP2LAMBDA''</lmin> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1440</width><height>1080</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (108, 'Encodage HD - MOV - XDCAM HD422', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>mpeg2video</vcodec> <target>xdcamhd422</target> <vtag>xd5c</vtag> <b>50000</b> <minrate>50000k</minrate> <maxrate>50000k</maxrate> <bufsize>25000k</bufsize> <g>12</g> <bf>2</bf> <sc_threshold>40</sc_threshold> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');
INSERT INTO t_etape VALUES (109, 'Encodage HD - MXF - XDCAM HD422', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>mono_multiples</type_mapping> <f>mxf</f><fieldorder>tff</fieldorder> <vcodec>mpeg2video</vcodec> <target>xdcamhd422</target> <vtag>xd5c</vtag> <b>50000</b> <minrate>50000k</minrate> <maxrate>50000k</maxrate> <bufsize>25000k</bufsize> <g>12</g> <bf>2</bf> <sc_threshold>40</sc_threshold> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>1</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> <newaudio>1</newaudio></param>');
INSERT INTO t_etape VALUES (110, 'Encodage HD - MXF - H264 1080i High422Intra - 100Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mxf</f> <fieldorder>tff</fieldorder> <vcodec>libx264</vcodec> <intra>1</intra> <refs>1</refs> <coder>1</coder> <level>4.1</level> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff></param>');
INSERT INTO t_etape VALUES (111, 'Encodage HD - MXF - H264 1080p High422Intra - 100Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mxf</f> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <intra>1</intra> <refs>1</refs> <coder>1</coder> <crf>10</crf> <level>4.1</level> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r></param>');
INSERT INTO t_etape VALUES (112, 'Encodage HD - MP4 - H264 1080p - 15Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>high</profile> <b>15000</b> <maxrate>17000k</maxrate> <bufsize>150000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1920</width><height>1080</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar> <ab>256k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (113, 'Encodage HD - MP4 - H264 1080p - 10Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>high</profile> <b>10000</b> <maxrate>12000k</maxrate> <bufsize>100000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1920</width><height>1080</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar> <ab>256k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (114, 'Encodage HD - MP4 - H264 1080p - 2Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>high</profile> <b>2000</b> <maxrate>2500k</maxrate> <bufsize>20000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1920</width><height>1080</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>128k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (115, 'Encodage SD - 16/9 - MOV - DV', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <target>dvcam</target> <fieldorder>bff</fieldorder> <vcodec>dvvideo</vcodec> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar> <r>25</r><bff>1</bff><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (116, 'Encodage SD - 4/3 - MOV - DV', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <target>dvcam</target> <fieldorder>bff</fieldorder> <vcodec>dvvideo</vcodec> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar> <r>25</r><bff>1</bff><anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (117, 'Encodage SD - 16/9 - MXF - IMX50', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>1_piste_4_canaux</type_mapping> <f>mxf</f> <target>imx50</target> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <r>25</r> <tff>1</tff></param>');
INSERT INTO t_etape VALUES (118, 'Encodage SD - 4/3 - MXF - IMX50', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>1_piste_4_canaux</type_mapping> <f>mxf</f> <target>imx50</target> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <r>25</r> <tff>1</tff></param>');
INSERT INTO t_etape VALUES (119, 'Encodage SD - 16/9 - MOV - IMX50', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>1_piste_4_canaux</type_mapping> <f>mov</f> <target>imx50</target> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <r>25</r> <tff>1</tff></param>');
INSERT INTO t_etape VALUES (120, 'Encodage SD - 4/3 - MOV - IMX50', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>1_piste_4_canaux</type_mapping> <f>mov</f> <target>imx50</target> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <r>25</r> <tff>1</tff></param>');
INSERT INTO t_etape VALUES (121, 'Encodage SD - 16/9 - MXF - MPEG2 - 15Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>mono_multiples</type_mapping> <f>mxf</f> <vcodec>mpeg2video</vcodec> <b>15000</b> <minrate>15000k</minrate> <maxrate>15000k</maxrate> <bufsize>10000k</bufsize> <g>12</g> <bf>2</bf> <sc_threshold>40</sc_threshold> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ar>48000</ar> <ac>1</ac> <r>25</r> <tff>1</tff><newaudio>1</newaudio><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (122, 'Encodage SD - 4/3 - MXF - MPEG2 - 15Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>mono_multiples</type_mapping> <f>mxf</f> <vcodec>mpeg2video</vcodec> <b>15000</b> <minrate>15000k</minrate> <maxrate>15000k</maxrate> <bufsize>10000k</bufsize> <g>12</g> <bf>2</bf> <sc_threshold>40</sc_threshold> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ar>48000</ar> <ac>1</ac> <r>25</r> <tff>1</tff><newaudio>1</newaudio><anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (123, 'Encodage SD - 16/9 - MXF - MPEG2 Intra - 50Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>mono_multiples</type_mapping> <f>mxf</f> <vcodec>mpeg2video</vcodec> <b>50000</b> <minrate>50000k</minrate> <maxrate>50000k</maxrate> <bufsize>25000k</bufsize> <intra>1</intra> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ar>48000</ar> <ac>1</ac> <r>25</r> <tff>1</tff><newaudio>1</newaudio><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (124, 'Encodage SD - 4/3 - MXF - MPEG2 Intra - 50Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>mono_multiples</type_mapping> <f>mxf</f> <vcodec>mpeg2video</vcodec> <b>50000</b> <minrate>50000k</minrate> <maxrate>50000k</maxrate> <bufsize>25000k</bufsize> <intra>1</intra> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ar>48000</ar> <ac>1</ac> <r>25</r> <tff>1</tff><newaudio>1</newaudio><anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (125, 'Encodage SD - 16/9 - MXF - H264 High422Intra - 25Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mxf</f> <vcodec>libx264</vcodec> <intra>1</intra> <refs>1</refs> <coder>1</coder> <crf>10</crf> <level>3.1</level> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <r>25</r> <tff>1</tff><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (126, 'Encodage SD - 4/3 - MXF - H264 High422Intra - 25Mb', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mxf</f> <vcodec>libx264</vcodec> <intra>1</intra> <refs>1</refs> <coder>1</coder> <crf>10</crf> <level>3.1</level> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width> <height>576</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <r>25</r> <tff>1</tff><anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (127, 'Encodage SD - 16/9 - MP4 - H264 - 3Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <profile>main</profile> <b>3000</b> <maxrate>3500k</maxrate> <bufsize>30000k</bufsize> <deinterlace>1</deinterlace> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1024</width> <height>576</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ar>44100</ar> <ac>2</ac> <ab>256k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (128, 'Encodage SD - 4/3 - MP4 - H264 - 3Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <profile>main</profile> <b>3000</b> <maxrate>3500k</maxrate> <bufsize>30000k</bufsize> <deinterlace>1</deinterlace> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>768</width> <height>576</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ar>44100</ar> <ac>2</ac> <ab>256k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (129, 'Encodage SD - 16/9 - MP4 - H264 - 1Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <profile>main</profile> <b>1000</b> <maxrate>1200k</maxrate> <bufsize>10000k</bufsize> <deinterlace>1</deinterlace> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1024</width> <height>576</height>  <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ar>44100</ar> <ac>2</ac> <ab>128k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (130, 'Encodage SD - 4/3 - MP4 - H264 - 1Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <vcodec>libx264</vcodec> <profile>main</profile> <b>1000</b> <maxrate>1200k</maxrate> <bufsize>10000k</bufsize> <deinterlace>1</deinterlace> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>768</width> <height>576</height>  <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ar>44100</ar> <ac>2</ac> <ab>128k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (131, 'Encodage HD - MOV - ProResLT', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>lt</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>1920</width><height>1080</height> <acodec>pcm_s16le</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> </param>');
INSERT INTO t_etape VALUES (132, 'Encodage HD - MP4 - H264 1080p - 2Mb - TCI', 3, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <movflags>faststart</movflags> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>main</profile> <vb>2000</vb> <maxrate>2500k</maxrate> <bufsize>20000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1920</width><height>1080</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>128</ab> <r>25</r><incrust_tc>1</incrust_tc></param>');
INSERT INTO t_etape VALUES (133, 'Encodage HD - MP4 - H264 720p - 1,5Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <faststart>auto</faststart> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>main</profile> <b>1500</b> <maxrate>2000k</maxrate> <bufsize>15000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1280</width><height>720</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar> <ab>256k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (134, 'Encodage SD - 16/9 - MOV - ProRes LT', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>lt</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width><height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> <anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (135, 'Encodage SD - 4/3 - MOV - ProRes LT', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>lt</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width><height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> <anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (136, 'Encodage SD - 16/9 - MOV - ProRes', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>std</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width><height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> <anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (137, 'Encodage SD - 4/3 - MOV - ProRes', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>std</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width><height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> <anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (138, 'Encodage SD - 16/9 - MOV - ProRes HQ', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>hq</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width><height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> <anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (139, 'Encodage SD - 4/3 - MOV - ProRes HQ', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping> <f>mov</f> <fieldorder>tff</fieldorder> <vcodec>prores</vcodec> <profile>hq</profile> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p10le</pix_fmt> <width>720</width><height>576</height> <acodec>pcm_s24le</acodec> <sample_fmt>s32</sample_fmt> <ac>2</ac> <ar>48000</ar>  <r>25</r> <tff>1</tff> <anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (140, 'Encodage SD - 16/9 - MPEG - MPEG2 - 15Mb', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>vob</f> <vcodec>mpeg2video</vcodec> <b>15000</b> <minrate>15000k</minrate> <maxrate>15000k</maxrate> <bufsize>10000k</bufsize> <g>12</g> <bf>2</bf> <sc_threshold>40</sc_threshold> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height> <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac><ab>256k</ab> <r>25</r> <tff>1</tff><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (141, 'Encodage SD - 4/3 - MPEG - MPEG2 - 15Mb', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>vob</f> <vcodec>mpeg2video</vcodec> <b>15000</b> <minrate>15000k</minrate> <maxrate>15000k</maxrate> <bufsize>10000k</bufsize> <g>12</g> <bf>2</bf> <sc_threshold>40</sc_threshold> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height> <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <ab>256k</ab> <r>25</r> <tff>1</tff><anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (142, 'Encodage SD - 16/9 - MPEG - MPEG2 Intra - 50Mb', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>vob</f> <vcodec>mpeg2video</vcodec> <b>50000</b> <minrate>50000k</minrate> <maxrate>50000k</maxrate> <bufsize>25000k</bufsize> <intra>1</intra> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac><ab>384k</ab> <r>25</r> <tff>1</tff><anamorphose>16:9</anamorphose></param>');
INSERT INTO t_etape VALUES (143, 'Encodage SD - 4/3 - MPEG - MPEG2 Intra - 50Mb', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>vob</f> <vcodec>mpeg2video</vcodec> <b>50000</b> <minrate>50000k</minrate> <maxrate>50000k</maxrate> <bufsize>25000k</bufsize> <intra>1</intra> <fieldorder>tff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv422p</pix_fmt> <width>720</width> <height>576</height> <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <ab>384k</ab> <r>25</r> <tff>1</tff><anamorphose>4:3</anamorphose></param>');
INSERT INTO t_etape VALUES (144, 'Encodage SD - 16/9 - MP4 - H264 - 1Mb - TCI', 3, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <movflags>faststart</movflags> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>main</profile> <vb>1000</vb> <maxrate>1200k</maxrate> <bufsize>10000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width><height>576</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>128</ab> <r>25</r><anamorphose>16:9</anamorphose><incrust_tc>1</incrust_tc></param>');
INSERT INTO t_etape VALUES (145, 'Encodage SD - 4/3 - MP4 - H264 - 1Mb - TCI', 3, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <movflags>faststart</movflags> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>main</profile> <vb>1000</vb> <maxrate>1200k</maxrate> <bufsize>10000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width><height>576</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>44100</ar> <ab>128</ab> <r>25</r><anamorphose>4:3</anamorphose><incrust_tc>1</incrust_tc></param>');

-- > SD - 16/9 - DVD
-- > SD - 4/3 - DVD
--INSERT INTO t_etape VALUES (146, 'Encodage SD - 16/9 - DVD', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <target>dvd</target> <vcodec>mpeg2video</vcodec> <g>12</g> <bf>2</bf>  <sc_threshold>40</sc_threshold> <bff>1</bff> <fieldorder>bff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height>  <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <ab>256k</ab> <r>25</r><anamorphose>16:9</anamorphose></param>');
--INSERT INTO t_etape VALUES (147, 'Encodage SD - 4/3 - DVD', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <target>dvd</target> <vcodec>mpeg2video</vcodec> <g>12</g> <bf>2</bf>  <sc_threshold>40</sc_threshold> <bff>1</bff> <fieldorder>bff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height>  <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <ab>256k</ab> <r>25</r><anamorphose>4:3</anamorphose></param>');

-- > SD - 16/9 - DVD LP
-- > SD - 4/3 - DVD LP
--INSERT INTO t_etape VALUES (148, 'Encodage SD - 16/9 - DVD LP', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <target>dvd</target> <vcodec>mpeg2video</vcodec> <b>5000</b> <minrate>5000k</minrate> <maxrate>5000k</maxrate> <bufsize>2500k</bufsize><g>12</g> <bf>2</bf>  <sc_threshold>40</sc_threshold> <bff>1</bff> <fieldorder>bff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height>  <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <ab>256k</ab> <r>25</r><anamorphose>16:9</anamorphose></param>');
--INSERT INTO t_etape VALUES (149, 'Encodage SD - 4/3 - DVD LP', 22, '.', '.mpg', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <target>dvd</target> <vcodec>mpeg2video</vcodec> <b>5000</b> <minrate>5000k</minrate> <maxrate>5000k</maxrate> <bufsize>2500k</bufsize><g>12</g> <bf>2</bf>  <sc_threshold>40</sc_threshold> <bff>1</bff> <fieldorder>bff</fieldorder> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>4:3</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>720</width> <height>576</height>  <acodec>mp2</acodec> <sample_fmt>s16</sample_fmt> <ar>48000</ar> <ac>2</ac> <ab>256k</ab> <r>25</r><anamorphose>4:3</anamorphose></param>');


INSERT INTO t_etape VALUES (150, 'HD - MOV - AVC-Intra 100', 22, '.', '.mov', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_multiples</type_mapping><f>mov</f><fieldorder>tff</fieldorder><target>avcintra100</target><vcodec>libx264</vcodec><aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio><pix_fmt>yuv422p10le</pix_fmt><width>1920</width><height>1080</height><acodec>pcm_s16le</acodec><sample_fmt>s16</sample_fmt><ac>2</ac><ar>48000</ar><r>25</r><tff>1</tff></param>');
INSERT INTO t_etape VALUES (151, 'HD - MXF - AVC-Intra 100', 22, '.', '.mxf', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>mono_multiples</type_mapping><f>mxf</f><fieldorder>tff</fieldorder><target>avcintra100</target><vcodec>libx264</vcodec><aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio><pix_fmt>yuv422p10le</pix_fmt><width>1920</width><height>1080</height><acodec>pcm_s16le</acodec><sample_fmt>s16</sample_fmt><ac>1</ac><ar>48000</ar><r>25</r><tff>1</tff> <newaudio>1</newaudio></param>');
INSERT INTO t_etape VALUES (152, 'Encodage HD - MP4 - H264 720p - 7Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>high</profile> <b>7000</b> <maxrate>9000k</maxrate> <bufsize>70000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1280</width><height>720</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar> <ab>256k</ab> <r>25</r></param>');
INSERT INTO t_etape VALUES (153, 'Encodage HD - MP4 - H264 720p - 12Mb', 22, '.', '.mp4', '<param><threads>3</threads><map_audio_channel>auto</map_audio_channel><type_mapping>stereo_unique</type_mapping> <f>mp4</f> <deinterlace>1</deinterlace> <vcodec>libx264</vcodec> <profile>high</profile> <b>12000</b> <maxrate>15000k</maxrate> <bufsize>120000k</bufsize> <aspect_mode>letterbox</aspect_mode> <aspect_ratio>16:9</aspect_ratio> <pix_fmt>yuv420p</pix_fmt> <width>1280</width><height>720</height> <acodec>libfaac</acodec> <sample_fmt>s16</sample_fmt> <ac>2</ac> <ar>48000</ar> <ab>256k</ab> <r>25</r></param>');

INSERT INTO t_etape VALUES (160, 'Cut', 20, '.', '%s', '<param><use_link>1</use_link></param>');

-- Etapes DVD (logo et fonds à changer)
INSERT INTO t_etape VALUES (190, 'DVD - SD - 16/9 - PAL', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>PAL</standard> <aspect_ratio>16:9</aspect_ratio> </param>');
INSERT INTO t_etape VALUES (191, 'DVD - SD - 4/3 - PAL', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>PAL</standard> <aspect_ratio>4:3</aspect_ratio> </param>');
INSERT INTO t_etape VALUES (192, 'DVD - SD - 16/9 - NTSC', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>NTSC</standard> <aspect_ratio>16:9</aspect_ratio> </param>');
INSERT INTO t_etape VALUES (193, 'DVD - SD - 4/3 - NTSC', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>NTSC</standard> <aspect_ratio>4:3</aspect_ratio> </param>');
INSERT INTO t_etape VALUES (194, 'DVD - SD - 16/9 - PAL - loop', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>PAL</standard> <aspect_ratio>16:9</aspect_ratio> <loop>1</loop> </param>');
INSERT INTO t_etape VALUES (195, 'DVD - SD - 4/3 - PAL - loop', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>PAL</standard> <aspect_ratio>4:3</aspect_ratio> <loop>1</loop> </param>');
INSERT INTO t_etape VALUES (196, 'DVD - SD - 16/9 - NTSC - loop', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>NTSC</standard> <aspect_ratio>16:9</aspect_ratio> <loop>1</loop> </param>');
INSERT INTO t_etape VALUES (197, 'DVD - SD - 4/3 - NTSC - loop', 17, '', 'image.iso', '<param><threads>3</threads> <logo>logoCrlynen.jpg</logo> <fond>fond_noir_dvd.png</fond> <couleur_texte>255,255,255</couleur_texte> <logo_margin_left>75</logo_margin_left> <logo_margin_top>50</logo_margin_top> <marge_vert>50</marge_vert> <marge_hrz>75</marge_hrz> <font>Vera.ttf</font> <standard>NTSC</standard> <aspect_ratio>4:3</aspect_ratio> <loop>1</loop> </param>');

-- ENCODAGES IMAGES LIV
-- redondance avec livraison fichier d'origine, commenté pour l'instant, à supprimer si aucune utilité
-- INSERT INTO t_etape VALUES (200, 'Encodage THD - format d''origine', 25, '.', '.jpg', '<param><logo_marge>10</logo_marge><logo_position>center</logo_position></param>');
INSERT INTO t_etape VALUES (201, 'Encodage HD - JPEG', 25, '.', '.jpg', '<param><logo_marge>10</logo_marge><logo_position>center</logo_position><height>2000</height><width>2000</width><kr>1</kr></param>');
INSERT INTO t_etape VALUES (202, 'Encodage MD - JPEG', 25, '.', '.jpg', '<param><logo_marge>10</logo_marge><logo_position>center</logo_position><height>1000</height><width>1000</width><kr>1</kr></param>');
INSERT INTO t_etape VALUES (203, 'Encodage SD - JPEG', 25, '.', '.jpg', '<param><logo_marge>10</logo_marge><logo_position>center</logo_position><height>480</height><width>480</width><kr>1</kr></param>');





TRUNCATE t_etat_arch;
INSERT INTO t_etat_arch VALUES (1, 'FR', 'en ligne');
INSERT INTO t_etat_arch VALUES (1, 'EN', 'online');
INSERT INTO t_etat_arch VALUES (2, 'FR', 'en ligne (sauvegardé)');
INSERT INTO t_etat_arch VALUES (2, 'EN', 'online (saved)');
INSERT INTO t_etat_arch VALUES (3, 'FR', 'archivé (en librairie)');
INSERT INTO t_etat_arch VALUES (3, 'EN', 'archived (in library)');
INSERT INTO t_etat_arch VALUES (4, 'FR', 'archivé (externalisé)');
INSERT INTO t_etat_arch VALUES (4, 'EN', 'archived (externalized)');
INSERT INTO t_etat_arch VALUES (5, 'FR', 'manquant');
INSERT INTO t_etat_arch VALUES (5, 'EN', 'missing');

TRUNCATE t_etat_doc;
INSERT INTO t_etat_doc VALUES (1, 'FR', 'Media à valider');
INSERT INTO t_etat_doc VALUES (2, 'FR', 'À indexer');
INSERT INTO t_etat_doc VALUES (3, 'FR', 'Indexation à valider');
INSERT INTO t_etat_doc VALUES (4, 'FR', 'Validé');
INSERT INTO t_etat_doc VALUES (1, 'EN', 'Media to validate');
INSERT INTO t_etat_doc VALUES (2, 'EN', 'To be indexed');
INSERT INTO t_etat_doc VALUES (3, 'EN', 'Indexing to validate');
INSERT INTO t_etat_doc VALUES (4, 'EN', 'Validated');

TRUNCATE t_etat_job;
INSERT INTO t_etat_job VALUES (0, 'FR', 'Init');
INSERT INTO t_etat_job VALUES (1, 'FR', 'En attente');
INSERT INTO t_etat_job VALUES (2, 'FR', 'En cours');
INSERT INTO t_etat_job VALUES (3, 'FR', 'Demande d''annulation');
INSERT INTO t_etat_job VALUES (4, 'FR', 'En cours d''annulation');
INSERT INTO t_etat_job VALUES (5, 'FR', 'Demande de suppression');
INSERT INTO t_etat_job VALUES (6, 'FR', 'En cours de suppression');
INSERT INTO t_etat_job VALUES (7, 'FR', 'Arrêté');
INSERT INTO t_etat_job VALUES (8, 'FR', 'Erreur');
INSERT INTO t_etat_job VALUES (9, 'FR', 'Fini');
INSERT INTO t_etat_job VALUES (0, 'EN', 'Init');
INSERT INTO t_etat_job VALUES (1, 'EN', 'Waiting');
INSERT INTO t_etat_job VALUES (2, 'EN', 'Runing');
INSERT INTO t_etat_job VALUES (3, 'EN', 'Waiting for cancel');
INSERT INTO t_etat_job VALUES (4, 'EN', 'Cancelling');
INSERT INTO t_etat_job VALUES (5, 'EN', 'Waiting for delete');
INSERT INTO t_etat_job VALUES (6, 'EN', 'Deleting');
INSERT INTO t_etat_job VALUES (7, 'EN', 'Canceled');
INSERT INTO t_etat_job VALUES (8, 'EN', 'Error');
INSERT INTO t_etat_job VALUES (9, 'EN', 'Finished');

TRUNCATE t_etat_lex;
INSERT INTO t_etat_lex VALUES (1, 'FR', 'candidat');
INSERT INTO t_etat_lex VALUES (2, 'FR', 'valide');
INSERT INTO t_etat_lex VALUES (1, 'EN', 'candidate');
INSERT INTO t_etat_lex VALUES (2, 'EN', 'valid');

TRUNCATE t_etat_pan;
INSERT INTO t_etat_pan VALUES (1, 'FR', 'Courant');
INSERT INTO t_etat_pan VALUES (1, 'EN', 'Current');
INSERT INTO t_etat_pan VALUES (2, 'FR', 'En attente de validation');
INSERT INTO t_etat_pan VALUES (2, 'EN', 'Waiting for validation');
INSERT INTO t_etat_pan VALUES (3, 'FR', 'En cours de traitement');
INSERT INTO t_etat_pan VALUES (3, 'EN', 'Currently processing');
INSERT INTO t_etat_pan VALUES (4, 'FR', 'Traité');
INSERT INTO t_etat_pan VALUES (4, 'EN', 'Processed');

TRUNCATE t_etat_usager;
INSERT into t_etat_usager (ID_ETAT_USAGER, ID_LANG, ETAT_USAGER, ETAT_USAGER_CODE)  VALUES
(0,'FR','inactif','INACTIF'),
(1,'FR','email validé','MAIL_VALID'),
(2,'FR','actif','ACTIF'),
(3,'FR','temporaire','TEMP'),
(0,'EN','inactive','INACTIF'),
(1,'EN','valid email','MAIL_VALID'),
(2,'EN','active','ACTIF'),
(3,'EN','temporary','TEMP');

TRUNCATE t_fonction_usager;

TRUNCATE t_fonds;
INSERT INTO t_fonds VALUES (1, 'FR', 'Cinémathèque Robert Lynen', 0, 'Cinémathèque Robert Lynen', '','');
INSERT INTO t_fonds VALUES (1, 'EN', 'Cinémathèque Robert Lynen', 0, 'Cinémathèque Robert Lynen', '','');

truncate t_format_mat;
insert into t_format_mat (format_mat, format_online, format_id_media, format_file_ext) values
('SD - 16:9 - MP4 - H264', '1', 'V', 'mp4'),
('SD - 4:3 - MP4 - H264', '1', 'V', 'mp4'),
('HD - MP4 - H264', '1', 'V', 'mp4'),
('HLS', '1', 'V', '');

TRUNCATE t_frais;
INSERT INTO t_frais VALUES (1, 9999, '00:00:00', 99999, '', '', 1.5, 1);

TRUNCATE t_groupe;
INSERT INTO t_groupe VALUES (1, 'Admin');
INSERT INTO t_groupe VALUES (2, 'Public');

TRUNCATE t_groupe_fonds;
INSERT INTO t_groupe_fonds VALUES (1, 1, 5);
INSERT INTO t_groupe_fonds VALUES (2, 1, 1);

TRUNCATE t_lang;
INSERT INTO t_lang VALUES ('FR', 'français');

TRUNCATE t_media;
INSERT INTO t_media VALUES ('A', 'audio');
INSERT INTO t_media VALUES ('P', 'photo');
INSERT INTO t_media VALUES ('V', 'video');
INSERT INTO t_media VALUES ('R', 'reportage');

TRUNCATE t_module ;
INSERT INTO t_module VALUES (1, 'story', 'STORY');
INSERT INTO t_module VALUES (3, 'ffmpeg', 'ENCOD');
INSERT INTO t_module VALUES (4, 'ftp', 'FTP');
--INSERT INTO t_module VALUES (12, 'youtube', 'DIFF');
INSERT INTO t_module VALUES (13, 'dailymotion', 'DIFF');
INSERT INTO t_module VALUES (14, 'facebook', 'DIFF');
INSERT INTO t_module VALUES (15, 'storypdf', 'STORY');
INSERT INTO t_module VALUES (17, 'dvd', 'ENCOD');
INSERT INTO t_module VALUES (18, 'backup', 'BACK');
INSERT INTO t_module VALUES (20, 'cut', 'ENCOD');
INSERT INTO t_module VALUES (21, 'webservice', 'WEBSV');
INSERT INTO t_module VALUES (22, 'ffmbc', 'ENCOD');
INSERT INTO t_module VALUES (23, 'ocr', 'TEXTE');
INSERT INTO t_module VALUES (25, 'imagick', 'ENCOD');
INSERT INTO t_module VALUES (26, 'storyimg', 'STORY');
INSERT INTO t_module VALUES (30, 'zip', 'ZIP');
INSERT INTO t_module VALUES (8, 'unoconv', 'ENCOD');
INSERT INTO t_module VALUES (6, 'validation', 'VALID');
--INSERT INTO t_module VALUES (32, 'authot', 'PAROL');

TRUNCATE t_pays;
INSERT INTO t_pays VALUES ('fr', 'FR', 'France');
INSERT INTO t_pays VALUES ('ca', 'FR', 'Canada');
INSERT INTO t_pays VALUES ('uk', 'FR', 'Royaume-Uni');

TRUNCATE t_privilege;
insert into t_privilege (id_priv, id_lang, priv) values ('1', 'FR', 'Consultation');
insert into t_privilege (id_priv, id_lang, priv) values ('5', 'FR', 'Modification');


TRUNCATE t_proc;
-- IMPORTS
INSERT INTO t_proc VALUES (1, 'Import Video');
INSERT INTO t_proc VALUES (2, 'Import Image');
INSERT INTO t_proc VALUES (3, 'Import PDF');
INSERT INTO t_proc VALUES (4,'Import Document');
INSERT INTO t_proc VALUES (5,'Import Audio');
INSERT INTO t_proc VALUES (6,'Import DCP');


-- Livraisons / encodages videos
INSERT INTO t_proc VALUES (100, 'Livraison fichier d''origine');
INSERT INTO t_proc VALUES (101, 'HD - MOV - DNXHD120');
INSERT INTO t_proc VALUES (102, 'HD - MOV - DNXHD185x');
INSERT INTO t_proc VALUES (103, 'HD - MOV - HDV');
INSERT INTO t_proc VALUES (104, 'HD - MOV - ProRes');
INSERT INTO t_proc VALUES (105, 'HD - MOV - ProResHQ');
INSERT INTO t_proc VALUES (106, 'HD - MOV - XDCAM EX');
INSERT INTO t_proc VALUES (107, 'HD - MOV - XDCAM HD');
INSERT INTO t_proc VALUES (108, 'HD - MOV - XDCAM HD422');
INSERT INTO t_proc VALUES (109, 'HD - MXF - XDCAM HD422');
INSERT INTO t_proc VALUES (110, 'HD - MXF - H264 1080i High422Intra - 100Mb');
INSERT INTO t_proc VALUES (111, 'HD - MXF - H264 1080p High422Intra - 100Mb');
INSERT INTO t_proc VALUES (112, 'HD - MP4 - H264 1080p - 15Mb');
INSERT INTO t_proc VALUES (113, 'HD - MP4 - H264 1080p - 10Mb');
INSERT INTO t_proc VALUES (114, 'HD - MP4 - H264 1080p - 2Mb');
INSERT INTO t_proc VALUES (115, 'SD - 16/9 - MOV - DV');
INSERT INTO t_proc VALUES (116, 'SD - 4/3 - MOV - DV');
INSERT INTO t_proc VALUES (117, 'SD - 16/9 - MXF - IMX50');
INSERT INTO t_proc VALUES (118, 'SD - 4/3 - MXF - IMX50');
INSERT INTO t_proc VALUES (119, 'SD - 16/9 - MOV - IMX50');
INSERT INTO t_proc VALUES (120, 'SD - 4/3 - MOV - IMX50');
INSERT INTO t_proc VALUES (121, 'SD - 16/9 - MXF - MPEG2 - 15Mb');
INSERT INTO t_proc VALUES (122, 'SD - 4/3 - MXF - MPEG2 - 15Mb');
INSERT INTO t_proc VALUES (123, 'SD - 16/9 - MXF - MPEG2 Intra - 50Mb');
INSERT INTO t_proc VALUES (124, 'SD - 4/3 - MXF - MPEG2 Intra - 50Mb');
INSERT INTO t_proc VALUES (125, 'SD - 16/9 - MXF - H264 High422Intra - 25Mb');
INSERT INTO t_proc VALUES (126, 'SD - 4/3 - MXF - H264 High422Intra - 25Mb');
INSERT INTO t_proc VALUES (127, 'SD - 16/9 - MP4 - H264 - 3Mb');
INSERT INTO t_proc VALUES (128, 'SD - 4/3 - MP4 - H264 - 3Mb');
INSERT INTO t_proc VALUES (129, 'SD - 16/9 - MP4 - H264 - 1Mb');
INSERT INTO t_proc VALUES (130, 'SD - 4/3 - MP4 - H264 - 1Mb');
INSERT INTO t_proc VALUES (131, 'HD - MOV - ProResLT');
INSERT INTO t_proc VALUES (132, 'HD - MP4 - H264 1080p - 2Mb - TCI');
INSERT INTO t_proc VALUES (133, 'HD - MP4 - H264 720p - 1,5Mb');
INSERT INTO t_proc VALUES (134, 'SD - 16/9 - MOV - ProRes LT');
INSERT INTO t_proc VALUES (135, 'SD - 4/3 - MOV - ProRes LT');
INSERT INTO t_proc VALUES (136, 'SD - 16/9 - MOV - ProRes');
INSERT INTO t_proc VALUES (137, 'SD - 4/3 - MOV - ProRes');
INSERT INTO t_proc VALUES (138, 'SD - 16/9 - MOV - ProRes HQ');
INSERT INTO t_proc VALUES (139, 'SD - 4/3 - MOV - ProRes HQ');
INSERT INTO t_proc VALUES (140, 'SD - 16/9 - MPEG - MPEG2 - 15Mb');
INSERT INTO t_proc VALUES (141, 'SD - 4/3 - MPEG - MPEG2 - 15Mb');
INSERT INTO t_proc VALUES (142, 'SD - 16/9 - MPEG - MPEG2 Intra - 50Mb');
INSERT INTO t_proc VALUES (143, 'SD - 4/3 - MPEG - MPEG2 Intra - 50Mb');
INSERT INTO t_proc VALUES (144, 'SD - 16/9 - MP4 - H264 - 1Mb - TCI');
INSERT INTO t_proc VALUES (145, 'SD - 4/3 - MP4 - H264 - 1Mb - TCI');
--INSERT INTO t_proc VALUES (146, 'SD - 16/9 - DVD');
--INSERT INTO t_proc VALUES (147, 'SD - 4/3 - DVD');
--INSERT INTO t_proc VALUES (148, 'SD - 16/9 - DVD LP');
--INSERT INTO t_proc VALUES (149, 'SD - 4/3 - DVD LP');
INSERT INTO t_proc VALUES (150, 'HD - MOV - AVC-Intra 100');
INSERT INTO t_proc VALUES (151, 'HD - MXF - AVC-Intra 100');
INSERT INTO t_proc VALUES (152, 'HD - MP4 - H264 720p - 7Mb');
INSERT INTO t_proc VALUES (153, 'HD - MP4 - H264 720p - 12Mb');

INSERT INTO t_proc VALUES (160, 'Livraison fichier de visionnage');

-- Processus DVD
INSERT INTO t_proc VALUES (190, 'DVD - SD - 16/9 - PAL');
INSERT INTO t_proc VALUES (191, 'DVD - SD - 4/3 - PAL');
INSERT INTO t_proc VALUES (192, 'DVD - SD - 16/9 - NTSC');
INSERT INTO t_proc VALUES (193, 'DVD - SD - 4/3 - NTSC');
INSERT INTO t_proc VALUES (194, 'DVD - SD - 16/9 - PAL - loop');
INSERT INTO t_proc VALUES (195, 'DVD - SD - 4/3 - PAL - loop');
INSERT INTO t_proc VALUES (196, 'DVD - SD - 16/9 - NTSC - loop');
INSERT INTO t_proc VALUES (197, 'DVD - SD - 4/3 - NTSC - loop');

-- Encodage image liv
-- redondance avec livraison fichier d'origine, commenté pour l'instant, à supprimer si aucune utilité
-- INSERT INTO t_proc VALUES (200, 'THD - Fichier d''origine');
INSERT INTO t_proc VALUES (201, 'HD - JPEG');
INSERT INTO t_proc VALUES (202, 'MD - JPEG');
INSERT INTO t_proc VALUES (203, 'SD - JPEG');

TRUNCATE t_proc_etape;
-- imports (0+)
INSERT INTO t_proc_etape VALUES (1, 10, 1 , 0);
INSERT INTO t_proc_etape VALUES (1, 1, 2, 0);
INSERT INTO t_proc_etape VALUES (1, 2, 3 , 10);
INSERT INTO t_proc_etape VALUES (2, 5, 1, 0);
INSERT INTO t_proc_etape VALUES (2, 6, 2, 5);
INSERT INTO t_proc_etape VALUES (3, 3, 1, 0);
INSERT INTO t_proc_etape VALUES (3, 7, 2, 0);
INSERT INTO t_proc_etape VALUES (4, 4, 1, 0);
INSERT INTO t_proc_etape VALUES (4, 3, 2, 4);
INSERT INTO t_proc_etape VALUES (4, 7, 3, 4);
INSERT INTO t_proc_etape VALUES (5, 8, 1, 0);
INSERT INTO t_proc_etape VALUES (6, 9, 1, 0);
INSERT INTO t_proc_etape VALUES (6, 2, 2, 9);


-- encodage liv video (100+)
INSERT INTO t_proc_etape VALUES (100, 100, 1, 0);
INSERT INTO t_proc_etape VALUES (101, 101, 1, 0);
INSERT INTO t_proc_etape VALUES (102, 102, 1, 0);
INSERT INTO t_proc_etape VALUES (103, 103, 1, 0);
INSERT INTO t_proc_etape VALUES (104, 104, 1, 0);
INSERT INTO t_proc_etape VALUES (105, 105, 1, 0);
INSERT INTO t_proc_etape VALUES (106, 106, 1, 0);
INSERT INTO t_proc_etape VALUES (107, 107, 1, 0);
INSERT INTO t_proc_etape VALUES (108, 108, 1, 0);
INSERT INTO t_proc_etape VALUES (109, 109, 1, 0);
INSERT INTO t_proc_etape VALUES (110, 110, 1, 0);
INSERT INTO t_proc_etape VALUES (111, 111, 1, 0);
INSERT INTO t_proc_etape VALUES (112, 112, 1, 0);
INSERT INTO t_proc_etape VALUES (113, 113, 1, 0);
INSERT INTO t_proc_etape VALUES (114, 114, 1, 0);
INSERT INTO t_proc_etape VALUES (115, 115, 1, 0);
INSERT INTO t_proc_etape VALUES (116, 116, 1, 0);
INSERT INTO t_proc_etape VALUES (117, 117, 1, 0);
INSERT INTO t_proc_etape VALUES (118, 118, 1, 0);
INSERT INTO t_proc_etape VALUES (119, 119, 1, 0);
INSERT INTO t_proc_etape VALUES (120, 120, 1, 0);
INSERT INTO t_proc_etape VALUES (121, 121, 1, 0);
INSERT INTO t_proc_etape VALUES (122, 122, 1, 0);
INSERT INTO t_proc_etape VALUES (123, 123, 1, 0);
INSERT INTO t_proc_etape VALUES (124, 124, 1, 0);
INSERT INTO t_proc_etape VALUES (125, 125, 1, 0);
INSERT INTO t_proc_etape VALUES (126, 126, 1, 0);
INSERT INTO t_proc_etape VALUES (127, 127, 1, 0);
INSERT INTO t_proc_etape VALUES (128, 128, 1, 0);
INSERT INTO t_proc_etape VALUES (129, 129, 1, 0);
INSERT INTO t_proc_etape VALUES (130, 130, 1, 0);
INSERT INTO t_proc_etape VALUES (131, 131, 1, 0);
INSERT INTO t_proc_etape VALUES (132, 132, 1, 0);
INSERT INTO t_proc_etape VALUES (133, 133, 1, 0);
INSERT INTO t_proc_etape VALUES (134, 134, 1, 0);
INSERT INTO t_proc_etape VALUES (135, 135, 1, 0);
INSERT INTO t_proc_etape VALUES (136, 136, 1, 0);
INSERT INTO t_proc_etape VALUES (137, 137, 1, 0);
INSERT INTO t_proc_etape VALUES (138, 138, 1, 0);
INSERT INTO t_proc_etape VALUES (139, 139, 1, 0);
INSERT INTO t_proc_etape VALUES (140, 140, 1, 0);
INSERT INTO t_proc_etape VALUES (141, 141, 1, 0);
INSERT INTO t_proc_etape VALUES (142, 142, 1, 0);
INSERT INTO t_proc_etape VALUES (143, 143, 1, 0);
INSERT INTO t_proc_etape VALUES (144, 144, 1, 0);
INSERT INTO t_proc_etape VALUES (145, 145, 1, 0);
--INSERT INTO t_proc_etape VALUES (146, 146, 1, 0);
--INSERT INTO t_proc_etape VALUES (147, 147, 1, 0);
--INSERT INTO t_proc_etape VALUES (148, 148, 1, 0);
--INSERT INTO t_proc_etape VALUES (149, 149, 1, 0);
INSERT INTO t_proc_etape VALUES (150, 150, 1, 0);
INSERT INTO t_proc_etape VALUES (151, 151, 1, 0);
INSERT INTO t_proc_etape VALUES (152, 152, 1, 0);
INSERT INTO t_proc_etape VALUES (153, 153, 1, 0);

INSERT INTO t_proc_etape VALUES (160, 160, 1, 0);

-- encodage liv images (200+)
INSERT INTO t_proc_etape VALUES (200, 200, 1, 0);
INSERT INTO t_proc_etape VALUES (201, 201, 1, 0);
INSERT INTO t_proc_etape VALUES (202, 202, 1, 0);
INSERT INTO t_proc_etape VALUES (203, 203, 1, 0);

-- publication 300+
-- INSERT INTO t_proc_etape VALUES (300, 300, 1, 0);




TRUNCATE t_role;
insert into t_role (id_role, id_lang, role, id_type_desc) values ('ACH', 'FR', 'Achat','DEP');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('AUT', 'FR', 'Auteur','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('AYD', 'FR', 'Ayant droit','AYD');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('DEP', 'FR', 'Dépôt','DEP');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('DIS', 'FR', 'Distributeur','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('DON', 'FR', 'Don','DEP');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('EDI', 'FR', 'Editeur','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('ILL', 'FR', 'Illustrateur','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('IMA', 'FR', 'Image','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('IMP', 'FR', 'Imprimeur','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('INT', 'FR', 'Intervenant','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('MRQ', 'FR', 'Marque','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('MUS', 'FR', 'Musique','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('PHO', 'FR', 'Photographe','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('PRO', 'FR', 'Producteur','CTB');
insert into t_role (id_role, id_lang, role, id_type_desc) values ('REA', 'FR', 'Réalisateur','CTB');

TRUNCATE t_type_commande;
INSERT INTO t_type_commande VALUES (2, 'FR', 'Téléchargement');
INSERT INTO t_type_commande VALUES (4, 'FR', 'Image DVD');

TRUNCATE t_type_cat;
INSERT INTO t_type_cat VALUES ('THM', 'FR', 'Thème');
INSERT INTO t_type_cat VALUES ('UNE', 'FR', 'Accueil');

TRUNCATE t_type_desc;
insert into t_type_desc (id_type_desc, id_lang, type_desc) values ('AYD', 'FR', 'Ayant droit');
insert into t_type_desc (id_type_desc, id_lang, type_desc) values ('CTB', 'FR', 'contributeur(s)');
insert into t_type_desc (id_type_desc, id_lang, type_desc) values ('DEP', 'FR', 'déposant');

TRUNCATE t_type_doc;
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('1', 'FR', 'Film');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('2', 'FR', 'Photo');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('3', 'FR', 'Lanterne Magique');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('4', 'FR', 'Objet');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('5', 'FR', 'Film fixe');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('6', 'FR', 'Bibliothèque');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('7', 'FR', 'Archive Sonore');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('8', 'FR', 'Dossier');
insert into t_type_doc (id_type_doc, id_lang, type_doc) values ('9', 'FR', 'Affiche / Archive');

TRUNCATE t_type_import;
INSERT INTO t_type_import VALUES ('FTP', 'FR', 'Upload FTP');
INSERT INTO t_type_import VALUES ('DD', 'FR', 'Upload disque dur');
INSERT INTO t_type_import VALUES ('HTTP', 'FR', 'Chargement HTTP');
INSERT INTO t_type_import VALUES ('URL', 'FR', 'Chargement depuis URL');
INSERT INTO t_type_import VALUES ('DIR', 'FR', 'Import depuis Watchfolder');

TRUNCATE t_type_lex;
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('LG', 'FR', 'Lieu',1, 0);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('NC', 'FR', 'Nom commun',1, 0);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('PE', 'FR', 'Personnalité',1, 0);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('PP', 'FR', 'Personne',0, 1);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('AY', 'FR', 'Ayant-droit',0, 1);
insert into t_type_lex (id_type_lex, id_lang, type_lex,hierarchique,personne) values ('DE', 'FR', 'Déposant',0, 1);

TRUNCATE t_type_mat;
INSERT INTO t_type_mat VALUES ('COPIE');
INSERT INTO t_type_mat VALUES ('MASTER');
INSERT INTO t_type_mat VALUES ('VISIO');
INSERT INTO t_type_mat VALUES ('ORIGINAL');

TRUNCATE t_type_son;

TRUNCATE t_type_usager;
insert into t_type_usager (id_type_usager, type_usager) values ('1', 'simple');
insert into t_type_usager (id_type_usager, type_usager) values ('2', 'avancé');
insert into t_type_usager (id_type_usager, type_usager) values ('3', 'contributeur');
insert into t_type_usager (id_type_usager, type_usager) values ('4', 'administrateur');

TRUNCATE t_type_val;
insert into t_type_val (id_type_val, id_lang, type_val) values ('ATEL', 'FR', 'Intitulé de l''atelier');
insert into t_type_val (id_type_val, id_lang, type_val) values ('COUL', 'FR', 'Couleur');
insert into t_type_val (id_type_val, id_lang, type_val) values ('CSV', 'FR', 'Lieux de conservation');
insert into t_type_val (id_type_val, id_lang, type_val) values ('DESC', 'FR', 'Description');
insert into t_type_val (id_type_val, id_lang, type_val) values ('FIMA', 'FR', 'Format Image');
insert into t_type_val (id_type_val, id_lang, type_val) values ('FMT', 'FR', 'Format pellicule/vidéo/passe vue');
insert into t_type_val (id_type_val, id_lang, type_val) values ('FOND', 'FR', 'Fonds');
insert into t_type_val (id_type_val, id_lang, type_val) values ('GENR', 'FR', 'Genre');
insert into t_type_val (id_type_val, id_lang, type_val) values ('IMP', 'FR', 'Impression');
insert into t_type_val (id_type_val, id_lang, type_val) values ('LANG', 'FR', 'Langue');
insert into t_type_val (id_type_val, id_lang, type_val) values ('LOC', 'FR', 'Autre localisation');
insert into t_type_val (id_type_val, id_lang, type_val) values ('MAT', 'FR', 'Matériaux');
insert into t_type_val (id_type_val, id_lang, type_val) values ('MRQ', 'FR', 'Marque du support');
insert into t_type_val (id_type_val, id_lang, type_val) values ('ORI', 'FR', 'Support d''origine');
insert into t_type_val (id_type_val, id_lang, type_val) values ('PAYS', 'FR', 'Nationalité');
insert into t_type_val (id_type_val, id_lang, type_val) values ('PCD', 'FR', 'Procédé');
insert into t_type_val (id_type_val, id_lang, type_val) values ('PUBL', 'FR', 'Public');
insert into t_type_val (id_type_val, id_lang, type_val) values ('REST', 'FR', 'Restaurateur');
insert into t_type_val (id_type_val, id_lang, type_val) values ('SON', 'FR', 'Son');
insert into t_type_val (id_type_val, id_lang, type_val) values ('STAT', 'FR', 'Statut');
insert into t_type_val (id_type_val, id_lang, type_val) values ('STOK', 'FR', 'Stockage');
insert into t_type_val (id_type_val, id_lang, type_val) values ('SUP', 'FR', 'Type de support');
insert into t_type_val (id_type_val, id_lang, type_val) values ('TIMA', 'FR', 'Technique d''image');
insert into t_type_val (id_type_val, id_lang, type_val) values ('TIR', 'FR', 'Tirage réalisé par');
insert into t_type_val (id_type_val, id_lang, type_val) values ('TOBJ', 'FR', 'Type d''objet');
insert into t_type_val (id_type_val, id_lang, type_val) values ('TPEL', 'FR', 'Type de pellicule');
insert into t_type_val (id_type_val, id_lang, type_val) values ('TREA', 'FR', 'Techniques de réalisation');
insert into t_type_val (id_type_val, id_lang, type_val) values ('TYPE', 'FR', 'Type');


TRUNCATE t_usager;
INSERT INTO t_usager (id_usager, us_id_type_usager, us_id_etat_usager, us_nom, us_prenom, us_societe, us_soc_mail, us_login, us_password, us_salt) VALUES (1, 4, 2,  'Admin', 'Opsomai', 'Opsomai', 'pchampon@opsomai.com', 'admin', '50b378fdeb3167cebee030fd5302449e102591fe20e1e01aa6122e8cc4dfabe5', 'nNblUDSHRQr5z7hOEUlFKf2ibZM7X7I0G4o36k8hjdb9A038InKpft6tQPbOv5Y1');


TRUNCATE t_usager_groupe;
insert into t_usager_groupe values (1, 1);


truncate t_engine;
insert into t_engine select id_module,id_module,concat(module_nom,'_localhost'),'localhost',2 from t_module;


